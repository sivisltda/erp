<?php

session_start();
include ("configini.php");
include ("Connections/funcoesAux.php");
include ("util/util.php");

$contrato = isset($_POST["empresa"]) ? addslashes(trim($_POST["empresa"])) : FALSE;
$login = isset($_POST["login"]) ? addslashes(trim($_POST["login"])) : FALSE;
$senha = isset($_POST["password"]) ? trim($_POST["password"]) : FALSE;
$comeback = isset($_POST["comeback"]) ? addslashes(trim($_POST["comeback"])) : FALSE;
$_POST["txtTks"] = $_GET["ar"];
$mainPageGo = "";

if (!$login || !$senha || !$contrato) {
    if (!isset($_GET["ar"])) {
        if ($comeback) {
            echo "<script>alert('Digite seu login e senha corretamente!');window.top.location.href = '/Modulos/POS/Ponto-de-Venda.php';</script>";
        } else {
            echo "<script>alert('Digite seu login e senha corretamente!');window.top.location.href = 'login.php" .
            (isset($_POST["txtTks"]) ? "?ar=" . $_POST["txtTks"] : "") . "';</script>";
        }
    }
}

if (isset($_POST["txtTks"]) && strlen($_POST["txtTks"]) > 0) {
    $resp = explode("|", encrypt($_POST["txtTks"], "VipService123", false));
    if (count($resp) > 0) {
        $contrato = $resp[0];
        $login = $resp[1];
    }
}

$cur = odbc_exec($con, "select *, (select max(c.numero_contrato) numero_contrato 
from ca_contratos_dependente cd inner join ca_contratos c on cd.para_contrato = c.id
where cd.de_contrato = ca_contratos.id) contrato_dep
from ca_contratos left join sf_fornecedores_despesas on id_fornecedores_despesas = id_cliente
left join tb_estados on estado_codigo = estado left join tb_cidades on cidade_codigo = cidade
where inativa in (0,2) and numero_contrato = " . valoresTexto2($contrato));
while ($RFP = odbc_fetch_array($cur)) {
    $contrato = utf8_encode($RFP['numero_contrato']);
    $razao_social_contrato = utf8_encode($RFP['razao_social']);
    $hostname = utf8_encode($RFP['local']);
    $username = utf8_encode($RFP['login']);
    $password = utf8_encode($RFP['senha']);
    $database = utf8_encode($RFP['banco']);
    $_SESSION["cpf_cnpj"] = utf8_encode($RFP["cnpj"]);
    $_SESSION["modulos"] = utf8_encode($RFP["mdl_adm"]) . utf8_encode($RFP["mdl_fin"]) . utf8_encode($RFP["mdl_est"]) . 
    utf8_encode($RFP["mdl_nfe"]) . utf8_encode($RFP["mdl_crm"]) . utf8_encode($RFP["mdl_ser"]) . utf8_encode($RFP["mdl_aca"]) . 
    utf8_encode($RFP["mdl_tur"]) . utf8_encode($RFP["mdl_gbo"]) . utf8_encode($RFP["mdl_srs"]) . utf8_encode($RFP["mdl_esd"]) .
    utf8_encode($RFP["mdl_bodweb"]) . utf8_encode($RFP["mdl_clb"]) . utf8_encode($RFP["mdl_cnt"]) . utf8_encode($RFP["mdl_seg"]) . 
    utf8_encode($RFP['mdl_sau']) . utf8_encode($RFP['mdl_wha']) . utf8_encode($RFP['mdl_cht']);
    $mdl_acad_ = utf8_encode($RFP["mdl_aca"]);
    $_SESSION["contrato"] = utf8_encode($RFP['numero_contrato']);
    $_SESSION["contrato_dep"] = utf8_encode($RFP['contrato_dep']);
    $_SESSION["razao_social_contrato"] = utf8_encode($RFP['razao_social']);
    $_SESSION["nome_fantasia_contrato"] = utf8_encode($RFP['nome_fantasia']);
    $_SESSION["hostname"] = utf8_encode($RFP['local']);
    $_SESSION["username"] = utf8_encode($RFP['login']);
    $_SESSION["password"] = utf8_encode($RFP['senha']);
    $_SESSION["database"] = utf8_encode($RFP['banco']);
    $_SESSION["inativa"] = utf8_encode($RFP['inativa']);
    $_SESSION["mod_emp"] = utf8_encode($RFP['mdl_empresa']);
    $asscontrato = utf8_encode($RFP['contrato_usr']);
    $dataCntt = utf8_encode($RFP['contrato_dat']);
}
if ($contrato != '') {
    if (isset($_POST["txtTks"]) && strlen($_POST["txtTks"]) > 0) {
        $where = " and login_user = " . valoresTexto2($login);
        $cur = odbc_exec($con, "select id_usuario,nome,master,login_user,imagem, B.* from sf_usuarios A " .
                "inner join sf_filiais B on A.filial = B.id_filial where inativo = 0 " . $where);
        if (odbc_num_rows($cur) > 0) {
            $where = " and login_user = 'admin'";
        }
        if ($where == " and login_user = 'admin'") {
            $query = "INSERT INTO ca_acessos_logs(data_acao,sys_login,empresa_login,acao,ip) VALUES (GETDATE()," .
                    valoresTexto2($resp[1]) . "," . valoresTexto2($resp[0]) . ",'Login'," . valoresTexto2($_SERVER['REMOTE_ADDR']) . ")";
            odbc_exec($con, $query);
        }
    } else {
        $where = " and login_user = " . valoresTexto2($login) . " and senha = " . valoresTexto2(encrypt($senha, "VipService123", true));
    }
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password) or die(odbc_errormsg());
    $cur = odbc_exec($con, "select id_usuario,nome,master,login_user,filial,imagem,senha, B.*, P.out_pag_avancada, P.pag_avancada, sf_turnos.* " .
            "from sf_usuarios A inner join sf_filiais B on A.filial = B.id_filial " .
            "left join sf_usuarios_permissoes P on A.master = P.id_permissoes " .
            "left join sf_fornecedores_despesas on sf_fornecedores_despesas.id_fornecedores_despesas = A.funcionario " .
            "left join sf_turnos on sf_turnos.cod_turno = sf_fornecedores_despesas.fornecedores_hr " .
            "where A.inativo = 0 " . $where);
    if (odbc_num_rows($cur) == 0) {
        if ($comeback) {
            echo "<script>alert('Digite seu login e senha corretamente!');window.top.location.href = '/Modulos/POS/Ponto-de-Venda.php';</script>";
        } else {
            echo "<script>alert('Digite seu login e senha corretamente!');window.top.location.href = 'login.php" .
            (isset($_POST["txtTks"]) ? "?ar=" . $_POST["txtTks"] : "") . "';</script>";
        }
    } else {
        while ($RFP = odbc_fetch_array($cur)) {
            if (is_numeric($RFP['cod_turno'])) {
                if ((strlen($RFP['dia_semana1']) == 7 && substr($RFP['dia_semana1'], date('w'), 1) == "1" && strlen($RFP['faixa1_ini']) == 5 && strlen($RFP['faixa1_fim']) == 5 && ($RFP['faixa1_ini'] < date('H:i') && $RFP['faixa1_fim'] > date('H:i'))) ||                        
                    (strlen($RFP['dia_semana2']) == 7 && substr($RFP['dia_semana2'], date('w'), 1) == "1" && strlen($RFP['faixa2_ini']) == 5 && strlen($RFP['faixa2_fim']) == 5 && ($RFP['faixa2_ini'] < date('H:i') && $RFP['faixa2_fim'] > date('H:i'))) ||                        
                    (strlen($RFP['dia_semana3']) == 7 && substr($RFP['dia_semana3'], date('w'), 1) == "1" && strlen($RFP['faixa3_ini']) == 5 && strlen($RFP['faixa3_fim']) == 5 && ($RFP['faixa3_ini'] < date('H:i') && $RFP['faixa3_fim'] > date('H:i'))) ||                        
                    (strlen($RFP['dia_semana4']) == 7 && substr($RFP['dia_semana4'], date('w'), 1) == "1" && strlen($RFP['faixa4_ini']) == 5 && strlen($RFP['faixa4_fim']) == 5 && ($RFP['faixa4_ini'] < date('H:i') && $RFP['faixa4_fim'] > date('H:i'))) ||                        
                    (strlen($RFP['dia_semana5']) == 7 && substr($RFP['dia_semana5'], date('w'), 1) == "1" && strlen($RFP['faixa5_ini']) == 5 && strlen($RFP['faixa5_fim']) == 5 && ($RFP['faixa5_ini'] < date('H:i') && $RFP['faixa5_fim'] > date('H:i')))) {
                } else {
                    echo "<script>alert('Funcionário fora de horário!');window.top.location.href = 'login.php" . (isset($_POST["txtTks"]) ? "?ar=" . $_POST["txtTks"] : "") . "';</script>"; exit;                    
                }             
            }
            setcookie("cookie_user", utf8_encode($RFP['id_usuario']), time() + 172800);
            setcookie("cookie_contrato", $contrato, time() + 172800);
            $_SESSION["id_usuario"] = utf8_encode($RFP['id_usuario']);
            $_SESSION["nome_usuario"] = formatName(utf8_encode($RFP['nome']));
            $_SESSION["login_usuario"] = utf8_encode($RFP['login_user']);
            $_SESSION["permissao"] = utf8_encode($RFP['master']);
            $_SESSION["filial"] = utf8_encode($RFP['filial']);
            $_SESSION["imagem"] = utf8_encode($RFP['imagem']);
            $cabecalho = $_SESSION["razao_social_contrato"] . "<br>" . utf8_encode($RFP['endereco']) . " n°" . utf8_encode($RFP['numero']) . "," . utf8_encode($RFP['bairro']) . "<br>" . utf8_encode($RFP['cidade_nome']) . " " . utf8_encode($RFP['estado']) . " - CEP: " . utf8_encode($RFP["cep"]) . "<br>CNPJ: " . utf8_encode($RFP["cnpj"]) . " * IE: " . utf8_encode($RFP["inscricao_estadual"]) . "<br>" . utf8_encode($RFP["telefone"] . " " . strtolower(utf8_encode($RFP["site"])));
            $_SESSION["cabecalho"] = $cabecalho;
            if ($RFP['master'] > 0) {
                $mainPageGo = ($RFP['out_pag_avancada'] == 1 && $RFP['pag_avancada'] == 1 ? "index2.php" : "index.php");
            }
        }
        $query = "INSERT INTO sf_login_logs(data_acao,sys_login,ip) VALUES (GETDATE()," . valoresTexto2($login) . "," . valoresTexto2($_SERVER['REMOTE_ADDR']) . ")";
        odbc_exec($con, $query);
        $cur = odbc_exec($con, "select pag_avancada_adm from sf_configuracao where id = 1");
        while ($RFP = odbc_fetch_array($cur)) {
            if ($mainPageGo == "" && $RFP['pag_avancada_adm'] == 1) {
                $mainPageGo = "index2.php";
            } elseif ($mainPageGo == "") {
                $mainPageGo = "index.php";
            }
        }
        if ($asscontrato == $_SESSION["id_usuario"] && $dataCntt == '') {
            header("Location: /Modulos/Geral/contrato.php?bd=" . $contrato);
        } else {
            if ($mdl_acad_ == "1") {
                header("Location: /Modulos/Geral/baixaCReceber.php");
                exit;
            }
            if ($comeback) {
                header("Location: /Modulos/POS/Ponto-de-Venda.php");
            } else {
                header("Location: " . $mainPageGo . (isset($_POST["txtTks"]) ? "?ar=" . $_POST["txtTks"] : ""));
            }
        }
        exit;
    }
}
odbc_close($con);
