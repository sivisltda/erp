<div class="indcont" style="width:calc(49.5% - 2px)">    
    <div class="indhead">Acesso Rápido</div>
    <div class="indicon">
        <div class="box-25"
        <?php
            if ($ckb_adm_cli_ == 0) {
                echo "style=\"background:#999\"";
            } else {
                echo "onClick=\"window.top.location.href = 'Modulos/Comercial/Gerenciador-Clientes.php'\"";
            }
            ?>>
            <div class="box-icon"><i class="material-icons">person</i></div>
            <div class="box-text">CLIENTES</div>
        </div>
        <div class="box-25"
        <?php
            if ($ckb_crm_pro_ == 0) {
                echo "style=\"background:#999\"";
            } else {
                echo "onClick=\"window.top.location.href = 'Modulos/CRM/Gerenciador-Prospects.php'\"";
            }
            ?>>
            <div class="box-icon"><i class="material-icons">person_add</i></div>
            <div class="box-text">PROSPECTS</div>
        </div>
        <div id="icobox" class="box-25" <?php
            if ($ckb_fin_rec_ == 0) {
                echo "style=\"background:#999\"";
            } else {
                echo "onClick=\"window.top.location.href = 'Modulos/Contas-a-pagar/Contas-a-Receber.php?id=1&tp=0'\"";
            }
            ?>>
            <div id="icocnt">
                <div class="box-icon"><i class="material-icons">download</i></div>
                <div class="box-text">A RECEBER</div>
            </div>
        </div>
        <div class="box-25" <?php
            if ($ckb_fin_pag_ == 0) {
                echo "style=\"background:#999\"";
            } else {
                echo "onClick=\"window.top.location.href = 'Modulos/Contas-a-pagar/Contas-a-Pagar.php?id=1&tp=0'\"";
            }
            ?>>
            <div class="box-icon"><i class="material-icons">upload</i></div>
            <div class="box-text">A PAGAR</div>
        </div>
        <?php if ($mdl_seg_ == 1) { ?>
        <div class="box-50"
        <?php
            if ($ckb_pro_veiculo_ == 0) {
                echo "style=\"background:#999\"";
            } else {
                echo "onClick=\"window.top.location.href = 'Modulos/Seguro/Veiculos.php'\"";
            }
            ?>>
            <div class="box-icon"><i class="material-icons">traffic_jam</i></div>
            <div class="box-text">VEÍCULOS</div>
        </div>        
        <div class="box-25" <?php
            if ($ckb_pro_vistoria_ == 0) {
                echo "style=\"background:#999\"";
            } else {
                echo "onClick=\"window.top.location.href = 'Modulos/Seguro/Vistorias.php'\"";
            }
            ?>>
            <div class="box-icon"><i class="material-icons">checklist</i></div>
            <div class="box-text">VISTORIAS</div>
        </div>
        <div class="box-25" <?php
            if ($ckb_pro_sinistro_ == 0) {
                echo "style=\"background:#999\"";
            } else {
                echo "onClick=\"window.top.location.href = 'Modulos/Seguro/Sinistros.php'\"";
            }
            ?>>
            <div class="box-icon"><i class="material-icons">auto_towing</i></div>
            <div class="box-text">EVENTOS</div>
        </div>
        <?php } else { ?>
        <div class="box-50"
        <?php
            if ($ckb_clb_gconvite_ == 0) {
                echo "style=\"background:#999\"";
            } else {
                echo "onClick=\"window.top.location.href = 'Modulos/Clube/Gerar-Convites.php'\"";
            }
            ?>>
            <div class="box-icon"><i class="material-icons">calendar_month</i></div>
            <div class="box-text">AGENDA</div>
        </div>        
        <div class="box-25" <?php
            if ($ckb_clb_baixar_layout_ == 0) {
                echo "style=\"background:#999\"";
            } else {
                echo "onClick=\"window.top.location.href = 'Modulos/Clube/Baixar-Layout.php'\"";
            }
            ?>>
            <div class="box-icon"><i class="material-icons">tv_options_edit_channels</i></div>
            <div class="box-text">BAIXAR LAYOUT</div>
        </div>
        <div class="box-25" <?php
            if ($ckb_aca_turmas_ == 0) {
                echo "style=\"background:#999\"";
            } else {
                echo "onClick=\"window.top.location.href = 'Modulos/Academia/TurmasList.php'\"";
            }
            ?>>
            <div class="box-icon"><i class="material-icons">groups</i></div>
            <div class="box-text">TURMAS</div>
        </div>        
        <?php } ?>
        <div style="clear:both"></div>
    </div>
</div>
<div class="indcont" style="width:calc(49.5% - 2px); margin-left:1%">       
    <div class="indhead">Bloco de Notas
        <span style="float:right">
            <button class="button" style="border:0; background: var(--cor-secundaria);" type="button" id="bntSave" name="bntSave">
                <span class="ico-ok icon-white"></span>
            </button>
        </span>
    </div>
    <div id="notas" class="indicon">
        <textarea name="txt_notas" id="txt_notas" class="box-note" rows="10" maxlength="4096"><?php
            $cur = odbc_exec($con, "select bloco_notas from sf_usuarios where id_usuario = '" . $_SESSION["id_usuario"] . "'");
            while ($RFP = odbc_fetch_array($cur)) {
                echo utf8_encode($RFP["bloco_notas"]);
            }
            ?></textarea>
    </div>
</div>
<div class="content" style="width: 24%;float: left;margin-top: 10px;">
    <div id="bx" class="box30">
        <div id="bx-01" class="boxhead" style="">
            <div style="height: 29px;overflow: hidden;" class="boxtext">
                <select id="txtTipo" name="txtTipo" class="select" style="width: 80%;">
                    <option value="0" selected>Agendas</option>
                    <?php if ($mdl_tur_ > 0) { ?>
                        <option value="1">Turmas</option>
                    <?php } ?>
                </select>
                <span style="cursor: pointer;" class="ico-edit" onclick="alterar()"></span>
                <span style="cursor: pointer;" class="ico-printer" onclick="printCalendar()"></span>                
            </div>
            <input id="txtAgenda" type="hidden" value="" />
        </div>
        <div id="bx-02" style="min-height: 520px;background-color: #F2F2F2;border: 1px solid #D8D8D8;">
            <table id="tb_agenda" class="table" style="float:none;text-align: center;" cellpadding="0" cellspacing="0" width="100%">
            </table>
        </div>
    </div>
</div>
<div style="width: 75%;float: left;margin-top: 10px; margin-left: 1%">
    <div id='calendar' style="background-color: white;float: right;width: 100%;"></div>
</div>