<?php

$lang['dmy'] = "m/d/Y";
$lang['prefix'] = "$ ";

function valoresNumericos2($nome) {
    $xTeste = str_replace(",", "", str_replace("%", "", str_replace("$ ", "", $nome)));
    if (!is_numeric($xTeste)) {
        return 0;
    } else {
        return $xTeste;
    }
}

function valoresData2($nome) {
    $explode = explode("/", str_replace("_", "/", $nome));
    if (count($explode) == 3) {
        if (checkdate($explode[0], $explode[1], $explode[2])) {
            return "'" . $explode[1] . "/" . $explode[0] . "/" . $explode[2] . "'";
        } else {
            return "null";
        }
    } else {
        return "null";
    }
}

function escreverNumero($nome, $ident = 0, $decimal = 2, $dec_point = ".", $thousands_sep = ",") {
    return ($ident == 1 ? "$ " : "") . number_format($nome, $decimal, $dec_point, $thousands_sep);
}

function escreverData($data, $mask = "m/d/Y") {
    return ($data == "" ? "" : date_format(date_create($data), $mask));
}

function escreverDataHora($data) {
    return ($data == "" ? "" : date_format(date_create($data), 'm/d/Y H:i'));
}

function getData($type, $tag = "") {
    if ($type == "B") {
        return $tag == "" ? date("m/01/Y") : date("m/01/Y", strtotime($tag));
    } elseif ($type == "T") {
        return $tag == "" ? date("m/d/Y") : date("m/d/Y", strtotime($tag));
    } elseif ($type == "E") {
        return $tag == "" ? date("m/t/Y") : date("m/t/Y", strtotime($tag));
    }
}

function geraTimestamp($data) {
    $partes = explode('/', $data);
    if (count($partes) == 3) {
        return mktime(0, 0, 0, $partes[0], $partes[1], $partes[2]);
    } else {
        return 0;
    }    
}

function escreverDataSoma($data, $tag, $mask = "m/d/Y") {
    $partes = explode('/', str_replace("'", "", $data));
    $date = date_create($partes[2] . "-" . $partes[0] . "-" . $partes[1]);
    date_add($date, date_interval_create_from_date_string($tag));
    return date_format($date, $mask);
}
