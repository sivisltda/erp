<?php
$protocolo = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http');
$host = $_SERVER['HTTP_HOST'];
$UrlAtual = $protocolo . '://' . $host;
$SERVER_PATH = $_SERVER['PHP_SELF'];
$SERVER_FILE = basename($SERVER_PATH);

function returnPart1($nome,$posicao){
    if (is_numeric(substr($nome, $posicao, 1))){
        return substr($nome, $posicao, 1);
    }else{
        return "1";
    }
}

$mdl_adm_ = returnPart1($_SESSION["modulos"],0);
$mdl_fin_ = returnPart1($_SESSION["modulos"],1);
$mdl_est_ = returnPart1($_SESSION["modulos"],2);
$mdl_nfe_ = returnPart1($_SESSION["modulos"],3);
$mdl_crm_ = returnPart1($_SESSION["modulos"],4);
$mdl_ser_ = returnPart1($_SESSION["modulos"],5);
$mdl_aca_ = returnPart1($_SESSION["modulos"],6);
$mdl_tur_ = returnPart1($_SESSION["modulos"],7);
$mdl_gbo_ = returnPart1($_SESSION["modulos"],8);
$mdl_srs_ = returnPart1($_SESSION["modulos"],9);
$mdl_esd_ = returnPart1($_SESSION["modulos"],10);
$mdl_bod_ = returnPart1($_SESSION["modulos"],11);
$mdl_clb_ = returnPart1($_SESSION["modulos"],12);
$mdl_cnt_ = returnPart1($_SESSION["modulos"],13);
$mdl_seg_ = returnPart1($_SESSION["modulos"],14);
$mdl_sau_ = returnPart1($_SESSION["modulos"],15);
$mdl_wha_ = returnPart1($_SESSION["modulos"],16);
$mdl_cht_ = returnPart1($_SESSION["modulos"],17);

switch ($SERVER_FILE) {
    case "index.php":
    case "index2.php":
        echo "<script>window.onload = function(){ document.getElementById('ind_1').setAttribute('style','font-weight:bold; color:white;'); }</script>";
        break;
    case "BI-InformacoesGerenciais.php":
        echo "<script>window.onload = function(){ document.getElementById('ind_1').setAttribute('style','font-weight:bold; color:white;'); }</script>";
        break;
    case "BI-Previsao-Financeira.php":
        echo "<script>window.onload = function(){ document.getElementById('bi_2').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "BI-Centro-de-Custos.php":
        echo "<script>window.onload = function(){ document.getElementById('bi_3').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "BI-Fluxo-de-Caixa.php":
        echo "<script>window.onload = function(){ document.getElementById('bi_4').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "BI-Faturamento.php":
        echo "<script>window.onload = function(){ document.getElementById('bi_5').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "BI-Tipos-de-Documentos.php":
        echo "<script>window.onload = function(){ document.getElementById('bi_6').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "BI-Produtividade-Funcionario.php":
        echo "<script>window.onload = function(){ document.getElementById('bi_7').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "BI-Gestao-de-Vendas.php":
        echo "<script>window.onload = function(){ document.getElementById('bi_10').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "BI-Mapa.php":
        echo "<script>window.onload = function(){ document.getElementById('bi_22').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "Controle-de-Atendimentos.php":
        echo "<script>window.onload = function(){ document.getElementById('bi_11').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "BI-Leads.php":
        echo "<script>window.onload = function(){ document.getElementById('bi_21').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "BI-Prospects.php":
        echo "<script>window.onload = function(){ document.getElementById('bi_12').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "Gerenciador-Agenda-Compromissos.php":
        echo "<script>window.onload = function(){ document.getElementById('bi_15').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "BI-Turmas.php":
        echo "<script>window.onload = function(){ document.getElementById('bi_13').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "BI-Auditoria.php":
        echo "<script>window.onload = function(){ document.getElementById('bi_14').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "BI-Matriculas-Renovacoes.php":
        echo "<script>window.onload = function(){ document.getElementById('bi_16').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "BI-Comparativo-Faturamento.php":
        echo "<script>window.onload = function(){ document.getElementById('bi_17').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "BI-Churn.php":
        echo "<script>window.onload = function(){ document.getElementById('bi_18').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "BI-Inadimplencia.php":
        echo "<script>window.onload = function(){ document.getElementById('bi_20').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;    
    case "BI-Evasao.php":
        echo "<script>window.onload = function(){ document.getElementById('bi_19').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "Faq.php":
        echo "<script>window.onload = function(){ document.getElementById('ind_3').setAttribute('style','font-weight:bold; color:white;'); }</script>";
        break;
    case "Gerenciador-Clientes.php":
        echo "<script>window.onload = function(){ document.getElementById('adm_1').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "Fornecedores-de-despesas.php":
        echo "<script>window.onload = function(){ document.getElementById('adm_2').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "Funcionarios.php":
        echo "<script>window.onload = function(){ document.getElementById('adm_3').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "FuncionariosForm.php":
        echo "<script>window.onload = function(){ document.getElementById('adm_3').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "Usuarios.php":
        echo "<script>window.onload = function(){ document.getElementById('adm_4').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "Permissoes.php":
        echo "<script>window.onload = function(){ document.getElementById('adm_5').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "Grupo-de-pessoas.php":
        echo "<script>window.onload = function(){ document.getElementById('adm_6').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "Departamentos.php":
        echo "<script>window.onload = function(){ document.getElementById('adm_7').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "FormConfiguracao.php":
        echo "<script>window.onload = function(){ document.getElementById('adm_8').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "Contratos.php":
        echo "<script>window.onload = function(){ document.getElementById('adm_13').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "Contrato_make.php":
        echo "<script>window.onload = function(){ document.getElementById('adm_13').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "EmailsList.php":
        echo "<script>window.onload = function(){ document.getElementById('crm_11').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('crm_0').setAttribute('class','active'); }</script>";
        break;
    case "DisparosAutoList.php":
        echo "<script>window.onload = function(){ document.getElementById('crm_12').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('crm_0').setAttribute('class','active'); }</script>";
        break;
    case "Gerenciador-Agenda.php":
        echo "<script>window.onload = function(){ document.getElementById('adm_11').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "Parentesco.php":
        echo "<script>window.onload = function(){ document.getElementById('adm_12').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "Tipos-de-Documentos.php":
        echo "<script>window.onload = function(){ document.getElementById('fin_1').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "Bancos.php":
        echo "<script>window.onload = function(){ document.getElementById('fin_2').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "Centro-custo.php":
        echo "<script>window.onload = function(){ document.getElementById('fin_22').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "Grupo-de-contas.php":
        echo "<script>window.onload = function(){ document.getElementById('fin_3').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "Contas-de-Movimentos.php":
        echo "<script>window.onload = function(){ document.getElementById('fin_4').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;    
    case "ComissaoClientes.php":
        echo "<script>window.onload = function(){ document.getElementById('fin_21').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;        
    case "Lancamento-de-Movimentos.php":
    case "Lancamento-de-Renegociacao.php":
        if($_GET['tp']=="1"){
            echo "<script>window.onload = function(){ document.getElementById('fin_5').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('fin_0').setAttribute('class','active'); }</script>";
            break;
        }else{
            echo "<script>window.onload = function(){ document.getElementById('fin_17').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('fin_0').setAttribute('class','active'); }</script>";
            break;
        }
    case "Solicitacao-de-Autorizacao.php":
    case "FormSolicitacao-de-Autorizacao.php":
        echo "<script>window.onload = function(){ document.getElementById('fin_6').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('fin_0').setAttribute('class','active'); }</script>";
        break;
    case "Contas-a-Pagar.php":
        echo "<script>window.onload = function(){ document.getElementById('fin_7').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('fin_0').setAttribute('class','active'); }</script>";
        break;
    case "Contas-a-Receber.php":
        echo "<script>window.onload = function(){ document.getElementById('fin_8').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('fin_0').setAttribute('class','active'); }</script>";
        break;
    case "Fluxo-de-Caixa.php":
        echo "<script>window.onload = function(){ document.getElementById('fin_9').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('fin_0').setAttribute('class','active'); }</script>";
        break;
    case "Relatorio-Grupo-de-Contas.php":
        echo "<script>window.onload = function(){ document.getElementById('fin_10').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('fin_0').setAttribute('class','active'); }</script>";
        break;
    case "Relatorio-Tipos-de-Documentos.php":
        echo "<script>window.onload = function(){ document.getElementById('fin_13').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('fin_0').setAttribute('class','active'); }</script>";
        break;
    case "Relatorio-Movimento-Caixa.php":
        echo "<script>window.onload = function(){ document.getElementById('fin_14').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('fin_0').setAttribute('class','active'); }</script>";
        break;
    case "Relatorio-Boletos-Registrados.php":
        echo "<script>window.onload = function(){ document.getElementById('fin_12').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('fin_0').setAttribute('class','active'); }</script>";
        break;
    case "Cheques.php":
        echo "<script>window.onload = function(){ document.getElementById('fin_11').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('fin_0').setAttribute('class','active'); }</script>";
        break;
    case "Cartoes.php":
        echo "<script>window.onload = function(){ document.getElementById('fin_19').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('fin_0').setAttribute('class','active'); }</script>";
        break;
    case "Email-Cobranca.php":
        echo "<script>window.onload = function(){ document.getElementById('siv_8').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('siv_0').setAttribute('class','active'); }</script>";
        break;
    case "Comissoes.php":
        echo "<script>window.onload = function(){ document.getElementById('est_9').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('fin_0').setAttribute('class','active'); }</script>";
        break;
    case "Vendas.php":
    case "FormVendas.php":
        echo "<script>window.onload = function(){ document.getElementById('est_5').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('fin_0').setAttribute('class','active'); }</script>";
        break;
    case "Requisicoes.php":
    case "FormRequisicoes.php":
        echo "<script>window.onload = function(){ document.getElementById('est_14').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('est_0').setAttribute('class','active'); }</script>";
        break;
    case "Cotacoes.php":
    case "FormCotacoes.php":
        echo "<script>window.onload = function(){ document.getElementById('est_17').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('est_0').setAttribute('class','active'); }</script>";
        break;
    case "Orcamentos.php":
    case "FormOrcamentos.php":
        echo "<script>window.onload = function(){ document.getElementById('est_6').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('fin_0').setAttribute('class','active'); }</script>";
        break;
    case "Retorno-Banco.php":
        echo "<script>window.onload = function(){ document.getElementById('fin_16').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('fin_0').setAttribute('class','active'); }</script>";
        break;
    case "Compras.php":
    case "FormCompras.php":
        echo "<script>window.onload = function(){ document.getElementById('est_7').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('est_0').setAttribute('class','active'); }</script>";
        break;
    case "Entradas-Saidas.php":
    case "FormEntradas-Saidas.php":
        echo "<script>window.onload = function(){ document.getElementById('est_15').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('est_0').setAttribute('class','active'); }</script>";
        break;
    case "Grupo-de-estoque.php":
        if($_GET['tp'] == "0") {
            echo "<script>window.onload = function(){ document.getElementById('est_1').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('est_0').setAttribute('class','active'); }</script>";
            break;
        } else if($mdl_aca_ == 1 && $_GET['tp'] == "1") {
            echo "<script>window.onload = function(){ document.getElementById('aca_10').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('aca_0').setAttribute('class','active'); }</script>";
            break;
        } else if($mdl_aca_ == 0 && $_GET['tp'] == "1") {
            echo "<script>window.onload = function(){ document.getElementById('ser_6').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('ser_0').setAttribute('class','active'); }</script>";
            break;
        } else if($_GET['tp'] == "2") {
            echo "<script>window.onload = function(){ document.getElementById('car_2').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('car_0').setAttribute('class','active'); }</script>";
            break;            
        }
    case "Produtos.php":
        echo "<script>window.onload = function(){ document.getElementById('est_2').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('est_0').setAttribute('class','active'); }</script>";
        break;
    case "Servicos.php":
        if ($mdl_aca_ == 0) {
            echo "<script>window.onload = function(){ document.getElementById('ser_4').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('ser_0').setAttribute('class','active'); }</script>";
            break;
        } else {
            echo "<script>window.onload = function(){ document.getElementById('aca_11').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('aca_0').setAttribute('class','active'); }</script>";
            break;
        }
    case "Planos.php":
        echo "<script>window.onload = function(){ document.getElementById('est_13').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('aca_0').setAttribute('class','active'); }</script>";
        break;
    case "Localizacoes.php":
        echo "<script>window.onload = function(){ document.getElementById('est_4').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('est_0').setAttribute('class','active'); }</script>";
        break;
    case "Cores.php":
        echo "<script>window.onload = function(){ document.getElementById('est_11').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('est_0').setAttribute('class','active'); }</script>";
        break;
    case "Tamanhos.php":
        echo "<script>window.onload = function(){ document.getElementById('est_10').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('est_0').setAttribute('class','active'); }</script>";
        break;
    case "Fabricantes.php":
        echo "<script>window.onload = function(){ document.getElementById('est_12').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('est_0').setAttribute('class','active'); }</script>";
        break;
    case "Justificativas.php":
        echo "<script>window.onload = function(){ document.getElementById('est_16').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('est_0').setAttribute('class','active'); }</script>";
        break;
    case "Fluxo-de-Estoque.php":
        echo "<script>window.onload = function(){ document.getElementById('est_8').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('est_0').setAttribute('class','active'); }</script>";
        break;
    case "Cenarios.php":
        echo "<script>window.onload = function(){ document.getElementById('nfe_1').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('nfe_0').setAttribute('class','active'); }</script>";
        break;
    case "Transportadoras.php":
        echo "<script>window.onload = function(){ document.getElementById('nfe_2').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('nfe_0').setAttribute('class','active'); }</script>";
        break;
    case "NF-Mensagens.php":
        echo "<script>window.onload = function(){ document.getElementById('nfe_3').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('nfe_0').setAttribute('class','active'); }</script>";
        break;
    case "Emitentes.php":
        echo "<script>window.onload = function(){ document.getElementById('nfe_4').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('nfe_0').setAttribute('class','active'); }</script>";
        break;
    case "Estados.php":
        echo "<script>window.onload = function(){ document.getElementById('nfe_9').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('nfe_0').setAttribute('class','active'); }</script>";
        break;
    case "Cidades.php":
        echo "<script>window.onload = function(){ document.getElementById('nfe_5').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('nfe_0').setAttribute('class','active'); }</script>";
        break;
    case "Enquadramento-Servico.php":
        echo "<script>window.onload = function(){ document.getElementById('nfe_8').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('nfe_0').setAttribute('class','active'); }</script>";
        break;
    case "Emitir-Nota.php":
    case "FormEmitir-Nota.php":
    case "Xml-Arquivos.php":
        echo "<script>window.onload = function(){ document.getElementById('nfe_6').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('nfe_0').setAttribute('class','active'); }</script>";
        break;
    case "FormEmitir-Nota-Servico.php":
    case "Emitir-Nota-Servico.php":
        echo "<script>window.onload = function(){ document.getElementById('nfe_7').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('nfe_0').setAttribute('class','active'); }</script>";
        break;
    case "Procedencia-de-clientes.php":
        echo "<script>window.onload = function(){ document.getElementById('crm_1').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "Fechamento-de-chamados.php":
        echo "<script>window.onload = function(){ document.getElementById('crm_10').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "Indicadores.php":
    case "FormIndicadores.php":        
        echo "<script>window.onload = function(){ document.getElementById('crm_8').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "Recepcao.php":
        echo "<script>window.onload = function(){ document.getElementById('crm_9').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('crm_0').setAttribute('class','active'); }</script>";
        break;
    case "Lead.php":
        echo "<script>window.onload = function(){ document.getElementById('crm_13').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('crm_0').setAttribute('class','active'); }</script>";
        break;
    case "Lead-Campanha.php":
        echo "<script>window.onload = function(){ document.getElementById('crm_14').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('crm_0').setAttribute('class','active'); }</script>";
        break;
    case "ChatbotList.php":
        echo "<script>window.onload = function(){ document.getElementById('crm_15').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('crm_0').setAttribute('class','active'); }</script>";
        break;
    case "Gerenciador-Prospects.php":
        echo "<script>window.onload = function(){ document.getElementById('crm_2').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('crm_0').setAttribute('class','active'); }</script>";
        break;
    case "ProspectsForm.php":
        echo "<script>window.onload = function(){ document.getElementById('crm_2').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('crm_0').setAttribute('class','active'); }</script>";
        break;
    case "CRMForm.php":
        echo "<script>window.onload = function(){ document.getElementById('adm_1').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "Telemarketing.php":
        echo "<script>window.onload = function(){ document.getElementById('crm_3').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('crm_0').setAttribute('class','active'); }</script>";
        break;
    case "Especificacoes.php":
        echo "<script>window.onload = function(){ document.getElementById('crm_5').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "ServicosSer.php":
        echo "<script>window.onload = function(){ document.getElementById('ser_1').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('ser_0').setAttribute('class','active'); }</script>";    
        break;
    case "Servicos_Atendimentos.php":
        echo "<script>window.onload = function(){ document.getElementById('ser_1').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('ser_0').setAttribute('class','active'); }</script>";
        break;
    case "Gerenciador-ClientesSer.php":
        echo "<script>window.onload = function(){ document.getElementById('ser_2').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('ser_0').setAttribute('class','active'); }</script>";
        break;
    case "Relatorio-Pacotes.php":
        echo "<script>window.onload = function(){ document.getElementById('ser_5').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('ser_0').setAttribute('class','active'); }</script>";
        break;
    case "CadAcad.php":
        echo "<script>window.onload = function(){ document.getElementById('siv_1').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('siv_0').setAttribute('class','active'); }</script>";
        break;
    case "CadERP.php":
        echo "<script>window.onload = function(){ document.getElementById('siv_5').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('siv_0').setAttribute('class','active'); }</script>";
        break;
    case "GerarSenha.php":
        echo "<script>window.onload = function(){ document.getElementById('siv_2').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('siv_0').setAttribute('class','active'); }</script>";
        break;
    case "LogERP.php":
        echo "<script>window.onload = function(){ document.getElementById('siv_9').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('siv_0').setAttribute('class','active'); }</script>";
        break;
    case "SaldoDCC.php":
        echo "<script>window.onload = function(){ document.getElementById('siv_3').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('siv_0').setAttribute('class','active'); }</script>";
        break;
    case "SaldoSMS.php":
        echo "<script>window.onload = function(){ document.getElementById('siv_6').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('siv_0').setAttribute('class','active'); }</script>";
        break;
    case "UsuarioPortal.php":
        echo "<script>window.onload = function(){ document.getElementById('siv_4').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('siv_0').setAttribute('class','active'); }</script>";
        break;
    case "CadastrosPendentesGoBody.php":
        echo "<script>window.onload = function(){ document.getElementById('siv_7').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('siv_0').setAttribute('class','active'); }</script>";
        break;
    case "ClientesForm.php":
        echo "<script>window.onload = function(){ document.getElementById('adm_1').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('adm_0').setAttribute('class','active'); }</script>";
        break;
    case "HorariosList.php":
        echo "<script>window.onload = function(){ document.getElementById('aca_2').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('aca_0').setAttribute('class','active'); }</script>";
        break;
    case "ConveniosList.php":
        echo "<script>window.onload = function(){ document.getElementById('aca_3').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('aca_0').setAttribute('class','active'); }</script>";
        break;
    case "CredenciaisList.php":
        echo "<script>window.onload = function(){ document.getElementById('aca_4').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('aca_0').setAttribute('class','active'); }</script>";
        break;
    case "CatracasList.php":
        echo "<script>window.onload = function(){ document.getElementById('aca_5').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('aca_0').setAttribute('class','active'); }</script>";
        break;
    case "LeitorFacial.php":
        echo "<script>window.onload = function(){ document.getElementById('aca_16').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('aca_0').setAttribute('class','active'); }</script>";
        break;
    case "CartaoList.php":
        echo "<script>window.onload = function(){ document.getElementById('aca_15').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('aca_0').setAttribute('class','active'); }</script>";
        break;
    case "AmbientesList.php":
        echo "<script>window.onload = function(){ document.getElementById('aca_6').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('aca_0').setAttribute('class','active'); }</script>";
        break;
    case "FeriasForm.php":
        echo "<script>window.onload = function(){ document.getElementById('aca_7').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('aca_0').setAttribute('class','active'); }</script>";
        break;
    case "AcessosRelatorio.php":
        echo "<script>window.onload = function(){ document.getElementById('aca_8').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "GrupoPlanosList.php":
        echo "<script>window.onload = function(){ document.getElementById('aca_9').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('aca_0').setAttribute('class','active'); }</script>";
        break;
    case "TurmasForm.php":
    case "TurmasList.php":
    case "MapaTurmas.php":
    echo "<script>window.onload = function(){ document.getElementById('aca_12').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('aca_0').setAttribute('class','active'); }</script>";
        break;
    case "DocumentosList.php":
        echo "<script>window.onload = function(){ document.getElementById('aca_13').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('aca_0').setAttribute('class','active'); }</script>";
        break;
    case "MapaEstudio.php":
        echo "<script>window.onload = function(){ document.getElementById('aca_14').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('aca_0').setAttribute('class','active'); }</script>";
        break;
    case "Acessorios.php":
        echo "<script>window.onload = function(){ document.getElementById('car_1').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('car_0').setAttribute('class','active'); }</script>";
        break;
    case "VistoriaItens.php":
        echo "<script>window.onload = function(){ document.getElementById('car_4').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('car_0').setAttribute('class','active'); }</script>";
        break;
    case "VeiculoTipo.php":
        echo "<script>window.onload = function(){ document.getElementById('car_8').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('car_0').setAttribute('class','active'); }</script>";
        break;
    case "VistoriaTipo.php":
        echo "<script>window.onload = function(){ document.getElementById('car_6').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('car_0').setAttribute('class','active'); }</script>";
        break;
    case "Rastreadores.php":
        echo "<script>window.onload = function(){ document.getElementById('car_17').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('car_0').setAttribute('class','active'); }</script>";
        break;
    case "Operadoras.php":
        echo "<script>window.onload = function(){ document.getElementById('car_18').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('car_0').setAttribute('class','active'); }</script>";
        break;
    case "GrupoCidade.php":
        echo "<script>window.onload = function(){ document.getElementById('car_9').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('car_0').setAttribute('class','active'); }</script>";
        break;
    case "GrupoFabricante.php":
        echo "<script>window.onload = function(){ document.getElementById('car_10').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('car_0').setAttribute('class','active'); }</script>";
        break;
    case "Classificacao.php":
        echo "<script>window.onload = function(){ document.getElementById('car_13').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('car_0').setAttribute('class','active'); }</script>";
        break;
    case "Montadoras.php":
        echo "<script>window.onload = function(){ document.getElementById('car_14').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('car_0').setAttribute('class','active'); }</script>";
        break;
    case "Modelos.php":
        echo "<script>window.onload = function(){ document.getElementById('car_15').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('car_0').setAttribute('class','active'); }</script>";
        break;
    case "ServicosVeiculos.php":
        echo "<script>window.onload = function(){ document.getElementById('car_16').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('car_0').setAttribute('class','active'); }</script>";
        break;
    case "Veiculos.php":
        echo "<script>window.onload = function(){ document.getElementById('car_3').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('car_0').setAttribute('class','active'); }</script>";
        break;
    case "Vistorias.php":
        echo "<script>window.onload = function(){ document.getElementById('car_5').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('car_0').setAttribute('class','active'); }</script>";
        break;
    case "Sinistros.php":
        echo "<script>window.onload = function(){ document.getElementById('car_11').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('car_0').setAttribute('class','active'); }</script>";
        break;
    case "OficinasForm.php":
    case "Oficinas.php":
        echo "<script>window.onload = function(){ document.getElementById('car_12').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('car_0').setAttribute('class','active'); }</script>";
        break;
    case "TerceirosForm.php":
    case "Terceiros.php":
        echo "<script>window.onload = function(){ document.getElementById('car_19').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('car_0').setAttribute('class','active'); }</script>";
        break;
    case "Taxas.php":
        echo "<script>window.onload = function(){ document.getElementById('car_7').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "Locacoes.php":
        echo "<script>window.onload = function(){ document.getElementById('clb_1').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('clb_0').setAttribute('class','active'); }</script>";
        break;
    case "Eventos.php":
        echo "<script>window.onload = function(){ document.getElementById('clb_2').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('clb_0').setAttribute('class','active'); }</script>";
        break;
    case "Categoria-Socio.php":
        echo "<script>window.onload = function(){ document.getElementById('clb_3').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('clb_0').setAttribute('class','active'); }</script>";
        break;
    case "Tipo-Socio.php":
        echo "<script>window.onload = function(){ document.getElementById('clb_4').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('clb_0').setAttribute('class','active'); }</script>";
        break;
    case "Layout-Remessa.php":
        echo "<script>window.onload = function(){ document.getElementById('clb_5').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('clb_0').setAttribute('class','active'); }</script>";
        break;
    case "Layout-Retorno.php":
        echo "<script>window.onload = function(){ document.getElementById('clb_6').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('clb_0').setAttribute('class','active'); }</script>";
        break;
    case "Excecao-Vencimento.php":
        echo "<script>window.onload = function(){ document.getElementById('clb_11').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('clb_0').setAttribute('class','active'); }</script>";
        break;
    case "Gerar-Boletos.php":
        echo "<script>window.onload = function(){ document.getElementById('fin_20').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('fin_0').setAttribute('class','active'); }</script>";
        break;
    case "Gerar-Convites.php":
        echo "<script>window.onload = function(){ document.getElementById('clb_7').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('clb_0').setAttribute('class','active'); }</script>";
        break;
    case "Clientes-Documentos.php":
        echo "<script>window.onload = function(){ document.getElementById('clb_12').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('clb_0').setAttribute('class','active'); }</script>";
        break;
    case "Clientes-Folha.php":
        echo "<script>window.onload = function(){ document.getElementById('clb_13').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('clb_0').setAttribute('class','active'); }</script>";
        break;
    case "Baixar-Layout.php":
        echo "<script>window.onload = function(){ document.getElementById('clb_8').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('clb_0').setAttribute('class','active'); }</script>";
        break;
    case "Relatorio-Eventos-Marcados.php":
        echo "<script>window.onload = function(){ document.getElementById('clb_9').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
    case "Relatorio-Categoria-Socio.php":
        echo "<script>window.onload = function(){ document.getElementById('clb_10').setAttribute('style','font-weight:bold; color:white;'); document.getElementById('bi_0').setAttribute('class','active'); }</script>";
        break;
} 

//BI-Estatísticas
if ($ckb_bi_auditoria_ == 0 && $SERVER_FILE == "BI-Auditoria.php"){exit;}
if ($ckb_bi_turmas_ == 0 && $SERVER_FILE == "BI-Turmas.php"){exit;}
if ($ckb_bi_mat_ren_ == 0 && $SERVER_FILE == "BI-Matriculas-Renovacoes.php"){exit;}
if ($ckb_bi_retencao_ == 0 && $SERVER_FILE == "BI-Churn.php"){exit;}
if ($ckb_bi_inadimplencia_ == 0 && $SERVER_FILE == "BI-Inadimplencia.php"){exit;}
if ($ckb_bi_evasao_ == 0 && $SERVER_FILE == "BI-Evasao.php"){exit;}
if ($ckb_bi_prev_fin_ == 0 && $SERVER_FILE == "BI-Previsao-Financeira.php"){exit;}
if ($ckb_bi_cen_custo_ == 0 && $SERVER_FILE == "BI-Centro-de-Custos.php"){exit;}
if ($ckb_bi_fluxo_caixa_ == 0 && $SERVER_FILE == "BI-Fluxo-de-Caixa.php"){exit;}
if ($ckb_bi_faturamento_ == 0 && $SERVER_FILE == "BI-Faturamento.php"){exit;}
if ($ckb_bi_com_faturamento_ == 0 && $SERVER_FILE == "BI-Comparativo-Faturamento.php"){exit;}
if ($ckb_bi_formas_pagamento_ == 0 && $SERVER_FILE == "BI-Tipos-de-Documentos.php"){exit;}
if ($ckb_bi_gestao_vendas_ == 0 && $SERVER_FILE == "BI-Gestao-de-Vendas.php"){exit;}
if ($ckb_bi_gestao_mapa_ == 0 && $SERVER_FILE == "BI-Mapa.php"){exit;}
if ($ckb_bi_produtividade_ == 0 && $SERVER_FILE == "BI-Produtividade-Funcionario.php"){exit;}
if ($ckb_bi_atendimentos_ == 0 && $SERVER_FILE == "Controle-de-Atendimentos.php"){exit;}
if ($ckb_bi_leads_ == 0 && $SERVER_FILE == "BI-Leads.php"){exit;}
if ($ckb_bi_prospects_ == 0 && $SERVER_FILE == "BI-Prospects.php"){exit;}
if ($ckb_bi_agendamentos_ == 0 && $SERVER_FILE == "Gerenciador-Agenda-Compromissos.php"){exit;}
//Clube
if ($ckb_clb_loc_ == 0 && $SERVER_FILE == "Locacoes.php"){exit;}
if ($ckb_clb_eventos_ == 0 && $SERVER_FILE == "Eventos.php"){exit;} 
if ($ckb_clb_categoria_ == 0 && $SERVER_FILE == "Categoria-Socio.php"){exit;} 
if ($ckb_clb_tipo_ == 0 && $SERVER_FILE == "Tipo-Socio.php") {exit;}
if ($ckb_clb_layout_remessa_ == 0 && $SERVER_FILE == "Layout-Remessa.php") {exit;}
if ($ckb_clb_layout_retorno_ == 0 && $SERVER_FILE == "Layout-Retorno.php") {exit;}
if ($ckb_clb_excecao_vencimento_ == 0 && $SERVER_FILE == "Excecao-Vencimento.php") {exit;}
if ($ckb_clb_gboleto_ == 0 && $SERVER_FILE == "Gerar-Boletos.php") {exit;}
if ($ckb_clb_gconvite_ == 0 && $SERVER_FILE == "Gerar-Convites.php") {exit;}
if ($ckb_clb_gestao_documentos_ == 0 && $SERVER_FILE == "Clientes-Documentos.php") {exit;}
if ($ckb_clb_gestao_folha_ == 0 && $SERVER_FILE == "Clientes-Folha.php") {exit;}
if ($ckb_clb_baixar_layout_ == 0 && $SERVER_FILE == "Baixar-Layout.php") {exit;}
if ($ckb_clb_rel_eventos_ == 0 && $SERVER_FILE == "Relatorio-Eventos-Marcados.php") {exit;}
if ($ckb_clb_rel_socios_ == 0 && $SERVER_FILE == "Relatorio-Categoria-Socio.php") {exit;}
//Informações Gerenciais
if ($ckb_aca_inf_gerenciais_ == 0 && $SERVER_FILE == "BI-InformacoesGerenciais.php") {exit;}
//Administrativo
if ($ckb_adm_cli_ == 0 && $SERVER_FILE == "Gerenciador-Clientes.php"){exit;}
if ($ckb_adm_for_ == 0 && $SERVER_FILE == "Fornecedores-de-despesas.php"){exit;}
if ($ckb_adm_fun_ == 0 && $SERVER_FILE == "Funcionarios.php"){exit;}
if ($ckb_adm_usu_ == 0 && $SERVER_FILE == "Usuarios.php"){exit;}
if ($ckb_adm_per_ == 0 && $SERVER_FILE == "Permissoes.php"){exit;}
if ($ckb_adm_dep_ == 0 && $SERVER_FILE == "Departamentos.php"){exit;}
if ($ckb_adm_gru_ == 0 && ($SERVER_FILE == "Grupo-de-pessoas.php" || $SERVER_FILE == "GrupoPlanosList.php")){exit;}
if ($ckb_adm_email_ == 0 && $SERVER_FILE == "EmailsList.php"){exit;}
if ($ckb_adm_config_ == 0 && $SERVER_FILE == "FormConfiguracao.php"){exit;}
if ($ckb_adm_contratos_ == 0 && $SERVER_FILE == "Contratos.php"){exit;}
if ($ckb_adm_disparos_ == 0 && $SERVER_FILE == "DisparosAutoList.php"){exit;}
if ($ckb_adm_agenda_ == 0 && $SERVER_FILE == "Gerenciador-Agenda.php"){exit;}
if ($ckb_adm_parentesco_ == 0 && $SERVER_FILE == "Parentesco.php"){exit;}
if ($ckb_fin_doc_ == 0 && $SERVER_FILE == "Tipos-de-Documentos.php"){exit;}
if ($ckb_fin_ban_ == 0 && $SERVER_FILE == "Bancos.php"){exit;}
if ($ckb_com_forma_ == 0 && $SERVER_FILE == "ComissaoClientes.php"){exit;}
if ($ckb_fin_ccusto_ == 0 && $SERVER_FILE == "Centro-custo.php"){exit;}
if ($ckb_fin_con_ == 0 && $SERVER_FILE == "Grupo-de-contas.php"){exit;}
if ($ckb_fin_mov_ == 0 && $SERVER_FILE == "Contas-de-Movimentos.php"){exit;}
if ($ckb_crm_cli_ == 0 && $SERVER_FILE == "Procedencia-de-clientes.php"){exit;}
if ($ckb_crm_fch_ == 0 && $SERVER_FILE == "Fechamento-de-chamados.php"){exit;}
if ($ckb_crm_esp_ == 0 && $SERVER_FILE == "Especificacoes.php"){exit;}
if ($ckb_crm_ind_ == 0 && $SERVER_FILE == "Indicadores.php"){exit;}
//Academia
if ($ckb_aca_hor_ == 0 && $SERVER_FILE == "HorariosList.php"){exit;}
if ($ckb_aca_pla_ == 0 && $SERVER_FILE == "GrupoPlanosList.php"){exit;}
if ($ckb_aca_ser_ == 0 && $ckb_est_mov_ == 0 && $ckb_est_ser_ == 0 && $SERVER_FILE == "Grupo-de-estoque.php"){exit;}
if ($ckb_aca_pla_ == 0 && $SERVER_FILE == "Planos.php"){exit;}
if ($ckb_aca_ser_ == 0 && $SERVER_FILE == "Servicos.php"){exit;}
if ($ckb_aca_conv_ == 0 && $SERVER_FILE == "ConveniosList.php"){exit;}
if ($ckb_aca_cred_ == 0 && $SERVER_FILE == "CredenciaisList.php"){exit;}
if ($ckb_aca_catr_ == 0 && $SERVER_FILE == "CatracasList.php"){exit;}
if ($ckb_aca_leif_ == 0 && $SERVER_FILE == "LeitorFacial.php"){exit;}
if ($adm_catraca_urna_ == 0 && $SERVER_FILE == "CartaoList.php"){exit;}
if ($ckb_aca_amb_ == 0 && $SERVER_FILE == "AmbientesList.php"){exit;}
if ($ckb_aca_fer_ == 0 && $SERVER_FILE == "FeriasForm.php"){exit;}
if ($ckb_aca_turmas_ == 0 && $SERVER_FILE == "TurmasList.php"){exit;}
if ($ckb_aca_documentos_ == 0 && $SERVER_FILE == "DocumentosList.php"){exit;}
if ($ckb_aca_estudio_ == 0 && $SERVER_FILE == "MapaEstudio.php"){exit;}
if ($ckb_bi_rel_acessos_ == 0 && $SERVER_FILE == "AcessosRelatorio.php"){exit;}
//Financeiro
if ($ckb_fin_fix_ == 0 && $SERVER_FILE == "Lancamento-de-Movimentos.php"){exit;}
if ($ckb_fin_lsol_ == 0 && $SERVER_FILE == "Solicitacao-de-Autorizacao.php"){exit;}
if ($ckb_fin_pag_ == 0 && $SERVER_FILE == "Contas-a-Pagar.php"){exit;}
if ($ckb_fin_rec_ == 0 && $SERVER_FILE == "Contas-a-Receber.php"){exit;}
if ($ckb_fin_flu_ == 0 && $SERVER_FILE == "Fluxo-de-Caixa.php"){exit;}
if ($ckb_est_com_ == 0 && $SERVER_FILE == "Comissoes.php"){exit;}
if ($ckb_fin_lped_ == 0 && $SERVER_FILE == "Requisicoes.php"){exit;}
if ($ckb_fin_lcot_ == 0 && $SERVER_FILE == "Cotacoes.php"){exit;}
if ($ckb_est_lorc_ == 0 && $SERVER_FILE == "Orcamentos.php"){exit;}
if ($ckb_est_lven_ == 0 && $SERVER_FILE == "Vendas.php"){exit;}
if ($ckb_est_lcom_ == 0 && $SERVER_FILE == "Compras.php"){exit;}
if ($ckb_est_ent_sai_ == 0 && $SERVER_FILE == "Entradas-Saidas.php"){exit;}
if ($ckb_fin_rel_bol_ == 0 && $SERVER_FILE == "Relatorio-Boletos-Registrados.php"){exit;}
if ($ckb_out_rban_ == 0 && $SERVER_FILE == "Retorno-Banco.php"){exit;}
if ($ckb_fin_chq_ == 0 && $SERVER_FILE == "Cheques.php"){exit;}
if ($ckb_fin_rcrt_ == 0 && $SERVER_FILE == "Cartoes.php"){exit;}
if ($ckb_fin_rel_gct_ == 0 && $SERVER_FILE == "Relatorio-Grupo-de-Contas.php"){exit;}
if ($ckb_fin_rel_tdd_ == 0 && $SERVER_FILE == "Relatorio-Tipos-de-Documentos.php"){exit;}
if ($ckb_fin_rel_mov_caixa_ == 0 && $SERVER_FILE == "Relatorio-Movimento-Caixa.php"){exit;}
//Estoque
if ($ckb_aca_ser_ == 0 && $ckb_est_mov_ == 0 && $ckb_est_ser_ == 0 && $SERVER_FILE == "Grupo-de-estoque.php"){exit;}
if ($ckb_est_pro_ == 0 && $SERVER_FILE == "Produtos.php"){exit;}
if ($ckb_est_loc_ == 0 && $SERVER_FILE == "Localizacoes.php"){exit;}
if ($ckb_est_cor_ == 0 && $SERVER_FILE == "Cores.php"){exit;}
if ($ckb_est_tam_ == 0 && $SERVER_FILE == "Tamanhos.php"){exit;}
if ($ckb_est_fab_ == 0 && $SERVER_FILE == "Fabricantes.php"){exit;}
if ($ckb_est_jus_ == 0 && $SERVER_FILE == "Justificativas.php"){exit;}
if ($ckb_est_flu_ == 0 && $SERVER_FILE == "Fluxo-de-Estoque.php"){exit;}
//Nota Fiscal
if ($ckb_nfe_cen_ == 0 && $SERVER_FILE == "Cenarios.php"){exit;}
if ($ckb_nfe_tra_ == 0 && $SERVER_FILE == "Transportadoras.php"){exit;}
if ($ckb_nfe_msg_ == 0 && $SERVER_FILE == "NF-Mensagens.php"){exit;}
if ($ckb_nfe_emi_ == 0 && $SERVER_FILE == "Emitentes.php"){exit;}
if ($ckb_nfe_est_ == 0 && $SERVER_FILE == "Estados.php"){exit;}
if ($ckb_nfe_cid_ == 0 && $SERVER_FILE == "Cidades.php"){exit;}
if ($ckb_nfe_eqs_ == 0 && $SERVER_FILE == "Enquadramento-Servico.php"){exit;}
if ($ckb_nfe_not_ == 0 && $SERVER_FILE == "Emitir-Nota.php"){exit;}
if ($ckb_nfe_nos_ == 0 && $SERVER_FILE == "Emitir-Nota-Servico.php"){exit;}
//CRM
if ($ckb_crm_lead_ == 0 && $SERVER_FILE == "Lead.php"){exit;}
if ($ckb_crm_lead_camp_ == 0 && $SERVER_FILE == "Lead-Campanha.php"){exit;}
if ($ckb_crm_chatbot_ == 0 && $SERVER_FILE == "ChatbotList.php"){exit;}
if ($ckb_crm_rec_ == 0 && $SERVER_FILE == "Recepcao.php"){exit;}
if ($ckb_crm_pro_ == 0 && $SERVER_FILE == "Gerenciador-Prospects.php"){exit;}
if ($ckb_crm_tel_ == 0 && $SERVER_FILE == "Telemarketing.php"){exit;}
//Serviços
if ($ckb_aca_ser_ == 0 && $ckb_est_mov_ == 0 && $ckb_est_ser_ == 0 && $SERVER_FILE == "Grupo-de-estoque.php"){exit;}
if ($ckb_aca_ser_ == 0 && $SERVER_FILE == "Servicos.php"){exit;}
if ($ckb_ser_cli_ == 0 && $SERVER_FILE == "Gerenciador-ClientesSer.php"){exit;}
if ($ckb_ser_ser_ == 0 && $SERVER_FILE == "ServicosSer.php"){exit;}
if ($ckb_ser_rpc_ == 0 && $SERVER_FILE == "Relatorio-Pacotes.php"){exit;}
//Proteção
if ($ckb_pro_sinistro_ == 0 && $SERVER_FILE == "Sinistros.php"){exit;}
if ($ckb_pro_oficina_ == 0 && $SERVER_FILE == "Oficinas.php"){exit;}
if ($ckb_pro_cotas_ == 0 && $SERVER_FILE == "Taxas.php"){exit;}
//Sivis
if ($ckb_siv_cadAcad_ == 0 && $SERVER_FILE == "CadAcad.php"){exit;}
if ($ckb_siv_cadErp_ == 0 && $SERVER_FILE == "CadERP.php"){exit;}
if ($ckb_siv_GerSenha_ == 0 && $SERVER_FILE == "GerarSenha.php"){exit;}
if ($ckb_siv_LogErp_ == 0 && $SERVER_FILE == "LogERP.php"){exit;}
if ($ckb_siv_UserSite_ == 0 && $SERVER_FILE == "UsuarioPortal.php"){exit;}
if ($ckb_siv_DCC_ == 0 && $SERVER_FILE == "SaldoDCC.php"){exit;}
if ($ckb_siv_DCC_ == 0 && $SERVER_FILE == "SaldoSMS.php"){exit;}
if ($ckb_out_ecob_ == 0 && $SERVER_FILE == "Email-Cobranca.php"){exit;}
if ($ckb_siv_PenGB_ == 0 && $SERVER_FILE == "CadastrosPendentesGoBody.php"){exit;}
//GoBody
if ($mdl_bod_ == 0 && $SERVER_FILE == "login.php"){exit;}
//Ajuda
if ($ckb_out_aju_ == 0 && $SERVER_FILE == "Faq.php"){exit;}

$logoMnuLat = "/img/logo.png";    
$logoMnuLatThumb = "/img/logo-thumb.png";  
    
if($_SESSION["mod_emp"] == 1) {
    $logoMnuLat = "/img/LOGO-PAGTO.png";
    $logoMnuLatThumb = "/img/LOGO-PAGTO-thumb.png";
} elseif($_SESSION["mod_emp"] == 2) {
    $logoMnuLat = "/img/logo_mu.png";
    $logoMnuLatThumb = "/img/logo_mu-thumb.png";
} elseif($_SESSION["mod_emp"] == 3) {
    $logoMnuLat = "/img/logo-pc.png";
    $logoMnuLatThumb = "/img/logo-pc-thumb.png";
} 

if (file_exists(__DIR__ . "/Pessoas/" . $contrato . "/Empresa/Menu/logo.png")) {
    $logoMnuLat = "/Pessoas/" . $contrato . "/Empresa/Menu/logo.png";
}

?>
<style type="text/css">
    .navigation li ul li #subSub {
        margin-left: 10px;
        color: #666;
    }
    .destaque {
        font-weight: bold;
        color: #68af27;
        background-color: #FFF;
    }
    .destaqueAmarelo {
        font-weight: bold;
        color: #ffaa31;
        background-color: #FFF;
    }
    .ui-tooltip {
        padding: 8px;
        position: absolute;
        z-index: 9999;
        max-width: 300px;
        -webkit-box-shadow: 0 0 5px #aaa;
        box-shadow: 0 0 5px #aaa;
    }
    body .ui-tooltip {
        border-width: 2px;
    }
    .navigation > li > a { border-left-color: var(--cor-destaque) !important; }
    .navigation > li > ul > li > a {
        font-weight: bold;
        color: var(--cor-destaque) !important;
    }
    .navigation > li > ul > li > ul {
        margin-left: 8px;
        border-left: 3px solid var(--cor-destaque);
    }
    .button_theme {
        background-color: #afbf00;
        border-radius: 20px;    
        margin-top: 5%;
        width: 85%;
    }
    .bola {
        border-radius: 50%;
        display: inline-block;
        height: 8px;
        width: 8px;
        background-color: greenyellow;
    }
    .bolaoff {
        border-radius: 50%;
        display: inline-block;
        height: 8px;
        width: 8px;
        background-color: darkgray;
    }    
    #footer-dash {
        position: fixed;
        width: 100%;
        padding: 5px;
        text-align: center;
        margin: 0 auto;
        left: 0;
        bottom: 0;
        z-index: 20;
        background: #191818;
    }    
    .widget button {
        color: white;
        background-color: var(--cor-primaria);
    }
</style>
<div class="sidebar sidebar-mu">
    <div class="top"><a href="#" class="logo" style="background-image: url('<?php echo $logoMnuLat; ?>');background-repeat: no-repeat;background-position: center center;" onClick="changeMenu(0)"></a></div>
    <div class="nContainer <?php echo ($_SESSION["mod_emp"] == 3 ? "cbColor" : ""); ?>" <?php echo ($_SESSION["mod_emp"] == 2 ? "style=\"border-top: solid 1px white;\"" : "");?>>
        <input type="hidden" id="system" value="<?php echo $_SESSION["mod_emp"];?>">
        <input type="hidden" name="txtModAcad" id="txtModAcad" value="<?php echo $mdl_aca_;?>">
        <input type="hidden" name="txtIdUsu" id="txtIdUsu" value="<?php echo $_SESSION["id_usuario"]; ?>">
        <input type="hidden" name="txtUserLog" id="txtUserLog" value="<?php echo $_SESSION["login_usuario"]; ?>">
        <input type="hidden" name="txtnomUsu" id="txtnomUsu" value="<?php echo $_SESSION["nome_usuario"]; ?>">     
        <input type="hidden" name="txtLojaStatus" id="txtLojaStatus" value="<?php echo $mu_loja_status; ?>">  
        <ul class="navigation" style="margin-bottom: 10px;">
            <li>
                <a id="ind_1" href="<?php echo $UrlAtual . "/" . $mainPageGo; ?>">
                    <div class="menu-icon"><i class="material-menu">home</i></div>
                    <span>Página Principal</span>
                </a>
            </li>
            <?php if (($ckb_clb_rel_eventos_ + $ckb_clb_rel_socios_) +
            ($ckb_bi_auditoria_ + $ckb_bi_turmas_ + $ckb_bi_mat_ren_ + $ckb_bi_retencao_ + $ckb_bi_inadimplencia_ + $ckb_bi_evasao_ + $ckb_bi_rel_acessos_) +
            ($ckb_bi_prev_fin_ + $ckb_bi_cen_custo_ + $ckb_bi_fluxo_caixa_ + $ckb_bi_faturamento_ + $ckb_bi_com_faturamento_ + $ckb_bi_formas_pagamento_ + $ckb_bi_gestao_vendas_ + $ckb_bi_gestao_mapa_ + $ckb_bi_produtividade_ + $ckb_pro_cotas_) +
            ($ckb_bi_atendimentos_ + $ckb_bi_leads_ + $ckb_bi_prospects_ + $ckb_bi_agendamentos_) > 0) { ?>
                <li id="bi_0">
                    <a href="#">
                        <div class="menu-icon"><i class="material-menu">monitoring</i></div>
                        <span>BI - Estatísticas</span>
                    </a>
                    <div class="open"></div>
                    <ul>
                        <?php if (($ckb_clb_rel_eventos_ + $ckb_clb_rel_socios_) > 0) { ?>
                        <li><a href="#">Clube</a>
                            <ul id="subSub">
                                <?php if($ckb_clb_rel_eventos_ == 1) {?>
                                    <li><a id="clb_9" href="<?php echo $UrlAtual; ?>/Modulos/Clube/Relatorio-Eventos-Marcados.php">Rel. Eventos Marcados</a></li>
                                <?php } if ($ckb_clb_rel_socios_ == 1) { ?>
                                    <li><a id="clb_10" href="<?php echo $UrlAtual; ?>/Modulos/Clube/Relatorio-Categoria-Socio.php">Rel. de Sócios</a></li>
                                <?php } ?>
                            </ul>
                        </li>                            
                        <?php } if(($ckb_bi_auditoria_ + $ckb_bi_turmas_ + $ckb_bi_mat_ren_ + $ckb_bi_retencao_ + $ckb_bi_inadimplencia_ + $ckb_bi_evasao_ + $ckb_bi_rel_acessos_) > 0) { ?>
                        <li><a href="#"><?php echo ($mdl_clb_ == 1 || $mdl_seg_ == 1 ? "Gestão" : "Academia")?></a>
                            <ul id="subSub">
                                <?php if($ckb_bi_auditoria_ == 1) {?>
                                    <li><a id="bi_14" href="<?php echo $UrlAtual; ?>/Modulos/BI/BI-Auditoria.php">Auditoria</a></li>
                                <?php } if ($ckb_bi_turmas_ == 1) { ?>                                
                                    <li><a id="bi_13" href="<?php echo $UrlAtual; ?>/Modulos/BI/BI-Turmas.php">Turmas</a></li>
                                <?php } if ($ckb_bi_mat_ren_ == 1) { ?>                                                                    
                                    <li><a id="bi_16" href="<?php echo $UrlAtual; ?>/Modulos/BI/BI-Matriculas-Renovacoes.php">Matrículas x Renovações</a></li>
                                <?php } if ($ckb_bi_retencao_ == 1) { ?>                                
                                    <li><a id="bi_18" href="<?php echo $UrlAtual; ?>/Modulos/BI/BI-Churn.php">Retenção (Churn)</a></li>
                                <?php } if ($ckb_bi_inadimplencia_ == 1) { ?>                                
                                    <li><a id="bi_20" href="<?php echo $UrlAtual; ?>/Modulos/BI/BI-Inadimplencia.php">Inadimplência</a></li>                                    
                                <?php } if ($ckb_bi_evasao_ == 1) { ?>                                
                                    <li><a id="bi_19" href="<?php echo $UrlAtual; ?>/Modulos/BI/BI-Evasao.php">Evasão</a></li>
                                <?php } if ($mdl_seg_ == 0 && $ckb_bi_rel_acessos_ == 1) { ?>
                                    <li><a id="aca_8" href="<?php echo $UrlAtual; ?>/Modulos/Academia/AcessosRelatorio.php">Rel. Acessos</a></li>
                                <?php } ?>                                
                            </ul>
                        </li>                        
                        <?php } if(($ckb_bi_prev_fin_ + $ckb_bi_cen_custo_ + $ckb_bi_fluxo_caixa_ + $ckb_bi_faturamento_ + $ckb_bi_com_faturamento_ + $ckb_bi_formas_pagamento_ + $ckb_bi_gestao_vendas_ + $ckb_bi_gestao_mapa_ + $ckb_bi_produtividade_ + $ckb_pro_cotas_) > 0) { ?>
                        <li><a href="#">Financeiro</a>
                            <ul id="subSub">
                                <?php if($ckb_bi_prev_fin_ == 1) {?>
                                    <li><a id="bi_2" href="<?php echo $UrlAtual; ?>/Modulos/BI/BI-Previsao-Financeira.php">Previsão Financeira</a></li>
                                <?php } if ($ckb_bi_cen_custo_ == 1) { ?>
                                    <li><a id="bi_3" href="<?php echo $UrlAtual; ?>/Modulos/BI/BI-Centro-de-Custos.php">Centro de Custos</a></li>
                                <?php } if ($ckb_bi_fluxo_caixa_ == 1) { ?>
                                    <li><a id="bi_4" href="<?php echo $UrlAtual; ?>/Modulos/BI/BI-Fluxo-de-Caixa.php?id=2&tp=0">Fluxo de Caixa</a></li>
                                <?php } if ($ckb_bi_faturamento_ == 1) { ?>
                                    <li><a id="bi_5" href="<?php echo $UrlAtual; ?>/Modulos/BI/BI-Faturamento.php">Faturamento</a></li>
                                <?php } if ($ckb_bi_com_faturamento_ == 1) { ?>
                                    <li><a id="bi_17" href="<?php echo $UrlAtual; ?>/Modulos/BI/BI-Comparativo-Faturamento.php">Comp. Faturamento</a></li>
                                <?php } if ($ckb_bi_formas_pagamento_ == 1) { ?>
                                    <li><a id="bi_6" href="<?php echo $UrlAtual; ?>/Modulos/BI/BI-Tipos-de-Documentos.php">Formas de Pagamento</a></li>
                                <?php } if ($ckb_bi_gestao_vendas_ == 1) { ?>
                                    <li><a id="bi_10" href="<?php echo $UrlAtual; ?>/Modulos/BI/BI-Gestao-de-Vendas.php">Gestão de Vendas</a></li>
                                <?php } if ($ckb_bi_gestao_mapa_ == 1) { ?>
                                    <li><a id="bi_22" href="<?php echo $UrlAtual; ?>/Modulos/BI/BI-Mapa.php">Mapa de Equipe</a></li>
                                <?php } if ($ckb_bi_produtividade_ == 1) { ?>
                                    <li><a id="bi_7" href="<?php echo $UrlAtual; ?>/Modulos/BI/BI-Produtividade-Funcionario.php">Produtividade</a></li>
                                <?php } if ($ckb_pro_cotas_ == 1) { ?>
                                    <li><a id="car_7" href="<?php echo $UrlAtual; ?>/Modulos/Seguro/Taxas.php"><?php echo ($mdl_seg_ > 0 ? "Distribuição de Cotas" : "Dem. Result. do Exercício");?></a></li>                                    
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } if(($ckb_bi_atendimentos_ + $ckb_bi_leads_ + $ckb_bi_prospects_ + $ckb_bi_agendamentos_) > 0) { ?>
                        <li><a href="#">CRM</a>
                            <ul id="subSub">
                                <?php if($ckb_bi_atendimentos_ == 1) {?>
                                    <li><a id="bi_11" href="<?php echo $UrlAtual; ?>/Modulos/BI/Controle-de-Atendimentos.php">Atendimentos</a></li>
                                <?php } if ($ckb_bi_leads_ == 1) { ?>
                                    <li><a id="bi_21" href="<?php echo $UrlAtual; ?>/Modulos/BI/BI-Leads.php">Leads</a></li>
                                <?php } if ($ckb_bi_prospects_ == 1) { ?>
                                    <li><a id="bi_12" href="<?php echo $UrlAtual; ?>/Modulos/BI/BI-Prospects.php">Gestão de Clientes</a></li>
                                <?php } if ($ckb_bi_agendamentos_ == 1) { ?>
                                    <li><a id="bi_15" href="<?php echo $UrlAtual; ?>/Modulos/Comercial/Gerenciador-Agenda-Compromissos.php">Agendamentos</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } if ($mdl_clb_ > 0 && ($ckb_clb_loc_ + $ckb_clb_eventos_ + $ckb_clb_categoria_ + $ckb_clb_tipo_ + $ckb_clb_gconvite_ + $ckb_clb_gestao_documentos_ +  $ckb_clb_gestao_folha_ + $ckb_clb_baixar_layout_) > 0) { ?>
                <li id="clb_0">
                    <a href="#">
                        <div class="menu-icon"><i class="material-menu">pool</i></div>
                        <span>Clube</span>
                    </a>
                    <div class="open"></div>
                    <ul>
                        <?php if (($ckb_clb_loc_ + $ckb_clb_eventos_ + $ckb_clb_categoria_ + $ckb_clb_tipo_) > 0) { ?>
                            <li><a href="#">Cadastros</a>
                                <ul id="subSub">
                                    <?php if($ckb_clb_loc_ == 1) {?>
                                        <li><a id="clb_1" href="<?php echo $UrlAtual; ?>/Modulos/Clube/Locacoes.php">Locações</a></li>
                                    <?php } if ($ckb_clb_eventos_ == 1) { ?>
                                        <li><a id="clb_2" href="<?php echo $UrlAtual; ?>/Modulos/Clube/Eventos.php">Eventos</a></li>
                                    <?php } if ($ckb_clb_categoria_ == 1) { ?>
                                        <li><a id="clb_3" href="<?php echo $UrlAtual; ?>/Modulos/Clube/Categoria-Socio.php">Categorias de Sócios</a></li>
                                    <?php } if ($ckb_clb_tipo_ == 1) { ?>
                                        <li><a id="clb_4" href="<?php echo $UrlAtual; ?>/Modulos/Clube/Tipo-Socio.php">Tipo de Sócios</a></li>
                                    <?php } if ($ckb_clb_layout_remessa_ == 1 && $_SESSION["id_usuario"] == 1) { ?>                                        
                                        <li><a id="clb_5" href="<?php echo $UrlAtual; ?>/Modulos/Clube/Layout-Remessa.php">Layout de Remessa</a></li>
                                    <?php } if ($ckb_clb_layout_retorno_ == 1 && $_SESSION["id_usuario"] == 1) { ?>                                        
                                        <li><a id="clb_6" href="<?php echo $UrlAtual; ?>/Modulos/Clube/Layout-Retorno.php">Layout de Retorno</a></li>
                                    <?php } if ($ckb_clb_excecao_vencimento_ == 1) { ?>                                        
                                        <li><a id="clb_11" href="<?php echo $UrlAtual; ?>/Modulos/Clube/Excecao-Vencimento.php">Exceção de Vencimentos</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } if (($ckb_clb_gconvite_ + $ckb_clb_gestao_documentos_ + $ckb_clb_gestao_folha_ + $ckb_clb_baixar_layout_) > 0) { ?>
                            <li><a href="#">Controle</a>
                                <ul id="subSub">
                                    <?php if ($ckb_clb_gconvite_ == 1) { ?>
                                        <li><a id="clb_7" href="<?php echo $UrlAtual; ?>/Modulos/Clube/Gerar-Convites.php">Gestão de Agenda</a></li>
                                    <?php } if ($ckb_clb_gestao_documentos_ == 1) { ?>                                        
                                        <li><a id="clb_12" href="<?php echo $UrlAtual; ?>/Modulos/Clube/Clientes-Documentos.php">Gestão de Documentos</a></li>
                                    <?php } if ($ckb_clb_gestao_folha_ == 1) { ?>                                        
                                        <li><a id="clb_13" href="<?php echo $UrlAtual; ?>/Modulos/Clube/Clientes-Folha.php">Gestão de Folha</a></li>
                                    <?php } if ($ckb_clb_baixar_layout_ == 1) { ?>                                        
                                        <li><a id="clb_8" href="<?php echo $UrlAtual; ?>/Modulos/Clube/Baixar-Layout.php">Baixar por Layout</a></li>
                                    <?php } ?>                                        
                                </ul>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } if ($_SESSION["mod_emp"] == 1) { ?>
                <li id="bi_0">
                    <a href="<?php echo $UrlAtual; ?>/Modulos/Academia/BI-InformacoesGerenciais.php">
                        <div class="menu-icon ico-chart-3"></div>
                        <span>Informações Gerenciais</span>
                    </a>
                </li>
            <?php } if ($mdl_adm_ > 0  && ($ckb_adm_cli_ + $ckb_adm_for_ + $ckb_adm_fun_ + $ckb_adm_usu_ + $ckb_adm_per_ + $ckb_adm_gru_ + $ckb_adm_config_ + $ckb_adm_agenda_ + $ckb_adm_parentesco_ + $ckb_fin_doc_ + $ckb_fin_ban_ + $ckb_com_forma_ + $ckb_fin_ccusto_ + $ckb_fin_con_ + $ckb_fin_mov_ + $ckb_crm_cli_ + $ckb_crm_fch_ + $ckb_crm_ind_ + $ckb_crm_esp_ > 0)) { ?>
                <li id="adm_0">
                    <a href="#">
                        <div class="menu-icon"><i class="material-menu">folder_open</i></div>
                        <span>Administrativo</span>
                    </a>
                    <div class="open"></div>
                    <ul>
                        <?php if ($ckb_adm_cli_ + $ckb_adm_for_ + $ckb_adm_fun_ + $ckb_adm_usu_ + $ckb_adm_per_ + $ckb_adm_gru_ + $ckb_adm_config_ + $ckb_adm_email + $ckb_adm_agenda_ + $ckb_adm_parentesco_ > 0) { ?>
                            <li><a href="#">Cadastros</a>
                                <ul id="subSub">
                                    <?php if($ckb_adm_cli_ == 1) {?>
                                        <li><a id="adm_1" href="<?php echo $UrlAtual; ?>/Modulos/Comercial/Gerenciador-Clientes.php">Clientes</a></li>
                                    <?php } if ($ckb_adm_for_ == 1 && $_SESSION["mod_emp"] != 1) { ?>
                                        <li><a id="adm_2" href="<?php echo $UrlAtual; ?>/Modulos/Financeiro/Fornecedores-de-despesas.php">Fornecedores</a></li>
                                    <?php } if ($ckb_adm_fun_ == 1 && $_SESSION["mod_emp"] != 1) { ?>
                                        <li><a id="adm_3" href="<?php echo $UrlAtual; ?>/Modulos/Financeiro/Funcionarios.php">Funcionários</a></li>
                                    <?php } if ($ckb_adm_usu_ == 1) { ?>
                                        <li><a id="adm_4" href="<?php echo $UrlAtual; ?>/Modulos/Comercial/Usuarios.php">Usuários</a></li>
                                    <?php } if ($ckb_adm_per_ == 1) { ?>
                                        <li><a id="adm_5" href="<?php echo $UrlAtual; ?>/Modulos/Comercial/Permissoes.php">Permissões</a></li>
                                    <?php } if ($ckb_adm_dep_ == 1) { ?>
                                        <li><a id="adm_7" href="<?php echo $UrlAtual; ?>/Modulos/Comercial/Departamentos.php">Departamentos</a></li>
                                    <?php } if ($ckb_adm_gru_ == 1) { ?>
                                        <li><a id="adm_6" href="<?php echo $UrlAtual; ?>/Modulos/Financeiro/Grupo-de-pessoas.php">Grupos de Pessoas</a></li>
                                    <?php } if ($ckb_adm_gru_ == 1 && $_SESSION["mod_emp"] == 1) { ?>
                                      <li><a id="adm_6" href="<?php echo $UrlAtual; ?>/Modulos/Academia/GrupoPlanosList.php">Grupo de Planos</a></li>
                                    <?php } if ($ckb_adm_config_ == 1) { ?>
                                        <li><a id="adm_8" href="<?php echo $UrlAtual; ?>/Modulos/Comercial/FormConfiguracao.php">Configuração</a></li>
                                    <?php } if($ckb_adm_contratos_ == 1) { ?>
                                      <li><a id="adm_13" href="<?php echo $UrlAtual; ?>/Modulos/Comercial/Contratos.php">Modulo Contrato</a></li>
                                    <?php } if ($ckb_adm_agenda_ == 1 && $_SESSION["mod_emp"] != 1) { ?>
                                        <li><a id="adm_11" href="<?php echo $UrlAtual; ?>/Modulos/Comercial/Gerenciador-Agenda.php">Agendas</a></li>
                                    <?php } if ($ckb_adm_parentesco_ == 1) { ?>
                                        <li><a id="adm_12" href="<?php echo $UrlAtual; ?>/Modulos/Comercial/Parentesco.php">Parentesco</a></li>
                                    <?php } if($_SESSION["mod_emp"] == 1){?>
                                        <li><a id="est_13" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Planos.php">Planos</a></li>
                                        <li><a id="aca_3" href="<?php echo $UrlAtual; ?>/Modulos/Academia/ConveniosList.php">Convênios</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } if ($ckb_fin_doc_ + $ckb_fin_ban_ + $ckb_com_forma_ + $ckb_fin_ccusto_ + $ckb_fin_con_ + $ckb_fin_mov_ > 0) { ?>
                            <li><a href="#">Cadastros Financeiros</a>
                                <ul id="subSub">
                                    <?php if ($ckb_fin_doc_ == 1) { ?>
                                        <li><a id="fin_1" href="<?php echo $UrlAtual; ?>/Modulos/Financeiro/Tipos-de-Documentos.php">Formas de Pagamento</a></li>
                                    <?php } if ($ckb_fin_ban_ == 1) { ?>
                                        <li><a id="fin_2" href="<?php echo $UrlAtual; ?>/Modulos/Financeiro/Bancos.php">Bancos</a></li>
                                    <?php } if ($ckb_com_forma_ == 1) { ?>
                                        <li><a id="fin_21" href="<?php echo $UrlAtual; ?>/Modulos/Seguro/ComissaoClientes.php">Comissão de Clientes</a></li>
                                    <?php } if ($ckb_fin_ccusto_ == 1  && $_SESSION["mod_emp"] != 1) { ?>
                                        <li><a id="fin_22" href="<?php echo $UrlAtual; ?>/Modulos/Financeiro/Centro-custo.php">Centro de Custos</a></li>
                                    <?php } if ($ckb_fin_con_ == 1  && $_SESSION["mod_emp"] != 1) { ?>
                                        <li><a id="fin_3" href="<?php echo $UrlAtual; ?>/Modulos/Financeiro/Grupo-de-contas.php">Grupos de Contas</a></li>
                                    <?php } if ($ckb_fin_mov_ == 1  && $_SESSION["mod_emp"] != 1) { ?>
                                        <li><a id="fin_4" href="<?php echo $UrlAtual; ?>/Modulos/Financeiro/Contas-de-Movimentos.php?tp=1">Subgrupos de Contas(C/D)</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } if ($ckb_crm_cli_ + $ckb_crm_fch_ + $ckb_crm_ind_ + $ckb_crm_esp_ > 0) { ?>
                            <li><a href="#">Cadastros CRM</a>
                                <ul id="subSub">
                                    <?php if ($ckb_crm_cli_ == 1) { ?>
                                        <li><a id="crm_1" href="<?php echo $UrlAtual; ?>/Modulos/CRM/Procedencia-de-clientes.php">Procedência de Clientes</a></li>
                                    <?php } if ($ckb_crm_fch_ == 1) { ?>
                                        <li><a id="crm_10" href="<?php echo $UrlAtual; ?>/Modulos/CRM/Fechamento-de-chamados.php">Fechamento de Chamados</a></li>
                                    <?php } if ($ckb_crm_esp_ == 1) { ?>
                                        <li><a id="crm_5" href="<?php echo $UrlAtual; ?>/Modulos/CRM/Especificacoes.php">Especificações</a></li>
                                    <?php } if ($ckb_crm_ind_ == 1) { ?>
                                        <li><a id="crm_8" href="<?php echo $UrlAtual; ?>/Modulos/CRM/Indicadores.php">Indicadores</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } if ($mdl_aca_ > 0 && ($ckb_aca_hor_ + $ckb_aca_conv_ + $ckb_aca_cred_ + $ckb_aca_catr_ + $ckb_aca_leif_ + $adm_catraca_urna_ + $ckb_aca_amb_ + $ckb_aca_fer_ + $ckb_aca_pla_ + $ckb_aca_ser_ + $ckb_aca_turmas_ + $ckb_aca_estudio_ > 0)) { ?>
                <li id="aca_0">
                    <a href="#">
                        <div class="menu-icon"><i class="material-menu">settings</i></div>
                        <span><?php echo ($mdl_clb_ == 1 || $mdl_seg_ == 1 ? "Configuração" : "Academia")?></span>
                    </a>
                    <div class="open"></div>
                    <ul>
                        <?php if ($ckb_aca_hor_ + $ckb_aca_conv_ + $ckb_aca_cred_ + $ckb_aca_catr_ + $ckb_aca_leif_ + $adm_catraca_urna_ + $ckb_aca_amb_ + $ckb_aca_fer_ + $ckb_aca_pla_ + $ckb_aca_ser_ + $ckb_aca_turmas_ + $ckb_aca_estudio_ > 0) { ?>
                            <li><a href="#">Cadastros</a>
                                <ul id="subSub">
                                    <?php if ($ckb_aca_hor_ == 1) { ?>
                                    <li><a id="aca_2" href="<?php echo $UrlAtual; ?>/Modulos/Academia/HorariosList.php">Horários</a></li>
                                    <?php } if ($ckb_aca_pla_ == 1) { ?>
                                    <li><a id="aca_9" href="<?php echo $UrlAtual; ?>/Modulos/Academia/GrupoPlanosList.php">Grupo de Planos</a></li>
                                    <?php } if ($ckb_aca_ser_ == 1) { ?>
                                    <li><a id="aca_10" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Grupo-de-estoque.php?tp=1">Grupo de Serviços</a></li>
                                    <?php } if ($ckb_aca_pla_ == 1) { ?>
                                    <li><a id="est_13" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Planos.php">Planos</a></li>
                                    <?php } if ($mdl_aca_ == 1 && $ckb_aca_ser_ == 1) { ?>                                    
                                    <li><a id="aca_11" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Servicos.php">Serviços</a></li>
                                    <?php } if ($ckb_aca_conv_ == 1) { ?>
                                    <li><a id="aca_3" href="<?php echo $UrlAtual; ?>/Modulos/Academia/ConveniosList.php">Convênios</a></li>
                                    <?php } if ($mdl_seg_ == 0 && $ckb_aca_cred_ == 1) { ?>
                                    <li><a id="aca_4" href="<?php echo $UrlAtual; ?>/Modulos/Academia/CredenciaisList.php">Credenciais</a></li>
                                    <?php } if ($mdl_seg_ == 0 && $ckb_aca_catr_ == 1 && $_SESSION["permissao"] == 0) { ?>
                                    <li><a id="aca_5" href="<?php echo $UrlAtual; ?>/Modulos/Academia/CatracasList.php">Catracas</a></li>
                                    <?php } if ($mdl_seg_ == 0 && $ckb_aca_leif_ == 1 && $_SESSION["permissao"] == 0) { ?>
                                    <li><a id="aca_16" href="<?php echo $UrlAtual; ?>/Modulos/Academia/LeitorFacial.php">Leitores Faciais</a></li>
                                    <?php } if ($mdl_seg_ == 0 && $adm_catraca_urna_ == 1) { ?>
                                    <li><a id="aca_15" href="<?php echo $UrlAtual; ?>/Modulos/Academia/CartaoList.php">Cartão Urna</a></li>
                                    <?php } if ($mdl_seg_ == 0 && $ckb_aca_amb_ == 1) { ?>
                                    <li><a id="aca_6" href="<?php echo $UrlAtual; ?>/Modulos/Academia/AmbientesList.php">Ambientes</a></li>
                                    <?php } if ($ckb_aca_fer_ == 1) { ?>
                                    <li><a id="aca_7" href="<?php echo $UrlAtual; ?>/Modulos/Academia/FeriasForm.php">Congelamento</a></li>
                                    <?php } if ($ckb_aca_turmas_ == 1 && $mdl_tur_ > 0) { ?>
                                    <li><a id="aca_12" href="<?php echo $UrlAtual; ?>/Modulos/Academia/TurmasList.php">Turmas</a></li>
                                    <?php } if ($ckb_aca_documentos_ == 1) {?>
                                    <li><a id="aca_13" href="<?php echo $UrlAtual; ?>/Modulos/Academia/DocumentosList.php">Documentos</a></li>
                                    <?php } if ($ckb_aca_estudio_ == 1 && $mdl_esd_ > 0) { ?>
                                    <li><a id="aca_14" href="<?php echo $UrlAtual; ?>/Modulos/Academia/MapaEstudio.php">Estúdio</a></li>
                                    <?php } ?>
                                </ul>
                            </li>                            
                        <?php } ?>
                    </ul>
                </li>
            <?php } if ($mdl_fin_ > 0 && ($ckb_fin_fix_ + $ckb_fin_lsol_ + $ckb_fin_pag_ + $ckb_fin_rec_ + $ckb_fin_flu_ + $ckb_fin_rel_mov_caixa_ + $ckb_est_com_ + 
                    $ckb_est_lorc_ + $ckb_est_lven_ + $ckb_clb_gboleto_ + $ckb_fin_rel_bol_ + $ckb_out_rban_ + 
                    $ckb_fin_chq_ + $ckb_fin_rcrt_ + $ckb_fin_rel_gct_ + $ckb_fin_rel_tdd_ + $ckb_fin_rel_mov_caixa_ > 0)) { ?>
                <li id="fin_0">
                    <a href="#">
                        <div class="menu-icon"><i class="material-menu">account_balance</i></div>
                        <span>Financeiro</span>
                    </a>
                    <div class="open"></div>
                    <ul>
                        <?php if ($ckb_fin_fix_ + $ckb_fin_lsol_ + $ckb_fin_pag_ + $ckb_fin_rec_ + $ckb_fin_flu_ + $ckb_est_com_ > 0) { ?>
                            <li><a href="#">Lançamentos</a>
                                <ul id="subSub">
                                    <?php if ($ckb_fin_fix_ == 1 && $_SESSION["mod_emp"] != 1) { ?>
                                        <li><a id="fin_17" href="<?php echo $UrlAtual; ?>/Modulos/Financeiro/Lancamento-de-Movimentos.php?tp=0">Contratos</a></li>
                                        <li><a id="fin_5" href="<?php echo $UrlAtual; ?>/Modulos/Financeiro/Lancamento-de-Movimentos.php?tp=1">Contas Fixas</a></li>
                                    <?php } if ($ckb_fin_lsol_ == 1 && $_SESSION["mod_emp"] != 1) { ?>
                                        <li><a id="fin_6" href="<?php echo $UrlAtual; ?>/Modulos/Contas-a-pagar/Solicitacao-de-Autorizacao.php?id=1">Contas Variáveis</a></li>
                                    <?php } if ($ckb_fin_pag_ == 1 && $_SESSION["mod_emp"] != 1) { ?>
                                        <li><a id="fin_7" href="<?php echo $UrlAtual; ?>/Modulos/Contas-a-pagar/Contas-a-Pagar.php?id=1&tp=0">Contas à Pagar</a></li>
                                    <?php } if ($ckb_fin_rec_ == 1) { ?>
                                        <li><a id="fin_8" href="<?php echo $UrlAtual; ?>/Modulos/Contas-a-pagar/Contas-a-Receber.php?id=1&tp=0">Contas à Receber</a></li>
                                    <?php } if ($ckb_fin_flu_ == 1) { ?>
                                        <li><a id="fin_9" href="<?php echo $UrlAtual; ?>/Modulos/Contas-a-pagar/Fluxo-de-Caixa.php">Fluxo de Caixa</a></li>
                                    <?php } if ($ckb_est_com_ == 1) { ?>
                                        <li><a id="est_9" href="<?php echo $UrlAtual; ?>/Modulos/Seguro/Comissoes.php">Comissões</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } if ($ckb_est_lorc_ + $ckb_est_lven_ + $ckb_clb_gboleto_ + $ckb_fin_rel_bol_ + $ckb_out_rban_ > 0) { ?>
                            <li><a href="#">Controle</a>
                                <ul id="subSub">
                                    <?php if ($ckb_est_lorc_ == 1 && $_SESSION["mod_emp"] == 0) { ?>
                                        <li><a id="est_6" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Orcamentos.php">Orçamentos</a></li>
                                    <?php } if ($ckb_est_lven_ == 1) { ?>
                                        <li><a id="est_5" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Vendas.php?id=1">Vendas</a></li>
                                    <?php } if($ckb_clb_gboleto_ == 1) { ?>
                                        <li><a id="fin_20" href="<?php echo $UrlAtual; ?>/Modulos/Clube/Gerar-Boletos.php">Gerar Boletos</a></li>                                        
                                    <?php } if ($ckb_fin_rel_bol_ == 1) { ?>
                                        <li><a id="fin_12" href="<?php echo $UrlAtual; ?>/Modulos/Financeiro/Relatorio-Boletos-Registrados.php">Remessa de Cobrança</a></li>
                                    <?php } if ($ckb_out_rban_ == 1) { ?>
                                        <li><a id="fin_16" href="<?php echo $UrlAtual; ?>/Modulos/Contas-a-pagar/Retorno-Banco.php">Retorno de Cobrança</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } if ($ckb_fin_chq_ + $ckb_fin_rcrt_ + $ckb_fin_rel_gct_ + $ckb_fin_rel_tdd_ + $ckb_fin_rel_mov_caixa_ > 0 && $_SESSION["mod_emp"] != 1) { ?>
                            <li><a href="#">Consultas</a>
                                <ul id="subSub">
                                    <?php if ($ckb_fin_chq_ == 1) { ?>
                                        <li><a id="fin_11" href="<?php echo $UrlAtual; ?>/Modulos/Financeiro/Cheques.php?id=1">Rel. Cheques</a></li>
                                    <?php } if ($ckb_fin_rcrt_ == 1) { ?>
                                        <li><a id="fin_19" href="<?php echo $UrlAtual; ?>/Modulos/Financeiro/Cartoes.php?id=1">Rel. Cartões</a></li>
                                    <?php } if ($ckb_fin_rel_gct_ == 1) { ?>
                                        <li><a id="fin_10" href="<?php echo $UrlAtual; ?>/Modulos/Financeiro/Relatorio-Grupo-de-Contas.php">Rel. Plano de Contas</a></li>
                                    <?php } if ($ckb_fin_rel_tdd_ == 1) { ?>
                                        <li><a id="fin_13" href="<?php echo $UrlAtual; ?>/Modulos/Financeiro/Relatorio-Tipos-de-Documentos.php">Rel. Formas de Pagamento</a></li>
                                    <?php } if ($ckb_fin_rel_mov_caixa_ == 1) { ?>
                                        <li><a id="fin_14" href="<?php echo $UrlAtual; ?>/Modulos/Financeiro/Relatorio-Movimento-Caixa.php">Movimento do Caixa</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } if ($mdl_est_ > 0 && ($ckb_est_mov_ + $ckb_est_pro_ + $ckb_est_loc_ + $ckb_est_cor_ + $ckb_est_tam_ + $ckb_est_fab_ + $ckb_est_jus_ + 
                    $ckb_fin_lped_ + $ckb_fin_lcot_ + $ckb_est_lcom_ + $ckb_est_ent_sai_ + $ckb_est_flu_ > 0)) { ?>
                <li id="est_0">
                    <a href="#">
                        <div class="menu-icon"><i class="material-menu">forklift</i></div>
                        <span>Estoque</span>
                    </a>
                    <div class="open"></div>
                    <ul>
                        <?php if ($ckb_est_mov_ + $ckb_est_pro_ + $ckb_est_loc_ + $ckb_est_cor_ + $ckb_est_tam_ + $ckb_est_fab_ + $ckb_est_jus_ > 0) { ?>
                            <li><a href="#">Cadastros</a>
                                <ul id="subSub">
                                    <?php if ($ckb_est_mov_ == 1) { ?>
                                        <li><a id="est_1" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Grupo-de-estoque.php?tp=0">Grupo de Produtos</a></li>
                                    <?php } if ($ckb_est_pro_ == 1) { ?>
                                        <li><a id="est_2" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Produtos.php">Produtos</a></li>
                                    <?php } if ($ckb_est_loc_ == 1) { ?>
                                        <li><a id="est_4" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Localizacoes.php">Locais</a></li>
                                    <?php } if ($ckb_est_cor_ == 1) { ?>
                                        <li><a id="est_11" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Cores.php">Cores</a></li>
                                    <?php } if ($ckb_est_tam_ == 1) { ?>
                                        <li><a id="est_10" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Tamanhos.php">Tamanhos</a></li>
                                    <?php } if ($ckb_est_fab_ == 1) { ?>
                                        <li><a id="est_12" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Fabricantes.php">Fabricantes</a></li>
                                    <?php } if ($ckb_est_jus_ == 1) { ?>
                                        <li><a id="est_16" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Justificativas.php">Justificativas</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } if ($ckb_fin_lped_ + $ckb_fin_lcot_ + $ckb_est_lcom_ > 0) { ?>
                            <li><a href="#">Compras</a>
                                <ul id="subSub">
                                    <?php if ($ckb_fin_lped_ == 1) { ?>
                                        <li><a id="est_14" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Requisicoes.php">Requisições</a></li>
                                    <?php } if ($ckb_fin_lcot_ == 1) { ?>
                                        <li><a id="est_17" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Cotacoes.php">Cotações</a></li>
                                    <?php } if ($ckb_est_lcom_ == 1) { ?>
                                        <li><a id="est_7" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Compras.php?id=1">Compras</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } if ($ckb_est_ent_sai_ > 0) { ?>
                            <li><a href="#">Controle</a>
                                <ul id="subSub">
                                    <?php if ($ckb_est_ent_sai_ == 1) { ?>
                                        <li><a id="est_15" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Entradas-Saidas.php?id=0">Entradas/Saídas</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } if ($ckb_est_flu_ > 0) { ?>
                            <li><a href="#">Consultas</a>
                                <ul id="subSub">
                                    <?php if ($ckb_est_flu_ == 1) { ?>
                                        <li><a id="est_8" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Fluxo-de-Estoque.php?id=0">Fluxo de Estoque</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } if ($mdl_nfe_ > 0 && ($ckb_nfe_cen_ + $ckb_nfe_tra_ + $ckb_nfe_msg_ + $ckb_nfe_emi_ + $ckb_nfe_est_ + $ckb_nfe_cid_ + $ckb_nfe_eqs_ + $ckb_nfe_not_ + $ckb_nfe_nos_ > 0)) { ?>
                <li id="nfe_0">
                    <a href="#">
                        <div class="menu-icon"><i class="material-menu">assignment</i></div>
                        <span>Nota Fiscal Eletrônica</span>
                    </a>
                    <div class="open"></div>
                    <ul>
                        <?php if ($ckb_nfe_cen_ + $ckb_nfe_tra_ + $ckb_nfe_msg_ + $ckb_nfe_emi_ + $ckb_nfe_est_ + $ckb_nfe_cid_ + $ckb_nfe_eqs_ > 0) { ?>
                            <li><a href="#">Cadastros</a>
                                <ul id="subSub">
                                <?php if ($ckb_nfe_cen_ == 1) { ?>
                                    <li><a id="nfe_1" href="<?php echo $UrlAtual; ?>/Modulos/NFE/Cenarios.php">Cenários</a></li>
                                <?php }if ($ckb_nfe_tra_ == 1) { ?>
                                    <li><a id="nfe_2" href="<?php echo $UrlAtual; ?>/Modulos/NFE/Transportadoras.php">Transportadoras</a></li>
                                <?php }if ($ckb_nfe_msg_ == 1) { ?>
                                    <li><a id="nfe_3" href="<?php echo $UrlAtual; ?>/Modulos/NFE/NF-Mensagens.php">Mensagens</a></li>
                                <?php }if ($ckb_nfe_emi_ == 1) { ?>
                                    <li><a id="nfe_4" href="<?php echo $UrlAtual; ?>/Modulos/NFE/Emitentes.php">Emitentes</a></li>
                                <?php }if ($ckb_nfe_est_ == 1) { ?>
                                    <li><a id="nfe_9" href="<?php echo $UrlAtual; ?>/Modulos/NFE/Estados.php">Estados</a></li>
                                <?php }if ($ckb_nfe_cid_ == 1) { ?>
                                    <li><a id="nfe_5" href="<?php echo $UrlAtual; ?>/Modulos/NFE/Cidades.php">Cidades</a></li>
                                <?php }if ($ckb_nfe_eqs_ == 1) { ?>
                                    <li><a id="nfe_8" href="<?php echo $UrlAtual; ?>/Modulos/NFE/Enquadramento-Servico.php">Enquadramento do Serviço</a></li>
                                <?php } ?>
                                </ul>
                            </li>
                        <?php } if ($ckb_nfe_not_ + $ckb_nfe_nos_ > 0) { ?>
                            <li><a href="#">Lançamentos</a>
                                <ul id="subSub">
                                <?php if ($ckb_nfe_not_ == 1) { ?>
                                    <li><a id="nfe_6" href="<?php echo $UrlAtual; ?>/Modulos/NFE/Emitir-Nota.php">Emis. Notas de Vendas</a></li>
                                <?php }if ($ckb_nfe_nos_ == 1) { ?>
                                    <li><a id="nfe_7" href="<?php echo $UrlAtual; ?>/Modulos/NFE/Emitir-Nota-Servico.php">Emis. Notas de Serviços</a></li>
                                <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } if ($mdl_crm_ > 0 && ($ckb_adm_email_ + $ckb_adm_disparos_ + $ckb_crm_pro_ + $ckb_crm_tel_ + $ckb_crm_rec_ + $ckb_crm_lead_ + $ckb_crm_lead_camp_ + $ckb_crm_chatbot_  > 0)) { ?>
                <li id="crm_0">
                    <a href="#">
                        <div class="menu-icon"><i class="material-menu">phone_in_talk</i></div>
                        <span>CRM</span>
                    </a>
                    <div class="open"></div>
                    <ul>
                        <?php if ($ckb_adm_email_ + $ckb_adm_disparos_ + $ckb_crm_lead_camp_ + $ckb_crm_chatbot_ > 0) { ?>
                            <li><a href="#">Cadastros</a>
                                <ul id="subSub">
                                    <?php if ($ckb_adm_email_ == 1) { ?>
                                        <li><a id="crm_11" href="<?php echo $UrlAtual; ?>/Modulos/CRM/EmailsList.php">Modelos de Disparos</a></li>                                      
                                    <?php } if ($ckb_adm_disparos_ == 1) { ?>
                                        <li><a id="crm_12" href="<?php echo $UrlAtual; ?>/Modulos/Comercial/DisparosAutoList.php">Disparos Automáticos</a></li>
                                    <?php } if ($ckb_crm_lead_camp_ == 1) { ?>
                                        <li><a id="crm_14" href="<?php echo $UrlAtual; ?>/Modulos/CRM/Lead-Campanha.php">Captação de Leads</a></li>
                                    <?php } if ($mdl_cht_ == 1 && $ckb_crm_chatbot_ == 1) { ?>
                                        <li><a id="crm_15" href="<?php echo $UrlAtual; ?>/Modulos/CRM/ChatbotList.php">Chatbot</a></li>
                                    <?php } ?>                                    
                                </ul>                        
                            </li>                                
                        <?php } if ($ckb_crm_pro_ + $ckb_crm_tel_ + $ckb_crm_rec_ + $ckb_crm_lead_ > 0) { ?>
                            <li><a href="#">Lançamentos</a>
                                <ul id="subSub">
                                    <?php if ($ckb_crm_rec_ == 1) { ?>
                                        <li><a id="crm_9" href="<?php echo $UrlAtual; ?>/Modulos/CRM/Recepcao.php<?php
                                            if (is_numeric($_SESSION["id_usuario"])) {
                                                echo "?us=" . $_SESSION["id_usuario"];
                                            } ?>">Recepção</a></li>
                                    <?php }if ($ckb_crm_lead_ == 1) { ?>
                                        <li><a id="crm_13" href="<?php echo $UrlAtual; ?>/Modulos/CRM/Lead.php">Leads</a></li>
                                    <?php }if ($ckb_crm_pro_ == 1) { ?>
                                        <li><a id="crm_2" href="<?php echo $UrlAtual; ?>/Modulos/CRM/Gerenciador-Prospects.php">Prospects</a></li>
                                    <?php } if ($ckb_crm_tel_ == 1) { ?>
                                        <li><a id="crm_3" href="<?php echo $UrlAtual; ?>/Modulos/CRM/Telemarketing.php<?php
                                            if (is_numeric($_SESSION["id_usuario"])) {
                                                echo "?us=" . $_SESSION["id_usuario"];
                                            } ?>">CRM</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } if ($mdl_ser_ > 0 && ($ckb_est_ser_ + $ckb_aca_ser_ + $ckb_ser_ser_ + $ckb_ser_rpc_  + $ckb_ser_cli_ > 0)) { ?>
                <li id="ser_0">
                    <a href="#">
                        <div class="menu-icon"><i class="material-menu">handyman</i></div>
                        <span>Serviços</span>
                    </a>
                    <div class="open"></div>
                    <ul>
                        <?php if ($mdl_aca_ == 0 && ($ckb_est_ser_ + $ckb_aca_ser_ > 0)) { ?>
                            <li><a href="#">Cadastros</a>
                                <ul id="subSub">
                                    <?php if ($ckb_est_ser_ == 1) { ?>
                                        <li><a id="ser_6" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Grupo-de-estoque.php?tp=1">Grupo de Serviços</a></li>
                                    <?php } if ($ckb_aca_ser_ == 1) { ?>
                                        <li><a id="ser_4" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Servicos.php">Serviços</a></li>
                                    <?php } ?>
                                </ul>
                            <li>
                        <?php } if ($ckb_ser_ser_ + $ckb_ser_cli_ > 0) { ?>
                            <li><a href="#">Lançamentos</a>
                                <ul id="subSub">
                                    <?php if ($ckb_ser_cli_ == 1 && $mdl_seg_ == 0) { ?>
                                        <li><a id="ser_2" href="<?php echo $UrlAtual; ?>/Modulos/Servicos/Gerenciador-ClientesSer.php">Clientes</a></li>
                                    <?php } if ($ckb_ser_ser_ == 1) { ?>
                                        <li><a id="ser_1" href="<?php echo $UrlAtual; ?>/Modulos/<?php echo ($mdl_seg_ == 1 ? "Seguro" : "Servicos"); ?>/ServicosSer.php<?php
                                            if (is_numeric($_SESSION["id_usuario"])) {
                                                echo "?us=" . $_SESSION["id_usuario"];
                                            } ?>">Serviços</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } if ($ckb_ser_rpc_ > 0) { ?>
                            <li><a href="#">Consultas</a>
                                <ul id="subSub">
                                    <?php if ($ckb_ser_rpc_ == 1) { ?>
                                        <li><a id="ser_5" href="<?php echo $UrlAtual; ?>/Modulos/Servicos/Relatorio-Pacotes.php">Rel. Pacotes</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } if (($contrato == "003") && ($ckb_siv_cadErp_ > 0)) { ?>
                <li id="siv_0">
                    <a href="#">
                        <div class="menu-icon"><i class="material-menu">database</i></div>
                        <span>SIVIS</span>
                    </a>
                    <div class="open"></div>
                    <ul>
                        <?php if ($ckb_siv_cadErp_ > 0) { ?>
                        <li><a href="#">Cadastros</a>
                            <ul id="subSub">
                                <?php if ($ckb_siv_cadErp_ > 0) { ?>
                                <li><a id="siv_5" href="<?php echo $UrlAtual; ?>/Modulos/Sivis/CadERP.php">CadERP</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } if ($mdl_seg_ == 1 && ($ckb_pro_grp_acess_ + $ckb_pro_acess_ + $ckb_pro_item_vis_ + $ckb_pro_tipo_vei_ + $ckb_operadoras_ + $ckb_pro_rastreadores_ + $ckb_pro_terceiros_ + $ckb_pro_tipo_vis_ + $ckb_pro_grp_cidade_ + $ckb_pro_grp_fabricantes_ + $ckb_pro_veiculo_ + $ckb_pro_vistoria_ + $ckb_pro_sinistro_ + $ckb_pro_oficina_ + $ckb_pro_classificacao_ + $ckb_pro_montadoras_ + $ckb_pro_modelos_ + $ckb_pro_serv_mod_ > 0)) { ?>
                <li id="car_0">
                    <a href="#">
                        <div class="menu-icon"><i class="material-menu">auto_towing</i></div>
                        <span>Proteção</span>
                    </a>
                    <div class="open"></div>
                    <ul>
                        <?php if ($ckb_pro_grp_acess_ + $ckb_pro_acess_ + $ckb_pro_item_vis_ + $ckb_pro_tipo_vei_ + $ckb_operadoras_ + $ckb_pro_rastreadores_ + $ckb_pro_terceiros_ + $ckb_pro_tipo_vis_ + $ckb_pro_grp_cidade_ + $ckb_pro_grp_fabricantes_ > 0) { ?>
                        <li> <a href="#"> Cadastros </a>
                            <ul id="subSub">
                                <?php if ($ckb_pro_grp_acess_ > 0) { ?>
                                <li><a id="car_2" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Grupo-de-estoque.php?tp=2">Grupo de Acessórios</a></li>
                                <?php } if ($ckb_pro_acess_ > 0) { ?>
                                <li><a id="car_1" href="<?php echo $UrlAtual; ?>/Modulos/Estoque/Acessorios.php">Acessórios</a></li>                                
                                <?php } if ($ckb_pro_tipo_vei_ > 0) { ?>
                                <li><a id="car_8" href="<?php echo $UrlAtual; ?>/Modulos/Seguro/VeiculoTipo.php">Tipo de Veículos</a></li>                                
                                <?php } if ($ckb_pro_rastreadores_ > 0) { ?>
                                <li><a id="car_17" href="<?php echo $UrlAtual; ?>/Modulos/Seguro/Rastreadores.php">Rastreadores</a></li>                                
                                <?php } if ($ckb_operadoras_ > 0) { ?>
                                <li><a id="car_18" href="<?php echo $UrlAtual; ?>/Modulos/Seguro/Operadoras.php">Operadoras</a></li>                                
                                <?php } if ($ckb_pro_item_vis_ > 0) { ?>
                                <li><a id="car_4" href="<?php echo $UrlAtual; ?>/Modulos/Seguro/VistoriaItens.php">Itens de Vistoria</a></li>
                                <?php } if ($ckb_pro_tipo_vis_ > 0) { ?>
                                <li><a id="car_6" href="<?php echo $UrlAtual; ?>/Modulos/Seguro/VistoriaTipo.php">Tipo de Vistoria</a></li>
                                <?php } if ($ckb_pro_grp_cidade_ > 0) { ?>                                
                                <li><a id="car_9" href="<?php echo $UrlAtual; ?>/Modulos/Seguro/GrupoCidade.php">Grupo de Cidades</a></li>
                                <?php } if ($ckb_pro_grp_fabricantes_ > 0) { ?>
                                <li><a id="car_10" href="<?php echo $UrlAtual; ?>/Modulos/Seguro/GrupoFabricante.php">Grupo de Fabricantes</a></li>                                
                                <?php } if ($ckb_pro_terceiros_ > 0) { ?>
                                <li><a id="car_19" href="<?php echo $UrlAtual; ?>/Modulos/Seguro/Terceiros.php">Terceiros</a></li>                                
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } if ($ckb_pro_classificacao_ + $ckb_pro_montadoras_ + $ckb_pro_modelos_ + $ckb_pro_serv_mod_ > 0) { ?>
                        <li> <a href="#"> Manutenção </a>
                            <ul id="subSub">                               
                                <?php if ($ckb_pro_classificacao_ > 0) { ?>
                                <li><a id="car_13" href="<?php echo $UrlAtual; ?>/Modulos/Seguro/Classificacao.php">Classificação</a></li>                                
                                <?php } if ($ckb_pro_montadoras_ > 0) { ?>
                                <li><a id="car_14" href="<?php echo $UrlAtual; ?>/Modulos/Seguro/Montadoras.php">Montadoras</a></li>
                                <?php } if ($ckb_pro_modelos_ > 0) { ?>
                                <li><a id="car_15" href="<?php echo $UrlAtual; ?>/Modulos/Seguro/Modelos.php">Modelos</a></li>
                                <?php } if ($ckb_pro_serv_mod_ > 0) { ?>                                
                                <li><a id="car_16" href="<?php echo $UrlAtual; ?>/Modulos/Seguro/ServicosVeiculos.php">Serviços por Modelos</a></li>
                                <?php } ?>                                
                            </ul>
                        </li>
                        <?php } if ($ckb_pro_veiculo_ + $ckb_pro_vistoria_ + $ckb_pro_sinistro_ + $ckb_pro_oficina_ > 0) { ?>                        
                        <li> <a href="#"> Gerenciamento </a>
                            <ul id="subSub">
                                <?php if ($ckb_pro_veiculo_ > 0) { ?>
                                <li><a id="car_3" href="<?php echo $UrlAtual; ?>/Modulos/Seguro/Veiculos.php">Veículos</a></li>
                                <?php } if ($ckb_pro_vistoria_ > 0) { ?>
                                <li><a id="car_5" href="<?php echo $UrlAtual; ?>/Modulos/Seguro/Vistorias.php">Vistorias</a></li>
                                <?php } if ($ckb_pro_sinistro_ > 0) { ?>
                                <li><a id="car_11" href="<?php echo $UrlAtual; ?>/Modulos/Seguro/Sinistros.php">Eventos</a></li>
                                <?php } if ($ckb_pro_oficina_ > 0) { ?>
                                <li><a id="car_12" href="<?php echo $UrlAtual; ?>/Modulos/Seguro/Oficinas.php">Oficinas</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>                        
                    </ul>
                </li>
            <?php } if ($ckb_out_aju_ == 1) { ?>
                <li>
                    <a id="ind_3" href="<?php echo $UrlAtual; ?>/Modulos/Servicos/Faq.php">
                        <div class="menu-icon"><i class="material-menu">help</i></div>
                        <span>Ajuda</span>
                    </a>
                </li>
            <?php } ?>
        </ul>
        <div style="clear:both"></div>
    </div>
    <link type="text/css" rel="stylesheet" media="all" href="<?php echo $UrlAtual; ?>/css/chat.css" />
    <script type="text/javascript">
        
        var windowFocus = true;
        var username;
        var chatHeartbeatCount = 0;
        var minChatHeartbeat = 1000;
        var maxChatHeartbeat = 33000;
        var chatHeartbeatTime = minChatHeartbeat;
        var originalTitle;
        var blinkOrder = 0;
        var chatboxFocus = new Array();
        var newMessages = new Array();
        var newMessagesWin = new Array();
        var chatBoxes = new Array();

        $(document).ready(function () {
            originalTitle = document.title;
            startChatSession();
            $([window, document]).blur(function () {
                windowFocus = false;
            }).focus(function () {
                windowFocus = true;
                document.title = originalTitle;
            });
        });

        function restructureChatBoxes() {
            align = 0;
            for (x in chatBoxes) {
                chatboxtitle = chatBoxes[x];
                if ($("#chatbox_" + chatboxtitle).css('display') !== 'none') {
                    if (align === 0) {
                        $("#chatbox_" + chatboxtitle).css('right', '20px');
                    } else {
                        width = (align) * (225 + 7) + 20;
                        $("#chatbox_" + chatboxtitle).css('right', width + 'px');
                    }
                    align++;
                }
            }
        }

        function chatWith(chatuser) {
            createChatBox(chatuser);
            $("#chatbox_" + chatuser + " .chatboxtextarea").focus();
        }

        function createChatBox(chatboxtitle, minimizeChatBox) {
            if ($("#chatbox_" + chatboxtitle).length > 0) {
                if ($("#chatbox_" + chatboxtitle).css('display') === 'none') {
                    $("#chatbox_" + chatboxtitle).css('display', 'block');
                    restructureChatBoxes();
                }
                $("#chatbox_" + chatboxtitle + " .chatboxtextarea").focus();
                return;
            }

            $("<div/>").attr("id", "chatbox_" + chatboxtitle)
                    .addClass("chatbox")
                    .html('<div class="chatboxhead"><div class="chatboxtitle">' + chatboxtitle + '</div><div class="chatboxoptions"><a href="javascript:void(0)" onclick="javascript:toggleChatBoxGrowth(\'' + chatboxtitle + '\')">-</a> <a href="javascript:void(0)" onclick="javascript:closeChatBox(\'' + chatboxtitle + '\')">X</a></div><br clear="all"/></div><div class="chatboxcontent"></div><div class="chatboxinput"><textarea class="chatboxtextarea" onkeydown="javascript:return checkChatBoxInputKey(event,this,\'' + chatboxtitle + '\');"></textarea></div>')
                    .appendTo($("body"));
            $("#chatbox_" + chatboxtitle).css('bottom', '0px');
            chatBoxeslength = 0;
            for (x in chatBoxes) {
                if ($("#chatbox_" + chatBoxes[x]).css('display') !== 'none') {
                    chatBoxeslength++;
                }
            }

            if (chatBoxeslength === 0) {
                $("#chatbox_" + chatboxtitle).css('right', '20px');
            } else {
                width = (chatBoxeslength) * (225 + 7) + 20;
                $("#chatbox_" + chatboxtitle).css('right', width + 'px');
            }

            chatBoxes.push(chatboxtitle);
            if (minimizeChatBox === 1) {
                minimizedChatBoxes = new Array();
                if ($.cookie('chatbox_minimized')) {
                    minimizedChatBoxes = $.cookie('chatbox_minimized').split(/\|/);
                }
                minimize = 0;
                for (j = 0; j < minimizedChatBoxes.length; j++) {
                    if (minimizedChatBoxes[j] === chatboxtitle) {
                        minimize = 1;
                    }
                }
                if (minimize === 1) {
                    $('#chatbox_' + chatboxtitle + ' .chatboxcontent').css('display', 'none');
                    $('#chatbox_' + chatboxtitle + ' .chatboxinput').css('display', 'none');
                }
            }

            chatboxFocus[chatboxtitle] = false;
            $("#chatbox_" + chatboxtitle + " .chatboxtextarea").blur(function () {
                chatboxFocus[chatboxtitle] = false;
                $("#chatbox_" + chatboxtitle + " .chatboxtextarea").removeClass('chatboxtextareaselected');
            }).focus(function () {
                chatboxFocus[chatboxtitle] = true;
                newMessages[chatboxtitle] = false;
                $('#chatbox_' + chatboxtitle + ' .chatboxhead').removeClass('chatboxblink');
                $("#chatbox_" + chatboxtitle + " .chatboxtextarea").addClass('chatboxtextareaselected');
            });

            $("#chatbox_" + chatboxtitle).click(function () {
                if ($('#chatbox_' + chatboxtitle + ' .chatboxcontent').css('display') != 'none') {
                    $("#chatbox_" + chatboxtitle + " .chatboxtextarea").focus();
                }
            });

            $("#chatbox_" + chatboxtitle).show();
        }

        function chatHeartbeat() {
            var itemsfound = 0;
            if (windowFocus == false) {
                var blinkNumber = 0;
                var titleChanged = 0;
                for (x in newMessagesWin) {
                    if (newMessagesWin[x] == true) {
                        ++blinkNumber;
                        if (blinkNumber >= blinkOrder) {
                            document.title = x + ' diz...';
                            titleChanged = 1;
                            break;
                        }
                    }
                }
                if (titleChanged == 0) {
                    document.title = originalTitle;
                    blinkOrder = 0;
                } else {
                    ++blinkOrder;
                }
            } else {
                for (x in newMessagesWin) {
                    newMessagesWin[x] = false;
                }
            }

            for (x in newMessages) {
                if (newMessages[x] == true) {
                    if (chatboxFocus[x] == false) {
                        $('#chatbox_' + x + ' .chatboxhead').toggleClass('chatboxblink');
                    }
                }
            }

            $.ajax({
                url: "<?php echo $UrlAtual; ?>/Connections/chat.php?action=chatheartbeat",
                cache: false,
                dataType: "json",
                success: function (data) {
                    $.each(data.items, function (i, item) {
                        if (item) { // fix strange ie bug
                            chatboxtitle = item.f;
                            if ($("#chatbox_" + chatboxtitle).length <= 0) {
                                createChatBox(chatboxtitle);
                            }
                            if ($("#chatbox_" + chatboxtitle).css('display') == 'none') {
                                $("#chatbox_" + chatboxtitle).css('display', 'block');
                                restructureChatBoxes();
                            }
                            if (item.s == 1) {
                                item.f = username;
                            }
                            if (item.s == 2) {
                                $("#chatbox_" + chatboxtitle + " .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxinfo">' + item.m + '</span></div>');
                            } else {
                                newMessages[chatboxtitle] = true;
                                newMessagesWin[chatboxtitle] = true;
                                $("#chatbox_" + chatboxtitle + " .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxmessagefrom">' + item.f + ':</span>&nbsp;<span class="chatboxmessagecontent">' + item.m + '</span></div>');
                            }
                            $("#chatbox_" + chatboxtitle + " .chatboxcontent").scrollTop($("#chatbox_" + chatboxtitle + " .chatboxcontent")[0].scrollHeight);
                            itemsfound += 1;
                        }
                    });
                    chatHeartbeatCount++;
                    if (itemsfound > 0) {
                        chatHeartbeatTime = minChatHeartbeat;
                        chatHeartbeatCount = 1;
                    } else if (chatHeartbeatCount >= 10) {
                        chatHeartbeatTime *= 2;
                        chatHeartbeatCount = 1;
                        if (chatHeartbeatTime > maxChatHeartbeat) {
                            chatHeartbeatTime = maxChatHeartbeat;
                        }
                    }
                    setTimeout('chatHeartbeat();', chatHeartbeatTime);
                }
            });
        }

        function closeChatBox(chatboxtitle) {
            $('#chatbox_' + chatboxtitle).css('display', 'none');
            restructureChatBoxes();
            $.post("<?php echo $UrlAtual; ?>/Connections/chat.php?action=closechat", {
                chatbox: chatboxtitle
            }, function (data) {
            });
        }

        function toggleChatBoxGrowth(chatboxtitle) {
            if ($('#chatbox_' + chatboxtitle + ' .chatboxcontent').css('display') == 'none') {
                var minimizedChatBoxes = new Array();
                if ($.cookie('chatbox_minimized')) {
                    minimizedChatBoxes = $.cookie('chatbox_minimized').split(/\|/);
                }
                var newCookie = '';
                for (i = 0; i < minimizedChatBoxes.length; i++) {
                    if (minimizedChatBoxes[i] != chatboxtitle) {
                        newCookie += chatboxtitle + '|';
                    }
                }
                newCookie = newCookie.slice(0, -1)
                $.cookie('chatbox_minimized', newCookie);
                $('#chatbox_' + chatboxtitle + ' .chatboxcontent').css('display', 'block');
                $('#chatbox_' + chatboxtitle + ' .chatboxinput').css('display', 'block');
                $("#chatbox_" + chatboxtitle + " .chatboxcontent").scrollTop($("#chatbox_" + chatboxtitle + " .chatboxcontent")[0].scrollHeight);
            } else {
                var newCookie = chatboxtitle;
                if ($.cookie('chatbox_minimized')) {
                    newCookie += '|' + $.cookie('chatbox_minimized');
                }
                $.cookie('chatbox_minimized', newCookie);
                $('#chatbox_' + chatboxtitle + ' .chatboxcontent').css('display', 'none');
                $('#chatbox_' + chatboxtitle + ' .chatboxinput').css('display', 'none');
            }
        }

        function checkChatBoxInputKey(event, chatboxtextarea, chatboxtitle) {
            if (event.keyCode == 13 && event.shiftKey == 0) {
                message = $(chatboxtextarea).val();
                message = message.replace(/^\s+|\s+$/g, "");
                $(chatboxtextarea).val('');
                $(chatboxtextarea).focus();
                $(chatboxtextarea).css('height', '44px');
                if (message != '') {
                    $.post("<?php echo $UrlAtual; ?>/Connections/chat.php?action=sendchat", {
                        to: chatboxtitle,
                        message: message
                    }, function (data) {
                        message = message.replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/\"/g, "&quot;");
                        $("#chatbox_" + chatboxtitle + " .chatboxcontent").append('<div class="chatboxmessage chatme"><span class="chatboxmessagefrom">' + username + ':</span>&nbsp;<span class="chatboxmessagecontent">' + message + '</span></div>');
                        $("#chatbox_" + chatboxtitle + " .chatboxcontent").scrollTop($("#chatbox_" + chatboxtitle + " .chatboxcontent")[0].scrollHeight);
                    });
                }
                chatHeartbeatTime = minChatHeartbeat;
                chatHeartbeatCount = 1;
                return false;
            }

            var adjustedHeight = chatboxtextarea.clientHeight;
            var maxHeight = 94;
            if (maxHeight > adjustedHeight) {
                adjustedHeight = Math.max(chatboxtextarea.scrollHeight, adjustedHeight);
                if (maxHeight)
                    adjustedHeight = Math.min(maxHeight, adjustedHeight);
                if (adjustedHeight > chatboxtextarea.clientHeight)
                    $(chatboxtextarea).css('height', adjustedHeight + 8 + 'px');
            } else {
                $(chatboxtextarea).css('overflow', 'auto');
            }
        }

        function startChatSession() {
            $.ajax({
                url: "<?php echo $UrlAtual; ?>/Connections/chat.php?action=startchatsession",
                cache: false,
                dataType: "json",
                success: function (data) {
                    username = data.username;
                    $.each(data.items, function (i, item) {
                        if (item) { // fix strange ie bug
                            chatboxtitle = item.f;
                            if ($("#chatbox_" + chatboxtitle).length <= 0) {
                                createChatBox(chatboxtitle, 1);
                            }
                            if (item.s == 1) {
                                item.f = username;
                            }
                            if (item.s == 2) {
                                $("#chatbox_" + chatboxtitle + " .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxinfo">' + item.m + '</span></div>');
                            } else {
                                if (item.s == 1) {
                                    var cor = " chatme";
                                } else {
                                    var cor = "";
                                }
                                $("#chatbox_" + chatboxtitle + " .chatboxcontent").append('<div class="chatboxmessage' + cor + '"><span class="chatboxmessagefrom">' + item.f + ':</span>&nbsp;<span class="chatboxmessagecontent">' + item.m + '</span></div>');
                            }
                        }
                    });
                    for (i = 0; i < chatBoxes.length; i++) {
                        chatboxtitle = chatBoxes[i];
                        $("#chatbox_" + chatboxtitle + " .chatboxcontent").scrollTop($("#chatbox_" + chatboxtitle + " .chatboxcontent")[0].scrollHeight);
                        setTimeout('$("#chatbox_"+chatboxtitle+" .chatboxcontent").scrollTop($("#chatbox_"+chatboxtitle+" .chatboxcontent")[0].scrollHeight);', 100); // yet another strange ie bug
                    }
                    setTimeout('chatHeartbeat();', chatHeartbeatTime);
                }
            });
        }

        jQuery.cookie = function (name, value, options) {
            if (typeof value != 'undefined') { // name and value given, set cookie
                options = options || {};
                if (value === null) {
                    value = '';
                    options.expires = -1;
                }
                var expires = '';
                if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
                    var date;
                    if (typeof options.expires == 'number') {
                        date = new Date();
                        date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
                    } else {
                        date = options.expires;
                    }
                    expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
                }
                var path = options.path ? '; path=' + (options.path) : '';
                var domain = options.domain ? '; domain=' + (options.domain) : '';
                var secure = options.secure ? '; secure' : '';
                document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
            } else { // only name given, get cookie
                var cookieValue = null;
                if (document.cookie && document.cookie != '') {
                    var cookies = document.cookie.split(';');
                    for (var i = 0; i < cookies.length; i++) {
                        var cookie = jQuery.trim(cookies[i]);
                        // Does this cookie string begin with the name we want?
                        if (cookie.substring(0, name.length + 1) == (name + '=')) {
                            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                            break;
                        }
                    }
                }
                return cookieValue;
            }
        };
        
        function contatos_online(tipo) {
            if (tipo) {
                $('.tip[style*="border-left:solid 3px #FFF"]').parent().hide();
                $('.tip[style*="border-left:solid 3px greenyellow"]').parent().show();
            } else {
                $('.tip[style*="border-left:solid 3px #FFF"]').parent().show();
                $('.tip[style*="border-left:solid 3px greenyellow"]').parent().hide();
            }
        }
    </script>
    <div style="text-align: center; margin-bottom: 5px;">
        <div class="bola"></div>
        <a href="javascript:void(0)" title="Online" style="color: #C9C9C9;" onclick="contatos_online(true);">On-line</a>        
        <div class="bolaoff" style="margin-left: 20px;"></div>
        <a href="javascript:void(0)" title="Offline" style="color: #C9C9C9;" onclick="contatos_online(false);">Off-line</a>
    </div>    
    <?php $cur = odbc_exec($con, "select id_usuario,login_user,imagem,"
            . "case when ultimo_acesso is null then 0 when datediff(second,ultimo_acesso,getdate()) < 30 then 1 else 0 "
            . "end ultimo_acesso from sf_usuarios where id_usuario not in (" . $_SESSION["id_usuario"] . ") and inativo = 0 "
            . "order by login_user");
    while ($RFP = odbc_fetch_array($cur)) { ?>
        <a href="javascript:void(0)" style="<?php echo ($RFP['ultimo_acesso'] == 0 ? "display: none;" : ""); ?>" title="<?php echo utf8_encode($RFP['login_user']); ?>" onclick="javascript:chatWith('<?php echo str_replace(array(" ", "."), "_", utf8_encode($RFP['login_user'])); ?>')">
            <img class="tip" src="<?php
            if ($RFP['imagem'] != '') {
                echo "./../../Pessoas/" . $contrato . "/" . $RFP['imagem'];
            } else {
                echo "./../../img/dmitry_m.gif";
            }
            if ($RFP['ultimo_acesso'] == 0) {
                $style = 'style="border-left:solid 3px #FFF; float:left;width: 30px;height: 30px;"';
            } else {
                $style = 'style="border-left:solid 3px greenyellow; float:left;width: 30px;height: 30px;"';
            }
            ?>" align="left" <?php echo $style; ?> height="30" title="<?php echo utf8_encode($RFP['login_user']); ?>"/>
        </a>
    <?php } ?>        
    <div class="widget">
        <div class="datepicker"></div>
    </div>    
    <script type="text/javascript">
        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
        }
        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(";");
            for(var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while(c.charAt(0) === " ") c = c.substring(1);
                if(c.indexOf(name) === 0) return c.substring(name.length, c.length);
            }
            return "";
        }
        function changeMenu(exec){
            var modulo;
            if($("#system").val() == 1){
              modulo = "100%";
            }else{
              modulo = "";
            }
            if(getCookie("menu") !== "FECHADO" || exec === 1){
                if($("#system").val() != 1){
                   document.getElementsByClassName("logo")[0].style.backgroundImage = "url('<?php echo $UrlAtual . $logoMnuLatThumb; ?>')";
                   $(".sidebar .top .logo").css("height",40);
                }else{
                   $(".sidebar .top .logo").css("height",20); 
                }
                $(".body").css("margin-left",80);
                $(".sidebar").css("width",50);
                $(".sidebar .top").css("width",50);
                $(".sidebar .top .logo").css("width",50);                
                $(".sidebar .top .logo").css("background-size",50);
                $(".sidebar .navigation.bordered").css("width",50);
                $(".sidebar .navigation.bordered > li").css("width",50);
                $(".sidebar .navigation.bordered > li > a").css("width",32);
                $(".sidebar .navigation.bordered > li .open").css("width",14);
                $(".sidebar .navigation.bordered > li > ul").addClass("closed");
                $(".sidebar .navigation.bordered > li > a > span").fadeOut();
                $(".sidebar > a, .sidebar > .widget").hide();
                $(".button_label .txt_label").hide();
                $(".table_label").hide();                
                $(".button_theme").css("width","55%");
                setCookie("menu", "FECHADO", 365);
            } else {
                if($("#system").val() != 1){
                   document.getElementsByClassName("logo")[0].style.backgroundImage = "url('<?php echo $UrlAtual . $logoMnuLat; ?>')";
                }
                $(".body").css("margin-left","");
                $(".sidebar").css("width","");
                $(".sidebar .top").css("width","");
                $(".sidebar .top .logo").css("width","");
                $(".sidebar .top .logo").css("height","");
                $(".sidebar .top .logo").css("background-size", modulo);
                $(".sidebar .navigation.bordered").css("width","");
                $(".sidebar .navigation.bordered > li").css("width","");
                $(".sidebar .navigation.bordered > li > a").css("width","");
                $(".sidebar .navigation.bordered > li .open").css("width","");
                $(".sidebar .navigation.bordered > li > ul").removeClass("closed");
                $(".sidebar .navigation.bordered > li > a > span").fadeIn();
                $(".sidebar > a, .sidebar > .widget").delay(1000).show(0);
                $(".button_label .txt_label").show();
                $(".table_label").show();
                $(".button_theme").css("width","85%");
                setCookie("menu", "ABERTO", 365);
            }
            if(getCookie("submenu") === "FECHADO"){ $(".sidebar .navigation > li").removeClass("active"); }
        }
        $(document).ready(function (){
            if(getCookie("menu") === "FECHADO"){
                setTimeout(function(){ changeMenu(1); }, 1000);
            }
            $(".sidebar .navigation > li > a, .sidebar .navigation > li > .open").click(function(){
                setTimeout(function(){
                    if($(".sidebar .navigation").find("li.active").length !== 0){
                        setCookie("submenu", "ABERTO", 365);
                    } else { setCookie("submenu", "FECHADO", 365); }
                }, 100);
            });
        });
    </script>
</div>
<footer class="footers d-print-none" id="footer-dash">
    <div class="container-fluid">
        <div class="row justify-content-center">            
            <div class="col-md-4" style="text-align: center; color: #C9C9C9;"> 
                Desenvolvido por <a style="color: #C9C9C9;" target="_blank" href="http://www.sivis.com.br/"><b> SIVIS Tecnologia</b></a>
            </div>
        </div>
    </div>
</footer>