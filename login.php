<?php

$contrato = "";
$bg_main = "background-color: #361973;";
$bg_button = "";
$logoMnuLat = "img/logo_login_sivis.png";

/*if (is_numeric($_GET["emp"]) && in_array($_GET["emp"], ["503","507","800","866","901","905","907","910","926","968","977"])) {
    header("Location: https://sivisweb.com.br/login.php?emp=" . $contrato);
} else */
if (isset($_GET["emp"]) && is_numeric($_GET["emp"]) && $_GET["emp"] > 0 and $_GET["emp"] < 1000) {    
    include("./Connections/configSivis.php");
    include("./Connections/funcoesAux.php");  
    $cur2 = odbc_exec($con2, "select *,(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = id_fornecedores_despesas) email
    from ca_contratos left join sf_fornecedores_despesas on id_fornecedores_despesas = id_cliente 
    left join tb_estados on estado_codigo = estado
    left join tb_cidades on cidade_codigo = cidade 
    where inativa = 0 and numero_contrato = '" . $_GET['emp'] . "'");
    while ($RFP = odbc_fetch_array($cur2)) {
        $contrato = utf8_encode($RFP['numero_contrato']);
        $hostname = utf8_encode($RFP['local']);
        $username = utf8_encode($RFP['login']);
        $password = utf8_encode($RFP['senha']);
        $database = utf8_encode($RFP['banco']);
    }
    odbc_close($con2);
    if (is_numeric($contrato)) {        
        $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
        $cur = odbc_exec($con, "select * from sf_configuracao where id = 1") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $bg_main = "background-color: " . $RFP['cor_primaria'];
            $bg_button = "background-color: " . $RFP['cor_secundaria'];
        }
        if (file_exists(__DIR__ . "/Pessoas/" . $contrato . "/Empresa/Menu/logo.png")) {
            $logoMnuLat = "/Pessoas/" . $contrato . "/Empresa/Menu/logo.png";
        }  
        odbc_close($con);
    }
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Login - SIVIS Finance</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/ico" href="favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap3.3.7.min.css">
        <link rel="stylesheet" type="text/css" href="css/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/fonts/utilIndex.css">
        <link rel="stylesheet" type="text/css" href="css/fonts/mainIndex.css">
    </head>
    <body>
        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100">
                    <form class="login100-form validate-form" style="<?php echo $bg_main; ?>" action="login_vai.php" method="post">                        
                        <span class="login100-form-title p-b-10 p-t-40">
                            LOGIN
                        </span>
                        <img src="<?php echo $logoMnuLat; ?>" width="60%" class="center-block">       
                        <br>
                        <div class="wrap-input100 validate-input" data-validate="Loja Inválida">
                            <input class="input100 has-val" type="text" name="empresa" value="<?php echo $contrato; ?>">
                            <span class="focus-input100"></span>
                            <span class="label-input100">Loja</span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Usuário Inválido">
                            <input class="input100 has-val" type="text" name="login">
                            <span class="focus-input100"></span>
                            <span class="label-input100">Usuário</span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Senha Inválida">
                            <input class="input100 has-val" type="password" name="password">
                            <span class="focus-input100"></span>
                            <span class="label-input100">Senha</span>
                        </div>
                        <div class="flex-sb-m w-full p-t-3 p-b-32">
                            <div class="contact100-form-checkbox">
                                <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                                <label class="label-checkbox100" for="ckb1">
                                    Lembrar de mim
                                </label>
                            </div>
                            <div>
                                <a href="#" class="txt1"></a>
                            </div>
                        </div>
                        <div class="container-login100-form-btn">
                            <button class="login100-form-btn" name="bntLogin" style="<?php echo $bg_button; ?>">
                                Entrar
                            </button>
                        </div>
                        <div class="text-center p-t-20 p-b-10">
                            <span class="txt2"></span>
                        </div>
                        <div class="login100-form-social flex-c-m">
                            <a href="https://www.facebook.com/sivistecnologia/" class="login100-form-social-item flex-c-m bg1 m-r-5">
                                <i class="fa fa-facebook-f" aria-hidden="true"></i>
                            </a>
                            <a href="https://www.instagram.com/sivistecnologia/" class="login100-form-social-item flex-c-m bg3 m-r-5">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                            </a>
                            <a href="https://br.linkedin.com/company/sivistecnologia" class="login100-form-social-item flex-c-m bg2 m-r-5">
                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>
                        </div>
                    </form>
                    <div class="login100-more" style="background-image: url('img/sivis_login.png');"></div>
                </div>
            </div>
        </div>
        <script src="js/plugins/jquery/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="js/plugins/bootstrap/bootstrap.min.js" type="text/javascript"></script>        
        <script src="js/mainLogin.js" type="text/javascript"></script>
        <script type="text/javascript">
            
            $(window).load(function () {
                if ($("input[name=empresa]").val().length > 0) {
                    $("input[name=login]").focus();
                } else {
                    $("input[name=empresa]").focus();
                }
            });
            
            $("input[name=empresa]").on("change", function() {
                window.location.href = window.location.origin + window.location.pathname + "?emp=" + $("input[name=empresa]").val();
            });
        </script>
    </body>        
</html>