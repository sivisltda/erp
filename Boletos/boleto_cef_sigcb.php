<?php
$dadosboleto["campo_fixo_obrigatorio"] = $campo_fixo_obrigatorio;       // campo fixo obrigatorio - valor = 1 
$dadosboleto["inicio_nosso_numero"] = $inicio_nosso_numero;          // Inicio do Nosso numero - obrigatoriamente deve come�ar com 9;
$dadosboleto["nosso_numero"] = $nosso_numero;  // Nosso numero sem o DV - REGRA: Máximo de 16 caracteres! (Pode ser um n�mero sequencial do sistema, o cpf ou o cnpj)

// Composi��o Nosso Numero - CEF SIGCB
$dadosboleto["nosso_numero_const1"] = $registro; //constanto 1 , 1=registrada , 2=sem registro
$dadosboleto["nosso_numero_const2"] = "4"; //constanto 2 , 4=emitido pelo proprio cliente

if(strlen($nosso_numero) == 15){
    $dadosboleto["nosso_numero1"] = "900"; // tamanho 3
    $dadosboleto["nosso_numero2"] = "000"; // tamanho 3
    $dadosboleto["nosso_numero3"] = substr($nosso_numero,(strlen($nosso_numero)-8),strlen($nosso_numero)); // tamanho 9
}

$dadosboleto["numero_documento"] = $numero_documento;	// Num do pedido ou do documento
$dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"] = $data_documento; // Data de emissão do Boleto
$dadosboleto["data_processamento"] = $data_processamento; // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = $sacado;
$dadosboleto["sacadocnpj"] = $sacadocnpj;
$dadosboleto["endereco1"] = $endereco1;
$dadosboleto["endereco2"] = $endereco2;

// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = $demonstrativo1;
$dadosboleto["demonstrativo2"] = $demonstrativo2;
$dadosboleto["demonstrativo3"] = $demonstrativo3;

// INSTRUÇÕES PARA O CAIXA
$dadosboleto["instrucoes1"] = $instrucoes1;
$dadosboleto["instrucoes2"] = $instrucoes2;
$dadosboleto["instrucoes3"] = $instrucoes3;
$dadosboleto["instrucoes4"] = $instrucoes4;

// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = $quantidade;
$dadosboleto["valor_unitario"] = $valor_unitario;
$dadosboleto["aceite"] = $aceite;		
$dadosboleto["especie"] = $especie;
$dadosboleto["especie_doc"] = $especie_doc;


// ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //


// DADOS DA SUA CONTA - CEF
$dadosboleto["agencia"] = $agencia; // Num da agencia, sem digito
$dadosboleto["conta"] = $conta;     // Num da conta, sem digito
$dadosboleto["conta_dv"] = $dv;     // Digito do Num da conta

// DADOS PERSONALIZADOS - CEF
$dadosboleto["conta_cedente"] = str_replace(" ","",$cli_conta); //$cli_conta // Código Cedente do Cliente, com 6 digitos (Somente Números)
$dadosboleto["carteira"] = $cli_carteira;     // Código da Carteira: pode ser SR (Sem Registro) ou CR (Com Registro) - (Confirmar com gerente qual usar)

// SEUS DADOS
$dadosboleto["identificacao"] = $identificacao;
$dadosboleto["cpf_cnpj"] = $cpf_cnpj;
$dadosboleto["endereco"] = $endereco;
$dadosboleto["cidade_uf"] = $cidade_uf;
$dadosboleto["cedente"] = $cedente;

// NÃO ALTERAR!
include("include/funcoes_cef_sigcb.php"); 
if (isset($dicionario)){
}elseif ($tipo_bol == 0){
    include("include/layout_cef.php");
}elseif ($tipo_bol == 8){
    include("include/layout_cef2.php");
}
