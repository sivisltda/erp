<?php

//---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO ----------------------------------------------
$dadosboleto["codigo_cliente"] = str_replace(" ", "", $cli_conta); // Código do Cliente (PSK) (Somente 7 digitos)
$dadosboleto["ponto_venda"] = $agencia; // Ponto de Venda = Agencia
$dadosboleto["carteira"] = $cli_carteira;     // Código da Carteira: pode ser SR (Sem Registro) ou CR (Com Registro) - (Confirmar com gerente qual usar)
$dadosboleto["carteira_descricao"] = "ECR";  // Descri��o da Carteira

$dadosboleto["nosso_numero"] = $nosso_numero;  // Nosso numero - REGRA: Máximo de 8 caracteres!
$dadosboleto["numero_documento"] = $numero_documento; // Num do pedido ou do documento
$dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"] = $data_documento; // Data de emissão do Boleto
$dadosboleto["data_processamento"] = $data_processamento; // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto;  // Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula

if (is_numeric($desconto) && $desconto > 0) {
    $dadosboleto["desconto"] = escreverNumero($desconto);
}
// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = $sacado;
$dadosboleto["sacadocnpj"] = $sacadocnpj;
$dadosboleto["endereco1"] = $endereco1;
$dadosboleto["endereco2"] = $endereco2;

// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = $demonstrativo1;
$dadosboleto["demonstrativo2"] = $demonstrativo2;
$dadosboleto["demonstrativo3"] = $demonstrativo3;

// INSTRUÇÕES PARA O CAIXA
$dadosboleto["instrucoes1"] = $instrucoes1;
$dadosboleto["instrucoes2"] = $instrucoes2;
$dadosboleto["instrucoes3"] = $instrucoes3;
$dadosboleto["instrucoes4"] = $instrucoes4;

// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = $quantidade;
$dadosboleto["valor_unitario"] = $valor_unitario;
$dadosboleto["aceite"] = $aceite;
$dadosboleto["especie"] = $especie;
$dadosboleto["especie_doc"] = $especie_doc;

// SEUS DADOS
$dadosboleto["identificacao"] = $identificacao;
$dadosboleto["cpf_cnpj"] = $cpf_cnpj;
$dadosboleto["endereco"] = $endereco;
$dadosboleto["cidade_uf"] = $cidade_uf;
$dadosboleto["cedente"] = $cedente;

// NÃO ALTERAR!
include("include/funcoes_santander_banespa.php");
if (isset($dicionario)){
}elseif ($tipo_bol == 5){
    include("include/layout_santander_banespa.php");
}elseif ($tipo_bol == 10){
    include("include/layout_santander_banespa2.php");
}
