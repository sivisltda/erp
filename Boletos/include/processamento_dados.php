<?php

$identificacao = "Sivis Web - Sistema financeiro";
$campo_fixo_obrigatorio = "1";
$inicio_nosso_numero = "9";
$msg_banco = "";
$desconto = "";
$totCli = 0;

$count = explode("|", $id_get);

for ($fab = 0; $fab < count($count); $fab++) {
    $WhereA = '0';
    $WhereV = '0';
    $WhereM = '0';
    $WhereD = '0';
    $nosso_numero = '';
    $multa = 0;
    $juros = 0;
    $item = explode("-", $count[$fab]);
    $teste = false;
    $tipo_bol = "0";
    $tipo_bol2 = "0";
    if (count($item) == 2) {
        $especie = $item[0];
        if ($item[0] == 'A') {
            if (is_numeric($item[1])) {
                $WhereA = $item[1];
                $teste = true;
            }
        } else if ($item[0] == 'V') {
            if (is_numeric($item[1])) {
                $WhereV = $item[1];
                $teste = true;
            }
        } else if ($item[0] == 'M') {
            if (is_numeric($item[1])) {
                $WhereM = $item[1];
                $teste = true;
            }
        } else if ($item[0] == 'D' || $item[0] == 'Y') {
            if (is_numeric($item[1])) {
                $WhereD = $item[1];
                $teste = true;
            }
        }
    }
    if (count($item) == 2 && $teste = true) {
        $cliente = 0;
        $cur = odbc_exec($con, "select f.razao_social razao_social,endereco,numero,complemento,bairro,cidade_nome,estado_sigla,cep,isnull(sf_carteira,b.carteira) carteira,sf_carteira_registrada,
            'PAG MENSALIDADE' historico_baixa,'MENSALIDADE' descricao,'01/01' pa,bol_valor valor_parcela,
            agencia,conta,cedente,dv_cedente,especie_doc,mensagem1,mensagem2,mensagem3,mensagem4,mensagem5,mensagem6,b.multa,b.juros,
            bol_nosso_numero,isnull(bol_data_parcela,dt_inicio_mens) data_parcela,bol_data_criacao data_cadastro,id_mens id_parcela,
            id_mens sa_descricao,dt_pagamento_mens data_pagamento,b.tipo_bol,b.tipo_bol2,f.cnpj fcnpj,bol_data_criacao,f.id_fornecedores_despesas,isnull(bol_valor,0) bol_valor,bol_desconto, msg_email, dt_inicio_mens, dt_fim_mens, bol_descricao 
            from sf_boleto inner join sf_vendas_planos_mensalidade on sf_boleto.id_referencia = sf_vendas_planos_mensalidade.id_mens and tp_referencia = 'M' 
            inner join sf_vendas_planos on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens 
            left join sf_vendas_itens on sf_vendas_itens.id_item_venda = sf_vendas_planos_mensalidade.id_item_venda_mens 
            left join sf_tipo_documento on sf_boleto.tipo_documento = sf_tipo_documento.id_tipo_documento
            inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = favorecido
            inner join sf_bancos b on b.id_bancos = bol_id_banco                            
            left join tb_cidades cid on cid.cidade_codigo = f.cidade
            left join tb_estados est on est.estado_codigo = f.estado
            left join sf_bancos_carteiras bc on bc.sf_carteiras_id = carteira_id
            where sf_boleto.inativo = 0 and id_mens in (" . $WhereM . ")") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            if (is_numeric($RFP['bol_valor'])) {
                $valor_cobrado = $RFP['bol_valor'];
            } else {
                $valor_cobrado = $RFP['valor_parcela'];
            }
            // DADOS DO SEU CLIENTE
            $totCli++;
            $cliente = $RFP['id_fornecedores_despesas'];
            $pagoEm = utf8_encode($RFP['data_pagamento']);
            $sacado = utf8_encode($RFP['razao_social']);
            $sacadocnpj = utf8_encode($RFP['fcnpj']);
            $endereco1 = utf8_encode($RFP['endereco']) . " n° " . utf8_encode($RFP['numero']) . " " . utf8_encode($RFP['complemento']) . "," . utf8_encode($RFP['bairro']);
            $endereco2 = utf8_encode($RFP['cidade_nome']) . "-" . utf8_encode($RFP['estado_sigla']) . ", CEP:" . utf8_encode($RFP['cep']);
            // INFORMACOES PARA O CLIENTE
            $msg1 = ($RFP['multa'] > 0 ? "MULTA DE " . escreverNumero((($valor_cobrado * $RFP['multa']) / 100), 1) . " APOS : " . date_format(date_create($RFP['data_parcela']), 'd/m/Y') : "");
            $msg2 = ($RFP['juros'] > 0 ? "JUROS DE " . escreverNumero((($valor_cobrado * $RFP['juros']) / 100), 1) . " AO DIA" : "");
            $tipo_bol = $RFP['tipo_bol'];
            $tipo_bol2 = $RFP['tipo_bol2'];
            if ($RFP['mensagem4'] != '') {
                $demonstrativo1 = $msg1 . "<br/>" . utf8_encode($RFP['mensagem4']);
            } else {
                $demonstrativo1 = $msg1;
            }
            if ($RFP['mensagem5'] != '') {
                $demonstrativo2 = $msg2 . "<br/>" . utf8_encode($RFP['mensagem5']);
            } else {
                $demonstrativo2 = $msg2;
            }
            if ($RFP['mensagem6'] != '') {
                $demonstrativo3 = $msg3 . "<br/>" . utf8_encode($RFP['mensagem6']);
            } else {
                $demonstrativo3 = $msg3;
            }
            // DADOS BANCO
            $agencia = utf8_encode($RFP['agencia']);
            $item2 = explode("-", $RFP['conta']);
            if (count($item2) == 2) {
                $conta = $item2[0];
                $dv = $item2[1];
            }
            $cli_conta = utf8_encode($RFP['cedente']);            // ContaCedente do Cliente, sem digito (Somente Números)
            $cli_dv = utf8_encode($RFP['dv_cedente']);            // Digito da ContaCedente do Cliente
            $cli_carteira = utf8_encode($RFP['carteira']);        // Código da Carteira: pode ser SR (Sem Registro) ou CR (Com Registro) - (Confirmar com gerente qual usar)        
            if ($RFP['sf_carteira_registrada'] == "1") {
                $registro = 1;
            } else {
                $registro = 2;
            }
            $nosso_numero = utf8_encode($RFP['bol_nosso_numero']); // Nosso numero sem o DV - REGRA: Máximo de 16 caracteres! (Pode ser um n�mero sequencial do sistema, o cpf ou o cnpj)
            $especie_doc = utf8_encode($RFP['especie_doc']);      //DM
            //Dados da Conta
            $data_venc = date_format(date_create($RFP['data_parcela']), 'd/m/Y');
            $valor_boleto = escreverNumero($valor_cobrado + $taxa_boleto);
            $numero_documento = $RFP['sa_descricao']; // Num do pedido ou do documento 
            $data_documento = date_format(date_create($RFP['data_cadastro']), 'd/m/Y');
            $data_processamento = date("d/m/Y"); //date_format(date_create($RFP['bol_data_criacao']), 'd/m/Y');                        
            if ($RFP['mensagem1'] != '') {
                $instrucoes1 = $msg1 . "<br/>" . dynamicBoleto($RFP['mensagem1'], $RFP['dt_inicio_mens'], $RFP['dt_fim_mens']);
            } else {
                $instrucoes1 = $msg1;
            }
            if ($RFP['mensagem2'] != '') {
                $instrucoes2 = $msg2 . "<br/>" . dynamicBoleto($RFP['mensagem2'], $RFP['dt_inicio_mens'], $RFP['dt_fim_mens']);
            } else {
                $instrucoes2 = $msg2;
            }
            if ($RFP['mensagem3'] != '') {
                $instrucoes3 = $msg3 . "<br/>" . dynamicBoleto($RFP['mensagem3'], $RFP['dt_inicio_mens'], $RFP['dt_fim_mens']);
            } else {
                $instrucoes3 = $msg3;
            }
            $desconto = $RFP['bol_desconto'];
            $msg_banco = (strlen($RFP['msg_email']) > 0 ? " <br>" . $RFP['msg_email'] : "");
            $bol_descricao = $RFP['bol_descricao'];
        }
        $cur = odbc_exec($con, "select f.razao_social razao_social,endereco,numero,complemento,bairro,cidade_nome,estado_sigla,cep,isnull(sf_carteira,b.carteira) carteira,sf_carteira_registrada,
            historico_baixa,'VENDA' descricao,pa,valor_parcela,agencia,conta,cedente,dv_cedente,especie_doc,mensagem1,mensagem2,mensagem3,mensagem4,mensagem5,mensagem6,b.multa,b.juros,
            bol_nosso_numero,isnull(bol_data_parcela,data_parcela) data_parcela,data_cadastro,id_parcela,isnull(sa_descricao,'') sa_descricao, sp.data_pagamento,b.tipo_bol,b.tipo_bol2,f.cnpj fcnpj,bol_data_criacao,f.id_fornecedores_despesas,isnull(bol_valor,0) bol_valor,bol_desconto,msg_email, bol_descricao
            from sf_venda_parcelas sp 
            inner join sf_vendas s on s.id_venda = sp.venda
            inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = s.cliente_venda
            inner join sf_bancos b on b.id_bancos = sp.bol_id_banco                            
            left join tb_cidades cid on cid.cidade_codigo = f.cidade
            left join tb_estados est on est.estado_codigo = f.estado
            left join sf_bancos_carteiras bc on bc.sf_carteiras_id = sp.carteira_id
            where sp.inativo = 0 and s.status = 'Aprovado' and sp.id_parcela in (" . $WhereV . ")") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            if (is_numeric($RFP['bol_valor'])) {
                $valor_cobrado = $RFP['bol_valor'];
            } else {
                $valor_cobrado = $RFP['valor_parcela'];
            }
            // DADOS DO SEU CLIENTE
            $totCli++;
            $cliente = $RFP['id_fornecedores_despesas'];
            $pagoEm = utf8_encode($RFP['data_pagamento']);
            $sacado = utf8_encode($RFP['razao_social']);
            $sacadocnpj = utf8_encode($RFP['fcnpj']);
            $endereco1 = utf8_encode($RFP['endereco']) . " n° " . utf8_encode($RFP['numero']) . " " . utf8_encode($RFP['complemento']) . "," . utf8_encode($RFP['bairro']);
            $endereco2 = utf8_encode($RFP['cidade_nome']) . "-" . utf8_encode($RFP['estado_sigla']) . ", CEP:" . utf8_encode($RFP['cep']);
            // INFORMACOES PARA O CLIENTE
            $msg1 = ($RFP['multa'] > 0 ? "MULTA DE " . escreverNumero((($valor_cobrado * $RFP['multa']) / 100), 1) . " APOS : " . date_format(date_create($RFP['data_parcela']), 'd/m/Y') : "");
            $msg2 = ($RFP['juros'] > 0 ? "JUROS DE " . escreverNumero((($valor_cobrado * $RFP['juros']) / 100), 1) . " AO DIA" : "");
            $tipo_bol = $RFP['tipo_bol'];
            $tipo_bol2 = $RFP['tipo_bol2'];
            if ($RFP['mensagem4'] != '') {
                $demonstrativo1 = $msg1 . "<br/>" . utf8_encode($RFP['mensagem4']);
            } else {
                $demonstrativo1 = $msg1;
            }
            if ($RFP['mensagem5'] != '') {
                $demonstrativo2 = $msg2 . "<br/>" . utf8_encode($RFP['mensagem5']);
            } else {
                $demonstrativo2 = $msg2;
            }
            if ($RFP['mensagem6'] != '') {
                $demonstrativo3 = $msg3 . "<br/>" . utf8_encode($RFP['mensagem6']);
            } else {
                $demonstrativo3 = $msg3;
            }
            // DADOS BANCO
            $agencia = utf8_encode($RFP['agencia']);
            $item2 = explode("-", $RFP['conta']);
            if (count($item2) == 2) {
                $conta = $item2[0];
                $dv = $item2[1];
            }
            $cli_conta = utf8_encode($RFP['cedente']);            // ContaCedente do Cliente, sem digito (Somente Números)
            $cli_dv = utf8_encode($RFP['dv_cedente']);            // Digito da ContaCedente do Cliente
            $cli_carteira = utf8_encode($RFP['carteira']);        // Código da Carteira: pode ser SR (Sem Registro) ou CR (Com Registro) - (Confirmar com gerente qual usar)        
            if ($RFP['sf_carteira_registrada'] == "1") {
                $registro = 1;
            } else {
                $registro = 2;
            }
            $nosso_numero = utf8_encode($RFP['bol_nosso_numero']); // Nosso numero sem o DV - REGRA: Máximo de 16 caracteres! (Pode ser um n�mero sequencial do sistema, o cpf ou o cnpj)
            $especie_doc = utf8_encode($RFP['especie_doc']);      //DM
            //Dados da Conta
            $data_venc = date_format(date_create($RFP['data_parcela']), 'd/m/Y');
            $valor_boleto = escreverNumero($valor_cobrado + $taxa_boleto);
            $numero_documento = $RFP['sa_descricao']; // Num do pedido ou do documento 
            $data_documento = date_format(date_create($RFP['data_cadastro']), 'd/m/Y');
            $data_processamento = date("d/m/Y"); //date_format(date_create($RFP['bol_data_criacao']), 'd/m/Y');                        
            if ($RFP['mensagem1'] != '') {
                $instrucoes1 = $msg1 . "<br/>" . utf8_encode($RFP['mensagem1']);
            } else {
                $instrucoes1 = $msg1;
            }
            if ($RFP['mensagem2'] != '') {
                $instrucoes2 = $msg2 . "<br/>" . utf8_encode($RFP['mensagem2']);
            } else {
                $instrucoes2 = $msg2;
            }
            if ($RFP['mensagem3'] != '') {
                $instrucoes3 = $msg3 . "<br/>" . utf8_encode($RFP['mensagem3']);
            } else {
                $instrucoes3 = $msg3;
            }
            $desconto = $RFP['bol_desconto'];
            $msg_banco = (strlen($RFP['msg_email']) > 0 ? " <br>" . $RFP['msg_email'] : "");
            $bol_descricao = $RFP['bol_descricao'];
        }
        $cur = odbc_exec($con, "select f.razao_social razao_social,endereco,numero,complemento,bairro,cidade_nome,estado_sigla,cep,isnull(sf_carteira,b.carteira) carteira,sf_carteira_registrada,
            historico_baixa,c.descricao descricao,pa,valor_parcela,agencia,conta,cedente,dv_cedente,especie_doc,mensagem1,mensagem2,mensagem3,mensagem4,mensagem5,mensagem6,b.multa,b.juros,
            bol_nosso_numero,isnull(bol_data_parcela,data_parcela) data_parcela,data_cadastro,id_parcela,isnull(sa_descricao,'') sa_descricao, sp.data_pagamento,b.tipo_bol,b.tipo_bol2,f.cnpj fcnpj,isnull(bol_valor,0) bol_valor,f.id_fornecedores_despesas,bol_desconto, msg_email, bol_descricao 
            from sf_solicitacao_autorizacao_parcelas sp 
            inner join sf_solicitacao_autorizacao s on s.id_solicitacao_autorizacao = sp.solicitacao_autorizacao
            inner join sf_contas_movimento c on c.id_contas_movimento = s.conta_movimento 
            inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = s.fornecedor_despesa 
            inner join sf_bancos b on b.id_bancos = sp.bol_id_banco                            
            left join tb_cidades cid on cid.cidade_codigo = f.cidade
            left join tb_estados est on est.estado_codigo = f.estado   
            left join sf_bancos_carteiras bc on bc.sf_carteiras_id = sp.carteira_id
            where sp.inativo = 0 and s.status = 'Aprovado' and sp.id_parcela in (" . $WhereA . ")") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            if (is_numeric($RFP['bol_valor'])) {
                $valor_cobrado = $RFP['bol_valor'];
            } else {
                $valor_cobrado = $RFP['valor_parcela'];
            }
            // DADOS DO SEU CLIENTE
            $pagoEm = utf8_encode($RFP['data_pagamento']);
            $sacado = utf8_encode($RFP['razao_social']);
            $sacadocnpj = utf8_encode($RFP['fcnpj']);
            $endereco1 = utf8_encode($RFP['endereco']) . " n° " . utf8_encode($RFP['numero']) . " " . utf8_encode($RFP['complemento']) . "," . utf8_encode($RFP['bairro']);
            $endereco2 = utf8_encode($RFP['cidade_nome']) . "-" . utf8_encode($RFP['estado_sigla']) . ", CEP:" . utf8_encode($RFP['cep']);
            // INFORMACOES PARA O CLIENTE
            $totCli++;
            $cliente = $RFP['id_fornecedores_despesas'];
            $msg1 = ($RFP['multa'] > 0 ? "MULTA DE " . escreverNumero((($valor_cobrado * $RFP['multa']) / 100), 1) . " APOS : " . date_format(date_create($RFP['data_parcela']), 'd/m/Y') : "");
            $msg2 = ($RFP['juros'] > 0 ? "JUROS DE " . escreverNumero((($valor_cobrado * $RFP['juros']) / 100), 1) . " AO DIA" : "");
            $tipo_bol = $RFP['tipo_bol'];
            $tipo_bol2 = $RFP['tipo_bol2'];
            if ($RFP['mensagem4'] != '') {
                $demonstrativo1 = $msg1 . "<br/>" . utf8_encode($RFP['mensagem4']);
            } else {
                $demonstrativo1 = $msg1;
            }
            if ($RFP['mensagem5'] != '') {
                $demonstrativo2 = $msg2 . "<br/>" . utf8_encode($RFP['mensagem5']);
            } else {
                $demonstrativo2 = $msg2;
            }
            if ($RFP['mensagem6'] != '') {
                $demonstrativo3 = $msg3 . "<br/>" . utf8_encode($RFP['mensagem6']);
            } else {
                $demonstrativo3 = $msg3;
            }
            // DADOS BANCO
            $agencia = utf8_encode($RFP['agencia']);
            $item2 = explode("-", $RFP['conta']);
            if (count($item2) == 2) {
                $conta = $item2[0];
                $dv = $item2[1];
            }
            $cli_conta = utf8_encode($RFP['cedente']);            // ContaCedente do Cliente, sem digito (Somente Números)
            $cli_dv = utf8_encode($RFP['dv_cedente']);            // Digito da ContaCedente do Cliente
            $cli_carteira = utf8_encode($RFP['carteira']);        // Código da Carteira: pode ser SR (Sem Registro) ou CR (Com Registro) - (Confirmar com gerente qual usar)
            if ($RFP['sf_carteira_registrada'] == "1") {
                $registro = 1;
            } else {
                $registro = 2;
            }
            $nosso_numero = utf8_encode($RFP['bol_nosso_numero']); // Nosso numero sem o DV - REGRA: Máximo de 16 caracteres! (Pode ser um n�mero sequencial do sistema, o cpf ou o cnpj)
            $especie_doc = utf8_encode($RFP['especie_doc']);      //DM
            //Dados da Conta
            $data_venc = date_format(date_create($RFP['data_parcela']), 'd/m/Y');
            $valor_boleto = escreverNumero($valor_cobrado + $taxa_boleto);
            $numero_documento = $RFP['sa_descricao']; // Num do pedido ou do documento 
            $data_documento = date_format(date_create($RFP['data_cadastro']), 'd/m/Y');
            $data_processamento = date("d/m/Y"); //date_format(date_create($RFP['bol_data_criacao']), 'd/m/Y');                        
            if ($RFP['mensagem1'] != '') {
                $instrucoes1 = $msg1 . "<br/>" . utf8_encode($RFP['mensagem1']);
            } else {
                $instrucoes1 = $msg1;
            }
            if ($RFP['mensagem2'] != '') {
                $instrucoes2 = $msg2 . "<br/>" . utf8_encode($RFP['mensagem2']);
            } else {
                $instrucoes2 = $msg2;
            }
            if ($RFP['mensagem3'] != '') {
                $instrucoes3 = $msg3 . "<br/>" . utf8_encode($RFP['mensagem3']);
            } else {
                $instrucoes3 = $msg3;
            }
            $desconto = $RFP['bol_desconto'];
            $msg_banco = (strlen($RFP['msg_email']) > 0 ? " <br>" . $RFP['msg_email'] : "");
            $bol_descricao = $RFP['bol_descricao'];
        }
        $cur = odbc_exec($con, "select f.razao_social razao_social,endereco,numero,complemento,bairro,cidade_nome,estado_sigla,cep,isnull(sf_carteira,b.carteira) carteira,sf_carteira_registrada,especie_doc,mensagem1,mensagem2,mensagem3,mensagem4,mensagem5,mensagem6,b.multa,b.juros,
            historico_baixa,data_cadastro,c.descricao descricao,pa,valor_parcela,agencia,conta,cedente,dv_cedente,bol_nosso_numero,isnull(bol_data_parcela,data_vencimento) data_vencimento,id_parcela,isnull(sa_descricao,'') sa_descricao,lp.data_pagamento,b.tipo_bol,b.tipo_bol2,f.cnpj fcnpj,f.id_fornecedores_despesas,isnull(bol_valor,0) bol_valor,bol_desconto, msg_email, bol_descricao 
            from sf_lancamento_movimento_parcelas lp inner join sf_lancamento_movimento l on l.id_lancamento_movimento = lp.id_lancamento_movimento
            inner join sf_contas_movimento c on c.id_contas_movimento = l.conta_movimento 
            inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = l.fornecedor_despesa                            
            inner join sf_bancos b on b.id_bancos = lp.bol_id_banco                            
            left join tb_cidades cid on cid.cidade_codigo = f.cidade
            left join tb_estados est on est.estado_codigo = f.estado    
            left join sf_bancos_carteiras bc on bc.sf_carteiras_id = lp.carteira_id
            where lp.inativo = 0 and l.status = 'Aprovado' and lp.id_parcela in (" . $WhereD . ")") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            if (is_numeric($RFP['bol_valor'])) {
                $valor_cobrado = $RFP['bol_valor'];
            } else {
                $valor_cobrado = $RFP['valor_parcela'];
            }
            // DADOS DO SEU CLIENTE
            $pagoEm = utf8_encode($RFP['data_pagamento']);
            $sacado = utf8_encode($RFP['razao_social']);
            $sacadocnpj = utf8_encode($RFP['fcnpj']);
            $endereco1 = utf8_encode($RFP['endereco']) . " n° " . utf8_encode($RFP['numero']) . " " . utf8_encode($RFP['complemento']) . "," . utf8_encode($RFP['bairro']);
            $endereco2 = utf8_encode($RFP['cidade_nome']) . "-" . utf8_encode($RFP['estado_sigla']) . ", CEP: " . utf8_encode($RFP['cep']);
            // INFORMACOES PARA O CLIENTE   
            $multa = $RFP['multa'];
            $juros = $RFP['juros'];
            $totCli++;
            $cliente = $RFP['id_fornecedores_despesas'];
            $msg1 = ($RFP['multa'] > 0 ? "MULTA DE " . escreverNumero((($valor_cobrado * $RFP['multa']) / 100), 1) . " APOS : " . date_format(date_create($RFP['data_vencimento']), 'd/m/Y') : "");
            $msg2 = ($RFP['juros'] > 0 ? "JUROS DE " . escreverNumero((($valor_cobrado * $RFP['juros']) / 100), 1) . " AO DIA" : "");
            $tipo_bol = $RFP['tipo_bol'];
            $tipo_bol2 = $RFP['tipo_bol2'];
            if ($RFP['mensagem4'] != '') {
                $demonstrativo1 = $msg1 . "<br/>" . utf8_encode($RFP['mensagem4']);
            } else {
                $demonstrativo1 = $msg1;
            }
            if ($RFP['mensagem5'] != '') {
                $demonstrativo2 = $msg2 . "<br/>" . utf8_encode($RFP['mensagem5']);
            } else {
                $demonstrativo2 = $msg2;
            }
            if ($RFP['mensagem6'] != '') {
                $demonstrativo3 = $msg3 . "<br/>" . utf8_encode($RFP['mensagem6']);
            } else {
                $demonstrativo3 = $msg3;
            }
            // DADOS BANCO
            $agencia = utf8_encode($RFP['agencia']);
            $item2 = explode("-", $RFP['conta']);
            if (count($item2) == 2) {
                $conta = $item2[0];
                $dv = $item2[1];
            }
            $cli_conta = utf8_encode($RFP['cedente']);            // ContaCedente do Cliente, sem digito (Somente Números)
            $cli_dv = utf8_encode($RFP['dv_cedente']);            // Digito da ContaCedente do Cliente
            $cli_carteira = utf8_encode($RFP['carteira']);        // Código da Carteira: pode ser SR (Sem Registro) ou CR (Com Registro) - (Confirmar com gerente qual usar)
            if ($RFP['sf_carteira_registrada'] == "1") {
                $registro = 1;
            } else {
                $registro = 2;
            }
            $nosso_numero = utf8_encode($RFP['bol_nosso_numero']); // Nosso numero sem o DV - REGRA: Máximo de 16 caracteres! (Pode ser um n�mero sequencial do sistema, o cpf ou o cnpj)
            $especie_doc = utf8_encode($RFP['especie_doc']);      //DM
            //Dados da Conta
            $data_venc = date_format(date_create($RFP['data_vencimento']), 'd/m/Y');
            $valor_boleto = escreverNumero($valor_cobrado + $taxa_boleto);
            $numero_documento = $RFP['sa_descricao'];
            $data_documento = date_format(date_create($RFP['data_cadastro']), 'd/m/Y');
            $data_processamento = date("d/m/Y"); //date_format(date_create($RFP['bol_data_criacao']), 'd/m/Y'); 
            if ($RFP['mensagem1'] != '') {
                $instrucoes1 = $msg1 . "<br/>" . utf8_encode($RFP['mensagem1']);
            } else {
                $instrucoes1 = $msg1;
            }
            if ($RFP['mensagem2'] != '') {
                $instrucoes2 = $msg2 . "<br/>" . utf8_encode($RFP['mensagem2']);
            } else {
                $instrucoes2 = $msg2;
            }
            if ($RFP['mensagem3'] != '') {
                $instrucoes3 = $msg3 . "<br/>" . utf8_encode($RFP['mensagem3']);
            } else {
                $instrucoes3 = $msg3;
            }
            $desconto = $RFP['bol_desconto'];
            $msg_banco = (strlen($RFP['msg_email']) > 0 ? " <br>" . $RFP['msg_email'] : "");
            $bol_descricao = $RFP['bol_descricao'];
        }
        //---------------------------------------------------------------------------------------
        if ($especie == 'D' && strlen($nosso_numero) > 0) {
            $cur = odbc_exec($con, "select  isnull(sp.bol_valor,0) valor,mensagem1,mensagem2,valor_parcela,data_parcela 
                from sf_venda_parcelas sp inner join sf_vendas s on s.id_venda = sp.venda
                inner join sf_bancos b on b.id_bancos = sp.bol_id_banco
                where sp.inativo = 0 and s.status = 'Aprovado' and s.cliente_venda = " . $cliente . " and bol_nosso_numero = " . $nosso_numero) or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                if (is_numeric($RFP['bol_valor'])) {
                    $valor_cobradoP = $RFP['bol_valor'];
                } else {
                    $valor_cobradoP = $RFP['valor_parcela'];
                }
                $valor_cobrado = $valor_cobrado + $valor_cobradoP;
                $msg1 = ($multa > 0 ? "MULTA DE " . escreverNumero((($valor_cobrado * $multa) / 100), 1) . " APOS : " . date_format(date_create($RFP['data_parcela']), 'd/m/Y') : "");
                $msg2 = ($juros > 0 ? "JUROS DE " . escreverNumero((($valor_cobrado * $juros) / 100), 1) . " AO DIA" : "");
                if ($RFP['mensagem1'] != '') {
                    $instrucoes1 = $msg1 . "<br/>" . utf8_encode($RFP['mensagem1']);
                } else {
                    $instrucoes1 = $msg1;
                }
                if ($RFP['mensagem2'] != '') {
                    $instrucoes2 = $msg2 . "<br/>" . utf8_encode($RFP['mensagem2']);
                } else {
                    $instrucoes2 = $msg2;
                }
            }
            $valor_boleto = escreverNumero($valor_cobrado + $taxa_boleto);
        }
        if ($especie == 'V' && strlen($nosso_numero) > 0) {
            $valor_cobrado = 0;
            $cur = odbc_exec($con, "select sum(sp.bol_valor) valor_parcela, max(b.multa) multa, max(b.juros) juros, max(mensagem1) mensagem1, max(mensagem2) mensagem2,
                sum(isnull(bol_valor,0)) bol_valor, max(data_parcela) data_parcela from sf_venda_parcelas sp inner join sf_vendas s on s.id_venda = sp.venda
                inner join sf_bancos b on b.id_bancos = sp.bol_id_banco where bol_nosso_numero = " . $nosso_numero) or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                if (is_numeric($RFP['bol_valor'])) {
                    $valor_cobradoP = $RFP['bol_valor'];
                } else {
                    $valor_cobradoP = $RFP['valor_parcela'];
                }
                $valor_cobrado = $valor_cobrado + $valor_cobradoP;
                $msg1 = ($RFP['multa'] > 0 ? "MULTA DE " . escreverNumero(utf8_encode(($valor_cobrado * $RFP['multa']) / 100), 1) . " APOS : " . date_format(date_create($RFP['data_parcela']), 'd/m/Y') : "");
                $msg2 = ($RFP['juros'] > 0 ? "JUROS DE " . escreverNumero(utf8_encode(($valor_cobrado * $RFP['juros']) / 100), 1) . " AO DIA" : "");
                if ($RFP['mensagem1'] != '') {
                    $instrucoes1 = $msg1 . "<br/>" . utf8_encode($RFP['mensagem1']);
                } else {
                    $instrucoes1 = $msg1;
                }
                if ($RFP['mensagem2'] != '') {
                    $instrucoes2 = $msg2 . "<br/>" . utf8_encode($RFP['mensagem2']);
                } else {
                    $instrucoes2 = $msg2;
                }
                $valor_boleto = escreverNumero($valor_cobrado + $taxa_boleto);
            }
        }
        if ($especie == 'M' && strlen($nosso_numero) > 0) {
            $valor_cobrado = 0;
            $cur = odbc_exec($con, "select sum(valor_parcela) valor_parcela, max(multa) multa, max(juros) juros, max(mensagem1) mensagem1, max(mensagem2) mensagem2,
            sum(isnull(bol_valor,0)) bol_valor, max(bol_data_parcela) data_parcela, max(dt_inicio_mens) dt_inicio_mens, max(dt_fim_mens) dt_fim_mens, max(informativo) informativo from (
            select sp.bol_valor valor_parcela, b.multa, b.juros, mensagem1, mensagem2, bol_valor, isnull(bol_data_parcela,dt_inicio_mens) bol_data_parcela, dt_inicio_mens, dt_fim_mens,
            STUFF((SELECT distinct ',' + descricao + case when placa is not null then ' [' + placa + ']' else '' end 
            FROM sf_vendas_planos vp left join sf_fornecedores_despesas_veiculo v on v.id = vp.id_veiculo 
            left join sf_produtos p on p.conta_produto = vp.id_prod_plano
            WHERE vp.id_plano = sf_vendas_planos_mensalidade.id_plano_mens FOR XML PATH('')), 1, 1, '') informativo                               
            from sf_boleto sp inner join sf_vendas_planos_mensalidade on sp.id_referencia = sf_vendas_planos_mensalidade.id_mens and tp_referencia = 'M' 
            inner join sf_bancos b on b.id_bancos = sp.bol_id_banco where sp.inativo = 0 and sp.bol_nosso_numero = '" . $nosso_numero . "') as x ") or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                if (is_numeric($RFP['bol_valor'])) {
                    $valor_cobradoP = $RFP['bol_valor'];
                } else {
                    $valor_cobradoP = $RFP['valor_parcela'];
                }
                $valor_cobrado = $valor_cobrado + $valor_cobradoP;
                $msg1 = ($RFP['multa'] > 0 ? "MULTA DE " . escreverNumero(utf8_encode(($valor_cobrado * $RFP['multa']) / 100), 1) . " APOS : " . date_format(date_create($RFP['data_parcela']), 'd/m/Y') : "");
                $msg2 = ($RFP['juros'] > 0 ? "JUROS DE " . escreverNumero(utf8_encode(($valor_cobrado * $RFP['juros']) / 100), 1) . " AO DIA" : "");
                if ($RFP['mensagem1'] != '') {
                    $instrucoes1 = $msg1 . "<br/>" . dynamicBoleto($RFP['mensagem1'], $RFP['dt_inicio_mens'], $RFP['dt_fim_mens']);
                } else {
                    $instrucoes1 = $msg1;
                }
                if ($RFP['mensagem2'] != '') {
                    $instrucoes2 = $msg2 . "<br/>" . dynamicBoleto($RFP['mensagem2'], $RFP['dt_inicio_mens'], $RFP['dt_fim_mens']);
                } else {
                    $instrucoes2 = $msg2;
                }
                if ($RFP['informativo'] != '') {
                    $instrucoes3 = utf8_encode($RFP['informativo']);
                }
                $valor_boleto = escreverNumero($valor_cobrado + $taxa_boleto);
            }
        }
        if ($especie == 'Y' && strlen($nosso_numero) > 0) {
            $cur = odbc_exec($con, "select sum(lp.valor_parcela) valor_parcela, max(b.multa) multa, max(b.juros) juros, max(mensagem1) mensagem1, max(mensagem2) mensagem2,sum(isnull(bol_valor,0)) bol_valor, max(data_vencimento) data_vencimento 
                from sf_lancamento_movimento_parcelas lp inner join sf_bancos b on b.id_bancos = lp.bol_id_banco 
                where lp.bol_nosso_numero = " . $nosso_numero) or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                if (is_numeric($RFP['bol_valor'])) {
                    $valor_cobrado = $RFP['bol_valor'];
                } else {
                    $valor_cobrado = $RFP['valor_parcela'];
                }
                $msg1 = ($RFP['multa'] > 0 ? "MULTA DE " . escreverNumero(utf8_encode(($valor_cobrado * $RFP['multa']) / 100), 1) . " APOS : " . date_format(date_create($RFP['data_vencimento']), 'd/m/Y') : "");
                $msg2 = ($RFP['juros'] > 0 ? "JUROS DE " . escreverNumero(utf8_encode(($valor_cobrado * $RFP['juros']) / 100), 1) . " AO DIA" : "");
                if ($RFP['mensagem1'] != '') {
                    $instrucoes1 = $msg1 . "<br/>" . utf8_encode($RFP['mensagem1']);
                } else {
                    $instrucoes1 = $msg1;
                }
                if ($RFP['mensagem2'] != '') {
                    $instrucoes2 = $msg2 . "<br/>" . utf8_encode($RFP['mensagem2']);
                } else {
                    $instrucoes2 = $msg2;
                }
            }
            $cur2 = odbc_exec($con, "select  isnull(sp.bol_valor,0) valor from sf_venda_parcelas sp inner join sf_vendas s on s.id_venda = sp.venda
                inner join sf_bancos b on b.id_bancos = sp.bol_id_banco
                where sp.inativo = 0 and s.status = 'Aprovado' and s.cliente_venda = " . $cliente . " and bol_nosso_numero = " . $nosso_numero) or die(odbc_errormsg());
            while ($RFP2 = odbc_fetch_array($cur2)) {
                $valor_cobrado = $valor_cobrado + $RFP2['valor'];
            }
            $valor_boleto = escreverNumero($valor_cobrado + $taxa_boleto);
        }
        $especie = "R$";
        $quantidade = "";
        $valor_unitario = "";
        $aceite = "NÃO";

        $cur = odbc_exec($con, "select top 1 f.id_fornecedores_despesas,f.cnpj fcnpj,f.razao_social razao_social,endereco,numero,
        complemento,bairro,cidade_nome,estado_sigla,cep from sf_fornecedores_despesas f 
        left join tb_cidades cid on cid.cidade_codigo = f.cidade
        left join tb_estados est on est.estado_codigo = f.estado 
        inner join sf_fornecedores_despesas_dependentes fd on fd.id_titular = f.id_fornecedores_despesas
        where inativo = 0 and tipo = 'C' and fd.id_dependente = " . $cliente) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $cliente = $RFP['id_fornecedores_despesas'];
            $sacadocnpj = utf8_encode($RFP['fcnpj']);
            $sacado = utf8_encode($RFP['razao_social']);
            $endereco1 = utf8_encode($RFP['endereco']) . " n° " . utf8_encode($RFP['numero']) . " " . utf8_encode($RFP['complemento']) . "," . utf8_encode($RFP['bairro']);
            $endereco2 = utf8_encode($RFP['cidade_nome']) . "-" . utf8_encode($RFP['estado_sigla']) . ", CEP: " . utf8_encode($RFP['cep']);
        }
        $cur = odbc_exec($con, "select top 1 f.id_fornecedores_despesas,f.cnpj fcnpj,f.razao_social razao_social,endereco,numero,
        complemento,bairro,cidade_nome,estado_sigla,cep from sf_fornecedores_despesas_responsavel fr
        inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = fr.id_responsavel
        left join tb_cidades cid on cid.cidade_codigo = f.cidade
        left join tb_estados est on est.estado_codigo = f.estado
        where inativo = 0 and fr.id_dependente = " . $cliente) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $cliente = $RFP['id_fornecedores_despesas'];
            $sacadocnpj = utf8_encode($RFP['fcnpj']);
            $sacado = utf8_encode($RFP['razao_social']);
            $endereco1 = utf8_encode($RFP['endereco']) . " n° " . utf8_encode($RFP['numero']) . " " . utf8_encode($RFP['complemento']) . "," . utf8_encode($RFP['bairro']);
            $endereco2 = utf8_encode($RFP['cidade_nome']) . "-" . utf8_encode($RFP['estado_sigla']) . ", CEP: " . utf8_encode($RFP['cep']);
        }
        if (strpos($bol_descricao, "mundipagg.com/boleto") !== false || strpos($bol_descricao, "pagar.me") !== false || 
            strpos($bol_descricao, "boleto_pix.php") !== false || strpos($bol_descricao, "galaxpay.com.br") !== false || 
            strpos($bol_descricao, "cel.cash") !== false) {
            header('Location: ' . $bol_descricao);
        } else if ($nosso_numero != '' && $numero_documento != '' && $pagoEm == "") {
            if (count($count) > 2 && is_numeric($tipo_bol2)) {
                $tipo_bol = $tipo_bol2;
            }
            if ($tipo_bol == 0 || $tipo_bol == 8) {
                include __DIR__ . "./../boleto_cef_sigcb.php";
            } elseif ($tipo_bol == 1 || $tipo_bol == 2 || $tipo_bol == 3) {
                include __DIR__ . "./../boleto_itau.php";
            } elseif ($tipo_bol == 4 || $tipo_bol == 9) {
                include __DIR__ . "./../boleto_bradesco.php";
            } elseif ($tipo_bol == 5 || $tipo_bol == 10) {
                include __DIR__ . "./../boleto_santander_banespa.php";
            } elseif ($tipo_bol == 6 || $tipo_bol == 7 || $tipo_bol == 11) {
                include __DIR__ . "./../boleto_bb.php";
            } elseif ($tipo_bol == 12 || $tipo_bol == 13) {
                include __DIR__ . "./../boleto_sicoob.php";
            } elseif ($tipo_bol == 14 || $tipo_bol == 15) {
                include __DIR__ . "./../boleto_sicredi.php";
            }
        } else if (!isset($dicionario)) {
            echo utf8_decode("Boleto com baixa!");
        }
    }
}