<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>
<HTML>
    <HEAD>
        <TITLE>SIVIS</TITLE>
        <META http-equiv="Content-Type" content="text/html" charset="utf-8">
        <meta name="Generator" content="" />
        <style type="text/css">
            <!--.cp {  font: bold 10px Arial; color: black}
            <!--.ti {  font: 9px Arial, Helvetica, sans-serif}
            <!--.ld { font: bold 15px Arial; color: #000000}
            <!--.ct { FONT: 9px "Arial Narrow"; COLOR: #000033} 
            <!--.cn { FONT: 9px Arial; COLOR: black }
            <!--.bc { font: bold 20px Arial; color: #000000 }
            <!--.ld2 { font: bold 12px Arial; color: #000000 }
            --></style> 
    </head>

    <BODY text=#000000 bgColor=#ffffff topMargin=0 rightMargin=0>
        <table width=666 cellspacing=0 cellpadding=0 border=0><tr><td valign=top class=cp><DIV ALIGN="CENTER">Instruções 
                        de Impressão</DIV></TD></TR><TR><TD valign=top class=cp><DIV ALIGN="left">
                        <p>
                        <li>Imprima em impressora jato de tinta (ink jet) ou laser em qualidade normal ou alta (Não use modo econômico).<br>
                        <li>Utilize folha A4 (210 x 297 mm) ou Carta (216 x 279 mm) e margens mínimas à esquerda e à direita do formulário.<br>
                        <li>Corte na linha indicada. Não rasure, risque, fure ou dobre a região onde se encontra o código de barras.<br>
                        <li>Caso não apareça o código de barras no final, clique em F5 para atualizar esta tela.
                        <li>Caso tenha problemas ao imprimir, copie a sequencia numérica abaixo e pague no caixa eletrônico ou no internet banking:<br><br>
                            <span class="ld2">
                                &nbsp;&nbsp;&nbsp;&nbsp;Linha Digitável: &nbsp;<?php echo str_replace(".","",str_replace(" ","",$dadosboleto["linha_digitavel"])); ?><br>
                                &nbsp;&nbsp;&nbsp;&nbsp;Valor: &nbsp;&nbsp;R$ <?php echo $dadosboleto["valor_boleto"] ?><br>
                            </span>
                    </DIV></td></tr></table><br><table cellspacing=0 cellpadding=0 width=666 border=0><TBODY><TR><TD class=ct width=666><img height=1 src="imagens/6.png" width=665 border=0></TD></TR><TR><TD class=ct width=666><div align=right><b class=cp>Recibo 
                                do Pagador</b></div></TD></tr></tbody></table><table width=666 cellspacing=5 cellpadding=0 border=0><tr><td width=41></TD></tr></table>
        <BR><table cellspacing=0 cellpadding=0 width=666 border=0><tr><td class=cp width=113> 
                    <span class="campo"><IMG 
                            src="imagens/logocaixa.jpg" height="20" 
                            border=0></span></td>
                <td width=3 valign=bottom><img height=22 src="imagens/3.png" width=2 border=0></td><td class=cpt width=58 valign=bottom><div align=center><font class=bc><?php echo $dadosboleto["codigo_banco_com_dv"] ?></font></div></td><td width=3 valign=bottom><img height=22 src="imagens/3.png" width=2 border=0></td><td class=ld align=right width=453 valign=bottom><span class=ld> 
                        <span class="campotitulo">
                            <?php echo $dadosboleto["linha_digitavel"] ?>
                        </span></span></td>
            </tr><tbody><tr><td colspan=5><img height=2 src="imagens/2.png" width=666 border=0></td></tr></tbody></table>                                
                <table cellspacing=0 cellpadding=0 border=0><tbody>
                    <tr>
                        <td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td>
                        <td class=ct valign=top width=352 height=13>Beneficiário: <span class="cp" style="font-size: 9px;"><?php echo $dadosboleto["cedente"]; ?></span></td>
                        <td class=ct valign=top width=5 height=13><img height=13 src="imagens/1.png" width=1 border=0></td>
                        <td class=ct valign=top width=100 height=13>Agência/Código Beneficiário</td>
                        <td class=ct valign=top width=5 height=13><img height=13 src="imagens/1.png" width=1 border=0></td>
                        <td class=ct valign=top width=34 height=13>Espécie</td>
                        <td class=ct valign=top width=5 height=13><img height=13 src="imagens/1.png" width=1 border=0></td>
                        <td class=ct valign=top width=42 height=13>Quantidade</td>
                        <td class=ct valign=top width=6 height=13><img height=13 src="imagens/1.png" width=1 border=0></td>
                        <td class=ct valign=top width=110 height=13>Nosso número</td>
                    </tr>
                    <tr>
                        <td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td>
                        <td class=ct valign=top width=352 height=12>Endereço: <span class="cp"><?php echo $dadosboleto["endereco"] . " " . $dadosboleto["cidade_uf"]; ?></span></td>
                        <td class=cp valign=top width=5 height=12><img height=12 src="imagens/1.png" width=1 border=0></td>
                        <td class=cp valign=top width=100 height=12><span class="campo"><?php echo $dadosboleto["agencia_codigo"] ?></span></td>
                        <td class=cp valign=top width=5 height=12><img height=12 src="imagens/1.png" width=1 border=0></td>
                        <td class=cp valign=top  width=34 height=12><span class="campo"><?php echo $dadosboleto["especie"] ?></span></td>
                        <td class=cp valign=top width=5 height=12><img height=12 src="imagens/1.png" width=1 border=0></td>
                        <td class=cp valign=top  width=42 height=12><span class="campo"><?php echo $dadosboleto["quantidade"] ?></span></td>
                        <td class=cp valign=top width=6 height=12><img height=12 src="imagens/1.png" width=1 border=0></td>
                        <td class=cp valign=top width=110 align=right height=12> 
                            <span class="campo">
                                <?php 
                                    $antes = substr($dadosboleto["nosso_numero"], 0, strlen($dadosboleto["nosso_numero"]) - 1);
                                    $depois = substr($dadosboleto["nosso_numero"], (strlen($dadosboleto["nosso_numero"])- 1));                            
                                    $nn = $antes . "-" . $depois; 
                                    $antes = substr($nn, 0, 2 );
                                    $depois = substr($nn, (strlen($nn) + 2 - strlen($nn)));                            
                                    $nn = $antes . "/" . $depois; 
                                    echo $nn; ?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td valign=top colspan="10" width=666 height=1><img height=1 src="imagens/2.png" width=666 border=0></td>                        
                    </tr></tbody>
                </table>                                                
                <table cellspacing=0 cellpadding=0 border=0><tbody><tr><td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top colspan=3 height=13>Número
                        do Documento</td><td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=132 height=13>CPF/CNPJ</td><td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=134 height=13>Vencimento</td><td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=180 height=13>Valor 
                        do Documento</td></tr><tr><td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top colspan=3 height=12> 
                        <span class="campo">
                            <?php echo $dadosboleto["numero_documento"] ?>
                        </span></td>
                    <td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top width=132 height=12> 
                        <span class="campo">
                            <?php echo $dadosboleto["cpf_cnpj"] ?>
                        </span></td>
                    <td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top width=134 height=12> 
                        <span class="campo">
                            <?php echo ($data_venc != "") ? $dadosboleto["data_vencimento"] : "Contra Apresenta��o" ?>
                        </span></td>
                    <td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top align=right width=180 height=12> 
                        <span class="campo">
                            <?php echo $dadosboleto["valor_boleto"] ?>
                        </span></td>
                </tr><tr><td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=113 height=1><img height=1 src="imagens/2.png" width=113 border=0></td><td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=72 height=1><img height=1 src="imagens/2.png" width=72 border=0></td><td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=132 height=1><img height=1 src="imagens/2.png" width=132 border=0></td><td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=134 height=1><img height=1 src="imagens/2.png" width=134 border=0></td><td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=180 height=1><img height=1 src="imagens/2.png" width=180 border=0></td></tr></tbody></table>
        <table cellspacing=0 cellpadding=0 border=0><tbody><tr><td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td>
                    <td class=ct valign=top width=113 height=13>(-) Desconto</td>
                    <td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td>
                    <td class=ct valign=top width=122 height=13>(-) Outras Deduções / Abatimentos</td>
                    <td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td>                                        
                    <td class=ct valign=top width=103 height=13>(+) Mora / Multa / Juros</td>
                    <td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td>
                    <td class=ct valign=top width=113 height=13>(+) Outros Acréscimos</td>
                    <td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td>
                    <td class=ct valign=top width=180 height=13>(=) Valor Cobrado</td></tr>
                <tr>
                    <td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td>
                    <td class=cp valign=top align=right width=113 height=12></td>
                    <td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td>
                    <td class=cp valign=top align=right width=122 height=12></td>
                    <td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td>                    
                    <td class=cp valign=top align=right width=103 height=12></td>
                    <td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td>
                    <td class=cp valign=top align=right width=113 height=12></td>
                    <td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td>
                    <td class=cp valign=top align=right width=180 height=12></td>
                </tr>
                <tr><td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=7 border=0></td>
                    <td valign=top width=113 height=1><img height=1 src="imagens/2.png" width=113 border=0></td>
                    <td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=7 border=0></td>
                    <td valign=top width=122 height=1><img height=1 src="imagens/2.png" width=122 border=0></td>
                    <td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=7 border=0></td>                    
                    <td valign=top width=103 height=1><img height=1 src="imagens/2.png" width=103 border=0></td>
                    <td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=7 border=0></td>
                    <td valign=top width=113 height=1><img height=1 src="imagens/2.png" width=113 border=0></td>
                    <td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=7 border=0></td>
                    <td valign=top width=180 height=1><img height=1 src="imagens/2.png" width=180 border=0></td>
                </tr>
            </tbody>
        </table>
        <table cellspacing=0 cellpadding=0 border=0><tbody><tr>
                    <td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td>
                    <td class=ct valign=top width=659 height=13>Pagador</td></tr><tr><td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top width=659 height=12> 
                    <span class="campo">
                        <?php echo $dadosboleto["sacado"] ?>
                    </span>
                    <span style="float: right" class="campo">                    
                        CNPJ/CPF: <?php echo $dadosboleto["sacadocnpj"] ?>
                    </span>                                        
                </td>
                </tr><tr><td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=659 height=1><img height=1 src="imagens/2.png" width=659 border=0></td></tr></tbody></table>
        <table cellspacing=0 cellpadding=0 border=0>
            <tbody>
                <tr><td class=ct  width=7 height=12></td>
                    <td class=ct  width=407>Demonstrativo</td>
                    <td class=ct  width=7 height=12></td>
                    <td class=ct  width=50></td>
                    <td class=ct  width=195>Autenticação mecânica</td>
                </tr>
                <tr>
                    <td class=ct  width=7 height=12></td>
                    <td class=cp width=407>                                
                        <span class="campo">
                            <br>
                            SAC CAIXA: 0800 726 0101 (Informações, reclamações, sugestões e elogios) <br> Para pessoas com deficiência auditiva ou de fala: 0800 726 2492 <br> Ouvidoria: 0800 725 7474 <br> caixa.gov.br <br>                                
                        </span>                        
                    </td>
                    <td class=ct  width=7 height=12></td>
                    <td class=ct  width=50></td>
                    <td class=cp width=195 colspan="2">                                
                        <span class="campo">
                            <?php echo $dadosboleto["demonstrativo1"] ?><br>
                            <?php echo $dadosboleto["demonstrativo2"] ?><br>
                            <?php echo $dadosboleto["demonstrativo3"] ?><br>                            
                        </span>                            
                    </td>                            
                </tr>
            </tbody>
        </table>
        <table cellspacing=0 cellpadding=0 width=666 border=0><tr><td class=ct width=666></td></tr><tbody><tr><td class=ct width=666> 
                        <div align=right>Corte na linha pontilhada</div></td></tr><tr><td class=ct width=666><img height=1 src="imagens/6.png" width=665 border=0></td></tr></tbody></table><br><table cellspacing=0 cellpadding=0 width=666 border=0><tr><td class=cp width=113> 
                    <span class="campo"><IMG 
                            src="imagens/logocaixa.jpg" height="20" 
                            border=0></span></td>
                <td width=3 valign=bottom><img height=22 src="imagens/3.png" width=2 border=0></td><td class=cpt width=58 valign=bottom><div align=center><font class=bc><?php echo $dadosboleto["codigo_banco_com_dv"] ?></font></div></td><td width=3 valign=bottom><img height=22 src="imagens/3.png" width=2 border=0></td><td class=ld align=right width=453 valign=bottom><span class=ld> 
                        <span class="campotitulo">
                            <?php echo $dadosboleto["linha_digitavel"] ?>
                        </span></span></td>
            </tr><tbody><tr><td colspan=5><img height=2 src="imagens/2.png" width=666 border=0></td></tr></tbody></table><table cellspacing=0 cellpadding=0 border=0><tbody><tr><td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=472 height=13>Local 
                        de pagamento</td><td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=180 height=13>Vencimento</td></tr><tr><td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top width=472 height=12>
                PREFERENCIALMENTE NAS CASAS LOTÉRICAS ATÉ O VALOR LIMITE</td><td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top align=right width=180 height=12> 
                        <span class="campo">
                            <?php echo ($data_venc != "") ? $dadosboleto["data_vencimento"] : "Contra Apresenta��o" ?>
                        </span></td>
                </tr><tr><td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=7 border=0></td>
                    <td valign=top width=472 height=1><img height=1 src="imagens/2.png" width=472 border=0></td>
                    <td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=7 border=0></td>
                    <td valign=top width=180 height=1><img height=1 src="imagens/2.png" width=180 border=0></td></tr></tbody></table>
                        <table cellspacing=0 cellpadding=0 border=0><tbody><tr>
                            <td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td>
                            <td class=ct valign=top width=472 height=13>Beneficiário: <span class="cp" style="font-size: 9px;">
                            <?php echo $dadosboleto["cedente"]; ?></span><span class="cp" style="float:right;">CNPJ:<?php echo $dadosboleto["cpf_cnpj"] ?></span> </td>
                            <td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td>
                            <td class=ct valign=top width=180 height=13>Agência/Código Beneficiário</td>
                        </tr><tr>
                            <td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td>
                            <td class=ct valign=top width=472 height=12> 
                                Endereço: <span class="cp"><?php echo $dadosboleto["endereco"] . " " . $dadosboleto["cidade_uf"]; ?></span>
                            </td>
                    <td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td>
                    <td class=cp valign=top align=right width=180 height=12> 
                        <span class="campo">
                            <?php echo $dadosboleto["agencia_codigo"] ?>
                        </span></td>
                </tr><tr><td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=472 height=1><img height=1 src="imagens/2.png" width=472 border=0></td><td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=180 height=1><img height=1 src="imagens/2.png" width=180 border=0></td></tr></tbody></table><table cellspacing=0 cellpadding=0 border=0><tbody><tr><td class=ct valign=top width=7 height=13> 
                        <img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=113 height=13>Data 
                        do documento</td><td class=ct valign=top width=7 height=13> <img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=133 height=13>N<u>o</u>
            documento</td><td class=ct valign=top width=7 height=13> <img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=62 height=13>Espécie
            doc.</td><td class=ct valign=top width=7 height=13> <img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=34 height=13>Aceite</td><td class=ct valign=top width=7 height=13> 
            <img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=102 height=13>Data
            processamento</td><td class=ct valign=top width=7 height=13> <img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=180 height=13>Nosso
            número</td></tr><tr><td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top  width=113 height=12><div align=left> 
                <span class="campo">
                    <?php echo $dadosboleto["data_documento"] ?>
                </span></div></td><td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top width=133 height=12>
            <span class="campo">
                <?php echo $dadosboleto["numero_documento"] ?>
            </span></td>
        <td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top  width=62 height=12><div align=left><span class="campo">
                    <?php echo $dadosboleto["especie_doc"] ?>
                </span> 
            </div></td><td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top  width=34 height=12><div align=left><span class="campo">
                    <?php echo substr($dadosboleto["aceite"], 0, 1); ?>
                </span> 
            </div></td><td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top  width=102 height=12><div align=left>
                <span class="campo">
                    <?php echo $dadosboleto["data_processamento"] ?>
                </span></div></td><td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top align=right width=180 height=12>
            <span class="campo">
                <?php echo $nn; ?>
            </span></td>
    </tr><tr><td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=113 height=1><img height=1 src="imagens/2.png" width=113 border=0></td><td valign=top width=7 height=1> 
            <img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=133 height=1><img height=1 src="imagens/2.png" width=133 border=0></td><td valign=top width=7 height=1>
            <img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=62 height=1><img height=1 src="imagens/2.png" width=62 border=0></td><td valign=top width=7 height=1>
            <img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=34 height=1><img height=1 src="imagens/2.png" width=34 border=0></td><td valign=top width=7 height=1> 
            <img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=102 height=1><img height=1 src="imagens/2.png" width=102 border=0></td><td valign=top width=7 height=1>
            <img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=180 height=1>
            <img height=1 src="imagens/2.png" width=180 border=0></td></tr></tbody></table><table cellspacing=0 cellpadding=0 border=0><tbody><tr>
            <td class=ct valign=top width=7 height=13> <img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top COLSPAN="3" height=13>Uso 
                do banco</td><td class=ct valign=top height=13 width=7> <img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=83 height=13>Carteira</td><td class=ct valign=top height=13 width=7> 
                <img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=43 height=13>Espécie</td><td class=ct valign=top height=13 width=7>
                <img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=103 height=13>Quantidade</td><td class=ct valign=top height=13 width=7>
                <img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=102 height=13>
                Valor do Documento</td><td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=180 height=13>(=) 
                Valor do Documento</td></tr><tr> <td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td valign=top class=cp height=12 COLSPAN="3"><div align=left> 
                </div></td><td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top  width=83> 
                <div align=left> <span class="campo">
                        <?php echo ($dadosboleto["carteira"] == "001" ? "RG" : $dadosboleto["carteira"]); ?>
                    </span></div></td><td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top  width=43><div align=left><span class="campo">
                        <?php echo $dadosboleto["especie"] ?>
                    </span> 
                </div></td><td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top  width=103><span class="campo">
                    <?php echo $dadosboleto["quantidade"] ?>
                </span> 
            </td>
            <td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top  width=102>
                <span class="campo">
                    <?php echo $dadosboleto["valor_unitario"] ?>
                </span></td>
            <td class=cp valign=top width=7 height=12> <img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top align=right width=180 height=12> 
                <span class="campo">
                    <?php echo $dadosboleto["valor_boleto"] ?>
                </span></td>
        </tr><tr><td valign=top width=7 height=1> <img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=75 border=0></td><td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=31 height=1><img height=1 src="imagens/2.png" width=31 border=0></td><td valign=top width=7 height=1> 
                <img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=83 height=1><img height=1 src="imagens/2.png" width=83 border=0></td><td valign=top width=7 height=1> 
                <img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=43 height=1><img height=1 src="imagens/2.png" width=43 border=0></td><td valign=top width=7 height=1>
                <img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=103 height=1><img height=1 src="imagens/2.png" width=103 border=0></td><td valign=top width=7 height=1>
                <img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=102 height=1><img height=1 src="imagens/2.png" width=102 border=0></td><td valign=top width=7 height=1>
                <img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=180 height=1><img height=1 src="imagens/2.png" width=180 border=0></td></tr></tbody> 
</table><table cellspacing=0 cellpadding=0 width=666 border=0><tbody><tr><td align=right width=10><table cellspacing=0 cellpadding=0 border=0 align=left><tbody> 
                        <tr> <td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td></tr><tr> 
                            <td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td></tr><tr> 
                            <td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=1 border=0></td></tr></tbody></table></td><td valign=top width=468 rowspan=5><font class=ct>Instruções 
                (Texto de responsabilidade do Beneficiário)</font><br><br><span class=cp> <FONT class=campo>
                    <?php echo $dadosboleto["instrucoes1"]; ?><br>
                    <?php echo $dadosboleto["instrucoes2"]; ?><br>
                    <?php echo $dadosboleto["instrucoes3"]; ?><br>
                    <?php echo $dadosboleto["instrucoes4"]; ?></FONT><br><br> 
                </span></td>
            <td align=right width=188><table cellspacing=0 cellpadding=0 border=0><tbody><tr><td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=180 height=13>(-) 
                                Desconto</td></tr><tr> <td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top align=right width=180 height=12></td></tr><tr> 
                            <td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=180 height=1><img height=1 src="imagens/2.png" width=180 border=0></td></tr></tbody></table></td></tr><tr><td align=right width=10> 
                <table cellspacing=0 cellpadding=0 border=0 align=left><tbody><tr><td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td></tr><tr><td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td></tr><tr><td valign=top width=7 height=1> 
                                <img height=1 src="imagens/2.png" width=1 border=0></td></tr></tbody></table></td><td align=right width=188><table cellspacing=0 cellpadding=0 border=0><tbody><tr><td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=180 height=13>(-) 
                                Outras Deduções / Abatimentos</td></tr><tr><td class=cp valign=top width=7 height=12> <img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top align=right width=180 height=12></td></tr><tr><td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=180 height=1><img height=1 src="imagens/2.png" width=180 border=0></td></tr></tbody></table></td></tr><tr><td align=right width=10> 
                <table cellspacing=0 cellpadding=0 border=0 align=left><tbody><tr><td class=ct valign=top width=7 height=13> 
                                <img height=13 src="imagens/1.png" width=1 border=0></td></tr><tr><td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td></tr><tr><td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=1 border=0></td></tr></tbody></table></td><td align=right width=188> 
                <table cellspacing=0 cellpadding=0 border=0><tbody><tr><td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=180 height=13>(+) 
                                Mora / Multa / Juros</td></tr><tr><td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top align=right width=180 height=12></td></tr><tr> 
                            <td valign=top width=7 height=1> <img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=180 height=1> 
                                <img height=1 src="imagens/2.png" width=180 border=0></td></tr></tbody></table></td></tr><tr><td align=right width=10><table cellspacing=0 cellpadding=0 border=0 align=left><tbody><tr> 
                            <td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td></tr><tr><td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td></tr><tr><td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=1 border=0></td></tr></tbody></table></td><td align=right width=188> 
                <table cellspacing=0 cellpadding=0 border=0><tbody><tr> <td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=180 height=13>(+) 
                                Outros Acréscimos</td></tr><tr> <td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top align=right width=180 height=12></td></tr><tr><td valign=top width=7 height=1><img height=1 src="imagens/2.png" width=7 border=0></td><td valign=top width=180 height=1><img height=1 src="imagens/2.png" width=180 border=0></td></tr></tbody></table></td></tr><tr><td align=right width=10><table cellspacing=0 cellpadding=0 border=0 align=left><tbody><tr><td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td></tr><tr><td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td></tr></tbody></table></td><td align=right width=188><table cellspacing=0 cellpadding=0 border=0><tbody><tr><td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td><td class=ct valign=top width=180 height=13>(=) 
                                Valor Cobrado</td></tr><tr><td class=cp valign=top width=7 height=12><img height=12 src="imagens/1.png" width=1 border=0></td><td class=cp valign=top align=right width=180 height=12></td></tr></tbody> 
                </table></td></tr></tbody></table><table cellspacing=0 cellpadding=0 width=666 border=0><tbody><tr><td valign=top width=666 height=1><img height=1 src="imagens/2.png" width=666 border=0></td></tr></tbody></table>               
<table cellspacing=0 cellpadding=0 border=0 width=666>
    <tbody>
        <tr>
            <td class=cp valign=top width=7 height=13>
                <img height=13 src="imagens/1.png" width=1 border=0>
            </td>
            <td class=cp valign=top width=70 height=13>
                <span class="campo">
                    Pagador:                    
                </span>
            </td>
            <td class=cp valign=top width=402 height=13>
                <span class="campo">
                    <?php echo $dadosboleto["sacado"] ?>
                </span>
                <span style="float: right" class="campo">                    
                    CNPJ/CPF: <?php echo $dadosboleto["sacadocnpj"] ?>
                </span>
            </td>
            <td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td>
            <td class=ct valign=top width=180 height=13>Cód. baixa</td>             
        </tr>
        <tr>
            <td class=cp valign=top width=7 height=13>
                <img height=13 src="imagens/1.png" width=1 border=0>
            </td>
            <td class=cp valign=top width=70 height=13>
                <span class="campo">
                    Endereço:
                </span>
            </td>
            <td class=cp valign=top width=402 height=11>
                <span class="campo">
                    <?php echo $dadosboleto["endereco1"] ?>
                    <?php echo $dadosboleto["endereco2"] ?>
                </span>
            </td>
            <td class=ct valign=top width=7 height=13><img height=13 src="imagens/1.png" width=1 border=0></td>
            <td class=ct valign=top width=180 height=13></td>            
        </tr>
        <tr>
            <td colspan="5" valign=top width=666 height=1>
                <img height=1 src="imagens/2.png" width=666 border=0>
            </td>
        </tr>
    </tbody>
</table>                                
                <TABLE cellSpacing=0 cellPadding=0 border=0 width=666><TBODY><TR><TD class=ct  width=7 height=12></TD><TD class=ct  width=409 >Pagador/Avalista</TD><TD class=ct  width=250 ><div align=right>Autenticação 
                    mecânica - <b class=cp>Ficha de Compensação</b></div></TD></TR><TR><TD class=ct  colspan=3 ></TD></tr></tbody></table><TABLE cellSpacing=0 cellPadding=0 width=666 border=0><TBODY><TR><TD vAlign=bottom align=left height=50><?php fbarcode($dadosboleto["codigo_barras"]); ?> 
            </TD>
        </tr></tbody></table><TABLE cellSpacing=0 cellPadding=0 width=666 border=0><TR><TD class=ct width=666></TD></TR><TBODY><TR><TD class=ct width=666><div align=right>Corte 
                    na linha pontilhada</div></TD></TR><TR><TD class=ct width=666><img height=1 src="imagens/6.png" width=665 border=0></TD></tr></tbody></table>
</BODY></HTML>
