<?php
$codigobanco = "001";
$codigo_banco_com_dv = geraCodigoBanco($codigobanco);
$nummoeda = "9";
$fator_vencimento = fator_vencimento($dadosboleto["data_vencimento"]);

//valor tem 10 digitos, sem virgula
$valor = formata_numero($dadosboleto["valor_boleto"], 10, 0, "valor");
//agencia à sempre 4 digitos
$agencia = substr($dadosboleto["agencia"],0, 4);
//conta à sempre 8 digitos
$conta = formata_numero($dadosboleto["conta"], 8, 0);
//carteira 18
$carteira = $dadosboleto["carteira"];
//agencia e conta
$agencia_codigo = $agencia . "-" . modulo_11_bb($agencia) . " / " . $conta . "-" . modulo_11_bb($conta);
//Zeros: usado quando convenio de 7 digitos
$livre_zeros = '000000';

// Carteira 18 com Conv�nio de 8 dígitos
if ($dadosboleto["formatacao_convenio"] == "8") {
    $convenio = formata_numero($dadosboleto["convenio"], 8, 0, "convenio");
    $nossonumero = formata_numero($dadosboleto["nosso_numero"], 9, 0);
    $dv = modulo_11_bb("$codigobanco$nummoeda$fator_vencimento$valor$livre_zeros$convenio$nossonumero$carteira");
    $linha = "$codigobanco$nummoeda$dv$fator_vencimento$valor$livre_zeros$convenio$nossonumero$carteira";
    //montando o nosso numero que aparecer� no boleto
    $nossonumero = $convenio . $nossonumero . "-" . modulo_11_bb($convenio . $nossonumero);
}

// Carteira 18 com Conv�nio de 7 dígitos
if ($dadosboleto["formatacao_convenio"] == "7") {
    $convenio = formata_numero($dadosboleto["convenio"], 7, 0, "convenio");
    $nossonumero = formata_numero($dadosboleto["nosso_numero"], 10, 0);
    $dv = modulo_11_bb("$codigobanco$nummoeda$fator_vencimento$valor$livre_zeros$convenio$nossonumero$carteira");
    $linha = "$codigobanco$nummoeda$dv$fator_vencimento$valor$livre_zeros$convenio$nossonumero$carteira";
    $nossonumero = $convenio . $nossonumero;
}

// Carteira 18 com Conv�nio de 6 dígitos
if ($dadosboleto["formatacao_convenio"] == "6") {
    echo "1.2"; exit;    
    $convenio = formata_numero($dadosboleto["convenio"], 6, 0, "convenio");
    if ($dadosboleto["formatacao_nosso_numero"] == "1") {
        // Nosso número de até 5 dígitos
        $nossonumero = formata_numero($dadosboleto["nosso_numero"], 5, 0);
        $dv = modulo_11_bb("$codigobanco$nummoeda$fator_vencimento$valor$convenio$nossonumero$agencia$conta$carteira");
        $linha = "$codigobanco$nummoeda$dv$fator_vencimento$valor$convenio$nossonumero$agencia$conta$carteira";
        //montando o nosso numero que aparecer� no boleto
        $nossonumero = $convenio . $nossonumero . "-" . modulo_11_bb($convenio . $nossonumero);
    }
    if ($dadosboleto["formatacao_nosso_numero"] == "2") {
        // Nosso número de até 17 dígitos
        $nservico = "21";
        $nossonumero = formata_numero($dadosboleto["nosso_numero"], 17, 0);
        $dv = modulo_11_bb("$codigobanco$nummoeda$fator_vencimento$valor$convenio$nossonumero$nservico");
        $linha = "$codigobanco$nummoeda$dv$fator_vencimento$valor$convenio$nossonumero$nservico";
    }
}

$dadosboleto["codigo_barras"] = $linha;
$dadosboleto["linha_digitavel"] = monta_linha_digitavel($linha);
$dadosboleto["agencia_codigo"] = $agencia_codigo;
$dadosboleto["nosso_numero"] = $nossonumero;
$dadosboleto["codigo_banco_com_dv"] = $codigo_banco_com_dv;