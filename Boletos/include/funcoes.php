<?php

// FUNÇÕES
// Algumas foram retiradas do Projeto PhpBoleto e modificadas para atender as particularidades de cada banco

function digitoVerificador_barra($numero) {
    $resto2 = modulo_11($numero, 9, 1);
    $digito = 11 - $resto2;
    if ($digito == 0 || $digito == 1 || $digito == 10 || $digito == 11) {
        $dv = 1;
    } else {
        $dv = $digito;
    }
    return $dv;
}

function formata_numero($numero, $loop, $insert, $tipo = "geral") {
    if ($tipo == "geral") {
        $numero = str_replace(".", "", str_replace(",", "", $numero));
        while (strlen($numero) < $loop) {
            $numero = $insert . $numero;
        }
    }
    if ($tipo == "valor") {
        $numero = str_replace(".", "", str_replace(",", "", $numero));
        while (strlen($numero) < $loop) {
            $numero = $insert . $numero;
        }
    }
    if ($tipo == "convenio") {
        while (strlen($numero) < $loop) {
            $numero = $numero . $insert;
        }
    }
    return $numero;
}

function fbarcode($valor) {

    $fino = 1;
    $largo = 3;
    $altura = 50;

    $barcodes[0] = "00110";
    $barcodes[1] = "10001";
    $barcodes[2] = "01001";
    $barcodes[3] = "11000";
    $barcodes[4] = "00101";
    $barcodes[5] = "10100";
    $barcodes[6] = "01100";
    $barcodes[7] = "00011";
    $barcodes[8] = "10010";
    $barcodes[9] = "01010";
    for ($f1 = 9; $f1 >= 0; $f1--) {
        for ($f2 = 9; $f2 >= 0; $f2--) {
            $f = ($f1 * 10) + $f2;
            $texto = "";
            for ($i = 1; $i < 6; $i++) {
                $texto .= substr($barcodes[$f1], ($i - 1), 1) . substr($barcodes[$f2], ($i - 1), 1);
            }
            $barcodes[$f] = $texto;
        }
    }


//Desenho da barra
//Guarda inicial
    ?><img src="imagens/p.png" width=<?php echo $fino ?> height=<?php echo $altura ?> border=0><img 
        src="imagens/b.png" width=<?php echo $fino ?> height=<?php echo $altura ?> border=0><img 
        src="imagens/p.png" width=<?php echo $fino ?> height=<?php echo $altura ?> border=0><img 
        src="imagens/b.png" width=<?php echo $fino ?> height=<?php echo $altura ?> border=0><img 
    <?php
    $texto = $valor;
    if ((strlen($texto) % 2) <> 0) {
        $texto = "0" . $texto;
    }

// Draw dos dados
    while (strlen($texto) > 0) {
        $i = round(esquerda($texto, 2));
        $texto = direita($texto, strlen($texto) - 2);
        $f = $barcodes[$i];
        for ($i = 1; $i < 11; $i+=2) {
            if (substr($f, ($i - 1), 1) == "0") {
                $f1 = $fino;
            } else {
                $f1 = $largo;
            }
            ?>
                src="imagens/p.png" width=<?php echo $f1 ?> height=<?php echo $altura ?> border=0><img 
                <?php
                if (substr($f, $i, 1) == "0") {
                    $f2 = $fino;
                } else {
                    $f2 = $largo;
                }
                ?>
                src="imagens/b.png" width=<?php echo $f2 ?> height=<?php echo $altura ?> border=0><img 
                <?php
            }
        }

// Draw guarda final
        ?>
        src="imagens/p.png" width=<?php echo $largo ?> height=<?php echo $altura ?> border=0><img 
        src="imagens/b.png" width=<?php echo $fino ?> height=<?php echo $altura ?> border=0><img 
        src="imagens/p.png" width=<?php echo 1 ?> height=<?php echo $altura ?> border=0> 
    <?php
}

//Fim da função

function esquerda($entra, $comp) {
    return substr($entra, 0, $comp);
}

function direita($entra, $comp) {
    return substr($entra, strlen($entra) - $comp, $comp);
}

function fator_vencimento($data) {
  if ($data != "") {
    $data = explode("/",$data);
    $ano = $data[2];
    $mes = $data[1];
    $dia = $data[0];
    $toReturn = abs((_dateToDays("1997","10","07")) - (_dateToDays($ano, $mes, $dia)));
    if (strlen($toReturn) > 4) {
        $toReturn = abs((_dateToDays("2022","05","29")) - (_dateToDays($ano, $mes, $dia)));
        return $toReturn;
    } else {
        return $toReturn;
    }    
  } else {
    return "0000";
  }
}

function _dateToDays($year, $month, $day) {
    $century = substr($year, 0, 2);
    $year = substr($year, 2, 2);
    if ($month > 2) {
        $month -= 3;
    } else {
        $month += 9;
        if ($year) {
            $year--;
        } else {
            $year = 99;
            $century--;
        }
    }
    return ( floor(( 146097 * $century) / 4) +
            floor(( 1461 * $year) / 4) +
            floor(( 153 * $month + 2) / 5) +
            $day + 1721119);
}

function modulo_10($num) {
    $numtotal10 = 0;
    $fator = 2;

    // Separacao dos numeros
    for ($i = strlen($num); $i > 0; $i--) {
        // pega cada numero isoladamente
        $numeros[$i] = substr($num, $i - 1, 1);
        // Efetua multiplicacao do numero pelo (falor 10)
        // 2002-07-07 01:33:34 Macete para adequar ao Mod10 do Ita�
        $temp = $numeros[$i] * $fator;
        $temp0 = 0;
        foreach (preg_split('//', $temp, -1, PREG_SPLIT_NO_EMPTY) as $k => $v) {
            $temp0+=$v;
        }
        $parcial10[$i] = $temp0; //$numeros[$i] * $fator;
        // monta sequencia para soma dos digitos no (modulo 10)
        $numtotal10 += $parcial10[$i];
        if ($fator == 2) {
            $fator = 1;
        } else {
            $fator = 2; // intercala fator de multiplicacao (modulo 10)
        }
    }

    // v�rias linhas removidas, vide função original
    // Calculo do modulo 10
    $resto = $numtotal10 % 10;
    $digito = 10 - $resto;
    if ($resto == 0) {
        $digito = 0;
    }

    return $digito;
}

function modulo_11($num, $base = 9, $r = 0) {

    $soma = 0;
    $fator = 2;

    /* Separacao dos numeros */
    for ($i = strlen($num); $i > 0; $i--) {
        // pega cada numero isoladamente
        $numeros[$i] = substr($num, $i - 1, 1);
        // Efetua multiplicacao do numero pelo falor
        $parcial[$i] = $numeros[$i] * $fator;
        // Soma dos digitos
        $soma += $parcial[$i];
        if ($fator == $base) {
            // restaura fator de multiplicacao para 2 
            $fator = 1;
        }
        $fator++;
    }

    /* Calculo do modulo 11 */
    if ($r == 0) {
        $soma *= 10;
        $digito = $soma % 11;
        if ($digito == 10) {
            $digito = 0;
        }
        return $digito;
    } elseif ($r == 1) {
        $resto = $soma % 11;
        return $resto;
    }
}

// Alterada por Glauber Portella para especifica��o do Ita�
function monta_linha_digitavel($codigo) {
    // campo 1
    $banco = substr($codigo, 0, 3);
    $moeda = substr($codigo, 3, 1);
    $ccc = substr($codigo, 19, 3);
    $ddnnum = substr($codigo, 22, 2);    
    $dv1 = modulo_10($banco . $moeda . $ccc . $ddnnum);    
    // campo 2
    $resnnum = substr($codigo, 24, 6);
    $dac1 = substr($codigo, 30, 1); //modulo_10($agencia.$conta.$carteira.$nnum);
    $dddag = substr($codigo, 31, 3);
    $dv2 = modulo_10($resnnum . $dac1 . $dddag);    
    // campo 3
    $resag = substr($codigo, 34, 1);
    $contadac = substr($codigo, 35, 6); //substr($codigo,35,5).modulo_10(substr($codigo,35,5));
    $zeros = substr($codigo, 41, 3);
    $dv3 = modulo_10($resag . $contadac . $zeros);
    // campo 4
    $dv4 = substr($codigo, 4, 1);
    // campo 5
    $fator = substr($codigo, 5, 4);
    $valor = substr($codigo, 9, 10);
    $campo1 = substr($banco . $moeda . $ccc . $ddnnum . $dv1, 0, 5) . '.' . substr($banco . $moeda . $ccc . $ddnnum . $dv1, 5, 5);
    $campo2 = substr($resnnum . $dac1 . $dddag . $dv2, 0, 5) . '.' . substr($resnnum . $dac1 . $dddag . $dv2, 5, 6);
    $campo3 = substr($resag . $contadac . $zeros . $dv3, 0, 5) . '.' . substr($resag . $contadac . $zeros . $dv3, 5, 6);
    $campo4 = $dv4;
    $campo5 = $fator . $valor;
    return "$campo1 $campo2 $campo3 $campo4 $campo5";
}

function geraCodigoBanco($numero) {
    $parte1 = substr($numero, 0, 3);
    $parte2 = modulo_11($parte1);
    return $parte1 . "-" . $parte2;
}

function modulo_11_bb($num, $base=9, $r=0) {
    $soma = 0;
    $fator = 2;
    for ($i = strlen($num); $i > 0; $i--) {
        $numeros[$i] = substr($num,$i-1,1);
        $parcial[$i] = $numeros[$i] * $fator;
        $soma += $parcial[$i];
        if ($fator == $base) {
            $fator = 1;
        }
        $fator++;
    }
    if ($r == 0) {
        $soma *= 10;
        $digito = $soma % 11;

        if ($digito == 10) {
            $digito = "X";
        }
        if (strlen($num) == "43") {
            if ($digito == "0" or $digito == "X" or $digito > 9) {
                $digito = 1;
            }
        }
        return $digito;
    }
    elseif ($r == 1){
        $resto = $soma % 11;
        return $resto;
    }
}

function digitoVerificador_nossonumero($numero) {
	$resto2 = modulo_11($numero, 9, 1);
     $digito = 11 - $resto2;
     if ($digito == 10 || $digito == 11) {
        $dv = 0;
     } else {
        $dv = $digito;
     }
	 return $dv;
}

function digitoVerificador_nossonumero_brad($numero) {
	$resto2 = modulo_11($numero, 7, 1);
     $digito = 11 - $resto2;
     if ($digito == 10) {
        $dv = "P";
     } elseif($digito == 11) {
     	$dv = 0;
	} else {
        $dv = $digito;
     	}
	 return $dv;
}

function digitoVerificador_cedente($numero) {
  $resto2 = modulo_11($numero, 9, 1);
  $digito = 11 - $resto2;
  if ($digito == 10 || $digito == 11) $digito = 0;
  $dv = $digito;
  return $dv;
}

function digitoVerificador_barra_caixa($numero) {
	$resto2 = modulo_11($numero, 9, 1);
     if ($resto2 == 0 || $resto2 == 1 || $resto2 == 10) {
        $dv = 1;
     } else {
        $dv = 11 - $resto2;
     }
	 return $dv;
}


function monta_linha_digitavel_caixa($codigo) {
        $p1 = substr($codigo, 0, 4);
        $p2 = substr($codigo, 19, 5);
        $p3 = modulo_10("$p1$p2");
        $p4 = "$p1$p2$p3";
        $p5 = substr($p4, 0, 5);
        $p6 = substr($p4, 5);
        $campo1 = "$p5.$p6";
        // e livre e DV (modulo10) deste campo
        $p1 = substr($codigo, 24, 10);
        $p2 = modulo_10($p1);
        $p3 = "$p1$p2";
        $p4 = substr($p3, 0, 5);
        $p5 = substr($p3, 5);
        $campo2 = "$p4.$p5";
        // e livre e DV (modulo10) deste campo
        $p1 = substr($codigo, 34, 10);
        $p2 = modulo_10($p1);
        $p3 = "$p1$p2";
        $p4 = substr($p3, 0, 5);
        $p5 = substr($p3, 5);
        $campo3 = "$p4.$p5";
        // 4. Campo - digito verificador do codigo de barras
        $campo4 = substr($codigo, 4, 1);
        // 5. Campo composto pelo fator vencimento e valor nominal do documento, sem
        // indicacao de zeros a esquerda e sem edicao (sem ponto e virgula). Quando se
        // tratar de valor zerado, a representacao deve ser 000 (tres zeros).
		$p1 = substr($codigo, 5, 4);
		$p2 = substr($codigo, 9, 10);
		$campo5 = "$p1$p2";
        return "$campo1 $campo2 $campo3 $campo4 $campo5"; 
}

function dataJuliano($data) {
    $dia = (int) substr($data, 1, 2);
    $mes = (int) substr($data, 3, 2);
    $ano = (int) substr($data, 6, 4);
    $dataf = strtotime("$ano/$mes/$dia");
    $datai = strtotime(($ano - 1) . '/12/31');
    $dias = (int) (($dataf - $datai) / (60 * 60 * 24));
    return str_pad($dias, 3, '0', STR_PAD_LEFT) . substr($data, 9, 4);
}

function modulo_11_invertido($num) {  // Calculo de Modulo 11 "Invertido" (com pesos de 9 a 2  e não de 2 a 9)
    $ftini = 2;
    $fator = $ftfim = 9;
    $soma = 0;
    for ($i = strlen($num); $i > 0; $i--) {
        $soma += substr($num, $i - 1, 1) * $fator;
        if (--$fator < $ftini)
            $fator = $ftfim;
    }
    $digito = $soma % 11;
    if ($digito > 9)
        $digito = 0;
    return $digito;
}

function dynamicBoleto($mensagem, $dt_ini, $dt_fim) {
    return str_replace(array("@dt_ini", "@dt_fim"), array(escreverData($dt_ini), escreverData($dt_fim)), utf8_encode($mensagem));
}

function dv_sicoob($NossoNumero, $agencia, $num_contrato_con) {
    $sequencia = $agencia . $num_contrato_con . $NossoNumero;
    $cont = 0;
    for ($num = 0; $num <= strlen($sequencia); $num++) {
        $cont++;
        if ($cont == 1) {
            $constante = 3;
        }
        if ($cont == 2) {
            $constante = 1;
        }
        if ($cont == 3) {
            $constante = 9;
        }
        if ($cont == 4) {
            $constante = 7;
            $cont = 0;
        }
        $calculoDv = $calculoDv + (substr($sequencia, $num, 1) * $constante);
    }
    $Resto = $calculoDv % 11;
    if ($Resto == 0 || $Resto == 1) {
        $Dv = 0;
    } else {
        $Dv = 11 - $Resto;
    }
    return $Dv;
}

function digitoVerificador_campolivre($numero) {
    $resto2 = modulo_11($numero, 9, 1);
    if ($resto2 <= 1) {
        $dv = 0;
    } else {
        $dv = 11 - $resto2;
    }
    return $dv;
}