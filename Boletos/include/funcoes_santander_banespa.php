<?php

$codigobanco = "033"; //Antigamente era 353
$codigo_banco_com_dv = geraCodigoBanco($codigobanco);
$nummoeda = "9";
$fixo = "9";   // Numero fixo para a posi��o 05-05
$ios = "0";   // IOS - somente para Seguradoras (Se 7% informar 7, limitado 9%)
$fator_vencimento = fator_vencimento($dadosboleto["data_vencimento"]);

$valor = formata_numero($dadosboleto["valor_boleto"], 10, 0, "valor");
$carteira = $dadosboleto["carteira"];
$codigocliente = formata_numero($dadosboleto["codigo_cliente"], 7, 0);

$nnum = formata_numero($dadosboleto["nosso_numero"], 7, 0);
$dv_nosso_numero = modulo_11($nnum, 9, 0);
$nossonumero = "00000" . $nnum . $dv_nosso_numero;
$vencimento = $dadosboleto["data_vencimento"];
$vencjuliano = dataJuliano($vencimento);

$barra = "$codigobanco$nummoeda$fator_vencimento$valor$fixo$codigocliente$nossonumero$ios$carteira";

$dv = digitoVerificador_barra($barra);
$linha = substr($barra, 0, 4) . $dv . substr($barra, 4);

$dadosboleto["codigo_barras"] = $linha;
$dadosboleto["linha_digitavel"] = monta_linha_digitavel($linha);
$dadosboleto["nosso_numero"] = $nossonumero;
$dadosboleto["codigo_banco_com_dv"] = $codigo_banco_com_dv;