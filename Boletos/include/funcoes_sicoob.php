<?php

$codigobanco = "756";
$numeroFixo = "001";
$nummoeda = "9";
$codigo_banco_com_dv = geraCodigoBanco($codigobanco);
$fator_vencimento = fator_vencimento($dadosboleto["data_vencimento"]);
$valor = formata_numero($dadosboleto["valor_boleto"], 10, 0, "valor");
$agencia = substr($dadosboleto["agencia"], 0, 4);
$conta = formata_numero($dadosboleto["conta"], 5, 0);
$conta_dv = formata_numero($dadosboleto["conta_dv"], 1, 0);
$carteira = $dadosboleto["carteira"];
$conta_cedente = str_pad($dadosboleto["conta_cedente"], 6, "0", STR_PAD_LEFT);
$conta_cedente_dv = $dadosboleto["conta_cedente_dv"];
$nnum = formata_numero($dadosboleto["nosso_numero"], 7, 0);
$nnumdv = dv_sicoob($nnum, $agencia, ("000" . $conta_cedente . $conta_cedente_dv));
$nossonumero = $nnum . '-' . $nnumdv;

$dv = digitoVerificador_barra($codigobanco . $nummoeda . $fator_vencimento . $valor . $carteira . $agencia .
str_pad($carteira, 2, "0", STR_PAD_LEFT) . $conta_cedente . $conta_cedente_dv .
$nnum . $nnumdv . $numeroFixo, 9, 0);

$linha = $codigobanco . $nummoeda . $dv . $fator_vencimento . $valor . $carteira . $agencia .
str_pad($carteira, 2, "0", STR_PAD_LEFT) . $conta_cedente . $conta_cedente_dv .
$nnum . $nnumdv . $numeroFixo;

$agencia_codigo = $agencia . " / " . $conta_cedente . "-" . $conta_cedente_dv;
$dadosboleto["codigo_barras"] = $linha;
$dadosboleto["linha_digitavel"] = monta_linha_digitavel($linha);
$dadosboleto["agencia_codigo"] = $agencia_codigo;
$dadosboleto["nosso_numero"] = $nossonumero;
$dadosboleto["codigo_banco_com_dv"] = $codigo_banco_com_dv;