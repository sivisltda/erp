<?php

$dadosboleto["campo_fixo_obrigatorio"] = $campo_fixo_obrigatorio;
$dadosboleto["inicio_nosso_numero"] = $inicio_nosso_numero;
$dadosboleto["nosso_numero_const1"] = $registro;
$dadosboleto["nosso_numero_const2"] = "4";

$dadosboleto["nosso_numero"] = $nosso_numero;
$dadosboleto["numero_documento"] = $numero_documento;
$dadosboleto["data_vencimento"] = $data_venc;
$dadosboleto["data_documento"] = $data_documento;
$dadosboleto["data_processamento"] = $data_processamento;
$dadosboleto["valor_boleto"] = $valor_boleto;

if (is_numeric($desconto) && $desconto > 0) {
    $dadosboleto["desconto"] = escreverNumero($desconto);
}

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = $sacado;
$dadosboleto["sacadocnpj"] = $sacadocnpj;
$dadosboleto["endereco1"] = $endereco1;
$dadosboleto["endereco2"] = $endereco2;

// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = $demonstrativo1;
$dadosboleto["demonstrativo2"] = $demonstrativo2;
$dadosboleto["demonstrativo3"] = $demonstrativo3;

// INSTRUÇÕES PARA O CAIXA
$dadosboleto["instrucoes1"] = $instrucoes1;
$dadosboleto["instrucoes2"] = $instrucoes2;
$dadosboleto["instrucoes3"] = $instrucoes3;
$dadosboleto["instrucoes4"] = $instrucoes4;

// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = $quantidade;
$dadosboleto["valor_unitario"] = $valor_unitario;
$dadosboleto["aceite"] = $aceite;
$dadosboleto["especie"] = $especie;
$dadosboleto["especie_doc"] = $especie_doc;

// ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //
// DADOS DA SUA CONTA - CEF
$dadosboleto["agencia"] = $agencia; // Num da agencia, sem digito
$dadosboleto["conta"] = $conta;     // Num da conta, sem digito
$dadosboleto["conta_dv"] = $dv;     // Digito do Num da conta
// DADOS PERSONALIZADOS - CEF
$dadosboleto["conta_cedente"] = str_replace(" ", "", $cli_conta); //$cli_conta // Código Cedente do Cliente, com 6 digitos (Somente Números)
$dadosboleto["conta_cedente_dv"] = $cli_dv;
$dadosboleto["carteira"] = $cli_carteira;     // Código da Carteira: pode ser SR (Sem Registro) ou CR (Com Registro) - (Confirmar com gerente qual usar)
// SEUS DADOS
$dadosboleto["identificacao"] = $identificacao;
$dadosboleto["cpf_cnpj"] = $cpf_cnpj;
$dadosboleto["endereco"] = $endereco;
$dadosboleto["cidade_uf"] = $cidade_uf;
$dadosboleto["cedente"] = $cedente;

include("include/funcoes_sicoob.php");

if (isset($dicionario)){
} elseif ($tipo_bol == 12) {
    include("include/layout_sicoob.php");
} elseif ($tipo_bol == 13) {
    include("include/layout_sicoob2.php");
}
