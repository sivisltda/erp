<?php

// DADOS DO BOLETO PARA O SEU CLIENTE
$taxa_boleto = 0;
$data_venc = "11/12/1986";
$valor_cobrado = "10.000,00";
$valor_cobrado = str_replace(",", ".", $valor_cobrado);
$valor_boleto = escreverNumero($valor_cobrado + $taxa_boleto);

$dadosboleto["nosso_numero"] = "22.2222";                // Nosso numero DV - REGRA: Máximo de 10 caracteres!
$dadosboleto["numero_documento"] = "333.333"; // Num do pedido ou do documento = Nosso numero
$dadosboleto["data_vencimento"] = $data_venc;                   // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA

$dadosboleto["data_documento"] = "16/07/2013";    // Data de processamento do boleto (opcional)
$dadosboleto["data_processamento"] = "16/07/2013";    // Data de processamento do boleto (opcional)

$dadosboleto["valor_boleto"] = $valor_boleto;    // Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula
// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = "Razão Social";
$dadosboleto["endereco1"] = "Rua A";
$dadosboleto["endereco2"] = "Rua 2";

// INFORMACOES PARA O CLIENTE

$dadosboleto["demonstrativo1"] = "";
$dadosboleto["demonstrativo2"] = "";
$dadosboleto["demonstrativo3"] = "";

$dadosboleto["instrucoes1"] = "- Sr. Caixa, não receber após o vencimento.";
# $dadosboleto["instrucoes2"]         = "- Receber até 10 dias após o vencimento";
# $dadosboleto["instrucoes3"]         = "- Em caso de dúvidas entre em contato conosco: xxxx@xxxx.com.br";
# $dadosboleto["instrucoes4"]         = "&nbsp;";
// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "001";
$dadosboleto["valor_unitario"] = $valor_boleto;
$dadosboleto["aceite"] = "";
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "REC";


// ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //
// DADOS DA SUA CONTA
$dadosboleto["agencia"] = "123.456";     // Num da agencia, sem digito
$dadosboleto["agencia_dv"] = "9";  // Digito do Num da agencia
$dadosboleto["conta"] = "9";       // Num da conta, sem digito
$dadosboleto["conta_dv"] = "78456";    // Digito do Num da conta
// DADOS PERSONALIZADOS
$dadosboleto["conta_cedente"] = $dadosboleto["conta"];    // ContaCedente do Cliente, sem digito (Somente Números)
$dadosboleto["conta_cedente_dv"] = $dadosboleto["conta_dv"]; // Digito da ContaCedente do Cliente
$dadosboleto["carteira"] = "121";                    // Código da Carteira: 121
// SEUS DADOS
$dadosboleto["identificacao"] = "aaaaaa";
$dadosboleto["cpf_cnpj"] = "bbbbb";
$dadosboleto["endereco"] = "ccccccc";
$dadosboleto["cidade_uf"] = "dddddd";
$dadosboleto["cedente"] = "eeeeee";


// NÃO ALTERAR!
include("include/funcoes_sofisa.php");
include("include/layout_sofisa.php");
?>
