<?php

$dadosboleto["inicio_nosso_numero"] = substr($data_documento, -2);
$dadosboleto["nosso_numero"] = $nosso_numero;  // Nosso numero - REGRA: Máximo de 8 caracteres!
$dadosboleto["numero_documento"] = $numero_documento;	// Num do pedido ou do documento
$dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"] = $data_documento; // Data de emissão do Boleto
$dadosboleto["data_processamento"] = $data_processamento; // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = $sacado;
$dadosboleto["sacadocnpj"] = $sacadocnpj;
$dadosboleto["endereco1"] = $endereco1;
$dadosboleto["endereco2"] = $endereco2;

// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = $demonstrativo1;
$dadosboleto["demonstrativo2"] = $demonstrativo2;
$dadosboleto["demonstrativo3"] = $demonstrativo3;

// INSTRUÇÕES PARA O CAIXA
$dadosboleto["instrucoes1"] = $instrucoes1;
$dadosboleto["instrucoes2"] = $instrucoes2;
$dadosboleto["instrucoes3"] = $instrucoes3;
$dadosboleto["instrucoes4"] = $instrucoes4;

// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = $quantidade;
$dadosboleto["valor_unitario"] = $valor_unitario;
$dadosboleto["aceite"] = $aceite;		
$dadosboleto["especie"] = $especie;
$dadosboleto["especie_doc"] = $especie_doc;
// ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //
// DADOS DA SUA CONTA - SICREDI
$ag = explode("-", $agencia);
$dadosboleto["agencia"] = $ag[0]; // Num da agencia, sem digito
$dadosboleto["conta"] = $conta;     // Num da conta, sem digito
$dadosboleto["conta_dv"] = $dv;     // Digito do Num da conta
// DADOS PERSONALIZADOS - SICREDI
$dadosboleto["posto"] = $ag[1];      // Código do posto da cooperativa de cr�dito
$dadosboleto["byte_idt"] = "2";   // Byte de identifica��o do cedente do bloqueto utilizado para compor o nosso n�mero.
// 1 - Idtf emitente: Cooperativa | 2 a 9 - Idtf emitente: Cedente
$dadosboleto["carteira"] = $cli_carteira;     // Código da Carteira: pode ser SR (Sem Registro) ou CR (Com Registro) - (Confirmar com gerente qual usar)
// SEUS DADOS
$dadosboleto["identificacao"] = $identificacao;
$dadosboleto["cpf_cnpj"] = $cpf_cnpj;
$dadosboleto["endereco"] = $endereco;
$dadosboleto["cidade_uf"] = $cidade_uf;
$dadosboleto["cedente"] = $cedente;

include("include/funcoes_sicredi.php");

if (isset($dicionario)){
} elseif ($tipo_bol == 14) {
    include("include/layout_sicredi.php");
} elseif ($tipo_bol == 15) {
    include("include/layout_sicredi2.php");
}