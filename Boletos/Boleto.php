<?php
session_start();

$contrato = "";
$_GET['crt'] = str_replace("erp", "", $_GET['crt']);

if (is_numeric($_GET['crt'])) {
    $contrato = $_GET['crt'];
}

if ($contrato != '') {
    include("./../Connections/configSivis.php");
    include("./../Connections/funcoesAux.php");
    include("include/funcoes.php");    
    $cur2 = odbc_exec($con2, "select *,(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = id_fornecedores_despesas) email
    from ca_contratos left join sf_fornecedores_despesas on id_fornecedores_despesas = id_cliente 
    left join tb_estados on estado_codigo = estado
    left join tb_cidades on cidade_codigo = cidade 
    where inativa = 0 and numero_contrato = '" . $contrato . "'");
    while ($RFP = odbc_fetch_array($cur2)) {
        $cedente = utf8_encode($RFP['razao_social']);
        $cpf_cnpj = utf8_encode($RFP['cnpj']);
        $endereco = utf8_encode($RFP['endereco']) . " n°" . utf8_encode($RFP['numero']) . "," . utf8_encode($RFP['bairro']);
        $cidade_uf = utf8_encode($RFP['cidade_nome']) . " " . utf8_encode($RFP['estado_sigla']);
        $email = utf8_encode($RFP['email']);
        $site = utf8_encode($RFP['contato']);
        $contrato = utf8_encode($RFP['numero_contrato']);
        $hostname = utf8_encode($RFP['local']);
        $username = utf8_encode($RFP['login']);
        $password = utf8_encode($RFP['senha']);
        $database = utf8_encode($RFP['banco']);
    }
    odbc_close($con2);
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
    $id_get = $_GET['id'];
    include("include/processamento_dados.php");
}

$cel_destinatario = "";
$email_destinatario = "";
if (is_numeric($cliente)) {
    $cur = odbc_exec($con, "select conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = " . $cliente) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $cel_destinatario = $RFP['conteudo_contato'];
    }
    $cur = odbc_exec($con, "select conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = " . $cliente) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $email_destinatario .= $RFP['conteudo_contato'] . ",";
    }
}
?>
<html>
    <head>        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Boleto</title>        
        <script type="text/javascript" src="../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="../assets/plugins/tagsinput/jquery.tagsinput.js"></script>
        <link rel="stylesheet" type="text/css" href="../assets/plugins/tagsinput/jquery.tagsinput.css" />    
    </head>        
    <body>
        <input id="txtMnLanguage" type="hidden" value="pt" />
        <input id="txtId" type="hidden" value="<?php echo $cliente; ?>" />
        <input id="txtIdMens" type="hidden" value="<?php echo $id_get; ?>" />
        <input id="txtSendSms" type="hidden" value="" />
        <style>
            .btnModel {
                border: 1px solid #0099cc;
                box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.15);                
                background: linear-gradient(to bottom, #089dcf 0%,#0099cc 100%);
                padding: 5px 10px 4px 10px;
                display: inline-block;             
                border-radius: 2px;
                font-weight: bold;
                color: #fff;
                margin: 0 1px;            
                transition: all 0.15s ease-in-out;
                opacity: 1;
                text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.3);                
                display: block;
            }
            iframe {
                width: 100%;
            }
            .fancybox-inner {
                border: 1px solid #ccc!important;
                background-color: #f7f7f7;
            }
        </style>
        <style media="print">
            .botao { 
                display: none;
            }
        </style>                    
        <br/>    
        <?php if (isset($_SESSION["contrato"]) && $totCli == 1 && $cliente > 0) { ?>
            <div class="botao" style="float: left;">
                <button class="btn btnModel" onclick="self:print()">Imprimir</button>
            </div>
    <?php if (strlen($cel_destinatario) > 0 && strlen($id_get) > 0) { ?>                
                <div class="botao" style="float: left;">
                    <button type="button" class="btn btnModel" onclick="abrirTelaSMS('<?php echo $cel_destinatario; ?>', true);">Enviar WhatsApp</button>
                    <div class="tagsinput" style="float: left;">
                        <span class="tag" style="background-color: rgb(205, 230, 156); width: 110px;">
                            <span><?php echo $cel_destinatario; ?></span>
                        </span>
                    </div>            
                </div>
                <div class="botao" style="float: left;">
                    <button type="button" class="btn btnModel" onclick="abrirTelaSMS('<?php echo $cel_destinatario; ?>', false);">Enviar Sms</button>
                    <div class="tagsinput" style="float: left;">
                        <span class="tag" style="background-color: rgb(205, 230, 156); width: 110px;">
                            <span><?php echo $cel_destinatario; ?></span>
                        </span>
                    </div>            
                </div>
    <?php } if (strlen($email_destinatario) > 0) { ?>                
                <div class="botao" style="float: left; margin-left: 1%;">
                    <button class="btn btnModel" onclick="abrirTelaEmail();">Enviar email</button>                
                    <input id="txtEmails" name="txtEmails" type="text" class="tags" value="<?php echo $email_destinatario; ?>"/>
                </div>                        
    <?php }
} ?>
        <script type="text/javascript" src="../js/util.js"></script>
        <script type="text/javascript">

                $('#txtEmails').tagsInput({
                    width: 'auto', onChange: function (elem, elem_tags) {
                        if (isEmail(elem_tags)) {
                            $('.tag span').each(function () {
                                if ($(this).text().replace(/^\s+|\s+$/g, "") === elem_tags) {
                                    $(this).parent().css('background-color', '#cde69c');
                                }
                            });
                        }
                    }
                });

                function isEmail(email) {
                    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    return regex.test(email);
                }

                function FecharBox() {
                    $("#newbox").remove();
                }

                function listaEmails() {
                    let emails = [];
                    let lista = $("#txtEmails").val().split(',');
                    $.each(lista, function (index, value) {
                        emails.push(value + "|" + $("#txtId").val() + "||" + $("#txtIdMens").val());
                    });
                    return emails;
                }

                function listaSMS() {
                    let sms = [];
                    sms.push($("#txtSendSms").val() + "|" + $("#txtId").val() + "||" + $("#txtIdMens").val());
                    return sms;
                }

                function abrirTelaEmail() {
                    abrirTelaBox("../../Modulos/CRM/EmailsEnvioForm.php?bol=S", 650, 720);
                }

                function abrirTelaSMS(sms, zap) {
                    abrirTelaBox("../../Modulos/CRM/SMSEnvioForm.php?bol=S" + (zap ? "&zap=s" : ""), 400, 400);
                    $("#txtSendSms").val(sms);
                }

        </script>    
    </body>
</html>