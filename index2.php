<?php
include "Connections/configini.php"; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="css/stylesheet.css"/>
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
        <link rel="stylesheet" type="text/css" href="css/calendar/daterangepicker-bs3.css"/>
        <link rel="stylesheet" type="text/css" href="css/calendar/fullcalendar.css"/>
        <script type="text/javascript" src="js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <style>
            body, .body { background: #F7F7F7; }
            #tabela_2 { width: 100%; border-spacing: 5px; border-collapse: separate; }
            #tabela_2 td { padding: 15px 5px; text-align: center; font-size: 12px; background: #EEE; }            
            .indcont {
                float: left;
                background: #FFF;
                margin-bottom: 10px;
                border: 1px solid #E6E9ED;
            }
            .indhead {
                color: #006568;
                font-size: 18px;
                background: #FFF;
                margin-left: 15px;
                line-height: 40px;
                text-align: center;
                width: calc(100% - 30px);
                border-bottom: 2px solid #E6E9ED;
            }
            .indfoot {
                padding: 10px 10px 0;
            }
            .indlist {
                padding: 5px 0;
                border-bottom: 1px solid #EEE;
            }
            .indlist img {
                float: left;
                width: 38px;
                height: 38px;
                border-radius: 50%;
                border: 1px solid #DDD;
            }
            .indlist .txtname {
                float: left;
                color: #666;
                font-size: 14px;
                margin-left: 10px;
                line-height: 18px;
                margin-bottom: 2px;
                width: calc(100% - 50px);
            }
            .indlist .txtpend {
                color: #FFF;
                float: right;
                font-size: 10px;
                padding: 2px 5px;
                line-height: 16px;
                border-radius: 3px;
                background: var(--cor-secundaria);
                margin: 0;
            }
            #tablediv1 {
                border-spacing: 0;
                border-collapse: collapse;
            }
            #tablediv1 a { color: #73879C; }
            #tablediv1 th {
                color: var(--cor-secundaria);
                padding: 0 5px;
                background: #EDEDED;
            }
            #tablediv1 td {
                color: #73879C;
                padding: 5px;
            }
            #tablediv1 td span {
                font-size: 15px;
            }
            #tablediv1 tr.odd td {
                background: #FFF;
            }
            #tablediv1 tr.even td {
                background: #F9F9F9;
            }
            #tablediv2 {
                width: 100%;
                border-spacing: 2px;
                border-collapse: separate;
            }
            #tablediv2 th {
                color: #666;
                padding: 0 5px;
            }
            #tablediv2 td {
                font-size: 12px;
                background: #EEE;
                padding: 15px 5px;
                text-align: center;
            }
            .txthead {
                color: var(--cor-secundaria);
                margin: 0 10px;
                font-size: 18px;
            }
            .txttext {
                color: #666;
                margin: 0 10px 5px;
            }
            .txttext b {
                color: #FF0000;
            }
            .btnajust {
                border: 0;
                background: var(--cor-primaria);
                font-size: 13px;
                float: right;
                height: 52.77px;
                width: 49%;
                margin: 0;
            }
            .tophead {
                height: 77px;
            }
            .topant {
                text-align: right;
                font-size: 11px;                
                color: gray;
            }
            .topant span {
                color: red;
            }
            .topant span.ico-arrow-up {
                color: #006568;
            }
            .topinto {
                float: right;
                margin-top: 25px;
                margin-right: 5px;
            }         
            @media (max-width: 1150px){
                #tablediv2 th {
                    font-size: 9px;
                }
                #tablediv2 td {
                    font-size: 9px;
                    padding: 10px 2px;
                }
            }
        </style>
    </head>
    <body>
        <div id="loader"><img src="img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("menuLateral.php"); ?>
            <div class="body">
                <?php
                include("top.php");
                include("querys_finance_dashboard.php");
                ?>
                <div class="content">
                    <div class="page-header" style="justify-content: space-between; display:flex; padding: 10px 0;">
                        <div class="" style="width: 50%;">
                            <div class="icon"><span class="ico-arrow-right"></span></div>
                            <h3>Página Principal<small>Resumo dos Módulos</small></h3>
                        </div>
                        <span style="float:right;width:27%;">     
                            <?php if ($mdl_aca_ > 0 && $ckb_aca_inf_gerenciais_) { ?>
                                <button class="button btnajust" style="margin-left: 2%;width:53%;" type="button" onClick="window.top.location.href = 'Modulos/Academia/BI-InformacoesGerenciais.php'">Informações Gerenciais</button>
                            <?php } ?>                              
                            <button class="button btnajust" style="width: 45%;" type="button" onClick="window.top.location.href = 'index.php'">Página Básica</button>
                        </span>
                    </div>
                    <div style="clear:both"></div>
                    <div class="topcont">
                        <div class="tophead" style="width:calc(22.5% - 1px); border:0">
                            <div class="topname">Faturamento Mês Atual (Vendas)</div>
                            <div class="topresp">
                                <div class="topnumb"><?php echo escreverNumero($Faturamentos[3]["value"]); ?></div>
                                <div style="clear:both"></div>                                
                            </div>
                            <div class="topant"><span class="ico-arrow-<?php echo ($Faturamentos[3]["value"] > $totalAtivosAnt  ? "up" : "down"); ?>"></span> MÊS ANTERIOR: <span>R$ <?php echo escreverNumero($totalAtivosAnt); ?></span></div>
                        </div>
                        <div class="tophead" style="width:calc(17.5% - 1px)">
                            <div class="topname">Saldo Caixa do Dia (Vendas)</div>
                            <div class="topresp">
                                <div class="topnumb"><?php echo escreverNumero($manha + $tarde + $noite); ?></div>
                                <div style="clear:both"></div>
                            </div>
                        </div>
                        <div class="tophead" style="width:calc(12% - 2px)">
                            <div class="topname">Clientes Ativos</div>
                            <div class="topresp">                                    
                                <div class="topnumb" style="float: right;">
                                    <?php 
                                    $total = 0;
                                    $suspenso = 0;
                                    if (!empty($Status_alunos)) {
                                        foreach ($Status_alunos as $status) {
                                            if (substr($status["name"], 0, 5) == "Ativo") {
                                                $total = $total + $status["value"];
                                            } else if ($status["name"] == "Suspenso") {
                                                $suspenso = $suspenso + $status["value"];
                                            }
                                        }
                                    }
                                    echo $total; ?>
                                </div>
                                <div class="topinto">
                                    <span class="ico-arrow-<?php echo ($total > $clientesAtivosAnt  ? "up" : "down"); ?>"></span> 
                                    <span style="color: red;"><?php echo $clientesAtivosAnt; ?></span>
                                </div>                                
                                <div style="clear:both"></div>                                
                            </div>
                            <div class="topant">SUSPENSOS: <span style="color: #e09f4f;"><?php echo $suspenso; ?></span></div>
                        </div>
                        <?php if ($mdl_seg_ > 0 ) { ?>
                        <div class="tophead" style="width:calc(12% - 2px)">
                            <div class="topname">Veículos Protegidos</div>
                            <div class="topresp">
                                <div class="topnumb"><?php echo $veiculos; ?></div>
                                <div class="topinto">
                                    <span class="ico-arrow-<?php echo ($veiculos > $veiculosAtivosAnt  ? "up" : "down"); ?>"></span> 
                                    <span style="color: red;"><?php echo $veiculosAtivosAnt; ?></span>
                                </div>                                
                                <div style="clear:both"></div>
                            </div>
                            <div class="topant">SUSPENSOS: <span style="color: #e09f4f;"><?php echo $veiculoSuspenso; ?></span></div>
                        </div>
                        <?php } ?>
                        <div class="tophead" style="width:calc(12% - 2px)">
                            <div class="topname">
                                <?php if ($ckb_bi_mat_ren_ == 1) { ?>                                                                    
                                    <a href="<?php echo $UrlAtual; ?>/Modulos/BI/BI-Matriculas-Renovacoes.php">Novos planos no mês</a>
                                <?php } else { ?>
                                    Novos planos no mês
                                <?php } ?>                                
                            </div>
                            <div class="topresp">
                                <div class="topnumb"><?php echo $contratos_atual; ?></div>
                                <div class="topicon"><span class="ico-plus"></span></div>
                                <div class="topinto">
                                    <span class="ico-arrow-<?php echo ($contratos_atual > $contratos_passado  ? "up" : "down"); ?>"></span> 
                                    <span style="color: red;"><?php echo $contratos_passado; ?></span>
                                </div>                                
                                <div style="clear:both"></div>
                            </div>
                            <div class="topant">CRESCIMENTO: <span><?php echo escreverNumero($contratos_passado > 0 ? ((($contratos_atual/$contratos_passado)-1)*100) : 0); ?>%</span></div>
                        </div>
                        <div class="tophead" style="width:calc(12% - 2px)">
                            <div class="topname">
                                <?php if ($ckb_bi_retencao_ == 1) { ?>
                                    <a href="<?php echo $UrlAtual; ?>/Modulos/BI/BI-Churn.php">Evasão no mês</a>
                                <?php } else { ?>
                                    Evasão no mês
                                <?php } ?>
                            </div>
                            <div class="topresp">
                                <div class="topnumb"><?php echo $nrenovados; ?></div>
                                <div class="topicon"><span class="ico-warning"></span></div>
                                <div style="clear:both"></div>
                            </div>
                            <div class="topant">CHURN (MÊS ATUAL): <span><?php echo escreverNumero(($nrenovados * 100)/(($renovados + $nrenovados) > 0 ? ($renovados + $nrenovados) : 1)); ?>%</span></div>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div style="margin-top:10px">
                        <div class="indcont" style="width:calc(24.25% - 2px)">
                            <div class="indhead">Contas a Pagar Mês Atual</div>
                            <div class="indfoot" style="height:320px" id="chartdiv1"></div>
                        </div>
                        <div class="indcont" style="width:calc(24.25% - 2px); margin-left:1%">
                            <div class="indhead">Status dos Clientes</div>
                            <div class="indfoot" style="height:320px" id="chartdiv2"></div>
                        </div>
                        <div class="indcont" style="width:calc(49.5% - 2px); margin-left:1%">
                            <div class="indhead">Minhas Pendências</div>
                            <div class="indfoot" style="height:320px">
                                <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tablediv1">
                                    <thead>
                                        <tr>
                                            <th><center>Data/Hora</center></th>
                                            <th>Cliente</th>
                                            <th>Origem</th>
                                            <th>Tipo</th>
                                            <th>Atendente</th>
                                            <th><center>Pend</center></th>
                                        </tr>
                                    </thead>
                                </table>
                                <div style="clear:both"></div>
                            </div>
                        </div>
                        <div style="clear:both"></div>
                        <div style="float:left; width:49.5%">
                            <div class="indcont" style="width:calc(100% - 2px)">
                                <div class="indhead">Meta de Clientes Ativos
                                    <span style="float:right">
                                        <button class="button" style="border:0; background:var(--cor-secundaria)" type="button" onclick="AbrirBox()">
                                            <span class="ico-cog-3 icon-white"></span>
                                        </button>
                                    </span>
                                </div>
                                <div class="indfoot" style="float:left; width:100%; height:40px; padding:5px 0" id="chartdiv4"></div>
                                <div style="width:70px; height:50px; position:relative; top:-38px; left:50%; margin-left:-35px; color:#FFF; font-size:19px; font-weight:bold; text-align:center">
                                    <?php echo ($Meta_alunos > 0 ? escreverNumero(($total * 100) / $Meta_alunos) : 0)."%"; ?>
                                </div>
                            </div>
                            <?php if ($mdl_seg_ > 0 ) { ?>
                            <div class="indcont" style="width:calc(100% - 2px)">
                                <div class="indhead">Meta de Veículos Protegidos</div>
                                <div class="indfoot" style="float:left; width:100%; height:40px; padding:5px 0" id="chartdiv5"></div>
                                <div style="width:70px; height:50px; position:relative; top:-38px; left:50%; margin-left:-35px; color:#FFF; font-size:19px; font-weight:bold; text-align:center">
                                    <?php echo ($Meta_veiculos > 0 ? escreverNumero(($veiculos * 100) / $Meta_veiculos) : 0)."%"; ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="indcont" style="width:calc(100% - 2px)">
                                <div class="indhead">Faturamento nos Últimos Meses</div>
                                <div class="indfoot" style="height:260px; padding:5px 10px" id="chartdiv3"></div>
                            </div>
                        </div>
                        <div style="float:left; width:49.5%; margin-left:1%">
                            <div class="indcont" style="width:<?php echo ($_SESSION["mod_emp"] != 3 ? "calc(49% - 2px)" : "100%"); ?>">
                                <div class="indhead">Atendimentos</div>
                                <div class="indfoot" style="height:468px; overflow-y:scroll">
                                    <?php if (!empty($Pendencias_A)) {
                                        foreach ($Pendencias_A as $Pendencia) { ?>
                                        <div class="indlist">
                                            <img src="<?php if (file_exists("Pessoas/".$contrato."/".$Pendencia["id"].".png")) {
                                                            echo "Pessoas/".$contrato."/".$Pendencia["id"].".png";
                                                        } else {
                                                            echo "img/dmitry_m.gif";
                                                        } ?>"/>
                                            <div class="txtname"><?php echo $Pendencia["name"]; ?></div>
                                            <div class="txtpend"><?php echo $Pendencia["value"]; ?> Pendente(s)</div>
                                            <div style="clear:both"></div>
                                        </div>
                                    <?php }} ?>
                                </div>
                            </div>
                            <?php if ($_SESSION["mod_emp"] != 3) { ?> 
                            <div class="indcont" style="width:calc(49% - 2px); margin-left:2%">
                                <div class="indhead">Serviços</div>
                                <div class="indfoot" style="height:468px; overflow-y: scroll;">
                                    <?php if (!empty($Pendencias_S)) {
                                        foreach ($Pendencias_S as $Pendencia) { ?>
                                        <div class="indlist">
                                            <img src="<?php if (file_exists ("Pessoas/".$contrato."/".$Pendencia["id"].".png")) {
                                                            echo "Pessoas/".$contrato."/".$Pendencia["id"].".png";
                                                        } else {
                                                            echo "img/dmitry_m.gif";
                                                        } ?>"/>
                                            <div class="txtname"><?php echo $Pendencia["name"]; ?></div>
                                            <div class="txtpend"><?php echo $Pendencia["value"]; ?> Pendente(s)</div>
                                            <div style="clear:both"></div>
                                        </div>
                                    <?php }} ?>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <div style="clear:both"></div>
                        <div class="indcont" style="width:calc(100% - 2px)">
                            <div class="indhead">Fluxo de Caixa Projetado</div>
                            <div class="indfoot" style="margin-bottom:10px">
                                <div class="txthead">Faça uma análise das contas a pagar / receber e da sua necessidade de caixa para cada mês:</div>
                                <div class="txttext"><b>Importante:</b> Aqui você terá uma visão consolidada do seu contas a pagar / receber. Obeserve que a Necessidade de Caixa é o valor que você precisa ter no caixa para conseguir honrar seus compromissos financeiros.</div>
                                <table style="float:none" id="tabela_2">
                                    <thead>
                                        <tr>
                                            <th style="width:130px"></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <script type="text/javascript" src="js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
                    <script type="text/javascript" src="js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
                    <script type="text/javascript" src="js/plugins/jquery/globalize.js"></script>
                    <script type="text/javascript" src="js/plugins/other/excanvas.js"></script>
                    <script type="text/javascript" src="js/plugins/other/jquery.mousewheel.min.js"></script>
                    <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>
                    <script type="text/javascript" src="js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
                    <script type="text/javascript" src="js/plugins/epiechart/jquery.easy-pie-chart.js"></script>
                    <script type="text/javascript" src="js/plugins/sparklines/jquery.sparkline.min.js"></script>
                    <script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
                    <script type="text/javascript" src="js/plugins/uniform/jquery.uniform.min.js"></script>
                    <script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
                    <script type="text/javascript" src="js/plugins.js"></script>
                    <script type="text/javascript" src="js/actions.js"></script>
                    <script type="text/javascript" src="Modulos/BI/amcharts/amcharts.js"></script>
                    <script type="text/javascript" src="Modulos/BI/amcharts/pie.js"></script>
                    <script type="text/javascript" src="Modulos/BI/amcharts/serial.js"></script>
                    <script type="text/javascript" src="Modulos/BI/amcharts/themes/light.js"></script>
                    <script type='text/javascript' src='js/util.js'></script>
                    <script type='text/javascript' src="../../js/numeral.min.js"></script>                    
                    <script type="text/javascript">

                        function AbrirBox() {
                            abrirTelaBox("Modulos/BI/FormMetas.php", 425, 650);
                        }
                        
                        function FecharBox(opc) {
                            $("#newbox").remove();
                        }
                        
                        $(document).ready(function () {
                            var chartCores = ["#D16349", "#F0E774", "#92B9C4", "#8FB08C", "#7F7F7F"];
                            var chartData = [<?php foreach ((array)$Status_alunos as $status) {
                                if ($mdl_seg_ == 0 || ($mdl_seg_ > 0 && !in_array($status["name"], ["Inativo", "Cancelado", "Bloqueado"]))) {
                                    if($status["name"] == "AtivoCred"){
                                        echo "{desc: \"Credencial\", valor: " . $status["value"] . "},";
                                    }elseif($status["name"]  == "AtivoEmAberto"){
                                        echo "{desc: \"Ativo Em Aberto\", valor: " . $status["value"] . "},";
                                    }elseif($status["name"]  == "DesligadoEmAberto"){
                                        echo "{desc: \"Desligado Em Aberto\", valor: " . $status["value"] . "},";
                                    }elseif($status["name"]  == "AtivoAbono"){
                                        echo "{desc: \"Ativo Abono\", valor: " . $status["value"] . "},";
                                    }else{
                                        echo "{desc: \"" . $status["name"] . "\", valor: " . $status["value"] . "},";
                                    }
                            }}?>];
                            var chartCores = [<?php foreach ((array)$Status_alunos as $status) {
                                if ($mdl_seg_ == 0 || ($mdl_seg_ > 0 && !in_array($status["name"], ["Inativo", "Cancelado", "Bloqueado"]))) {
                                    if($status["name"]  == "Ativo") {
                                        echo "\"#68AF27\",";
                                    } elseif($status["name"]  == "AtivoCred") {
                                        echo "\"#62d84e\",";
                                    } elseif($status["name"]  == "AtivoEmAberto") {
                                        echo "\"#8b0000\",";
                                    } elseif($status["name"]  == "AtivoAbono") {
                                        echo "\"#8fbc8f\",";
                                    } elseif($status["name"]  == "Excluido") {
                                        echo "\"#999\",";
                                    } elseif($status["name"]  == "Inativo") {
                                        echo "\"#dc5039\",";
                                    } elseif($status["name"]  == "Cancelado") {
                                        echo "\"#005683\",";
                                    } elseif($status["name"]  == "Serasa") {
                                        echo "\"#333\",";
                                    } elseif($status["name"]  == "Bloqueado") {
                                        echo "\"#333\",";
                                    } elseif($status["name"]  == "Dependente") {
                                        echo "\"#01454f\",";
                                    } elseif($status["name"]  == "Suspenso") {
                                        echo "\"#e09f4f\",";
                                    } elseif($status["name"]  == "Desligado") {
                                        echo "\"#c0c0c0\",";
                                    } elseif($status["name"]  == "DesligadoEmAberto") {
                                        echo "\"#696969\",";
                                    } elseif($status["name"]  == "Aguardando") {
                                        echo "\"#f9b785\",";
                                    }
                                }
                            }?>];
                            var chartData2 = [<?php foreach ((array)$Contas_pagar as $conta) {
                                echo "{desc: \"" . $conta["name"] . "\", valor: " . $conta["value"] . "},";
                            }
                            ?>];
                            var chartData3 = [<?php foreach ((array)$Faturamentos as $Faturamento) {
                                echo "{desc: \"" . $Faturamento["name"] . "\", valor: " . $Faturamento["value"] . "},";
                            }
                            ?>];
                            var chart1 = AmCharts.makeChart("chartdiv1", {
                                "type": "pie",
                                "theme": "light",
                                "dataProvider": chartData2,
                                "valueField": "valor",
                                "titleField": "desc",
                                "legend": {
                                    "align": "center",
                                    "position": "bottom",
                                    "markerType": "circle",
                                    "labelText": "[[title]]",
                                    "valueText": "",
                                    "fontSize": 12
                                },
                                "marginTop": 0,
                                "marginBottom": 0,
                                "pullOutRadius": 10,
                                "autoMargins": false,
                                "innerRadius": "50%",
                                "numberFormatter": {"precision": 2, "decimalSeparator": lang["centsSeparator"], "thousandsSeparator": lang["thousandsSeparator"]},                                
                                "labelText": "",
                                "balloonText": "[[title]]: " + lang["prefix"] + " [[value]] ([[percents]]%)",
                                "colors": ["#D16349", "#8FB08C"]
                            });
                            var chart2 = AmCharts.makeChart("chartdiv2", {
                                "type": "pie",
                                "theme": "light",
                                "dataProvider": chartData,
                                "valueField": "valor",
                                "titleField": "desc",
                                "legend": {
                                    "align": "center",
                                    "position": "bottom",
                                    "markerType": "circle",
                                    "labelText": "[[title]]",
                                    "valueText": "",
                                    "fontSize": 12
                                },
                                "marginTop": 0,
                                "marginBottom": 0,
                                "pullOutRadius": 10,
                                "autoMargins": false,
                                "labelText": "",
                                "balloonText": "[[title]]: [[value]] ([[percents]]%)",
                                "colors": chartCores
                            });
                            var chart3 = AmCharts.makeChart("chartdiv3", {
                                "type": "serial",
                                "theme": "light",
                                "dataProvider": chartData3,
                                "graphs": [{
                                        "bullet": "round",
                                        "fillAlphas": 0.5,
                                        "lineThickness": 2,
                                        "bulletColor": "#FFF",
                                        "bulletBorderAlpha": 1,
                                        "bulletBorderThickness": 2,
                                        "useLineColorForBulletBorder": true,
                                        "valueField": "valor"
                                    }],
                                "chartCursor": {"limitToGraph": "g1"},
                                "categoryField": "desc",
                                "numberFormatter": {"precision": 2, "decimalSeparator": lang["centsSeparator"], "thousandsSeparator": lang["thousandsSeparator"]},
                                "colors": ["#0C5D4B"]
                            });
                            var chart4 = AmCharts.makeChart("chartdiv4", {
                                "type": "serial",
                                "theme": "light",
                                "dataProvider": [{"year": "2003", "europe": <?php echo $total; ?>, "namerica": <?php echo ($Meta_alunos - $total > 0 ? $Meta_alunos - $total : 0); ?>, "asia": <?php echo ($Meta_alunos > 0 ? ($Meta_alunos * 1.2 - $Meta_alunos) : 0); ?>}],
                                "valueAxes": [{
                                        "stackType": "100%",
                                        "axisAlpha": 0,
                                        "gridAlpha": 0,
                                        "labelsEnabled": false,
                                        "position": "left"
                                    }],
                                "graphs": [{
                                        "showBalloon": false,
                                        "fillAlphas": 1,
                                        "title": "Europe",
                                        "type": "column",
                                        "fillColors": ["#0C5D4B"],
                                        "lineColor": ["#0C5D4B"],
                                        "valueField": "europe"
                                    }, {
                                        "showBalloon": false,
                                        "fillAlphas": 1,
                                        "title": "North America",
                                        "type": "column",
                                        "fillColors": ["#149B7E"],
                                        "lineColor": ["#149B7E"],
                                        "valueField": "namerica"
                                    }, {
                                        "showBalloon": false,
                                        "fillAlphas": 1,
                                        "title": "Asia-Pacific",
                                        "type": "column",
                                        "fillColors": ["#72C3B1"],
                                        "lineColor": ["#72C3B1"],
                                        "valueField": "asia"
                                    }],                                        
                                "rotate": true,
                                "showBalloon": false,
                                "marginTop": 5,
                                "marginLeft": 10,
                                "marginRight": 10,
                                "marginBottom": 5,
                                "autoMargins": false,
                                "categoryField": "year",                                                              
                                "categoryAxis": {
                                    "gridPosition": "start",
                                    "axisAlpha": 0,
                                    "gridAlpha": 0
                                }
                            });
                            <?php if ($mdl_seg_ > 0 ) { ?>
                            var chart5 = AmCharts.makeChart("chartdiv5", {
                                "type": "serial",
                                "theme": "light",
                                "dataProvider": [{"year": "2003", "europe": <?php echo $veiculos; ?>, "namerica": <?php echo ($Meta_veiculos - $veiculos > 0 ? $Meta_veiculos - $veiculos : 0); ?>, "asia": <?php echo ($Meta_veiculos > 0 ? ($Meta_veiculos * 1.2 - $Meta_veiculos) : 0); ?>}],
                                "valueAxes": [{
                                        "stackType": "100%",
                                        "axisAlpha": 0,
                                        "gridAlpha": 0,
                                        "labelsEnabled": false,
                                        "position": "left"
                                    }],
                                "graphs": [{
                                        "showBalloon": false,
                                        "fillAlphas": 1,
                                        "title": "Europe",
                                        "type": "column",
                                        "fillColors": ["#0C5D4B"],
                                        "lineColor": ["#0C5D4B"],
                                        "valueField": "europe"
                                    }, {
                                        "showBalloon": false,
                                        "fillAlphas": 1,
                                        "title": "North America",
                                        "type": "column",
                                        "fillColors": ["#149B7E"],
                                        "lineColor": ["#149B7E"],
                                        "valueField": "namerica"
                                    }, {
                                        "showBalloon": false,
                                        "fillAlphas": 1,
                                        "title": "Asia-Pacific",
                                        "type": "column",
                                        "fillColors": ["#72C3B1"],
                                        "lineColor": ["#72C3B1"],
                                        "valueField": "asia"
                                    }],                                        
                                "rotate": true,
                                "showBalloon": false,
                                "marginTop": 5,
                                "marginLeft": 10,
                                "marginRight": 10,
                                "marginBottom": 5,
                                "autoMargins": false,
                                "categoryField": "year",                                                              
                                "categoryAxis": {
                                    "gridPosition": "start",
                                    "axisAlpha": 0,
                                    "gridAlpha": 0
                                }
                            });
                            <?php } ?>
                            $("#tablediv1").dataTable({
                                "iDisplayLength": 10,
                                "bProcessing": true,
                                "bServerSide": true,
                                "bFilter": false,
                                "bInfo": false,
                                "bLengthChange": false,
                                "sPaginationType": "full_numbers",
                                "sAjaxSource": "./Modulos/Servicos/Atendimentos_server_processing.php?imp=0&home=S&modAcad=1&tpc=1&zp=4&use=<?php echo $_SESSION["id_usuario"]; ?>",
                                "oLanguage": {
                                    "oPaginate": {
                                        "sFirst": "Primeiro",
                                        "sLast": "Último",
                                        "sNext": "Próximo",
                                        "sPrevious": "Anterior"
                                    },
                                    "sProcessing": "Processando...",
                                    "sLoadingRecords": "Carregando...",
                                    "sEmptyTable": "Não foi encontrado nenhum resultado",
                                    "sZeroRecords": "Não foi encontrado nenhum resultado"
                                }
                            });
                            
                            var chartData2 = [];
                            $.getJSON("./Modulos/BI/BI-Previsao-Financeira-Conta_server_processing.php?sr=0" + 
                                "&filial=" + $("#txtMnFilial").val() + "&ano=" + $("#txt_dt_begin").val(), function (data) {
                                $.each(data, function (key, val) {
                                    chartData2[key] = {mes: val.mes, receber: val.receber, preceber: val.preceber, pagar: val.pagar};
                                });
                            }).done(function () {
                                $.each(chartData2, function (idx) {
                                    $("#tabela_2 thead tr").append("<th>" + chartData2[idx]["mes"] + "</th>");
                                });
                                var conta = [];
                                for (var i = 0; i < 12; i++) {
                                    if ((parseFloat(chartData2[i]["pagar"]) - (parseFloat(chartData2[i]["receber"]) + parseFloat(chartData2[i]["preceber"]))) > 0) {
                                        conta[i] = (parseFloat(chartData2[i]["pagar"]) - (parseFloat(chartData2[i]["receber"]) + parseFloat(chartData2[i]["preceber"])));
                                    } else {
                                        conta[i] = 0.00;
                                    }
                                }
                                var dados_2 = [
                                    ["Contas a Receber", numberFormat(chartData2[0]["receber"], 1), numberFormat(chartData2[1]["receber"], 1), numberFormat(chartData2[2]["receber"], 1), numberFormat(chartData2[3]["receber"], 1), numberFormat(chartData2[4]["receber"], 1), numberFormat(chartData2[5]["receber"], 1), numberFormat(chartData2[6]["receber"], 1), numberFormat(chartData2[7]["receber"], 1), numberFormat(chartData2[8]["receber"], 1), numberFormat(chartData2[9]["receber"], 1), numberFormat(chartData2[10]["receber"], 1), numberFormat(chartData2[11]["receber"], 1)],
                                    ["Previsão a Receber", numberFormat(chartData2[0]["preceber"], 1), numberFormat(chartData2[1]["preceber"], 1), numberFormat(chartData2[2]["preceber"], 1), numberFormat(chartData2[3]["preceber"], 1), numberFormat(chartData2[4]["preceber"], 1), numberFormat(chartData2[5]["preceber"], 1), numberFormat(chartData2[6]["preceber"], 1), numberFormat(chartData2[7]["preceber"], 1), numberFormat(chartData2[8]["preceber"], 1), numberFormat(chartData2[9]["preceber"], 1), numberFormat(chartData2[10]["preceber"], 1), numberFormat(chartData2[11]["preceber"], 1)],
                                    ["Contas a Pagar", numberFormat(chartData2[0]["pagar"], 1), numberFormat(chartData2[1]["pagar"], 1), numberFormat(chartData2[2]["pagar"], 1), numberFormat(chartData2[3]["pagar"], 1), numberFormat(chartData2[4]["pagar"], 1), numberFormat(chartData2[5]["pagar"], 1), numberFormat(chartData2[6]["pagar"], 1), numberFormat(chartData2[7]["pagar"], 1), numberFormat(chartData2[8]["pagar"], 1), numberFormat(chartData2[9]["pagar"], 1), numberFormat(chartData2[10]["pagar"], 1), numberFormat(chartData2[11]["pagar"], 1)],
                                    ["Necessidade de Caixa", numberFormat(conta[0], 1), numberFormat(conta[1], 1), numberFormat(conta[2], 1), numberFormat(conta[3], 1), numberFormat(conta[4], 1), numberFormat(conta[5], 1), numberFormat(conta[6], 1), numberFormat(conta[7], 1), numberFormat(conta[8], 1), numberFormat(conta[9], 1), numberFormat(conta[10], 1), numberFormat(conta[11], 1)]
                                ];
                                $("#tabela_2").DataTable({
                                    aaData: dados_2, bPaginate: false, bFilter: false, bInfo: false, bSort: false,
                                    aoColumnDefs: [{
                                        aTargets: [0], fnCreatedCell: function (nTd, sData, iRow, iCol) {
                                            if (iCol === 0) {
                                                $(nTd).css({"background-color": "#00B050", "color": "#FFFFFF"});
                                            }
                                            if (iCol === 1) {
                                                $(nTd).css({"background-color": "#61b020", "color": "#FFFFFF"});
                                            }
                                            if (iCol === 2) {
                                                $(nTd).css({"background-color": "#CC3333", "color": "#FFFFFF"});
                                            }
                                            if (iCol === 3) {
                                                $(nTd).css({"background-color": "#6699CC", "color": "#FFFFFF"});
                                            }
                                        }
                                    }, {
                                        aTargets: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], fnCreatedCell: function (nTd, sData, iRow, iCol) {
                                            if (iCol === 3) {
                                                var myData = textToNumber(sData);
                                                if (myData > 0.00) {
                                                    $(nTd).css({"background-color": "#FFC7CE", "color": "#9C0006"});
                                                } else if (myData === 0.00) {
                                                    $(nTd).css({"background-color": "#C6EFCE", "color": "#006100"});
                                                }
                                            }
                                        }
                                    }]
                                });
                            });                                                        
                        });
                    </script>
                </div>
            </div>
        </div>
    </body>
    <?php odbc_close($con); ?>
</html>
