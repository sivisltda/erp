<?php

$where = '';
if (isset($filial) && is_numeric($filial)) {
    $whereVenda = "and sf_vendas.empresa = " . $filial;
    $whereLanMov = "and sf_lancamento_movimento.empresa = " . $filial;
    $whereSolAut = "and sf_solicitacao_autorizacao.empresa = " . $filial;
}

$cp_hoje = "set dateformat dmy;
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento 
on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'D' 
AND sf_lancamento_movimento_parcelas.inativo = '0' and sf_lancamento_movimento.status = 'Aprovado' AND valor_pago = 0 " . $whereLanMov . "  
and data_vencimento <= '" . date("d/m/Y") . "'
union
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'D' 
AND sf_solicitacao_autorizacao_parcelas.inativo = '0'  AND valor_pago = 0 and status = 'Aprovado' " . $whereSolAut . " 
and data_parcela <= '" . date("d/m/Y") . "'
union
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_venda_parcelas inner join sf_vendas
on sf_venda_parcelas.venda = sf_vendas.id_venda
inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas where sf_vendas.cov = 'C' 
AND sf_venda_parcelas.inativo = '0'  AND valor_pago = 0 and status = 'Aprovado' " . $whereVenda . "
and data_parcela <= '" . date("d/m/Y") . "'";

$cr_hoje = "set dateformat dmy;
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento 
on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'C' 
AND sf_lancamento_movimento_parcelas.inativo = '0' and sf_lancamento_movimento.status = 'Aprovado' AND valor_pago = 0 " . $whereLanMov . " 
and data_vencimento <= '" . date("d/m/Y") . "'
union
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'C' 
AND sf_solicitacao_autorizacao_parcelas.inativo = '0'  AND valor_pago = 0 and status = 'Aprovado' " . $whereSolAut . " 
and data_parcela <= '" . date("d/m/Y") . "'
union
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_venda_parcelas inner join sf_vendas
on sf_venda_parcelas.venda = sf_vendas.id_venda
inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas where sf_vendas.cov = 'V' 
AND sf_venda_parcelas.inativo = '0'  AND valor_pago = 0 and status = 'Aprovado' " . $whereVenda . "
and data_parcela <= '" . date("d/m/Y") . "'";

$cp_semana = "set dateformat dmy;
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento 
on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'D' 
AND sf_lancamento_movimento_parcelas.inativo = '0' and sf_lancamento_movimento.status = 'Aprovado' AND valor_pago = 0 " . $whereLanMov . " 
and data_vencimento between '" . date("d/m/Y") . "' and '" . date("d/m/Y", strtotime("+7 days")) . "'
union
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'D' 
AND sf_solicitacao_autorizacao_parcelas.inativo = '0'  AND valor_pago = 0 and status = 'Aprovado' " . $whereSolAut . " 
and data_parcela between '" . date("d/m/Y") . "' and '" . date("d/m/Y", strtotime("+7 days")) . "'
union
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_venda_parcelas inner join sf_vendas
on sf_venda_parcelas.venda = sf_vendas.id_venda
inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas where sf_vendas.cov = 'C' 
AND sf_venda_parcelas.inativo = '0'  AND valor_pago = 0 and status = 'Aprovado' " . $whereVenda . "
and data_parcela between '" . date("d/m/Y") . "' and '" . date("d/m/Y", strtotime("+7 days")) . "'";

$cr_semana = "set dateformat dmy;
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento 
on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'C' 
AND sf_lancamento_movimento_parcelas.inativo = '0' and sf_lancamento_movimento.status = 'Aprovado' AND valor_pago = 0  " . $whereLanMov . "
and data_vencimento between '" . date("d/m/Y") . "' and '" . date("d/m/Y", strtotime("+7 days")) . "'
union
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'C' 
AND sf_solicitacao_autorizacao_parcelas.inativo = '0'  AND valor_pago = 0 and status = 'Aprovado' " . $whereSolAut . "  
and data_parcela between '" . date("d/m/Y") . "' and '" . date("d/m/Y", strtotime("+7 days")) . "'
union
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_venda_parcelas inner join sf_vendas
on sf_venda_parcelas.venda = sf_vendas.id_venda
inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas where sf_vendas.cov = 'V' 
AND sf_venda_parcelas.inativo = '0'  AND valor_pago = 0 and status = 'Aprovado' " . $whereVenda . "
and data_parcela between '" . date("d/m/Y") . "' and '" . date("d/m/Y", strtotime("+7 days")) . "'";

$cp_mes = "set dateformat dmy;
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento 
on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'D' 
AND sf_lancamento_movimento_parcelas.inativo = '0' and sf_lancamento_movimento.status = 'Aprovado' AND valor_pago = 0  " . $whereLanMov . "
and data_vencimento between '01/" . date("m/Y") . "' and '" . date("t/m/Y") . "'
union
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'D' 
AND sf_solicitacao_autorizacao_parcelas.inativo = '0'  AND valor_pago = 0 and status = 'Aprovado'  " . $whereSolAut . "
and data_parcela between '01/" . date("m/Y") . "' and '" . date("t/m/Y") . "'
union
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_venda_parcelas inner join sf_vendas
on sf_venda_parcelas.venda = sf_vendas.id_venda
inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas where sf_vendas.cov = 'C' 
AND sf_venda_parcelas.inativo = '0'  AND valor_pago = 0 and status = 'Aprovado' " . $whereVenda . "
and data_parcela between '01/" . date("m/Y") . "' and '" . date("t/m/Y") . "'";

$cr_mes = "set dateformat dmy;
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento 
on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'C' 
AND sf_lancamento_movimento_parcelas.inativo = '0' and sf_lancamento_movimento.status = 'Aprovado' AND valor_pago = 0  " . $whereLanMov . "
and data_vencimento between '01/" . date("m/Y") . "' and '" . date("t/m/Y") . "'
union
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'C' 
AND sf_solicitacao_autorizacao_parcelas.inativo = '0'  AND valor_pago = 0 and status = 'Aprovado'  " . $whereSolAut . "
and data_parcela between '01/" . date("m/Y") . "' and '" . date("t/m/Y") . "'
union
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_venda_parcelas inner join sf_vendas
on sf_venda_parcelas.venda = sf_vendas.id_venda
inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas where sf_vendas.cov = 'V' 
AND sf_venda_parcelas.inativo = '0'  AND valor_pago = 0 and status = 'Aprovado' " . $whereVenda . "
and data_parcela between '01/" . date("m/Y") . "' and '" . date("t/m/Y") . "'";

$cr_total = "set dateformat dmy;
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento 
on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'C' 
AND sf_lancamento_movimento_parcelas.inativo = '0' and sf_lancamento_movimento.status = 'Aprovado' AND valor_pago = 0  " . $whereLanMov . "
union
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'C' 
and status = 'Aprovado' AND sf_solicitacao_autorizacao_parcelas.inativo = '0'  AND valor_pago = 0  " . $whereSolAut . "
union
select count(valor_parcela) quantidade ,isnull(SUM(valor_parcela),0) total
from sf_venda_parcelas inner join sf_vendas
on sf_venda_parcelas.venda = sf_vendas.id_venda
inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas where sf_vendas.cov = 'V' 
AND sf_venda_parcelas.inativo = '0'  AND valor_pago = 0 and status = 'Aprovado' " . $whereVenda;

function Quantidade($SQL, $con) {
    $toReturn = 0;
    $cur = odbc_exec($con, $SQL);
    while ($RFP = odbc_fetch_array($cur)) {
        $toReturn = $toReturn + $RFP['quantidade'];
    }
    return $toReturn;
}

function Total($SQL, $con) {
    $toReturn = 0;
    $cur = odbc_exec($con, $SQL);
    while ($RFP = odbc_fetch_array($cur)) {
        $toReturn = $toReturn + $RFP['total'];
    }
    return $toReturn;
}
