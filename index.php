<?php
include "Connections/configini.php";

if (isset($_POST["bntSave"])) {
    if (utf8_decode($_POST["txt_notas"]) != "") {
        odbc_exec($con, "UPDATE sf_usuarios SET bloco_notas = " . valoresTexto('txt_notas') . " WHERE id_usuario = '" . $_SESSION["id_usuario"] . "'");
        echo "YES";
    } else {
        echo "Preencha o Bloco de Notas";
    }
    exit;
}
if (isset($_GET['Delx']) && is_numeric($_GET['Delx'])) {
    odbc_exec($con, "DELETE FROM sf_telemarketing_item WHERE id_telemarketing = " . $_GET['Delx']);
    odbc_exec($con, "DELETE FROM sf_mensagens_telemarketing WHERE id_tele = " . $_GET['Delx']);
    odbc_exec($con, "DELETE FROM sf_telemarketing WHERE id_tele = " . $_GET['Delx']);
    echo "<script>window.top.location.href = 'index.php'; </script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="favicon.ico"/>
        <link href="css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="css/main.css" rel="stylesheet" type="text/css"/>
        <link href="css/styleLeo.css" rel="stylesheet" type="text/css"/>
        <link href='css/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
        <link rel="stylesheet" type="text/css" href="../SpryAssets/SpryValidationTextField.css"/>
        <link href="css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <style>
            body, .body { background: #F7F7F7; }
            .indcont {
                float: left;
                background: #FFF;
                margin-bottom: 10px;
                border: 1px solid #E6E9ED;
            }
            .indhead {
                color: #006568;
                font-size: 18px;
                background: #FFF;
                margin-left: 15px;
                line-height: 40px;
                text-align: center;
                width: calc(100% - 30px);
                border-bottom: 2px solid #E6E9ED;
            }
            .indicon { padding: 4% 5%; }
            .indfoot { padding: 10px 10px 0; }
            .indlist {
                padding: 5px 0;
                border-bottom: 1px solid #EEE;
            }
            .indlist img {
                float: left;
                width: 38px;
                height: 38px;
                border-radius: 50%;
                border: 1px solid #DDD;
            }
            .indlist .txtname {
                float: left;
                color: #666;
                font-size: 14px;
                margin-left: 10px;
                line-height: 18px;
                margin-bottom: 2px;
                width: calc(100% - 50px);
            }
            .indlist .txtpend {
                color: #FFF;
                float: right;
                font-size: 10px;
                padding: 2px 5px;
                line-height: 16px;
                border-radius: 3px;
                background: #0C5D4B;
                margin: 0;
            }
            #tablediv1 {
                border-spacing: 0;
                border-collapse: collapse;
            }
            #tablediv1 a { color: var(--cor-secundaria); }
            #tablediv1 th {
                color: var(--cor-secundaria);
                padding: 0 5px;
                background: #EDEDED;
            }
            #tablediv1 td {
                color: #73879C;
                padding: 5px;
            }
            #tablediv1 td span {
                font-size: 15px;
            }
            #tablediv1 tr.odd td {
                background: #FFF;
            }
            #tablediv1 tr.even td {
                background: #F9F9F9;
            }
            .box-25, .box-50 {
                margin: 1%;
                float: left;
                color: #F7F7F7;
                cursor: pointer;
                background: var(--cor-secundaria);
            }
            .box-25:hover, .box-50:hover { opacity: 0.9; }
            .box-50 { width: 48%; }
            .box-25 { width: 23%; }
            .box-icon {
                line-height: 1;
                font-size: 100px;
                text-align: center;
            }
            .box-text {
                line-height: 1;
                font-size: 20px;
                text-align: center;
            }
            .box-note {
                resize: none;
                margin: 5px 0;
                background: #FCFCFC;
                border: 2px solid #EEE;
                height: calc(100% - 10px);
            }
            .verAtivas{
                margin:20px 0;
            }
            @media (min-width: 1000px) and (max-width: 1199px) {
                .indicon { padding: 2% 3%; }
                .ic-head { font-size: 28px; }
                .box-text { font-size: 10px; }
                .box-icon { font-size: 50px; }
                .verAtivas{ margin: 4px 0; }
            }
            @media (min-width: 1200px) and (max-width: 1399px) {
                .indicon { padding: 2% 3%; }
                .ic-head { font-size: 20px; }
                .box-text { font-size: 12px; }
                .box-icon { font-size: 60px; }
            }
            @media (min-width: 1400px) and (max-width: 1599px) {
                .indicon { padding: 3% 4%; }
                .ic-head { font-size: 20px; }
                .box-text { font-size: 12px; }
                .box-icon { font-size: 70px; }
            }
            @media (min-width: 1600px) and (max-width: 1799px) {
                .indicon { padding: 3% 4%; }
                .ic-head { font-size: 25px; }
                .box-text { font-size: 12px; }
                .box-icon { font-size: 80px; }
            }
            @media (min-width: 1800px) and (max-width: 1999px) {
                .indicon { padding: 4% 5%; }
                .ic-head { font-size: 30px; }
                .box-text { font-size: 10px; }
                .box-icon { font-size: 90px; }
            }
            h7{
                font-weight: bold;
                margin-top: 5px;
            }
            .subHeader{
                width: 100%;
                height: 50px;
                clear: both;
                margin: 0 -20px;
                background-color: #e4e4e4;
                padding-right: 40px;
                display: flex;
                align-items: center;
                flex-direction: row-reverse;
                margin-top: -10px;
            }
            .subHeader div{
                padding: 10px;
                width: 100px;
                height: 8px;
                display: flex;
                align-items: center;
                color: #515356;
                text-align: center;
                align-items: center;
            }
            .subHeader .ic-head{
                width: 18px!important;
            }
            .subHeader a{
                color: #515356;
                width: 40px;
                display: block;
            }
            .subHeader .ajustMenu{
                display: flex;
                flex-direction: row;
                cursor: pointer;
            }
            .past{
                text-decoration: line-through;
            }           
        </style>
    </head>
    <body>
        <div id="loader"><img src="img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("menuLateral.php"); ?>
            <div class="body">
                <?php
                include("top.php");
                include("querys_finance.php");
                ?>
                <div class="content">
                    <?php if ($_SESSION["mod_emp"] == 1) { ?>
                        <div class="subHeader">
                            <?php if ($ckb_fin_rec_ == 1) { ?>
                                <div class="ajustMenu" onClick="window.top.location.href = 'Modulos/Contas-a-pagar/Contas-a-Receber.php?id=1&tp=0'">
                                    <a href="#">
                                        <div class="ico-download-alt ic-head" data-tooltip="A RECEBER"></div>
                                    </a>
                                    <span>A Receber</span>
                                </div>
                            <?php } if ($ckb_crm_tel_ == 1) { ?>
                                <div class="ajustMenu" onClick="window.top.location.href = 'Modulos/CRM/Telemarketing.php'">
                                    <a href="#">
                                        <div class="ico-folder-close ic-head" data-tooltip="TELEMARKETING"></div>
                                    </a>
                                    <span>Telemarketing</span>
                                </div>
                            <?php } if ($ckb_est_lven_ == 1 && $_SESSION["mod_emp"] != 1) { ?>
                                <div class="ajustMenu" onClick="window.top.location.href = 'Modulos/Estoque/Vendas.php?id=1'">
                                    <a href="#">
                                        <div class="ico-tags ic-head" data-tooltip="VENDAS"></div>
                                    </a>
                                    <span>Vendas</span>
                                </div>
                            <?php } if ($ckb_adm_cli_ == 1) { ?>
                                <div class="ajustMenu" onClick="window.top.location.href = 'Modulos/Comercial/Gerenciador-Clientes.php'">
                                    <a href="#">
                                        <div class="ico-user ic-head" data-tooltip="CLIENTES"></div>
                                    </a>
                                    <span>Clientes</span>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <div class="page-header" style="display:flex;margin: 0;padding: 0;padding: 10px 0;justify-content: space-between;">
                        <div class="" style="width:40%;">
                            <div class="icon"><span class="ico-arrow-right"></span></div>
                            <h3>Página Principal<small>Resumo dos Módulos</small></h3>
                        </div>
                        <span style="float:right;width:27%;">
                            <?php if ($mdl_aca_ > 0 && $ckb_aca_inf_gerenciais_) { ?>
                                <button class="button btnajust" style="margin-left: 2%;width:53%;" type="button" onClick="window.top.location.href = 'Modulos/Academia/BI-InformacoesGerenciais.php'">Informações Gerenciais</button>
                            <?php } ?>
                            <?php if ($ckb_out_pag_avancada_ > 0 && $_SESSION["mod_emp"] != 1) { ?>
                                <button class="button btnajust" style="width:45%;" type="button" onClick="window.top.location.href = 'index2.php'">Página Avançada</button>
                            <?php } ?>
                        </span>
                    </div>
                    <div style="clear:both"></div>
                    <div style="margin-top:10px">
                        <div class="indcont" style="width:calc(100% - 2px)">
                            <div class="indhead">Minhas Pendências</div>
                            <div class="indfoot" style="height:320px">
                                <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tablediv1">
                                    <thead>
                                        <tr>
                                            <th><center>Data/Hora</center></th>
                                            <th>Cliente</th>
                                            <th>Origem</th>
                                            <th>Tipo</th>
                                            <th>Atendente</th>
                                            <th><center>Pendente</center></th>
                                            <th>Encaminhado</th>
                                            <th><center>Prazo</center></th>
                                            <th><center>Periodo para Cont.</center></th>
                                            <th><center>Ações</center></th>
                                        </tr>
                                    </thead>
                                </table>
                                <div style="clear:both"></div>
                            </div>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <?php
                    if ($_SESSION["mod_emp"] != 1) {
                        include("calendarioHome.php");
                    }
                    if ($_SESSION["mod_emp"] == 1) { ?>
                        <div class="indcont" style="width:calc(100% - 2px);height: 300px;">
                            <form action="index.php" method="POST">
                                <div class="indhead">Bloco de Notas
                                    <span style="float:right">
                                        <button class="button" style="border:0; background: var(--cor-secundaria);" type="submit" name="bntSave">
                                            <span class="ico-ok icon-white"></span>
                                        </button>
                                    </span>
                                </div>
                                <div id="notas2" class="indicon" style="height: 190px!important">
                                    <textarea name="txt_notas" class="box-note" rows="300"><?php
                                        $cur = odbc_exec($con, "select bloco_notas from sf_usuarios where id_usuario = '" . $_SESSION["id_usuario"] . "'");
                                        while ($RFP = odbc_fetch_array($cur)) {
                                            echo utf8_encode($RFP["bloco_notas"]);
                                        }
                                        ?></textarea>
                                </div>
                            </form>
                        </div>
                    <?php } ?>
                    <div class="dialog" id="source" title="Source"></div>
                    <script type='text/javascript' src="js/plugins/select/select2.min.js"></script>
                    <script type="text/javascript" src="js/justgage.1.0.1.min.js"></script>
                    <script type="text/javascript" src="js/moment.min.js"></script>
                    <script type="text/javascript" src="js/fullcalendar/moment.min.js"></script>
                    <script type="text/javascript" src="js/fullcalendar/fullcalendar.min.js"></script>
                    <script type="text/javascript" src="js/fullcalendar/locale-all.js"></script>
                    <script type='text/javascript' src='js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
                    <script type='text/javascript' src='js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
                    <script type='text/javascript' src='js/plugins/jquery/globalize.js'></script>
                    <script type='text/javascript' src='js/plugins/other/jquery.mousewheel.min.js'></script>
                    <script type='text/javascript' src='js/plugins/bootstrap/bootstrap.min.js'></script>
                    <script type='text/javascript' src='js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
                    <script type='text/javascript' src='js/plugins/sparklines/jquery.sparkline.min.js'></script>
                    <script type='text/javascript' src='js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
                    <script type='text/javascript' src="js/plugins/uniform/jquery.uniform.min.js"></script>
                    <script type='text/javascript' src='js/plugins.js'></script>
                    <script type='text/javascript' src='js/actions.js'></script>
                    <script type='text/javascript' src='js/plugins/datatables/jquery.dataTables.min.js'></script>
                    <script type='text/javascript' src='js/util.js'></script>
                    <script type="text/javascript" src="js/plugins/bootbox/bootbox.js"></script>
                    <script type="text/javascript">

                        $(document).ready(function () {
                            myResize();
                            $(window).resize(function () {
                                myResize();
                            });
                            $("#tablediv1").dataTable({
                                "iDisplayLength": 10,
                                "bProcessing": true,
                                "bServerSide": true,
                                "bFilter": false,
                                "bInfo": false,
                                "bLengthChange": false,
                                "sPaginationType": "full_numbers",
                                "sAjaxSource": "./Modulos/Servicos/Atendimentos_server_processing.php?imp=0&home=S&modAcad=<?php echo $mdl_aca_; ?>&tpc=1&zp=4&use=<?php echo $_SESSION["id_usuario"]; ?>",
                                "oLanguage": {
                                    "oPaginate": {
                                        "sFirst": "Primeiro",
                                        "sLast": "Último",
                                        "sNext": "Próximo",
                                        "sPrevious": "Anterior"
                                    },
                                    "sProcessing": "Processando...",
                                    "sLoadingRecords": "Carregando...",
                                    "sEmptyTable": "Não foi encontrado nenhum resultado",
                                    "sZeroRecords": "Não foi encontrado nenhum resultado"
                                }
                            });

                            $('#calendar').fullCalendar({
                                header: {left: 'prev,next today', center: 'title', right: 'month,agendaWeek,agendaDay,listMonth'},
                                defaultDate: $.datepicker.formatDate('yy-mm-dd', new Date()),
                                locale: 'pt-br',
                                navLinks: true,
                                editable: true,
                                eventLimit: true,
                                events: {
                                    url: "./Modulos/Comercial/ajax/agenda_server_processing.php",
                                    data: function () {
                                        return {id: $("#txtAgenda").val(), tipo: $("#txtTipo").val()};
                                    }
                                },
                                eventRender: function (calev, elt, view) {
                                    if (calev.completed === "1") {
                                        elt.addClass("past");
                                        elt.children().addClass("past");
                                    }
                                    if (calev.pg_pendente === "1") {
                                        elt.addClass("yellow");
                                        elt.children().addClass("yellow");
                                    }
                                    elt.attr("title" , calev.description);
                                },
                                eventClick: function (calEvent, jsEvent, view) {
                                    AbrirBoxF(calEvent.id, '');
                                },
                                dayClick: function (date, jsEvent, view) {
                                    AbrirBoxF(0, date.format());
                                },
                                eventAfterAllRender: function( view ) { 
                                    if($(".fc-day-grid-container div").height() !== null) {
                                        $("#bx-02").css('min-height', ($(".fc-day-grid-container div").height() + 20) + 'px');
                                    }
                                }                                
                            });

                            $.each($.fullCalendar.locales, function (localeCode) {
                                $('#locale-selector').append($('<option/>').attr('value', localeCode).prop('selected', localeCode === 'pt-br').text(localeCode));
                            });

                            $("#txtTipo").select2("val", 0);
                            $('#txtTipo').change(function () {
                                if ($(this).val()) {
                                    listaMenu($(this).val());
                                }
                            });
                            listaMenu(0);
                        });                                               
                        
                        $('#bntSave').click(function () {
                            var dados = {
                                bntSave: 'S',
                                txt_notas: $("#txt_notas").val()
                            };
                            $.post("index.php", dados).done(function (data) {
                                if (data.trim() === "YES") {
                                    bootbox.alert("Salvo com Sucesso!");
                                } else {
                                    bootbox.alert(data + "!");
                                }           
                            });
                        });
                        
                        function printCalendar() {
                            var pRel = "&NomeArq=" + "Agendas" +
                            "&lbl=Código|Data Início|Data Fim|Observações - Cliente / Prospect" +
                            "&siz=60|100|100|440" +
                            "&pdf=6" + // Colunas do server processing que não irão aparecer no pdf
                            "&filter=" + //Label que irá aparecer os parametros de filtro
                            "&PathArqInclude=" + "./../Modulos/Comercial/ajax/agenda_server_processing.php" +
                            "&id=" + $("#txtAgenda").val() + "&tipo=" + $("#txtTipo").val() + 
                            "&start=" + $('#calendar').fullCalendar("getView").start.format() + 
                            "&end=" + $('#calendar').fullCalendar("getView").end.format();                                    
                            window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
                        }

                        function myResize() {
                            $(".box-25, .box-50").height($(".box-25").width() + "px");
                            $("#notas").height($("#icobox").outerWidth(true) * 2 + "px");
                            $(".box-icon").css("margin-top", (($(".box-25").width() - $("#icocnt").height()) / 2) - 3 + "px");
                            $("#bx-02").css('min-height', ($(".fc-day-grid-container div").height() + 20) + 'px');
                        }

                        function AbrirBox(id, gb, op) {
                            if (id === 0) {
                                $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:667px; height:500px; position:absolute; top:50%; left:50%; margin-left:-333px; margin-top:-250px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:500px' src='./Modulos/CRM/FormTelemarketing.php?gb=" + gb + "&jn=i'></iframe></div></div></div></div></div>");
                            } else if (op === 0) {
                                $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:667px; height:500px; position:absolute; top:50%; left:50%; margin-left:-333px; margin-top:-250px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:500px' src='./Modulos/CRM/FormTelemarketing.php?id=" + id + "&gb=" + gb + "&jn=i'></iframe></div></div></div></div></div>");
                            } else if (op === 1) {
                                $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:667px; height:500px; position:absolute; top:50%; left:50%; margin-left:-333px; margin-top:-250px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:500px' src='./Modulos/CRM/FormTelemarketing.php?id=" + id + "&gb=" + gb + "&jn=i&fx=s'></iframe></div></div></div></div></div>");
                            } else {
                                $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:667px; height:500px; position:absolute; top:50%; left:50%; margin-left:-333px; margin-top:-250px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:500px' src='./Modulos/CRM/FormTelemarketing.php?id=" + id + "&gb=" + gb + "&jn=i&fx=t'></iframe></div></div></div></div></div>");
                            }
                        }

                        function FecharBox() {
                            var oTable = $("#tablediv1").dataTable();
                            oTable.fnDraw();
                            $("#newbox").remove();
                        }

                        $(".ajustMenu").hover(
                                function () {
                                    $(this).find('a').css("text-decoration", "underline");
                                }, function () {
                            $(this).find('a').css("text-decoration", "none");
                        });

                        function AbrirBoxF(id, data) {
                            if ($("#txtAgenda").val() !== "") {
                                if ($("#txtTipo").val() === "0") {
                                    abrirTelaBox("./Modulos/Comercial/FormAgenda.php" + (id > 0 ? "?id=" + id : "") + (data !== "" ? "?dt=" + data : ""), 465, 500);
                                }
                            } else {
                                bootbox.alert("Selecione um Item!");
                            }
                        }

                        function FecharBoxF() {
                            $("#newbox").remove();
                            $('#calendar').fullCalendar('refetchEvents');
                        }

                        function listaMenu(tipo) {
                            $.getJSON('./Modulos/Comercial/tipo.agenda.ajax.php', {txtTipo: tipo, ajax: 'true'}, function (j) {
                                var options = '';
                                var top = '';
                                for (var i = 0; i < j.length; i++) {
                                    if (i === 0) {
                                        top = j[i].id;
                                    }
                                    options += '<tr><td id=\"td_' + j[i].id + '\" style=\"color: #333;background: #F9F9F9;cursor: pointer;\" onclick=\"seleciona(' + j[i].id + ');\">' + j[i].descricao + '</td></tr>';
                                }
                                $('#tb_agenda').html(options).show();
                                seleciona(top);
                            });
                        }

                        function seleciona(id) {
                            $('#tb_agenda td').css('color', '#333;');
                            $('#tb_agenda td').css('background', '#F9F9F9');
                            $('#td_' + id).css('color', '#fff;');
                            $('#td_' + id).css('background', 'var(--cor-primaria)');
                            $("#txtAgenda").val(id);
                            FecharBoxF();
                        }

                        function alterar() {
                            if ($("#txtTipo").val() === "0") {
                                window.location = './Modulos/Comercial/Gerenciador-Agenda.php';
                            } else if ($("#txtTipo").val() === "1") {
                                window.location = './Modulos/Academia/TurmasList.php';
                            }
                        }

                    <?php if ($_SESSION["inativa"] == 2) { ?>
                        var mensagem = "<center><h5>Atenção</h5> Vimos por meio deste informar que o seu sistema será <span style=\"background-color:red;\">BLOQUEADO</span> nos próximos dias, " +
                                "favor entrar em contato URGENTE com a SIVIS TECNOLOGIA para saber o motivo e evitar " +
                                "suspensão.<br>" +
                                "Desculpe-nos o transtorno.<br><br>" +
                                "SIVIS TECNOLOGIA<br>" +
                                "24 3348-5953<br>" +
                                "vipservice@sivis.com.br<br>" +
                                "</center>";
                        bootbox.alert(mensagem);
                    <?php } ?>
                    </script>
                </div>
            </div>
        </div>
    </body>
<?php odbc_close($con); ?>
</html>
