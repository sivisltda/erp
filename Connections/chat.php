<?php
session_start();

if ($_GET['action'] == "chatheartbeat") {
    chatHeartbeat();
}
if ($_GET['action'] == "sendchat") {
    sendChat();
}
if ($_GET['action'] == "closechat") {
    closeChat();
}
if ($_GET['action'] == "startchatsession") {
    startChatSession();
}

if (!isset($_SESSION['chatHistory'])) {
    $_SESSION['chatHistory'] = array();
}

if (!isset($_SESSION['openChatBoxes'])) {
    $_SESSION['openChatBoxes'] = array();
}

function chatHeartbeat() {
    $hostname = $_SESSION["hostname"];
    $username = $_SESSION["username"];
    $password = $_SESSION["password"];
    $database = $_SESSION["database"];
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
    $sql = "select id,REPLACE(u1.login_user,'.','_') 'from',REPLACE(u2.login_user,'.','_') 'to',data sent,mensagem message,lido recd 
            from sf_mensagens_usuarios m inner join sf_usuarios u1 on m.id_usuario_de = u1.id_usuario
            inner join sf_usuarios u2 on m.id_usuario_para = u2.id_usuario where (id_usuario_para = " . $_SESSION["id_usuario"] . " AND lido = 0) order by id ASC";
    $query = odbc_exec($con, $sql) or die(odbc_errormsg());
    $items = '';
    while ($chat = odbc_fetch_array($query)) {
        if (!isset($_SESSION['openChatBoxes'][$chat['from']]) && isset($_SESSION['chatHistory'][$chat['from']])) {
            $items = $_SESSION['chatHistory'][$chat['from']];
        }
        $chat['message'] = sanitize(utf8_encode($chat['message']));
        $items .= <<<EOD
					   {
			"s": "0",
			"f": "{$chat['from']}",
			"m": "{$chat['message']}"
	   },
EOD;
        if (!isset($_SESSION['chatHistory'][$chat['from']])) {
            $_SESSION['chatHistory'][$chat['from']] = '';
        }
        $_SESSION['chatHistory'][$chat['from']] .= <<<EOD
						   {
			"s": "0",
			"f": "{$chat['from']}",
			"m": "{$chat['message']}"
	   },
EOD;

        unset($_SESSION['tsChatBoxes'][$chat['from']]);
        $_SESSION['openChatBoxes'][$chat['from']] = $chat['sent'];
    }
    if (!empty($_SESSION['openChatBoxes'])) {
        foreach ($_SESSION['openChatBoxes'] as $chatbox => $time) {
            if (!isset($_SESSION['tsChatBoxes'][$chatbox])) {
                $now = time() - strtotime($time);
                $time = date('g:iA M dS', strtotime($time));
                $message = "Enviado $time";
                if ($now > 180) {
                    $items .= <<<EOD
{
"s": "2",
"f": "$chatbox",
"m": "{$message}"
},
EOD;
                    if (!isset($_SESSION['chatHistory'][$chatbox])) {
                        $_SESSION['chatHistory'][$chatbox] = '';
                    }
                    $_SESSION['chatHistory'][$chatbox] .= <<<EOD
		{
"s": "2",
"f": "$chatbox",
"m": "{$message}"
},
EOD;
                    $_SESSION['tsChatBoxes'][$chatbox] = 1;
                }
            }
        }
    }

    $sql = "update sf_mensagens_usuarios set lido = 1 where id_usuario_para = '" . $_SESSION["id_usuario"] . "' and lido = 0;
            update sf_usuarios set ultimo_acesso = GETDATE() where id_usuario = '" . $_SESSION["id_usuario"] . "'";
    $query = odbc_exec($con, $sql);
    if ($items != '') {
        $items = substr($items, 0, -1);
    }
    header('Content-type: application/json');
    ?>
    {
    "items": [
    <?php echo $items; ?>
    ]
    }
    <?php
    odbc_close($con);
    exit(0);
}

function chatBoxSession($chatbox) {
    $items = '';
    if (isset($_SESSION['chatHistory'][$chatbox])) {
        $items = $_SESSION['chatHistory'][$chatbox];
    }
    return $items;
}

function startChatSession() {
    $items = '';
    if (!empty($_SESSION['openChatBoxes'])) {
        foreach ($_SESSION['openChatBoxes'] as $chatbox => $void) {
            $items .= chatBoxSession($chatbox);
        }
    }

    if ($items != '') {
        $items = substr($items, 0, -1);
    }

    header('Content-type: application/json');
    ?>
    {
    "username": "<?php echo $_SESSION['login_usuario']; ?>",
    "items": [
    <?php echo $items; ?>
    ]
    }
    <?php
    exit(0);
}

function sendChat() {
    $hostname = $_SESSION["hostname"];
    $username = $_SESSION["username"];
    $password = $_SESSION["password"];
    $database = $_SESSION["database"];
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
    $from = $_SESSION["id_usuario"];
    $to = $_POST['to'];
    $message = $_POST['message'];
    $_SESSION['openChatBoxes'][$_POST['to']] = date('Y-m-d H:i:s', time());
    $messagesan = sanitize($message);
    if (!isset($_SESSION['chatHistory'][$_POST['to']])) {
        $_SESSION['chatHistory'][$_POST['to']] = '';
    }
    $_SESSION['chatHistory'][$_POST['to']] .= <<<EOD
					   {
			"s": "1",
			"f": "{$to}",
			"m": "{$messagesan}"
	   },
EOD;
    unset($_SESSION['tsChatBoxes'][$_POST['to']]);
    $sql = "select id_usuario from sf_usuarios where login_user = '" . str_replace("_", ".", $to) . "'";
    $query = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($chat = odbc_fetch_array($query)) {
        $toPeople = $chat['id_usuario'];
    }
    $sql = "insert into sf_mensagens_usuarios(id_usuario_de,id_usuario_para,mensagem,data,lido) values (" . $from . ", " . $toPeople . ",'" . utf8_decode(str_replace("'", "", $message)) . "',getDate(),0)";
    $query = odbc_exec($con, $sql);
    odbc_close($con);
    exit(0);
}

function closeChat() {
    unset($_SESSION['openChatBoxes'][$_POST['chatbox']]);
    echo "1";
    exit(0);
}

function sanitize($text) {
    $text = htmlspecialchars($text, ENT_QUOTES);
    $text = str_replace("\n\r", "\n", $text);
    $text = str_replace("\r\n", "\n", $text);
    $text = str_replace("\n", "<br>", $text);
    return $text;
}
