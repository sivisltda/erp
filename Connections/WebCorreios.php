<?php

require_once('../util/phpQuery-onefile.php');

$cep = $_GET['cep'];
simple_curl($cep);

function simple_curl($cep) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://viacep.com.br/ws/');
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $cep . '/json/');
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    $res = curl_exec($ch);
    curl_close($ch);
    phpQuery::newDocumentHTML($res, $charset = 'utf-8');
    $dados = array(
        'logradouro' => utf8_encode(trim(pq('.caixacampobranco .resposta:contains("Logradouro: ") + .respostadestaque:eq(0)')->html())),
        'bairro' => utf8_encode(trim(pq('.caixacampobranco .resposta:contains("Bairro: ") + .respostadestaque:eq(0)')->html())),
        'cidade/uf' => trim(pq('.caixacampobranco .resposta:contains("Localidade / UF: ") + .respostadestaque:eq(0)')->html()),
        'cep' => trim(pq('.caixacampobranco .resposta:contains("CEP: ") + .respostadestaque:eq(0)')->html())
    );

    $dados['cidade/uf'] = explode('/', $dados['cidade/uf']);
    $dados['cidade'] = utf8_encode(trim($dados['cidade/uf'][0]));
    $dados['estado'] = trim($dados['cidade/uf'][1]);
    unset($dados['cidade/uf']);
    die(json_encode($dados));
}
