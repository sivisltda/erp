<?php

function valoresTexto($nome) {
    return valoresTexto2($_POST[$nome]);
}

function valoresTextoUpper($nome) {
    return strtoupper(valoresTexto2($_POST[$nome]));
}

function valoresCheck($nome) {
    return valoresCheck2($_POST[$nome]);
}

function valoresSelect($nome) {
    return valoresSelect2($_POST[$nome]);
}

function valoresSelectTexto($nome) {
    return valoresSelectTexto2($_POST[$nome]);
}

function valoresNumericos($nome) {
    return valoresNumericos2($_POST[$nome]);
}

function valoresData($nome) {
    return valoresData2($_POST[$nome]);
}

function valoresHora($nome) {
    return valoresHora2($_POST[$nome]);
}

function valoresDataHora($data, $hora) {
    return valoresDataHora2($_POST[$data], $_POST[$hora]);
}

function valoresDataHoraUnico($datahora) {
    return valoresDataHoraUnico2($_REQUEST[$datahora]);
}

//------------------------------------------------Funções-Idiomas----------------------------------------
$languages = isset($languages)  ? $languages : "pt";
require_once(__DIR__ . "./../Languages/" . $languages . "/lang." . $languages . ".php");

//------------------------------------------------Funções-Processamento-----------------------------------------
function valoresTexto2($nome) {
    return "'" . utf8_decode(str_replace("'", "", $nome)) . "'";
}

function valoresCheck2($nome) {
    if ($nome == "1") {
        return 1;
    } else {
        return 0;
    }
}

function valoresSelect2($nome) {
    if ($nome != "") {
        return $nome;
    } else {
        return "null";
    }
}

function valoresSelectTexto2($nome) {
    if ($nome != "") {
        return "'" . $nome . "'";
    } else {
        return "null";
    }
}

function valoresHora2($nome) {
    $explode = explode(":", $nome);
    if (count($explode) == 2 || count($explode) == 3) {
        if (is_numeric($explode[0]) && $explode[0] >= 0 && $explode[0] < 24 &&
        is_numeric($explode[1]) && $explode[1] >= 0 && $explode[1] < 60 &&
        (count($explode) == 2 || (count($explode) == 3 && $explode[2] >= 0 && $explode[2] < 60))) {
            return "'" . $nome . "'";
        } else {
            return "null";
        }
    } else {
        return "null";
    }
}

function valoresDataHora2($data, $hora) {
    if (valoresData2($data) != "null" && valoresHora2($hora) != "null") {
        return str_replace("''", " ", valoresData2($data) . valoresHora2($hora));
    } elseif (valoresData2($data) != "null") {
        return valoresData2($data);
    } else {
        return "null";
    }
}

function valoresDataHoraUnico2($datahora) {
    $explode = explode(" ", str_replace("_", "/", $datahora));
    if (count($explode) == 2) {
        return valoresDataHora2($explode[0], $explode[1]);
    } else {
        return "null";
    }
}

//------------------------------------------------Funções-Gerais-----------------------------------------
function returnPart($nome, $posicao) {
    if (is_numeric(substr($nome, $posicao, 1))) {
        return substr($nome, $posicao, 1);
    } else {
        return "1";
    }
}

function deleteDirectory($dir) {
    if (!file_exists($dir)) {
        return true;
    }
    if (!is_dir($dir)) {
        return unlink($dir);
    }
    foreach (scandir($dir) as $item) {
        if ($item == '.' || $item == '..') {
            continue;
        }
        if (!deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
            return false;
        }
    }
    return rmdir($dir);
}

function dataDiff($data1, $data2, $intervalo) {
    switch ($intervalo) {
        case 'y':
            $Q = 86400 * 365;
            break; //ano
        case 'm':
            $Q = 2592000;
            break; //mes
        case 'd':
            $Q = 86400;
            break; //dia
        case 'h':
            $Q = 3600;
            break; //hora
        case 'n':
            $Q = 60;
            break; //minuto
        default:
            $Q = 1;
            break; //segundo
    }
    return round((strtotime($data2) - strtotime($data1)) / $Q);
}

function prepareCollum($valor, $tipo) {
    if (strripos($valor, " ")) {
        $result = explode(" ", str_replace(" as ", " ", $valor));
        return $result[$tipo];
    } else {
        return $valor;
    }
}

function getCollor($i) {
    switch ($i) {
        case 0:
            return "#68AF27"; //verde
        case 1:
            return "#08c"; //Azul
        case 2:
            return "#333"; //preto
        case 3:
            return "#dc5039"; //vermelho
        case 4:
            return "#e09f4f"; //laranja
        case 5:
            return "#fdd400"; //verde claro
        case 6:
            return "#722672"; //roxo
        case 7:
            return "#996632"; //marrom
        case 8:
            return "#666666"; //cinza
        default:
            return "#333"; //preto                
    }
}

function getColorStatusAl($status) {
    if ($status == "Ativo" || $status == "Ativos") {
        return "#68AF27";
    }if ($status == "Inativo") {
        return "#dc5039";
    }if ($status == "Bolsistas" || $status == "AtivoCred") {
        return "#fdd400";
    }if ($status == "AtivoAbono") {
        return "#8fbc8f";
    }if ($status == "Serasa") {
        return "#333";
    }if ($status == "Cancelado") {
        return "#607d8b";
    }if ($status == "Dependente") {
        return "#01454f";
    }if ($status == "AtivoEmAberto") {
        return "#8b0000";
    }if ($status == "Desligado") {
        return "#c0c0c0";
    }if ($status == "DesligadoEmAberto") {
        return "#696969";
    }if ($status == "Bloqueado") {
        return "#333";
    } else {
        return "#e09f4f";
    }
}

function getDataBaseServer($server) {
    switch ($server) {
        case "1":
            return "148.72.177.101,6000"; //finance
        case "2":
            return ""; //condor
        default:
            return "";
    }
}

function getStatus($valor) {
    if ($valor == 1) {
        return "Ativo";
    } elseif ($valor == 2) {
        return "Suspenso";
    } elseif ($valor == 3) {
        return "Inativo";
    } elseif ($valor == 4) {
        return "AtivoCred";
    } elseif ($valor == 5) {
        return "Ativo Ausente 1";
    } elseif ($valor == 6) {
        return "Ativo Ausente 2";
    } elseif ($valor == 7) {
        return "Ativo Ausente 3";
    } elseif ($valor == 8) {
        return "Ativo Ausente 4";
    } elseif ($valor == 9) {
        return "Ativo Ausente 5";
    } elseif ($valor == 10) {
        return "Serasa";
    } elseif ($valor == 11) {
        return "Dependente";
    } elseif ($valor == 12) {
        return "AtivoEmAberto";
    } elseif ($valor == 13) {
        return "Desligado";
    } elseif ($valor == 14) {
        return "AtivoAbono";
    } elseif ($valor == 15) {
        return "DesligadoEmAberto";
    } elseif ($valor == 16) {
        return "Cancelado";
    }
    return "";
}

function formatNameCompact($name) {
    $CompleteName = explode(" ", $name);
    $nomeUserLogin = $CompleteName[0];
    if (count($CompleteName) > 1) {
        $nomeUserLogin .= " " . $CompleteName[count($CompleteName) - 1];
    }
    return $nomeUserLogin;
}

function getImageCar($list, $key, $contrato, $id, $pasta) {
    $toReturn = "./../../img/noimage.png";
    foreach ($list as $value) {
        $k = explode(".", $value);
        if ($key == $k[0]) {
            $toReturn = "./../../Pessoas/" . $contrato . "/" . $pasta . "/" . $id . "/" . $value;
        }
    }
    return $toReturn;
}

function legendaPlano($valor) {
    if ($valor == 0) {
        return "Dcc";
    } elseif ($valor == -1) {
        return "Bol";
    } elseif ($valor == -2) {
        return "Pix";
    } elseif ($valor == 1) {
        return "Mensal";
    } elseif ($valor == 2) {
        return "Bimestral";
    } elseif ($valor == 3) {
        return "Trimestral";
    } elseif ($valor == 4) {
        return "Quadrimestral";
    } elseif ($valor == 6) {
        return "Semestral";
    } elseif ($valor == 12) {
        return "Anual";
    } else {
        return $valor . " Meses";
    }
}

function planoLegenda($valor) {
    if ($valor == "Dcc") {
        return 0;
    } elseif ($valor == "Bol") {
        return -1;
    } elseif ($valor == "Pix") {
        return -2;
    } elseif ($valor == "Mensal") {
        return 1;
    } elseif ($valor == "Bimestral") {
        return 2;
    } elseif ($valor == "Trimestral") {
        return 3;
    } elseif ($valor == "Quadrimestral") {
        return 4;
    } elseif ($valor == "Semestral") {
        return 6;
    } elseif ($valor == "Anual") {
        return 12;
    } else {
        return str_replace(" Meses", "", $valor);
    }
}

function bandeira_crt($cc_num) {
    if (preg_match("/^3[47]/", $cc_num) && strlen($cc_num) == 15) {
        return 6; //"amex";
    } elseif (preg_match("/^30[0-5]/", $cc_num) && strlen($cc_num) == 14) {
        return 4; //"diners_club_carte_blanche";
    } elseif (preg_match("/^36/", $cc_num) && strlen($cc_num) == 14) {
        return 4; //"diners_club_international";
    } elseif (preg_match("/^35(2[89]|[3-8][0-9])/", $cc_num) && strlen($cc_num) == 16) {
        return 0; //"jcb";
    } elseif (preg_match("/^(6304|670[69]|6771)/", $cc_num) && strlen($cc_num) >= 16 && strlen($cc_num) <= 19) {
        return 0; //"laser";
    } elseif (preg_match("/^(4026|417500|4508|4844|491(3|7))/", $cc_num) && strlen($cc_num) == 16) {
        return 7; //"visa_electron";
    } elseif (preg_match("/^4/", $cc_num) && strlen($cc_num) == 16) {
        return 1; //"visa";
    } elseif (preg_match("/^5[1-5]/", $cc_num) && strlen($cc_num) == 16) {
        return 2; //"mastercard";
    } elseif (preg_match("/^(5018|5020|5038|6304|6759|676[1-3])/", $cc_num) && strlen($cc_num) >= 12 && strlen($cc_num) <= 19) {
        return 9; //"maestro";
    } elseif (preg_match("/^(5859)/", $cc_num) && strlen($cc_num) == 16) {
        return 11; //"banescard";        
    } elseif (preg_match("/^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)/", $cc_num) && strlen($cc_num) == 16) {
        return 10; //"discover";
    } elseif (preg_match("/^(636368|438935|504175|451416|636297|5067|4576|4011)\d+$/", $cc_num) && strlen($cc_num) == 16) {
        return 3; //"elo";
    } elseif (preg_match("/^50[0-9]/", $cc_num) && strlen($cc_num) == 16) {
        return 5; //"aura";
    } elseif (preg_match("/^(38|60)\d{11}(?:\d{3})?(?:\d{3})?$/", $cc_num) && strlen($cc_num) == 16) {
        return 8; //"hiper";
    } else {
        return 0;
    }
}

function getTypeComissao($tp, $detail) {
    switch ($tp) :
        case  0: return 'Plano 1ª Parcela';
        case  1: return 'Plano Recorrente' . ($detail ? ' N1' : '');
        case  2: return 'Comissão Serviço';
        case  3: return 'Comissão Produto';
        case  4: return 'Plano Recorrente' . ($detail ? ' N2' : '');
        case  5: return 'Plano Recorrente' . ($detail ? ' N3' : '');
        case  6: return 'Plano Recorrente' . ($detail ? ' N4' : '');
        case  7: return 'Plano Recorrente' . ($detail ? ' N5' : '');
    endswitch;
}

function ocultar_email($value, $ocultar) {
    $itens = explode("@", $value);
    if ($ocultar && count($itens) > 1) {
        return substr($itens[0], 0, 2) . "******" . substr($itens[0], -2) . "@" . $itens[1];
    } else {
        return $value;
    }
}

function ocultar_celular($value, $ocultar) {
    $itens = explode("-", $value);
    if ($ocultar && count($itens) > 1) {
        return substr($itens[0], 0, 7) . "***-***" . substr($itens[1], -1);
    } else {
        return $value;
    }
}