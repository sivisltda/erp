<?php

session_start();
date_default_timezone_set('America/Bahia');
$protocolo = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http');
$host = $_SERVER['HTTP_HOST'];
$UrlAtual = $protocolo . '://' . $host;
if (!isset($_SESSION["contrato"]) || !isset($_SESSION["hostname"]) || !isset($_SESSION["username"]) || !isset($_SESSION["password"]) || !isset($_SESSION["database"]) || !isset($_SESSION["id_usuario"]) || !isset($_SESSION["nome_usuario"])) {
    header("Location: ./../../login.php");
    exit;
}
$contrato = $_SESSION["contrato"];
$hostname = $_SESSION["hostname"];
$username = $_SESSION["username"];
$password = $_SESSION["password"];
$database = $_SESSION["database"];
$con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password) or die(odbc_errormsg());
$nomeperm_ = "Administrador";
$mainPageGo = "";
$ckb_adm_cli_ = 1;
$ckb_adm_for_ = 1;
$ckb_adm_fun_ = 1;
$ckb_adm_usu_ = 1;
$ckb_adm_per_ = 1;
$ckb_adm_gru_ = 1;
$ckb_adm_dep_ = 1;
$ckb_adm_config_ = 1;
$ckb_adm_email_ = 1;
$ckb_adm_disparos_ = 1;
$ckb_adm_agenda_ = 1;
$ckb_adm_parentesco_ = 1;
$ckb_aca_usu_resp_ = 1;
$ckb_adm_cpf_obrig_ = 0;
$txt_adm_cpf_limite_ = 0;
$ckb_fin_doc_ = 1;
$ckb_fin_ban_ = 1;
$ckb_com_forma_ = 1;
$ckb_fin_ccusto_ = 1;
$ckb_fin_con_ = 1;
$ckb_fin_mov_ = 1;
$ckb_fin_fix_ = 1;
$ckb_fin_afix_ = 1;
$ckb_fin_flu_ = 1;
$ckb_fin_lsol_ = 1;
$ckb_fin_asol_ = 1;
$ckb_fin_pag_ = 1;
$ckb_fin_rec_ = 1;
$ckb_fin_rel_mov_caixa_ = 1;
$ckb_fin_aped_ = 1;
$ckb_fin_lped_ = 1;
$ckb_fin_lcot_ = 1;
$ckb_est_mov_ = 1;
$ckb_est_pro_ = 1;
$ckb_est_ser_ = 1;
$ckb_est_loc_ = 1;
$ckb_est_lven_ = 1;
$ckb_est_lven_read_ = 0;
$ckb_est_aven_ = 1;
$ckb_est_lorc_ = 1;
$ckb_est_aorc_ = 1;
$ckb_est_lcom_ = 1;
$ckb_est_acom_ = 1;
$ckb_est_ent_sai_ = 1;
$ckb_est_aent_sai_ = 1;
$ckb_est_flu_ = 1;
$ckb_est_com_ = 1;
$ckb_est_cor_ = 1;
$ckb_est_tam_ = 1;
$ckb_est_fab_ = 1;
$ckb_est_jus_ = 1;
$ckb_nfe_cen_ = 1;
$ckb_nfe_tra_ = 1;
$ckb_nfe_msg_ = 1;
$ckb_nfe_emi_ = 1;
$ckb_nfe_est_ = 1;
$ckb_nfe_cid_ = 1;
$ckb_nfe_not_ = 1;
$ckb_nfe_eqs_ = 1;
$ckb_nfe_nos_ = 1;
$ckb_aca_pgr_ = 1;
$ckb_crm_cli_ = 1;
$ckb_crm_fch_ = 1;
$ckb_crm_ind_ = 1;
$ckb_crm_pro_ = 1;
$ckb_crm_tel_ = 1;
$ckb_crm_esp_ = 1;
$ckb_aca_rem_plano_ = 1;
$ckb_aca_can_plano_ = 1;
$ckb_crm_exc_atend_ = 1;
$ckb_crm_fec_atend_ = 1;
$ckb_out_ges_ = 1;
$ckb_out_aju_ = 1;
$ckb_ser_ser_ = 1;
$ckb_ser_cli_ = 1;
$ckb_ser_rpc_ = 1;
$ckb_crm_rec_ = 1;
$ckb_crm_lead_ = 1;
$ckb_crm_lead_camp_ = 1;
$ckb_crm_chatbot_ = 1;
$ckb_fin_dmp_ = 1;
$ckb_crm_cpc_ = 1;
$ckb_adm_cli_read_ = 0;
$ckb_adm_cli_read_obs_ = 0;
$ckb_fin_rel_gct_ = 1;
$ckb_fin_crt_ = 1;
$ckb_fin_rcrt_ = 1;
$ckb_fin_chq_ = 1;
$ckb_out_term_ = 1;
$ckb_out_terc_ = 1;
$ckb_out_rban_ = 1;
$ckb_out_rexc_ = 1;
$ckb_out_ecob_ = 1;
$ckb_fin_rel_bol_ = 1;
$ckb_fin_rel_tdd_ = 1;
$ckb_est_pla_ = 1;
$ckb_siv_cadAcad_ = 1;
$ckb_siv_CreGB_ = 1;
$ckb_siv_cadErp_ = 1;
$ckb_siv_PenGB_ = 1;
$ckb_siv_LogErp_ = 1;
$ckb_siv_GerSenha_ = 1;
$ckb_siv_UserSite_ = 1;
$ckb_siv_DCC_ = 1;
$ckb_aca_hor_ = 1;
$ckb_aca_conv_ = 1;
$ckb_aca_cred_ = 1;
$ckb_aca_catr_ = 1;
$ckb_aca_leif_ = 1;
$ckb_aca_amb_ = 1;
$ckb_aca_fer_ = 1;
$ckb_aca_fer_par_ = 1;
$ckb_aca_fer_exc_ = 1;
$ckb_aca_pla_ = 1;
$ckb_aca_ser_ = 1;
$ckb_out_todos_operadores_ = 1;
$ckb_out_desconto_ = 1;
$ckb_aca_alt_val_mens_ = 1;
$ckb_aca_pro_rata_mens_= 1;
$ckb_aca_documento_ = 1;
$ckb_aca_est_mens_ = 1;
$ckb_aca_exc_mens_ = 1;
$ckb_aca_exc_mret_ = 1;
$ckb_out_pag_avancada_ = 1;
$ckb_aca_inf_gerenciais_ = 1;
$ckb_aca_add_convenio_ = 1;
$ckb_aca_add_credencial_ = 1;
$ckb_aca_dtf_credencial_ = 1;
$ckb_aca_add_bloqueios_ = 1;
$ckb_aca_cancelar_dcc_ = 1;
$ckb_aca_plan_comp_ = 1;
$ckb_aca_rel_acesso_ = 1;
$ckb_aca_turmas_ = 1;
$ckb_aca_estudio_ = 1;
$ckb_aca_documentos_ = 1;
$ckb_adm_emlobg_ = 0;
$ckb_adm_telobg_ = 0;
$ckb_adm_celobg_ = 0;
$ckb_adm_endobg_ = 0;
$ckb_adm_comissao_ = 0;
$ckb_clb_titobg_ = 0;
$ckb_adm_procobg_ = 0;
$ckb_adm_sexoobg_ = 0;
$ckb_aca_abomens_ = 1;
$ckb_aca_one_item_ = 0;
$ckb_aca_abocre_ = 1; 
$ckb_aca_abodeb_ = 1;
$ckb_aca_pg_prod_ = 1;
$ckb_aca_can_item_ = 1;
$ckb_aca_agenda_regras_ = 1;
$ckb_aca_data_inicio_ = 1;
$ckb_aca_igCaixa_ = 1;
$ckb_aca_igStatus_ = 1;
$ckb_aca_Contrato_ = 1;
$ckb_aca_filAvn_ = 1;
$ckb_aca_renovacoes_ = 1;
$ckb_aca_dcc_ = 1;
$ckb_aca_bol_ = 1;
$ckb_aca_pix_ = 1;
$ckb_aca_acesso_ = 1;
$ckb_out_incserasa_ = 1;
$ckb_out_remserasa_ = 1;
$ckb_aca_cli_block = 1;
$ckb_aca_cntt_dtCanc = 1;
$ckb_aca_cntt_dtCanc_limit = 0;
$ckb_crm_upt_rem = 1;
$ckb_crm_upt_rem_new = 0;
$crm_pro_ure_ = 0;
$crm_cli_ure_ = 0;
$use_dist_pro_ = 1;
$crm_lea_ure_ = 0;
$ckb_fin_cx_ticket = 1;
$ckb_fin_cx_mat = 1;
$ckb_fin_cx_mod = 1;
$ckb_clb_loc_ = 1;
$ckb_clb_eventos_ = 1;
$ckb_clb_categoria_ = 1;
$ckb_clb_tipo_ = 1;
$ckb_clb_layout_remessa_ = 1;
$ckb_clb_layout_retorno_ = 1;
$ckb_clb_excecao_vencimento_ = 1;
$ckb_clb_baixar_layout_ = 1;
$ckb_clb_gboleto_ = 1;
$ckb_clb_cboleto_ = 1;
$ckb_clb_gconvite_ = 1;
$ckb_clb_rel_eventos_ = 1;
$ckb_clb_rel_socios_ = 1;
$ckb_clb_trancamento_ = 1;
$ckb_clb_gestao_documentos_ = 1;
$ckb_clb_gestao_folha_ = 1;
$ckb_clb_excluir_convites_ = 1;
$ckb_fin_cancel_multa_ = 1;
$ckb_fin_lpg_ = 1;
$ckb_adm_contratos_ = 1;
$ckb_adm_atendimentos_ = 1;
$ckb_exc_cpagar_ = 1;
$ckb_exc_creceber_ = 1;
$ckb_pro_grp_acess_ = 1;
$ckb_pro_acess_ = 1;
$ckb_pro_tipo_vei_ = 1;
$ckb_pro_rastreadores_ = 1;
$ckb_operadoras_ = 1;
$ckb_pro_item_vis_ = 1;
$ckb_pro_tipo_vis_ = 1;
$ckb_pro_grp_cidade_ = 1;
$ckb_pro_grp_fabricantes_ = 1;
$ckb_pro_veiculo_ = 1;
$ckb_pro_vistoria_ = 1;
$ckb_pro_sinistro_ = 1;
$ckb_pro_oficina_ = 1;
$ckb_pro_cotas_ = 1;
$ckb_pro_fipe_ = 1;
$ckb_pro_classificacao_ = 1;
$ckb_pro_montadoras_ = 1;
$ckb_pro_modelos_ = 1;
$ckb_pro_serv_mod_ = 1;
$ckb_pro_terceiros_ = 1;
$ckb_cpagar_read_ = 0;
$ckb_creceber_read_ = 0;
$ckb_fcaixa_read_ = 0;
$ckb_pro_apv_veic_ = 1;
$ckb_pro_apv_sin_ = 1;
$ckb_pro_rastreador_ = 1;
$ckb_pro_exc_vei_ = 1;
$ckb_pro_cad_veic_ = 1;
$ckb_bi_auditoria_ = 1;
$ckb_bi_mat_ren_ = 1;
$ckb_bi_gve_sfin_ = 0;
$ckb_bi_retencao_ = 1;
$ckb_bi_inadimplencia_ = 1;
$ckb_bi_evasao_ = 1;
$ckb_bi_prev_fin_ = 1;
$ckb_bi_cen_custo_ = 1;
$ckb_bi_fluxo_caixa_ = 1;
$ckb_bi_faturamento_ = 1;
$ckb_bi_com_faturamento_ = 1;
$ckb_bi_formas_pagamento_ = 1;
$ckb_bi_gestao_vendas_ = 1;
$ckb_bi_gestao_mapa_ = 1;
$ckb_bi_produtividade_ = 1;
$ckb_bi_com_produtividade_ = 1;
$ckb_bi_com_comissionado_ = 1;
$ckb_bi_atendimentos_ = 1;
$ckb_bi_leads_ = 1;
$ckb_bi_prospects_ = 1;
$ckb_bi_agendamentos_ = 1;
$ckb_bi_turmas_ = 1;
$ckb_bi_rel_acessos_ = 1;

$cur = odbc_exec($con, "select * from sf_usuarios_permissoes 
inner join sf_usuarios on sf_usuarios.master = sf_usuarios_permissoes.id_permissoes 
where id_usuario = " . $_SESSION["id_usuario"]);
while ($RFP = odbc_fetch_array($cur)) {
    $nomeperm_ = utf8_encode($RFP['nome_permissao']);
    $ckb_adm_cli_ = utf8_encode($RFP['adm_cli']);
    $ckb_adm_for_ = utf8_encode($RFP['adm_for']);
    $ckb_adm_fun_ = utf8_encode($RFP['adm_fun']);
    $ckb_adm_usu_ = utf8_encode($RFP['adm_usu']);
    $ckb_adm_per_ = utf8_encode($RFP['adm_per']);
    $ckb_adm_gru_ = utf8_encode($RFP['adm_gru']);
    $ckb_adm_dep_ = utf8_encode($RFP['adm_dep']);
    $ckb_adm_config_ = utf8_encode($RFP['adm_config']);
    $ckb_adm_email_ = utf8_encode($RFP['adm_email']);
    $ckb_adm_disparos_ = utf8_encode($RFP['adm_disparos']);
    $ckb_adm_agenda_ = utf8_encode($RFP['adm_agenda']);
    $ckb_adm_parentesco_ = utf8_encode($RFP['adm_parentesco']);
    $ckb_aca_usu_resp_ = utf8_encode($RFP['aca_usu_resp']);
    $ckb_adm_cpf_obrig_ = utf8_encode($RFP['adm_cpf_obrig']);    
    $txt_adm_cpf_limite_ = utf8_encode($RFP['adm_cpf_limite']);    
    $ckb_fin_doc_ = utf8_encode($RFP['fin_doc']);
    $ckb_fin_ban_ = utf8_encode($RFP['fin_ban']);
    $ckb_com_forma_ = utf8_encode($RFP['com_forma']);
    $ckb_fin_con_ = utf8_encode($RFP['fin_con']);
    $ckb_fin_ccusto_ = utf8_encode($RFP['est_ccu']);
    $ckb_fin_mov_ = utf8_encode($RFP['fin_mov']);
    $ckb_fin_fix_ = utf8_encode($RFP['fin_fix']);
    $ckb_fin_afix_ = utf8_encode($RFP['fin_afix']);
    $ckb_fin_flu_ = utf8_encode($RFP['fin_flu']);
    $ckb_fin_lsol_ = utf8_encode($RFP['fin_lsol']);
    $ckb_fin_asol_ = utf8_encode($RFP['fin_asol']);
    $ckb_fin_pag_ = utf8_encode($RFP['fin_pag']);
    $ckb_fin_rec_ = utf8_encode($RFP['fin_rec']);
    $ckb_fin_rel_mov_caixa_ = utf8_encode($RFP['fin_rel_mov_caixa']);
    $ckb_fin_lped_ = utf8_encode($RFP['fin_lped']);
    $ckb_fin_lcot_ = utf8_encode($RFP['fin_lcot']);
    $ckb_fin_aped_ = utf8_encode($RFP['fin_aped']);
    $ckb_est_mov_ = utf8_encode($RFP['est_mov']);
    $ckb_est_pro_ = utf8_encode($RFP['est_pro']);
    $ckb_est_ser_ = utf8_encode($RFP['est_ser']);
    $ckb_est_loc_ = utf8_encode($RFP['est_loc']);
    $ckb_est_lven_ = utf8_encode($RFP['est_lven']);
    $ckb_est_lven_read_ = utf8_encode($RFP['est_lven_read']);
    $ckb_est_aven_ = utf8_encode($RFP['est_aven']);
    $ckb_est_lorc_ = utf8_encode($RFP['est_lorc']);
    $ckb_est_aorc_ = utf8_encode($RFP['est_aorc']);
    $ckb_est_lcom_ = utf8_encode($RFP['est_lcom']);
    $ckb_est_acom_ = utf8_encode($RFP['est_acom']);
    $ckb_est_ent_sai_ = utf8_encode($RFP['est_ent_sai']);
    $ckb_est_aent_sai_ = utf8_encode($RFP['est_aent_sai']);
    $ckb_est_flu_ = utf8_encode($RFP['est_flu']);
    $ckb_est_com_ = utf8_encode($RFP['est_com']);
    $ckb_est_cor_ = utf8_encode($RFP['est_cor']);
    $ckb_est_tam_ = utf8_encode($RFP['est_tam']);
    $ckb_est_fab_ = utf8_encode($RFP['est_fab']);
    $ckb_est_jus_ = utf8_encode($RFP['est_jus']);
    $ckb_est_pla_ = utf8_encode($RFP['est_pla']);
    $ckb_nfe_cen_ = utf8_encode($RFP['nfe_cen']);
    $ckb_nfe_tra_ = utf8_encode($RFP['nfe_tra']);
    $ckb_nfe_msg_ = utf8_encode($RFP['nfe_msg']);
    $ckb_nfe_emi_ = utf8_encode($RFP['nfe_emi']);
    $ckb_nfe_est_ = utf8_encode($RFP['nfe_est']);
    $ckb_nfe_cid_ = utf8_encode($RFP['nfe_cid']);
    $ckb_nfe_not_ = utf8_encode($RFP['nfe_not']);
    $ckb_nfe_eqs_ = utf8_encode($RFP['nfe_enq']);
    $ckb_nfe_nos_ = utf8_encode($RFP['nfe_ess']);
    $ckb_aca_pgr_ = utf8_encode($RFP['aca_pgr']);
    $ckb_crm_cli_ = utf8_encode($RFP['crm_cli']);
    $ckb_crm_fch_ = utf8_encode($RFP['crm_fch']);
    $ckb_crm_ind_ = utf8_encode($RFP['crm_ind']);
    $ckb_crm_pro_ = utf8_encode($RFP['crm_pro']);
    $ckb_crm_tel_ = utf8_encode($RFP['crm_tel']);
    $ckb_crm_esp_ = utf8_encode($RFP['crm_esp']);
    $ckb_aca_rem_plano_ = utf8_encode($RFP['aca_rem_plano']);
    $ckb_aca_can_plano_ = utf8_encode($RFP['aca_can_plano']);
    $ckb_crm_exc_atend_ = utf8_encode($RFP['crm_exc_atend']);
    $ckb_crm_fec_atend_ = utf8_encode($RFP['crm_fec_atend']);
    $ckb_out_ges_ = utf8_encode($RFP['out_ges']);
    $ckb_out_aju_ = utf8_encode($RFP['out_aju']);
    $ckb_ser_ser_ = utf8_encode($RFP['ser_ser']);
    $ckb_ser_cli_ = utf8_encode($RFP['ser_cli']);
    $ckb_ser_rpc_ = utf8_encode($RFP['est_spc']);
    $ckb_crm_rec_ = utf8_encode($RFP['crm_rec']);
    $ckb_crm_lead_ = utf8_encode($RFP['crm_lead']);
    $ckb_crm_lead_camp_ = utf8_encode($RFP['crm_lead_camp']);
    $ckb_crm_chatbot_ = utf8_encode($RFP['crm_chatbot']);
    $ckb_bi_inadimplencia_ = utf8_encode($RFP['bi_inadimplencia']);
    $ckb_fin_dmp_ = utf8_encode($RFP['fin_dmp']);
    $ckb_crm_cpc_ = utf8_encode($RFP['crm_cpc']);
    $ckb_adm_cli_read_ = utf8_encode($RFP['adm_cli_read']);
    $ckb_adm_cli_read_obs_ = utf8_encode($RFP['adm_cli_read_obs']);
    $ckb_fin_rel_gct_ = utf8_encode($RFP['fin_rel_gct']);
    $ckb_fin_crt_ = utf8_encode($RFP['fin_crt']);
    $ckb_fin_rcrt_ = utf8_encode($RFP['fin_rcrt']);
    $ckb_fin_chq_ = utf8_encode($RFP['fin_chq']);
    $ckb_out_term_ = utf8_encode($RFP['out_term']);
    $ckb_out_terc_ = utf8_encode($RFP['out_terc']);
    $ckb_out_rban_ = utf8_encode($RFP['out_rban']);
    $ckb_out_rexc_ = utf8_encode($RFP['out_rexc']);
    $ckb_out_ecob_ = utf8_encode($RFP['out_ecob']);
    $ckb_fin_rel_bol_ = utf8_encode($RFP['fin_bol']);
    $ckb_fin_rel_tdd_ = utf8_encode($RFP['fin_tdd']);
    $ckb_siv_cadAcad_ = utf8_encode($RFP['siv_cadacad']);
    $ckb_siv_cadErp_ = utf8_encode($RFP['siv_caderp']);
    $ckb_siv_GerSenha_ = utf8_encode($RFP['siv_gersenha']);
    $ckb_siv_UserSite_ = utf8_encode($RFP['siv_usersite']);
    $ckb_siv_DCC_ = utf8_encode($RFP['siv_dcc']);
    $ckb_siv_CreGB_ = utf8_encode($RFP['siv_cregb']);
    $ckb_siv_PenGB_ = utf8_encode($RFP['siv_pengb']);
    $ckb_siv_LogErp_ = utf8_encode($RFP['siv_logerp']);
    $ckb_aca_hor_ = utf8_encode($RFP['aca_hor']);
    $ckb_aca_conv_ = utf8_encode($RFP['aca_conv']);
    $ckb_aca_cred_ = utf8_encode($RFP['aca_cred']);
    $ckb_aca_catr_ = utf8_encode($RFP['aca_catr']);
    $ckb_aca_leif_ = utf8_encode($RFP['aca_leif']);
    $ckb_aca_amb_ = utf8_encode($RFP['aca_amb']);
    $ckb_aca_fer_ = utf8_encode($RFP['aca_fer']);
    $ckb_aca_fer_par_ = utf8_encode($RFP['aca_fer_par']);
    $ckb_aca_fer_exc_ = utf8_encode($RFP['aca_fer_exc']);
    $ckb_aca_pla_ = utf8_encode($RFP['aca_pla']);
    $ckb_aca_ser_ = utf8_encode($RFP['aca_ser']);
    $ckb_out_todos_operadores_ = utf8_encode($RFP['out_todos_operadores']);
    $ckb_out_desconto_ = utf8_encode($RFP['out_desconto']);
    $ckb_aca_alt_val_mens_ = utf8_encode($RFP['aca_alt_val_mens']);
    $ckb_aca_pro_rata_mens_ = utf8_encode($RFP['aca_pro_rata_mens']);
    $ckb_aca_documento_ = utf8_encode($RFP['aca_documento']);
    $ckb_aca_est_mens_ = utf8_encode($RFP['aca_est_mens']);
    $ckb_aca_exc_mens_ = utf8_encode($RFP['aca_exc_mens']);
    $ckb_aca_exc_mret_ = utf8_encode($RFP['aca_exc_mret']);
    $ckb_out_pag_avancada_ = utf8_encode($RFP['out_pag_avancada']);
    $ckb_aca_inf_gerenciais_ = utf8_encode($RFP['aca_inf_gerenciais']);
    $ckb_aca_add_convenio_ = utf8_encode($RFP['aca_add_convenio']);
    $ckb_aca_add_credencial_ = utf8_encode($RFP['aca_add_credencial']);
    $ckb_aca_dtf_credencial_ = utf8_encode($RFP['aca_dtf_credencial']);
    $ckb_aca_add_bloqueios_ = utf8_encode($RFP['aca_add_bloqueio']);
    $ckb_aca_cancelar_dcc_ = utf8_encode($RFP['aca_cancelar_dcc']);
    $ckb_aca_plan_comp_ = utf8_encode($RFP['aca_plan_comp']);
    $ckb_aca_rel_acesso_ = utf8_encode($RFP['aca_rel_acesso']);
    $ckb_aca_turmas_ = utf8_encode($RFP['aca_turmas']);
    $ckb_aca_estudio_ = utf8_encode($RFP['aca_estudio']);
    $ckb_aca_documentos_ = utf8_encode($RFP['aca_documentos']);
    $ckb_adm_emlobg_ = utf8_encode($RFP['adm_email_obrig']);
    $ckb_adm_telobg_ = utf8_encode($RFP['adm_tel_obrig']);
    $ckb_adm_celobg_ = utf8_encode($RFP['adm_cel_obrig']);
    $ckb_adm_endobg_ = utf8_encode($RFP['adm_end_obrig']);
    $ckb_adm_comissao_ = utf8_encode($RFP['adm_comissao']);
    $ckb_clb_titobg_ = utf8_encode($RFP['clb_tit_obrig']);
    $ckb_adm_procobg_ = utf8_encode($RFP['adm_proc_obrig']);
    $ckb_adm_sexoobg_ = utf8_encode($RFP['adm_sexo_obrig']);
    $ckb_aca_abomens_ = utf8_encode($RFP['aca_abonar_mens']);    
    $ckb_aca_one_item_ = utf8_encode($RFP['aca_one_item']);    
    $ckb_aca_abocre_ = utf8_encode($RFP['aca_abonar_cre']);
    $ckb_aca_abodeb_ = utf8_encode($RFP['aca_abonar_deb']);        
    $ckb_aca_pg_prod_ = utf8_encode($RFP['aca_pg_prod']);        
    $ckb_aca_can_item_ = utf8_encode($RFP['aca_can_item']); 
    $ckb_aca_agenda_regras_ = utf8_encode($RFP['aca_agenda_regras']);
    $ckb_aca_data_inicio_ = utf8_encode($RFP['aca_data_inicio']);
    $ckb_aca_igCaixa_ = utf8_encode($RFP['aca_infoger_caixa']);
    $ckb_aca_igStatus_ = utf8_encode($RFP['aca_infoger_stats']);
    $ckb_aca_Contrato_ = utf8_encode($RFP['aca_infoger_contr']);
    $ckb_aca_filAvn_ = utf8_encode($RFP['aca_infoger_filav']);
    $ckb_aca_renovacoes_ = utf8_encode($RFP['aca_infoger_renov']);
    $ckb_aca_dcc_ = utf8_encode($RFP['aca_infoger_dcc']);
    $ckb_aca_bol_ = utf8_encode($RFP['aca_infoger_bol']);
    $ckb_aca_pix_ = utf8_encode($RFP['aca_infoger_pix']);
    $ckb_aca_acesso_ = utf8_encode($RFP['aca_infoger_acess']);
    $ckb_out_incserasa_ = utf8_encode($RFP['out_incluir_serasa']);
    $ckb_out_remserasa_ = utf8_encode($RFP['out_remover_serasa']);
    $ckb_aca_cli_block = utf8_encode($RFP['aca_cli_block']);
    $ckb_aca_cntt_dtCanc = utf8_encode($RFP['aca_cntt_dtCanc']);
    $ckb_aca_cntt_dtCanc_limit = utf8_encode($RFP['aca_cntt_dtCanc_limit']);
    $ckb_crm_upt_rem = utf8_encode($RFP['crm_upt_rem']);
    $ckb_crm_upt_rem_new = utf8_encode($RFP['crm_upt_rem_new']);
    $crm_pro_ure_ = utf8_encode($RFP['crm_pro_ure']);    
    $crm_cli_ure_ = utf8_encode($RFP['crm_cli_ure']);
    $use_dist_pro_ = utf8_encode($RFP['use_dist_pro']);        
    $crm_lea_ure_ = utf8_encode($RFP['crm_lea_ure']);        
    $ckb_fin_cx_ticket = utf8_encode($RFP['fin_cx_ticket']);
    $ckb_fin_cx_mat = utf8_encode($RFP['fin_cx_mat']);
    $ckb_fin_cx_mod = utf8_encode($RFP['fin_cx_mod']);
    $ckb_clb_loc_ = utf8_encode($RFP['clb_locacoes']);
    $ckb_clb_eventos_ = utf8_encode($RFP['clb_eventos']);
    $ckb_clb_categoria_ = utf8_encode($RFP['clb_categoria']);
    $ckb_clb_tipo_ = utf8_encode($RFP['clb_tipo']);
    $ckb_clb_layout_remessa_ = utf8_encode($RFP['clb_layout_remessa']);
    $ckb_clb_layout_retorno_ = utf8_encode($RFP['clb_layout_retorno']);
    $ckb_clb_excecao_vencimento_ = utf8_encode($RFP['clb_excecao_vencimento']);
    $ckb_clb_baixar_layout_ = utf8_encode($RFP['clb_baixar_layout']);
    $ckb_clb_gboleto_ = utf8_encode($RFP['clb_gboleto']);
    $ckb_clb_cboleto_ = utf8_encode($RFP['clb_cboleto']);
    $ckb_clb_gconvite_ = utf8_encode($RFP['clb_gconvite']);
    $ckb_clb_rel_eventos_ = utf8_encode($RFP['clb_rel_eventos']);
    $ckb_clb_rel_socios_ = utf8_encode($RFP['clb_rel_socios']);
    $ckb_clb_trancamento_ = utf8_encode($RFP['clb_trancamento']);
    $ckb_clb_gestao_documentos_ = utf8_encode($RFP['clb_gestao_documentos']);
    $ckb_clb_gestao_folha_ = utf8_encode($RFP['clb_gestao_folha']);
    $ckb_clb_excluir_convites_ = utf8_encode($RFP['clb_excluir_convites']);
    $ckb_fin_cancel_multa_ = utf8_encode($RFP['fin_cancel_multa']);
    $ckb_fin_lpg_ = utf8_encode($RFP['fin_lpg']);
    $ckb_adm_contratos_ = utf8_encode($RFP['adm_contratos']);
    $ckb_adm_atendimentos_ = utf8_encode($RFP['adm_atendimentos']);
    $ckb_exc_cpagar_ = utf8_encode($RFP['exc_cpagar']);
    $ckb_exc_creceber_ = utf8_encode($RFP['exc_creceber']);       
    $ckb_pro_grp_acess_ = utf8_encode($RFP['ckb_pro_grp_acess']);
    $ckb_pro_acess_ = utf8_encode($RFP['ckb_pro_acess']);
    $ckb_pro_tipo_vei_ = utf8_encode($RFP['ckb_pro_tipo']);
    $ckb_pro_rastreadores_ = utf8_encode($RFP['ckb_pro_rastreadores']);
    $ckb_operadoras_ = utf8_encode($RFP['ckb_pro_operadoras']);
    $ckb_pro_item_vis_ = utf8_encode($RFP['ckb_pro_item_vis']);
    $ckb_pro_tipo_vis_ = utf8_encode($RFP['ckb_pro_item_vis']);
    $ckb_pro_grp_cidade_ = utf8_encode($RFP['ckb_pro_grp_cidade']);
    $ckb_pro_grp_fabricantes_ = utf8_encode($RFP['ckb_pro_grp_fabricantes']);
    $ckb_pro_veiculo_ = utf8_encode($RFP['ckb_pro_veiculo']);
    $ckb_pro_vistoria_ = utf8_encode($RFP['ckb_pro_vistoria']);
    $ckb_pro_sinistro_ = utf8_encode($RFP['ckb_pro_sinistro']);
    $ckb_pro_oficina_ = utf8_encode($RFP['ckb_pro_oficina']);
    $ckb_pro_cotas_ = utf8_encode($RFP['ckb_pro_cotas']);    
    $ckb_pro_fipe_ = utf8_encode($RFP['ckb_pro_fipe']);    
    $ckb_pro_classificacao_ = utf8_encode($RFP['ckb_pro_classificacao']);
    $ckb_pro_montadoras_ = utf8_encode($RFP['ckb_pro_montadoras']);
    $ckb_pro_modelos_ = utf8_encode($RFP['ckb_pro_modelos']);
    $ckb_pro_serv_mod_ = utf8_encode($RFP['ckb_pro_serv_mod']);    
    $ckb_pro_terceiros_ = utf8_encode($RFP['ckb_pro_terceiros']);    
    $ckb_cpagar_read_ = utf8_encode($RFP['ckb_cpagar_read']);
    $ckb_creceber_read_ = utf8_encode($RFP['ckb_creceber_read']);
    $ckb_fcaixa_read_ = utf8_encode($RFP['ckb_fcaixa_read']);
    $ckb_pro_apv_veic_ = utf8_encode($RFP['ckb_pro_apv_veic']); 
    $ckb_pro_apv_sin_ = utf8_encode($RFP['ckb_pro_apv_sin']); 
    $ckb_pro_rastreador_ = utf8_encode($RFP['ckb_pro_rastreador']); 
    $ckb_pro_exc_vei_ = utf8_encode($RFP['ckb_pro_exc_vei']);
    $ckb_pro_cad_veic_ = utf8_encode($RFP['ckb_pro_cad_veic']);
    $ckb_bi_auditoria_ = utf8_encode($RFP['bi_auditoria']);
    $ckb_bi_mat_ren_ = utf8_encode($RFP['bi_mat_ren']);
    $ckb_bi_gve_sfin_ = utf8_encode($RFP['bi_gve_sfin']);
    $ckb_bi_retencao_ = utf8_encode($RFP['bi_retencao']);
    $ckb_bi_evasao_ = utf8_encode($RFP['bi_evasao']);
    $ckb_bi_prev_fin_ = utf8_encode($RFP['bi_prev_fin']);
    $ckb_bi_cen_custo_ = utf8_encode($RFP['bi_cen_custo']);
    $ckb_bi_fluxo_caixa_ = utf8_encode($RFP['bi_fluxo_caixa']);
    $ckb_bi_faturamento_ = utf8_encode($RFP['bi_faturamento']);
    $ckb_bi_com_faturamento_ = utf8_encode($RFP['bi_com_faturamento']);
    $ckb_bi_formas_pagamento_ = utf8_encode($RFP['bi_formas_pagamento']);
    $ckb_bi_gestao_vendas_ = utf8_encode($RFP['bi_gestao_vendas']);
    $ckb_bi_gestao_mapa_ = utf8_encode($RFP['bi_gestao_mapa']);
    $ckb_bi_produtividade_ = utf8_encode($RFP['bi_produtividade']);
    $ckb_bi_com_produtividade_ = utf8_encode($RFP['bi_com_produtividade']);
    $ckb_bi_com_comissionado_ = utf8_encode($RFP['bi_com_comissionado']);
    $ckb_bi_atendimentos_ = utf8_encode($RFP['bi_atendimentos']);
    $ckb_bi_leads_ = utf8_encode($RFP['bi_leads']);
    $ckb_bi_prospects_ = utf8_encode($RFP['bi_prospects']);
    $ckb_bi_agendamentos_ = utf8_encode($RFP['bi_agendamentos']);
    $ckb_bi_turmas_ = utf8_encode($RFP['bi_turmas']);
    $ckb_bi_rel_acessos_ = utf8_encode($RFP['bi_rel_acessos']);
    if ($RFP['out_pag_avancada'] == 1 && $RFP['pag_avancada'] == 1) {
        $mainPageGo = "index2.php";
    } else {
        $mainPageGo = "index.php";
    }
}
$wherex = "";
$mensagens = 0;
$last_msg = "";
$tarefas = 0;
$cur = odbc_exec($con, "select top 30 count(*) total from sf_mensagens_usuarios where lido = 0 and id_usuario_para = " . $_SESSION["id_usuario"]);
while ($RFP = odbc_fetch_array($cur)) {
    $mensagens = utf8_encode($RFP['total']);
    if ($mensagens > 0) {
        $cur = odbc_exec($con, "select top 1 mensagem from dbo.sf_mensagens_usuarios where lido = 0 and id_usuario_para = " . $_SESSION["id_usuario"] . " order by data");
        while ($ASR = odbc_fetch_array($cur)) {
            $last_msg = utf8_encode($ASR['mensagem']);
        }
    }
}
if (is_numeric($_SESSION["id_usuario"])) {
    $wherex = " or (t.acompanhar = 1 and t.de_pessoa = " . $_SESSION["id_usuario"] . ") or t.para_pessoa = " . $_SESSION["id_usuario"] . " or id_tele in (select id_tele from dbo.sf_mensagens_telemarketing where id_usuario_para = " . $_SESSION["id_usuario"] . ")";
}
$cur = odbc_exec($con, "select count (*) total from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa
LEFT JOIN sf_usuarios u2 on u2.id_usuario = t.para_pessoa where pendente = 1 and ((t.para_pessoa is null and t.de_pessoa = " . $_SESSION["id_usuario"] . ") " . $wherex . ")");
while ($RFP = odbc_fetch_array($cur)) {
    $tarefas = utf8_encode($RFP['total']);
}

$languages = "pt";
$_tipoContrato = 0;
$diasTolerancias = 0;
$clb_pagamento_agrupado_ = 0;
$servico_adesao = 0;
$adm_catraca_urna_ = 0;
$white_total_ = 0;
$white_count_ = 0;
$black_count_ = 0;
$dcc_multa_ = 0;
$comissao_forma_ = 0;
$recibo_tipo_ = 0;
$recibo_vias_ = 0;
$convite_tipo_ = 0;
$veiPlanObrig_ = 0;
$adm_desc_ven_ = 0;
$adm_cli_ordem_ = 0;
$dcc_boleto_tipo_ = 0;
$desconto_decimal_ = 2;
        
$cur = odbc_exec($con, "select *, 
(select COUNT(*) total from sf_login_white) white_total,
(select COUNT(*) total from sf_login_white where ip = '" . $_SERVER['REMOTE_ADDR'] . "' and (id_usuario is null or id_usuario = " . $_SESSION["id_usuario"] . ")) white_count,
(select COUNT(*) total from sf_login_black where ip = '" . $_SERVER['REMOTE_ADDR'] . "') black_count
from sf_configuracao where id = 1");
while ($RFP = odbc_fetch_array($cur)) {
    $languages = $RFP['conf_regionais'];
    $_tipoContrato = $RFP['use_cnt'];
    $diasTolerancias = $RFP['ACA_PLN_DIAS_TOLERANCIA'];
    $clb_pagamento_agrupado_ = $RFP['clb_pagamento_agrupado'];
    $servico_adesao = $RFP['ACA_SERVICO_ADESAO'];    
    $adm_catraca_urna_ = $RFP['adm_catraca_urna'];    
    if ($mainPageGo == "" && $RFP['pag_avancada_adm'] == 1) {
        $mainPageGo = "index2.php";
    } elseif ($mainPageGo == "") {
        $mainPageGo = "index.php";
    }
    $white_total_ = $RFP['white_total'];
    $white_count_ = $RFP['white_count'];
    $black_count_ = $RFP['black_count'];
    $dcc_multa_ = $RFP['dcc_multa'];    
    $comissao_forma_ = $RFP['com_forma'];    
    $recibo_tipo_ = $RFP['recibo_tipo'];
    $recibo_vias_ = $RFP['recibo_vias'];
    $convite_tipo_ = $RFP['convite_tipo'];
    $veiPlanObrig_ = $RFP['seg_vei_plan_obrig'];  
    $adm_desc_ven_ = $RFP['adm_desc_vencimento'];
    $cor_primaria_ = $RFP['cor_primaria'];    
    $cor_secundaria_ = $RFP['cor_secundaria'];    
    $cor_destaque_ = $RFP['cor_destaque'];    
    $clb_pagamento_credencial_ = $RFP['clb_pagamento_credencial'];     
    $adm_cli_ordem_ = $RFP['adm_cli_ordem'];     
    $dcc_boleto_tipo_ = $RFP['dcc_boleto_tipo'];     
    $desconto_decimal_ = $RFP['desconto_decimal'];     
}

if ($white_total_ > 0 && $white_count_ == 0 && $_SESSION["id_usuario"] > 1 && $black_count_ == 0) {    
    include __DIR__ . "./../util/util.php";        
    $link_go = encrypt(($_SERVER['REMOTE_ADDR'] . "|" . $contrato . "|L|" . $_SESSION["id_usuario"] . "|" . date("d/m/Y H:i")), "VipService123", true);    
    $query = "insert into sf_whatsapp (telefone, mensagem, data_envio, fornecedor_despesas, sys_login, status)
    select (select top 1 replace(replace(replace(replace(isnull(conteudo_contato,''), '(',''), ')',''), ' ',''), '-','') from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = c.id_fornecedores_despesas) celular," .
    utf8_decode("'Olá *' + login_user + '*, uma tentativa de acesso foi feita ao seu usuário foi feita pelo ip: *" . $_SERVER['REMOTE_ADDR'] . "* dia *" . date("d/m/Y H:i") . "* %0a
    Caso esteja tentando fazer esse acesso, acesse o link abaixo para liberar%0a Se não o acesso continuará bloqueado.%0a%0a ") .
    "https://sivisweb.com.br/Modulos/Sivis/ws_security.php?link=" . $link_go . "', 
    getdate(), c.id_fornecedores_despesas, login_user, 0 from sf_usuarios u 
    inner join sf_fornecedores_despesas c on u.funcionario = c.id_fornecedores_despesas
    where u.id_usuario = " . $_SESSION["id_usuario"] . " and id_fornecedores_despesas not in (select fornecedor_despesas from sf_whatsapp where dateadd(minute,5,data_envio) > getdate());";
    odbc_exec($con, $query) or die(odbc_errormsg());
}

if (($white_total_ > 0 && $white_count_ == 0) || ($black_count_ > 0)) {    
    header("Location: ./../../login.php");
    exit;
}

include "funcoesAux.php";