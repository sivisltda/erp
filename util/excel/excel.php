<?php

$j = 1;
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');
$colunas = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
"AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ");

require_once ('PHPExcel.php');
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("")->setTitle("Office 2007 XLSX Test Document")->setSubject("Office 2007 XLSX Test Document")->setDescription("Test document for Office 2007 XLSX")->setKeywords("office 2007 openxml php");
$objPHPExcel->getActiveSheet()->setTitle("Plan1");
$BStyle = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $j, $relatorio)->mergeCells('A' . $j . ':' . $colunas[count($tamanhos) - 1] . $j)->getStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A' . $j . ':' . $colunas[count($tamanhos) - 1] . $j)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('D3D3D3');
if (strlen($titulo) > 0) {
    $j++;
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $j, $titulo)->mergeCells('A' . $j . ':' . $colunas[count($tamanhos) - 1] . $j)->getStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A' . $j . ':' . $colunas[count($tamanhos) - 1] . $j)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('DCDCDC');
}
$j++;
for ($i = 0; $i < count($tamanhos); $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($colunas[$i] . $j, strip_tags($titulos[$i]));
}
$objPHPExcel->getActiveSheet()->getStyle('A' . $j . ':' . $colunas[count($tamanhos) - 1] . $j)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('696969');
$phpColor = new PHPExcel_Style_Color();
$phpColor->setRGB('FFFFFF');
$objPHPExcel->getActiveSheet()->getStyle('A' . $j . ':' . $colunas[count($tamanhos) - 1] . $j)->getFont()->setColor($phpColor);
$j++;
foreach ($output['aaData'] as $person) {
    $k = 0;
    for ($i = 0; $i < count($person); $i++) {
        if (!in_array($i, $invisivel)) {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($colunas[$k] . $j, strip_tags((isset($person[$i]) ? $person[$i] : "")));
            $k++;
        }
    }
    $j++;
}
for ($i = 0; $i < count($tamanhos); $i++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($colunas[$i])->setWidth($tamanhos[$i]);
}
$objPHPExcel->getActiveSheet()->getStyle('A1:' . $colunas[count($tamanhos) - 1] . ($j - 1))->applyFromArray($BStyle);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $_GET["NomeArq"] . '.xlsx"');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: cache, must-revalidate');
header('Pragma: public');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');