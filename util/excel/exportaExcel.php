<?php

function exportaExcel($dados, $topo) {
    if (isset($_GET['ex']) && $_GET['ex'] == 1) {
        $arquivo = 'planilha.xls';
        $html = '';
        $html .= '<table>';
        $html .= $topo;
        $table = $dados;
        for ($i = 0; $i < count($table); $i++) {
            $html .= '<tr>';
            for ($j = 0; $j < count($table[$i]); $j++) {
                $html .= '<td>' . $table[$i][$j] . '</td>';
            }
            $html .= '</tr>';
        }
        $html .= '</table>';
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: application/x-msexcel");
        header("Content-Disposition: attachment; filename=\"{$arquivo}\"");
        header("Content-Description: PHP Generated Data");
        echo "\xEF\xBB\xBF";
        echo $html;
        exit;
    }
}
