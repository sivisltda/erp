<?php

//https://finance.dragon296.startdedicated.com/util/gympass/webhooks.php?l=109&c=6
$read = file_get_contents("php://input");
$x = json_decode($read, true);

$loja = $_GET["l"];
$credencial = $_GET["c"];
$tipo = $x["event_type"];
$usuario = $x["event_data"]["user"]["unique_token"];
$lat = $x["event_data"]["location"]["lat"];
$lon = $x["event_data"]["location"]["lon"];
$gym_id = $x["event_data"]["gym"]["id"];
$gym_title = $x["event_data"]["gym"]["title"];
$gym_prod_id = $x["event_data"]["gym"]["product"]["id"];
$gym_prod_desc = $x["event_data"]["gym"]["product"]["description"];
$gym_data = $x["event_data"]["timestamp"];
$auth_token = "";

$content = date("d/m/Y H:i") . "\r\n";
$content .= "\r\n";
$content .= "Loja: " . $loja . " (" . $credencial . ")\r\n";
$content .= "Tipo: " . $tipo . "\r\n";
$content .= "Usuário: " . $usuario . "\r\n";
$content .= "Local: (" . $lat . ") (" . $lon . ")\r\n";
$content .= "Academia: " . $gym_id . "-" . $gym_title . "\r\n";
$content .= "Produto: " . $gym_prod_id . "-" . $gym_prod_desc . "\r\n";
$content .= "Data: " . date("d/m/Y H:i:s", $gym_data) . "\r\n";

if (is_numeric($loja) && is_numeric($credencial)) {
    include("./../../Connections/configSivis.php");
    include("./../../Connections/funcoesAux.php");        
    $hostname = "";
    $cur2 = odbc_exec($con2, "select *,(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = id_fornecedores_despesas) email
    from ca_contratos
    left join sf_fornecedores_despesas on id_fornecedores_despesas = id_cliente 
    left join tb_estados on estado_codigo = estado
    left join tb_cidades on cidade_codigo = cidade 
    where inativa = 0 and numero_contrato = '" . $loja . "'");
    while ($RFP = odbc_fetch_array($cur2)) {
        $hostname = utf8_encode($RFP['local']);
        $username = utf8_encode($RFP['login']);
        $password = utf8_encode($RFP['senha']);
        $database = utf8_encode($RFP['banco']);
    }
    odbc_close($con2);
    if (strlen($hostname) > 0) {
        $pessoa = 0;
        $total = 0;
        $nome_pessoa = "Não Identificado";
        $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
        $cur = odbc_exec($con, "select id_fornecedores_despesas, razao_social, (select count(id_fornecedores_despesas_credencial)
        from sf_fornecedores_despesas_credenciais where sf_fornecedores_despesas_credenciais.id_fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas 
        and dt_cancelamento is null and dt_inicio = cast(getdate() as date) and id_credencial = " . $credencial . ") total 
        from sf_fornecedores_despesas where gyp_idaluno = " . valoresTexto2($usuario));
        while ($RFP = odbc_fetch_array($cur)) {
            $pessoa = utf8_encode($RFP['id_fornecedores_despesas']);
            $nome_pessoa = utf8_encode($RFP['razao_social']);
            $total = utf8_encode($RFP['total']);
        }
        $cur = odbc_exec($con, "select gyp_recid,gyp_recid2,gyp_ptype,gyp_ptype2,gyp_token,gyp_token2 from sf_configuracao");
        while ($RFP = odbc_fetch_array($cur)) {
            if ($gym_id == $RFP['gyp_recid']) {
                $auth_token = utf8_encode($RFP['gyp_token']);
            } else if ($gym_id == $RFP['gyp_recid2']) {
                $auth_token = utf8_encode($RFP['gyp_token2']);                
            }
        }
        if ($pessoa > 0 && $total == 0) {
            odbc_exec($con, "set dateformat dmy; insert into sf_fornecedores_despesas_credenciais(id_fornecedores_despesas,id_credencial, dt_inicio, dt_fim, motivo, usuario_resp, dt_cadastro) values
            (" . $pessoa . "," . $credencial . ",GETDATE(),GETDATE(),'GYM PASS',1, GETDATE())") or die(odbc_errormsg());
            odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
            values ('sf_fornecedores_despesas_credenciais', " . $credencial . ", 'ADMIN', 'C', 'INCLUIR - CREDENCIAL - GYMPASS', GETDATE(), " . $pessoa . ")");
            odbc_exec($con, "UPDATE sf_fornecedores_despesas set fornecedores_status = dbo.FU_STATUS_CLI(" . $pessoa . ") where id_fornecedores_despesas = " . $pessoa);
        }
        $content .= "Pessoa: " . $pessoa . "-" . $nome_pessoa . " (" . $total . ")\r\n";
        $content .= "-------------------------------------------------------------\r\n";
        if(strlen($auth_token) > 0 && $pessoa > 0 && $total == 0) {
            $url = "https://www.gympass.com/transactions/validate_number.json?pass_number=" . $usuario . "&gym_number=" . $gym_id . "&auth_token=" . $auth_token;
            $json = file_get_contents($url);
            $y = json_decode($json, true);
            $content .= $y["status"]["code"] . " - " . $y["status"]["description"] . "\r\n";
            $content .= $y["results"]["person_name"] . "\r\n";
            $content .= $y["results"]["gym_title"] . "\r\n";
            $content .= $y["results"]["pass_type_title"] . "\r\n";
            $content .= $y["results"]["unique_token"] . "\r\n";
            $content .= $y["results"]["transaction_token"] . "\r\n";
        }
        odbc_close($con);
    }
}

$dir = "./../../Pessoas/003/Webhooks/gympass/" . $loja . "/" . date("Y/m/d") . "/";
if (!file_exists($dir)) {
    mkdir($dir, 0777, true);
}
$myfile = fopen($dir . date("Y_m_d-H_i_s") . ".txt", "w") or die("Unable to open file!");
fwrite($myfile, $content);
fclose($myfile);

echo "OK";
