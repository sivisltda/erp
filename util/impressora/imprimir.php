<?php
require_once('tcpdf.php');

class MYPDF extends TCPDF {
    public function ColoredTable($header, $data, $dataGrupo, $dataTotal, $x, $invisivel) {                    
        $arrayDetalhes = array();
        for ($i = 0; $i < count($data); $i++) {
            $itensPag = array();
            $linha = $data[$i];
            for ($j = 0; $j < count($linha); $j ++) {
                if (!in_array($j, $invisivel)) {
                    $itensPag[] = strip_tags($linha[$j]);
                }
            }
            $arrayDetalhes[] = $itensPag;
        }   
        $totGroup = (is_array($dataGrupo) ? count($dataGrupo) : 0);
        for ($j = 0; $j < ($totGroup == 0 ? 1 : $totGroup); $j++) { 
            $total = 0;
            $totalDetalhes = 0;
            $this->SetFillColor(211, 211, 211);
            $this->SetLineWidth(0);
            $this->SetFont('', 'B');
            $num_headers = count($header);
            if($totGroup > 0) {
                $this->Cell(array_sum($x), 6, strtoupper(utf8_decode($dataGrupo[$j][1])), 'LTR', 0, 'C', 1);                                             
                $this->Ln();                
            }
            for ($i = 0; $i < $num_headers; $i++) {
                $this->Cell($x[$i], 6, strtoupper(utf8_decode($header[$i])), ($i == $num_headers - 1 ? 'LTRB' : 'LTB'), 0, 'C', 1);
            }            
            $this->Ln();
            $this->SetFont('');
            foreach ($arrayDetalhes as $row) {
                if ($totGroup == 0 || $dataGrupo[$j][0] == $row[$dataGrupo[$j][2]]) { 
                    $total++; $totalPage++; $totalDetalhes++;
                    if (is_array($dataTotal) && count($dataTotal) > 0) { 
                        for ($i = 0; $i < count($dataTotal); $i++) {                            
                            $dataTotal[$i][4] += valoresNumericos2($row[$dataTotal[$i][0]]);
                            $dataTotal[$i][5] += valoresNumericos2($row[$dataTotal[$i][0]]);
                        }
                    }
                    for ($i = 0; $i < $num_headers; $i++) {
                        $this->Cell($x[$i], 6, strip_tags(utf8_decode($row[$i])), ($i == 0 ? 'LRB' : 'RB'), 0, 'L', 0, '', 1, false, 'T', 'M');
                    }
                    $this->Ln();
                }
            }
            if(is_array($dataTotal) && count($dataTotal) > 0) {
                $this->SetFont('', 'B');
                $this->Cell($x[0], 6, "Tot.Reg.:", 'LRB', 0, 'L', 0, '', 1, false, 'T', 'M');
                $this->Cell($x[1], 6, $total, 'RB', 0, 'L', 0, '', 1, false, 'T', 'M');
                for ($i = 2; $i < $num_headers; $i++) {
                    $conteudo = "";
                    for ($k = 0; $k < count($dataTotal); $k++) {
                        if ($dataTotal[$k][0] == $i) {
                            $conteudo = isset($dataTotal[$k][6]) && $dataTotal[$k][6] == 0 ? escreverNumero($dataTotal[$k][4],0) : escreverNumero($dataTotal[$k][4],1);            
                            $dataTotal[$k][4] = 0;
                        } elseif (($dataTotal[$k][0] - 1) == $i && $dataTotal[$k][2] != "") {
                            $conteudo = $dataTotal[$k][2];
                        }
                    }                    
                    $this->Cell($x[$i], 6, $conteudo, 'RB', 0, 'L', 0, '', 1, false, 'T', 'M');
                }
                $this->Ln();
                $this->Ln();                
            } else {                         
                $this->Cell(array_sum($x), 6, 'Total de Registros: ' . $totalDetalhes, 'LRB');
                $totalDetalhes = 0;
                $this->Ln();
                $this->Ln();
            }                              
        } if($totGroup > 0) {           
            $this->Cell($x[0], 6, "Tot.Reg.:", 'LTRB', 0, 'L', 0, '', 1, false, 'T', 'M');
            $this->Cell($x[1], 6, $totalPage, 'TRB', 0, 'L', 0, '', 1, false, 'T', 'M');
            for ($i = 2; $i < $num_headers; $i++) {
                $conteudo = "";
                if(is_array($dataTotal) && count($dataTotal) > 0) {
                    for ($j = 0; $j < count($dataTotal); $j++) {
                        if ($dataTotal[$j][1] == $i) {
                            $conteudo = isset($dataTotal[$j][6]) && $dataTotal[$j][6] == 0 ? escreverNumero($dataTotal[$j][5],0) : escreverNumero($dataTotal[$j][5],1);           
                        } elseif (($dataTotal[$j][1] - 1) == $i && $dataTotal[$j][3] != "") {
                            $conteudo = utf8_decode($dataTotal[$j][3]);
                        }
                    }       
                }
                $this->Cell($x[$i], 6, $conteudo, 'TRB', 0, 'L', 0, '', 1, false, 'T', 'M');
            }
        }       
    }
}
   
$fonte = (isset($_GET["Fonte"]) ? $_GET["Fonte"] : 6.5);
$pagina = ($_GET["pOri"] == "L" ? "L" : "P");
$empresa = str_replace("<br>", "\n", $_SESSION["cabecalho"]);
$pdf = new MYPDF($pagina, 'mm', 'A4', false, 'UTF-8', false);
$pdf->SetTitle('Imprimir');
$pdf->setFooterFont(Array('helvetica', '', 8));
$pdf->SetMargins(5, 5, 5);
$pdf->SetHeaderMargin(5);
$pdf->SetFooterMargin(10);
$pdf->SetAutoPageBreak(true, 11);
$pdf->AddPage();
$pdf->SetFont('helvetica', '', 6.5);
$pdf->MultiCell(($_GET["pOri"] == "L" ? 227 : 140), 20, utf8_decode($empresa), 'LB', 'R', 0, 0, '', '', true, 0, false, true, 20, 'M');
$pdf->SetFont('helvetica', 'B', 12);
$pdf->MultiCell(60, 14, utf8_decode($relatorio), 'LRB', 'C', 0, 1, '', '', true, 0, false, true, 14, 'M');
$pdf->SetFont('helvetica', '', 8);
$pdf->MultiCell(30, 6, "Data:", 'LB', 'C', 0, 0,($_GET["pOri"] == "L" ? '232' : '145'), '', true, 0, false, true, 6, 'M');
$pdf->MultiCell(30, 6, getData("T"), 'LRB', 'C', 0, 1, '', '', true, 0, false, true, 6, 'M');
if (file_exists($imagem)) {
    $pdf->Image($imagem, 8, 8, 42, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);
    $pdf->Ln(20);
} else {
    $pdf->Ln(3);
}
$pdf->Write(0, utf8_decode($titulo), '', 0, 'L', true, 0, false, false, 0);
$pdf->Ln(3);
$pdf->SetFont('helvetica', '', $fonte);
$pdf->ColoredTable($titulos, $output['aaData'], $output["aaGrupo"], $output["aaTotal"], $tamanhos, $invisivel);
$pdf->Output('imprimir.pdf', 'I');