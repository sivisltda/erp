<?php

require_once('tcpdf_include.php');
require_once('../tcpdf.php');
require_once('fpdi2/src/autoload.php');

$pdf = new \setasign\Fpdi\TcpdfFpdi(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('ERP');

$pdf->AddPage();
//$pdf->Write(0, 'page 2 created by TCPDF');
$pdf->Cell(0, 0, 'PAGE 1', 1, 1, 'C');
$pdf->Image('images/image_demo.jpg', 11, 20, 170, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);

$pages = $pdf->setSourceFile('middle.pdf');
for ($i = 0; $i < 1; $i++) {
    $pdf->AddPage();
    $pdf->Cell(0, 0, 'PAGE 2', 1, 1, 'C');
    $tplIdx = $pdf->importPage($i + 1);
    $pdf->useTemplate($tplIdx, 11, 20, 180);
}

$pdf->Output('imprimir.pdf', 'I');
