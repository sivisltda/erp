<?php

header('Content-Type: text/html; charset=utf-8');
require('../src/eNotasGW.php');

use eNotasGW\Api\Exceptions as Exceptions;

eNotasGW::configure(array(
    'apiKey' => 'MWVmYmU4NmYtM2NiZC00MmQ4LWEyZGMtMTk4OGQ0NjAwMjAw'
));
$empresaId = 'FD460D8A-E63E-47A1-94F8-2F0055620200';

try {
    $nfeId = '75afd644-d5f8-4aa4-b27c-cc5c2c6e0200';
    $dadosNFe = eNotasGW::$NFeApi->consultar($empresaId, $nfeId);
    echo 'Dados da nota (consulta por id único): </br>';
    echo json_encode($dadosNFe);
    $idExterno = '1';
    $dadosNFe2 = eNotasGW::$NFeApi->consultarPorIdExterno($empresaId, $idExterno);
    echo '</br></br>';
    echo 'Dados da nota (consulta por id externo): </br>';
    echo json_encode($dadosNFe2);
} catch (Exceptions\invalidApiKeyException $ex) {
    echo 'Erro de autenticação: </br></br>';
    echo $ex->getMessage();
} catch (Exceptions\unauthorizedException $ex) {
    echo 'Acesso negado: </br></br>';
    echo $ex->getMessage();
} catch (Exceptions\apiException $ex) {
    echo 'Erro de validação: </br></br>';
    echo $ex->getMessage();
} catch (Exceptions\requestException $ex) {
    echo 'Erro na requisição web: </br></br>';
    echo 'Requested url: ' . $ex->requestedUrl;
    echo '</br>';
    echo 'Response Code: ' . $ex->getCode();
    echo '</br>';
    echo 'Message: ' . $ex->getMessage();
    echo '</br>';
    echo 'Response Body: ' . $ex->responseBody;
}