<?php

header('Content-Type: text/html; charset=utf-8');
require('../src/eNotasGW.php');

use eNotasGW\Api\Exceptions as Exceptions;

eNotasGW::configure(array(
    'apiKey' => 'MWVmYmU4NmYtM2NiZC00MmQ4LWEyZGMtMTk4OGQ0NjAwMjAw'
));
$empresaId = 'FD460D8A-E63E-47A1-94F8-2F0055620200';
$idExterno = '0';
//------------------------------------------------------------------------------
$infAdFisco = 'CONTRATO DE MANUTENÇÃO  DE 27 RELÓGIOS DE PONTO MD REP.  REF. 03/12.';
$CNAE = '4751201';
$Aliquota = 3.84;
$ufPreSer = 'RJ';
$cidPreSer = 'Volta Redonda';
//------------------------------------------------------------------------------
$xNome = 'CBSI - CIA BRASILEIRA DE SERV. DE INFRAESTRUTURA';
$CNPJ = '13623957000217';
$CPF = '';
$IE = '79472760';
$IM = '';
$email = 'fabricio@sivis.com.br';
$fone = '(24) 3512-3100';
$xLgr = 'RUA 16';
$nro = '186';
$xCpl = 'EDIFICIO MILENIO';
$xBairro = 'VILA SANTA CECILIA';
$cMun = '3306305';
$xMun = 'Volta Redonda';
$UF = 'RJ';
$CEP = '27260110';
//------------------------------------------------------------------------------
$xProd = 'CONTRATO DE MANUTENÇÃO MENSAL';
//------------------------------------------------------------------------------
$issq = false;
$vNF = 1404.00;
//------------------------------------------------------------------------------

try {
    $post = array(
        "tipo" => "NFS-e", //Tipo da Nota Fiscal. "NFS-e" para NFe de Serviço e "NF-e" para NFe de Produto
        "ambienteEmissao" => "Homologacao",        
        //"id" => "string", //Identificador único da Nota Fiscal. Usado apenas em casos de atualização de uma nota fiscal existente
        "idExterno" => $idExterno, //Identificador externo da Nota Fiscal informado pelo consumidor da API ao emitir a Nota Fiscal        
        "consumidorFinal" => true, //Utilizado somente para Nota Conjugada (ex: Brasília/DF). Indica se a nota é para consumidor final seja ele Pessoa Física ou Jurídica. Caso não seja informado será considerado o valor padrão = true        
        "enviarPorEmail" => true, //Indica se esta nota deve ser enviada para o email do cliente.
        "indicadorPresencaConsumidor" => "NaoSeAplica", //Utilizado somente para Nota Conjugada (ex: Brasília/DF). Indica a presença do consumidor para recebimento do serviço. Caso não seja informado será considerado o valor padrão = "NaoSeAplica". Possíveis valores: "NaoSeAplica", "OperacaoPresencial", "OperacaoPelaInternet", "OperacaoTeleAtendimento", "Outros".
        "cliente" => array(          
            "tipoPessoa" => ($CNPJ == '' ? "F" : "J"), //"F" para pessoa física e "J" para pessoa jurídica
            "nome" => $xNome, //Nome do cliente
            "cpfCnpj" => ($CNPJ == '' ? $CPF : $CNPJ), //CPF para pessoa física ou CNPJ para pessoa jurídica
            "inscricaoEstadual" => $IE, //Inscrição estadual do cliente, deve ser preenchido apenas para pessoa Jurídica           
            "inscricaoMunicipal" => $IM, //Inscrição municipal do cliente, deve ser preenchido apenas para pessoa física ou Jurídica do mesmo município que o Prestador
            //"indicadorContribuinteICMS" => "string",            
            "email" => $email, //Endereço de email
            "telefone" => $fone, //Telefone do cliente
            "endereco" => array(
                "logradouro" => $xLgr,
                "numero" => $nro,
                "cidade" => $xMun, //Nome ou código IBGE da cidade
                "uf" => $UF,
                "complemento" => $xCpl,
                "bairro" => $xBairro,                
                "cep" => $CEP,
                "pais" => "BRA" //Sigla do País com 3 dígitos. Exemplo: BRA - Brasil, ARG - Argentina. Para maiores detalhes consulte http://portal.enotasgw.com.br/article-categories/paises. Caso não seja informado será considerado como padrão o valor "BRA".               
            )
        ),
        "servico" => array( //Detalhamento do serviço prestado
            "ufPrestacaoServico" => $ufPreSer, //Sigla do Estado onde o serviço foi prestado. Opcional caso o serviço tenha sido prestado no mesmo munícipio do prestador.
            "municipioPrestacaoServico" => $cidPreSer, //Nome ou código IBGE do munícipio onde o serviço foi prestado. Opcional caso o serviço tenha sido prestado no mesmo munícipio do prestador.                         
            "descricao" => $xProd, //Descrição do serviço prestado.
            "aliquotaIss" => $Aliquota, //Valor da aliquota Iss. Caso não seja informado será considerado o valor padrão configurado no cadastro da empresa (CNPJ emissor).            
            "cnae" => $CNAE, //Código CNAE que identifica o serviço prestado. Caso não seja informado será considerado o valor padrão configurado no cadastro da empresa (CNPJ emissor). 
            //"codigoServicoMunicipio" => "string", //Código do serviço municipal conforme cadastro na prefeitura. Caso não seja informado será considerado o valor padrão configurado no cadastro da empresa (CNPJ emissor). Este código varia de acordo com a prefeitura, para maiores detalhes consulte http://portal.enotasgw.com.br/article-categories/prefeituras 
            "descricaoServicoMunicipio" => $infAdFisco, //Descrição do serviço municipal conforme cadastro na prefeitura. Caso não seja informado será considerado o valor padrão configurado no cadastro da empresa (CNPJ emissor). Este descrição varia de acordo com a prefeitura, para maiores detalhes consulte http://portal.enotasgw.com.br/article-categories/prefeituras
            //"itemListaServicoLC116" => "string", //Item da lista de serviço conforme a Lei Complementar 116 (LC116). Para maiores detalhes consulte http://portal.enotasgw.com.br/article-categories/lista-servicos-lc116                        
            "issRetidoFonte" => $issq, //Indica se o Iss deverá ser retido na fonte. Retenção fonte ISSQN ? Padrão é (false), estava true
            "valorInss" => 0, //Valor do INSS
            "valorIr" => 0, //Valor do IR            
            "valorCsll" => 0, //Valor do CSLL
            "valorPis" => 0, //Valor do PIS
            "valorCofins" => 0 //Valor do COFINS
        ),
        "valorTotal" => $vNF, //Valor total da Nota Fiscal
        //"idExternoSubstituir" => "string", //Id externo da nota que deseja substituir, apenas um dos parametros devem ser informados "idExternoSubstituir" ou "nfeIdSubstitituir"
        //"nfeIdSubstitituir" => "string" //Id único da nota que deseja substituir, apenas um dos parametros devem ser informados "idExternoSubstituir" ou "nfeIdSubstitituir"
    );    
    //print_r($post);
    //exit;    
    $nfeId = eNotasGW::$NFeApi->emitir($empresaId,$post);
    echo $nfeId;
} catch (Exceptions\invalidApiKeyException $ex) {
    echo 'Erro de autenticação: </br></br>';
    echo $ex->getMessage();
} catch (Exceptions\unauthorizedException $ex) {
    echo 'Erro Acesso negado: </br></br>';
    echo $ex->getMessage();
} catch (Exceptions\apiException $ex) {
    echo 'Erro de validação: </br></br>';
    echo $ex->getMessage();
} catch (Exceptions\requestException $ex) {
    echo 'Erro na requisição web: </br></br>';
    echo 'Requested url: ' . $ex->requestedUrl;
    echo '</br>';
    echo 'Response Code: ' . $ex->getCode();
    echo '</br>';
    echo 'Message: ' . $ex->getMessage();
    echo '</br>';
    echo 'Response Body: ' . $ex->responseBody;
}