<?php

$path = "../Pessoas/" . $_SESSION['contrato'] . "/";
$zip = new ZipArchive;
$i = 0;
if (file_exists($path . "fotos.zip")) {
    unlink($path . "fotos.zip");
}
if ($zip->open($path . "fotos.zip", ZipArchive::CREATE) === TRUE) {
    foreach ($output['aaData'] as $person) {
        $pathArq = $path . "Clientes/" . strip_tags($person[0]) . ".png";
        if (file_exists($pathArq)) {
            $zip->addFile($pathArq, strip_tags($person[0]) . ".png");
            $i++;
        }
    }
    $zip->close();
    if ($i > 0) {
        header('Content-Type: application/zip');
        header('Content-Disposition: attachment; filename=fotos.zip');
        header('Pragma: no-cache');
        readfile($path . "fotos.zip");
    } else {
        echo "Nenhuma imagem encontrada para esta operação!";
        exit;
    }
}
