<?php

$login_usuario = 'SISTEMA';

if (isset($_GET["hostname"]) && isset($_GET["database"]) && isset($_GET["username"]) && isset($_GET["password"])) {
    $hostname = $_GET["hostname"];
    $database = $_GET["database"];
    $username = $_GET["username"];
    $password = $_GET["password"];
    $contrato = str_replace("ERP", "", strtoupper($_GET['database']));
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
    include "./../../Connections/funcoesAux.php";
}

if (isset($_GET["txtValor"])) {
    $_POST["txtValor"] = $_GET["txtValor"];
}

if (isset($_GET["txtValorReal"])) {
    $_POST["txtValorReal"] = $_GET["txtValorReal"];
}

if (isset($_GET["txtPlanoItem"])) {
    $_POST["txtPlanoItem"] = $_GET["txtPlanoItem"];
}

if (isset($_SESSION["login_usuario"]) && strlen($_SESSION["login_usuario"]) > 0) {
    $login_usuario = strtoupper($_SESSION["login_usuario"]);
} elseif (isset($_GET["login_usuario"]) && strlen($_GET["login_usuario"]) > 0) {
    $login_usuario = strtoupper($_GET["login_usuario"]);
}

if (valoresNumericos("txtValor") > 0 && valoresNumericos("txtValor") < 10000) {
    $identificacao = "";
    $informativo = "";
    $produto = null;
    $credencial = null;
    $cur = odbc_exec($con, "select dt_inicio_mens,id_prod_plano, favorecido, p.descricao, v.placa, v.modelo
    from sf_vendas_planos_mensalidade m
    inner join sf_vendas_planos l on id_plano_mens = id_plano 
    inner join sf_produtos p on p.conta_produto = l.id_prod_plano
    left join sf_fornecedores_despesas_veiculo v on l.id_veiculo = v.id
    where id_mens = " . valoresSelect("txtPlanoItem"));
    while ($RFP = odbc_fetch_array($cur)) {
        $dt_mensalidade = date_format(date_create($RFP['dt_inicio_mens']), 'd/m/Y');
        $identificacao = "Mens." . str_replace(array("Feb", "Apr", "May", "Aug", "Sep", "Oct", "Dec"), array("Fev", "Abr", "Mai", "Ago", "Set", "Out", "Dez"), date_format(date_create($RFP['dt_inicio_mens']), 'M/Y'));
        $informativo = utf8_encode($RFP['descricao']) . (strlen(utf8_encode($RFP['placa'])) > 0 ? " [" . utf8_encode($RFP['placa']) . "] - " . utf8_encode($RFP['modelo']) : "");        
        $produto = $RFP['id_prod_plano'];
    }
    if (is_numeric($produto) && strlen($dt_mensalidade) > 0) {
        $query = "SELECT TOP 1 favorecido, razao_social, cnpj, data_nascimento, juridico_tipo,            
        (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = id_fornecedores_despesas AND tipo_contato = 2) as email,
        (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = id_fornecedores_despesas AND tipo_contato = 1) as celular,
        endereco, numero, complemento, bairro, cep, 
        (SELECT TOP 1 cidade_nome FROM tb_cidades WHERE cidade_codigo = fd.cidade) as cidade,
        (SELECT TOP 1 estado_sigla FROM tb_estados WHERE estado_codigo = fd.estado) as estado,
        pm.dt_inicio_mens, b.id_boleto, b.bol_data_parcela, b.bol_valor, bol_descricao, vp.prev_mens, vp.id_plano_mens,
        vp.id_prim_mens, pm.id_mens, 
        case when (pm.dt_inicio_mens >= cast(getdate() as date) and id_prim_mens = pm.id_mens) then dateadd(day,3,pm.dt_inicio_mens) 
        when pm.dt_inicio_mens <= cast(getdate() as date) then dateadd(day,2,getdate())
        else pm.dt_inicio_mens end as dt_vencimento_mens
        from sf_vendas_planos_mensalidade pm
        inner join (select vpm.*, vp.favorecido, 
                (select max(id_mens) from sf_vendas_planos_mensalidade where id_plano_mens = vpm.id_plano_mens and dt_inicio_mens < vpm.dt_inicio_mens) as prev_mens,
                (SELECT TOP 1 id_mens FROM sf_vendas_planos_mensalidade where id_plano_mens = vp.id_plano order by dt_inicio_mens asc) as id_prim_mens
                from sf_vendas_planos vp inner join sf_vendas_planos_mensalidade vpm ON vpm.id_plano_mens = vp.id_plano where vp.planos_status not in ('Cancelado')
        ) vp on vp.id_mens = pm.id_mens
        inner join sf_produtos_parcelas pp on pp.id_parcela = pm.id_parc_prod_mens
        inner join sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = vp.favorecido
        left join sf_boleto b on b.id_referencia = pm.id_mens and b.tp_referencia = 'M' and (b.inativo = 0 or b.inativo is null)
        where pm.id_mens = " . valoresSelect("txtPlanoItem") . " and pm.dt_pagamento_mens is null";
        $cur = odbc_exec($con, $query);
        $data = [];
        while ($RFP = odbc_fetch_array($cur)) {
            $data = [
                'id_mens' => $RFP['id_mens'],
                'id_aluno' => $RFP['favorecido'],
                'nome' => trim(utf8_encode($RFP['razao_social'])),
                'data_nascimento' => trim(escreverData($RFP['data_nascimento'])),
                'email' => trim(utf8_decode($RFP['email'])),
                'celular' => trim(utf8_decode($RFP['celular'])),
                'cnpj' => trim($RFP['cnpj']),
                'juridico_tipo' => $RFP['juridico_tipo'],
                'endereco' => utf8_encode($RFP['endereco']),
                'numero' => $RFP['numero'],
                'complemento' => utf8_encode($RFP['complemento']),
                'bairro' => utf8_encode($RFP['bairro']),
                'cidade' => utf8_encode($RFP['cidade']),
                'estado' => utf8_encode($RFP['estado']),
                'cep' => $RFP['cep'],
                'data_inicio' => trim($RFP['dt_inicio_mens']),
                'id_mens_ant' => $RFP['prev_mens'],
                'id_prim_mens' => $RFP['id_prim_mens'],
                'id_boleto' => $RFP['id_boleto'],
                'data_venc_boleto' => $RFP['bol_data_parcela'],
                'url_boleto' => utf8_encode($RFP['bol_descricao']),
                'valor_boleto' => escreverNumero($RFP['bol_valor'], 0, 2, '.', ''),
                'data_vencimento' => trim($RFP['dt_vencimento_mens']),
                'valor' => valoresNumericos2($_GET["txtValor"]),
                'valor_real' => valoresNumericos2($_GET["txtValorReal"])
            ];
        }        
        $cur = odbc_exec($con, "select ACA_TIPO_MULTA, ACA_DESC_TIPO,
        ACA_TAXA_MULTA,ACA_MORA_MULTA,PIX_FORMA_PAGAMENTO, 
        ACA_DIAS_LIMITE, ACA_DESC_DIAS, ACA_DESC_VALOR,
        dcc_pix_tipo,PIX_CHAVE,dcc_galaxid,dcc_galaxpay,dcc_galaxteste from sf_configuracao where id = 1");
        while ($RFP = odbc_fetch_array($cur)) {
            $data["tipo_multa"] = $RFP['ACA_TIPO_MULTA'];
            $data["tipo_desconto"] = $RFP['ACA_DESC_TIPO'];            
            $data["dias_limite"] = $RFP['ACA_DIAS_LIMITE'];
            $data["dias_desconto"] = $RFP['ACA_DESC_DIAS'];                
            $data["taxa_multa"] = escreverNumero($RFP['ACA_TAXA_MULTA'], 0, 2, '.', '');
            $data["mora_multa"] = escreverNumero($RFP['ACA_MORA_MULTA'], 0, 2, '.', '');             
            $data["valor_desconto"] = escreverNumero($RFP['ACA_DESC_VALOR'], 0, 2, '.', '');            
            $data["informativo"] = $informativo;            
            $credencial = $RFP['dcc_pix_tipo'];                
            if ($credencial == 1) {
                $chave = $RFP['PIX_CHAVE'];
            } else if ($credencial == 2) {
                $config = ['galaxId' => $RFP['dcc_galaxid'], 'galaxHash' => $RFP['dcc_galaxpay'], 'sandbox' => ($RFP['dcc_galaxteste'] == 1)];
            }
            $pix_forma_pagamento = $RFP['PIX_FORMA_PAGAMENTO'];            
        }
        if (is_numeric($pix_forma_pagamento) && isset($data['id_aluno']) && is_numeric($data['id_aluno'])) {
            if ($data['id_boleto']) {
                if ($data['valor'] == $data['valor_boleto']) {
                    echo $data['id_boleto'];
                } else {
                    echo "Cobrança PIX em aberto!";
                }
                exit;                
            } else {
                //TODO: Verificar se o ultimo boleto foi pago após o vencimento e calcular o juros do mesmo caso tiver
            }            
            if ($credencial == 1 && strlen($chave) > 0) {
                include "./U4cdev/loadClassPix.php";                
                $u4cdev = makeChargeRequestPix($data, $identificacao, $chave);
                $last_transaction = mkJson($u4cdev);
                if (is_numeric($last_transaction->externalId)) {
                    $query = "set dateformat dmy; insert into sf_boleto(id_referencia, tp_referencia, bol_id_banco, carteira_id, bol_data_criacao, bol_data_parcela, bol_valor, 
                    bol_descricao, bol_nosso_numero, tipo_documento, sa_descricao, exp_remessa, syslogin, inativo, mundi_id, mundi_code, mundi_gateway_id, exp_email)
                    values(" . valoresSelect("txtPlanoItem") . ", 'M', 1, null, getdate(), '" . escreverData($u4cdev->billingDueDate->dueDate) . "', " . valoresNumericos("txtValor") . ", " . 
                    valoresTexto2("https://sivisweb.com.br/Boletos/boleto_pix.php?id=M-" . $u4cdev->externalId . "&crt=" . $contrato) . ", null, " . $pix_forma_pagamento . ", " . 
                    valoresTexto2($last_transaction->data->textContent) . ", 1, " . valoresTexto2($login_usuario) . ", 0," . valoresTexto2($last_transaction->data->reference) . ", " . 
                    valoresTexto2($last_transaction->data->generatedImage->imageContent) . "," . valoresTexto2($last_transaction->itemId) . ", 0);SELECT SCOPE_IDENTITY() ID;";
                    $result = odbc_exec($con, $query) or die(odbc_errormsg());
                    odbc_next_result($result);
                    $id = odbc_result($result, 1);
                    if (is_numeric($id)) {
                        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) 
                        select 'sf_boleto', id_mens, '" . $login_usuario . "', 'I', 'CADASTRO PIX: ' + cast(bol_nosso_numero as varchar), GETDATE(), favorecido 
                        from sf_boleto inner join sf_vendas_planos_mensalidade on id_referencia = id_mens
                        inner join sf_vendas_planos on id_plano_mens = id_plano where id_boleto = " . $id);
                        echo $id;
                    } else {
                        echo "Ocorreu um erro ao salvar o PIX";
                    }
                } else {
                    echo "Ocorreu um erro ao gerar o PIX";
                }
            } else if ($credencial == 2 && isset($config)) {                
                require_once('Galaxpay/galaxpay.php');
                $auth = new \ODGalaxpay\ODGalaxpayAuth($config);
                $authorization = $auth->authService()->authenticate("charges.write");                
                $galaxpay = new \ODGalaxpay\ODGalaxpay($authorization->getAccessToken(), $config['sandbox']);     
                $last_transaction = $galaxpay->charges()->create(makeChargeRequestPix($data, $identificacao));
                if ($last_transaction->type == 1) {                    
                    $transactions = $last_transaction->Charge->Transactions[0];
                    $query = "set dateformat dmy; insert into sf_boleto(id_referencia, tp_referencia, bol_id_banco, carteira_id, bol_data_criacao, bol_data_parcela, bol_valor, 
                    bol_descricao, bol_nosso_numero, tipo_documento, sa_descricao, exp_remessa, syslogin, inativo, mundi_id, mundi_code, mundi_gateway_id, exp_email)
                    values(" . valoresSelect("txtPlanoItem") . ", 'M', 1, null, getdate(), '" . escreverData($data['data_vencimento']) . "', " . valoresNumericos("txtValor") . ", " .
                    valoresTexto2("https://sivisweb.com.br/Boletos/boleto_pix.php?id=M-" . $last_transaction->Charge->myId . "&crt=" . $contrato) . ", null, " . $pix_forma_pagamento . ", " . 
                    valoresTexto2($transactions->Pix->qrCode) . ", 1, " . valoresTexto2($login_usuario) . ", 0," . valoresTexto2($transactions->galaxPayId) . ", " . 
                    valoresTexto2($transactions->Pix->image) . "," . valoresTexto2($last_transaction->Charge->chargeGalaxPayId) . ", 0);SELECT SCOPE_IDENTITY() ID;";
                    $result = odbc_exec($con, $query) or die(odbc_errormsg());
                    odbc_next_result($result);
                    $id = odbc_result($result, 1);
                    if (is_numeric($id)) {                        
                        odbc_exec($con, "update sf_boleto set bol_nosso_numero = isnull((select max(bol_nosso_numero) from sf_boleto where bol_nosso_numero is not null and mundi_id is not null),0) + 1 where id_boleto = " . $id);                                                                        
                        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) 
                        select 'sf_boleto', id_mens, '" . $login_usuario . "', 'I', 'CADASTRO PIX: ' + cast(bol_nosso_numero as varchar), GETDATE(), favorecido 
                        from sf_boleto inner join sf_vendas_planos_mensalidade on id_referencia = id_mens
                        inner join sf_vendas_planos on id_plano_mens = id_plano where id_boleto = " . $id);
                        echo $id;
                    } else {
                        echo "Ocorreu um erro ao salvar o PIX";
                    }
                } else {
                    echo "Ocorreu um erro ao gerar o PIX";
                }
            } else {
                echo "Erro na Forma de Pagamento!";
            }                                    
        } else {
            echo "Cliente não encontrado!";
        }
    } else {
        echo "Erro nos Parâmetros de Pagamento!";
    }
} else {
    echo "Valor inválido para esta operação, o valor deve ser maior que 0 e menor que 10.000,00!";
}