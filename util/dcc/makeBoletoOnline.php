<?php
include '../../Connections/configini.php';

$chave = "";
$id_boleto = "";
$dcc_boleto_tipo = "";
$dcc_forma_pagamento = "";
$data_vencimento = "cast(getdate() as date)";

$cur = odbc_exec($con, "select dcc_boleto_tipo, bol_forma_pagamento, dcc_galaxid, dcc_galaxpay, dcc_galaxteste,
ACA_TIPO_MULTA, ACA_TAXA_MULTA, ACA_MORA_MULTA, ACA_DIAS_LIMITE, ACA_DESC_TIPO, ACA_DESC_DIAS, ACA_DESC_VALOR, DCC_CHAVE
from sf_configuracao where id = 1");
while ($RFP = odbc_fetch_array($cur)) {   
    $chave = $RFP['DCC_CHAVE'];    
    $config = ['galaxId' => $RFP['dcc_galaxid'], 'galaxHash' => $RFP['dcc_galaxpay'], 'sandbox' => ($RFP['dcc_galaxteste'] == 1)];  
    $dcc_boleto_tipo = $RFP['dcc_boleto_tipo'];
    $dcc_forma_pagamento = $RFP['bol_forma_pagamento'];    
    $dataConfig = ['tipo_multa' => $RFP['ACA_TIPO_MULTA'], 'taxa_multa' => escreverNumero($RFP['ACA_TAXA_MULTA'], 0, 2, '.', ''), 
    'mora_multa' => escreverNumero($RFP['ACA_MORA_MULTA'], 0, 2, '.', ''), 'dias_limite' => $RFP['ACA_DIAS_LIMITE'], 
    'tipo_desconto' => $RFP['ACA_DESC_TIPO'], 'dias_desconto' => $RFP['ACA_DESC_DIAS'], 'informativo' => "", 
    'valor_desconto' => escreverNumero($RFP['ACA_DESC_VALOR'], 0, 2, '.', '')];
}

if ($dcc_boleto_tipo == 0 && strlen($chave) > 0 && is_numeric($dcc_forma_pagamento)) {
    require_once('MundiPagg/mundiPagg.php');
    $client = new MundiAPILib\MundiAPIClient($chave, "");
    $charges = $client->getCharges();
} else if ($dcc_boleto_tipo == 2 && strlen($config["galaxHash"]) > 0 && is_numeric($dcc_forma_pagamento)) {
    require_once('Galaxpay/galaxpay.php');    
    $auth = new \ODGalaxpay\ODGalaxpayAuth($config);
    $authorization = $auth->authService()->authenticate((isset($_POST['imprimirBoleto']) ? "carnes.read" : "charges.write"));
} else {
    echo "Erro na configuração do boleto!"; exit;
}

if (isset($_POST['txtDataVencimento'])) {
    $data_vencimento = valoresData('txtDataVencimento');
}

if (isset($_POST['salvaBoletoMens'])) {    
    $i = 1;
    $cur = odbc_exec($con, "select id_mens, favorecido, id_prod_plano, dbo.VALOR_REAL_MENSALIDADE(id_mens) valor_mens 
    from sf_vendas_planos_mensalidade m inner join sf_vendas_planos pl on pl.id_plano = m.id_plano_mens
    where id_mens in (" . str_replace("|", ",", $_POST['id_mens']) . ",0)");
    while ($RFP = odbc_fetch_array($cur)) {
        $id_cli = $RFP['favorecido'];
        $_POST["txtIdItem_" . $i] = $RFP['id_prod_plano'];
        $_POST["txtIdPlano_" . $i] = $RFP['id_mens'];
        $_POST["txtTipo_" . $i] = "MENSALIDADE";
        $_POST["txtQtd_" . $i] = "1";
        $_POST["txtValor_" . $i] = escreverNumero($RFP["valor_mens"]);
        $_POST["txtMulta_" . $i] = escreverNumero("0");
        $_POST["txtValFinal_" . $i] = escreverNumero($RFP["valor_mens"]);
        $i++;
    }
    $_POST["txtIdCli"] = $id_cli;
    $_POST["txtQtdItens"] = ($i - 1);
    $_POST["salvaBoleto"] = "S";
}

if (isset($_POST['salvaBoleto'])) {
    $id_boleto = criarBoletoOnline($con, $_POST['txtIdCli'], $dcc_forma_pagamento, $data_vencimento);
    if (is_numeric($id_boleto)) {
        for ($i = 1; $i <= valoresNumericos('txtQtdItens'); $i++) {
            if (isset($_POST['txtIdItem_' . $i])) {
                criarBoletoOnlineItem($con, $id_boleto, $_POST['txtIdPlano_' . $i], $_POST['txtIdItem_' . $i], $_POST['txtQtd_' . $i], $_POST['txtValor_' . $i], $_POST['txtValFinal_' . $i], $_POST['txtMulta_' . $i]);
            }
        }
        $data = getDados($con, $id_boleto, $dataConfig);
        //print_r($data); exit;      
        if ($dcc_boleto_tipo == 0) {
            echo processarMundi($con, $id_boleto, $data, $charges);
        } else if ($dcc_boleto_tipo == 2) {
            echo processarGalax($con, $id_boleto, $data, $authorization, $config['sandbox']);      
        }
    } else {
        echo "Ocorreu um erro ao salvar o boleto";
    }
}

if (isset($_POST['processarBoleto'])) {
    if (is_numeric($_POST['txtIdBol'])) {
        $data = getDados($con, $_POST['txtIdBol'], $dataConfig);
        //print_r($data); exit;     
        if ($dcc_boleto_tipo == 0) {
            echo processarMundi($con, $_POST['txtIdBol'], $data, $charges);
        } else if ($dcc_boleto_tipo == 2) {
            echo processarGalax($con, $_POST['txtIdBol'], $data, $authorization, $config['sandbox']);               
        }
    } else {
        echo "Ocorreu um erro ao processar o boleto";        
    }
}

if (isset($_POST['cancelarBoleto'])) {
    if (is_numeric($_POST['txtIdBol'])) {
        if ($dcc_boleto_tipo == 0) {
            echo cancelarBoletoOnline($con, $_POST['txtIdBol']);
        } else if ($dcc_boleto_tipo == 2) {        
            $mundi_id = getOnlineId($con, $_POST['txtIdBol']);            
            echo cancelarGalax($con, $_POST['txtIdBol'], $mundi_id, $authorization, $config['sandbox']);
        }
    } else {
        echo "Ocorreu um erro ao cancelar o boleto";
    }
}

if (isset($_POST['imprimirBoleto'])) {
    if (strlen($_POST['txtIdBol']) > 0) {
        if ($dcc_boleto_tipo == 0) {
        } else if ($dcc_boleto_tipo == 2) {        
            echo imprimirGalax(explode(",", $_POST['txtIdBol']), $authorization, $config['sandbox']);
        }
    } else {
        echo "Ocorreu um erro ao imprimir o carnê";
    }        
}

if (isset($_POST['excluirBoleto'])) {    
    if (is_numeric($_POST['txtIdBol'])) {
        echo excluirBoleto($con, $_POST['txtIdBol']);
    } else {
        echo "Ocorreu um erro ao excluir o boleto";
    }
}

function processarGalax($con, $id_boleto, $data, $authorization, $sandbox) {
    $galaxpay = new \ODGalaxpay\ODGalaxpay($authorization->getAccessToken(), $sandbox);  
    $last_transaction = $galaxpay->charges()->create(makeChargeRequestBoleto($data, $data["identificacao"]));
    if ($last_transaction->type == 1) {                    
        $transactions = $last_transaction->Charge->Transactions[0];
        return atualizarBoletoOnline($con, $id_boleto, $transactions->Boleto->pdf, $transactions->Boleto->bankLine, $transactions->galaxPayId);
    } else {
        echo $last_transaction; exit;        
    }     
}

function processarMundi($con, $id_boleto, $data, $charges) {
    $result = $charges->createCharge(makeChargeRequestBoleto($data))->jsonSerialize();
    $last_transaction = $result["last_transaction"]->jsonSerialize();
    if ($last_transaction['success']) {
        return atualizarBoletoOnline($con, $id_boleto, $last_transaction['url'], $last_transaction['line'], $last_transaction['gateway_id']);
    } else {
        echo $last_transaction; exit; 
    }
}

function cancelarGalax($con, $id_boleto, $mundi_id, $authorization, $sandbox) {
    $galaxpay = new \ODGalaxpay\ODGalaxpay($authorization->getAccessToken(), $sandbox);
    $last_transaction = $galaxpay->charges()->cancel($mundi_id);
    if ($last_transaction->type == 1) {                    
        return cancelarBoletoOnline($con, $id_boleto);
    } else {
        echo $last_transaction; exit;        
    }    
}

function imprimirGalax($mundi_id, $authorization, $sandbox) {
    $galaxpay = new \ODGalaxpay\ODGalaxpay($authorization->getAccessToken(), $sandbox);
    $last_transaction = $galaxpay->charges()->print($mundi_id);
    if (isset($last_transaction->Carnes[0])) {
        return $last_transaction->Carnes[0]->pdf;
    } else {
        echo $last_transaction; exit;        
    }    
}

function getDados($con, $id_boleto, $data) {    
    $cur = odbc_exec($con, "select id_boleto, id_pessoa, razao_social, cnpj, endereco, numero, complemento, bairro, cep, 
    (SELECT TOP 1 cidade_nome FROM tb_cidades WHERE cidade_codigo = p.cidade) as cidade,
    (SELECT TOP 1 estado_sigla FROM tb_estados WHERE estado_codigo = p.estado) as estado,
    (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = id_fornecedores_despesas AND tipo_contato = 2) as email,
    (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = id_fornecedores_despesas AND tipo_contato = 1) as celular,    
    (SELECT cast(case when min(dt_inicio_mens) > dateadd(day,2, getdate()) and min(dt_inicio_mens) >= b.bol_data_parcela then min(dt_inicio_mens) 
    when b.bol_data_parcela > dateadd(day,2, getdate()) then b.bol_data_parcela else dateadd(day,2, getdate()) end as date) bol_vencimento 
    FROM sf_boleto_online_itens bb inner join sf_vendas_planos_mensalidade mm on bb.id_mens = mm.id_mens where bb.id_boleto = b.id_boleto) bol_vencimento,    
    (select sum(valor_total) from sf_boleto_online_itens bb where bb.id_boleto = b.id_boleto) bol_valor,
    (select sum(valor_bruto) from sf_boleto_online_itens bb where bb.id_boleto = b.id_boleto) bol_bruto,
    STUFF((SELECT distinct ',' + descricao + case when placa is not null then ' [' + placa + ']' else '' end 
    FROM sf_boleto_online_itens bi inner join sf_produtos p on p.conta_produto = bi.id_produto
    left join sf_vendas_planos_mensalidade m on m.id_mens = bi.id_mens left join sf_vendas_planos vp on vp.id_plano = m.id_plano_mens
    left join sf_fornecedores_despesas_veiculo v on v.id = vp.id_veiculo WHERE bi.id_boleto = b.id_boleto FOR XML PATH('')), 1, 1, '') informativo,
    STUFF((SELECT distinct ',' + FORMAT(m.dt_inicio_mens, 'MMM/yyyy') FROM sf_boleto_online_itens bi inner join sf_vendas_planos_mensalidade m on m.id_mens = bi.id_mens
    WHERE bi.id_boleto = b.id_boleto FOR XML PATH('')), 1, 1, '') identificacao
    from sf_boleto_online b inner join sf_fornecedores_despesas p on p.id_fornecedores_despesas = b.id_pessoa 
    where id_boleto = " . $id_boleto);
    while ($RFP = odbc_fetch_array($cur)) {
        $data['id_mens'] = 100000 + $RFP['id_boleto'];
        $data['data_vencimento'] = $RFP['bol_vencimento'];          
        $data['valor_real'] = escreverNumero($RFP['bol_valor'], 0, 2, '.', '');
        $data['valor'] = escreverNumero($RFP['bol_bruto'], 0, 2, '.', '');
        $data['id_aluno'] = $RFP['id_pessoa'];            
        $data['nome'] = trim(utf8_encode($RFP['razao_social']));
        $data['cnpj'] = trim($RFP['cnpj']);           
        $data['email'] = ($RFP['email'] == "" ? "teste@gmail.com" : trim(utf8_decode($RFP['email'])));            
        $data['celular'] = ($RFP['celular'] == "" ? "(24) 99999-9999" : trim(utf8_decode($RFP['celular'])));
        $data['cep'] = $RFP['cep'];            
        $data['endereco'] = utf8_encode($RFP['endereco']);          
        $data['numero'] = (is_numeric($RFP['numero']) && $RFP['numero'] > 0 ? $RFP['numero'] : "1");
        $data['bairro'] = utf8_encode($RFP['bairro']);            
        $data['cidade'] = utf8_encode($RFP['cidade']);
        $data['estado'] = utf8_encode($RFP['estado']);
        $data['complemento'] = utf8_encode($RFP['complemento']);  
        $data['informativo'] = utf8_encode($RFP['informativo']);  
        $data["identificacao"] = "Mens." . str_replace(array("Feb", "Apr", "May", "Aug", "Sep", "Oct", "Dec"), array("Fev", "Abr", "Mai", "Ago", "Set", "Out", "Dez"), utf8_encode($RFP['identificacao']));
    }
    return $data;
}

function getOnlineId($con, $id_boleto) {
    $toReturn = 0;
    $cur = odbc_exec($con, "select galaxy_id from sf_boleto_online where id_boleto = " . $id_boleto);
    while ($RFP = odbc_fetch_array($cur)) { 
        $toReturn = $RFP['galaxy_id'];
    }
    return $toReturn;
}

function criarBoletoOnline($con, $id_pessoa, $dcc_forma_pagamento, $data_vencimento) {
    $query = "set dateformat dmy;
    insert into sf_boleto_online (id_pessoa, bol_data_criacao, syslogin, tipo_documento, bol_data_parcela, exp_remessa, inativo)
    values (" . valoresNumericos2($id_pessoa) . ", getdate(), '" . (isset($_SESSION["login_usuario"]) ? $_SESSION["login_usuario"] : 'SISTEMA') . "',
    " . valoresSelect2($dcc_forma_pagamento) . "," . $data_vencimento . ",0,2);
    SELECT SCOPE_IDENTITY() ID;";
    $result = odbc_exec($con, $query) or die(odbc_errormsg());
    odbc_next_result($result);
    $id_boleto = odbc_result($result, 1);
    return $id_boleto;
}

function atualizarBoletoOnline($con, $id_boleto, $bol_link, $bol_linha, $galaxy_id) {
    $query = "update sf_boleto_online set bol_valor = (select sum(valor_total) from sf_boleto_online_itens bb where bb.id_boleto = sf_boleto_online.id_boleto),
    bol_data_parcela = (SELECT cast(case when min(dt_inicio_mens) > dateadd(day,2, getdate()) and min(dt_inicio_mens) >= bol_data_parcela then min(dt_inicio_mens) 
    when bol_data_parcela > dateadd(day,2, getdate()) then bol_data_parcela else dateadd(day,2, getdate()) end as date) bol_vencimento 
    FROM sf_boleto_online_itens bb inner join sf_vendas_planos_mensalidade mm on bb.id_mens = mm.id_mens where bb.id_boleto = sf_boleto_online.id_boleto),
    bol_link = " . valoresTexto2($bol_link) . ", bol_linha = " . valoresTexto2($bol_linha) . ", galaxy_id = " . valoresTexto2($galaxy_id) . ", inativo = 0 
    where id_boleto = " . $id_boleto;
    odbc_exec($con, $query) or die(odbc_errormsg());    
    $query_log = "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    select 'sf_boleto', id_boleto, '" . (isset($_SESSION["login_usuario"]) ? $_SESSION["login_usuario"] : 'SISTEMA') . "', 
    'I', 'CADASTRO BOLETO: ' + bol_linha, GETDATE(), id_pessoa from sf_boleto_online where id_boleto = " . $id_boleto;
    odbc_exec($con, $query_log) or die(odbc_errormsg());    
    return $id_boleto;
}

function criarBoletoOnlineItem($con, $id_boleto, $id_mens, $id_produto, $quantidade, $valor_total, $valor_bruto, $valor_multa) {
    $query = "insert into sf_boleto_online_itens (id_boleto, id_mens, id_produto, quantidade, valor_total, valor_bruto, valor_multa)
    values (" . $id_boleto . "," . valoresSelect2($id_mens) . "," . valoresSelect2($id_produto) . "," . valoresNumericos2($quantidade) . "," . 
    valoresNumericos2($valor_total) . "," . valoresNumericos2($valor_bruto) . "," . valoresNumericos2($valor_multa) . ");";
    odbc_exec($con, $query) or die(odbc_errormsg());
}

function cancelarBoletoOnline($con, $id_boleto) {
    $query = "update sf_boleto_online set inativo = 1 where id_boleto = " . $id_boleto;
    odbc_exec($con, $query) or die(odbc_errormsg());    
    $query_log = "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    select 'sf_boleto', id_boleto, '" . (isset($_SESSION["login_usuario"]) ? $_SESSION["login_usuario"] : 'SISTEMA') . "', 
    'C', 'CANCELAMENTO BOLETO: ' + bol_linha, GETDATE(), id_pessoa from sf_boleto_online where id_boleto = " . $id_boleto;
    odbc_exec($con, $query_log) or die(odbc_errormsg());    
    return $id_boleto;
}

function excluirBoleto($con, $id_boleto) {    
    $query_log = "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    select 'sf_boleto', id_boleto, '" . (isset($_SESSION["login_usuario"]) ? $_SESSION["login_usuario"] : 'SISTEMA') . "', 
    'E', 'EXCLUSAO BOLETO: ' + isnull(bol_linha,''), GETDATE(), id_pessoa from sf_boleto_online where id_boleto = " . $id_boleto;
    odbc_exec($con, $query_log) or die(odbc_errormsg());       
    $query = "delete from sf_boleto_online_itens where id_boleto = " . $id_boleto . ";
    delete from sf_boleto_online where id_boleto = " . $id_boleto . ";";
    odbc_exec($con, $query) or die(odbc_errormsg());    
    return $id_boleto;
}