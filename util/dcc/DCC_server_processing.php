<?php

$url = dirname(__FILE__) . "./../../";
require_once($url . 'Connections/configini.php');
require_once($url . 'util/util.php');
require_once($url . 'util/excel/exportaExcel.php');

$aColumns = array("id_transacao", "dt_modalidade", "dcc_nome_cartao", "orderID", "transactionID", "dcc_numero_cartao", "processorMessage", "valor_transacao", "f_inicio", "f_referencia", "transactionTimestamp", "responseMessage", "errorMessage", "sf_vendas_itens_dcc.id_fornecedores_despesas", "mkr_login");
$table = "sf_vendas_itens_dcc inner join sf_fornecedores_despesas on sf_vendas_itens_dcc.id_fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas
left join sf_tipo_documento on sf_tipo_documento.id_tipo_documento = sf_vendas_itens_dcc.id_tipo_documento";
$iTotal = 0;
$iFilteredTotal = 0;
$sLimit = 0;
$sQtd = 20;
$imprimir = 0;
$total = 0;
$totalLiquido = 0;

if (is_numeric($_GET['imp']) && $_GET['imp'] == "1") {
    $imprimir = 1;
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

$where = " WHERE id_transacao > 0 ";
$whereMens = " WHERE id_item_venda_mens is null AND B.dt_cancelamento is null ";
$whereAgend = " WHERE id_agendamento > 0 ";

if (isset($_GET['fil'])) {
    $where .= " and sf_fornecedores_despesas.empresa = " . $_GET['fil'];
    $whereMens .= " and f.empresa = " . $_GET['fil'];
    $whereAgend .= " and f.empresa = " . $_GET['fil'];
}

if (is_numeric($_GET['tcl'])) {
    if ($_GET['tcl'] == "1" && is_numeric($_GET['cli'])) {
        $where .= " and sf_fornecedores_despesas.id_fornecedores_despesas = " . $_GET['cli'];
        $whereMens .= " and favorecido = " . $_GET['cli'];
        $whereAgend .= " and f.id_fornecedores_despesas = " . $_GET['cli'];
    }
}

if ($_GET['dck'] == "1") {
    if (is_numeric($_GET['dtp']) && $_GET['din'] != "" && $_GET['dfn'] != "") {
        $where .= " and " . ($_GET['dtp'] == 1 ? "cast(transactionTimestamp as date)" : "dt_modalidade") . " between " . valoresData2($_GET['din']) . " and " . valoresData2($_GET['dfn']) . "";
        $whereMens .= " and dt_inicio_mens between " . valoresData2($_GET['din']) . " and " . valoresData2($_GET['dfn']) . "";
        $whereAgend .= " and dt_cancel_agendamento between " . valoresData2($_GET['din']) . " and " . valoresData2($_GET['dfn']) . "";
    }
}

if (is_numeric($_GET['cri'])) {
    $where .= " and SUBSTRING(dcc_numero_cartao,1,4) = '" . $_GET['cri'] . "'";
    $whereMens .= " and SUBSTRING(legenda_cartao,1,4) = '" . $_GET['cri'] . "'";
    $whereAgend .= " and SUBSTRING(legenda_cartao,1,4) = '" . $_GET['cri'] . "'";
}

if (is_numeric($_GET['crf'])) {
    $where .= " and SUBSTRING(dcc_numero_cartao,len(dcc_numero_cartao)-3,4) = '" . $_GET['crf'] . "'";
    $whereMens .= " and SUBSTRING(legenda_cartao,len(legenda_cartao)-3,4) = '" . $_GET['crf'] . "'";
    $whereAgend .= " and SUBSTRING(legenda_cartao,len(legenda_cartao)-3,4) = '" . $_GET['crf'] . "'";
}

if ($_GET['car'] != "null" && $_GET['car'] != "undefined" && strlen($_GET['car']) > 0) {
    $where .= " and sf_vendas_itens_dcc.id_tipo_documento in (" . $_GET['car'] . ")";
}

if (is_numeric($_GET['sts'])) {
    switch ($_GET['sts']) {
        case 1: //APROVADO
            $where .= " and processorMessage = 'APPROVED' and status_transacao = 'A'"; //DECLINED
            break;
        case 2: //NÃO ENVIADO
            $where .= " and processorMessage = 'NAOENVIADO'";
            break;
        case 3: //TODOS
            $where .= " and (processorMessage <> 'APPROVED' or (processorMessage = 'APPROVED' and status_transacao = 'A' )) ";
            break;
        case 4: //ESTORNADO
            $where .= " and status_transacao = 'E'";
            break;
        case 5: //NEGADO
            $where .= " and status_transacao = 'D'";
            break;
        case 6: //CANCELADO
            $where .= " and status_transacao = 'C'";
            break;
    }
}

if (isset($_GET['inv']) && $_GET['inv'] !== "") {
    if ($_GET['inv'] === "0") {
        $where .= " and orderID <> '' ";
    }
} else {
    $where .= " and orderID <> '' ";
}

$sQuery = "set dateformat dmy;
select SUM(x.total) total, isnull(SUM(x.vtotal),0) vtotal, isnull(SUM(x.vtotalliq),0) vtotalliq from( SELECT count(id_transacao) total,
sum(case when processorMessage = 'APPROVED' then valor_transacao
when processorMessage = 'SETTLED' then (-1 *  valor_transacao) else 0 end) vtotal,
sum(case when processorMessage = 'APPROVED' and taxa > 0 then valor_transacao - (valor_transacao * (taxa / 100))
when processorMessage = 'APPROVED' and taxa = 0 then valor_transacao
when processorMessage = 'SETTLED' and taxa > 0 then (-1 * valor_transacao) - ((-1 * valor_transacao) * (taxa / 100))
when processorMessage = 'SETTLED' and taxa = 0 then (-1 * valor_transacao)
else 0 end) vtotalliq from " . $table . $where;
if (($_GET['sts'] == "2" || $_GET['sts'] == "3") && $_GET['dtp'] == 0) {
    $sQuery .= " union select COUNT(*) total, SUM(valor_mens) vtotal, SUM(valor_mens) vtotalliq
    from sf_vendas_planos_mensalidade A inner join sf_vendas_planos B on A.id_plano_mens = B.id_plano 
    inner join sf_fornecedores_despesas F on f.id_fornecedores_despesas = B.favorecido AND F.inativo = 0
    " . $whereMens . " AND id_parc_prod_mens in (select id_parcela from sf_produtos A inner join sf_produtos_parcelas B on A.conta_produto = B.id_produto where parcela = 0)" .
    " and (select count(id_agendamento) from sf_vendas_planos_dcc_agendamento where id_plano_agendamento = B.id_plano and dt_cancel_agendamento <= dt_inicio_mens) = 0";
}
if ($_GET['sts'] == "6" || $_GET['sts'] == "3") {
    $sQuery .= " union select count(*) total, '0' vtotal, '0' vtotalliq from sf_vendas_planos_dcc_agendamento A     
    inner join sf_vendas_planos B on A.id_plano_agendamento = B.id_plano
    inner join sf_fornecedores_despesas F on f.id_fornecedores_despesas = B.favorecido AND F.inativo = 0" . $whereAgend;
}
$sQuery .= ") as x";
//echo $sQuery; exit;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
    $iFilteredTotal = $RFP['total'];
    $total = tableFormato($RFP['vtotal'], 'N', "", "");
    $totalLiquido = tableFormato($RFP['vtotalliq'], 'N', "", "");
}

for ($i = 0; $i < count($aColumns); $i++) {
    $colunas .= $aColumns[$i] . ",";
}

$colunas = substr_replace($colunas, "", -1);
$sQuery1 = "set dateformat dmy; SELECT * FROM(SELECT ROW_NUMBER() OVER (ORDER BY id_transacao desc) AS row, * FROM
(select " . $colunas . ",case when processorMessage = 'APPROVED' and taxa > 0 then valor_transacao - (valor_transacao * (taxa / 100))
when processorMessage = 'APPROVED' and taxa = 0 then valor_transacao
when processorMessage = 'SETTLED' and taxa > 0 then (-1 * valor_transacao) - ((-1 * valor_transacao) * (taxa / 100))
when processorMessage = 'SETTLED' and taxa = 0 then (-1 * valor_transacao) else 0 end vtotalliq FROM " . $table . $where;
if (($_GET['sts'] == "2" || $_GET['sts'] == "3") && $_GET['dtp'] == 0) {
    $sQuery1 .= " union select id_mens id_transacao, dt_inicio_mens dt_modalidade, C.nome_cartao dcc_nome_cartao, '' orderID, '' TransactionID, C.legenda_cartao dcc_numero_cartao,
    'NAO ENVIADO' processorMessage, valor_mens, '' f_inicio, '' f_referencia, null transactionTimestamp, 'NAO ENVIADO' responseMessage, '' errorMessage, favorecido id_fornecedores_despesas, '' mkr_login, 0 vtotalliq
    from sf_vendas_planos_mensalidade A inner join sf_vendas_planos B on A.id_plano_mens = B.id_plano
    inner join sf_fornecedores_despesas F on f.id_fornecedores_despesas = b.favorecido AND F.inativo = 0
    left join sf_fornecedores_despesas_cartao C on (B.favorecido = C.id_fornecedores_despesas and C.id_cartao = (select max(id_cartao) from sf_fornecedores_despesas_cartao where id_fornecedores_despesas = B.favorecido))
    " . $whereMens . " AND id_parc_prod_mens in (select id_parcela from sf_produtos A inner join sf_produtos_parcelas B on A.conta_produto = B.id_produto where parcela = 0)
    and (select count(id_agendamento) from sf_vendas_planos_dcc_agendamento where id_plano_agendamento = B.id_plano and dt_cancel_agendamento <= dt_inicio_mens) = 0";
}
if ($_GET['sts'] == "6" || $_GET['sts'] == "3") {
    $sQuery1 .= " union select id_agendamento id_transacao, dt_cancel_agendamento dt_modalidade, C.nome_cartao dcc_nome_cartao, '' orderID, '' TransactionID,
    C.legenda_cartao dcc_numero_cartao, 'CANCELADO' processorMessage, '' valor_mens, '' f_inicio, '' f_referencia,
    dt_cadastro_agendamento transactionTimestamp, 'CANCELADO' responseMessage, '' errorMessage, favorecido id_fornecedores_despesas, '' mkr_login, 0 vtotalliq from dbo.sf_vendas_planos_dcc_agendamento A
    inner join sf_vendas_planos B on A.id_plano_agendamento = B.id_plano
    inner join sf_fornecedores_despesas F on f.id_fornecedores_despesas = B.favorecido AND F.inativo = 0
    left join sf_fornecedores_despesas_cartao C on (B.favorecido = C.id_fornecedores_despesas and C.id_cartao = (select max(id_cartao) from sf_fornecedores_despesas_cartao where id_fornecedores_despesas = B.favorecido))" . $whereAgend;
}
$sQuery1 .= ") as a) as b WHERE b.row > " . $sLimit . " and b.row <= " . ($sLimit + $sQtd) . " or 1=" . $imprimir . "" . $sOrder;
//echo $sQuery1;exit();
$cur = odbc_exec($con, $sQuery1);

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$resumo = "<input id=\"txt_dcc_total\" value=\"" . $iTotal . "\" type=\"hidden\"/>";
$resumo .= "<input id=\"txt_dcc_vbruto\" value=\"" . escreverNumero($total) . "\" type=\"hidden\"/>";
$resumo .= "<input id=\"txt_dcc_vliquido\" value=\"" . escreverNumero($totalLiquido) . "\" type=\"hidden\"/>";

$i = 0;
while ($aRow = odbc_fetch_array($cur)) {
    $invalid = ((tableFormato($aRow[$aColumns[3]], 'T', "", "") == "" && $aRow[$aColumns[6]] == null) == true ? "<span style='color:#e09f4f';>" : "");
    $invalid2 = ((tableFormato($aRow[$aColumns[3]], 'T', "", "") == "" && $aRow[$aColumns[6]] == null) == true ? "</span>" : "");
    $row = array();
    $row[] = $invalid.($aRow[$aColumns[6]] == "NAO ENVIADO" ? "<input name=\"nEnviado[]\" type=\"hidden\" value=\"" . $aRow[$aColumns[0]] . "\"/>" : "") . escreverData($aRow[$aColumns[1]]) . ($i == 0 && $imprimir == 0 ? $resumo : "").$invalid2;
    $row[] = $invalid.tableFormato($aRow["id_fornecedores_despesas"], 'T', "", "").$invalid2;
    $row[] = $invalid.tableFormato($aRow[$aColumns[2]], 'T', "", "").$invalid2;
    $row[] = $invalid.tableFormato($aRow[$aColumns[3]], 'T', "", "").$invalid2;
    if ($aRow["orderID"] !== "") {
        $row[] = tableFormato($aRow[$aColumns[4]], 'T', "", "");
    } else {
        $row[] = tableFormato($aRow[$aColumns[12]], 'T', "", "");
    }
    $row[] = $invalid.tableFormato($aRow[$aColumns[5]], 'T', "", "").$invalid2;
    $row[] = ($aRow[$aColumns[6]] == "APPROVED" || $aRow[$aColumns[6]] == "SETTLED") ? getTextDCC(tableFormato($aRow[$aColumns[6]], 'T', "", "")) : getTextDCC(tableFormato($aRow[$aColumns[11]], 'T', "", ""));
    $row[] = $invalid.escreverNumero($aRow[$aColumns[7]]).$invalid2;
    $row[] = $invalid.$aRow[$aColumns[8]].$invalid2;
    $row[] = $invalid.tableFormato($aRow[$aColumns[9]], 'T', "", "").$invalid2;
    $row[] = $aRow["orderID"] !== "" ? $invalid.escreverDataHora($aRow[$aColumns[10]]).$invalid2 : '';
    $row[] = $invalid.tableFormato($aRow[$aColumns[14]], 'T', "", "").$invalid2;
    if ($imprimir == 1) {
        $row[] = ($aRow[$aColumns[14]] == "SISTEMA" ? "0" : "1");
        $row[] = escreverNumero($aRow["vtotalliq"]);
    }
    $output['aaData'][] = $row;
    $i++;
}

if ($imprimir == 1) {
    $output['aaTotal'][] = array(6, 6, "Sub Total", "Total Bruto", 0, 0);
    $output['aaTotal'][] = array(9, 4, "Sub Total", "Total Líquido", 0, 0);
    $output['aaGrupo'][] = array("0", "DCC Recorrente", 8);
    $output['aaGrupo'][] = array("1", "DCC Hoje", 8);
}

if (isset($_GET['ex']) && $_GET['ex'] == 1) {
    $topo = '<tr><td><b>Dt.Mens. </b></td><td><b>Matricula </b></td><td><b>Nm.Cartão </b></td><td><b>Pedido </b><td><b>Transação </b></td><td><b>Cartão </b></td><td><b>Estado </b></td><td><b>Valor </b></td><td><b>Cod.Mod. </b></td><td><b>Recor. </b></td><td><b>Dt.Pgto. </b></td></tr>';
    exportaExcel($output['aaData'], $topo);
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
