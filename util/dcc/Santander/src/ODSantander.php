<?php

namespace ODSantander;

use ODSantander\Exception\ODSantanderException;
use ODSantander\Request\Client;
use ODSantander\Services\WorkspaceService;
use ODSantander\Services\ChargeService;

class ODSantander {

    protected $client;

    public function __construct($accessToken, $config) {
        if (empty($accessToken)) {
            throw new ODSantanderException('Provide an access token');
        }
        $this->client = new Client($accessToken, $config);
    }

    public function workspaces(): WorkspaceService {
        return new WorkspaceService($this->client);
    }
    
    public function charges(): ChargeService {
        return new ChargeService($this->client);
    }    

}
