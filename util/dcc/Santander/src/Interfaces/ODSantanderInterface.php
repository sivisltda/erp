<?php

namespace ODSantander\Interfaces;

interface ODSantanderInterface {

    /**
     * @param $json
     *
     * @return \stdClass
     */
    public static function fromJson($json);

    /**
     * @param \stdClass $data
     */
    public function populate(\stdClass $data);
}
