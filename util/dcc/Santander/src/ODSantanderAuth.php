<?php

namespace ODSantander;

use ODSantander\Request\Auth;
use ODSantander\Services\AuthService;

class ODSantanderAuth {

    protected $client;

    public function __construct($config) {
        if (!isset($config['client_id']) || empty($config['client_id'])) {
            throw new ODGalaxpayException('Provide your Client Id');
        }
        if (!isset($config['client_secret']) || empty($config['client_secret'])) {
            throw new ODGalaxpayException('Provide your Client Secret');
        }        
        if (!isset($config['cert']) || empty($config['cert'])) {
            throw new ODGalaxpayException('Provide your Cert');
        }        
        $this->client = new Auth($config);
    }

    public function authService(): AuthService {
        return new AuthService($this->client);
    }

}