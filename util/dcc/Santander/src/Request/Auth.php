<?php

namespace ODSantander\Request;

use GuzzleHttp\Client as GuzzleClient;

class Auth extends Base {

    const PRODUCTION_ENDPOINT = "https://trust-open.api.santander.com.br/auth/oauth/v2/";
    const SANDBOX_ENDPOINT = "https://trust-sandbox.api.santander.com.br/auth/oauth/v2/";

    public function __construct($config) {
        $this->client = new GuzzleClient([
            'base_uri' => $config['sandbox'] ? self::SANDBOX_ENDPOINT : self::PRODUCTION_ENDPOINT,
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Accept' => 'application/json'
            ],
            'cert' => __DIR__ . './../../path/to/' . $config['cert']
        ]);
    }        

}
