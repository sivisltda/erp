<?php

namespace ODSantander\Request;

use GuzzleHttp\Client as GuzzleClient;

class Client extends Base {

    const PRODUCTION_ENDPOINT = "https://trust-open.api.santander.com.br/collection_bill_management/v2/";
    const SANDBOX_ENDPOINT = "https://trust-sandbox.api.santander.com.br/collection_bill_management/v2/";

    public function __construct($accessToken, $config) {
        $header = [
            'Authorization' => sprintf('Bearer %s', $accessToken),
            'X-Application-Key' => $config['client_id'],
            'Accept' => 'application/json'
        ];
        $this->client = new GuzzleClient([
            'base_uri' => $config['sandbox'] ? self::SANDBOX_ENDPOINT : self::PRODUCTION_ENDPOINT,
            'headers' => $header,
            'cert' => __DIR__ . './../../path/to/' . $config['cert']
        ]);
    }

}
