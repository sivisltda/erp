<?php

namespace ODSantander\Services;

use ODSantander\Response\AuthResponse;
use ODSantander\Response\Response;

class AuthService extends BaseService {

    public function authenticate($config): Response {
        
        $authResponse = new AuthResponse();
        $response = $this->client->post('token', [
            'form_params' => [
                'client_id' => $config['client_id'],
                'client_secret' => $config['client_secret'],                
                'grant_type' => 'client_credentials'
            ]
        ]);
        return $authResponse->fromJson($response);
    }

}
