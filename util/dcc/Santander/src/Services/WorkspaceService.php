<?php

namespace ODSantander\Services;

use ODSantander\Response\Response;

class WorkspaceService extends BaseService {

    public function create($params): Response {
        //echo json_encode($params);exit;
        $response = $this->client->post('workspaces', [
            'json' => $params
        ]);
        return $this->response->fromJson($response);
    }
        
    public function list_all(): Response {
        $response = $this->client->get("workspaces");
        return $this->response->fromJson($response);
    }  
        
    public function list_id($id): Response {
        $response = $this->client->get("workspaces/{$id}");
        return $this->response->fromJson($response);
    }  
                
    public function cancel($id): Response {
        $response = $this->client->delete("workspaces/{$id}");
        return $this->response->fromJson($response);
    }  
    
}