<?php

namespace ODSantander\Services;

use ODSantander\Response\Response;

class ChargeService extends BaseService {

    public function create($params, $workspaces): Response {
        //echo json_encode($params);exit;
        $response = $this->client->post("workspaces/{$workspaces}/bank_slips", [
            'json' => $params
        ]);
        return $this->response->fromJson($response);
    }
        
    public function cancel($id): Response {
        $response = $this->client->delete("charges/{$id}/santanderPayId");
        return $this->response->fromJson($response);
    }  
    
}
