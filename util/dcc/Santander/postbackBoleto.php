<?php

require_once __DIR__ . '/vendor/autoload.php';

$read = json_decode(file_get_contents("php://input"), true);
$content = date("d/m/Y H:i:s") . "\r\n";
$conexao = [];
$result = "";

$content .= "Sucesso " . $_GET["loja"] . "\r\n";
/*$content .= "id: " . $read["webhookId"] . "\r\n";
$content .= "confirmHash: " . $read["confirmHash"] . "\r\n";
$content .= "event: " . $read["event"] . "\r\n";
$content .= "----------------------------------------------------------\r\n";
$content .= "chargeMyId: " . $read["Transaction"]["chargeMyId"] . "\r\n";
$content .= "galaxPayId: " . $read["Transaction"]["galaxPayId"] . "\r\n";
$content .= "custo: " . $read["Transaction"]["value"] . "\r\n";
$content .= "dataPagamento: " . $read["Transaction"]["paydayDate"] . "\r\n";
$content .= "status: " . $read["Transaction"]["status"] . "\r\n";

if (in_array($read["Transaction"]["status"], array("payedBoleto", "payedPix", "payExternal", "cancelByContract")) && 
    is_numeric($read["Transaction"]["galaxPayId"]) && is_numeric($_GET["loja"])) {
    
    require_once "../../../Connections/configSivis.php";
    require_once "../../../Connections/funcoesAux.php";
    
    $sql = "select top 1 * from ca_contratos where inativa = 0 and numero_contrato = '" . $_GET["loja"] . "'";
    $res = odbc_exec($con2, $sql);
    while ($row = odbc_fetch_array($res)) {
        $conexao['host'] = utf8_encode($row['local']);
        $conexao['login'] = utf8_encode($row['login']);
        $conexao['senha'] = utf8_encode($row['senha']);
        $conexao['banco'] = utf8_encode($row['banco']);
        $conexao['numero_contrato'] = $row['numero_contrato'];
    }  
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $conexao['host'] . "; DATABASE=" . $conexao['banco'] . ";", $conexao['login'], $conexao['senha'])or die(odbc_errormsg());
    if ($read["Transaction"]["status"] == "cancelByContract") {
        $query = "update sf_boleto set inativo = 1 where mundi_id = '" . $read["Transaction"]["galaxPayId"] . "';";           
        $query .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) 
        values ('sf_boleto', " . $read["Transaction"]["galaxPayId"] . ", 'SISTEMA', 'C', 'AUTO CANCELAMENTO BOLETO: ' + 
        (select top 1 cast(sa_descricao as varchar(max)) from sf_boleto where mundi_id = '" . $read["Transaction"]["galaxPayId"] . "'), GETDATE(), 
        (select isnull((select favorecido from sf_vendas_planos_mensalidade 
        inner join sf_vendas_planos on id_plano_mens = id_plano 
        inner join sf_boleto C on C.id_referencia = sf_vendas_planos_mensalidade.id_mens and tp_referencia = 'M'
        where mundi_id = '" . $read["Transaction"]["galaxPayId"] . "'),0)));";
        odbc_exec($con, $query) or die(odbc_errormsg());        
        $result = "C|" . $read["Transaction"]["galaxPayId"];
    } else {
        
        $cur = odbc_exec($con, "EXEC dbo.SP_MK_VENDA @tr_boleto = '" . $read["Transaction"]["galaxPayId"] . "', @data_pagamento = '" . $read["Transaction"]["paydayDate"] . "', @valor_pago = " . $read["Transaction"]["value"] . ";");
        $result = odbc_result($cur, 1);
    }
    $content .= $result;
} else {
    $result = "N|" .$read["Transaction"]["status"];
}

echo $result;*/

$url = __DIR__ . "./../../../Pessoas/" . $_GET["loja"] . "/Santander/" . date("Y/m/d") . "/";
if (!file_exists($url)) {
    mkdir($url, 0777, true);
}
$handle = fopen($url . $read["webhookId"] . "_" . date("d_m_Y__H_i_s") . ".txt", "x+");
fwrite($handle, $content);
fclose($handle);

if (strlen($result) == 0) {
    header('HTTP/1.1 524 Unauthorized');
    http_response_code(524);
}