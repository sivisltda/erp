<?php

require('vendor/autoload.php');

function ValoresNumericosMP($val) {
    $und = "00";
    $number = explode('.', $val);
    if (count($number) > 1) {
        $val = $number[0];
        $und = $number[1] . "00";
    }
    return $val . substr($und, 0, 2);
}

function valorNumeroPuro($val) {
    return preg_replace('/[^0-9]/', '', $val);
}

function makeChargeRequestBoleto($data = []) {
    $payer = [
        'name' => 'João da Silva e silva',
        'documentType' => 'CPF',
        'documentNumber' => '94620639079',
        'address' => 'rua nove de janeiro',
        'neighborhood' => 'bela vista',
        'city' => 'sao paulo',
        'state' => 'SP',
        'zipCode' => '05134-897'
    ];

    $discount = [
        'type' => 'VALOR_DATA_FIXA',
        'discountOne' => [
            'value' => 1.50,
            'limitDate' => '2023-08-15'
        ]
    ];

    $data = [
        'nsuCode' => '5', //O NSU deve ser único (não deve se repetir) por data/convênio
        'nsuDate' => '2023-08-15', //É a data do envio do NSU no registro do boleto
        'environment' => 'TESTE', //TESTE|PRODUCAO
        'covenantCode' => '3567206', // Código do Convênio
        'issueDate' => '2023-08-15', //Data de emissão de emissão do boleto
        'dueDate' => '2023-08-18', //Data de vencimento do Boleto
        'bankNumber' => '9000145', //Nosso Número
        'clientNumber' => '102030', //Código alfanumérico de até 15 posições, para a identificação do boleto na Empresa
        'nominalValue' => '10.00', //Valor nominal do boleto
        'payer' => $payer,
        'documentKind' => 'DUPLICATA_MERCANTIL',
        'paymentType' => 'REGISTRO',
        'messages' => [
            'mensagem um',
            'mensagem dois'
        ],
        'discount' => $discount,
        'finePercentage' => '10.00',
        'FineDate' => '2023-08-15', //Multa data
        'interestPercentage' => '30.00', //Multa valor
        'writeOffQuantityDays' => '1',
        'deductionValue' => '0.10'
    ];
    
    return $data;
}

function makeWorkspaces($convenio, $loja, $nome) {
    $data = [
        'type' => 'BILLING',
        'covenants' => [
            [
                "code" => $convenio
            ]
        ],
        'description' => $nome,
        'bankSlipBillingWebhookActive' => true,
        'pixBillingWebhookActive' => true,
        'webhookURL' => 'https://teste.sivisweb.com.br/util/dcc/Santander/postbackBoleto.php?loja=' . $loja
    ];
    return $data;
}