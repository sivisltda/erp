<?php
require_once "vendor/autoload.php";

use MundiAPILib\Models;

$retorno = "";
$transacao = "";

function ValoresTextoMP($val) {
    return preg_replace('/[^A-Za-z0-9\- ]/', '', $val);
}

function ValoresNumericosMP($val) {
    $cent = str_replace(".", "", $val);
    $und = "00";
    $number = explode(',', $cent);
    if (count($number) > 1) {
        $cent = $number[0];
        $und = $number[1] . "00";
    }
    return $cent . substr($und, 0, 2);
}

if (isset($_POST["txtCredencial"])) {
    $basicAuthUserName = $_POST["txtCredencial"];
    $basicAuthPassword = '';
}

if (isset($_POST["btnEnviar"])) {
    try {
        $client = new MundiAPILib\MundiAPIClient($basicAuthUserName, $basicAuthPassword);
        $charges = $client->getCharges();
        $chargeRequest = new Models\CreateChargeRequest();
        $chargeRequest->amount = ValoresNumericosMP($_POST["txtValor"]);
        
        $customer = new Models\CreateCustomerRequest();
        $customer->name = ValoresTextoMP($_POST["txtHolderName"]);    
        $customer->email = "marcos@sivis.com.br";                
        $customer->document = "04639282702";
        $customer->type = "individual";
        $chargeRequest->customer = $customer;
        
        $payment = new Models\CreatePaymentRequest();
        //----------------------------------------------------------------------
        $chargeRequest->code = 1;
        $payment->paymentMethod = "boleto";        
        $boleto = new Models\CreateBoletoPaymentRequest();
        $boleto->bank = "033";        
        $boleto->dueAt = new \DateTime('2020-08-25T00:00:00Z');
        $payment->boleto = $boleto;
        //----------------------------------------------------------------------        
        /*$payment->paymentMethod = "debit_card";
        $debitCard = new Models\CreateDebitCardPaymentRequest();
        $debitCard->statementDescriptor = "Venda DCC " . $_POST["txtVenda"];
        $card = new Models\CreateCardRequest();
        $card->number = $_POST["txtCartao"];
        $card->cvv = $_POST["txtDv"];
        $card->holderName = ValoresTextoMP($_POST["txtHolderName"]);
        $card->expMonth = $_POST["txtMesValid"];
        $card->expYear = $_POST["txtAnoValid"];
        $debitCard->card = $card;
        $payment->debit_card = $debitCard;*/
        //----------------------------------------------------------------------        
//        $payment->paymentMethod = "credit_card";
//        $creditCard = new Models\CreateCreditCardPaymentRequest();
//        $creditCard->capture = true;
//        $creditCard->installments = $_POST["txtParcelas"];        
//        $card = new Models\CreateCardRequest();
//        $card->number = $_POST["txtCartao"];
//        $card->cvv = $_POST["txtDv"];
//        $card->holderName = ValoresTextoMP($_POST["txtHolderName"]);
//        $card->expMonth = $_POST["txtMesValid"];
//        $card->expYear = $_POST["txtAnoValid"];
//        $creditCard->card = $card;
//        $creditCard->statementDescriptor = "Venda DCC " . $_POST["txtVenda"];
//        $payment->creditCard = $creditCard;
//        
        //----------------------------------------------------------------------  
        //https://dashboard.pagar.me/#/myaccount/recipients
        //Guppi - rp_b7P49XRmuwi7B1rq(producao)        
//        $splitSivis = new Models\CreateSplitRequest(); //bruno@rio2c.com - rio2c@2018
//        $splitSivis->type = "percentage";
//        $splitSivis->amount = 99;        
//        $splitSivis->recipientId = "rp_ZR4JqQbInu4zJX18"; //rp_RGgW8LsbZtZgWjoY, rp_ZR4JqQbInu4zJX18(producao) //rp_5M9yLNYcwrtkpRwr(teste)
//        $splitSivisOptions = new Models\CreateSplitOptionsRequest();
//        $splitSivisOptions->liable = true;
//        $splitSivisOptions->chargeProcessingFee = true;
//        $splitSivis->options = $splitSivisOptions;        
//        $split[] = $splitSivis;                
//        $splitCliente = new Models\CreateSplitRequest();
//        $splitCliente->type = "percentage";
//        $splitCliente->amount = 1;        
//        $splitCliente->recipientId = "rp_QMw5KZnumjF3rPZe"; //rp_dxG4d6BhxpU1LBDy, rp_QMw5KZnumjF3rPZe(producao) //rp_n9pDReLsjhZ2MOv4(teste)           
//        $splitClienteOptions = new Models\CreateSplitOptionsRequest();
//        $splitClienteOptions->liable = false;
//        $splitClienteOptions->chargeProcessingFee = false;
//        $splitCliente->options = $splitClienteOptions;             
//        $split[] = $splitCliente;          
//        $payment->split = $split;
        //----------------------------------------------------------------------
        $chargeRequest->payment = $payment;        
        $result = $charges->createCharge($chargeRequest)->jsonSerialize();                
        $last_transaction = $result["last_transaction"]->jsonSerialize(); 
        print_r($result);              
        $retorno = "SET DATEFORMAT DMY;update sf_vendas_online set " .
                "responseCode = " . ($result["status"] == "paid" ? "'0'" : "'1'") . "," .
                "authCode = '" . ($result["status"] == "paid" ? $result["code"] : "") . "',orderID = '" . $result["gateway_id"] . "',transactionID = '" . $result["id"] . "'," .
                "transactionTimestamp = getdate(),responseMessage = " . ($result["status"] == "paid" ? "'CAPTURED'" : "'DECLINED'") . ",avsResponseCode = '',cvvResponseCode = ''," .
                "processorCode = " . valoresTexto2($last_transaction["acquirer_return_code"]) . "," .
                "processorMessage = " . ($result["status"] == "paid" ? "'APPROVED'" : "''") . "," .
                "errorMessage = " . ($result["status"] == "paid" ? "''" : valoresTexto2(substr($last_transaction["acquirer_message"], 0, 512))) . "," .
                "status_transacao = " . ($result["status"] == "paid" ? "'A'" : "'D'") . " WHERE id = " . $_POST["txtVenda"];
        if ($result["status"] == "paid") {
            echo $_POST["txtVenda"];
        } elseif ($result["status"] == "failed" && $last_transaction["status"] == "not_authorized") {
            echo "Operação Negada: " . $last_transaction["acquirer_message"];
        } elseif ($result["status"] == "failed" && $last_transaction["status"] == "with_error") {
            echo 'Erro, Timeout com a adquirente!';
        } elseif ($result["status"] == "processing") {
            echo 'Erro, Processamento com a adquirente!';
        } else {
            echo 'Erro, ' . $result["status"] . "," . $last_transaction["status"];
        }                
        echo "<br>" . $last_transaction["url"];        
    } catch (Exception $e) {
        $retorno = 'Erro, ' . $e->getMessage();
    }
}

if (isset($_POST["btnEstornar"])) {
    try {
        $client = new MundiAPILib\MundiAPIClient($basicAuthUserName, $basicAuthPassword);
        $charges = $client->getCharges();
        $cancelRequest = new Models\CreateCancelChargeRequest();
        $cancelRequest->amount = ValoresNumericosMP($_POST["txtValor"]);
        $result = $charges->cancelCharge($_POST["txtTransacao"], $cancelRequest)->jsonSerialize();
        if ($result["status"] == "canceled") {
            $retorno = "update Estornos set transactionID = '" . $result["id"] . "' WHERE cod_pagto = " . $_POST["txtVenda"];
        } else {
            $retorno = $result["status"];
        }
    } catch (Exception $e) {
        $retorno = 'Erro, ' . $e->getMessage();
    }
}

if (isset($_POST["btnListar"])) {
    $client = new MundiAPILib\MundiAPIClient($basicAuthUserName, $basicAuthPassword);
    $charges = $client->getCharges();
    $total = $charges->getChargesSummary()->jsonSerialize();
    $retorno = "Total de Transações: " . $total["total"];
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>MundiPagg</title>
        <style type="text/css">
            body{
                font-family: "Segoe UI", arial, sans-serif;
                font-size: 12px;
            }
            div{
                float: left;
                width: 45%;
                margin-right: 4%;
            }
            input,textarea{
                width: 100%;
            }
            p{
                margin-top: -25px;
                background-color: white;
                width: 100px;
                margin-bottom: 5px;
            }
            .borda{
                float: left;                
                border: 1px solid gray;
                width: 50%;
                padding: 15px;
                margin-bottom: 20px;
            }            
        </style>
    </head>
    <body>
        <form method="post" action="makeDCCMundi.php">
            <div class="borda">
                <p>Pagamento:</p>
                <div>
                    <label for="txtHolderName">Nome:</label>
                    <input id="txtHolderName" name="txtHolderName" value="<?php echo (isset($_POST["txtHolderName"]) ? $_POST["txtHolderName"] : "Viviane Ferreira") ?>" />
                </div>
                <div>
                    <label for="txtCredencial">Cred.:</label>
                    <input id="txtCredencial" name="txtCredencial" value="<?php echo (isset($_POST["txtCredencial"]) ? $_POST["txtCredencial"] : "sk_test_e08oWEYcPiOW1mJD") ?>" />
                </div>
                <div>
                    <label for="txtCartao">Cartão:</label>
                    <input id="txtCartao" name="txtCartao" value="<?php echo (isset($_POST["txtCartao"]) ? $_POST["txtCartao"] : "4011185771285580") ?>" />
                </div>
                <div style="width: 21%">
                    <label for="txtValor">Valor:</label>
                    <input id="txtValor" name="txtValor" value="<?php echo (isset($_POST["txtValor"]) ? $_POST["txtValor"] : "79,90") ?>" />
                </div>
                <div style="width: 20%">
                    <label for="txtVenda">Venda:</label>
                    <input id="txtVenda" name="txtVenda" value="<?php echo (isset($_POST["txtVenda"]) ? $_POST["txtVenda"] : "44") ?>" />
                </div>
                <div style="width: 7%">
                    <label for="txtMesValid">Validade:</label>
                    <input id="txtMesValid" name="txtMesValid" value="<?php echo (isset($_POST["txtMesValid"]) ? $_POST["txtMesValid"] : "12") ?>" />
                </div>
                <div style="width: 10%;margin-top: 17px;">
                    <label for="txtAnoValid"></label>
                    <input id="txtAnoValid" name="txtAnoValid" value="<?php echo (isset($_POST["txtAnoValid"]) ? $_POST["txtAnoValid"] : "2019") ?>" />
                </div>
                <div style="width: 20%">
                    <label for="txtDv">Dv:</label>
                    <input id="txtDv" name="txtDv" value="<?php echo (isset($_POST["txtDv"]) ? $_POST["txtDv"] : "651") ?>" />
                </div>
                <div style="margin-top: 16px;">
                    <input id="btnEnviar" name="btnEnviar" style="width: 100px;" type="submit" value="Enviar"/>
                </div>
            </div>
            <div class="borda">
                <p>Estorno:</p>
                <div>
                    <label for="txtTransacao">Transação:</label>
                    <input id="txtTransacao" name="txtTransacao" value="<?php echo $transacao; ?>" />
                </div>
                <div style="margin-top: 16px;">
                    <input id="btnEstornar" name="btnEstornar" style="width: 100px;" type="submit" value="Enviar"/>
                </div>                
            </div>    
            <div class="borda">
                <div class="borda" style="width: 95%;margin-bottom: 0px;"><?php echo $retorno; ?></div>
            </div>     
            <div>
                <input id="btnListar" name="btnListar" style="width: 100px;" type="submit" value="Total"/>
            </div>     
        </form>
    </body>
</html>
