<?php

$p = file_get_contents('php://input');
$dados = json_decode($p, true);

$empresa = "001";
if ($dados["account"]["id"] == "acc_N8Z2WLBtVVueDMAv") {
    $empresa = "007";    
} else if ($dados["account"]["id"] == "acc_nm8B1DKHQZF8oLog") {
    $empresa = "167";    
} else if ($dados["account"]["id"] == "acc_LlreprdncqcMB4PJ") {
    $empresa = "168";
} else if ($dados["account"]["id"] == "acc_VNJYG8pIkdUXxPdb") {
    $empresa = "173";
}

if ($dados["data"]["payment_method"] == "boleto" && in_array($dados["type"], ['charge.paid', 'charge.overpaid'])) {
    $url = __DIR__ . "./../../../Pessoas/" . $empresa . "/MundiPagg/webhooks/";    
} else {
    $url = __DIR__ . "./../../../Pessoas/" . $empresa . "/MundiPagg/" . date("Y/m/d") . "/";    
}

if (!file_exists($url)) {
    mkdir($url, 0777, true);
}

$filename = $url . $dados["data"]["id"] . "_" .date("d_m_Y__H_i_s") . ".txt";
$handle = fopen($filename, "x+");
fwrite($handle, $p);
fclose($handle);

if (!file_exists($filename)) {
    header('HTTP/1.1 524 Unauthorized');
    http_response_code(524);
}