<?php

if (!(isset($_GET["hostname"]) && isset($_GET["database"]) && isset($_GET["username"]) && isset($_GET["password"]))) {
    echo "Parametros inválidos";
    exit;
}

$hostname = $_GET["hostname"];
$database = $_GET["database"];
$username = $_GET["username"];
$password = $_GET["password"];
$contrato = str_replace("ERP", "", strtoupper($_GET['database']));
$con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());

require_once __DIR__ . "./../../../Connections/funcoesAux.php";

$url = __DIR__ . "./../../../Pessoas/" . $contrato . "/MundiPagg/webhooks/";
$dados = ['MensagemDCC' => '', 'qtdSucesso'  => 0, 'qtdErro'     => 0, 'TotalDcc'    => 0];
if (file_exists($url)) {
    $url_to = __DIR__ . "./../../../Pessoas/" . $contrato . "/MundiPagg/" . date("Y/m/d") . "/";
    if (!file_exists($url_to)) {
        mkdir($url_to, 0777, true);
    }
    $cdir = scandir($url);
    foreach ($cdir as $key => $value) {
        if (!in_array($value, array(".", ".."))) {
            $arquivo = read_webhooks($url . $value);
            $data = $arquivo["data"];
            $retorno = exec_webhooks($con, $data);
            if (strlen($retorno) > 0) {
                move_webhooks($url . $value, $retorno);
                rename($url . $value, $url_to . $value);
                $dados['qtdSucesso']++;
            } else {
                $dados['qtdErro']++;
            }
        }
    }
}

echo json_encode($dados);  
exit;

function move_webhooks($from_file, $text) {
    $myfile = fopen($from_file, "a") or die("Unable to open file!");
    fwrite($myfile, "\n" . $text);
    fclose($myfile);
}

function read_webhooks($file) {
    $json_data = file_get_contents($file);
    return json_decode($json_data, true);
}

function exec_webhooks($con, $data) {
    $cur = odbc_exec($con, "EXEC dbo.SP_MK_VENDA @tr_boleto = '" . $data['id'] . "', @data_pagamento = '" . $data['paid_at'] . "', @valor_pago = " . $data['paid_amount'] . ";");
    return odbc_result($cur, 1);
}
