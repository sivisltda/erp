<?php
/*
 * MundiAPILib
 *
 * 
 */

namespace MundiAPILib\Models;

use JsonSerializable;
use MundiAPILib\Utils\DateTimeHelper;

 /**
 *Response object for getting a charge
 */
class GetHookResponse implements JsonSerializable
{
    /**
     * @todo Write general description for this property
     * @required
     * @var string $id public property
     */
    public $id;

    /**
     * @todo Write general description for this property
     * @required
     * @var string $url public property
     */
    public $url;

    /**
     * @todo Write general description for this property
     * @required
     * @var string $event public property
     */
    public $event;

    /**
     * @todo Write general description for this property
     * @required
     * @var string $status public property
     */
    public $status;

    /**
     * @todo Write general description for this property
     * @required
     * @var string $attempts public property
     */
    public $attempts;

    /**
     * @todo Write general description for this property
     * @required
     * @maps last_attempt
     * @factory \MundiAPILib\Utils\DateTimeHelper::fromRfc3339DateTime
     * @var \DateTime $lastAttempt public property
     */
    public $lastAttempt;

    /**
     * @todo Write general description for this property
     * @required
     * @maps created_at
     * @factory \MundiAPILib\Utils\DateTimeHelper::fromRfc3339DateTime
     * @var \DateTime $createdAt public property
     */
    public $createdAt;

    /**
     * @todo Write general description for this property
     * @required
     * @maps response_status
     * @var integer $responseStatus public property
     */
    public $responseStatus;

     /**
     * Type event data response
     * @maps data
     * @var \MundiAPILib\Models\GetChargeResponse $data public property
     */
    public $data;


    /**
     * Constructor to set initial or default values of member properties
     * @param string                                  $id                      Initialization value for $this->id
     * @param string                                  $url                     Initialization value for $this->url
     * @param string                                  $event                   Initialization value for $this->event    
     * @param string                                  $status                  Initialization value for $this->status
     * @param string                                  $attempts                Initialization value for $this->attempts
     * @param \DateTime                               $lastAttempt             Initialization value for $this->lastAttempt                                                                          >createdAt
     * @param \DateTime                               $createdAt               Initialization value for $this->createdAt
     * @param integer                                 $responseStatus          Initialization value for $this->responseStatus
     * @param  \MundiAPILib\Models\GetChargeResponse  $data                Initialization value for $this->data                                                                                      
     */
    public function __construct()
    {
        if (9 == func_num_args()) {
            $this->id               = func_get_arg(0);
            $this->url              = func_get_arg(1);
            $this->event            = func_get_arg(2);
            $this->status           = func_get_arg(3);
            $this->attempts         = func_get_arg(4);
            $this->lastAttempt      = func_get_arg(5);
            $this->createdAt        = func_get_arg(6);
            $this->responseStatus   = func_get_arg(7);
            $this->data             = func_get_arg(8);
        }
    }

    /**
     * Encode this object to JSON
     */
    public function jsonSerialize()
    {
        $json = array();
        $json['id']                 = $this->id;
        $json['url']                = $this->url;
        $json['event']              = $this->event;
        $json['status']             = $this->status;
        $json['attempts']           = $this->attempts;
        $json['last_attempt']       = DateTimeHelper::toRfc3339DateTime($this->lastAttempt);
        $json['created_at']         = DateTimeHelper::toRfc3339DateTime($this->createdAt);
        $json['response_status']    = $this->responseStatus;
        $json['data']               = $this->data;
        return $json;
    }
}