<?php

require_once "vendor/autoload.php";

use MundiAPILib\Models;

function ValoresTextoMP($val) {
    return preg_replace('/[^A-Za-z0-9\- ]/', '', $val);
}

function ValoresNumericosMP($val) {
    $und = "00";
    $number = explode('.', $val);
    if (count($number) > 1) {
        $val = $number[0];
        $und = $number[1] . "00";
    }
    return $val . substr($und, 0, 2);
}

function valoresTextoPuro($val) {
    return preg_replace('/[^A-Za-z0-9\ ]/', '', $val);
}

function valorNumeroPuro($val) {
    return preg_replace('/[^0-9\ ]/', '', $val);
}

function makeChargeRequest($data) {
    $customer = new Models\CreateCustomerRequest();
    $customer->name = ValoresTextoMP($data["bname"]);
    $card = new Models\CreateCardRequest();
    $card->number = $data["number"];
    $card->cvv = $data["cvvNumber"];
    $card->holderName = ValoresTextoMP($data["bname"]);
    $card->expMonth = $data["expMonth"];
    $card->expYear = $data["expYear"];
    $payment = new Models\CreatePaymentRequest();
    if ($data["tpPayment"] == "1") {
        $creditCard = new Models\CreateCreditCardPaymentRequest();
        $creditCard->capture = true;
        $creditCard->installments = $data["parcelas"];
        $creditCard->card = $card;
        $payment->paymentMethod = "credit_card";
        $payment->creditCard = $creditCard;
    } else if ($data["tpPayment"] == "2") {
        $threedSecure = new Models\CreateThreeDSecureRequest();
        $threedSecure->mpi = "acquirer";
        $threedSecure->successUrl = "https://www.mundipagg.com";
        $authentication = new Models\CreatePaymentAuthenticationRequest();
        $authentication->type = 'threed_secure';
        $authentication->threedSecure = $threedSecure;
        $debitCard = new Models\CreateDebitCardPaymentRequest();
        $debitCard->card = $card;
        $debitCard->authentication = $authentication;
        $payment->paymentMethod = "debit_card";
        $payment->debitCard = $debitCard;
    }
    $chargeRequest = new Models\CreateChargeRequest();
    $chargeRequest->amount = ValoresNumericosMP($data["chargeTotal"]);
    $chargeRequest->customer = $customer;
    $chargeRequest->payment = $payment;
    return $chargeRequest;
}

function makeChargeRequestBoleto($data) {
    $customer = new Models\CreateCustomerRequest();
    $customer->name = $data['nome'];
    if ($data['email']) {
        $customer->email = $data['email'];
    }
    $customer->document = ValoresTextoPuro($data['cnpj']);
    $customer->type = "individual";
    if ($data['cep'] && $data['endereco'] && $data['cidade'] && $data['estado']) {
        $address = new stdClass();
        $address->line_1 = $data['numero'] . ', ' . $data['endereco'] . ', ' . $data['bairro'];
        $address->line_2 = $data['complemento'] ? utf8_encode($data['complemento']) : '';
        $address->zip_code = valorNumeroPuro($data['cep']);
        $address->city = $data['cidade'];
        $address->state = $data['estado'];
        $address->country = 'BR';
        $customer->address = $address;
    }

    $boleto = new Models\CreateBoletoPaymentRequest();
    $boleto->bank = "001";
    $boleto->instructions = "Pagar até o vencimento";
    $boleto->dueAt = new \DateTime(escreverData($data['data_vencimento'], 'Y-m-d') . 'T00:00:00Z');
    /*$boleto->maxDaysToPayPastDue = valorNumeroPuro($data['dias_limite']);

    if ($data["tipo_multa"] == "2" || $data["tipo_multa"] == "1") { //juros
        $juros = new Models\Interest();
        $juros->days = valorNumeroPuro($data['dias_taxa_multa'] + 1);
        $juros->type = ($data["forma_taxa_multa"] == "0" ? "percentage" : "flat");
        $juros->amount = $data['taxa_multa'];
        $boleto->interest = $juros;
    }

    if ($data["tipo_multa"] == "2" || $data["tipo_multa"] == "3") { //multa
        $multa = new Models\Fine();
        $multa->days = valorNumeroPuro($data['dias_taxa_multa'] + 1);
        $multa->type = ($data["forma_taxa_multa"] == "0" ? "percentage" : "flat");
        $multa->amount = $data['mora_multa'];
        $boleto->fine = $multa;
    }*/

    $payment = new Models\CreatePaymentRequest();
    $payment->paymentMethod = "boleto";
    $payment->boleto = $boleto;

    $chargeRequest = new Models\CreateChargeRequest();
    $chargeRequest->amount = ValoresNumericosMP($data['valor']);
    $chargeRequest->customer = $customer;
    $chargeRequest->payment = $payment;
    return $chargeRequest;
}

function makeCancelRequest($data) {
    $cancelRequest = new Models\CreateCancelChargeRequest();
    $cancelRequest->amount = ValoresNumericosMP($data["chargeTotal"]);
    return $cancelRequest;
}
