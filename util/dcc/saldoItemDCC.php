<?php

$tp_operacao = 0;
$data = array("period" => "range", "startDate" => "01/01/2012", "endDate" => date("m/d/Y"));
if (isset($_GET["hostname"]) && isset($_GET["database"]) && isset($_GET["username"]) && isset($_GET["password"])) {
    $hostname = $_GET["hostname"];
    $database = $_GET["database"];
    $username = $_GET["username"];
    $password = $_GET["password"];
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
    include "./../../Connections/funcoesAux.php";
}

if (isset($_GET["txtCredencial"])) {
    $credencial = $_GET["txtCredencial"];
}

if (isset($_GET["tp_operacao"])) {
    $tp_operacao = $_GET["tp_operacao"];
}

include "loadClassDcc.php";
if ($credencial == 0 && strlen($chave) > 0) {
    if ($dcc_empresa == 0) { //$maxiPago->setDebug(true);        
        $maxiPago->pullReport($data);
        if ($maxiPago->isErrorResponse()) {
            echo "Erro de conexão com portal! (Credencial 01)!";
            exit();
        } else {
            echo $maxiPago->getTotalNumberOfRecords();
        }
    } else if ($dcc_empresa == 1) {
        $total = $charges->getChargesSummary()->jsonSerialize();
        echo (is_numeric($total["total"]) ? $total["total"] : "0");
    } else if ($dcc_empresa == 2) {
        include "../../Connections/configSivis.php";
        $query = "select count(id_trans) total from ca_transacoes_rede where affiliation = " .
        valoresTexto2($loja) . " and environmentType = " . ($tipo == "LIVE" ? 1 : 0);
        $cur = odbc_exec($con2, $query);
        while ($RFP = odbc_fetch_array($cur)) {
            echo $RFP['total'];
        }
        odbc_close($con2);
    }
} elseif ($credencial == 1 && strlen($chave2) > 0) {
    if ($dcc_empresa2 == 0 && $loja != $loja2 && $chave != $chave2) {
        $maxiPago2->pullReport($data);
        if ($maxiPago2->isErrorResponse()) {
            echo "Erro de conexão com portal! (Credencial 02)!";
            exit();
        } else {
            echo $maxiPago2->getTotalNumberOfRecords();
        }
    } else if ($dcc_empresa2 == 1) {
        $total = $charges2->getChargesSummary()->jsonSerialize();
        echo (is_numeric($total["total"]) ? $total["total"] : "0");
    } else if ($dcc_empresa2 == 2) {
        include "../../Connections/configSivis.php";
        $query = "select count(id_trans) total from ca_transacoes_rede where affiliation = " .
        valoresTexto2($loja2) . " and environmentType = " . ($tipo2 == "LIVE" ? 1 : 0);
        $cur = odbc_exec($con2, $query);
        while ($RFP = odbc_fetch_array($cur)) {
            echo $RFP['total'];
        }
        odbc_close($con2);
    } else {
        echo "0";
    }
} else {
    echo "0";
}
odbc_close($con);
