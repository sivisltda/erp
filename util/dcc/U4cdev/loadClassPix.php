<?php

define('ENDPOINT', "https://hml-api.u4cdev.com/pix/brcode/erp/dynamic");
define('PARTNER_INFO', "71941c56-9dc6-4ac6-854c-6bc22307b0f9");
define('TOKEN_INFO', "df28c876-9f6e-11ea-bb37-0242ac130002");

function makeChargeRequestPix($RFP, $identificacao, $chave) {
    $data = new stdClass();
    $data->customerId = $chave;
    $data->externalId = $RFP["id_mens"];
    $data->dynamicQRCodeType = "BILLING_DUE_DATE";
        $content = new stdClass();
        $content->content = $identificacao;
    $data->additionalInformation[] = $content;
        $billingDueDate = new stdClass();
        $billingDueDate->dueDate = escreverData($RFP["data_vencimento"], "Y-m-d");
        $billingDueDate->daysAfterDueDate = 30;
        $payerInformation = new stdClass();
            $payerInformation->name = utf8_decode($RFP["nome"]);
            $payerInformation->cpfCnpj = str_replace(array(".", "-", "/"), "", $RFP["cnpj"]);
                $addressing = new stdClass();
                $addressing->street = utf8_decode($RFP["endereco"]);
                $addressing->city = utf8_decode($RFP["cidade"]);
                $addressing->uf = utf8_decode($RFP["estado"]);
                $addressing->cep = str_replace(array(".", "-"), "", $RFP["cep"]);
            $payerInformation->addressing = $addressing;
        $billingDueDate->payerInformation = $payerInformation;
            $paymentValue = new stdClass();
            $paymentValue->documentValue = valoresNumericos("txtValor");
            $discounts = new stdClass();
                $discounts->modality = 1;
                    $fixedDateDiscounts = new stdClass();
                    $fixedDateDiscounts->date = escreverData($RFP["data_vencimento"], "Y-m-d");
                    $fixedDateDiscounts->valuePerc = "0";
                $discounts->fixedDateDiscounts[] = $fixedDateDiscounts;                        
            $paymentValue->discounts = $discounts;                   
                $fines = new stdClass();
                $fines->modality = 1;
                $fines->valuePerc = 1;
            $paymentValue->fines = $fines;
                $interests = new stdClass();
                $interests->modality = 2;
                $interests->valuePerc = 0.5;
            $paymentValue->interests = $interests;
        $billingDueDate->paymentValue = $paymentValue;
    $data->billingDueDate = $billingDueDate;
    return $data;
}

function mkJson($data) {
    $header = [
        "Cache-Control: no-cache",
        "Content-Type: application/json",
        'partner: ' . PARTNER_INFO,
        'token: ' . TOKEN_INFO
    ];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, ENDPOINT);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $output = curl_exec($ch);
    curl_close($ch);
    return json_decode($output);
}
