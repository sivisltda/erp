<?php

if (!isset($_GET['pdf'])) {
    include '../../Connections/configini.php';
}

$iTotal = 0;
$iFilteredTotal = 0;
$sLimit = 20;
$imprimir = 0;
$agrupar = 0;
$sOrder = " ORDER BY dt_inicio_mens desc ";

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['grp'])) {
    $agrupar = $_GET['grp'];
}

$sQuery = "set dateformat dmy;
SELECT id_fornecedores_despesas,m.id_mens, dt_inicio_mens, dt_pagamento_mens, dbo.VALOR_REAL_MENSALIDADE(m.id_mens) valor_mens,
razao_social,(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'email',
case when (cnpj = '' or endereco = '' or numero = '' or bairro = '' or estado = '' or cidade = '' or cep = '') then 1 else 0 end validar,case when b.id_boleto is not null then b.id_boleto else bo.id_boleto end id_boleto,
case when b.id_boleto is not null then b.galaxy_id else bo.mundi_gateway_id end mundi_gateway_id,
case when b.id_boleto is not null then b.galaxy_id else bo.mundi_id end mundi_id,
case when b.id_boleto is not null then b.exp_remessa else bo.exp_email end exp_email,
case when b.id_boleto is not null then b.bol_link else bo.bol_descricao end bol_descricao
into #tempMensalidade 
FROM sf_vendas_planos_mensalidade m 
inner join sf_vendas_planos vp on id_plano = id_plano_mens
inner join sf_fornecedores_despesas on id_fornecedores_despesas = favorecido
inner join sf_produtos_parcelas on id_parc_prod_mens = id_parcela 
left join sf_boleto_online_itens bi on bi.id_mens = m.id_mens
left join sf_boleto_online b on b.id_boleto = bi.id_boleto
left join sf_boleto bo on m.id_mens = id_referencia and tp_referencia = 'M' ";
$sQuery .= "WHERE sf_fornecedores_despesas.tipo = 'C' and vp.dt_cancelamento is null and sf_fornecedores_despesas.inativo = 0 and parcela = -1" . 
(is_numeric($_GET['sts']) ? " and id_item_venda_mens is null" : "") .
(is_numeric($_GET['pln']) ? " and vp.id_plano = " . $_GET['pln'] : "");
if (is_numeric($_GET['tcl']) && $_GET['tcl'] == "1" && is_numeric($_GET['cli'])) {
    $sQuery .= " and sf_fornecedores_despesas.id_fornecedores_despesas = " . $_GET['cli'];
}
if ($_GET['dck'] == "1" && is_numeric($_GET['dtp']) && $_GET['din'] != "" && $_GET['dfn'] != "") {
    $sQuery .= " and " . ($_GET['dtp'] == 1 ? "cast(dt_pagamento_mens as date)" : "dt_inicio_mens") . " between " . valoresData2($_GET['din']) . " and " . valoresData2($_GET['dfn']) . "";
}
if (is_numeric($_GET['sts'])) {
    switch ($_GET['sts']) {
        case 1: //GERADOS
            $sQuery .= " and (b.inativo = 0 or bo.inativo = 0) ";
            break;
        case 2: //NÃO GERADOS
            $sQuery .= " and b.inativo is null and bo.inativo is null and dt_pagamento_mens is null and dt_cancelamento is null ";
            break;
        case 3: //ENVIADOS
            $sQuery .= " and (b.inativo = 0 or bo.inativo = 0) and ((b.id_boleto is not null and b.exp_remessa = 1) or (bo.id_boleto is not null and bo.exp_email = 1)) ";
            break;
        case 4: //NÃO ENVIADOS
            $sQuery .= " and (b.inativo = 0 or bo.inativo = 0) and ((b.id_boleto is not null and b.exp_remessa in (0,2)) or (bo.id_boleto is not null and bo.exp_email in (0,2))) ";
            break;
        case 5: //CANCELADOS
            $sQuery .= " and (b.inativo = 1 or bo.inativo = 1) ";
            break;
    }
}
//echo $sQuery; exit;
odbc_exec($con, $sQuery) or die(odbc_errormsg());

if ($agrupar == 1) {
    $sQuery = "select id_fornecedores_despesas, min(dt_inicio_mens) dt_inicio_mens, min(dt_pagamento_mens) dt_pagamento_mens, sum(valor_mens) valor_mens, 
    max(razao_social) razao_social, max(email) email, max(validar) validar,
    max(id_boleto) id_boleto, max(mundi_gateway_id) mundi_gateway_id, max(mundi_id) mundi_id, max(exp_email) exp_email, max(bol_descricao) bol_descricao
    into #tempMensalidadeGroup from #tempMensalidade m group by id_fornecedores_despesas";
    odbc_exec($con, $sQuery) or die(odbc_errormsg());    
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

$sQuery = "SELECT COUNT(*) total FROM " . ($agrupar == 1 ? "#tempMensalidadeGroup" : "#tempMensalidade");
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];    
    $iFilteredTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row, ";
if ($agrupar == 1) {
    $sQuery1 .= "*, STUFF((SELECT distinct '|' + cast(id_mens as varchar) FROM #tempMensalidade vp 
    WHERE vp.id_fornecedores_despesas = m.id_fornecedores_despesas FOR XML PATH('')), 1, 1, '') as id_mens
    FROM #tempMensalidadeGroup m";    
} else {    
    $sQuery1 .= "* FROM #tempMensalidade";    
}
$sQuery1 .= ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1=" . $imprimir . " " . $sOrder;
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();          
    $boletoLink = "";
    $invalidb = (!is_numeric($aRow["id_boleto"]) && ($aRow["validar"] == 1 || $aRow["email"] == "") ? "<span style='color:red;';>" : "");
    $invalide = (!is_numeric($aRow["id_boleto"]) && ($aRow["validar"] == 1 || $aRow["email"] == "") ? "</span>" : "");            
    $row[] = "<center>" . $invalidb . (!is_numeric($aRow["id_boleto"]) ? "<input name=\"nEnviadoB[]\" type=\"hidden\" value=\"" . $aRow["id_mens"] . "\"/>" : "") . escreverData($aRow["dt_inicio_mens"]) . $invalide . "</center>";
    $row[] = $invalidb . utf8_encode($aRow["id_fornecedores_despesas"]) . $invalide;
    $row[] = "<a href='javascript:void(0)' onClick='AbrirCli(" . utf8_encode($aRow["id_fornecedores_despesas"]) . ")'><div id='formPQ'>" . utf8_encode($aRow["razao_social"]) . "</div></a>";            
    $row[] = $invalidb . utf8_encode($aRow["mundi_gateway_id"]) . $invalide;
    $row[] = $invalidb . utf8_encode($aRow["mundi_id"]) . $invalide;    
    $row[] = $invalidb . escreverNumero($aRow["valor_mens"]) . $invalide;
    $row[] = $invalidb . escreverData($aRow["dt_pagamento_mens"]) . $invalide;
    $row[] = (is_numeric($aRow["id_boleto"]) ? "<center><a style=\"margin-right: 6px;\" href=\"" . $aRow["bol_descricao"] . "\" target=\"_blank\"><img src=\"../../img/boleto.png\"></a></center>" : "");
    $row[] = $invalidb . ($aRow["exp_email"] == 1 ? "<img src=\"../../img/1368741588_check.png\" style=\"height:16px;\" alt=\"\">" : "") . $invalide;
    if ($imprimir > 0) {
        $row[] = utf8_encode($aRow["email"]);    
        $row[] = $aRow["id_fornecedores_despesas"];    
        $row[] = $aRow["id_mens"];
    }
    $output['aaData'][] = $row;    
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);