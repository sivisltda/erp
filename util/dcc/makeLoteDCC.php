<?php

$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
if (isset($_POST['GeraDCC'])) {
    try {
        $explode = explode(",", $_POST['Mens']);
        if (count($explode) > 0 && strlen($_POST['Mens']) > 0) {
            include "preProcessamentoDCC.php";
            //if ($totalSivis > ($totalMaxpago + count($explode)) || substr($contrato, 0, 1) == "9" || $dcc_tp_operacao == 1) { //|| $tipo == "TEST"
                $totalSucesso = 0;
                $totalErro = 0;
                $rows = array();
                $_POST['isList'] = 1;
                $_POST['btnSalvar'] = 1;
                $queryListaDCC = "set dateformat dmy;select id_mens, valor_mens, id_fornecedores_despesas, empresa, id_produto,id_plano,
                (select max(id_cartao) from sf_fornecedores_despesas_cartao where id_fornecedores_despesas = C.favorecido) id_cartao 
                from sf_vendas_planos_mensalidade A
                inner join sf_produtos_parcelas B on (A.id_parc_prod_mens = B.id_parcela and parcela = 0)
                inner join sf_vendas_planos C on A.id_plano_mens = C.id_plano AND C.dt_cancelamento is null
                inner join sf_fornecedores_despesas D on C.favorecido = D.id_fornecedores_despesas
                and dt_pagamento_mens is null and D.inativo = 0
                and (select count(id_agendamento) from sf_vendas_planos_dcc_agendamento
                where id_plano_agendamento = C.id_plano and dateadd(month,1,dt_inicio_mens) > dt_cancel_agendamento) = 0
                and id_mens in (" . $_POST['Mens'] . ")";
                $cur = odbc_exec($con, $queryListaDCC) or die(odbc_errormsg());
                while ($RFP = odbc_fetch_array($cur)) {
                    $dadosConv = "?&verificaConvenio=" . $RFP['id_fornecedores_despesas'] .
                            "&idProd=" . $RFP['id_produto'] .
                            "&idPlano=" .$RFP['id_mens'] .                            
                            "&idConvProd=" . $RFP['id_plano'] .                            
                            "&hostname=" . $hostname .
                            "&database=" . $database .
                            "&username=" . $username .
                            "&password=" . $password .
                            "&valorProduto=" . $RFP['valor_mens'];
                    $jsonurl = $url . "/modulos/academia/form/ClientesFormServer.php" . $dadosConv;
                    $jsonValorCon = file_get_contents($jsonurl);
                    $dadosAcres = "?&verificaAcrescimo=" . $RFP['id_fornecedores_despesas'] .
                            "&idProd=" . $RFP['id_plano'] .
                            "&hostname=" . $hostname .
                            "&database=" . $database .
                            "&username=" . $username .
                            "&password=" . $password .
                            "&valorProduto=" . $RFP['valor_mens'];
                    $jsonurl = $url . "/modulos/academia/form/ClientesFormServer.php" . $dadosAcres;
                    $jsonValorAcres = file_get_contents($jsonurl);
                    $valorFinal = ($RFP['valor_mens'] + str_replace(',', '.', $jsonValorAcres)) - str_replace(',', '.', $jsonValorCon);
                    $dados = "?txtCartao=" . $RFP['id_cartao'] .
                            "&txtValor=" . escreverNumero($valorFinal) . //$RFP['valor_mens']
                            "&txtPlanoItem=" . $RFP['id_mens'] .
                            "&login_usuario=" . $_SESSION["login_usuario"] .
                            "&hostname=" . $hostname .
                            "&database=" . $database .
                            "&username=" . $username .
                            "&password=" . $password;
                    $jsonurl = $url . "/util/dcc/makeItemDCC.php" . $dados;
                    $json = file_get_contents($jsonurl);
                    if (is_numeric($json)) {
                        $totalSucesso = $totalSucesso + 1;
                        $dadosPagamento = "?Trn=" . $json .
                                "&login_usuario=" . $_SESSION["login_usuario"] .
                                "&hostname=" . $hostname .
                                "&database=" . $database .
                                "&username=" . $username .
                                "&password=" . $password;
                        $jsonUrlPg = $url . "/Modulos/Sivis/Dcc_Transacao_Pagamento.php" . $dadosPagamento;
                        $jsonreult = file_get_contents($jsonUrlPg);
                    } else {
                        $totalErro = $totalErro + 1;
                    }
                }
                $rows['MensagemDCC'] = '';
                $rows['TotalSucesso'] = $totalSucesso;
                $rows['TotalErro'] = $totalErro;
                print(json_encode($rows));
            /*} else {
                echo "Saldo em transações insuficiente para esta operação!";
            }*/
        } else {
            echo "Não há transações para esta operação!";
        }
    } catch (Exception $e) {
        echo "Erro no Processamento!" . $e->getMessage() . " in " . $e->getFile() . " on line " . $e->getLine();
    }
}