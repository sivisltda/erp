<?php
require('vendor/autoload.php');

$transacao = "";

function ValoresNumericosMP($val) {
    $cent = str_replace(".", "", $val);
    $und = "00";
    $number = explode(',', $cent);
    if (count($number) > 1) {
        $cent = $number[0];
        $und = $number[1] . "00";
    }
    return $cent . substr($und, 0, 2);
}

if (isset($_POST["btnEnviar"])) {
    try {
        $pagarme = new PagarMe\Client($_POST["txtCredencial"]);
        $transaction = $pagarme->transactions()->create([
            'amount' => ValoresNumericosMP($_POST["txtValor"]),
            'payment_method' => 'boleto',
            'async' => false,
            'postback_url' => 'https://teste.sivisweb.com.br/util/dcc/Pagarme/postbackBoleto.php?loja=007',
            'customer' => [
                'external_id' => $_POST["txtVenda"],
                'name' => $_POST["txtHolderName"],
                'type' => 'individual',
                'country' => 'br',
                'documents' => [
                    [
                        'type' => 'cpf',
                        'number' => str_replace([".", "-"], "", $_POST["txtCpf"])
                    ]
                ],
                'phone_numbers' => ['+5524988887777'],
                'email' => 'cliente@email.com'
            ]
        ]);
        $transacao = $transaction->id;
        print_r($transaction);
    } catch (Exception $e) {
        $retorno = 'Erro, ' . $e->getMessage();
    }
}

if (isset($_POST["btnPagar"])) {
    $pagarme = new PagarMe\Client($_POST["txtCredencial"]);
    $paidBoleto = $pagarme->transactions()->simulateStatus([
        'id' => $_POST["txtPagar"], 'status' => 'paid'
    ]);
    print_r($paidBoleto);
}

if (isset($_POST["btnCriar"])) {
    $retorno = "";
    echo str_replace("&", "<br>", urldecode($retorno));
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Pagarme</title>
        <style type="text/css">
            body{
                font-family: "Segoe UI", arial, sans-serif;
                font-size: 12px;
            }
            div{
                float: left;
                width: 45%;
                margin-right: 4%;
            }
            input,textarea{
                width: 100%;
            }
            p{
                margin-top: -25px;
                background-color: white;
                width: 100px;
                margin-bottom: 5px;
            }
            .borda{
                float: left;                
                border: 1px solid gray;
                width: 50%;
                padding: 15px;
                margin-bottom: 20px;
            }            
        </style>
    </head>
    <body>
        <form method="post" action="index.php">
            <div class="borda">
                <p>Boleto</p>
                <div>
                    <label for="txtHolderName">Nome:</label>
                    <input id="txtHolderName" name="txtHolderName" value="<?php echo (isset($_POST["txtHolderName"]) ? $_POST["txtHolderName"] : "Marcos R. Freitas") ?>" />
                </div>
                <div>
                    <label for="txtCredencial">Cred.:</label>
                    <input id="txtCredencial" name="txtCredencial" value="<?php echo (isset($_POST["txtCredencial"]) ? $_POST["txtCredencial"] : "ak_test_NJI7IChwpot4uIj3p5DpNUKsif1b0I") ?>" />
                </div>
                <div>
                    <label for="txtCartao">CPF:</label>
                    <input id="txtCpf" name="txtCpf" value="<?php echo (isset($_POST["txtCpf"]) ? $_POST["txtCpf"] : "201.991.310-07") ?>" />
                </div>
                <div style="width: 21%">
                    <label for="txtValor">Valor:</label>
                    <input id="txtValor" name="txtValor" value="<?php echo (isset($_POST["txtValor"]) ? $_POST["txtValor"] : "103,80") ?>" />
                </div>
                <div style="width: 20%">
                    <label for="txtVenda">Venda:</label>
                    <input id="txtVenda" name="txtVenda" value="<?php echo (isset($_POST["txtVenda"]) ? $_POST["txtVenda"] : "44") ?>" />
                </div>                
                <div style="width: 20%; margin-top: 16px;">
                    <input id="btnEnviar" name="btnEnviar" style="width: 100px;" type="submit" value="Enviar"/>
                </div>                
            </div>
            <div class="borda">
                <p>Pagar:</p>
                <div>
                    <label for="txtPagar">Transação:</label>
                    <input id="txtPagar" name="txtPagar" value="<?php echo $transacao; ?>" />
                </div>
                <div style="margin-top: 16px;">
                    <input id="btnPagar" name="btnPagar" style="width: 100px;" type="submit" value="Enviar"/>
                </div>                
            </div>
            <div class="borda">
                <div class="borda" style="width: 95%;margin-bottom: 0px;">                    
                    <select id="txtTipoList" name="txtTipoList" style="width: 100px; margin-left: 3%;">
                        <option value="0">Total</option>
                        <option value="1">Pagos</option>
                    </select>
                    <input id="btnListar" name="btnListar" style="width: 100px; float: right;" type="submit" value="Total"/>
                </div>                
            </div>             
        </form>
    </body>
</html>