<?php

require('vendor/autoload.php');

function ValoresTextoMP($val) {
    return preg_replace('/[^A-Za-z0-9\- ]/', '', $val);
}

function ValoresNumericosMP($val) {
    $und = "00";
    $number = explode('.', $val);
    if (count($number) > 1) {
        $val = $number[0];
        $und = $number[1] . "00";
    }
    return $val . substr($und, 0, 2);
}

function valoresTextoPuro($val) {
    return preg_replace('/[^A-Za-z0-9\ ]/', '', $val);
}

function valorNumeroPuro($val) {
    return str_replace(" ", "", preg_replace('/[^0-9\ ]/', '', $val));
}

function makeChargeRequestBoleto($data, $loja) {
    $numeroDocumento = ValoresTextoPuro($data['cnpj']);
    $documento = [];
    $documento["type"] = ((strlen($numeroDocumento) == 14) ? "cnpj" : "cpf");
    $documento["number"] = $numeroDocumento;
    $customer = [];
    $customer["external_id"] = $data["id_aluno"];
    $customer["name"] = $data['nome'];
    $customer["type"] = ((strlen($numeroDocumento) == 14) ? "corporation": "individual");
    $customer["country"] = "br";

    $customer["documents"][] = $documento;
    if ($data['celular']) {
        $customer["phone_numbers"][] = "+55" . valorNumeroPuro($data['celular']);
    }
    if ($data['email']) {
        $customer["email"] = $data['email'];
    }
    if ($data['data_nascimento']) {
        $customer["birthday"] = trim(implode('-', array_reverse(explode('/', $data['data_nascimento']))));
    }
    $toReturn = [];
    $toReturn["amount"] = ValoresNumericosMP($data["valor"]);
    $toReturn["payment_method"] = "boleto";
    $toReturn["async"] = false;
    $toReturn["postback_url"] = "https://" . $_SERVER['HTTP_HOST'] . "/util/dcc/Pagarme/postbackBoleto.php?loja=" . $loja;
    $toReturn["customer"] = $customer;

    if ($data['cep'] && $data['endereco'] && $data['cidade'] && $data['estado']) {
        $address = [];
        $address["country"] = "br";
        $address["state"] = $data['estado'];
        $address["city"] = $data['cidade'];
        $address["neighborhood"] = $data['bairro'];
        $address["street"] = $data['endereco'];
        $address["street_number"] = $data['numero'];
        if (strlen($data['complemento']) > 0) {
            $address["complementary"] = $data['complemento'];
        }
        $address["zipcode"] = valorNumeroPuro($data['cep']);
        $billing = [];
        $billing["name"] = $data['nome'];
        $billing["address"] = $address;
        $toReturn["billing"] = $billing;
    }

    return $toReturn;
}