<?php

require("vendor/autoload.php");
$pagarme = new PagarMe\Client('ak_test_NJI7IChwpot4uIj3p5DpNUKsif1b0I');
$read = file_get_contents("php://input");

$conexao = [];
$content = date("d/m/Y H:i:s") . "\r\n";
$isValidPostback = $pagarme->postbacks()->validate($read, $_SERVER['HTTP_X_HUB_SIGNATURE']);
if ($isValidPostback) {
    $content .= "Sucesso " . $_GET["loja"] . "\r\n";
    $content .= "id: " . $_REQUEST["id"] . "\r\n";
    $content .= "status: " . $_REQUEST["current_status"] . "\r\n";
    $content .= "pago: " . $_REQUEST["transaction"]["paid_amount"] . "\r\n";
    $content .= "custo: " . $_REQUEST["transaction"]["cost"] . "\r\n";
    $content .= "transacao: " . $_REQUEST["transaction"]["acquirer_id"] . "\r\n";
    $content .= "tipo: " . $_REQUEST["transaction"]["payment_method"] . "\r\n";
} else {
    $content .= "erro:" . $read;
}

if ($isValidPostback && $_REQUEST["current_status"] == "paid" && is_numeric($_REQUEST["id"]) && is_numeric($_GET["loja"])) {    
    require_once "../../../Connections/configSivis.php";
    require_once "../../../Connections/funcoesAux.php";    
    /*$sql = "select top 1 * from ca_contratos inner join ca_clientes on ca_clientes.sf_cliente = id_cliente        
    where inativa = 0 and numero_contrato = '" . $_GET["loja"] . "'";*/    
    $sql = "select top 1 * from ca_contratos where inativa = 0 and numero_contrato = '" . $_GET["loja"] . "'";
    $res = odbc_exec($con2, $sql);
    while ($row = odbc_fetch_array($res)) {
        $conexao['host'] = utf8_encode($row['local']);
        $conexao['login'] = utf8_encode($row['login']);
        $conexao['senha'] = utf8_encode($row['senha']);
        $conexao['banco'] = utf8_encode($row['banco']);
        $conexao['numero_contrato'] = $row['numero_contrato'];
    }    
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' . ($_SERVER['HTTP_HOST'] === 'localhost' ? $_SERVER['HTTP_HOST'] . '/erp' : $_SERVER['HTTP_HOST']);
    if ($_REQUEST["transaction"]["payment_method"] == "boleto") {
        $params = "?Trn=" . $_REQUEST["id"] 
                . "&hostname=" . $conexao['host'] . "&username=" . $conexao['login'] . "&password=" . $conexao['senha'] . "&database=" . $conexao['banco'] . "&contrato=" . $conexao['numero_contrato'] 
                . "&valorpago=" . $_REQUEST["transaction"]["paid_amount"]
                . "&datapagto=" . date("d/m/Y") . "&status=charge.paid";
        $response = file_get_contents($url . "/Modulos/Sivis/BaixaBoletoWebhook.php" . $params);
        if (is_numeric($response)) {
            echo "OK";            
        } else {            
            echo "Erro: " . $response;            
        }
        $content .= (is_numeric($response) ? "Sucesso:" : "Erro: ") . $response;        
    }
}

$url = __DIR__ . "./../../../Pessoas/" . $_GET["loja"] . "/Pagarme/" . date("Y/m/d") . "/";
if (!file_exists($url)) {
    mkdir($url, 0777, true);
}
$handle = fopen($url . substr($_REQUEST["current_status"], 0, 2) . "_" . date("d_m_Y__H_i_s") . ".txt", "x+");
fwrite($handle, $content);
fclose($handle);
