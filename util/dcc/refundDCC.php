<?php

if (isset($_POST["btnSalvar"])) {
    include "preProcessamentoDCC.php";
    //if ($totalSivis > $totalMaxpago || substr($contrato, 0, 1) == "9" || $dcc_tp_operacao == 1) { //|| $tipo == "TEST" 
        if (is_numeric(valoresSelect("txtPlanoItem")) || is_numeric(valoresSelect("txtVendaItem"))) {
            $cur = odbc_exec($con, "select * from sf_vendas_itens_dcc where processorMessage = 'APPROVED' and status_transacao = 'A' and " . 
            (is_numeric(valoresSelect("txtPlanoItem")) ? "id_plano_item = " . valoresSelect("txtPlanoItem") : 
            "id_venda_transacao = " . valoresSelect("txtVendaItem")));                                
            while ($RFP = odbc_fetch_array($cur)) {
                $id_aluno = $RFP['id_fornecedores_despesas'];
                $nome_cartao = $RFP['dcc_nome_cartao'];
                $dv_cartao = str_replace(" ", "", $RFP['dcc_dv']);
                $validade_cartao = date_format(date_create($RFP['dcc_validade_cartao']), 'd/m/Y');
                $legenda_cartao = $RFP['dcc_numero_cartao'];
                $orderID = $RFP['orderID'];
                $transactionID = $RFP['transactionID'];
                $valor = escreverNumero($RFP['valor_transacao']);
                $txtTipoPag = $RFP['id_tipo_documento'];
                $dt_mensalidade = date_format(date_create($RFP['dt_modalidade']), 'd/m/Y');
                $produto = $RFP['id_produto'];
                $credencial = $RFP['credencial'];
            }
            if ($valor > 0 && $valor < 10000) {
                include "loadClassDcc.php";
                if (is_numeric($id_aluno) && is_numeric($txtTipoPag) && is_numeric($produto) && strlen($dt_mensalidade) > 0 && strlen($nome_cartao) > 0 && strlen($orderID) > 0 && strlen($transactionID) > 0 && is_numeric($dv_cartao) && is_numeric($credencial)) {
                    $query = "SET DATEFORMAT DMY;INSERT INTO sf_vendas_itens_dcc(id_plano_item,dcc_nome_cartao,dcc_validade_cartao,dcc_numero_cartao,dcc_dv,f_referencia,valor_transacao,id_fornecedores_despesas,id_tipo_documento,dt_modalidade,id_produto,credencial,mkr_login)
                    VALUES (" . valoresSelect("txtPlanoItem") . "," . valoresTexto2($nome_cartao) . "," . valoresTexto2($validade_cartao) . "," . valoresTexto2($legenda_cartao) . "," . valoresTexto2($dv_cartao) . ",'01/01'," . valoresNumericos2($valor) . "
                    ," . $id_aluno . "," . $txtTipoPag . "," . valoresData2($dt_mensalidade) . "," . $produto . "," . $credencial . "," . valoresTexto2(strtoupper($_SESSION["login_usuario"])) . ")
                    SELECT SCOPE_IDENTITY() ID;";
                    $result = odbc_exec($con, $query) or die(odbc_errormsg());
                    odbc_next_result($result);
                    $id = odbc_result($result, 1);
                    if (is_numeric($id)) {
                        $data = array(
                            "orderID" => $orderID,
                            "referenceNum" => valoresSelect("txtPlanoItem"),
                            "chargeTotal" => valoresNumericos2($valor)
                        );
                        $url = "https://sivis.com.br/erede/util/dcc/Erede/action/cancelDcc.php";
                        if ($credencial == 0 && strlen($chave) > 0) {
                            try {
                                if ($dcc_empresa == 0) {
                                    $maxiPago->creditCardRefund($data);
                                    $query = "SET DATEFORMAT DMY;UPDATE sf_vendas_itens_dcc SET responseCode = " . valoresTexto2($maxiPago->response["responseCode"]) . "," .
                                            "authCode = " . valoresTexto2($maxiPago->response["authCode"]) . "," .
                                            "orderID = " . valoresTexto2($maxiPago->response["orderID"]) . "," .
                                            "transactionID = " . valoresTexto2($maxiPago->response["transactionID"]) . "," .
                                            "transactionTimestamp = " . valoresTexto2(date('d/m/Y H:i:s', $maxiPago->response["transactionTimestamp"])) . "," .
                                            "responseMessage = " . valoresTexto2($maxiPago->response["responseMessage"]) . "," .
                                            "avsResponseCode = " . valoresTexto2($maxiPago->response["avsResponseCode"]) . "," .
                                            "cvvResponseCode = " . valoresTexto2($maxiPago->response["cvvResponseCode"]) . "," .
                                            "processorCode = " . valoresTexto2($maxiPago->response["processorCode"]) . "," .
                                            "processorMessage = " . valoresTexto2('SETTLED') . "," .
                                            "errorMessage = " . valoresTexto2($maxiPago->response["errorMessage"]) . "," .
                                            "f_inicio = 1, f_intervalo = 'monthly', f_recorrencia = 1 " .
                                            "WHERE id_transacao = " . $id;
                                    $result = odbc_exec($con, $query) or die(odbc_errormsg());
                                    $query = "SET DATEFORMAT DMY;UPDATE sf_vendas_itens_dcc SET status_transacao = 'E' " .
                                            "WHERE orderID = " . valoresTexto2($maxiPago->response["orderID"]);
                                    $result = odbc_exec($con, $query) or die(odbc_errormsg());
                                    if ($maxiPago->isErrorResponse()) {
                                        echo "Falha de Transação!" . $maxiPago->getMessage();
                                    } elseif ($maxiPago->isTransactionResponse()) {
                                        if ($maxiPago->getResponseCode() == "0") {
                                            echo $id;
                                        } else {
                                            echo "Operação Negada para este cartão!";
                                        }
                                    }
                                } else if ($dcc_empresa == 1) {
                                    $result = $charges->cancelCharge($transactionID, makeCancelRequest($data))->jsonSerialize();
                                    $query = "SET DATEFORMAT DMY;UPDATE sf_vendas_itens_dcc SET responseCode = '0'," .
                                            "authCode = '',orderID = " . valoresTexto2($result["gateway_id"]) . "," .
                                            "transactionID = " . valoresTexto2($result["id"]) . "," .
                                            "transactionTimestamp = getdate()," .
                                            "responseMessage = 'CAPTURED', avsResponseCode = '',cvvResponseCode = ''," .
                                            "processorCode = 'A',processorMessage = " . valoresTexto2('SETTLED') . "," .
                                            "errorMessage = '" . ($result["status"] == "canceled" ? "" : $result["status"]) . "'," .
                                            "f_inicio = 1, f_intervalo = 'monthly', f_recorrencia = 1 " .
                                            "WHERE id_transacao = " . $id;
                                    odbc_exec($con, $query) or die(odbc_errormsg());
                                    if ($result["status"] == "canceled") {
                                        $query = "SET DATEFORMAT DMY;UPDATE sf_vendas_itens_dcc SET status_transacao = 'E' " .
                                                "WHERE orderID = " . valoresTexto2($result["gateway_id"]);
                                        odbc_exec($con, $query) or die(odbc_errormsg());
                                        echo $id;
                                    } else {
                                        echo "Operação Negada para este cartão! " . $result["status"];
                                    }
                                } else if ($dcc_empresa == 2) {
                                    $postfields = array(
                                        'txtAffiliation' => $loja,
                                        'txtKey' => $chave,
                                        'txtEnvironmentType' => ($tipo == "LIVE" ? 1 : 0),
                                        'txtOperadora' => $operadora,
                                        'txtTid' => $orderID,
                                    );
                                    $ch = curl_init();
                                    curl_setopt($ch, CURLOPT_URL, $url);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                                    curl_setopt($ch, CURLOPT_POST, 1);
                                    curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
                                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                                    $res = curl_exec($ch);
                                    $retorno = json_decode($res);
                                    $query = "SET DATEFORMAT DMY;UPDATE sf_vendas_itens_dcc SET responseCode = " . valoresTexto2($retorno->returnCode) . "," .
                                            "authCode = " . valoresTexto2("") . ",orderID = " . valoresTexto2($retorno->tid) . "," .
                                            "transactionID = " . valoresTexto2($retorno->nsu) . "," .
                                            "transactionTimestamp = " . valoresTexto2($retorno->data_retorno) . "," .
                                            "responseMessage = " . valoresTexto2("CAPTURED") . "," .
                                            "avsResponseCode = " . valoresTexto2("") . "," .
                                            "cvvResponseCode = " . valoresTexto2("") . "," .
                                            "processorCode = " . ($retorno->returnCode == "00" ? "'A'" : "'D'") . "," .
                                            "processorMessage = " . ($retorno->returnCode == "00" ? "'SETTLED'" : "'DECLINED'") . "," .
                                            "errorMessage = " . ($retorno->returnCode == "00" ? "''" : valoresTexto2(substr($retorno->message), 0, 50)) . "," .
                                            "f_inicio = 1, f_intervalo = 'monthly', f_recorrencia = 1 " .
                                            "WHERE id_transacao = " . $id;
                                    $result = odbc_exec($con, $query) or die(odbc_errormsg());
                                    $query = "SET DATEFORMAT DMY;UPDATE sf_vendas_itens_dcc SET status_transacao = 'E' " .
                                            "WHERE orderID = " . valoresTexto2($retorno->tid);
                                    $result = odbc_exec($con, $query) or die(odbc_errormsg());
                                    if ($retorno->returnCode == "00") {
                                        echo $id;
                                    } else {
                                        echo "Operação Negada para este cartão!";
                                    }
                                }
                            } catch (Exception $e) {
                                $retorno = 'Erro, ' . $e->getMessage();
                            }
                        } else if ($credencial == 1 && strlen($chave2) > 0) {
                            try {
                                if ($dcc_empresa2 == 0) {
                                    $maxiPago2->creditCardRefund($data);
                                    $query = "SET DATEFORMAT DMY;UPDATE sf_vendas_itens_dcc SET responseCode = " . valoresTexto2($maxiPago2->response["responseCode"]) . "," .
                                            "authCode = " . valoresTexto2($maxiPago2->response["authCode"]) . "," .
                                            "orderID = " . valoresTexto2($maxiPago2->response["orderID"]) . "," .
                                            "transactionID = " . valoresTexto2($maxiPago2->response["transactionID"]) . "," .
                                            "transactionTimestamp = " . valoresTexto2(date('d/m/Y H:i:s', $maxiPago2->response["transactionTimestamp"])) . "," .
                                            "responseMessage = " . valoresTexto2($maxiPago2->response["responseMessage"]) . "," .
                                            "avsResponseCode = " . valoresTexto2($maxiPago2->response["avsResponseCode"]) . "," .
                                            "cvvResponseCode = " . valoresTexto2($maxiPago2->response["cvvResponseCode"]) . "," .
                                            "processorCode = " . valoresTexto2($maxiPago2->response["processorCode"]) . "," .
                                            "processorMessage = " . valoresTexto2('SETTLED') . "," .
                                            "errorMessage = " . valoresTexto2($maxiPago2->response["errorMessage"]) . "," .
                                            "f_inicio = 1, f_intervalo = 'monthly', f_recorrencia = 1 " .
                                            "WHERE id_transacao = " . $id;
                                    $result = odbc_exec($con, $query) or die(odbc_errormsg());
                                    $query = "SET DATEFORMAT DMY;UPDATE sf_vendas_itens_dcc SET status_transacao = 'E' " .
                                            "WHERE orderID = " . valoresTexto2($maxiPago2->response["orderID"]);
                                    $result = odbc_exec($con, $query) or die(odbc_errormsg());
                                    if ($maxiPago2->isErrorResponse()) {
                                        echo "Falha de Transação!" . $maxiPago2->getMessage();
                                    } elseif ($maxiPago2->isTransactionResponse()) {
                                        if ($maxiPago2->getResponseCode() == "0") {
                                            echo $id;
                                        } else {
                                            echo "Operação Negada para este cartão!";
                                        }
                                    }
                                } else if ($dcc_empresa2 == 1) {                                   
                                    $result = $charges2->cancelCharge($transactionID, makeCancelRequest($data))->jsonSerialize();
                                    $query = "SET DATEFORMAT DMY;UPDATE sf_vendas_itens_dcc SET responseCode = '0'," .
                                            "authCode = '',orderID = " . valoresTexto2($result["gateway_id"]) . "," .
                                            "transactionID = " . valoresTexto2($result["id"]) . "," .
                                            "transactionTimestamp = getdate()," .
                                            "responseMessage = 'CAPTURED', avsResponseCode = '',cvvResponseCode = ''," .
                                            "processorCode = 'A',processorMessage = " . valoresTexto2('SETTLED') . "," .
                                            "errorMessage = '" . ($result["status"] == "canceled" ? "" : $result["status"]) . "'," .
                                            "f_inicio = 1, f_intervalo = 'monthly', f_recorrencia = 1 " .
                                            "WHERE id_transacao = " . $id; 
                                    odbc_exec($con, $query) or die(odbc_errormsg());
                                    if ($result["status"] == "canceled") {
                                        $query = "SET DATEFORMAT DMY;UPDATE sf_vendas_itens_dcc SET status_transacao = 'E' " .
                                                "WHERE orderID = " . valoresTexto2($result["gateway_id"]);
                                        odbc_exec($con, $query) or die(odbc_errormsg());
                                        echo $id;
                                    } else {
                                        echo "Operação Negada para este cartão! " . $result["status"];
                                    }
                                } else if ($dcc_empresa2 == 2) {
                                    $postfields = array(
                                        'txtAffiliation' => "0",
                                        'txtKey' => $chave2,
                                        'txtEnvironmentType' => ($tipo2 == "LIVE" ? 1 : 0),
                                        'txtOperadora' => $operadora2,
                                        'txtTid' => $orderID,
                                    );
                                    $ch = curl_init();
                                    curl_setopt($ch, CURLOPT_URL, $url);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                                    curl_setopt($ch, CURLOPT_POST, 1);
                                    curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
                                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                                    $res = curl_exec($ch);
                                    $retorno = json_decode($res);
                                    $query = "SET DATEFORMAT DMY;UPDATE sf_vendas_itens_dcc SET responseCode = " . valoresTexto2($retorno->returnCode) . "," .
                                            "authCode = " . valoresTexto2("") . "," .
                                            "orderID = " . valoresTexto2($retorno->tid) . "," .
                                            "transactionID = " . valoresTexto2($retorno->nsu) . "," .
                                            "transactionTimestamp = " . valoresTexto2($retorno->data_retorno) . "," .
                                            "responseMessage = " . valoresTexto2("CAPTURED") . "," .
                                            "avsResponseCode = " . valoresTexto2("") . "," .
                                            "cvvResponseCode = " . valoresTexto2("") . "," .
                                            "processorCode = " . ($retorno->returnCode == "00" ? "'A'" : "'D'") . "," .
                                            "processorMessage = " . ($retorno->returnCode == "00" ? "'SETTLED'" : "'DECLINED'") . "," .
                                            "errorMessage = " . ($retorno->returnCode == "00" ? "''" : valoresTexto2(substr($retorno->message), 0, 50)) . "," .
                                            "f_inicio = 1, f_intervalo = 'monthly', f_recorrencia = 1 " .
                                            "WHERE id_transacao = " . $id;
                                    $result = odbc_exec($con, $query) or die(odbc_errormsg());
                                    $query = "SET DATEFORMAT DMY;UPDATE sf_vendas_itens_dcc SET status_transacao = 'E' " .
                                            "WHERE orderID = " . valoresTexto2($retorno->tid);
                                    $result = odbc_exec($con, $query) or die(odbc_errormsg());
                                    if ($retorno->returnCode == "00") {
                                        echo $id;
                                    } else {
                                        echo "Operação Negada para este cartão!";
                                    }
                                }
                            } catch (Exception $e) {
                                $retorno = 'Erro, ' . $e->getMessage();
                            }
                        }
                    } else {
                        echo "Erro no Processamento!";
                    }
                } else {
                    echo "Verifique os parâmetros deste Cartão!";
                }
            } else {
                echo "Valor inválido para esta operação, o valor deve ser maior que 0 e menor que 10.000,00!";
            }
        } else {
            echo "Código de Mensalidade inválido para esta operação!";
        }
    /*} else {
        echo "Saldo em transações insuficiente para esta operação!";
    }*/
}
