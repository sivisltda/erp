<?php
if (!isset($_POST["isList"]) || $_POST["isList"] != 1) {
    include "../../Connections/configini.php";
}

$id = 0;
$last_tp_oper = "";
$last_crd_dcc = "";
$last_sqt_dcc = "";
$last_sdt_dcc = "";
$dcc_boleto_tipo = 0;
$dcc_pix_tipo = 0;
$dcc_tp_operacao = 0;
$totalSivis = 0;
$totalMaxpago = 0;

$id_aluno = "";
$nome_cartao = "";
$numero_cartao = "";
$dv_cartao = "";
$validade_cartao = "";
$legenda_cartao = "";
$dt_mensalidade = "";
$produto = "";
$msgReturn = "";
$orderID = "";
$transactionID = "";
$valor = "0,00";

$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' .
    ($_SERVER['HTTP_HOST'] === 'localhost' ? $_SERVER['HTTP_HOST'].'/erp'  : $_SERVER['HTTP_HOST']);
$cur = odbc_exec($con, "select * from sf_configuracao where id = 1");
while ($RFP = odbc_fetch_array($cur)) {
    $last_tp_oper = valoresNumericos2($RFP['tp_operacao_dcc']);
    $last_crd_dcc = valoresNumericos2($RFP['last_crd_dcc']);
    $last_sqt_dcc = valoresNumericos2($RFP['last_sqt_dcc']);
    $last_sdt_dcc = $RFP['last_sdt_dcc'];
    $dcc_boleto_tipo = $RFP['dcc_boleto_tipo'];
    $dcc_pix_tipo = $RFP['dcc_pix_tipo'];
}

if (isset($_POST["mkBoleto"]) && in_array($dcc_boleto_tipo, array(1, 2))) {
    $dcc_tp_operacao = 1;
} else if (isset($_POST["mkPix"]) && in_array($dcc_pix_tipo, array(1, 2))) {
    $dcc_tp_operacao = 1;
} else if ($last_sdt_dcc == "" || escreverData($last_sdt_dcc) != getData("T")) {
    include "../../Connections/configSivis.php";
    /*$query = "select top 1 dcc_bloqueio, isnull(dcc_tp_operacao,0) dcc_tp_operacao from ca_contratos A 
    inner join ca_clientes B on A.id_cliente = B.sf_cliente where numero_contrato = " . valoresTexto2($contrato);
    $cur = odbc_exec($con2, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $dcc_tp_operacao = $RFP['dcc_tp_operacao'];
        if ($RFP['dcc_bloqueio'] == 1) {
            $msgReturn = "Erro! Entre em contato com a Sivis!";
            exit();
        }
    }*/
    $query = "select isnull(sum(case when produto = 530 then -1 else 1 end * quantidade),0) total from sf_vendas v 
    inner join sf_vendas_itens vi on v.id_venda = vi.id_venda inner join ca_contratos on cliente_venda = id_cliente
    where cov = 'V' and status = 'Aprovado' and produto in('143','530') and numero_contrato = " . valoresTexto2($contrato);
    $cur = odbc_exec($con2, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $totalSivis = $RFP['total'];
    }   
    $json = file_get_contents($url . "/util/dcc/saldoItemDCC.php?txtCredencial=0&tp_operacao=" . $dcc_tp_operacao . "&hostname=" . $hostname . "&database=" . $database . "&username=" . $username . "&password=" . $password);
    $json = "0";
    if (is_numeric($json)) {
        $totalMaxpago = $totalMaxpago + $json;
    } else {
        $msgReturn = "Erro de conexão com portal! (Credencial 01)!";
        exit();
    }   
    $json = file_get_contents($url . "/util/dcc/saldoItemDCC.php?txtCredencial=1&tp_operacao=" . $dcc_tp_operacao . "&hostname=" . $hostname . "&database=" . $database . "&username=" . $username . "&password=" . $password);    
    if (is_numeric($json)) {
        $totalMaxpago = $totalMaxpago + $json;
    } else {
        $msgReturn = "Erro de conexão com portal! (Credencial 02)!";
        exit();
    }
    /*if (substr($contrato, 0, 1) !== "9") {
        $query = "update ca_clientes set dcc_saldo = " . $totalMaxpago . ", dcc_ult_sinc = getDate() where sf_cliente in (select id_cliente from dbo.ca_contratos where numero_contrato = " . valoresTexto2($contrato) . ")";
        odbc_exec($con2, $query) or die(odbc_errormsg());
    }*/
    $query = "update sf_configuracao set tp_operacao_dcc = " . $dcc_tp_operacao . ", last_crd_dcc = " . $totalSivis . ",last_sqt_dcc = " . $totalMaxpago . ",last_sdt_dcc = getDate() where id = 1";
    odbc_exec($con, $query) or die(odbc_errormsg());
    odbc_close($con2);    
} else {
    $dcc_tp_operacao = $last_tp_oper;
    $totalSivis = $last_crd_dcc;
    $totalMaxpago = $last_sqt_dcc;
    $cur = odbc_exec($con, "set dateformat dmy;select count(id_transacao) total from sf_vendas_itens_dcc where len(orderID) > 0 and transactionTimestamp between '" . date("d/m/Y") . " 00:00:00' and '" . date("d/m/Y") . " 23:59:59'");
    while ($RFP = odbc_fetch_array($cur)) {
        $totalMaxpago = $totalMaxpago + valoresNumericos2($RFP['total']);
    }
}