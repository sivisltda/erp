<?php
$mundi_id = "";
    
if (isset($_POST["btnSalvar"])) {
    $_POST["mkBoleto"] = "S";    
    include "preProcessamentoDCC.php";
    //if ($totalSivis > $totalMaxpago || substr($contrato, 0, 1) == "9" || $dcc_tp_operacao == 1) { //|| $tipo == "TEST" 
        if (is_numeric(valoresSelect("txtPlanoItem"))) {
            $cur = odbc_exec($con, "select mundi_code from sf_boleto where id_referencia = " . valoresSelect("txtPlanoItem"));              
            while ($RFP = odbc_fetch_array($cur)) {
                $mundi_id = $RFP['mundi_code'];
            }
            if (is_numeric($mundi_id)) {
                include "loadClassDcc.php";
                if ($dcc_boleto_tipo == 2 && strlen($config["galaxHash"]) > 0) {
                    try{
                        $charge = new \ODGalaxpay\Entities\Charge();        
                        $result = $galaxpay->charges()->cancel($mundi_id);
                        echo json_encode($result);                        
                    } catch (Exception $ex) {
                        echo $ex;
                    }
                } else {
                    echo "Configuração inválida!";
                }
            } else {
                echo "Cobrança inválida!";
            }
        } else {
            echo "Código de Mensalidade inválido para esta operação!";
        }
    /*} else {
        echo "Saldo em transações insuficiente para esta operação!";
    }*/
}
