<?php

$login_usuario = 'SISTEMA';

if (!(isset($_GET["hostname"]) && isset($_GET["database"]) && isset($_GET["username"]) && isset($_GET["password"]))) {
    echo "Parametros inválidos";
    exit;
}

$hostname = $_GET["hostname"];
$database = $_GET["database"];
$username = $_GET["username"];
$password = $_GET["password"];
$contrato = str_replace("ERP", "", strtoupper($_GET['database']));
$con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
include "./../../Connections/funcoesAux.php";

$res = odbc_exec($con, "select top 1 DCC_RECORRENCIA from sf_configuracao;");
$recorrencia = odbc_result($res, 1);

if (isset($_GET["dtInicio"])) {
    $_POST["dtInicio"] = $_GET["dtInicio"];
} else {
    $_GET["dtInicio"] = getData('T');
}

if (isset($_GET["dtFim"])) {
    $_POST["dtFim"] = $_GET["dtFim"];
}

if (isset($_SESSION["login_usuario"]) && strlen($_SESSION["login_usuario"]) > 0) {
    $login_usuario = strtoupper($_SESSION["login_usuario"]);
} elseif (isset($_GET["login_usuario"]) && strlen($_GET["login_usuario"]) > 0) {
    $login_usuario = strtoupper($_GET["login_usuario"]);
}
$credencial = 0;
$_POST['validaDCC'] = 1;
require_once "loadClassDcc.php";
$dados = [
    'MensagemDCC' => '',
    'qtdSucesso'  => 0,
    'qtdErro'     => 0,
    'TotalDcc'    => 0
];

if ($credencial === 0 && is_numeric($dcc_forma_pagamento) || $credencial == 1 && is_numeric($dcc_forma_pagamento2)) {
    $hook = $client->getHooks();
    $dtInicio   = isset($_GET['dtInicio']) ? $_GET['dtInicio'] : null;
    $dtFim      = isset($_GET['dtFim']) ? $_GET['dtFim'] : null;
    $events     = ['charge.paid', 'charge.overpaid', 'charge.underpaid', 'charge.payment_failed', 'charge.refunded'];
    $hooks = [];
    foreach($events as $event) {
        $hooks = array_merge($hooks, getListHook($hook, 1, 'sent', $event, $dtInicio, $dtFim, $recorrencia));
    }
    
    $filterHooks = array_filter($hooks, function($item) {
        return $item->data->paymentMethod === 'boleto';
    });

    $idCharges= array_map(function($item) {
        return valoresTexto2($item->data->id);
    }, $filterHooks);

    if (count($filterHooks) > 0) {
        $sqlTransacao = "select pm.id_mens, mundi_id from sf_boleto 
        inner join sf_vendas_planos_mensalidade pm on id_referencia = id_mens and tp_referencia = 'M'
        where (id_venda is null or dt_pagamento_mens is null) and inativo = 0 and mundi_id in (".implode(', ', $idCharges).");";
        $res = odbc_exec($con, $sqlTransacao);
        $dados['TotalDcc'] = odbc_num_rows($res);
        $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' .
        ($_SERVER['HTTP_HOST'] === 'localhost' ? $_SERVER['HTTP_HOST'].'/erp'  : $_SERVER['HTTP_HOST']);
        $retry = isset($_GET['retry']) ? intval($_GET['retry']) : 0;
        while($row = odbc_fetch_array($res)) {
            $key = array_search(valoresTexto2($row['mundi_id']), $idCharges);
            $hookTrans = $filterHooks[$key];
            $params = array_merge([
                'Trn'       => $row['mundi_id'],
                'contrato'  => $contrato,
                'valorpago' => $hookTrans->data->lastTransaction->paidAmount,
                'datapagto' => $hookTrans->data->lastTransaction->paidAt->format('d/m/Y'),
                'status'    => $hookTrans->event
            ], $_GET);
           for ($i = 0; $i <= $retry; $i++) {
               $response = baixaBoletoFunc($url, $params);
               if ($response) {
                    $dados['qtdSucesso']++;
                    break;
               } else if ($i === $retry) {
                    $dados['qtdErro']++;
               }
           }
        }
    }
}
echo json_encode($dados);  
exit;  

/**
 * @param string $url
 * @param array $params
 * @return integer|bool 
 */
function baixaBoletoFunc($url, $params) {
    $urlBaixa = $url."/Modulos/Sivis/BaixaBoletoWebhook.php?".http_build_query($params);
    $response = file_get_contents($urlBaixa);
    if (is_numeric($response)) {
       return $response;
    } else {
        return false;
    }
}

/**
 * @param MundiAPILib\Controllers\HooksController $hook
 * @param integer $page
 * @param string $status
 * @param string $event
 * @param string $dtSince
 * @param string $dtUntil
 * @return MundiAPILib\Models\GetHookResponse[]
 */
function getListHook($hook, $page, $status, $event, $dtSince, $dtUntil, $recorrencia) {
    $dtSinceTime = $dtSince ? DateTime::createFromFormat('d/m/Y H:i:s', $dtSince." 00:00:00")->modify("-{$recorrencia} days") : null;
    $dtUntilTime = $dtUntil ? DateTime::createFromFormat('d/m/Y H:i:s', $dtUntil." 00:00:00") : null;
    $hooks = $hook->getHooks($page, 20, $status, $event, $dtSinceTime, $dtUntilTime);
    $data = $hooks->data;
    if ($hooks->paging->next) {
       $data = array_merge($data, getListHook($hook, $page+1, $status, $event, $dtSince, $dtUntil, $recorrencia));
    }
    return $data;
}
