<?php

$login_usuario = 'SISTEMA';
$tpPayment = "1";
$parcelas = 1;

if (isset($_GET["hostname"]) && isset($_GET["database"]) && isset($_GET["username"]) && isset($_GET["password"])) {
    $_POST["hostname"] = $_GET["hostname"];
    $_POST["database"] = $_GET["database"];
    $_POST["username"] = $_GET["username"];
    $_POST["password"] = $_GET["password"];
}

if (isset($_POST["hostname"]) && isset($_POST["database"]) && isset($_POST["username"]) && isset($_POST["password"])) {
    $hostname = $_POST["hostname"];
    $database = $_POST["database"];
    $username = $_POST["username"];
    $password = $_POST["password"];
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
    include "./../../Connections/funcoesAux.php";
}

if (isset($_GET["txtCartao"])) {
    $_POST["txtCartao"] = $_GET["txtCartao"];
}

if (isset($_GET["txtValor"])) {
    $_POST["txtValor"] = $_GET["txtValor"];
}

if (isset($_GET["txtPlanoItem"])) {
    $_POST["txtPlanoItem"] = $_GET["txtPlanoItem"];
}

if (isset($_REQUEST["txtParcelas"]) && is_numeric($_REQUEST["txtParcelas"]) && $_REQUEST["txtParcelas"] > 1) {
    $parcelas = $_REQUEST["txtParcelas"];
}

if (isset($_GET["txtId"]) && isset($_GET["txtCartaoNumero"]) && isset($_GET["txtCartaoBandeira"]) && isset($_GET["txtCartaoNome"]) && 
    isset($_GET["txtCartaoValidade"]) && isset($_GET["txtCartaoCodSeg"]) && isset($_GET["txtCartaoTipo"]) && isset($_GET["txtTpPayment"])) {
    $_POST["txtId"] = $_GET["txtId"];
    $_POST["txtCartaoNumero"] = $_GET["txtCartaoNumero"];
    $_POST["txtCartaoBandeira"] = $_GET["txtCartaoBandeira"];
    $_POST["txtCartaoNome"] = str_replace("_", " ", $_GET["txtCartaoNome"]);
    $_POST["txtCartaoValidade"] = $_GET["txtCartaoValidade"];
    $_POST["txtCartaoCodSeg"] = $_GET["txtCartaoCodSeg"];
    $_POST["txtCartaoTipo"] = $_GET["txtCartaoTipo"];
    $_POST["txtTpPayment"] = $_GET["txtTpPayment"];
    $_POST['txtTipoPag'] = isset($_GET['txtTipoPag']) ? $_GET['txtTipoPag'] : null;
}

if (isset($_SESSION["login_usuario"]) && strlen($_SESSION["login_usuario"]) > 0) {
    $login_usuario = strtoupper($_SESSION["login_usuario"]);
} elseif ((isset($_GET["login_usuario"]) && strlen($_GET["login_usuario"]) > 0) || (isset($_POST["login_usuario"]) && strlen($_POST["login_usuario"]) > 0)) {
    $login_usuario = isset($_GET['login_usuario']) ? strtoupper($_GET["login_usuario"]) : strtoupper($_POST["login_usuario"]);
}

if (valoresNumericos("txtValor") > 0 && valoresNumericos("txtValor") < 10000) {
    
    $produto = 0;
    $dt_mensalidade = getData("T");
    $identificacao = "Avulso";
    
    $cur = odbc_exec($con, "select dt_inicio_mens,id_prod_plano, favorecido from sf_vendas_planos_mensalidade 
    inner join sf_vendas_planos on id_plano_mens = id_plano where id_mens = " . valoresSelect("txtPlanoItem"));
    while ($RFP = odbc_fetch_array($cur)) {
        $produto = $RFP['id_prod_plano'];  
        $dt_mensalidade = date_format(date_create($RFP['dt_inicio_mens']), 'd/m/Y');        
        $identificacao = "Mens." . str_replace(array("Feb", "Apr", "May", "Aug", "Sep", "Oct", "Dec"), array("Fev", "Abr", "Mai", "Ago", "Set", "Out", "Dez"), date_format(date_create($RFP['dt_inicio_mens']), 'M/Y'));
    }
    
    if (isset($_REQUEST["txtCartao"])) {
        $cur = odbc_exec($con, "select * from sf_fornecedores_despesas_cartao where id_cartao = " . valoresSelect("txtCartao"));
        while ($RFP = odbc_fetch_array($cur)) {
            $id_aluno = $RFP['id_fornecedores_despesas'];
            $nome_cartao = utf8_encode($RFP['nome_cartao']);
            $numero_cartao = base64_decode($RFP['num_cartao']);
            $dv_cartao = str_replace(" ", "", $RFP['dv_cartao']);
            $validade_cartao = $RFP['validade_cartao'];
            $legenda_cartao = $RFP['legenda_cartao'];
            $credencial = $RFP['credencial'];
            $bandeira = $RFP['id_bandeira'];
            $tpPayment = "1";
        }
    } else if (isset($_POST["txtCartaoNome"]) && isset($_POST["txtCartaoNumero"]) && isset($_POST["txtCartaoCodSeg"]) && isset($_POST["txtCartaoValidade"]) && isset($_POST["txtCartaoTipo"]) && isset($_POST["txtTpPayment"])) {
        $id_aluno = $_POST["txtId"];
        $nome_cartao = $_POST["txtCartaoNome"];
        $numero_cartao = str_replace(' ', '', $_POST["txtCartaoNumero"]);
        $dv_cartao = $_POST["txtCartaoCodSeg"];
        $validade = explode("/", $_POST["txtCartaoValidade"]);
        $validade_cartao = trim($validade[1]) . "-". trim($validade[0]) . "-01";
        $legenda_cartao = substr($_POST['txtCartaoNumero'], 0, 4) . "..." . substr($_POST['txtCartaoNumero'], -4, 4);
        $credencial = $_POST["txtCartaoTipo"];
        $bandeira = $_POST["txtCartaoBandeira"];
        $tpPayment = $_POST["txtTpPayment"];
        $txtTipoPag = isset($_POST['txtTipoPag']) ? $_POST['txtTipoPag'] : null;        
        if ($_POST["txtSaveCard"] == "1") {
            odbc_exec($con, "insert into sf_fornecedores_despesas_cartao(id_fornecedores_despesas,nome_cartao, num_cartao, legenda_cartao, 
            dv_cartao, validade_cartao, credencial, id_bandeira) values (" . valoresTexto('txtId') . ",UPPER(" . valoresTexto('txtCartaoNome') . "),'" . 
            base64_encode($_POST['txtCartaoNumero']) . "','" . $legenda_cartao . "'," . valoresTexto('txtCartaoCodSeg') . 
            ",'" . $validade_cartao . "'," . $_POST["txtCartaoTipo"] . "," . valoresNumericos("txtCartaoBandeira") . ") ") or die(odbc_errormsg());        
            $res = odbc_exec($con, "select top 1 id_cartao from sf_fornecedores_despesas_cartao order by id_cartao desc") or die(odbc_errormsg());
            $id_card = odbc_result($res, 1);         
            odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
            select 'sf_fornecedores_despesas_cartao', id_cartao, " . valoresTexto2($login_usuario) . ", 'I',
            'INCLUSAO DE CARTAO ' + legenda_cartao + ' (' + replace(dv_cartao, ' ','') + ') - ' + nome_cartao, GETDATE(), id_fornecedores_despesas
            from sf_fornecedores_despesas_cartao where id_cartao = " . $id_card);           
        }        
    }
    
    if (is_numeric($id_aluno) && strlen($numero_cartao) > 0) {
        include "loadClassDcc.php";
        if ($credencial == 0 && is_numeric($dcc_forma_pagamento) || $credencial == 1 && is_numeric($dcc_forma_pagamento2)) {
            $id_tipo_documento = isset($txtTipoPag) && $txtTipoPag ? $txtTipoPag :  ($credencial == 0 ? $dcc_forma_pagamento : $dcc_forma_pagamento2);
            $query = "SET DATEFORMAT DMY;INSERT INTO sf_vendas_itens_dcc(id_plano_item,dcc_nome_cartao,dcc_validade_cartao,dcc_numero_cartao,dcc_dv,f_referencia,valor_transacao,
            id_fornecedores_despesas,id_tipo_documento,dt_modalidade,id_produto,transactionTimestamp,credencial,mkr_login,id_bandeira)
            VALUES (" . valoresSelect("txtPlanoItem") . "," . valoresTexto2($nome_cartao) . ",'" . escreverData($validade_cartao) . "'," . valoresTexto2($legenda_cartao) . "," . valoresTexto2($dv_cartao) . ",'01/01'," .
                valoresNumericos("txtValor") . "," . $id_aluno . "," .$id_tipo_documento. "," . valoresData2($dt_mensalidade) . "," . valoresNumericos2($produto) . ",getdate()," .
                valoresNumericos2($credencial) . "," . valoresTexto2($login_usuario) . "," . valoresNumericos2($bandeira) . ") SELECT SCOPE_IDENTITY() ID;";
            $result = odbc_exec($con, $query) or die(odbc_errormsg());
            odbc_next_result($result);
            $id = odbc_result($result, 1);
            if (is_numeric($id)) {
                if (strlen($nome_cartao) > 0 && is_numeric($dv_cartao) && str_replace("-", "", $validade_cartao) > date("Ymd")) {
                    $data = array(
                        "processorID" => ($credencial == 0 ? $operadora : $operadora2),
                        "referenceNum" => $id,
                        "chargeTotal" => valoresNumericos("txtValor"),
                        "bname" => utf8_encode($nome_cartao),
                        "number" => $numero_cartao,
                        "expMonth" => substr($validade_cartao, 5, 2),
                        "expYear" => substr($validade_cartao, 0, 4),
                        "cvvNumber" => $dv_cartao,
                        "tpPayment" => $tpPayment,
                        "parcelas" => $parcelas
                    );
                    $url = "https://sivis.com.br/erede/util/dcc/Erede/action/makeDcc.php";
                    if ($credencial == 0 && strlen($chave) > 0) {
                        try {
                            if ($dcc_empresa == 0) {
                                $maxiPago->creditCardSale($data);
                                $query = "SET DATEFORMAT DMY;UPDATE sf_vendas_itens_dcc SET responseCode = " . valoresTexto2($maxiPago->response["responseCode"]) . "," .
                                    "authCode = " . valoresTexto2($maxiPago->response["authCode"]) . "," .
                                    "orderID = " . valoresTexto2($maxiPago->response["orderID"]) . "," .
                                    "transactionID = " . valoresTexto2($maxiPago->response["transactionID"]) . "," .
                                    "transactionTimestamp = " . valoresTexto2(date('d/m/Y H:i:s', $maxiPago->response["transactionTimestamp"])) . "," .
                                    "responseMessage = " . valoresTexto2($maxiPago->response["responseMessage"]) . "," .
                                    "avsResponseCode = " . valoresTexto2($maxiPago->response["avsResponseCode"]) . "," .
                                    "cvvResponseCode = " . valoresTexto2($maxiPago->response["cvvResponseCode"]) . "," .
                                    "processorCode = " . valoresTexto2($maxiPago->response["processorCode"]) . "," .
                                    "processorMessage = " . ($maxiPago->response["responseCode"] == "0" ? "'APPROVED'" : valoresTexto2(substr($maxiPago->response["processorMessage"], 0, 50))) . "," .
                                    "status_transacao = " . ($maxiPago->response["responseCode"] == "0" ? "'A'" : "'D'") . "," .
                                    "errorMessage = " . valoresTexto2($maxiPago->response["errorMessage"]) . "," .
                                    "f_inicio = 1, f_intervalo = 'monthly', f_recorrencia = 1 " .
                                    "WHERE id_transacao = " . $id;
                                $result = odbc_exec($con, $query) or die(odbc_errormsg());
                                if ($maxiPago->isErrorResponse()) {
                                    echo "Falha de Transação!" . $maxiPago->getMessage();
                                } elseif ($maxiPago->isTransactionResponse()) {
                                    if ($maxiPago->getResponseCode() == "0") {
                                        echo $id;
                                    } else {
                                        echo "Operação Negada: " . valoresTexto2($maxiPago->response["responseMessage"]);
                                    }
                                }
                            } else if ($dcc_empresa == 1) {
                                $result = $charges->createCharge(makeChargeRequest($data))->jsonSerialize();
                                $last_transaction = $result["last_transaction"]->jsonSerialize();
                                $query = "SET DATEFORMAT DMY;update sf_vendas_itens_dcc set " .
                                    "responseCode = " . ($result["status"] == "paid" ? "'0'" : "'1'") . "," .
                                    "authCode = '" . ($result["status"] == "paid" ? $result["code"] : "") . "',orderID = '" . $result["gateway_id"] . "',transactionID = '" . $result["id"] . "'," .
                                    "transactionTimestamp = getdate(), responseMessage = " . ($result["status"] == "paid" ? "'CAPTURED'" : "'DECLINED'") . ",avsResponseCode = '',cvvResponseCode = ''," .
                                    "processorCode = " . valoresTexto2($last_transaction["acquirer_return_code"]) . "," .
                                    "processorMessage = " . ($result["status"] == "paid" ? "'APPROVED'" : "''") . "," .
                                    "errorMessage = " . ($result["status"] == "paid" ? "''" : valoresTexto2(substr($last_transaction["acquirer_message"], 0, 512))) . "," .
                                    "status_transacao = " . ($result["status"] == "paid" ? "'A'" : "'D'") . "," .
                                    "f_inicio = 1, f_intervalo = 'monthly', f_recorrencia = 1 WHERE id_transacao = " . $id;
                                odbc_exec($con, $query) or die(odbc_errormsg());
                                if ($result["status"] == "paid") {
                                    echo $id;
                                } elseif ($result["status"] == "failed" && $last_transaction["status"] == "not_authorized") {
                                    echo "Operação Negada: " . $last_transaction["acquirer_message"];
                                } elseif ($result["status"] == "failed" && $last_transaction["status"] == "with_error") {
                                    echo 'Erro, Timeout com a adquirente!';
                                } elseif ($result["status"] == "processing") {
                                    echo 'Erro, Processamento com a adquirente!';
                                } else {
                                    echo 'Erro, ' . $result["status"] . "," . $last_transaction["status"];
                                }
                            } else if ($dcc_empresa == 2) {
                                $postfields = array(
                                    'txtAffiliation' => "0",
                                    'txtKey' => $chave,
                                    'txtEnvironmentType' => ($tipo == "LIVE" ? 1 : 0),
                                    'txtOperadora' => $operadora,
                                    'txtTotal' => escreverNumero(valoresNumericos("txtValor")),
                                    'txtPedido' => $id,
                                    'txtNumero' => $numero_cartao,
                                    'txtCodigo' => $dv_cartao,
                                    'txtMesValidade' => substr($validade_cartao, 5, 2),
                                    'txtAnoValidade' => substr($validade_cartao, 0, 4),
                                    'txtNome' => utf8_encode($nome_cartao),
                                    'txtIdentificacao' => $identificacao,
                                    'txtParcelas' => '0');
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                                curl_setopt($ch, CURLOPT_POST, 1);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                                $res = curl_exec($ch);
                                $retorno = json_decode($res);
                                $query = "SET DATEFORMAT DMY;UPDATE sf_vendas_itens_dcc SET responseCode = " . ($retorno->id_retorno == "00" ? "'0'" : "'1'") . "," .
                                    "authCode = " . valoresTexto2($retorno->nautorizacao) . "," .
                                    "orderID = " . valoresTexto2($retorno->tid) . "," .
                                    "transactionID = " . valoresTexto2($retorno->nsu) . "," .
                                    "transactionTimestamp = " . valoresTexto2($retorno->data_retorno) . "," .
                                    "responseMessage = " . ($retorno->id_retorno == "00" ? "'CAPTURED'" : "'DECLINED'") . "," .
                                    "avsResponseCode = '',cvvResponseCode = ''," .
                                    "processorCode = " . ($retorno->id_retorno == "00" ? "'00'" : "'111'") . "," .
                                    "processorMessage = " . ($retorno->id_retorno == "00" ? "'APPROVED'" : "''") . "," .
                                    "status_transacao = " . ($retorno->id_retorno == "00" ? "'A'" : "'D'") . "," .
                                    "errorMessage = " . ($retorno->id_retorno == "00" ? "''" : valoresTexto2(substr($retorno->status, 0, 50))) . "," .
                                    "f_inicio = 1, f_intervalo = 'monthly', f_recorrencia = 1 " .
                                    "WHERE id_transacao = " . $id;
                                $result = odbc_exec($con, $query) or die(odbc_errormsg());
                                if ($retorno->id_retorno == "00") {
                                    echo $id;
                                } else {
                                    echo "Operação Negada: " . $retorno->status;
                                }
                            }
                        } catch (Exception $e) {
                            echo 'Erro, ' . $e->getMessage();
                        }
                    } else if ($credencial == 1 && strlen($chave2) > 0) {
                        try {
                            if ($dcc_empresa2 == 0) {
                                $maxiPago2->creditCardSale($data);
                                $query = "SET DATEFORMAT DMY;UPDATE sf_vendas_itens_dcc SET responseCode = " . valoresTexto2($maxiPago2->response["responseCode"]) . "," .
                                    "authCode = " . valoresTexto2($maxiPago2->response["authCode"]) . "," .
                                    "orderID = " . valoresTexto2($maxiPago2->response["orderID"]) . "," .
                                    "transactionID = " . valoresTexto2($maxiPago2->response["transactionID"]) . "," .
                                    "transactionTimestamp = " . valoresTexto2(date('d/m/Y H:i:s', $maxiPago2->response["transactionTimestamp"])) . "," .
                                    "responseMessage = " . valoresTexto2($maxiPago2->response["responseMessage"]) . "," .
                                    "avsResponseCode = " . valoresTexto2($maxiPago2->response["avsResponseCode"]) . "," .
                                    "cvvResponseCode = " . valoresTexto2($maxiPago2->response["cvvResponseCode"]) . "," .
                                    "processorCode = " . valoresTexto2($maxiPago2->response["processorCode"]) . "," .
                                    "processorMessage = " . ($maxiPago2->response["responseCode"] == "0" ? "'APPROVED'" : valoresTexto2(substr($maxiPago2->response["processorMessage"], 0, 50))) . "," .
                                    "status_transacao = " . ($maxiPago2->response["responseCode"] == "0" ? "'A'" : "'D'") . "," .
                                    "errorMessage = " . valoresTexto2($maxiPago2->response["errorMessage"]) . "," .
                                    "f_inicio = 1, f_intervalo = 'monthly', f_recorrencia = 1 " .
                                    "WHERE id_transacao = " . $id;
                                $result = odbc_exec($con, $query) or die(odbc_errormsg());
                                if ($maxiPago2->isErrorResponse()) {
                                    echo "Falha de Transação!" . $maxiPago2->getMessage();
                                } elseif ($maxiPago2->isTransactionResponse()) {
                                    if ($maxiPago2->getResponseCode() == "0") {
                                        echo $id;
                                    } else {
                                        echo "Operação Negada: " . valoresTexto2($maxiPago2->response["responseMessage"]);
                                    }
                                }
                            } else if ($dcc_empresa2 == 1) {
                                $result = $charges2->createCharge(makeChargeRequest($data))->jsonSerialize();
                                $last_transaction = $result["last_transaction"]->jsonSerialize();
                                $query = "SET DATEFORMAT DMY;update sf_vendas_itens_dcc set " .
                                    "responseCode = " . ($result["status"] == "paid" ? "'0'" : "'1'") . "," .
                                    "authCode = '" . ($result["status"] == "paid" ? $result["code"] : "") . "',orderID = '" . $result["gateway_id"] . "',transactionID = '" . $result["id"] . "'," .
                                    "transactionTimestamp = getdate(),responseMessage = " . ($result["status"] == "paid" ? "'CAPTURED'" : "'DECLINED'") . ",avsResponseCode = '',cvvResponseCode = ''," .
                                    "processorCode = " . valoresTexto2($last_transaction["acquirer_return_code"]) . "," .
                                    "processorMessage = " . ($result["status"] == "paid" ? "'APPROVED'" : "''") . "," .
                                    "errorMessage = " . ($result["status"] == "paid" ? "''" : valoresTexto2(substr($last_transaction["acquirer_message"], 0, 512))) . "," .
                                    "status_transacao = " . ($result["status"] == "paid" ? "'A'" : "'D'") . "," .
                                    "f_inicio = 1, f_intervalo = 'monthly', f_recorrencia = 1 WHERE id_transacao = " . $id;
                                odbc_exec($con, $query) or die(odbc_errormsg());
                                if ($result["status"] == "paid") {
                                    echo $id;
                                } elseif ($result["status"] == "failed" && $last_transaction["status"] == "not_authorized") {
                                    echo "Operação Negada: " . $last_transaction["acquirer_message"];
                                } elseif ($result["status"] == "failed" && $last_transaction["status"] == "with_error") {
                                    echo 'Erro, Timeout com a adquirente!';
                                } elseif ($result["status"] == "processing") {
                                    echo 'Erro, Processamento com a adquirente!';
                                } else {
                                    echo 'Erro, ' . $result["status"] . "," . $last_transaction["status"];
                                }
                            } else if ($dcc_empresa2 == 2) {
                                $postfields = array(
                                    'txtAffiliation' => "0",
                                    'txtKey' => $chave2,
                                    'txtEnvironmentType' => ($tipo2 == "LIVE" ? 1 : 0),
                                    'txtOperadora' => $operadora2,
                                    'txtTotal' => escreverNumero(valoresNumericos("txtValor")),
                                    'txtPedido' => $id,
                                    'txtNumero' => $numero_cartao,
                                    'txtCodigo' => $dv_cartao,
                                    'txtMesValidade' => substr($validade_cartao, 5, 2),
                                    'txtAnoValidade' => substr($validade_cartao, 0, 4),
                                    'txtNome' => utf8_encode($nome_cartao),
                                    'txtIdentificacao' => $identificacao,
                                    'txtParcelas' => '0');
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                                curl_setopt($ch, CURLOPT_POST, 1);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                                $res = curl_exec($ch);
                                $retorno = json_decode($res);
                                $query = "SET DATEFORMAT DMY;UPDATE sf_vendas_itens_dcc SET responseCode = " . ($retorno->id_retorno == "00" ? "'0'" : "'1'") . "," .
                                    "authCode = " . valoresTexto2($retorno->nautorizacao) . "," .
                                    "orderID = " . valoresTexto2($retorno->tid) . "," .
                                    "transactionID = " . valoresTexto2($retorno->nsu) . "," .
                                    "transactionTimestamp = " . valoresTexto2($retorno->data_retorno) . "," .
                                    "responseMessage = " . ($retorno->id_retorno == "00" ? "'CAPTURED'" : "'DECLINED'") . "," .
                                    "avsResponseCode = '',cvvResponseCode = ''," .
                                    "processorCode = " . ($retorno->id_retorno == "00" ? "'00'" : "'111'") . "," .
                                    "processorMessage = " . ($retorno->id_retorno == "00" ? "'APPROVED'" : "''") . "," .
                                    "status_transacao = " . ($retorno->id_retorno == "00" ? "'A'" : "'D'") . "," .
                                    "errorMessage = " . ($retorno->id_retorno == "00" ? "''" : valoresTexto2(substr($retorno->status, 0, 50))) . "," .
                                    "f_inicio = 1, f_intervalo = 'monthly', f_recorrencia = 1 " .
                                    "WHERE id_transacao = " . $id;
                                $result = odbc_exec($con, $query) or die(odbc_errormsg());
                                if ($retorno->id_retorno == "00") {
                                    echo $id;
                                } else {
                                    echo "Operação Negada: " . $retorno->status;
                                }
                            }
                        } catch (Exception $e) {
                            echo 'Erro, ' . $e->getMessage();
                        }
                    } else {
                        echo "Erro de Credenciais!";    
                    }
                } else {
                    $query = "SET DATEFORMAT DMY;UPDATE sf_vendas_itens_dcc SET " .
                        "responseMessage = 'INVALID'," .
                        "processorMessage = 'INVALID'," .
                        "status_transacao = 'I'," .
                        "errorMessage = " . valoresTexto2("Parâmetros inválidos do Cartão!") . " " .
                        "WHERE id_transacao = " . $id;
                    $result = odbc_exec($con, $query) or die(odbc_errormsg());
                    echo "Verifique os parâmetros deste Cartão!";
                }
            } else {
                echo "Erro no Processamento!";
            }
        } else {
            echo "Erro na Forma de Pagamento!";
        }
    } else {
        echo "Erro nos Parâmetros de Pagamento!";
    }
} else {
    echo "Valor inválido para esta operação, o valor deve ser maior que 0 e menor que 10.000,00!";
}