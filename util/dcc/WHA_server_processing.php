<?php

if (!isset($_GET['pdf'])) {
    include '../../Connections/configini.php';
}
$aColumns = array('id', 'id_fornecedores_despesas', 'razao_social', 'telefone', 'mensagem', 'data_envio', 'sys_login', 'status', 'descricao_grupo');
$from = "sf_whatsapp inner join sf_fornecedores_despesas on id_fornecedores_despesas = fornecedor_despesas
left join sf_grupo_cliente on grupo_pessoa = id_grupo";

$iTotal = 0;
$iFilteredTotal = 0;
$sWhere = "id > 0 ";
$colunas = "";
$sOrder = " ORDER BY data_envio desc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0 && intval($_GET['iSortCol_' . $i]) < 9) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY data_envio desc";
    }
}

if (isset($_GET['fil'])) {    
    //$sWhere .= " and empresa = " . $_GET['fil'];
}

if (is_numeric($_GET['tcl']) && $_GET['tcl'] == "1" && is_numeric($_GET['cli'])) {
    $sWhere .= " and id_fornecedores_despesas = " . $_GET['cli'];
}

if ($_GET['dck'] == "1" && is_numeric($_GET['dtp']) && $_GET['din'] != "" && $_GET['dfn'] != "") {
    $sWhere .= " and data_envio between " . valoresDataHoraUnico2($_GET['din'] . " 00:00:00") . " and " . valoresDataHoraUnico2($_GET['dfn'] . " 23:59:59") . "";
}

if (is_numeric($_GET['sts'])) {
    $sWhere .= " and status = " . $_GET['sts'];
}

if (isset($_POST["cancelTrans"])) {
    $sQuery = "set dateformat dmy;
    update sf_whatsapp set status = 2 FROM " . $from . " WHERE " . $sWhere;
    odbc_exec($con, $sQuery) or die(odbc_errormsg());
    echo "YES"; exit;
}

$sQuery = "set dateformat dmy;
SELECT COUNT(*) total FROM " . $from . " WHERE " . $sWhere;
//echo $sQuery; exit;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

for ($i = 0; $i < count($aColumns); $i++) {
    $colunas .= $aColumns[$i] . ",";
}

$sQuery1 = "set dateformat dmy;
SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row," . substr_replace($colunas, "", -1) . "
FROM " . $from . " WHERE " . $sWhere . ") as a 
WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1=" . $imprimir . " " . $sOrder;
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();  
    $row[] = "<center><span class='label-" . ($aRow['status'] == "1" ? "Ativo" : ($aRow['status'] == "2" ? "Inativo" : ($aRow['status'] == "3" ? "Serasa" : ($aRow['status'] == "4" ? "Ferias" : ($aRow['status'] == "16" ? "Cancelado" : "Suspenso"))))) . "' title='" . ($aRow['status'] == "1" ? "ENVIADO" : ($aRow['status'] == "2" ? "CANCELADO" : ($aRow['status'] == "3" ? "INVÁLIDO" : ($aRow['status'] == "4" ? "DUPLICADO" : "NÃO ENVIADOS")))) . "' style='display: inline-block;width: 13px;height: 13px;border-radius: 50%!important;'></span></center>";
    $row[] = escreverDataHora($aRow['data_envio']);
    $row[] = utf8_encode($aRow['id_fornecedores_despesas']);    
    $row[] = "<a href='javascript:void(0)' onClick='AbrirCli(" . utf8_encode($aRow['id_fornecedores_despesas']) . ")'><div id='formPQ' title='" . utf8_encode($aRow['razao_social']) . "'>" . utf8_encode($aRow['razao_social']) . "</div></a>";                
    $row[] = utf8_encode($aRow['descricao_grupo']);    
    $row[] = utf8_encode($aRow['telefone']);    
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow['mensagem']) . "'>" . (strlen(utf8_encode($aRow['mensagem'])) > 120 ? utf8_encode(substr($aRow['mensagem'], 0, 120)) . "..." : utf8_encode($aRow['mensagem'])) . "</div>";
    $row[] = utf8_encode($aRow['sys_login']);   
    $row[] = ($aRow['status'] == "1" ? "ENVIADO" : ($aRow['status'] == "2" ? "CANCELADO" : "NÃO ENVIADO"));   
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
