<?php
include '../../Connections/configini.php';

$id_link = "";
$link_forma_pagamento = "";
$link_max_parcelas = "";

$cur = odbc_exec($con, "select link_forma_pagamento, link_max_parcelas from sf_configuracao where id = 1");
while ($RFP = odbc_fetch_array($cur)) {   
    $link_forma_pagamento = $RFP['link_forma_pagamento'];    
    $link_max_parcelas = $RFP['link_max_parcelas'];    
}

if (isset($_POST['salvaLink'])) {
    $id_link = criarLinkOnline($con, $_POST['txtIdCli'], $link_forma_pagamento, $link_max_parcelas);
    if (is_numeric($id_link)) {
        if (isset($_POST['txtAgendamento'])) {
            criarLinkAgendaItem($con, $id_link, $_POST['txtAgendamento']);
        } else {
            for ($i = 1; $i <= valoresNumericos('txtQtdItens'); $i++) {
                if (isset($_POST['txtIdItem_' . $i])) {
                    criarLinkOnlineItem($con, $id_link, $_POST['txtIdPlano_' . $i], $_POST['txtIdItem_' . $i], $_POST['txtQtd_' . $i], $_POST['txtValor_' . $i], $_POST['txtValFinal_' . $i], $_POST['txtMulta_' . $i]);
                }
            }
        }
        echo processarLinkOnline($con, $id_link);        
    } else {
        echo "Ocorreu um erro ao salvar o Link";
    }
}

if (isset($_POST['cancelarLink'])) {
    if (is_numeric($_POST['txtIdLink'])) {
        echo cancelarLinkOnline($con, $_POST['txtIdLink']);
    } else {
        echo "Ocorreu um erro ao cancelar o Link";
    }
}

if (isset($_POST['excluirLink'])) {    
    if (is_numeric($_POST['txtIdLink'])) {
        echo excluirLinkOnline($con, $_POST['txtIdLink']);
    } else {
        echo "Ocorreu um erro ao excluir o Link";
    }
}

function criarLinkOnline($con, $id_pessoa, $dcc_forma_pagamento, $link_max_parcelas) {
    $query = "set dateformat dmy;
    insert into sf_link_online (id_pessoa, data_criacao, syslogin, tipo_documento, data_parcela, max_parcela, inativo)
    values (" . valoresNumericos2($id_pessoa) . ", getdate(), '" . (isset($_SESSION["login_usuario"]) ? $_SESSION["login_usuario"] : 'SISTEMA') . "',
    " . valoresSelect2($dcc_forma_pagamento) . ", cast(getdate() as date)," . valoresNumericos2($link_max_parcelas) . ",2);
    SELECT SCOPE_IDENTITY() ID;";
    $result = odbc_exec($con, $query) or die(odbc_errormsg());
    odbc_next_result($result);
    $id_link = odbc_result($result, 1);
    return $id_link;
}

function criarLinkOnlineItem($con, $id_link, $id_mens, $id_produto, $quantidade, $valor_total, $valor_bruto, $valor_multa) {
    $query = "insert into sf_link_online_itens (id_link, id_mens, id_produto, quantidade, valor_total, valor_bruto, valor_multa)
    values (" . $id_link . "," . valoresSelect2($id_mens) . "," . valoresSelect2($id_produto) . "," . valoresNumericos2($quantidade) . "," . 
    valoresNumericos2($valor_total) . "," . valoresNumericos2($valor_bruto) . "," . valoresNumericos2($valor_multa) . ");";
    odbc_exec($con, $query) or die(odbc_errormsg());
}

function criarLinkAgendaItem($con, $id_link, $id_agendamento) {
    $query = "insert into sf_link_online_itens (id_link, id_mens, id_produto, quantidade, valor_total, valor_bruto, valor_multa, id_agendamento)
    select " . $id_link . ", null, conta_produto, 1.000, preco_venda, preco_venda, 0.00, id_agendamento from sf_agendamento ag
    inner join sf_agenda a on ag.id_agenda = a.id_agenda inner join sf_produtos p on p.conta_produto = a.id_servico 
    where id_agendamento = " . $id_agendamento;  
    odbc_exec($con, $query) or die(odbc_errormsg());
}

function processarLinkOnline($con, $id_link) {
    $query = "update sf_link_online set valor = (select sum(valor_bruto) from sf_link_online_itens bb where bb.id_link = sf_link_online.id_link),
    data_parcela = (SELECT cast(case when min(dt_inicio_mens) > dateadd(day,2, getdate()) then min(dt_inicio_mens) else dateadd(day,2, getdate()) end as date) bol_vencimento 
    FROM sf_link_online_itens bb inner join sf_vendas_planos_mensalidade mm on bb.id_mens = mm.id_mens where bb.id_link = sf_link_online.id_link), inativo = 0 
    where id_link = " . $id_link;
    odbc_exec($con, $query) or die(odbc_errormsg());    
    $query_log = "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    select 'sf_link_online', id_link, '" . (isset($_SESSION["login_usuario"]) ? $_SESSION["login_usuario"] : 'SISTEMA') . "', 
    'I', 'CADASTRO LINK: ' + cast(id_link as varchar), GETDATE(), id_pessoa from sf_link_online where id_link = " . $id_link;
    odbc_exec($con, $query_log) or die(odbc_errormsg());    
    return $id_link;
}

function cancelarLinkOnline($con, $id_link) {
    $query = "update sf_link_online set inativo = 1 where id_link = " . $id_link;
    odbc_exec($con, $query) or die(odbc_errormsg());    
    $query_log = "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    select 'sf_link_online', id_link, '" . (isset($_SESSION["login_usuario"]) ? $_SESSION["login_usuario"] : 'SISTEMA') . "', 
    'C', 'CANCELAMENTO LINK: ' + cast(id_link as varchar), GETDATE(), id_pessoa from sf_link_online where id_link = " . $id_link;
    odbc_exec($con, $query_log) or die(odbc_errormsg());    
    return $id_link;
}

function excluirLinkOnline($con, $id_link) {    
    $query_log = "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    select 'sf_link_online', id_link, '" . (isset($_SESSION["login_usuario"]) ? $_SESSION["login_usuario"] : 'SISTEMA') . "', 
    'E', 'EXCLUSAO LINK: ' + cast(id_link as varchar), GETDATE(), id_pessoa from sf_link_online where id_link = " . $id_link;
    odbc_exec($con, $query_log) or die(odbc_errormsg());       
    $query = "delete from sf_link_online_itens where id_link = " . $id_link . ";
    delete from sf_link_online where id_link = " . $id_link . ";";
    odbc_exec($con, $query) or die(odbc_errormsg());    
    return $id_link;
}