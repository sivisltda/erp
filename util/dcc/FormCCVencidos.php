<?php include '../../../Connections/configini.php'; ?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="/css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="/css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<link href="/css/bootbox.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="/fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
<script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<body>
    <div class="contCCvenc">
        <div class="frmhead">
            <div class="frmtext">Vencimentos de Cartões</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont">
            <input name="txtId" id="txtId" value="" type="hidden"/>
            <div style="width: 49%;float: left;padding-right: 1%;">
                <span style="display:block;">Início Vencimento:</span>
                <input type="text" style="vertical-align: top;" id="txt_dt_begin" class="datepicker" name="txt_dt_begin" value="<?php echo getData("B"); ?>" placeholder="Data inicial">
            </div>
            <div style="width: 49%;float: left;padding-left: 1%;">
                <span style="display:block;">Fim Vencimento:</span>
                <input type="text" style="vertical-align: top;" id="txt_dt_end" class="datepicker" name="txt_dt_end" value="<?php echo getData("E"); ?>" placeholder="Data Fim">
            </div>
            <div style="clear:both;height: 5px;"></div>
            <div style="width: 100%;float: left;margin: 10px 0;">
                <span>Seleção de Status:</span>
                <select name="itemsStat[]" id="mscStat" multiple="multiple" class="select" style="width:100%" class="input-medium">
                    <option value="Ativo">Ativo</option>
                    <option value="Suspenso">Suspenso</option>
                    <option value="Inativo">Inativo</option>
                </select>
            </div>
            <div style="width: 100%;float: left;margin-bottom: 10px;">
                <span>Tipo:</span>
                <select name="itemsTipo[]" id="multiTipo" multiple="multiple" class="select" style="width:100%" class="input-medium">
                    <?php
                    $query = "select dcc_descricao, dcc_descricao2 from sf_configuracao where id = 1";
                    $cur = odbc_exec($con, $query);
                    while ($RFP = odbc_fetch_array($cur)) {
                        echo "<option value='0'>" . $RFP["dcc_descricao"] . "</option>";
                        echo "<option value='1'>" . $RFP["dcc_descricao2"] . "</option>";
                    }?>
                </select>
            </div>
            <div style="clear:both;height: 5px;"></div>
        </div>
        <div class="frmfoot" style="position: absolute;bottom: 0;width: 96%;">
            <div class="frmbtn">
                <button onClick="imprimir()" class="button button-blue btn-primary" type="submit" name="bntSave" id="bntOK" ><span class="ico-print"></span> Gerar</button>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/js/plugins/jquery/jquery-1.9.1.min.js"></script>    
    <script type="text/javascript" src="/js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
    <script type='text/javascript' src='/js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='/js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='/js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='/js/plugins/other/jquery.mousewheel.min.js'></script>
    <script type='text/javascript' src='/js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='/js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src='/js/plugins/sparklines/jquery.sparkline.min.js'></script>
    <script type='text/javascript' src='/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
    <script type='text/javascript' src='/js/plugins.js'></script>
    <script type='text/javascript' src='/js/charts.js'></script>
    <script type='text/javascript' src='/js/actions.js'></script>
    <script type='text/javascript' src="/js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src='/js/plugins/datatables/jquery.dataTables.min.js'></script>
    <script type='text/javascript' src="/js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='/js/plugins/highlight/jquery.highlight-4.js'></script>
    <script type="text/javascript" src="/js/plugins/jquery.mask.js"></script>    
    <script type="text/javascript" src="/js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../js/CCVencidoForm.js"></script>
    <?php odbc_close($con); ?>
</body>
