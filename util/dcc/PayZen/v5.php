<?php

class commonRequest {

    public $paymentSource; // string
    public $submissionDate; // dateTime
    public $contractNumber; // string
    public $comment; // string

}

class commonResponse {

    public $responseCode; // int
    public $responseCodeDetail; // string
    public $transactionStatusLabel; // string
    public $shopId; // string
    public $paymentSource; // string
    public $submissionDate; // dateTime
    public $contractNumber; // string
    public $paymentToken; // string

}

class queryRequest {

    public $uuid; // string

}

class settlementRequest {

    public $transactionUuids; // string
    public $commission; // long
    public $date; // dateTime

}

class cardRequest {

    public $number; // string
    public $scheme; // string
    public $expiryMonth; // int
    public $expiryYear; // int
    public $cardSecurityCode; // string
    public $cardHolderBirthDay; // dateTime
    public $paymentToken; // string

}

class customerRequest {

    public $billingDetails; // billingDetailsRequest
    public $shippingDetails; // shippingDetailsRequest
    public $extraDetails; // extraDetailsRequest

}

class billingDetailsRequest {

    public $reference; // string
    public $title; // string
    public $type; // custStatus
    public $firstName; // string
    public $lastName; // string
    public $phoneNumber; // string
    public $email; // string
    public $streetNumber; // string
    public $address; // string
    public $district; // string
    public $zipCode; // string
    public $city; // string
    public $state; // string
    public $country; // string
    public $language; // string
    public $cellPhoneNumber; // string
    public $legalName; // string

}

class shippingDetailsRequest {

    public $type; // custStatus
    public $firstName; // string
    public $lastName; // string
    public $phoneNumber; // string
    public $streetNumber; // string
    public $address; // string
    public $address2; // string
    public $district; // string
    public $zipCode; // string
    public $city; // string
    public $state; // string
    public $country; // string
    public $deliveryCompanyName; // string
    public $shippingSpeed; // deliverySpeed
    public $shippingMethod; // deliveryType
    public $legalName; // string

}

class extraDetailsRequest {

    public $ipAddress; // string

}

class paymentRequest {

    public $transactionId; // string
    public $amount; // long
    public $currency; // int
    public $expectedCaptureDate; // dateTime
    public $manualValidation; // int
    public $paymentOptionCode; // string

}

class paymentResponse {

    public $transactionId; // string
    public $amount; // long
    public $currency; // int
    public $effectiveAmount; // long
    public $effectiveCurrency; // int
    public $expectedCaptureDate; // dateTime
    public $manualValidation; // int
    public $operationType; // int
    public $creationDate; // dateTime
    public $externalTransactionId; // string
    public $liabilityShift; // string
    public $transactionUuid; // string

}

class orderResponse {

    public $orderId; // string
    public $extInfo; // extInfo

}

class extInfo {

    public $key; // string
    public $value; // string

}

class cardResponse {

    public $number; // string
    public $scheme; // string
    public $brand; // string
    public $country; // string
    public $productCode; // string
    public $bankCode; // string
    public $expiryMonth; // int
    public $expiryYear; // int

}

class authorizationResponse {

    public $mode; // string
    public $amount; // long
    public $currency; // int
    public $date; // dateTime
    public $number; // string
    public $result; // int

}

class captureResponse {

    public $date; // dateTime
    public $number; // int
    public $reconciliationStatus; // int
    public $refundAmount; // long
    public $refundCurrency; // int

}

class customerResponse {

    public $billingDetails; // billingDetailsResponse
    public $shippingDetails; // shippingDetailsResponse
    public $extraDetails; // extraDetailsResponse

}

class billingDetailsResponse {

    public $reference; // string
    public $title; // string
    public $type; // custStatus
    public $firstName; // string
    public $lastName; // string
    public $phoneNumber; // string
    public $email; // string
    public $streetNumber; // string
    public $address; // string
    public $district; // string
    public $zipCode; // string
    public $city; // string
    public $state; // string
    public $country; // string
    public $language; // string
    public $cellPhoneNumber; // string
    public $legalName; // string

}

class shippingDetailsResponse {

    public $type; // custStatus
    public $firstName; // string
    public $lastName; // string
    public $phoneNumber; // string
    public $streetNumber; // string
    public $address; // string
    public $address2; // string
    public $district; // string
    public $zipCode; // string
    public $city; // string
    public $state; // string
    public $country; // string
    public $deliveryCompanyName; // string
    public $shippingSpeed; // deliverySpeed
    public $shippingMethod; // deliveryType
    public $legalName; // string

}

class extraDetailsResponse {

    public $ipAddress; // string

}

class markResponse {

    public $amount; // long
    public $currency; // int
    public $date; // dateTime
    public $number; // string
    public $result; // int

}

class threeDSResponse {

    public $authenticationRequestData; // authenticationRequestData
    public $authenticationResultData; // authenticationResultData

}

class authenticationRequestData {

    public $threeDSAcctId; // string
    public $threeDSAcsUrl; // string
    public $threeDSBrand; // string
    public $threeDSEncodedPareq; // string
    public $threeDSEnrolled; // string
    public $threeDSRequestId; // string

}

class authenticationResultData {

    public $brand; // string
    public $enrolled; // string
    public $status; // string
    public $eci; // string
    public $xid; // string
    public $cavv; // string
    public $cavvAlgorithm; // string
    public $signValid; // string
    public $transactionCondition; // string

}

class extraResponse {

    public $paymentOptionCode; // string
    public $paymentOptionOccNumber; // int

}

class fraudManagementResponse {

    public $riskControl; // riskControl
    public $riskAnalysis; // riskAnalysis

}

class riskControl {

    public $name; // string
    public $result; // string

}

class riskAnalysis {

    public $score; // string
    public $resultCode; // string
    public $status; // vadRiskAnalysisProcessingStatus
    public $requestId; // string
    public $extraInfo; // extInfo

}

class techRequest {

    public $browserUserAgent; // string
    public $browserAccept; // string

}

class orderRequest {

    public $orderId; // string
    public $extInfo; // extInfo

}

class createPayment {

    public $commonRequest; // commonRequest
    public $threeDSRequest; // threeDSRequest
    public $paymentRequest; // paymentRequest
    public $orderRequest; // orderRequest
    public $cardRequest; // cardRequest
    public $customerRequest; // customerRequest
    public $techRequest; // techRequest

}

class updatePayment {

    public $commonRequest; // commonRequest
    public $paymentRequest; // paymentRequest
    public $queryRequest; // queryRequest

}

class cancelPayment {

    public $commonRequest; // commonRequest
    public $queryRequest; // queryRequest

}

class refundPayment {

    public $commonRequest; // commonRequest
    public $paymentRequest; // paymentRequest
    public $queryRequest; // queryRequest

}

class capturePayment {

    public $settlementRequest; // settlementRequest

}

class getPaymentDetails {

    public $queryRequest; // queryRequest

}

class validatePayment {

    public $queryRequest; // queryRequest

}

class threeDSRequest {

    public $mode; // threeDSMode
    public $requestId; // string
    public $pares; // string
    public $brand; // string
    public $enrolled; // string
    public $status; // string
    public $eci; // string
    public $xid; // string
    public $cavv; // string
    public $algorithm; // string

}

class createPaymentResponse {

    public $createPaymentResult; // createPaymentResult

}

class createPaymentResult {

    public $commonResponse; // commonResponse
    public $paymentResponse; // paymentResponse
    public $orderResponse; // orderResponse
    public $cardResponse; // cardResponse
    public $authorizationResponse; // authorizationResponse
    public $captureResponse; // captureResponse
    public $customerResponse; // customerResponse
    public $markResponse; // markResponse
    public $threeDSResponse; // threeDSResponse
    public $extraResponse; // extraResponse
    public $subscriptionResponse; // subscriptionResponse
    public $fraudManagementResponse; // fraudManagementResponse

}

class updatePaymentResponse {

    public $updatePaymentResult; // createPaymentResult

}

class updatePaymentResult {

    public $commonResponse; // commonResponse
    public $paymentResponse; // paymentResponse
    public $orderResponse; // orderResponse
    public $cardResponse; // cardResponse
    public $authorizationResponse; // authorizationResponse
    public $captureResponse; // captureResponse
    public $customerResponse; // customerResponse
    public $markResponse; // markResponse
    public $threeDSResponse; // threeDSResponse
    public $extraResponse; // extraResponse
    public $subscriptionResponse; // subscriptionResponse
    public $fraudManagementResponse; // fraudManagementResponse

}

class cancelPaymentResponse {

    public $cancelPaymentResponse; // cancelPaymentResponse

}

class cancelPaymentResult {

    public $commonResponse; // commonResponse
    public $paymentResponse; // paymentResponse
    public $orderResponse; // orderResponse
    public $cardResponse; // cardResponse
    public $authorizationResponse; // authorizationResponse
    public $captureResponse; // captureResponse
    public $customerResponse; // customerResponse
    public $markResponse; // markResponse
    public $threeDSResponse; // threeDSResponse
    public $extraResponse; // extraResponse
    public $subscriptionResponse; // subscriptionResponse
    public $fraudManagementResponse; // fraudManagementResponse

}

class refundPaymentResponse {

    public $refundPaymentResponse; // refundPaymentResponse

}

class refundPaymentResult {

    public $commonResponse; // commonResponse
    public $paymentResponse; // paymentResponse
    public $orderResponse; // orderResponse
    public $cardResponse; // cardResponse
    public $authorizationResponse; // authorizationResponse
    public $captureResponse; // captureResponse
    public $customerResponse; // customerResponse
    public $markResponse; // markResponse
    public $threeDSResponse; // threeDSResponse
    public $extraResponse; // extraResponse
    public $subscriptionResponse; // subscriptionResponse
    public $fraudManagementResponse; // fraudManagementResponse

}

class capturePaymentResponse {

    public $capturePaymentResponse; // capturePaymentResponse

}

class capturePaymentResult {

    public $commonResponse; // commonResponse

}

class getPaymentDetailsResponse {

    public $getPaymentDetailsResponse; // refundPaymentResponse

}

class getPaymentDetailsResult {

    public $commonResponse; // commonResponse
    public $paymentResponse; // paymentResponse
    public $orderResponse; // orderResponse
    public $cardResponse; // cardResponse
    public $authorizationResponse; // authorizationResponse
    public $captureResponse; // captureResponse
    public $customerResponse; // customerResponse
    public $markResponse; // markResponse
    public $threeDSResponse; // threeDSResponse
    public $extraResponse; // extraResponse
    public $subscriptionResponse; // subscriptionResponse
    public $fraudManagementResponse; // fraudManagementResponse

}

class validatePaymentResponse {

    public $validatePaymentResponse; // refundPaymentResponse

}

class validatePaymentResult {

    public $commonResponse; // commonResponse

}

?>