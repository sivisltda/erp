<?php

include_once 'v5.php'; // Arquivo contendo a definição dos diferentes objetos
include_once 'function.php'; // Arquivo contendo todas as funções úteis (geração do uuid, etc...)

$shopId = "33015842";
$key = "6820023378838080";
$mode = "TEST";
$wsdl = "https://secure.payzen.com.br/vads-ws/v5?wsdl";

$client = new soapClient($wsdl, $options = array(
    'trace' => 1,
    'exceptions' => 0,
    'encoding' => 'UTF-8',
    'soapaction' => '')
);

$requestId = gen_uuid();
$timestamp = gmdate("Y-m-d\TH:i:s\Z");
$authToken = base64_encode(hash_hmac('sha256', $requestId . $timestamp, $key, true));

setHeaders($shopId, $requestId, $timestamp, $mode, $authToken, $key, $client);
$commonRequest = new commonRequest;
$commonRequest->paymentSource = 'EC';
$commonRequest->submissionDate = new DateTime('now', new DateTimeZone('UTC'));

$threeDSRequest = new threeDSRequest;
$threeDSRequest->mode = "DISABLED";

$paymentRequest = new paymentRequest;
$paymentRequest->amount = "2990";
$paymentRequest->currency = "986";
$paymentRequest->manualValidation = '0';

$orderRequest = new orderRequest;
$orderRequest->orderId = "myOrder";
$cardRequest = new cardRequest;
$cardRequest->number = "4970100000000000";
$cardRequest->scheme = "VISA";
$cardRequest->expiryMonth = "12";
$cardRequest->expiryYear = "2023";
$cardRequest->cardSecurityCode = "123";
$cardRequest->cardHolderBirthDay = "2008-12-31";
$customerRequest = new customerRequest;
$customerRequest->billingDetails = new billingDetailsRequest;
$customerRequest->billingDetails->email = "reinaldo.santos@tiweba.com.br";
$customerRequest->extraDetails = new extraDetailsRequest;
$techRequest = new techRequest;

try {
    $createPaymentRequest = new createPayment;
    $createPaymentRequest->commonRequest = $commonRequest;
    $createPaymentRequest->threeDSRequest = $threeDSRequest;
    $createPaymentRequest->paymentRequest = $paymentRequest;
    $createPaymentRequest->orderRequest = $orderRequest;
    $createPaymentRequest->cardRequest = $cardRequest;
    $createPaymentRequest->customerRequest = $customerRequest;
    $createPaymentRequest->techRequest = $techRequest;
    $createPaymentRequest->commonRequest->submissionDate = $createPaymentRequest->commonRequest->submissionDate->format(dateTime::W3C);
    $createPaymentResponse = new createPaymentResponse();
    $createPaymentResponse = $client->createPayment($createPaymentRequest);
} catch (SoapFault $fault) {
    trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
}
/*
  echo "<hr> [Request Header] <br/>", htmlspecialchars($client->__getLastRequestHeaders()),"<br/>";
  echo "<hr> [Request] <br/>", htmlspecialchars($client->__getLastRequest()), "<br/>";
  echo "<hr> [Response Header]<br/>", htmlspecialchars($client->__getLastResponseHeaders()), "<br/>";
  echo "<hr> [Response]<br/>", htmlspecialchars($client->__getLastResponse()), "<br/>"; */
//Analise da resposta
$dom = new DOMDocument;
$dom->loadXML($client->__getLastResponse(), LIBXML_NOWARNING);
$path = new DOMXPath($dom);
$headers = $path->query('//*[local-name()="Header"]/*');
$responseHeader = array();
foreach ($headers as $headerItem) {
    $responseHeader[$headerItem->nodeName] = $headerItem->nodeValue;
}
$authTokenResponse = base64_encode(hash_hmac('sha256', $responseHeader['timestamp'] . $responseHeader['requestId'], $key, true));
if ($authTokenResponse !== $responseHeader['authToken']) {
    echo 'Erro interno encontrado';
} else {
    if ($createPaymentResponse->createPaymentResult->commonResponse->responseCode != "0") {
    } else {
        if (isset($createPaymentResponse->createPaymentResult->commonResponse->transactionStatusLabel)) {
            switch ($createPaymentResponse->createPaymentResult->commonResponse->transactionStatusLabel) {
                case "AUTHORISED":
                    echo "pagamento aceito";
                    break;
                case "WAITING_AUTHORISATION":
                    echo "pagamento aceito";
                    break;
                case "AUTHORISED_TO_VALIDATE":
                    echo "pagamento aceito";
                    break;
                case "WAITING_AUTHORISATION_TO_VALIDATE":
                    echo "pagamento aceito";
                    break;
                default:
                    echo "pagamento recusado";
                    break;
            }
        } else {
            $cookie = getJsessionId($client);
            $MD = setJsessionId($client) . "+" . $createPaymentResponse->createPaymentResult->threeDSResponse->authenticationRequestData->threeDSRequestId;
            $threeDsAcsUrl = $createPaymentResponse->createPaymentResult->threeDSResponse->authenticationRequestData->threeDSAcsUrl;
            $threeDsEncodedPareq = $createPaymentResponse->createPaymentResult->threeDSResponse->authenticationRequestData->threeDSEncodedPareq;
            $threeDsServerResponseUrl = "http://www.suaoloja.com.br/retour3DS.php";
            $JSESSIONID = setJsessionId($client);
            if ($mode == "TEST") {
                $threeDsAcsUrl = $threeDsAcsUrl . ";jsessionid=" . $JSESSIONID;
            }
            formConstructor($threeDsAcsUrl, $MD, $threeDsEncodedPareq, $threeDsServerResponseUrl);
        }
    }
}
?>
