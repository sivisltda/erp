<?php

$dcc_empresa = "";
$chave = "";
$dcc_forma_pagamento = "";

$dcc_empresa2 = "";
$chave2 = "";
$dcc_forma_pagamento2 = "";

$dcc_boleto_tipo = 0;
$chave3 = "";

$cur = odbc_exec($con, "select DCC_EMPRESA,DCC_CHAVE,DCC_FORMA_PAGAMENTO,DCC_EMPRESA2,DCC_CHAVE2,DCC_FORMA_PAGAMENTO2, 
dcc_boleto_tipo, dcc_pagarme, dcc_galaxid, dcc_galaxpay, dcc_galaxteste from sf_configuracao where id = 1");
while ($RFP = odbc_fetch_array($cur)) {
    $dcc_empresa = $RFP['DCC_EMPRESA'];
    $chave = $RFP['DCC_CHAVE'];
    $dcc_forma_pagamento = $RFP['DCC_FORMA_PAGAMENTO'];

    $dcc_empresa2 = $RFP['DCC_EMPRESA2'];
    $chave2 = $RFP['DCC_CHAVE2'];
    $dcc_forma_pagamento2 = $RFP['DCC_FORMA_PAGAMENTO2'];
    
    $dcc_boleto_tipo = $RFP['dcc_boleto_tipo'];
    $chave3 = $RFP['dcc_pagarme'];
    
    $config = ['galaxId' => $RFP['dcc_galaxid'], 'galaxHash' => $RFP['dcc_galaxpay'], 'sandbox' => ($RFP['dcc_galaxteste'] == 1)];
}

if (!isset($_POST['validaDCC'])) {
    if (isset($_POST["mkBoleto"]) && $dcc_boleto_tipo == 1 && strlen($chave3) > 0) {
    } else if (isset($_POST["mkBoleto"]) && $dcc_boleto_tipo == 2 && strlen($chave3) > 0) {
    } else if ((is_numeric($dcc_empresa) || is_numeric($dcc_empresa2)) && (
            (strlen($chave) > 0 && is_numeric($dcc_forma_pagamento)) ||
            (strlen($chave2) > 0 && is_numeric($dcc_forma_pagamento2)))) {    
    } else {
        echo "Verifique os parâmetros de conexão do DCC!";
        exit();
    }
}

if (isset($_POST["mkBoleto"]) && $dcc_boleto_tipo == 1 && strlen($chave3) > 0) {
    require_once('Pagarme/pagarme.php');
    $pagarme = new PagarMe\Client($chave3);
} elseif (isset($_POST["mkBoleto"]) && $dcc_boleto_tipo == 2 && strlen($config["galaxHash"]) > 0) {
    require_once('Galaxpay/galaxpay.php');
    $auth = new \ODGalaxpay\ODGalaxpayAuth($config);
    $authorization = $auth->authService()->authenticate("charges.write");
    $galaxpay = new \ODGalaxpay\ODGalaxpay($authorization->getAccessToken(), $config['sandbox']);
} elseif ($credencial == 0 && $dcc_empresa == 0 && is_numeric($loja) && strlen($chave) > 0) {
    require_once "MaxPago/lib/maxipago/Autoload.php";
    require_once "MaxPago/lib/maxiPago.php";
    $maxiPago = new maxiPago;
    $maxiPago->setCredentials($loja, $chave);
    $maxiPago->setEnvironment($tipo);
} elseif ($credencial == 0 && $dcc_empresa == 1 && strlen($chave) > 0) {
    require_once "MundiPagg/mundiPagg.php";
    $client = new MundiAPILib\MundiAPIClient($chave, "");
    $charges = $client->getCharges();
} elseif ($credencial == 1 && $dcc_empresa2 == 0 && is_numeric($loja2) && strlen($chave2) > 0) {
    require_once "MaxPago/lib/maxipago/Autoload.php";
    require_once "MaxPago/lib/maxiPago.php";
    $maxiPago2 = new maxiPago;
    $maxiPago2->setCredentials($loja2, $chave2);
    $maxiPago2->setEnvironment($tipo2);
} elseif ($credencial == 1 && $dcc_empresa2 == 1 && strlen($chave2) > 0) {
    require_once "MundiPagg/mundiPagg.php";
    $client2 = new MundiAPILib\MundiAPIClient($chave2, "");
    $charges2 = $client2->getCharges();
}