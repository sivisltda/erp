<?php

if (!isset($_GET['pdf'])) {
    include '../../Connections/configini.php';
}
$aColumns = array('dt_inicio_mens', 'id_fornecedores_despesas', 'razao_social', 
'mundi_gateway_id', 'mundi_id', 'valor_mens', 'dt_pagamento_mens', 'id_boleto', 'exp_email', 'id_mens',
'cnpj', 'endereco', 'numero', 'bairro', 'estado', 'cidade', 'cep');
$from = "sf_vendas_planos_mensalidade 
inner join sf_vendas_planos on id_plano = id_plano_mens
inner join sf_fornecedores_despesas on id_fornecedores_despesas = favorecido
inner join sf_produtos_parcelas on id_parc_prod_mens = id_parcela
left join sf_boleto on id_mens = id_referencia and tp_referencia = 'M'";
$iTotal = 0;
$iFilteredTotal = 0;
$sWhere = " parcela = -2 and sf_fornecedores_despesas.tipo = 'C' and sf_fornecedores_despesas.inativo = 0 ";
$colunas = "";
$sOrder = " ORDER BY dt_inicio_mens desc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0 && intval($_GET['iSortCol_' . $i]) < 9) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY dt_inicio_mens desc";
    }
}

if (isset($_GET['fil'])) {
    $sWhere .= " and sf_fornecedores_despesas.empresa = " . $_GET['fil'];
}

if (is_numeric($_GET['tcl']) && $_GET['tcl'] == "1" && is_numeric($_GET['cli'])) {
    $sWhere .= " and sf_fornecedores_despesas.id_fornecedores_despesas = " . $_GET['cli'];
}

if ($_GET['dck'] == "1" && is_numeric($_GET['dtp']) && $_GET['din'] != "" && $_GET['dfn'] != "") {
    $sWhere .= " and " . ($_GET['dtp'] == 1 ? "cast(dt_pagamento_mens as date)" : "dt_inicio_mens") . " between " . valoresData2($_GET['din']) . " and " . valoresData2($_GET['dfn']) . "";
}

if (is_numeric($_GET['sts'])) {
    switch ($_GET['sts']) {
        case 1: //GERADOS
            $sWhere .= " and sf_boleto.inativo = 0 ";
            break;
        case 2: //NÃO GERADOS
            $sWhere .= " and sf_boleto.inativo is null ";
            break;
        case 3: //ENVIADOS
            $sWhere .= " and sf_boleto.inativo = 0 and exp_email = 1 ";
            break;
        case 4: //NÃO ENVIADOS
            $sWhere .= " and sf_boleto.inativo = 0 and exp_email = 0 ";
            break;
        case 5: //CANCELADOS
            $sWhere .= " and sf_boleto.inativo = 1 ";
            break;
    }
}

$sQuery = "set dateformat dmy;
SELECT COUNT(*) total FROM " . $from . " WHERE " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

for ($i = 0; $i < count($aColumns); $i++) {
    $colunas .= $aColumns[$i] . ",";
}

$sQuery1 = "set dateformat dmy;
SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row," . substr_replace($colunas, "", -1) . ",
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'email'
FROM " . $from . " WHERE " . $sWhere . ") as a 
WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1=" . $imprimir . " " . $sOrder;
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();  
    $boletoLink = "";
    $invalidb = (!is_numeric($aRow[$aColumns[7]]) && ($aRow[$aColumns[10]] == "" || $aRow[$aColumns[11]] == "" || $aRow[$aColumns[12]] == "" || $aRow[$aColumns[13]] == "" || 
    $aRow[$aColumns[14]] == "" || $aRow[$aColumns[15]] == "" || $aRow[$aColumns[16]] == "") ? "<span style='color:red;';>" : "");
    $invalide = (!is_numeric($aRow[$aColumns[7]]) && ($aRow[$aColumns[10]] == "" || $aRow[$aColumns[11]] == "" || $aRow[$aColumns[12]] == "" || $aRow[$aColumns[13]] == "" || 
    $aRow[$aColumns[14]] == "" || $aRow[$aColumns[15]] == "" || $aRow[$aColumns[16]] == "") ? "</span>" : "");    
    $row[] = "<center>" . $invalidb . (!is_numeric($aRow[$aColumns[7]]) ? "<input name=\"nEnviadoB[]\" type=\"hidden\" value=\"" . $aRow[$aColumns[9]] . "\"/>" : "") . escreverData($aRow[$aColumns[0]]) . $invalide . "</center>";
    $row[] = "<center>" . $invalidb . utf8_encode($aRow[$aColumns[1]]) . $invalide . "</center>";
    $row[] = "<a href='javascript:void(0)' onClick='AbrirCli(" . utf8_encode($aRow[$aColumns[1]]) . ")'><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[2]]) . "'>" . $invalidb . utf8_encode($aRow[$aColumns[2]]) . $invalide . "</div></a>";        
    $row[] = "<center>" . $invalidb . utf8_encode($aRow[$aColumns[3]]) . $invalide . "</center>";
    $row[] = "<center>" . $invalidb . utf8_encode($aRow[$aColumns[4]]) . $invalide . "</center>";
    $row[] = "<center>" . $invalidb . escreverNumero($aRow[$aColumns[5]]) . $invalide . "</center>";    
    $row[] = "<center>" . $invalidb . escreverData($aRow[$aColumns[6]]) . $invalide . "</center>";
    if (is_numeric($aRow[$aColumns[7]])) {
        $boletoLink = "<a style=\"margin-right: 6px;\" href=\"./../../Boletos/boleto_pix.php?id=M-" . $aRow[$aColumns[9]] . "&amp;crt=" . $_SESSION["contrato"] . "\" target=\"_blank\"><img src=\"../../img/pix.png\"></a>";
    }
    $row[] = "<center>" . $boletoLink . "</center>";
    $row[] = "<center>" . ($aRow[$aColumns[8]] > 0 ? "<img src=\"../../img/1368741588_check.png\" style=\"height:16px;\" alt=\"\">" : "") . "</center>";
    if ($imprimir > 0) {
        $row[] = $aRow["email"];    
        $row[] = $aRow["id_fornecedores_despesas"];    
        $row[] = $aRow["id_mens"];    
    }
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
