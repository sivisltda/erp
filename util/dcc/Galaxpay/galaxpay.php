<?php

require('vendor/autoload.php');

function ValoresNumericosMP($val) {
    $und = "00";
    $number = explode('.', $val);
    if (count($number) > 1) {
        $val = $number[0];
        $und = $number[1] . "00";
    }
    return $val . substr($und, 0, 2);
}

function valorNumeroPuro($val) {
    return preg_replace('/[^0-9]/', '', $val);
}

function makeChargeRequestBoleto($data, $identificacao) {
    $charge = new \ODGalaxpay\Entities\Charge();
    $charge->setMyId($data['id_mens'])
            ->setValue(ValoresNumericosMP(($data["tipo_desconto"] == "2" ? $data['valor_real'] : $data['valor'])))
            ->setAdditionalInfo(substr($identificacao, 0, 100))
            ->setPayday(escreverData($data['data_vencimento'], 'Y-m-d'))
            ->setPayedOutsideGalaxPay(false)
            ->setMainPaymentMethodId(\ODGalaxpay\Entities\Charge::PAYMENT_TYPE_BOLETO);
    //--------------------------------------------------------------------------
    $address = new \ODGalaxpay\Entities\Address();
    $address->setStreet($data['endereco'])
            ->setNumber($data['numero'])
            ->setNeighborhood($data['bairro'])
            ->setCity($data['cidade'])
            ->setState($data['estado'])
            ->setComplement($data['complemento'])
            ->setZipCode(valorNumeroPuro($data['cep']));
    $charge->getCustomer()
            ->setMyId($data['id_aluno'])
            ->setName($data['nome'])
            ->setDocument(valorNumeroPuro($data['cnpj']))
            ->setEmails([$data['email']])
            ->setPhones([valorNumeroPuro($data['celular'])])
            ->setAddress($address);
    //--------------------------------------------------------------------------    
    if ($data["tipo_desconto"] == "1" && $data["valor_desconto"] > 0) {
        $charge->getPaymentMethodBoleto()
                ->setFine(ValoresNumericosMP($data['taxa_multa']))
                ->setInterest(ValoresNumericosMP($data['mora_multa']))
                ->setInstructions(substr($identificacao . ", " . $data['informativo'], 0, 100) .
                        ($data['taxa_multa'] > 0 ? " Juros " . $data['taxa_multa'] : "") .
                        ($data['mora_multa'] > 0 ? " Multas de " . $data['mora_multa'] . "% ao dia" : ""))
                ->setDeadlineDays($data['dias_limite'])->getDiscount()
                ->setQtdDaysBeforePayDay($data['dias_desconto'])->setType("percent")->setValue(ValoresNumericosMP($data['valor_desconto']));
    } else if ($data["tipo_desconto"] == "2" && ($data['valor_real'] - $data['valor']) > 0) {
        $charge->getPaymentMethodBoleto()
                ->setFine(ValoresNumericosMP($data['taxa_multa']))
                ->setInterest(ValoresNumericosMP($data['mora_multa']))
                ->setInstructions(substr($identificacao . ", " . $data['informativo'], 0, 100) .
                        ($data['taxa_multa'] > 0 ? " Juros " . $data['taxa_multa'] : "") .
                        ($data['mora_multa'] > 0 ? " Multas de " . $data['mora_multa'] . "% ao dia" : ""))
                ->setDeadlineDays($data['dias_limite'])->getDiscount()
                ->setQtdDaysBeforePayDay("00")->setType("fixed")->setValue(ValoresNumericosMP(($data['valor_real'] - $data['valor'])));
    } else {
        $charge->getPaymentMethodBoleto()
                ->setFine(ValoresNumericosMP($data['taxa_multa']))
                ->setInterest(ValoresNumericosMP($data['mora_multa']))
                ->setInstructions(substr($identificacao . ", " . $data['informativo'], 0, 100) .
                        ($data['taxa_multa'] > 0 ? " Juros " . $data['taxa_multa'] : "") .
                        ($data['mora_multa'] > 0 ? " Multas de " . $data['mora_multa'] . "% ao dia" : ""))
                ->setDeadlineDays($data['dias_limite']);
    }
    return $charge;
}

function makeChargeRequestPix($data, $identificacao) {
    $charge = new \ODGalaxpay\Entities\Charge();
    $charge->setMyId($data['id_mens'])
            ->setValue(ValoresNumericosMP(($data["tipo_desconto"] == "2" ? $data['valor_real'] : $data['valor'])))
            ->setAdditionalInfo($identificacao)
            ->setPayday(escreverData($data['data_inicio'], 'Y-m-d'))
            ->setPayedOutsideGalaxPay(false)
            ->setMainPaymentMethodId(\ODGalaxpay\Entities\Charge::PAYMENT_TYPE_PIX);
    //--------------------------------------------------------------------------
    $address = new \ODGalaxpay\Entities\Address();
    $address->setStreet($data['endereco'])
            ->setNumber($data['numero'])
            ->setNeighborhood($data['bairro'])
            ->setCity($data['cidade'])
            ->setState($data['estado'])
            ->setComplement($data['complemento'])
            ->setZipCode(valorNumeroPuro($data['cep']));
    $charge->getCustomer()
            ->setMyId($data['id_aluno'])
            ->setName($data['nome'])
            ->setDocument(valorNumeroPuro($data['cnpj']))
            ->setEmails([$data['email']])
            ->setPhones([valorNumeroPuro($data['celular'])])
            ->setAddress($address);
    //--------------------------------------------------------------------------   
    $deadlineDays = new \ODGalaxpay\Entities\Deadline();
    $deadlineDays->setType("days")->setValue($data['dias_limite']);
    //--------------------------------------------------------------------------   
    if ($data["tipo_desconto"] == "1" && $data["valor_desconto"] > 0) {
        $charge->getPaymentMethodPix()
                ->setFine(ValoresNumericosMP($data['taxa_multa']))
                ->setInterest(ValoresNumericosMP($data['mora_multa']))
                ->setInstructions($identificacao . ", " . $data['informativo'] .
                        ($data['taxa_multa'] > 0 ? " Juros " . $data['taxa_multa'] : "") .
                        ($data['mora_multa'] > 0 ? " Multas de " . $data['mora_multa'] . "% ao dia" : ""))
                ->setDeadlineDays($deadlineDays)->getDiscount()
                ->setQtdDaysBeforePayDay($data['dias_desconto'])->setType("percent")->setValue(ValoresNumericosMP($data['valor_desconto']));
    } else if ($data["tipo_desconto"] == "2" && ($data['valor_real'] - $data['valor']) > 0) {
        $charge->getPaymentMethodPix()
                ->setFine(ValoresNumericosMP($data['taxa_multa']))
                ->setInterest(ValoresNumericosMP($data['mora_multa']))
                ->setInstructions($identificacao . ", " . $data['informativo'] .
                        ($data['taxa_multa'] > 0 ? " Juros " . $data['taxa_multa'] : "") .
                        ($data['mora_multa'] > 0 ? " Multas de " . $data['mora_multa'] . "% ao dia" : ""))
                ->setDeadlineDays($deadlineDays)->getDiscount()
                ->setQtdDaysBeforePayDay("00")->setType("fixed")->setValue(ValoresNumericosMP(($data['valor_real'] - $data['valor'])));
    } else {
        $charge->getPaymentMethodPix()
                ->setFine(ValoresNumericosMP($data['taxa_multa']))
                ->setInterest(ValoresNumericosMP($data['mora_multa']))
                ->setInstructions($identificacao . ", " . $data['informativo'] .
                        ($data['taxa_multa'] > 0 ? " Juros " . $data['taxa_multa'] : "") .
                        ($data['mora_multa'] > 0 ? " Multas de " . $data['mora_multa'] . "% ao dia" : ""))
                ->setDeadlineDays($deadlineDays);
    }
    return $charge;
}
