<?php

namespace ODGalaxpay\Request;

use GuzzleHttp\Client as GuzzleClient;

class Base {
    
    const PRODUCTION_ENDPOINT = "https://api-celcash.celcoin.com.br/v2/";
    const SANDBOX_ENDPOINT = "https://api.sandbox.cel.cash/v2/";

    protected $client;

    public function __construct() {
        
    }

    public function __call($method, $args) {
        if (count($args) < 1) {
            throw new \InvalidArgumentException(
            'Magic request methods require a URI and optional options array'
            );
        }
        $uri = $args[0];
        $options = $args[1] ?? [];
        return $this->request($method, $uri, $options);
    }

    public function request($method, $uri, $options) {
        $method = strtoupper($method);
        $response = $this->client->request($method, $uri, $options);
        return $response->getBody();
    }

}
