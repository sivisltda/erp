<?php

namespace ODGalaxpay\Request;

use GuzzleHttp\Client as GuzzleClient;

class Client extends Base {

    const PRODUCTION_ENDPOINT = "https://api-celcash.celcoin.com.br/v2/";
    const SANDBOX_ENDPOINT = "https://api.sandbox.cel.cash/v2/";

    public function __construct($accessToken, $sandbox) {
        $header = [
            'Authorization' => sprintf('Bearer %s', $accessToken),
            'Accept' => 'application/json'
        ];
        $this->client = new GuzzleClient([
            'base_uri' => $sandbox ? self::SANDBOX_ENDPOINT : self::PRODUCTION_ENDPOINT,
            'headers' => $header
        ]);
    }

}
