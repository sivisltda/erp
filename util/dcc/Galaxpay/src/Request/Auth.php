<?php

namespace ODGalaxpay\Request;

use GuzzleHttp\Client as GuzzleClient;

class Auth extends Base {

    const PRODUCTION_ENDPOINT = "https://api-celcash.celcoin.com.br/v2/";
    const SANDBOX_ENDPOINT = "https://api.sandbox.cel.cash/v2/";

    public function __construct($base64EncodedCredentials, $base64EncodedPartner, $sandbox) {
        $this->client = new GuzzleClient([
            'base_uri' => $sandbox ? self::SANDBOX_ENDPOINT : self::PRODUCTION_ENDPOINT,
            'headers' => [
                'Authorization' => sprintf('Basic %s', $base64EncodedCredentials),
                'AuthorizationPartner' => sprintf('%s', $base64EncodedPartner),
                'Accept' => 'application/json'
            ]
        ]);
    }        

}
