<?php

namespace ODGalaxpay\Response;

use ODGalaxpay\Interfaces\ODGalaxpayInterface;

class Response implements ODGalaxpayInterface {

    /**
     * @param $json
     *
     * @return self
     */
    public static function fromJson($json) {
        $object = json_decode($json);
        $self = new self();
        $self->populate($object);

        foreach ($self as $k => $v) {
            if (empty($v)) {
                unset($self->$k);
            }
        }

        return $self;
    }

    /**
     * @param \stdClass $data
     */
    public function populate(\stdClass $data) {
        $dataProps = get_object_vars($data);
        if (!empty($dataProps)) {
            foreach ($dataProps as $k => $v) {
                $this->$k = $v;
            }
        }
    }

}
