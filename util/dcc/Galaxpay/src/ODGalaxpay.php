<?php

namespace ODGalaxpay;

use ODGalaxpay\Exception\ODGalaxpayException;
use ODGalaxpay\Request\Client;
use ODGalaxpay\Services\ChargeService;

class ODGalaxpay {

    protected $client;

    public function __construct($accessToken, $sandbox) {
        if (empty($accessToken)) {
            throw new ODGalaxpayException('Provide an access token');
        }
        $this->client = new Client($accessToken, $sandbox);
    }

    public function charges(): ChargeService {
        return new ChargeService($this->client);
    }

}
