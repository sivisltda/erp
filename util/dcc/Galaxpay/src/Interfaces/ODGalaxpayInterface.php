<?php

namespace ODGalaxpay\Interfaces;

interface ODGalaxpayInterface {

    /**
     * @param $json
     *
     * @return \stdClass
     */
    public static function fromJson($json);

    /**
     * @param \stdClass $data
     */
    public function populate(\stdClass $data);
}
