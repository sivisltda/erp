<?php

namespace ODGalaxpay\Services;

use ODGalaxpay\Request\Base;
use ODGalaxpay\Response\Response;

class BaseService {

    /**
     * @var Base
     */
    protected $client;

    /**
     * @var Response
     */
    protected $response;

    public function __construct(Base $client, Response $response = null) {
        $this->client = $client;
        $this->response = empty($response) ? new Response() : $response;
    }

}
