<?php

namespace ODGalaxpay\Services;

use ODGalaxpay\Response\Response;

class ChargeService extends BaseService {

    public function create($params): Response {
        //echo json_encode($params);exit;
        $response = $this->client->post('charges', [
            'json' => $params
        ]);
        return $this->response->fromJson($response);
    }
        
    public function cancel($id): Response {
        $response = $this->client->delete("charges/{$id}/galaxPayId");
        return $this->response->fromJson($response);
    }  
        
    public function print($params): Response {
        $response = $this->client->post('carnes/onePDFTransaction/none', [
            'json' => [
                "galaxPayIds" => $params
            ]
        ]);
        return $this->response->fromJson($response);
    }  
    
}
