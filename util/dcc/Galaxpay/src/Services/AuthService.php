<?php

namespace ODGalaxpay\Services;

use ODGalaxpay\Response\AuthResponse;
use ODGalaxpay\Response\Response;

class AuthService extends BaseService {

    public function authenticate($scope): Response {

        $authResponse = new AuthResponse();
        $response = $this->client->post('token', [
            'json' => [
                'grant_type' => 'authorization_code',
                'scope' => $scope
            ]
        ]);
        return $authResponse->fromJson($response);
    }

}
