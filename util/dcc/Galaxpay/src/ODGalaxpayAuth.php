<?php

namespace ODGalaxpay;

use ODGalaxpay\Exception\ODGalaxpayException;
use ODGalaxpay\Request\Auth;
use ODGalaxpay\Services\AuthService;

class ODGalaxpayAuth {

    protected $client;

    public function __construct($config) {
        $this->client = new Auth($this->getBase64EncodedCredentials($config), $this->getBase64EncodedPartner($config), $config['sandbox']);
    }

    public function authService(): AuthService {
        return new AuthService($this->client);
    }

    private function getBase64EncodedCredentials($config) {
        if (!isset($config['galaxId']) || empty($config['galaxId'])) {
            throw new ODGalaxpayException('Provide your galax Id');
        }
        if (!isset($config['galaxHash']) || empty($config['galaxHash'])) {
            throw new ODGalaxpayException('Provide your galaxHash');
        }
        return base64_encode(sprintf('%s:%s', $config['galaxId'], $config['galaxHash']));
    }
    
    private function getBase64EncodedPartner($config) {
        if ($config['sandbox']) {
            return base64_encode(sprintf('%s:%s', "5473", "83Mw5u8988Qj6fZqS4Z8K7LzOo1j28S706R0BeFe"));
        } else {
            return base64_encode(sprintf('%s:%s', "49916", "2u68BbA6Gi4tPqM5ZpYyNsJ46oQiS14aQ6R1D4Lr"));
        }
    }

}

//49916 - 2u68BbA6Gi4tPqM5ZpYyNsJ46oQiS14aQ6R1D4Lr
//25759 - EnTcAu0hOcWxIrQpY3ImKaF7Qs9oR57r926bCdHr