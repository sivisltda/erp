<?php

namespace ODGalaxpay\Entities;

class Deadline extends BaseEntity {

    /**
     * @var string $type
     */
    protected $type;

    /**
     * @var int $value
     */
    protected $value;

    /**
     * Get the value of type
     *
     * @return  string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function getValue() {
        return $this->value;
    }    
    
    /**
     * Set $type
     *
     * @param  string  $type  $type
     *
     * @return  self
     */
    public function setType(string $type) {
        $this->type = $type;

        return $this;
    }
    
    /**
     * Set $value
     *
     * @param  int  $value  $value
     *
     * @return  self
     */
    public function setValue(int $value) {
        $this->value = $value;

        return $this;
    }

}
