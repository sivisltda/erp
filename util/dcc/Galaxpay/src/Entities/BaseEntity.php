<?php

namespace ODGalaxpay\Entities;

use ODGalaxpay\Interfaces\ODGalaxpaySerializable;

class BaseEntity implements ODGalaxpaySerializable {

    public function jsonSerialize() {
        $arr = get_object_vars($this);
        foreach ($arr as $k => $v) {
            if (!is_bool($v) && $v !== 0 && empty($v)) {
                unset($arr[$k]);
            }
        }
        return $arr;
    }

}
