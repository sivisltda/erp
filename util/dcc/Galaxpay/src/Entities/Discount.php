<?php

namespace ODGalaxpay\Entities;

class Discount extends BaseEntity {
    
    /**
     * @var int $qtdDaysBeforePayDay
     */
    protected $qtdDaysBeforePayDay;

    /**
     * @var string $type
     */
    protected $type;
    
    /**
     * @var int $value
     */
    protected $value;
    
    /**
     * Get $qtdDaysBeforePayDay
     *
     * @return  int
     */
    public function getQtdDaysBeforePayDay() {
        return $this->qtdDaysBeforePayDay;
    }

    /**
     * Get $type
     *
     * @return  string
     */
    public function getType() {
        return $this->type;
    }
    
    /**
     * Get $value
     *
     * @return  int
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * Set $qtdDaysBeforePayDay
     *
     * @param  int  $qtdDaysBeforePayDay  $qtdDaysBeforePayDay
     *
     * @return  self
     */
    public function setQtdDaysBeforePayDay(string $qtdDaysBeforePayDay) {
        $this->qtdDaysBeforePayDay = $qtdDaysBeforePayDay;

        return $this;
    }

    /**
     * Set $type
     *
     * @param  string  $type  $type
     *
     * @return  self
     */
    public function setType(string $type) {
        $this->type = $type;

        return $this;
    }
    
    /**
     * Set $value
     *
     * @param  int  $value  $value
     *
     * @return  self
     */
    public function setValue(string $value) {
        $this->value = $value;

        return $this;
    }    
}