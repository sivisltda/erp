<?php

namespace ODGalaxpay\Entities;

class Customer extends BaseEntity {

    /**
     * @var string $myId
     */
    protected $myId;

    /**
     * @var string $name
     */
    protected $name;

    /**
     * @var string $document
     */
    protected $document;

    /**
     * @var array $emails
     */
    protected $emails;

    /**
     * @var array $phones
     */
    protected $phones;

    /**
     * @var Address $address
     */
    protected $address;

    /**
     * Get $myId
     *
     * @return  string
     */
    public function getMyId() {
        return $this->myId;
    }

    /**
     * Get $name
     *
     * @return  string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Get $document
     *
     * @return  string
     */
    public function getDocument() {
        return $this->document;
    }

    /**
     * Get $emails
     *
     * @return  array
     */
    public function getEmails() {
        return $this->emails;
    }

    /**
     * Get $phones
     *
     * @return  array
     */
    public function getPhones() {
        return $this->phones;
    }

    /**
     * Get $address
     *
     * @return  Address
     */
    public function getAddress() {
        return empty($this->address) ? new Address() : $this->address;
    }

    /**
     * Set $name
     *
     * @param  string  $myId  $myId
     *
     * @return  self
     */
    public function setMyId(string $myId) {
        $this->myId = $myId;

        return $this;
    }

    /**
     * Set $name
     *
     * @param  string  $name  $name
     *
     * @return  self
     */
    public function setName(string $name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Set $document
     *
     * @param  string  $document  $document
     *
     * @return  self
     */
    public function setDocument(string $document) {
        $this->document = $document;

        return $this;
    }

    /**
     * Set $emails
     *
     * @param  array  $emails  $emails
     *
     * @return  self
     */
    public function setEmails(array $emails) {
        $this->emails = $emails;

        return $this;
    }

    /**
     * Set $phones
     *
     * @param  array  $phones  $phones
     *
     * @return  self
     */
    public function setPhones(array $phones) {
        $this->phones = $phones;

        return $this;
    }

    /**
     * Set $address
     *
     * @param  Address  $address  $address
     *
     * @return  self
     */
    public function setAddress(Address $address) {
        $this->Address = $address;

        return $this;
    }

}
