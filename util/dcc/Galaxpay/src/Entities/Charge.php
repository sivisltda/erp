<?php

namespace ODGalaxpay\Entities;

class Charge extends BaseEntity {

    /**
     * @var string $myId
     */
    protected $myId;

    /**
     * @var int $value
     */
    protected $value;

    /**
     * @var string $additionalInfo
     */
    protected $additionalInfo;

    /**
     * @var string $payday
     */
    protected $payday;

    /**
     * @var bool $payedOutsideGalaxPay
     */
    protected $payedOutsideGalaxPay;

    /**
     * @var string $mainPaymentMethodId
     */
    protected $mainPaymentMethodId;

    /**
     * @var Customer $customer
     */
    protected $customer;

    /**
     * @var PaymentMethodBoleto $paymentMethodBoleto
     */
    protected $paymentMethodBoleto;

    /**
     * @var PaymentMethodPix $paymentMethodPix
     */
    protected $paymentMethodPix;

    const PAYMENT_TYPE_BOLETO = 'boleto';
    const PAYMENT_TYPE_PIX = 'pix';

    /**
     * Get $myId
     *
     * @return  string
     */
    public function getMyId() {
        return $this->myId;
    }

    /**
     * Get $value
     *
     * @return  int
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * Get $additionalInfo
     *
     * @return  string
     */
    public function getAdditionalInfo() {
        return $this->additionalInfo;
    }

    /**
     * Get $payday
     *
     * @return  string
     */
    public function getPayday() {
        return $this->payday;
    }

    /**
     * Get $payedOutsideGalaxPay
     *
     * @return  bool
     */
    public function getPayedOutsideGalaxPay() {
        return $this->payedOutsideGalaxPay;
    }

    /**
     * Get $mainPaymentMethodId
     *
     * @return  string
     */
    public function getMainPaymentMethodId() {
        return $this->mainPaymentMethodId;
    }

    /**
     * Get $customer
     *
     * @return  Customer
     */
    public function getCustomer() {
        $this->Customer = empty($this->Customer) ? new Customer() : $this->Customer;
        return $this->Customer;
    }

    /**
     * Get $paymentMethodBoleto
     *
     * @return  PaymentMethodBoleto
     */
    public function getPaymentMethodBoleto() {
        $this->PaymentMethodBoleto = empty($this->PaymentMethodBoleto) ? new PaymentMethodBoleto() : $this->PaymentMethodBoleto;
        return $this->PaymentMethodBoleto;
    }

    /**
     * Get $paymentMethodPix
     *
     * @return  PaymentMethodPix
     */
    public function getPaymentMethodPix() {
        $this->PaymentMethodPix = empty($this->PaymentMethodPix) ? new PaymentMethodPix() : $this->PaymentMethodPix;
        return $this->PaymentMethodPix;
    }

    /**
     * Set $myId
     *
     * @param  string  $myId  $myId
     *
     * @return  self
     */
    public function setMyId(string $myId) {
        $this->myId = $myId;

        return $this;
    }

    /**
     * Set $value
     *
     * @param  int  $value  $value
     *
     * @return  self
     */
    public function setValue(int $value) {
        $this->value = $value;

        return $this;
    }

    /**
     * Set $additionalInfo
     *
     * @param  string  $additionalInfo  $additionalInfo
     *
     * @return  self
     */
    public function setAdditionalInfo(string $additionalInfo) {
        $this->additionalInfo = $additionalInfo;

        return $this;
    }

    /**
     * Set $payday
     *
     * @param  string  $payday  $payday
     *
     * @return  self
     */
    public function setPayday(string $payday) {
        $this->payday = $payday;

        return $this;
    }

    /**
     * Set $payedOutsideGalaxPay
     *
     * @param  bool  $payedOutsideGalaxPay  $payedOutsideGalaxPay
     *
     * @return  self
     */
    public function setPayedOutsideGalaxPay(bool $payedOutsideGalaxPay) {
        $this->payedOutsideGalaxPay = $payedOutsideGalaxPay;

        return $this;
    }

    /**
     * Set $mainPaymentMethodId
     *
     * @param  string  $mainPaymentMethodId  $mainPaymentMethodId
     *
     * @return  self
     */
    public function setMainPaymentMethodId(string $mainPaymentMethodId) {
        $this->mainPaymentMethodId = $mainPaymentMethodId;

        return $this;
    }

    /**
     * Set $customer
     *
     * @param  Customer  $customer  $customer
     *
     * @return  self
     */
    public function setCustomer(Customer $customer) {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Set $paymentMethodBoleto
     *
     * @param  PaymentMethodBoleto  $paymentMethodBoleto  $paymentMethodBoleto
     *
     * @return  self
     */
    public function setPaymentMethodBoleto(PaymentMethodBoleto $paymentMethodBoleto) {
        $this->paymentMethodBoleto = $paymentMethodBoleto;

        return $this;
    }

    /**
     * Set $paymentMethodPix
     *
     * @param  PaymentMethodPix  $paymentMethodPix  $paymentMethodPix
     *
     * @return  self
     */
    public function setPaymentMethodPix(PaymentMethodPix $paymentMethodPix) {
        $this->paymentMethodPix = $paymentMethodPix;

        return $this;
    }

}
