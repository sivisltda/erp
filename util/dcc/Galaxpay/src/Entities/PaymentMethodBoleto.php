<?php

namespace ODGalaxpay\Entities;

class PaymentMethodBoleto extends BaseEntity {

    /**
     * @var int $fine
     */
    protected $fine;

    /**
     * @var int $interest
     */
    protected $interest;

    /**
     * @var string $instructions
     */
    protected $instructions;

    /**
     * @var int $deadlineDays
     */
    protected $deadlineDays;
    
    /**
     * @var Discount $discount
     */
    protected $discount;    

    /**
     * Get $fine
     *
     * @return  int
     */
    public function getFine() {
        return $this->fine;
    }

    /**
     * Get $interest
     *
     * @return  int
     */
    public function getInterest() {
        return $this->interest;
    }

    /**
     * Get $instructions
     *
     * @return  string
     */
    public function getInstructions() {
        return $this->instructions;
    }

    /**
     * Get $deadlineDays
     *
     * @return  int
     */
    public function getDeadlineDays() {
        return $this->deadlineDays;
    }
    
    /**
     * Get $discount
     *
     * @return  Discount
     */
    public function getDiscount() {
        $this->Discount = empty($this->Discount) ? new Discount() : $this->Discount;
        return $this->Discount;
    }

    /**
     * Set $fine
     *
     * @param  int  $fine  $fine
     *
     * @return  self
     */
    public function setFine(string $fine) {
        $this->fine = $fine;

        return $this;
    }

    /**
     * Set $interest
     *
     * @param  int  $interest  $interest
     *
     * @return  self
     */
    public function setInterest(string $interest) {
        $this->interest = $interest;

        return $this;
    }

    /**
     * Set $instructions
     *
     * @param  int  $instructions  $instructions
     *
     * @return  self
     */
    public function setInstructions(string $instructions) {
        $this->instructions = $instructions;

        return $this;
    }

    /**
     * Set $deadlineDays
     *
     * @param  int  $deadlineDays  $deadlineDays
     *
     * @return  self
     */
    public function setDeadlineDays(string $deadlineDays) {
        $this->deadlineDays = $deadlineDays;

        return $this;
    }
    
    /**
     * Set $discount
     *
     * @param  Discount  $discount  $discount
     *
     * @return  self
     */
    public function setDiscount(Discount $discount) {
        $this->discount = $discount;

        return $this;
    }    

}
