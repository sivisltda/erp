<?php

function columnSelect($val) {
    if (strripos($val, " ") == true) {
        $partes = explode(" ", $val);
        return $partes[1];
    } else {
        if (strripos($val, ".") == true) {
            $partes = explode(".", $val);
            return $partes[1];
        } else {
            return $val;
        }
    }
}

function tableFormato($valor, $tipo, $pos, $diretorio) {
    if ($pos == "C") {
        $begps = "<center>";
        $endpg = "</center>";
    }
    switch ($tipo) {
        case "T":
            return mb_strtoupper($begps . utf8_encode($valor), 'UTF-8') . $endpg;
            break;
        case "D":
            if ($valor != null) {
                return $begps . date_format(date_create($valor), 'd/m/Y') . $endpg;
            } else {
                return "";
            }
            break;
        case "DT":
            if ($valor != null) {
                return $begps . date_format(date_create($valor), 'd/m/Y H:i:s') . $endpg;
            } else {
                return "";
            }
            break;
        case "MA":
            if ($valor != null) {
                return $begps . date_format(date_create($valor), 'm/Y') . $endpg;
            } else {
                return "";
            }
            break;
        case "N":
            return $begps . $valor . $endpg;
            break;
        case "I":
            if (is_numeric($valor) && file_exists($diretorio . $valor . ".png")) {
                return $begps . "<img height=\"40px\" class=\"img-circle\" src=\"data:image/png;base64," . base64_encode(file_get_contents($diretorio . $valor . ".png")) . "\" alt=\"\" class=\"img1\">" . $endpg;
            } else {
                return $begps . "<img height=\"40px\" class=\"img-circle\" src=\"./../../assets/img/avatar.png\" alt=\"\" class=\"img1\">" . $endpg;
            }
            break;
        case "SN":
            if ($valor == 1) {
                return $begps . "Sim" . $endpg;
            } else {
                return $begps . "Não" . $endpg;
            }
            break;
        case "DE":
            return $begps . number_format($valor, 2, ",", ".") . $endpg;
            break;
    }
}

function trataAcesso($tipo) {
    $descr = "";
    if (substr($tipo, 0, 1) === '#') {
        $descr = ' - LIB. MANUAL(LUPA)';
        $tipo = substr_replace($tipo, "", 0, 1);
    }
    if (substr($tipo, 0, 1) === '+') {
        $descr = ' - LIB. CREDENCIAL';
        $tipo = substr_replace($tipo, "", 0, 1);
    }
    switch ($tipo) {
        case "AP":
            return "ACESSO PERMITIDO" . $descr;
        case "ANP":
            return "ACESSO NÃO PERMITIDO" . $descr;
        case "VCD":
            return "VENCIDO" . $descr;
        case "EVC":
            return "EM VENCIMENTO" . $descr;
        case "FH":
            return "FORA DE HORÁRIO" . $descr;
        case "FA":
            return "FORA DE AMBIENTE" . $descr;
        case "LIB":
            return "LIBERAÇÃO MANUAL" . $descr;
        case "SM":
            return "SEM MATRÍCULA" . $descr;
        case "EX":
            return "DOCUMENTO VENCIDO" . $descr;
        case "LB":
            return "LIBERAÇÃO MANUAL" . $descr;
        case "BL":
            return "BLOQUEADO" . $descr;
        case "CV":
            return "DÉBITO EM CARTEIRA" . $descr;
        case "MV":
            return "MENSALIDADE EM ABERTO" . $descr;
        case "LTA":
            return "LIMITE DE ACESSO" . $descr;
        case "CVD":
            return "CARTEIRA VENCIDA" . $descr;
        case "DOC":
            return "DOCUMENTO PENDENTE" . $descr;
    }
}

function getColor($i) {
    switch ($i) {
        case 0:
            return "rgb(132,183,97)";
        case 1:
            return "rgb(253,212,0)";
        case 2:
            return "rgb(103,183,220)";
        case 3:
            return "rgb(204,71,72)";
        case 4:
            return "rgb(205,130,173)";
        case 5:
            return "rgb(47,64,116)";
        case 6:
            return "rgb(68, 142, 77)";
        case 7:
            return "rgb(183, 184, 63)";
        case 8:
            return "rgb(185, 120, 63)";
    }
}

function getTextDCC($texto) {
    switch ($texto) {
        case "APPROVED":
            return "APROVADO";
            break;
        case "SETTLED":
            return "ESTORNADO";
            break;
        case "DECLINED":
            return "NEGADO";
            break;
        case "INVALID REQUEST":
            return "RETORNO INVÁLIDO";
            break;
        case "NAO ENVIADO":
            return "NÃO ENVIADO";
            break;
        case "CANCELADO":
            return "CANCELADO";
            break;
    }
}

function sameDateNextMonth(DateTime $createdDate, DateTime $currentDate) {
    $addMon = clone $currentDate;
    $addMon->add(new DateInterval("P1M"));

    $nextMon = clone $currentDate;
    $nextMon->modify("last day of next month");

    if ($addMon->format("n") == $nextMon->format("n")) {
        $recurDay = $createdDate->format("j");
        $daysInMon = $addMon->format("t");
        $currentDay = $currentDate->format("j");
        if ($recurDay > $currentDay && $recurDay <= $daysInMon) {
            $addMon->setDate($addMon->format("Y"), $addMon->format("n"), $recurDay);
        }
        return $addMon;
    } else {
        return $nextMon;
    }
}

function valorPorExtenso($valor = 0, $bolExibirMoeda = true, $bolPalavraFeminina = false) {
    $singular = null;
    $plural = null;
    if ($bolExibirMoeda) {
        $singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
        $plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões", "quatrilhões");
    } else {
        $singular = array("", "", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
        $plural = array("", "", "mil", "milhões", "bilhões", "trilhões", "quatrilhões");
    }
    $c = array("", "cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
    $d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa");
    $d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezesete", "dezoito", "dezenove");
    $u = array("", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove");
    if ($bolPalavraFeminina) {
        if ($valor == 1) {
            $u = array("", "uma", "duas", "três", "quatro", "cinco", "seis", "sete", "oito", "nove");
        } else {
            $u = array("", "um", "duas", "três", "quatro", "cinco", "seis", "sete", "oito", "nove");
        }
        $c = array("", "cem", "duzentas", "trezentas", "quatrocentas", "quinhentas", "seiscentas", "setecentas", "oitocentas", "novecentas");
    }
    $z = 0;
    $valor = number_format($valor, 2, ".", ".");
    $inteiro = explode(".", $valor);
    for ($i = 0; $i < count($inteiro); $i++) {
        for ($ii = mb_strlen($inteiro[$i]); $ii < 3; $ii++) {
            $inteiro[$i] = "0" . $inteiro[$i];
        }
    }
    $rt = null;
    $fim = count($inteiro) - ($inteiro[count($inteiro) - 1] > 0 ? 1 : 2);
    for ($i = 0; $i < count($inteiro); $i++) {
        $valor = $inteiro[$i];
        $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
        $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
        $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";
        $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd && $ru) ? " e " : "") . $ru;
        $t = count($inteiro) - 1 - $i;
        $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
        if ($valor == "000") {
            $z++;
        } elseif ($z > 0) {
            $z--;
        }
        if (($t == 1) && ($z > 0) && ($inteiro[0] > 0)) {
            $r .= ( ($z > 1) ? " de " : "") . $plural[$t];
        }
        if ($r) {
            $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
        }
    }
    $rt = mb_substr($rt, 1);
    return($rt ? trim($rt) : "zero");
}

function encrypt($frase, $chave, $crypt) {
    $retorno = "";
    if ($crypt) {
        $string = $frase;
        $i = strlen($string) - 1;
        $j = strlen($chave);
        do {
            $retorno .= ($string{$i} ^ $chave{$i % $j});
        } while ($i--);
        return base64_encode(strrev($retorno));
    } else {
        $string = base64_decode($frase);
        $i = strlen($string) - 1;
        $j = strlen($chave);
        do {
            $retorno .= ($string{$i} ^ $chave{$i % $j});
        } while ($i--);
        return strrev($retorno);
    }
}

function formatName($name) {
    $CompleteName = explode(" ", $name);
    $nomeUserLogin = $CompleteName[0];
    if (count($CompleteName) > 1) {
        $nomeUserLogin .= " " . $CompleteName[count($CompleteName) - 1];
    }
    return $nomeUserLogin;
}

function numPad($number, $dec = 8) {
    return str_pad($number, $dec, 0, STR_PAD_LEFT);
}

function now($formato = 'd/m/Y H:i:s') {
    $timeZone = new DateTimeZone('America/Sao_Paulo');
    return (new DateTime('NOW', $timeZone))->format($formato);
}

function salvaLog($tipoLog, $caminho, $filename, $message, $append = false) {
    $dr = DIRECTORY_SEPARATOR;
    $dir = __DIR__ . "./../Pessoas/003/" . $tipoLog . "/" . date("Y/m/d") . "/" . $caminho;
    $arquivo = $dir . '/' . $filename;
    if (!file_exists($dir)) {
        mkdir($dir, 0777, true);
    }
    $fp = fopen($arquivo, ($append ? 'a' : 'w'));
    fwrite($fp, $message . "----------------------------------------------------------------------------------------------"."\r\n");
    fclose($fp);
}