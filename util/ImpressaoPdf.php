<?php

error_reporting(0);

if (isset($_GET["crt"])) {
    require dirname(__FILE__) . "./../Connections/funcoesAux.php";
    $hostname = getDataBaseServer(1);
    $database = "ERP" . $_GET['crt'];
    $username = "erp" . $_GET['crt'];
    $password = "!Password123";
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
    $contrato = $_GET['crt'];
    $_SESSION["contrato"] = $_GET['crt'];
    $cur = odbc_exec($con, "select razao_social_contrato,nome_fantasia_contrato,endereco,numero,bairro,cidade_nome,estado,cep,cnpj,inscricao_estadual,telefone,site from sf_filiais") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $cabecalho = "<b>" . utf8_encode($RFP['razao_social_contrato']) . "</b> (" . utf8_encode($RFP['nome_fantasia_contrato']) . ")<br>" . utf8_encode($RFP['endereco']) . " n°" . utf8_encode($RFP['numero']) . ", " . utf8_encode($RFP['bairro']) . "<br>" . utf8_encode($RFP['cidade_nome']) . " " . utf8_encode($RFP['estado']) . " - CEP: " . utf8_encode($RFP["cep"]) . "<br>CNPJ: " . utf8_encode($RFP["cnpj"]) . " * IE: " . utf8_encode($RFP["inscricao_estadual"]) . "<br>" . utf8_encode($RFP["telefone"] . " | " . strtolower(utf8_encode($RFP["site"])));
        $_SESSION["cabecalho"] = $cabecalho;
        $filial_orc = "001";
    }    
} else if (file_exists('../Connections/configini.php')) {
    include '../Connections/configini.php';
}

function ajustarEscala($valor, $pagina) {
    return ((($pagina == "L" ? 287 : 200) * $valor) / ($pagina == "L" ? 1010 : 700));
}

if ($_GET["PathArq"] == "GenericModelPDF.php") {
    $imagem = dirname(__FILE__) . "./../Pessoas/" . $_SESSION["contrato"] . "/Empresa/logo_" . ($filial == "" ? "001" : $filial) . ".jpg";
    $relatorio = $_GET["NomeArq"];
    $titulo = $_GET["filter"];
    $titulos = explode("|", $_GET["lbl"]);
    $tamanhos = explode("|", $_GET["siz"]);
    $invisivel = explode("|", $_GET["pdf"]);
    if ($titulos[count($tamanhos) - 1] == "Ação") {
        $subTotal = 0;
        array_pop($titulos);
        $ult = $tamanhos[count($tamanhos) - 1];
        for ($i = 0; $i < count($tamanhos) - 1; $i++) {            
            $tamanhos[$i] = $tamanhos[$i] + floor(($tamanhos[$i] * $ult) / 100);
            $subTotal = $subTotal + $tamanhos[$i];
        }
        for ($i = 0; $i < (100 - $subTotal); $i++) {
            $tamanhos[$i] = $tamanhos[$i] + 1;
        }
        array_pop($tamanhos);
    }
    include (dirname(__FILE__) . "../" . $_GET["PathArqInclude"]);
    for ($i = 0; $i < count($tamanhos); $i++) {
        $tamanhos[$i] = ajustarEscala($tamanhos[$i], $_GET["pOri"]);
    }
    if (isset($_GET["tpImp"]) && $_GET["tpImp"] == "I") {
        $_GET["imp"] = 1;
    }
    if (isset($_GET["tpImp"]) && $_GET["tpImp"] == "E") {
        $_GET["imp"] = 2;
    }
    if (isset($_GET["imp"]) && $_GET["imp"] == 1) {
        require_once (dirname(__FILE__) . "/impressora/imprimir.php");
    } elseif (isset($_GET["imp"]) && $_GET["imp"] == 2) {
        require_once (dirname(__FILE__) . "/excel/excel.php");
    } elseif (isset($_GET["imp"]) && $_GET["imp"] == 3) {
        require_once (dirname(__FILE__) . "/ExportarImagens.php");
    }
} else {
    require_once('impressora/tcpdf.php');
    $pdfPage = 'A4';
    if ($recibo_tipo_ == 0 && isset($_GET['pAlt']) && isset($_GET['pLar'])) {
        $pdfPage = array($_GET['pAlt'], $_GET['pLar']);
    }
    $orientation = "P";
    if (isset($_GET['pOri'])) {
        $orientation = $_GET['pOri'];
    }
    $idSave = $_GET["id"];
    $pdf = new TCPDF($orientation, PDF_UNIT, $pdfPage, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Sivis Tecnologia');
    $pdf->SetTitle($_GET["NomeArq"] . ' - ' . $idSave);
    $pdf->SetSubject('Orcamento');
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    if ($_GET["PathArq"] == "ContratoAcademia" || $_GET["NomeArq"] == "Contrato") {
        $pdf->SetMargins(7 + (isset($_GET['red']) ? $_GET['red'] : 0), 5, 7);
    } else if (isset($_GET['pMrg']) && $_GET['pMrg'] == 0) {
        $pdf->SetMargins(0, 0, 0);
    } else {
        $pdf->SetMargins(5, 5, 5);
    }
    $pdf->SetHeaderMargin(0);
    $pdf->SetFooterMargin(0);
    $pdf->SetAutoPageBreak(TRUE, 5);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    $pdf->SetFont('helvetica', '', (isset($_GET['pFon']) ? $_GET['pFon'] : 10));
    $pdf->AddPage();
    ob_start();
    $pathArq = $_GET["PathArq"];
    $tipe_event = 0;

    $cur = odbc_exec($con, "select use_cnt from sf_configuracao") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $tipe_event = $RFP["use_cnt"];
    }
    if ($_GET["PathArq"] == 'ContratoAcademia') {
        if (file_exists('../Connections/configini.php')) {
            if (isset($_POST['contratoSivis'])) {
                $pathArq = '../Modulos/Academia/modelos/Contratos/Contrato_003.php';
            } else if ($tipe_event == "0") {
                $pathArq = '../Modulos/Academia/modelos/Contratos/Contrato_' . $_SESSION['contrato'] . $_GET["Modelo"] . '.php';
            } else if ($tipe_event == "1") {
                $_GET["id_plan"] = $idSave;
                $_GET["model"] = "S";
                $_GET["emp"] = "1";
                $pathArq = '../Modulos/Comercial/modelos/ModeloContrato.php';
            }
        } elseif (file_exists('../../Connections/configini.php')) {
            if (isset($_POST['contratoSivis'])) {
                $pathArq = '../../Modulos/Academia/modelos/Contratos/Contrato_003.php';
            } else if ($tipe_event == "0") {
                $pathArq = '../../Modulos/Academia/modelos/Contratos/Contrato_' . $_SESSION['contrato'] . $_GET["Modelo"] . '.php';
            } else if ($tipe_event == "1") {
                $_GET["id_plan"] = $idSave;
                $_GET["model"] = "S";
                $_GET["emp"] = "1";
                $pathArq = '../../Modulos/Comercial/modelos/ModeloContrato.php';
            }
        }
    }

    include $pathArq;
    $string = ob_get_clean();
    $html = $string;
    $pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();
    if ($_GET["tpImp"] == "I") {
        $pdf->Output(dirname(__FILE__) . '..\..\Pessoas\/' . $_SESSION['contrato'] . '\/' . $_GET["NomeArq"] . '\/' . $idSave . '.pdf', 'I');
    } else if ($_GET["tpImp"] == "F") {
        $url = dirname(__FILE__) . '..\..\Pessoas\/' . $_SESSION['contrato'] . '\/' . $_GET["NomeArq"] . '\/' . (isset($_GET["idCliente"]) ? $_GET["idCliente"] . '\/' : "");
        if (!is_dir($url)) {
            mkdir($url, 0777, true);
        }        
        if (isset($_POST['contratoSivis'])) {
            $pdf->Output($url . "SivisContrato.pdf", 'F');
        } else if (isset($_GET["id_vistoria"])) {
            $pdf->Output($url . "vistoria_" . $_GET["id_vistoria"] . '.pdf', 'F');
        } else if (isset($_GET["id_evento"])) {
            $pdf->Output($url . "evento_" . $_GET["id_evento"] . '.pdf', 'F');
        } else {
            $pdf->Output($url . $idSave . '.pdf', 'F');
        }
    }
}