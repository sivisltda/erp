<?php

require_once '../oneapi/client.php';

$customerProfileClient = new CustomerProfileClient('sivishomologacao', 'Sivis2016!');
$customerProfileClient->login();

$accountBalance = $customerProfileClient->getAccountBalance();

if(!$accountBalance->isSuccess()) {
    echo $accountBalance->exception;
    die();
}

echo 'accountBalance=', $accountBalance->balance, $accountBalance->currency->symbol;
