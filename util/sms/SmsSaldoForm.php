<?php

include "../../Connections/configSivis.php";

if (isset($_GET["hostname"]) && isset($_GET["database"]) && isset($_GET["username"]) && isset($_GET["password"])) {
    $contrato = $_GET['txtContrato'];
    $_POST['totTrans'] = 0;
    $hostname = $_GET["hostname"];
    $database = $_GET["database"];
    $username = $_GET["username"];
    $password = $_GET["password"];
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
    include "../../Connections/funcoesAux.php";    
} else {
    include "../../Connections/configini.php";    
}

if (isset($_REQUEST["tipo"]) && $_REQUEST["tipo"] == "1") { //Whatzap, verificar se vai controlar saldo?
    echo "100000"; exit;
}

$saldoAtual = 0;
$valorTransacao = 0.08;

$cur = odbc_exec($con, "select SMS_USUARIO,SMS_SENHA,isnull(last_saldo_sms,0) last_saldo_sms,last_update_sms from sf_configuracao where id = 1");
while ($RFP = odbc_fetch_array($cur)) {
    $usernameSMS = $RFP['SMS_USUARIO'];
    $passwordSMS = $RFP['SMS_SENHA'];
    $last_saldo_sms = $RFP['last_saldo_sms'];
    $last_update_sms = $RFP['last_update_sms'];
}

$transacoes = $_POST['totTrans'];
if ($last_update_sms == "" || date_format(date_create($last_update_sms), "d/m/Y") != date("d/m/Y")) {
    /*if (substr($contrato, 0, 1) !== "9" && $contrato !== "001") {
        $query = "select sms_custo from ca_clientes where sf_cliente in (select id_cliente from dbo.ca_contratos where numero_contrato = " . valoresTexto2($contrato) . ")";
        $cur = odbc_exec($con2, $query) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            if ($RFP['sms_custo'] !== "" && $RFP['sms_custo'] > 0) {
                $valorTransacao = $RFP['sms_custo'];
            }
        }
    }*/    
    $url = "https://api.smsfire.com.br/v1/account/balance";
    $header = [
        "Cache-Control: no-cache",
        "Content-Type: application/json",
        "Authorization: Basic " . base64_encode($usernameSMS . ":" . $passwordSMS)
    ];
    $ch = curl_init($url);                                                                      
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");                                                                 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                                                                    
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);        
    $output = curl_exec($ch);
    curl_close($ch);        
    $toReturn = json_decode($output);   
    $saldoApi = $toReturn->balance;
    $saldoAtual = floor(($toReturn->balance / $valorTransacao));
    /*if (substr($contrato, 0, 1) !== "9") {
        $query = "update ca_clientes set sms_saldo = " . $saldoAtual . ", sms_ult_sinc = getDate() where sf_cliente in (select id_cliente from dbo.ca_contratos where numero_contrato = " . valoresTexto2($contrato) . ")";
        odbc_exec($con2, $query) or die(odbc_errormsg());
    }*/
    $query = "update sf_configuracao set last_saldo_sms = " . $saldoAtual . ",last_update_sms = getDate() where id = 1";
    odbc_exec($con, $query) or die(odbc_errormsg());
} else {
    $saldoAtual = $last_saldo_sms;
    $cur = odbc_exec($con, "set dateformat dmy;select count(id_sms_hist) total from sf_sms_hist where data_envio between '" . date("d/m/Y") . " 00:00:00' and '" . date("d/m/Y") . " 23:59:59'");
    while ($RFP = odbc_fetch_array($cur)) {
        $saldoAtual = $saldoAtual - $RFP['total'];
    }
}

echo ($saldoAtual - $transacoes);
