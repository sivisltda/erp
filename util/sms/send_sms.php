<?php

if ($_GET['btnEnviar']) {
    $_POST['btnEnviar'] = $_GET['btnEnviar'];
    $_POST['txtId'] = $_GET['txtId'];
    $_POST['txtSendSms'] = $_GET["txtSendSms"];
    $_POST["ckeditor"] = $_GET["ckeditor"];
    $_SESSION["id_usuario"] = $_GET['txtUser'];
    $_SESSION["login_usuario"] = "SISTEMA";
}

if (isset($_POST["btnEnviar"])) {
    if (isset($_GET["hostname"]) && isset($_GET["database"]) && isset($_GET["username"]) && isset($_GET["password"])) {
        $hostname = $_GET["hostname"];
        $database = $_GET["database"];
        $username = $_GET["username"];
        $password = $_GET["password"];
        $contrato = str_replace("ERP", "", strtoupper($_GET['database']));
        $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
        require_once(__DIR__ . '/../../Connections/funcoesAux.php');
    } else {
        include "../../Connections/configini.php";
    }
    if (is_numeric($_POST['txtId'])) {
        $queryTexto = "select mensagem from sf_emails where tipo > 0 and id_email = " . $_GET['txtId'];
        $cur = odbc_exec($con, $queryTexto);
        while ($RFP = odbc_fetch_array($cur)) {
            $_POST["ckeditor"] = utf8_encode($RFP['mensagem']);
        }
        if ($_POST["ckeditor"] == "") {
            $local[] = array('msg_erro' => "Erro ao ler Mensagem");
            echo(json_encode($local));
            exit;
        }
    }
    $fromSMS = "";
    $usernameSMS = "";
    $passwordSMS = "";
    $cur = odbc_exec($con, "select * from sf_configuracao where id = 1");
    while ($RFP = odbc_fetch_array($cur)) {
        $fromSMS = $RFP['SMS_USUARIO'];
        $usernameSMS = $RFP['SMS_USUARIO'];
        $passwordSMS = $RFP['SMS_SENHA'];
    }
    if ($_REQUEST["txtTipo"] != "1" && ($usernameSMS == "" || $passwordSMS == "")) {
        $local[] = array('msg_erro' => "Credenciais inválidas");
        echo(json_encode($local));
        exit;
    }
    $items = explode(",", $_POST['txtSendSms']);
    $local[] = recursiveSend($con, $items, 0, $_POST["ckeditor"], $_REQUEST["txtTipo"], $fromSMS, $usernameSMS, $passwordSMS, 0, 0, $contrato);
    echo(json_encode($local));
    exit;
}

function recursiveSend($con, $items, $k, $mensagem, $tipo, $fromSMS, $usernameSMS, $passwordSMS, $qtd_sucesso, $qtd_erro, $contrato) {
    $sms = explode("|", $items[$k]);
    $numero_cel = str_replace(array(" ", "(", ")", "-"), "", $sms[0]);
    $pessoa = $sms[1];
    $tmpMsg = $mensagem;
    if (strpos($mensagem, '||') !== false) { //0 - Sms, 1 - id pessoa, 2 - plano, 3 - mensalidade
        $tipo_conta = (isset($sms[3]) ? explode("-", $sms[3]) : "");        
        if (count($tipo_conta) > 1 && $tipo_conta[0] == "D" || $tipo_conta[0] == "Y") {
            $_GET["id_contrato"] = $tipo_conta[1];
        } else if (count($tipo_conta) > 1 && $tipo_conta[0] == "A") {
            $_GET["id_variavel"] = $tipo_conta[1];
        } else if (count($tipo_conta) > 1 && $tipo_conta[0] == "V") {
            $_GET["id_venda"] = $tipo_conta[1];
        } else if ((count($tipo_conta) > 1 && $tipo_conta[0] == "M")) {
            $_GET["id_mensalidade"] = $tipo_conta[1];    
        } else if (isset($sms[3]) && is_numeric($sms[3])) {
            $_GET["id_mensalidade"] = $sms[3];                
        } else if (isset($sms[2]) && is_numeric($sms[2])) {
            $_GET["id_plano"] = $sms[2];                        
        } else if (isset($sms[1]) && is_numeric($sms[1])) {
            $_GET["id_pessoa"] = $sms[1];    
        }        
        include __DIR__ . "./../../Modulos/Comercial/modelos/variaveis.php";
        for ($i = 0; $i < count($dicionario); $i++) {
            $tmpMsg = str_replace("||" . $dicionario[$i][0] . (strpos($dicionario[$i][0], "|") ? "|" : "||"), $dicionario[$i][1], $tmpMsg);
        }
    }
    $permitir = 1;
    if (is_numeric($sms[1])) {
        $cur = odbc_exec($con, "select celular_marketing from sf_fornecedores_despesas where id_fornecedores_despesas = " . $sms[1]);
        while ($RFP = odbc_fetch_array($cur)) {
            $permitir = $RFP['celular_marketing'];
        }
    }
    if ($tipo == "1" && $permitir > 0) {
        $query = "insert into sf_whatsapp(telefone, mensagem, data_envio, fornecedor_despesas, sys_login, status) values (" . 
        valoresTextoSMS($numero_cel) . "," . valoresTextoSMS($tmpMsg) . "," . 
        ($_SESSION["login_usuario"] == "SISTEMA" ? "dateadd(minute, (select top 1 add_whatsapp from sf_configuracao), GETDATE())" : "GETDATE()") . "," . 
        $pessoa . "," . valoresTextoSMS($_SESSION["login_usuario"]) . ",0);";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $qtd_sucesso = $qtd_sucesso + 1;
    } else if ($tipo == "1" && $permitir == 0) {
        $qtd_erro = $qtd_erro + 1;
    } else {
        $postData = array("to" => array("55" . $numero_cel), "from" => $fromSMS, "text" => $tmpMsg);
        $url = "https://api.smsfire.com.br/v1/sms/send";
        $header = [
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Basic " . base64_encode($usernameSMS . ":" . $passwordSMS)
        ];
        if ($permitir > 0) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            $output = curl_exec($ch);
            curl_close($ch);
            $toReturn = json_decode($output);
            $retorno = $toReturn->sms[0]->status->code;
        } else {
            $retorno = 0;
        }
        if ($retorno == 1) {
            $qtd_sucesso = $qtd_sucesso + 1;
        } else {
            $qtd_erro = $qtd_erro + 1;
        }
        $query = "insert into sf_sms_hist (id_fornecedores_despesas, descricao, data_envio, id_user_resp, numero_telefone, retorno) values
        (" . $pessoa . "," . valoresTextoSMS($tmpMsg) . ",getdate()," . $_SESSION["id_usuario"] . "," . valoresTextoSMS($numero_cel) . "," . $retorno . ");";
        odbc_exec($con, $query) or die(odbc_errormsg());
        if ($retorno == 1) {
            $query = "insert into sf_telemarketing(de_pessoa,para_pessoa,data_tele,pendente,proximo_contato,obs,pessoa,data_tele_fechamento, obs_fechamento, de_pessoa_fechamento,especificacao,atendimento_servico,garantia,relato,origem,acompanhar,dt_inclusao)
            values (" . $_SESSION["id_usuario"] . "," . $_SESSION["id_usuario"] . ",getdate(),0,getdate()," . valoresTextoSMS($numero_cel) . "," . $pessoa . ",getdate(),'SMS MARKETING', " . $_SESSION["id_usuario"] . ",'S', 0,0," . valoresTextoSMS($tmpMsg) . ",0,0,getdate());";
            odbc_exec($con, $query) or die(odbc_errormsg());
        }
    }
    $k++;
    if ($k < sizeof($items)) {
        return recursiveSend($con, $items, $k, $mensagem, $tipo, $fromSMS, $usernameSMS, $passwordSMS, $qtd_sucesso, $qtd_erro, $contrato);
    } else {
        return array('qtd_total' => sizeof($items), 'qtd_sucesso' => $qtd_sucesso, 'qtd_erro' => $qtd_erro);
    }
}

function valoresTextoSMS($nome) {
    return "'" . utf8_decode(str_replace("'", "", $nome)) . "'";
}
