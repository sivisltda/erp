<?php

$path = 'Vistorias';
$dirs = array();
$dir = dir($path);
while (false !== ($entry = $dir->read())) {
    if ($entry != '.' && $entry != '..') {
        if (is_dir($path . '/' . $entry)) {
            $dirs[] = $path . '/' . $entry;
        }
    }
}

foreach ($dirs as $value) {
    echo readall($value);
}

function readall($dir) {    
    $data = "";
    $toReturn = "";
    $cdir = scandir($dir);    
    foreach ($cdir as $key => $value) {
        if (!in_array($value, array(".", ".."))) {            
            if (strlen($data) == 0) {
                $data = date("d/m/Y", filemtime($dir . "/". $value));
            } else if ($toReturn == "" && $data != date("d/m/Y", filemtime($dir . "/". $value))) {
                $toReturn = $dir . "<br>";
            }
        }
    }
    return $toReturn;
}