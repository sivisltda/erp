<?php

$loja = "000";

if (is_numeric($_GET["id"])) {
    $loja = $_GET["id"];
}

$url_de = './../../Pessoas/' . $loja . '/A_Pagar/';
if ($handle = opendir($url_de)) {
    while (false !== ($entry = readdir($handle))) {
        $extension = explode(".", $entry);
        $extension = $extension[count($extension) - 1];
        if ($entry != "." && $entry != ".." && in_array($extension, array("gif", "jpeg", "jpg", "png", "pdf", "GIF", "JPEG", "JPG", "PNG"))) {
            $pasta = str_replace(["." . $extension, "-"], "", $entry);
            if (!file_exists($url_de . $pasta)) {
                mkdir($url_de . $pasta, 0777, true);
            }  
            rename($url_de . $entry, $url_de . $pasta . '/' . $entry);
            echo $entry . "|" . $pasta . "<br>";
        }
    }
    closedir($handle);
}
