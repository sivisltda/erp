<?php

require_once('./../../Excel/excel_reader.php');

$hostname2 = "148.72.177.101,6000";
$database2 = "erp169";
$username2 = "erp169";
$password2 = "!Password123";
$conn = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname2 . "; DATABASE=" . $database2 . ";", $username2, $password2)or die(odbc_errormsg());
$url = "./../../Pessoas/169/Integracao/";

function conectarFTP() {
    $servidor = 'condominioweb.novi.srv.br'; // Endereço
    $usuario = 'ftp01'; // Usuário
    $senha = 'Abacate@2019'; // Senha
    $porta = 21; // Porta padrão
    $timeout = 9000; // Tempo em segundos para encerrar a conexão caso não haja resposta
    $ftp = ftp_connect($servidor, $porta, $timeout); // Retorno: true ou false
    if (!$ftp) {
        return false;
    }
    $login = ftp_login($ftp, $usuario, $senha); // Retorno: true ou false
    if (!$login) {
        return false;
    }
    return $ftp;
}

function listarArquivosPorFTP() {
    $ftp = conectarFTP();
    if (!is_resource($ftp)) {
        return false;
    }
    ftp_pasv($ftp, true);
    ftp_chdir($ftp, "");
    $arquivos = ftp_nlist($ftp, ".");
    ftp_close($ftp); // Fecha a conexão com o FTP
    return $arquivos;
}

function baixarArquivosPorFTP($strArquivoRemoto, $strArquivoLocal) {
    $ftp = conectarFTP();
    if (!is_resource($ftp)) {
        return false;
    }
    ftp_pasv($ftp, true);
    $download = ftp_get($ftp, $strArquivoLocal, $strArquivoRemoto, FTP_BINARY); // Retorno: true / false
    if ($download) {
        echo "Arquivo '{$strArquivoRemoto}' baixado com sucesso do FTP para '{$strArquivoLocal}'<br>";
    } else {
        echo "Falha ao baixar o arquivo {$strArquivoRemoto} do FTP!<br>";
        ftp_close($ftp); // Fecha a conexão com o FTP
        return false;
    }
    ftp_close($ftp); // Fecha a conexão com o FTP
    return true;
}

//print_r(listarArquivosPorFTP()); 

if (isset($_GET["tipo"]) && $_GET["tipo"] == "clientes") {    
    //http://localhost/erp/util/integracao/169.php?tipo=clientes
    baixarArquivosPorFTP("RelatorioUnidades.xls", $url . "RelatorioUnidades.xls");
    $i = 1;
    $documento = [];
    $excel = new PhpExcelReader;
    $excel->read($url . "RelatorioUnidades.xls");
    $nr_sheets = count($excel->sheets);
    while ($i <= $excel->sheets[2]['numRows']) {
        $itens = $excel->sheets[2]['cells'][$i];
        if (strlen(trim($itens[19])) == 14 || strlen(trim($itens[19])) == 18) {
            $documento[trim($itens[19])] = array(
            getValue($itens, 4), getValue($itens, 5), getValue($itens, 19), 0,
            getValue($itens, 8), getValue($itens, 9), getValue($itens, 10), getValue($itens, 11), 
            getValue($itens, 12), getValue($itens, 13), getValue($itens, 14), 
            getValue($itens, 15));
        }        
        $i++;
    }
    $documento_ = buscarCliente($conn, $documento);
    foreach ($documento_ as $value) {
        if ($value[3] == "0") {
            criarCliente($conn, $value);
        }
    }    
}

if (isset($_GET["tipo"]) && $_GET["tipo"] == "pagamentos") {
    //http://localhost/erp/util/integracao/169.php?tipo=pagamentos
    baixarArquivosPorFTP("RecibosPagos.xls", $url . "RecibosPagos.xls");
    $i = 1;
    $documento = [];
    $excel = new PhpExcelReader;
    $excel->read($url . "RecibosPagos.xls");
    $nr_sheets = count($excel->sheets);
    while ($i <= $excel->sheets[3]['numRows']) {
        $itens = $excel->sheets[3]['cells'][$i];
        if (strlen($itens[5]) == 4) {
            $documento[$itens[1]] = array($itens[5], $itens[8], $itens[9], "", "", "", "", $itens[1]);
        }
        $i++;
    }
    foreach ($documento as $value) {
        $documento[$value[7]] = buscarPlano($conn, $value);
        criarPlano($conn, $documento[$value[7]]);
    }
}

function getValue($item, $i) {
    if (isset($item[$i])) {
        return utf8_decode(utf8_encode($item[$i]));
    } else {
        return "";
    }
}

function criarCliente($conn, $value) {
    $query = "set dateformat dmy;insert into sf_fornecedores_despesas(empresa,razao_social,cnpj,sexo, juridico_tipo,tipo,inativo,dt_cadastro,Observacao,
    endereco,numero,complemento,bairro,cidade,estado,cep,fornecedores_status)    
    values ('001', '" . $value[1] . "', '" . $value[2] . "', 'M','" . (strlen($value[2]) == 14 ? "F" : "J") . "', 'C', 0, getdate(), '" . $value[0] . "',
    '"  . $value[4] . " " .  $value[5] . "', '" . $value[6] . "', '" . $value[7] . "', '" . $value[8] . "',
    (select max(cidade_codigo) from tb_cidades inner join tb_estados on estado_codigo = cidade_codigoEstado
    where cidade_nome = '" . $value[9] . "' and estado_sigla = '" . $value[10] . "'),
    (select max(estado_codigo) from tb_estados where estado_sigla = '" . $value[10] . "'), '" . $value[11] . "','Inativo');
    DECLARE @idCliente BIGINT;SELECT @idCliente = SCOPE_IDENTITY();
    insert into sf_fornecedores_despesas_campos_livres (campos_campos, fornecedores_campos, conteudo_campo)
    values(1, @idCliente, '" . $value[0] . "');";
    odbc_exec($conn, $query);
    echo $value[0] . "-" . $value[1] . "-" . $value[2] . "-" . $value[3] . "-" . $value[4] . "-" . $value[5] . "-" . $value[6] . "-" . $value[7] . "-" . $value[8] . "-" . $value[9] . "-" . $value[10] . "-" . $value[11] . "<br>";
}

function buscarCliente($conn, $documento) {
    $where = "''";
    foreach ($documento as $value) {
        $where .= ",'" . $value[2] . "'";
    }
    $cur = odbc_exec($conn, "select razao_social, cnpj from sf_fornecedores_despesas 
    where inativo = 0 and cnpj in (" . $where . ") and len(cnpj) > 0;");
    while ($RFP = odbc_fetch_array($cur)) {
        $documento[$RFP['cnpj']][3] = 1;
    }
    return $documento;
}

function buscarPlano($conn, $value) {
    $cur = odbc_exec($conn, "set dateformat dmy;
    select fornecedores_campos, id_plano, id_mens, id_item_venda_mens
    from sf_fornecedores_despesas_campos_livres c
    inner join sf_fornecedores_despesas p on p.id_fornecedores_despesas = c.fornecedores_campos
    left join sf_vendas_planos v on c.fornecedores_campos = v.favorecido and id_prod_plano = 2
    left join sf_vendas_planos_mensalidade m on v.id_plano = m.id_plano_mens and dt_inicio_mens = '" . $value[1] . "'
    where c.campos_campos = 1 and c.conteudo_campo = '" . $value[0] . "' and p.inativo = 0 and fornecedores_campos not in (select id_dependente from sf_fornecedores_despesas_dependentes);");
    while ($RFP = odbc_fetch_array($cur)) {
        $value[3] = $RFP['fornecedores_campos'];
        $value[4] = $RFP['id_plano'];
        $value[5] = $RFP['id_mens'];
        $value[6] = $RFP['id_item_venda_mens'];
    }
    return $value;
}

function criarPlano($conn, $value) {
    if (is_numeric($value[3])) {
        if (!is_numeric($value[4])) {
            $query = "set dateformat dmy; 
            insert into sf_vendas_planos(id_turno, dt_inicio, dt_fim, favorecido, dias_ferias, dt_cadastro, usuario_resp, id_prod_plano, planos_status) 
            values (1, '" . $value[1] . "', '" . $value[1] . "', " . $value[3] . ", 0, getdate(), 1, 2, 'Novo')
            SELECT SCOPE_IDENTITY() ID;";
            $result = odbc_exec($conn, $query) or die(odbc_errormsg());
            odbc_next_result($result);
            $value[4] = odbc_result($result, 1);
        }
        if (is_numeric($value[4]) && !is_numeric($value[5])) {
            $query = "set dateformat dmy; 
            insert into sf_vendas_planos_mensalidade(id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens)
            values (" . $value[4] . ", '" . $value[1] . "', dateadd(day,-1,dateadd(month,1,'" . $value[1] . "')), 1, getdate(), 0.01)
            SELECT SCOPE_IDENTITY() ID;";
            $result = odbc_exec($conn, $query) or die(odbc_errormsg());
            odbc_next_result($result);
            $value[5] = odbc_result($result, 1);            
        }
        if (is_numeric($value[4]) && is_numeric($value[5]) && !is_numeric($value[6])) {
            $query = "insert into sf_vendas(vendedor, cliente_venda, historico_venda, data_venda, sys_login, empresa, destinatario, status, 
            cov, descontop, descontos, descontotp, descontots, n_servicos, data_aprov, grupo_conta, conta_movimento, favorito, reservar_estoque, n_produtos)
            values ('ADMIN', " . $value[3] . ", 'ABONO / PLANO', getdate(), 'ADMIN', 1, 'ADMIN', 'Aprovado', 'V', 0.00, 0.00, 0, 0, 1, getdate(), 14, 1, 0, 0, 1)
            SELECT SCOPE_IDENTITY() ID;";
            $result = odbc_exec($conn, $query) or die(odbc_errormsg());
            odbc_next_result($result);
            $venda = odbc_result($result, 1);
            $query = "insert into sf_vendas_itens(id_venda, grupo, produto, quantidade, valor_total, valor_bruto, valor_multa)
            values (" . $venda . ", 160, 2, 1.000, 0.00, 0.00, 0.00)
            SELECT SCOPE_IDENTITY() ID;";
            $result = odbc_exec($conn, $query) or die(odbc_errormsg());
            odbc_next_result($result);
            $venda_item = odbc_result($result, 1);            
            $query = "set dateformat dmy;
            update sf_vendas_planos_mensalidade set dt_pagamento_mens = '" . $value[2] . "', id_item_venda_mens = " . $venda_item . " where id_mens = " . $value[5];
            odbc_exec($conn, $query) or die(odbc_errormsg());              
            $query = "insert into sf_vendas_planos_mensalidade (id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens)
            select id_plano_mens, dateadd(month ,1,dt_inicio_mens), dateadd(day,-1,dateadd(month,2,dt_inicio_mens)), id_parc_prod_mens, GETDATE(),valor_unico valor_mens 
            from sf_vendas_planos_mensalidade m1 inner join sf_produtos_parcelas on id_parc_prod_mens = id_parcela 
            inner join sf_produtos on sf_produtos.conta_produto = sf_produtos_parcelas.id_produto
            inner join sf_vendas_planos on id_plano = id_plano_mens                                
            where inativar_renovacao = 0 and id_mens = " . $value[5] . " and dt_cancelamento is null and parcela in (-1,0,1) 
            and (select COUNT(*) from sf_vendas_planos_mensalidade m2
            where m2.id_plano_mens = m1.id_plano_mens and m2.id_parc_prod_mens = m1.id_parc_prod_mens
            and month(m2.dt_inicio_mens) = month(dateadd(month ,1,m1.dt_inicio_mens))
            and year(m2.dt_inicio_mens) = year(dateadd(month ,1,m1.dt_inicio_mens))) = 0;";
            odbc_exec($conn, $query) or die(odbc_errormsg());            
            $query = "update sf_vendas_planos set dt_fim = dbo.FU_PLANO_FIM(id_plano) where id_plano = " . $value[4] . ";
            update sf_fornecedores_despesas set fornecedores_status = dbo.FU_STATUS_CLI(id_fornecedores_despesas) where tipo = 'C' and id_fornecedores_despesas = " . $value[3] . ";";
            odbc_exec($conn, $query) or die(odbc_errormsg());             
            $value[6] = $venda_item;            
            echo "Pago - " . $value[3] . " - " . $value[2] . "<br>";
        } else {
            echo "Falha - " . $value[7] . " (" . $value[0] . "," . $value[1] . "," . $value[2] . "," . $value[3] . "," . $value[4] . "," . $value[5] . "," . $value[6] . ")<br>";
        }
    } else {
        echo "Não encontrado - " . $value[0] . "|" . $value[1] . "<br>";
    }
}

