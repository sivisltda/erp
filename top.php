<?php
$protocolo = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http');
$host = $_SERVER['HTTP_HOST'];
$UrlAtual = $protocolo . '://' . $host;

if (isset($_POST['bntBlock'])) {
    session_start();
    session_destroy();
}
if (isset($_POST['bntSair'])) {
    session_start();
    session_destroy();
}

$cur = odbc_exec($con, "select A.id_filial_f from sf_usuarios_filiais A
                        inner join sf_usuarios B on (A.id_usuario_f = B.id_usuario and A.id_filial_f = B.filial)
                        where A.id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
while ($RFP = odbc_fetch_array($cur)) {
    $filial = $RFP['id_filial_f'];
}

if ($filial == '') {
    $cur = odbc_exec($con, "select TOP 1 id_filial_f from sf_usuarios_filiais A 
    inner join sf_usuarios B on (A.id_usuario_f = B.id_usuario and A.id_filial_f = B.filial)        
    where id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
    while ($RFP = odbc_fetch_array($cur)) {
        $filial = $RFP['id_filial_f'];
    }
    odbc_exec($con, "UPDATE sf_usuarios set filial = " . $filial . " where id_usuario = '" . $_SESSION["id_usuario"] . "'");
}

$Orcamentos_Auto = 0;
$Vendas_Auto = 0;
$Compras_Auto = 0;
$Entrada_Saida = 0;
$Requisicao_Auto = 0;
$Cotacao_Auto = 0;
$Contratos_Auto = 0;
$ContasFixas_Auto = 0;
$Solicitacao_autorizacao = 0;
$LeadsPendentes = 0;
$AssitenciasPendentes = 0;
$VistoriasPendentes = 0;
$SinistrosPendentes = 0;
$ProspectsEmAnalise = 0;

$cur = odbc_exec($con, "select COUNT(id_venda) quantidade from sf_vendas where status = 'Aguarda' and empresa = " . $filial . " and destinatario = '" . $_SESSION["login_usuario"] . "' and cov = 'O' AND ent_sai = 0");
while ($RFP = odbc_fetch_array($cur)) {
    $Orcamentos_Auto = utf8_encode($RFP['quantidade']);
}
$cur = odbc_exec($con, "select COUNT(id_venda) quantidade from sf_vendas where status = 'Aguarda' and empresa = " . $filial . " and destinatario = '" . $_SESSION["login_usuario"] . "' and cov = 'V' AND ent_sai = 0");
while ($RFP = odbc_fetch_array($cur)) {
    $Vendas_Auto = utf8_encode($RFP['quantidade']);
}
$cur = odbc_exec($con, "select COUNT(id_venda) quantidade from sf_vendas where status = 'Aguarda' and empresa = " . $filial . " and destinatario = '" . $_SESSION["login_usuario"] . "' and cov = 'C' AND ent_sai = 0");
while ($RFP = odbc_fetch_array($cur)) {
    $Compras_Auto = utf8_encode($RFP['quantidade']);
}
$cur = odbc_exec($con, "select COUNT(id_venda) quantidade from sf_vendas where status = 'Aguarda' and empresa = " . $filial . " and destinatario = '" . $_SESSION["login_usuario"] . "' and cov in ('C','V') AND ent_sai = 1");
while ($RFP = odbc_fetch_array($cur)) {
    $Entrada_Saida = utf8_encode($RFP['quantidade']);
}
$cur = odbc_exec($con, "select COUNT(id_requisicao) quantidade from sf_requisicao where status = 'Aguarda' and empresa = " . $filial . " and destinatario = '" . $_SESSION["login_usuario"] . "' and cov = 'R'");
while ($RFP = odbc_fetch_array($cur)) {
    $Requisicao_Auto = utf8_encode($RFP['quantidade']);
}
$cur = odbc_exec($con, "select COUNT(id_requisicao) quantidade from sf_requisicao where status = 'Aprovado' and status_cotacao = 'Aguarda' and empresa = " . $filial . " and destinatario_cotacao = '" . $_SESSION["login_usuario"] . "' and cov = 'R'");
while ($RFP = odbc_fetch_array($cur)) {
    $Cotacao_Auto = utf8_encode($RFP['quantidade']);
}
$cur = odbc_exec($con, "select count(id_solicitacao_autorizacao) quantidade from sf_solicitacao_autorizacao where status = 'Aguarda' and empresa = " . $filial . " and (destinatario = '" . $_SESSION["login_usuario"] . "' or destinatario2 = '" . $_SESSION["login_usuario"] . "')");
while ($RFP = odbc_fetch_array($cur)) {
    $Solicitacao_autorizacao = utf8_encode($RFP['quantidade']);
}
$cur = odbc_exec($con, "select count(id_lancamento_movimento) quantidade from sf_lancamento_movimento l
inner join sf_contas_movimento cm on cm.id_contas_movimento = l.conta_movimento    
where status = 'Aguarda' and cm.tipo = 'C' and l.inativo = 0 and empresa = " . $filial . " and destinatario = '" . $_SESSION["login_usuario"] . "'");
while ($RFP = odbc_fetch_array($cur)) {
    $Contratos_Auto = utf8_encode($RFP['quantidade']);
}
$cur = odbc_exec($con, "select count(id_lancamento_movimento) quantidade from sf_lancamento_movimento l
inner join sf_contas_movimento cm on cm.id_contas_movimento = l.conta_movimento    
where status = 'Aguarda' and cm.tipo = 'D' and l.inativo = 0 and empresa = " . $filial . " and destinatario = '" . $_SESSION["login_usuario"] . "'");
while ($RFP = odbc_fetch_array($cur)) {
    $ContasFixas_Auto = utf8_encode($RFP['quantidade']);
}
$cur = odbc_exec($con, "SELECT COUNT(*) total FROM sf_fornecedores_despesas_veiculo v 
inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = v.id_fornecedores_despesas 
left join sf_vendas_planos on id_veiculo = v.id and dt_cancelamento is null left join sf_produtos on id_prod_plano = conta_produto 
WHERE id > 0 AND v.status = 'Aguarda' and id_externo is null and sf_vendas_planos.planos_status in ('Ativo')");
while ($RFP = odbc_fetch_array($cur)) {
    $AssitenciasPendentes = utf8_encode($RFP['total']);
}
$cur = odbc_exec($con, "SELECT COUNT(*) total FROM sf_fornecedores_despesas_veiculo v 
inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = v.id_fornecedores_despesas 
left join sf_vendas_planos on id_veiculo = v.id and dt_cancelamento is null left join sf_produtos on id_prod_plano = conta_produto 
WHERE id > 0 AND v.status = 'Aguarda' and id_externo is not null and sf_vendas_planos.planos_status in ('Ativo')");
while ($RFP = odbc_fetch_array($cur)) {
    $VeiculosAguardando = utf8_encode($RFP['total']);
}
$cur = odbc_exec($con, "SELECT COUNT(*) total from sf_vistorias vi 
inner join sf_fornecedores_despesas_veiculo v on vi.id_veiculo = v.id
inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = v.id_fornecedores_despesas 
left join sf_vendas_planos on sf_vendas_planos.id_veiculo = v.id and dt_cancelamento is null left join sf_produtos on id_prod_plano = conta_produto 
WHERE vi.id > 0 AND vi.data_aprov is null and sf_vendas_planos.planos_status in ('Ativo')");
while ($RFP = odbc_fetch_array($cur)) {
    $VistoriasPendentes = utf8_encode($RFP['total']);
}
$cur = odbc_exec($con, "select COUNT(*) total from sf_telemarketing t 
where t.atendimento_servico = 2 AND t.status = 'Aguarda'");
while ($RFP = odbc_fetch_array($cur)) {
    $SinistrosPendentes = utf8_encode($RFP['total']);
}
$cur = odbc_exec($con, "select count(*) total FROM sf_fornecedores_despesas 
WHERE id_fornecedores_despesas > 0 AND tipo = 'P' and sf_fornecedores_despesas.inativo = 0 
and fornecedores_status = 'Analise'");
while ($RFP = odbc_fetch_array($cur)) {
    $ProspectsEmAnalise = utf8_encode($RFP['total']);
}
?>
<style type="text/css">
    .boxIdEsq {
        float: left;
        color: #666;
        height: 100px;
        line-height: 1;
        font-size: 11px;
        max-width: 170px;
        margin-top: 10px;
        padding-left: 10px;
        border-left: solid 2px #F60;
    }
    .boxIdDir {
        width: 160px;
        height: 80px;
        padding: 10px;
        line-height: 1;
        font-size: 12px;
        background: #555;
        margin-left: 2px;
    }
    .headerBar {
        height: 59px;
        color: #515356;
        line-height: 1;
        margin: 0 -20px;
        background: #EDEDED;
        border-bottom: 1px solid #D9DEE4;
    }
    h3 {
        color: #006568;
        font-weight: 600;
        line-height: 38px;
    }
    h3 small { margin-left: 10px; }
    .headerBar .headerComp {
        float: left;
        height: 59px;
        cursor: pointer;
        padding-right: 10px;
    }
    .headerBar .headerComp:hover { background: #D9DEE4; }
    .headerBar .headerComp .headerIcon {
        float: left;
        color: var(--cor-primaria);
        font-size: 26px;
        margin: 17px 0 0 20px;
    }
    .headerBar .headerComp .headerText {
        float: left;
        font-size: 10px;
        font-weight: bold;
        margin: 18px 0 0 10px;
    }
    .headerBar .headerComp .headerText div:last-child {
        margin-top: 2px;
        text-transform: uppercase;
    }
    .headerBar .headerUser {
        float: right;
        height: 59px;
        cursor: pointer;
        padding-left: 10px;
    }
    .headerBar .headerUser img {
        float: left;
        width: 35px;
        height: 35px;
        border-radius: 50%;
        margin: 12px 10px 0 0;
    }
    .headerBar .headerUser .headerName {
        float: left;
        font-size: 15px;
        margin: 15px 10px 0 0;
    }
    .headerBar .headerUser .headerName div:last-child {
        font-size: 10px;
        margin-top: 3px;
        font-weight: bold;
        text-transform: uppercase;
    }
    .headerBar .headerUser .headerIcon {
        float: left;
        color: #006568;
        font-size: 26px;
        margin: 18px 20px 0 0;
    }
    .headerBar .headerUser:hover,
    .headerBar .headerTask:hover { background: #D9DEE4; }
    .headerBar .headerTask[data-tooltip]:hover:after {
        content: attr(data-tooltip);
        white-space: nowrap;
        position: absolute;
        margin-left: -5px;
        background: #000;
        padding: 4px 8px;
        top: 60px;
        color: #fff;
    }
    .headerBar .headerTask {
        float: right;
        height: 44px;
        cursor: pointer;
        padding: 15px 5px 0;
    }
    .headerBar .headerTask > span {
        width: 30px;
        display: block;
        font-size: 18px;
        background: #FFF;
        text-align: center;
        border-radius: 50%;
    }
    .headerBar .headerTask > span.ico-locked,
    .headerBar .headerTask > span.ico-warning,
    .headerBar .headerTask > span.ico-calendar { height: 24px; padding-top: 6px; }
    .headerBar .headerTask > span.ico-tasks,
    .headerBar .headerTask > span.ico-tags-2,
    .headerBar .headerTask > span.ico-asterisk { height: 23px; padding-top: 7px; }
    .headerBar .headerWind,
    .headerBar .headerMenu,
    .headerBar .headerDrop {
        top: 59px;
        z-index: 100;
        padding: 5px;
        display: none;
        background: #FFF;
        position: absolute;
        border: 1px solid #D9DEE4;
    }
    .headerBar .headerWind { padding: 10px 7px 7px 10px; }
    .headerBar .headerWind span {
        float: left;
        margin-top: 1px;
    }
    .headerBar .headerWind label {
        font-size: 13px;
        font-weight: 600;
        margin: 0 5px 5px 0;
    }
    .headerBar .headerWind select {
        width: 100%;
        padding: 0 5px;
    }
    .headerBar .headerItem {
        padding: 5px;
        font-size: 13px;
        font-weight: 600;
        cursor: auto;
    }
    .headerBar .headerItem:hover { background: #F8F8F8; }
    .headerBar .headerItem > img.myPhot {
        float: left;
        width: 30px;
        height: 30px;
        border-radius: 50%;
    }
    .headerBar .headerItem > span.myIcon {
        float: left;
        width: 30px;
        height: 23px;
        font-size: 18px;
        background: #EEE;
        padding-top: 7px;
        text-align: center;
        border-radius: 50%;
    }
    .headerBar .headerItem > span.myName {
        float: left;
        margin-left: 5px;
        line-height: 30px;
    }
    .headerBar #SubLoja {
        left: 0px;
        padding: 10px;
    }
    .headerBar #SubUser { right: 0px; }
    .headerBar #SubShop,
    .headerBar #SubAniv { margin-left: -5px; }

    <?php echo ":root {
        --cor-primaria:" . $cor_primaria_ . ";
        --cor-secundaria:" . $cor_secundaria_ . ";
        --cor-destaque:" . $cor_destaque_ . ";
    }"; ?>
</style>
<div class="headerBar headerBarMu">
    <div id="BtnLoja" class="headerComp" onClick="mudaLoja()">
        <input id="txtMnContrato" type="hidden" value="<?php echo $contrato; ?>" />        
        <input id="txtMnFilial" type="hidden" value="<?php echo $filial; ?>" />
        <input id="txtMnLanguage" type="hidden" value="<?php echo $languages; ?>" />
        <input id="txtMnDesconDecimal" type="hidden" value="<?php echo $desconto_decimal_; ?>" />
        <div class="headerIcon ico-briefcase"></div>
        <div class="headerText">
            <div><?php echo "EMPRESA: " . $contrato . " - LOJA: " . $filial; ?></div>
            <div><?php echo $_SESSION["razao_social_contrato"]; ?></div>
        </div>
    </div>
    <div id="BtnUser" class="headerUser" onClick="mudaUser()">
        <img src="<?php
        if ($_SESSION["imagem"] != "") {
            echo "./../../Pessoas/" . $contrato . "/" . $_SESSION["imagem"];
        } else {
            echo "./../../img/dmitry_m.gif";
        }
        ?>"/>
        <div class="headerName">
            <div><?php echo $_SESSION["nome_usuario"]; ?></div>
            <div><?php echo $nomeperm_; ?></div>
        </div>
        <div class="headerIcon ico-chevron-right"></div>
    </div>
    <?php if($_SESSION["mod_emp"] != 2) { ?>
    <div class="headerTask" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/login.php'" data-tooltip="Bloquear Sessão">
        <span class="ico-locked"></span>
    </div>
    <?php } if ($ckb_fin_fix_ + $ckb_fin_lsol_ + $ckb_fin_pag_ + $ckb_fin_rec_ > 0 && $_SESSION["mod_emp"] != 1 && $_SESSION["mod_emp"] != 2) { ?>
        <div class="headerTask"  onClick="mudaFinanceiro()" data-tooltip="Financeiro">
            <span class="ico-coins" style="padding: 6px 1px 4px 1px;"></span>
            <div id="SubFinanceiro" class="headerMenu">
                <?php if ($ckb_fin_fix_ == 1) { ?>
                    <div class="headerItem" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/Financeiro/Lancamento-de-Movimentos.php?tp=0'">
                        <span class="myIcon ico-coins"></span>
                        <span class="myName">Contratos</span>
                        <div style="clear:both"></div>
                    </div>
                    <div class="headerItem" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/Financeiro/Lancamento-de-Movimentos.php?tp=1'">
                        <span class="myIcon ico-coins"></span>
                        <span class="myName">Contas Fixas</span>
                        <div style="clear:both"></div>
                    </div>
                <?php } if ($ckb_fin_lsol_ == 1) { ?>
                    <div class="headerItem" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/Contas-a-pagar/Solicitacao-de-Autorizacao.php?id=1'">
                        <span class="myIcon ico-coins"></span>
                        <span class="myName">Contas Variáveis</span>
                        <div style="clear:both"></div>
                    </div>
                <?php } if ($ckb_fin_pag_ == 1) { ?>
                    <div class="headerItem" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/Contas-a-pagar/Contas-a-Pagar.php?id=1&tp=0'">
                        <span class="myIcon ico-coins"></span>
                        <span class="myName">Contas à Pagar</span>
                        <div style="clear:both"></div>
                    </div>
                <?php } if ($ckb_fin_rec_ == 1) { ?>
                    <div class="headerItem" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/Contas-a-pagar/Contas-a-Receber.php?id=1&tp=0'">
                        <span class="myIcon ico-coins"></span>
                        <span class="myName">Contas à Receber</span>
                        <div style="clear:both"></div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <div class="headerTask" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/index.php'" data-tooltip="Pendências">
        <span class="ico-tasks"></span>
    </div>
    <div id="BtnPen" class="headerTask" onClick="mudaPen()" data-tooltip="Autorizações">
        <span class="ico-warning" style="<?php echo (($Orcamentos_Auto + $Vendas_Auto + $Compras_Auto + $Entrada_Saida + $Requisicao_Auto + $Cotacao_Auto + $Solicitacao_autorizacao + $LeadsPendentes + $AssitenciasPendentes + $VeiculosAguardando + $VistoriasPendentes + $SinistrosPendentes + $ProspectsEmAnalise + $Contratos_Auto + $ContasFixas_Auto) > 0 ? "color: #CC4748;" : "");?>"></span>
        <div id="PenShop" class="headerMenu">
            <?php if ($Orcamentos_Auto > 0) { ?>
                <div class="headerItem" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/Estoque/Orcamentos.php?id=1'">
                    <span class="myIcon ico-warning-sign"></span>
                    <span class="myName">Orçamentos (<?php echo $Orcamentos_Auto; ?>)</span>
                    <div style="clear:both"></div>
                </div>
            <?php }if ($Vendas_Auto > 0) { ?>
                <div class="headerItem" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/Estoque/Vendas.php?id=1'">
                    <span class="myIcon ico-warning-sign"></span>
                    <span class="myName">Vendas (<?php echo $Vendas_Auto; ?>)</span>
                    <div style="clear:both"></div>
                </div>
            <?php }if ($Compras_Auto > 0) { ?>
                <div class="headerItem" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/Estoque/Compras.php?id=1'">
                    <span class="myIcon ico-warning-sign"></span>
                    <span class="myName">Compras (<?php echo $Compras_Auto; ?>)</span>
                    <div style="clear:both"></div>
                </div>
            <?php }if ($Entrada_Saida > 0) { ?>
                <div class="headerItem" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/Estoque/Entradas-Saidas.php?id=1'">
                    <span class="myIcon ico-warning-sign"></span>
                    <span class="myName">Entradas/Saídas (<?php echo $Entrada_Saida; ?>)</span>
                    <div style="clear:both"></div>
                </div>
            <?php }if ($Requisicao_Auto > 0) { ?>
                <div class="headerItem" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/Estoque/Requisicoes.php?id=1'">
                    <span class="myIcon ico-warning-sign"></span>
                    <span class="myName">Requisições (<?php echo $Requisicao_Auto; ?>)</span>
                    <div style="clear:both"></div>
                </div>
            <?php }if ($Cotacao_Auto > 0) { ?>
                <div class="headerItem" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/Estoque/Cotacoes.php?id=1'">
                    <span class="myIcon ico-warning-sign"></span>
                    <span class="myName">Cotações (<?php echo $Cotacao_Auto; ?>)</span>
                    <div style="clear:both"></div>
                </div>
            <?php }if ($Solicitacao_autorizacao > 0) { ?>
                <div class="headerItem" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/Contas-a-pagar/Solicitacao-de-Autorizacao.php?id=1'">
                    <span class="myIcon ico-warning-sign"></span>
                    <span class="myName">Contas Variáveis (<?php echo $Solicitacao_autorizacao; ?>)</span>
                    <div style="clear:both"></div>
                </div>
            <?php }if ($Contratos_Auto > 0) { ?>
                <div class="headerItem" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/Financeiro/Lancamento-de-Movimentos.php?tp=0&id=1'">
                    <span class="myIcon ico-warning-sign"></span>
                    <span class="myName">Contratos (<?php echo $Contratos_Auto; ?>)</span>
                    <div style="clear:both"></div>
                </div>
            <?php }if ($ContasFixas_Auto > 0) { ?>
                <div class="headerItem" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/Financeiro/Lancamento-de-Movimentos.php?tp=1&id=1'">
                    <span class="myIcon ico-warning-sign"></span>
                    <span class="myName">Contas Fixas (<?php echo $ContasFixas_Auto; ?>)</span>
                    <div style="clear:both"></div>
                </div>
            <?php }if ($LeadsPendentes > 0) { ?>
                <div class="headerItem" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/Contas-a-pagar/Solicitacao-de-Autorizacao.php?id=1'">
                    <span class="myIcon ico-warning-sign"></span>
                    <span class="myName">Leads (<?php echo $LeadsPendentes; ?>)</span>
                    <div style="clear:both"></div>
                </div>
            <?php }if ($AssitenciasPendentes > 0) { ?>
                <div class="headerItem" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/Seguro/Veiculos.php?id=3'">
                    <span class="myIcon ico-warning-sign"></span>
                    <span class="myName">Pendência (Assist.24h) (<?php echo $AssitenciasPendentes; ?>)</span>
                    <div style="clear:both"></div>
                </div>
            <?php }if ($VeiculosAguardando > 0) { ?>
                <div class="headerItem" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/Seguro/Veiculos.php?id=0'">
                    <span class="myIcon ico-warning-sign"></span>
                    <span class="myName">Aprovação de Veículos (<?php echo $VeiculosAguardando; ?>)</span>
                    <div style="clear:both"></div>
                </div>
            <?php }if ($VistoriasPendentes > 0) { ?>
                <div class="headerItem" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/Seguro/Vistorias.php?id=0'">
                    <span class="myIcon ico-warning-sign"></span>
                    <span class="myName">Vistorias Pendentes (<?php echo $VistoriasPendentes; ?>)</span>
                    <div style="clear:both"></div>
                </div>
            <?php }if ($SinistrosPendentes > 0) { ?>
                <div class="headerItem" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/Seguro/Sinistros.php?id=0'">
                    <span class="myIcon ico-warning-sign"></span>
                    <span class="myName">Eventos Pendentes (<?php echo $SinistrosPendentes; ?>)</span>
                    <div style="clear:both"></div>
                </div>
            <?php }if ($ProspectsEmAnalise > 0) { ?>
                <div class="headerItem" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/CRM/Gerenciador-Prospects.php?id=0'">
                    <span class="myIcon ico-warning-sign"></span>
                    <span class="myName">Prospects em Análise (<?php echo $ProspectsEmAnalise; ?>)</span>
                    <div style="clear:both"></div>
                </div>
            <?php
                }if (($Orcamentos_Auto + $Vendas_Auto + $Compras_Auto + $Solicitacao_autorizacao + $LeadsPendentes + $AssitenciasPendentes + $VistoriasPendentes + $SinistrosPendentes + $ProspectsEmAnalise + $Contratos_Auto + $ContasFixas_Auto) == 0) {
                    echo "<div class=\"headerItem\">Não há itens pendentes.</div>";
                }
            ?>
        </div>
    </div>
    <?php if ($ckb_out_term_ == 1 && $_SESSION["mod_emp"] != 1) { ?>
        <div id="BtnShop" class="headerTask" onClick="mudaShop()" data-tooltip="Terminais">
            <span class="ico-tags-2"></span>
            <div id="SubShop" class="headerMenu">
                <div class="headerItem" onClick="window.open('<?php echo $UrlAtual; ?>/Modulos/POS/Ponto-de-Venda.php?tp=0', '_blank')">
                    <span class="myIcon ico-tags-2"></span>
                    <span class="myName">Terminal de Vendas</span>
                    <div style="clear:both"></div>
                </div>
                <div class="headerItem" onClick="window.open('<?php echo $UrlAtual; ?>/Modulos/POS/Tela-de-Producao.php', '_blank')">
                    <span class="myIcon ico-stack-3"></span>
                    <span class="myName">Terminal de Produção</span>
                    <div style="clear:both"></div>
                </div>
                <div class="headerItem" onClick="window.open('<?php echo $UrlAtual; ?>/Modulos/POS/Tela-de-Senha.php', '_blank')">
                    <span class="myIcon ico-qrcode"></span>
                    <span class="myName">Terminal de Senhas</span>
                    <div style="clear:both"></div>
                </div>
            </div>
        </div>
    <?php } if ($_SESSION["mod_emp"] != 2) { ?>
    <div id="BtnAniv" class="headerTask" onClick="mudaAniv()" data-tooltip="Aniversários">
        <span class="ico-calendar"></span>
        <div id="SubAniv" class="headerDrop" style="padding:0;">
            <div class="headerItem" style="padding: 10px;margin: 0;border-bottom: 1px solid #484646;border-top: 1px solid #484646;background-color:#EDEDED;">
                Funcionários
            </div>
            <?php
            $countAniv = 0;
            $piscaAniv = 0;
            $query = "select * from sf_fornecedores_despesas where tipo = 'E' and dt_demissao is null and data_nascimento is not null and month(data_nascimento) = '" . date("m") . "' order by day(data_nascimento)";
            $cur2 = odbc_exec($con, $query);
            while ($RFP = odbc_fetch_array($cur2)) { ?>
                <div class="headerItem" <?php
                if (escreverData($RFP["data_nascimento"], "d") == date("d")) {
                    echo "style='background:gold'";
                    $piscaAniv = 1;
                }
                ?>>
                    <img class="myPhot" src="<?php
                    if ($RFP["imagem"] != "") {
                        echo "./../../Pessoas/" . $contrato . "/" . $RFP["imagem"];
                    } else {
                        echo "./../../img/dmitry_m.gif";
                    }
                    ?>"/>
                    <span class="myName"><?php echo escreverData($RFP["data_nascimento"], "d") . " - " . utf8_encode($RFP["razao_social"]); ?></span>
                    <div style="clear:both"></div>
                </div>
                <?php $countAniv = $countAniv + 1; 
            } 
            if ($countAniv == 0) {
                echo "<div style=\"padding: 10px 5px;\" class=\"headerItem\">Não há funcionários aniversariantes este mês.</div>";
            }
            $query = "set dateformat dmy;select count(*) as tot from sf_fornecedores_despesas where tipo = 'C' and data_nascimento is not null and month(data_nascimento) = '" . date("m") . "'";
            $cur = odbc_exec($con, $query);
            while ($RFP = odbc_fetch_array($cur)) {
                $row[] = $RFP["tot"];
            }
            $total = $row[0];
            $monthCurrent = date("m");
            $lastday = date('t', strtotime('today'));
            ?>
            <div class="headerItem" <?php if ($ckb_adm_cli_ > 0) { ?>onClick="window.top.location.href = '<?php echo $UrlAtual . '/Modulos/Comercial/Gerenciador-Clientes.php?dti=01/' . $monthCurrent . "&dtf=" . $lastday . "/" . $monthCurrent . "'"; ?>"<?php } ?> style="padding: 10px;margin: 0;border-bottom: 1px solid #484646;border-top: 1px solid #484646;background-color:#EDEDED;cursor: pointer;">
            Clientes - Esse mês são <?php echo $total; ?> aniversariantes.</div>
            <?php
            $countAniv2 = 0;
            $query2 = "set dateformat dmy;select razao_social,data_nascimento, fornecedores_status from sf_fornecedores_despesas where tipo = 'C' and data_nascimento is not null and month(data_nascimento) = '" . date("m") . "' and day(data_nascimento) = '" . date("d") . "' and fornecedores_status like 'Ativo%' order by day(data_nascimento)";
            $cur3 = odbc_exec($con, $query2);
            while ($RFP = odbc_fetch_array($cur3)) { ?>
                <div class="headerItem" <?php
                if (escreverData($RFP["data_nascimento"], "d") == date("d")) {
                    echo "style='background:gold'";
                    $piscaAniv = 1;
                }
                ?>>
                    <img class="myPhot" src="<?php
                    if ($RFP["imagem"] != "") {
                        echo "./../../Pessoas/" . $contrato . "/" . $RFP["imagem"];
                    } else {
                        echo "./../../img/dmitry_m.gif";
                    }
                    ?>"/>
                    <span class="myName"><?php echo escreverData($RFP["data_nascimento"], "d") . " - " . utf8_encode($RFP["razao_social"]); ?></span>
                    <div style="clear:both"></div>
                </div>
                <?php $countAniv2 = $countAniv2 + 1; ?>
            <?php } ?>
            <?php
            if ($countAniv2 == 0) {
                echo "<div style=\"padding: 10px 5px;\" class=\"headerItem\">Não há clientes aniversariantes este dia.</div>";
            }
            ?>
        </div>
    </div>
    <?php } ?>    
    <div id="SubUser" class="headerWind">
        <button class="btn yellow" style="width:158px; margin-bottom: 2px;" type="button" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/Modulos/Comercial/MudarSenha.php'">
            <span class="ico-user"></span> Alterar Dados da Conta
        </button>
        <br>
        <button class="btn red" style="width:90px" type="button" onClick="window.top.location.href = '<?php echo $UrlAtual; ?>/login.php'">
            <span class="ico-locked"></span> Bloquear
        </button>
        <button class="btn green" style="width:65px" type="button" onClick="window.top.location.href = '<?php echo $UrlAtual . '/login.php?ex=1&emp=' . $contrato; ?>'">
            <span class="ico-off"></span> Sair
        </button>
    </div>
    <div id="SubLoja" class="headerWind">
        <label>Selecione a Loja: </label>
        <select name="txtLojaSel" id="txtLojaSel">
            <?php
            $filial = "";
            $filial_grupo_filter = "";
            $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
            while ($RFP = odbc_fetch_array($cur)) { ?>
                <option value="<?php echo utf8_encode($RFP['id_filial']); ?>" <?php
                if ($RFP['id_filial'] == $RFP['filial']) {
                    echo "SELECTED";
                    $filial = $RFP["id_filial"];
                    $filial_grupo_filter = $RFP["id_grupo"];
                }
                ?>><?php echo utf8_encode($RFP['numero_filial']) . " - " . utf8_encode($RFP['Descricao']); ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#SubLoja").css("width", $("#BtnLoja").width() - 6);
    });
    function mudaLoja() {
        if ($("#SubLoja").is(":visible")) {
            $("#SubLoja").fadeOut();
            $("#BtnLoja").css("background", "");
        } else {
            $("#SubLoja").fadeIn();
            $("#BtnLoja").css("background", "#D9DEE4");
            $("#SubShop, #SubUser, #SubMenu, #SubAniv").fadeOut();
            $("#BtnShop, #BtnUser, #BtnMenu, #BtnAniv").css("background", "");
        }
    }
    function mudaShop() {
        if ($("#SubShop").is(":visible")) {
            $("#SubShop").fadeOut();
            $("#BtnShop").css("background", "");
        } else {
            $("#SubShop").fadeIn();
            $("#BtnShop").css("background", "#D9DEE4");
            $("#SubLoja, #SubUser, #SubMenu, #SubAniv").fadeOut();
            $("#BtnLoja, #BtnUser, #BtnMenu, #BtnAniv").css("background", "");
        }
    }
    function mudaPen() {
        if ($("#PenShop").is(":visible")) {
            $("#PenShop").fadeOut();
            $("#BtnPen").css("background", "");
        } else {
            $("#PenShop").fadeIn();
            $("#BtnPen").css("background", "#D9DEE4");
            $("#SubLoja, #SubUser, #SubMenu, #SubAniv").fadeOut();
            $("#BtnLoja, #BtnUser, #BtnMenu, #BtnAniv").css("background", "");
        }
    }
    function mudaAniv() {
        if ($("#SubAniv").is(":visible")) {
            $("#SubAniv").fadeOut();
            $("#BtnAniv").css("background", "");
        } else {
            $("#SubAniv").fadeIn();
            $("#BtnAniv").css("background", "#D9DEE4");
            $("#SubLoja, #SubUser, #SubMenu, #SubShop").fadeOut();
            $("#BtnLoja, #BtnUser, #BtnMenu, #BtnShop").css("background", "");
        }
    }
    function mudaUser() {
        if ($("#SubUser").is(":visible")) {
            $("#SubUser").fadeOut();
            $("#BtnUser").css("background", "");
        } else {
            $("#SubUser").fadeIn();
            $("#BtnUser").css("background", "#D9DEE4");
            $("#SubLoja, #SubMenu, #SubShop, #SubAniv").fadeOut();
            $("#BtnLoja, #BtnMenu, #BtnShop, #BtnAniv").css("background", "");
        }
    }

    function mudaFinanceiro() {
        if ($("#SubFinanceiro").is(":visible")) {
            $("#SubFinanceiro").fadeOut();
            $("#BtnFinanceiro").css("background", "");
        } else {
            $("#SubFinanceiro").fadeIn();
            $("#BtnFinanceiro").css("background", "#D9DEE4");
            $("#SubLoja, #SubShop, #SubUser, #SubMenu, #SubAniv").fadeOut();
            $("#BtnLoja, #BtnShop, #BtnUser, #BtnMenu, #BtnAniv").css("background", "");
        }
    }

    $(function () {
        $('#txtLojaSel').change(function () {
            if ($(this).val()) {
                $.getJSON("<?php echo $UrlAtual; ?>/Connections/trocar.loja.php", {loc: $(this).val(), usr:<?php echo $_SESSION["id_usuario"]; ?>, ajax: "true"}, function (j) {});
            }
            window.location = '<?php echo $UrlAtual . $_SERVER["REQUEST_URI"]; ?>';
        });        
        function pulsate(element) {
            $(element || this).fadeOut(500).fadeIn(500, pulsate);
        }
<?php if ($piscaAniv > 0) { ?>
            pulsate($(".ico-calendar")); //Aniversariantes Piscando
<?php } if ($tarefas > 0) { ?>
            pulsate($(".ico-tasks")); //BI Piscando
<?php } if (($Orcamentos_Auto + $Vendas_Auto + $Compras_Auto + $Solicitacao_autorizacao + $LeadsPendentes + $Contratos_Auto + $ContasFixas_Auto + $AssitenciasPendentes) > 0) { ?>
            pulsate($(".ico-warning")); //Autorizações Piscando
<?php } ?>
    });
    if (localStorage.getItem('piscar') === undefined) {
        $.post("/Modulos/Sivis/ajax/update_server_processing.php", {op: "news"})
        .done(function (data) {
            if (data.trim() === "YES") {
                $(".ico-info-sign").addClass("piscar");
                localStorage.setItem('piscar', 'true');
            } else {
                localStorage.setItem('piscar', 'false');
            }

        });
    } else {
        if (localStorage.getItem('piscar') === 'true') {
            $(".ico-info-sign").addClass("piscar");
        }
    }

</script>