<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

$request_body = file_get_contents('php://input');
$data = json_decode($request_body);

$id = "";
$contrato = $data->contrato;
$hostname = "148.72.177.101,6000";
$database = "erp" . $contrato;
$username = "erp" . $contrato;
$password = "!Password123";
$con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
include("./../../../Connections/funcoesAux.php");

if (!isset($data->functionPage)) {
    echo "Função não definida para este evento! 3.0";
    exit;
}

/*if ((!isset($data->userSession->id) || !is_numeric($data->userSession->id) || $data->userSession->id == 0) 
    //&& ($data->functionPage != "signup" && $data->functionPage != "register") -> antigo
    //&& (!isset($data->idVistoria) || !is_numeric($data->idVistoria)) -> novo
    ) {
    echo "Sessão inválida!";
    exit;
}*/

if ($data->functionPage == "config") {
    $row = array();    
    $sQuery = "select cor_primaria, cor_secundaria, cor_destaque, adm_doc_assinatura,
    (select nome_fantasia_contrato from sf_filiais where id_filial = 1) nome,
    (select cnpj from sf_filiais where id_filial = 1) cnpj,
    (select telefone from sf_filiais where id_filial = 1) telefone
    from sf_configuracao";
    //echo $sQuery; exit;
    $cur = odbc_exec($con, $sQuery);
    while ($RFP = odbc_fetch_array($cur)) {
        $row["cor_primaria"] = utf8_encode($RFP['cor_primaria']);
        $row["cor_secundaria"] = utf8_encode($RFP['cor_secundaria']);
        $row["cor_destaque"] = utf8_encode($RFP['cor_destaque']);
        $row["nome"] = utf8_encode($RFP['nome']);
        $row["cnpj"] = utf8_encode($RFP['cnpj']);
        $row["telefone"] = utf8_encode($RFP['telefone']);
        $row["logo"] = (file_exists(__DIR__ . "./../../../Pessoas/" . $contrato . "/Empresa/Whatsapp/logo.png") ? "Pessoas/" . $contrato . "/Empresa/Whatsapp/logo.png" : "");
        $row["ativo"] = utf8_encode($RFP['adm_doc_assinatura']);        
    }   
    $query = "select descricao, assinatura, marcar, id_contrato_individual 
    from sf_contratos where inativo = 0 and id = " . valoresNumericos2($data->id_contrato);
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row["descricao"] = utf8_encode($RFP['descricao']);        
        $row["assinatura"] = utf8_encode($RFP['assinatura']);        
        $row["marcar"] = utf8_encode($RFP['marcar']);        
        $row["id_contrato_individual"] = valoresNumericos2($RFP['id_contrato_individual']);
    }
    echo json_encode($row);
}

if ($data->functionPage == "signup") {
    $img = "";
    if (isset($data->username) && isset($data->password)) {
        $query = "select top 1 id_fornecedores_despesas, razao_social,
        (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = sf_Fornecedores_despesas.id_fornecedores_despesas) 'email',
        case when tipo = 'E' then 0 when tipo = 'C' then 1 else 2 end prioridade
        from sf_fornecedores_despesas where replace(replace(replace(cnpj,'.',''),'-',''),'/','') = " . str_replace(".", "", str_replace("-", "", valoresTexto2($data->password))) . " and tipo in ('C','P','E') and id_fornecedores_despesas in 
        (select fornecedores_despesas from sf_fornecedores_despesas_contatos where tipo_contato = 2 and conteudo_contato = " . valoresTexto2($data->username) . ") order by prioridade";
        $cur = odbc_exec($con, $query);
        while ($RFP = odbc_fetch_array($cur)) {
            $row = array();
            $id = utf8_encode($RFP['id_fornecedores_despesas']);
            if (file_exists(__DIR__ . "./../../../Pessoas/" . $contrato . "/Clientes/" . $id . '.png')) {
                $path = __DIR__ . "./../../../Pessoas/" . $contrato . "/Clientes/" . $id . ".png";
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($path);
                $img = 'data:image/' . $type . ';base64,' . base64_encode($data);
            }
            $row["id"] = $id;
            $row["name"] = utf8_encode($RFP['razao_social']);
            $row["email"] = utf8_encode($RFP['email']);
            $row["img"] = $img;
        }
    }
    if (is_numeric($id)) {
        echo json_encode($row);
    } else {
        echo "Erro, Usuário ou Senha Inválido!";
    }
}

if ($data->functionPage == "vistoriaList") {
    $toReturn = array();
    $aColumns = array('sf_vistorias.id', 'data', 'sf_vistorias_tipo.descricao', 'placa', 'marca', 'modelo', 'ano_modelo', 
    'combustivel', 'razao_social', 'cnpj', 'sf_vistorias.data_aprov', 'sf_vistorias.obs');
    $sTable = "sf_vistorias 
    inner join sf_fornecedores_despesas_veiculo on id_veiculo = sf_fornecedores_despesas_veiculo.id
    inner join sf_fornecedores_despesas on sf_fornecedores_despesas.id_fornecedores_despesas = sf_fornecedores_despesas_veiculo.id_fornecedores_despesas
    inner join sf_vistorias_tipo on sf_vistorias_tipo.id = sf_vistorias.tipo
    left join sf_telemarketing on id_telemarketing = id_tele";
    $sWhere = " WHERE sf_vistorias.id > 0 ";
    $sOrder = "ORDER BY 1 desc";
    $sLimit = "";

    if ($data->dt_ini != "" && $data->dt_fim != "") {
        $sWhere .= " AND data BETWEEN '" . $data->dt_ini . " 00:00:00' AND '" . $data->dt_fim . " 23:59:59'";
    }

    if (isset($data->page) && $data->page != '-1') {
        $total = 20;
        $sLimit = " OFFSET " . ($total * $data->page) . " ROWS FETCH NEXT " . (($total * $data->page) + ($total)) . " ROWS ONLY";
    }

    if (isset($data->tipo)) {
        if ($data->tipo == 0) {
            $sWhere .= " AND sf_vistorias.data_aprov is not null ";
        } else if ($data->tipo == 1) {
            $sWhere .= " AND sf_vistorias.data_aprov is null ";            
        }
    }
    
    if(isset($data->search) && $data->search) {
        $sWhere .= " AND replace(sf_fornecedores_despesas_veiculo.placa,'-','') like '%" . str_replace("-", "",$data->search). "%'";
    }
    
    if (isset($data->idVistoria) && is_numeric($data->idVistoria)) {
        $sWhere .= " AND sf_vistorias.id = " . $data->idVistoria;
    }
    
    if (isset($data->userSession->id)) {
        $sWhere .= " AND sf_fornecedores_despesas.tipo in ('C','P') AND ((sf_fornecedores_despesas.id_fornecedores_despesas = " . $data->userSession->id . ")" .
        " or (id_user_resp in (select id_usuario from sf_usuarios where funcionario = " . $data->userSession->id . "))" . 
        " or (de_pessoa in (select id_usuario from sf_usuarios where funcionario = " . $data->userSession->id . ")))";
    }
    
    for ($i = 0; $i < count($aColumns); $i++) {
        if ($i == 0) {
            $colunas = $aColumns[$i];
        } else {
            $colunas = $colunas . "," . $aColumns[$i];
        }
    }

    $sQuery = "SELECT $colunas FROM $sTable $sWhere $sOrder $sLimit";
    //echo $sQuery; exit;
    $cur = odbc_exec($con, $sQuery);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["id"] = $RFP["id"];
        $row["data"] = $RFP[$aColumns[1]];
        $row["tipo"] = utf8_encode($RFP["descricao"]);
        $row["placa"] = utf8_encode($RFP[$aColumns[3]]);
        $row["marca"] = utf8_encode($RFP[$aColumns[4]]);
        $row["modelo"] = utf8_encode($RFP[$aColumns[5]]);
        $row["ano_modelo"] = utf8_encode($RFP[$aColumns[6]]);
        $row["combustivel"] = utf8_encode($RFP[$aColumns[7]]);
        $row["razao_social"] = utf8_encode($RFP[$aColumns[8]]);
        $row["cnpj"] = utf8_encode($RFP[$aColumns[9]]);
        $row["data_aprov"] = utf8_encode($RFP["data_aprov"]);
        $row["observacao"] = utf8_encode($RFP["obs"]);
        $row["vistoriaEtapas"] = getEtapas($RFP["id"], $contrato, $con);
        $toReturn[] = $row;
    }
    echo json_encode($toReturn);
}

if ($data->functionPage == "acessoriosList") {
    $listItens = [];    
    $cur = odbc_exec($con, "select id, descricao, id_vistoria from sf_fornecedores_despesas_veiculo_item
    left join sf_vistorias_itens on sf_vistorias_itens.id_item = sf_fornecedores_despesas_veiculo_item.id 
    and id_vistoria = " . $data->idVistoria . " where tipo in (select v.tipo_veiculo from sf_vistorias vi
    inner join sf_fornecedores_despesas_veiculo v on vi.id_veiculo = v.id where vi.id = " . $data->idVistoria . ")");
    while ($RFP = odbc_fetch_array($cur)) {
        $listItens[] = array('id' => $RFP['id'],
        'id' => utf8_encode($RFP['id']),
        'name' => utf8_encode($RFP['descricao']),
        'check' => (is_numeric($RFP['id_vistoria']) ? 1 : 0));
    }
    echo json_encode($listItens);
}

if ($data->functionPage == "avariasList") {
    $toReturn = array();
    $sQuery = "SELECT id, id_vistoria, observacao FROM sf_vistorias_danos WHERE id_vistoria = " . $data->idVistoria;
    //echo $sQuery; exit;
    $cur = odbc_exec($con, $sQuery);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["id"] = $RFP["id"];
        $row["imagem"] = getImageAvaria($RFP["id"], $RFP["id_vistoria"], $contrato);
        $row["observacao"] = utf8_encode($RFP["observacao"]);
        $toReturn[] = $row;
    }
    echo json_encode($toReturn);
}

if ($data->functionPage == "observacaoSave") {
    odbc_exec($con, "update sf_vistorias set obs = " . valoresTexto2($data->observacao) . " where id = " . $data->idVistoria) or die(odbc_errormsg());
    echo "OK";
}

if ($data->functionPage == "acessoriosSave") {  
    $notIn = '0';
    $id = $data->idVistoria;    
    foreach ($data->itens as $item) {
        if ($item->check == "1") {
            $notIn .= "," . $item->id;
        }
    }
    odbc_exec($con, "delete from sf_vistorias_itens where id_vistoria = " . $id . " and id_item not in (" . $notIn . ")");    
    foreach ($data->itens as $item) {
        if ($item->check == "1") {
            odbc_exec($con, "IF NOT EXISTS (SELECT id_vistoria, id_item FROM sf_vistorias_itens WHERE id_vistoria = " . $id . " AND id_item = " . $item->id . ")
            BEGIN INSERT INTO sf_vistorias_itens(id_vistoria, id_item) VALUES (" . $id . "," . $item->id . "); END	
            ELSE BEGIN UPDATE sf_vistorias_itens SET id_vistoria = " . $id . ", id_item = " . $item->id . " WHERE id_vistoria = " . $id . " AND id_item = " . $item->id . "; END");            
        }
    }
    echo "OK";
}

if ($data->functionPage == "avariasSave") {
    odbc_exec($con, "insert into sf_vistorias_danos (id_vistoria, observacao) values (" . $data->idVistoria . "," . 
    valoresTexto2($data->observacao) . ")") or die(odbc_errormsg());
    $res = odbc_exec($con, "select top 1 id from sf_vistorias_danos order by id desc") or die(odbc_errormsg());
    $id = odbc_result($res, 1);
    saveBase64ToImageAvaria($data->cache, $id, $contrato, $data->idVistoria);    
    $row = array();
    $row["id"] = $id;
    $row["imagem"] = getImageAvaria($id, $data->idVistoria, $contrato);
    $row["observacao"] = $data->observacao;
    echo json_encode($row);
}

if ($data->functionPage == "vistoriaSave") {
    foreach ($data->vistoriaEtapas as $etapa) {
        foreach ($etapa->imagens as $imagem) {
            if (strlen($imagem->cache) > 0) {
                if ($imagem->tipo == "imagem") {
                    saveBase64ToImage($imagem->cache, $imagem->id, $contrato, $data->id);
                } else if ($imagem->tipo == "video") {
                    saveBase64ToVideo($imagem->cache, $imagem->id, $contrato, $data->id);
                } else if ($imagem->tipo == "button") {
                    $toSave = "";
                    if (isset($data->dt_ini)) {
                        $toSave .= ",data = '" . $data->dt_ini . "'";
                    }                    
                    if (isset($data->userSession->id)) {
                        $cur = odbc_exec($con, "select top 1 login_user from sf_usuarios where funcionario = " . $data->userSession->id);
                        while ($RFP = odbc_fetch_array($cur)) {
                            $toSave .= ",sys_login = '" . $RFP["login_user"] . "'";
                        }                    
                    }
                    if (isset($imagem->latitude) && isset($imagem->longitude)) {
                        $toSave .= ",latitude = '" . $imagem->latitude . "',longitude = '" . $imagem->longitude . "'";
                    }
                    $query = "set dateformat dmy;
                    UPDATE sf_vistorias SET data_aprov = '" . $imagem->cache . "' " . $toSave . " WHERE id = " . $data->id;
                    odbc_exec($con, $query) or die(odbc_errormsg());
                }
            }
        }
    }
    $row = array();
    $row["id"] = $data->id;
    $row["data"] = $data->data;
    $row["tipo"] = $data->tipo;
    $row["placa"] = $data->placa;
    $row["marca"] = $data->marca;
    $row["modelo"] = $data->modelo;
    $row["ano_modelo"] = $data->ano_modelo;
    $row["combustivel"] = $data->combustivel;
    $row["razao_social"] = $data->razao_social;
    $row["cnpj"] = $data->cnpj;
    $row["data_aprov"] = $data->data_aprov;
    $row["vistoriaEtapas"] = getEtapas($data->id, $contrato, $con);
    echo json_encode($row);
}

function getEtapas($id, $contrato, $con) {
    $i = 0; $j = 0;
    $etapas = [];   
    $etapaTemp = "";
    $cur = odbc_exec($con, "select sf_vistorias_tipo_item.* from sf_vistorias_tipo_item
    inner join sf_vistorias_tipo on sf_vistorias_tipo.id = sf_vistorias_tipo_item.id_vistoria_tipo
    inner join sf_vistorias on sf_vistorias_tipo.id = sf_vistorias.tipo
    where sf_vistorias.id = " . $id . " order by sf_vistorias_tipo_item.etapa, sf_vistorias_tipo_item.id");
    while ($RFP = odbc_fetch_array($cur)) {
        if ($etapaTemp == $RFP["etapa"]) {
            $etapas[$i - 1]["imagens"][] = array("id" => str_pad($j, 2, "0", STR_PAD_LEFT), "nome" => utf8_encode($RFP["descricao"]), "tipo" => utf8_encode($RFP["tipo"]), "cache" => "", 
            "imagem" => ($RFP["tipo"] == "imagem" ? getImage($id, $contrato, str_pad($j, 2, "0", STR_PAD_LEFT), $RFP["imagem"]) : ($RFP["tipo"] == "video" ? getVideo($id, $contrato, str_pad($j, 2, "0", STR_PAD_LEFT), $RFP["imagem"]) : "/Modulos/Seguro/Api/img/" . $RFP["imagem"])),
            "observacao" => utf8_encode($RFP["observacao"]));
        } else {
            $etapas[$i] = array("title" => "Etapa " . $RFP["etapa"], "imagens" => []);
            $etapas[$i]["imagens"][] = array("id" => str_pad($j, 2, "0", STR_PAD_LEFT), "nome" => utf8_encode($RFP["descricao"]), "tipo" => utf8_encode($RFP["tipo"]), "cache" => "", 
            "imagem" => ($RFP["tipo"] == "imagem" ? getImage($id, $contrato, str_pad($j, 2, "0", STR_PAD_LEFT), $RFP["imagem"]) : ($RFP["tipo"] == "video" ? getVideo($id, $contrato, str_pad($j, 2, "0", STR_PAD_LEFT), $RFP["imagem"]) : "/Modulos/Seguro/Api/img/" . $RFP["imagem"])),
            "observacao" => utf8_encode($RFP["observacao"]));
            $i++;            
        }
        $etapaTemp = $RFP["etapa"];        
        $j++;        
    }
    return $etapas;        
}

function getImage($vistoria, $contrato, $id, $default) {
    if (file_exists(__DIR__ . "./../../../Pessoas/" . $contrato . "/Vistorias/" . $vistoria . "/" . $vistoria . "_img" . $id . ".jpeg")) {
        $data = date("d_m_Y_H_i_s", filemtime(__DIR__ . "./../../../Pessoas/" . $contrato . "/Vistorias/" . $vistoria . "/" . $vistoria . "_img" . $id . ".jpeg"));        
        return "/util/timthumb.php?src=./../Pessoas/" . $contrato . "/Vistorias/" . $vistoria . "/" . $vistoria . "_img" . $id . ".jpeg&w=150&dt=" . $data;
    } else {
        return "/Modulos/Seguro/Api/img/" . $default;
    }
}

function getImageAvaria($id, $vistoria, $contrato) {
    if (file_exists(__DIR__ . "./../../../Pessoas/" . $contrato . "/Danos/" . $vistoria . "/" . $id . ".jpeg")) {
        $data = date("d_m_Y_H_i_s", filemtime(__DIR__ . "./../../../Pessoas/" . $contrato . "/Danos/" . $vistoria . "/" . $id . ".jpeg"));        
        return "/util/timthumb.php?src=./../Pessoas/" . $contrato . "/Danos/" . $vistoria . "/" . $id . ".jpeg&w=150&dt=" . $data;
    } else if (file_exists(__DIR__ . "./../../../Pessoas/" . $contrato . "/Danos/" . $vistoria . "/" . $id . ".jpg")) {
        $data = date("d_m_Y_H_i_s", filemtime(__DIR__ . "./../../../Pessoas/" . $contrato . "/Danos/" . $vistoria . "/" . $id . ".jpg"));        
        return "/util/timthumb.php?src=./../Pessoas/" . $contrato . "/Danos/" . $vistoria . "/" . $id . ".jpg&w=150&dt=" . $data;
    } else if (file_exists(__DIR__ . "./../../../Pessoas/" . $contrato . "/Danos/" . $vistoria . "/" . $id . ".png")) {
        $data = date("d_m_Y_H_i_s", filemtime(__DIR__ . "./../../../Pessoas/" . $contrato . "/Danos/" . $vistoria . "/" . $id . ".png"));        
        return "/util/timthumb.php?src=./../Pessoas/" . $contrato . "/Danos/" . $vistoria . "/" . $id . ".png&w=150&dt=" . $data;
    } else {
        return "/img/noimage.png";
    }
}

function getVideo($vistoria, $contrato, $id, $default) {
    if (file_exists(__DIR__ . "./../../../Pessoas/" . $contrato . "/Vistorias/" . $vistoria . "/" . $vistoria . "_img" . $id . ".mp4")) {        
        return "/Pessoas/" . $contrato . "/Vistorias/" . $vistoria . "/" . $vistoria . "_img" . $id . ".mp4";
    } else if (file_exists(__DIR__ . "./../../../Pessoas/" . $contrato . "/Vistorias/" . $vistoria . "/" . $vistoria . "_img" . $id . ".3gp")) {        
        return "/Pessoas/" . $contrato . "/Vistorias/" . $vistoria . "/" . $vistoria . "_img" . $id . ".3gp";
    } else {
        return "/Modulos/Seguro/Api/img/" . $default;
    }
}

function saveBase64ToImage($image, $id, $contrato, $vistoria) {
    if (is_numeric($id)) {
        if (!file_exists(__DIR__ . "./../../../Pessoas/" . $contrato . "/Vistorias/" . $vistoria)) {
            mkdir(__DIR__ . "./../../../Pessoas/" . $contrato . "/Vistorias/" . $vistoria, 0777, true);
        }
        $file = $vistoria . "_img" . $id . ".jpeg";
        $data = base64_decode(str_replace(' ', '+', str_replace('data:image/jpeg;base64,', '', $image)));
        $success = file_put_contents(__DIR__ . "./../../../Pessoas/" . $contrato . "/Vistorias/" . $vistoria . '/' . $file, $data);
        return $success ? $file : '';
    }
}

function saveBase64ToImageAvaria($image, $id, $contrato, $vistoria) {
    if (is_numeric($id)) {
        if (!file_exists(__DIR__ . "./../../../Pessoas/" . $contrato . "/Danos/" . $vistoria)) {
            mkdir(__DIR__ . "./../../../Pessoas/" . $contrato . "/Danos/" . $vistoria, 0777, true);
        }
        $file = $id . ".jpeg";
        $data = base64_decode(str_replace(' ', '+', str_replace(array('data:image/jpeg;base64,','data:image/png;base64,'), '', $image)));
        $success = file_put_contents(__DIR__ . "./../../../Pessoas/" . $contrato . "/Danos/" . $vistoria . '/' . $file, $data);
        return $success ? $file : '';
    }
}

function saveBase64ToVideo($image, $id, $contrato, $vistoria) {
    if (is_numeric($id)) {
        if (!file_exists(__DIR__ . "./../../../Pessoas/" . $contrato . "/Vistorias/" . $vistoria)) {
            mkdir(__DIR__ . "./../../../Pessoas/" . $contrato . "/Vistorias/" . $vistoria, 0777, true);
        }                 
        $file = $vistoria . "_img" . $id . "." . (strpos($image, "data:video/3gpp") !== false ? "3gp" : "mp4");
        $data = base64_decode(str_replace(' ', '+', str_replace(array('data:video/mp4;base64,', 'data:video/mp4;;base64,', 'data:video/3gpp;base64,'), '', $image)));
        $success = file_put_contents(__DIR__ . "./../../../Pessoas/" . $contrato . "/Vistorias/" . $vistoria . '/' . $file, $data);
        return $success ? $file : '';
    }
}

odbc_close($con);