<?php
include '../../Connections/configini.php';
if (is_numeric($_GET['Del'])) {
    $totalPlano = 0;
    $cur = odbc_exec($con, "select count(id_plano) total from sf_vendas_planos where id_veiculo = " . $_GET['Del']);
    while ($RFP = odbc_fetch_array($cur)) {
        $totalPlano = $RFP['total'];
    }
    if ($totalPlano > 0) {
        echo "<script>alert('Não é possível excluir um veículo com um plano relacionado!');</script>";
    } else {   
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        select 'sf_fornecedores_despesas_veiculo', " . $_GET['Del'] . ", '" . $_SESSION["login_usuario"] . "', 'D', 'EXCLUSAO DE VEICULO: ' + placa, GETDATE(), id_fornecedores_despesas from sf_fornecedores_despesas_veiculo where id = " . $_GET['Del'] . ";") or die(odbc_errormsg());        
        odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_veiculo WHERE id = " . $_GET['Del']) or die(odbc_errormsg());
        echo "<script>window.top.location.href = 'Veiculos.php'; </script>";
    }
}

$totEquipe = 0;
$cur = odbc_exec($con, "select count(sf_usuarios_dependentes.id_usuario) total from sf_usuarios_dependentes 
inner join sf_usuarios on id_fornecedor_despesas = funcionario
where sf_usuarios.id_usuario = " . $_SESSION["id_usuario"]) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $totEquipe = $RFP['total'];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>        
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>        
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Proteção<small>Veículos</small></h1>
                    </div>
                    <input id="txtFilter" name="txtFilter" type="hidden" value=""/>
                    <div class="topcont" style="clear: both;display:flex; margin-bottom: 10px;">
                        <div class="tophead" style="width:calc(25% - 1px); border:0;">
                            <a href="javascript:void(0)" onclick="selectStatus('0')">
                            <div class="topname">Comprovante de Pagamento</div>
                            <div class="topresp">
                                <div id="lbl_pagamento" class="topnumb">0</div>
                                <div style="clear:both"></div>
                            </div></a>
                        </div>
                        <div class="tophead" style="width:calc(25% - 1px)">
                            <a href="javascript:void(0)" onclick="selectStatus('1')">
                            <div class="topname">Comprovante de Residência</div>
                            <div class="topresp">
                                <div id="lbl_residencia" class="topnumb">0</div>
                                <div style="clear:both"></div>
                            </div></a>
                        </div>
                        <div class="tophead" style="width:calc(25% - 1px)">
                            <a href="javascript:void(0)" onclick="selectStatus('2')">
                            <div class="topname">Comprovante de Habilitação</div>
                            <div class="topresp">
                                <div id="lbl_habilitacao" class="topnumb">0</div>
                                <div style="clear:both"></div>
                            </div></a>
                        </div>
                        <div class="tophead" style="width:calc(25% - 1px)">
                            <a href="javascript:void(0)" onclick="selectStatus('3')">
                            <div class="topname">Assinatura de Contrato</div>
                            <div class="topresp">
                                <div id="lbl_contrato" class="topnumb">0</div>
                                <div style="clear:both"></div>
                            </div></a>
                        </div>
                        <div style="clear:both"></div>
                    </div>                                        
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">                               
                                <div style="display: flex; justify-content: space-between;; width: 100%;">
                                    <div style="min-width: 210px; margin-top: 15px;">
                                        <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir('I')" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                        <button id="btnExcel" class="button button-blue btn-primary" type="button" onclick="imprimir('E')" title="Exportar Excel"><span class="ico-download-3"></span></button>                                    
                                        <button id="btnEmail" class="button button-turquoise btn-primary" type="button" onclick="AbrirBox(5);"><span class="ico-envelope-3 icon-white"></span></button>
                                        <button type="button" class="button button-blue btn-primary" onclick="AbrirBox(6);"><span class="ico-comment icon-white"></span></button>
                                        <?php if ($mdl_wha_ > 0) { ?>
                                        <button type="button" class="button button-blue btn-primary" onclick="AbrirBox(7);"><span class="ico-phone-4 icon-white"></span></button>
                                        <?php } ?>                                
                                    </div>                                     
                                    <div>
                                        <label>Selecione a Filial:</label>
                                        <select id="txtFilial" name="txtFilial[]" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                            <?php $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo utf8_encode($RFP['id_filial']); ?>" selected><?php echo utf8_encode($RFP['numero_filial']) . " - " . utf8_encode($RFP['Descricao']); ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>                                    
                                    <div style="margin-left: 0.5%;">
                                        <span>Tipo:</span>
                                        <select style="width: 100%" name="txtTipoCli" id="txtTipoCli">
                                            <option value="null">Todos</option>                 
                                            <option value="P">Prospects</option>
                                            <option value="C" <?php echo (!isset($_GET["id"]) ? "selected" : "")?>>Clientes</option>
                                        </select>                                 
                                    </div>                                     
                                    <div style="margin-left: 0.5%;">
                                        <span>Assist. 24h:</span>
                                        <select style="width: 100%" name="txtAssistencia" id="txtAssistencia">
                                            <option value="null">Selec.</option>                 
                                            <option value="0" <?php echo (isset($_GET["id"]) && in_array($_GET["id"], ["0"]) ? "selected" : "")?>>Com</option>
                                            <option value="1" <?php echo (isset($_GET["id"]) && in_array($_GET["id"], ["3"]) ? "selected" : "")?>>Sem</option>
                                        </select>                                 
                                    </div>                                     
                                    <div style="margin-left: 0.5%;">
                                        <span>Rastreador:</span>
                                        <select style="width: 100%" name="txtRastreador" id="txtRastreador">
                                            <option value="null">Selec.</option>                 
                                            <option value="0">Com</option>
                                            <option value="1">Sem</option>
                                        </select>                                 
                                    </div>                                     
                                    <div style="margin-left: 0.5%; max-width: 150px;">
                                        <span>St.Plano:</span>
                                        <select style="width: 100%" name="txtStatus[]" id="txtStatus" multiple="multiple" class="select">
                                            <option value="Ativo" <?php echo (!isset($_GET["id"]) || in_array($_GET["id"], ["0", "3"]) ? "selected" : "")?>>Ativo</option>
                                            <option value="Inativo">Inativo</option>
                                            <option value="Suspenso">Suspenso</option>
                                            <option value="Novo">Novo</option>
                                            <option value="Cancelado">Cancelado</option>
                                        </select>
                                    </div>                         
                                    <div style="margin-left: 0.5%;">
                                        <span>St.Vistorias:</span>
                                        <select style="width: 100%" name="txtVistorias" id="txtVistorias">
                                            <option value="null">Selecione</option>                                                                
                                            <option value="0" <?php echo (!isset($_GET["id"]) ? "selected" : "")?>>Concluídas</option>                                               
                                            <option value="1">Pendentes</option>                                                                                               
                                            <option value="2">Sem Vistoria</option>                                            
                                        </select>                                 
                                    </div>                         
                                    <div style="margin-left: 0.5%;">
                                        <span>St.Veículos:</span>
                                        <select style="width: 100%" name="txtStatusAp[]" id="txtStatusAp" multiple="multiple" class="select">
                                            <option value="3" <?php echo (isset($_GET["id"]) && in_array($_GET["id"], ["0", "3"]) ? "selected" : "")?>>Pendente</option>                                                
                                            <option value="0" <?php echo (isset($_GET["id"]) && in_array($_GET["id"], ["0", "3"]) ? "selected" : "")?>>Aguardando</option>                                                
                                            <option value="1">Aprovados</option>                                               
                                            <option value="2">Reprovados</option>                                                                                               
                                        </select>                                 
                                    </div>
                                    <div style="margin-left: 0.5%;">
                                        <span>Categoria:</span>
                                        <select style="width:100%" name="txtCategoria" id="txtCategoria">
                                            <option value="null">Selecione</option>
                                            <option value="1">Particular</option>
                                            <option value="2">Aluguel</option>
                                            <option value="3">Aplicativo</option>
                                        </select>
                                    </div>                                    
                                </div>
                                <div style="display: flex; justify-content: flex-start; width: 100%;">
                                    <div>
                                        <span>Tipo Veículo:</span>
                                        <select style="width: 100%" name="txtTipoVei[]" id="txtTipoVei" multiple="multiple" class="select">                                                      
                                            <?php $sql = "select id, descricao from sf_veiculo_tipos where v_inativo = 0 ORDER BY id";
                                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id'] ?>"><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                            <?php } ?>                                 
                                        </select>
                                    </div>
                                    <div style="margin-left: 0.5%; min-width: 200px;">
                                        <span>Usuário Resp.:</span>
                                        <select class="input-medium" style="width:100%" name="txtUserResp" id="txtUserResp" <?php echo ($crm_pro_ure_ > 0 && $totEquipe == 0 ? "disabled" : "");?>>
                                            <option value="null">Selecione</option>
                                            <?php $cur = odbc_exec($con, "select id_usuario, nome from sf_usuarios 
                                            where inativo = 0 and id_usuario > 1 " . ($crm_pro_ure_ > 0 ? "and (id_usuario = " . $_SESSION["id_usuario"] . " or id_usuario in (select sf_usuarios_dependentes.id_usuario from sf_usuarios_dependentes 
                                            inner join sf_usuarios on id_fornecedor_despesas = funcionario
                                            where sf_usuarios.id_usuario = " . $_SESSION["id_usuario"] . "))" : "") . " order by nome") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id_usuario']; ?>" <?php echo ($crm_pro_ure_ > 0 && $RFP['id_usuario'] == $_SESSION["id_usuario"] ? "selected" : "");?>><?php echo formatNameCompact(strtoupper(utf8_encode($RFP['nome']))); ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>                                    
                                    <div style="margin-left: 0.5%; min-width: 250px; ">
                                        <span>Planos:</span>
                                        <select style="width: 100%" name="txtTipoPlano[]" id="txtTipoPlano" multiple="multiple" class="select"></select>
                                    </div>                                    
                                    <div style="margin-left: 0.5%; min-width: 250px; ">
                                        <span>Acessórios:</span>
                                        <select style="width: 100%" name="txtTipoItem[]" id="txtTipoItem" multiple="multiple" class="select"></select>
                                    </div>                                    
                                    <div style="float:left; margin-left: 0.5%;">
                                        <div width="100%">Valor a partir de:</div>
                                        <input id="txtValPlanoAte" maxlength="30" type="text" value="0,00" title=""/>
                                    </div>                                    
                                    <div style="margin-left: 0.5%; min-width: 250px;">
                                        <div width="100%">Status Clientes:</div>
                                        <select name="txtStatusCli[]" id="txtStatusCli" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                            <option value="Ativo">Ativo</option>
                                            <option value="AtivoCred">Ativo Credencial</option>
                                            <option value="AtivoAbono">Ativo Abono</option>
                                            <option value="AtivoEmAberto">Ativo Em Aberto</option>
                                            <option value="Suspenso">Suspenso</option>
                                            <option value="Cancelado">Cancelado</option>                                            
                                            <option value="Inativo">Inativo</option>                                            
                                            <option value="Bloqueado">Bloqueado</option>                                                                                                                                   
                                            <option value="Excluido">Excluido</option>                                                                                                                                   
                                        </select>
                                    </div>                                    
                                    <div style="float:left; margin-left: 0.5%;">
                                        <div width="100%">Busca:</div>
                                        <input id="txtBusca" maxlength="100" type="text" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title=""/>
                                    </div>
                                    <div style="margin-top: 15px;">
                                        <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Veículos</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tbListaVeiculo">
                            <thead>
                                <tr>                                    
                                    <th width="2%"></th>
                                    <th width="6%">Placa:</th>                                               
                                    <th width="19%">Modelo:</th>                                                                                    
                                    <th width="9%">IMEI:</th>
                                    <th width="7%">Chip:</th>                                    
                                    <th width="6%">Valor</th>                                    
                                    <th width="8%">CPF:</th>
                                    <th width="13%">Cliente:</th>                                    
                                    <th width="5%">Plano:</th>
                                    <th width="5%">V.Plano:</th>
                                    <th width="5%">V.Add.:</th>                                    
                                    <th width="5%">Validade:</th>
                                    <th width="5%">Status:</th>
                                    <th width="5%"><center>Ação:</center></th>                                    
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>                        
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div> 
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type="text/javascript" src="../../js/jquery.priceformat.min.js"></script>        
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>                     
        <script type="text/javascript" src="js/Veiculo.js"></script>                     
        <script type="text/javascript">
            
            $("#txtValPlanoAte").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
            
            function selectStatus(status) {
                $("#txtFilter").val(status);
                $("#btnfind").click();
            }
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }
            
            function AbrirBox(opc) {
                if (opc === 5) {
                    abrirTelaBox("../CRM/EmailsEnvioForm.php", 650, 720);
                }
                if (opc === 6) {
                    abrirTelaBox("../CRM/SMSEnvioForm.php", 400, 400);
                }
                if (opc === 7) {
                    abrirTelaBox("../CRM/SMSEnvioForm.php?zap=s", 400, 400);
                }                
            }
            
            function FecharBox(opc) {
                $("#newbox").remove();
            }
            
            function listaEmails() {
                var emails = [];
                $.ajax({
                    url: finalFindVeiculo(1) + "&emailMarketing=1",
                    async: false,
                    dataType: "json",
                    success: function (j) {
                        for (var x in j.aaData) {
                            if (j.aaData[x][2].trim() !== "") {
                                emails.push(j.aaData[x][2].replace(/ /g, "") + "|" + j.aaData[x][0]);
                            }
                        }
                    }
                });
                return emails;
            }   
            
            function listaSMS() {
                var sms = [];
                $.ajax({
                    url: finalFindVeiculo(1) + "&envioSms=1",
                    async: false,
                    dataType: "json",
                    success: function (j) {
                        for (var x in j.aaData) {
                            if (j.aaData[x][1].trim() !== "") {
                                sms.push(j.aaData[x][1].replace(/ /g, "") + "|" + j.aaData[x][0]);
                            }
                        }
                    }
                });
                return sms;
            }            
                    
            function AbrirBoxCli(tipo, id) {
                let url = (tipo === "C" ? "./../Academia/ClientesForm.php?id=" : "./../CRM/CRMForm.php?id=") + id;
                window.open(url,'_blank');
            }

            function imprimir(tipo) {
                var pRel = "&NomeArq=" + "Veículos" +
                "&lbl=" + "Placa|" + (tipo === "E" ? "Marca|" : "") + "Modelo|" + (tipo === "E" ? "Combustível|Chassi|Renavam|Cor|Ano Fab.|Ano Mod.|Cep|Endereço|Número|Complemento|Bairro|Cidade|Estado|Tipo|Celular|Email|" : "") + 
                "IMEI|Chip|Valor|CPF|Cliente|Plano|V.Plano|V.Add|Validade|Status" + (tipo === "E" ? "|Valor Mens.|Consultor|Dt.Ass.Contrato" : "") +
                "&pOri=L"+ //parametro de paisagem
                "&siz=" + "50|" + (tipo === "E" ? "100|" : "") + "170|" + (tipo === "E" ? "50|100|70|70|50|50|50|150|30|50|100|50|50|50|70|130|" : "") + 
                "70|70|60|70|160|160|50|50|50|50" + (tipo === "E" ? "|100|100|100" : "") +
                "&pdf=" + "0|" + (tipo === "E" ? "33" : "14") + // Colunas do server processing que não irão aparecer no pdf 9
                "&filter=" + "Veículos " + //Label que irá aparecer os parametros de filtro
                "&PathArqInclude=../Modulos/Seguro/" + finalFindVeiculo(1).replace("?", "&"); // server processing            
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tipo + "&PathArq=GenericModelPDF.php", '_blank');
            }                                    
            
            $("#btnfind").click(function () {
                tbListaVeiculo.fnReloadAjax(finalFindVeiculo(0));
            });  
            
            $('#txtTipoVei').change(function () {
                getAcessorios();
                getPlanos();                
            });

            function getPlanos() {
                $.getJSON("form/FormVeiculosServer.php?getPlanos=" + $("#txtTipoVei").val(), {isAjax: "S"}, function (json) {
                    let options = "";
                    if (json !== null) {
                        for (var x = 0; x < json.length; x++) {
                            options += "<option value=\"" + json[x].conta_produto + "\" val=\"" + json[x].preco_venda + "\">" + json[x].descricao + "</option>";
                        }
                    }
                    $("#txtTipoPlano").html(options);
                    $("#txtTipoPlano").select2("val", "");
                }); 
            }

            function getAcessorios() {
                $.getJSON("form/FormVeiculosServer.php?getAcessorios=" + $("#txtTipoVei").val(), {isAjax: "S"}, function (json) {
                    let options = "";
                    if (json !== null) {
                        for (var x = 0; x < json.length; x++) {
                            options += "<option value=\"" + json[x].conta_produto + "\" val=\"" + json[x].preco_venda + "\">" + json[x].descricao + "</option>";
                        }
                    }
                    $("#txtTipoItem").html(options);
                    $("#txtTipoItem").select2("val", "");
                }); 
            }
                                    
        </script>        
        <?php odbc_close($con); ?>
    </body>
</html>