<?php
include "../../Connections/configini.php";
include "form/FormVeiculosServer.php";
?>
<!DOCTYPE html>
<head>
    <link rel="icon" type="image/ico" href="../../../favicon.ico"/>
    <link href="../../../css/stylesheets.css" rel="stylesheet" type="text/css" />
    <link href="../../../css/stylesheet.css" rel="stylesheet" type="text/css" />
    <link href="../../../css/main.css" rel="stylesheet">
    <link href="../../../css/bootbox.css" rel="stylesheet" type="text/css"/>
    <link href="../../../js/jBox/jBox.css" rel="stylesheet" type="text/css"/> 
    <link href="../../../js/jBox/themes/TooltipBorder.css" rel="stylesheet" type="text/css"/>    
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    <style type="text/css">
        .labcont {
            width: 100%;
            border: 1px solid transparent;
        }
        .labhead {
            padding: 0;
            border: 1px solid transparent;
        }
        .liscont {
            width: 100%;
            border: 1px solid #EEE;
        }
        .lishead {
            padding: 0;
            background: #EEE;
            border: 1px solid #FFF;
        }
        .lisbody {
            padding: 0 5px;
            line-height: 28px;
            background: #F6F6F6;
            border: 1px solid #FFF;
        }
        .lisfoot {
            padding: 0 5px;
            background: #EEE;
            text-align: right;
            line-height: 28px;
            font-weight: bold;
            border: 1px solid #FFF;
        }
        .lisbody:nth-child(3) { text-align: center; }
        .lisbody:nth-child(4), .lisbody:nth-child(5) { text-align: right; }
        .lisbody.selected { background: #acbad4 !important;}        
        .img-car {
            border: 1px solid #ddd;
            float: left;
            margin-left: 1%;
            margin-bottom: 1%;
            width: 23%;
            height: 50px; 
        }
        .img-car-master {
            margin-top: 3.5%;
            width: 97%;
            height: 200px;
        }
    </style>
</head>
<body>
    <div class="row-fluid">
        <form id="frmVeiculos" name="frmVeiculos" action="FormVeiculos.php" method="POST" enctype="multipart/form-data">
            <div class="frmhead">
                <div class="frmtext">Veículo</div>
                <div class="frmicon" style="top:13px" onClick="parent.FecharBoxVeiculo()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <input name="txtMesRef" id="txtMesRef" value="<?php echo $mes_referencia; ?>" type="hidden"/>
            <input name="txtIdPlano" id="txtIdPlano" value="<?php echo $venda_plano; ?>" type="hidden"/>
            <input name="txtIdCliente" id="txtIdCliente" value="<?php echo $id_fornecedores_despesas; ?>" type="hidden"/>
            <input name="txtIdExterno" id="txtIdExterno" value="<?php echo $id_externo; ?>" type="hidden"/>
            <input name="txtIdVistoria" id="txtIdVistoria" value="<?php echo $id_vistoria; ?>" type="hidden"/>
            <input id="txtPisoFranquia" value="<?php echo $pisoFranquia; ?>" type="hidden"/>
            <input id="txtTipoProtegido" value="<?php echo $tipo_protegido; ?>" type="hidden"/>
            <div style="float: left; width: 30%; overflow: hidden; margin-left: 1%;">
                <img alt="Imagem" class="img-car img-car-master" src="<?php echo getImageCar($Imagens, $id_vistoria . "_img" . str_pad(0, 2, "0", STR_PAD_LEFT), $contrato, $id_vistoria, "Vistorias"); ?>"/>                
                <div id="imgVeiculo" style="min-height: 388px;">          
                    <?php 
                    for ($i = 0; $i < 20; $i++) { ?>
                    <a href="javascript:void(0)">
                        <img alt="Imagem" class="img-car" src="<?php echo getImageCar($Imagens, $id_vistoria . "_img" . str_pad($i, 2, "0", STR_PAD_LEFT), $contrato, $id_vistoria, "Vistorias"); ?>"/>
                    </a>
                    <?php } ?>
                </div>                
                <div id="imgDanos" style="min-height: 388px; display: none;">
                    <?php 
                    if (is_numeric($id_vistoria)) {
                        $cur = odbc_exec($con, "select * from sf_vistorias_danos where id_vistoria = " . $id_vistoria);
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                        <a href="javascript:void(0)">
                            <img alt="Imagem" title="<?php echo utf8_encode($RFP["observacao"]); ?>" class="img-car" src="<?php echo getImageCar($ImagensDanos, $RFP["id"], $contrato, $id_vistoria, "Danos"); ?>"/>
                        </a>
                    <?php }} ?>
                </div>                
                <div style="text-align: center; float: left; width: 100%;">
                    <input type="radio" name="rbOption" value="V" checked>
                    <span>Perfil do Veículo</span>
                    <input type="radio" name="rbOption" value="D">
                    <span>Danos, Avarias e Outros</span>
                </div>                
            </div>
            <div style="float: left; width: 69%;">                
                <div class="tabbable frmtabs">
                <ul class="nav nav-tabs" style="margin:0">
                    <li class="active"><a href="#tab1" data-toggle="tab">Dados Principais</a></li>
                    <li class=""><a href="#tab2" data-toggle="tab">Plano</a></li>                    
                    <li class=""><a href="#tab3" data-toggle="tab">Acessórios</a></li>                    
                    <li class=""><a href="#tab4" data-toggle="tab">Documentos</a></li>
                    <li class=""><a href="#tab5" data-toggle="tab">Vistorias</a></li>
                    <li class=""><a href="#tab6" data-toggle="tab">Histórico de Km</a></li>
                    <li class=""><a href="#tab7" data-toggle="tab">Transferência</a></li>
                </ul>
                <div class="tab-content frmcont" style="height:420px; font-size:11px !important; overflow: unset;">
                    <div class="tab-pane active" id="tab1">
                        <div style="float: left; width: 100%; <?php echo ($ckb_pro_fipe_ == "0" ? "display: none;" : "");?>">
                            <input type="radio" name="rbtipoFipe" value="0" <?php echo ($consulta_fipe == "0" ? "checked" : "");?> <?php echo $disabled; ?>><span>Fipe</span>
                            <input type="radio" name="rbtipoFipe" value="1" <?php echo ($consulta_fipe == "1" ? "checked" : "");?> <?php echo $disabled; ?>><span>Personalizado</span>
                            <input type="radio" name="rbtipoFipe" value="2" <?php echo ($consulta_fipe == "2" ? "checked" : "");?> <?php echo $disabled; ?>><span>Zero Km</span>
                        </div>                        
                        <div style="clear:both;"></div>
                        <hr style="margin: 5px 0;">                        
                        <div style="width:15%; float:left;">
                            <span>Placa: <span class="ico-question-sign informacao" title="PARA VEÍCULO ZERO KM UTILIZAR A PLACA AAA-0000"></span></span>
                            <input id="txtPlaca" name="txtPlaca" class="input-xlarge" maxlength="8" type="text" style="min-height: 28px;" value="<?php echo $placa; ?>" item="<?php echo $placa; ?>" <?php echo $disabled; ?> <?php echo ($consulta_fipe == "2" ? "readonly" : "");?>/>
                        </div>                        
                        <div style="width:20%; float:left; margin-left:1%">
                            <span>Tipo:</span>
                            <select id="txtTipo" name="txtTipo" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>                                
                                <option value="null">Selecione</option>                                                             
                                <?php $sql = "select id, descricao from sf_veiculo_tipos " . (is_numeric($idCidade) ? "where v_inativo = 0 and (v_regiao is null or v_regiao in (select id_regiao from sf_regioes_cidades where id_cidade = " . $idCidade . "))" : "where v_inativo = 0") . " ORDER BY id";
                                $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP['id'] ?>"<?php
                                    if ($RFP["id"] == $tipo) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                <?php } ?>                                 
                            </select>
                        </div>                                                
                        <div style="width:28%; float:left; margin-left:1%">
                            <span>Montadora:</span>
                            <select id="txtMontadora" name="txtMontadora" sval="<?php echo $marca; ?>" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>                                
                                <option value="null">Selecione</option>
                            </select>
                        </div>                                                
                        <div style="width:23%; float:left; margin-left:1%">
                            <span>Ano mod:</span>
                            <select id="txtAnoMod" name="txtAnoMod" sval="<?php echo $ano_modelo; ?>" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>                                
                                <option value="null">Selecione</option>                                
                            </select>
                        </div>
                        <div style="width:10%; float:left; margin-left:1%">
                            <span>Ano Fab:</span>
                            <input id="txtAnoFab" name="txtAnoFab" class="input-xlarge" maxlength="4" type="text" style="min-height: 28px;" value="<?php echo $ano_fab; ?>" <?php echo $disabled; ?>/>
                        </div>                                                                       
                        <div style="clear:both;"></div>
                        <div style="width:42%; float:left;">
                            <span>Modelo:</span>
                            <select id="txtModelo" name="txtModelo" sval="<?php echo $modelo; ?>" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>                                
                                <option value="null">Selecione</option> 
                            </select>
                        </div>       
                        <div style="width:22%; float:left; margin-left:1%">
                            <span>Combustível:</span>
                            <select id="txtCombustivel" name="txtCombustivel" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>                                
                                <option value="null">Selecione</option>
                                <option value="Gasolina" <?php echo ($combustivel == "Gasolina" ? "selected" : "");?>>Gasolina</option>
                                <option value="Álcool" <?php echo ($combustivel == "Álcool" ? "selected" : "");?>>Álcool</option>
                                <option value="Flex" <?php echo ($combustivel == "Flex" ? "selected" : "");?>>Flex</option>
                                <option value="Diesel" <?php echo ($combustivel == "Diesel" ? "selected" : "");?>>Diesel</option>
                                <option value="Bio-Gás" <?php echo ($combustivel == "Bio-Gás" ? "selected" : "");?>>Bio-Gás</option>
                            </select>
                        </div>                        
                        <div style="width:13%; float:left; margin-left:1%">
                            <span>N° Passageiros:</span>
                            <input id="txtNPassageiros" name="txtNPassageiros" class="input-xlarge" maxlength="2" type="text" style="min-height: 28px;" value="<?php echo $nPassageiros; ?>" <?php echo $disabled; ?>/>
                        </div> 
                        <div style="width:20%; float:left; margin-left:1%">
                            <span>Veículo 0 KM:</span>
                            <select id="txtVeiculoZ" name="txtVeiculoZ" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                                <option value="0" <?php echo ($veiculoZ == "0" ? "selected" : "");?>>NÃO</option>
                                <option value="1" <?php echo ($veiculoZ == "1" ? "selected" : "");?>>SIM</option>
                            </select>
                        </div>                        
                        <div style="clear:both;"></div>
                        <div style="width:25%; float:left;">
                            <span>Chassi:</span>
                            <input id="txtChassi" name="txtChassi" class="input-xlarge" type="text" style="min-height: 28px;" value="<?php echo $chassi; ?>" <?php echo $disabled; ?>/>
                        </div>
                        <div style="width:20%; float:left; margin-left:1%">
                            <span>Renavam:</span>
                            <input id="txtRenavam" name="txtRenavam" class="input-xlarge" type="text" style="min-height: 28px;" value="<?php echo $renavam; ?>" <?php echo $disabled; ?>/>
                        </div>
                        <div style="width:21%; float:left; margin-left:1%">
                            <span>N° Portas:</span>
                            <select id="txtNPortas" name="txtNPortas" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                                <option value="null">Selecione</option>
                                <option value="2" <?php echo ($nPortas == "2" ? "selected" : "");?>>2 portas</option>
                                <option value="3" <?php echo ($nPortas == "3" ? "selected" : "");?>>3 portas</option>
                                <option value="4" <?php echo ($nPortas == "4" ? "selected" : "");?>>4 portas</option>
                                <option value="5" <?php echo ($nPortas == "5" ? "selected" : "");?>>5 portas</option>
                            </select>
                        </div>                                                
                        <div style="width:10%; float:left; margin-left:1%">
                            <span>Cilindrada:</span>
                            <input id="txtCilindrada" name="txtCilindrada" class="input-xlarge" type="text" style="min-height: 28px;" value="<?php echo $cilindrada; ?>" <?php echo $disabled; ?>/>
                        </div>
                        <div style="width:20%; float:left; margin-left:1%">
                            <span>KM:</span>
                            <input id="txtKM" name="txtKM" class="input-xlarge" type="text" style="min-height: 28px;" value="<?php echo $km; ?>" <?php echo $disabled; ?>/>
                        </div>                        
                        <div style="clear:both;"></div>
                        <div style="width:21%; float:left;">
                            <span>N° Motor:</span>
                            <input id="txtMotor" name="txtMotor" class="input-xlarge" type="text" style="min-height: 28px;" value="<?php echo $motor; ?>" <?php echo $disabled; ?>/>
                        </div>
                        <div style="width:18%; float:left; margin-left:1%">
                            <span>Categoria:</span>
                            <select id="txtCategoria" name="txtCategoria" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                                <option value="null">Selecione</option>
                                <option value="1" <?php echo ($categoria == "1" ? "selected" : "");?>>Particular</option>
                                <option value="2" <?php echo ($categoria == "2" ? "selected" : "");?>>Aluguel</option>
                                <option value="3" <?php echo ($categoria == "3" ? "selected" : "");?>>Aplicativo</option>
                            </select>
                        </div>                                                
                        <div style="width:10%; float:left; margin-left:1%">
                            <span>Gás Natural:</span>
                            <select id="txtGnv" name="txtGnv" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                                <option value="0" <?php echo ($gnv == "0" ? "selected" : "");?>>NÃO</option>
                                <option value="1" <?php echo ($gnv == "1" ? "selected" : "");?>>SIM</option>                                
                            </select>
                        </div>
                        <div style="width:10%; float:left; margin-left:1%">
                            <span>Alienado:</span>
                            <select id="txtAlienado" name="txtAlienado" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                                <option value="0" <?php echo ($alienado == "0" ? "selected" : "");?>>NÃO</option>
                                <option value="1" <?php echo ($alienado == "1" ? "selected" : "");?>>SIM</option>
                            </select>
                        </div>                        
                        <div style="width:10%; float:left; margin-left:1%">
                            <span>Cota:</span>
                            <input id="txtFranquia" name="txtFranquia" class="input-xlarge" type="text" style="min-height: 28px;" value="<?php echo $franquia; ?>" <?php echo $disabled; ?>/>
                        </div>                            
                        <div style="width:6%; float:left; margin-top: 15px;">
                            <button class="btn btn-primary" id="bntProdDesconto" type="button" style="width:35px; padding-bottom:4px; text-align:center" <?php echo $disabled; ?>><span id="prod_sinal"><?php echo ($descontotp == "0" ? "%" : "$"); ?></span></button>
                            <input name="txtProdSinal" id="txtProdSinal" value="<?php echo $descontotp; ?>" type="hidden"/>                            
                        </div>
                        <div style="width:20%; float:left; margin-left:1%">
                            <span>Câmbio:</span>
                            <select id="txtCambio" name="txtCambio" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                                <option value="null">Selecione</option>
                                <option value="1" <?php echo ($cambio == "1" ? "selected" : "");?>>Manual</option>
                                <option value="2" <?php echo ($cambio == "2" ? "selected" : "");?>>Automático</option>
                                <option value="3" <?php echo ($cambio == "3" ? "selected" : "");?>>Semi-Automático</option>
                            </select>
                        </div>
                        <div style="clear:both;"></div>                        
                        <div style="width:25%; float:left;">
                            <span>Cor:</span>
                            <select id="txtCor" name="txtCor" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                                <option value="null">Selecione</option>
                                <?php $cur = odbc_exec($con, "select * from sf_produtos_cor");
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo utf8_encode($RFP['id_produto_cor']); ?>" <?php echo ($cor == $RFP['id_produto_cor'] ? "selected" : ""); ?>><?php echo utf8_encode($RFP['nome_produto_cor']); ?></option>
                                <?php } ?>
                            </select>
                        </div>  
                        <div style="width:10%; float:left; margin-left:1%">
                            <span>Depreciação:</span>
                            <select id="txtDepreciacao" name="txtDepreciacao" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                                <option value="0" <?php echo ($depreciacao == "0" ? "selected" : "");?>>NÃO</option>
                                <option value="1" <?php echo ($depreciacao == "1" ? "selected" : "");?>>SIM</option>                                
                            </select>
                        </div>                                                
                        <div style="width:31%; float:left; margin-left:1%">
                            <span>Responsável pela Autorização:</span>
                            <select id="txtAutorizacao" name="txtAutorizacao" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                                <option value="null">Selecione</option>
                                <?php $cur = odbc_exec($con, "select login_user,nome from sf_usuarios where inativo = 0 and login_user != 'Admin' and (master = 0 or master in (select id_permissoes from sf_usuarios_permissoes where ckb_pro_apv_veic = 1)) order by nome") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP['login_user'] ?>"<?php
                                    if (!(strcmp($RFP['login_user'], $destinatario))) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP['nome']) ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div style="width:13%; float:left; margin-left:1%">
                            <span>Status:</span>
                            <input id="txtStatus" name="txtStatus" value="<?php echo $status; ?>" disabled type="text" class="input-medium"/>
                        </div>                        
                        <div style="width:17%; float:left; margin-left:1%">
                            <span>Data/Hora:</span>
                            <input id="txtData" name="txtData" class="input-xlarge" type="text" style="min-height: 28px;" value="<?php echo $data; ?>" disabled/>
                        </div>                        
                        <div style="clear:both;"></div>
                        <hr style="margin: 10px 0;">
                        <div style="width:15%; float:left;">
                            <span style="color: red;">Valor FIPE:</span>
                            <input id="txtValorFipe" name="txtValorFipe" class="input-xlarge" type="text" style="min-height: 28px;" value="<?php echo $valorFipe; ?>" disabled/>
                        </div>                        
                        <div style="width:5%; float:left; margin-left:1%; margin-top: 15px;">
                            <button id="bntFipeToProtegido" type="button" class="btn yellow" onclick="fipeToProtegido();" <?php echo $disabled; ?>><span class="ico-double-angle-right"></span></button>
                        </div>                                                                                              
                        <div style="width:15%; float:left; margin-left:1%">
                            <span>Valor Protegido:</span>
                            <input id="txtProtegido" name="txtProtegido" class="input-xlarge" type="text" style="min-height: 28px;" <?php echo $disabled; ?> value="<?php echo $protegido; ?>" <?php echo (in_array($consulta_fipe, ["0", "2"]) ? "readonly" : "");?>/>
                        </div>                                                                                              
                        <div style="width:12%; float:left; margin-left:1%">
                            <span>Cod.FIPE:</span>
                            <input id="txtCodFipe" name="txtCodFipe" class="input-xlarge" type="text" style="min-height: 28px;" value="<?php echo $codFipe; ?>" <?php echo $disabled; ?>/>
                        </div>                        
                        <div style="width:8%; float:left; margin-left:1%">
                            <span>% FIPE:</span>
                            <input id="txtPorcentagem" name="txtPorcentagem" class="input-xlarge" maxlength="6" type="text" style="min-height: 28px;" value="<?php echo $porcentagem; ?>" <?php echo $disabled; ?>/>
                        </div>
                        <div style="width:40%; float:left; margin-left:1%">
                            <span>Plano:</span>
                            <span style="float: right;">Cota de Participação: <span id="lblFranquia" style="font-weight: bold;">R$ 0,00</span></span>
                            <select id="txtPlano" name="txtPlano" plObg="<?php echo $veiPlanObrig_; ?>" sval="<?php echo $txtPlano; ?>" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>                                
                                <option price="0,00" value="null">Selecione</option>                                
                            </select>
                        </div>                        
                        <div style="clear:both;"></div>
                        <hr style="margin: 10px 0;">                                                    
                        <div style="width:15%; float:left;">
                            <span>Data Instalação:</span>
                            <input id="txtDtInstalacao" name="txtDtInstalacao" class="datepicker inputCenter" maxlength="32" type="text" style="min-height: 28px;" value="<?php echo $dtInstalacao; ?>" <?php echo $disabled; ?>/>
                        </div>                         
                        <div style="width:34%; float:left; margin-left:1%">
                            <span>Instalador do Rastreador:</span>
                            <select id="txtFuncionario" name="txtFuncionario" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>                              
                                <option value="null">Selecione</option>
                                <?php $query = "SELECT id_fornecedores_despesas, razao_social FROM sf_usuarios u 
                                INNER JOIN sf_fornecedores_despesas f ON u.funcionario = f.id_fornecedores_despesas  
                                WHERE f.recebe_comissao = 1 and f.inativo = 0 AND dt_demissao IS NULL AND f.tipo = 'E'";
                                $cur = odbc_exec($con, $query);
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo utf8_encode($RFP['id_fornecedores_despesas']); ?>" <?php echo ($funcionario == $RFP['id_fornecedores_despesas'] ? "SELECTED" : ""); ?>><?php echo utf8_encode($RFP['razao_social']); ?></option>
                                <?php } ?>                                    
                            </select>
                        </div>
                        <div style="width:49%; float:left; margin-left:1%">
                            <span>Pagamento de Serviço (Rastreador):</span>
                            <select id="txtVendaItem" name="txtVendaItem" class="select input-medium" style="width:100%" <?php echo $disabled; ?> <?php echo (is_numeric($id_item_venda) ? "readonly" : "");?>>                              
                                <option value="null">Selecione</option>
                                <?php if (is_numeric($id_fornecedores_despesas)) {
                                    $query = "select id_item_venda, data_venda, descricao, vendedor_comissao, quantidade 
                                    from sf_vendas_itens vi inner join sf_vendas v on v.id_venda = vi.id_venda
                                    inner join sf_produtos p on p.conta_produto = vi.produto
                                    where produto = 41 and cliente_venda = " . $id_fornecedores_despesas . " and (vendedor_comissao is null or id_item_venda = " . valoresNumericos2($id_item_venda) . ")";  
                                    $cur = odbc_exec($con, $query);
                                    while ($RFP = odbc_fetch_array($cur)) {
                                        if ($id_item_venda == $RFP['id_item_venda']) {
                                            $funcionario = $RFP['vendedor_comissao'];
                                        } ?>
                                <option value="<?php echo utf8_encode($RFP['id_item_venda']); ?>" <?php echo ($id_item_venda == $RFP['id_item_venda'] ? "SELECTED" : ""); ?>><?php echo escreverDataHora($RFP['data_venda']) . " Qtd. " . escreverNumero($RFP['quantidade'], 0, 0); ?></option>
                                <?php }} ?>   
                            </select>
                        </div>  
                        <div style="clear:both;"></div>
                        <div style="width:37%; float:left;">
                            <span>Rastreador:</span>
                            <select id="txtRastreador" name="txtRastreador" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>                                
                                <option value="null">Selecione</option>                                                             
                                <?php $sql = "select id, descricao from sf_rastreador ORDER BY id";
                                $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP['id'] ?>"<?php
                                    if ($RFP["id"] == $rastreador) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                <?php } ?>                                 
                            </select>
                        </div>
                        <div style="width:26%; float:left; margin-left:1%">
                            <span>IMEI Rastreador:</span>
                            <input id="txtImei" name="txtImei" class="input-xlarge" maxlength="32" type="text" style="min-height: 28px;" value="<?php echo $imei; ?>" <?php echo $disabled; ?>/>
                        </div>
                        <div style="width:16%; float:left; margin-left:1%">
                            <span>Número Chip:</span>
                            <input id="txtChip" name="txtChip" class="input-xlarge" type="text" style="min-height: 28px;" value="<?php echo $chip; ?>" <?php echo $disabled; ?>/>
                        </div>                        
                        <div style="width:18%; float:left; margin-left:1%">
                            <span>Operadora:</span>
                            <select id="txtOperadora" name="txtOperadora" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>                                
                                <option value="null">Selecione</option>                                                             
                                <?php $sql = "select id, descricao from sf_rastreador_operadora ORDER BY id";
                                $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP['id'] ?>"<?php
                                    if ($RFP["id"] == $operadora) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                <?php } ?>                                 
                            </select>
                        </div>                                               
                        <div style="clear:both;"></div>
                    </div>
                    <div class="tab-pane" id="tab2">
                        <div class="body" style="margin-left:0; padding:0">
                            <div class="content" style="min-height:153px; margin-top:0">
                                <table id="tblHistPlanos" class="table" cellpadding="0" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width:150px;text-align:center;"><b>Cliente</b></th>                                            
                                            <th style="width:83px;text-align:center;"><b>Plano</b></th>
                                            <th style="width:56px;text-align:center;"><b>Inicio</b></th>
                                            <th style="width:56px;text-align:center;"><b>Fim</b></th>
                                            <th style="width:56px;text-align:center;"><b>Cancel.</b></th>
                                            <th style="width:56px;text-align:center;"><b>Status</b></th>                                            
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="content" style="min-height:169px; margin-top:0">
                                <hr style="margin: 5px 0px 0px 0px">
                                <div id="divConvenio">
                                    <div style="float:left; width:15%">
                                        <span>Valor de Convênio:</span>
                                        <input type="text" class="input-medium" id="txtConvenios" value="0,00" maxlength="10" <?php echo (!is_numeric($id) ? "disabled" : ""); ?>>
                                    </div>  
                                    <div style="float:left">
                                        <span style="width:100%; display:block;">&nbsp;</span>
                                        <span style="width:100%; display:block;">
                                            <div class="btn-group"><button class="btn btn-primary" id="btnTpValor" type="button" <?php echo (!is_numeric($id) ? "disabled" : ""); ?> style="width:35px; padding-bottom:3px; text-align:center; background:#308698 !important">
                                            <span id="tp_sinal">%</span></button></div>
                                            <input id="txtSinal" value="P" type="hidden">
                                        </span>
                                    </div>                                    
                                    <div style="width:12%; float:left;margin-left: 1%">
                                        <span>Inicio:</span>
                                        <input type="text" id="txtConvDtIni" class="datepicker inputCenter" <?php echo (!is_numeric($id) ? "disabled" : ""); ?>/>
                                    </div>
                                    <div style="width:12%; float:left;margin-left: 1%">
                                        <span>Fim:</span>
                                        <input type="text" id="txtConvDtFim" class="datepicker inputCenter" <?php echo (!is_numeric($id) ? "disabled" : ""); ?>/>
                                    </div>
                                    <div style="width:13%; float:left;margin-left: 1%;<?php echo ($adm_desc_ven_ == 1 ? "display: none;" : ""); ?>">
                                        <span>Até o Vencimento:</span>
                                        <input type="checkbox" id="txtAteVencimento" class="input-medium" <?php echo ($adm_desc_ven_ > 0 ? "checked" : ""); ?> value="1" <?php echo (!is_numeric($id) ? "disabled" : ""); ?>/>
                                    </div>
                                    <div style="float:left;margin-left: 1%;">
                                        <?php if($ckb_adm_cli_read_ == 0) { ?>
                                        <button id="btnIncluirConv" type="button" class="btn dblue" <?php echo (!is_numeric($id) ? "disabled" : ""); ?> style="line-height:19px; width:75px;margin-top: 16px;background-color: #308698 !important;"> Incluir <span class="icon-arrow-next icon-white"></span></button>
                                        <?php } ?>
                                    </div>
                                    <div style="clear:both; height:5px"></div>
                                </div>                                
                                <table id="tblConvenio" class="table" cellpadding="0" cellspacing="0" width="100%" style="display: table;">
                                    <thead>
                                        <tr><th width="513px"><b>Convênio</b></th>
                                            <th width="150px"><b>Desconto</b></th>
                                            <th width="150px"><b>Inicio</b></th>
                                            <th width="150px"><b>Fim</b></th>
                                            <th width="150px"><b>Até Vencimento</b></th>
                                            <th width="43px"><b>Ação</b></th>
                                        </tr></thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>               
                    </div>                    
                    <div class="tab-pane" id="tab3">
                        <div class="labcont">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="labhead" style="padding:0 5px">Produtos</td>
                                    <td width="50px" class="labhead" style="text-align:center">Ação</td>
                                </tr>
                            </table>
                        </div>
                        <div class="liscont">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="lishead" style="max-width: 100px; overflow: hidden;">
                                        <select id="txtProduto" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                                            <option value="null">--Selecione--</option>
                                            <?php $sql = "select conta_produto, descricao, preco_venda from sf_produtos where inativa = 0 and tipo = 'A' and (veiculo_tipo is null or veiculo_tipo = " . $tipo . ") ORDER BY descricao";
                                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP["conta_produto"] ?>" val="<?php echo $RFP["preco_venda"] ?>">
                                                    <?php echo utf8_encode($RFP["descricao"]); ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                    <td width="110px" class="lishead">
                                        <input id="txtValToTP" type="text" disabled value="0,00" class="input-medium" style="text-align:right">
                                    </td>
                                    <td width="50px" class="lishead">                                        
                                        <button class="btn green" type="button" onclick="novaLinha()" style="width:100%; height:27px; margin:0" <?php echo $disabled; ?>><span class="ico-plus icon-white"></span></button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="liscont" style="height:338px; overflow-y:scroll">
                            <table id="tbProdutos" cellpadding="0" cellspacing="0" width="100%"></table>
                        </div>
                        <div class="liscont" style="border-top:0">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="lisfoot"><div style="float: right;">Total em Produtos:</div></td>
                                    <td width="100px" class="lisfoot"><span id="totalP"><?php echo escreverNumero(0,1); ?></span></td>
                                    <td width="40px" class="lisfoot">&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab4">
                        <table width="650" border="0" cellpadding="3" class="textosComuns">
                            <tr>
                                <td width="122" height="37" align="right"> Envio de Arquivos: </td>
                                <td colspan="3"><input <?php echo $disabled; ?>  type="file" name="file" id="file" style="font-size:11px; height:23px; margin-top:4px"/></td>
                                <td width="100" align="right">
                                    <?php if (is_numeric($id)) { ?>
                                        <button class="btn btn-success" <?php echo $disabled; ?> type="submit" name="bntSendFile"><span class="ico-checkmark"></span> Enviar</button>
                                    <?php } ?>
                                </td>
                            </tr>
                        </table>
                        <div style="height:35px; width:138px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Data de Upload</b></div>
                        <div style="height:35px; width:345px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Nome do Arquivo</b></div>
                        <div style="height:35px; width:78px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Tamanho</b></div>
                        <div style="height:35px; width:51px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Ação</b></div>
                        <div style="clear:both"></div>
                        <div style="overflow:scroll; height:340px">
                            <?php
                            if (is_numeric($id)) {
                                $pasta = "./../../Pessoas/" . $contrato . "/Veiculos/" . $id . "/";
                                if (file_exists($pasta)) {
                                    $diretorio = dir($pasta);
                                    while ($arquivo = $diretorio->read()) {
                                        if ($arquivo != "." && $arquivo != "..") { ?>
                                            <div style="height:30px; width:138px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo date($lang['dmy'] . " H:i:s", filectime($pasta . $arquivo)); ?></div>
                                            <div style="height:30px; width:355px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo "<a href=\"" . $pasta . $arquivo . "\" target=\"_blank\">" . $arquivo; ?></a></div>
                                            <div style="height:30px; width:83px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo filesize($pasta . $arquivo) . " bytes"; ?></div>
                                            <div style="height:23px; width:39px; background:#FFF; margin-left:1px; padding-top:7px; padding-left:3px; float:left">
                                                <a onClick="return confirm('Deseja deletar esse registro?')" href="FormVeiculos.php?id=<?php echo $id; ?>&Del_F=<?php echo $arquivo; ?>"><img src="../../img/1365123843_onebit_33 copy.PNG" width="16" height="16"/></a>
                                            </div>
                                            <div style="clear:both"></div>
                                            <?php
                                        }
                                    }
                                    $diretorio->close();
                                }
                            } ?>
                        </div>
                    </div>                    
                    <div class="tab-pane" id="tab5">
                        <div class="body" style="margin-left:0; padding:0">
                            <div class="content" style="min-height:153px; margin-top:0">
                                <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tblHistVistorias">
                                    <thead>
                                        <tr>
                                            <th style="width: 20%;">Data Vistoria</th>
                                            <th style="width: 30%;">Descrição</th>
                                            <th style="width: 30%;">Vistoriador</th>
                                            <th style="width: 20%;">Data Conclusão</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab6">
                        <div class="body" style="margin-left:0; padding:0">
                            <div id="divKms">
                                <div style="float:left; width:12%;">
                                    <span>Data:</span>
                                    <input type="text" id="txtDtKms" class="datepicker inputCenter" <?php echo (!is_numeric($id) ? "disabled" : ""); ?>/>
                                </div>                            
                                <div style="float:left; width:15%; margin-left: 1%">
                                    <span>Kms:</span>
                                    <input type="text" class="input-medium" id="txtKms" value="0" maxlength="10" <?php echo (!is_numeric($id) ? "disabled" : ""); ?>>
                                </div>  
                                <div style="float:left;margin-left: 1%;">
                                    <button id="btnIncluirKm" type="button" class="btn dblue" <?php echo (!is_numeric($id) ? "disabled" : ""); ?> style="line-height:19px; width:75px;margin-top: 15px;background-color: #308698 !important;"> Incluir <span class="icon-arrow-next icon-white"></span></button>
                                </div>
                                <div style="clear:both; height:5px"></div>
                            </div>                                
                            <div class="content">
                                <table id="tblKm" class="table" cellpadding="0" cellspacing="0" width="100%" style="display: table;">
                                    <thead>
                                        <tr><th width="25%"><b>Data</b></th>
                                            <th width="20%"><b>Kms</b></th>
                                            <th width="25%"><b>Dt.Cad.</b></th>
                                            <th width="20%"><b>Usuário</b></th>
                                            <th width="10%"><b>Ação</b></th>
                                        </tr></thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab7">
                        <div style="width:100%; float:left;">
                            <span>Destinatário Transferência :</span>
                            <input type="text" id="txtTransfPara" style="width:100%" <?php echo (!is_numeric($id) ? "disabled" : ""); ?>/>
                            <input id="txtTransfParaCod" type="hidden"/>
                        </div>
                        <div style="clear:both; height:5px"></div>
                        <div style="width:100%; float:left;">
                            <span>Motivo :</span>
                            <textarea id="txtMotivo" <?php echo (!is_numeric($id) ? "disabled" : ""); ?> style="width:100%; height:127px; resize:none"></textarea>
                        </div>
                        <div style="width:100%; float:left; text-align: right; margin-top: 15px;">
                            <?php if (!is_numeric($id_externo)) { ?>
                                <?php if($ckb_adm_cli_read_ == 0 && $ckb_pro_cad_veic_ == 1) { ?>
                                    <button class="btn btn-success" type="button" id="btnSalvaTransf" title="Salvar" <?php echo (!is_numeric($id) ? "disabled" : ""); ?>><span class="ico-checkmark"></span> Transferir</button>                                                    
                                <?php } ?>                                
                            <?php } else { ?>
                                <span style="color: red;">* Cancele a sincronização com a assistência 24h para transferir o veículo!</span>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            </div>     
            <div style="clear:both;"></div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <div style="float: left; font-size: medium; width: 32%; text-align: left;">Valor Total do Plano <span id="lblTotal" style="font-weight: bold;">R$ 0,00</span></div>
                    <?php if ($ckb_adm_cli_read_ == 0 || ($typePage == "Prospect" && $user_resp == $_SESSION["id_usuario"])) { ?>                            
                    <?php if ($disabled == "") { ?>
                        <button id="bntSave" name="bntSave" type="submit" class="btn green" onClick="return validaForm();"><span class="ico-checkmark"></span> Gravar</button>
                        <?php if ($_POST["txtId"] == "") { ?>
                            <button class="btn yellow" onClick="parent.FecharBoxVeiculo()" id="bntCancelar"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                            <button class="btn yellow" type="submit" name="bntAlterar" id="bntAlterar"><span class="ico-reply"></span> Cancelar</button>
                        <?php
                        }
                    } else { ?>
                        <div style="float:left;">
                            <?php if (is_numeric($id_vistoria_ok)) { ?> 
                                <button class="btn turquoise" type="button" name="bntSinc" id="bntSinc" <?= !(is_numeric($id_externo)) ? '' : 'style="display: none;"' ?> data-id="<?= $id ?>"><span class="ico-checkmark"></span> Sincronizar (Assist.24h)</button>
                                <button class="btn yellow" type="button" name="bntSincCancel" id="bntSincCancel" <?= (is_numeric($id_externo)) ? '' : 'style="display: none;"' ?> data-id="<?= $id ?>"><span class="ico-reply"></span> Cancelar (Assist.24h)</button>                                                    
                            <?php if ($status == 'Aprovado' && strlen($ra_chave) > 0) { ?>
                                <button class="btn turquoise" type="button" name="bntSincRast" id="bntSincRast" <?= (is_numeric($id_externo_rast) && $status_ext_veic == "S") ? 'style="display: none;"' : '' ?> data-id="<?= $id ?>"><span class="ico-checkmark"></span> Sincronizar (Rastreio)</button>
                                <button class="btn yellow" type="button" name="bntSincRastCancel" id="bntSincRastCancel" <?= (is_numeric($id_externo_rast) && $status_ext_veic == "S") ? '' : 'style="display: none;"' ?> data-id="<?= $id ?>"><span class="ico-reply"></span> Cancelar (Rastreio)</button>
                            <?php }} if ($status == 'Aprovado' || $status == 'Reprovado') { ?>
                                <button class="btn yellow" type="submit" name="bntCancelarStatus" title="Cancelar Status" id="bntCancelarStatus"><span class="ico-reply"></span> Cancelar (<?php echo $status; ?>)</button>
                            <?php } else if (($status == 'Aguarda' || $status == 'Pendente') && is_numeric($id) && $ckb_pro_apv_veic_ > 0 && $_SESSION["login_usuario"] != "ADMIN") { ?>
                                <button class="btn btn-success" type="submit" name="bntAprov" title="Aprovar" id="bntAprov"><span class="ico-checkmark"></span> Aprovar</button>
                                <button class="btn red" type="button" onclick="AbonarMens()" title="Reprovar" id="bntReprov" name="bntReprov"><span class="ico-remove"></span> Reprovar</button>
                            <?php } else if (!is_numeric($id_vistoria_ok)) { ?>    
                                <span style="color: red;">* Veículo sem Vistoria Aprovada</span>
                            <?php } ?>
                        </div>                             
                        <button class="btn green" style="display: none;" type="submit" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" type="submit" name="bntEdit" id="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                        <?php if ($ckb_pro_exc_vei_ > 0) { ?>
                            <button class="btn red" type="submit" name="bntDelete" id="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class=" ico-remove"></span> Excluir</button>
                        <?php } ?>
                    <?php }} ?>
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins.js"></script>
    <script type="text/javascript" src="../../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type="text/javascript" src="../../../js/plugins/datatables/fnReloadAjax.js"></script>
    <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../../js/jquery.priceformat.min.js"></script>    
    <script type="text/javascript" src="../../../js/jBox/jBox.min.js"></script>    
    <script type="text/javascript" src="../../../js/util.js"></script>                                        
    <script type="text/javascript" src="js/FormVeiculos.js"></script>        
</body>
<?php odbc_close($con); ?>
</html>
