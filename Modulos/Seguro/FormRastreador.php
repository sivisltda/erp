<?php
include '../../Connections/configini.php';

if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "set dateformat dmy;update sf_fornecedores_despesas_veiculo set " .
        "imei = " . valoresTexto('txtImei') . "," .
        "chip = " . valoresTexto('txtChip') . "," .
        "dt_instalacao = " . valoresData('txtDtInstalacao') . " where id = " . $_POST['txtId']) or die(odbc_errormsg());
        echo "<script>alert('Registro atualizado com sucesso'); parent.FecharBoxVeiculo(1);</script>";
    }
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_fornecedores_despesas_veiculo where id = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id'];
        $imei = utf8_encode($RFP['imei']);
        $chip = utf8_encode($RFP['chip']);
        $dtInstalacao = escreverData($RFP['dt_instalacao']);        
    }
} else {
    $id = '';
    $imei = "";
    $chip = "";
    $dtInstalacao = "";    
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src='../../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<body>
    <form action="FormRastreador.php" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Rastreador</div>
            <div class="frmicon" onClick="parent.FecharBoxVeiculo()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont">
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>            
            <div style="width: 100%;float: left">
                <span>Data Instalação:</span>
                <input id="txtDtInstalacao" name="txtDtInstalacao" class="datepicker" maxlength="32" type="text" style="min-height: 28px;" value="<?php echo $dtInstalacao; ?>"/>
            </div>
            <div style="width: 100%;float: left">
                <span>IMEI Rastreador:</span>
                <input id="txtImei" name="txtImei" class="input-xlarge" maxlength="32" type="text" style="min-height: 28px;" value="<?php echo $imei; ?>"/>
            </div>
            <div style="width: 100%;float: left">
                <span>Número Chip:</span>
                <input id="txtChip" name="txtChip" class="input-xlarge" type="text" style="min-height: 28px;" value="<?php echo $chip; ?>"/>
            </div>
            <div style="clear:both;"></div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <button class="btn green" type="submit" name="bntSave" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                <button class="btn yellow" onClick="parent.FecharBoxVeiculo()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>                    
            </div>
        </div>
    </form>
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins.js"></script>
    <script type="text/javascript" src="../../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type="text/javascript" src="../../../js/plugins/datatables/fnReloadAjax.js"></script>
    <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../../js/jquery.priceformat.min.js"></script>    
    <script type="text/javascript" src="../../../js/jBox/jBox.min.js"></script>    
    <script type="text/javascript" src="../../../js/util.js"></script>  
    <script type="text/javascript">
        
        $("#txtDtInstalacao").mask('99/99/9999');
        $("#txtChip").mask("(99) 99999-9999");
        
    </script>
    <?php odbc_close($con); ?>
</body>