<?php

if (!isset($_GET['pdf'])) {
    include '../../Connections/configini.php';
}
$aColumns = array('vi.id', 'data', 'vt.descricao', 'sys_login', 'v.id id_carro', 'placa', 'marca', 'modelo', 'ano_modelo', 
'combustivel', 'valor', 'cnpj', 'razao_social', 'v.id_fornecedores_despesas', 'f.tipo tipo_cli', 'vi.data_aprov');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$colunas = "";
$sOrder = " ORDER BY vi.id desc ";
$sOrderX = " ORDER BY id desc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0 && intval($_GET['iSortCol_' . $i]) < 9) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY vi.id desc";
        $sOrderX = " ORDER BY id desc";
    }
}

if (is_numeric($_GET['idCli'])) {
    $sWhereX .= " AND v.id_fornecedores_despesas = " . $_GET['idCli'];
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $sWhereX .= " and vi.data between " . valoresDataHoraUnico2($_GET['dti'] . " 00:00:00") . " and " . valoresDataHoraUnico2($_GET['dtf'] . " 23:59:59");
}

if (isset($_GET['idSin'])) {
    $sWhereX .= " AND vi.id_telemarketing = " . valoresNumericos2($_GET['idSin']);
} else {
    $sWhereX .= " AND vi.id_telemarketing is null";
}

if (isset($_GET['vistorias'])) {
    if ($_GET['vistorias'] == "0") {
        $sWhereX .= " AND vi.data_aprov is not null";
    } else if ($_GET['vistorias'] == "1") {
        $sWhereX .= " AND vi.data_aprov is null";
    }
}

if (isset($_GET['statusap'])) {   
    if ($_GET['statusap'] == "0") {
        $sWhereX .= " AND v.status = 'Aguarda'";
    } elseif ($_GET['statusap'] == "1") {
        $sWhereX .= " AND v.status = 'Aprovado'";
    } elseif ($_GET['statusap'] == "2") {
        $sWhereX .= " AND v.status = 'Reprovado'";
    }
}

if (isset($_GET['status']) && $_GET['status'] != 'null') {
    $sWhereX .= " and sf_vendas_planos.planos_status in ('" . str_replace(",", "','", $_GET['status']) . "') ";
}

if (isset($_GET['assistencia'])) {
    if ($_GET['assistencia'] == "0") {
        $sWhereX .= " AND v.id_externo is not null";
    } else if ($_GET['assistencia'] == "1") {
        $sWhereX .= " AND v.id_externo is null";
    }
}

if (isset($_GET['tpCli'])) {
    $sWhereX .= " AND f.tipo = " . valoresTexto2($_GET['tpCli']);
} 

if ($_GET['grupo_pessoa'] == "-1") {
    $sWhereX .= " and f.grupo_pessoa is null ";
} else if ($_GET['grupo_pessoa'] != "") {
    $sWhereX .= " and f.grupo_pessoa = " . $_GET['grupo_pessoa'];
}

if ($_GET['Search'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= prepareCollum($aColumns[$i], 0) . " LIKE '%" . utf8_decode($_GET['Search']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
        $sWhere .= prepareCollum($aColumns[$i], 0) . " AND LIKE '%" . utf8_decode($_GET['sSearch_' . $i]) . "%' ";
    }
}

for ($i = 0; $i < count($aColumns); $i++) {
    $colunas .= $aColumns[$i] . ",";
}

$sQuery = "set dateformat dmy; SELECT COUNT(*) total FROM sf_vistorias vi 
inner join sf_fornecedores_despesas_veiculo v on vi.id_veiculo = v.id
inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = v.id_fornecedores_despesas
left join sf_vendas_planos on sf_vendas_planos.id_veiculo = v.id and dt_cancelamento is null left join sf_produtos on id_prod_plano = conta_produto
inner join sf_vistorias_tipo vt on vt.id = vi.tipo
WHERE vi.id > 0 " . $sWhereX . " " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "set dateformat dmy; SELECT COUNT(*) total FROM sf_vistorias vi 
inner join sf_fornecedores_despesas_veiculo v on vi.id_veiculo = v.id
inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = v.id_fornecedores_despesas
left join sf_vendas_planos on sf_vendas_planos.id_veiculo = v.id and dt_cancelamento is null left join sf_produtos on id_prod_plano = conta_produto
inner join sf_vistorias_tipo vt on vt.id = vi.tipo
WHERE vi.id > 0 $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$colunas = substr_replace($colunas, "", -1);
$sQuery1 = "set dateformat dmy; SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row," . $colunas . "
from sf_vistorias vi inner join sf_fornecedores_despesas_veiculo v on vi.id_veiculo = v.id
inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = v.id_fornecedores_despesas
left join sf_vendas_planos on sf_vendas_planos.id_veiculo = v.id and dt_cancelamento is null left join sf_produtos on id_prod_plano = conta_produto
inner join sf_vistorias_tipo vt on vt.id = vi.tipo
WHERE vi.id > 0 " . $sWhereX . " " . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1=" . $imprimir . " " . $sOrderX;
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();        
    $row[] = "<center><a href='javascript:void(0)' onClick='AbrirBoxVistorias(" . utf8_encode($aRow["id"]) . ")'><div id='formPQ' title='" . escreverDataHora($aRow[$aColumns[1]]) . "'>" . escreverDataHora($aRow[$aColumns[1]]) . "</div></a></center>";    
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["descricao"]) . "'>" . utf8_encode($aRow["descricao"]) . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[3]]) . "'>" . utf8_encode($aRow[$aColumns[3]]) . "</div>";
    if (!isset($_GET['idSin'])) {
        $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[5]]) . "'>" . utf8_encode($aRow[$aColumns[5]]) . "</div>";    
        $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[6]]) . "'>" . utf8_encode($aRow[$aColumns[6]]) . "</div>";   
        $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[7]]) . "'>" . utf8_encode($aRow[$aColumns[7]]) . "</div>";   
        $row[] = "<div id='formPQ' title='" . ($aRow[$aColumns[8]] == "32000" ? "Zero Km" : $aRow[$aColumns[8]]) . "'>" . ($aRow[$aColumns[8]] == "32000" ? "Zero Km" : $aRow[$aColumns[8]]) . "</div>";  
        $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[9]]) . "'>" . utf8_encode($aRow[$aColumns[9]]) . "</div>";   
        $row[] = "<div id='formPQ' title='" . escreverNumero($aRow[$aColumns[10]]) . "'>" . escreverNumero($aRow[$aColumns[10]]) . "</div>";       
    }
    if (!is_numeric($_GET['idCli']) && !isset($_GET['idSin'])) {
        $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[11]]) . "'>" . utf8_encode($aRow[$aColumns[11]]) . "</div>";       
        $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[12]]) . "'><a href='javascript:void(0)' onclick=\"AbrirBoxCli('" . utf8_encode($aRow["tipo_cli"]) . "'," . utf8_encode($aRow["id_fornecedores_despesas"]) . ")\">" . utf8_encode($aRow[$aColumns[12]]) . "</a></div>";
    }
    if (utf8_encode($aRow["data_aprov"]) == '') {
        $row[] = "<div id='formPQ' style=\"color:orange\" title=\"Pendente\"><center>Pendente</center></div>";
    } else {
        $row[] = "<div id='formPQ' style=\"color:#0066cc\" title=\"Concluída\"><center>Concluída</center></div>";
    }
    if (!isset($_GET['idSin'])) {
        $row[] = ($ckb_adm_cli_read_ == 0 ? "<center><a title='Excluir' href='Vistorias.php?Del=" . $aRow["id"] . "'><input name='' type='image' onClick=\"return confirm('Deseja deletar esse registro?')\" src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>" : "");
    }
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
