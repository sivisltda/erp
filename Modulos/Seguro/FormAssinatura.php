<?php
$assinatura = "";

function base64_to_jpeg($base64_string, $output_file) {
    $ifp = fopen($output_file, 'wb');
    $data = explode(',', $base64_string);
    fwrite($ifp, base64_decode($data[1]));
    fclose($ifp);
    return $output_file;
}

function getJson($contrato, $id) {
    $url = "https://" . $_SERVER['HTTP_HOST'] . "/Modulos/Seguro/Api/AmdApi.php";
    $header = [
        "Cache-Control: no-cache",
        "Content-Type: application/json"
    ];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"contrato\": \"" . $contrato . "\",\"id_contrato\": \"" . $id . "\",\"functionPage\": \"config\"}");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $output = curl_exec($ch);
    curl_close($ch);
    return json_decode($output);
}

if (!is_numeric($_GET['crt']) || !is_numeric($_GET['idContrato']) || !is_numeric($_GET['idCliente'])) {
    exit;
}

$config_contrato = getJson($_GET['crt'], $_GET['idContrato']);
       
if (isset($_POST['bntSave']) && strlen($_POST['txtId']) > 0) {
    $url = dirname(__FILE__) . "./../../Pessoas/" . $_GET['crt'] . "/Assinaturas/" . $_GET['idCliente'];
    if ($_GET['id'] != 0 && !is_dir($url)) {
        mkdir($url, 0777, true);
    }
    $id_planos = explode(",", $_GET['id']);
    $id_cont_ind = $config_contrato->id_contrato_individual;    
    if ($_GET['id'] == 0 && $_GET['idContrato'] == 0 && $_GET['idCliente'] == 0) {
        $url = dirname(__FILE__) . "./../../Pessoas/" . $_GET['crt'] . "/Empresa/Assinatura";
        if (!is_dir($url)) {
            mkdir($url, 0777, true);
        }
        base64_to_jpeg($_POST['txtId'], $url . "/responsavel.png");
    } else if (isset($_GET['id_vistoria'])) {
        base64_to_jpeg($_POST['txtId'], $url . "/" . "vistoria_" . $_GET['id_vistoria'] . ".png");      
        file_get_contents("https://" . $_SERVER['HTTP_HOST'] . "/util/ImpressaoPdf.php?id_vistoria=" . $_GET['id_vistoria'] . 
        "&id=" . $_GET['idContrato'] . "&idCliente=" . $_GET['idCliente'] . "&model=S&tpImp=F&crt=" . $_GET['crt'] . 
        "&NomeArq=Documentos&PathArq=../Modulos/Comercial/modelos/ModeloContrato.php&emp=1");
    } else if (isset($_GET['id_evento'])) {
        base64_to_jpeg($_POST['txtId'], $url . "/" . "evento_" . $_GET['id_evento'] . ".png");      
        file_get_contents("https://" . $_SERVER['HTTP_HOST'] . "/util/ImpressaoPdf.php?id_evento=" . $_GET['id_evento'] . 
        "&id=" . $_GET['idContrato'] . "&idCliente=" . $_GET['idCliente'] . "&model=S&tpImp=F&crt=" . $_GET['crt'] . 
        "&NomeArq=Documentos&PathArq=../Modulos/Comercial/modelos/ModeloContrato.php&emp=1");
    } else if (!isset($_GET['id_vistoria']) && !isset($_GET['id_evento']) && count($id_planos) > 1) {
        //Gerar contrato agrupado
        base64_to_jpeg($_POST['txtId'], $url . "/" . $_GET['id'] . "_" . $_GET['idContrato'] . ".png");                 
        file_get_contents("https://" . $_SERVER['HTTP_HOST'] . "/util/ImpressaoPdf.php?id_plan=" . $_GET['id'] . "&" .
        "idContrato=" . $_GET['idContrato'] . "&model=S&PathArq=../Modulos/Comercial/modelos/ModeloContrato.php&emp=1&tpImp=F&crt=" . $_GET['crt'] .
        "&NomeArq=Documentos&id=" . $_GET['id'] . "_" . $_GET['idContrato'] . "&idCliente=" . $_GET['idCliente']);
        //Gerar contratos individuais
        if ($id_cont_ind > 0) {                    
            for ($i = 0; $i < count($id_planos); $i++) {
                base64_to_jpeg($_POST['txtId'], $url . "/" . $id_planos[$i] . "_" . $id_cont_ind . ".png");                 
                file_get_contents("https://" . $_SERVER['HTTP_HOST'] . "/util/ImpressaoPdf.php?id_plan=" . $id_planos[$i] . "&" .
                "idContrato=" . $id_cont_ind . "&model=S&PathArq=../Modulos/Comercial/modelos/ModeloContrato.php&emp=1&tpImp=F&crt=" . $_GET['crt'] .
                "&NomeArq=Documentos&id=" . $id_planos[$i] . "_" . $id_cont_ind . "&idCliente=" . $_GET['idCliente']);
            }
        }
    } else if (!isset($_GET['id_vistoria']) && !isset($_GET['id_evento'])) {        
        base64_to_jpeg($_POST['txtId'], $url . "/" . $_GET['id'] . "_" . $_GET['idContrato'] . ".png");         
        file_get_contents("https://" . $_SERVER['HTTP_HOST'] . "/util/ImpressaoPdf.php?id_plan=" . $_GET['id'] . "&" .
        "idContrato=" . $_GET['idContrato'] . "&model=S&PathArq=../Modulos/Comercial/modelos/ModeloContrato.php&emp=1&tpImp=F&crt=" . $_GET['crt'] .
        "&NomeArq=Documentos&id=" . $_GET['id'] . "_" . $_GET['idContrato'] . "&idCliente=" . $_GET['idCliente']);
    }
}

if (file_exists(dirname(__FILE__) . "./../../Pessoas/" . $_GET['crt'] . "/Documentos/" . $_GET['idCliente'] . "/" . $_GET['id'] . "_" . $_GET['idContrato'] . ".pdf")) {
    header("Location: ./../../Pessoas/" . $_GET['crt'] . "/Documentos/" . $_GET['idCliente'] . "/" . $_GET['id'] . "_" . $_GET['idContrato'] . ".pdf");
} else if (file_exists(dirname(__FILE__) . "./../../Pessoas/" . $_GET['crt'] . "/Documentos/" . $_GET['idCliente'] . "/vistoria_" . $_GET['id_vistoria'] . ".pdf")) {
    header("Location: ./../../Pessoas/" . $_GET['crt'] . "/Documentos/" . $_GET['idCliente'] . "/vistoria_" . $_GET['id_vistoria'] . ".pdf");
} else if (file_exists(dirname(__FILE__) . "./../../Pessoas/" . $_GET['crt'] . "/Documentos/" . $_GET['idCliente'] . "/evento_" . $_GET['id_evento'] . ".pdf")) {
    header("Location: ./../../Pessoas/" . $_GET['crt'] . "/Documentos/" . $_GET['idCliente'] . "/evento_" . $_GET['id_evento'] . ".pdf");
} else if ($_GET['id'] == 0 && $_GET['idContrato'] == 0 && $_GET['idCliente'] == 0 && file_exists(dirname(__FILE__) . "./../../Pessoas/" . $_GET['crt'] . "/Empresa/Assinatura/responsavel.png")) {
    $assinatura = "./../../Pessoas/" . $_GET['crt'] . "/Empresa/Assinatura/responsavel.png";    
} else if (file_exists(dirname(__FILE__) . "./../../Pessoas/" . $_GET['crt'] . "/Assinaturas/" . $_GET['idCliente'] . "/" . $_GET['id'] . "_" . $_GET['idContrato'] . ".png")) {
    $assinatura = "./../../Pessoas/" . $_GET['crt'] . "/Assinaturas/" . $_GET['idCliente'] . "/" . $_GET['id'] . "_" . $_GET['idContrato'] . ".png";
} else if (file_exists(dirname(__FILE__) . "./../../Pessoas/" . $_GET['crt'] . "/Assinaturas/" . $_GET['idCliente'] . "/vistoria_" . $_GET['id_vistoria'] . ".png")) {
    $assinatura = "./../../Pessoas/" . $_GET['crt'] . "/Assinaturas/" . $_GET['idCliente'] . "/vistoria_" . $_GET['id_vistoria'] . ".png";
} else if (file_exists(dirname(__FILE__) . "./../../Pessoas/" . $_GET['crt'] . "/Assinaturas/" . $_GET['idCliente'] . "/evento_" . $_GET['id_evento'] . ".png")) {
    $assinatura = "./../../Pessoas/" . $_GET['crt'] . "/Assinaturas/" . $_GET['idCliente'] . "/evento_" . $_GET['id_evento'] . ".png";
}

$dvLeitura = "display: none;";
$dvAssinatura = "display: none;";
$dvImagem = "display: none;";
  
if ($config_contrato->marcar == 1 && strlen($assinatura) == 0) {
    $dvLeitura = "";
} else if ($config_contrato->assinatura == 1 && strlen($assinatura) == 0) {
    $dvAssinatura =  "";
} else if (strlen($assinatura) > 0) {
    $dvImagem = "";
} else {
    header("Location: ./../../util/ImpressaoPdf.php?id_plan=" . $_GET['id'] . "&crt=" . $_GET['crt'] . "&idContrato=" . $_GET['idContrato'] .
    "&model=S&tpImp=I&NomeArq=Contrato&PathArq=../Modulos/Comercial/modelos/ModeloContrato.php&emp=1");
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link rel="icon" type="image/ico" href="../../../favicon.ico"/>
        <link href="../../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../../css/main.css" rel="stylesheet">
        <link href="../../../css/formularios.css" rel="stylesheet" type="text/css" />
        <link href="../../../css/bootbox.css" rel="stylesheet" type="text/css"/>        
        <style type="text/css">
            div {
                margin-top:1em;
                margin-bottom:1em;
            }
            input {
                padding: .5em;
                margin: .5em;
            }
            #signatureparent {
                color:darkblue;
                background-color:lightgray;
                padding:20px;
            }
            #signature {
                border: 2px dotted black;
                background-color:white;
            }
            .btn-round {
                display: block;
                float: none;                
                padding: 15px;
                font-size: 30px;
                margin: 4px 2px;
                border-radius: 50%;
                width: 100%;
                color: white;
            }
            .btn-group-vertical {
                position: relative;
                float: right;
                height: 100px;
                margin-right: 50px;
                margin-top: 60px;
            }
            .table .thead-light th {
                color: #495057;
                background-color: #e9ecef;
                border-color: #dee2e6;
            }
            input[type=checkbox] {
                width:70px; 
                height:70px;
            }
            h1 {
                margin: 25px;
            }
            body, html {
                height: 100%;
            }       
            #dvLeitura {
                height: 100%;                
                margin-top:0em;
                margin-bottom:0em;
            }
            .line {
                margin-top:0em;
                margin-bottom:0em;
                font-size: xx-large;
                text-align: center;                
            }
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div id="dvLeitura" style="display: flex;flex-direction: column; <?php echo $dvLeitura; ?>">
            <div class="line" style="height: 50px; background-color: <?php echo $config_contrato->cor_primaria;?>;"></div>
            <div class="line" style="height: 300px; border-bottom: 1.5px solid #000;">
                <div style="float: left; width: 30%;">
                    <img style="width: 200px;" alt="Logo" src="<?php echo "./../../" . $config_contrato->logo;?>" class="img-fluid">                    
                </div>
                <div style="float: left; width: 70%; text-align: left;">
                    <p><?php echo $config_contrato->descricao;?></p>
                    <p><?php echo $config_contrato->cnpj;?></p>
                    <p><b><?php echo $config_contrato->nome;?></b></p>
                    <p><?php echo $config_contrato->telefone;?></p>
                </div>
            </div>
            <div class="line" style="height: calc(100% - 400px); padding: 5%; overflow: scroll;">
                <table class="table table-bordered" style="background: white; box-shadow: 0 15px 15px rgba(0,0,0,.4);">
                    <thead class="thead-light">
                        <tr>
                            <th><h1>Antes de assinar o contrato, <br>é necessário a leitura dos termos abaixo:</h1></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $pasta = "../../Pessoas/" . $_GET['crt'] . "/Regulamentos/";
                        if (file_exists($pasta)) {
                            $cdir = scandir($pasta);
                            foreach ($cdir as $key => $value) {
                                if (!in_array($value, array(".", ".."))) { ?>
                                    <tr>
                                        <td><a style="float: left;" href="<?php echo $pasta . $value; ?>" target="_blank"><h1><?php echo $value; ?></h1></a> <input style="float: right; margin: 30px;" type="checkbox"></td>
                                    </tr>                                    
                                    <?php
                                }
                            }
                        } ?>
                        <tr>
                            <td>
                                <button id="btnContinuar" style="float: right; padding: 10px;" class="btn btn-primary" disabled><h1>Continuar</h1></button>
                            </td>
                        </tr>
                    </tbody>
                </table>                
            </div>
            <div class="line" style="height: 50px; background-color: #e8e8e8;"><b><?php echo date("Y");?> © SIVIS TECNOLOGIA</b></div>
        </div>
        <div id="dvAssinatura" style="<?php echo $dvAssinatura;?>">
            <form action="FormAssinatura.php?id=<?php echo $_GET['id']; ?>&idContrato=<?php echo $_GET['idContrato']; ?>&crt=<?php echo $_GET['crt']; ?>&idCliente=<?php echo $_GET['idCliente']; ?><?php echo (isset($_GET['id_vistoria']) ? "&id_vistoria=" . $_GET['id_vistoria'] : ""); ?><?php echo (isset($_GET['id_evento']) ? "&id_evento=" . $_GET['id_evento'] : ""); ?>" name="frmEnviaDados" method="POST">
                <input id="txtId" name="txtId" value="" type="hidden"/>   
                <div class="btn-group btn-group-vertical">
                    <button id="btnSalvar" type="button" class="btn-round btn-primary"><span class="ico-ok"></span></button>
                    <button id="btnImprimir" type="button" class="btn-round" style="background-color: #0099cc;"><span class="ico-print"></span></button>
                    <button id="btnLimpar" type="button" class="btn-round btn-danger"><span class="ico-trash"></span></button>
                    <input id="bntSave" name="bntSave" type="submit" style="display: none;">
                </div>                
                <div id="content">
                    <div id="signatureparent"><div id="signature"></div></div>                    
                </div>                
                <center><img src="../../img/phoneturn.gif" style="margin-top: 400px;"></center>
            </form>
        </div>
        <div id="dvImagem" style="<?php echo $dvImagem;?>">
            <img src="<?php echo $assinatura; ?>" alt="imagem"/>
        </div>
        <script type="text/javascript" src="../../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="../../../js/jSignature/jSignature.js"></script>
        <script type="text/javascript" src="../../../js/jSignature/jSignature.UndoButton.js"></script>
        <script type="text/javascript" src="../../../js/jSignature/jSignature.SignHere.js"></script> 
        <script type='text/javascript' src='../../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>        
        <script>
            var $sigdiv = $("#signature").jSignature({'UndoButton': true});

            $("#btnSalvar").click(function () {
                bootbox.confirm('Confirma salvar a assinatura?', function (result) {
                    if (result === true) {
                        $("#btnSalvar").attr("disabled", true);
                        $("#loader").show();
                        $("#txtId").val($sigdiv.jSignature('getData', 'default'));
                        $("#bntSave").click();
                    }
                });
            });

            $("#btnLimpar").click(function () {
                $sigdiv.jSignature('reset');
            });

            $("#loader").hide();

            $('input:checkbox').change(function () {
                if ($('input:checkbox').length === $('input:checkbox:checked').length) {
                    $("#btnContinuar").removeAttr('disabled');
                } else {
                    $("#btnContinuar").attr('disabled', 'disabled');
                }
            });

            $("#btnContinuar").click(function () {
                <?php if ($config_contrato->assinatura == 1) { ?>
                    $("#dvLeitura").hide();
                    $("#dvAssinatura").show();
                <?php } else { ?>
                    window.location = "../../util/ImpressaoPdf.php?id_plan=<?php echo $_GET['id']; ?>&crt=<?php echo $_GET['crt']; ?>" + 
                    "&idContrato=<?php echo $_GET['idContrato']; ?>" +
                    "&model=S&tpImp=I&NomeArq=Contrato&PathArq=../Modulos/Comercial/modelos/ModeloContrato.php&emp=1";
                <?php } ?>
            });
            
            $("#btnImprimir").click(function () {
                window.open("../../util/ImpressaoPdf.php?id_plan=<?php echo $_GET['id']; ?>&crt=<?php echo $_GET['crt']; ?>" + 
                "&idContrato=<?php echo $_GET['idContrato']; ?>" +
                "&model=S&tpImp=I&NomeArq=Contrato&PathArq=../Modulos/Comercial/modelos/ModeloContrato.php&emp=1", '_blank');
            });
            
        </script>
    </body>
</html>