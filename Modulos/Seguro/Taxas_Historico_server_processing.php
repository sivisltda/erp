<?php

if (!isset($_GET['pdf'])) {
    include '../../Connections/configini.php';
}
$aColumns = array('id', 'valor', 'data', 'limite');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhere = "";
$colunas = "";
$filter = "";
$sOrder = " ORDER BY id desc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0 && intval($_GET['iSortCol_' . $i]) < 9) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY id desc";
    }
}

if ($_GET['Search'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        if ($i !== 6) {
            $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['Search']) . "%' OR ";
        }
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
        $sWhere .= $aColumns[$i] . " AND LIKE '%" . utf8_decode($_GET['sSearch_' . $i]) . "%' ";
    }
}

for ($i = 0; $i < count($aColumns); $i++) {
    $colunas .= $aColumns[$i] . ",";
}

$sQuery = "SELECT COUNT(*) total FROM sf_cotas_lote WHERE id > 0 " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row," . $colunas . "
(select count(m.id_mens) from sf_vendas_planos_mensalidade m where id_cota = id) tot_mens,
(select count(m.id_mens) from sf_vendas_planos_mensalidade m 
inner join sf_boleto_online_itens bi on bi.id_mens = m.id_mens
inner join sf_boleto_online b on b.id_boleto = bi.id_boleto
where m.id_cota = id and galaxy_id is not null) tot_bol
from sf_cotas_lote WHERE id > 0 " . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . 
" or 1=" . $imprimir . " " . $sOrder;
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $detalhe = "";
    $row = array();
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[2]]) . "'>" . escreverData($aRow[$aColumns[2]]) . "</div>";   
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[1]]) . "'>" . escreverNumero($aRow[$aColumns[1]], 1) . "</div>";   
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[3]]) . "'>" . escreverNumero($aRow[$aColumns[3]], 1) . "</div>";       
    $row[] = "<center>" . $aRow['tot_mens'] . "/" . $aRow['tot_bol'] . "</center>";       
    if ($aRow['tot_mens'] > $aRow['tot_bol']) {
        $detalhe .= "<li><a onclick=\"GerarBoletos('" . $aRow[$aColumns[0]] . "')\">Gerar Boletos</a></li>";
    }
    $detalhe .= "<li><a onclick=\"printDetalhes('" . $aRow[$aColumns[0]] . "')\">Detalhes</a></li>";    
    $detalhe .= "<li><a onclick=\"RemoverLote('" . $aRow[$aColumns[0]] . "')\">Excluir</a></li>";    
    $row[] = "<center><div style=\"text-align:left;\" class=\"btn-group\">
    <button style=\"height: 13px;background-color: gray;\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\"><span style=\"margin-top: 1px;\" class=\"caret\"></span></button>
    <ul style=\"left:-130px;\" class=\"dropdown-menu\">" . $detalhe . "</ul>
    </div></center>";
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
