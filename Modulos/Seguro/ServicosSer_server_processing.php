<?php

if (!isset($_GET['pdf'])) {
    include '../../Connections/configini.php';
}

$aColumns = array('data_tele', 'razao_social', 'contato', 'tipo', 'u.login_user de_pessoax', 'pendente', 'u2.login_user para_pessoax', 'proximo_contato', 'proximo_contato', 'placa', 'modelo');
$iTotal = 0;
$iFilteredTotal = 0;
$imprimir = 0;
$sWhere = "";
$sWhereX = "";
$sOrder = " ORDER BY data_tele desc ";
$sOrder2 = " ORDER BY data_tele desc ";

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i])], 1) . " " . $_GET['sSortDir_' . $i] . ", ";
            $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i])], 0) . " " . $_GET['sSortDir_' . $i] . ", ";
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY data_tele desc";
        $sOrder2 = " ORDER BY data_tele desc";
    }
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $sWhereX .= " and t.data_tele between " . valoresDataHoraUnico2($_GET['dti'] . " 00:00:00") . " and " . valoresDataHoraUnico2($_GET['dtf'] . " 23:59:59");
}

if (is_numeric($_GET['statusap'])) {
    $sWhereX .= " AND t.pendente = " . $_GET['statusap'];
}

if ($_GET['sSearch'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= prepareCollum($aColumns[$i], 0) . " LIKE '%" . utf8_decode($_GET['sSearch']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

$sQuery = "set dateformat dmy; SELECT COUNT(*) total 
    from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa
    LEFT JOIN sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = t.pessoa
    left join sf_fornecedores_despesas_veiculo v on v.id = t.id_veiculo
    LEFT JOIN sf_usuarios u2 on u2.id_usuario = t.para_pessoa
    where atendimento_servico = 1 and pessoa > 0 " . $sWhereX . $sWhere;
//echo $sQuery; exit;
$cur2 = odbc_exec($con, $sQuery);
while ($aRow = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $aRow['total'];
}

$sQuery = "set dateformat dmy; SELECT COUNT(*) total 
    from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa
    LEFT JOIN sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = t.pessoa
    left join sf_fornecedores_despesas_veiculo v on v.id = t.id_veiculo    
    LEFT JOIN sf_usuarios u2 on u2.id_usuario = t.para_pessoa
    where atendimento_servico = 1 and pessoa > 0 " . $sWhereX;
//echo $sQuery; exit;
$cur2 = odbc_exec($con, $sQuery);
while ($aRow = odbc_fetch_array($cur2)) {
    $iTotal = $aRow['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

odbc_exec($con, "set dateformat dmy;");
$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder2 . ") as row,id_tele,u.login_user as de_pessoax,u2.login_user as para_pessoax,data_tele,
pendente,proximo_contato,t.obs,de_pessoa,para_pessoa,razao_social,tipo,nome_fantasia,fd.id_fornecedores_despesas,atendimento_servico,nome_procedencia,
fd.endereco, fd.numero,fd.complemento,fd.bairro,cidade_nome, estado_sigla, fd.contato,
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = fd.id_fornecedores_despesas) telefone_contato,
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = fd.id_fornecedores_despesas) email_contato,
(select count(*) from sf_mensagens_telemarketing tm where tm.id_tele = t.id_tele) quantidade,especificacao, v.placa, v.modelo,
STUFF((SELECT distinct ',' + nome_especificacao from sf_telemarketing_especificacao tx inner join sf_telemarketing_item ti on tx.id_especificacao = ti.id_especificacao 
where ti.id_telemarketing = t.id_tele FOR XML PATH('')), 1, 1, '') nome_especificacao
from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa
LEFT JOIN sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = t.pessoa
left join sf_fornecedores_despesas_veiculo v on v.id = t.id_veiculo
LEFT JOIN sf_usuarios u2 on u2.id_usuario = t.para_pessoa
LEFT JOIN sf_procedencia on t.procedencia = id_procedencia and tipo_procedencia = 1
LEFT join tb_cidades on (fd.cidade = tb_cidades.cidade_codigo)
LEFT join tb_estados on (fd.estado = tb_estados.estado_codigo)
where atendimento_servico = 1 and fd.id_fornecedores_despesas is not null " . $sWhereX . $sWhere . ") as a " . 
(!isset($_GET['pdf']) ? "WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) : "") . " " . $sOrder;
//echo $sQuery1;exit;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $backColor = "";
    $pendente = "NÃO";
    $dias = "-";    
    if ($aRow['pendente'] == 1) {
        $backColor = " style='color:#08c' ";
        $pendente = utf8_encode("SIM");
        if ($aRow['proximo_contato'] != "") {
            $diferenca = dataDiff(date('Y-m-d'), escreverData($aRow['proximo_contato'], 'Y-m-d'), 'd');
            if ($diferenca > 0) {
                $dias = $diferenca . " dia(s) restantes";
            } else {
                if ($diferenca == 0) {
                    $dias = "Hoje";
                } else {
                    $backColor = " style='color:red' ";
                    $dias = (-1 * $diferenca) . " dia(s) atrasados";
                }
            }
        }
    }        
    $row[] = "<div id='formPQ'><center>" . $aRow['id_tele'] . "</center></div>";
    $row[] = "<center><a title=\"" . escreverDataHora($aRow['data_tele']) . "\" href=\"javascript:void(0)\" onClick=\"AbrirBoxServicos(" . $aRow['id_tele'] . "," . utf8_encode($aRow['id_fornecedores_despesas']) . ",0,2)\"><div id='formPQ' title='" . escreverDataHora($aRow['data_tele']) . "'>" . escreverDataHora($aRow['data_tele']) . "</div></a></center>";    
    if ($aRow['id_fornecedores_despesas'] == 0) {
        $cliente = "LIGAÇÃO AVULSA";
    } else {
        $cliente = utf8_encode($aRow['razao_social']);
        if (utf8_encode($aRow['nome_fantasia']) != "") {
            $cliente = $cliente . " (" . utf8_encode($aRow['nome_fantasia']) . ")";
        }
    }
    $row[] = "<div id='formPQ' " . $backColor . ">" . $cliente . "</div>";
    $row[] = "<div id='formPQ' " . $backColor . "><center>" . utf8_encode($aRow['placa']) . "</center></div>";
    $row[] = "<div id='formPQ' " . $backColor . ">" . utf8_encode($aRow['modelo']) . "</div>";
    $row[] = "<div id='formPQ' " . $backColor . "><center>" . utf8_encode($aRow['para_pessoax']) . "</center></div>";
    $row[] = "<div id='formPQ' " . $backColor . ">" . utf8_encode($aRow['nome_especificacao']) . "</div>";
    $row[] = "<center><div id='formPQ' " . $backColor . ">" . escreverDataHora($aRow['proximo_contato']) . "</div></center>";    
    $row[] = "<div id='formPQ' " . $backColor . "><center>" . $pendente . "</center></div>";
    $row[] = "<center><div id='formPQ' " . $backColor . ">" . $dias . "</div></center>";    
    
    $detalhe = "<a href=\"javascript:void(0)\" onClick=\"AbrirBoxTelemarketing(" . $aRow['id_tele'] . "," . utf8_encode($aRow['id_fornecedores_despesas']) . ",2,2)\"><img src=\"../../img/1365123843_onebit_323 copy.PNG\" width='16' height='16' title=\"Histórico de Ações\"/></a>";
    $action = (($aRow['pendente'] == 1 && $ckb_crm_fec_atend_ == 1) ? "<a href=\"javascript:void(0)\" onClick=\"AbrirBoxTelemarketing(" . $aRow['id_tele'] . "," . utf8_encode($aRow['id_fornecedores_despesas']) . ",1,2)\"><img src=\"../../img/close.png\" width='16' height='16' title=\"Encerrar Pendência\"/></a>" : "");
    $delete = (($ckb_crm_exc_atend_ == 1 && ($aRow['de_pessoa'] == $_SESSION["id_usuario"] || $aRow['para_pessoa'] == $_SESSION["id_usuario"])) ? "<a href=\"javascript:void(0)\" onClick=\"" . (is_numeric($_GET['tp']) ? "delServico" : "delChamado") . "(" . $aRow['id_tele'] . ");\"><img src=\"../../img/1365123843_onebit_33 copy.PNG\" width='16' height='16' title=\"Excluir\"/></a>" : "");
    $row[] = (($ckb_adm_cli_read_ == 0 || $aRow['tipo'] == "P") ? "<center style='display: flex;justify-content: space-around;'>" . $detalhe . $action . $delete . "</center>" : "");    
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);