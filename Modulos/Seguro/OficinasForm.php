<?php include "../../Connections/configini.php"; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
        <title>Cadastro de Oficina</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <link rel="stylesheet" href="../../js/plugins/jquery.ajax-combobox/dist/jquery.ajax-combobox.css">
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>   
    </head>
    <style>
        input {text-transform: uppercase;}
        #tblItensPag td{
            vertical-align: middle;
        }
        .dataTables_scrollHeadInner{
            width: 100% !important;
        }
        .dataTables_scrollHeadInner table.dataTable.no-footer{
            width: 100% !important;
        }
        #tblAlunosTurma_wrapper div.dataTables_scrollHeadInner table.dataTable.no-footer{
            width: 99% !important;
        }
    </style>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1 style="margin-top:8px">Oficina</h1>
                    </div>
                    <div class="row-fluid">
                        <form name="frmTurmasForm" id="frmTurmasForm">
                            <div class="span12">
                                <div class="block">
                                    <div id="formulario">
                                        <div class="data-fluid tabbable" style="margin-top:10px; margin-bottom:15px">
                                            <ul class="nav nav-tabs" style="margin-bottom:0px">
                                                <li class="active"><a href="#tab1" data-toggle="tab"><b>Dados Oficina</b></a></li>
                                            </ul>
                                            <div class="tab-content" style="overflow:initial; border:1px solid #ddd; border-top:0px; padding-top:10px; background:#F6F6F6;min-height: 616px;">
                                                <div class="tab-pane active" id="tab1" style="margin-bottom:5px">
                                                    <div id="divDadosTurma">
                                                        <div style="width:19%; float:left">
                                                            <input name="txtId" id="txtId" value="<?php echo $_GET['id']; ?>" type="hidden"/>
                                                            <input name="txtHorario" id="txtHorario" value="1" type="hidden"/>
                                                            <span><span style="color:red;">*</span>Descrição:</span>
                                                            <input type="text" id="txtDescricao" name="txtDescricao" class="input-medium"/>
                                                        </div>
                                                        <div style="width:12%; float:left;margin-left: 1%;">
                                                            <span><span style="color:red;">*</span>Início Em:</span>
                                                            <input type="text" id="txtDtInicio" name="txtDtInicio" class="datepicker inputCenter"/>
                                                        </div>
                                                        <div style="width:12%; float:left;margin-left: 1%;">
                                                            <span>Criada Em:</span>
                                                            <input type="text" id="txtDtCriacao" name="txtDtCriacao" value="<?php echo getData("T"); ?>" class="datepicker inputCenter" readonly/>
                                                        </div>
                                                        <div style="float: left;width: 139px;margin-left: 1%;margin-top: 20px;text-align: right;">
                                                            <input name="ckbInativo" id="ckbInativo" type="checkbox" value="1" class="input-medium"/> Desativar Oficina
                                                        </div>
                                                        <div style="clear:both; height:5px"></div>
                                                        <div style="width:30%; float:left;">
                                                            <span> Responsável:</span>
                                                            <select class="select" style="width:100%" id="txtProfessor" name="txtProfessor"> </select>
                                                        </div>
                                                        <div style="width:14%; float:left;margin-left: 1%;">
                                                            <span><span style="color:red;">*</span>Capacidade Total:</span>
                                                            <input type="text" id="txtCapacidade" maxlength="6" name="txtCapacidade" class="input-medium"/>
                                                        </div>
                                                        <div style="width:20%; float:left; margin-left:1%">
                                                            <span>Bairro:</span>
                                                            <select class="select" style="width:100%" id="txtAmbiente" name="txtAmbiente"> </select>
                                                        </div>
                                                        <div style="clear:both; height:5px"></div>
                                                        <div style="width:50%; float:left;">
                                                            <div style="width:100%; margin:10px 0">
                                                                <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Planos</span>
                                                                <hr style="margin:2px 0; border-top:1px solid #ddd">
                                                            </div>
                                                            <select name="itemsPlanos[]" multiple="multiple" id="mscPlanos">
                                                                <option value=""></option>
                                                            </select>
                                                        </div>
                                                        <div style="clear:both; height:5px"></div>
                                                        <div style="width:100%; margin:10px 0">
                                                            <div style="width: 70%;margin-top: 12px;float: left;">
                                                                <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Em manutenção</span>
                                                                <span><b>Qtd.: <span id="qtdAluno">0</span></b></span>
                                                            </div>
                                                        </div>
                                                        <div style="clear:both; height:5px"></div>
                                                        <div class="body" style="margin-left:0; padding:0">
                                                            <div class="content" style="min-height:153px; margin-top:0">
                                                                <table id="tblAlunosTurma" class="table" cellpadding="0" cellspacing="0" width="100%" style="display: table;">
                                                                    <thead>
                                                                        <tr>
                                                                            <th width="5%"></th>
                                                                            <th width="10%">Placa</th>
                                                                            <th width="20%">Cliente</th>
                                                                            <th width="20%">Modelo</th>
                                                                            <th width="10%">Evento</th>
                                                                            <th width="10%">Prazo</th>
                                                                            <th width="10%">Celular</th>
                                                                            <th width="15%">E-mail</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody></tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div style="clear:both; height:5px"></div>
                                                    </div>
                                                    <div class="toolbar bottom tar" style="background-color: #f6f6f6;border: 1px solid #DDD;">
                                                        <div class="data-fluid" style="margin: 0 9px 8px 0;">
                                                            <div class="btn-group" style="margin-top: 10px;">
                                                                <button class="btn btn-success" type="button" id="btnSave" name="btnSave" style="display: none;"><span class="ico-checkmark"></span> Gravar</button>
                                                                <button class="btn yellow" type="button" id="btnCancelar" name="btnCancelar" style="display: none;"><span class="ico-reply"></span> Cancelar</button>
                                                                <button class="btn  btn-success" type="button" title="Novo" name="btnNovo" id="btnNovo" hidden><span class="ico-plus-sign"> </span> Novo</button>
                                                                <button class="btn  btn-success" type="button" title="Alterar" name="btnAlterar" id="btnAlterar" hidden> <span class="ico-edit"> </span> Alterar</button>
                                                                <button class="btn red" type="button" title="Excluir" name="btnExcluir" id="btnExcluir" hidden><span class=" ico-remove"> </span> Excluir</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                                                
                                            </div>
                                            <div style="clear:both"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/highlight/jquery.highlight-4.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/faq.js'></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/jquery.creditCardValidator.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/amcharts.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/pie.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/serial.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/light.js"></script>
        <script type="text/javascript" src="../../js/moment.min.js"></script>
        <script type="text/javascript" src="../../js/jquery.cpfcnpj.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.ajax-combobox/dist/jquery.ajax-combobox.js"></script>
        <script type='text/javascript' src='../../js/plugins/multiselect/jquery.multi-select.min.js'></script>        
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript" src="js/OficinasForm.js"></script>
    </body>
    <?php odbc_close($con); ?>
</html>