<?php
include '../../Connections/configini.php';
if (is_numeric($_GET['Del'])) {
    odbc_exec($con, "DELETE FROM sf_veiculo_modelo where id = " . $_GET['Del']);
    echo "<script>window.top.location.href = 'Montadoras.php'; </script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>         
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Proteção<small>Modelos</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="margin-top: 15px;float:left;">
                                    <div style="float:left">
                                        <form method="POST">
                                            <button  class="button button-green btn-primary" type="button" onClick="AbrirBox(0, 1)"><span class="ico-file-4 icon-white"></span></button>
                                            <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir()" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                        </form>
                                    </div>
                                </div>
                                <div style="float:right;width: 55%;">                                    
                                    <div style="float:left;margin-left: 1%;width: 20%;">                                    
                                        <div width="100%">Montadoras:</div>
                                        <select id="txtMontadoras">
                                            <option value="null">Selecione</option>
                                            <?php $sql = "select id, descricao from sf_veiculo_marca ORDER BY id";
                                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id'] ?>"><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div style="float:left;margin-left: 1%;width: 39%;">                                    
                                        <div width="100%">Classificação:</div>
                                        <select id="txtClassificacao">
                                            <option value="null">Selecione</option>
                                            <?php $sql = "select id, descricao from sf_veiculo_classificacao ORDER BY id";
                                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id'] ?>"><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                            <?php } ?>
                                        </select>                                           
                                    </div>
                                    <div style="float:left;margin-left: 1%;width: 30%;">
                                        <div width="100%">Busca:</div>
                                        <input name="txtBusca"  id="txtBusca" maxlength="100" type="text" style="width:175px;" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title=""/>
                                    </div>
                                    <div style="float:left;margin-left: 1%;width: 7%;margin-top: 15px;">
                                        <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                    </div>
                                </div>                                
                            </div>                            
                        </div>                        
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Modelos</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblGrConta">
                            <thead>
                                <tr>
                                    <th width="5%"><center>Cód</center></th>
                                    <th>Descrição</th>
                                    <th width="20%">Montadora</th>
                                    <th width="20%">Classificação</th>
                                    <th width="5%"><center>Ação:</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>
        <script type="text/javascript" src="../../fancyapps/source/jquery.fancybox.js?v=2.1.4"></script>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>        
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }
            
            function finalFind(imp) {
                var retPrint = "";
                retPrint = '?imp=0';
                if (imp === 1) {
                    retPrint = '?imp=1';
                }
                if ($("#txtMontadoras").val() !== "null") {
                    retPrint = retPrint + '&txtMontadoras=' + $("#txtMontadoras").val();
                }
                if ($("#txtClassificacao").val() !== "null") {
                    retPrint = retPrint + '&txtClassificacao=' + $("#txtClassificacao").val();
                }                
                if ($("#txtBusca").val() !== "") {
                    retPrint = retPrint + '&txtBusca=' + $("#txtBusca").val();
                }
                return retPrint.replace(/\//g, "_");
            }
            
            function AbrirBox(id, opc) {
                if (opc === 1) {
                    abrirTelaBox("FormModelos.php", 282, 380);
                }else if (opc === 2) {
                    abrirTelaBox("FormModelos.php?id=" + id, 282, 380);
                }
            }
            
            function FecharBox(opc) {
                var oTable = $('#tblGrConta').dataTable();
                oTable.fnDraw(false);
                $("#newbox").remove();
            }
            
            function imprimir() {
                var pRel = "&NomeArq=" + "Modelos" +
                        "&lbl=Cod|Nome|Montadora|Classificação" +
                        "&siz=100|300|150|150" +
                        "&pdf=2" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/Seguro/Modelos_server_processing.php" + finalFind(1).replace("?", "&"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }
            
            function listaTable() {
                $('#tblGrConta').dataTable({
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "aoColumns": [{"bSortable": true},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false}],
                    "sAjaxSource": "Modelos_server_processing.php" + finalFind(0),
                    'oLanguage': {
                        'oPaginate': {
                            'sFirst': "Primeiro",
                            'sLast': "Último",
                            'sNext': "Próximo",
                            'sPrevious': "Anterior"
                        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                        'sLengthMenu': "Visualização de _MENU_ registros",
                        'sLoadingRecords': "Carregando...",
                        'sProcessing': "Processando...",
                        'sSearch': "Pesquisar:",
                        'sZeroRecords': "Não foi encontrado nenhum resultado"},
                    "sPaginationType": "full_numbers"
                });
            }            
            
            $(document).ready(function () {
                listaTable();
                $("#btnfind").click(function () {
                    $("#tblGrConta").dataTable().fnDestroy();
                    listaTable();
                });                
            });
        </script>       
        <?php odbc_close($con); ?>
    </body>
</html>