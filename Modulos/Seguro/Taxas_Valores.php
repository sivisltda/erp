<?php

require_once(__DIR__ . '/../../Connections/configini.php');
require_once(__DIR__ . '/../../util/util.php');

$i = 0;
$DateBegin = getData("B");
$DateEnd = getData("E");

if ($_GET['data'] != "") {
    $DateBegin = str_replace("_", "/", $_GET['data']);
    $DateEnd = str_replace("_", "/", $_GET['data']);
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => 0,
    "iTotalDisplayRecords" => 0,
    "aaData" => array()
);

if ($_GET['tipo'] == "1") {
    $C = escreverNumero(0, 1);
    $S = escreverNumero(0, 1);
    $P = escreverNumero(0, 1);    
    $query_temp = "set dateformat dmy;
    select venda, produto, sf_contas_movimento.descricao grupo, sf_produtos.descricao, v.historico_venda historico,    
    sf_produtos.tipo, razao_social, vp.data_pagamento, vp.tipo_documento,
    valor_pago, valor_total, (select isnull(sum(valor_total),0) from sf_vendas_itens where id_venda = venda) total_itens
    into #tempTable from sf_vendas v 
    inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = v.cliente_venda
    inner join sf_vendas_itens vi on v.id_venda = vi.id_venda
    inner join sf_produtos on conta_produto = produto        
    left join sf_contas_movimento on id_contas_movimento = sf_produtos.conta_movimento    
    inner join sf_venda_parcelas vp on venda = v.id_venda
    where vp.data_pagamento between " . valoresDataHora2($DateBegin, "00:00:00") . " and dateadd(day,-1,dateadd(month,1," . valoresDataHora2($DateEnd, "23:59:59") . "))
    and cov = 'V' and status = 'Aprovado' and vp.inativo in (0,2) and vp.somar_rel = 1";    
    //echo $query_temp; exit;    
    odbc_exec($con, $query_temp);
    $query = "";
    if (!isset($_GET['id_contas'])) {
        $query .= "select sum(total) total, tipo from (";
    }
    $query .= "select *, dbo.CALC_PROPORCAO(valor_pago,valor_total, total_itens) total from #tempTable";
    if (!isset($_GET['id_contas'])) {
        $query .= ") as x group by tipo";
    }
    //echo $query; exit;
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        if (!isset($_GET['id_contas'])) {        
            if ($RFP["tipo"] == "C") {
                $C = escreverNumero($RFP["total"], 1);
            } else if ($RFP["tipo"] == "S") {
                $S = escreverNumero($RFP["total"], 1);            
            } else if ($RFP["tipo"] == "P") {
                $P = escreverNumero($RFP["total"], 1);            
            }
        } else if (($RFP["tipo"] == "C" && $_GET['id_contas'] == "-1") || 
                   ($RFP["tipo"] == "S" && $_GET['id_contas'] == "-2") || 
                   ($RFP["tipo"] == "P" && $_GET['id_contas'] == "-3")) {
            $row = array();
            $row[] = escreverData($RFP["data_pagamento"]);
            $row[] = utf8_encode($RFP["razao_social"]);                        
            $row[] = utf8_encode($RFP["grupo"]);                        
            $row[] = utf8_encode($RFP["descricao"]);                        
            $row[] = utf8_encode($RFP["historico"]);                     
            $row[] = escreverNumero($RFP["total"], 1); 
            $output['aaData'][] = $row;
        }
    }
    if (!isset($_GET['id_contas'])) {
        $i = 3;    
        $row = array();
        $row[] = "<center><img src=\"./../../img/details_open.png\" data-id='0'></center>";
        $row[] = "<a href=\"javascript:void(0)\" onclick=\"AnaliticoValor(1, -1, 'RECEBIMENTO DE MENSALIDADES');\">RECEBIMENTO DE MENSALIDADES</a>";
        $row[] = $C;
        $output['aaData'][] = $row;
        $row = array();
        $row[] = "<center><img src=\"./../../img/details_disabled.png\" data-id='1'></center>";
        $row[] = "<a href=\"javascript:void(0)\" onclick=\"AnaliticoValor(1, -2, 'RECEBIMENTO DE SERVIÇOS');\">RECEBIMENTO DE SERVIÇOS</a>";    
        $row[] = $S;
        $output['aaData'][] = $row;
        $row = array();
        $row[] = "<center><img src=\"./../../img/details_disabled.png\" data-id='2'></center>";
        $row[] = "<a href=\"javascript:void(0)\" onclick=\"AnaliticoValor(1, -3, 'RECEBIMENTO DE PRODUTOS');\">RECEBIMENTO DE PRODUTOS</a>";    
        $row[] = $P;
        $output['aaData'][] = $row;
    }    
    $query = "set dateformat dmy;";
    if (!isset($_GET['id_contas'])) {    
        $query .= "select sum(valor_pago) total, id_contas_movimento, descricao from (";
    }
    $query .= "select sf_contas_movimento.id_contas_movimento, sf_grupo_contas.descricao grupo, sf_contas_movimento.descricao, historico, sf_lancamento_movimento_parcelas.valor_pago, data_pagamento, razao_social
    from sf_lancamento_movimento_parcelas 
    inner join sf_lancamento_movimento on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
    inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
    inner join sf_grupo_contas on id_grupo_contas = sf_contas_movimento.grupo_conta        
    inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas
    where sf_lancamento_movimento.status = 'Aprovado' and data_pagamento is not null AND sf_lancamento_movimento_parcelas.inativo = '0'
    and sf_contas_movimento.tipo = 'C'    
    " . (isset($_GET['id_contas']) ? " and sf_contas_movimento.id_contas_movimento = " . $_GET['id_contas'] : "") . "    
    and data_pagamento between " . valoresDataHora2($DateBegin, "00:00:00") . " and dateadd(day,-1,dateadd(month,1," . valoresDataHora2($DateEnd, "23:59:59") . ")) and sf_lancamento_movimento_parcelas.tipo_documento not in (12)    
    union all
    select sf_contas_movimento.id_contas_movimento, sf_grupo_contas.descricao grupo, sf_contas_movimento.descricao, historico, sf_solicitacao_autorizacao_parcelas.valor_pago, data_pagamento, razao_social
    from sf_solicitacao_autorizacao_parcelas 
    inner join sf_solicitacao_autorizacao on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
    inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
    inner join sf_grupo_contas on id_grupo_contas = sf_contas_movimento.grupo_conta        
    inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas 
    where sf_solicitacao_autorizacao.status = 'Aprovado' and data_pagamento is not null AND sf_solicitacao_autorizacao_parcelas.inativo = '0' 
    and sf_contas_movimento.tipo = 'C'
    " . (isset($_GET['id_contas']) ? " and sf_contas_movimento.id_contas_movimento = " . $_GET['id_contas'] : "") . "    
    and data_pagamento between " . valoresDataHora2($DateBegin, "00:00:00") . " and dateadd(day,-1,dateadd(month,1," . valoresDataHora2($DateEnd, "23:59:59") . ")) and sf_solicitacao_autorizacao_parcelas.tipo_documento not in (12)";
    if (!isset($_GET['id_contas'])) {    
        $query .= ") as x group by id_contas_movimento, descricao";
    }
    //echo $query; exit;    
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        if (!isset($_GET['id_contas'])) {
            $row[] = "<center><img src=\"./../../img/details_disabled.png\" data-id='" . utf8_encode($RFP["id_contas_movimento"]) . "'></center>";
            $row[] = "<a href=\"javascript:void(0)\" onclick=\"AnaliticoValor(1, " . utf8_encode($RFP["id_contas_movimento"]) . ", '" . utf8_encode($RFP["descricao"]) . "');\">" . utf8_encode($RFP["descricao"]) . "</a>";     
            $row[] = escreverNumero($RFP["total"], 1);
        } else {
            $row[] = escreverData($RFP["data_pagamento"]);
            $row[] = utf8_encode($RFP["razao_social"]);                        
            $row[] = utf8_encode($RFP["grupo"]);                        
            $row[] = utf8_encode($RFP["descricao"]);                        
            $row[] = utf8_encode($RFP["historico"]);                        
            $row[] = escreverNumero($RFP["valor_pago"], 1);            
        }
        $output['aaData'][] = $row;  
        $i++;
    }        
} else if ($_GET['tipo'] == "2") {
    $query_temp = "set dateformat dmy;";
    $query_temp .= "select cc.id id_centro_custo, gc.id_grupo_contas, c.id_contas_movimento, cc.descricao centro_custo, gc.descricao grupo, c.descricao, 
    v.historico_venda historico, data_pagamento, razao_social, sf_produtos.descricao produto,
    valor_pago valor_item, valor_total, (select isnull(sum(valor_total),0) from sf_vendas_itens where id_venda = venda) total_itens
    into #tempTable from sf_vendas v 
    inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = v.cliente_venda
    inner join sf_contas_movimento c on v.conta_movimento = c.id_contas_movimento
    inner join sf_grupo_contas gc on gc.id_grupo_contas = c.grupo_conta
    inner join sf_centro_custo cc on cc.id = gc.centro_custo    
    inner join sf_vendas_itens vi on v.id_venda = vi.id_venda
    inner join sf_produtos on conta_produto = produto
    inner join sf_venda_parcelas vp on venda = v.id_venda
    where vp.data_pagamento between " . valoresDataHora2($DateBegin, "00:00:00") . " and dateadd(day,-1,dateadd(month,1," . valoresDataHora2($DateEnd, "23:59:59") . ")) " . 
    (isset($_GET['id_centro_custo']) ? " and cc.id = " . $_GET['id_centro_custo'] : "") . 
    (isset($_GET['id_contas']) ? " and c.id_contas_movimento = " . $_GET['id_contas'] : "") . 
    " and cov = 'C' and status = 'Aprovado' and vp.inativo in (0,2) and vp.somar_rel = 1";   
    //echo $query_temp; exit;     
    odbc_exec($con, $query_temp);    
    $query = "set dateformat dmy;";
    if (!isset($_GET['id_centro_custo']) && !isset($_GET['id_contas']) && !isset($_GET['pdf'])) {
        $query .= "select sum(valor_pago) total, id_centro_custo, centro_custo from (";
    } else if (!isset($_GET['id_contas']) && !isset($_GET['pdf'])) {
        $query .= "select sum(valor_pago) total, id_contas_movimento, grupo, descricao from (";
    }
    $query .= "select id_centro_custo, id_grupo_contas, id_contas_movimento, centro_custo, grupo, descricao, historico, dbo.CALC_PROPORCAO(valor_item,valor_total, total_itens) valor_pago, 
    data_pagamento, razao_social collate SQL_Latin1_General_CP1_CI_AS + ' - ' + produto collate SQL_Latin1_General_CP1_CI_AS razao_social from #tempTable
    union all    
    select cc.id id_centro_custo, sf_grupo_contas.id_grupo_contas, sf_contas_movimento.id_contas_movimento, cc.descricao centro_custo, sf_grupo_contas.descricao grupo, sf_contas_movimento.descricao, historico, sf_lancamento_movimento_parcelas.valor_pago, data_pagamento, razao_social
    from sf_lancamento_movimento_parcelas 
    inner join sf_lancamento_movimento on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
    inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
    inner join sf_grupo_contas on id_grupo_contas = sf_contas_movimento.grupo_conta    
    inner join sf_centro_custo cc on cc.id = sf_grupo_contas.centro_custo
    inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas
    where sf_lancamento_movimento.status = 'Aprovado' and data_pagamento is not null AND sf_lancamento_movimento_parcelas.inativo = '0'
    and sf_contas_movimento.tipo = 'D' " . 
    (isset($_GET['id_centro_custo']) ? " and cc.id = " . $_GET['id_centro_custo'] : "") .
    (isset($_GET['id_contas']) ? " and sf_contas_movimento.id_contas_movimento = " . $_GET['id_contas'] : "") . 
    " and data_pagamento between " . valoresDataHora2($DateBegin, "00:00:00") . " and dateadd(day,-1,dateadd(month,1," . valoresDataHora2($DateEnd, "23:59:59") . ")) and sf_lancamento_movimento_parcelas.tipo_documento not in (12)
    union all
    select cc.id id_centro_custo, sf_grupo_contas.id_grupo_contas, sf_contas_movimento.id_contas_movimento, cc.descricao centro_custo, sf_grupo_contas.descricao grupo, sf_contas_movimento.descricao, historico, sf_solicitacao_autorizacao_parcelas.valor_pago, data_pagamento, razao_social
    from sf_solicitacao_autorizacao_parcelas 
    inner join sf_solicitacao_autorizacao on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
    inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
    inner join sf_grupo_contas on id_grupo_contas = sf_contas_movimento.grupo_conta 
    inner join sf_centro_custo cc on cc.id = sf_grupo_contas.centro_custo
    inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas 
    where sf_solicitacao_autorizacao.status = 'Aprovado' and data_pagamento is not null AND sf_solicitacao_autorizacao_parcelas.inativo = '0' 
    and sf_contas_movimento.tipo = 'D' " . 
    (isset($_GET['id_centro_custo']) ? " and cc.id = " . $_GET['id_centro_custo'] : "") .            
    (isset($_GET['id_contas']) ? " and sf_contas_movimento.id_contas_movimento = " . $_GET['id_contas'] : "") . 
    " and data_pagamento between " . valoresDataHora2($DateBegin, "00:00:00") . " and dateadd(day,-1,dateadd(month,1," . valoresDataHora2($DateEnd, "23:59:59") . ")) and sf_solicitacao_autorizacao_parcelas.tipo_documento not in (12)";
    if (!isset($_GET['id_centro_custo']) && !isset($_GET['id_contas']) && !isset($_GET['pdf'])) {
        $query .= ") as x group by id_centro_custo, centro_custo order by total desc";
    } else if (!isset($_GET['id_contas']) && !isset($_GET['pdf'])) {
        $query .= ") as x group by id_contas_movimento, grupo, descricao order by total desc";
    }
    //echo $query; exit;
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        if (!isset($_GET['id_centro_custo']) && !isset($_GET['id_contas']) && !isset($_GET['pdf'])) {
            $row[] = "<center><img src=\"./../../img/details_open.png\" data-id='" . utf8_encode($RFP["id_centro_custo"]) . "'></center>";
            $row[] = utf8_encode($RFP["centro_custo"]);     
            $row[] = escreverNumero($RFP["total"], 1);
        } else if (!isset($_GET['id_contas']) && !isset($_GET['pdf'])) {
            $row[] = utf8_encode($RFP["grupo"]);            
            $row[] = "<a href=\"javascript:void(0)\" onclick=\"AnaliticoValor(2, " . utf8_encode($RFP["id_contas_movimento"]) . ", '" . utf8_encode($RFP["descricao"]) . "');\">" . utf8_encode($RFP["descricao"]) . "</a>";     
            $row[] = escreverNumero($RFP["total"], 1);
        } else {
            $row[] = escreverData($RFP["data_pagamento"]);
            $row[] = utf8_encode($RFP["razao_social"]);                        
            $row[] = utf8_encode($RFP["grupo"]);     
            $row[] = utf8_encode($RFP["descricao"]);                        
            $row[] = utf8_encode($RFP["historico"]);                        
            $row[] = escreverNumero($RFP["valor_pago"], 1);
        }
        if (isset($_GET['pdf'])) {
            $row[] = utf8_encode($RFP['descricao']);
            $grp = array(utf8_encode($RFP['descricao']), ($RFP['descricao'] == "" ? "SEM GRUPO" : utf8_encode($RFP['descricao'])), 5);
            if (!in_array($grp, $grupoArray)) {
                $grupoArray[] = $grp;
            }
        }        
        $output['aaData'][] = $row;  
        $i++;
    }        
} else if ($_GET['tipo'] == "3") {
    $query = "set dateformat dmy;
    select sum(total_cred) total_cred, sum(total_debt) total_debt, id_banco, razao_social from (
    select 
    isnull(sum(case when sf_contas_movimento.tipo = 'C' then ROUND(valor_pago, 2) else 0 end),0) total_cred,
    isnull(sum(case when sf_contas_movimento.tipo = 'D' then ROUND(valor_pago, 2) else 0 end),0) total_debt, id_banco 
    from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
    inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
    inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas
    inner join sf_tipo_documento td on sf_lancamento_movimento_parcelas.tipo_documento = td.id_tipo_documento
    where sf_lancamento_movimento.status = 'Aprovado' and data_pagamento is not null AND sf_lancamento_movimento.empresa in (1) and td.id_tipo_documento not in (12) 
    AND sf_lancamento_movimento_parcelas.inativo = '0' and sf_contas_movimento.tipo in ('C', 'D') group by id_banco
    union all
    select 
    isnull(sum(case when sf_contas_movimento.tipo = 'C' then ROUND(valor_pago, 2) else 0 end),0) total_cred,
    isnull(sum(case when sf_contas_movimento.tipo = 'D' then ROUND(valor_pago, 2) else 0 end),0) total_debt, id_banco 
    from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
    on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
    inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
    inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas
    inner join sf_tipo_documento td on sf_solicitacao_autorizacao_parcelas.tipo_documento = td.id_tipo_documento
    where sf_solicitacao_autorizacao.status = 'Aprovado' AND sf_solicitacao_autorizacao.empresa in (1) and data_pagamento is not null  and td.id_tipo_documento not in (12) 
    AND sf_solicitacao_autorizacao_parcelas.inativo = '0' and sf_contas_movimento.tipo in ('C', 'D') group by id_banco
    union all
    select 
    isnull(sum(case when sf_vendas.cov = 'V' then ROUND(valor_pago, 2) else 0 end),0) total_cred,
    isnull(sum(case when sf_vendas.cov = 'C' then ROUND(valor_pago, 2) else 0 end),0) total_debt, id_banco 
    from sf_venda_parcelas inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda
    inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas
    inner join sf_tipo_documento td on sf_venda_parcelas.tipo_documento = td.id_tipo_documento
    where sf_vendas.status = 'Aprovado' AND sf_vendas.empresa in (1) and data_pagamento is not null and td.id_tipo_documento not in (12) 
    AND sf_venda_parcelas.inativo in (0,2) and sf_vendas.cov in ('V', 'C') group by id_banco
    union all
    select isnull(sum(valor_trans),0) total_cred, 0 total_debt, banco_destino_trans id_banco from sf_transferencia_banco 
    where sf_transferencia_banco.empresa in (1) group by banco_destino_trans    
    union all
    select 0 total_cred, isnull(sum(valor_trans),0) total_debt, banco_origem_trans id_banco from sf_transferencia_banco
    where sf_transferencia_banco.empresa in (1)  group by banco_origem_trans
    ) as x inner join sf_bancos on id_banco = id_bancos and ativo = 0
    group by id_banco, razao_social";    
    //echo $query; exit;    
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row[] = utf8_encode($RFP["razao_social"]);     
        $row[] = escreverNumero(($RFP["total_cred"] - $RFP["total_debt"]), 1);
        $output['aaData'][] = $row;  
        $i++;
    }
} else if ($_GET['tipo'] == "4") {
    $query_temp = "set dateformat dmy;";    
    $query_temp .= "select id_mens, id_plano, id_prod_plano, favorecido, razao_social, g.descricao grupo,
    data_pagamento, v.historico_venda, pp.valor_unico / (case when pp.parcela > 1 then pp.parcela else 1 end) valor_plano, 
    (select ISNULL(sum(case when b.tp_preco = 1 then (c.segMax * b.preco_venda)/100 
    when b.tp_preco = 2 then (e.valor * b.preco_venda)/100 else b.preco_venda end),0) from sf_vendas_planos_acessorios a 
    inner join sf_produtos b on b.conta_produto = a.id_servico_acessorio
    inner join sf_produtos c on c.conta_produto = pl.id_prod_plano
    inner join sf_vendas_planos d on d.id_plano = a.id_venda_plano
    inner join sf_fornecedores_despesas_veiculo e on e.id = d.id_veiculo    
    where a.id_venda_plano = pl.id_plano) valor_acessorios,
    dbo.CALC_PROPORCAO(vp.valor_pago, vi.valor_total, (select isnull(sum(valor_total),0) from sf_vendas_itens where id_venda = venda)) total_parcela,
    segMax, preco_venda preco_fipe, p.descricao into #tempTable
    from sf_venda_parcelas vp 
    inner join sf_vendas v on venda = v.id_venda
    inner join sf_vendas_itens vi on v.id_venda = vi.id_venda
    inner join sf_vendas_planos_mensalidade m on m.id_item_venda_mens = vi.id_item_venda
    inner join sf_vendas_planos pl on pl.id_plano = m.id_plano_mens
    inner join sf_fornecedores_despesas_veiculo veic on veic.id = pl.id_veiculo
    inner join sf_fornecedores_despesas c on c.id_fornecedores_despesas = pl.favorecido
    inner join sf_produtos p on p.conta_produto = pl.id_prod_plano
    left join sf_contas_movimento g on g.id_contas_movimento = p.conta_movimento
    inner join sf_produtos_parcelas pp on pp.id_parcela = m.id_parc_prod_mens
    where vp.data_pagamento between " . valoresDataHora2($DateBegin, "00:00:00") . " and dateadd(day,-1,dateadd(month,1," . valoresDataHora2($DateEnd, "23:59:59") . "))
    and cov = 'V' and v.status = 'Aprovado' and vp.inativo in (0,2) and vp.somar_rel = 1"; 
    //echo $query_temp; exit;     
    odbc_exec($con, $query_temp);    
    $query = "set dateformat dmy;";
    if (!isset($_GET['id_contas']) && !isset($_GET['pdf'])) {
        $query .= "select id_prod_plano, descricao, sum(ROUND((total_parcela * (valor_item * 100 / valor_mens))/100, 2)) valor_pago from (";
    } else {
        $query .= "select *, ROUND(((total_parcela * (valor_item * 100 / valor_mens))/100), 2) valor_pago from (";
    }    
    $query .= "select id_mens, " . (isset($_GET['id_contas']) ? "id_prod_plano" : "0") . " id_prod_plano, t.data_pagamento, t.razao_social, grupo, " . (isset($_GET['id_contas']) ? "p.descricao" : utf8_decode("'Cota de Participação'")) . " descricao, t.historico_venda, 
    total_parcela, (valor_plano + valor_acessorios) valor_mens, t.valor_plano valor_item
    from #tempTable t inner join sf_produtos p on p.conta_produto = t.id_prod_plano
    where preco_venda > 0 and total_parcela > 0 " . (isset($_GET['id_contas']) && $_GET['id_contas'] > 0 ? " and p.conta_produto = " . $_GET['id_contas'] : "") . "
    union all
    select id_mens, p.conta_produto, t.data_pagamento, t.razao_social, g.descricao grupo, p.descricao, t.historico_venda, 
    total_parcela, (valor_plano + valor_acessorios) valor_mens,
    case when tp_preco = 1 then ((p.preco_venda * t.segMax)/100) 
    when tp_preco = 2 then ((preco_fipe * p.preco_venda)/100)
    else p.preco_venda end valor_item
    from #tempTable t inner join sf_vendas_planos_acessorios a on t.id_plano = a.id_venda_plano
    inner join sf_produtos p on p.conta_produto = a.id_servico_acessorio
    left join sf_contas_movimento g on g.id_contas_movimento = p.conta_movimento
    where preco_venda > 0 and total_parcela > 0 " . (isset($_GET['id_contas']) ? " and p.conta_produto = " . $_GET['id_contas'] : "") . "";    
    if (!isset($_GET['id_contas']) && !isset($_GET['pdf'])) {
        $query .= ") as x group by id_prod_plano, descricao order by valor_pago desc";        
    } else {
        $query .= ") as x order by id_mens, valor_item desc";
    }
    //echo $query; exit;
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        if (!isset($_GET['id_contas']) && !isset($_GET['pdf'])) {
            $row[] = "";            
            $row[] = "<a href=\"javascript:void(0)\" onclick=\"AnaliticoValor(4, " . utf8_encode($RFP["id_prod_plano"]) . ", '" . utf8_encode($RFP["descricao"]) . "');\">" . utf8_encode($RFP["descricao"]) . "</a>";     
            $row[] = escreverNumero($RFP["valor_pago"], 1);
        } else {
            $row[] = escreverData($RFP["data_pagamento"]);
            $row[] = utf8_encode($RFP["razao_social"]);                        
            $row[] = utf8_encode($RFP["grupo"]);     
            $row[] = utf8_encode($RFP["descricao"]);                        
            $row[] = utf8_encode($RFP["historico_venda"]);                        
            $row[] = escreverNumero($RFP["valor_pago"], 1);
        }
        if (isset($_GET['pdf'])) {
            /*$row[] = utf8_encode($RFP['descricao']);
            $grp = array(utf8_encode($RFP['descricao']), ($RFP['descricao'] == "" ? "SEM GRUPO" : utf8_encode($RFP['descricao'])), 5);
            if (!in_array($grp, $grupoArray)) {
                $grupoArray[] = $grp;
            }*/
        }     
        $output['aaData'][] = $row;  
        $i++;
    }
}

$output['iTotalRecords'] = $i;
$output['iTotalDisplayRecords'] = $i;
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
} else {
    $output['aaTotal'][] = array(4, 4, "Sub Total", "Total", 0, 0);
    $output['aaGrupo'] = $grupoArray;
}
odbc_close($con);