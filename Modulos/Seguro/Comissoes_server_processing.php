<?php

if (isset($_GET["functionPage"]) && isset($_GET["loja"]) && is_numeric($_GET["loja"]) && in_array($_SERVER['REMOTE_ADDR'], ["148.72.177.101"])) {
    require_once(__DIR__ . '/../../Connections/funcoesAux.php');
    $hostname = getDataBaseServer(1);
    $database = "erp" . str_pad($_GET["loja"], 3, "0", STR_PAD_LEFT);
    $username = "erp" . str_pad($_GET["loja"], 3, "0", STR_PAD_LEFT);
    $password = "!Password123";
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());    
    $id_usuario = $_GET["id_usuario"];
    $_GET["txtTipo"] = 2;    
} else {
    require_once(__DIR__ . '/../../Connections/configini.php');
    $id_usuario = $_SESSION["id_usuario"];
}

$cont = 0;
$imprimir = 0;
$sWhere = "";
$id_usuario = "";
$login = "";
$id_funcionario = "0";
$comissao_plano = 0.00;
$comissao_plano_pri = 0.00;
$comissao_servico = 0.00;
$comissao_produto = 0.00;
$regraServico = "";
$regraProduto = "";
$tipos = [0, 1, 2, 3];

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (is_numeric($_GET['txtTipo']) && is_numeric($_GET['txtUsuario'])) {
    if ($_GET["txtTipo"] == "3") {
        $id_funcionario = $_GET['txtUsuario'];
    } else {
        $cur = odbc_exec($con, "select id_usuario, login_user, funcionario from sf_usuarios where id_usuario = " . $_GET['txtUsuario']);
        while ($RFP = odbc_fetch_array($cur)) {
            $id_usuario = $RFP['id_usuario'];
            $login = utf8_encode($RFP['login_user']);
            $id_funcionario = $RFP['funcionario'];
        }
    }
    $cur = odbc_exec($con, "select * from sf_fornecedores_despesas where id_fornecedores_despesas = " . valoresNumericos2($id_funcionario));
    while ($RFP = odbc_fetch_array($cur)) {
        if ($_GET["txtTipo"] == "4" && $RFP['tbcomiss_gere'] > 0) {
            $comissao_plano = "(vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) * " . ($RFP['tbcomiss_gere'] == 1 ? "p.valor_comissao_venda" : $RFP['comiss_gere']) . "/100)";
            $comissao_plano_pri = "(vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) * " . ($RFP['tbcomiss_gere'] == 1 ? "p.valor_comissao_venda" : $RFP['comiss_gerePri']) . "/100)";
        } else if ($RFP['tbcomiss_plano'] > 0) {
            $comissao_plano = "(vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) * " . ($RFP['tbcomiss_plano'] == 1 ? "p.valor_comissao_venda" : $RFP['comiss_plano']) . "/100)";
            $comissao_plano_pri = "(vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) * " . ($RFP['tbcomiss_plano'] == 1 ? "p.valor_comissao_venda" : $RFP['comiss_planoPri']) . "/100)";
        }
        if ($_GET["txtTipo"] != "4" && $RFP['tbcomiss_serv'] > 0) {
            $regraServico = ($RFP['tbcomiss_ser_tipo'] > 0 ? " and vi.vendedor_comissao = " . valoresNumericos2($id_funcionario) : "");
            $comissao_servico = "((vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio)) * " . ($RFP['tbcomiss_serv'] == 1 ? "p.valor_comissao_venda" : $RFP['comiss_serv']) . "/100)";
        }
        if ($_GET["txtTipo"] != "4" && $RFP['tbcomiss_prod'] > 0) {
            $regraProduto = ($RFP['tbcomiss_prod_tipo'] > 0 ? " and vi.vendedor_comissao = " . valoresNumericos2($id_funcionario) : "");
            $comissao_produto = "((vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio)) * " . ($RFP['tbcomiss_prod'] == 1 ? "p.valor_comissao_venda" : $RFP['comiss_prod']) . "/100)";
        }
    }
    if ($_GET["txtTipo"] == "0") { //Vendedor
        $sWhere .= " and v.vendedor in (" . valoresTexto2($login) . ")";
    } else if ($_GET["txtTipo"] == "1") { //Indicador
        $sWhere .= " and v.indicador in (" . valoresNumericos2($id_funcionario) . ")";
    } else if ($_GET["txtTipo"] == "2") { //Usuário Responsável
        $sWhere .= " and (c.id_user_resp in (" . valoresNumericos2($id_usuario) . ") or vi.vendedor_comissao in (" . valoresNumericos2($id_funcionario) . "))";
    } else if ($_GET["txtTipo"] == "3") { //Professor Responsável
        $sWhere .= " and (c.prof_resp in (" . valoresNumericos2($id_funcionario) . ") or vi.vendedor_comissao in (" . valoresNumericos2($id_funcionario) . "))";
    } else if ($_GET["txtTipo"] == "4") { //Gerência de Usuário Responsável
        $sWhere .= " and (c.id_user_resp in (select ud.id_usuario from sf_usuarios_dependentes ud
        inner join sf_usuarios u on u.id_usuario = ud.id_usuario
        INNER JOIN sf_fornecedores_despesas f ON u.funcionario = f.id_fornecedores_despesas  
        WHERE f.inativo = 0 AND dt_demissao IS NULL AND f.tipo = 'E' AND (tbcomiss_prod <> 0 or comiss_planoPri <> 0 or tbcomiss_serv <> 0 or tbcomiss_plano <> 0)
        and ud.id_fornecedor_despesas = " . valoresNumericos2($id_usuario) . ") or 
        vi.vendedor_comissao in (select f.id_fornecedores_despesas from sf_usuarios_dependentes ud
        inner join sf_usuarios u on u.id_usuario = ud.id_usuario
        INNER JOIN sf_fornecedores_despesas f ON u.funcionario = f.id_fornecedores_despesas  
        WHERE f.inativo = 0 AND dt_demissao IS NULL AND f.tipo = 'E' AND (tbcomiss_prod <> 0 or comiss_planoPri <> 0 or tbcomiss_serv <> 0 or tbcomiss_plano <> 0)
        and ud.id_fornecedor_despesas = " . valoresNumericos2($id_funcionario) . "))";
    }
    $cur = odbc_exec($con, "select id_usuario, login_user, funcionario from sf_usuarios where id_usuario = " . $_GET['txtUsuario']);
    while ($RFP = odbc_fetch_array($cur)) {
        $id_usuario = $RFP['id_usuario'];
        $login = utf8_encode($RFP['login_user']);
        $id_funcionario = $RFP['funcionario'];
    }
}

if (is_numeric($_GET['txtForma'])) {  
    if ($_GET['txtForma'] == "1") {
        $sWhere .= " and sa.status = 'Aprovado' ";
    } elseif ($_GET['txtForma'] == "2") {
        $sWhere .= " and sa.status is null ";
    } elseif ($_GET['txtForma'] == "3") {
        $sWhere .= " and sa.status = 'Aguarda' ";
    } elseif ($_GET['txtForma'] == "4") {
        $sWhere .= " and sa.status = 'Reprovado' ";
    }    
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $sWhere .= " and data_venda between " . valoresDataHoraUnico2(str_replace("_", "/", $_GET['dti']) . " 00:00:00") . " and " . valoresDataHoraUnico2(str_replace("_", "/", $_GET['dtf']) . " 23:59:59");
}

if (isset($_GET['filial']) && $_GET['filial'] != "null") {
    $sWhere .= " and c.empresa in (" . str_replace("\"", "'", str_replace(array("[", "]"), "", $_GET["filial"])) . ")";
}

if (isset($_GET['filtro']) && $_GET['filtro'] != "null") {
    $tipos = explode(",", $_GET['filtro']);
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => 0,
    "iTotalDisplayRecords" => 0,
    "aaData" => array()
);

$sQuery = "set dateformat dmy; ";
$sQuery .= "select id_venda, data_venda, vendedor, razao_social, empresa, sum(preco_plano) preco_plano, sum(preco_servico) preco_servico, 
sum(preco_produto) preco_produto, sum(comissao_plano_pri) comissao_plano_pri, sum(comissao_plano) comissao_plano, 
sum(comissao_servico) comissao_servico, sum(comissao_produto) comissao_produto, id_solicitacao_autorizacao, status from (";
$sQuery .= "select v.id_venda, v.data_venda, c.razao_social, c.empresa,  
case when p.tipo = 'C' then vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) else 0 end preco_plano,
case when p.tipo = 'S' then vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) else 0 end preco_servico, 
case when p.tipo = 'P' then vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) else 0 end preco_produto, 
case when p.tipo = 'C' and dbo.FU_PRIMEIRO_PAGAMENTO(vi.id_item_venda) > 0 then ( 
case when (pp.parcela < 1 or p.parcelar = 1) then $comissao_plano_pri else ($comissao_plano_pri)/pp.parcela end
) else 0 end comissao_plano_pri, 
case when p.tipo = 'C' and dbo.FU_PRIMEIRO_PAGAMENTO(vi.id_item_venda) = 0 then (
case when (pp.parcela < 1 or p.parcelar = 1) then $comissao_plano else ($comissao_plano)/pp.parcela end
) else 0 end comissao_plano,     
case when p.tipo = 'S' $regraServico then $comissao_servico else 0 end comissao_servico,
case when p.tipo = 'P' $regraProduto then $comissao_produto else 0 end comissao_produto, 
v.vendedor, v.indicador, c.id_user_resp, c.prof_resp, vi.vendedor_comissao comissionado, sa.id_solicitacao_autorizacao, sa.status
from sf_vendas v 
inner join sf_vendas_itens vi on vi.id_venda = v.id_venda
inner join sf_produtos p on p.conta_produto = vi.produto
left join sf_vendas_planos_mensalidade m on m.id_item_venda_mens = vi.id_item_venda
left join sf_produtos_parcelas pp on pp.id_parcela = m.id_parc_prod_mens
inner join sf_fornecedores_despesas c on c.id_fornecedores_despesas = v.cliente_venda
left join sf_comissao_historico ch on v.id_venda = ch.hist_venda
left join sf_solicitacao_autorizacao sa on sa.id_solicitacao_autorizacao = ch.hist_solicitacao_autorizacao 
and sa.fornecedor_despesa = $id_funcionario
where v.cov = 'V' and v.status = 'Aprovado' and p.tipo in ('C', 'P', 'S') " . $sWhere;
$sQuery .= ") as x group by id_venda, data_venda, vendedor, razao_social, empresa, id_solicitacao_autorizacao, status order by data_venda";
//echo $sQuery; exit;
$cur = odbc_exec($con, $sQuery);
while ($aRow = odbc_fetch_array($cur)) {
    if (isset($_GET["functionPage"])) {
        if (($aRow["comissao_plano_pri"] > 0 && in_array("0", $tipos)) || ($aRow["comissao_plano"] > 0 && in_array("1", $tipos)) || ($aRow["comissao_servico"] > 0 && in_array("2", $tipos)) || ($aRow["comissao_produto"] > 0 && in_array("3", $tipos))) {
            $valor = ($aRow["comissao_plano_pri"] + $aRow["comissao_plano"] + $aRow["comissao_servico"] + $aRow["comissao_produto"]);            
            $row["data_venda"] = escreverDataHora($aRow["data_venda"]);
            $row["id_venda"] = $aRow["id_venda"];
            $row["nome"] = utf8_encode($aRow['razao_social']);
            $row["parcela"] = ($aRow["prov"] + 1) . "/" . $aRow["parcela"];        
            $row["preco_plano"] = escreverNumero($aRow["preco_plano"], 1);
            $row["preco_servico"] = escreverNumero($aRow["preco_servico"], 1);
            $row["preco_produto"] = escreverNumero($aRow["preco_produto"], 1);
            $row["comissao_plano_pri"] = escreverNumero($aRow["comissao_plano_pri"], 1);
            $row["comissao_plano"] = escreverNumero($aRow["comissao_plano"], 1);        
            $row["comissao_servico"] = escreverNumero($aRow["comissao_servico"], 1);
            $row["comissao_produto"] = escreverNumero($aRow["comissao_produto"], 1);
            $row["valor_comissao"] = escreverNumero($valor, 1);        
            $row["produto"] = "";
            $row["status"] = (strlen($aRow['status']) > 0 ? $aRow['status'] : "Pendente");
            $output['aaData'][] = $row;
            $cont++;
        }
    } else {  
        $valor = ($aRow["comissao_plano_pri"] + $aRow["comissao_plano"] + $aRow["comissao_servico"] + $aRow["comissao_produto"]);
        $row = array();
        $row[] = "<input type=\"checkbox\" name=\"items[]\" val=\"" . escreverNumero($valor) . "\" value=\"" . $aRow["id_venda"] . "\" " . ($valor > 0 && !is_numeric($aRow['id_solicitacao_autorizacao']) ? "" : "disabled") . ">";
        $row[] = $aRow["id_venda"];
        $row[] = escreverDataHora($aRow["data_venda"]);
        $row[] = utf8_encode($aRow["vendedor"]);
        $row[] = utf8_encode($aRow["razao_social"]);
        $row[] = escreverNumero($aRow["preco_plano"], 1);
        $row[] = escreverNumero($aRow["preco_servico"], 1);
        $row[] = escreverNumero($aRow["preco_produto"], 1);
        $row[] = escreverNumero($aRow["comissao_plano_pri"], 1);
        $row[] = escreverNumero($aRow["comissao_plano"], 1);
        $row[] = escreverNumero($aRow["comissao_servico"], 1);
        $row[] = escreverNumero($aRow["comissao_produto"], 1);
        $row[] = escreverNumero($valor, 1);
        if (is_numeric($aRow['id_solicitacao_autorizacao'])) {
            $Solicitacao = "<a title=\"" . $aRow['status'] . "\" href=\"./../Contas-a-pagar/FormSolicitacao-de-Autorizacao.php?id=" . $aRow['id_solicitacao_autorizacao'] . "\">";
            $Solicitacao .= "<img src=\"../../img/";
            if ($aRow['status'] == 'Reprovado') {
                $Solicitacao .= "inativo";
            } else if ($aRow['status'] == 'Aprovado') {
                $Solicitacao .= "pago";
            } else if ($aRow['status'] == 'Aguarda') {
                $Solicitacao .= "apagar";
            } else {
                $Solicitacao .= "back";
            }
            $Solicitacao .= ".PNG\" value=\"\"></a>";
            $row[] = $Solicitacao;
        } else {
            $row[] = "<img src=\"../../img/dollar" . ($valor > 0 ? "" : "2") . ".png\" value=\"\">";    
        }
        $output['aaData'][] = $row;
        $cont++;
    }
}
$output["iTotalRecords"] = $cont;
$output["iTotalDisplayRecords"] = $cont;
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
