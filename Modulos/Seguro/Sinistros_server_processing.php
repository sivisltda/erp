<?php

if (!isset($_GET['pdf'])) {
    include '../../Connections/configini.php';
}

$aColumns = array('id_tele', 'placa', 'modelo', 'f.id_fornecedores_despesas', 'f.cnpj', 'f.razao_social', 'planos_status', 'f.tipo', 't.status',
'marca', 'ano_modelo', 'combustivel', 'valor', 'descricao', 'preco_venda', 'dt_fim', 'nome_especificacao');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "atendimento_servico = 2 ";
$sWhereY = "";
$sWhereZ = "";
$sWhere = "";
$colunas = "";
$filter = "and sf_vendas_planos.dt_cancelamento is null ";
$sOrder = " ORDER BY datediff(day, getdate(), proximo_contato) asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0 && intval($_GET['iSortCol_' . $i]) < 9) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY datediff(day, getdate(), proximo_contato) asc";
    }
}

if (is_numeric($_GET['idCli'])) {
    $sWhereX .= " AND (f.id_fornecedores_despesas = " . $_GET['idCli'] . " or proprietario = " . $_GET['idCli'] . ")";
    $filter .= "and dt_cancelamento is null ";
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $sWhereX .= " and data_tele between " . valoresDataHoraUnico2($_GET['dti'] . " 00:00:00") . " and " . valoresDataHoraUnico2($_GET['dtf'] . " 23:59:59");
    $sWhereY .= " and data_venda between " . valoresDataHoraUnico2($_GET['dti'] . " 00:00:00") . " and " . valoresDataHoraUnico2($_GET['dtf'] . " 23:59:59");
    $sWhereZ .= " and data_cad between " . valoresDataHoraUnico2($_GET['dti'] . " 00:00:00") . " and " . valoresDataHoraUnico2($_GET['dtf'] . " 23:59:59");
}

if (isset($_GET['statusap']) && $_GET['statusap'] != 'null') {
    $sWhereX .= " and (";
    $dataAux = explode(",", $_GET['statusap']);
    for ($i = 0; $i < count($dataAux); $i++) {
        $sWhereX .= ($i > 0 ? " or " : "");
        if ($dataAux[$i] == "0") {
            $sWhereX .= "(t.status = 'Aguarda')";
        } elseif ($dataAux[$i] == "1") {
            $sWhereX .= "(t.status = 'Aprovado' and t.pendente = 1 and t.id_tele not in (select ft.id_tele from sf_fornecedores_despesas_turmas ft where ft.id_tele is not null))";
        } elseif ($dataAux[$i] == "2") {
            $sWhereX .= "(t.status = 'Aprovado' and t.pendente = 1 and t.id_tele in (select ft.id_tele from sf_fornecedores_despesas_turmas ft where ft.id_tele is not null))";
        } elseif ($dataAux[$i] == "3") {
            $sWhereX .= "(t.status = 'Aprovado' and t.pendente = 0)";
        } elseif ($dataAux[$i] == "4") {
            $sWhereX .= "(t.status = 'Reprovado')";
        } elseif ($dataAux[$i] == "5") {
            $sWhereX .= "(t.status = 'Sindicancia')";
        } elseif ($dataAux[$i] == "6") {
            $sWhereX .= "(t.status = 'Aprovado' and t.pendente = 2)";
        } elseif ($dataAux[$i] == "7") {
            $sWhereX .= "(t.status = 'Aprovado' and t.pendente = 3)";
        } elseif ($dataAux[$i] == "8") {
            $sWhereX .= "(t.status = 'Aprovado' and t.pendente = 4)";
        }
    }
    $sWhereX .= ")";
}

if (isset($_GET['acao']) && $_GET['acao'] != 'null') {
    $sWhereX .= " and t.id_especificacao in (" . $_GET['acao'] . ")";
}

if (isset($_GET['oficina']) && $_GET['oficina'] != 'null') {
    $sWhereX .= " and t.id_tele in (select id_tele from sf_fornecedores_despesas_turmas where id_turma in (" . $_GET['oficina'] . "))";
}

if ($_GET['Search'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        if ($i !== 6) {
            $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['Search']) . "%' OR ";
        }
    }
    $sWhere .= "c.razao_social LIKE '%" . utf8_decode($_GET['Search']) . "%' OR ";
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
        $sWhere .= $aColumns[$i] . " AND LIKE '%" . utf8_decode($_GET['sSearch_' . $i]) . "%' ";
    }
}

for ($i = 0; $i < count($aColumns); $i++) {
    $colunas .= $aColumns[$i] . ",";
}

if (isset($_GET['getEstatisticas'])) {
    $row = array();    
    $sQueryA = " from sf_telemarketing t left join sf_fornecedores_despesas_veiculo v on t.id_veiculo = v.id
    inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = t.pessoa
    left join sf_vendas_planos on sf_vendas_planos.id_veiculo = v.id " . $filter . "left join sf_produtos on id_prod_plano = conta_produto  
    WHERE " . $sWhereX;    
    $sQuery = "set dateformat dmy; SELECT 
    isnull(sum(case when x.status = 'Aguarda' then 1 else 0 end),0) aguardando,
    isnull(sum(case when x.status = 'Sindicancia' then 1 else 0 end),0) sindicancia,
    isnull(sum(case when x.status = 'Reprovado' then 1 else 0 end),0) reprovado,
    isnull(sum(case when x.status = 'Aprovado' and x.pendente = 0 then 1 else 0 end),0) finalizado,    
    isnull(sum(case when x.status = 'Aprovado' and x.pendente = 2 then 1 else 0 end),0) cancelado,
    isnull(sum(case when x.status = 'Aprovado' and x.pendente = 3 then 1 else 0 end),0) subrogacao,
    isnull(sum(case when x.status = 'Aprovado' and x.pendente = 4 then 1 else 0 end),0) indeferido,
    isnull(sum(case when x.status = 'Aprovado' and x.pendente = 1 and x.total_manutencao = 0 then 1 else 0 end),0) em_aberto,
    isnull(sum(case when x.status = 'Aprovado' and x.pendente = 1 and x.total_manutencao > 0 then 1 else 0 end),0) em_reparo from (
    select t.status, t.pendente, (select count(id_turma) from sf_fornecedores_despesas_turmas ft where ft.id_tele = t.id_tele) total_manutencao
    " . $sQueryA . ") as x";
    //echo $sQuery; exit;
    $cur2 = odbc_exec($con, $sQuery);
    while ($RFP = odbc_fetch_array($cur2)) {
        $row["aguardando"] = $RFP['aguardando'];
        $row["sindicancia"] = $RFP['sindicancia'];
        $row["em_aberto"] = $RFP['em_aberto'];
        $row["em_reparo"] = $RFP['em_reparo'];
        $row["finalizado"] = $RFP['finalizado'];
        $row["cancelado"] = $RFP['cancelado'];
        $row["subrogacao"] = $RFP['subrogacao'];
        $row["indeferido"] = $RFP['indeferido'];
        $row["total"] = $RFP['aguardando'] + $RFP['sindicancia'] + $RFP['em_aberto'] + $RFP['em_reparo'] + $RFP['finalizado'] + $RFP['cancelado'] + $RFP['subrogacao'] + $RFP['indeferido'];
    }        
    $n_vendas = 0;
    $tot_vendas = 0;
    $tot_previsto = 0;
    $sQueryB = "set dateformat dmy;select ISNULL((select SUM(valor_total) from sf_vendas_itens v inner join sf_produtos c on v.produto = c.conta_produto 
    where tipo in ('P','S','C') and id_venda = sf_vendas.id_venda),0) as total
    from sf_vendas left join sf_filiais on sf_filiais.id_filial = sf_vendas.empresa 
    inner join sf_fornecedores_despesas f on sf_vendas.cliente_venda = f.id_fornecedores_despesas where cov = 'C' and status = 'Aprovado' " . 
    $sWhereY . " AND cod_pedido in (select t.id_tele " . $sQueryA . ")";
    //echo $sQueryB; exit;
    $cur3 = odbc_exec($con, $sQueryB);
    while ($RFP = odbc_fetch_array($cur3)) {
        $n_vendas++;
        $tot_vendas += $RFP['total'];    
    }    
    $sQueryC = "set dateformat dmy;select SUM(quantidade * valor) total 
    from sf_telemarketing_previsao WHERE id_tele in (select t.id_tele " . $sQueryA . ") " . $sWhereZ;
    //echo $sQueryC; exit;
    $cur4 = odbc_exec($con, $sQueryC);
    while ($RFP = odbc_fetch_array($cur4)) {
        $tot_previsto += $RFP['total'];    
    }
    $row["num_compras"] = $n_vendas;
    $row["tot_compras"] = escreverNumero($tot_vendas, 1);
    $row["tot_previsto"] = escreverNumero($tot_previsto, 1);
    $row["tic_compras"] = escreverNumero(($n_vendas > 0 ? ($tot_vendas/$n_vendas) : 0), 1);
    echo json_encode($row); exit;    
}

$sQuery = "set dateformat dmy; SELECT COUNT(*) total from sf_telemarketing t 
left join sf_fornecedores_despesas_veiculo v on t.id_veiculo = v.id
inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = t.pessoa
left join sf_fornecedores_despesas c on c.id_fornecedores_despesas = t.proprietario
left join sf_vendas_planos on sf_vendas_planos.id_veiculo = v.id " . $filter . "left join sf_produtos on id_prod_plano = conta_produto
left join sf_telemarketing_especificacao on sf_telemarketing_especificacao.id_especificacao = t.id_especificacao    
WHERE " . $sWhereX . " " . $sWhere;
//echo $sQuery; exit;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "set dateformat dmy; SELECT COUNT(*) total from sf_telemarketing t 
left join sf_fornecedores_despesas_veiculo v on t.id_veiculo = v.id
inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = t.pessoa
left join sf_fornecedores_despesas c on c.id_fornecedores_despesas = t.proprietario
left join sf_vendas_planos on sf_vendas_planos.id_veiculo = v.id " . $filter . "left join sf_produtos on id_prod_plano = conta_produto 
left join sf_telemarketing_especificacao on sf_telemarketing_especificacao.id_especificacao = t.id_especificacao    
WHERE " . $sWhereX;
//echo $sQuery; exit;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "set dateformat dmy; SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row, " . $colunas . " de_pessoa, para_pessoa, pendente, proximo_contato, data_tele,
c.id_fornecedores_despesas id_cliente,c.cnpj cnpj_cliente,c.razao_social razao_social_cliente,c.tipo tipo_cliente,
(select descricao from sf_veiculo_tipos where id = tipo_veiculo) tipo_veiculo,
(select ISNULL(SUM(valor_total),0) total from sf_vendas v inner join sf_vendas_itens vi on v.id_venda = vi.id_venda where cov = 'C' and status = 'Aprovado' AND cod_pedido = id_tele) total_compras,    
(select sum(case when p.tp_preco = 1 then (px.segMax * p.preco_venda)/100 
when p.tp_preco = 2 then (v.valor * p.preco_venda)/100 else p.preco_venda end)
from sf_vendas_planos vp inner join sf_vendas_planos_acessorios vpa on vp.id_plano = vpa.id_venda_plano
inner join sf_fornecedores_despesas_veiculo v on v.id = vp.id_veiculo
inner join sf_produtos p on p.conta_produto = vpa.id_servico_acessorio
inner join sf_produtos px on px.conta_produto = vp.id_prod_plano
WHERE p.tipo='A' AND vpa.id_venda_plano = sf_vendas_planos.id_plano) adicionais,
(select count(id_turma) from sf_fornecedores_despesas_turmas ft where ft.id_tele = t.id_tele) total_manutencao,
(select max(descricao) from sf_fornecedores_despesas_turmas A inner join sf_turmas B on A.id_turma = B.id_turma where A.id_tele = t.id_tele) oficina,
(select max(data_cadastro) from sf_fornecedores_despesas_turmas A inner join sf_turmas B on A.id_turma = B.id_turma where A.id_tele = t.id_tele) oficina_entrada,
(select sum(quantidade * valor) from sf_telemarketing_previsao tpp where tpp.id_tele = t.id_tele) previsao_total,
STUFF((SELECT distinct '|' + CONVERT(VARCHAR, data, 103) + ' - ' + de.login_user + ' para ' + para.login_user  + ' - ' + t1.mensagem
FROM sf_mensagens_telemarketing t1 INNER JOIN sf_usuarios de ON de.id_usuario = t1.id_usuario_de
LEFT JOIN sf_usuarios para ON para.id_usuario = t1.id_usuario_para
WHERE t1.id_tele = t.id_tele FOR XML PATH('')), 1, 1, '') as historico
from sf_telemarketing t left join sf_fornecedores_despesas_veiculo v on t.id_veiculo = v.id
inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = t.pessoa
left join sf_fornecedores_despesas c on c.id_fornecedores_despesas = t.proprietario
left join sf_vendas_planos on sf_vendas_planos.id_veiculo = v.id " . $filter . " left join sf_produtos on id_prod_plano = conta_produto 
left join sf_telemarketing_especificacao on sf_telemarketing_especificacao.id_especificacao = t.id_especificacao
WHERE " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1=" . $imprimir . " " . $sOrder;
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $backColor = "";
    $pendente = "NÃO";
    $proxContato = "";
    $dias = "-";
    if ($aRow['proximo_contato'] != null) {
        $proxContato = escreverDataHora($aRow['proximo_contato']);
    }
    if ($aRow['pendente'] == 1) {
        $backColor = " style='color: #08c' ";
        $pendente = utf8_encode("SIM");
        if ($aRow['proximo_contato'] != null) {
            $diferenca = dataDiff(date('Y-m-d'), escreverData($aRow['proximo_contato'], 'Y-m-d'), 'd');
            if ($diferenca > 0) {
                $dias = $diferenca . " dia(s) restantes";
            } else if ($diferenca == 0) {
                $dias = "Hoje";
            } else {
                $backColor = " style='color: red' ";
                $dias = (-1 * $diferenca) . " dia(s) atrasados";
            }
        }
    }    
    $row[] = "<center><span title=\"" . (strlen($aRow[$aColumns[6]]) > 0 ? $aRow[$aColumns[6]] : "Cancelado") . "\" class=\"label-" . (strlen($aRow[$aColumns[6]]) > 0 ? $aRow[$aColumns[6]] : "Cancelado") . "\" style=\"display: inline-block;width: 13px;height: 13px;border-radius: 50%!important;\"></span></center>";            
    $row[] = "<center><a href='javascript:void(0)' onClick='AbrirBoxSinistro(" . utf8_encode($aRow[$aColumns[0]]) . ",0)'><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[0]]) . "'>" . str_pad($aRow[$aColumns[0]], 8, "0", STR_PAD_LEFT) . "</div></a></center>";    
    $row[] = "<center><div id='formPQ' " . $backColor . " title='" . strtoupper($aRow[$aColumns[1]]) . "'>" . strtoupper($aRow[$aColumns[1]]) . "</div></center>";    
    if (is_numeric($_GET['idCli'])) {
        $row[] = "<div id='formPQ' " . $backColor . " title='" . utf8_encode($aRow[$aColumns[9]]) . "'>" . utf8_encode($aRow[$aColumns[9]]) . "</div>";        
    }
    $row[] = "<div id='formPQ' " . $backColor . " title='" . utf8_encode($aRow[$aColumns[2]]) . "'>" . formatNameCompact(utf8_encode($aRow[$aColumns[2]])) . "</div>";
    $row[] = "<div id='formPQ' " . $backColor . " title='" . escreverDataHora($aRow["data_tele"]) . "'>" . escreverDataHora($aRow["data_tele"]) . "</div>";              
    $row[] = "<center><div id='formPQ' " . $backColor . " title='" . $proxContato . "'>" . $proxContato . "</div></center>";
    $row[] = "<center><div id='formPQ' " . $backColor . " title='" . $dias . "'>" . $dias . "</div></center>";        
    if (!is_numeric($_GET['idCli'])) {
        $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["razao_social"]) . "'><a href='javascript:void(0)' onclick=\"AbrirBoxCli('" . utf8_encode($aRow["tipo"]) . "'," . utf8_encode($aRow["id_fornecedores_despesas"]) . ")\">" . utf8_encode($aRow["razao_social"]) . "</a></div>";
        if (is_numeric($aRow["id_cliente"]) && $aRow["id_cliente"] > 0) {
            $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["razao_social_cliente"]) . "'><a href='javascript:void(0)' onclick=\"AbrirBoxCli('" . utf8_encode($aRow["tipo_cliente"]) . "'," . utf8_encode($aRow["id_cliente"]) . ")\">" . utf8_encode($aRow["razao_social_cliente"]) . "</a></div>";    
        } else {
            $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["razao_social"]) . "'><a href='javascript:void(0)' onclick=\"AbrirBoxCli('" . utf8_encode($aRow["tipo"]) . "'," . utf8_encode($aRow["id_fornecedores_despesas"]) . ")\">" . utf8_encode($aRow["razao_social"]) . "</a></div>";
        }        
        $row[] = "<div id='formPQ' " . $backColor . " title='" . utf8_encode($aRow[$aColumns[16]]) . "'>" . utf8_encode($aRow[$aColumns[16]]) . "</div>";
        $row[] = "<div id='formPQ' " . $backColor . " title='" . utf8_encode($aRow["oficina"]) . "'>" . utf8_encode($aRow["oficina"]) . "</div>";
        $row[] = "<div id='formPQ' " . $backColor . " title='" . escreverNumero($aRow["previsao_total"], 1) . "'>" . escreverNumero($aRow["previsao_total"], 1) . "</div>";
    }
    $status = "";    
    $color = "style=\"color:";    
    if ($aRow["status"] == "Aguarda") {
        $status = "Aguardando";        
        $color = $color . "#bc9f00\"";
    } else if ($aRow["status"] == "Reprovado") {
        $status = "Reprovado";
        $color = $color . "#dc5039\"";
    } else if ($aRow["status"] == "Sindicancia") {
        $status = "Sindicância";
        $color = $color . "#e09f4f\"";
    } else if ($aRow["status"] == "Aprovado" && $aRow['pendente'] == 1 && $aRow['total_manutencao'] == 0) {
        $status = "Em Aberto";
        $color = $color . "#0066cc\"";
    } else if ($aRow["status"] == "Aprovado" && $aRow['pendente'] == 1 && $aRow['total_manutencao'] > 0) {
        $status = "Em Reparo";
        $color = $color . "black\"";
    } else if ($aRow["status"] == "Aprovado" && $aRow['pendente'] == 0) {
        $status = "Finalizado";        
        $color = $color . "#68AF27\"";
    } else if ($aRow["status"] == "Aprovado" && $aRow['pendente'] == 2) {
        $status = "Cancelado";        
        $color = $color . "#dc5039\"";
    } else if ($aRow["status"] == "Aprovado" && $aRow['pendente'] == 3) {
        $status = "Sub-Rogação";        
        $color = $color . "black\"";
    } else if ($aRow["status"] == "Aprovado" && $aRow['pendente'] == 4) {
        $status = "Indeferido";        
        $color = $color . "#dc5039\"";
    }    
    //Em Reparo    
    $row[] = "<div id='formPQ' " . $color . " title=\"" . $status . "\"><center>" . $status . "</center></div>";    
    if (!is_numeric($_GET['idCli'])) {        
        $detalhe = "<a href=\"javascript:void(0)\" onClick=\"AbrirBoxSinistro(" . utf8_encode($aRow[$aColumns[0]]) . ",1)\"><img src=\"../../img/1365123843_onebit_323 copy.PNG\" width='16' height='16' title=\"Histórico de Ações\"/></a>";
        $action = "";
        $delete = "";
        if ($aRow['pendente'] == 1) {
            $action = "<a href=\"javascript:void(0)\" onClick=\"AbrirBoxSinistro(" . utf8_encode($aRow[$aColumns[0]]) . ",2)\"><img src=\"../../img/close.png\" width='16' height='16' title=\"Encerrar Pendência\"/></a>";
        }
        if ($aRow['de_pessoa'] == $_SESSION["id_usuario"] || $aRow['para_pessoa'] == $_SESSION["id_usuario"]) {
            $delete = "<a title='Excluir' href='Sinistros.php?Del=" . $aRow[$aColumns[0]] . "' onClick=\"return confirm('Deseja deletar esse registro?')\"><img src=\"../../img/1365123843_onebit_33 copy.PNG\" width='16' height='16' title=\"Excluir\"/></a>";
        }
        if ($ckb_pro_exc_vei_ > 0) {
            $row[] = "<center style='display: flex;justify-content: space-around;'>" . $detalhe . $action . $delete . "</center>";
        } else {
            $row[] = "";
        }
    }
    if (isset($_GET['tpImp']) && $_GET['tpImp'] == "E") {
        $row[] = utf8_encode($aRow["historico"]);
        $row[] = utf8_encode($aRow["tipo_veiculo"]);
        $row[] = escreverNumero($aRow["total_compras"], 1);
    }
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
} else {
    $output['aaTotal'][] = array(9, 9, "Total", "", 0, 0);
}
odbc_close($con);