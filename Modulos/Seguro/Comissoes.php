<?php 
include '../../Connections/configini.php'; 
$DateBegin = getData("B");
$DateEnd = getData("E");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link href="../../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <style>
            #tbComissoes td { padding: 5px 5px; }
            #tbComissoes td:nth-child(1) { text-align:center }
            #tbComissoes td:nth-child(6) { text-align:right; background-color: #9EF7D8; }
            #tbComissoes td:nth-child(7) { text-align:right; background-color: #f0eacf; }
            #tbComissoes td:nth-child(8) { text-align:right; background-color: #a3ddec; }
            #tbComissoes td:nth-child(9) { text-align:right; background-color: #9EF7D8; }
            #tbComissoes td:nth-child(10) { text-align:right; background-color: #9EF7D8; }
            #tbComissoes td:nth-child(11) { text-align:right; background-color: #f0eacf; }
            #tbComissoes td:nth-child(12) { text-align:right; background-color: #a3ddec; }
            #tbComissoes td:nth-child(13) { text-align:right; background-color: LightPink; font-weight: bold; }            
            #tbComissoes td:nth-child(14) { text-align:center; }            
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">      
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Proteção<small>Comissões</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="float:left; width: 100%;">
                                    <div style="float:left">
                                        <form method="POST">
                                            <button class="button button-green btn-primary" type="button" onClick="AbrirBox()"><span class="ico-file-4 icon-white"></span> Gerar Comissão</button>
                                            <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir('I');" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                            <button id="btnExcel" class="button button-blue btn-primary" type="button" onclick="imprimir('E');" title="Exportar Excel"><span class="ico-download-3"></span></button>
                                        </form>
                                    </div>                                                                         
                                </div>
                                <div style="float:left;width: 100%;">
                                    <div style="float: left;">
                                        <label for="txt_dt_begin">De</label>                                
                                        <input type="text" maxlength="7" style="width:80px; height: 31px;" class="datepicker" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial"/>
                                    </div>
                                    <div style="float: left;margin-left: 0.5%;">
                                        <label for="txt_dt_end">até</label>                                    
                                        <input type="text" maxlength="7" style="width:80px; height: 31px;" class="datepicker" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final"/>                             
                                    </div>                                    
                                    <div style="float:left; width: 12%; margin-left: 0.5%;">
                                        <label>Tipo:</label>
                                        <select id="txtFiltro" name="txtFiltro[]" multiple="multiple" class="select" style="width:100%" class="input-medium">                                            
                                            <option value="0" selected>C.Plano 1ª</option>
                                            <option value="1">C.Plano</option>
                                            <option value="2">C.Serv.</option>
                                            <option value="3">C.Prod.</option>
                                        </select>
                                    </div>                                        
                                    <div style="float:left; width: 16%; margin-left: 0.5%;">
                                        <label>Selecione a Filial:</label>
                                        <select id="txtFilial" name="txtFilial[]" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                            <?php $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo utf8_encode($RFP['id_filial']); ?>"><?php echo utf8_encode($RFP['numero_filial']) . " - " . utf8_encode($RFP['Descricao']); ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div style="float:left; width: 10%; margin-left: 0.5%;">                                    
                                        <div width="100%">Grupo:</div>
                                        <select id="txtTipo">
                                            <option value="null">Selecione</option>                                               
                                            <?php if ($comissao_forma_ == 0) { ?>
                                                <option value="0">Cadastro (Vendas)</option>
                                                <option value="1">Indicador (Vendas)</option>
                                                <option value="2" selected>Usuário Responsável (Cliente)</option>
                                            <?php if ($_SESSION["mod_emp"] != 3) { ?>
                                                <option value="3">Funcionário Responsável (Cliente)</option>
                                                <option value="4">Gerência Usuário Resp. (Cliente)</option>
                                            <?php }} else { ?>
                                                <option value="5" selected>Usuário Funcionários</option>
                                                <option value="6">Usuário Indicadores</option>                                                
                                                <option value="7">Usuário Prospects</option>                                                
                                                <option value="8">Usuário Clientes</option>
                                            <?php } ?>
                                        </select>                                           
                                    </div>
                                    <div style="float:left; width: 18%; margin-left: 0.5%;">                                    
                                        <div width="100%">Nome:</div>
                                        <select id="txtUsuario">
                                            <option value="null">Selecione</option>
                                        </select>                                           
                                    </div>
                                    <div style="float:left; width: 10%; margin-left: 0.5%;">                                    
                                        <div width="100%">Situação:</div>
                                        <select id="txtForma">
                                            <option value="0" selected>Todas</option>
                                            <option value="1">Geradas</option>
                                            <option value="2">A Gerar</option>
                                            <option value="3">Aguardando</option>
                                            <option value="4">Reprovadas</option>
                                        </select>                                          
                                    </div> 
                                    <?php if ($comissao_forma_ == 1) { ?>
                                    <div style="float:left; width: 10%; margin-left: 0.5%;">                                    
                                        <div width="100%">Exibir:</div>
                                        <select id="txtExibir">
                                            <option value="0">Usuário Comissão</option>
                                            <option value="1" selected>Usuário Responsável</option>
                                        </select>                                          
                                    </div>                             
                                    <?php } ?>                                    
                                    <div style="float:left;margin-left: 1%;width: 4%;margin-top: 15px;">
                                        <button id="btnfind" class="button button-turquoise btn-primary" type="button" title="Buscar"><span class="ico-search icon-white"></span></button>
                                    </div>
                                </div>                                
                            </div>                            
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Comissões <?php echo ($comissao_forma_ == 0 ? "Definidas no Funcionário" : "Definidas no Cliente");?></div>
                    </div>
                    <div class="boxtable">
                        <table id="tbComissoes" class="table" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="2%"><input id="checkAll" type="checkbox" class="checkall"/></th>
                                    <th width="6%"><center>Código</center></th>
                                    <th width="8%"><center>Dt.Venda</center></th>
                                    <th width="7%">Usuário</th>
                                    <th width="17%">Cliente</th>                           
                                    <th width="7%" style="text-align:right;">Val.Plano.</th>                                    
                                    <th width="7%" style="text-align:right;">Val.Serv.</th>                                    
                                    <th width="7%" style="text-align:right;">Val.Prod.</th>
                                    <th width="7%" style="text-align:right;">C.Plano 1ª</th>
                                    <th width="7%" style="text-align:right;">C.Plano</th>
                                    <th width="7%" style="text-align:right;">C.Serv.</th>                                      
                                    <th width="7%" style="text-align:right;">C.Prod.</th>                                
                                    <th width="7%" style="text-align:right;padding:0px 10px;">C.Total</th>
                                    <th width="4%"><center>Aviso</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="2">TOTAL</th>
                                    <th colspan="3">0</th>                                    
                                    <th style="text-align:right;">0,00</th>
                                    <th style="text-align:right;">0,00</th>
                                    <th style="text-align:right;">0,00</th>
                                    <th style="text-align:right;">0,00</th>
                                    <th style="text-align:right;">0,00</th>
                                    <th style="text-align:right;">0,00</th>
                                    <th style="text-align:right;">0,00</th>
                                    <th style="text-align:right;">0,00</th>
                                    <th></th>
                                </tr>                                
                            </tfoot>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" /> 
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>   
        <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>        
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            
            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
            filtroUsuario();

            function finalFind(imp) {
                var retPrint = "";
                retPrint = '?imp=0';
                if (imp === 1) {
                    retPrint = '?imp=1';
                }
                if ($("#txtTipo").val() !== "null") {
                    retPrint = retPrint + '&txtTipo=' + $("#txtTipo").val();
                }
                if ($("#txtUsuario").val() !== "null") {
                    retPrint = retPrint + '&txtUsuario=' + $("#txtUsuario").val();
                }
                if ($("#txtForma").val() !== "null") {
                    retPrint = retPrint + '&txtForma=' + $("#txtForma").val();
                }
                if ($("#txtExibir").val() !== "null") {
                    retPrint = retPrint + '&txtExibir=' + $("#txtExibir").val();
                }
                if ($('#txt_dt_begin').val() !== "") {
                    retPrint = retPrint + "&dti=" + $('#txt_dt_begin').val().replace(/\//g, "_");
                }
                if ($('#txt_dt_end').val() !== "") {
                    retPrint = retPrint + "&dtf=" + $('#txt_dt_end').val().replace(/\//g, "_");
                }                   
                if ($('#txtFilial').val() !== null) {
                    retPrint = retPrint + "&filial=" + $('#txtFilial').val();
                }                   
                if ($('#txtFiltro').val() !== null) {
                    retPrint = retPrint + "&filtro=" + $('#txtFiltro').val();
                }
                return "<?php echo ($comissao_forma_ == 0 ? "Comissoes_server_processing.php" : "Comissoes_clientes_all_server_processing.php");?>" + retPrint.replace(/\//g, "_");
            }

            function AbrirBox() {
                if (validaForm()) {
                    abrirTelaBox("FormComissao.php?us=" + $('#txtUsuario').val() + "&tp=" + $('#txtTipo').val(), 370, 400);
                }
            }
            
            function getAllSelected() {
                let id = '';
                let valor = 0.00;
                $("input[name='items[]']:checked").each(function () {
                    id += $(this).val() + '|';
                    valor += textToNumber($(this).attr("val"));
                });
                return [id, valor];
            }
            
            function validaForm() {
                if ($('#txtTipo').val() === "null") {
                    bootbox.alert("Selecione um Grupo!");
                    return false;
                }
                if ($('#txtUsuario').val() === "null") {
                    bootbox.alert("Selecione um Usuário!");
                    return false;
                }
                if ($('#txtForma').val() !== "2") {
                    bootbox.alert("Situação inválida, permitido apenas A Gerar!");
                    return false;
                }
                if($("input[name='items[]']:checked").length === 0) {
                    bootbox.alert("Selecione uma Venda!");
                    return false;                    
                }
                return true;
            }

            function FecharBox() {
                $("#newbox").remove();
                tbLista.fnReloadAjax(finalFind(0));
            }

            function imprimir(tipo) {
                var pRel = "&NomeArq=Comissões" + 
                        "&pOri=L" +
                        "&lbl=Código|Dt.Venda|Usuário|Cliente|Val.Plano.|Val.Serv.|Val.Prod.|C.Plano 1ª|C.Plano|C.Serv.|C.Prod.|C.Total" +
                        (tipo === "E" ? "|Índice|Desconto|Consultor" : "") +
                        "&siz=60|80|70|240|70|70|70|70|70|70|70|70" +
                        (tipo === "E" ? "|50|50|150" : "") +
                        "&pdf=0|13" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=De " + $("#txt_dt_begin").val() + " até " + $("#txt_dt_end").val() + ", " + $( "#txtTipo option:selected" ).text() + ": " +
                        $( "#txtUsuario option:selected" ).text() + ", " + $( "#txtForma option:selected" ).text() + ", " + $( "#txtExibir option:selected" ).text() + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/Seguro/" + finalFind(1).replace("?", "&"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tipo + "&PathArq=GenericModelPDF.php", '_blank');
            }
            
            var tbLista = $('#tbComissoes').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,                
                "bPaginate": false,
                "bInfo": false,
                "bSort": false,
                "sAjaxSource": finalFind(0),
                "fnDrawCallback": function (data) {
                    let totPd = 0.00;
                    let totSv = 0.00;
                    let totPl = 0.00;
                    let comPd = 0.00;
                    let comPp = 0.00;
                    let comSv = 0.00;
                    let comPl = 0.00;
                    let total = 0.00;
                    let itens = 0;
                    let vendas = [];
                    if (data["aoData"].length > 0) {
                        $.each(data["aoData"], function (key, val) {
                            let id_venda = $(val["_aData"][0]).val().split('-')[0];
                            if (!vendas.includes(id_venda)) {
                                vendas.push(id_venda);
                                totPd += textToNumber(val["_aData"][5]);
                                totSv += textToNumber(val["_aData"][6]);
                                totPl += textToNumber(val["_aData"][7]);
                            }
                            comPd += textToNumber(val["_aData"][8]);
                            comPp += textToNumber(val["_aData"][9]);
                            comSv += textToNumber(val["_aData"][10]);
                            comPl += textToNumber(val["_aData"][11]);
                            total += textToNumber(val["_aData"][12]);
                            itens++;
                        });
                    }
                    $('#tbComissoes tfoot tr th:nth(1)').html(itens);
                    $('#tbComissoes tfoot tr th:nth(2)').html(numberFormat(totPd,1));
                    $('#tbComissoes tfoot tr th:nth(3)').html(numberFormat(totSv,1));
                    $('#tbComissoes tfoot tr th:nth(4)').html(numberFormat(totPl,1));
                    $('#tbComissoes tfoot tr th:nth(5)').html(numberFormat(comPd,1));
                    $('#tbComissoes tfoot tr th:nth(6)').html(numberFormat(comPp,1));
                    $('#tbComissoes tfoot tr th:nth(7)').html(numberFormat(comSv,1));
                    $('#tbComissoes tfoot tr th:nth(8)').html(numberFormat(comPl,1));
                    $('#tbComissoes tfoot tr th:nth(9)').html(numberFormat(total,1));                    
                },                
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"},
                "sPaginationType": "full_numbers"
            });
            
            function filtroUsuario() {              
                if ($('#txtTipo').val() !== "null") {
                    $.getJSON('comissao.ajax.php', {txtTipo: $('#txtTipo').val(), filial: $('#txtFilial').val() + ""}, function (j) {
                        var options = '<option value="null">Selecione</option>';
                        for (var i = 0; i < j.length; i++) {
                            options += '<option value="' + j[i].login_user + '">' + j[i].nome + '</option>';
                        }
                        $('#txtUsuario').html(options).show();
                    });
                } else {
                    $('#txtUsuario').html('<option value="null">Selecione</option>');
                }
            }
            
            function AbrirCli(id) {
                window.open("./../Academia/ClientesForm.php?id=" + id, '_blank');         
            }            
            
            $('#txtTipo, #txtFilial').change(function () {
                filtroUsuario();
            });
            
            $("#btnfind").click(function () {
                tbLista.fnReloadAjax(finalFind(0));
            });               
            
        </script>
        <?php odbc_close($con); ?>
    </body>
</html>
