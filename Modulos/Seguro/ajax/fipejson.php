<?php

header("Content-Type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

require_once __DIR__."/../../../util/phpQuery-onefile.php"; 

$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

if (isset($request_body['contrato'])) {
    $contrato = $request_body['contrato'];
    $hostname = '148.72.177.101,6000';
    $database = "erp" . $contrato;
    $username = "erp" . $contrato;
    $password = '!Password123';
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password) or die(odbc_errormsg());
    require_once __DIR__."/../../../Connections/funcoesAux.php";
} else {
    require_once __DIR__."/../../../Connections/configini.php";    
}

//Trocar busca fipe com erro mudar 0 para 2
$tipo_tabela = 0;
if (isset($request_body["tipoFipe"])) { 
    //pode vir 0-Fipe, 1-Personalizado, 2-Zero(Fipe) -> converto para 0
    $tipo_tabela = ($request_body["tipoFipe"] == "1" ? 1 : 0);
}

$cur = odbc_exec($con, "select seg_placa_tp, seg_placa_cod, seg_ano_veiculo_piso from sf_configuracao") or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $seg_placa_tp = $RFP['seg_placa_tp'];    
    $seg_placa_cod = utf8_encode($RFP['seg_placa_cod']);
    $seg_ano_veiculo_piso = $RFP['seg_ano_veiculo_piso'];    
} 
  
if (isset($request_body["tipo"])) {
    $sql = "select top 1 v_tipo from sf_veiculo_tipos where id = ".valoresSelect2($request_body["tipo"]);
    $res =  odbc_exec($con, $sql);
    $tipo_veiculo = odbc_result($res, 1);
    if (!$tipo_veiculo) {
        http_response_code(400);
        echo json_encode(["erro" => "Código de Veiculo invalido"]);
        exit;
    }
    if ($tipo_tabela == 2) { 
        $tipo_veiculo = getTipoParall($tipo_veiculo);
    }
}

if ($tipo_tabela == 0) {
    $referencia = FipeJson("ConsultarTabelaDeReferencia");
    $data = json_decode(json_decode($referencia))[0];
    $tabela_referencia = $data->Codigo;
}

if ($request_body["functionPage"] == "getMarca") {
    if ($tipo_tabela == 0) {
        $marcas = FipeJson("ConsultarMarcas", array("codigoTabelaReferencia" => $tabela_referencia, "codigoTipoVeiculo" => $tipo_veiculo));
        echo json_decode($marcas);        
    } else if ($tipo_tabela == 1) {
        $cur = odbc_exec($con, "select id, descricao from sf_veiculo_marca order by descricao") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $marcas[] = ["Label" => utf8_encode($RFP['descricao']), "Value" => $RFP['id']];
        }
        echo json_encode($marcas);
    } else if ($tipo_tabela == 2) {
        $url = "https://parallelum.com.br/fipe/api/v1/" . $tipo_veiculo . "/marcas"; 
        $return = json_decode(file_get_contents($url), true);
        foreach ($return as $value) {
            $marcas[] = ["Label" => $value["nome"], "Value" => $value["codigo"]];
        }
        echo json_encode($marcas);
    }
    exit;
}

if ($request_body["functionPage"] == "getModelo") {
    $marca = $request_body["codMarca"];
    if ($tipo_tabela == 0) {
        $modelos = FipeJson("ConsultarModelos", array("codigoTabelaReferencia" => $tabela_referencia, "codigoTipoVeiculo" => $tipo_veiculo, "codigoMarca" => $marca));
        echo json_decode($modelos);
    } else if ($tipo_tabela == 1) {
        $cur = odbc_exec($con, "select id, descricao from sf_veiculo_modelo where id_marca = " . $marca . " order by descricao") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $modelos["Modelos"][] = ["Label" => utf8_encode($RFP['descricao']), "Value" => $RFP['id']];
        }
        $ano = date("Y");
        for ($i = 0; $i < $seg_ano_veiculo_piso; $i++) {
            $modelos["Anos"][] = ["Label" => ($ano - $i) . " Personalizado", "Value" => ($ano - $i) . "-1"];    
        }        
        echo json_encode($modelos);     
    } else if ($tipo_tabela == 2) {
        $url = "https://parallelum.com.br/fipe/api/v1/" . $tipo_veiculo . "/marcas/" . $marca . "/modelos"; 
        $return = json_decode(file_get_contents($url), true);
        foreach ($return["modelos"] as $value) {
            $modelos["Modelos"][] = ["Label" => $value["nome"], "Value" => $value["codigo"]];
        }
        foreach ($return["anos"] as $value) {
            $modelos["Anos"][] = ["Label" => $value["nome"], "Value" => $value["codigo"]];
        }
        echo json_encode($modelos);        
    }        
    exit;
}

if ($request_body['functionPage'] == "getModeloPorAno") {
    $marca = $request_body["codMarca"];
    $anomodelo = $request_body["codAno"];
    $dados = explode("-", $anomodelo);
    $ano = $dados[0];
    $combustivel = $dados[1];     
    if ($tipo_tabela == 0) {       
        $modelos = FipeJson("ConsultarModelosAtravesDoAno", array("codigoTabelaReferencia" => $tabela_referencia,
        "codigoTipoVeiculo" => $tipo_veiculo,"ano" => $anomodelo,"codigoMarca" => $marca,"codigoTipoCombustivel" => $combustivel,"anoModelo" => $ano));
        echo json_decode($modelos);        
    } else if ($tipo_tabela == 1) {
        $cur = odbc_exec($con, "select id, descricao from sf_veiculo_modelo where id_marca = " . $marca . " order by descricao") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $modelos[] = ["Label" => utf8_encode($RFP['descricao']), "Value" => $RFP['id']];
        }  
        echo json_encode($modelos);        
    } else if ($tipo_tabela == 2) {
        $url = "https://parallelum.com.br/fipe/api/v1/" . $tipo_veiculo . "/marcas/" . $marca . "/modelos"; 
        $return = json_decode(file_get_contents($url), true);
        foreach ($return["modelos"] as $value) {
            $modelos[] = ["Label" => $value["nome"], "Value" => $value["codigo"]];
        }
        echo json_encode($modelos);  
    }    
    exit;
}

if ($request_body["functionPage"] == "getAnos") {
    $marca = $request_body["codMarca"];
    $modelo = $request_body["codModelo"];
    if ($tipo_tabela == 0) {           
        $ano_modelo = FipeJson("ConsultarAnoModelo", array("codigoTabelaReferencia" => $tabela_referencia,
        "codigoTipoVeiculo" => $tipo_veiculo,"codigoMarca" => $marca, "codigoModelo" => $modelo));
        echo json_decode($ano_modelo);        
    } else if ($tipo_tabela == 1) {
        $ano = date("Y");
        for ($i = 0; $i < $seg_ano_veiculo_piso; $i++) {
            $ano_modelo[] = ["Label" => ($ano - $i) . " Personalizado", "Value" => ($ano - $i) . "-1"];    
        }        
        echo json_encode($ano_modelo);    
    } else if ($tipo_tabela == 2) {
        $url = "https://parallelum.com.br/fipe/api/v1/" . $tipo_veiculo . "/marcas/" . $marca . "/modelos/" . $modelo . "/anos"; 
        $return = json_decode(file_get_contents($url), true);
        foreach ($return as $value) {
            $marcas[] = ["Label" => $value["nome"], "Value" => $value["codigo"]];
        }
        echo json_encode($marcas);        
    }    
    exit;
}

if ($request_body["functionPage"] == "getVeiculo") {
    $marca = $request_body["codMarca"];
    $modelo = $request_body["codModelo"];
    $anomodelo = $request_body["codAno"];
    $dados = explode("-", $anomodelo);
    $ano = $dados[0];
    $combustivel = $dados[1];
    if ($tipo_tabela == 0) {               
        $valor_veiculo = FipeJson("ConsultarValorComTodosParametros", array("codigoTabelaReferencia" => $tabela_referencia, "codigoTipoVeiculo" => $tipo_veiculo,
        "codigoMarca" => $marca, "codigoModelo" => $modelo, "ano" => $anomodelo, "codigoTipoCombustivel" => $combustivel, "anoModelo" => $ano, "tipoConsulta" => "tradicional"));
        echo json_decode($valor_veiculo);        
    } else if ($tipo_tabela == 1) {
        $cur = odbc_exec($con, "select ma.descricao marca, mo.descricao modelo 
        from sf_veiculo_marca ma inner join sf_veiculo_modelo mo on ma.id = mo.id_marca
        where id_marca = " . $marca . " and mo.id = " . $modelo) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $modelo = ["AnoModelo" => $ano, 
                "Autenticacao" => "",
                "CodigoFipe" => "000000-0",
                "Combustivel" => "Gasolina",
                "DataConsulta" => "",
                "Marca" => utf8_encode($RFP['marca']),
                "MesReferencia" => "",
                "Modelo" => utf8_encode($RFP['modelo']),
                "SiglaCombustivel" => "G",
                "TipoVeiculo" => $tipo_veiculo,
                "Valor" => "R$ " . $request_body["valor"],
            ];
        }                 
        echo json_encode($modelo); 
    } else if ($tipo_tabela == 2) {
        $url = "https://parallelum.com.br/fipe/api/v1/" . $tipo_veiculo . "/marcas/" . $marca . "/modelos/" . $modelo . "/anos/" . $anomodelo; 
        $return = file_get_contents($url);
        print_r($return);
    }    
    exit;
}

if ($request_body["functionPage"] == "getPlaca" && isset($request_body["codPlaca"])) {    
    if (in_array($tipo_tabela, array(0,2)) && $seg_placa_tp == 0) {
        $url = "https://www.planoauto.com.br/Base/BuscarDadosDoVeiculoPelaPlaca?placa=" . str_replace("-", "", $request_body["codPlaca"]);
        echo getPlanoauto($url);
    } elseif (in_array($tipo_tabela, array(0,2)) && $seg_placa_tp == 1) {
        $placa = substr($request_body["codPlaca"], 0, 5) . str_replace(array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j"), array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9"), strtolower(substr($request_body["codPlaca"], 5, 1))) . substr($request_body["codPlaca"], 6, 2);
        $return = getYouse($placa, 0, $seg_placa_cod);
        if ($return->LicensePlate == null) {
            $return = getYouse($placa, 0, $seg_placa_cod);
        }
        echo json_encode($return);        
    } elseif (in_array($tipo_tabela, array(0,2)) && $seg_placa_tp == 2) {
        echo json_encode(getPlacaIpva(str_replace("-", "", $request_body["codPlaca"])));
    } elseif ($tipo_tabela == 1) {
        echo json_encode([]);         
    }
}

function getTipoParall($tipo) {
    if ($tipo == "1") {
        return "carros";        
    } else if ($tipo == "2") {        
        return "motos";        
    } else {        
        return "caminhoes";
    }
}

function FipeJson($resource, $data = null) {
    $url = "https://veiculos.fipe.org.br/api/veiculos";
    $header = [
        "Cache-Control: no-cache",
        "Content-Type: application/json",
        "Host: veiculos.fipe.org.br",
        "Referer: https://veiculos.fipe.org.br"
    ];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url . "/" . $resource);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $output = curl_exec($ch);
    curl_close($ch);
    return json_encode($output);
}

function getPlanoauto($url) {
    $useragent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36';
    $timeout = 120;
    $dir = dirname(__FILE__);
    $cookie_file = $dir . '/cookies/' . md5($_SERVER['REMOTE_ADDR']) . '.txt';
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_FAILONERROR, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_ENCODING, "");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_AUTOREFERER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
    curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com/');
    $content = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'error:' . curl_error($ch);
    } else {
        return $content;
    }
    curl_close($ch);
}

function getYouse($placa, $i, $token) {
    $url = "https://www.youse.com.br/auto/order/" . $token . "/vehicle_completion?license_plate=" . $placa;
    $return = json_decode(file_get_contents($url), true);
    if (is_array($return) && count($return) == 0 && $i < 5) {
        $i++;
        return getYouse($placa, $i, $token);
    } else {
        $modelo = explode(" (", $return["data"][0]["version"]);
        $row = array();
        $row["LicensePlate"] = $return["data"][0]["license_plate"];
        $row["Brand"] = $return["data"][0]["make"] . "/" . $return["data"][0]["model"];
        $row["Model"] = $return["data"][0]["make"] . "/" . $return["data"][0]["model"];
        $row["YearModel"] = $return["data"][0]["year"];
        $row["Chassi"] = "";
        $row["Color"] = "";
        $row["City"] = "";
        $row["Uf"] = "";
        $row["FipeCode"] = "";
        $row["Guid"] = (count($modelo) > 0 ? str_replace(")", "", $modelo[1]) : "");
        $row["Status"] = "";
        $row["CreationDateTime"] = "";
        $row["Active"] = true;
        $row["Id"] = "";
        $row["IdFipe"] = "";
        $row["Ano"] = $return["data"][0]["year"];
        $row["Marca"] = $return["data"][0]["make"];
        $row["Modelo"] = (count($modelo) > 0 ? $modelo[0] : "");
        $row["IdFipeAno"] = "";
        return $row;
    }
}

function getPlacaIpva($placa) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://placaipva.com.br/placa/' . strtoupper($placa));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $res = curl_exec($ch);
    curl_close($ch);          
    phpQuery::newDocumentHTML($res, $charset = 'utf-8');      
    $data = [];
    $data["LicensePlate"] = $placa;
    $data["Brand"] = clearText(pq('.fipeTablePriceDetail tr:contains("Marca:"):eq(0)')->html());
    $data["Model"] = clearText(pq('.fipeTablePriceDetail tr:contains("Modelo:"):eq(0)')->html());
    $data["YearModel"] = clearText(pq('.fipeTablePriceDetail tr:contains("Ano Modelo:"):eq(0)')->html());
    $data["Chassi"] = clearText(pq('.fipeTablePriceDetail tr:contains("Chassi:"):eq(0)')->html());
    $data["Color"] = clearText(pq('.fipeTablePriceDetail tr:contains("Cor:"):eq(0)')->html());
    $data["City"] = clearText(pq('.fipeTablePriceDetail tr:contains("Município:"):eq(0)')->html());
    $data["Uf"] = clearText(pq('.fipeTablePriceDetail tr:contains("UF:"):eq(0)')->html());
    $data["FipeCode"] = "";
    $data["Guid"] = "";
    $data["Status"] = 0;
    $data["CreationDateTime"] = "/Date(1674224508487)/";
    $data["Active"] = true;
    $data["Id"] = "";
    $data["IdFipe"] = "";
    $data["Ano"] = clearText(pq('.fipeTablePriceDetail tr:contains("Ano:"):eq(0)')->html());
    $data["Marca"] = clearText(pq('.fipeTablePriceDetail tr:contains("Marca:"):eq(0)')->html());
    $data["Modelo"] = clearText(pq('.fipeTablePriceDetail tr:contains("Modelo:"):eq(0)')->html());
    $data["Combustivel"] = clearText(pq('.fipeTablePriceDetail tr:contains("Combustível:"):eq(0)')->html());    
    $data["IdFipeAno"] = "";
    return $data;
}

function clearText($text) {
    return trim(explode("</b>", str_replace(['<td>', '</td>'], "", $text))[1]);
}