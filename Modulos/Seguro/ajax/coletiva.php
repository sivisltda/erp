<?php

$sql = "select top 1 wa_ambiente, wa_senha, wa_login, wa_chave, wa_empresa from sf_configuracao;";
$rs = odbc_exec($con, $sql);
while ($row = odbc_fetch_array($rs)) {
    $homologacao = $row['wa_ambiente'] ? "https://" : 'http://teste.';
    define('ENDPOINT', $homologacao . 'integrar24horas.com.br/api/v2');
    define('LOGIN_INFO', utf8_encode($row['wa_login']));
    define('SENHA_INFO', utf8_encode($row['wa_senha']));
}

$request_body = $_REQUEST;
$wa_chave = getToken();
if (isset($wa_chave->token)) {
    define('CHAVE_INFO', $wa_chave->token);
}

if ($request_body['functionPage'] == "getNegociacoes") {
    print_r($wa_chave);
    exit;
}

if ($request_body['functionPage'] == 'criarNovoVeiculo') {
    if (isset($request_body['id_veiculo']) && $request_body['id_veiculo']) {
        $data = buscarVeiculo($con, $request_body['id_veiculo'], (isset($request_body['excluir']) ? 'INATIVO' : 'ATIVO'));
        if (count($data) > 0) {
            unset($data["codigo"]);
            $veiculo = synchronyJson('sincronizar', json_encode($data));
            //print_r($veiculo); exit;
            if ($veiculo->status == "CONCLUIDO") {
                try {
                    atualizaProprietarioVeiculo($con, $request_body['id_veiculo'], $veiculo->cod_sincronizacao);
                    addLog($con, $request_body['id_veiculo'], (isset($request_body['excluir']) ? 'E' : 'I'));
                    echo 'OK';
                } catch (Exception $e) {
                    echo 'Ocorreu um ao atualizar as informações: ' . $e->getMessage();
                }
                exit;
            }
            echo 'Ocorreu um erro ao sincronizar: ' . $veiculo->body;
            exit;
        } else {
            echo "Nenhum veículo encontrado";
            exit;
        }
    }
    echo "Campo de código de Veiculo é obrigatório";
    exit;
}

if ($request_body['functionPage'] == 'alterarVeiculo') {
    if (isset($request_body['id_veiculo']) && $request_body['id_veiculo']) {
        $data = buscarVeiculo($con, $request_body['id_veiculo'], (isset($request_body['excluir']) ? 'INATIVO' : 'ATIVO'));
        if (count($data) > 0) {
            unset($data["codigo"]);
            $veiculo = synchronyJson('sincronizar', json_encode($data));
            //print_r($veiculo); exit;            
            if ($veiculo->status == "CONCLUIDO") {
                try {
                    atualizaProprietarioVeiculo($con, $request_body['id_veiculo'], (isset($request_body['excluir']) ? 'null' : $veiculo->cod_sincronizacao));
                    addLog($con, $request_body['id_veiculo'], (isset($request_body['excluir']) ? 'E' : 'I'));
                    echo 'OK';
                } catch (Exception $e) {
                    echo 'Ocorreu um ao atualizar as informações: ' . $e->getMessage();
                }
                exit;
            }
            echo 'Ocorreu um erro ao sincronizar: ' . $veiculo->body;
            exit;
        } else {
            echo "Nenhum veículo encontrado";
            exit;
        }
    }
    echo "Campo de código de Veiculo é obrigatório";
    exit;
}

function getToken() {
    $header = [
        "Content-Type: application/json"
    ];
    return mkCurl("login", json_encode(['usuario' => LOGIN_INFO, 'senha' => SENHA_INFO]), $header);
}

function synchronyJson($resource, $data = []) {
    $header = [
        "Authorization: Bearer " . CHAVE_INFO,
        "Content-Type: application/json"
    ];
    return mkCurl($resource, $data, $header);
}

function mkCurl($resource, $data = [], $header = []) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, ENDPOINT . "/" . $resource);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $output = curl_exec($ch);
    //echo $output;
    curl_close($ch);
    return json_decode($output);
}

function buscarVeiculo($con, $id_veiculo, $status) {
    $data = [];    
    $produtos[] = filtroNegociacoes($con, valoresSelect2($id_veiculo), $status);
    $sql = "select top 1 fd.id_fornecedores_despesas, razao_social, cnpj, inscricao_estadual, data_nascimento, dt_cadastro, sexo, endereco, numero, complemento, bairro, cep, 
        (select top 1 cidade_nome from tb_cidades where cidade_codigo = fd.cidade) as cidade, 
        (select top 1 estado_sigla from tb_estados where estado_codigo = fd.estado) as estado,        
        (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 2) as email,
        (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 0) as telefone,
        (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 1) as celular,                
        (select top 1 nome_produto_cor from sf_produtos_cor where id_produto_cor = ve.cor) as cor,
        (select top 1 descricao from sf_veiculo_tipos where id = tipo_veiculo) t_veiculo,
        (select count(vc.id) total from sf_fornecedores_despesas_veiculo vc where id_externo is not null and vc.id_fornecedores_despesas = fd.id_fornecedores_despesas and vc.id not in (ve.id)) total_vc,
        marca, modelo, tipo_veiculo, placa, cambio, ano_modelo, ano_fab, combustivel, gas_natural, chassi, renavam, codigo_fipe, valor, ve.id, id_gobody, id_externo        
        from sf_fornecedores_despesas fd 
        inner join sf_fornecedores_despesas_veiculo ve on ve.id_fornecedores_despesas = fd.id_fornecedores_despesas
        where len(isnull(ve.chassi, '')) > 0 and ve.id = " . valoresSelect2($id_veiculo) . ";";
    $rs = odbc_exec($con, $sql);
    while ($row = odbc_fetch_array($rs)) {
        $veiculo[] = [
            "codigo_veiculo" => (float) $row['id'],
            "placa" => str_replace('-', '', $row['placa']),
            "chassi" => $row['chassi'],
            "renavam" => $row['renavam'],
            "ano_fabricacao" => $row['ano_fab'],
            "ano_modelo" => $row['ano_modelo'],
            "valor_fipe" => valoresNumericos2(escreverNumero($row['valor'])),
            "codigo_fipe" => $row['codigo_fipe'],
            "descricao_modelo" => utf8_encode($row['modelo']),
            "status" => $status,
            "descricao_cor" => $row['cor'],
            "descricao_combustivel" => $row['combustivel'],
            "descricao_tipoveiculo" => utf8_encode($row['t_veiculo']),
            "descricao_marca" => utf8_encode($row['marca']),
            "produtos" => $produtos
        ];
        $data[] = [
            'codigo' => $row['id_externo'],            
            'codigo_associado' => (float) $row['id_fornecedores_despesas'],
            'nome_associado' => utf8_encode($row['razao_social']),
            'cpf' => preg_replace('/[^0-9]/', '', $row['cnpj']),
            'rg' => $row['inscricao_estadual'],
            'data_contrato' => escreverData($row['dt_cadastro'], "Y-m-d"),
            'hora_cadastro' => escreverData($row['dt_cadastro'], "H:i:s"),
            'data_nascimento' => escreverData($row['data_nascimento'], "Y-m-d"),
            'cep' => $row['cep'],
            'logradouro' => utf8_encode($row['endereco']),
            'numero' => $row['numero'],
            'complemento' => utf8_encode($row['complemento']),
            'bairro' => utf8_encode($row['bairro']),
            'cidade' => utf8_encode($row['cidade']),
            'estado' => utf8_encode($row['estado']),            
            'ddd' => substr(preg_replace('/[^0-9]/', '', (strlen($row['telefone']) > 0 ? $row['telefone'] : $row['celular'])), 0, 2),
            'telefone' => substr(preg_replace('/[^0-9]/', '', (strlen($row['telefone']) > 0 ? $row['telefone'] : $row['celular'])), 2),
            'ddd_celular' => substr(preg_replace('/[^0-9]/', '', $row['celular']), 0, 2),
            'telefone_celular' => substr(preg_replace('/[^0-9]/', '', $row['celular']), 2),            
            'email' => $row['email'],
            'status' => ($row['total_vc'] > 0 ? "ATIVO" : $status),
            'veiculos' => $veiculo,
        ];              
    }
    return $data;
}

function filtroNegociacoes($con, $id_veiculo, $status) {    
    $toReturn = [];
    $sql = "select top 1 codigo_interno, codigo_barra from sf_vendas_planos vp
            inner join sf_vendas_planos_acessorios vpa on vp.id_plano = vpa.id_venda_plano
            inner join sf_produtos p on p.conta_produto = vpa.id_servico_acessorio
            where vp.id_veiculo = " . valoresSelect2($id_veiculo) . "
            and vpa.id_servico_acessorio in (select conta_produto from sf_produtos where len(isnull(codigo_interno, '')) > 0);";
    $res = odbc_exec($con, $sql);
    while ($row = odbc_fetch_array($res)) {
        $toReturn = ['codigo_produto' => (float) $row['codigo_interno'], 'descricao_produto' => utf8_encode($row['codigo_barra']), 'status' => $status];
    }
    return $toReturn;
}

function atualizaProprietarioVeiculo($con, $id_veiculo, $id_sinc) {
    try {
        odbc_autocommit($con, false);
        $sqlAtualizaVeiculo = "update sf_fornecedores_despesas_veiculo set id_externo = " . valoresSelect2($id_sinc) . " where id = " . valoresSelect2($id_veiculo);
        odbc_exec($con, $sqlAtualizaVeiculo);
        odbc_commit($con);
    } catch (Exception $e) {
        odbc_rollback($con);
        throw new Exception($e->getMessage());
    } finally {
        odbc_autocommit($con, true);
    }
}

function addLog($con, $id, $tipo) {
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
    ('sf_fornecedores_despesas_veiculo', " . $id . ", '" . (isset($_SESSION["login_usuario"]) ? $_SESSION["login_usuario"] : 'SISTEMA') . "',
    '" . $tipo . "', '" . ($tipo == "E" ? "EXCLUSAO" : "INCLUSAO") . " ASSISTENCIA 24H: " . $id . "', GETDATE(),(select id_fornecedores_despesas from sf_fornecedores_despesas_veiculo where id = " . $id . "));") or die(odbc_errormsg());
}
