<?php

$sql = "select top 1 wa_ambiente, wa_empresa, wa_login, wa_senha, wa_chave from sf_configuracao;";
$rs = odbc_exec($con, $sql);
while ($row = odbc_fetch_array($rs)) {
    define('ENDPOINT', 'https://' . ($row['wa_ambiente'] == "0" ? 'homolog-api' : 'api') . '.veniti.com.br/api/v2/veniti/' . $row['wa_empresa'] . '/cliente/' . $row['wa_login'] . '/');
    define('CHAVE_INFO', $row['wa_chave']);
}

$request_body = $_REQUEST;

if ($request_body['functionPage'] == "getNegociacoes") {
    //https://teste.sivisweb.com.br/Modulos/Seguro/ajax/assistencia.php?functionPage=getNegociacoes
    $produtos = synchronyJson('planos', [], false);
    echo json_encode($produtos);
    exit;
}

if ($request_body['functionPage'] == 'getTodosBeneficiarios') {
    //https://teste.sivisweb.com.br/Modulos/Seguro/ajax/assistencia.php?functionPage=getTodosBeneficiarios
    $beneficiarios = synchronyJson('beneficiarios', [], false);
    echo json_encode($beneficiarios);
    exit;
}

if ($request_body['functionPage'] == 'criarNovoVeiculo') {
    if (isset($request_body['id_veiculo']) && $request_body['id_veiculo']) {
        $pessoa = buscarVeiculo($con, $request_body['id_veiculo'], 1);
        if (count($pessoa) > 0) {
            if (!is_numeric($pessoa["codigo"])) {
                unset($pessoa["codigo"]);              
                $result = synchronyJson('beneficiario', $pessoa);  
                if (is_numeric($result->id)) {
                    atualizaPessoaVeiculo($con, $request_body['id_veiculo'], $result->id);
                } else {
                    print_r($result);
                    echo "Erro ao salvar Pessoa"; exit;
                }
            }
            $veiculo = buscarVeiculo($con, $request_body['id_veiculo'], 2);
            if (!is_numeric($veiculo["codigo"])) {
                unset($veiculo["codigo"]);
                $result = synchronyJson('beneficiario/' . $pessoa["cpf_cnpj"] . '/veiculo', $veiculo);  
                if (is_numeric($result->id)) {
                    atualizaProprietarioVeiculo($con, $request_body['id_veiculo'], $result->id);
                } else {
                    print_r($result);
                    echo "Erro ao salvar Veículo"; exit;
                }
            }
            $produto = filtroNegociacoes($con, $request_body['id_veiculo']);
            if (count($produto) > 0) {
                $result = synchronyJson('beneficiario/' . $pessoa["cpf_cnpj"] . '/veiculo/' . $veiculo["placa"] . '/plano', $produto);
                if (is_numeric($result->id)) {
                    atualizaPlanoVeiculo($con, $request_body['id_veiculo'], $result->id);
                    addLog($con, $request_body['id_veiculo'], 'I');                    
                    echo 'OK'; exit;                                    
                } else {
                    echo "Erro ao salvar Plano"; exit;    
                }
            } else {
                echo "Plano não encontrado!"; exit;
            }
        } else {
            echo "Nenhum cliente encontrado"; exit;
        }
    }
    echo "Campo de código de Veiculo é obrigatório"; exit;
}

if ($request_body['functionPage'] == 'alterarVeiculo') {
    if (isset($request_body['id_veiculo']) && $request_body['id_veiculo']) {
        $pessoa = buscarVeiculo($con, $request_body['id_veiculo'], 1);
        if (count($pessoa) > 0) {
            $veiculo = buscarVeiculo($con, $request_body['id_veiculo'], 2);
            if (is_numeric($veiculo["codigo"]) && isset($request_body['excluir'])) {                
                $produto = filtroNegociacoes($con, $request_body['id_veiculo']);
                if (count($produto) > 0) {                
                    $result = synchronyJson('beneficiario/' . $pessoa["cpf_cnpj"] . '/veiculo/' . $veiculo["placa"] . '/plano/' . $produto["plano"], [], false, true);
                    if ($result->status == "removido com sucesso") {                        
                        atualizaPlanoVeiculo($con, $request_body['id_veiculo'], 'null');
                        addLog($con, $request_body['id_veiculo'], 'E');                    
                        echo 'OK'; exit;                                      
                    } else {
                        print_r($result);
                        echo "Erro ao remover Plano"; exit;    
                    }
                } else {
                    echo "Plano não encontrado!"; exit;
                }                    
            } else {
                echo "Código de Veiculo não encontrado!"; exit;
            }
        } else {
            echo "Nenhum cliente encontrado"; exit;
        }
    }
    echo "Campo de código de Veiculo é obrigatório"; exit;    
}

function synchronyJson($resource, $data = [], $post = true, $delete = false) {
    $header = ["Authorization: " . CHAVE_INFO];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, ENDPOINT . "/" . $resource);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    if ($post) {
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    } else if ($delete)  {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");        
    }
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $output = curl_exec($ch); 
    //echo $output;
    curl_close($ch);
    return json_decode($output);
}

function buscarVeiculo($con, $id_veiculo, $tipo) {
    $sql = "select top 1 fd.id_fornecedores_despesas, razao_social, cnpj, inscricao_estadual, 
        cast(data_nascimento as date) data_nascimento, sexo, endereco, numero, complemento, bairro, 
        cast(dt_cadastro as date) dt_cadastro, id_externo_veic,
        (select top 1 cidade_nome from tb_cidades where cidade_codigo = fd.cidade) as cidade, 
        (select top 1 estado_sigla from tb_estados where estado_codigo = fd.estado) as estado,
        cep, (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 2) as email,
        (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 0) as telefone,
        (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 1) as celular,
        ve.id, marca, modelo, tipo_veiculo, placa, cambio, ano_modelo, ano_fab, combustivel, gas_natural, chassi, renavam, codigo_fipe, valor, ve.id, id_gobody, id_externo,
        (select top 1 nome_produto_cor from sf_produtos_cor where id_produto_cor = ve.cor) as cor,
        (select top 1 isnull(v_tipo, 1) from sf_veiculo_tipos where id = tipo_veiculo) t_veiculo
        from sf_fornecedores_despesas fd 
        inner join sf_fornecedores_despesas_veiculo ve on ve.id_fornecedores_despesas = fd.id_fornecedores_despesas
        where len(isnull(ve.chassi, '')) > 0 and ve.id = " . valoresSelect2($id_veiculo) . ";";
    $rs = odbc_exec($con, $sql);
    $data = [];
    while ($row = odbc_fetch_array($rs)) {
        if ($tipo == 1) {
            $data = [                            
                'codigo' => $row['id_gobody'],            
                'razao_social' => utf8_encode($row['razao_social']),
                'nome' => utf8_encode($row['razao_social']),
                'cpf_cnpj' => preg_replace('/[^0-9]/', '', $row['cnpj']),
                'tipo_pessoa' => (strlen($row['cnpj']) == 14 ? 'PF' : 'PJ'),
                'rg' => preg_replace('/[^0-9]/', '', $row['inscricao_estadual']),
                'data_nascimento' => $row['data_nascimento'],
                'sexo' => $row['sexo'],
                'data_contrato' => $row['dt_cadastro'],                
                'cep' => preg_replace('/[^0-9]/', '', $row['cep']),            
                'logradouro' => utf8_encode($row['endereco']) . " " . $row['numero'],
                'bairro' => utf8_encode($row['bairro']),
                'estado' => utf8_encode($row['estado']),
                'observacao' => utf8_encode($row['complemento']),
                'cidade' => utf8_encode($row['cidade'])];
        } else if ($tipo == 2) {
            $data = [
                'codigo' => $row['id_externo_veic'],                 
                'tipo' => ($row['t_veiculo'] == 3 ? "CAMINHAO" : ($row['t_veiculo'] == 2 ? "MOTO" : "AUTOMOVEL")),                
                'cod_externo' => $row['id'],         
                'marca' => utf8_encode($row['marca']),
                'modelo' => utf8_encode($row['modelo']),                
                'ano_fabricacao' => $row['ano_fab'],                
                'ano_modelo' => $row['ano_modelo'],
                'cor' => $row['cor'],
                'combustivel' => $row['combustivel'],
                'chassi' => $row['chassi'],
                'renavam' => $row['renavam'],
                'fipe_codigo' => $row['codigo_fipe'],
                'fipe_valor' => valoresNumericos2(escreverNumero($row['valor'])),
                'observacao' => "",
                'placa' => str_replace('-', '', $row['placa']),
            ];
        }
    }
    return $data;
}

function filtroNegociacoes($con, $id_veiculo) {
    $toReturn = [];
    $sql = "select top 1 codigo_interno, codigo_barra,
            datediff(s,'01/01/1970', getdate()) dt_begin, 
            datediff(s,'01/01/1970', dateadd(year, 10, getdate())) dt_end
            from sf_vendas_planos vp inner join sf_vendas_planos_acessorios vpa on vp.id_plano = vpa.id_venda_plano
            inner join sf_produtos p on p.conta_produto = vpa.id_servico_acessorio
            where vp.id_veiculo = " . valoresSelect2($id_veiculo) . "
            and vpa.id_servico_acessorio in (select conta_produto from sf_produtos where len(isnull(codigo_interno, '')) > 0);";
    $res = odbc_exec($con, $sql);
    while ($row = odbc_fetch_array($res)) {
        $toReturn = ['plano' => utf8_encode($row['codigo_interno']), 'vigencia_de' => $row['dt_begin'], 'vigencia_ate' => $row['dt_end']];
    }
    return $toReturn;
}

function atualizaPessoaVeiculo($con, $id_veiculo, $id_sinc) {
    try {
        odbc_autocommit($con, false);
        odbc_exec($con, "update sf_fornecedores_despesas set id_gobody = " . valoresSelect2($id_sinc) . " 
        where id_fornecedores_despesas in (select id_fornecedores_despesas from sf_fornecedores_despesas_veiculo where id = " . valoresSelect2($id_veiculo) . ")");
        odbc_commit($con);
    } catch (Exception $e) {
        odbc_rollback($con);
        throw new Exception($e->getMessage());
    } finally {
        odbc_autocommit($con, true);
    }
}

function atualizaProprietarioVeiculo($con, $id_veiculo, $id_sinc) {
    try {
        odbc_autocommit($con, false);
        $sqlAtualizaVeiculo = "update sf_fornecedores_despesas_veiculo set id_externo_veic = " . valoresSelect2($id_sinc) . "
        where id = " . valoresSelect2($id_veiculo);
        odbc_exec($con, $sqlAtualizaVeiculo);
        odbc_commit($con);
    } catch (Exception $e) {
        odbc_rollback($con);
        throw new Exception($e->getMessage());
    } finally {
        odbc_autocommit($con, true);
    }
}

function atualizaPlanoVeiculo($con, $id_veiculo, $id_sinc) {
    try {
        odbc_autocommit($con, false);
        $sqlAtualizaVeiculo = "update sf_fornecedores_despesas_veiculo set id_externo = " . valoresSelect2($id_sinc) . "
        where id = " . valoresSelect2($id_veiculo);
        odbc_exec($con, $sqlAtualizaVeiculo);
        odbc_commit($con);
    } catch (Exception $e) {
        odbc_rollback($con);
        throw new Exception($e->getMessage());
    } finally {
        odbc_autocommit($con, true);
    }
}

function addLog($con, $id, $tipo) {
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
    ('sf_fornecedores_despesas_veiculo', " . $id . ", '" . (isset($_SESSION["login_usuario"]) ? $_SESSION["login_usuario"] : 'SISTEMA') . "',
    '" . $tipo . "', '" . ($tipo == "E" ? "EXCLUSAO" : "INCLUSAO") . " ASSISTENCIA 24H: " . $id . "', GETDATE(),(select id_fornecedores_despesas from sf_fornecedores_despesas_veiculo where id = " . $id . "));") or die(odbc_errormsg());
}
