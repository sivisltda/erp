<?php

$sql = "select top 1 ra_ambiente, ra_chave from sf_configuracao;";
$rs = odbc_exec($con, $sql);
while ($row = odbc_fetch_array($rs)) {
    $homologacao = $row['ra_ambiente'] ? '167.99.120.165' : '167.99.120.165';
    define('ENDPOINT', 'http://' . $homologacao . ':8890');
    define('CHAVE_INFO', $row['ra_chave']);
}

$request_body = $_REQUEST;

if ($request_body['functionPage'] == 'getTipoVeiculos') {
    echo json_encode(hapoloJson("listas/tipos", false));
}

if ($request_body['functionPage'] == "getTodosOperadoras") {
    echo json_encode(hapoloJson("listas/operadoras", false));
}

if ($request_body['functionPage'] == 'getTodosRastreadores') {
    echo json_encode(hapoloJson("listas/rastreadores", false));
}

if ($request_body['functionPage'] == 'criarNovoVeiculo') {
    if (isset($request_body['id_veiculo']) && $request_body['id_veiculo']) {
        $pessoa = buscarVeiculo($con, $request_body['id_veiculo'], 0);
        if (count($pessoa) > 0) {
            if (!is_numeric($pessoa["id"])) {
                unset($pessoa["id"]);
                $result = hapoloJson('create_cliente', true, $pessoa);
                if ($result->success) {
                    atualizaPessoaVeiculo($con, $request_body['id_veiculo'], $result->id);
                } else {
                    echo "Erro ao salvar Pessoa: " . $result->msg; 
                    exit;
                }
            }
            $veiculo = buscarVeiculo($con, $request_body['id_veiculo'], 1);  
            if (!is_numeric($veiculo["id"])) {
                unset($veiculo["id"]);
            }
            $result = hapoloJson('add_bem_v2', true, $veiculo);
            if ($result->success) {
                atualizaProprietarioVeiculo($con, $request_body['id_veiculo'], $result->id, "S");
                addLog($con, $request_body['id_veiculo'], 'I');                    
                echo 'OK'; 
                exit;
            }
            echo 'Ocorreu um erro ao sincronizar: ' . $result->msg; 
            exit;           
        } else {
            echo "Nenhum cliente encontrado"; 
            exit;            
        }
    }
    echo "Campo de código de Veiculo é obrigatório";
    exit;
}

if ($request_body['functionPage'] == 'alterarVeiculo') {
    if (isset($request_body['id_veiculo']) && $request_body['id_veiculo']) {
        $veiculo = buscarVeiculo($con, $request_body['id_veiculo'], 1); 
        $veiculo["activated"] = "N";
        if (count($veiculo) > 0) {
            $result = hapoloJson('add_bem_v2', true, $veiculo);
            if ($result->success) {
                atualizaProprietarioVeiculo($con, $request_body['id_veiculo'], $result->id, "N");
                addLog($con, $request_body['id_veiculo'], 'E');                    
                echo 'OK';
                exit;
            }
            echo 'Ocorreu um erro ao sincronizar: ' . $result->msg;
            exit;
        }
        echo "Nenhum veículo encontrado";
        exit;
    }
    echo "Campo de código de Veiculo é obrigatório";
    exit;
}

function hapoloJson($resource, $post = true, $data = []) {
    $header = [
        "Cache-Control: no-cache",
        "Accept: application/json"
    ];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, ENDPOINT . "/" . $resource);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
    if ($post) {
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);      
    } else {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');        
    }    
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $output = curl_exec($ch);
    curl_close($ch);
    return json_decode($output);
}

function buscarVeiculo($con, $id_veiculo, $tipo) {
    $sql = "select top 1 ve.id, razao_social, cnpj, data_nascimento, cep, endereco, numero, complemento, bairro, cidade_nome, estado_sigla, id_rastreio,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where fornecedores_despesas = fd.id_fornecedores_despesas and tipo_contato = 1) as celular,    
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where fornecedores_despesas = fd.id_fornecedores_despesas and tipo_contato = 2) as email,    
    placa, marca, modelo, ano_fab, chassi, renavam, ve.id, id_externo_rast, imei, chip,
    (select top 1 nome_produto_cor from sf_produtos_cor where id_produto_cor = ve.cor) as cor,
    (select top 1 v_cod_rastreio from sf_veiculo_tipos where id = tipo_veiculo) t_veiculo,
    (select top 1 v_cod from sf_rastreador where id = id_rastreador) t_rastreador,
    (select top 1 v_cod from sf_rastreador_operadora where id = id_operadora) t_operadora
    from sf_fornecedores_despesas fd 
    inner join sf_fornecedores_despesas_veiculo ve on ve.id_fornecedores_despesas = fd.id_fornecedores_despesas
    left join tb_cidades on tb_cidades.cidade_codigo = fd.cidade 
    left join tb_estados on tb_cidades.cidade_codigoEstado = tb_estados.estado_codigo
    where len(isnull(ve.chassi, '')) > 0 and ve.id = " . valoresSelect2($id_veiculo) . ";";
    $rs = odbc_exec($con, $sql);
    $data = [];
    while ($row = odbc_fetch_array($rs)) {
        if ($tipo == 0) {
            $data = [
                'token' => CHAVE_INFO,
                'email' => utf8_encode($row['email']),
                'nome' => utf8_encode($row['razao_social']),
                'cpf' => utf8_encode($row['cnpj']),
                'usuario' => utf8_encode($row['email']),
                'senha' => str_replace([".", "-", "/"], "", $row['cnpj']),
                'ativo' => "S",
                'nascimento' => utf8_encode($row['data_nascimento']),
                'tel_celular' => utf8_encode($row['celular']),
                'cep' => utf8_encode($row['cep']),
                'endereco' => utf8_encode($row['endereco']),
                'numero' => utf8_encode($row['numero']),
                'complemento' => utf8_encode($row['complemento']),
                'bairro' => utf8_encode($row['bairro']),
                'cidade' => utf8_encode($row['cidade_nome']),
                'uf' => utf8_encode($row['estado_sigla']),
                'id' => $row['id_rastreio']               
            ];            
        } else if ($tipo == 1) {
            $data = [
                'token' => CHAVE_INFO,
                'imei' => $row['imei'],
                'nome' => utf8_encode($row['marca']),
                'placa' => str_replace('-', '', $row['placa']),            
                'chip' => $row['chip'],            
                'id_tipo' => $row['t_veiculo'],
                'cor_veiculo' => $row['cor'],            
                'id_rastreador' => $row['t_rastreador'],
                'identificacao' => utf8_encode($row['modelo']),
                'id_operadora' => $row['t_operadora'],            
                'chassis' => $row['chassi'],
                'ano_veiculo' => $row['ano_fab'],
                'renavam' => $row['renavam'],
                'proprietario' => utf8_encode($row['razao_social']),
                'telefone_proprietario' => $row['celular'],
                'motorista' => utf8_encode($row['razao_social']),
                'obs' => $row['id'],          
                'activated' => "S",
                'id_cliente' => $row['id_rastreio'],
                'id' => $row['id_externo_rast']
            ];
        }
    }
    return $data;
}

function atualizaPessoaVeiculo($con, $id_veiculo, $id_sinc) {
    try {
        odbc_autocommit($con, false);
        odbc_exec($con, "update sf_fornecedores_despesas set id_rastreio = " . valoresSelect2($id_sinc) . " 
        where id_fornecedores_despesas in (select id_fornecedores_despesas from sf_fornecedores_despesas_veiculo where id = " . valoresSelect2($id_veiculo) . ")");
        odbc_commit($con);
    } catch (Exception $e) {
        odbc_rollback($con);
        throw new Exception($e->getMessage());
    } finally {
        odbc_autocommit($con, true);
    }
}

function atualizaProprietarioVeiculo($con, $id_veiculo, $id_sinc, $status) {
    try {
        odbc_autocommit($con, false);
        $sqlAtualizaVeiculo = "update sf_fornecedores_despesas_veiculo set id_externo_rast = " . valoresSelect2($id_sinc) . 
        ", status_ext_veic = " . valoresTexto2($status) . " where id = " . valoresSelect2($id_veiculo);
        odbc_exec($con, $sqlAtualizaVeiculo);
        odbc_commit($con);
    } catch (Exception $e) {
        odbc_rollback($con);
        throw new Exception($e->getMessage());
    } finally {
        odbc_autocommit($con, true);
    }
}

function addLog($con, $id, $tipo) {
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
    ('sf_rastreador', " . $id . ", '" . (isset($_SESSION["login_usuario"]) ? $_SESSION["login_usuario"] : 'SISTEMA') . "',
    '" . $tipo . "', '" . ($tipo == "E" ? "EXCLUSAO" : "INCLUSAO") . " RASTREIO: " . $id . "', GETDATE(),(select id_fornecedores_despesas from sf_fornecedores_despesas_veiculo where id = " . $id . "));") or die(odbc_errormsg());
}