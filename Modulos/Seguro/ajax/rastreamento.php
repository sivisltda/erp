<?php

header("Content-Type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

if (isset($_GET["hostname"]) && isset($_GET["database"]) && isset($_GET["username"]) && isset($_GET["password"])) {
    $contrato = $_GET['txtContrato'];
    $hostname = $_GET["hostname"];
    $database = $_GET["database"];
    $username = $_GET["username"];
    $password = $_GET["password"];
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
    require_once(__DIR__ . "./../../../Connections/funcoesAux.php");
} else {
    require_once(__DIR__ . "./../../../Connections/configini.php");
}

$tipo = 0;
$sql = "select top 1 ra_tipo from sf_configuracao;";
$rs = odbc_exec($con, $sql);
while ($row = odbc_fetch_array($rs)) {
    $tipo = $row['ra_tipo'];
}

if ($tipo == 0) {
    require_once(__DIR__ . "./hapolo.php");
}