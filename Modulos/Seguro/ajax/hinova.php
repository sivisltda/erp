<?php

$sql = "select top 1 wa_ambiente, wa_empresa, wa_login, wa_senha, wa_chave from sf_configuracao;";
$rs = odbc_exec($con, $sql);
while ($row = odbc_fetch_array($rs)) {
    $homologacao = $row['wa_ambiente'] ? 'care' : 'care_homologacao';
    define('ENDPOINT', 'https://aid.hinova.com.br/aid/' . $homologacao . '/ws/webservice.php');
    define('EMPRESA', $row['wa_empresa']);
    define('SENHA', $row['wa_senha']);
}

$request_body = $_REQUEST;

if ($request_body['functionPage'] == "getNegociacoes") {
    $xml = synchronyJson('fObtemNegociacoes', array('intervenienteCnpj' => EMPRESA, 'formato' => 'xml'));
    echo "<pre>"; print_r($xml->negociacoes); exit;
}

if ($request_body['functionPage'] == 'getTodosBeneficiarios') {
    $xml = synchronyJson('fObtemAssociados', array('intervenienteCnpj' => EMPRESA, 'senha' => SENHA, 'formato' => 'xml'));
    echo "<pre>"; print_r($xml->associados); exit;
}

if ($request_body['functionPage'] == 'criarNovoVeiculo') {
    if (isset($request_body['id_veiculo']) && $request_body['id_veiculo']) {
        $plano = filtroNegociacoes($con, $request_body['id_veiculo']);
        if (is_numeric($plano)) {
            $veiculo = buscarVeiculo($con, $request_body['id_veiculo'], $plano);
            if (count($veiculo) > 0 && !is_numeric($veiculo["codigo"])) {
                unset($veiculo["codigo"]);
                $xml = synchronyJson('fCadastrarAssociado', array('intervenienteCnpj' => EMPRESA, 'idInterveniente' => SENHA, 'xmlOriginal' => $veiculo));
                //echo "<pre>"; print_r($xml); exit;
                if ($xml->veiculo->produtos->produto->status == "APROVADO") {
                    try {
                        $codigo = end(explode('_', $xml->veiculo->produtos->produto->id_pedido));
                        atualizaProprietarioVeiculo($con, $request_body['id_veiculo'], $codigo);
                        addLog($con, $request_body['id_veiculo'], 'I');
                        echo 'OK';
                    } catch (Exception $e) {
                        echo 'Ocorreu um ao atualizar as informações: ' . $e->getMessage();
                    }
                    exit;
                }
                echo 'Ocorreu um erro ao sincronizar: ' . $xml->veiculo->produtos->produto->status;
                exit;
            }
            echo "Nenhum veículo encontrado";
            exit;
        }
        echo "Nenhum Plano encontrado";
        exit;
    }
    echo "Campo de código de Veiculo é obrigatório";
    exit;
}

if ($request_body['functionPage'] == 'alterarVeiculo') {
    if (isset($request_body['id_veiculo']) && $request_body['id_veiculo']) {
        $plano = filtroNegociacoes($con, $request_body['id_veiculo']);
        if (is_numeric($plano)) {
            $veiculo = buscarVeiculo($con, $request_body['id_veiculo'], $plano);
            if (count($veiculo) > 0 && is_numeric($veiculo["codigo"])) {
                unset($veiculo["codigo"]);          
                $xml = synchronyJson('fRemoverProdutos', array('intervenienteCnpj' => EMPRESA, 'idInterveniente' => SENHA, 'xmlOriginal' => $veiculo));
                //echo "<pre>"; print_r($xml); exit;
                if ($xml->veiculo->produtos->produto->status == "REMOVIDO") {
                    try {
                        atualizaProprietarioVeiculo($con, $request_body['id_veiculo'], null);
                        addLog($con, $request_body['id_veiculo'], 'E');
                        echo 'OK';
                    } catch (Exception $e) {
                        echo 'Ocorreu um ao atualizar as informações: ' . $e->getMessage();
                    }
                    exit;
                }
                echo 'Ocorreu um erro ao sincronizar: ' . $xml->veiculo->produtos->produto->status;
                exit;
            }
            echo "Nenhum veículo encontrado";
            exit;
        }
        echo "Nenhum Plano encontrado";
        exit;
    }
    echo "Campo de código de Veiculo é obrigatório";
    exit;
}

function synchronyJson($resource, $options = []) {
    $client = new SoapClient(ENDPOINT . '?WSDL', array('trace' => true));
    $result = $client->__soapCall($resource, $options);
    //$result = $client->__getFunctions();
    //print_r($result); exit;
    return simplexml_load_file($result);
}

function buscarVeiculo($con, $id_veiculo, $plano) {
    $sql = "select top 1 fd.id_fornecedores_despesas, razao_social, cnpj, data_nascimento, sexo, endereco, numero, complemento, bairro,
        inscricao_estadual, org_exped_rg, num_cnh, cat_cnh,
        (select top 1 cidade_nome from tb_cidades where cidade_codigo = fd.cidade) as cidade, 
        (select top 1 estado_sigla from tb_estados where estado_codigo = fd.estado) as estado,
        cep, (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 2) as email,
        (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 0) as telefone,
        (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 1) as celular,
        marca, modelo, tipo_veiculo, placa, cambio, ano_modelo, ano_fab, combustivel, gas_natural, chassi, renavam, codigo_fipe, valor, ve.id, id_gobody, id_externo,
        n_motor, kms, cilindrada, n_passageiros, isnull(categoria,1) categoria,
        (select top 1 nome_produto_cor from sf_produtos_cor where id_produto_cor = ve.cor) as cor,
        (select top 1 isnull(v_tipo, 1) from sf_veiculo_tipos where id = tipo_veiculo) t_veiculo
        from sf_fornecedores_despesas fd 
        inner join sf_fornecedores_despesas_veiculo ve on ve.id_fornecedores_despesas = fd.id_fornecedores_despesas
        where len(isnull(ve.chassi, '')) > 0 and ve.id = " . valoresSelect2($id_veiculo) . ";";
    $rs = odbc_exec($con, $sql);
    $data = [];
    while ($row = odbc_fetch_array($rs)) {
        $dadosAssociado = [
            'codigo' => $row['id_externo'],
            'item' => [
                'dadosAssociado' => [
                    'nomeAssociado' => utf8_encode($row['razao_social']),
                    'cpf' => preg_replace('/[^0-9]/', '', $row['cnpj']),
                    'rg' => utf8_encode($row['inscricao_estadual']),
                    'dataExpedicaoRg' => '0000-00-00',
                    'orgaoExpedidor' => utf8_encode($row['org_exped_rg']),
                    'sexo' => $row['sexo'],
                    'profissao' => utf8_encode($row['profissao']),
                    'receberEmail' => 'N',
                    'receberSMS' => 'N',
                    'cep' => utf8_encode($row['cep']),
                    'logradouro' => utf8_encode($row['endereco']),
                    'numero' => utf8_encode($row['numero']),
                    'complemento' => utf8_encode($row['complemento']),
                    'bairro' => utf8_encode($row['bairro']),
                    'cidade' => utf8_encode($row['cidade']),
                    'estado' => utf8_encode($row['estado']),
                    'telefone' => [
                        'telefoneItem' => [
                            'tipoTelefone' => '1',
                            'ddd' => substr(preg_replace('/[^0-9]/', '', $row['celular']), 0, 2),
                            'numeroTelefone' => substr(preg_replace('/[^0-9]/', '', $row['celular']), 2),
                            'operadora' => 'NAO INFORMADO',
                            'departamentoTelefone' => 'CLIENTE',
                            'observacaoTelefone' => 'NAO INFORMADO',
                        ]
                    ],
                    'email' => [
                        'emailItem' => [
                            'tipoEmail' => 'PESSOAL',
                            'email' => utf8_encode($row['email']),
                            'departamentoEmail' => 'CLIENTE',
                        ]
                    ]
                ],
                'veiculos' => [
                    'veiculoItem' => [
                        'dadosVeiculo' => [
                            'id' => $row['id_externo'],
                            'idExternoVeiculo' => '',
                            'placa' => str_replace('-', '', $row['placa']),
                            'chassi' => $row['chassi'],
                            'modelo' => utf8_encode($row['modelo']),
                            'anoModelo' => $row['ano_modelo'],
                            'anoFabricacao' => $row['ano_fab'],
                            'tipoVeiculo' => ($row['t_veiculo'] == 3 ? "3" : ($row['t_veiculo'] == 2 ? "2" : "4")),
                            'categoriaVeiculo' => $row['categoria'],
                            'codigoFipe' => $row['codigo_fipe'],
                            'valorFipe' => $row['valor'],
                            'combustivel' => utf8_encode($row['combustivel']),
                            'cor' => utf8_encode($row['cor']),
                            'renavam' => utf8_encode($row['renavam']),
                            'kmRodado' => utf8_encode($row['kms']),
                            'numeroPassageiros' => $row['n_passageiros'],
                            'tipoCarga' => 'NAO INFORMADO',
                            'tipoCarroceria' => 'NAO INFORMADO',
                        ],
                        'produtos' => [
                            'produtoItem' => [
                                'codigoProduto' => $plano
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }
    return $dadosAssociado;
}

function filtroNegociacoes($con, $id_veiculo) {
    $toReturn = "";
    $sql = "select top 1 codigo_interno from sf_vendas_planos vp
    inner join sf_vendas_planos_acessorios vpa on vp.id_plano = vpa.id_venda_plano
    inner join sf_produtos p on p.conta_produto = vpa.id_servico_acessorio
    where vp.id_veiculo = " . valoresSelect2($id_veiculo) . "
    and vpa.id_servico_acessorio in (select conta_produto from sf_produtos where len(isnull(codigo_interno, '')) > 0);";
    $res = odbc_exec($con, $sql);
    while ($row = odbc_fetch_array($res)) {
        $toReturn = $row['codigo_interno'];
    }
    return $toReturn;
}

function atualizaProprietarioVeiculo($con, $id_veiculo, $codigo) {
    try {
        odbc_autocommit($con, false);
        $sqlAtualizaVeiculo = "update sf_fornecedores_despesas_veiculo set id_externo = " . valoresSelect2($codigo) . " where id = " . valoresSelect2($id_veiculo);
        odbc_exec($con, $sqlAtualizaVeiculo);
        odbc_commit($con);
    } catch (Exception $e) {
        odbc_rollback($con);
        throw new Exception($e->getMessage());
    } finally {
        odbc_autocommit($con, true);
    }
}

function addLog($con, $id, $tipo) {
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
    ('sf_fornecedores_despesas_veiculo', " . $id . ", '" . (isset($_SESSION["login_usuario"]) ? $_SESSION["login_usuario"] : 'SISTEMA') . "',
    '" . $tipo . "', '" . ($tipo == "E" ? "EXCLUSAO" : "INCLUSAO") . " ASSISTENCIA 24H: " . $id . "', GETDATE(),(select id_fornecedores_despesas from sf_fornecedores_despesas_veiculo where id = " . $id . "));") or die(odbc_errormsg());
}
