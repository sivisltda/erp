<?php

$sql = "select top 1 wa_ambiente, wa_senha, wa_login, wa_chave from sf_configuracao;";
$rs = odbc_exec($con, $sql);
while ($row = odbc_fetch_array($rs)) {
    $homologacao = $row['wa_ambiente'] ? 'assistme' : 'teste';
    define('ENDPOINT', 'http://assistencia.logicasolucoes.com.br/' . $homologacao . '/cadastroService/');
    define('LOGIN_INFO', $row['wa_login']);
    define('SENHA_INFO', $row['wa_senha']);
    define('CHAVE_INFO', $row['wa_chave']);
}

$request_body = $_REQUEST;

if ($request_body['functionPage'] == "getNegociacoes") {
    $produtos = getProdutos();
    echo json_encode($produtos);
    exit;
}

if ($request_body['functionPage'] == 'getTodosBeneficiarios') {
    $beneficiarios = logicasolucoesJson('listaItem', [
        'pagina' => 0
    ]);
    echo json_encode($beneficiarios);
    exit;
}

if ($request_body['functionPage'] == 'getBeneficiarioPorCPF') {
    if (isset($request_body['cpf'])) {
        $beneficiario = logicasolucoesJson('getItem', [
            'beneficiarioCpf' => $request_body['cpf'], 'tipo' => 'VEICULO'
        ]);
        echo json_encode($beneficiario);
        exit;
    }
    echo "Campo de CPF obrigatório";
    exit;
}

if ($request_body['functionPage'] == 'criarNovoVeiculo') {
    if (isset($request_body['id_veiculo']) && $request_body['id_veiculo']) {
        $data = buscarVeiculo($con, $request_body['id_veiculo']);
        $produto = filtroNegociacoes($con, $request_body['id_veiculo']);
        $data = array_merge($data, $produto);
        if (count($data) > 0) {
            //print_r($data); exit;
            $veiculo = logicasolucoesJson('incluir', $data);
            //print_r($veiculo); exit;
            if ($veiculo->retorno) {
                try {
                    atualizaProprietarioVeiculo($con, $request_body['id_veiculo'], $veiculo);
                    addLog($con, $request_body['id_veiculo'], (isset($request_body['excluir']) ? 'E' : 'I'));                    
                    echo 'OK';
                } catch (Exception $e) {
                    echo 'Ocorreu um ao atualizar as informações: ' . $e->getMessage();
                }
                exit;
            }
            echo 'Ocorreu um erro ao sincronizar: ' . $veiculo->mensagem;
            exit;
        }
        echo "Nenhum veículo encontrado";
        exit;
    }
    echo "Campo de código de Veiculo é obrigatório";
    exit;
}

if ($request_body['functionPage'] == 'alterarVeiculo') {
    if (isset($request_body['id_veiculo']) && $request_body['id_veiculo']) {
        $data = buscarVeiculo($con, $request_body['id_veiculo']);
        $produto = filtroNegociacoes($con, $request_body['id_veiculo']);
        $data = array_merge($data, $produto);
        if (count($data) > 0) {
            if (!isset($request_body['excluir'])) {
                $data["tipoAlteracao"] = "A";
            }
            $veiculo = logicasolucoesJson((isset($request_body['excluir']) ? 'remover' : 'alterar'), $data);
            if ($veiculo->retorno) {
                try {
                    $veiculo->codigo = (isset($request_body['excluir']) ? '' : $veiculo->codigo);
                    atualizaProprietarioVeiculo($con, $request_body['id_veiculo'], $veiculo);
                    addLog($con, $request_body['id_veiculo'], (isset($request_body['excluir']) ? 'E' : 'I'));                    
                    echo 'OK';
                } catch (Exception $e) {
                    echo 'Ocorreu um ao atualizar as informações: ' . $e->getMessage();
                }
                exit;
            }
            echo 'Ocorreu um erro ao sincronizar: ' . $veiculo->mensagem;
            exit;
        }
        echo "Nenhum veículo encontrado";
        exit;
    }
    echo "Campo de código de Veiculo é obrigatório";
    exit;
}

function logicasolucoesJson($resource, $data = []) {
    $url = ENDPOINT;
    $header = [
        "Cache-Control: no-cache",
        "Accept: application/json"
    ];
    $ch = curl_init();
    $data = array_merge($data, ['usuario' => LOGIN_INFO, 'senha' => SENHA_INFO, 'clienteCNPJ' => CHAVE_INFO]);
    curl_setopt($ch, CURLOPT_URL, $url . "/" . $resource);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $output = curl_exec($ch);
    curl_close($ch);
    return json_decode($output);
}

function getProdutos() {
    return logicasolucoesJson('listaNegociacao');
}

function buscarVeiculo($con, $id_veiculo) {
    $sql = "select top 1 fd.id_fornecedores_despesas, razao_social, cnpj, data_nascimento, sexo, endereco, numero, complemento, bairro, 
        (select top 1 cidade_nome from tb_cidades where cidade_codigo = fd.cidade) as cidade, 
        (select top 1 estado_sigla from tb_estados where estado_codigo = fd.estado) as estado,
        cep, (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 2) as email,
        (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 0) as telefone,
        (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 1) as celular,
        marca, modelo, tipo_veiculo, placa, cambio, ano_modelo, ano_fab, combustivel, gas_natural, chassi, renavam, codigo_fipe, valor, ve.id, id_gobody, id_externo,
        (select top 1 nome_produto_cor from sf_produtos_cor where id_produto_cor = ve.cor) as cor,
        (select top 1 descricao from sf_veiculo_tipos where id = tipo_veiculo) t_veiculo
        from sf_fornecedores_despesas fd 
        inner join sf_fornecedores_despesas_veiculo ve on ve.id_fornecedores_despesas = fd.id_fornecedores_despesas
        where len(isnull(ve.chassi, '')) > 0 and ve.id = " . valoresSelect2($id_veiculo) . ";";
    $rs = odbc_exec($con, $sql);
    $data = [];
    while ($row = odbc_fetch_array($rs)) {
        $data = [
            'codigo' => $row['id_externo'],
            'beneficiarioCpf' => preg_replace('/[^0-9]/', '', $row['cnpj']),
            'beneficiarioNome' => utf8_encode($row['razao_social']),
            'beneficiarioDataNascimento' => escreverData($row['data_nascimento']),
            'beneficiarioSexo' => ($row['sexo'] == "M" ? "Masculino" : "Feminino"),
            'beneficiarioCep' => $row['cep'],
            'beneficiarioEnderecoCep' => $row['cep'],
            'beneficiarioEnderecoEndereco' => utf8_encode($row['endereco']),
            'beneficiarioEnderecoComplemento' => utf8_encode($row['complemento']),
            'beneficiarioEnderecoNumero' => $row['numero'],
            'beneficiarioEnderecoBairro' => utf8_encode($row['bairro']),
            'beneficiarioEnderecoCidade' => utf8_encode($row['cidade']),
            'beneficiarioEnderecoEstado' => utf8_encode($row['estado']),
            'beneficiarioTelefoneTelefone' => $row['telefone'],
            'beneficiarioTelefone1Telefone' => $row['celular'],
            'tipo' => 'VEICULO',
            'veiculoChassi' => $row['chassi'],
            'veiculoPlaca' => str_replace('-', '', $row['placa']),
            'veiculoRenavam' => $row['renavam'],
            'veiculoTipo' => utf8_encode($row['t_veiculo']),
            'veiculoAnoFabricacao' => $row['ano_fab'],
            'veiculoAnoModelo' => $row['ano_modelo'],
            'veiculoMarca' => utf8_encode($row['marca']),
            'veiculoModelo' => utf8_encode($row['modelo']),
            'veiculoCor' => $row['cor'],
            'veiculoCambio' => ($row['cambio'] == "1" ? "MANUAL" : "AUTOMATICO"),
            'veiculoFipeCodigo' => $row['codigo_fipe'],
            'veiculoFipeValor' => escreverNumero($row['valor'])
        ];
    }
    return $data;
}

function filtroNegociacoes($con, $id_veiculo) {
    $toReturn = "";
    $sql = "select top 1 codigo_interno from sf_vendas_planos vp
            inner join sf_vendas_planos_acessorios vpa on vp.id_plano = vpa.id_venda_plano
            inner join sf_produtos p on p.conta_produto = vpa.id_servico_acessorio
            where vp.id_veiculo = " . valoresSelect2($id_veiculo) . "
            and vpa.id_servico_acessorio in (select conta_produto from sf_produtos where len(isnull(codigo_interno, '')) > 0)
            order by id_plano desc;";
    $res = odbc_exec($con, $sql);
    while ($row = odbc_fetch_array($res)) {
        $toReturn = $row['codigo_interno'];
    }
    return ['planoCodigo' => $toReturn];
}

function atualizaProprietarioVeiculo($con, $id_veiculo, $data) {
    try {
        odbc_autocommit($con, false);
        $sqlAtualizaVeiculo = "update sf_fornecedores_despesas_veiculo set id_externo = " . valoresSelect2($data->codigo) . " where id = " . valoresSelect2($id_veiculo);
        odbc_exec($con, $sqlAtualizaVeiculo);
        odbc_commit($con);
    } catch (Exception $e) {
        odbc_rollback($con);
        throw new Exception($e->getMessage());
    } finally {
        odbc_autocommit($con, true);
    }
}

function addLog($con, $id, $tipo) {
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
    ('sf_fornecedores_despesas_veiculo', " . $id . ", '" . (isset($_SESSION["login_usuario"]) ? $_SESSION["login_usuario"] : 'SISTEMA') . "',
    '" . $tipo . "', '" . ($tipo == "E" ? "EXCLUSAO" : "INCLUSAO") . " ASSISTENCIA 24H: " . $id . "', GETDATE(),(select id_fornecedores_despesas from sf_fornecedores_despesas_veiculo where id = " . $id . "));") or die(odbc_errormsg());
}