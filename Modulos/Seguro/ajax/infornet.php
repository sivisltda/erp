<?php

$sql = "select top 1 wa_ambiente, wa_empresa, wa_senha, wa_login, wa_chave from sf_configuracao;";
$rs = odbc_exec($con, $sql);
while ($row = odbc_fetch_array($rs)) {
    $homologacao = $row['wa_ambiente'] ? $row['wa_empresa'] : 'homologacao';
    define('ENDPOINT', 'https://sistemas.infornet.net.br/webassist/' . $homologacao . '/ws/rest/api_v1/');
    define('LOGIN_INFO', $row['wa_login']);
    define('SENHA_INFO', $row['wa_senha']);
    define('CHAVE_INFO', $row['wa_chave']);
}

$request_body = $_REQUEST;

if ($request_body['functionPage'] == "getCombustivel") {
    $combustiveis = getCombustivel();
    echo json_encode($combustiveis);
    exit;
}

if ($request_body['functionPage'] == "getNegociacoes") {
    $produtos = getProdutos();
    echo json_encode($produtos);
    exit;
}

if ($request_body['functionPage'] == 'getRetornos') {
    $retornos = getStatusRetornos();
    echo json_encode($retornos);
    exit;
}

if ($request_body['functionPage'] == 'getTodosBeneficiarios') {
    $beneficiarios = infornetJson('beneficiario/consultarBeneficiarios.php');
    echo json_encode($beneficiarios);
    exit;
}

if ($request_body['functionPage'] == 'getBeneficiarioPorCPF') {
    if (isset($request_body['cpf'])) {
        $beneficiario = infornetJson('beneficiario/consultarBeneficiario.php', [
            'documentoBeneficiario' => $request_body['cpf']
        ]);
        echo json_encode($beneficiario);
        exit;
    }
    echo "Campo de CPF obrigatório";
    exit;
}

if ($request_body['functionPage'] == 'getStatusTransmissao') {
    if (isset($request_body['codTransmissao'])) {
        $transmissao = infornetJson('parametros/consultarSituacaoTransmissao.php', [
            'codTransmissao' => $request_body['codTransmissao']
        ]);
        echo json_encode($transmissao);
        exit;
    }
    echo "Campo de código de Transmissão é obrigatório";
    exit;
}

if ($request_body['functionPage'] == 'getProdutoVeiculo') {
    if (isset($request_body['id_veiculo'])) {
        $data = buscarVeiculo($con, $request_body['id_veiculo']);
        if (count($data) > 0) {
            $veiculo = infornetJson('veiculo/consultarProdutos.php', [
                'cpf' => $data['documentoBeneficiario'],
                'chassi' => $data['chassiVeiculo']
            ]);
            echo json_encode($veiculo);
            exit;
        }
        echo "Nenhum veículo encontrado";
        exit;
    }
    echo "Campo de código de Veiculo é obrigatório";
    exit;
}

if ($request_body['functionPage'] == 'criarNovoVeiculo') {
    if (isset($request_body['id_veiculo']) && $request_body['id_veiculo']) {        
        $data = buscarVeiculo($con, $request_body['id_veiculo']);
        if (count($data) > 0) {               
            $data['codCombustivel'] = filtraCombustivel($data['combustivel'], $data['gas_natural']);    
            $produto = filtroNegociacoes($data['tipo_veiculo']);        
            $data = array_merge($data, $produto);            
            try {
                foreach (listaNegociacoes($con, $request_body['id_veiculo']) as $codProduto) {
                    $data_total = $data;
                    $data_total["codProduto"] = $codProduto;
                    $veiculo = infornetJson('veiculo/novo.php', $data_total);
                    if ($veiculo->retorno) {
                        atualizaProprietarioVeiculo($con, $request_body['id_veiculo'], $data_total['id_fornecedores_despesas'], $veiculo);
                        addLog($con, $request_body['id_veiculo'], $codProduto, (isset($request_body['excluir']) ? 'E' : 'I'));  
                    } else {
                        echo 'Ocorreu um erro ao sincronizar: ' . $veiculo->mensagem;            
                    }
                }
                echo 'OK'; exit;
            } catch (Exception $e) {
                echo 'Ocorreu um ao atualizar as informações: ' . $e->getMessage(); exit;
            }
        } else {
            echo "Nenhum veículo encontrado"; exit;
        }
    }
    echo "Campo de código de Veiculo é obrigatório"; exit;
}

if ($request_body['functionPage'] == 'alterarVeiculo') {
    if (isset($request_body['id_veiculo']) && $request_body['id_veiculo']) {
        $data = buscarVeiculo($con, $request_body['id_veiculo']);
        if (count($data) > 0) {
            $data['codCombustivel'] = filtraCombustivel($data['combustivel'], $data['gas_natural']);
            $data['movimentacao'] = isset($request_body['trocar']) ? 'T' : (isset($request_body['excluir']) ? 'E' : 'A');
            $produto = filtroNegociacoes($data['tipo_veiculo']);        
            $data = array_merge($data, $produto);  
            try {
                foreach (listaNegociacoes($con, $request_body['id_veiculo']) as $codProduto) {
                    $data_total = $data;
                    $data_total["codProduto"] = $codProduto;          
                    $veiculo = infornetJson('veiculo/alterar.php', $data_total);
                    if ($veiculo->retorno) {                                       
                        $veiculo->codVeiculo = $data_total['movimentacao'] === 'E' ? '' : $veiculo->codVeiculo;
                        atualizaProprietarioVeiculo($con, $request_body['id_veiculo'], $data_total['id_fornecedores_despesas'], $veiculo);
                        addLog($con, $request_body['id_veiculo'], $codProduto, (isset($request_body['excluir']) ? 'E' : 'I'));
                    } else {
                        echo 'Ocorreu um erro ao sincronizar: ' . $veiculo->mensagem;            
                    }
                }
                echo 'OK'; exit;
            } catch (Exception $e) {
                echo 'Ocorreu um ao atualizar as informações: ' . $e->getMessage();
            }                
        } else {
            echo "Nenhum veículo encontrado"; exit;    
        }
    }
    echo "Campo de código de Veiculo é obrigatório"; exit;
}

function infornetJson($resource, $data = []) {
    $url = ENDPOINT;
    $header = [
        "Cache-Control: no-cache",
        "Accept: application/json"
    ];
    $ch = curl_init();
    $data = array_merge($data, ['login' => LOGIN_INFO, 'senha' => SENHA_INFO, 'chaveCliente' => CHAVE_INFO]);
    curl_setopt($ch, CURLOPT_URL, $url . "/" . $resource);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $output = curl_exec($ch);
    curl_close($ch);
    return json_decode($output);
}

function buscarVeiculo($con, $id_veiculo) {
    $sql = "select top 1 fd.id_fornecedores_despesas, razao_social, cnpj, data_nascimento, endereco, numero, complemento, bairro, 
        (select top 1 cidade_nome from tb_cidades where cidade_codigo = fd.cidade) as cidade, 
        (select top 1 estado_sigla from tb_estados where estado_codigo = fd.estado) as estado,
        cep, (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 2) as email,
        (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 1) as celular,
        (select top 1 v_cod from sf_veiculo_tipos where id = tipo_veiculo) tipo_veiculo, marca, modelo, placa, ano_modelo, combustivel, gas_natural, chassi, renavam, codigo_fipe, valor, ve.id, id_gobody, id_externo,
        (select top 1 nome_produto_cor from sf_produtos_cor where id_produto_cor = ve.cor) as cor
        from sf_fornecedores_despesas fd 
        inner join sf_fornecedores_despesas_veiculo ve on ve.id_fornecedores_despesas = fd.id_fornecedores_despesas
        where len(isnull(ve.chassi, '')) > 0 and ve.id = " . valoresSelect2($id_veiculo) . ";";
    $rs = odbc_exec($con, $sql);
    $data = [];
    while ($row = odbc_fetch_array($rs)) {
        $data = [
            'id_fornecedores_despesas' => $row['id_fornecedores_despesas'],
            'nomeBeneficiario' => utf8_encode($row['razao_social']),
            'documentoBeneficiario' => preg_replace('/[^0-9]/', '', $row['cnpj']),
            'dataNascimentoBeneficiario' => escreverData($row['data_nascimento'], 'Y-m-d'),
            'logradouro' => utf8_encode($row['endereco']),
            'numero' => $row['numero'],
            'complemento' => utf8_encode($row['complemento']),
            'bairro' => utf8_encode($row['bairro']),
            'cidade' => utf8_encode($row['cidade']),
            'estado' => utf8_encode($row['estado']),
            'celular' => $row['celular'],
            'montadoraVeiculo' => utf8_encode($row['marca']),
            'modeloVeiculo' => utf8_encode($row['modelo']),
            'corVeiculo' => $row['cor'],
            'placaVeiculo' => str_replace('-', '', $row['placa']),
            'anoModeloVeiculo' => $row['ano_modelo'],
            'chassiVeiculo' => $row['chassi'],
            'renavamVeiculo' => $row['renavam'],
            'codigoFipe' => $row['codigo_fipe'],
            'valorFipe' => escreverNumero($row['valor']),
            'combustivel' => utf8_encode($row['combustivel']),
            'tipo_veiculo' => $row['tipo_veiculo'],
            'gas_natural' => $row['gas_natural'],
            'codBeneficiario' => $row['id_gobody'],
            'codVeiculo' => $row['id_externo']
        ];
    }
    return $data;
}

function getCombustivel() {
    return infornetJson('parametros/consultarCombustivel.php');
}

function getProdutos() {
    return infornetJson('parametros/consultarNegociacoes.php');
}

function getStatusRetornos() {
    return infornetJson('parametros/consultarRetornos.php');
}

function filtraCombustivel($combustivel, $gas_natural) {
    $combustiveis = getCombustivel();
    if ($combustiveis->retorno == 1) {
        $combustivel = $combustiveis == 'Álcool' ? 'Etanol' : ($combustivel == 'Gasolina' && $gas_natural ? 'GASOLINA + GÁS' : $combustivel);
        $item = array_filter($combustiveis->combustivel, function($c) use($combustivel) {
            return strtolower(utf8_decode($c->nomeCombustivel)) == strtolower($combustivel);
        });
        if (count($item) > 0) {
            return current($item)->codigoCombustivel;
        } else {
            return 8;
        }
    }
    return null;
}

function listaNegociacoes($con, $id_veiculo) {
    $toReturn = [];
    $sql = "select codigo_interno from sf_vendas_planos vp
            inner join sf_vendas_planos_acessorios vpa on vp.id_plano = vpa.id_venda_plano
            inner join sf_produtos p on p.conta_produto = vpa.id_servico_acessorio
            where vp.id_veiculo = " . valoresSelect2($id_veiculo) . " and vpa.id_servico_acessorio in (select conta_produto from sf_produtos where len(isnull(codigo_interno, '')) > 0 and conta_movimento in(
            select id_contas_movimento from sf_contas_movimento where grupo_conta = 35));";
    $res = odbc_exec($con, $sql);
    while ($row = odbc_fetch_array($res)) {
        $toReturn[] = $row['codigo_interno'];
    }
    return $toReturn;
}

function filtroNegociacoes($tipo_veiculo) {
    $produtos = getProdutos();
    $codTipoVeiculo = null;
    if ($produtos->retorno == 1) {
        $item = array_filter($produtos->negociacoes, function($p) use($tipo_veiculo) {
            return strtolower($p->codigoTipoVeiculo) == strtolower($tipo_veiculo);
        });
        if (count($item) > 0) {
            $codTipoVeiculo = current($item)->codigoTipoVeiculo;
        }        
    }
    return ['codTipoVeiculo' => $codTipoVeiculo, 'codProduto' => ""];
}

function atualizaProprietarioVeiculo($con, $id_veiculo, $id_fornecedores, $data) {
    try {
        odbc_autocommit($con, false);
        $sqlAtualizaVeiculo = "update sf_fornecedores_despesas_veiculo set id_externo = " . valoresSelect2($data->codVeiculo) . " where id = " . valoresSelect2($id_veiculo);
        odbc_exec($con, $sqlAtualizaVeiculo);
        $sqlAtualizaProprietario = "update sf_fornecedores_despesas set id_gobody = " . valoresSelect2($data->codBeneficiario) . " where id_fornecedores_despesas = " . valoresSelect2($id_fornecedores);
        odbc_exec($con, $sqlAtualizaProprietario);
        odbc_commit($con);
    } catch (Exception $e) {
        odbc_rollback($con);
        throw new Exception($e->getMessage());
    } finally {
        odbc_autocommit($con, true);
    }
}

function addLog($con, $id, $codProduto, $tipo) {
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
    ('sf_fornecedores_despesas_veiculo', " . $id . ", '" . (isset($_SESSION["login_usuario"]) ? $_SESSION["login_usuario"] : 'SISTEMA') . "',
    '" . $tipo . "', '" . ($tipo == "E" ? "EXCLUSAO" : "INCLUSAO") . " ASSISTENCIA 24H: " . $id . " - " . $codProduto . "', GETDATE(),(select id_fornecedores_despesas from sf_fornecedores_despesas_veiculo where id = " . $id . "));") or die(odbc_errormsg());
}