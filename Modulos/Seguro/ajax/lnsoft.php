<?php

$sql = "select top 1 wa_ambiente, wa_senha, wa_login, wa_chave, wa_empresa from sf_configuracao;";
$rs = odbc_exec($con, $sql);
while ($row = odbc_fetch_array($rs)) {
    define('ENDPOINT', 'https://api.lnsoft.com.br/devsul/integracao/veiculos');
    define('CHAVE_INFO', utf8_encode($row['wa_chave']));
    define('COD_EMPRESA', utf8_encode($row['wa_empresa']));
}

$request_body = $_REQUEST;

if ($request_body['functionPage'] == 'criarNovoVeiculo') {
    if (isset($request_body['id_veiculo']) && $request_body['id_veiculo']) {
        $data = buscarVeiculo($con, $request_body['id_veiculo'], (isset($request_body['excluir']) ? 'INATIVO' : 'ATIVO'));
        if (count($data) > 0) {
            unset($data["codigo"]);              
            $veiculo = synchronyJson(json_encode($data));
            //print_r($veiculo); exit;                
            if ($veiculo->value == "1") {
                try {
                    atualizaProprietarioVeiculo($con, $request_body['id_veiculo'], $request_body['id_veiculo']);
                    addLog($con, $request_body['id_veiculo'], (isset($request_body['excluir']) ? 'E' : 'I'));
                    echo 'OK';
                } catch (Exception $e) {
                    echo 'Ocorreu um ao atualizar as informações: ' . $e->getMessage();
                }
                exit;
            }
            echo 'Ocorreu um erro ao sincronizar: ' . print_r($veiculo);
            exit;
        } else {
            echo "Nenhum veículo encontrado";
            exit;
        }
    }
    echo "Campo de código de Veiculo é obrigatório";
    exit;
}

if ($request_body['functionPage'] == 'alterarVeiculo') {
    if (isset($request_body['id_veiculo']) && $request_body['id_veiculo']) {
        $data = buscarVeiculo($con, $request_body['id_veiculo'], (isset($request_body['excluir']) ? 'INATIVO' : 'ATIVO'));
        if (count($data) > 0) {
            unset($data["codigo"]);         
            $veiculo = synchronyJson(json_encode($data));
            //print_r($veiculo); exit;            
            if ($veiculo->value == "1") {
                try {
                    atualizaProprietarioVeiculo($con, $request_body['id_veiculo'], (isset($request_body['excluir']) ? 'null' : $request_body['id_veiculo']));
                    addLog($con, $request_body['id_veiculo'], (isset($request_body['excluir']) ? 'E' : 'I'));
                    echo 'OK';
                } catch (Exception $e) {
                    echo 'Ocorreu um ao atualizar as informações: ' . $e->getMessage();
                }
                exit;
            }
            echo 'Ocorreu um erro ao sincronizar: ' . print_r($veiculo);
            exit;
        } else {
            echo "Nenhum veículo encontrado";
            exit;
        }
    }
    echo "Campo de código de Veiculo é obrigatório";
    exit;
}

function synchronyJson($data = []) {
    $header = [
        "Authorization: Bearer " . CHAVE_INFO,
        "Content-Type: application/json"
    ];
    return mkCurl($data, $header);
}

function mkCurl($data = [], $header = []) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, ENDPOINT);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $output = curl_exec($ch);
    //echo $output;
    curl_close($ch);
    return json_decode($output);
}

function buscarVeiculo($con, $id_veiculo, $status) {
    $data = [];    
    $produtos = filtroNegociacoes($con, valoresSelect2($id_veiculo), $status);
    $sql = "select top 1 fd.id_fornecedores_despesas, razao_social, cnpj, inscricao_estadual, data_nascimento, dt_cadastro, sexo, endereco, numero, complemento, bairro, cep, 
        (select top 1 cidade_nome from tb_cidades where cidade_codigo = fd.cidade) as cidade, 
        (select top 1 estado_sigla from tb_estados where estado_codigo = fd.estado) as estado,        
        (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 2) as email,
        (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 0) as telefone,
        (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 1) as celular,                
        (select top 1 nome_produto_cor from sf_produtos_cor where id_produto_cor = ve.cor) as cor,
        (select top 1 descricao from sf_veiculo_tipos where id = tipo_veiculo) t_veiculo,
        (select count(vc.id) total from sf_fornecedores_despesas_veiculo vc where id_externo is not null and vc.id_fornecedores_despesas = fd.id_fornecedores_despesas and vc.id not in (ve.id)) total_vc,
        marca, modelo, tipo_veiculo, placa, cambio, ano_modelo, ano_fab, combustivel, gas_natural, chassi, renavam, codigo_fipe, valor, ve.id, id_gobody, id_externo        
        from sf_fornecedores_despesas fd 
        inner join sf_fornecedores_despesas_veiculo ve on ve.id_fornecedores_despesas = fd.id_fornecedores_despesas
        where len(isnull(ve.chassi, '')) > 0 and ve.id = " . valoresSelect2($id_veiculo) . ";";
    $rs = odbc_exec($con, $sql);
    while ($row = odbc_fetch_array($rs)) {
        $data = [
            "codigo" => $row['id_externo'],
            "CodigoExterno" => $row['id'],
            "Logradouro" => utf8_encode($row['endereco']),
            "ClassificacaoBeneficiario"  => "",
            "RG" => $row['inscricao_estadual'],
            "Cor" => $row['cor'],                    
            "Email" => $row['email'],
            "Bairro" => utf8_encode($row['bairro']),
            "DataNascimento" => escreverData($row['data_nascimento'], "Y-m-d"),
            "Chassi" => $row['chassi'],                    
            "DDDCelular" => substr(preg_replace('/[^0-9]/', '', $row['celular']), 0, 2),
            "DDD" => substr(preg_replace('/[^0-9]/', '', (strlen($row['telefone']) > 0 ? $row['telefone'] : $row['celular'])), 0, 2),
            "DataAdesao" => escreverData($row['dt_cadastro'], "Y-m-d"),
            "Cliente" => (float) COD_EMPRESA,
            "DataContrato" => escreverData($row['dt_cadastro'], "Y-m-d"),
            "Modelo" => utf8_encode($row['modelo']),                    
            "AnoFabricacao" => $row['ano_fab'],                    
            "Cep" => $row['cep'],
            "Documento" => preg_replace('/[^0-9]/', '', $row['cnpj']),
            "Numero" => $row['numero'],
            "Situacao" => $status,
            "Complemento" => utf8_encode($row['complemento']),
            "TipoVeiculo" => utf8_encode($row['t_veiculo']),
            "ClassificacaoVeiculo" => "",
            "Marca" => utf8_encode($row['marca']),                    
            "ValorFipe" => (float) valoresNumericos2(escreverNumero($row['valor'])),
            "Placa" => str_replace('-', '', $row['placa']),                    
            "Nome" => utf8_encode($row['razao_social']),
            "Combustivel" => $row['combustivel'],                    
            "Renavam" => $row['renavam'],                    
            "Telefone" => substr(preg_replace('/[^0-9]/', '', (strlen($row['telefone']) > 0 ? $row['telefone'] : $row['celular'])), 2),
            "Celular" => substr(preg_replace('/[^0-9]/', '', $row['celular']), 2), 
            "CodigoFipe" => $row['codigo_fipe'],                    
            "AnoModelo" => $row['ano_modelo'],                    
            "Produtos" => $produtos
        ];
    }
    return $data;
}

function filtroNegociacoes($con, $id_veiculo, $status) {    
    $toReturn = [];
    $sql = "select top 1 codigo_interno, codigo_barra from sf_vendas_planos vp
            inner join sf_vendas_planos_acessorios vpa on vp.id_plano = vpa.id_venda_plano
            inner join sf_produtos p on p.conta_produto = vpa.id_servico_acessorio
            where vp.id_veiculo = " . valoresSelect2($id_veiculo) . "
            and vpa.id_servico_acessorio in (select conta_produto from sf_produtos where len(isnull(codigo_interno, '')) > 0);";
    $res = odbc_exec($con, $sql);
    while ($row = odbc_fetch_array($res)) {
        $array = [];
        $array["Codigo"] = $row['codigo_interno'];
        $array["Descricao"] = utf8_encode($row['codigo_barra']);
        $toReturn[] = $array;
    }
    return $toReturn;
}

function atualizaProprietarioVeiculo($con, $id_veiculo, $id_sinc) {
    try {
        odbc_autocommit($con, false);
        $sqlAtualizaVeiculo = "update sf_fornecedores_despesas_veiculo set id_externo = " . valoresSelect2($id_sinc) . " where id = " . valoresSelect2($id_veiculo);
        odbc_exec($con, $sqlAtualizaVeiculo);
        odbc_commit($con);
    } catch (Exception $e) {
        odbc_rollback($con);
        throw new Exception($e->getMessage());
    } finally {
        odbc_autocommit($con, true);
    }
}

function addLog($con, $id, $tipo) {
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
    ('sf_fornecedores_despesas_veiculo', " . $id . ", '" . (isset($_SESSION["login_usuario"]) ? $_SESSION["login_usuario"] : 'SISTEMA') . "',
    '" . $tipo . "', '" . ($tipo == "E" ? "EXCLUSAO" : "INCLUSAO") . " ASSISTENCIA 24H: " . $id . "', GETDATE(),(select id_fornecedores_despesas from sf_fornecedores_despesas_veiculo where id = " . $id . "));") or die(odbc_errormsg());
}
