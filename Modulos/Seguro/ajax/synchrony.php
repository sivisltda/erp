<?php

$sql = "select top 1 wa_ambiente, wa_chave from sf_configuracao;";
$rs = odbc_exec($con, $sql);
while ($row = odbc_fetch_array($rs)) {
    define('ENDPOINT', 'https://' . ($row['wa_ambiente'] == "0" ? 'homologacao' : 'satelite') . '.ativo247.com.br/synchrony-web/api/v1/integration/sync/vehicle/');
    define('CHAVE_INFO', $row['wa_chave']);
}

$request_body = $_REQUEST;

if ($request_body['functionPage'] == "getNegociacoes") {
    //https://teste.sivisweb.com.br/Modulos/Seguro/ajax/assistencia.php?functionPage=getNegociacoes
    $produtos = getProdutos();
    echo json_encode($produtos);
    exit;
}

if ($request_body['functionPage'] == 'getTodosBeneficiarios') {
    //https://teste.sivisweb.com.br/Modulos/Seguro/ajax/assistencia.php?functionPage=getTodosBeneficiarios
    $beneficiarios = synchronyJson('list/all');
    echo json_encode($beneficiarios);
    exit;
}

if ($request_body['functionPage'] == 'criarNovoVeiculo') {
    if (isset($request_body['id_veiculo']) && $request_body['id_veiculo']) {
        $data = buscarVeiculo($con, $request_body['id_veiculo']);
        $produto = filtroNegociacoes($con, $request_body['id_veiculo']);
        $data = array_merge($data, $produto);
        if (count($data) > 0) {
            unset($data["codigo"]);            
            $veiculo = synchronyJson('active', json_encode($data));
            if ($veiculo->title == "SUCCES") {
                try {
                    atualizaProprietarioVeiculo($con, $request_body['id_veiculo'], $request_body['id_veiculo']);
                    addLog($con, $request_body['id_veiculo'], (isset($request_body['excluir']) ? 'E' : 'I'));                    
                    echo 'OK';
                } catch (Exception $e) {
                    echo 'Ocorreu um ao atualizar as informações: ' . $e->getMessage();
                }
                exit;
            }
            echo 'Ocorreu um erro ao sincronizar: ' . $veiculo->body;
            exit;
        } else {
            echo "Nenhum veículo encontrado";
            exit;
        }
    }
    echo "Campo de código de Veiculo é obrigatório";
    exit;
}

if ($request_body['functionPage'] == 'alterarVeiculo') {
    if (isset($request_body['id_veiculo']) && $request_body['id_veiculo']) {
        $data = buscarVeiculo($con, $request_body['id_veiculo']);
        $produto = filtroNegociacoes($con, $request_body['id_veiculo']);
        $data = array_merge($data, $produto);
        if (count($data) > 0) {
            if (isset($request_body['excluir'])) {
                $veiculo = synchronyJson('deactivate', json_encode(['chassi' => $data["chassi"]]));
            } else {
                unset($data["codigo"]);
                $veiculo = synchronyJson('active', json_encode($data));
            }
            if ($veiculo->title == "SUCCES" || $veiculo->title == "SUCCESS") {
                try {
                    atualizaProprietarioVeiculo($con, $request_body['id_veiculo'], (isset($request_body['excluir']) ? 'null' : $request_body['id_veiculo']));
                    addLog($con, $request_body['id_veiculo'], (isset($request_body['excluir']) ? 'E' : 'I'));                    
                    echo 'OK';
                } catch (Exception $e) {
                    echo 'Ocorreu um ao atualizar as informações: ' . $e->getMessage();
                }
                exit;
            }
            echo 'Ocorreu um erro ao sincronizar: ' . $veiculo->body;
            exit;
        } else {
            echo "Nenhum veículo encontrado";
            exit;
        }
    }
    echo "Campo de código de Veiculo é obrigatório";
    exit;    
}

function synchronyJson($resource, $data = []) {
    $header = [
        "Authorization: " . CHAVE_INFO,
        "Content-Type: application/json"
    ];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, ENDPOINT . "/" . $resource);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $output = curl_exec($ch); 
    //echo $output;
    curl_close($ch);
    return json_decode($output);
}

function getProdutos() {
    return synchronyJson('product/list');
}

function buscarVeiculo($con, $id_veiculo) {
    $sql = "select top 1 fd.id_fornecedores_despesas, razao_social, cnpj, data_nascimento, sexo, endereco, numero, complemento, bairro, 
        (select top 1 cidade_nome from tb_cidades where cidade_codigo = fd.cidade) as cidade, 
        (select top 1 estado_sigla from tb_estados where estado_codigo = fd.estado) as estado,
        cep, (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 2) as email,
        (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 0) as telefone,
        (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 1) as celular,
        marca, modelo, tipo_veiculo, placa, cambio, ano_modelo, ano_fab, combustivel, gas_natural, chassi, renavam, codigo_fipe, valor, ve.id, id_gobody, id_externo,
        (select top 1 nome_produto_cor from sf_produtos_cor where id_produto_cor = ve.cor) as cor,
        (select top 1 descricao from sf_veiculo_tipos where id = tipo_veiculo) t_veiculo
        from sf_fornecedores_despesas fd 
        inner join sf_fornecedores_despesas_veiculo ve on ve.id_fornecedores_despesas = fd.id_fornecedores_despesas
        where len(isnull(ve.chassi, '')) > 0 and ve.id = " . valoresSelect2($id_veiculo) . ";";
    $rs = odbc_exec($con, $sql);
    $data = [];
    while ($row = odbc_fetch_array($rs)) {
        $data = [
            'codigo' => $row['id_externo'],            
            'placa' => str_replace('-', '', $row['placa']),
            'chassi' => $row['chassi'],
            'renavam' => $row['renavam'],
            'cpf' => preg_replace('/[^0-9]/', '', $row['cnpj']),
            'rg' => null,
            'logradouro' => utf8_encode($row['endereco']),
            'numero' => $row['numero'],            
            'complemento' => utf8_encode($row['complemento']),            
            'bairro' => utf8_encode($row['bairro']),
            'cidade' => utf8_encode($row['cidade']),
            'estado' => utf8_encode($row['estado']),
            'cep' => $row['cep'],            
            'telefone' => $row['telefone'],
            'email' => $row['email'],            
            'ano_modelo' => $row['ano_modelo'],
            'ano_fabricacao' => $row['ano_fab'],            
            'codigo_fipe' => $row['codigo_fipe'],
            'valor_fipe' => valoresNumericos2(escreverNumero($row['valor'])),            
            'nome_associado' => utf8_encode($row['razao_social']),
            'data_nascimento' => escreverData($row['data_nascimento']),
            'telefone_celular' => $row['celular'],
            'descricao_marca' => utf8_encode($row['marca']),
            'descricao_modelo' => utf8_encode($row['modelo']),
            'descricao_cor' => $row['cor'],
            'descricao_combustivel' => $row['combustivel'],
            'descricao_tipoveiculo' => utf8_encode($row['t_veiculo'])
        ];
    }
    return $data;
}

function filtroNegociacoes($con, $id_veiculo) {
    $toReturn = [];
    $sql = "select top 1 codigo_interno, codigo_barra from sf_vendas_planos vp
            inner join sf_vendas_planos_acessorios vpa on vp.id_plano = vpa.id_venda_plano
            inner join sf_produtos p on p.conta_produto = vpa.id_servico_acessorio
            where vp.id_veiculo = " . valoresSelect2($id_veiculo) . "
            and vpa.id_servico_acessorio in (select conta_produto from sf_produtos where len(isnull(codigo_interno, '')) > 0);";
    $res = odbc_exec($con, $sql);
    while ($row = odbc_fetch_array($res)) {
        $toReturn = ['codigo_produto' => utf8_encode($row['codigo_interno']), 'descricao_produto' => utf8_encode($row['codigo_barra'])];
    }
    return $toReturn;
}

function atualizaProprietarioVeiculo($con, $id_veiculo, $id_sinc) {
    try {
        odbc_autocommit($con, false);
        $sqlAtualizaVeiculo = "update sf_fornecedores_despesas_veiculo set id_externo = " . valoresSelect2($id_sinc) . " where id = " . valoresSelect2($id_veiculo);
        odbc_exec($con, $sqlAtualizaVeiculo);
        odbc_commit($con);
    } catch (Exception $e) {
        odbc_rollback($con);
        throw new Exception($e->getMessage());
    } finally {
        odbc_autocommit($con, true);
    }
}

function addLog($con, $id, $tipo) {
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
    ('sf_fornecedores_despesas_veiculo', " . $id . ", '" . (isset($_SESSION["login_usuario"]) ? $_SESSION["login_usuario"] : 'SISTEMA') . "',
    '" . $tipo . "', '" . ($tipo == "E" ? "EXCLUSAO" : "INCLUSAO") . " ASSISTENCIA 24H: " . $id . "', GETDATE(),(select id_fornecedores_despesas from sf_fornecedores_despesas_veiculo where id = " . $id . "));") or die(odbc_errormsg());
}