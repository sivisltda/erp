<?php

require_once('../../../Connections/configini.php');
require_once('../../../util/util.php');

$aColumns = array('id', 'data_cad', 'descricao', 'quantidade', 'valor', 'fornecedor');
$iTotal = 0;
$iFilteredTotal = 0;
$sOrder = " ORDER BY " . $aColumns[0] . " asc ";
$sOrder2 = " ORDER BY " . $aColumns[0] . " asc ";
$whereX = " WHERE id_tele = " . $_GET['id'];
$sLimit = 20;

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

$sQuery = "SELECT COUNT(*) total from sf_telemarketing_previsao" . $whereX;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
    $iFilteredTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,* from sf_telemarketing_previsao " . $whereX .") as a WHERE a.row > " . 
$sLimit . " and a.row <= " . ($sLimit + $sQtd) . " " . $sOrder2;
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<center>" . escreverData($aRow[$aColumns[1]]) . "</center>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[2]]) . "'>" . utf8_encode($aRow[$aColumns[2]]) . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[5]]) . "'>" . utf8_encode($aRow[$aColumns[5]]) . "</div>";
    $row[] = escreverNumero($aRow[$aColumns[3]]);
    $row[] = escreverNumero($aRow[$aColumns[4]]);
    $row[] = escreverNumero(($aRow[$aColumns[3]] * $aRow[$aColumns[4]]));
    $row[] = "<center><input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\" onclick=\"RemoverPrev('" . $aRow[$aColumns[0]] . "')\" /></center>";
    $output['aaData'][] = $row;
}

echo json_encode($output);
odbc_close($con);