<?php

require_once(__DIR__ . '/../../Connections/configini.php');
require_once(__DIR__ . '/../../util/util.php');

$DateBegin = getData("B");
$DateEnd = getData("E");
$comando = "";
$i = 0;

if ($_GET['dti'] != "" && $_GET['dtf'] != "") {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
    $comando .= " and ((" . valoresData2($DateBegin) . " between dt_inicio_mens and dt_fim_mens) or (" . valoresData2($DateEnd) . " between dt_inicio_mens and dt_fim_mens))";
}

if (is_numeric($_GET['prd']) && $_GET['ts'] == 1) {
    $comando .= " and id_prod_plano = " . $_GET['prd'];
}

$queryx = "from sf_vendas_planos_mensalidade
inner join sf_vendas_planos on id_plano_mens = id_plano
inner join sf_fornecedores_despesas on favorecido = id_fornecedores_despesas
where tipo = 'C' and inativo = 0 and dt_cancelamento is null " . $comando;

if ($_GET['ts'] == 0) {
    $query = "set dateformat dmy;select id_prod_plano, descricao, total, preco_venda from (select count(distinct id_plano) total, id_prod_plano " . 
    $queryx . "group by id_prod_plano) as x inner join sf_produtos on x.id_prod_plano = conta_produto order by preco_venda";
} else if ($_GET['ts'] == 1) {
    $query .= "set dateformat dmy;select distinct id_fornecedores_despesas,razao_social,data_nascimento,max(dt_inicio_mens) dt_inicio,max(dt_fim_mens) dt_fim, planos_status status, id_plano,
    (select top 1 case when nome = '' then login_user else nome end login_user from sf_usuarios c where c.inativo = 0 and c.id_usuario = sf_fornecedores_despesas.id_user_resp) estado,            
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'telefone',    
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'celular',
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'email'" .
    $queryx . " group by id_fornecedores_despesas,razao_social,data_nascimento, planos_status, id_plano, sf_fornecedores_despesas.id_user_resp order by 2";
}
//echo $query; exit;
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => 0,
    "iTotalDisplayRecords" => 0,
    "aaData" => array()
);

$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $row = array();    
    if ($_GET['ts'] == 0) {
        $row[] = "<a href=\"javascript:void(0)\" onclick=\"Analitico(" . $RFP['id_prod_plano'] . ", '" . utf8_encode($RFP['descricao']) . "');\">" . utf8_encode($RFP['descricao']) . "</a>";
        $row[] = utf8_encode($RFP['total']);
        $row[] = escreverNumero($RFP['preco_venda'], 1);
        $row[] = "";
        $row[] = "";
        $row[] = "";
    } else if ($_GET['ts'] == 1) {
        $row[] = utf8_encode($RFP['id_fornecedores_despesas']);
        $row[] = "<a href=\"javascript:void(0)\" onclick=\"AbrirBox(2, " . $RFP['id_fornecedores_despesas'] . ")\">" . utf8_encode($RFP['razao_social']) . "</a>";
        $row[] = escreverData($RFP['dt_inicio']) . " até " . escreverData($RFP['dt_fim']);
        $row[] = utf8_encode($RFP['status']);
        $row[] = formatNameCompact(utf8_encode($RFP['estado']));
        $row[] = utf8_encode($RFP['telefone']);        
        $row[] = ($RFP['celular'] != "" ? "<a href=\"javascript:void(0)\" onclick=\"abrirTelaSMS('" . utf8_encode($RFP['celular']) . "',true," . $RFP['id_fornecedores_despesas'] . ")\">" . utf8_encode($RFP['celular']) . "</a>" : "");                
        $row[] = ($RFP['email'] != "" ? "<a href=\"javascript:void(0)\" onclick=\"abrirTelaEmail('" . utf8_encode($RFP['email']) . "'," . $RFP['id_fornecedores_despesas'] . ")\">" . utf8_encode($RFP['email']) . "</a>" : "");
        $row[] = utf8_encode($RFP['cnpj']);
        $row[] = escreverNumero($RFP['Valor'], 1);
    }
    $output['aaData'][] = $row;    
    $i++;
}

$output['iTotalRecords'] = $i;
$output['iTotalDisplayRecords'] = $i;

if (!isset($_GET['isPdf']) && !isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);