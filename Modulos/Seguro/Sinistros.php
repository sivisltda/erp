<?php
include '../../Connections/configini.php';
if (is_numeric($_GET['Del'])) {
    odbc_exec($con, "insert into sf_logs values('sf_telemarketing'," . $_GET['Del'] . ", '" . $_SESSION["login_usuario"] . "','R', 'EXCLUSAO EVENTO - " . $_GET['Del'] . "', getdate(), (select max(pessoa) from sf_telemarketing where id_tele = " . $_GET['Del'] . "))");      
    odbc_exec($con, "DELETE FROM sf_telemarketing_item WHERE id_telemarketing = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_mensagens_telemarketing WHERE id_tele = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_telemarketing WHERE id_tele = " . $_GET['Del']);             
    echo "<script>window.top.location.href = 'Sinistros.php'; </script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>        
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>        
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Proteção<small>Eventos</small></h1>
                    </div>                 
                    <div class="topcont" style="clear: both;display:flex; margin-bottom: 10px;">
                        <div class="tophead" style="width:calc(11% - 1px); border:0;">
                            <a href="javascript:void(0)" onclick="selectStatus('0')">
                            <div class="topname">Aguardando</div>
                            <div class="topresp">
                                <div id="lbl_aguardando" class="topnumb">0</div>
                                <div style="clear:both"></div>
                            </div></a>
                        </div>
                        <div class="tophead" style="width:calc(11% - 1px)">
                            <a href="javascript:void(0)" onclick="selectStatus('5')">
                            <div class="topname">Sindicância</div>
                            <div class="topresp">
                                <div id="lbl_sindicancia" class="topnumb">0</div>
                                <div style="clear:both"></div>
                            </div></a>
                        </div>
                        <div class="tophead" style="width:calc(11% - 1px)">
                            <a href="javascript:void(0)" onclick="selectStatus('1')">
                            <div class="topname">Em Aberto</div>
                            <div class="topresp">
                                <div id="lbl_em_aberto" class="topnumb">0</div>
                                <div style="clear:both"></div>
                            </div></a>
                        </div>
                        <div class="tophead" style="width:calc(11% - 1px)">
                            <a href="javascript:void(0)" onclick="selectStatus('2')">
                            <div class="topname">Em Reparo</div>
                            <div class="topresp">
                                <div id="lbl_em_reparo" class="topnumb">0</div>
                                <div style="clear:both"></div>
                            </div></a>
                        </div>
                        <div class="tophead" style="width:calc(11% - 1px)">
                            <a href="javascript:void(0)" onclick="selectStatus('6')">
                            <div class="topname">Cancelado</div>
                            <div class="topresp">
                                <div id="lbl_cancelado" class="topnumb">0</div>
                                <div style="clear:both"></div>
                            </div></a>
                        </div>
                        <div class="tophead" style="width:calc(11% - 1px)">
                            <a href="javascript:void(0)" onclick="selectStatus('7')">
                            <div class="topname">Sub-Rogação</div>
                            <div class="topresp">
                                <div id="lbl_subrogacao" class="topnumb">0</div>
                                <div style="clear:both"></div>
                            </div></a>
                        </div>
                        <div class="tophead" style="width:calc(11% - 1px)">
                            <a href="javascript:void(0)" onclick="selectStatus('8')">
                            <div class="topname">Indeferido</div>
                            <div class="topresp">
                                <div id="lbl_indeferido" class="topnumb">0</div>
                                <div style="clear:both"></div>
                            </div></a>
                        </div>
                        <div class="tophead" style="width:calc(11% - 1px)">
                            <a href="javascript:void(0)" onclick="selectStatus('3')">
                            <div class="topname">Finalizado</div>
                            <div class="topresp">
                                <div id="lbl_finalizado" class="topnumb">0</div>
                                <div style="clear:both"></div>
                            </div></a>
                        </div>
                        <div class="tophead" style="width:calc(12% - 1px)">
                            <a href="javascript:void(0)" onclick="selectStatus('null')">
                            <div class="topname">Total</div>
                            <div class="topresp">
                                <div id="lbl_total" class="topnumb">0</div>
                                <div style="clear:both"></div>
                            </div></a>
                        </div>
                        <div style="clear:both"></div>
                    </div>                    
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="float:left;width: 10%;margin-top: 15px;">
                                    <button class="button button-green btn-primary" type="button" onClick="AbrirBoxSinistro(0,0)"><span class="ico-file-4 icon-white"></span></button>                                    
                                    <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir('I')" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                    <button id="btnExcel" class="button button-blue btn-primary" type="button" onclick="imprimir('E')" title="Exportar Excel"><span class="ico-download-3 icon-white"></span></button>
                                </div>
                                <div style="width: 90%; display: flex; justify-content: flex-end;">
                                    <div style="float:left; margin-left: 1%;">
                                        <span style="display:block;">Data:</span>
                                        <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin" name="txt_dt_begin" value="" placeholder="Data inicial" maxlength="10" autocomplete="off">
                                        <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end" name="txt_dt_end" value="" placeholder="Data Final" maxlength="10" autocomplete="off">
                                    </div>                                    
                                    <div style="margin-left: 1%;">
                                        <span>Status do Evento:</span>
                                        <select name="txtStatusAp[]" id="txtStatusAp" multiple="multiple" class="select" style="width:100%" class="input-medium">                 
                                            <option value="0" selected>Aguardando</option>                                                
                                            <option value="1" <?php echo (isset($_GET["id"]) ? "" : "selected")?>>Em Aberto</option>                                               
                                            <option value="2" <?php echo (isset($_GET["id"]) ? "" : "selected")?>>Em Reparo</option>                                                                                               
                                            <option value="3">Finalizado</option>                                                                                               
                                            <option value="4">Reprovado</option>                                                                                               
                                            <option value="5" <?php echo (isset($_GET["id"]) ? "" : "selected")?>>Sindicância</option>                                                                                               
                                            <option value="6">Cancelados</option> 
                                            <option value="7">Sub-Rogação</option> 
                                            <option value="8">Indeferido</option> 
                                        </select>                                 
                                    </div>                                                                        
                                    <div style="margin-left: 1%;">
                                        <span>Oficinas:</span>
                                        <select name="txtOficina[]" id="txtOficina" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                            <?php $cur = odbc_exec($con, "select id_turma, descricao from sf_turmas where inativo = 0 order by descricao") or die(odbc_errormsg());
                                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id_turma'] ?>"><?php echo utf8_encode($RFP['descricao']);?></option>
                                            <?php } ?>                                            
                                        </select>
                                    </div>                                                                        
                                    <div style="margin-left: 1%;">
                                        <span>Ação do Evento:</span>
                                        <select name="txtAcaoEvento[]" id="txtAcaoEvento" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                            <?php $cur = odbc_exec($con, "select id_especificacao, nome_especificacao from sf_telemarketing_especificacao order by nome_especificacao") or die(odbc_errormsg());
                                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id_especificacao'] ?>"><?php echo utf8_encode($RFP['nome_especificacao']);?></option>
                                            <?php } ?>                                            
                                        </select>
                                    </div>                                                                        
                                    <div style="float:left; margin-left: 1%;">
                                        <div width="100%">Busca:</div>
                                        <input id="txtBusca" maxlength="100" type="text" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title=""/>
                                    </div>
                                    <div style="margin-top: 15px;">
                                        <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Eventos</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tbListaSinistro">
                            <thead>
                                <tr>                                    
                                    <th width="2%"></th>
                                    <th width="6%">N° Evento</th>
                                    <th width="5%">Placa:</th>                                               
                                    <th width="7%">Modelo:</th>                                                                                    
                                    <th width="7%">Dt.Evento:</th>
                                    <th width="7%">Prazo:</th>
                                    <th width="10%">Período Cont.:</th>
                                    <th width="11%">Proprietário:</th>
                                    <th width="11%">Cliente:</th>
                                    <th width="8%">Ação do Evento:</th>                                    
                                    <th width="9%">Oficina Atual:</th>
                                    <th width="7%">Previsto:</th>
                                    <th width="6%">Status:</th>
                                    <th width="4%"><center>Ação:</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>                        
                        <div style="clear:both"></div>
                    </div>                        
                    <table class="table" cellpadding="0" cellspacing="0" width="100%">                                            
                        <tr>
                            <td style="text-align: right">
                                <div>Número de Compras :<strong><span id="lbl_num_compras">0</span></strong></div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <div>Total em Compras :<strong><span id="lbl_tot_compras"><?php echo escreverNumero(0,1); ?></span></strong></div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <div>Total Previsto :<strong><span id="lbl_tot_previsto"><?php echo escreverNumero(0,1); ?></span></strong></div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <div>Ticket Médio :<strong><span id="lbl_tic_compras"><?php echo escreverNumero(0,1); ?></span></strong></div>            
                            </td>
                        </tr>
                    </table>                        
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div> 
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>        
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>                     
        <script type="text/javascript" src="js/Sinistro.js"></script>                     
        <script type="text/javascript">
            
            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }
                    
            function AbrirBoxCli(tipo, id) {
                let url = (tipo === "C" ? "./../Academia/ClientesForm.php?id=" : (tipo === "O" ? "./../Seguro/TerceirosForm.php?id=" : "./../CRM/CRMForm.php?id=")) + id;
                window.open(url,'_blank');
            }

            function imprimir(tipo) {
                var pRel = "&NomeArq=" + "Eventos" +
                "&lbl=" + "N° Evento|Placa|Modelo|Dt.Evento|Prazo|Período para Cont.|Proprietário|Cliente|Ação do Evento|Oficina Atual|Total Previsto|Status" + 
                (tipo === "E" ? "|Histórico|Tipo Veiculo|Total Compras" : "") +
                "&pOri=L"+ //parametro de paisagem
                "&siz=" + "55|55|80|70|70|90|135|135|90|90|70|70" + 
                (tipo === "E" ? "|1000|100|100" : "") +                
                "&pdf=" + "0|12" + // Colunas do server processing que não irão aparecer no pdf 9
                "&filter=" + "Eventos " + //Label que irá aparecer os parametros de filtro
                "&PathArqInclude=../Modulos/Seguro/" + finalFindSinistro(1).replace("?", "&"); // server processing            
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tipo + "&PathArq=GenericModelPDF.php", '_blank');
            }            
            
            $("#btnfind").click(function () {
                tbListaSinistro.fnReloadAjax(finalFindSinistro(0));
            });
            
            function selectStatus(status) {
                $("#txtStatusAp").val(status);
                $("#btnfind").click();
            }
                                    
        </script>        
        <?php odbc_close($con); ?>
    </body>
</html>