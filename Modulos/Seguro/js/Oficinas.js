$.getJSON("../Academia/form/ClientesFormServer.php", {isAjax:"S", listProfessor: "S"}, function (j) {
    var options = "<option value=\"\">Selecione</option>";
    if(j !== null){
        if(j.length > 0){
            for (var i = 0; i < j.length; i++) {
                options = options + "<option value=\""+ j[i].id_fornecedores_despesas+"\">" + j[i].razao_social + "</option>";
            }
        }
        $("#txtProfessor").html(options);
    }
});

$.getJSON("../Academia/form/ClientesFormServer.php", {isAjax:"S", listAmbiente: "S"}, function (j) {
    var options = "<option value=\"\">Selecione</option>";
    if(j !== null){
        if(j.length > 0){
            for (var i = 0; i < j.length; i++) {
                options = options + "<option value=\""+ j[i].id_ambiente+"\">" + j[i].nome_ambiente + "</option>";
            }
        }
        $("#txtAmbiente").html(options);
    }
});

$('#tblTurmas').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bFilter": false,
    "aoColumns": [{"bSortable": true },
        { "bSortable": true },
        { "bSortable": true },
        { "bSortable": true },
        { "bSortable": true },
        { "bSortable": true },
        { "bSortable": true },
        { "bSortable": true },
        { "bSortable": false }] ,
    "sAjaxSource": "ajax/Oficinas_server_processing.php" + finalFind(0),
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

$("#btnfind").click(function(){
    var tblTurmas = $('#tblTurmas').dataTable();
    tblTurmas.fnReloadAjax("ajax/Oficinas_server_processing.php" + finalFind(0));
});

function listaFiltro(){
    var filtros = "";
    if($("#txtProfessor").val() >= "0"){
        filtros += "   Especialidade: " + $("#txtProfessor option:selected").text();
    }
    if($("#txtAmbiente").val() >= "0"){
        filtros += "   Bairro: " + $("#txtAmbiente option:selected").text();
    }
    return filtros;
}

function imprimir(){
    var pRel = "&NomeArq=" + "Oficinas" +
        "&lbl=Nome da Oficina|Max|Vagas|Manutenção|Especialidade|Bairro|Status" +
        "&siz=180|70|70|80|120|100|80" +
        "&pdf=" + // Colunas do server processing que não irão aparecer no pdf
        "&filter=" + listaFiltro() + //Label que irá aparecer os parametros de filtro
        "&PathArqInclude=" + "../Modulos/Seguro/ajax/Oficinas_server_processing.php" + finalFind(1).replace("?","&");
    window.open("../../util/ImpressaoPdf.php?id=" + pRel+ "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
}

function finalFind(imp) {
    var retPrint = "";
    retPrint = '?imp=0';
    if(imp === 1){ retPrint = '?imp=1'; }
    if($("#txtProfessor").val()!== ""){ retPrint = retPrint + '&professor='+$("#txtProfessor").val(); }
    if($("#txtAmbiente").val()!== ""){ retPrint = retPrint + '&ambiente='+$("#txtAmbiente").val(); }
    return retPrint.replace(/\//g, "_");
}

function RemoverItem(idTurma){
    bootbox.confirm('Confirma a exclusão da Oficina?', function (result) {
        if (result === true) {
            $("#loader").show();
            $.post("form/OficinasFormServer.php", "isAjax=S&removerTurma="+idTurma).done(function (data) {
                if(data.trim() === "YES"){
                    var tblTurmas = $('#tblTurmas').dataTable();
                    tblTurmas.fnReloadAjax("ajax/Oficinas_server_processing.php"+finalFind(0));
                } else {
                    bootbox.alert("Erro ao excluir. Remova todos os veículos cadastrados na oficina e da lista de espera!");
                }
                $("#loader").hide();
            });
        }
    });
}

function AbrirBox(id) {
    window.location = './../Seguro/OficinasForm.php' + (id > 0 ? '?id=' + id : '');
}