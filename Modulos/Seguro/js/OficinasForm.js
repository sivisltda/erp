$(document).ready(function () {
    $("#loader").show();
    totalFimCarregamento = 0;    
    $("#txtDtInicio, #txtDtCriacao").mask(lang["dateMask"]);    

    $.getJSON("../Academia/form/ClientesFormServer.php", {isAjax:"S", listProfessor: "S"}, function (j) {
        var options = "<option value=\"\">Selecione</option>";
        if(j !== null ){
            if(j.length > 0){
                for (var i = 0; i < j.length; i++) {
                    options = options + "<option value=\""+ j[i].id_fornecedores_despesas+"\">" + j[i].razao_social + "</option>";
                }
            }
            $("#txtProfessor").html(options);
        }
    }).done(function(){
        trataCarregamentoTela();
    });

    $.getJSON("../Academia/form/AmbientesFormServer.php", {isAjax:"S", listAmbientes: "S"}, function (j) {
        var options = "<option value=\"\">Selecione</option>";
        if(j !== null ){
            if(j.length > 0){
                for (var i = 0; i < j.length; i++) {
                    options = options + "<option value=\""+ j[i].id_ambiente+"\">" + j[i].nome_ambiente + "</option>";
                }
            }
            $("#txtAmbiente").html(options);
        }
    }).done(function(){
        trataCarregamentoTela();
    });

    $.getJSON("../Seguro/form/OficinasFormServer.php", {isAjax:"S", listPlanosTurma: "S", idTurma: $("#txtId").val()}, function (j) {
        var options = "";
        if(j !== null ){
            if(j.length > 0){
                for (var i = 0; i < j.length; i++) {
                    var selecionado = j[i].isCheck === 'S' ? "SELECTED" : "";
                    options = options + "<option value=\""+ j[i].conta_produto+"\" "+selecionado+" >" + j[i].descricao + "</option>";
                }
            }
            $("#mscPlanos").html(options);
        }
    }).done(function() {
        $("#mscPlanos").multiSelect({
            selectableHeader: "<div class=\"multipleselect-header\">Planos (Todos)</div>",
            selectionHeader: "<div class=\"multipleselect-header\">Planos (Selecionados)</div>"
        });
    });

    function trataCarregamentoTela(){
        totalFimCarregamento = totalFimCarregamento + 1;
        if(totalFimCarregamento === 2){
            if($("#txtId").val() !== ""){
                $.getJSON("../Seguro/form/OficinasFormServer.php", {isAjax:"S", getTurma: $("#txtId").val()}, function (j) {
                    if(j !== null ){
                        $("#txtDescricao").val(j[0].descricao);
                        $("#txtDtInicio").val(j[0].dt_inicio);
                        $("#txtDtCriacao").val(j[0].dt_cadastro);
                        $('#ckbInativo').parents('span').removeClass("checked").end().removeAttr("checked");
                        if (j[0].inativo === "1") { $('#ckbInativo').click(); }
                        $("#txtProfessor").select2("val", j[0].professor);
                        $("#txtCapacidade").val(j[0].n_alunos);
                        $("#txtAmbiente").select2("val", j[0].ambiente);
                    }
                    listAlunosTurma();
                    $("#loader").hide();
                    TrataEnableCampos(false);
                });
            }else{
                $("#loader").hide();
                TrataEnableCampos(true);
            }
        }
    }
});

function listAlunosTurma(){
    $('#tblAlunosTurma').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "form/OficinasFormServer.php?isAjax=S&listAlunosTurma=S&txtId="+$("#txtId").val(),
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        "scrollY": "120px",
        "scrollCollapse": true,
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers",
        "fnInitComplete": function (oSettings, json) {
            $("#qtdAluno").text($('#tblAlunosTurma').dataTable().fnGetData().length);
        }
    });
}

function TrataEnableCampos(enable) {
    if (!enable) {
        $("#divDadosTurma :input").attr("disabled", true);
        $('#txtProfessor, #txtAmbiente').select2('disable');
        $('#ms-mscPlanos').find('li').addClass("disabled");
        $("#btnNovo, #btnAlterar, #btnExcluir").show();
        $("#btnSave, #btnCancelar").hide();
        $('.btnExcluir').hide();
    } else {
        $("#divDadosTurma :input").attr("disabled", false);
        $('#txtProfessor, #txtAmbiente').select2('enable');
        $('#ms-mscPlanos').find('li').removeClass("disabled");
        $("#btnNovo, #btnAlterar, #btnExcluir").hide();
        $("#btnSave, #btnCancelar").show();
        $('.btnExcluir').show();
    }
}

$("#txtCapacidade").change(function(e){
    if($("#txtCapacidade").val() < $("#qtdAluno").text()){
        bootbox.alert("Valor de capacidade menor que o número de matriculados!");
    }
});

function validaForm(){
    if($("#txtDescricao").val() === "" || $("#txtDtInicio").val() === "" || $("#txtCapacidade").val() === ""){
        bootbox.alert("Os campos descriçao, data de inicio, Capacidade e Horario são obrigatórios!");
        return false;
    }
    return true;
}

$('#btnSave').click(function (e) {
    if (validaForm()) {
        $.post("form/OficinasFormServer.php", "isAjax=S&btnSave=S&txtId=" + $("#txtId").val() + "&" + $(".tab-content :input").serialize()).done(function (data) {
            if(!isNaN(data)){
                window.location = "OficinasForm.php?id=" + data;
            }else{
                bootbox.alert(data);
            }
        });
    }
});

$('#btnAlterar').click(function (e) {
    TrataEnableCampos(true);
});

$('#btnCancelar').click(function (e) {
    if($("#txtId").val()!== ""){
        window.location = "OficinasForm.php?id=" + $("#txtId").val();
    }else{
        window.location = "OficinasForm.php";
    }
});

$('#btnNovo').click(function (e) {
    window.location = "OficinasForm.php";
});

$('#btnExcluir').click(function (e) {
    bootbox.confirm('Confirma a exclusão da turma?', function (result) {
        if (result === true) {
            $("#loader").show();
            $.post("form/OficinasFormServer.php", "isAjax=S&removerTurma="+$("#txtId").val()).done(function (data) {
                if(data.trim() === "YES"){
                    window.location = "OficinasForm.php";
                } else {
                    bootbox.alert("Erro ao excluir. Remova todos os alunos cadastrados na turma e da lista de espera!");
                }
                $("#loader").hide();
            });
        }
    });
});

function AtualizaListaTurma(){
    var tblAlunosFila = $('#tblAlunosTurma').dataTable();
    tblAlunosFila.fnReloadAjax("form/OficinasFormServer.php?isAjax=S&listAlunosTurma=S&txtId="+$("#txtId").val(), function(){
        $("#qtdAluno").text($('#tblAlunosTurma').dataTable().fnGetData().length);
    }, null);
}

function MatricularAluno(idAluno,nmAluno){
    abrirTelaBox("VeiculosOficinasForm.php?idAluno="+idAluno+"&nmAluno="+nmAluno, 531, 770);
}

function AbrirCliente(id){
    window.location = './../Academia/ClientesForm.php?id='+id;
}

function FecharBoxCliente (){
    $("#newbox").remove();
    AtualizaListaTurma();
}

