
function finalFindVistorias(imp) {    
    var retPrint = (imp === 1 ? '&imp=1' : '?imp=0');
    if ($("#txt_dt_begin").length > 0 && $("#txt_dt_begin").val() !== "" && !$.isNumeric($("#txtId", window.parent.document).val())) {
        retPrint += "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "/");
    }
    if ($("#txt_dt_end").length > 0 && $("#txt_dt_end").val() !== "" && !$.isNumeric($("#txtId", window.parent.document).val())) {
        retPrint += "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "/");
    }    
    if ($("#txtBusca").length > 0 && $("#txtBusca").val() !== "") {
        retPrint += '&Search=' + $("#txtBusca").val();
    }
    if ($("#txtStatusAp").length > 0 && $("#txtStatusAp").val() !== "null") {
        retPrint += '&statusap=' + $("#txtStatusAp").val();
    }
    if ($("#txtVistorias").length > 0 && $("#txtVistorias").val() !== "null") {
        retPrint += '&vistorias=' + $("#txtVistorias").val();
    }
    if ($("#txtAssistencia").length > 0 && $("#txtAssistencia").val() !== "null") {
        retPrint += '&assistencia=' + $("#txtAssistencia").val();
    }
    if ($("#txtStatus").length > 0 && $("#txtStatus").val() !== "null") {
        retPrint += "&status=" + $("#txtStatus").val();
    }
    if ($("#txtContaMov").length > 0 && $("#txtContaMov").val() !== "null") {
        retPrint += "&grupo_pessoa=" + $("#txtContaMov").val();
    }
    if ($("#txtTipoCli").length > 0 && $("#txtTipoCli").val() !== "null") {
        retPrint += "&tpCli=" + $("#txtTipoCli").val();
    }
    if ($.isNumeric($("#txtId", window.parent.document).val())) {
        retPrint += '&idCli=' + $("#txtId", window.parent.document).val();
    }
    return "./../Seguro/Vistorias_server_processing.php" + retPrint.replace(/\//g, "_");
}

function AbrirBoxVistorias(id) {
    abrirTelaBox("./../Seguro/FormVistorias.php" + (id > 0 ? "?id=" + id : ""), 580, 1000);
}

function FecharBoxVistorias() {
    $("#newbox").remove();
    tbListaVistorias.fnReloadAjax(finalFindVistorias(0));
    if ($.isFunction(window.refreshStatus)) {
        refreshStatus();
    } 
    if ($.fn.dataTable.fnIsDataTable("#tbListaVeiculo")) {
        tbListaVeiculo.fnReloadAjax(finalFindVeiculo(0));
    }
}

var tbListaVistorias = $('#tbListaVistorias').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bFilter": false,
    "ordering": false,
    "sAjaxSource": finalFindVistorias(0),
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});
