$("#txtPlaca").mask("AAA-9A99");
$("#txtCep").mask("99999-999");
$("#txtData, #txtDataProximo, #txtDataNota").mask(lang["dateMask"]);
$("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
$("#txtHora, #txtHoraProximo").mask("99:99");

ativarGarantia();
ListMensagens();

$('#txtPlaca').blur(function () {
    $.getJSON('form/FormVistoriasServer.php?isAjax=S', {getPlaca: $("#txtPlaca").val()}, function (json) {
        if (json !== null && json.length > 0) {
            $("#txtIdVeiculo").val(json[0].id);
            $("#txtIdPessoa").val(json[0].id_pessoa);            
            $("#txtNome").val(json[0].razao_social);
            $("#txtCPF").val(json[0].cnpj);
            $("#txtTipo").val(json[0].tipo_veiculo_descricao);            
            $("#txtMontadora").val(json[0].marca);
            $("#txtModelo").val(json[0].modelo);
            $("#txtAnoMod").val(json[0].ano_modelo);
            $("#txtAnoFab").val(json[0].ano_fab);            
            $("#txtRenavam").val(json[0].renavam);
            $("#txtChassi").val(json[0].chassi);
            $("#txtCor").val(json[0].cor);
            atualizaStatusCli(json[0].fornecedores_status);
        }        
    });    
});

function atualizaStatusCli(data) {
    $("#statusCli .button").attr("class", " ");
    $("#statusCli div").first().addClass("button");
    $(".name center").html(data.toUpperCase());
    if (data.trim().toUpperCase() === "SUSPENSO") {
        $("#statusCli div").first().addClass("label-Suspenso");
    } else if (data.trim().toUpperCase() === "CANCELADO") {
        $("#statusCli div").first().addClass("label-Cancelado");
    } else if (data.trim().toUpperCase() === "INATIVO") {
        $("#statusCli div").first().addClass("label-Inativo");
    } else if (data.trim().toUpperCase() === "ATIVOEMABERTO") {
        $("#statusCli div").first().addClass("label-AtivoEmAberto");    
        $(".name center").html("ATIVO EM ABERTO");              
    } else if (data.trim().toUpperCase() === "ATIVOABONO") {
        $("#statusCli div").first().addClass("label-AtivoAbono");    
        $(".name center").html("ATIVO ABONO");              
    } else if (data.trim().toUpperCase().substring(0, 5) === "ATIVO") {
        $("#statusCli div").first().addClass("label-Ativo");
    } else if (data.trim().toUpperCase() === "FERIAS") {
        $("#statusCli div").first().addClass("label-Ferias");
        $(".name center").html("EM FÉRIAS");
    } else if (data.trim().toUpperCase() === "DEPENDENTE") {
        $("#statusCli div").first().addClass("label-Dependente");
        $(".name center").html("DEPENDENTE");            
    } else if (data.trim().toUpperCase() === "DESLIGADO") {
        $("#statusCli div").first().addClass("label-Desligado");
        $(".name center").html("DESLIGADO");            
    } else if (data.trim().toUpperCase() === "DESLIGADOEMABERTO") {
        $("#statusCli div").first().addClass("label-DesligadoEmAberto");
        $(".name center").html("DESLIGADO EM ABERTO");            
    }
}

$('#txtResponsavel').change(function () {
    if ($("#txtResponsavel").val() === "0") {
        $("#txtNomeAssociado").parent().hide();
    } else {
        $("#txtNomeAssociado").parent().show();
    }
});

$("#txtNomeAssociado").autocomplete({
    source: function (request, response) {
        $.ajax({
            url: "../../Modulos/CRM/ajax.php",
            dataType: "json",
            data: {q: request.term, r: "S", t: "C"},
            success: function (data) {
                response(data);
            }
        });
    }, minLength: 3,
    select: function (event, ui) {
        $("#txtIdAssociado").val(ui.item.id);
        $("#txtNomeAssociado").val(ui.item.value);
    }
});

$("#txtCep").change(function () {
    if ($("#txtCep").val() !== "") {
        var cidade = '';
        $("#loader").show();
        $.get("https://viacep.com.br/ws/" + $(this).val() + "/json/",
                function (data) {
                    var str = data;
                    $("#txtEndereco, #txtBairro").attr("readonly", true);
                    $('#txtCidade, #txtEstado').select2('disable');
                    if (str.logradouro === undefined) {
                        $("#txtCep").val("");
                        bootbox.alert("CEP inválido!");
                    } else if (str.logradouro !== "") {
                        var estado = $("#txtEstado option[uf='" + str.uf.toUpperCase() + "']").val();
                        cidade = decodeURIComponent(str.localidade);
                        $("#txtEndereco").val(decodeURIComponent(str.logradouro));
                        $("#txtBairro").val(decodeURIComponent(str.bairro));
                        $("#txtCidade").val(decodeURIComponent(str.localidade));
                        $("#txtEstado").select2("val", estado);
                        $("#txtNumero").focus();
                        carregaCidade(estado, cidade);
                    } else if (str.logradouro === "") {
                        $("#txtEndereco, #txtBairro, #txtNumero").val("");
                        $("#txtCidade, #txtEstado").select2("val", "");
                        $("#txtEndereco, #txtBairro").attr("readonly", false);
                        $('#txtCidade, #txtEstado').select2('enable');
                        $("#txtEndereco").focus();
                    }
                }).always(function () {
            $("#loader").hide();
        });
    } else {
        $("#txtEndereco, #txtBairro").val("");
        $("#txtCidade, #txtEstado").select2("val", "");
        $("#txtEndereco, #txtBairro").attr("readonly", false);
        $('#txtCidade, #txtEstado').select2('enable');
    }
});

$('#txtEstado').change(function () {
    if ($("#txtEstado").val() !== "") {
        carregaCidade($(this).val(), "");
        $("#txtCidade").select2("val", "");
    } else {
        $('#txtCidade').html('<option value="">Selecione a cidade</option>');
    }
});

function carregaCidade(estado, cidade) {
    $("#s2id_txtCidade").hide();
    $('#carregando').show();
    var idCidade = ($.isNumeric(cidade) ? cidade : "");
    $.getJSON("../../Modulos/Comercial/locais.ajax.php?search=", {txtEstado: estado, ajax: "true"}, function (j) {
        var options = "<option value=\"\">Selecione a Cidade</option>";
        if (j.length > 0) {
            for (var i = 0; i < j.length; i++) {
                if (cidade === j[i].cidade_nome) {
                    idCidade = j[i].cidade_codigo;
                }
                options = options + "<option value=\"" + j[i].cidade_codigo + "\">" + j[i].cidade_nome + "</option>";
            }
            $("#txtCidade").html(options);
        }
    }).always(function () {
        $("#txtCidade").select2("val", idCidade);
        $("#s2id_txtCidade").show();
        $('#carregando').hide();
    });
}

$('input[type=radio][name=ckbGarantia]').change(function() {
    ativarGarantia();
});
     
function ativarGarantia() {
    $("#txtDataNota, #txtNotaFiscal").attr("disabled", 
    (!$('input[name="ckbGarantia"]').attr("disabled") && ($('input[name="ckbGarantia"]:checked').val() === '0') ? false : true));
}

function validaForm() {
    if ($("#txtIdVeiculo").val() === "") {
        bootbox.alert("Preencha um veículo corretamente!");
        return false;
    }
    if ($("#txtEspecificacao").val() === "null") {
        bootbox.alert("Preencha uma Ação do Evento!");
        return false;
    }
    if ($("#txtPara").val() === "null") {
        bootbox.alert("Preencha um Responsável pelo Evento!");
        return false;
    }   
    if ($("#txtRelato").val() === "") {
        bootbox.alert("Preencha uma Descrição da Ocorrência!");
        return false;
    }        
    if ($('input[name="ckbGarantia"]:checked').val() === "0" && $("#txtDataNota").val() === "") {
        bootbox.alert("Preencha a data do Boletim de Ocorrência!");
        return false;
    }
    if ($('input[name="ckbGarantia"]:checked').val() === "0" && $("#txtNotaFiscal").val() === "") {
        bootbox.alert("Preencha o número do Boletim de Ocorrência!");
        return false;
    }
    return true;
}

$('#tbDocumentos').dataTable({
    "iDisplayLength": 12,
    "ordering": false,
    "bFilter": false,
    "bInfo": false,
    "sAjaxSource": "../../Modulos/CRM/ajax/Documentos_server_processing.php?id=" + $("#txtId").val() + "&type=Sinistros",
    "bLengthChange": false,
    "bPaginate": true,
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
    'sLengthMenu': "Visualização de _MENU_ registros",
    'sLoadingRecords': "Carregando...",
    'sProcessing': "Processando...",
    'sSearch': "Pesquisar:",
    'sZeroRecords': "Não foi encontrado nenhum resultado"}
});

function refreshDoc() {
    let tblCRM = $('#tbDocumentos').dataTable();
    tblCRM.fnReloadAjax("../../Modulos/CRM/ajax/Documentos_server_processing.php?id=" + $("#txtId").val() + "&type=Sinistros");
}

function AdicionarDoc(e) {
    if ($("#txtId").val().length > 0) {
        parent.$("#loader").show();
        var formData = new FormData();
        formData.append("btnSendFile", "S");
        formData.append("txtId", $("#txtId").val());
        formData.append("type", "Sinistros");
        formData.append("file", $("#file")[0].files[0]); 
        $.ajax({
            type: 'POST',
            url: "../../Modulos/CRM/form/CRMDocumentos.php",
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            parent.$("#loader").hide();
            if (data === "YES") {
                $('#file').val('');
                refreshDoc();
            } else {
                bootbox.alert(data);
            }
        });
    } else {
        bootbox.alert("É necessário salvar antes de realizar esta operação!");
    }
}

function RemoverDoc(filename) {
    var nomedoc = encodeURIComponent(filename.toString());
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/CRM/form/CRMDocumentos.php", "btnDelFile=S&txtId=" + $("#txtId").val() + 
            "&type=Sinistros&txtFileName=" + nomedoc).done(function (data) {
                if (data === "YES") {
                    refreshDoc();
                } else {
                    bootbox.alert(data);
                }
            });
        } else {
            return;
        }
    });
}

function ListMensagens() {
    var usuario = parent.$("#txtIdUsu").val();
    var conteudo = "";
    $("#divMensagens").html("");
    $.getJSON("../../Modulos/CRM/mensagens.crm.ajax.php", {
        codCrm: $("#txtId").val(),
        ajax: "true"
    }, function (j) {
        for (var i = 0; i < j.length; i++) {
            conteudo = "<div style=\"padding:5px; margin:5px 0\" class=\"item " + (usuario === j[i].id_usuario ? "blue" : "dblue out") + "\">";
            conteudo += "<div class=\"text\">" + j[i].status + "<b>" + j[i].data + " - " + j[i].login_user + " fala para " + j[i].login_userp + ": " +  
            (j[i].privado === "1" ? "(Privado)" : "(Público)") +"</b>"; 
            conteudo += "<div style=\"text-align:left; margin-left: 5px; float: right\" class=\"btn-group\">" +
                "<button style=\"height: 13px;background-color: gray;\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\">" +
                "<span style=\"margin-top: 1px;\" class=\"caret\"></span></button>" +
                "<ul style=\"left:-130px;\" class=\"dropdown-menu\">" +
                "    <li><a onclick=\"sendMensagens(" + j[i].id + ", 0)\">Enviar para Proprietário</a></li>";
            if ($("#txtResponsavel").val() === "1") {
                conteudo += "    <li><a onclick=\"sendMensagens(" + j[i].id + ", 1)\">Enviar para Associado</a></li>";
            }
            if (usuario === j[i].id_usuario) {
                conteudo += "    <li><a onclick=\"delMensagens(" + j[i].id + ")\">Excluir</a></li>";
            }
            conteudo += "</ul></div><br/>" + j[i].mensagem + "</div></div>";
            $("#divMensagens").prepend(conteudo);
        }
    });
}

function addMensagens() {
    if ($("#textoArea").val().length > 0) {
        $.post("../../Modulos/CRM/mensagens.crm.controler.ajax.php", {
            id: $("#txtId").val(),
            data: $("#txtDataHistorico").val(),
            hora: $("#txtHoraHistorico").val(),
            de: $("#txtDeHistorico").val(),
            para: $("#txtParaHistorico").val(),
            txtMsg: $("#textoArea").val(),
            txtTipo: $("#txtTipoHistorico").val(),
            acao: 0
        }).done(function (data) {
            $("#textoArea").val("");
            ListMensagens();
        });
    } else {
        bootbox.alert("Preencha as informações de mensagens corretamente!");
    }
}

function sendMensagens(numero, tipo) {
    bootbox.confirm("Confirma o envio desta mensagem?", function (result) {
        if (result === true) {
            $.post("../../Modulos/CRM/mensagens.crm.controler.ajax.php", 
                {id: numero, acao: 1, pessoa: (tipo === 0 ? $("#txtIdPessoa").val() : $("#txtIdAssociado").val())}).done(function (data) {
                ListMensagens();
            });            
        } else {
            return;
        }
    });
}

function delMensagens(numero) {
    bootbox.confirm("Confirma a exclusão desta mensagem?", function (result) {
        if (result === true) {
            $.post("../../Modulos/CRM/mensagens.crm.controler.ajax.php", {id: numero, acao: 2}).done(function (data) {
                ListMensagens();
            });            
        } else {
            return;
        }
    });
}

$("#btnfind").click(function () {
    refresh();
});

$('#tbCompras').dataTable({    
    "iDisplayLength": 10,
    "bProcessing": true,
    "bServerSide": true,
    "bFilter": false,
    "bInfo": false,    
    "ordering": false,
    "bLengthChange": false,    
    "sAjaxSource": finalFind(0),
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "fnDrawCallback": function () {
        refreshTotal();
    },
    "sPaginationType": "full_numbers"
});

function refresh() {
    var tblCompras = $('#tbCompras').dataTable();
    tblCompras.fnReloadAjax(finalFind(0));
}

function finalFind(imp) {
    return "../../Modulos/Estoque/Compras_server_processing.php?filial=" + 
    parent.$("#txtLojaSel").val() + "&imp=" + imp + "&id=2&tp=null&com=" + $("#txtId").val() + 
    "&dti=" + $("#txt_dt_begin").val() + '&dtf=' + $("#txt_dt_end").val();
}

function refreshTotal() {
    $('#lblTotParc2').html('Número de Compras :<strong>' + ($('#qtdparc').val() !== undefined ? $('#qtdparc').val() : "0") + '</strong>').show('slow');
    $('#lblTotParc3').html('Total em Compras :<strong>' + numberFormat($('#totparc').val(), 1) + '</strong>').show('slow');
    $('#lblTotParc4').html('Ticket Médio :<strong>' + numberFormat(($('#totparc').val() / $('#qtdparc').val()), 1) + '</strong>').show('slow');    
}

function SalvarVistoria() {
    if ($("#txtTipoVistoria").val() !== "null") {
        bootbox.confirm("Confirma o lançamento de uma nova vistoria?", function (result) {
            if (result === true) {
                $.post("../../Modulos/Seguro/form/FormVistoriasServer.php", {bntSave: "S", isAjax: "S",
                txtIdVeiculo: $("#txtIdVeiculo").val(), txtTipo: $("#txtTipoVistoria").val(), txtSinistro: $("#txtId").val()}
                ).done(function (data) {
                    refreshVistorias();
                });            
            } else {
                return;
            }
        });        
    } else {
        bootbox.alert("Selecione um modelo de vistoria!");
    }
}

function AbrirBoxVistorias(id) {
    $("#txtIdVistoria").val(id);
    $(".deleteImg.red").hide();
    $(".deleteImg.yellow").show();
    $(".img-car").attr("src", "./../../img/noimage.png");
    $.post("../../Modulos/Seguro/form/FormVistoriasServer.php", {txtId: id, isAjax: "S", Imagens: "S"}).done(function (data) {
        $.each(JSON.parse(data), function(index, value) {
            let imgEx = value.split('.');
            let imagem = (imgEx[1] === "3gp" || imgEx[1] === "mp4" ? "./../../img/novideo.png" : "./../../Pessoas/" + parent.$("#txtMnContrato").val() + "/Vistorias/" + id + "/" + value);
            if (index === 0) {
                $("#img").attr("src", imagem);    
            }
            index = parseInt(imgEx[0].split('_')[1]);
            $("#img" + index).attr("src", imagem);
            $("#img" + index).attr("data", "./../../Pessoas/" + parent.$("#txtMnContrato").val() + "/Vistorias/" + id + "/" + value);
            $("#add" + index).hide();
            $("#del" + index).show();            
        });
    });
}

function adicionarImg(id) {
    $("#txtImgSel").val(("00" + id).slice(-2));
    $("#fileVistoria").click();
}

function validaFormImg() {
    if ($("#txtImgSel").val() === "") {
        bootbox.alert("Selecione uma Imagem!");
        return false;
    }
    if ($("#fileVistoria").val() === "") {
        bootbox.alert("Selecione uma Arquivo!");
        return false;
    }
}

$('input[name=fileVistoria]').change(function(ev) {
    if ($(this).val() !== "") {
        $("#bntSendFile").click();    
    }
});

if ($.isNumeric($("#txtIdVistoria").val())) {
    AbrirBoxVistorias($("#txtIdVistoria").val());
}

function removerImg(id) {
    bootbox.confirm('Confirma a exclusão da imagem?', function (result) {
        if (result === true) {
            let img = $("#img" + id).attr("src").split(/[/ ]+/).pop();
            $.post("form/FormVistoriasServer.php", "isAjax=S&RemoverImg=" + img + "&txtId=" + $("#txtIdVistoria").val()).done(function (data) {
                if (data.trim() === "YES") {
                    AbrirBoxVistorias($("#txtIdVistoria").val());
                } else {
                    bootbox.alert("Não foi possível excluir esta imagem!");
                }
            });
        }
    });
}

$('.img-car').dblclick(function () {
    if ($(this).attr("src") !== "./../../img/noimage.png") {
        let img = $(this).attr("data").split("/");
        let imgT = img[img.length - 1].split(".");
        if (imgT[1] === "3gp" || imgT[1] === "mp4") {
            window.open($(this).attr("data"), '_blank');
        } else {
            window.open("./../Seguro/Galeria.php?id=" + $("#txtIdVistoria").val() +
            "&img=" + img[img.length - 1] +
            "&loja=" + parent.$("#txtMnContrato").val(), '_blank');
        }
    }
});

$('.img-car').click(function () {
    $(".img-car-master").attr("src", $(this).attr("src"));
});

$('#tbListaVistorias').dataTable({
    "iDisplayLength": 20,
    "bProcessing": true,
    "bServerSide": true,
    "bFilter": false,
    "ordering": false,
    "bLengthChange": false,
    "sAjaxSource": finalFindVistorias(),
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

function refreshVistorias() {
    var tblCompras = $('#tbListaVistorias').dataTable();
    tblCompras.fnReloadAjax(finalFindVistorias());
}

function finalFindVistorias() {
    return "../../Modulos/Seguro/Vistorias_server_processing.php?idSin=" + $("#txtId").val();
}

$("body").on("click", "#tblTurmasAluno #btnEditData", function () {
    let nRow = $(this).parents('tr')[0];
    let id = $(this).attr("data-id");
    let idDes = $(this).attr("des-id");
    let jqTds = $('>td', nRow);    
    jqTds[0].innerHTML = '<input id=\"txDataMensIni_' + id + '\" type="text" style=\"min-width: 110px;max-width: 110px;\" data-mask="00/00/0000 00:00" data-mask-reverse="true" class="form-control input" value="' + ($(jqTds[0].innerHTML)[0].innerText) + '">';
    jqTds[1].innerHTML = '<input id=\"txDataMensFim_' + id + '\" type="text" style=\"min-width: 110px;max-width: 110px;\" data-mask="00/00/0000 00:00" data-mask-reverse="true" class="form-control input" value="' + ($(jqTds[1].innerHTML)[0].innerText) + '">';
    jqTds[5].innerHTML = '<center><a href="javascript:;" data-id="' + id + '" des-id="' + idDes + '" class="btn green-haze save" title="Salvar" style=\"padding: 0px 4px;background:#5bb75b;\"><span class="ico-checkmark"></span></a>'+
    '<a href="javascript:;" data-id="' + id + '" class="btn red-haze cancel" title="Cancelar" style=\"padding: 0px 4px;background:red;\"><span class="ico-reply"></span></a> <center>';
});

$("body").on('click', '.cancel', function (e) {
    atualizaTurmas();
    atualizaTurmaAluno();
});

$("body").on('click', '.save', function (e) {
    let id = $(this).attr("data-id");   
    let idDes = $(this).attr("des-id");
    if ($("#txDataMensIni_" + id).val() !== "" && !moment($("#txDataMensIni_" + id).val(), lang["dmy"] + " hh:mm", true).isValid()) {
        bootbox.alert("Data Inicial Inválida!");
        return;
    }
    if ($("#txDataMensFim_" + id).val() !== "" && !moment($("#txDataMensFim_" + id).val(), lang["dmy"] + " hh:mm", true).isValid()) {
        bootbox.alert("Data Final Inválida!");
        return;
    }
    bootbox.confirm('Confirma a alteração?', function (result) {
        if (result === true) {
            $.post("../../Modulos/Seguro/form/OficinasFormServer.php", 
            "atualizaTurma=S&isAjax=S&dtIni=" + $("#txDataMensIni_" + id).val() + "&dtFim=" + $("#txDataMensFim_" + id).val() + 
            "&idTurma=" + id + "&idDesmatricula=" + idDes + "&idPessoa=" + $("#txtIdPessoa").val() + 
            "&idTele=" + $("#txtId").val() + "&idPlano=" + $("#txtPlanoCli").val()).done(function (data) {
                if (data.trim() === "YES") {
                    atualizaTurmas();
                    atualizaTurmaAluno();
                } else {
                    bootbox.alert("Não é possível efetuar esta operação!");            
                }
            });
        }
    });
});

$("#txtQtdPrev").mask("99", {placeholder: ""});
$("#txtValorPrev, #txtTotPrev").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});

$('#tbPrevisao').dataTable({   
    "iDisplayLength": 20,
    "bProcessing": true,
    "bServerSide": true,
    "bFilter": false,
    "ordering": false,
    "bLengthChange": false,
    "sAjaxSource": "../../Modulos/Seguro/ajax/Previsao_server_processing.php?id=" + $("#txtId").val(),
    "fnDrawCallback": function (data) {
        let planos = 0;
        for (var i = 0; i < data["aoData"].length; i++) {
            planos += (textToNumber(data["aoData"][i]["_aData"][3]) * textToNumber(data["aoData"][i]["_aData"][4]));
        }
        $("#lblTotParcPrev, #lblTotParcPrev5").html(numberFormat(planos, 1));
    },     
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"        
});

function refreshPrev() {
    let tblCRM = $('#tbPrevisao').dataTable();
    tblCRM.fnReloadAjax("../../Modulos/Seguro/ajax/Previsao_server_processing.php?id=" + $("#txtId").val());    
}

function limparPrev() {
    $("#txt_dt_previsao").val(moment().format(lang["dmy"]));
    $("#txtItemPrev").val("");
    $("#txtFornecPrev").val("");
    $("#txtQtdPrev").val("1");
    $("#txtValorPrev").val("0,00");    
    $("#txtTotPrev").val("0,00");        
}

$("#btnAddPrev").click(function () {
    if ($("#txtItemPrev").val() !== "") {
        parent.$("#loader").show();
        var formData = new FormData();
        formData.append("bntSave", "S");
        formData.append("txtId", $("#txtId").val());    
        formData.append("txt_dt_previsao", $("#txt_dt_previsao").val());
        formData.append("txtItemPrev", $("#txtItemPrev").val());
        formData.append("txtFornecPrev", $("#txtFornecPrev").val());
        formData.append("txtQtdPrev", $("#txtQtdPrev").val());
        formData.append("txtValorPrev", $("#txtValorPrev").val());
        $.ajax({
            type: 'POST',
            url: "../../Modulos/Seguro/form/FormPrevisao.php",
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            parent.$("#loader").hide();
            if (data === "YES") {
                refreshPrev();
                limparPrev();
            } else {
                bootbox.alert(data);
            }
        });    
    } else {
        bootbox.alert("Preencha os dados corretamente!");
    }
});

function RemoverPrev(id) {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/Seguro/form/FormPrevisao.php", 
            "btnDel=S&txtId=" + id).done(function (data) {
                if (data === "YES") {
                    refreshPrev();
                } else {
                    bootbox.alert(data);
                }
            });
        } else {
            return;
        }
    });
}

$('#txtQtdPrev, #txtValorPrev').change(function() {
    $("#txtTotPrev").val(numberFormat(textToNumber($("#txtQtdPrev").val()) * textToNumber($("#txtValorPrev").val())));
});