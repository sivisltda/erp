   
$('#tblTurmas').dataTable({
    "iDisplayLength": 6,
    "bProcessing": true,
    "bServerSide": true,
    "bFilter": false,
    "ordering": false,
    "bLengthChange": false,
    "sAjaxSource": finalFindTurmas(),
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

$('#tblTurmasAluno').dataTable({
    "iDisplayLength": 6,
    "bProcessing": true,
    "bServerSide": true,
    "bFilter": false,
    "ordering": false,
    "bLengthChange": false,
    "sAjaxSource": finalFindTurmasAluno(),
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

function finalFindTurmas() {
    return "./../Academia/ajax/Turmas_server_processing.php?isCliente=S&plano=" + $("#txtPlano").val();
}

function finalFindTurmasAluno() {
    return "../Seguro/form/OficinasFormServer.php?isAjax=S&listTurmaAluno=" + $("#txtId").val();
}

function atualizaTurmas() {
    var tblTurmas = $('#tblTurmas').dataTable();
    tblTurmas.fnReloadAjax(finalFindTurmas());
}

function atualizaTurmaAluno() {
    var tblTurmasAluno = $('#tblTurmasAluno').dataTable();
    tblTurmasAluno.fnReloadAjax(finalFindTurmasAluno());
}

function AbrirBox(id) {
    window.open("./../Seguro/OficinasForm.php?id=" + id, '_blank');
}

function Imprimir() {    
    window.open("./../../../util/ImpressaoPdf.php?id=" + $("#txtId").val() + "&tpImp=I&NomeArq=OS&PathArq=../Modulos/Servicos/modelos/Evento.php&crt=" + parent.$("#txtMnContrato").val(), '_blank');
}

function matricularAluno(idTurma, vagas) {
    if (vagas === 0) {
        bootbox.alert("Oficina não possui vaga!");
        return;
    } else if ($('#txtTurma').val() === "") {
        bootbox.alert("Selecione uma oficina!");
        return;
    }
    bootbox.confirm('Confirma a inclusão do veículo nesta oficina?', function (result) {
        if (result === true) {
            $("#loader").show();
            $.post("../Seguro/form/OficinasFormServer.php", "isAjax=S&addAlunoTurma=" + $("#txtIdPessoa").val() + 
            "&idTurma=" + idTurma + "&idPlano=" + $('#txtPlanoCli').val() + "&idTele=" + $('#txtId').val()).done(function (data) {
                $("#loader").hide();                
                if (data.trim() === "TURMA") {
                    bootbox.alert("Veículo já está nesta oficina!");
                } else if (data.trim() === "YES") {
                    atualizaTurmas();
                    atualizaTurmaAluno();                
                } else {
                    bootbox.alert("Erro ao adicionar o veículo!" + data.trim());
                }
            });
        }
    });
}

function desmatricularTurma(idTurma, idAluno) {
    bootbox.confirm('Confirma a exclusão desse veículo da oficina?', function (result) {
        if (result === true) {
            bootbox.prompt("Informe a conclusão do serviço!", function (result) {
                if (result === "" || result === null) {
                    result = "SEM MOTIVO";
                }
                $.post("../Seguro/form/OficinasFormServer.php", "isAjax=S&excluirAlunoTurma=" + idAluno + 
                "&idTurma=" + idTurma + "&txtMotivo=" + result + "&idTele=" + $('#txtId').val()).done(function (data) {
                    $("#loader").hide();                    
                    if (data.trim() === "YES") {
                        atualizaTurmaAluno();
                        atualizaTurmas();                        
                    } else {
                        bootbox.alert("Erro ao remover aluno!" + data.trim());
                    }
                });
            });
        }
    });
}