$(function () {
    new jBox('Tooltip', {
        attach: '.informacao',
        animation: 'pulse',
        theme: 'TooltipBorder',
        addClass: 'tooltipMU',
        content: ''
    });
});

$("#txtProtegido, #txtConvenios, #txtFranquia, #txtPorcentagem").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
$("#txtKM").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"], centsLimit: 0});
$("#txtDtInstalacao, #txtConvDtIni, #txtConvDtFim").mask('99/99/9999');
$("#txtChip").mask("(99) 99999-9999");
$("#txtPlaca").mask("AAA-9A99");

if ($.isNumeric($("#txtId", window.parent.document).val())) {
    $("#txtIdCliente").val($("#txtId", window.parent.document).val());
}

if ($("#bntNew").length > 0 && $.isNumeric($("#txtId", window.parent.document).val())) {
    $("#bntNew").show();
}

$("#bntProdDesconto").click(function () {
    if ($("#prod_sinal").html() === "%") {
        $("#prod_sinal").html("$");
        $("#txtProdSinal").val("1");
    } else {
        $("#prod_sinal").html("%");
        $("#txtProdSinal").val("0");
    }
    refreshTotal();
});

function validaForm() {
    if ($("#txtPlaca").val() === "" && ($('input[name=rbtipoFipe]:checked').val() === "0" || $('input[name=rbtipoFipe]:checked').val() === "1")) {
        bootbox.alert("Placa é Obrigatória!");
        return false;
    }
    if ($("#txtTipo").val() === "null") {
        bootbox.alert("Tipo é Obrigatório!");
        return false;
    }
    if ($("#txtMontadora").val() === "null") {
        bootbox.alert("Montadora é Obrigatória!");
        return false;
    }
    if ($("#txtModelo").val() === "null") {
        bootbox.alert("Modelo é Obrigatório!");
        return false;
    }
    if ($("#txtAnoMod").val() === "null") {
        bootbox.alert("Ano é Obrigatório!");
        return false;
    }
    if ($("#txtCombustivel").val() === "null") {
        bootbox.alert("Combustível é Obrigatório!");
        return false;
    }
    if (!$.isNumeric($("#txtIdCliente").val())) {
        bootbox.alert("Cliente é Obrigatório!");
        return false;
    }
    if ($("#txtPlano").attr("plObg") === "1" && !$.isNumeric($("#txtPlano").val())) {
        bootbox.alert("Plano é Obrigatório!");
        return false;
    }
    if ($("#txtVendaItem").val() !== "null" && $("#txtFuncionario").val() === "null") {
        bootbox.alert("Selecione um Instalador de Rastreador!");
        return false;
    }    
    if ($("#txtVendaItem").val() === "null" && $("#txtFuncionario").val() !== "null") {
        bootbox.alert("Selecione um Pagamento de Serviço para este Instalador de Rastreador!");
        return false;
    }
    if (textToNumber($("#txtPorcentagem").val()) === 0) {
        bootbox.alert("O valor % FIPE deve ser maior que 0!");
        return false;
    }
    if (textToNumber($("#lblFranquia").html()) < textToNumber($("#txtPisoFranquia").val())) {
        bootbox.alert("O valor mínimo para cota de participação é de R$ " + $("#txtPisoFranquia").val() + "!");
        if ($("#txtProdSinal").val() === "1") {
            $("#txtFranquia").val($("#txtPisoFranquia").val());            
        } else {
            $("#txtFranquia").val(numberFormat(roundUsing(Math.ceil,(textToNumber($("#txtPisoFranquia").val()) * 100) / textToNumber($("#txtProtegido").val()), 2)));
        }
        refreshTotal();
        return false;        
    }
    return true;
}

function roundUsing(func, number, prec) {
    var tempnumber = number * Math.pow(10, prec);
    tempnumber = func(tempnumber);
    return tempnumber / Math.pow(10, prec);
}

$('#txtPlaca').change(function () {
    if ($('input[name="rbtipoFipe"]:checked').val() === "0") {
        parent.$("#loader").show();    
        $.get("./ajax/fipejson.php?functionPage=getPlaca&codPlaca=" + $('#txtPlaca').val()).done(function (json) {
            parent.$("#loader").hide();
            let brand = json["Brand"].split("/");
            $("#txtCombustivel").select2("val", (json["Combustivel"] ? json["Combustivel"] : "Gasolina"));
            if (json["Marca"] && json["Modelo"]) {
                $("#txtMontadora").attr("sval", json["Marca"]);
                $("#txtModelo").attr("sval", json["Modelo"]);
            } else if (brand.length > 1) {
                $("#txtMontadora").attr("sval", brand[0]);
                $("#txtModelo").attr("sval", brand[1]);
            }
            $("#txtAnoMod").attr("sval", json["YearModel"]);
            $("#txtAnoFab").val(json["Ano"]);
            $("#txtCor").val($("#txtCor option:containsIN('" + json["Color"] + "')").val());
            $("#txtCodFipe").val(json["FipeCode"]);
            getMarca();
        });
    }
});

function getMarca() {
    let parametros = "&tipo=" + $("#txtTipo").val() + "&tipoFipe=" + $('input[name="rbtipoFipe"]:checked').val();
    $.get("./ajax/fipejson.php?functionPage=getMarca" + parametros).done(function (json) {
        let options = "<option value=\"null\">Selecione</option>";
        let idSelect = "null";
        if (json !== null) {
            for (var x = 0; x < json.length; x++) {
                options += "<option val=\"" + json[x].Value + "\" value=\"" + json[x].Label + "\">" + json[x].Label + "</option>";
                if ($("#txtMontadora").attr("sval") && $("#txtMontadora").attr("sval").length > 0 && json[x].Label.toUpperCase() === $("#txtMontadora").attr("sval").toUpperCase()) {
                    idSelect = json[x].Label;
                } else {
                    //console.log(json[x].Label.toUpperCase() + ' - ' + $("#txtMontadora").attr("sval").toUpperCase());
                }
            }
            if (idSelect === "null") {
                for (var x = 0; x < json.length; x++) {
                    if ($("#txtMontadora").attr("sval") && $("#txtMontadora").attr("sval").length > 0 && json[x].Label.toUpperCase().includes($("#txtMontadora").attr("sval").toUpperCase())) {
                        idSelect = json[x].Label;
                    }
                }
            }
            $("#txtMontadora").html(options);
        }
        if(idSelect === "null" && $("#txtMontadora").attr("sval")) {
            let find_modelo = $("#txtMontadora").attr("sval").split(" ");
            for (var i = find_modelo.length; i > 0; i--) {
                let item = find_modelo.slice(0, i).join(' ');
                if (idSelect === "null" && $('#txtMontadora option:containsIN(' + item + ')').length > 0) {
                    idSelect = $('#txtMontadora option:containsIN(' + item + ')').val();
                    $("#txtMontadora").select2("val", idSelect);
                }
            }
        } else {
            $("#txtMontadora").select2("val", idSelect);            
        }
        getModeloAno();
    });
}

function getAcessorios() {
    $.getJSON("form/FormVeiculosServer.php", {isAjax: "S", getAcessorios: $("#txtTipo").val()}, function (json) {
        let options = "<option value=\"null\">Selecione</option>";
        if (json !== null) {
            for (var x = 0; x < json.length; x++) {
                options += "<option value=\"" + json[x].conta_produto + "\" val=\"" + json[x].preco_venda + "\">" + json[x].descricao + "</option>";
            }
        }
        $("#txtProduto").html(options);
        $("#txtProduto").select2("val", "null");
    }); 
}

function getModeloAno() {
    if ($.isNumeric($("#txtMontadora option:selected").attr("val"))) {
        let parametros = "&tipo=" + $("#txtTipo").val() + "&codMarca=" + $("#txtMontadora option:selected").attr("val") + 
        "&tipoFipe=" + $('input[name="rbtipoFipe"]:checked').val();
        $.get("./ajax/fipejson.php?functionPage=getModelo" + parametros).done(function (json) {
            let optionsModelo = "<option value=\"null\">Selecione</option>";
            let idSelectModelo = "null";
            let optionsAno = "<option value=\"null\">Selecione</option>";
            let idSelectAno = "null";
            if (json !== null) {
                for (var x = 0; x < json.Modelos.length; x++) {
                    optionsModelo += "<option val=\"" + json.Modelos[x].Value + "\" value=\"" + json.Modelos[x].Label + "\">" + json.Modelos[x].Label + "</option>";
                    if ($("#txtModelo").attr("sval") && $("#txtModelo").attr("sval").replace(/\s/g,'').toUpperCase() === json.Modelos[x].Label.replace(/\s/g,'').toUpperCase()) {
                        idSelectModelo = json.Modelos[x].Label;
                    }
                }                
                for (var x = 0; x < json.Anos.length; x++) {
                    if (($('input[name="rbtipoFipe"]:checked').val() === "2" && json.Anos[x].Value.startsWith("32000")) || $('input[name="rbtipoFipe"]:checked').val() !== "2") {
                        optionsAno += "<option val=\"" + json.Anos[x].Value + "\" value=\"" + json.Anos[x].Label + "\">" + json.Anos[x].Label.replace("32000", "Zero KM") + "</option>";
                        if ($("#txtAnoMod").attr("sval") && $("#txtAnoMod").attr("sval").toUpperCase() + "-" + getCombustivel() === json.Anos[x].Value.toUpperCase()) {
                            idSelectAno = json.Anos[x].Label;
                        }                        
                    }
                }                                
            }
            $("#txtModelo").html(optionsModelo);
            $("#txtModelo").select2("val", idSelectModelo);
            $("#txtAnoMod").html(optionsAno);
            $("#txtAnoMod").select2("val", idSelectAno);
            if (idSelectModelo !== "null" && idSelectAno !== "null") {
                getVeiculo();
            } else if(idSelectModelo === "null" && $("#txtModelo").attr("sval")) {
                getModelo();
            }
        });
    } else {
        $("#txtModelo").html("<option value=\"null\">Selecione</option>");
        $("#txtModelo").select2("val", "null");
        $("#txtAnoMod").html("<option value=\"null\">Selecione</option>");
        $("#txtAnoMod").select2("val", "null");
    }
}

function getModelo() {
    if ($.isNumeric($("#txtMontadora option:selected").attr("val")) && $("#txtAnoMod option:selected").attr("val")) {        
        let parametros = "&tipo=" + $("#txtTipo").val() + "&codMarca=" + $("#txtMontadora option:selected").attr("val") +
        "&codAno=" + $("#txtAnoMod option:selected").attr("val") + "&tipoFipe=" + $('input[name="rbtipoFipe"]:checked').val();
        $.get("./ajax/fipejson.php?functionPage=getModeloPorAno" + parametros).done(function (json) {
            let optionsModelo = "<option value=\"null\">Selecione</option>";
            let idSelectModelo = "null";
            if (json !== null) {
                for (var x = 0; x < json.length; x++) {
                    optionsModelo += "<option val=\"" + json[x].Value + "\" value=\"" + json[x].Label + "\">" + json[x].Label + "</option>";
                    if ($("#txtModelo").attr("sval").replace(/\s/g,'').toUpperCase() === json[x].Value.replace(/\s/g,'').toUpperCase()) {
                        idSelectModelo = json[x].Label;
                    }
                }
            }
            $("#txtModelo").html(optionsModelo);              
            if(idSelectModelo === "null" && $("#txtModelo").attr("sval")) {
                let find_modelo = $("#txtModelo").attr("sval").split(" ");
                for (var i = find_modelo.length; i > 0; i--) {
                    let item = find_modelo.slice(0, i).join(' ');            
                    if (idSelectModelo === "null" && $('#txtModelo option:containsIN(' + item + ')').length > 0) {
                        idSelectModelo = $('#txtModelo option:containsIN(' + item + ')').val();
                    }
                }
            }        
            $("#txtModelo").select2("val", idSelectModelo);
            getVeiculo();
        });
    } else {
        $("#txtModelo").html("<option value=\"null\">Selecione</option>");
        $("#txtModelo").select2("val", "null");
    }
}

function getAnos() {
    if ($.isNumeric($("#txtMontadora option:selected").attr("val")) && $.isNumeric($("#txtModelo option:selected").attr("val"))) {
        let parametros = "&tipo=" + $("#txtTipo").val() + "&codMarca=" + $("#txtMontadora option:selected").attr("val") +
        "&codModelo=" + $("#txtModelo option:selected").attr("val") + "&tipoFipe=" + $('input[name="rbtipoFipe"]:checked').val();
        $.get("./ajax/fipejson.php?functionPage=getAnos" + parametros).done(function (json) {
            let optionsAnos = "<option value=\"null\">Selecione</option>";
            let idSelectAnos = "null";
            if (json !== null) {
                for (var x = 0; x < json.length; x++) {
                    if (($('input[name="rbtipoFipe"]:checked').val() === "2" && json.Anos[x].Value.startsWith("32000")) || $('input[name="rbtipoFipe"]:checked').val() !== "2") {                    
                        optionsAnos += "<option val=\"" + json[x].Value + "\" value=\"" + json[x].Label + "\">" + json[x].Label + "</option>";
                        if ($("#txtAnoMod").attr("sval").toUpperCase() + "-" + getCombustivel() === json[x].Value.toUpperCase()) {
                            idSelectAnos = json[x].Label;
                        }
                    }
                }
            }
            $("#txtAnoMod").html(optionsAnos);
            $("#txtAnoMod").select2("val", idSelectAnos);
            getVeiculo();
        });
    } else {
        $("#txtAnoMod").html("<option value=\"null\">Selecione</option>");
        $("#txtAnoMod").select2("val", "null");
    }
}

function getVeiculo() {
    if ($.isNumeric($("#txtMontadora option:selected").attr("val")) && $.isNumeric($("#txtModelo option:selected").attr("val")) && $("#txtAnoMod").val() !== "null") {
        let parametros = "&tipo=" + $("#txtTipo").val() + "&codMarca=" + $("#txtMontadora option:selected").attr("val") +
        "&codModelo=" + $("#txtModelo option:selected").attr("val") + "&codAno=" + $("#txtAnoMod option:selected").attr("val") + 
        "&tipoFipe=" + $('input[name="rbtipoFipe"]:checked').val();
        $.get("./ajax/fipejson.php?functionPage=getVeiculo" + parametros).done(function (json) {
            let valor = textToNumber(("Valor" in json) ? json.Valor.replace("R$ ", "") : "0");
            let porce = textToNumber($("#txtPorcentagem").val());
            $("#txtCodFipe").val(("CodigoFipe" in json) ? json.CodigoFipe : "");
            $("#txtValorFipe").val(numberFormat(valor));
            if (!$.isNumeric($("#txtId").val())) {
                $("#txtProtegido").val(numberFormat((valor * porce) / 100));
                getPlanos($("#txtValorFipe").val());
            } else if ($("#txtTipoProtegido").val() === "0") {
                getPlanos($("#txtValorFipe").val());
            } else if ($("#txtTipoProtegido").val() === "1") {
                let prot_hist = textToNumber($("#txtProtegido").val());
                getPlanos(numberFormat((prot_hist * 100)/porce));                    
            } else if ($("#txtTipoProtegido").val() === "2") {                
                let prot_hist = textToNumber($("#txtProtegido").val());
                getPlanos(numberFormat(prot_hist));                                                                                        
            }
        });
    } else {
        $("#txtCodFipe").val("");
        $("#txtValorFipe").val("0,00");
        $("#txtProtegido").val("0,00");
    }
}

$('#txtTipo').change(function () {
    getMarca();
    getAcessorios();
});

$('#txtMontadora').change(function () {
    getModeloAno();
});

$('#txtAnoMod').change(function () {
    if ($("#txtAnoMod option:selected").attr("val")) {
        let itens = $("#txtAnoMod option:selected").attr("val").split('-');        
        $("#txtAnoFab").val((parseInt(itens[0]) > parseInt(moment().format('YYYY')) ? moment().format('YYYY') : itens[0]));
        $("#txtCombustivel").select2("val", setCombustivel(itens[1]));
    }
    if ($.isNumeric($("#txtModelo option:selected").attr("val"))) {
        getVeiculo();
    } else {
        getModelo();    
    }
});

$('#txtModelo').change(function () {
    if ($("#txtAnoMod option:selected").attr("val")) {
        getVeiculo();
    } else {
        getAnos();        
    }
});

$('#txtPorcentagem').change(function () {
    let valor = textToNumber($("#txtValorFipe").val());
    let porce = textToNumber($("#txtPorcentagem").val());
    $("#txtProtegido").val(numberFormat((valor * porce) / 100));
    refreshTotal();
    getVeiculo();    
});

$('#txtProtegido').change(function () {
    let prot_hist = textToNumber($("#txtProtegido").val());
    let porce = textToNumber($("#txtPorcentagem").val());    
    getPlanos(numberFormat((prot_hist * 100)/porce));    
    console.log('txtProtegido');
});

function getCombustivel() {
    let toReturn = "";
    switch ($("#txtCombustivel").val()) {
        case "Gasolina":
            toReturn = "1";
            break;
        case "Álcool":
            toReturn = "2";
            break;
        case "Diesel":
            toReturn = "3";
            break;
        default:
            toReturn = "1";
    }
    return toReturn;
}

function setCombustivel(id) {
    let toReturn = "";
    switch (id) {
        case "1":
            toReturn = "Gasolina";
            break;
        case "2":
            toReturn = "Álcool";
            break;
        case "3":
            toReturn = "Diesel";
            break;
        default:
            toReturn = "Gasolina";
    }
    return toReturn;
}

$('#txtProduto').change(function () {
    $("#txtValToTP").val(numberFormat($("#txtProduto option:selected").attr("val")));
});

function listaProdutos() {
    $('#tbProdutos tbody').remove();
    $.getJSON('form/FormVeiculosServer.php?isAjax=S', {listaItens: $("#txtIdPlano").val()}, function (j) {
        if (j !== null) {
            for (var i = 0; i < j.length; i++) {
                addlinha(i, j[i].id, j[i].id_produto, j[i].valor_total, j[i].produtodesc);
            }
        }
    }).done(function () {
        somaTotal();
    });
}

function addlinha(conta, idItem, produto, valTotal, descrProd) {
    var disabled = $("#txtCombustivel").is('[disabled]') ? "disabled" : "";
    $("#tbProdutos").append("<tr id=\"tabela_linha_" + conta + "\">" +
    "<td style=\"display: none;\" class=\"item_count\" >" + conta + "</td>" +
    "<td class=\"lisbody\"><input name=\"txtIP[]\" type=\"hidden\" value=\"" + idItem + "\">" +
    "<input name=\"txtPD[]\" type=\"hidden\" value=\"" + produto + "\">" +
    "<input name=\"txtVP[]\" type=\"hidden\" value=\"" + valTotal + "\">" + descrProd + "</td>" +
    "<td class=\"lisbody vtotal\" width=\"100px\" style=\"text-align: right;\">" + valTotal + "</td>" +
    "<td class=\"lisbody\" width=\"23px\"><center><input class=\"btn red\" type=\"button\" value=\"x\" " + disabled + " onclick=\"removeLinha('tabela_linha_" + conta + "')\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\"></center></td></tr>");
}

function novaLinha() {
    if ($("#txtProduto").val() === "null") {
        bootbox.alert("Escolha um produto para adicionar!");
    } else {
        var conta = 1;
        $("#tbProdutos").find("tr > .item_count").each(function () {
            if (parseFloat($(this).html()) >= conta) {
                conta = parseFloat($(this).html()) + 1;
            }
        });
        addlinha(conta, "", $("#txtProduto").val(), $("#txtValToTP").val(), $("#txtProduto option:selected").text());
        somaTotal();
        refreshTotal();
    }
}

function removeLinha(linha) {
    $("#" + linha).remove();
    somaTotal();
    refreshTotal();
}

function somaTotal() {
    var totalProdutos = 0.00;
    $("#tbProdutos tr").each(function () {
        totalProdutos = totalProdutos + textToNumber($(this).find(".vtotal").html());
    });
    $("#totalP").html(numberFormat(totalProdutos, 1));
}

getMarca();
listaProdutos();

$('#tblHistPlanos').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    bPaginate: false, bFilter: false, bInfo: false, bSort: false,
    "sAjaxSource": "form/FormVeiculosServer.php?isAjax=S&listPlanosTodos=" + $("#txtId").val(),
    'oLanguage': {
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sZeroRecords': "Não foi encontrado nenhum resultado"
    }
});

$('#tblHistVistorias').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    bPaginate: false, bFilter: false, bInfo: false, bSort: false,
    "sAjaxSource": "form/FormVeiculosServer.php?isAjax=S&tblHistVistorias=" + $("#txtId").val(),
    'oLanguage': {
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sZeroRecords': "Não foi encontrado nenhum resultado"
    }
});

$('.img-car').dblclick(function () {
    if ($(this).attr("src") !== "./../../img/noimage.png") {
        let img = $(this).attr("src").split("/");
        let imgT = img[img.length - 1].split(".");
        if (imgT[1] === "3gp" || imgT[1] === "mp4") {
            window.open($(this).attr("data"), '_blank');
        } else {
            window.open("./../Seguro/Galeria.php?id=" + $("#txtIdVistoria").val() +
            "&img=" + img[img.length - 1] +
            "&loja=" + parent.$("#txtMnContrato").val(), '_blank');
        }
    }
});

$('.img-car').click(function () {
    $(".img-car-master").attr("src", $(this).attr("src"));
});

function getPlanos(valor) {
    $.getJSON('form/FormVeiculosServer.php?isAjax=S&getPlano=S', {tipo: $("#txtTipo").val(), valor: valor, ano: $("#txtAnoFab").val(),
        pessoa: $("#txtIdCliente").val(), marca: $("#txtMontadora option:selected").attr("val")}, function (json) {
        let options = "<option price=\"0,00\" value=\"null\">Selecione</option>";
        let idSelect = "null";
        if (json !== null) {
            for (var x = 0; x < json.length; x++) {
                options += "<option plano=\"" + json[x].Plano + "\" price=\"" + json[x].Price + "\" value=\"" + json[x].Value + 
                    "\" parcela=\"" + json[x].Parcela + "\" cota=\"" + json[x].Cota + "\" cotaTipo=\"" + json[x].Tipo + "\">" + json[x].Label + "</option>";
                if ($("#txtPlano").attr("sval").toUpperCase() === json[x].Value.toUpperCase()) {
                    idSelect = json[x].Value;
                }
            }
        }
        $("#txtPlano").html(options);
        $("#txtPlano").select2("val", idSelect);
        refreshTotal();
    });
}

function getDesconto(val) {
    $.get("./../Academia/form/ClientesFormServer.php?isAjax=S&verificaConvenio=" + $('#txtIdCliente').val() + 
    "&valorProduto=" + val + "&idProd=" + $("#txtPlano option:selected").attr("plano") +
    "&idConvProd=" + $("#txtIdPlano").val()).done(function (v) {
        let total = val - textToNumber(v);
        let meses = (textToNumber($("#txtPlano option:selected").attr("parcela")) > 1 ? textToNumber($("#txtPlano option:selected").attr("parcela")) : 1);        
        $("#lblTotal").html(numberFormat(total / meses, 1));    
        let valor = textToNumber($("#txtProtegido").val());
        let franq = textToNumber($("#txtFranquia").val());
        let porct = textToNumber($("#txtPorcentagem").val());
        if ($("#txtProdSinal").val() === "1") {
            $("#lblFranquia").html(numberFormat(franq, 1));            
        } else {
            $("#lblFranquia").html(numberFormat((franq > 0 ? ((porct > 0 ? ((valor * 100) / porct) : valor) * franq / 100) : 0), 1));                        
        }
    });
}

function getPagamentosVeiculos(id_pessoa) {
    $.getJSON("form/FormVeiculosServer.php", {isAjax: "S", getPagamentos: id_pessoa}, function (json) {
        let options = "<option value=\"null\">Selecione</option>";
        if (json !== null) {
            for (var x = 0; x < json.length; x++) {
                options += "<option value=\"" + json[x].Value + "\">" + json[x].Label + "</option>";
            }
        }
        $("#txtVendaItem").html(options);
    });    
}

$('#txtPlano, #txtFranquia').change(function () {    
    refreshTotal();
});

function refreshTotal() {
    if ($("#txtFranquia").val() === "0,00") {
        $("#txtFranquia").val(numberFormat($("#txtPlano option:selected").attr("cota")));
        $("#txtProdSinal").val($("#txtPlano option:selected").attr("cotaTipo"));
    }    
    $("#prod_sinal").html($("#txtProdSinal").val() === "1" ? "$" : "%");    
    let total = 0.00;
    let parcela = (textToNumber($("#txtPlano option:selected").attr("parcela")) < 1 ? 1 : textToNumber($("#txtPlano option:selected").attr("parcela")));
    total += textToNumber($("#txtPlano option:selected").attr("price"));    
    $("#tbProdutos tr").each(function () {
        total += textToNumber($(this).find(".vtotal").html()) * parcela;
    });
    getDesconto(total);
}

$('input[name=rbOption]').change(function(ev) {
    if ($(this).val() === "V") {
        $("#imgVeiculo").show();
        $("#imgDanos").hide();        
    } else if ($(this).val() === "D") {
        $("#imgVeiculo").hide();
        $("#imgDanos").show();                
    }
});

$('input[name=rbtipoFipe]').change(function(ev) {
    if ($(this).val() === "0") {
        $("#txtPlaca").attr("readonly", false);         
        $("#txtPlaca").val($("#txtPlaca").attr("item"));        
        $("#txtProtegido").attr("readonly", true); 
        $("#txtVeiculoZ").select2("val", 0);
    } else if ($(this).val() === "1") {
        $("#txtPlaca").attr("readonly", false);         
        $("#txtPlaca").val($("#txtPlaca").attr("item"));        
        $("#txtProtegido").attr("readonly", false); 
        $("#txtVeiculoZ").select2("val", 0);        
    } else if ($(this).val() === "2") {
        $("#txtPlaca").attr("readonly", true);         
        $("#txtPlaca").val("");
        $("#txtProtegido").attr("readonly", true); 
        $("#txtVeiculoZ").select2("val", 1);        
    }
    getMarca();    
});

getPagamentosVeiculos($("#txtIdCliente").val());

$('#bntSinc').on('click', function(e) {
    e.preventDefault();
    if ($("#txtChassi").val().length > 10) {
        const id = $(this).data('id');
        parent.$("#loader").show();
        $.ajax({
            type: 'POST', url: './ajax/assistencia.php',
            data: 'functionPage=criarNovoVeiculo&id_veiculo='+id,
            dataType: 'text',
            success: function (res) {
                console.log(res);
                parent.$("#loader").hide();
                if(res === 'OK') {
                    $('#bntSinc').css('height','0px').hide();
                    $('#bntSincCancel').css('height','28px').show();
                }else {
                    bootbox.alert(res);
                }
            }
        });
   } else {
       bootbox.alert("Número do Chassi inválido!");
   } 
});

$('#bntSincCancel').on('click', function(e) {
    e.preventDefault();
    const id = $(this).data('id');
    parent.$("#loader").show();
    $.ajax({
        type: 'POST', url: './ajax/assistencia.php',
        data: 'functionPage=alterarVeiculo&id_veiculo='+id+'&excluir=S',
        dataType: 'text',
        success: function (res) {
            console.log(res);
            parent.$("#loader").hide();
            if (res === 'OK') {
                $('#bntSincCancel').css('height','0px').hide();
                $('#bntSinc').css('height','28px').show();
            } else {
                bootbox.alert(res);
            }
        }
    });
});

$('#bntSincRast').on('click', function(e) {
    e.preventDefault();
    if ($("#txtChassi").val().length > 10) {
        const id = $(this).data('id');
        parent.$("#loader").show();
        $.ajax({
            type: 'POST', url: './ajax/rastreamento.php',
            data: 'functionPage=criarNovoVeiculo&id_veiculo='+id,
            dataType: 'text',
            success: function (res) {
                console.log(res);
                parent.$("#loader").hide();
                if(res === 'OK') {
                    $('#bntSincRast').css('height','0px').hide();
                    $('#bntSincRastCancel').css('height','28px').show();
                }else {
                    bootbox.alert(res);
                }
            }
        });
   } else {
       bootbox.alert("Número do Chassi inválido!");
   } 
});

$('#bntSincRastCancel').on('click', function(e) {
    e.preventDefault();
    const id = $(this).data('id');
    parent.$("#loader").show();
    $.ajax({
        type: 'POST', url: './ajax/rastreamento.php',
        data: 'functionPage=alterarVeiculo&id_veiculo='+id+'&excluir=S',
        dataType: 'text',
        success: function (res) {
            console.log(res);
            parent.$("#loader").hide();
            if (res === 'OK') {
                $('#bntSincRastCancel').css('height','0px').hide();
                $('#bntSincRast').css('height','28px').show();
            } else {
                bootbox.alert(res);
            }
        }
    });
});

$('#tblConvenio').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": "./../Academia/form/ClientesConveniosServer.php?listConvenios=" + $("#txtIdCliente").val() + "&txtIdPlano=" + $("#txtIdPlano").val(),
    "bFilter": false,
    "bSort": false,
    "bPaginate": false,
    'oLanguage': {
        'sInfo': "",
        'sInfoEmpty': "",
        'sInfoFiltered': "",
        'sLengthMenu': "",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

function RemoverConvenio(idConv) {
    $.post("./../Academia/form/ClientesConveniosServer.php", "btnExcluirConvenio=S&txtId=" + $("#txtIdCliente").val() + "&txtIdConv=" + idConv).done(function (data) {
        var tblConvenio = $('#tblConvenio').dataTable();
        tblConvenio.fnReloadAjax("./../Academia/form/ClientesConveniosServer.php?listConvenios=" + $("#txtIdCliente").val() + "&txtIdPlano=" + $("#txtIdPlano").val());
        refreshTotal();
    });
}

$("#btnTpValor").click(function () {
    if ($("#tp_sinal").html() === "%") {
        $("#tp_sinal").html("$");
        $("#txtSinal").val("D");
    } else {
        $("#tp_sinal").html("%");
        $("#txtSinal").val("P");
    }
});

$('#btnIncluirConv').click(function (e) {
    if (textToNumber($("#txtConvenios").val()) > 0) {
        var data1 = moment($("#txtConvDtIni").val(), lang["dmy"]);
        var data2 = moment($("#txtConvDtFim").val(), lang["dmy"]);
        if (data2 < data1) {
            bootbox.alert("Periodo inválido!");
        } else {
            $.post("./../Academia/form/ClientesConveniosServer.php", 
            "SalvarConvenioAdd=S&txtIdPlano=" + $("#txtIdPlano").val() + "&txtValConv=" + $("#txtConvenios").val() + "&txtSinal=" + $("#txtSinal").val() +
            "&dti=" + $("#txtConvDtIni").val() + "&dtf=" + $("#txtConvDtFim").val() + 
            "&txtAteVencimento=" + ($("#txtAteVencimento").attr("checked") ? "1" : "0")).done(function (data) {
                if (data.trim() === "YES") {
                    var tblConvenio = $('#tblConvenio').dataTable();
                    tblConvenio.fnReloadAjax("./../Academia/form/ClientesConveniosServer.php?listConvenios=" + $("#txtIdCliente").val() + "&txtIdPlano=" + $("#txtIdPlano").val());
                } else {
                    bootbox.alert("Erro ao incluir convênio!");
                }
                $("#txtConvenios").val("0,00");
                $("#tp_sinal").html("%");
                $("#txtSinal").val("P");                
                $("#txtConvDtIni, #txtConvDtFim").val("");
                if ($("#txtAteVencimento").attr("checked")) {
                    $("#txtAteVencimento").click();    
                }
                refreshTotal();
            });
        }
    } else {
        bootbox.alert("Valor de Convênio Inválido!");
    }
});

function fipeToProtegido() {
    bootbox.confirm('Confirma a atualização do valor Protegido?', function (result) {
        if (result === true) {
            let valor = textToNumber($("#txtValorFipe").val());
            let porce = textToNumber($("#txtPorcentagem").val());
            $("#txtProtegido").val(numberFormat((valor * porce) / 100));
            getPlanos($("#txtValorFipe").val());
            refreshTotal();            
        }
    });
}

$('#tblKm').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": "./../Seguro/form/ClientesKmServer.php?listKms=" + $("#txtId").val(),
    "bFilter": false,
    "bSort": false,
    "bPaginate": false,
    'oLanguage': {
        'sInfo': "",
        'sInfoEmpty': "",
        'sInfoFiltered': "",
        'sLengthMenu': "",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

function RemoverKms(idKm) {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("./../Seguro/form/ClientesKmServer.php", "btnExcluirKm=S&txtId=" + $("#txtId").val() + "&txtIdKm=" + idKm).done(function (data) {
                var tblKm = $('#tblKm').dataTable();
                tblKm.fnReloadAjax("./../Seguro/form/ClientesKmServer.php?listKms=" + $("#txtId").val());
            });
        } else {
            return;
        }
    });        
}

$('#btnIncluirKm').click(function (e) {
    $.post("./../Seguro/form/ClientesKmServer.php", "SalvarKm=S&txtId=" + $("#txtId").val() + 
        "&txtKms=" + $("#txtKms").val() + "&dti=" + $("#txtDtKms").val()).done(function (data) {
        if (data.trim() === "YES") {
            var tblKm = $('#tblKm').dataTable();
            tblKm.fnReloadAjax("./../Seguro/form/ClientesKmServer.php?listKms=" + $("#txtId").val());
        } else {
            bootbox.alert("Erro ao incluir Kms!");
        }
        $("#txtKms").val("0");
        $("#txtDtKms").val("");
    });
});

$("#txtTransfPara").autocomplete({
    source: function (request, response) {
        $("#txtTransfParaCod").val("");
        $.ajax({
            url: "../../Modulos/CRM/ajax.php",
            dataType: "json",
            data: {
                q: request.term,
                t: 'C',
                n: $("#txtId", window.parent.document).val()
            },
            success: function (data) {
                response(data);
            }
        });
    },
    minLength: 3,
    select: function (event, ui) {
        $("#txtTransfParaCod").val(ui.item.id);
    }
});

$('#btnSalvaTransf').click(function (e) {
    if ($("#txtTransfParaCod").val() === "" || $("#txtMotivo").val() === "") {
        bootbox.alert("Destinatário e Motivo são obrigatórios!");
    } else {
        bootbox.confirm('Confirma a Transferêcia? Essa ação não poderá ser revertida.', function (result) {
            if (result === true) {
                $.post("./../Academia/form/ClientesPlanosServer.php", 
                        "transfVeiculo=" + $("#txtId").val() + 
                        "&txtTransfParaCod=" + $("#txtTransfParaCod").val() +
                        "&txtIdDe=" + $("#txtId", window.parent.document).val() +
                        "&txtMotivo=" + $("#txtMotivo").val()).done(function (data) {
                    if (data.trim() === "YES") {
                        bootbox.alert("Transferência efetuada com sucesso!");
                        parent.FecharBoxVeiculo();
                    } else {
                        bootbox.alert("Erro ao salvar dados!");
                    }
                });
            }
        });
    }
});

function AbonarMens() {   
    let bb = bootbox.prompt({title: "Confirma a reprovação deste veículo?<br>Informe o motivo, este motivo será enviado ao cliente:", inputType: 'textarea', callback: function (result) {
        if (result === null) {
            return;            
        } else if (result.length > 0) {
            escolherUsuario(result);
        } else {            
            bb.find('.bootbox-input-textarea').css("border-color", "red");
            return false;
        }
    }});        
}

function escolherUsuario(motivo) {
    $.getJSON('./../CRM/departamento.ajax.php', {txtDepartamento: "null"}, function (itens) {
        let arrayOptions = [];   
        arrayOptions.push({text: 'Selecione', value: 'null'});
        for (var i = 0; i < itens.length; i++) {
            arrayOptions.push({text: itens[i].nome, value: itens[i].id_usuario});
        }                
        bootbox.prompt({
            title: "Selecione um destinatário para encaminhamento desta pendência:",
            inputType: 'select',
            inputOptions: arrayOptions,
            callback: function (result) {
                if (result !== 'null') {
                    $.post("./../Seguro/form/FormVeiculosServer.php",
                        "isAjax=S&bntReprov=S&txtId=" + $("#txtId").val() +
                        "&motivo=" + motivo + "&txtPara=" + result).done(function (data) {
                        if (data.trim() === "YES") {
                            parent.FecharBoxVeiculo();
                        } else {
                            bootbox.alert("Erro ao reprovar veículo!");
                        }
                    });
                } else {
                    bootbox.alert("É necessário selecionar um contrato usuário!");
                }
            }
        });
    });            
}

$.extend($.expr[":"], {
    "containsIN": function(elem, i, match, array) {
        return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
});