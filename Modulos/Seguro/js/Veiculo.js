
function finalFindVeiculo(imp) {
    var retPrint = "?imp=" + imp;
    if ($("#txtBusca").length > 0 && $("#txtBusca").val() !== "") {
        retPrint += '&Search=' + $("#txtBusca").val();
    }
    if ($("#txtFilter").length > 0 && $("#txtFilter").val() !== "") {
        retPrint += '&filter=' + $("#txtFilter").val();
    }
    if ($("#txtUserResp").length > 0 && $('#txtUserResp').val() !== "null") {
        retPrint += "&ur=" + $('#txtUserResp').val();
    }    
    if ($("#txtTipoCli").length > 0 && $('#txtTipoCli').val() !== "null") {
        retPrint += "&tpCli=" + $('#txtTipoCli').val();
    }
    if ($("#txtStatusAp").length > 0 && $("#txtStatusAp").val() !== "null") {
        retPrint += '&statusap=' + $("#txtStatusAp").val();
    }
    if ($("#txtCategoria").length > 0 && $("#txtCategoria").val() !== "null") {
        retPrint += '&categoria=' + $("#txtCategoria").val();
    }
    if ($("#txtValPlanoAte").length > 0 && $("#txtValPlanoAte").val() !== "null") {
        retPrint += '&valor=' + $("#txtValPlanoAte").val();
    }
    if ($("#txtVistorias").length > 0 && $("#txtVistorias").val() !== "null") {
        retPrint += '&vistorias=' + $("#txtVistorias").val();
    }
    if ($("#txtAssistencia").length > 0 && $("#txtAssistencia").val() !== "null") {
        retPrint += '&assistencia=' + $("#txtAssistencia").val();
    }
    if ($("#txtRastreador").length > 0 && $("#txtRastreador").val() !== "null") {
        retPrint += '&rastreador=' + $("#txtRastreador").val();
    }
    if ($("#txtStatus").length > 0 && $("#txtStatus").val() !== "null") {
        retPrint += "&status=" + $("#txtStatus").val();
    }
    if ($("#txtStatusCli").length > 0 && $("#txtStatusCli").val() !== "null") {
        retPrint += "&statusCli=" + $("#txtStatusCli").val();
    }
    if ($("#txtTipoVei").length > 0 && $("#txtTipoVei").val() !== "null") {
        retPrint += "&tpVei=" + $("#txtTipoVei").val();
    }
    if ($("#txtTipoPlano").length > 0 && $("#txtTipoPlano").val() !== "null") {
        retPrint += "&tpPlano=" + $("#txtTipoPlano").val();
    }
    if ($("#txtTipoItem").length > 0 && $("#txtTipoItem").val() !== "null") {
        retPrint += "&tpItem=" + $("#txtTipoItem").val();
    }
    if ($("#txtFilial").length > 0 && $("#txtFilial").val() !== "null") {
        retPrint += "&filial=" + $('#txtFilial').val();
    }
    if ($.isNumeric($("#txtId", window.parent.document).val())) {
        retPrint += '&idCli=' + $("#txtId", window.parent.document).val();
    }
    return "./../Seguro/Veiculos_server_processing.php" + retPrint.replace(/\//g, "_");
}

function AbrirBoxVeiculo(id, id_pessoa) {
    abrirTelaBox("./../Seguro/FormVeiculos.php?id_pessoa=" + id_pessoa + (id > 0 ? "&id=" + id : ""), 618, 1000);
}

function AbrirBoxRastreador(id) {
    abrirTelaBox("./../Seguro/FormRastreador.php" + (id > 0 ? "?id=" + id : ""), 280, 400);
}

function FecharBoxVeiculo() {
    $("#newbox").remove();
    tbListaVeiculo.fnReloadAjax(finalFindVeiculo(0));
    if ($("#tblPlanosClientes").length > 0) {
        AtualizaListPlanosClientes();
    }
    FecharBoxVistorias();
}

var tbListaVeiculo = $('#tbListaVeiculo').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "ordering": false,
    "sAjaxSource": finalFindVeiculo(0),
    "fnDrawCallback": function (data) {
        if ($("#txtFilter").length > 0) {
            $.getJSON(finalFindVeiculo(0) + "&getEstatisticas=S", function (json) {
                $.each(json, function (key, val) {
                    $("#" + key).html(val);
                });
            });
        }
    },    
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});