
function finalFindSinistro(imp) {
    var retPrint = (imp === 1 ? '&imp=1' : '?imp=0');
    if ($("#txt_dt_begin").length > 0 && $("#txt_dt_begin").val() !== "") {
        retPrint += "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "/");
    }
    if ($("#txt_dt_end").length > 0 && $("#txt_dt_end").val() !== "") {
        retPrint += "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "/");
    }    
    if ($("#txtBusca").length > 0 && $("#txtBusca").val() !== "") {
        retPrint += '&Search=' + $("#txtBusca").val();
    }
    if ($("#txtStatusAp").length > 0 && $("#txtStatusAp").val() !== "null") {
        retPrint += '&statusap=' + $("#txtStatusAp").val();
    }
    if ($("#txtAcaoEvento").length > 0 && $("#txtAcaoEvento").val() !== "null") {
        retPrint += '&acao=' + $("#txtAcaoEvento").val();
    }
    if ($("#txtOficina").length > 0 && $("#txtOficina").val() !== "null") {
        retPrint += '&oficina=' + $("#txtOficina").val();
    }
    if ($.isNumeric($("#txtId", window.parent.document).val())) {
        retPrint += '&idCli=' + $("#txtId", window.parent.document).val();
    }
    return "./../Seguro/Sinistros_server_processing.php" + retPrint.replace(/\//g, "_");
}

function AbrirBoxSinistro(id, tipo) {
    abrirTelaBox("./../Seguro/FormSinistros.php?tipo=" + tipo + (id > 0 ? "&id=" + id : ""), 668, 800);
}

function FecharBoxSinistros() {
    $("#newbox").remove();
    tbListaSinistro.fnReloadAjax(finalFindSinistro(0));
    if ($("#tblPlanosClientes").length > 0) {
        AtualizaListPlanosClientes();
    }
    FecharBoxVistorias();
}

var tbListaSinistro = $('#tbListaSinistro').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bFilter": false,
    "ordering": false,
    "sAjaxSource": finalFindSinistro(0),
    "fnDrawCallback": function (data) {
        $.getJSON(finalFindSinistro(0) + "&getEstatisticas=S", function(json) {
            $.each(json, function(key, val) {
                $("#lbl_" + key).html(val);
            });
        });
    },
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});