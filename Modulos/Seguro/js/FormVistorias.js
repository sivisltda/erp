$("#txtPlaca").mask("AAA-9A99");

function addItem(id, name, check) {
    let item = `<div class="item-ckb">
    <input id="ckb_` + id + `" name="itemsGrupo[]" value="` + id + `" type="checkbox" class="input-medium" ` + (check ? `checked` : ``) + ` ` + 
    ($("#txtPlaca").prop("disabled") ? `disabled` : ``) + `/><label style="line-height: 20px; margin-left: 3px;" for="ckb_` + id + `">` + name + `</label></div>`;
    $("#txtItens").append(item);
}

function validaForm() {
    if ($("#txtPlaca").val() === "") {
        bootbox.alert("Placa é Obrigatória!");
        return false;
    }
    if (!$.isNumeric($("#txtIdVeiculo").val())) {
        bootbox.alert("Veículo é Obrigatório!");
        return false;
    }
    if ($("#txtVendaItem").val() !== "null" && $("#txtFuncionario").val() === "null") {
        bootbox.alert("Selecione um Vistoriador!");
        return false;
    }    
    if ($("#txtVendaItem").val() === "null" && $("#txtFuncionario").val() !== "null") {
        bootbox.alert("Selecione um Pagamento de Serviço para este Vistoriador!");
        return false;
    }    
    if ($("#txtTipo").val() === "null") {
        bootbox.alert("Selecione um Tipo de Vistoria!");
        return false;
    }    
}

function validaFormImg() {
    if ($("#txtImgSel").val() === "") {
        bootbox.alert("Selecione uma Imagem!");
        return false;
    }
    if ($("#file").val() === "") {
        bootbox.alert("Selecione uma Arquivo!");
        return false;
    }
}

function validaFormImgDanos() {
    if ($("#txtDanos").val() === "") {
        bootbox.alert("Preencha uma Observação!");
        return false;
    }
    if ($("#fileDanos").val() === "") {
        bootbox.alert("Selecione uma Arquivo!");
        return false;
    }
}

function getItens() {
    $("#txtItens").html("");
    $.getJSON('form/FormVistoriasServer.php?isAjax=S&getItens=S', 
    {id: $("#txtId").val(), tipo: $("#txtVeiculoTipo").val()}, function (json) {
        if (json !== null) {
            for (var x = 0; x < json.length; x++) {
                addItem(json[x].id, json[x].name, json[x].check);
            }
        }
    });
}

function getPagamentosVistorias(id_pessoa) {
    $.getJSON("form/FormVistoriasServer.php", {isAjax: "S", getPagamentos: id_pessoa}, function (json) {
        let options = "<option value=\"null\">Selecione</option>";
        if (json !== null) {
            for (var x = 0; x < json.length; x++) {
                options += "<option value=\"" + json[x].Value + "\">" + json[x].Label + "</option>";
            }
        }
        $("#txtVendaItem").html(options);
    });    
}

function removerImg(img, id) {
    bootbox.confirm('Confirma a exclusão da imagem?', function (result) {
        if (result === true) {
            $.post("form/FormVistoriasServer.php", "isAjax=S&RemoverImg=" + img + "&txtId=" + $("#txtId").val()).done(function (data) {
                if (data.trim() === "YES") {
                    $("#img" + id).attr("src", "./../../img/noimage.png");
                    $("#del" + id).hide();
                } else {
                    bootbox.alert("Não foi possível excluir esta imagem!");
                }
            });
        }
    });
}

function removerImgDanos(img, id) {
    bootbox.confirm('Confirma a exclusão da imagem?', function (result) {
        if (result === true) {
            $.post("form/FormVistoriasServer.php", "isAjax=S&RemoverImgDanos=" + img + 
                "&txtId=" + $("#txtId").val() + "&txtIdDano=" + id).done(function (data) {
                if (data.trim() === "YES") {
                    location.reload();
                } else {
                    bootbox.alert("Não foi possível excluir esta imagem!");
                }
            });
        }
    });
}

$('.img-car').dblclick(function () {
    if ($(this).attr("data") !== "./../../img/noimage.png") {
        let img = $(this).attr("data").split("/");
        let imgT = img[img.length - 1].split(".");
        if (imgT[1] === "3gp" || imgT[1] === "mp4") {
            window.open($(this).attr("data"), '_blank');
        } else {
            window.open("./../Seguro/Galeria.php?id=" + $("#txtId").val() +
            "&img=" + img[img.length - 1] + "&loja=" + parent.$("#txtMnContrato").val(), '_blank');
        }
    }    
});

$('.img-car').click(function () {
    if ($('#txtReadOnly').val() === "0") {
        $('.img-car').css("border-color", "#ddd");        
        if ($.isNumeric($("#txtId").val())) {
            $(this).css("border-color", "limegreen");
            if ($(this).attr("id") === "img99") {
                $("#fileDanos").click();        
            } else if ($(this).attr("id") && $(this).attr("src") === "./../../img/noimage.png") {
                $("#txtImgSel").val($(this).attr("id"));
                $("#file").click();
            }
        } else {
            bootbox.alert("É necessário salvar o registro antes de enviar a imagem!");
        }
    }
});

$('input[name=file]').change(function(ev) {    
    //$("#bntSendFile").attr("disabled", ($(this).val() === ""));    
    if ($(this).val() !== "") {
        $("#bntSendFile").click();    
    }
});

$('input[name=fileDanos]').change(function(ev) {
    $("#bntSendFileDanos").attr("disabled", ($(this).val() === ""));
});

$('#txtPlaca').blur(function () {
    $.getJSON('form/FormVistoriasServer.php?isAjax=S', {getPlaca: $("#txtPlaca").val()}, function (json) {
        if (json !== null && json.length > 0) {
            $("#txtIdVeiculo").val(json[0].id);
            $("#txtVeiculoTipo").val(json[0].tipo_veiculo);
            $("#txtInfo").html(`<p>` + json[0].veiculo + `</p><p>` + json[0].proprietario + `</p>`);
            getItens();
            getPagamentosVistorias(json[0].id_pessoa);
            getVistoriaTipo(json[0].tipo_veiculo);
        } else {
            $("#txtIdVeiculo").val("");
            $("#txtVeiculoTipo").val("");
            $("#txtInfo").html("<p></p><p></p>");            
            $("#txtVendaItem").html("<option value=\"null\">Selecione</option>");
            $("#txtTipo").html("<option value=\"null\">Selecione</option>");            
        }        
    });    
});   

function getVistoriaTipo(id_tipo) {
    $.getJSON("form/FormVistoriasServer.php", {isAjax: "S", getVistoriaTipo: id_tipo}, function (json) {
        let options = "<option value=\"null\">Selecione</option>";
        if (json !== null) {
            for (var x = 0; x < json.length; x++) {
                options += "<option value=\"" + json[x].id + "\">" + json[x].descricao + "</option>";
            }
        }
        $("#txtTipo").html(options);
    });    
}            
            
getItens();
