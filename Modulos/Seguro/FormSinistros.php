<?php
include "../../Connections/configini.php";

$disabled = 'disabled';
$DateBegin = getData("B");
$DateEnd = getData("E");

if (isset($_POST['bntSave'])) {
    if (is_numeric($_POST['txtId'])) {        
        $queryZ = "";
        if (in_array($_POST['txtPendente'], ["0", "2", "3", "4"]) && valoresSelect('txtEraPendente') == "1") {
            $queryZ = "[data_tele_fechamento] = getDate()," .
                      "[de_pessoa_fechamento] = " . $_SESSION["id_usuario"] . "," .
                      "[obs_fechamento] = " . valoresTexto('txtObsFechamento') . ",";
        }
        $query = "SET DATEFORMAT DMY;" .
                "UPDATE [sf_telemarketing] SET " .
                "[atendimento_servico] = 2," . $queryZ .
                "[pessoa] = " . valoresNumericos("txtIdPessoa") . "," .
                "[id_veiculo] = " . valoresNumericos("txtIdVeiculo") . "," .
                "[id_especificacao] = " . valoresSelect('txtEspecificacao') . "," .
                "[data_tele] = " . valoresDataHora("txtData", "txtHora") . "," .
                "[de_pessoa] = " . valoresTexto("txtDe") . "," .
                "[para_pessoa] = " . valoresTexto("txtPara") . "," .
                "[proximo_contato] = " . valoresDataHora("txtDataProximo", "txtHoraProximo") . "," .
                "[relato] = " . valoresTexto("txtRelato") . "," .
                "[garantia] = " . valoresCheck("ckbGarantia") . "," .
                "[nomecon] = '', [departamento] = null, [acompanhar] = 0," . 
                "[obs] = " . valoresTexto("txtObs") . "," . 
                "[tipo_proprietario] = " . valoresSelect("txtResponsavel") . "," . 
                "[proprietario] = " . (valoresSelect("txtResponsavel") == "0" ? "null" : valoresNumericos("txtIdAssociado")) . "," .
                "[especificacao] = 'O'," .
                "[nota_fiscal] = " . valoresTexto("txtNotaFiscal") . "," .
                "[data_nf] = " . valoresData("txtDataNota") . "," .
                "[pendente] = " . valoresSelect('txtPendente') . "," .
                "[cep] = " . valoresTexto('txtCep') . "," .
                "[endereco] = " . valoresTexto('txtEndereco') . "," .
                "[numero] = " . valoresTexto('txtNumero') . "," .
                "[bairro] = " . valoresTexto('txtBairro') . "," .
                "[estado] = " . valoresSelect('txtEstado') . "," .
                "[cidade] = " . valoresSelect('txtCidade') . "," .
                "[complemento] = " . valoresTexto('txtComplemento') . " " .
                "WHERE id_tele = " . $_POST['txtId'];
        //echo $query; exit;
        odbc_exec($con, $query) or die(odbc_errormsg());        
    } else {
        $datF = '';
        $pesF = '';
        $ObsF = '';
        if (in_array($_POST['txtPendente'], ["0", "2", "3", "4"])) {
            $datF = 'GetDate()';
            $pesF = $_SESSION["id_usuario"];
            $ObsF = utf8_decode($_POST['txtObsFechamento']);
        }        
        $query = "SET DATEFORMAT DMY;
        INSERT INTO [sf_telemarketing] ([atendimento_servico],[status],
        [pessoa],[id_veiculo],[id_especificacao],[data_tele],[especificacao],
        [de_pessoa],[para_pessoa],[proximo_contato],[obs], [tipo_proprietario], 
        [proprietario],[relato],[acompanhar], [garantia],[nomecon],[departamento],[nota_fiscal],[data_nf], 
        [cep], [endereco], [numero], [bairro], [estado], [cidade], [complemento],[pendente],
        [dt_inclusao],[data_tele_fechamento],[de_pessoa_fechamento],[obs_fechamento])VALUES(2,'Aguarda'," .
            valoresNumericos("txtIdPessoa") . "," .
            valoresNumericos("txtIdVeiculo") . "," .
            valoresSelect('txtEspecificacao') . "," .                
            valoresDataHora("txtData", "txtHora") . ",'O'," .
            valoresTexto("txtDe") . "," .
            valoresSelect("txtPara") . "," .
            valoresDataHora("txtDataProximo", "txtHoraProximo") . "," . 
            valoresTexto("txtObs") . "," .
            valoresSelect("txtResponsavel") . "," . 
            valoresNumericos("txtIdAssociado") . "," .                
            valoresTexto("txtRelato") . ",0," .
            valoresCheck("ckbGarantia") . ",'',null," .
            valoresTexto("txtNotaFiscal") . "," .
            valoresData("txtDataNota") . "," .
            valoresTexto('txtCep') . "," .
            valoresTexto('txtEndereco') . "," .
            valoresTexto('txtNumero') . "," .
            valoresTexto('txtBairro') . "," .
            valoresSelect('txtEstado') . "," .
            valoresSelect('txtCidade') . "," .
            valoresTexto('txtComplemento') . "," .
            valoresSelect('txtPendente') . "," .                
            "getdate()," . valoresSelect2($datF) . "," . 
            valoresSelect2($pesF) . "," . 
            valoresTexto2($ObsF) . ")";
        //echo $query; exit;        
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_tele from sf_telemarketing order by id_tele desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }    
}

if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        odbc_exec($con, "insert into sf_logs values('sf_telemarketing'," . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "','R', 'EXCLUSAO EVENTO - " . $_POST['txtId'] . "', getdate(), (select max(pessoa) from sf_telemarketing where id_tele = " . $_POST['txtId'] . "))");      
        odbc_exec($con, "DELETE FROM sf_telemarketing_item WHERE id_telemarketing = " . $_POST['txtId']);
        odbc_exec($con, "DELETE FROM sf_mensagens_telemarketing WHERE id_tele = " . $_POST['txtId']);
        odbc_exec($con, "DELETE FROM sf_telemarketing WHERE id_tele = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox(2);</script>";
    }    
} 

if (isset($_POST['bntAprov'])) {
    if (is_numeric($_POST['txtId'])) {
        $query = "update sf_telemarketing set status = 'Aprovado', data_aprov = getdate() where status in ('Aguarda', 'Sindicancia') and para_pessoa = '" . $_SESSION["id_usuario"] . "' and id_tele in (" . $_POST['txtId'] . ")";
        odbc_exec($con, $query) or die(odbc_errormsg());
    }
}

if (isset($_POST['bntReprov'])) {
    if (is_numeric($_POST['txtId'])) {    
        $query = "update sf_telemarketing set status = 'Reprovado', data_aprov = getdate() where status in ('Aguarda', 'Sindicancia') and para_pessoa = '" . $_SESSION["id_usuario"] . "' and id_tele in (" . $_POST['txtId'] . ")";
        odbc_exec($con, $query) or die(odbc_errormsg());
    }
}

if (isset($_POST['bntDesfazer'])) {
    if (is_numeric($_POST['txtId'])) {    
        $query = "update sf_telemarketing set status = 'Aguarda', data_aprov = null where status in ('Reprovado') and para_pessoa = '" . $_SESSION["id_usuario"] . "' and id_tele in (" . $_POST['txtId'] . ")";
        odbc_exec($con, $query) or die(odbc_errormsg());
    }
}

if (isset($_POST['bntSindic'])) {
    if (is_numeric($_POST['txtId'])) {    
        $query = "update sf_telemarketing set status = 'Sindicancia', data_aprov = getdate() where status in ('Aguarda', 'Sindicancia') and para_pessoa = '" . $_SESSION["id_usuario"] . "' and id_tele in (" . $_POST['txtId'] . ")";
        odbc_exec($con, $query) or die(odbc_errormsg());
    }
}

if (isset($_POST['bntSendFile']) && is_numeric($_POST['txtIdVistoria'])) {
    $allowedExts = array("gif", "jpeg", "jpg", "png", "pdf", "mp4", "3gp", "GIF", "JPEG", "JPG", "PNG", "PDF", "MP4", "3GP");
    $extension = explode(".", $_FILES["fileVistoria"]["name"]);
    $extension = $extension[count($extension) - 1];
    if ((($_FILES["fileVistoria"]["type"] == "image/gif") || ($_FILES["fileVistoria"]["type"] == "image/jpeg") || ($_FILES["fileVistoria"]["type"] == "image/jpg") || ($_FILES["fileVistoria"]["type"] == "image/pjpeg") || ($_FILES["fileVistoria"]["type"] == "image/x-png") || ($_FILES["fileVistoria"]["type"] == "image/png") 
    || ($_FILES["fileVistoria"]["type"] == "application/pdf") || ($_FILES["fileVistoria"]["type"] == "video/mp4") || ($_FILES["fileVistoria"]["type"] == "video/3gpp")) && ($_FILES["fileVistoria"]["size"] < 10000000) && in_array($extension, $allowedExts)) {
        if ($_FILES["fileVistoria"]["error"] == 0) {
            $pathDir = "./../../Pessoas/" . $contrato . "/Vistorias/" . $_POST['txtIdVistoria'];
            if (!file_exists($pathDir)) {
                mkdir($pathDir, 0777, true);
            }            
            move_uploaded_file($_FILES["fileVistoria"]["tmp_name"], $pathDir . "/" . $_POST['txtIdVistoria'] . "_" . $_POST['txtImgSel'] . "." . $extension);
        }
    } else {
        echo "<script type='text/javascript'>alert('Erro ao enviar o arquivo.\\nTamanho máximo permitido: 10 mb.\\nFormatos aceitos: gif, jpeg, jpg, png, mp4, 3gp e pdf.');</script>";
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select t.*,v.id, tipo_veiculo, marca, modelo, ano_modelo, ano_fab, combustivel, valor, cnpj, razao_social, id_prod_plano, 
    id_plano, p.id_fornecedores_despesas, renavam, chassi, nome_produto_cor, vt.descricao tipo_veiculo_descricao, placa,
    us.login_user login_user_fechamento, (select count(id_turma) from sf_fornecedores_despesas_turmas ft where ft.id_tele = t.id_tele) total_manutencao,
    (select top 1 razao_social from sf_fornecedores_despesas ps where ps.id_fornecedores_despesas = t.proprietario) nome_proprietario, p.fornecedores_status
    from sf_telemarketing t left join sf_fornecedores_despesas_veiculo v on v.id = t.id_veiculo
    left join sf_vendas_planos vp on vp.id_veiculo = v.id and vp.dt_cancelamento is null
    inner join sf_fornecedores_despesas p on p.id_fornecedores_despesas = t.pessoa
    LEFT JOIN sf_usuarios us on us.id_usuario = t.de_pessoa_fechamento    
    left join sf_produtos_cor c on c.id_produto_cor = v.cor
    left join sf_veiculo_tipos vt on vt.id = v.tipo_veiculo
    where id_tele = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_tele']);
        $idPessoa = utf8_encode($RFP['pessoa']);
        $idVeiculo = utf8_encode($RFP['id_veiculo']);
        $idPlano = utf8_encode($RFP['id_prod_plano']);
        $idPlanoCli = utf8_encode($RFP['id_plano']);
        $placa = utf8_encode($RFP['placa']);
        $nome = utf8_encode($RFP['razao_social']);
        $cpf = utf8_encode($RFP['cnpj']);
        $responsavel = utf8_encode($RFP['tipo_proprietario']);
        $idAssociado = utf8_encode($RFP['proprietario']);     
        $nomeAssociado = utf8_encode($RFP['nome_proprietario']);
        $tipo = utf8_encode($RFP['tipo_veiculo_descricao']);
        $marca = utf8_encode($RFP['marca']);
        $ano_modelo = utf8_encode($RFP['ano_modelo']);
        $ano_fab = utf8_encode($RFP['ano_fab']);
        $modelo = utf8_encode($RFP['modelo']);
        $renavam = utf8_encode($RFP['renavam']);
        $chassi = utf8_encode($RFP['chassi']);
        $cor = utf8_encode($RFP['nome_produto_cor']);
        $id_especificacao = utf8_encode($RFP['id_especificacao']);        
        $de = utf8_encode($RFP['de_pessoa']);
        $data = escreverData($RFP['data_tele']);
        $hora = escreverData($RFP['data_tele'], 'H:i');
        $relato = utf8_encode($RFP['relato']);
        $obs = utf8_encode($RFP['obs']);
        $departamento = utf8_encode($RFP['departamento']);
        $para = utf8_encode($RFP['para_pessoa']);
        $dataProximo = escreverData($RFP['proximo_contato']);
        $horaProximo = escreverData($RFP['proximo_contato'], 'H:i');
        $nota_fiscal = utf8_encode($RFP['nota_fiscal']);
        $data_nota = escreverData($RFP['data_nf']);
        $ckb_garantia = utf8_encode($RFP['garantia']);
        $ckbAcompanhar = utf8_encode($RFP['acompanhar']);
        $tipoChamado = utf8_encode($RFP['especificacao']);
        $cep = utf8_encode($RFP['cep']);
        $endereco = utf8_encode($RFP['endereco']);
        $numero = utf8_encode($RFP['numero']);
        $bairro = strtoupper(utf8_encode($RFP['bairro']));
        $estado = utf8_encode($RFP['estado']);
        $cidade = utf8_encode($RFP['cidade']);
        $complemento = utf8_encode($RFP['complemento']);        
        $operadorFechamento = utf8_encode($RFP['login_user_fechamento']);
        $dataFechamento = escreverData($RFP['data_tele_fechamento']);
        $horaFechamento = escreverData($RFP['data_tele_fechamento'], 'H:i');
        $obsFechamento = utf8_encode($RFP['obs_fechamento']);
        $status_save = utf8_encode($RFP['status']);         
        $pendente = utf8_encode($RFP['pendente']);
        $total_manutencao = utf8_encode($RFP['total_manutencao']);
        $statusAluno = utf8_encode($RFP['fornecedores_status']);    
    }    
} else {     
    $disabled = '';
    $id = '';
    $idPessoa = '';
    $idVeiculo = '';
    $idPlano = '';
    $idPlanoCli = '';
    $placa = '';
    $nome = '';
    $cpf = '';
    $responsavel = '0';
    $idAssociado = '';
    $nomeAssociado = '';    
    $tipo = '';
    $marca = '';
    $ano_modelo = '';
    $ano_fab = '';
    $modelo = '';
    $renavam = '';
    $chassi = '';
    $cor = '';
    $id_especificacao = "";    
    $de = $_SESSION["id_usuario"];
    $data = getData("T");
    $hora = date("H:i");
    $relato = '';
    $obs = '';
    $departamento = '';
    $para = "null";
    $dataProximo = getData("T");
    $horaProximo = date("H:i");
    $nota_fiscal = '';
    $data_nota = '';
    $ckb_garantia = '0';
    $ckbAcompanhar = 0;
    $tipoChamado = "T";
    $cep = "";
    $endereco = "";
    $numero = "";
    $bairro = "";
    $estado = "";
    $cidade = "";
    $complemento = "";    
    $status_save = "Aguarda";    
    $pendente = 1;
    $total_manutencao = 0;
    $statusAluno = "";
}

if ($pendente == 1) {
    $operadorFechamento = $_SESSION["login_usuario"];
    $dataFechamento = getData("T");
    $horaFechamento = date("H:i");
    $obsFechamento = "";    
}

$status = "";    
$color = "";    
if ($status_save == "Aguarda") {
    $status = "Aguardando";        
    $color = $color . "#bc9f00";
} else if ($status_save == "Reprovado") {
    $status = "Reprovado";
    $color = $color . "#dc5039";
} else if ($status_save == "Sindicancia") {
    $status = "Sindicância";
    $color = $color . "#e09f4f";
} else if ($status_save == "Aprovado" && $pendente == 1 && $total_manutencao == 0) {
    $status = "Em Aberto";
    $color = $color . "#0066cc";
} else if ($status_save == "Aprovado" && $pendente == 1 && $total_manutencao > 0) {
    $status = "Em Reparo";
    $color = $color . "black";
} else if ($status_save == "Aprovado" && $pendente == 0) {
    $status = "Finalizado";        
    $color = $color . "#68AF27";
} else if ($status_save == "Aprovado" && $pendente == 2) {
    $status = "Cancelado";
    $color = $color . "#dc5039";
} else if ($status_save == "Aprovado" && $pendente == 3) {
    $status = "Sub-Rogação";
    $color = $color . "black";
} else if ($status_save == "Aprovado" && $pendente == 4) {
    $status = "Indeferido";
    $color = $color . "#dc5039";
}

if ((isset($_POST['bntEdit']) && $_POST['txtId'] != '') || $_GET['tipo'] > 0) {
    $disabled = '';    
}
?>
<!DOCTYPE html>
<head>
    <link rel="icon" type="image/ico" href="../../../favicon.ico"/>
    <link href="../../../css/stylesheets.css" rel="stylesheet" type="text/css" />
    <link href="../../../css/stylesheet.css" rel="stylesheet" type="text/css" />
    <link href="../../../css/main.css" rel="stylesheet">
    <link href="../../../css/bootbox.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-1.9.1.min.js"></script> 
    <style>
        .table th {
            line-height: 30px;
            background: #E9E9E9;
            padding: 0px 10px;
            margin: 0px;
            font-size: 11px;
            color: #666;
        }
        .table th, .table td {
            padding: 4px;
            line-height: 20px;            
        }               
        table td #formPQ {
            height: 15px;
            overflow: hidden;
        }  
        .box {
            float: left;
            margin-right: 2%;
            margin-bottom: 2%;         
            width: 23%;            
        }
        .img-car {
            border: 1px solid #ddd;
            height: 50px;            
            width: 100%;                
        }        
        .img-car-master {
            margin-top: 3.5%;
            margin-bottom: 2%;            
            height: 200px;            
            width: 98%;
        }
        .left-imgs {
            float: left; 
            width: 34%; 
            overflow: hidden; 
        }
        .item-ckb {
            float:left; 
            width:24%;
            margin-left: 1%;
            display: flex;
        }
        .deleteImg {
            position: absolute; 
            margin: 0px; 
            padding: 2px 4px 3px; 
            line-height: 10px; 
            cursor: pointer;
        }        
    </style>
</head>
<body>
    <div class="row-fluid">
        <form id="frmSinistros" name="frmSinistros" action="FormSinistros.php" method="POST" enctype="multipart/form-data">
            <div class="frmhead">
                <div class="frmtext">Evento</div>
                <div class="frmicon" style="top:13px" onClick="parent.FecharBoxSinistros()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <input id="txtId" name="txtId" value="<?php echo $id; ?>" type="hidden"/>                 
            <input id="txtIdPessoa" name="txtIdPessoa" value="<?php echo $idPessoa; ?>" type="hidden"/>                      
            <input id="txtIdVeiculo" name="txtIdVeiculo" value="<?php echo $idVeiculo; ?>" type="hidden"/>                        
            <input id="txtEraPendente" name="txtEraPendente" value="<?php echo $pendente; ?>" type="hidden"/>                        
            <input id="txtPlano" name="txtPlano" value="<?php echo $idPlano; ?>" type="hidden"/>            
            <input id="txtPlanoCli" name="txtPlanoCli" value="<?php echo $idPlanoCli; ?>" type="hidden"/>            
            <input id="txtDe" name="txtDe" value="<?php echo $de; ?>" type="hidden"/>            
            <div style="float: left; width: 99%; margin-left: 1%;">                
                <div class="tabbable frmtabs" style="padding-left: 0px;">
                    <ul class="nav nav-tabs" style="margin:0">
                        <li class="<?php echo ($_GET['tipo'] == 0 && !isset($_POST['bntSendFile']) ? "active" : ""); ?>"><a href="#tab1" data-toggle="tab">Ocorrência</a></li>
                        <li class=""><a href="#tab8" data-toggle="tab">Documentos</a></li>                        
                        <li class=""><a href="#tab7" data-toggle="tab">Observação</a></li>                        
                        <?php if (is_numeric($id)) { ?>
                        <li class="<?php echo (isset($_POST['bntSendFile']) ? "active" : ""); ?>"><a href="#tab2" data-toggle="tab">Danos</a></li>                        
                        <li class=""><a href="#tab3" data-toggle="tab">Acompanhamento</a></li>
                        <li class=""><a href="#tab9" data-toggle="tab">Previsão</a></li>                        
                        <li class=""><a href="#tab4" data-toggle="tab">Compras</a></li>
                        <li class="<?php echo ($_GET['tipo'] == 1 && !isset($_POST['bntSendFile']) ? "active" : ""); ?>"><a href="#tab5" data-toggle="tab">Histórico</a></li>
                        <li class="<?php echo ($_GET['tipo'] == 2 && !isset($_POST['bntSendFile']) ? "active" : ""); ?>"><a href="#tab6" data-toggle="tab">Fechamento</a></li>
                        <?php } ?>
                    </ul>
                    <div class="tab-content frmcont" style="height:480px; font-size:11px !important; overflow: unset;">
                        <div class="tab-pane <?php echo ($_GET['tipo'] == 0 && !isset($_POST['bntSendFile']) ? "active" : ""); ?>" id="tab1">
                            <div style="width:12%; float:left;">
                                <span>Placa:</span>
                                <input id="txtPlaca" name="txtPlaca" class="input-xlarge" maxlength="8" type="text" style="min-height: 28px;" value="<?php echo $placa; ?>" <?php echo $disabled; ?>/>
                            </div>
                            <div style="width:20%; float:left; margin-left: 1%;">
                                <span>Montadora:</span>
                                <input id="txtMontadora" name="txtMontadora" class="input-xlarge" maxlength="8" type="text" style="min-height: 28px;" value="<?php echo $marca; ?>" disabled/>
                            </div>                                                                 
                            <div style="width:37%; float:left; margin-left: 1%;">
                                <span>Modelo:</span>
                                <input id="txtModelo" name="txtModelo" class="input-xlarge" maxlength="8" type="text" style="min-height: 28px;" value="<?php echo $modelo; ?>" disabled/>
                            </div>
                            <div style="width:12%; float:left; margin-left: 1%;">
                                <span>Cor:</span>
                                <input id="txtCor" name="txtCor" class="input-xlarge" maxlength="8" type="text" style="min-height: 28px;" value="<?php echo $cor; ?>" disabled/>
                            </div>                            
                            <div style="width:7%; float:left; margin-left:1%">
                                <span>Ano mod:</span>
                                <input id="txtAnoMod" name="txtAnoMod" class="input-xlarge" maxlength="4" type="text" style="min-height: 28px;" value="<?php echo $ano_modelo; ?>" disabled="">
                            </div>
                            <div style="width:7%; float:left; margin-left:1%">
                                <span>Ano Fab:</span>
                                <input id="txtAnoFab" name="txtAnoFab" class="input-xlarge" maxlength="4" type="text" style="min-height: 28px;" value="<?php echo $ano_fab; ?>" disabled="">
                            </div>
                            <div style="width:12%; float:left;">
                                <span>Tipo:</span>
                                <input id="txtTipo" name="txtTipo" class="input-xlarge" maxlength="8" type="text" style="min-height: 28px;" value="<?php echo $tipo; ?>" disabled/>
                            </div>                            
                            <div style="width:33%; float:left; margin-left: 1%;">
                                <span>Nome:</span>
                                <input id="txtNome" name="txtNome" class="input-xlarge" maxlength="8" type="text" style="min-height: 28px;" value="<?php echo $nome; ?>" disabled/>
                            </div>                                                                 
                            <div style="width:16%; float:left; margin-left: 1%;">
                                <span>CPF:</span>
                                <input id="txtCPF" name="txtCPF" class="input-xlarge" maxlength="8" type="text" style="min-height: 28px;" value="<?php echo $cpf; ?>" disabled/>
                            </div>                            
                            <div style="width:15%; float:left; margin-left: 1%;">
                                <span>RENAVAM:</span>
                                <input id="txtRenavam" name="txtRenavam" class="input-xlarge" maxlength="8" type="text" style="min-height: 28px;" value="<?php echo $renavam; ?>" disabled/>
                            </div>                                                                 
                            <div style="width:20%; float:left; margin-left: 1%;">
                                <span>CHASSI:</span>
                                <input id="txtChassi" name="txtChassi" class="input-xlarge" maxlength="8" type="text" style="min-height: 28px;" value="<?php echo $chassi; ?>" disabled/>
                            </div>
                            <div style="width:17%; float:left;">
                                <span>Proprietário do veículo:</span>
                                <select class="select" id="txtResponsavel" name="txtResponsavel" style="width:100%" <?php echo $disabled; ?>>
                                    <option value="0" <?php echo ($responsavel == "0" ? "selected" : ""); ?>>Associado</option>
                                    <option value="1" <?php echo ($responsavel == "1" ? "selected" : ""); ?>>Terceiro</option>
                                </select>
                            </div>
                            <div style="width:45%; float:left; margin-left: 1%; <?php echo ($responsavel == "0" ? "display: none;" : ""); ?>">
                                <span>Nome do Associado:</span>
                                <input id="txtIdAssociado" name="txtIdAssociado" value="<?php echo $idAssociado; ?>" type="hidden"/>                                
                                <input id="txtNomeAssociado" name="txtNomeAssociado" class="input-xlarge" maxlength="8" type="text" style="min-height: 28px;" value="<?php echo $nomeAssociado; ?>" <?php echo $disabled; ?>/>
                            </div> 
                            <div id="statusCli" style="float:right;text-align: right;">
                                <div class="button label-<?php echo $statusAluno; ?>" style="width:132px; margin-top: 10px;">
                                    <div class="name"><center><?php echo strpos($statusAluno, 'Cred') ? 'ATIVO' : ($statusAluno == 'Ferias' ? 'CONGELADO' : ($statusAluno == 'AtivoEmAberto' ? 'ATIVO EM ABERTO' : ($statusAluno == 'AtivoAbono' ? 'ATIVO ABONO' : ($statusAluno == 'DesligadoEmAberto' ? 'DESLIGADO EM ABERTO' : strtoupper($statusAluno))))); ?></center></div>
                                </div>
                            </div>
                            <div style="width:100%; float:left;"><hr style="margin: 12px 0px 8px;"></div>
                            <div style="width:14%; float:left;">
                                <span>Aviso de Evento:</span>
                                <input id="txtData" name="txtData" class="datepicker inputCenter" type="text" value="<?php echo $data; ?>" <?php echo $disabled; ?>>
                            </div>
                            <div style="width:14%; float:left; margin-left:1%;">
                                <span>Hora da Ocorrência:</span>
                                <input id="txtHora" name="txtHora" type="text" class="input-medium" value="<?php echo $hora; ?>" <?php echo $disabled; ?>>
                            </div>
                            <div style="width:70%; float:left; margin-left:1%">
                                <span>Ação do Evento:</span>
                                <select id="txtEspecificacao" name="txtEspecificacao" class="select" style="width:100%" <?php echo $disabled; ?>>
                                    <option value="null">Selecione</option>
                                    <?php $cur = odbc_exec($con, "select id_especificacao, nome_especificacao from sf_telemarketing_especificacao order by nome_especificacao") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                        <option value="<?php echo $RFP['id_especificacao'] ?>"<?php
                                        if (!(strcmp($RFP['id_especificacao'], $id_especificacao))) {
                                            echo "SELECTED";
                                        } ?>><?php echo utf8_encode($RFP['nome_especificacao']);?></option>
                                    <?php } ?>
                                </select>
                            </div>                                
                            <div style="width:100%; float:left;">
                                <span>Preencha o endereço da ocorrência:</span>
                                <hr style="margin: 0px;">
                            </div>
                            <div style="width:16%; float:left">
                                <span>CEP:</span>
                                <input type="text" id="txtCep" name="txtCep" class="input-medium" value="<?php echo $cep; ?>" <?php echo $disabled; ?>/>
                            </div>
                            <div style="width:41%; float:left; margin-left:1%">
                                <span>Logradouro:</span>
                                <input type="text" id="txtEndereco" name="txtEndereco" maxlength="256" class="input-medium" value="<?php echo $endereco; ?>" readonly <?php echo $disabled; ?>/>
                            </div>
                            <div style="width:10%; float:left; margin-left:1%">
                                <span>Número:</span>
                                <input type="text" id="txtNumero" name="txtNumero" maxlength="6" class="input-medium" value="<?php echo $numero; ?>" <?php echo $disabled; ?>/>
                            </div>
                            <div style="width:30%; float:left; margin-left:1%">
                                <span>Bairro:</span>
                                <input type="text" id="txtBairro" name="txtBairro" maxlength="40" class="input-medium" value="<?php echo $bairro; ?>" readonly <?php echo $disabled; ?>/>
                            </div>
                            <div style="clear:both; height:5px"></div>
                            <div style="width:28.5%; float:left; ">
                                <span>Estado:</span>
                                <select class="select" id="txtEstado" name="txtEstado" style="width:100%" readonly <?php echo $disabled; ?>>
                                    <option uf="" value="">Selecione o estado</option>
                                    <?php $cur = odbc_exec($con, "select estado_codigo,estado_nome,estado_sigla from tb_estados where estado_codigo > 0 order by estado_nome") or die(odbc_errormsg());
                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                        <option uf="<?php echo strtoupper(utf8_encode($RFP['estado_sigla'])); ?>" value="<?php echo $RFP['estado_codigo'] ?>"<?php
                                        if (!(strcmp($RFP['estado_codigo'], $estado))) {
                                            echo "SELECTED";
                                        }
                                        ?>><?php echo strtoupper(utf8_encode($RFP['estado_nome'])); ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div style="width:28.5%; float:left;margin-left:1%">
                                <span>Cidade:</span>
                                <div id="carregando" style=" margin-top: 10px;;display:none">
                                    <span style="color:#666;">Aguarde, carregando...</span>
                                </div>
                                <select class="select" id="txtCidade" name="txtCidade" style="width:100%;text-transform: uppercase" readonly <?php echo $disabled; ?>>
                                    <option value="">Selecione a cidade</option>
                                    <?php
                                    if ($estado != "") {
                                        $sql = "select cidade_codigo,cidade_nome from tb_cidades where cidade_codigo > 0 AND cidade_codigoEstado = " . $estado . " ORDER BY cidade_nome";
                                        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) {
                                            ?>
                                            <option value="<?php echo $RFP['cidade_codigo'] ?>"<?php
                                            if (!(strcmp($RFP['cidade_codigo'], $cidade))) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo strtoupper(utf8_encode($RFP['cidade_nome'])) ?>
                                            </option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div style="width:41%; float:left; margin-left:1%">
                                <span>Complemento:</span>
                                <input type="text" id="txtComplemento" name="txtComplemento" class="input-medium" value="<?php echo $complemento; ?>" <?php echo $disabled; ?>/>
                            </div>
                            <div style="width:100%; float:left;"><hr style="margin: 12px 0px 8px;"></div>
                            <div style="float: left;">
                                <div style="width:30%; float:left; margin-left:1%">
                                    <span>Boletim de Ocorrência:</span>
                                    <div style="width:100%; float:left; margin-top: 7px;">
                                        <div class="radio"><input type="radio" name="ckbGarantia" <?php echo ($ckb_garantia == "0" ? "checked" : "");?> value="0" style="opacity: 0;" <?php echo $disabled; ?>></div>SIM
                                        <div class="radio"><input type="radio" name="ckbGarantia" <?php echo ($ckb_garantia == "1" ? "checked" : "");?> value="1" style="opacity: 0;" <?php echo $disabled; ?>></div>NÂO
                                    </div>    
                                </div>                                
                                <div style="width:20%; float:left;">
                                    <span>Data:</span>
                                    <input id="txtDataNota" name="txtDataNota" class="datepicker inputCenter" type="text" value="<?php echo $data_nota; ?>" <?php echo $disabled; ?>>
                                </div>
                                <div style="width:46%; float:left; margin-left:1%;">
                                    <span>Número do Boletim:</span>
                                    <input id="txtNotaFiscal" name="txtNotaFiscal" type="text" class="input-medium" value="<?php echo $nota_fiscal; ?>"<?php echo $disabled; ?>>
                                </div>                            
                                <div style="width:20%; float:left;">
                                    <span>Data prazo:</span>
                                    <input id="txtDataProximo" name="txtDataProximo" class="datepicker inputCenter" type="text" maxlength="10" value="<?php echo $dataProximo; ?>" <?php echo $disabled; ?>>
                                </div>
                                <div style="width:20%; float:left; margin-left:1%;">
                                    <span>Hora prazo:</span>
                                    <input id="txtHoraProximo" name="txtHoraProximo" type="text" class="input-medium" maxlength="5" value="<?php echo $horaProximo; ?>" <?php echo $disabled; ?>>
                                </div>
                                <div style="width:56%;float:left; margin-left:1%; <?php echo ($_SESSION["mod_emp"] == 3 ? "display: none;" : ""); ?>">
                                    <span>Responsável</span>
                                    <select class="select" name="txtPara" id="txtPara" class="input-medium" style="width:100%" <?php echo $disabled; ?>>
                                        <option value="null">Selecione</option>
                                        <?php $cur = odbc_exec($con, "select id_usuario,login_user,nome from sf_usuarios where inativo = 0 and login_user != 'Admin' and (master = 0 or master in (select id_permissoes from sf_usuarios_permissoes where ckb_pro_apv_sin = 1)) order by nome") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP["id_usuario"] ?>"<?php
                                            if (!(strcmp($RFP["id_usuario"], $para))) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP["nome"]); ?></option>
                                        <?php } ?>
                                    </select>                                
                                </div>
                            </div>                                                        
                            <div style="clear:both; height:5px"></div>                            
                        </div>           
                        <div class="tab-pane" id="tab8">
                            <div style="float: left; width: 80%; margin-right: 8%;">
                                <label id="lblarquivo" for="arquivo">Arquivo:</label>
                                <input class="btn btn-primary" style="height:20px; line-height:21px;width: 100%;" type="file" name="file" id="file" <?php echo $disabled; ?>>
                            </div>                            
                            <div style="float:left; width:11%; margin-left:1%;">                                
                                <button type="button" id="bntAdicionaCarteira" name="bntAdicionaCarteira" onclick="AdicionarDoc(0)" class="btn btn-success" style="height:27px; padding-top:6px; margin-top: 15px;" <?php echo $disabled; ?>><span class="ico-plus"></span></button>
                            </div>                            
                            <table id="tbDocumentos" class="table" cellpadding="0" cellspacing="0" width="100%" <?php echo $disabled; ?>>
                                <thead>
                                    <tr>
                                        <th width="40%">Data</th>
                                        <th width="40%">Nome</th>
                                        <th width="20%"><center>Ação</center></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>      
                        </div>
                        <div class="tab-pane" id="tab7">
                            <div style="width:100%; float:left; height:240px;">
                                <span>Descrição da Ocorrência:</span>
                                <textarea style="width:100%; height:220px; resize:none" id="txtRelato" name="txtRelato" <?php echo $disabled; ?>><?php echo $relato; ?></textarea>                                                               
                            </div>
                            <div style="width:100%; float:left; height:240px;">
                                <span>Observação da Ocorrência:</span>
                                <textarea style="width:100%; height:220px; resize:none" id="txtObs" name="txtObs" <?php echo $disabled; ?>><?php echo $obs; ?></textarea>                                                               
                            </div>
                        </div>
                        <div class="tab-pane <?php echo (isset($_POST['bntSendFile']) ? "active" : ""); ?>" id="tab2">
                            <div style="float: left; width: 39.5%; overflow: hidden;">
                                <input id="txtImgSel" name="txtImgSel" value="" type="hidden"/>                                
                                <input id="fileVistoria" name="fileVistoria" type="file" style="display: none;"/>                                
                                <input id="txtIdVistoria" name="txtIdVistoria" value="<?php echo $_POST['txtIdVistoria'];?>" type="hidden"/>
                                <button id="bntSendFile" name="bntSendFile" class="btn btn-success" type="submit" style="display: none;" onClick="return validaFormImg();"><span class="ico-upload-alt"></span> Enviar Imagem</button>                
                                <img id="img" alt="Imagem" class="img-car img-car-master" src="./../../img/noimage.png"/>                
                                <?php for ($i = 0; $i < 16; $i++) { ?>
                                    <div class="box">
                                        <input id="add<?php echo $i; ?>" class="btn yellow deleteImg" style="display: none;" onclick="adicionarImg('<?php echo $i; ?>')" type="button" value="+">
                                        <input id="del<?php echo $i; ?>" class="btn red deleteImg" style="display: none;" onclick="removerImg('<?php echo $i; ?>')" type="button" value="x">
                                        <a href="javascript:void(0)">
                                            <img id="img<?php echo $i; ?>" alt="Imagem" class="img-car" src="./../../img/noimage.png"/>
                                        </a>
                                    </div>
                                <?php } ?>
                                <div id="imgDanos" style="min-height: 388px; display: none;">
                                    <?php 
                                    if (is_numeric($id_vistoria)) {
                                        $cur = odbc_exec($con, "select * from sf_vistorias_danos where id_vistoria = " . $id_vistoria);
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                        <a href="javascript:void(0)">
                                            <img alt="Imagem" title="<?php echo utf8_encode($RFP["observacao"]); ?>" class="img-car" src="<?php echo getImageCar($ImagensDanos, $RFP["id"], $contrato, $id_vistoria, "Danos"); ?>"/>
                                        </a>
                                    <?php }} ?>
                                </div>                
                            </div>
                            <div style="float: left; width: 59.5%; margin-left: 1%;">
                                <div style="width:100%; margin-top: 2.5%;">
                                    <div style="background:#F1F1F1; border:1px solid #DDD; padding:10px;vertical-align: top;">
                                        <div style="width:88%; float:left; margin-left:1%">
                                            <select id="txtTipoVistoria" name="txtTipoVistoria" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>                                
                                                <option value="null">Selecione</option>
                                                <?php $query = "SELECT * from sf_vistorias_tipo where sinistro = 1 and inativo = 0";
                                                $cur = odbc_exec($con, $query);
                                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                                    <option value="<?php echo utf8_encode($RFP['id']); ?>" <?php echo ($tipo == $RFP['id'] ? "SELECTED" : ""); ?>><?php echo utf8_encode($RFP['descricao']); ?></option>
                                                <?php } ?>                                
                                            </select>
                                        </div>
                                        <?php if($ckb_adm_cli_read_ == 0) { ?>
                                        <button class="button button-green btn-primary" style="margin-left: 2%; padding: 3px 10px 3px 10px;" type="button" onClick="SalvarVistoria()" <?php echo $disabled; ?>><span class="ico-file-4 icon-white"></span></button>
                                        <?php } ?>        
                                    </div>
                                </div>
                                <table class="table" style="margin-top: 15px;" cellpadding="0" cellspacing="0" width="100%" id="tbListaVistorias">
                                    <thead>
                                        <tr>
                                            <th width="25%">Data:</th>    
                                            <th width="25%">Tipo:</th>    
                                            <th width="25%">Usuário:</th>                                
                                            <th width="25%">Status:</th>            
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>                        
                        </div>
                        <div class="tab-pane" id="tab3">
                            <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblTurmas">
                                <thead>
                                    <tr>
                                        <th width="20%">Oficina</th>
                                        <th width="10%">Cap.</th>
                                        <th width="10%">Vagas</th>
                                        <th width="10%">Manut.</th>
                                        <th width="10%">Horário</th>
                                        <th width="15%">Especialidade</th>
                                        <th width="15%">Bairro</th>
                                        <th width="10%">Ação</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>       
                            <div style="clear:both; height:5px"></div>
                            <div style="width:100%;">
                                <span style="color:black; font-size:14px; text-shadow:0 1px #FFF; padding-left:5px">Histórico de manutenções do Evento</span>
                                <hr style="margin:2px 0; border-top:1px solid #DDD">
                            </div>
                            <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblTurmasAluno">
                                <thead>
                                    <tr>                                        
                                        <th width="10%">Entrada</th>                                        
                                        <th width="10%">Saída</th>
                                        <th width="25%">Oficina</th>                                        
                                        <th width="25%">Especialidade</th>                                        
                                        <th width="20%">Bairro</th>
                                        <th width="10%">Ação</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>                            
                        </div>
                        <div class="tab-pane" id="tab9">
                            <div class="span12">
                                <div style="width:10%; float:left;">
                                    <span>Data:</span>
                                    <input type="text" class="datepicker" style="width:79px; margin-bottom:1px" id="txt_dt_previsao" name="txt_dt_previsao" value="<?php echo getData("T"); ?>" placeholder="Data" <?php echo $disabled; ?>>                                        
                                </div>
                                <div style="width:31%; float:left; margin-left: 1%;">
                                    <span>Item:</span>
                                    <input id="txtItemPrev" name="txtItemPrev" class="input-xlarge" type="text" style="width:100%;" value="" <?php echo $disabled; ?>/>
                                </div>
                                <div style="width:20%; float:left; margin-left: 1%;">
                                    <span>Fornecedor:</span>
                                    <input id="txtFornecPrev" name="txtFornecPrev" class="input-xlarge" type="text" style="width:100%;" value="" <?php echo $disabled; ?>/>
                                </div>
                                <div style="width:6%; float:left; margin-left: 1%;">
                                    <span>Quant.:</span>
                                    <input id="txtQtdPrev" name="txtQtdPrev" class="input-xlarge" maxlength="2" type="text" style="width:100%;" value="1" <?php echo $disabled; ?>/>                                        
                                </div>
                                <div style="width:11%; float:left; margin-left: 1%;">
                                    <span>Val.Unit.:</span>
                                    <input id="txtValorPrev" name="txtValorPrev" class="input-xlarge" type="text" style="width:100%;" value="0,00" <?php echo $disabled; ?>/>
                                </div>
                                <div style="width:11%; float:left; margin-left: 1%;">
                                    <span>Val.Total:</span>
                                    <input id="txtTotPrev" name="txtTotPrev" class="input-xlarge" type="text" style="width:100%;" value="0,00" disabled/>
                                </div>
                                <div style="width:5%; float:left; margin-left: 1%; margin-top: 15px;">
                                    <button id="btnAddPrev" name="btnAddPrev" class="btn btn-success" type="button" title="Adicionar" <?php echo $disabled; ?>><span class="ico-plus icon-white"></span></button>
                                </div>                                                                   
                            </div>
                            <div style="clear:both;"></div>
                            <table class="table" cellpadding="0" style="margin-top: 15px;" cellspacing="0" width="100%" id="tbPrevisao">
                                <thead>
                                    <tr>    
                                        <th width="6%"><center>Data</center></th>
                                        <th width="37%">Item Previsto</th>
                                        <th width="35%">Fornecedor</th>
                                        <th width="6%">Quant.</th>
                                        <th width="6%">Val.Unit.</th>
                                        <th width="6%">Val.Total</th>
                                        <th width="4%"><center>Ação</center></th>
                                    </tr>
                                </thead>
                                <tbody>  
                                    <tr>
                                        <td colspan="7" class="dataTables_empty">Carregando dados</td>
                                    </tr>   
                                </tbody>
                            </table>
                            <table id="tbTotParcPrev" class="table" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="text-align: right">
                                        <div>
                                            Total Previsto: <strong id="lblTotParcPrev"><?php echo escreverNumero(0,1); ?></strong>
                                        </div>
                                    </td>
                                </tr>
                            </table> 
                        </div>
                        <div class="tab-pane" id="tab4">
                            <div class="span12" style="text-align: right;">      
                                <button style="float: left;" type="button" name="btnNovo" id="btnNovo" title="Novo" class="button button-green btn-primary" onClick="window.open('../../Modulos/Estoque/FormCompras.php?com=<?php echo $id; ?>', '_blank');"><span class="ico-file-4 icon-white"></span></button>                                            
                                <span style="padding-left:0px">                                           
                                    De <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial">
                                    até <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final">
                                    <button id="btnfind" name="btnfind" class="button button-turquoise btn-primary buttonX" type="button" title="Buscar"><span class="ico-search icon-white"></span></button>
                                </span>                                        
                            </div>
                            <div style="clear:both;"></div>
                            <table class="table" cellpadding="0" style="margin-top: 15px;" cellspacing="0" width="100%" id="tbCompras">
                                <thead>
                                    <tr>      
                                        <th width="10%"><center>Data</center></th>
                                        <th width="25%">Fornecedor</th>
                                        <th width="25%"><center>Histórico</center></th>
                                        <th width="10%" style="text-align:right">Tot.Produtos</th>
                                        <th width="10%" style="text-align:right">Tot.Serviços</th>
                                        <th width="10%" style="text-align:right">Tot.Geral</th>
                                        <th width="10%"><center>Status:</center></th>
                                    </tr>
                                </thead>
                                <tbody>  
                                    <tr>
                                        <td colspan="11" class="dataTables_empty">Carregando dados do Cliente</td>
                                    </tr>   
                                </tbody>
                            </table>
                            <table id="lblTotParc" name="lblTotParc" class="table" cellpadding="0" cellspacing="0" width="100%">                                            
                                <tr>
                                    <td style="text-align: right">
                                        <div id="lblTotParc2" name="lblTotParc2">
                                            Número de Compras :<strong>   0</strong>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">
                                        <div id="lblTotParc3" name="lblTotParc3">
                                            Total em Compras :<strong><?php echo escreverNumero(0,1); ?></strong>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">
                                        <div id="lblTotParc5" name="lblTotParc5">
                                            Total Previsto :<strong id="lblTotParcPrev5"><?php echo escreverNumero(0,1); ?></strong>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">
                                        <div id="lblTotParc4" name="lblTotParc4">
                                            Ticket Médio :<strong><?php echo escreverNumero(0,1); ?></strong>
                                        </div>            
                                    </td>
                                </tr>
                            </table> 
                        </div>
                        <div class="tab-pane <?php echo ($_GET['tipo'] == 1 && !isset($_POST['bntSendFile']) ? "active" : ""); ?>" id="tab5">
                            <div class="block">
                                <div class="toolbar dark" style="background:#DCDCDC">
                                    <div class="input-prepend input-append">
                                    </div>
                                    <table width="99%" border="0" cellpadding="3" class="textosComuns">
                                        <tr>
                                            <td colspan="2">
                                                <input id="txtDataHistorico" type="text" class="input-medium datepicker"  style="width:12%; text-align:center" value="<?php echo getData("T"); ?>" <?php echo $disabled; ?>/>
                                                <input id="txtHoraHistorico" type="text" class="input-medium" style="width:7%; text-align:center" value="<?php echo date("H:i"); ?>" <?php echo $disabled; ?>/>
                                                <select id="txtDeHistorico" class="input-medium" style="width:33%" <?php echo $disabled; ?>>
                                                    <?php if ($ckb_out_todos_operadores_ == 1) { ?>
                                                        <option value="null">Selecione</option>
                                                        <?php
                                                    } $cur = odbc_exec($con, "select id_usuario,login_user,nome from sf_usuarios where " . ($ckb_out_todos_operadores_ != 1 ? " id_usuario = " . $_SESSION["id_usuario"] . " AND " : "") . " inativo = 0 order by nome") or die(odbc_errormsg());
                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                        ?>
                                                        <option value="<?php echo $RFP['id_usuario'] ?>"<?php
                                                        if (!(strcmp($RFP["id_usuario"], $_SESSION["id_usuario"]))) {
                                                            echo "SELECTED";
                                                        }
                                                        ?>><?php echo utf8_encode($RFP["nome"]); ?></option>
                                                    <?php } ?>
                                                </select>
                                                <select id="txtParaHistorico" class="input-medium" style="width:33%" <?php echo $disabled; ?>>
                                                    <?php if ($ckb_out_todos_operadores_ == 1) { ?>
                                                        <option value="null">Selecione</option>
                                                        <?php
                                                    } $cur = odbc_exec($con, "select id_usuario,login_user,nome from sf_usuarios where " . ($ckb_out_todos_operadores_ != 1 ? " id_usuario = " . $_SESSION["id_usuario"] . " AND " : "") . " inativo = 0 order by nome") or die(odbc_errormsg());
                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                        ?>
                                                        <option value="<?php echo $RFP['id_usuario'] ?>"<?php
                                                        if (!(strcmp($RFP["id_usuario"], $_SESSION["id_usuario"]))) {
                                                            echo "SELECTED";
                                                        }
                                                        ?>><?php echo utf8_encode($RFP["nome"]); ?></option>
                                                    <?php } ?>
                                                </select>
                                                <select id="txtTipoHistorico" class="input-medium" style="width:13%" <?php echo $disabled; ?>>
                                                    <option value="0" SELECTED>Público</option>
                                                    <option value="1">Privado</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><textarea type="text" id="textoArea" class="input-medium" <?php echo $disabled; ?> row="2" style="width:100%"></textarea></td>
                                            <td width="80" align="right">
                                                <button type="button" onclick="addMensagens();" name="bntSaveMsg" <?php echo $disabled; ?> style="height:45px; width:92px" <?php
                                                if ($PegaURL == "") {
                                                    echo "DISABLED";
                                                }
                                                ?> class="btn dblue">Incluir <span class="icon-arrow-next icon-white"></span></button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="divMensagens" class="messages" style="height:375px; color: white; border-left:1px solid #DCDCDC; border-bottom:1px solid #DCDCDC; overflow-y:scroll"></div>
                            </div>
                        </div>
                        <div class="tab-pane <?php echo ($_GET['tipo'] == 2 && !isset($_POST['bntSendFile']) ? "active" : ""); ?>" id="tab6">
                            <table width="100%" border="0" cellpadding="3" class="textosComuns">
                                <tr>
                                    <td width="100" align="right">Operador:</td>
                                    <td width="300"><input type="text" name="txtOperadorFechamento" id="txtOperadorFechamento" class="input-medium" style="width:300px" value="<?php echo $operadorFechamento; ?>" disabled></td>
                                    <td width="100" align="right">Pendente:</td>
                                    <td><select name="txtPendente" id="txtPendente" class="input-medium" style="width:100%" <?php echo $disabled; ?>>
                                            <option value="1" <?php
                                            if ($pendente == 1) {
                                                echo "SELECTED";
                                            }
                                            ?>>Aguardando</option>
                                            <option value="0" <?php
                                            if ($pendente == 0) {
                                                echo "SELECTED";
                                            }
                                            ?>>Finalizado</option>
                                            <option value="2" <?php
                                            if ($pendente == 2) {
                                                echo "SELECTED";
                                            }
                                            ?>>Cancelado</option>
                                            <option value="3" <?php
                                            if ($pendente == 3) {
                                                echo "SELECTED";
                                            }
                                            ?>>Sub-Rogação</option>
                                            <option value="4" <?php
                                            if ($pendente == 4) {
                                                echo "SELECTED";
                                            }
                                            ?>>Indeferido</option>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td align="right">Data:</td>
                                    <td><input type="text" name="txtDataFechamento" id="txtDataFechamento" class="input-medium datepicker" style="width:123px; text-align:center" value="<?php echo $dataFechamento; ?>" disabled></td>
                                    <td align="right">Hora:</td>
                                    <td><input type="text" name="txtHoraFechamento" id="txtHoraFechamento" class="input-medium" style="text-align:center" value="<?php echo $horaFechamento; ?>" disabled></td>
                                </tr>                                
                                <tr>
                                    <td align="right">Solução:</td>
                                    <td colspan="3"><textarea name="txtObsFechamento" id="txtObsFechamento" style="width:100%; height:410px" <?php echo $disabled; ?>><?php echo $obsFechamento; ?></textarea></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>     
            <div style="clear:both;"></div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <div style="float: left; width: 20%;margin-top: -4px;">
                        Número OS: <b style="font-size: 20px;"><?php echo str_pad($id, 8, "0", STR_PAD_LEFT); ?></b>
                    </div> 
                    <div style="float: left; width: 15%; margin-left: 1%; text-align: left; color: <?php echo $color; ?>;">
                        <?php echo $status; ?>
                    </div>
                    <?php if (($status == "Aguardando" || $status == "Sindicância") && $_SESSION['id_usuario'] == $para) { ?>
                        <div style="float:left">
                            <button class="btn btn-success" type="submit" name="bntAprov" id="bntAprov" title="Aprovar" ><span class="ico-checkmark"></span></button>
                            <button class="btn red" type="submit" name="bntReprov" id="bntReprov" title="Reprovar"><span class="ico-remove"></span></button>
                            <?php if ($status != "Sindicância") { ?>
                            <button class="btn yellow" type="submit" name="bntSindic" title="Sindicância" id="bntSindic"><span class="ico-locked-2"></span></button>
                            <?php } ?>                            
                        </div>
                    <?php } else if (($status == "Reprovado") && $_SESSION['id_usuario'] == $para) { ?>
                        <div style="float:left">
                            <button class="btn red" type="submit" name="bntDesfazer" id="bntDesfazer" title="Desfazer"><span class="ico-undo"></span></button>
                        </div>                            
                    <?php } if ($disabled == "") { ?>
                    <button id="bntSave" name="bntSave" type="submit" class="btn green" onClick="return validaForm();"><span class="ico-checkmark"></span> Gravar</button>
                        <?php if ($_POST["txtId"] == "") { ?>
                        <button class="btn yellow" onClick="parent.FecharBoxSinistros()" id="bntCancelar"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntAlterar"><span class="ico-reply"></span> Cancelar</button>
                            <?php
                        }
                    } else { ?>
                        <div style="float: left; margin-top: 15px;">
                            <select id="txtContrato" class="select" style="width:100%">
                                <?php $cur = odbc_exec($con, "select cnt.id, cnt.descricao, cnt.logo, cnt.margem_d from sf_contratos cnt where cnt.inativo = 0 and cnt.tipo = 3") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP['id']; ?>" logo="<?php echo $RFP['logo']; ?>" mdi="<?php echo $RFP['margem_d']; ?>"><?php echo utf8_encode($RFP['descricao']); ?></option> 
                                <?php } ?>
                            </select>                     
                        </div> 
                        <button style="float: left; margin-top: 15px;" class="btn button-orange" type="button" name="bntAssinar" id="bntAssinar" onclick="Assinar()" title="Assinar" value="Assinar"><span class="ico-pen icon-white"></span></button>                                                                                                                                            
                        <button style="float: left; margin-top: 15px;" class="btn button-blue" type="button" name="bntPrint" id="bntPrint" onclick="Print()" title="Imprimir" value="Imprimir"><span class="icon-print icon-white"></span></button>
                        <button class="btn green" type="submit" name="bntNew" id="bntNew" title="Novo" value="Novo"><span class="ico-plus-sign"></span></button>
                        <button class="btn green" type="submit" name="bntEdit" id="bntEdit" title="Alterar" value="Alterar"><span class="ico-edit"></span></button>
                        <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins.js"></script>    
    <script type="text/javascript" src="../../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type="text/javascript" src="../../../js/plugins/datatables/fnReloadAjax.js"></script>
    <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../../js/jquery.priceformat.min.js"></script> 
    <script type="text/javascript" src="../../../js/util.js"></script>
    <script type="text/javascript" src="js/FormSinistros.js"></script>
    <script type="text/javascript" src="js/VeiculosOficinasForm.js"></script> 
    <script type="text/javascript">        
        
        function Print() {
            window.open("../../util/ImpressaoPdf.php?id_evento=" + $("#txtId").val() + "&id=" + $("#txtContrato").val() + 
            "&model=S&tpImp=I&NomeArq=Contrato&PathArq=../Modulos/Comercial/modelos/ModeloContrato.php&emp=1" + 
            ($("#txtContrato option:selected").attr("logo") === "1" ? "&logo=n" : "") + 
            ($("#txtContrato option:selected").attr("mdi") !== "0" ? "&red=" + $("#txtContrato option:selected").attr("mdi") : ""), '_blank');
        }
        
        function Assinar() {
            window.open("../../Modulos/Seguro/FormAssinatura.php?id_evento=" + $("#txtId").val() + 
            "&idContrato=" + $("#txtContrato").val() + "&crt=" + parent.$("#txtMnContrato").val() + 
            "&idCliente=" + $("#txtIdPessoa").val() + "&id=" + $("#txtId").val(), '_blank');
        }
        
    </script>             
</body>
<?php odbc_close($con); ?>
</html>