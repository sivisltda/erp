<?php

if (isset($_POST['isAjax']) || isset($_GET['isAjax'])) {
    require_once(__DIR__ . '/../../../Connections/configini.php');
    require_once(__DIR__ . '/../../../util/util.php');
}

$mdl_seg_ = returnPart($_SESSION["modulos"], 14);

if (isset($_GET['listPlanosTurma'])) {
    $where = " and B.id_turma = 0 ";
    if (isset($_GET['idTurma']) && is_numeric($_GET['idTurma'])) {
        $where = " and B.id_turma = " . $_GET['idTurma'];
    }
    $query = "select conta_produto, descricao,CASE WHEN B.id_produto is not null then 'S' else 'N' end isCheck
              from sf_produtos A
              left join sf_turmas_produtos B on (A.conta_produto = B.id_produto" . $where . ")
              where tipo = 'C' and necessita_turma = 1 and A.conta_produto > 0";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["conta_produto"] = utf8_encode($RFP["conta_produto"]);
        $row["descricao"] = utf8_encode($RFP["descricao"]);
        $row["isCheck"] = utf8_encode($RFP["isCheck"]);
        $records[] = $row;
    }
    echo json_encode($records);
}

if (isset($_GET['getTurma'])) {
    $query = "select *, (select COUNT(id_fornecedores_despesas) from dbo.sf_fornecedores_despesas_turmas
              where id_turma = A.id_turma) alunosTurma from dbo.sf_turmas A where id_turma = " . $_GET['getTurma'];
    $cur = odbc_exec($con, $query);

    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('id_turma' => $RFP['id_turma'],
            'descricao' => tableFormato($RFP['descricao'], 'T', '', ''),
            'dt_cadastro' => escreverData($RFP['dt_cadastro']),
            'dt_inicio' => escreverData($RFP['dt_inicio']),
            'n_alunos' => tableFormato($RFP['n_alunos'], 'N', '', ''),
            'professor' => utf8_encode($RFP['professor']),
            'horario' => tableFormato($RFP['horario'], 'T', '', ''),
            'ambiente' => utf8_encode($RFP['ambiente']),
            'desmatricular' => utf8_encode($RFP['desmatricular']),
            'dias_desmatricula' => tableFormato($RFP['dias_desmatricula'], 'N', '', ''),
            'inativo' => utf8_encode($RFP['inativo']),
            'alunosTurma' => utf8_encode($RFP['alunosTurma']));
    }
    echo(json_encode($local));
}

if (isset($_POST['btnSave'])) {
    $query = "set dateformat dmy;";
    if (is_numeric($_POST['txtId'])) {
        $query .= "update sf_turmas set " .
                "descricao = " . valoresTextoUpper('txtDescricao') . "," .
                "dt_inicio = " . valoresData('txtDtInicio') . "," .
                "desmatricular = " . valoresCheck('ckbDesmatricular') . "," .
                "dias_desmatricula = " . valoresNumericos('txDiasDesmatricula') . "," .
                "inativo = " . valoresCheck('ckbInativo') . "," .
                "professor = " . valoresSelect('txtProfessor') . "," .
                "horario = " . valoresSelect('txtHorario') . "," .
                "n_alunos = " . valoresNumericos('txtCapacidade') . "," .
                "ambiente = " . valoresSelect('txtAmbiente') . " where id_turma = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data)
        values ('sf_turmas', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'A', 'ALTERACAO TURMA', GETDATE())");
    } else {
        $query .= "INSERT INTO sf_turmas(descricao, dt_inicio,desmatricular,dias_desmatricula,inativo,professor,horario,n_alunos,ambiente,dt_cadastro)VALUES(" .
                valoresTextoUpper('txtDescricao') . "," .
                valoresData('txtDtInicio') . "," .
                valoresCheck('ckbDesmatricular') . "," .
                valoresNumericos('txDiasDesmatricula') . "," .
                valoresCheck('ckbInativo') . "," .
                valoresSelect('txtProfessor') . "," .
                valoresSelect('txtHorario') . "," .
                valoresNumericos('txtCapacidade') . "," .
                valoresSelect('txtAmbiente') . ",getDate())";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_turma from sf_turmas order by id_turma desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data)
        values ('sf_turmas', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'I', 'INCLUSAO TURMA', GETDATE())");
    }

    $query = "delete from sf_turmas_produtos where id_turma = " . $_POST['txtId'];
    odbc_exec($con, $query);
    if (isset($_POST['itemsPlanos'])) {
        for ($i = 0; $i < count($_POST['itemsPlanos']); $i++) {
            $query = "insert into sf_turmas_produtos(id_turma,id_produto) values(" . $_POST['txtId'] . "," . $_POST['itemsPlanos'][$i] . ")";
            odbc_exec($con, $query);
        }
    }
    echo $_POST['txtId'];
}

if (isset($_POST['removerTurma'])) {
    $query = "delete from sf_turmas where id_turma = " . $_POST['removerTurma'];
    odbc_exec($con, $query) or die(odbc_errormsg());
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data)
    values ('sf_turmas', " . $_POST['removerTurma'] . ", '" . $_SESSION["login_usuario"] . "', 'E', 'EXCLUSAO OFICINA', GETDATE())");
    echo "YES";
}

if (isset($_GET['listAlunosTurma'])) {
    $Where = "where A.id_turma > 0 ";   
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    if (isset($_GET['txtId'])) {
        $Where .= " and T.id_turma = " . $_GET['txtId'];
    }      
    $query = "select planos_status, placa, modelo, data_tele, proximo_contato,
    A.id_fornecedores_despesas, B.razao_social,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = A.id_fornecedores_despesas) celular,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = A.id_fornecedores_despesas) email
    from sf_turmas T left join sf_turnos TU on T.horario = TU.cod_turno
    inner join sf_fornecedores_despesas_turmas A on A.id_turma = T.id_turma
    inner join sf_fornecedores_despesas B on A.id_fornecedores_despesas = B.id_fornecedores_despesas
    inner join sf_telemarketing TL on TL.id_tele = A.id_tele
    inner join sf_fornecedores_despesas_veiculo v on TL.id_veiculo = v.id
    left join sf_vendas_planos on sf_vendas_planos.id_veiculo = v.id " . $Where . " order by razao_social";
    //echo $query; exit;
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row[] = "<center><span title=\"" . (strlen($RFP["planos_status"]) > 0 ? $RFP["planos_status"] : "Cancelado") . "\" class=\"label-" . (strlen($RFP["planos_status"]) > 0 ? $RFP["planos_status"] : "Cancelado") . "\" style=\"display: inline-block;width: 13px;height: 13px;border-radius: 50%!important;\"></span></center>";            
        $row[] = "<div title='" . tableFormato($RFP['placa'], 'T', '', '') . "'>" . tableFormato($RFP['placa'], 'T', '', '') . "</div>";
        $row[] = "<div title='" . tableFormato($RFP['razao_social'], 'T', '', '') . "'>" . tableFormato($RFP['razao_social'], 'T', '', '') . "</div>";
        $row[] = "<div title='" . tableFormato($RFP['modelo'], 'T', '', '') . "'>" . tableFormato($RFP['modelo'], 'T', '', '') . "</div>";
        $row[] = "<div title='" . escreverDataHora($RFP['data_tele']) . "'>" . escreverDataHora($RFP['data_tele']) . "</div>";
        $row[] = "<div title='" . escreverDataHora($RFP['proximo_contato']) . "'>" . escreverDataHora($RFP['proximo_contato']) . "</div>";
        $row[] = "<div title='" . tableFormato($RFP['celular'], 'T', '', '') . "'>" . tableFormato($RFP['celular'], 'T', '', '') . "</div>";
        $row[] = "<div title='" . tableFormato($RFP['email'], 'T', '', '') . "'>" . tableFormato($RFP['email'], 'T', '', '') . "</div>";
        $row[] = $RFP['id_turma'];
        $row[] = utf8_encode($RFP['descricao']);    
        $output['aaData'][] = $row;
        $cont++;        
    }
    $output["iTotalRecords"] = $cont;
    $output["iTotalDisplayRecords"] = $cont;
    echo json_encode($output);
}

if (isset($_GET['listTurmaAluno'])) {
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );    
    if (is_numeric($_GET['listTurmaAluno'])) {
        $cur = odbc_exec($con, "select A.data_cadastro, null data, B.descricao descr_turma, C.razao_social, 
        M.nome_ambiente, A.id_turma, A.id_fornecedores_despesas id_aluno,0 id_desmatricula
        from sf_fornecedores_despesas_turmas A 
        inner join sf_turmas B on A.id_turma = B.id_turma
        left join sf_turnos D on B.horario = D.cod_turno
        left join sf_ambientes M on B.ambiente = M.id_ambiente
        left join sf_fornecedores_despesas C on B.professor = C.id_fornecedores_despesas
        where A.id_tele = " . $_GET['listTurmaAluno'] . " union all
        select data_cadastro, data, B.descricao, C.razao_social, 
        M.nome_ambiente, A.id_turma, A.id_fornecedores_despesas, id_desmatricula 
        from sf_turmas_desmatriculas A
        inner join sf_turmas B on A.id_turma = B.id_turma
        left join sf_ambientes M on B.ambiente = M.id_ambiente
        left join sf_fornecedores_despesas C on B.professor = C.id_fornecedores_despesas
        where id_tele = " . $_GET['listTurmaAluno'] . " order by 1 desc");
        while ($RFP = odbc_fetch_array($cur)) {
            $cont = $cont + 1;
            $backColor = (strlen($RFP["data"]) > 0 ? "" : " style='color: red' ");
            $row = array();
            $row[] = "<div " . $backColor . ">" . escreverDataHora($RFP["data_cadastro"]) . "</div>";
            $row[] = "<div " . $backColor . ">" . escreverDataHora($RFP["data"]) . "</div>";
            $row[] = "<div id=\"formPQ\" " . $backColor . " title=\"" . utf8_encode($RFP['descr_turma']) . "\">" . utf8_encode($RFP['descr_turma']) . "</div>";
            $row[] = "<div id=\"formPQ\" " . $backColor . " title=\"" . utf8_encode($RFP['razao_social']) . "\">" . utf8_encode($RFP['razao_social']) . "</div>";
            $row[] = "<div id=\"formPQ\" " . $backColor . " title=\"" . utf8_encode($RFP['nome_ambiente']) . "\">" . utf8_encode($RFP['nome_ambiente']) . "</div>";
            $row[] = "<center><button id=\"btnEditData\" data-id=\"" . $RFP["id_turma"] . "\" des-id=\"" . $RFP["id_desmatricula"] . "\" class=\"btn btn-small\" type=\"button\"\"><span class=\"ico-edit\"></span></button></center>";
            $records['aaData'][] = $row;
        }
        $records["iTotalRecords"] = $cont;
        $records["iTotalDisplayRecords"] = $cont;
    }
    echo json_encode($records);
}

if (isset($_POST['atualizaTurma'])) {
    $query = "set dateformat dmy;";    
    if ($_POST['idDesmatricula'] > 0 && strlen($_POST['dtIni']) > 0 && strlen($_POST['dtFim']) > 0) {
        $query .= "update sf_turmas_desmatriculas set data_cadastro = " . valoresDataHoraUnico("dtIni") . ", data = " . valoresDataHoraUnico("dtFim") . 
        " where id_desmatricula = " . $_POST['idDesmatricula'] . ";";
    } else if ($_POST['idDesmatricula'] > 0 && strlen($_POST['dtIni']) > 0 && strlen($_POST['dtFim']) == 0) {
        $query .= "insert into sf_fornecedores_despesas_turmas(id_turma,id_fornecedores_despesas,id_plano,id_tele,data_cadastro) 
        values(" . $_POST['idTurma'] . "," . $_POST['idPessoa'] . "," . $_POST['idPlano'] . "," . $_POST['idTele'] . ", " . valoresDataHoraUnico("dtIni") . ");
        delete from sf_turmas_desmatriculas where id_desmatricula = " . $_POST['idDesmatricula'] . ";";
    } else if ($_POST['idDesmatricula'] == 0 && strlen($_POST['dtIni']) > 0 && strlen($_POST['dtFim']) > 0) {
        $query .= "insert into sf_turmas_desmatriculas (data, id_fornecedores_despesas, id_turma, descricao_turma, motivo, login_user, id_tele, data_cadastro) 
        values (" . valoresDataHoraUnico("dtIni") . "," . $_POST['idPessoa'] . "," . $_POST['idTurma'] . ",(select descricao from sf_turmas where id_turma = " . $_POST['idTurma'] . "),
        'ALTERAÇÃO DE DATAS','" . $_SESSION["login_usuario"] . "'," . $_POST['idTele'] . "," . valoresDataHoraUnico("dtFim") . ");
        delete from sf_fornecedores_despesas_turmas where id_fornecedores_despesas = " . $_POST['idPessoa'] . " and id_turma = " . $_POST['idTurma'] . ";";
    } else if ($_POST['idDesmatricula'] == 0 && strlen($_POST['dtIni']) > 0 && strlen($_POST['dtFim']) == 0) {
        $query .= "update sf_fornecedores_despesas_turmas set data_cadastro = " . valoresDataHoraUnico("dtIni") . 
        " where id_fornecedores_despesas = " . valoresSelect("idPessoa") . " and id_turma = " . valoresSelect("idTurma") . 
        " and id_plano = " . valoresSelect("idPlano") . " and id_tele = " . valoresSelect("idTele") . ";";
    }   
    $query .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values ('sf_alunos_turmas', " . $_POST['idTurma'] . 
    ", '" . $_SESSION["login_usuario"] . "', 'A', '" . ($mdl_seg_ > 0 ? "ALTERACAO NA OFICINA" : "ALTERACAO ALUNO TURMA") . " (' + (select descricao from sf_turmas where id_turma = " . $_POST['idTurma'] . ") + ')', GETDATE(), " . $_POST['idPessoa'] . ");";                
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";    
}

if (isset($_POST['addAlunoTurma'])) {
    $query = "select id_fornecedores_despesas from sf_fornecedores_despesas_turmas where id_turma = " . valoresSelect('idTurma') . " and id_fornecedores_despesas = " . valoresSelect('addAlunoTurma');
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['id_fornecedores_despesas'] !== "") {
            echo "TURMA";
            return;
        }
    }
    $query = "set dateformat dmy;
    BEGIN TRANSACTION
        insert into sf_fornecedores_despesas_turmas(id_turma,id_fornecedores_despesas,id_plano,id_tele,data_cadastro) values(" . valoresSelect('idTurma') . "," . valoresSelect('addAlunoTurma') . "," . valoresSelect('idPlano') . "," . valoresSelect('idTele') . ", getdate());
        delete from sf_turmas_espera where id_fornecedores_despesas = " . valoresSelect('addAlunoTurma') . " and id_turma = " . valoresSelect('idTurma') . ";
        insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values ('sf_alunos_turmas', " . valoresSelect('idTurma') . ", '" . $_SESSION["login_usuario"] . "', 'I', '" . ($mdl_seg_ > 0 ? "ENTRADA NA OFICINA" : "INCLUSAO ALUNO TURMA") . " (' + (select descricao from sf_turmas where id_turma = " . valoresSelect('idTurma') . ") + ')', GETDATE(), " . valoresSelect('addAlunoTurma') . ");
    IF @@ERROR = 0
    COMMIT
    ELSE
    ROLLBACK;";
    //echo $query; exit;
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}

if (isset($_POST['excluirAlunoTurma'])) {
    $query = "set dateformat dmy;
    BEGIN TRANSACTION
        insert into sf_turmas_desmatriculas (data, id_fornecedores_despesas, id_turma, descricao_turma, motivo, login_user, id_tele, data_cadastro) values (getdate()," . $_POST['excluirAlunoTurma'] . "," . 
        $_POST['idTurma'] . ",(select descricao from sf_turmas where id_turma = " . $_POST['idTurma'] . ")," . valoresTexto('txtMotivo') . ",'" . $_SESSION["login_usuario"] . "'," . $_POST['idTele'] . "," .
        "(select max(data_cadastro) from sf_fornecedores_despesas_turmas where id_fornecedores_despesas = " . $_POST['excluirAlunoTurma'] . " and id_tele = " . $_POST['idTele'] . " and id_turma = " . $_POST['idTurma'] . "));
        delete from sf_fornecedores_despesas_turmas where id_fornecedores_despesas = " . $_POST['excluirAlunoTurma'] . " and id_turma = " . $_POST['idTurma'] . ";
        insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values ('sf_alunos_turmas', " . $_POST['idTurma'] . ", '" . $_SESSION["login_usuario"] . "', 'R', '" . ($mdl_seg_ > 0 ? "SAIDA DA OFICINA" : "EXCLUSAO ALUNO TURMA") . " (' + (select descricao from sf_turmas where id_turma = " . $_POST['idTurma'] . ") + ')', GETDATE(), " . $_POST['excluirAlunoTurma'] . ");
    IF @@ERROR = 0
    COMMIT
    ELSE
    ROLLBACK;";
    //echo $query;
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}