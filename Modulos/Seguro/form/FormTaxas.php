<?php

include "../../../Connections/configini.php";

if (isset($_POST['bntCota'])) {
    $query = "set dateformat dmy;
    insert into sf_cotas_lote (valor, data, data_cadastro, sys_login, limite, bol_status)
    values (" . valoresNumericos("txtValorCob") . ", " . valoresData("txtDtCobranca") .
            ", getdate(), '" . $_SESSION["login_usuario"] . "'," . valoresNumericos("txtValorLim") . ", 0);";
    odbc_exec($con, $query) or die(odbc_errormsg());
    $res = odbc_exec($con, "select top 1 id from sf_cotas_lote order by id desc") or die(odbc_errormsg());
    echo odbc_result($res, 1);
    exit;
}

if (isset($_POST['bntSave'])) {
    $query = "set dateformat dmy;
    insert into sf_vendas_planos_mensalidade (id_plano_mens, id_parc_prod_mens, dt_inicio_mens, dt_fim_mens, dt_cadastro, valor_mens, id_cota)        
    select id_plano, min(id_parc_prod_mens), " . valoresData("txtDtCobranca") . ", dateadd(day,-1, dateadd(month,1," . valoresData("txtDtCobranca") . ")), getdate(),  
    " . valoresNumericos("txtValor") . ", " . valoresNumericos("txtId") . " from sf_vendas_planos_mensalidade
    inner join sf_vendas_planos on id_plano_mens = id_plano
    inner join sf_fornecedores_despesas on favorecido = id_fornecedores_despesas
    where tipo = 'C' and inativo = 0 and dt_cancelamento is null 
    and ((" . valoresData("txtDtBegin") . " between dt_inicio_mens and dt_fim_mens) or (" . valoresData("txtDtEnd") . " between dt_inicio_mens and dt_fim_mens))
    and id_prod_plano = " . valoresSelect("txtPlano") . " group by id_plano";
    //echo $query;        
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
    exit;
}

if (isset($_POST['bntDelete'])) {
    $query = "delete from sf_cotas_lote where id = " . $_POST['txtId'];
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
    exit;
}

if (isset($_POST['bntMensLote'])) {
    $listItens = [];
    $cur = odbc_exec($con, "select m.id_mens, favorecido, id_prod_plano, id_plano_mens, month(dt_inicio_mens) mes, year(dt_inicio_mens) ano, 
    b.id_boleto, galaxy_id from sf_vendas_planos_mensalidade m 
    inner join sf_vendas_planos p on m.id_plano_mens = p.id_plano
    left join sf_boleto_online_itens bi on bi.id_mens = m.id_mens
    left join sf_boleto_online b on b.id_boleto = bi.id_boleto
    where m.id_cota = " . valoresNumericos("txtId") . " and dt_pagamento_mens is null");
    while ($RFP = odbc_fetch_array($cur)) {
        if (is_numeric($RFP['id_boleto']) && !is_numeric($RFP['galaxy_id'])) {
            $row = [];
            $row["processarBoleto"] = "S";
            $row["txtIdBol"] = $RFP['id_boleto'];
            $listItens[] = $row;
        } else if (!is_numeric($RFP['id_boleto']) && !is_numeric($RFP['galaxy_id'])) {
            $listItens[] = getItensMens($con, $RFP['favorecido'], $RFP['id_prod_plano'], $RFP['id_plano_mens'], $RFP['mes'], $RFP['ano']);
        }
    }
    echo json_encode($listItens);
    exit;
}

if (isset($_GET['printDetalhes'])) {        
    $iTotal = 0;
    $toReturn = [];    
    $sQuery1 = "select c.id_fornecedores_despesas, c.razao_social, m.valor_mens, m.dt_inicio_mens,
    v.placa, v.marca, v.modelo, v.ano_modelo, b.id_boleto, b.galaxy_id
    from sf_cotas_lote l
    inner join sf_vendas_planos_mensalidade m on l.id = m.id_cota
    inner join sf_vendas_planos p on p.id_plano = m.id_plano_mens
    inner join sf_fornecedores_despesas c on c.id_fornecedores_despesas = p.favorecido
    left join sf_fornecedores_despesas_veiculo v on v.id = p.id_plano
    left join sf_boleto_online_itens bi on bi.id_mens = m.id_mens
    left join sf_boleto_online b on b.id_boleto = bi.id_boleto
    where l.id = " . $_GET['id_lote'] . " order by galaxy_id desc, id_fornecedores_despesas";
    //echo $sQuery1; exit;
    $cur = odbc_exec($con, $sQuery1);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row[] = $RFP['id_fornecedores_despesas'];
        $row[] = utf8_encode($RFP['razao_social']);
        $row[] = escreverData($RFP['dt_inicio_mens']);        
        $row[] = escreverNumero($RFP['valor_mens']);
        $row[] = "[" . utf8_encode($RFP['placa']) . "] " . utf8_encode($RFP['modelo']);
        $row[] = (is_numeric($RFP['id_boleto']) ? "SIM" : "NÃO");
        $row[] = (is_numeric($RFP['galaxy_id']) ? "SIM" : "NÃO");
        $toReturn[] = $row;
        $iTotal++;
    }
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => $iTotal,
        "iTotalDisplayRecords" => $iTotal,
        "aaData" => $toReturn
    );
}

function getItensMens($con, $id_cli, $id_produto, $plano, $mes, $ano) {
    $i = 1;
    $toReturn = [];
    $cur = odbc_exec($con, "select id_mens,dbo.VALOR_REAL_MENSALIDADE(id_mens) valor_mens 
    from sf_vendas_planos_mensalidade where id_plano_mens = " . $plano . " and month(dt_inicio_mens) = " . $mes .
            " and YEAR(dt_inicio_mens) = " . $ano . " and dt_pagamento_mens is null");
    while ($RFP = odbc_fetch_array($cur)) {
        $toReturn["txtIdItem_" . $i] = $id_produto;
        $toReturn["txtIdPlano_" . $i] = $RFP['id_mens'];
        $toReturn["txtTipo_" . $i] = "MENSALIDADE";
        $toReturn["txtQtd_" . $i] = "1";
        $toReturn["txtValor_" . $i] = escreverNumero($RFP["valor_mens"]);
        $toReturn["txtMulta_" . $i] = escreverNumero("0");
        $toReturn["txtValFinal_" . $i] = escreverNumero($RFP["valor_mens"]);
        $i++;
    }
    $toReturn["txtIdCli"] = $id_cli;
    $toReturn["txtQtdItens"] = ($i - 1);
    $toReturn["salvaBoleto"] = "S";
    return $toReturn;
}
