<?php

if (isset($_POST['isAjax']) || isset($_GET['isAjax'])) {
    include "../../../Connections/configini.php";
}

$disabled = 'disabled';
$id = '';
$id_fornecedores_despesas = '';
$pessoaNome = '';
$statusAluno = '';
$marca = '';
$modelo = '';
$ano_modelo = '';
$mes_referencia = '';
$venda_plano = '';
$txtPlano = '';
$placa = '';
$tipo = '1';
$ano_fab = '';
$combustivel = '';
$nPassageiros = '5';
$veiculoZ = '0';
$chassi = '';
$renavam = '';
$nPortas = '4';
$cilindrada = '';
$motor = '';
$km = '';
$categoria = '1';
$cambio = '1';
$cor = 'null';
$data = getData("T") . " " . date("H:i");
$alienado = '0';
$depreciacao = '0';
$codFipe = '';
$porcentagem = '100,00';
$valorFipe = '0,00';
$protegido = '0,00';
$franquia = '0,00';
$descontotp = '0';
$id_vistoria = "";
$id_vistoria_ok = "";
$id_item_venda = "";
$gnv = "0";
$funcionario = "";
$destinatario = $_SESSION["login_usuario"];
$status = "Aguarda";
$imei = "";
$chip = "";
$id_externo = "";
$id_externo_rast = "";
$status_ext_veic = "";
$wa_chave = "";
$dtInstalacao = "";
$typePage = "";
$user_resp = "";
$rastreador = "";
$operadora = "";
$ra_chave = "";
$Imagens = [];
$ImagensDanos = [];

$cur = odbc_exec($con, "select seg_piso_franquia, seg_cota_protegido, ra_chave from sf_configuracao where id = 1");
while ($RFP = odbc_fetch_array($cur)) {
    $pisoFranquia = escreverNumero($RFP['seg_piso_franquia']);
    $tipo_protegido = $RFP['seg_cota_protegido'];
    $ra_chave = $RFP['ra_chave'];
    $consulta_fipe = 0;    
}

$toReturn = "";
if ($_GET['tp'] != "") {
    $toReturn = "?tp=" . $_GET['tp'];
}

$idCidade = "";
if (isset($_GET['id_pessoa']) && is_numeric($_GET['id_pessoa'])) {
    $cur = odbc_exec($con, "select cidade from sf_fornecedores_despesas where id_fornecedores_despesas = " . $_GET['id_pessoa']);
    while ($RFP = odbc_fetch_array($cur)) {
        $idCidade = $RFP['cidade'];
    }
}

if (isset($_POST['bntSave'])) {
    $totalImei = 0;
    $cur = odbc_exec($con, "select count(id) total from sf_fornecedores_despesas_veiculo 
    where len(isnull(imei, '')) > 0 and id not in (" . valoresNumericos("txtId") . ") and imei = " . valoresTexto("txtImei"));
    while ($RFP = odbc_fetch_array($cur)) {
        $totalImei = $RFP['total'];
    }
    if ($totalImei > 0) {
        echo "<script>alert('Código Imei já cadastrado em outro veículo!');</script>";
    } else {
        if ($_POST['txtId'] != '') {
            odbc_exec($con, "set dateformat dmy;
                update sf_fornecedores_despesas_veiculo set " .
                "id_fornecedores_despesas = " . valoresSelect("txtIdCliente") . "," .
                "mes_referencia = " . valoresTexto("txtMesRef") . "," .
                "placa = " . valoresTextoUpper("txtPlaca") . "," .
                "tipo_veiculo = " . valoresSelect("txtTipo") . "," .
                "marca = " . valoresTexto("txtMontadora") . "," .
                "modelo = " . valoresTexto("txtModelo") . "," .
                "ano_modelo = " . valoresTexto2(explode(" ", $_POST["txtAnoMod"])[0]) . "," .
                "ano_fab = " . valoresNumericos("txtAnoFab") . "," .
                "combustivel = " . valoresTexto("txtCombustivel") . "," .
                "n_passageiros = " . valoresNumericos("txtNPassageiros") . "," .
                "zero_km = " . valoresCheck("txtVeiculoZ") . "," .
                "chassi = " . valoresTextoUpper("txtChassi") . "," .
                "renavam = " . valoresTexto("txtRenavam") . "," .
                "n_portas = " . valoresNumericos("txtNPortas") . "," .
                "cilindrada = " . valoresTexto("txtCilindrada") . "," .
                "n_motor = " . valoresTexto("txtMotor") . "," .
                "kms = " . valoresNumericos("txtKM") . "," .
                "categoria = " . valoresSelect("txtCategoria") . "," .
                "cambio = " . valoresSelect("txtCambio") . "," .
                "cor = " . valoresSelect("txtCor") . "," .
                "alienado = " . valoresSelect("txtAlienado") . "," .
                "depreciacao = " . valoresSelect("txtDepreciacao") . "," .
                "codigo_fipe = " . valoresTexto("txtCodFipe") . "," .
                "consulta_fipe = " . valoresNumericos("rbtipoFipe") . "," .                    
                "porcentagem = " . valoresNumericos("txtPorcentagem") . "," .
                "valor = " . valoresNumericos("txtProtegido") . "," .
                "id_item_venda = " . valoresSelect("txtVendaItem") . "," .
                "gas_natural = " . valoresSelect("txtGnv") . "," .
                "franquia = " . valoresNumericos("txtFranquia") . "," .                     
                "franquia_tp = " . valoresNumericos("txtProdSinal") . "," .
                "destinatario = " . valoresSelectTexto("txtAutorizacao") . "," .
                "imei = " . valoresTexto("txtImei") . "," .
                "chip = " . valoresTexto("txtChip") . "," .
                "dt_instalacao = " . valoresData("txtDtInstalacao") . "," .
                "id_rastreador = " . valoresSelect("txtRastreador") . "," .                    
                "id_operadora = " . valoresSelect("txtOperadora") . " where id = " . valoresNumericos("txtId")) or die(odbc_errormsg());
            $id = $_POST['txtId'];
        } else {
            $query = "set dateformat dmy; insert into sf_fornecedores_despesas_veiculo(id_fornecedores_despesas,mes_referencia,
                placa,tipo_veiculo,marca,modelo,ano_modelo,ano_fab,combustivel,n_passageiros,zero_km,
                chassi,renavam,n_portas,cilindrada,n_motor,kms,categoria,cambio,cor,data_cadastro,alienado,depreciacao,
                codigo_fipe,consulta_fipe,porcentagem,valor,id_item_venda,gas_natural,franquia, franquia_tp, destinatario, 
                status, imei, chip, dt_instalacao, id_rastreador, id_operadora) values (" .
                valoresSelect("txtIdCliente") . "," .
                valoresTexto("txtMesRef") . "," .
                valoresTextoUpper("txtPlaca") . "," .
                valoresSelect("txtTipo") . "," .
                valoresTexto("txtMontadora") . "," .
                valoresTexto("txtModelo") . "," .
                valoresTexto2(explode(" ", $_POST["txtAnoMod"])[0]) . "," .
                valoresNumericos("txtAnoFab") . "," .
                valoresTexto("txtCombustivel") . "," .
                valoresNumericos("txtNPassageiros") . "," .
                valoresCheck("txtVeiculoZ") . "," .
                valoresTextoUpper("txtChassi") . "," .
                valoresTexto("txtRenavam") . "," .
                valoresNumericos("txtNPortas") . "," .
                valoresTexto("txtCilindrada") . "," .
                valoresTexto("txtMotor") . "," .
                valoresNumericos("txtKM") . "," .
                valoresSelect("txtCategoria") . "," .
                valoresSelect("txtCambio") . "," .
                valoresSelect("txtCor") . "," .
                valoresData("txtData") . "," .
                valoresSelect("txtAlienado") . "," .
                valoresSelect("txtDepreciacao") . "," .
                valoresTexto("txtCodFipe") . "," .
                valoresNumericos("rbtipoFipe") . "," .                    
                valoresNumericos("txtPorcentagem") . "," .
                valoresNumericos("txtProtegido") . "," .
                valoresSelect("txtVendaItem") . "," .
                valoresSelect("txtGnv") . "," .
                valoresNumericos("txtFranquia") . "," .
                valoresNumericos("txtProdSinal") . "," .                    
                valoresSelectTexto("txtAutorizacao") . "," .
                "'Aguarda'," .
                valoresTexto("txtImei") . "," .
                valoresTexto("txtChip") . "," .
                valoresData("txtDtInstalacao") . "," .
                valoresSelect("txtRastreador") . "," .
                valoresSelect("txtOperadora") . ")";
                //echo $query; exit;
            odbc_exec($con, $query) or die(odbc_errormsg());
            $res = odbc_exec($con, "select top 1 id from sf_fornecedores_despesas_veiculo order by id desc") or die(odbc_errormsg());
            $_POST['txtId'] = odbc_result($res, 1);
            $id = $_POST['txtId'];
            $query = "insert into sf_vistorias (id_veiculo, data, tipo, sys_login, id_item_venda, obs) values (" .
            $id . ", getdate(), (select top 1 id from sf_vistorias_tipo where (id in (select id_vistoria from sf_vistorias_veiculo_tipos where id_tipo = " . valoresSelect("txtTipo") . ") or 
            (select count(id_tipo) from sf_vistorias_veiculo_tipos where id_vistoria = sf_vistorias_tipo.id) = 0) and inativo = 0 order by id)," . valoresTexto2($_SESSION["login_usuario"]) . ", null, '')";
            odbc_exec($con, $query) or die(odbc_errormsg());
        }
        if (is_numeric(valoresSelect("txtVendaItem"))) {
            odbc_exec($con, "update sf_vendas_itens set vendedor_comissao = " . valoresSelect("txtFuncionario") . " where id_item_venda = " . valoresSelect("txtVendaItem"));
        }
        $prod_grp = $_POST['txtPD'];
        if (is_numeric($_POST["txtIdPlano"])) {
            odbc_exec($con, "update sf_vendas_planos set id_prod_plano = (select max(id_produto) from sf_produtos_parcelas where id_parcela = " . valoresSelect("txtPlano") . ") where id_plano = " . $_POST["txtIdPlano"]);
            odbc_exec($con, "update sf_vendas_planos_mensalidade set id_parc_prod_mens = " . valoresSelect("txtPlano") . " where id_item_venda_mens is null and id_plano_mens = " . $_POST["txtIdPlano"]);
            $notIn = '0';
            if (is_array($prod_grp)) {
                for ($i = 0; $i < sizeof($prod_grp); $i++) {
                    if (is_numeric($prod_grp[$i])) {
                        if (is_numeric($_POST['txtIP'][$i])) {
                            $notIn .= "," . $_POST['txtIP'][$i];
                        }
                    }
                }
                odbc_exec($con, "delete from sf_vendas_planos_acessorios where id_venda_plano = " . $_POST["txtIdPlano"] . " and id not in (" . $notIn . ")");
                for ($i = 0; $i < sizeof($prod_grp); $i++) {
                    if (is_numeric($prod_grp[$i])) {
                        if (is_numeric($_POST['txtIP'][$i])) {
                            odbc_exec($con, "update sf_vendas_planos_acessorios set id_servico_acessorio = " . $prod_grp[$i] . " where id = " . $_POST['txtIP'][$i]) or die(odbc_errormsg());
                        } else {
                            odbc_exec($con, "insert into sf_vendas_planos_acessorios(id_venda_plano, id_servico_acessorio) values(" . $_POST["txtIdPlano"] . "," . $prod_grp[$i] . ")");
                        }
                    }
                }
            }
        } else {
            $gerarAuto = 0;
            $gerarAutoTotal = 0;
            $cur = odbc_exec($con, "select * from sf_configuracao where id = 1");
            while ($RFP = odbc_fetch_array($cur)) {
                $gerarAuto = $RFP['ACA_RENOVACAO_AUTO'];
                $gerarAutoTotal = $RFP['clb_mensalidades_aberto'];
            }
            odbc_exec($con, "insert into sf_vendas_planos (id_turno, dt_inicio, dt_fim, favorecido, dias_ferias, dt_cadastro, usuario_resp, 
            id_prod_plano, planos_status, id_veiculo) values (1, getdate(), getdate(), " . valoresSelect("txtIdCliente") . ", 0, getdate(), 1,
            (select max(id_produto) from sf_produtos_parcelas where id_parcela = " . valoresSelect("txtPlano") . "), 'Novo', " . $id . ")");
            $res = odbc_exec($con, "select top 1 id_plano from sf_vendas_planos order by id_plano desc") or die(odbc_errormsg());
            $vp = odbc_result($res, 1);
            odbc_exec($con, "insert into sf_vendas_planos_mensalidade (id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, usu_alt, dt_alt, valor_mens)
            values (" . $vp . ", getdate(), dateadd(day,-1,dateadd(month,1,getdate())), " . valoresSelect("txtPlano") . ", getdate(), '" . $_SESSION["login_usuario"] . "', getdate(),
            (select max(valor_unico) from sf_produtos_parcelas where id_parcela = " . valoresSelect("txtPlano") . "))");
            $res = odbc_exec($con, "select top 1 id_mens from sf_vendas_planos_mensalidade order by id_mens desc") or die(odbc_errormsg());
            $id_all_mens = odbc_result($res, 1);
            if ($gerarAuto == 1) {
                for ($i = 0; $i < $gerarAutoTotal; $i++) {
                    $query .= " insert into sf_vendas_planos_mensalidade (id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens)
                    select id_plano_mens, dateadd(month," . $i . ",dateadd(day,1,dt_fim_mens)),
                    case when tp_valid_acesso = 'C' then dateadd(day, qtd_dias_acesso,dt_fim_mens) else 
                        dateadd(day,-1, dateadd(month,1,dateadd(month," . $i . ",dateadd(day,1,dt_fim_mens))))
                    end,
                    id_parc_prod_mens, GETDATE(),valor_unico valor_mens
                    from sf_vendas_planos_mensalidade m1 inner join sf_produtos_parcelas pp on m1.id_parc_prod_mens = pp.id_parcela
                    inner join sf_produtos p on p.conta_produto = pp.id_produto
                    inner join sf_vendas_planos on id_plano = id_plano_mens                
                    where p.inativar_renovacao = 0 and id_mens in (" . $id_all_mens . ") and dt_cancelamento is null and parcela in (-1,0,1) and (select COUNT(*) from sf_vendas_planos_mensalidade m2
                    where m2.id_plano_mens = m1.id_plano_mens and m2.id_parc_prod_mens = m1.id_parc_prod_mens
                    and ((tp_valid_acesso <> 'C' and month(m2.dt_inicio_mens) = month(dateadd(month," . ($i + 1) . ",m1.dt_inicio_mens)) and year(m2.dt_inicio_mens) = year(dateadd(month," . ($i + 1) . ",m1.dt_inicio_mens))
                    ) or (tp_valid_acesso = 'C' and dateadd(day," . ($i + 1) . ",m1.dt_fim_mens) = dt_inicio_mens))) = 0;";
                }
            }
            odbc_exec($con, "update sf_vendas_planos set dt_fim = dbo.FU_PLANO_FIM(id_plano) where id_plano = " . $vp);
            if (is_array($prod_grp)) {
                for ($i = 0; $i < sizeof($prod_grp); $i++) {
                    if (is_numeric($prod_grp[$i])) {
                        odbc_exec($con, "insert into sf_vendas_planos_acessorios(id_venda_plano, id_servico_acessorio) values(" . $vp . "," . $prod_grp[$i] . ")");
                    }
                }
            }
            odbc_exec($con, "insert into sf_vendas_planos_acessorios (id_venda_plano, id_servico_acessorio)
            select " . $vp . ", id_materiaprima from sf_produtos_materiaprima
            inner join sf_produtos on sf_produtos.conta_produto = id_materiaprima where tipo = 'A' 
            and id_produtomp in (select id_produto from sf_produtos_parcelas where id_parcela = " . valoresSelect("txtPlano") . ")
            and conta_movimento not in (select id_contas_movimento from sf_vendas_planos_acessorios
            inner join sf_produtos on conta_produto = id_servico_acessorio
            inner join sf_contas_movimento on conta_movimento = id_contas_movimento
            where id_venda_plano = " . $vp . " and terminal = 0)");
        }
    }
}

if (isset($_POST['bntDelete']) && is_numeric($_POST['txtId'])) {    
    $totalPlano = 0;
    $cur = odbc_exec($con, "select count(id_plano) total from sf_vendas_planos where id_veiculo = " . $_POST['txtId']);
    while ($RFP = odbc_fetch_array($cur)) {
        $totalPlano = $RFP['total'];
    }
    if ($totalPlano > 0) {
        echo "<script>alert('Não é possível excluir um veículo com um plano relacionado!');</script>";
    } else {    
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        select 'sf_fornecedores_despesas_veiculo', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'D', 'EXCLUSAO DE VEICULO: ' + placa, GETDATE(), id_fornecedores_despesas from sf_fornecedores_despesas_veiculo where id = " . $_POST['txtId'] . ";") or die(odbc_errormsg());    
        odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_veiculo WHERE id = " . $_POST['txtId']);
        if (isset($_POST['bntDelete'])) {
            echo "<script>alert('Registro excluido com sucesso'); parent.FecharBoxVeiculo(1);</script>";
        } else {
            echo "YES";
        }
        exit();        
    }
}

if (isset($_POST['bntAprov']) && is_numeric($_POST['txtId'])) {
    $query = "update sf_fornecedores_despesas_veiculo set status = 'Aprovado', data_aprov = getdate(), destinatario = '" . $_SESSION["login_usuario"] . "' where status in ('Aguarda','Pendente') and id in (" . $_POST['txtId'] . ")";
    odbc_exec($con, $query) or die(odbc_errormsg());    
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    select 'sf_fornecedores_despesas_veiculo', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'I', 'VEICULO APROVADO: ' + placa, GETDATE(), id_fornecedores_despesas from sf_fornecedores_despesas_veiculo where id = " . $_POST['txtId'] . ";") or die(odbc_errormsg());                             
}

if (isset($_POST['bntReprov']) && is_numeric($_POST['txtId']) && is_numeric($_POST['txtPara'])) {
    if (isset($_POST['motivo']) && strlen($_POST['motivo'])) {        
        odbc_exec($con, "insert into sf_telemarketing(de_pessoa, para_pessoa, de_pessoa_fechamento, pessoa, obs, obs_fechamento, relato, data_tele, dt_inclusao, data_tele_fechamento, atendimento_servico, garantia, nomecon, origem, acompanhar, tipo_proprietario, pendente, especificacao)
        select " . $_SESSION["id_usuario"] . ", " . $_POST['txtPara'] . ", " . $_SESSION["id_usuario"] . ", id_fornecedores_despesas, " . valoresTexto('motivo') . ", " . valoresTexto('motivo') . ", " . valoresTexto('motivo') . ", getdate(), getdate(), getdate(), 0, 0, '', 0, 0, 0, 0, 'O'
        from sf_fornecedores_despesas_veiculo where id = " . $_POST['txtId'] . ";") or die(odbc_errormsg());                
    }
    odbc_exec($con, "update sf_fornecedores_despesas_veiculo set status = 'Reprovado', data_aprov = getdate(), destinatario = '" . $_SESSION["login_usuario"] . "' where status in ('Aguarda','Pendente') and id in (" . $_POST['txtId'] . ");") or die(odbc_errormsg());    
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    select 'sf_fornecedores_despesas_veiculo', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'R', 'VEICULO REPROVADO: ' + " . valoresTexto('motivo') . ", GETDATE(), id_fornecedores_despesas from sf_fornecedores_despesas_veiculo where id = " . $_POST['txtId'] . ";") or die(odbc_errormsg());                         
    
    $cur = odbc_exec($con, "select id_fornecedores_despesas from sf_fornecedores_despesas_veiculo where id in (" . $_POST['txtId'] . ");");
    while ($RFP = odbc_fetch_array($cur)) {        
        $_POST['txtId'] = $RFP['id_fornecedores_despesas'];
    }
    $id_padrao = "24";        
    include "../../Sivis/ws_disparo_imediato.php";  
    echo "YES";    
}

if (isset($_POST['bntCancelarStatus']) && is_numeric($_POST['txtId'])) {
    $query = "update sf_fornecedores_despesas_veiculo set status = 'Pendente', data_aprov = null, destinatario = '" . $_SESSION["login_usuario"] . "' where id in (" . $_POST['txtId'] . ")";
    odbc_exec($con, $query) or die(odbc_errormsg());    
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    select 'sf_fornecedores_despesas_veiculo', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'C', 'CANCELAMENTO (VEICULO APROVADO): ' + placa, GETDATE(), id_fornecedores_despesas from sf_fornecedores_despesas_veiculo where id = " . $_POST['txtId'] . ";") or die(odbc_errormsg());                         
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select sf_fornecedores_despesas_veiculo.* ,razao_social, fornecedores_status, 
    (select max(id_plano) from sf_vendas_planos where dt_cancelamento is null and id_veiculo = sf_fornecedores_despesas_veiculo.id) venda_plano,
    (select top 1 id_parcela from sf_vendas_planos inner join sf_vendas_planos_mensalidade on id_plano = id_plano_mens
    inner join sf_produtos_parcelas on id_parcela = id_parc_prod_mens where dt_cancelamento is null and id_veiculo = sf_fornecedores_despesas_veiculo.id order by id_mens desc) plano,
    (select top 1 id from sf_vistorias where id_veiculo = sf_fornecedores_despesas_veiculo.id order by data desc) vistoria,
    (select top 1 id from sf_vistorias where data_aprov is not null and id_veiculo = sf_fornecedores_despesas_veiculo.id order by data desc) vistoria_ok,
    id_user_resp, tipo
    from sf_fornecedores_despesas_veiculo
    inner join sf_fornecedores_despesas on sf_fornecedores_despesas.id_fornecedores_despesas = sf_fornecedores_despesas_veiculo.id_fornecedores_despesas
    where id = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id']);
        $id_fornecedores_despesas = utf8_encode($RFP['id_fornecedores_despesas']);
        $pessoaNome = utf8_encode($RFP['razao_social']);
        $statusAluno = utf8_encode($RFP['fornecedores_status']);
        $marca = utf8_encode($RFP['marca']);
        $modelo = utf8_encode($RFP['modelo']);
        $ano_modelo = utf8_encode($RFP['ano_modelo']);
        $mes_referencia = utf8_encode($RFP['mes_referencia']);
        $venda_plano = utf8_encode($RFP['venda_plano']);
        $txtPlano = utf8_encode($RFP['plano']);
        $placa = utf8_encode($RFP['placa']);
        $tipo = utf8_encode($RFP['tipo_veiculo']);
        $ano_fab = utf8_encode($RFP['ano_fab']);
        $combustivel = utf8_encode($RFP['combustivel']);
        $nPassageiros = utf8_encode($RFP['n_passageiros']);
        $veiculoZ = utf8_encode($RFP['zero_km']);
        $chassi = utf8_encode($RFP['chassi']);
        $renavam = utf8_encode($RFP['renavam']);
        $nPortas = utf8_encode($RFP['n_portas']);
        $cilindrada = utf8_encode($RFP['cilindrada']);
        $motor = utf8_encode($RFP['n_motor']);
        $km = utf8_encode($RFP['kms']);
        $categoria = utf8_encode($RFP['categoria']);
        $cambio = utf8_encode($RFP['cambio']);
        $cor = utf8_encode($RFP['cor']);
        $data = escreverDataHora($RFP['data_cadastro']);
        $alienado = utf8_encode($RFP['alienado']);
        $depreciacao = utf8_encode($RFP['depreciacao']);
        $codFipe = utf8_encode($RFP['codigo_fipe']);
        $porcentagem = escreverNumero($RFP['porcentagem'], 0);
        $valorFipe = escreverNumero('0');
        $protegido = escreverNumero($RFP['valor']);
        $id_vistoria = utf8_encode($RFP['vistoria']);
        $id_vistoria_ok = utf8_encode($RFP['vistoria_ok']);
        $id_item_venda = utf8_encode($RFP['id_item_venda']);
        $gnv = utf8_encode($RFP['gas_natural']);
        $franquia = escreverNumero($RFP['franquia']);        
        $descontotp = utf8_encode($RFP['franquia_tp']);        
        $destinatario = utf8_encode($RFP['destinatario']);
        $status = utf8_encode($RFP['status']);
        $imei = utf8_encode($RFP['imei']);
        $chip = utf8_encode($RFP['chip']);
        $dtInstalacao = escreverData($RFP['dt_instalacao']);
        $id_externo = utf8_encode($RFP['id_externo']);
        $id_externo_rast = utf8_encode($RFP['id_externo_rast']);
        $status_ext_veic = utf8_encode($RFP['status_ext_veic']);
        $consulta_fipe = utf8_encode($RFP['consulta_fipe']);
        $typePage = ($RFP['tipo'] == "P" ? "Prospect" : "Cliente");
        $user_resp = utf8_encode($RFP['id_user_resp']);       
        $rastreador = utf8_encode($RFP['id_rastreador']);       
        $operadora = utf8_encode($RFP['id_operadora']); 
    }
    $cur = odbc_exec($con, "select wa_chave from sf_configuracao where id = 1");
    while ($RFP = odbc_fetch_array($cur)) {
        $wa_chave = utf8_encode($RFP['wa_chave']);
    }
    if (is_numeric($id_vistoria)) {
        $pasta = "./../../Pessoas/" . $contrato . "/Vistorias/" . $id_vistoria . "/";
        if (file_exists($pasta)) {
            $diretorio = dir($pasta);
            while ($arquivo = $diretorio->read()) {
                if ($arquivo != "." && $arquivo != "..") {
                    $Imagens[] = $arquivo;
                }
            }
        }
        $pasta = "./../../Pessoas/" . $contrato . "/Danos/" . $id_vistoria . "/";
        if (file_exists($pasta)) {
            $diretorio = dir($pasta);
            while ($arquivo = $diretorio->read()) {
                if ($arquivo != "." && $arquivo != "..") {
                    $ImagensDanos[] = $arquivo;
                }
            }
        }
    }
} else {
    $disabled = '';
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}

if (isset($_GET['listaItens'])) {
    $listItens = [];
    if (is_numeric($_GET['listaItens'])) {    
        $cur = odbc_exec($con, "select vpa.id, p.conta_produto, p.descricao,
        case when p.tp_preco = 1 then (px.segMax * p.preco_venda)/100 
        when p.tp_preco = 2 then (v.valor * p.preco_venda)/100 else p.preco_venda end preco_venda
        from sf_vendas_planos vp inner join sf_vendas_planos_acessorios vpa on vp.id_plano = vpa.id_venda_plano
        inner join sf_fornecedores_despesas_veiculo v on v.id = vp.id_veiculo
        inner join sf_produtos p on p.conta_produto = vpa.id_servico_acessorio
        inner join sf_produtos px on px.conta_produto = vp.id_prod_plano
        WHERE p.tipo='A' AND vp.id_plano = " . $_GET['listaItens']);
        while ($RFP = odbc_fetch_array($cur)) {
            $listItens[] = array('id' => $RFP['id'],
                'id_produto' => utf8_encode($RFP['conta_produto']),
                'produtodesc' => utf8_encode($RFP['descricao']),
                'valor_total' => escreverNumero($RFP['preco_venda'], 2));
        }
    }
    echo json_encode($listItens);
}

if (isset($_GET['getPlano']) && valoresNumericos2($_GET['valor']) > 0 && valoresNumericos2($_GET['tipo']) > 0 && valoresNumericos2($_GET['ano']) > 1900) {   
    $listItens = [];
    $formas = array("DCC", "MENSAL", "BIMESTRAL", "TRIMESTRAL", "QUADRIMESTRAL", "5 Meses", "SEMESTRAL", "7 Meses", "8 Meses", "9 Meses", "10 Meses", "11 Meses", "ANUAL");
    $cur = odbc_exec($con, "set dateformat dmy; select id_produto, id_parcela, descricao, parcela, valor_unico, cota_defaut, cota_defaut_tp 
    from sf_produtos inner join sf_produtos_parcelas on id_produto = conta_produto
    where conta_produto > 0 and tipo = 'C' and inativa = 0
    and (sf_produtos.conta_produto in (select id_produto from sf_produtos_veiculo_tipos where id_tipo = " . valoresNumericos2($_GET['tipo']) . ") or
    sf_produtos.conta_produto not in (select id_produto from sf_produtos_veiculo_tipos))
    and (grupo_cidades is null or grupo_cidades in (select id_regiao from sf_regioes_cidades inner join sf_regioes on id = id_regiao inner join sf_fornecedores_despesas on id_cidade = cidade 
    where id_fornecedores_despesas = " . valoresNumericos2($_GET['pessoa']) . " and sf_regioes.inativo = 0))
    and ((select count(id) from sf_veiculo_grupo inner join sf_veiculo_grupo_fabricante on id = id_grupo where inativo = 0 and id_marca = " . valoresNumericos2($_GET['marca']) . ") = 0 or 
    grupo_fabricantes in (select id from sf_veiculo_grupo inner join sf_veiculo_grupo_fabricante on id = id_grupo where inativo = 0 and id_marca = " . valoresNumericos2($_GET['marca']) . "))
    and " . valoresNumericos2($_GET['valor']) . " between segMin and segMax and ano_piso > datediff(year, '01/01/" . valoresNumericos2($_GET['ano']) . "', getdate())");    
    while ($RFP = odbc_fetch_array($cur)) {
        $listItens[] = array('Value' => utf8_encode($RFP['id_parcela']),
            'Label' => utf8_encode($RFP['descricao']) . " - " . escreverNumero($RFP['valor_unico'], 2) . " (" . ($RFP['parcela'] == -2 ? "PIX" : ($RFP['parcela'] == -1 ? "BOL" : $formas[$RFP['parcela']])) . ")",
            'Price' => escreverNumero($RFP['valor_unico'], 2),
            'Parcela' => $RFP['parcela'],
            'Plano' => $RFP['id_produto'],
            'Cota' => $RFP['cota_defaut'],
            'Tipo' => $RFP['cota_defaut_tp']);
    }
    echo json_encode($listItens);
}

if ($_GET['Del_F'] != '') {
    if (file_exists("./../../Pessoas/" . $contrato . "/Veiculos/" . $id . "/" . $_GET['Del_F'])) {
        unlink("./../../Pessoas/" . $contrato . "/Veiculos/" . $id . "/" . $_GET['Del_F']);
    } else {
        echo "<script type='text/javascript'>alert('Erro ao excluir o arquivo.');</script>";
    }
}

if (isset($_POST['bntSendFile'])) {
    $allowedExts = array("gif", "jpeg", "jpg", "png", "pdf", "GIF", "JPEG", "JPG", "PNG", "PDF");
    $extension = explode(".", $_FILES["file"]["name"]);
    $extension = $extension[count($extension) - 1];
    if ((($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/pjpeg") || ($_FILES["file"]["type"] == "image/x-png") || ($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "application/pdf")) && ($_FILES["file"]["size"] < 10000000) && in_array($extension, $allowedExts)) {
        if ($_FILES["file"]["error"] == 0) {
            $pathDir = "./../../Pessoas/" . $contrato . "/Veiculos/" . $id;
            if (!file_exists($pathDir)) {
                mkdir($pathDir, 0777);
            }
            move_uploaded_file($_FILES["file"]["tmp_name"], $pathDir . "/" . str_replace(" ", "_", $_FILES["file"]["name"]));
        }
    } else {
        echo "<script type='text/javascript'>alert('Erro ao enviar o arquivo.\\nTamanho máximo permitido: 10 mb.\\nFormatos aceitos: gif, jpeg, jpg, png e pdf.');</script>";
    }
}

if (isset($_GET['listPlanosTodos'])) {
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    if (is_numeric($_GET['listPlanosTodos'])) {
        $cur = odbc_exec($con, "set dateformat dmy;
        select id_plano, favorecido, razao_social, descricao, dt_inicio, dt_fim, dt_cancelamento, planos_status, obs_cancelamento 
        from sf_vendas_planos inner join sf_fornecedores_despesas on id_fornecedores_despesas = favorecido
        inner join sf_produtos on conta_produto = id_prod_plano
        where id_veiculo = " . $_GET['listPlanosTodos'] . " order by id_plano");        
        while ($RFP = odbc_fetch_array($cur)) {
            $cont = $cont + 1;            
            $row = array();            
            $row[] = "<input title=\"Contrato\" class=\"btn\" type=\"button\" value=\"+\" onclick=\"parent.abrirContrato(" . $RFP["id_plano"] . ");\" style=\"padding:2px 4px 3px 4px; line-height:10px;\"> " . 
            $RFP["favorecido"] . " - " . formatNameCompact(utf8_encode($RFP["razao_social"]));
            $row[] = utf8_encode($RFP["descricao"]);
            $row[] = "<center>" . escreverData($RFP["dt_inicio"]) . "</center>";
            $row[] = "<center>" . escreverData($RFP["dt_fim"]) . "</center>";
            $row[] = "<center>" . escreverData($RFP["dt_cancelamento"]) . "</center>";
            $row[] = "<span title=\"" . utf8_encode($RFP["obs_cancelamento"]) . "\" class=\"label label-" . utf8_encode($RFP["planos_status"]) . "\" style=\"font-size: 11px; line-height: 13px;\">" . utf8_encode($RFP["planos_status"]) . "</span>";
            $records['aaData'][] = $row;
        }
        $records["iTotalRecords"] = $cont;
        $records["iTotalDisplayRecords"] = $cont;
    }
    echo json_encode($records);
}

if (isset($_GET['tblHistVistorias'])) {
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    if (is_numeric($_GET['tblHistVistorias'])) {
        $cur = odbc_exec($con, "select data,descricao,sys_login,data_aprov from sf_vistorias vi
        inner join sf_vistorias_tipo vt on vt.id = vi.tipo
        where id_veiculo = " . $_GET['tblHistVistorias'] . " order by data desc");
        while ($RFP = odbc_fetch_array($cur)) {
            $cont = $cont + 1;
            $row = array();
            $row[] = "<center>" . escreverDataHora($RFP["data"]) . "</center>";
            $row[] = utf8_encode($RFP["descricao"]);
            $row[] = utf8_encode($RFP["sys_login"]);
            $row[] = "<center>" . escreverDataHora($RFP["data_aprov"]) . "</center>";
            $records['aaData'][] = $row;
        }
        $records["iTotalRecords"] = $cont;
        $records["iTotalDisplayRecords"] = $cont;
    }
    echo json_encode($records);
}

if (isset($_GET['getPagamentos'])) {
    $listItens = [];
    $cur = odbc_exec($con, "select id_item_venda, data_venda, descricao, vendedor_comissao, quantidade 
    from sf_vendas_itens vi inner join sf_vendas v on v.id_venda = vi.id_venda
    inner join sf_produtos p on p.conta_produto = vi.produto
    where produto = 41 and vendedor_comissao is null and cliente_venda = " . valoresSelect2($_GET['getPagamentos']));
    while ($RFP = odbc_fetch_array($cur)) {
        $listItens[] = array('Value' => $RFP['id_item_venda'], 'Label' => escreverDataHora($RFP['data_venda']) . " Qtd. " . escreverNumero($RFP['quantidade'], 0, 0));
    }
    echo json_encode($listItens);
}

if (isset($_GET['getPlanos']) && $_GET['getPlanos'] != "null") {   
    $listItens = [];
    $cur = odbc_exec($con, "select conta_produto, descricao, preco_venda from sf_produtos 
    where tipo = 'C' and inativa = 0 and conta_produto in (select id_produto from sf_produtos_veiculo_tipos 
    where id_tipo in (" . str_replace("\"", "'", str_replace(array("[", "]"), "", $_GET["getPlanos"])) . ")) ORDER BY descricao");
    while ($RFP = odbc_fetch_array($cur)) {
        $listItens[] = array('conta_produto' => $RFP['conta_produto'], 'descricao' => utf8_encode($RFP['descricao']), 'preco_venda' => $RFP['preco_venda']);
    }
    echo json_encode($listItens);
}

if (isset($_GET['getAcessorios']) && $_GET['getAcessorios'] != "null") {
    $listItens = [];
    $cur = odbc_exec($con, "select conta_produto, descricao, preco_venda from sf_produtos where inativa = 0 and tipo = 'A'
    and (veiculo_tipo is null or veiculo_tipo in (" . str_replace("\"", "'", str_replace(array("[", "]"), "", $_GET["getAcessorios"])) . ")) ORDER BY descricao");
    while ($RFP = odbc_fetch_array($cur)) {
        $listItens[] = array('conta_produto' => $RFP['conta_produto'], 'descricao' => utf8_encode($RFP['descricao']), 'preco_venda' => $RFP['preco_venda']);
    }
    echo json_encode($listItens);
}