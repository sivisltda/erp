<?php

if (isset($_POST['isAjax']) || isset($_GET['isAjax'])) {
    include "../../../Connections/configini.php";
}

$disabled = 'disabled';
$id = '';
$tipo = '';
$data = getData("T") . " " . date("H:i");
$vistoriador = $_SESSION["login_usuario"];
$id_veiculo = "";
$veiculo_tipo = "";
$info_veiculo = "";
$info_id_cliente = "";
$info_cliente = "";
$placa = "";
$obs = "";
$id_item_venda = "";
$funcionario = "";
$data_aprov = "";
$latitude = "";
$longitude = "";
$id_telemarketing = "";
$data_tele = "";
$Imagens = [];
$ImagensDanos = [];

$toReturn = "";
if ($_GET['tp'] != "") {
    $toReturn = "?tp=" . $_GET['tp'];
}
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_vistorias set " .
        "id_veiculo = " . valoresSelect("txtIdVeiculo") . "," .
        "tipo = " . valoresSelectTexto("txtTipo") . "," .
        "sys_login = " . valoresTexto2($_SESSION["login_usuario"]) . "," .
        "id_item_venda = " . valoresSelect("txtVendaItem") . "," .                
        "obs = " . valoresTexto("txtObs") . "," .
        "latitude = " . valoresSelectTexto("txtLatitude") . "," .
        "longitude = " . valoresSelectTexto("txtLongitude") . "," .
        "id_telemarketing = " . valoresSelect("txtSinistro") . " " .
        "where id = " . valoresNumericos("txtId")) or die(odbc_errormsg());
        $id = $_POST['txtId'];
    } else {
        $query = "insert into sf_vistorias (id_veiculo, data, tipo, sys_login, id_item_venda, obs, latitude, longitude, id_telemarketing) values (" .
        valoresSelect("txtIdVeiculo") . ", getdate(), " . valoresSelectTexto("txtTipo") . "," .
        valoresTexto2($_SESSION["login_usuario"]) . "," . valoresSelect("txtVendaItem") . "," . 
        valoresTexto("txtObs") . "," . valoresSelectTexto("txtLatitude") . "," . 
        valoresSelectTexto("txtLongitude") . "," . valoresSelect("txtSinistro") . ")";
        //echo $query; exit;
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id from sf_vistorias order by id desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
        $id = $_POST['txtId'];
        
        $query = "update sf_fornecedores_despesas_veiculo set status = 'Pendente', data_aprov = null where id in (" . valoresSelect("txtIdVeiculo") . ")";
        odbc_exec($con, $query) or die(odbc_errormsg());
    }    
    if (is_numeric(valoresSelect("txtVendaItem"))) {
        odbc_exec($con, "update sf_vendas_itens set vendedor_comissao = " . valoresSelect("txtFuncionario") . " where id_item_venda = " . valoresSelect("txtVendaItem"));
    }
    $notIn = '0';    
    $prod_grp = $_POST['itemsGrupo'];    
    for ($i = 0; $i < sizeof($prod_grp); $i++) {
        if (is_numeric($prod_grp[$i])) {
            $notIn .= "," . $prod_grp[$i];
        }
    }
    odbc_exec($con, "delete from sf_vistorias_itens where id_vistoria = " . $id . " and id_item not in (" . $notIn . ")");
    for ($i = 0; $i < sizeof($prod_grp); $i++) {
        if (is_numeric($prod_grp[$i])) {
            odbc_exec($con, "IF NOT EXISTS (SELECT id_vistoria, id_item FROM sf_vistorias_itens WHERE id_vistoria = " . $id . " AND id_item = " . $prod_grp[$i] . ")
            BEGIN INSERT INTO sf_vistorias_itens(id_vistoria, id_item) VALUES (" . $id . "," . $prod_grp[$i] . "); END	
            ELSE BEGIN UPDATE sf_vistorias_itens SET id_vistoria = " . $id . ", id_item = " . $prod_grp[$i] . " WHERE id_vistoria = " . $id . " AND id_item = " . $prod_grp[$i] . "; END");
        }
    }
}

if (isset($_POST['bntDelete']) && is_numeric($_POST['txtId'])) {
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    values ('sf_vistorias', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'E', 'EXCLUSAO - VISTORIA', GETDATE(),
    (select max(id_fornecedores_despesas) from sf_vistorias vi
    inner join sf_fornecedores_despesas_veiculo v on vi.id_veiculo = v.id
    where vi.id = " . $_POST['txtId'] . "))");    
    odbc_exec($con, "DELETE FROM sf_vistorias_itens WHERE id_vistoria = " . $_POST['txtId']);
    odbc_exec($con, "DELETE FROM sf_vistorias_danos WHERE id_vistoria = " . $_POST['txtId']);
    odbc_exec($con, "DELETE FROM sf_vistorias WHERE id = " . $_POST['txtId']);
    echo "<script>alert('Registro excluido com sucesso'); parent.FecharBoxVistorias();</script>";
}

if (isset($_POST['bntAprov']) && is_numeric($_POST['txtId'])) {
    $query = "update sf_vistorias set data_aprov = getdate() where data_aprov is null and id in (" . $_POST['txtId'] . ")";
    odbc_exec($con, $query) or die(odbc_errormsg());
}

if (isset($_POST['bntReprov']) && is_numeric($_POST['txtId'])) {
    $query = "update sf_vistorias set data_aprov = null where data_aprov is not null and id in (" . $_POST['txtId'] . ")";
    odbc_exec($con, $query) or die(odbc_errormsg());
}

if (isset($_POST['RemoverImg']) && is_numeric($_POST['txtId'])) {
    if (file_exists("./../../../Pessoas/" . $contrato . "/Vistorias/" . $_POST['txtId'] . "/" . $_POST['RemoverImg'])) {
        unlink("./../../../Pessoas/" . $contrato . "/Vistorias/" . $_POST['txtId'] . "/" . $_POST['RemoverImg']);
        echo "YES"; exit;
    } else {
        echo "./../../../Pessoas/" . $contrato . "/Vistorias/" . $_POST['txtId'] . "/" . $_POST['RemoverImg'];
    }
}

if (isset($_POST['RemoverImgDanos']) && is_numeric($_POST['txtId']) && is_numeric($_POST['txtIdDano'])) {
    if (file_exists("./../../../Pessoas/" . $contrato . "/Danos/" . $_POST['txtId'] . "/" . $_POST['RemoverImgDanos'])) {
        unlink("./../../../Pessoas/" . $contrato . "/Danos/" . $_POST['txtId'] . "/" . $_POST['RemoverImgDanos']);
        odbc_exec($con, "DELETE FROM sf_vistorias_danos WHERE id = " . $_POST['txtIdDano']);
        echo "YES"; exit;
    } else {
        echo "./../../../Pessoas/" . $contrato . "/Danos/" . $_POST['txtId'] . "/" . $_POST['RemoverImgDanos'];
    }
}

if ($_GET['Del_F'] != '' && is_numeric($_POST['txtId'])) {
    if (file_exists("./../../Pessoas/" . $contrato . "/Vistorias/" . $_POST['txtId'] . "/" . $_GET['Del_F'])) {
        unlink("./../../Pessoas/" . $contrato . "/Vistorias/" . $_POST['txtId'] . "/" . $_GET['Del_F']);
    } else {
        echo "<script type='text/javascript'>alert('Erro ao excluir o arquivo.');</script>";
    }
}

if (isset($_POST['bntSendFile']) && is_numeric($_POST['txtId'])) {
    $allowedExts = array("gif", "jpeg", "jpg", "png", "pdf", "mp4", "3gp", "GIF", "JPEG", "JPG", "PNG", "PDF", "MP4", "3GP");
    $extension = explode(".", $_FILES["file"]["name"]);
    $extension = $extension[count($extension) - 1];
    if ((($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/pjpeg") || ($_FILES["file"]["type"] == "image/x-png") || ($_FILES["file"]["type"] == "image/png") 
    || ($_FILES["file"]["type"] == "application/pdf") || ($_FILES["file"]["type"] == "video/mp4") || ($_FILES["file"]["type"] == "video/3gpp")) && ($_FILES["file"]["size"] < 20000000) && in_array($extension, $allowedExts)) {
        if ($_FILES["file"]["error"] == 0) {
            $pathDir = "./../../Pessoas/" . $contrato . "/Vistorias/" . $_POST['txtId'];
            if (!file_exists($pathDir)) {
                mkdir($pathDir, 0777, true);
            }            
            move_uploaded_file($_FILES["file"]["tmp_name"], $pathDir . "/" . $_POST['txtId'] . "_" . $_POST['txtImgSel'] . "." . $extension);
        }
    } else {
        echo "<script type='text/javascript'>alert('Erro ao enviar o arquivo.\\nTamanho máximo permitido: 10 mb.\\nFormatos aceitos: gif, jpeg, jpg, png, mp4, 3gp e pdf.');</script>";
    }
}

if (isset($_POST['bntSendFileDanos']) && is_numeric($_POST['txtId'])) {
    $allowedExts = array("gif", "jpeg", "jpg", "png", "pdf", "GIF", "JPEG", "JPG", "PNG", "PDF");
    $extension = explode(".", $_FILES["fileDanos"]["name"]);
    $extension = $extension[count($extension) - 1];
    if ((($_FILES["fileDanos"]["type"] == "image/gif") || ($_FILES["fileDanos"]["type"] == "image/jpeg") || ($_FILES["fileDanos"]["type"] == "image/jpg") || ($_FILES["fileDanos"]["type"] == "image/pjpeg") || ($_FILES["fileDanos"]["type"] == "image/x-png") || ($_FILES["fileDanos"]["type"] == "image/png") || ($_FILES["fileDanos"]["type"] == "application/pdf")) && ($_FILES["fileDanos"]["size"] < 10000000) && in_array($extension, $allowedExts)) {
        if ($_FILES["fileDanos"]["error"] == 0) {
            $pathDir = "./../../Pessoas/" . $contrato . "/Danos/" . $_POST['txtId'];
            if (!file_exists($pathDir)) {
                mkdir($pathDir, 0777, true);
            }            
            odbc_exec($con, "insert into sf_vistorias_danos (id_vistoria, observacao) values (" . valoresNumericos("txtId") . "," . valoresTexto("txtDanos") . ")") or die(odbc_errormsg());
            $res = odbc_exec($con, "select top 1 id from sf_vistorias_danos order by id desc") or die(odbc_errormsg());
            $idImg = odbc_result($res, 1);            
            move_uploaded_file($_FILES["fileDanos"]["tmp_name"], $pathDir . "/" . $idImg . "." . $extension);
        }
    } else {
        echo "<script type='text/javascript'>alert('Erro ao enviar o arquivo.\\nTamanho máximo permitido: 10 mb.\\nFormatos aceitos: gif, jpeg, jpg, png e pdf.');</script>";
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select sf_vistorias.* , marca, modelo, ano_modelo, combustivel, valor, razao_social, cnpj, tipo_veiculo, placa, sf_fornecedores_despesas.id_fornecedores_despesas,id_telemarketing, data_tele  
    from sf_vistorias inner join sf_fornecedores_despesas_veiculo on id_veiculo = sf_fornecedores_despesas_veiculo.id
    inner join sf_fornecedores_despesas on sf_fornecedores_despesas.id_fornecedores_despesas = sf_fornecedores_despesas_veiculo.id_fornecedores_despesas
    left join sf_telemarketing on id_telemarketing = id_tele
    where sf_vistorias.id = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id']);
        $tipo = utf8_encode($RFP['tipo']);
        $data = escreverDataHora($RFP['data']);
        $vistoriador = utf8_encode($RFP['sys_login']);
        $id_veiculo = utf8_encode($RFP['id_veiculo']);
        $veiculo_tipo = utf8_encode($RFP['tipo_veiculo']);
        $info_veiculo = utf8_encode($RFP['marca']) . " " . utf8_encode($RFP['modelo']) . ", " . utf8_encode($RFP['ano_modelo']) . " - " . utf8_encode($RFP['combustivel']) . " Valor " . escreverNumero($RFP['valor'], 1);
        $info_id_cliente = utf8_encode($RFP['id_fornecedores_despesas']);
        $info_cliente = utf8_encode($RFP['cnpj']) . " - " . utf8_encode($RFP['razao_social']);        
        $placa = utf8_encode($RFP['placa']);
        $obs = utf8_encode($RFP['obs']);
        $id_item_venda = utf8_encode($RFP['id_item_venda']);
        $data_aprov = escreverDataHora($RFP['data_aprov']);
        $latitude = utf8_encode($RFP['latitude']);
        $longitude = utf8_encode($RFP['longitude']);
        $id_telemarketing = utf8_encode($RFP['id_telemarketing']);
        $data_tele = escreverDataHora($RFP['data_tele']);
    }    
    $pasta = (isset($_POST['isAjax']) ? "./../../../" : "./../../") . "Pessoas/" . $contrato . "/Vistorias/" . $id . "/";
    if (file_exists($pasta)) {
        $diretorio = dir($pasta);
        while ($arquivo = $diretorio->read()) {
            if ($arquivo != "." && $arquivo != "..") {
                $Imagens[] = $arquivo;
            }
        }
    }
    $pasta = (isset($_POST['isAjax']) ? "./../../../" : "./../../") . "Pessoas/" . $contrato . "/Danos/" . $id . "/";
    if (file_exists($pasta)) {
        $diretorio = dir($pasta);
        while ($arquivo = $diretorio->read()) {
            if ($arquivo != "." && $arquivo != "..") {
                $ImagensDanos[] = $arquivo;
            }
        }
    }        
} else {
    $disabled = '';
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}

if (isset($_GET['getItens'])) {
    $listItens = [];    
    $cur = odbc_exec($con, "select id, descricao, id_vistoria from sf_fornecedores_despesas_veiculo_item
    left join sf_vistorias_itens on sf_vistorias_itens.id_item = sf_fornecedores_despesas_veiculo_item.id and id_vistoria = " . 
    valoresNumericos2($_GET['id']) . " where tipo = " . valoresNumericos2($_GET['tipo']));
    while ($RFP = odbc_fetch_array($cur)) {
        $listItens[] = array('id' => $RFP['id'],
            'id' => utf8_encode($RFP['id']),
            'name' => utf8_encode($RFP['descricao']),
            'check' => (is_numeric($RFP['id_vistoria']) ? true : false));
    }
    echo json_encode($listItens);
}

if (isset($_GET['getPlaca'])) {
    $listItens = [];    
    $cur = odbc_exec($con, "select sf_fornecedores_despesas_veiculo.id, tipo_veiculo, marca, modelo, ano_modelo, ano_fab, combustivel, valor, cnpj, razao_social, 
    sf_fornecedores_despesas.id_fornecedores_despesas, renavam, chassi, nome_produto_cor, sf_veiculo_tipos.descricao tipo_veiculo_descricao, fornecedores_status
    from sf_fornecedores_despesas_veiculo 
    inner join sf_fornecedores_despesas on sf_fornecedores_despesas.id_fornecedores_despesas = sf_fornecedores_despesas_veiculo.id_fornecedores_despesas
    left join sf_produtos_cor on sf_produtos_cor.id_produto_cor = sf_fornecedores_despesas_veiculo.cor
    left join sf_veiculo_tipos on sf_veiculo_tipos.id = sf_fornecedores_despesas_veiculo.tipo_veiculo
    where placa = " . valoresTexto2($_GET['getPlaca']) . " order by 1 desc");
    while ($RFP = odbc_fetch_array($cur)) {
        $listItens[] = array('id' => $RFP['id'],
            'id' => utf8_encode($RFP['id']),
            'tipo_veiculo' => utf8_encode($RFP['tipo_veiculo']),
            'tipo_veiculo_descricao' => utf8_encode($RFP['tipo_veiculo_descricao']),
            'veiculo' => utf8_encode($RFP['marca']) . " " . utf8_encode($RFP['modelo']) . ", " . utf8_encode($RFP['ano_modelo']) . " - " . utf8_encode($RFP['combustivel']) . " Valor " . escreverNumero($RFP['valor'], 1),
            'renavam' => utf8_encode($RFP['renavam']),
            'chassi' => utf8_encode($RFP['chassi']),
            'cor' => utf8_encode($RFP['nome_produto_cor']),
            'cnpj' => utf8_encode($RFP['cnpj']),
            'razao_social' => utf8_encode($RFP['razao_social']),
            'proprietario' => utf8_encode($RFP['cnpj']) . " - " . utf8_encode($RFP['razao_social']),
            'id_pessoa' => utf8_encode($RFP['id_fornecedores_despesas']),                        
            'marca' => utf8_encode($RFP['marca']),
            'modelo' => utf8_encode($RFP['modelo']),
            'ano_modelo' => utf8_encode($RFP['ano_modelo']),
            'ano_fab' => utf8_encode($RFP['ano_fab']),
            'fornecedores_status' => utf8_encode($RFP['fornecedores_status'])
        );
    }
    echo json_encode($listItens);
}

if (isset($_GET['getPagamentos'])) {
    $listItens = [];    
    $cur = odbc_exec($con, "select id_item_venda, data_venda, descricao, vendedor_comissao, quantidade 
    from sf_vendas_itens vi inner join sf_vendas v on v.id_venda = vi.id_venda
    inner join sf_produtos p on p.conta_produto = vi.produto
    where produto = 13 and vendedor_comissao is null and cliente_venda = " . valoresSelect2($_GET['getPagamentos']));
    while ($RFP = odbc_fetch_array($cur)) {
        $listItens[] = array('Value' => $RFP['id_item_venda'], 'Label' => escreverDataHora($RFP['data_venda']) . " Qtd. " . escreverNumero($RFP['quantidade'], 0, 0));
    }
    echo json_encode($listItens);
}

if (isset($_GET['getVistoriaTipo'])) {
    $listItens = [];     
    $query = "SELECT id, descricao from sf_vistorias_tipo 
    where (id in (select id_vistoria from sf_vistorias_veiculo_tipos where id_tipo = " . $_GET['getVistoriaTipo'] . ") or 
    (select count(id_tipo) from sf_vistorias_veiculo_tipos where id_vistoria = sf_vistorias_tipo.id) = 0) and inativo = 0";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) { 
        $listItens[] = array('id' => $RFP['id'], 'descricao' => utf8_encode($RFP['descricao']));
    }    
    echo json_encode($listItens);    
}

if (isset($_POST['txtId']) && isset($_POST['Imagens'])) {
    echo json_encode($Imagens);    
}