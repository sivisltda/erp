<?php

include "../../../Connections/configini.php";

if (isset($_POST['btnSave'])) {
    if (is_numeric($_POST['id_produto']) && is_numeric($_POST['id_modelo']) && is_numeric($_POST['id_servico'])) {
        $query = "INSERT INTO sf_veiculo_modelo_produto(id_produto,id_modelo,id_servico) VALUES(" .
                $_POST['id_produto'] . "," .
                $_POST['id_modelo'] . "," .
                $_POST['id_servico'] . ")";
        odbc_exec($con, $query);
        echo "YES";        
    }
}

if (isset($_POST['btnDelete'])) {
    if (is_numeric($_POST['id_produto']) && is_numeric($_POST['id_modelo']) && is_numeric($_POST['id_servico'])) {
        $query = "delete from sf_veiculo_modelo_produto where id_produto = " . $_POST['id_produto'] . 
        " and id_modelo = " . $_POST['id_modelo'] . " and id_servico = " . $_POST['id_servico'];
        odbc_exec($con, $query);
        echo "YES";
    }
}

if (isset($_GET['listProd'])) {
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    if (is_numeric($_GET['id_modelo']) && is_numeric($_GET['id_servico'])) {
        $query = "select id_produto, id_modelo, id_servico, descricao,
        conta_movimento, quantidade_comercial, unidade_comercial, preco_venda
        from sf_veiculo_modelo_produto
        inner join sf_produtos on conta_produto = id_produto
        where tipo = 'P' and id_modelo = " . $_GET['id_modelo'] . " and id_servico = " . $_GET['id_servico'];
        $cur = odbc_exec($con, $query);
        while ($RFP = odbc_fetch_array($cur)) {
            $row = array();
            $row[] = utf8_encode($RFP["descricao"]);
            $row[] = "<center><input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\"
            onclick=\"removerProduto('" . $RFP["id_produto"] . "','" . $RFP["id_modelo"] . "','" . $RFP["id_servico"] . "');\" /></center>";    
            $row[] = utf8_encode($RFP["id_produto"]);
            $row[] = utf8_encode($RFP["conta_movimento"]);
            $row[] = escreverNumero($RFP["quantidade_comercial"]);
            $row[] = utf8_encode($RFP["unidade_comercial"]);
            $row[] = escreverNumero($RFP["preco_venda"]);            
            $output['aaData'][] = $row;
        }
    }
    echo json_encode($output);
}

odbc_close($con);