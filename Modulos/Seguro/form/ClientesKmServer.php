<?php

include "../../../Connections/configini.php";
require_once('../../../util/util.php');

if (isset($_POST['SalvarKm'])) {
    if (is_numeric($_POST['txtId'])) {
        $query = "set dateformat dmy; insert into sf_fornecedores_despesas_veiculo_kms values (" . $_POST['txtId'] . "," . 
        valoresData("dti") . "," . valoresTexto("txtKms") . ", '" . $_SESSION["login_usuario"] . "', getdate())";
        odbc_exec($con, $query) or die(odbc_errormsg());
        echo "YES";
    }
}

if (isset($_POST['btnExcluirKm'])) {
    if (is_numeric($_POST['txtId']) && is_numeric($_POST['txtIdKm'])) {
        $cur = odbc_exec($con, "select kms.*,id_fornecedores_despesas from sf_fornecedores_despesas_veiculo_kms kms
        inner join sf_fornecedores_despesas_veiculo v on id_veiculo = v.id 
        where kms.id_veiculo = " . $_POST['txtId'] . " and kms.id = " . $_POST['txtIdKm']);        
        while ($RFP = odbc_fetch_array($cur)) {
            odbc_exec($con, "set dateformat dmy;insert into sf_logs values('sf_fornecedores_despesas_veiculo_kms'," . $RFP['id'] . ", '" . $_SESSION["login_usuario"] . "','R','EXCLUSAO KMS " . $RFP['kms'] . " - " . escreverData($RFP['data']) . "', getdate(), " . $RFP['id_fornecedores_despesas'] . ")");
        }
        odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_veiculo_kms WHERE id_veiculo = " . $_POST['txtId'] . " and id = " . $_POST['txtIdKm']);
        echo "YES";
    }
}

if (isset($_GET['listKms'])) {
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );    
    if (is_numeric($_GET['listKms'])) {
        $cur = odbc_exec($con, "select * from sf_fornecedores_despesas_veiculo_kms
        where id_veiculo = " . $_GET['listKms'] . " order by data desc");
        while ($RFP = odbc_fetch_array($cur)) {
            $cont = $cont + 1;
            $row = array();
            $row[] = escreverData($RFP['data']);        
            $row[] = utf8_encode($RFP["kms"]);       
            $row[] = escreverDataHora($RFP["data_cad"]);        
            $row[] = utf8_encode($RFP["sys_login"]);              
            $row[] = "<center><input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\" onClick=\"RemoverKms(" . $RFP['id'] . ")\")></center>";
            $records['aaData'][] = $row;
        }
        $records["iTotalRecords"] = $cont;
        $records["iTotalDisplayRecords"] = $cont;
    }
    echo json_encode($records);
}