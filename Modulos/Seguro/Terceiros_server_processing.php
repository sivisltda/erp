<?php

include '../../Connections/configini.php';
$aColumns = array('id_fornecedores_despesas', 'cnpj', 'razao_social', 'inscricao_estadual', 'cidade', 'estado', 'telefone', 'email', 'dt_demissao');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY id_fornecedores_despesas asc ";
$sLimit = 20;
$imprimir = 0;
$_GET['txtBusca'] =  utf8_decode($_GET['txtBusca']);

if (is_numeric($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $sOrder .= $aColumns[intval($_GET['iSortCol_0'])] . " " . $_GET['sSortDir_0'] . ", ";
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY id_fornecedores_despesas asc";
    }
}

if ($_GET['txtBusca'] != "") {
    $sWhereX .= " and (id_fornecedores_despesas like '%" . $_GET['txtBusca'] . "%' or cnpj like '%" . $_GET['txtBusca'] . "%' or razao_social like '%" . $_GET['txtBusca'] . "%'
    or inscricao_estadual like '%" . $_GET['txtBusca'] . "%' or contato like '%" . $_GET['txtBusca'] . "%'
    or estado_nome like '%" . $_GET['txtBusca'] . "%' or cidade_nome like '%" . $_GET['txtBusca'] . "%')";
}

if (isset($_GET['txtFilial']) && is_numeric($_GET['txtFilial'])) {
    $sWhereX .= " and empresa = " . valoresNumericos2($_GET['txtFilial']);
}

$sQuery = "SELECT COUNT(*) total FROM sf_fornecedores_despesas left join tb_estados on estado = estado_codigo left join tb_cidades on cidade = cidade_codigo
WHERE tipo = 'O' $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total FROM sf_fornecedores_despesas left join tb_estados on estado = estado_codigo left join tb_cidades on cidade = cidade_codigo
WHERE tipo = 'O' $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "set dateformat dmy; SELECT * FROM (SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,id_fornecedores_despesas, razao_social, cnpj, inscricao_estadual, cidade_nome cidade, estado_sigla estado,
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = id_fornecedores_despesas) telefone, 
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = id_fornecedores_despesas) email, 
dt_demissao FROM sf_fornecedores_despesas left join sf_tipo_documento on forma_pagamento = id_tipo_documento left join tb_estados on estado = estado_codigo
left join tb_cidades on cidade = cidade_codigo WHERE id_fornecedores_despesas > 0 AND tipo = 'O' " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1=" . $imprimir . " " . $sOrder;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $cor = "red";
    if (utf8_encode($aRow[$aColumns[8]]) == '') {
        $cor = "green";
    }
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[0]]) . "'>" . utf8_encode($aRow[$aColumns[0]]) . "</div></center>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[1]]) . "'>" . utf8_encode($aRow[$aColumns[1]]) . "</div></center>";
    $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[$aColumns[0]]) . ")'><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[2]]) . "'>" . utf8_encode($aRow[$aColumns[2]]) . "</div></a>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[3]]) . "'>" . utf8_encode($aRow[$aColumns[3]]) . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[4]]) . "'>" . utf8_encode($aRow[$aColumns[4]]) . "</div>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[5]]) . "'>" . utf8_encode($aRow[$aColumns[5]]) . "</div></center>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[6]]) . "'>" . utf8_encode($aRow[$aColumns[6]]) . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[7]]) . "'>" . utf8_encode($aRow[$aColumns[7]]) . "</div>";
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}

odbc_close($con);
