<?php
include '../../Connections/configini.php';

$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_veiculo_tipos 
        set descricao = " . valoresTexto('txtdescricao') . "," . 
        "v_tipo = " . valoresSelect('txtTipo') . "," .
        "v_regiao = " . valoresSelect('txtGrupoCidade') . "," .
        "v_cod = " . valoresNumericos('txtCodSinc') . "," .
        "v_cod_rastreio = " . valoresNumericos('txtCodRast') . "," .                
        "v_ordem = " . valoresNumericos('txtOrdem') . "," .                
        "v_inativo = " . valoresSelect('ckbInativo') . 
        " where id = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_veiculo_tipos(descricao, v_tipo, v_regiao, v_cod, v_cod_rastreio, v_ordem, v_inativo) values (" .
        valoresTexto('txtdescricao') . "," . valoresSelect('txtTipo') . "," . 
        valoresSelect('txtGrupoCidade') . "," . valoresNumericos('txtCodSinc') . "," . 
        valoresNumericos('txtCodRast') . "," . valoresNumericos('txtOrdem') . "," . 
        valoresSelect('ckbInativo') . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id from sf_veiculo_tipos order by id desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_veiculo_tipos WHERE id = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_veiculo_tipos where id =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id'];
        $descricao = $RFP['descricao'];        
        $tipo = $RFP['v_tipo'];
        $cod_sinc = $RFP['v_cod'];
        $grupo_cidade = $RFP['v_regiao'];
        $cod_rast = $RFP['v_cod_rastreio'];
        $clbInativo = $RFP['v_inativo'];
        $ordem = $RFP['v_ordem'];
    }
} else {
    $disabled = '';
    $id = '';
    $descricao = '';    
    $tipo = '';
    $cod_sinc = '';
    $grupo_cidade = '';     
    $cod_rast = '';
    $clbInativo = '';
    $ordem = '';
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src='../../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<body>
    <form action="FormVeiculoTipo.php" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Tipo de Veículo</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont">
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <div style="width: 100%; float: left">
                <span>Descrição:</span>
                <input name="txtdescricao" id="txtdescricao" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="128" value="<?php echo utf8_encode($descricao); ?>"/>
            </div> 
            <div style="width:49%; float:left; ">
                <span>Tipo:</span>
                <select id="txtTipo" name="txtTipo" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>                                
                    <option value="null">Selecione</option>
                    <option value="1" <?php echo ($tipo == "1" ? "selected" : "");?>>Carro</option>
                    <option value="2" <?php echo ($tipo == "2" ? "selected" : "");?>>Moto</option>
                    <option value="3" <?php echo ($tipo == "3" ? "selected" : "");?>>Caminhão</option>                                
                </select>
            </div>
            <div style="width:50%; float:left; margin-left: 1%;">
                <span>Grupo de Cidade:</span>
                <select id="txtGrupoCidade" name="txtGrupoCidade" class="select" style="width:100%" <?php echo $disabled; ?>>
                    <option value="null">Selecione</option>
                    <?php $cur = odbc_exec($con, "select id, descricao from sf_regioes order by descricao") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                        <option value="<?php echo $RFP['id'] ?>"<?php
                        if (!(strcmp($RFP['id'], $grupo_cidade))) {
                            echo "SELECTED";
                        } ?>><?php echo utf8_encode($RFP['descricao']);?></option>
                    <?php } ?>
                </select>
            </div>            
            <div style="width: 24%; float: left">
                <span>Cod. Assistência:</span>
                <input name="txtCodSinc" id="txtCodSinc" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="3" value="<?php echo utf8_encode($cod_sinc); ?>"/>
            </div>            
            <div style="width: 27%; float: left; margin-left: 1%;">
                <span>Cod. Rastreamento:</span>
                <input name="txtCodRast" id="txtCodRast" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="3" value="<?php echo utf8_encode($cod_rast); ?>"/>
            </div>                                    
            <div style="width: 25%; float:left; margin-left: 1%;">
                <span>Inativo:</span>
                <select id="ckbInativo" name="ckbInativo" class="select" style="width:100%;" <?php echo $disabled; ?>>
                    <option value="0" <?php if($clbInativo == "0"){ echo "selected";} ?>>NÃO</option>            
                    <option value="1" <?php if($clbInativo == "1"){ echo "selected";} ?>>SIM</option>
                </select>
            </div>                         
            <div style="width: 21%; float: left; margin-left: 1%;">
                <span>Ordem:</span>
                <input name="txtOrdem" id="txtOrdem" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="3" value="<?php echo utf8_encode($ordem); ?>"/>
            </div>            
            <div style="clear:both;"></div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" name="bntSave" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK" ><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else {
                    ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type="text/javascript" src="../../../js/util.js"></script>    
    <?php odbc_close($con); ?>
</body>
