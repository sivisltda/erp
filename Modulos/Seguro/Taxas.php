<?php
include "./../../Connections/configini.php";
$DateBegin = escreverDataSoma(getData("B"), " -1 month");
$DateEnd = escreverDataSoma(getData("B"), " -1 day");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../SpryAssets/SpryValidationTextField.css"/>
        <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>        
        <style>
            #tbReceita td:last-child, #tbReceita th:last-child { text-align:right }
            #tbReceita td { padding:5px 10px }
            
            #dtReceita td:last-child, #dtReceita th:last-child { text-align:right }
            #dtReceita td { padding:5px 10px }
            #dtDespesas td:last-child, #dtDespesas th:last-child { text-align:right }
            #dtDespesas td { padding:5px 10px }
            #dtCaixa td:last-child, #dtCaixa th:last-child { text-align:right }
            #dtCaixa td { padding:5px 10px }
            
            #tbReceitaAnalitico td { padding:5px 10px }
            #tbReceitaAnalitico td:nth-child(7), #tbReceitaAnalitico th:nth-child(7) { text-align:right }
            #tbReceitaAnalitico td:nth-child(8), #tbReceitaAnalitico th:nth-child(8) { text-align:right }
            #tbReceitaAnalitico td:last-child, #tbReceitaAnalitico th:last-child { text-align:right }
            .boxheadTop { display: flex; justify-content: space-around; }
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"><span class="ico-arrow-right"></span></div>
                        <h1>SIVIS B.I.<small>Demonstração do Resultado do Exercício</small></h1>
                        <input name="txtId" id="txtId" type="hidden"/>
                        <input name="txtSendSms" id="txtSendSms" type="hidden"/>
                        <input name="txtSendEmail" id="txtSendEmail" type="hidden"/>
                        <input name="txtTotalSucesso" id="txtTotalSucesso" type="hidden"/>
                        <input name="txtTotalErro" id="txtTotalErro" type="hidden"/>                        
                    </div>
                    <div class="tabbable frmtabs" style="padding: 0px;">
                        <ul class="nav nav-tabs" style="margin-bottom:0px">
                            <li class="active"><a href="#tab1" data-toggle="tab"><b>DRE</b></a></li>
                            <?php if ($mdl_seg_ == 1) { ?>
                            <li><a href="#tab2" data-toggle="tab" id="tab_rateio"><b>RATEIO</b></a></li>                    
                            <?php } ?>
                        </ul>
                        <div class="tab-content frmcont" style="overflow:initial; border:1px solid #ddd; border-top:0px; padding-top:10px; overflow-y: auto">
                            <div class="tab-pane active" id="tab1" style="margin-bottom:5px">                                
                                <div style="float: left; width: 100%; margin-bottom: 5px;">
                                    <div style="float: left; margin-left: 0.5%;">
                                        <span>Mês:</span>
                                        <select id="txtMes">
                                            <option value="01" <?php echo (date('m') == "1" ? "selected" : ""); ?>>Janeiro</option>
                                            <option value="02" <?php echo (date('m') == "2" ? "selected" : ""); ?>>Fevereiro</option>
                                            <option value="03" <?php echo (date('m') == "3" ? "selected" : ""); ?>>Março</option>
                                            <option value="04" <?php echo (date('m') == "4" ? "selected" : ""); ?>>Abril</option>
                                            <option value="05" <?php echo (date('m') == "5" ? "selected" : ""); ?>>Maio</option>
                                            <option value="06" <?php echo (date('m') == "6" ? "selected" : ""); ?>>Junho</option>
                                            <option value="07" <?php echo (date('m') == "7" ? "selected" : ""); ?>>Julho</option>
                                            <option value="08" <?php echo (date('m') == "8" ? "selected" : ""); ?>>Agosto</option>
                                            <option value="09" <?php echo (date('m') == "9" ? "selected" : ""); ?>>Setembro</option>
                                            <option value="10" <?php echo (date('m') == "10" ? "selected" : ""); ?>>Outubro</option>
                                            <option value="11" <?php echo (date('m') == "11" ? "selected" : ""); ?>>Novembro</option>
                                            <option value="12" <?php echo (date('m') == "12" ? "selected" : ""); ?>>Dezembro</option>
                                        </select>
                                    </div>
                                    <div style="float: left; width: 60px; margin-left: 0.5%;">
                                        <span>Ano:</span>
                                        <input name="txtAno" id="txtAno" type="text" class="input-medium" maxlength="4" value="<?php echo date("Y"); ?>"/>
                                    </div>                                    
                                    <div style="float: left;margin-top: 15px;margin-left: 0.5%;">
                                        <button class="button button-turquoise btn-primary" type="button" name="btnBuscar" id="btnBuscar"><span class="ico-search icon-white"></span></button> 
                                    </div>                                                                
                                </div>   
                                <div style="float: left; width: 100%;">
                                    <div style="float: left; width: 49%;">
                                        <div class="boxhead boxheadTop">                          
                                            <div class="boxtext">Distribuição de Receitas</div>
                                            <b><div id="totReceita" class="boxtext">R$ 0,00</div></b>
                                        </div>
                                        <div class="boxtable" style="min-height: 200px;">
                                            <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="dtReceita">                                            
                                                <thead>
                                                    <tr>
                                                        <th width="10%"></th>
                                                        <th width="65%">Itens</th>
                                                        <th width="25%">Valor</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>   
                                    <div style="float: left; width: 49%; margin-left: 1%;">
                                        <div class="boxhead">                          
                                            <div class="boxtext">Saldo em Caixa</div>
                                        </div>
                                        <div class="boxtable" style="min-height: 200px;">
                                            <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="dtCaixa">
                                                <thead>
                                                    <tr>
                                                        <th>Itens</th>
                                                        <th>Valor</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Total</th>
                                                        <th><div id="totCaixa"></div></th>
                                                    </tr>
                                                </tfoot>                                            
                                            </table>
                                        </div>
                                    </div>                                                
                                </div>    
                                <div style="float: left; width: 100%;">
                                    <div style="float: left; width: 49%;">
                                        <div class="boxhead boxheadTop">                          
                                            <div class="boxtext">Distribuição de Despesas</div>
                                            <b><span id="totDespesas" class="boxtext">R$ 0,00</span>
                                        </div>
                                        <div class="boxtable" style="min-height: 200px;">
                                            <div style="margin-top: -5px; margin-bottom: 5px;">                                                
                                                <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir('I');" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                                <button id="btnExcel" class="button button-blue btn-primary" type="button" onclick="imprimir('E');" title="Exportar Excel"><span class="ico-download-3"></span></button></b>                                                
                                                <button id="btnDocs" class="button button-red btn-primary" type="button" onclick="documentos();" title="Imprimir Recibos"><span class="ico-print icon-white"></span></button>                                                
                                            </div>
                                            <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="dtDespesas">
                                                <thead>
                                                    <tr>
                                                        <th width="10%"></th>
                                                        <th width="65%">Itens</th>
                                                        <th width="25%">Valor</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>                                    
                                    <div style="float: left; width: 49%; margin-left: 1%; border: 1px solid #D8D8D8; text-align: center; min-height: 256px;">
                                        <div style="float: left; width: 100%; text-align: center">                                            
                                            <div style="border: 1px solid #D8D8D8; background-color: white; width: 50%; height: 75%; margin: 5% 25% 0% 25%;">
                                                <div style="width: 90%; font-weight: bold; padding: 10px;">
                                                    DÉFICIT/SUPERÁVIT
                                                </div>
                                                <div style="width: 90%; font-weight: bold; padding: 10px; font-size: 25px;">
                                                    <div id="totSaldo">R$ 0,00</div>
                                                </div>
                                            </div>
                                        </div>    
                                        <div style="float: left; width: 100%; text-align: center; margin-top: 10px">    
                                            <div style="float: left; width: 100%; margin-top: 20px;">
                                                <button class="btn green" style="width: 220px;" type="button" onclick="AbrirBoxTr()"><span class="ico-file-4 icon-white"></span> Transferência entre Contas</button>
                                            </div>
                                            <?php if ($mdl_seg_ == 1) { ?>
                                            <div style="float: left; width: 100%; margin-top: 20px;">                                    
                                                <button class="btn yellow" style="width: 220px;" type="button" onclick="$('#tab_rateio').click();"><span class="ico-checkmark icon-white"></span> Distribuição de Rateio</button>
                                            </div>
                                            <?php } ?>                                            
                                        </div>
                                    </div>
                                </div>   
                                <div style="float: left; width: 100%;">
                                    <div style="float: left; width: 100%;">
                                        <div class="boxhead">                          
                                            <div id="lblDetalhes" class="boxtext">Detalhes</div>
                                        </div>
                                        <div class="boxtable" style="min-height: 200px;">
                                            <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="dtDetalhes">
                                                <thead>
                                                    <tr>
                                                        <th width="10%">Data</th>
                                                        <th width="20%">Nome</th>
                                                        <th width="20%">Grupo</th>
                                                        <th width="20%">Item</th>
                                                        <th width="20%">Histórico</th>
                                                        <th width="10%">Valor</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th colspan="5">Total</th>
                                                        <th><div id="totDetalhes">R$ 0,00</div></th>
                                                    </tr>
                                                </tfoot>                                            
                                            </table>
                                        </div>
                                    </div>                                
                                </div>                                
                            </div>
                            <div class="tab-pane" id="tab2">                                
                                <div class="row-fluid" style="margin-bottom: 5px;">                                                               
                                    <div style="float: left;">
                                        <label for="txt_dt_begin" style="text-align: right;">Período de</label>                                
                                        <input type="text" maxlength="7" style="width:80px; height: 31px;" class="datepicker" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial"/>
                                    </div>
                                    <div style="float: left;margin-left: 0.5%;">
                                        <label for="txt_dt_end">participação</label>                                    
                                        <input type="text" maxlength="7" style="width:80px; height: 31px;" class="datepicker" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final"/>                             
                                    </div>                                                                                               
                                    <div style="float: left;margin-top: 15px;margin-left: 0.5%;">
                                        <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind"><span class="ico-search icon-white"></span></button> 
                                    </div>
                                    <div style="float: right;margin-top: 15px;margin-left: 0.5%;">
                                        <button id="btnLancamento" class="button button-orange" onclick="lancamento()" disabled>Criar</button>                        
                                    </div>                        
                                    <div style="float: right;margin-left: 0.5%;">
                                        <label for="txt_dt_cobranca">Dt. Cobrança</label>                                
                                        <input type="text" maxlength="7" style="width:80px; height: 31px;" class="datepicker" id="txt_dt_cobranca" name="txt_dt_cobranca" value="<?php echo getData("T"); ?>" placeholder="Data Cobrança" disabled/>
                                    </div>                                                
                                    <div style="float: right;margin-top: 15px;margin-left: 0.5%;">
                                        <button class="button button-turquoise btn-primary" type="button" onclick="calculate()"><span class="ico-plus icon-white"></span></button> 
                                    </div>                                                
                                    <div style="float: right;margin-left: 0.5%;">
                                        <label for="txt_limite">Limite</label>                                    
                                        <input type="text" style="width:80px; height: 31px;" id="txt_limite" value="0,00" placeholder="0,00"/>                             
                                    </div>                        
                                    <div style="float: right;margin-left: 0.5%;">
                                        <label for="txt_valor">Rateio</label>                                    
                                        <input type="text" style="width:80px; height: 31px;" id="txt_valor" value="0,00" placeholder="0,00"/>                             
                                    </div>                                                                                                                                                                                                                                                              
                                </div>
                                <div style="float: left; width: 70%;">
                                    <div class="boxhead">                          
                                        <div class="boxtext">Distribuição de Cotas</div>
                                    </div>
                                    <div class="boxtable" style="min-height: 380px;">
                                        <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbReceita">
                                            <thead>
                                                <tr>                                        
                                                    <th width="35%">Plano</th>
                                                    <th width="13%">Planos Ativos</th>                                        
                                                    <th width="13%">Valor Mensal</th>
                                                    <th width="13%">Valor Individual</th>
                                                    <th width="13%">Valor Excedido</th>                                        
                                                    <th width="13%">Valor Total</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Valor total</th>
                                                    <th><div id="txtQtd">0</div></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th><div id="txtTotal">0,00</div></th>
                                                </tr>
                                            </tfoot>
                                        </table>                                                                                    
                                    </div>
                                </div>
                                <div style="float: left; width: 29%; margin-left: 1%;">
                                    <div class="boxhead">
                                        <div class="boxtext">Histórico de lançamentos</div>
                                    </div>
                                    <div class="boxtable" style="min-height: 380px;">
                                        <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbHistorico">
                                            <thead>
                                                <tr>
                                                    <th width="20%">DATA</th>
                                                    <th width="20%"><center>VALOR</center></th>
                                                    <th width="20%"><center>LIMITE</center></th>
                                                    <th width="20%"><center>ITENS</center></th>
                                                    <th width="20%"><center>AÇÃO</center></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>                                                                                    
                                    </div>                        
                                </div>
                                <div style="clear:both"></div>
                                <span id="divAnalitico">
                                    <div class="boxhead">
                                        <div class="boxtext" style="position:relative">
                                            <div id="lbltitulo" class="boxtext">Detalhes</div>
                                            <input id="txtAnalitico" type="hidden" value="0"/>
                                            <div style="top:4px; right:2px; line-height:1; position:absolute">
                                                <button type="button" title="Imprimir" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printProdutividade(1)">
                                                    <span class="ico-print icon-white"></span>
                                                </button>
                                                <button type="button" title="Exportar Excel" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printProdutividade(2);">
                                                    <span class="ico-download-3 icon-white"></span>
                                                </button>
                                                <button id="btnEmail" title="Enviar E-mail" class="button button-turquoise btn-primary" type="button" onclick="AbrirBox(5, 0)">
                                                    <span class="ico-envelope-3 icon-white"></span>
                                                </button>
                                                <button type="button" title="Enviar SMS" class="button button-blue btn-primary buttonX" onclick="AbrirBox(6, 0);">
                                                    <span class="ico-comment icon-white"></span>
                                                </button>
                                                <?php if ($mdl_wha_ > 0) { ?>
                                                <button type="button" title="Enviar Whatsapp" class="button button-blue btn-primary buttonX" onclick="AbrirBox(7, 0);">
                                                    <span class="ico-phone-4 icon-white"></span>
                                                </button>
                                                <?php } ?>
                                            </div>
                                        </div>                            
                                    </div>
                                    <div class="boxtable">
                                        <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbReceitaAnalitico">
                                            <thead>
                                                <tr>                           
                                                    <th width="5%"><center>Mat.</center></th>
                                                    <th width="29%"><center>Nome</center></th>
                                                    <th width="15%">Período.</th>
                                                    <th width="7%">Status.</th>
                                                    <th width="11%">Consultor</th>
                                                    <th width="10%"><center>Telefone</center></th>
                                                    <th width="10%"><center>Celular</center></th>
                                                    <th width="13%"><center>Email</center></th>                            
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="7" style="text-align:right">Total:</th>
                                                    <th><div id="txtTotalAnalitico"></div></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" style="display: none;" title="Processando">
            <div id="progressbar"></div>
            <div style="display: flex;align-items: center;justify-content: center;margin-top: 20px;">
                <div id="pgIni">0</div><div style="padding: 5px;">até</div><div id="pgFim">0</div>            
            </div>            
        </div>        
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>   
        <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>        
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/jquery.priceformat.min.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">            

            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
            $("#txt_valor, #txt_limite").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
            
            function finalFindResumo(ts, cs) {
                return "Taxas_Valores.php?tipo=" + ts + (cs > 0 ? "&id_centro_custo=" + cs : "") + "&data=01_" + $('#txtMes').val() + "_" + $('#txtAno').val();                
            }      
            
            var dtReceita = $('#dtReceita').dataTable({
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                bSort: false,
                sAjaxSource: finalFindResumo(1, 0),
                "fnDrawCallback": function (data) {
                    $("#loader").hide();
                    let planos = 0;
                    for (var i = 0; i < data["aoData"].length; i++) {
                        planos += textToNumber(data["aoData"][i]["_aData"][2]);
                    }
                    $("#totReceita").html(numberFormat(planos, 1));
                    saldoTotal();                    
                },                 
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"
                }
            });            
            
            var dtDespesas = $('#dtDespesas').dataTable({
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                bSort: false,
                sAjaxSource: finalFindResumo(2, 0),
                "fnDrawCallback": function (data) {
                    $("#loader").hide();
                    let planos = 0;
                    for (var i = 0; i < data["aoData"].length; i++) {
                        planos += textToNumber(data["aoData"][i]["_aData"][2]);
                    }
                    $("#totDespesas").html(numberFormat(planos, 1));
                    saldoTotal();                    
                },                 
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"
                }
            });  
            
            function imprimir(tipo) {
                var pRel = "&NomeArq=Distribuição de Despesas" + 
                        "&lbl=Data|Nome|Grupo|Histórico|Valor" +
                        "&siz=60|250|120|200|70" +
                        "&pdf=3" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + $("#txtMes option:selected").text() + "/" + $("#txtAno").val() + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/Seguro/" + finalFindResumo(2, 0).replace("?", "&"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tipo + "&PathArq=GenericModelPDF.php", '_blank');
            }
            
            function documentos() {
                window.open("../Seguro/Taxas_Recibo.php?data=01_" + $('#txtMes').val() + "_" + $('#txtAno').val(), '_blank');
            }
            
            var dtDetalhes = $('#dtDetalhes').dataTable({
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                bSort: false,
                sAjaxSource: finalFindResumo(0, 0),
                "fnDrawCallback": function (data) {
                   $("#loader").hide();
                    let planos = 0;
                    for (var i = 0; i < data["aoData"].length; i++) {
                        planos += textToNumber(data["aoData"][i]["_aData"][5]);
                    }
                    $("#totDetalhes").html(numberFormat(planos, 1));                   
                },                 
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"
                }
            }); 
                        
            function AnaliticoValor(tipo, id, descricao) {
                $("#loader").show();
                $("#lblDetalhes").text(descricao);
                dtDetalhes.fnReloadAjax(finalFindResumo(tipo, 0) + "&id_contas=" + id);
            }              
            
            var dtCaixa = $('#dtCaixa').dataTable({
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                bSort: false,
                sAjaxSource: finalFindResumo(3, 0),
                "fnDrawCallback": function (data) {
                    $("#loader").hide();
                    let planos = 0;
                    for (var i = 0; i < data["aoData"].length; i++) {
                        planos += textToNumber(data["aoData"][i]["_aData"][1]);
                    }
                    $("#totCaixa").html(numberFormat(planos, 1));
                },                 
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"
                }
            });  
            
            function saldoTotal() {
                let credito = textToNumber($("#totReceita").html());
                let debito = textToNumber($("#totDespesas").html());
                $("#totSaldo").html(numberFormat((credito - debito), 1));
                $("#totSaldo").css("color", ((credito - debito) > 0 ? "#68AF27" : "red"));
            }
            
            function AbrirBoxTr() {
                abrirTelaBox("../Contas-a-pagar/Transferencia-entre-Bancos.php", 380, 430);                   
            }            
            
            $("#btnBuscar").click(function () {
                $("#loader").show();
                dtReceita.fnReloadAjax(finalFindResumo(1, 0));
                dtDespesas.fnReloadAjax(finalFindResumo(2, 0));
            });          

            function finalFind(ts) {
                var retPrint = "?ts=" + ts;                
                if ($('#txt_dt_begin').val() !== "") {
                    retPrint = retPrint + "&dti=" + $('#txt_dt_begin').val().replace(/\//g, "_");
                }                
                if ($('#txt_dt_end').val() !== "") {
                    retPrint = retPrint + "&dtf=" + $('#txt_dt_end').val().replace(/\//g, "_");
                }                
                if ($('#txtAnalitico').val() !== "") {
                    retPrint = retPrint + "&prd=" + $('#txtAnalitico').val();
                }                
                return "Taxas_server_processing.php" + retPrint;
            }                                    
            
            var tbReceita = $('#tbReceita').dataTable({
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                bSort: false,
                sAjaxSource: finalFind(0),
                "fnDrawCallback": function (data) {
                    $("#loader").hide();
                    let planos = 0;
                    for (var i = 0; i < data["aoData"].length; i++) {
                        planos += textToNumber(data["aoData"][i]["_aData"][1]);
                    }
                    $("#txtQtd").html(planos);
                },                 
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"
                }
            });            
            
            var tbReceitaAnalitico = $('#tbReceitaAnalitico').dataTable({
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                bSort: false,
                sAjaxSource: finalFind(1), 
                "fnDrawCallback": function (data) {
                    $("#loader").hide();
                    $("#txtTotalAnalitico").html(data["aoData"].length);
                },                
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"
                }           
            });
            
            $("#btnfind").click(function () {
                $("#loader").show();
                tbReceita.fnReloadAjax(finalFind(0));
                Analitico(0, 'Detalhes');
            });
            
            function Analitico(id, descricao) {
                $("#loader").show();
                $("#txtAnalitico").val(id);
                $("#lbltitulo").text(descricao);
                tbReceitaAnalitico.fnReloadAjax(finalFind(1));  
            }
            
            function printProdutividade(imp) {
                var pRel = "&NomeArq=" + "Distribuição de Taxas" + "&pOri=L" +
                        "&lbl=" + "Mat.|Nome|Período|Status|Consultor|Telefone|Celular|Email" +
                        "&siz=" + "40|225|170|95|100|100|100|180" +
                        "&pdf=" + "8" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + $("#lbltitulo").text() + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/Seguro/" + finalFind(1).replace("?", "&");
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&imp=" + imp + "&PathArq=GenericModelPDF.php", '_blank');
            }
            
            function AbrirBox(opc, id) {
                $("#txtId").val("");
                if (opc === 1) {
                    let url = "./../CRM/CRMForm.php?id=" + id;
                    window.open(url,'_blank');
                }
                if (opc === 2) {
                    let url = "./../Academia/ClientesForm.php?id=" + id;
                    window.open(url,'_blank');
                }
                if (opc === 5) {                    
                    abrirTelaBox("../CRM/EmailsEnvioForm.php", 650, 720);
                }
                if (opc === 6) {
                    abrirTelaBox("../CRM/SMSEnvioForm.php", 400, 400);
                }
                if (opc === 7) {
                    abrirTelaBox("../CRM/SMSEnvioForm.php?zap=s", 400, 400);
                }                
            }
            
            function abrirTelaSMS(sms, zap, id) {                               
                $("#txtId").val(id);                
                var link = "../CRM/SMSEnvioForm.php" + (zap === true ? "?zap=s" : "");
                abrirTelaBox(link, 265, 400);
                $("#txtSendSms").val(sms);
            }
            
            function abrirTelaEmail(email, id) {  
                $("#txtId").val(id);                
                abrirTelaBox("../CRM/EmailsEnvioForm.php", 650, 720);
                $("#txtSendEmail").val(email);
            }
            
            function FecharBox(opc) {
                $("#newbox").remove();
                dtCaixa.fnReloadAjax(finalFindResumo(3, 0));
            }
            
            function listaSMS() {
                var sms = [];
                if ($("#txtId").val() === "") {
                    $.ajax({                    
                        url: finalFind(1),
                        async: false,
                        dataType: "json",
                        success: function (j) {
                            for (var x in j.aaData) {
                                if (j.aaData[x][7].trim() !== ""){               
                                    sms.push($(j.aaData[x][7].replace(/ /g, "")).text() + "|" + j.aaData[x][0]);
                                }
                            }
                        }
                    });                    
                } else {
                    sms.push($("#txtSendSms").val() + "|" + $("#txtId").val());    
                }
                return sms;
            }
            
            function listaEmails() {                               
                var emails = [];
                if ($("#txtId").val() === "") {
                    $.ajax({
                        url: finalFind(1),
                        async: false,
                        dataType: "json",
                        success: function (j) {
                            for (var x in j.aaData) {
                                if (j.aaData[x][8].trim() !== "") {
                                    emails.push($(j.aaData[x][8].replace(/ /g, "")).text() + "|" + j.aaData[x][0]);
                                }
                            }
                        }
                    });
                } else {
                    emails.push($("#txtSendEmail").val() + "|" + $("#txtId").val());
                }
                return emails;
            }
            
            function calculate() {     
                if (textToNumber($("#txt_valor").val()) > 0) {                    
                    let total = 0.00;
                    let totalGerado = 0.00;                
                    $("#txt_dt_cobranca, #btnLancamento").attr("disabled", false);                    
                    $("#tbReceita tbody tr").each(function(index, tr) { 
                        let itens = $(tr).find("td");
                        let pessoas = textToNumber(itens[1].innerText);
                        let val_plano = textToNumber(itens[2].innerText);
                        let item = (pessoas * val_plano);
                        total += item;
                    });                
                    $("#tbReceita tbody tr").each(function(index, tr) { 
                        let itens = $(tr).find("td");
                        let pessoas = textToNumber(itens[1].innerText);
                        let val_plano = textToNumber(itens[2].innerText);                    
                        let item = ((val_plano * textToNumber($("#txt_valor").val()))/total);
                        let limite = textToNumber($("#txt_limite").val());
                        itens[3].innerText = numberFormat(item, 1);
                        itens[4].innerText = numberFormat((limite > 0 && item > limite ? (item - limite) : 0), 1);
                        itens[5].innerText = numberFormat((item * pessoas), 1);
                        totalGerado += (item * pessoas);
                    });
                    $("#txtTotal").html(numberFormat(totalGerado, 1));
                } else {
                    $("#txt_dt_cobranca, #btnLancamento").attr("disabled", true);
                    bootbox.alert("Preencha um valor superior a zero!");
                }
            }
            
            function lancamento(){
                bootbox.confirm('Confirma a distribuição de cotas no valor de R$ ' + $("#txt_valor").val() + 
                    ' com cobrança para o dia ' + $("#txt_dt_cobranca").val() + '?', function (result) {
                    if (result === true) {
                        $("#source").dialog({modal: true});
                        $("#progressbar").progressbar({value: 0});
                        $("#pgIni").html("0");
                        $("#pgFim").html($("#tbReceita tbody tr").length);
                        $.post("../Seguro/form/FormTaxas.php", "bntCota=S" + 
                        "&txtValorCob=" + $("#txt_valor").val() + 
                        "&txtValorLim=" + $("#txt_limite").val() + 
                        "&txtDtCobranca=" + $("#txt_dt_cobranca").val()).done(function (data) {
                            if ($.isNumeric(data.trim())) {
                                processar(data.trim());
                            } else {
                                bootbox.alert("Erro de lançamento de lote!");
                                refreshHistorico();
                                $("#source").dialog('close');
                            }    
                        });
                    } else {
                        return;
                    }
                });
            }
            
            function processar(lote) {
                var PgIni = parseInt($("#pgIni").html());
                var PgFim = parseInt($("#pgFim").html());
                if ((PgIni < PgFim) && PgFim > 0 && $('#source').dialog('isOpen')) {
                    let selectedRow = $($("#tbReceita tbody tr")[PgIni]).find("td");
                    let plano = $(selectedRow[0]).html().split('Analitico(')[1].split(',')[0].replace(/\s/g, '');
                    let valor = $(selectedRow[3]).html();                    
                    let limite = $(selectedRow[4]).html();                    
                    $("#progressbar").progressbar({value: ((PgIni * 100) / PgFim)});
                    $("#pgIni").html(PgIni + 1);
                    $.post("../Seguro/form/FormTaxas.php", "bntSave=S&txtId=" + lote + "&txtPlano=" + plano + 
                            "&txtValor=" + valor + "&txtLimite=" + limite +
                            "&txtDtBegin=" + $("#txt_dt_begin").val() + "&txtDtEnd=" + $("#txt_dt_end").val() + 
                            "&txtDtCobranca=" + $("#txt_dt_cobranca").val()).done(function (data) {
                        if (data.trim() === "YES") {
                            $(selectedRow[3]).html("");
                            $(selectedRow[4]).html("");
                            $(selectedRow[5]).html("");
                        }
                        processar(lote);
                    });
                } else {
                    $("#txtTotal").html("0,00");
                    $("#txt_valor").val("0,00");
                    $("#txt_limite").val("0,00");
                    $("#txt_dt_cobranca, #btnLancamento").attr("disabled", true);
                    refreshHistorico();
                    $("#source").dialog('close');
                }
            }
            
            var tbHistorico = $('#tbHistorico').dataTable({
                "iDisplayLength": 20,
                "aLengthMenu": [20, 30, 40, 50, 100],
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "ordering": false,
                "sAjaxSource": "Taxas_Historico_server_processing.php",
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"},
                "sPaginationType": "full_numbers"
            });
            
            function refreshHistorico() {
                tbHistorico.fnReloadAjax("Taxas_Historico_server_processing.php");
            }
            
            function RemoverLote(id) {
                bootbox.confirm('Confirma a exclusão deste lançamento?', function (result) {
                    if (result === true) {
                        $.post("../Seguro/form/FormTaxas.php", "bntDelete=S&txtId=" + id).done(function (data) {
                            if (data.trim() !== "YES") {
                                bootbox.alert("Erro ao excluir este lançamento!");
                            }    
                            refreshHistorico();
                        });
                    } else {
                        return;
                    }
                });
            }
            
            $('#dtReceita tbody td img').live('click', function () {
                var id = $(this).attr('data-id');
                var nTr = $(this).parents('tr')[0];
                var aData = $('#dtReceita').dataTable().fnGetData(nTr);
                if (id === "0") {
                    if ($('#dtReceita').dataTable().fnIsOpen(nTr)) {
                        this.src = "./../../img/details_open.png";
                        $('#dtReceita').dataTable().fnClose(nTr);
                    } else {
                        this.src = "./../../img/details_close.png";
                        fnFormatDetailsReceita(nTr, id);
                    }
                }
            });

            function fnFormatDetailsReceita(nTr, id) {
                $("#loader").show();
                $.getJSON(finalFindResumo(4, 0), function (j) {
                    $("#loader").hide();
                    var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="width: 100%;">';                                        
                    for (var i = 0; i < j.aaData.length; i++) {
                        sOut += '<tr><td style="width: 10%;"></td><td style="width: 70%;">' + j.aaData[i][1] + '</td><td style="width: 20%;">' + j.aaData[i][2] + '</td></tr>';
                    }
                    sOut += '</table>';
                    $('#dtReceita').dataTable().fnOpen(nTr, sOut, 'details');
                });
            }
            
            $('#dtDespesas tbody td img').live('click', function () {
                var id = $(this).attr('data-id');
                var nTr = $(this).parents('tr')[0];
                var aData = $('#dtDespesas').dataTable().fnGetData(nTr);
                if (aData[2].length > 0) {
                    if ($('#dtDespesas').dataTable().fnIsOpen(nTr)) {
                        this.src = "./../../img/details_open.png";
                        $('#dtDespesas').dataTable().fnClose(nTr);
                    } else {
                        this.src = "./../../img/details_close.png";
                        fnFormatDetailsDespesas(nTr, id);
                    }
                }
            });

            function fnFormatDetailsDespesas(nTr, id) {
                $("#loader").show();
                $.getJSON(finalFindResumo(2, id), function (j) {
                    $("#loader").hide();
                    var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="width: 100%;">';                                        
                    for (var i = 0; i < j.aaData.length; i++) {
                        sOut += '<tr><td style="width: 30%;">' + j.aaData[i][0] + '</td><td style="width: 50%;">' + j.aaData[i][1] + '</td><td style="width: 20%;">' + j.aaData[i][2] + '</td></tr>';
                    }
                    sOut += '</table>';
                    $('#dtDespesas').dataTable().fnOpen(nTr, sOut, 'details');
                });
            }
            
            function GerarBoletos(id) {
                bootbox.confirm('Confirma a geração do boleto com os itens selecionados?', function (value) {
                    if (value === true) {
                        $.post("../Seguro/form/FormTaxas.php", "bntMensLote=S&txtId=" + id).done(function (data) {
                            var response = jQuery.parseJSON(data);
                            $("#source").dialog({modal: true});
                            $("#progressbar").progressbar({value: 0});
                            $("#pgIni").html("0");
                            $("#pgFim").html(response.length);
                            $("#txtTotalSucesso, #txtTotalErro").val(0);
                            console.log(response);
                            processarBol(response);
                        });
                    } else {
                        return;
                    }
                });
            }
            
            function processarBol(lista) {
                var PgIni = parseInt($("#pgIni").html());
                var PgFim = parseInt($("#pgFim").html());
                if ((PgIni < PgFim) && PgFim > 0 && $('#source').dialog('isOpen')) {                    
                    $("#progressbar").progressbar({value: (((PgIni + 1) * 100) / PgFim)});
                    $("#pgIni").html((PgIni + 1));
                    $.ajax({
                        type: "POST", url: "../../util/dcc/makeBoletoOnline.php",
                        data: lista[PgIni], dataType: "json",
                        success: function (data) {
                            $("#txtTotalSucesso").val(parseInt($("#txtTotalSucesso").val()) + 1);
                            processarBol(lista);
                        },
                        error: function (error) {
                            $("#txtTotalErro").val(parseInt($("#txtTotalErro").val()) + 1);
                            processarBol(lista);    
                        }
                    });
                } else {
                    refreshHistorico();
                    $("#source").dialog('close');                    
                    bootbox.alert("Transações: " + $("#txtTotalSucesso").val() + " Aprovada(s) e " + $("#txtTotalErro").val() + " com erro(s)!");
                }
            }
            
            function printDetalhes(id) {
                var pRel = "&NomeArq=" + "Histórico de lançamentos" +
                "&lbl=" + "Mat.|Nome|Data|Valor|Placa|Boleto|Processado" +
                "&siz=" + "40|200|60|60|210|65|65" +
                "&pdf=" + "10" + // Colunas do server processing que não irão aparecer no pdf
                "&filter=" + //Label que irá aparecer os parametros de filtro
                "&PathArqInclude=" + "../Modulos/Seguro/form/FormTaxas.php&printDetalhes=S&id_lote=" + id;
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>