<?php
include '../../Connections/configini.php';

$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_vistorias_tipo set " .
                        "descricao = " . valoresTexto('txtdescricao') . "," .
                        "inativo = " . valoresCheck('ckbInativo') . "," .
                        "sinistro = " . valoresCheck('ckbSinistro') . " " .
                        "where id = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_vistorias_tipo(descricao, inativo, sinistro) values (" .
                        valoresTexto('txtdescricao') . "," .
                        valoresCheck('ckbInativo') . "," .
                        valoresCheck('ckbSinistro') . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id from sf_vistorias_tipo order by id desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
    $inValue = "0";
    for ($i = 0; $i < count($_POST['txtTipoVeiculo']); $i++) {
        if ($_POST['txtTipoVeiculo'][$i] != '') {
            $inValue .= "," . $_POST['txtTipoVeiculo'][$i];
        }
    }
    odbc_exec($con, "delete from sf_vistorias_veiculo_tipos where id_vistoria = " . $_POST['txtId'] . " and id_tipo not in (" . $inValue . ")") or die(odbc_errormsg());        
    for ($i = 0; $i < count($_POST['txtTipoVeiculo']); $i++) {        
        odbc_exec($con, "IF NOT EXISTS (SELECT id_vistoria, id_tipo FROM sf_vistorias_veiculo_tipos WHERE id_vistoria = " . $_POST['txtId'] . " AND id_tipo = " . $_POST['txtTipoVeiculo'][$i] . ")
        BEGIN INSERT INTO sf_vistorias_veiculo_tipos(id_vistoria, id_tipo) VALUES (" . $_POST['txtId'] . "," . $_POST['txtTipoVeiculo'][$i] . "); END");
    }
    if (isset($_POST['txtContatosTotal'])) {
        $max = $_POST['txtContatosTotal'];
        $notIn = '0';
        for ($i = 0; $i <= $max; $i++) {
            if (isset($_POST['txtIdItem_' . $i]) && is_numeric($_POST['txtIdItem_' . $i])) {
                $notIn .= "," . $_POST['txtIdItem_' . $i];
            }
        }
        odbc_exec($con, "delete from sf_vistorias_tipo_item where id_vistoria_tipo = " . $_POST['txtId'] . " and id not in (" . $notIn . ")");
        for ($i = 0; $i <= $max; $i++) {
            if (isset($_POST['txtIdItem_' . $i])) {
                if (is_numeric($_POST['txtIdItem_' . $i])) {                    
                    odbc_exec($con, "update sf_vistorias_tipo_item set " . 
                    "id_vistoria_tipo = " . $_POST['txtId'] . ",etapa = " . valoresNumericos('txtEtapa_' . $i) . "," . 
                    "imagem = " . valoresTexto('txtImagem_' . $i) . ",tipo = " . valoresTexto('txtTipo_' . $i) . "," . 
                    "descricao = " . valoresTexto('txtDescricao_' . $i) . ",observacao = " . valoresTexto('txtObservacao_' . $i) . 
                    " WHERE id = " . $_POST['txtIdItem_' . $i]) or die(odbc_errormsg());                    
                } else {
                    odbc_exec($con, "insert into sf_vistorias_tipo_item(id_vistoria_tipo,etapa,imagem,tipo,descricao,observacao) values(" .
                    $_POST['txtId'] . "," . valoresNumericos('txtEtapa_' . $i) . "," . 
                    valoresTexto('txtImagem_' . $i) . "," . valoresTexto('txtTipo_' . $i) . "," .
                    valoresTexto('txtDescricao_' . $i) . "," . valoresTexto('txtObservacao_' . $i) . ")") or die(odbc_errormsg());
                }
            }
        }
    }    
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_vistorias_veiculo_tipos WHERE id_vistoria = " . $_POST['txtId']);
        odbc_exec($con, "DELETE FROM sf_vistorias_tipo WHERE id = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_vistorias_tipo where id =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id'];
        $descricao = $RFP['descricao'];
        $inativo = $RFP['inativo'];
        $sinistro = $RFP['sinistro'];
    }
} else {
    $disabled = '';
    $id = '';
    $descricao = '';
    $inativo = '';
    $sinistro = '';
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../../js/plugins/select/select22.css"/>
<script type='text/javascript' src='../../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<body>
    <form action="FormVistoriaTipo.php" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Tipo de Vistoria</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="tabbable frmtabs">
            <ul class="nav nav-tabs" style="margin:0">
                <li class="active"><a href="#tab1" data-toggle="tab">Geral</a></li>
                <li><a href="#tab2" data-toggle="tab">Tipo</a></li>
            </ul>
            <div class="tab-content frmcont" style="height:366px; overflow:hidden;">
                <div class="tab-pane active" id="tab1">
                    <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                    <div style="width: 85%;float: left">
                        <span>Descrição:</span>
                        <input name="txtdescricao" id="txtdescricao" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="128" value="<?php echo utf8_encode($descricao); ?>"/>
                    </div>     
                    <div style="width: 13%;float:left;margin-left: 1%;display: flex;flex-direction: column;justify-content: center;">                               
                        <div style="display:flex;justify-content: space-between;">
                            <span>Evento:</span>
                            <input name="ckbSinistro" id="ckbSinistro" type="checkbox" <?php
                            if ($sinistro == "1") {
                                echo "checked";
                            }
                            ?> class="input-medium" <?php echo $disabled; ?> value="1" style="opacity: 0;"/>
                        </div>                                
                        <div style="display:flex;justify-content: space-between;">
                            <span>Inativo:</span>
                            <input name="ckbInativo" id="ckbInativo" type="checkbox" <?php
                            if ($inativo == "1") {
                                echo "checked";
                            }
                            ?> class="input-medium" <?php echo $disabled; ?> value="1" style="opacity: 0;"/>
                        </div>                
                    </div>
                    <div style="width: 100%; float: left;">
                        <input id="txtIdItemEdit" value="" type="hidden">
                        <span>Campos:</span>
                        <div style="width:100%; float:left">                  
                            <div style="background:#F1F1F1; border:1px solid #DDD; border-bottom:0; padding:10px 10px 0">
                                <input id="txtIdItem" value="" type="hidden"/>
                                <div style="float:left; width:15%;">
                                    <select id="txtEtapa" class="input-medium" style="width:100%" <?php echo $disabled; ?>>
                                        <?php for($i = 1; $i <= 15; $i++) { ?>
                                            <option value="<?php echo $i; ?>">Etapa <?php echo $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div style="float:left; width:15%; margin-left:1%;">
                                    <select id="txtImagem" name="txtImagem" style="width: 80px;" class="selectCr" <?php echo $disabled;?>>                                  
                                        <?php foreach (scandir("./Api/img/") as $object) {
                                            if ($object != "." && $object != "..") { ?>
                                            <option value="<?php echo $object; ?>"></option>
                                        <?php }} ?>                                       
                                    </select>
                                </div>
                                <div style="float:left; width:15%; margin-left:1%;">
                                    <select id="txtTipo" class="input-medium" style="width:100%" <?php echo $disabled; ?>>                                
                                        <option value="imagem">Imagem</option>                                
                                        <option value="video">Vídeo</option>
                                        <option value="button">Finalizar</option>
                                    </select>
                                </div>                        
                                <div style="float:left; width:43%; margin-left:1%;">
                                    <input id="txtDescricao" placeholder="Descrição" type="text" class="input-medium" maxlength="512" autocomplete="off" <?php echo $disabled; ?>>
                                </div>                        
                                <div style="float:left; width:8%; margin-left:1%;">
                                    <button type="button" onclick="addCampos();" class="btn dblue" <?php echo $disabled; ?> style="height: 26px;background-color: #308698 !important;">
                                        <span style="margin-top: 4px;" class="ico-plus icon-white"></span>
                                    </button>                            
                                </div>                        
                                <div style="float:left; width:100%; margin-top: 1%;">
                                    <textarea id="txtObservacao" placeholder="Observação" rows="8" cols="80" <?php echo $disabled; ?>></textarea>
                                </div>                        
                                <div style="clear:both"></div>                        
                            </div>
                            <div style="background:#F1F1F1; border:1px solid #DDD; border-top:0; padding:5px 10px 10px">
                                <div id="divContatos" style="height:195px; background:#FFF; border:1px solid #DDD; overflow-y:scroll;">
                                <?php
                                    $i = 0;
                                    if (is_numeric($id)) {
                                        $cur = odbc_exec($con, "select * from sf_vistorias_tipo_item where id_vistoria_tipo = " . $id . " order by id") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { $i++; ?>
                                        <div id="tabela_linha_<?php echo $i; ?>" style="padding:2px; border-bottom:1px solid #DDD; overflow: hidden;">
                                            <input id="txtIdItem_<?php echo $i; ?>" name="txtIdItem_<?php echo $i; ?>" value="<?php echo utf8_encode($RFP["id"]); ?>" type="hidden">
                                            <input id="txtEtapa_<?php echo $i; ?>" name="txtEtapa_<?php echo $i; ?>" value="<?php echo utf8_encode($RFP["etapa"]); ?>" type="hidden">
                                            <input id="txtTipo_<?php echo $i; ?>" name="txtTipo_<?php echo $i; ?>" value="<?php echo utf8_encode($RFP["tipo"]); ?>" type="hidden">
                                            <input id="txtImagem_<?php echo $i; ?>" name="txtImagem_<?php echo $i; ?>" value="<?php echo utf8_encode($RFP["imagem"]); ?>" type="hidden">
                                            <input id="txtDescricao_<?php echo $i; ?>" name="txtDescricao_<?php echo $i; ?>" value="<?php echo utf8_encode($RFP["descricao"]); ?>" type="hidden">
                                            <input id="txtObservacao_<?php echo $i; ?>" name="txtObservacao_<?php echo $i; ?>" value="<?php echo utf8_encode($RFP["observacao"]); ?>" type="hidden">
                                            <div id="tabela_desc_<?php echo $i; ?>" style="line-height:27px; float:left">[Etapa <?php echo utf8_encode($RFP["etapa"]); ?>] <img src="./Api/img/<?php echo utf8_encode($RFP["imagem"]); ?>" style="height: 25px;"> [<?php echo ($RFP["tipo"] == "imagem" ? "Imagem" : ($RFP["tipo"] == "video" ? "Vídeo" : "Finalizar")); ?>] <a href="javascript:void(0)" onclick="editCampos(<?php echo $i; ?>)"><?php echo utf8_encode($RFP["descricao"]); ?></a></div>
                                            <div style="width: 5%; padding: 4px 0px 3px; float: right; cursor: pointer;" onclick="return removeLinha('<?php echo $i; ?>');">
                                                <span class="ico-remove" style="font-size:20px"></span>
                                            </div>
                                        </div>
                                    <?php }} ?>                            
                                    <input id="txtContatosTotal" name="txtContatosTotal" type="hidden" value="<?php echo $i; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both;"></div>                    
                </div>        
                <div class="tab-pane" id="tab2">
                    <div style="width: 100%;float: left">
                        <span>Tipo:</span>
                        <select id="txtTipoVeiculo" name="txtTipoVeiculo[]" multiple="multiple" class="select" style="width:100%" class="input-medium" <?php echo $disabled; ?>>
                            <?php $cur = odbc_exec($con, "select t.*, id_vistoria from sf_veiculo_tipos t
                            left join sf_vistorias_veiculo_tipos pt on t.id = pt.id_tipo and pt.id_vistoria = " . valoresSelect2($id) . " where v_inativo = 0 order by t.descricao") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP['id'] ?>"<?php
                                if ($RFP["id_vistoria"] != null) {
                                    echo "SELECTED";
                                }
                                ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                            <?php } ?>
                        </select>
                    </div>                    
                </div>        
            </div>        
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" name="bntSave" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK" ><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else {
                    ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/select/select22.min.js"></script>    
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/multiselect/jquery.multi-select.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery.filter_input.js'></script>
    <script type='text/javascript' src='../../js/plugins/ckeditor/ckeditor.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script> 
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>    
    <script type="text/javascript" src="../../js/util.js"></script>
    <script type="text/javascript">
        
        function formatState (state) {
            if (!state.id) { 
                return state.text; 
            }
            var $state = $('<span><img src="./Api/img/' +  state.id + '" style="height: 25px;"/> ' + state.text + '</span>');
            return $state;
        };

        $(".selectCr").select2({
            templateResult: formatState,
            templateSelection: formatState
        });
            
        function addCampos() {
            if (validar()) {
                if($.isNumeric($("#txtIdItemEdit").val())) {
                    let id = $("#txtIdItemEdit").val();                    
                    $("#txtIdItem_" + id).val($("#txtIdItem").val());
                    $("#txtEtapa_" + id).val($("#txtEtapa").val());
                    $("#txtTipo_" + id).val($("#txtTipo").val());
                    $("#txtImagem_" + id).val($("#txtImagem").val());
                    $("#txtDescricao_" + id).val($("#txtDescricao").val());                    
                    $("#txtObservacao_" + id).val($("#txtObservacao").val());                    
                    $("#tabela_desc_" + id).html("[" + $("#txtEtapa option:selected").html() + "] <img src='./Api/img/" + $("#txtImagem").val() + "' style='height: 25px;'/> [" + $("#txtTipo option:selected").html() + "] <a href=\"javascript:void(0)\" onclick=\"editCampos(" + id + ")\">" + $("#txtDescricao").val() + "</a>");
                } else {
                    let id = parseInt($("#txtContatosTotal").val()) + 1;                    
                    let linha = "<div id=\"tabela_linha_" + id + "\" style=\"padding:2px; border-bottom:1px solid #DDD; overflow: hidden;\">" +                        
                    "<input id=\"txtIdItem_" + id + "\" name=\"txtIdItem_" + id + "\" value=\"" + $("#txtIdItem").val() + "\" type=\"hidden\">" +
                    "<input id=\"txtEtapa_" + id + "\" name=\"txtEtapa_" + id + "\" value=\"" + $("#txtEtapa").val() + "\" type=\"hidden\">" +                    
                    "<input id=\"txtTipo_" + id + "\" name=\"txtTipo_" + id + "\" value=\"" + $("#txtTipo").val() + "\" type=\"hidden\">" +                    
                    "<input id=\"txtImagem_" + id + "\" name=\"txtImagem_" + id + "\" value=\"" + $("#txtImagem").val() + "\" type=\"hidden\">" +
                    "<input id=\"txtDescricao_" + id + "\" name=\"txtDescricao_" + id + "\" value=\"" + $("#txtDescricao").val() + "\" type=\"hidden\">" +                                                                                     
                    "<input id=\"txtObservacao_" + id + "\" name=\"txtObservacao_" + id + "\" value=\"" + $("#txtObservacao").val() + "\" type=\"hidden\">" +                                                                                     
                    "<div id=\"tabela_desc_" + id + "\" style=\"line-height:27px; float:left\">" + "[" + $("#txtEtapa option:selected").html() + "] <img src='./Api/img/" + $("#txtImagem").val() + "' style='height: 25px;'/> [" + $("#txtTipo option:selected").html() + "] <a href=\"javascript:void(0)\" onclick=\"editCampos(" + id + ")\">" + $("#txtDescricao").val() + "</a>" + "</div>" +                        
                    "<div style=\"width: 5%; padding: 4px 0px 3px; float: right; cursor: pointer;\" onclick=\"return removeLinha('" + id + "');\"><span class=\"ico-remove\" style=\"font-size:20px\"></span></div></div>";
                    $("#divContatos").append(linha);                    
                    $("#txtContatosTotal").val(id);
                }
                $("#txtIdItemEdit").val("");                
            }
        }
        
        function editCampos(id) {
            $("#txtIdItemEdit").val(id);            
            $("#txtIdItem").val($("#txtIdItem_" + id).val());
            $("#txtEtapa").val($("#txtEtapa_" + id).val());
            $("#txtTipo").val($("#txtTipo_" + id).val());
            $("#txtImagem").val($("#txtImagem_" + id).val());
            $("#txtDescricao").val($("#txtDescricao_" + id).val());          
            $("#txtObservacao").val($("#txtObservacao_" + id).val());          
        }

        function validar() {
            if ($('#txtDescricao').val() === "") {
                bootbox.alert("Preencha o campo Descrição!");
                return false;           
            }
            return true;
        }

        function removeLinha(id) {
            $("#tabela_linha_" + id).remove();
        }
            
    </script>
    <?php odbc_close($con); ?>
</body>
