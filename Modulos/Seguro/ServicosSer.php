<?php
include '../../Connections/configini.php';
if (is_numeric($_GET['Del'])) {
    odbc_exec($con, "DELETE FROM sf_veiculo_tipos WHERE id =" . $_GET['Del']);
    echo "<script>window.top.location.href = 'VeiculoTipo.php'; </script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>                      
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">      
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Serviços<small>Ordem de Serviços</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="float:left;width: 15%;margin-top: 15px;">
                                    <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir('I')" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                    <button id="btnExcel" class="button button-blue btn-primary" type="button" onclick="imprimir('E')" title="Exportar Excel"><span class="ico-download-3 icon-white"></span></button>
                                </div>
                                <div style="width: 85%; display: flex; justify-content: flex-end;">
                                    <div style="float:left; margin-left: 1%;">
                                        <span style="display:block;">Data:</span>
                                        <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin" name="txt_dt_begin" value="" placeholder="Data inicial" maxlength="10" autocomplete="off">
                                        <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end" name="txt_dt_end" value="" placeholder="Data Final" maxlength="10" autocomplete="off">
                                    </div>
                                    <div style="float:left; margin-left: 1%;">
                                        <span>Status:</span>
                                        <select style="width: 100%" name="txtStatusAp" id="txtStatusAp">
                                            <option value="null">Selecione</option>                                       
                                            <option value="1" selected>Pendentes</option>
                                            <option value="0">Concluídas</option>
                                        </select>                                 
                                    </div>                                    
                                    <div style="float:left; margin-left: 1%;">
                                        <div width="100%">Busca:</div>
                                        <input id="txtBusca" maxlength="100" type="text" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title=""/>
                                    </div>
                                    <div style="margin-top: 15px;">
                                        <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Ordem de Serviços</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="example">
                            <thead>
                                <tr>
                                    <th width="5%">OS:</th>
                                    <th width="9%">Data/Hora:</th>
                                    <th width="16%">Cliente:</th>
                                    <th width="5%">Placa:</th>
                                    <th width="14%">Modelo:</th>
                                    <th width="6%">Responsável:</th>                                    
                                    <th width="15%">Especificação:</th>
                                    <th width="10%">Próx. Cont.:</th>
                                    <th width="5%">Pendente:</th>
                                    <th width="10%">Periodo para Cont.:</th>
                                    <th width="5%"><center>Ação:</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" /> 
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>        
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }

            function AbrirBoxServicos(idTele, idPess, Tipo) {
                var final_url = "";
                if (idTele !== 0) {
                    final_url += "&id=" + idTele;
                }
                if (idPess !== 0) {
                    final_url += "&cli=" + idPess;
                }
                abrirTelaBox("../../Modulos/CRM/form/FormTelemarketing.php?tp=" + Tipo + final_url, 550, 800);
            }
            
            function AbrirBoxTelemarketing(idTele, idPess, Tipo, tp) {
                $.getJSON('../../Modulos/CRM/form/CRMFormServerTelemarketing.php', {txtIdx: idTele, bntFind: "S", txtQualificacao: $("#etapa2 .subtxt").text()}, function (j) {
                    for (var i = 0; i < j.length; i++) {
                        $('#txtIdx').val(j[i].id_tele);
                        $('#txtDataHistorico').val(j[i].data_tele);
                        $('#txtHoraHistorico').val(j[i].data_teleh);
                        $("#txtTipoChamado").select2("val", j[i].especificacao);
                        $('#txtDataHistoricoPrev').val(j[i].proximo_contato);
                        $('#txtHoraHistoricoPrev').val(j[i].proximo_contatoh);
                        $("#txtDeHistorico").select2("val", j[i].de_pessoa);
                        $("#txtParaHistorico").select2("val", j[i].para_pessoa);
                        $('#textoArea').val(j[i].relato);
                        $('#ckbAcompanhar').parents('span').removeClass("checked").end().removeAttr("checked").change();
                        if (j[i].acompanhar === "1") {
                            $('#ckbAcompanhar').click();
                        }
                        TrataEnableCamposTele(j[i].pendente, j[i].para_pessoa, j[i].de_pessoa);
                    }
                }).done(function () {});
                if (Tipo > 0) {
                    if (idPess === null) {
                        abrirTelaBox("../../Modulos/CRM/CRMFecharChamado.php?idx=" + $('#txtIdx').val() + "&fx=" + tp + "&tp=2&id=" + Tipo, 360, 600);
                    } else {
                        abrirTelaBox("../../Modulos/CRM/CRMFecharChamado.php?idx=" + idTele + "&fx=" + tp + "&tp=" + Tipo, 360, 600);
                    }
                }
            }

            function FecharBox() {
                var oTable = $('#example').dataTable();
                oTable.fnDraw(false);
                $("#newbox").remove();
            }

            function imprimir() {
                var pRel = "&NomeArq=" + "Ordem de Serviços" +                        
                        "&lbl=OS|Data/Hora|Cliente|Placa|Modelo|Responsável|Especificação|Próx. Cont.|Pendente|Periodo" +
                        "&siz=40|80|190|60|170|80|170|80|60|80" +
                        "&pdf=11&pOri=L" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/Seguro/ServicosSer_server_processing.php" + finalFind(1).replace("?", "&"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }

            $(document).ready(function () {
                listaTable();
                $("#btnfind").click(function () {
                    $("#example").dataTable().fnDestroy();
                    listaTable();
                });

                function listaTable() {
                    $('#example').dataTable({
                        "iDisplayLength": 20,
                        "aLengthMenu": [20, 30, 40, 50, 100],
                        "bProcessing": true,
                        "bServerSide": true,
                        "bFilter": false,
                        "aoColumns": [
                            {"bSortable": false},
                            {"bSortable": false},
                            {"bSortable": false},
                            {"bSortable": false},
                            {"bSortable": false},
                            {"bSortable": false},
                            {"bSortable": false},
                            {"bSortable": false},
                            {"bSortable": false},
                            {"bSortable": false},
                            {"bSortable": false}],
                        "sAjaxSource": "ServicosSer_server_processing.php" + finalFind(0),
                        'oLanguage': {
                            'oPaginate': {
                                'sFirst': "Primeiro",
                                'sLast': "Último",
                                'sNext': "Próximo",
                                'sPrevious': "Anterior"
                            }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                            'sLengthMenu': "Visualização de _MENU_ registros",
                            'sLoadingRecords': "Carregando...",
                            'sProcessing': "Processando...",
                            'sSearch': "Pesquisar:",
                            'sZeroRecords': "Não foi encontrado nenhum resultado"},
                        "sPaginationType": "full_numbers"
                    });
                }
            });
            
            function finalFind(imp) {
                var retPrint = "?imp=" + imp;
                if ($("#txt_dt_begin").length > 0 && $("#txt_dt_begin").val() !== "") {
                    retPrint += "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "/");
                }
                if ($("#txt_dt_end").length > 0 && $("#txt_dt_end").val() !== "") {
                    retPrint += "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "/");
                }
                if ($("#txtStatusAp").length > 0 && $("#txtStatusAp").val() !== "null") {
                    retPrint += '&statusap=' + $("#txtStatusAp").val();
                }                
                if ($("#txtBusca").length > 0 && $("#txtBusca").val() !== "") {
                    retPrint += '&sSearch=' + $("#txtBusca").val();
                }                
                return retPrint.replace(/\//g, "_");
            }
        </script>
        <?php odbc_close($con); ?>
    </body>
</html>
