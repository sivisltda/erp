<?php
include '../../Connections/configini.php';
$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '' && $_POST['txtId2'] != '') {
        odbc_exec($con, "update sf_veiculo_modelo_servico set 
        id_modelo = " . valoresSelect("txtModelo") . ", id_servico = " . valoresSelect("txtIdServico") . ",
        tempo = " . valoresTexto("txtTempo") . ", valor = " . valoresNumericos("txtValor") . "
        where id_modelo = " . $_POST['txtId2'] . " and id_servico = " . $_POST['txtId']) or die(odbc_errormsg());
        odbc_exec($con, "update sf_veiculo_modelo_produto set 
        id_modelo = " . valoresSelect("txtModelo") . ", id_servico = " . valoresSelect("txtIdServico") . "
        where id_modelo = " . $_POST['txtId2'] . " and id_servico = " . $_POST['txtId']) or die(odbc_errormsg());        
    } else {
        odbc_exec($con, "insert into sf_veiculo_modelo_servico(id_modelo, id_servico, tempo, valor)values(" .
        valoresSelect("txtModelo") . ", " . valoresSelect("txtIdServico") . ", " . 
        valoresTexto("txtTempo") . ", " . valoresNumericos("txtValor") . ")") or die(odbc_errormsg());
    }
    $_POST['txtId'] = $_POST['txtIdServico'];
    $_POST['txtId2'] = $_POST['txtModelo'];    
}
if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtId']) && is_numeric($_POST['txtId2'])) {
        odbc_exec($con, "DELETE FROM sf_veiculo_modelo_produto WHERE id_servico = " . $_POST['txtId'] . " and id_modelo = " . $_POST['txtId2']);
        odbc_exec($con, "DELETE FROM sf_veiculo_modelo_servico WHERE id_servico = " . $_POST['txtId'] . " and id_modelo = " . $_POST['txtId2']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id_servico'] = '';
    $_POST['txtId'] = '';    
    $_GET['id_modelo'] = '';
    $_POST['txtId2'] = '';
}

if (($_GET['id_servico'] != '' || $_POST['txtId'] != '') && ($_GET['id_modelo'] != '' || $_POST['txtId2'] != '')) {
    $PegaURL = $_GET['id_servico'];
    $PegaURL2 = $_GET['id_modelo'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
        $PegaURL2 = $_POST['txtId2'];
    }
    $cur = odbc_exec($con, "select id_servico, sf_produtos.descricao, id_marca, id_modelo, tempo, valor 
    from sf_veiculo_modelo_servico inner join sf_produtos on conta_produto = id_servico
    inner join sf_veiculo_modelo on id = id_modelo
    where tipo = 'S' and id_modelo = " . $PegaURL2 . " and id_servico = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {       
        $servico = utf8_encode($RFP['id_servico']);
        $descricao = utf8_encode($RFP['descricao']);
        $montadora = utf8_encode($RFP['id_marca']);
        $modelo = utf8_encode($RFP['id_modelo']);
        $tempo = $RFP['tempo'];
        $valor = escreverNumero($RFP['valor']);
    }
} else {
    $disabled = '';
    $servico = '';
    $descricao = '';
    $montadora = '';
    $modelo = '';
    $tempo = '00:00';
    $valor = escreverNumero(0);
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <form action="FormServicosVeiculos.php" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Serviços por Modelos</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="tabbable frmtabs">
            <ul class="nav nav-tabs" style="margin:0">
                <li class="active"><a href="#tab1" data-toggle="tab">Principal</a></li>
                <li><a href="#tab2" data-toggle="tab">Produtos</a></li>                
            </ul>
            <div class="tab-content frmcont" style="height:313px; overflow:hidden;">
                <input name="txtId" id="txtId" value="<?php echo $servico; ?>" type="hidden"/>
                <input name="txtId2" id="txtId2" value="<?php echo $modelo; ?>" type="hidden"/>                                        
                <div class="tab-pane active" id="tab1">
                    <div style="width: 100%;float: left">
                        <span>Serviço:</span>
                        <input name="txtIdServico" id="txtIdServico" value="<?php echo $servico; ?>" type="hidden"/>
                        <input type="text" id="txtServicoNome" style="width:100%" value="<?php echo $descricao;?>" <?php echo $disabled; ?>/>
                    </div>                
                    <div style="width: 100%;float: left">
                        <span>Montadora:</span>
                        <select name="txtMontadora" id="txtMontadora" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                            <option value="null" >Selecione</option>
                            <?php $sql = "select id, descricao from sf_veiculo_marca order by descricao";
                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP["id"] ?>"<?php
                                if (!(strcmp($RFP["id"], $montadora))) {
                                    echo "SELECTED";
                                }
                                ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                            <?php } ?>
                        </select>                            
                    </div>
                    <div style="width: 100%;float: left">
                        <span>Modelo:</span>
                        <select name="txtModelo" id="txtModelo" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                            <option value="null">Selecione</option>                    
                            <?php if (is_numeric($montadora)) {
                                $sql = "select id, descricao from sf_veiculo_modelo where id_marca = " . $montadora . " order by descricao";
                                $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP["id"] ?>"<?php
                                    if (!(strcmp($RFP["id"], $modelo))) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                            <?php }} ?>
                        </select>                            
                    </div>
                    <div style="width: 20%;float: left">
                        <span>Tempo:</span>
                        <input name="txtTempo" id="txtTempo" <?php echo $disabled; ?> maxlength="6" type="text" class="input-medium" value="<?php echo $tempo; ?>"/>
                    </div>
                    <div style="width: 30%;float: left; margin-left: 1%;">
                        <span>Valor:</span>
                        <input name="txtValor" id="txtValor" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $valor; ?>"/>
                    </div>            
                    <div style="clear:both;height: 5px;"></div>
                </div>
                <div class="tab-pane" id="tab2">
                    <div class="body" style="padding: 0px; margin-left: 0px;">
                        <div class="content">
                            <div style="width: 95%;float: left">
                                <span>Produto:</span>
                                <input name="txtIdProduto" id="txtIdProduto" value="" type="hidden"/>
                                <input type="text" id="txtProdutoNome" style="width:100%" value=""/>
                            </div>                                    
                            <div style="width: 5%; margin-top: 16px; float: left">
                                <button type="button" name="bntAddProduto" onclick="AddProduto();" class="btn btn-success" style="height:27px; margin:0; padding-top:6px;float: left;"><span class="ico-plus"></span></button>
                            </div>                            
                            <table id="tbProduto" class="table" cellpadding="0" cellspacing="0" width="100%" style="margin-top: 5px;">
                                <thead>
                                    <tr>
                                        <th width="90%">Descrição</th>
                                        <th width="10%"><center>Ação</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="2" class="dataTables_empty"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" name="bntSave" id="bntSave" ><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntCancelar"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntAlterar" ><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else { ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntEdit" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../../js/plugins.js'></script>
    <script type="text/javascript" src="../../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type='text/javascript' src='../../../js/plugins/datatables/fnReloadAjax.js'></script>
    <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../../js/moment.min.js"></script>
    <script type='text/javascript' src='../../../js/jquery.priceformat.min.js'></script>    
    <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script> 
    <script type="text/javascript">
        
        $("#txtTempo").mask("99:99");
        $("#txtValor").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
        
        $("#txtServicoNome").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "./produto.ajax.php",
                    dataType: "json",
                    data: {t: request.term, v: "S"},
                    success: function (data) {
                        response(data);
                    }
                });
            }, minLength: 3,
            select: function (event, ui) {
                $("#txtIdServico").val(ui.item.id);
                $("#txtServicoNome").val(ui.item.value);              
            }
        });
        
        $("#txtProdutoNome").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "./produto.ajax.php",
                    dataType: "json",
                    data: {t: request.term, v: "P"},
                    success: function (data) {
                        response(data);
                    }
                });
            }, minLength: 3,
            select: function (event, ui) {
                $("#txtIdProduto").val(ui.item.id);
                $("#txtProdutoNome").val(ui.item.value);              
            }
        });
        
        $('#txtMontadora').change(function () {
            filtroModelo();
        });        
        
        function filtroModelo() {
            var options = '<option value="null">Selecione</option>';                
            if ($('#txtMontadora').val() !== "null") {
                $.getJSON('modelos.ajax.php', {txtMontadoras: $('#txtMontadora').val()}, function (j) {
                    for (var i = 0; i < j.length; i++) {
                        options += '<option value="' + j[i].id + '">' + j[i].descricao + '</option>';
                    }
                    $('#txtModelo').html(options).show();
                });
            } else {
                $('#txtModelo').html(options);
            }
        }
        
        $('#tbProduto').dataTable({
            "iDisplayLength": 20,
            "aLengthMenu": [20, 30, 40, 50, 100],
            "bProcessing": true,
            "bServerSide": true,
            "bSort": false,
            "bLengthChange": false,
            "bFilter": false,
            "sAjaxSource": "../../Modulos/Seguro/form/FormServicosVeiculos.php" + 
            "?listProd=S&id_servico=" + $("#txtIdServico").val() + "&id_modelo=" + $("#txtModelo").val(),
            'oLanguage': {
                'oPaginate': {
                    'sFirst': "Primeiro",
                    'sLast': "Último",
                    'sNext': "Próximo",
                    'sPrevious': "Anterior"
                }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                'sLengthMenu': "Visualização de _MENU_ registros",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sSearch': "Pesquisar:",
                'sZeroRecords': "Não foi encontrado nenhum resultado"},
            "sPaginationType": "full_numbers"
        });

        function refreshProduto() {
            let tblCarteira = $('#tbProduto').dataTable();
            tblCarteira.fnReloadAjax("../../Modulos/Seguro/form/FormServicosVeiculos.php" + 
            "?listProd=S&id_servico=" + $("#txtIdServico").val() + "&id_modelo=" + $("#txtModelo").val());
        }
        
        function removerProduto(id_produto, id_modelo, id_servico) {
            if (confirm("Confirma a exclusão do registro?")) {
                $.post("../../Modulos/Seguro/form/FormServicosVeiculos.php", {btnDelete: true, 
                    id_produto: id_produto, id_modelo: id_modelo, id_servico: id_servico}).done(function (data) {
                    if (data.trim() === "YES") {
                        refreshProduto();
                    } else {
                        bootbox.alert("Erro ao excluir registro!");
                    }
                });
            }
        }
        
        function AddProduto() {
            $.post("../../Modulos/Seguro/form/FormServicosVeiculos.php", {btnSave: true, 
                id_produto: $("#txtIdProduto").val(), id_modelo: $("#txtModelo").val(), 
                id_servico: $("#txtIdServico").val()}).done(function (data) {
                if (data.trim() === "YES") {
                    refreshProduto();
                } else {
                    bootbox.alert("Erro ao cadastrar registro!");
                }
            });            
        }
        
    </script>
    <?php odbc_close($con); ?>
</body>
