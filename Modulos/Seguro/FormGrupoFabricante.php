<?php
include '../../Connections/configini.php';

$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_veiculo_grupo set " .
                        "descricao = " . valoresTexto('txtdescricao') . "," .
                        "tipo = " . valoresSelect('txtTipo') . "," .
                        "inativo = " . valoresCheck('ckbInativo') . " where id = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_veiculo_grupo(descricao, tipo, inativo) values (" .
                        valoresTexto('txtdescricao') . "," .
                        valoresSelect('txtTipo') . "," .
                        valoresCheck('ckbInativo') . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id from sf_veiculo_grupo order by id desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
    if (isset($_POST['txtContatosTotal'])) {
        $notIn = '0';        
        $max = $_POST['txtContatosTotal'];
        for ($i = 0; $i <= $max; $i++) {
            if (isset($_POST['txtIdItem_' . $i]) && is_numeric($_POST['txtIdItem_' . $i])) {
                $notIn .= "," . $_POST['txtIdItem_' . $i];
            }
        }
        odbc_exec($con, "delete from sf_veiculo_grupo_fabricante where id_grupo = " . $_POST['txtId'] . " and id_marca not in (" . $notIn . ")");
        for ($i = 0; $i <= $max; $i++) {            
            if (isset($_POST['txtIdItem_' . $i]) && is_numeric($_POST['txtIdItem_' . $i])) {
                odbc_exec($con, "IF NOT EXISTS (select * from sf_veiculo_grupo_fabricante where id_grupo = " . $_POST['txtId'] . " and id_marca = " . $_POST['txtIdItem_' . $i] . ") 
                BEGIN insert into sf_veiculo_grupo_fabricante(id_grupo, id_marca, marca) values (" . $_POST['txtId'] . "," . $_POST['txtIdItem_' . $i] . ",'" . $_POST['txtDescItem_' . $i] . "'); END") or die(odbc_errormsg());
            }
        }
    }        
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_veiculo_grupo WHERE id = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_veiculo_grupo where id =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id'];
        $descricao = $RFP['descricao'];
        $tipo = $RFP['tipo'];      
        $inativo = $RFP['inativo'];
    }
} else {
    $disabled = '';
    $id = '';
    $descricao = '';
    $tipo = '';
    $inativo = '';
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../../js/plugins/select/select22.css"/>
<script type='text/javascript' src='../../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<body>
    <form action="FormGrupoFabricante.php" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Grupo de Fabricantes</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont" style="min-height: 350px;">
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <div style="width: 86%;float: left">
                <span>Descrição:</span>
                <input name="txtdescricao" id="txtdescricao" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="128" value="<?php echo utf8_encode($descricao); ?>"/>
            </div>
            <div style="width: 13%;float:left;margin-left: 1%;display: flex;flex-direction: column;justify-content: center;">
                <div style="display:flex;justify-content: space-between;">
                    <span>Inativo:</span>
                    <input name="ckbInativo" id="ckbInativo" type="checkbox" <?php
                    if ($inativo == "1") {
                        echo "checked";
                    }
                    ?> class="input-medium" <?php echo $disabled; ?> value="1" style="opacity: 0;"/>
                </div>                                
            </div>
            <div style="width: 100%; float: left;">
                <span>Montadoras:</span>
                <div style="width:100%; float:left">                  
                    <div style="background:#F1F1F1; border:1px solid #DDD; border-bottom:0; padding:10px 10px 0">
                        <input name="txtIdItemEdit" id="txtIdItemEdit" value="" type="hidden"/>
                        <div style="float:left; width:19%;">
                            <select id="txtTipo" name="txtTipo" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                                <option value="1" <?php echo ($tipo == "1" ? "SELECTED" : "");?>>Carro</option>
                                <option value="2" <?php echo ($tipo == "2" ? "SELECTED" : "");?>>Moto</option>
                                <option value="3" <?php echo ($tipo == "3" ? "SELECTED" : "");?>>Caminhão</option>
                            </select>
                        </div>
                        <div style="float:left; width:69%; margin-left:1%;">
                            <select id="txtMontadora" name="txtMontadora" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                            </select>
                        </div>
                        <div style="float:left; width:10%; margin-left:1%;">
                            <button type="button" onclick="addCampos();" class="btn dblue" <?php echo $disabled; ?> style="height: 26px;background-color: #308698 !important;">
                                <span style="margin-top: 4px;" class="ico-plus icon-white"></span>
                            </button>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div style="background:#F1F1F1; border:1px solid #DDD; border-top:0; padding:5px 10px 10px">
                        <div id="divContatos" style="height:230px; background:#FFF; border:1px solid #DDD; overflow-y:scroll;">
                        <?php
                            $i = 0;
                            if (is_numeric($id)) {
                                $cur = odbc_exec($con, "select id_marca, marca from sf_veiculo_grupo_fabricante where id_grupo = " . $id . " order by marca") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { $i++; ?>                            
                                <div id="tabela_linha_<?php echo $i; ?>" style="padding:2px; border-bottom:1px solid #DDD; overflow: hidden;">
                                    <input id="txtIdItem_<?php echo $i; ?>" name="txtIdItem_<?php echo $i; ?>" value="<?php echo utf8_encode($RFP["id_marca"]); ?>" type="hidden">
                                    <input id="txtDescItem_<?php echo $i; ?>" name="txtDescItem_<?php echo $i; ?>" value="<?php echo utf8_encode($RFP["marca"]); ?>" type="hidden">
                                    <div id="tabela_desc_<?php echo $i; ?>" style="line-height:27px; float:left"><a href="javascript:void(0)" onclick="editCampos(<?php echo $i; ?>)"><?php echo utf8_encode($RFP["marca"]); ?></a></div>
                                    <div style="width: 5%; padding: 4px 0px 3px; float: right; cursor: pointer;" onclick="return removeLinha('<?php echo $i; ?>');">
                                        <span class="ico-remove" style="font-size:20px"></span>
                                    </div>
                                </div>
                            <?php }} ?>                            
                            <input id="txtContatosTotal" name="txtContatosTotal" type="hidden" value="<?php echo $i; ?>">
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" onclick="return validaForm();" name="bntSave" id="bntSave" ><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntCancelar"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntAlterar" ><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else {
                    ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntEdit" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/select/select22.min.js"></script>    
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/multiselect/jquery.multi-select.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery.filter_input.js'></script>
    <script type='text/javascript' src='../../js/plugins/ckeditor/ckeditor.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script> 
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>    
    <script type="text/javascript" src="../../js/util.js"></script>
    <script type="text/javascript">

        function validaForm() {
            if ($("#txtdescricao").val() === "") {
                bootbox.alert("Preencha uma Descrição!");
                return false;
            }                                      
            return true;
        }

        function addCampos() {
            if (validar()) {
                if($.isNumeric($("#txtIdItemEdit").val())) {
                    let id = $("#txtIdItemEdit").val();
                    $("#txtIdItem_" + id).val($("#txtMontadora").val());
                    $("#txtDescItem_" + id).val($("#txtMontadora").select2('data')[0].text);
                    $("#tabela_desc_" + id).html("<a href=\"javascript:void(0)\" onclick=\"editCampos(" + id + ")\">" + $("#txtMontadora").select2('data')[0].text + "</a>");
                } else {
                    let id = parseInt($("#txtContatosTotal").val()) + 1;
                    let linha = "<div id=\"tabela_linha_" + id + "\" style=\"padding:2px; border-bottom:1px solid #DDD; overflow: hidden;\">" + 
                    "<input id=\"txtIdItem_" + id + "\" name=\"txtIdItem_" + id + "\" value=\"" + $("#txtMontadora").val() + "\" type=\"hidden\">" +
                    "<input id=\"txtDescItem_" + id + "\" name=\"txtDescItem_" + id + "\" value=\"" + $("#txtMontadora").select2('data')[0].text + "\" type=\"hidden\">" +
                    "<div id=\"tabela_desc_" + id + "\" style=\"line-height:27px; float:left\">" + 
                    "<a href=\"javascript:void(0)\" onclick=\"editCampos(" + id + ")\">" + $("#txtMontadora").select2('data')[0].text + "</a>" + "</div>" +
                    "<div style=\"width: 5%; padding: 4px 0px 3px; float: right; cursor: pointer;\" onclick=\"return removeLinha('" + id + "');\"><span class=\"ico-remove\" style=\"font-size:20px\"></span></div></div>";
                    $("#divContatos").append(linha);  
                    $("#txtContatosTotal").val(id);
                }
                $("#txtIdItemEdit").val("");
                $("#txtMontadora").select2("val", "null");
            }
        }
        
        function editCampos(id) {
            $("#txtIdItemEdit").val(id);
            $("#txtMontadora").val($("#txtIdItem_" + id).val()).trigger('change');
        }

        function validar() {
            if ($('#txtMontadora').val() === "null") {
                bootbox.alert("Preencha e selecione uma montadora!");
                return false;           
            }
            return true;
        }

        function removeLinha(id) {
            $("#tabela_linha_" + id).remove();
        }
        
        function getMarca() {
            let parametros = "&tipo=" + $('#txtTipo').val();
            $.get("./ajax/fipejson.php?functionPage=getMarca" + parametros).done(function (json) {
                let options = "<option value=\"null\">Selecione</option>";
                if (json !== null) {
                    for (var x = 0; x < json.length; x++) {
                        options += "<option value=\"" + json[x].Value + "\">" + json[x].Label + "</option>";
                    }
                }
                $("#txtMontadora").html(options);
                $("#txtMontadora").select2("val", "null");
            });
        }
        
        $('#txtTipo').change(function () {
            getMarca();
        });            
        
        getMarca();
            
    </script>
    <?php odbc_close($con); ?>
</body>