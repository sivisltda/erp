<?php include "../../Connections/configini.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Sivis Business</title>
    <link rel="icon" type="image/ico" href="../../favicon.ico"/>
    <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
    <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
    <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    <style>
        #tblConvenios td {
            padding: 5px;
            line-height: 16px;
        }
    </style>
</head>
<body>
    <div id="loader"><img src="../../img/loader.gif"/></div>
    <div class="wrapper">
    <?php include("../../menuLateral.php"); ?>
    <div class="body">
        <?php include("../../top.php"); ?>
        <div class="content">
            <div class="page-header">
                <div class="icon"> <span class="ico-arrow-right"></span></div>
                <h1>Proteção<small>Oficinas</small></h1>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="boxfilter block">
                        <div style="float:left;width: 10%;margin-top: 15px;">
                            <button class="button button-green btn-primary" type="button" onclick="AbrirBox(0)"><span class="ico-file-4 icon-white"></span></button>
                            <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir()" title="Imprimir"><span class="ico-print icon-white"></span></button>
                        </div>
                        <div style="width: 90%; display: flex; justify-content: flex-end;">
                            <div style="margin-left: 1%;">
                                <span>Especialidade:</span>
                                <select name="txtProfessor" id="txtProfessor" class="select input-medium" style="width: 100%;">
                                    <option value="">Selecione</option>
                                </select>
                            </div>
                            <div style="margin-left: 1%;">
                                <span>Bairro:</span>
                                <select name="txtAmbiente" id="txtAmbiente" class="select input-medium" style="width: 100%;">
                                    <option value="">Selecione</option>
                                </select>
                            </div>
                            <div style="margin-top: 15px;">
                                <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear:both"></div>
            <div class="boxhead">
                <div class="boxtext">Oficinas</div>
            </div>
            <div class="boxtable">
                <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tblTurmas">
                    <thead>
                        <tr>
                            <th width="3%"></th>
                            <th width="23%">Nome da Oficina</th>
                            <th width="8%">Max</th>
                            <th width="8%">Vagas</th>
                            <th width="8%">Manutenção</th>
                            <th width="17%">Especialidade</th>
                            <th width="15%">Bairro</th>
                            <th width="12%">Status</th>
                            <th width="6%"><center>Ação</center></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                <div style="clear:both"></div>
            </div>
        </div>
    </div>
</div>
<div class="dialog" id="source" title="Source"></div>
<script type="text/javascript" src="../../fancyapps/source/jquery.fancybox.js?v=2.1.4"></script>
<script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
<script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
<script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
<script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
<script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
<script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
<script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
<script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="../../js/actions.js"></script>
<script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>
<script type="text/javascript" src="../../js/graficos.js"></script>
<script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
<script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
<script type="text/javascript" src="../../js/util.js"></script>
<script type="text/javascript" src="js/Oficinas.js"></script>
<?php odbc_close($con); ?>
</body>
</html>