<?php
include '../../Connections/configini.php';
if (is_numeric($_GET['Del'])) {
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    values ('sf_vistorias', " . $_GET['Del'] . ", '" . $_SESSION["login_usuario"] . "', 'E', 'EXCLUSAO - VISTORIA', GETDATE(),
    (select max(id_fornecedores_despesas) from sf_vistorias vi
    inner join sf_fornecedores_despesas_veiculo v on vi.id_veiculo = v.id
    where vi.id = " . $_GET['Del'] . "))");        
    odbc_exec($con, "DELETE FROM sf_vistorias WHERE id = " . $_GET['Del']) or die(odbc_errormsg());
    echo "<script>window.top.location.href = 'Vistorias.php'; </script>";
}
$DateBegin = getData("B");
$DateEnd = getData("E");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>        
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Proteção<small>Vistorias</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="float:left;width: 10%;margin-top: 15px;">
                                    <?php if ($ckb_adm_cli_read_ == 0) { ?>
                                    <button class="button button-green btn-primary" type="button" onClick="AbrirBoxVistorias(0)"><span class="ico-file-4 icon-white"></span></button>                                    
                                    <?php } ?>
                                    <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir()" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                </div>
                                <div style="width: 90%; display: flex; justify-content: flex-end;">
                                    <div style="margin-left: 1%;">
                                        <span style="display:block;">Data:</span>
                                        <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial" maxlength="10" autocomplete="off">
                                        <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final" maxlength="10" autocomplete="off">
                                    </div>                                    
                                    <div style="margin-left: 1%; width: 10%;">
                                        <span>Assistência 24h:</span>
                                        <select style="width: 100%" name="txtAssistencia" id="txtAssistencia">
                                            <option value="null">Selecione</option>                 
                                            <option value="0">Com Assistência</option>
                                            <option value="1">Sem Assistência</option>
                                        </select>                                 
                                    </div>                                     
                                    <div style="margin-left: 1%; width: 10%;">
                                        <span>Tipo:</span>
                                        <select style="width: 100%" name="txtTipoCli" id="txtTipoCli">
                                            <option value="null">Todos</option>                 
                                            <option value="P">Prospects</option>
                                            <option value="C">Clientes</option>
                                        </select>                               
                                    </div>                                     
                                    <div style="margin-left: 1%; max-width: 150px;">
                                        <span>Status do Plano:</span>
                                        <select style="width: 100%" name="txtStatus[]" id="txtStatus" multiple="multiple" class="select">
                                            <option value="Ativo" <?php echo (isset($_GET["id"]) && $_GET["id"] == "0" ? "selected" : "")?>>Ativo</option>
                                            <option value="Inativo">Inativo</option>
                                            <option value="Suspenso">Suspenso</option>
                                            <option value="Novo">Novo</option>
                                            <option value="Cancelado">Cancelado</option>
                                        </select>
                                    </div>                         
                                    <div style="margin-left: 1%;  width: 10%;">
                                        <span>Status de Vistorias:</span>
                                        <select style="width: 100%" name="txtVistorias" id="txtVistorias">
                                            <option value="null">Selecione</option>                                                                
                                            <option value="0">Concluídas</option>                                               
                                            <option value="1" <?php echo (isset($_GET["id"]) && $_GET["id"] == "0" ? "selected" : "")?>>Pendentes</option>                                            
                                        </select>                                 
                                    </div>                         
                                    <div style="margin-left: 1%;  width: 10%;">
                                        <span>Status de Veículos:</span>
                                        <select style="width: 100%" name="txtStatusAp" id="txtStatusAp">
                                            <option value="null">Selecione</option>                 
                                            <option value="0">Aguardando</option>                                                
                                            <option value="1">Aprovados</option>                                               
                                            <option value="2">Reprovados</option>                                                                                               
                                        </select>                                 
                                    </div>             
                                    <div style="margin-left: 1%;  width: 10%;">
                                        <span>Grupo de Clientes:</span>
                                        <select style="width: 100%" name="txtContaMov" id="txtContaMov">
                                            <option value="null">Selecione</option>
                                            <?php
                                            $cur = odbc_exec($con, "select id_grupo,descricao_grupo from dbo.sf_grupo_cliente
                                            where tipo_grupo = 'C' and inativo_grupo = 0 " . (is_numeric($filial_grupo_filter) ? " and id_grupo = " . $filial_grupo_filter : "") . " order by descricao_grupo") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id_grupo']; ?>"<?php
                                                if (!(strcmp($RFP['id_grupo'], (is_numeric($filial_grupo_filter) ? $filial_grupo_filter : $grupopes)))) {
                                                    echo "SELECTED";
                                                }
                                                ?>><?php echo utf8_encode($RFP['descricao_grupo']); ?>
                                                </option><?php } ?>
                                            <option value="-1">Sem Grupo</option>                                                
                                        </select>                                 
                                    </div>             
                                    <div style="float:left; margin-left: 1%;  width: 12%;">
                                        <div width="100%">Busca:</div>
                                        <input id="txtBusca" maxlength="100" type="text" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title=""/>
                                    </div>
                                    <div style="margin-top: 15px;">
                                        <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                    </div>
                                </div>
                            </div>                                                                                                                
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Vistorias</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tbListaVistorias">
                            <thead>
                                <tr>
                                    <th width="9%">Data:</th>    
                                    <th width="5%">Tipo:</th>    
                                    <th width="5%">Usuário:</th>                                
                                    <th width="6%">Placa:</th>
                                    <th width="10%">Marca:</th>                                               
                                    <th width="19%">Modelo:</th>                                                
                                    <th width="4%">Ano:</th>                                                
                                    <th width="6%">Comb.:</th>                                                
                                    <th width="5%">Valor</th>                                                                        
                                    <th width="8%">CPF:</th>
                                    <th width="12%">Cliente:</th>                                
                                    <th width="7%">Status:</th>                                    
                                    <th width="4%"><center>Ação:</center></th>                                    
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div> 
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>
        <script type="text/javascript" src="js/Vistoria.js"></script>        
        <script type="text/javascript">
            
            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }            
            
            function AbrirBoxCli(tipo, id) {  
                let url = (tipo === "C" ? "./../Academia/ClientesForm.php?id=" : "./../CRM/CRMForm.php?id=") + id;
                window.open(url,'_blank');
            }

            function imprimir() {                               
                var pRel = "&NomeArq=" + "Vistorias" +
                "&lbl=" + "Data|Tipo|Usuário|Placa|Marca|Modelo|Ano|Comb.|Valor|CPF|Cliente" +
                "&pOri=L"+ //parametro de paisagem
                "&siz=" + "80|70|80|70|150|180|50|60|50|70|150" +
                "&pdf=" + "11" + // Colunas do server processing que não irão aparecer no pdf 9
                "&filter=" + "Vistorias " + //Label que irá aparecer os parametros de filtro
                "&PathArqInclude=../Modulos/Seguro/" + finalFindVistorias(1).replace("?", "&"); // server processing            
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }            
            
            $("#btnfind").click(function () {
                tbListaVistorias.fnReloadAjax(finalFindVistorias(0));
            });
                                    
        </script>        
        <?php odbc_close($con); ?>
    </body>
</html>