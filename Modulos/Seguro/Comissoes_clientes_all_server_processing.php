<?php

if (isset($_GET["functionPage"]) && isset($_GET["loja"]) && is_numeric($_GET["loja"]) && in_array($_SERVER['REMOTE_ADDR'], ["148.72.177.101"])) {
    require_once(__DIR__ . '/../../Connections/funcoesAux.php');
    $hostname = getDataBaseServer(1);
    $database = "erp" . str_pad($_GET["loja"], 3, "0", STR_PAD_LEFT);
    $username = "erp" . str_pad($_GET["loja"], 3, "0", STR_PAD_LEFT);
    $password = "!Password123";
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());    
    $id_usuario = $_GET["id_usuario"];
} else {
    require_once(__DIR__ . '/../../Connections/configini.php');
    $id_usuario = $_SESSION["id_usuario"];
}

$cont = 0;
$sWhere = "";
$sWhere2 = "";
$grupoArray = [];
$tipos = [0, 1, 2, 3];
$dt_begin = getData("B");
$dt_end = getData("E");
$comissao_valor = "cc.valor";
$colunas_valor = ",cc.valor comissao,0 desconto";
$inner_join = "";

$cur = odbc_exec($con, "select adm_desconto_range from sf_configuracao where id = 1");
while ($RFP = odbc_fetch_array($cur)) {
    if ($RFP['adm_desconto_range'] == "1") {
        $comissao_valor = "cc.valor - (case when cc.tipo_valor_desc = 1 then
        isnull((select max(valor) from sf_convenios_regras cvr inner join sf_convenios cv on cv.id_convenio = cvr.convenio
        where cv.id_conv_plano = vp.id_plano),0) else 0 end)";
        $colunas_valor = ",cc.valor comissao,(case when cc.tipo_valor_desc = 1 then
        isnull((select max(valor) from sf_convenios_regras cvr inner join sf_convenios cv on cv.id_convenio = cvr.convenio
        where cv.id_conv_plano = vp.id_plano),0) else 0 end) desconto";
        $inner_join = "left join sf_vendas_planos_mensalidade m on m.id_item_venda_mens = vi.id_item_venda
        left join sf_vendas_planos vp on vp.id_plano = m.id_plano_mens";        
    }
}
        
$sWhere .= " and u.id_usuario not in (select uln.id_usuario from sf_usuarios ux 
inner join sf_usuarios_lista_negra uln on ux.funcionario = uln.id_fornecedor_despesas 
where ux.id_usuario = '" . $id_usuario . "')";

if (is_numeric($_GET['txtUsuario'])) {
    $sWhere .= " and u.id_usuario = " . $_GET['txtUsuario'];
}

if ($crm_cli_ure_ > 0) {
    $sWhere .= " and (u.id_usuario = " . $id_usuario . " 
    or u.id_usuario in (select sf_usuarios_dependentes.id_usuario from sf_usuarios_dependentes 
    inner join sf_usuarios on id_fornecedor_despesas = funcionario where sf_usuarios.id_usuario = " . $id_usuario . "))";
}

if (isset($_GET['filial']) && $_GET['filial'] != "null") {
    $sWhere .= " and c.empresa in (" . str_replace("\"", "'", str_replace(array("[", "]"), "", $_GET["filial"])) . ")";
} else {
    $sWhere .= " and c.empresa in (select id_filial_f from sf_usuarios_filiais where id_usuario_f = " . $id_usuario . ")";
}

if (isset($_GET['filtro']) && $_GET['filtro'] != "null") {
    $tipos = explode(",", $_GET['filtro']);
}

if (is_numeric($_GET['txtForma'])) {
    if ($_GET['txtForma'] == "1") {
        $sWhere2 .= " and sa.status = 'Aprovado' ";
    } elseif ($_GET['txtForma'] == "2") {
        $sWhere2 .= " and sa.status is null ";
    } elseif ($_GET['txtForma'] == "3") {
        $sWhere2 .= " and sa.status = 'Aguarda' ";
    } elseif ($_GET['txtForma'] == "4") {
        $sWhere2 .= " and sa.status = 'Reprovado' ";
    }
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $dt_begin = str_replace("_", "/", $_GET['dti']);
    $dt_end = str_replace("_", "/", $_GET['dtf']);
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => 0,
    "iTotalDisplayRecords" => 0,
    "aaData" => array()
);
//-------------------------------------------------------------------------------------------------------------------------
//Planos contratuais que precisam ser provisionados
$sQuery = "set dateformat dmy; select v.id_venda, v.data_venda, c.id_fornecedores_despesas, c.razao_social, c.empresa, p.tipo tipop,
vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) / pp.parcela preco, cc.tipo tipoc, p.valor_comissao_venda, 
case when cc.tipo_valor = 0 then ((vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio)) * isnull(" . $comissao_valor . ", 0)/100) / pp.parcela else cc.valor end preco_pagar,    
u.id_usuario, u.nome vendedor, u.funcionario, vi.id_item_venda, 0 prov, pp.parcela, dt_cancelamento,
c.id_user_resp consultor " . $colunas_valor . " into #tempProdutividade 
from sf_vendas v inner join sf_vendas_itens vi on vi.id_venda = v.id_venda
inner join sf_produtos p on p.conta_produto = vi.produto
inner join sf_fornecedores_despesas c on c.id_fornecedores_despesas = v.cliente_venda
inner join sf_comissao_cliente cc on cc.id_cliente = c.id_fornecedores_despesas
inner join sf_usuarios u on u.id_usuario = cc.id_usuario
left join sf_vendas_planos_mensalidade m on m.id_item_venda_mens = vi.id_item_venda
left join sf_vendas_planos vp on vp.id_plano = m.id_plano_mens
left join sf_produtos_parcelas pp on pp.id_parcela = m.id_parc_prod_mens
where pp.parcela > 1 " . $sWhere . " and v.cov = 'V' and v.status = 'Aprovado' and p.tipo in ('C') and cc.tipo in (1)
and ('" . $dt_begin . " 00:00:00' <= DATEADD(month,pp.parcela,data_venda)) and (data_venda <= '" . $dt_end. " 23:59:59') ";
//echo $sQuery; exit;
odbc_exec($con, $sQuery) or die(odbc_errormsg());
$queryX = "set dateformat dmy;insert into #tempProdutividade ";
$cur = odbc_exec($con, "select * from #tempProdutividade");
while ($RFP = odbc_fetch_array($cur)) {
    for ($i = 1; $i < $RFP['parcela']; $i++) {               
        if (geraTimestamp($dt_begin) <= geraTimestamp(escreverDataSoma(escreverData($RFP['data_venda']), " +" . $i . " month")) && geraTimestamp($dt_end) >= geraTimestamp(escreverDataSoma(escreverData($RFP['data_venda']), " +" . $i . " month"))) {                            
            $queryX .= " select id_venda, dateadd(month," . $i . ",data_venda), id_fornecedores_despesas, razao_social, empresa, tipop, preco, tipoc, valor_comissao_venda, preco_pagar, 
            id_usuario, vendedor, funcionario, id_item_venda, " . $i . ", parcela, dt_cancelamento, consultor
            from #tempProdutividade where id_venda = " . $RFP['id_venda'] . " and tipoc = " . $RFP['tipoc'] . " and id_usuario = " . $RFP['id_usuario'] . " union all";
        }
    }
}
//echo substr_replace($queryX, "", -9); exit;
if (strlen($queryX) > 50) {
    odbc_exec($con, substr_replace($queryX, "", -9));
}
//-------------------------------------------------------------------------------------------------------------------------
//Planos mensais processamento em 2 partes
$sQuery = "set dateformat dmy; select v.id_venda, v.data_venda, c.id_fornecedores_despesas, c.razao_social, c.empresa, p.tipo tipop,
vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) preco, cc.tipo tipoc, p.valor_comissao_venda, 
case when cc.tipo_valor = 0 then ((vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio)) * isnull(" . $comissao_valor . ", 0)/100) else cc.valor end preco_pagar,    
u.id_usuario, u.nome vendedor, u.funcionario, vi.id_item_venda, c.id_user_resp consultor, cc.tipo_valor " . $colunas_valor . " into #temp_rel
from sf_vendas v inner join sf_vendas_itens vi on vi.id_venda = v.id_venda
inner join sf_produtos p on p.conta_produto = vi.produto
inner join sf_fornecedores_despesas c on c.id_fornecedores_despesas = v.cliente_venda
inner join sf_comissao_cliente cc on cc.id_cliente = c.id_fornecedores_despesas
inner join sf_usuarios u on u.id_usuario = cc.id_usuario " . 
$inner_join . " where v.cov = 'V' " . $sWhere . " and v.status = 'Aprovado' and p.tipo in ('C', 'P', 'S')
and data_venda between '" . $dt_begin . " 00:00:00' and '" . $dt_end. " 23:59:59' ";
//echo $sQuery; exit;
odbc_exec($con, $sQuery) or die(odbc_errormsg());
//-------------------------------------------------------------------------------------------------------------------------
$sQuery = "select id_item_venda, id_venda, data_venda,id_fornecedores_despesas, razao_social, vi.empresa, 
case when tipop = 'C' then preco else 0 end preco_plano,
case when tipop = 'S' then preco else 0 end preco_servico,
case when tipop = 'P' then preco else 0 end preco_produto,
case when tipop = 'C' and tipoc = 0 and (select min(vm.id_item_venda_mens) from sf_vendas_planos_mensalidade vm where vm.id_item_venda_mens is not null and vm.id_plano_mens = vp.id_plano) = vi.id_item_venda
then preco_pagar / case when pp.parcela > 1 and vi.tipo_valor = 0 then pp.parcela else 1 end else 0 end comissao_plano_pri,
case when tipop = 'C' and tipoc = 1 and (select min(vm.id_item_venda_mens) from sf_vendas_planos_mensalidade vm where vm.id_item_venda_mens is not null and vm.id_plano_mens = vp.id_plano) <> vi.id_item_venda
then preco_pagar / case when pp.parcela > 1 and vi.tipo_valor = 0 then pp.parcela else 1 end else 0 end comissao_plano,
case when tipop = 'S' and tipoc = 2 and valor_comissao_venda > 0 then preco_pagar else 0 end comissao_servico,
case when tipop = 'P' and tipoc = 3 and valor_comissao_venda > 0 then preco_pagar else 0 end comissao_produto,
vendedor, uc.nome consultor, id_solicitacao_autorizacao, status, 0 prov, pp.parcela, dt_cancelamento, comissao, desconto 
from #temp_rel vi 
left join sf_vendas_planos_mensalidade m on m.id_item_venda_mens = vi.id_item_venda
left join sf_produtos_parcelas pp on pp.id_parcela = m.id_parc_prod_mens
left join sf_vendas_planos vp on vp.id_plano = m.id_plano_mens
left join sf_comissao_historico ch on id_venda = ch.hist_venda and hist_parcela = 0 and hist_vendedor = id_usuario
left join sf_solicitacao_autorizacao sa on sa.id_solicitacao_autorizacao = ch.hist_solicitacao_autorizacao and sa.fornecedor_despesa = funcionario
left join sf_usuarios uc on uc.id_usuario = consultor
where id_item_venda > 0 " . $sWhere2 . " union all 
select id_item_venda, id_venda, data_venda, id_fornecedores_despesas, razao_social, vi.empresa, 
preco preco_plano, 0 preco_servico, 0 preco_produto, 0 comissao_plano_pri, preco_pagar comissao_plano, 0 comissao_servico, 0 comissao_produto,
vendedor, uc.nome consultor, id_solicitacao_autorizacao, status, prov, parcela, dt_cancelamento, comissao, desconto
from #tempProdutividade vi
left join sf_comissao_historico ch on id_venda = ch.hist_venda and hist_parcela = prov and hist_vendedor = id_usuario
left join sf_solicitacao_autorizacao sa on sa.id_solicitacao_autorizacao = ch.hist_solicitacao_autorizacao and sa.fornecedor_despesa = funcionario    
left join sf_usuarios uc on uc.id_usuario = consultor
where prov > 0" . $sWhere2;
//echo $sQuery; exit;
$queryX = "select id_venda, data_venda, vendedor, consultor, id_fornecedores_despesas, razao_social, empresa, sum(preco_plano) preco_plano, sum(preco_servico) preco_servico, 
sum(preco_produto) preco_produto, sum(comissao_plano_pri) comissao_plano_pri, sum(comissao_plano) comissao_plano, 
sum(comissao_servico) comissao_servico, sum(comissao_produto) comissao_produto, id_solicitacao_autorizacao, status, prov, parcela, dt_cancelamento, comissao, desconto
from (select id_item_venda, id_venda, data_venda, vendedor, consultor, id_fornecedores_despesas, razao_social, empresa, max(preco_plano) preco_plano, max(preco_servico) preco_servico, 
max(preco_produto) preco_produto, sum(comissao_plano_pri) comissao_plano_pri, sum(comissao_plano) comissao_plano, 
sum(comissao_servico) comissao_servico, sum(comissao_produto) comissao_produto, id_solicitacao_autorizacao, status, prov, 
case when parcela < 1 then 1 else isnull(parcela, 1) end parcela, dt_cancelamento, comissao, desconto from (" . $sQuery . ") as x group by id_item_venda, id_venda, data_venda, 
vendedor, consultor, id_fornecedores_despesas, razao_social, empresa, id_solicitacao_autorizacao, status, prov, parcela, dt_cancelamento, comissao, desconto) as y 
group by id_venda, data_venda, vendedor, consultor, id_fornecedores_despesas, razao_social, empresa, id_solicitacao_autorizacao, status, prov, parcela, dt_cancelamento, comissao, desconto order by data_venda";
//echo $queryX; exit;
$cur = odbc_exec($con, $queryX);
while ($aRow = odbc_fetch_array($cur)) {                
    if (isset($_GET["functionPage"])) {
        if (($aRow["comissao_plano_pri"] > 0 && in_array("0", $tipos)) || ($aRow["comissao_plano"] > 0 && in_array("1", $tipos)) || ($aRow["comissao_servico"] > 0 && in_array("2", $tipos)) || ($aRow["comissao_produto"] > 0 && in_array("3", $tipos))) {
            $valor = ($aRow["comissao_plano_pri"] + $aRow["comissao_plano"] + $aRow["comissao_servico"] + $aRow["comissao_produto"]);            
            $row["data_venda"] = escreverDataHora($aRow["data_venda"]);
            $row["id_venda"] = $aRow["id_venda"];
            $row["nome"] = utf8_encode($aRow['razao_social']);
            $row["parcela"] = ($aRow["prov"] + 1) . "/" . $aRow["parcela"];        
            $row["preco_plano"] = escreverNumero($aRow["preco_plano"], 1);
            $row["preco_servico"] = escreverNumero($aRow["preco_servico"], 1);
            $row["preco_produto"] = escreverNumero($aRow["preco_produto"], 1);
            $row["comissao_plano_pri"] = escreverNumero($aRow["comissao_plano_pri"], 1);
            $row["comissao_plano"] = escreverNumero($aRow["comissao_plano"], 1);        
            $row["comissao_servico"] = escreverNumero($aRow["comissao_servico"], 1);
            $row["comissao_produto"] = escreverNumero($aRow["comissao_produto"], 1);
            $row["valor_comissao"] = escreverNumero($valor, 1);        
            $row["produto"] = "";
            $row["status"] = (strlen($aRow['status']) > 0 ? $aRow['status'] : "Pendente");
            $output['aaData'][] = $row;
            $cont++;
        }
    } else {    
        $fontColor = "";
        if ($aRow["dt_cancelamento"]) {
            $fontColor = "style=\"color: red;\" title=\"Plano Cancelado em " . escreverData($aRow["dt_cancelamento"]) . "\" ";
        }    
        if (($aRow["comissao_plano_pri"] > 0 && in_array("0", $tipos)) || ($aRow["comissao_plano"] > 0 && in_array("1", $tipos)) || ($aRow["comissao_servico"] > 0 && in_array("2", $tipos)) || ($aRow["comissao_produto"] > 0 && in_array("3", $tipos))) {
            $valor = ($aRow["comissao_plano_pri"] + $aRow["comissao_plano"] + $aRow["comissao_servico"] + $aRow["comissao_produto"]);            
            $row = array();
            $row[] = "<input type=\"checkbox\" name=\"items[]\" val=\"" . escreverNumero($valor) . "\" value=\"" . $aRow["id_venda"] . "-" . $aRow["prov"] . "\" " . ($valor > 0 && !is_numeric($aRow['id_solicitacao_autorizacao']) ? "" : "disabled") . ">";
            $row[] = "<div " . $fontColor . ">" . $aRow["id_venda"] . " (" . ($aRow["prov"] + 1) . "/" . $aRow["parcela"] . ")" . "</div>";
            $row[] = "<div " . $fontColor . ">" . escreverDataHora($aRow["data_venda"]) . "</div>";
            if (isset($_GET['txtExibir']) && $_GET['txtExibir'] == "0") {
                $row[] = "<div " . $fontColor . ">" . formatNameCompact(utf8_encode($aRow["vendedor"])) . "</div>";        
            } else {
                $row[] = "<div " . $fontColor . ">" . formatNameCompact(utf8_encode($aRow["consultor"])) . "</div>";
            }
            $row[] = "<a href='javascript:void(0)' onClick='AbrirCli(" . utf8_encode($aRow['id_fornecedores_despesas']) . ")'><div " . $fontColor . " id='formPQ' title='" . utf8_encode($aRow['razao_social']) . "'>" . utf8_encode($aRow['razao_social']) . "</div></a>";
            $row[] = escreverNumero($aRow["preco_plano"], 1);
            $row[] = escreverNumero($aRow["preco_servico"], 1);
            $row[] = escreverNumero($aRow["preco_produto"], 1);
            $row[] = escreverNumero($aRow["comissao_plano_pri"], 1);
            $row[] = escreverNumero($aRow["comissao_plano"], 1);
            $row[] = escreverNumero($aRow["comissao_servico"], 1);
            $row[] = escreverNumero($aRow["comissao_produto"], 1);
            $row[] = escreverNumero($valor, 1);
            if (is_numeric($aRow['id_solicitacao_autorizacao'])) {
                $Solicitacao = "<a title=\"" . $aRow['status'] . "\" href=\"./../Contas-a-pagar/FormSolicitacao-de-Autorizacao.php?id=" . $aRow['id_solicitacao_autorizacao'] . "\">";
                $Solicitacao .= "<img src=\"../../img/";
                if ($aRow['status'] == 'Reprovado') {
                    $Solicitacao .= "inativo";
                } else if ($aRow['status'] == 'Aprovado') {
                    $Solicitacao .= "pago";
                } else if ($aRow['status'] == 'Aguarda') {
                    $Solicitacao .= "apagar";
                } else {
                    $Solicitacao .= "back";
                }
                $Solicitacao .= ".PNG\" value=\"\"></a>";
                $row[] = $Solicitacao;
            } else {
                $row[] = "<img src=\"../../img/dollar" . ($valor > 0 ? "" : "2") . ".png\" value=\"\">";
            }
            if (isset($_GET['tpImp']) && $_GET['tpImp'] == "E") {
                $row[] = escreverNumero($aRow["comissao"], 0);
                $row[] = escreverNumero($aRow["desconto"], 0);
            }
            if (isset($_GET['pdf'])) {
                if (isset($_GET['txtExibir']) && $_GET['txtExibir'] == "0") {
                    $row[] = utf8_encode($aRow['vendedor']);
                    $grp = array(utf8_encode($aRow['vendedor']), ($aRow['vendedor'] == "" ? "SEM GRUPO" : utf8_encode($aRow['vendedor'])), 12);
                    if (!in_array($grp, $grupoArray)) {
                        $grupoArray[] = $grp;
                    }
                } else {
                    $row[] = utf8_encode($aRow['consultor']);
                    $grp = array(utf8_encode($aRow['consultor']), ($aRow['consultor'] == "" ? "SEM GRUPO" : utf8_encode($aRow['consultor'])), 12);
                    if (!in_array($grp, $grupoArray)) {
                        $grupoArray[] = $grp;
                    }
                }
            }
            $output['aaData'][] = $row;
            $cont++;
        }
    }    
}
$output["iTotalRecords"] = $cont;
$output["iTotalDisplayRecords"] = $cont;
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
} else {
    $output['aaTotal'][] = array(7, 7, "Sub Total", "Total", 0, 0);
    $output['aaTotal'][] = array(8, 8, "", "", 0, 0);
    $output['aaTotal'][] = array(9, 9, "", "", 0, 0);
    $output['aaTotal'][] = array(10, 10, "", "", 0, 0);
    $output['aaTotal'][] = array(11, 11, "", "", 0, 0);
    $output['aaGrupo'] = $grupoArray;
}
odbc_close($con);
