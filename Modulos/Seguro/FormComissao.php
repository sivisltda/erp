<?php
include ('../../Connections/configini.php');
$dataVenc = getData("T");
$id_funcionario = 0;
$nome_funcionario = '';
$usuario = "";

if ($_GET['us'] != "") {
    $usuario = $_GET['us'];
}

if (is_numeric($_GET['tp'])) {    
    if ($_GET['tp'] == "3") { //Professor Responsável
        $query = "select id_fornecedores_despesas, razao_social from sf_fornecedores_despesas f 
        where f.tipo in ('E','I') and f.id_fornecedores_despesas = " . valoresSelect2($usuario);
    } else { //Usuário Responsável
        $query = "select id_fornecedores_despesas, razao_social from sf_fornecedores_despesas f
        inner join sf_usuarios u on f.id_fornecedores_despesas = u.funcionario 
        where u.id_usuario = " . valoresSelect2($usuario);
    }
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $id_funcionario = utf8_encode($RFP['id_fornecedores_despesas']);
        $nome_funcionario = utf8_encode($RFP['razao_social']);
    }
}

if (isset($_POST['bntSave'])) {
    $dtVencimento = valoresData("txtVencimento");
    $valorParcela = valoresNumericos("txtValor");
    if (is_numeric($valorParcela) && $valorParcela > 0 && $dtVencimento != "null" && $id_funcionario > 0) {
        $query = "set dateformat dmy;insert into sf_solicitacao_autorizacao(empresa,grupo_conta,conta_movimento,fornecedor_despesa,tipo_documento,historico,destinatario,comentarios,sys_login,data_lanc,status)values('001'," .
        valoresSelect("txtGrupoConta") . "," . valoresSelect("txtContaConta") . "," . $id_funcionario . "," .
        valoresSelect("txtTipo") . "," . valoresTexto("txtHistorico") . "," . valoresTexto("txtDestinatario") . "," .
        valoresTexto("txtComentarios") . ",'" . $_SESSION["login_usuario"] . "',GetDate(),'Aguarda')";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_solicitacao_autorizacao from sf_solicitacao_autorizacao order by id_solicitacao_autorizacao desc") or die(odbc_errormsg());
        $nn = odbc_result($res, 1);
        //----------------------------------------------Parcelas-----------------------------------------                                
        odbc_exec($con, "set dateformat dmy; insert into sf_solicitacao_autorizacao_parcelas(solicitacao_autorizacao,numero_parcela,data_parcela,sa_descricao,valor_parcela,historico_baixa,pa,bol_id_banco,bol_data_criacao,bol_valor,bol_juros,bol_multa,bol_nosso_numero,data_cadastro,tipo_documento) values (" .
        $nn . ",1," . $dtVencimento . ",''," . $valorParcela . "," . valoresTexto("txtHistorico") . ",'01/01',null,null,null,null,null,null,GETDATE()," . valoresSelect("txtTipo") . ")") or die(odbc_errormsg());
        //----------------------------------------------Historico----------------------------------------
        $count = explode("|", $_POST['txtId']);
        if (count($count) > 0) {
            $i = 0;
            while ($i < count($count) - 1) {
                $item = explode("-", $count[$i]);
                if (is_numeric($item[0])) {
                    odbc_exec($con, "INSERT sf_comissao_historico (hist_venda, hist_vendedor, hist_solicitacao_autorizacao, hist_parcela)
                    SELECT DISTINCT " . $item[0] . ", " . valoresSelect2($usuario) . ", " . $nn . ", " . valoresNumericos2($item[1]) . " FROM sf_comissao_historico cr WHERE NOT EXISTS (SELECT * FROM sf_comissao_historico c
                    WHERE hist_venda = " . $item[0] . " and hist_vendedor = " . valoresSelect2($usuario) . " and hist_solicitacao_autorizacao = " . $nn . " and hist_parcela = " . valoresNumericos2($item[1]) . ")") or die(odbc_errormsg());
                }
                $i++;
            }
        }
        echo "<script>parent.FecharBox();</script>";
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Comissões</title>
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
    </head>
    <link rel="icon" type="image/ico" href="../../favicon.ico"/>
    <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
    <link href="../../css/main.css" rel="stylesheet"/>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>    
    <body style="overflow:hidden">
        <div style="width:400px; margin:0; padding:0;">
            <form name="frmForm" action="FormComissao.php<?php echo "?tp=" . $_GET['tp'] . "&us=" . $_GET['us'];?>" method="POST">
                <input id="txtId" name="txtId" type="hidden" value=""/>
                <div style="float: left; width: 100%; margin-top: 10px;">
                    <div style="float: left;width: 29%; text-align: right;margin-top: 5px;">
                        Destinatário:
                    </div>
                    <div style="float: left;width: 60%; margin-left: 1%;">
                        <select name="txtDestinatario" id="txtDestinatario" style="width: 220px;" class="input-medium">
                            <option value="null">--Selecione--</option>
                            <?php $cur = odbc_exec($con, "select login_user,nome from sf_usuarios where inativo = 0 and login_user != 'Admin' order by nome") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP['login_user']; ?>" <?php echo ($RFP['login_user'] == $_SESSION["login_usuario"] ? "selected" : "");?>><?php echo utf8_encode($RFP['nome']) ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div style="float: left;width: 10%">
                        <button style="border:none; float:right; width:25px; margin-right:10px; color:#FFFFFF; background-color:#333333" onClick="parent.FecharBox();" id="bntOK"><b>x</b></button>
                    </div>
                </div>
                <div style="float: left; width: 100%; margin-top: 5px;">
                    <div style="float: left;width: 29%; text-align: right;margin-top: 5px;">
                        Grupo:
                    </div>
                    <div style="float: left;width: 60%; margin-left: 1%;">
                        <select name="txtGrupoConta" id="txtGrupoConta" style="width: 220px;" class="input-medium">
                            <option value="null">--Selecione--</option>
                            <?php $cur = odbc_exec($con, "select id_grupo_contas,descricao from sf_grupo_contas where deb_cre = 'D' and inativo = 0 order by descricao") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP['id_grupo_contas']; ?>"><?php echo utf8_encode($RFP['descricao']) ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>                
                <div style="float: left; width: 100%; margin-top: 5px;">
                    <div style="float: left;width: 29%; text-align: right;margin-top: 5px;">
                        Conta:
                    </div>
                    <div style="float: left;width: 60%; margin-left: 1%;">
                        <select name="txtContaConta" id="txtContaConta" style="width: 220px;" class="input-medium">
                            <option value="null">--Selecione--</option>
                        </select>
                    </div>
                </div>
                <div style="float: left; width: 100%; margin-top: 5px;">
                    <div style="float: left;width: 29%; text-align: right;margin-top: 5px;">
                        Forma de Pagto:
                    </div>
                    <div style="float: left;width: 60%; margin-left: 1%;">
                        <select name="txtTipo" id="txtTipo" style="width: 220px;" class="input-medium">
                            <option value="null">--Selecione--</option>
                            <?php $cur = odbc_exec($con, "select id_tipo_documento,descricao from sf_tipo_documento where s_tipo in ('A','D') order by descricao") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP['id_tipo_documento']; ?>"><?php echo utf8_encode($RFP['descricao']) ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>                
                <div style="float: left; width: 100%; margin-top: 5px;">
                    <div style="float: left;width: 29%; text-align: right; margin-top: 5px;">
                        Vencimento:
                    </div>
                    <div style="float: left;width: 60%; margin-left: 1%;">
                        <input type="text" name="txtVencimento" id="txtVencimento" class="datepicker input-medium inputCenter" style="width:100px" value="<?php echo $dataVenc; ?>"/>
                    </div>
                </div>
                <div style="float: left; width: 100%; margin-top: 5px;">
                    <div style="float: left;width: 29%; text-align: right; margin-top: 5px;">
                        Valor:
                    </div>
                    <div style="float: left;width: 60%; margin-left: 1%;">
                        <input type="text" name="txtValor" id="txtValor" class="input-medium inputCenter" style="width:100px" value="0,00" readonly/>
                    </div>
                </div>
                <div style="float: left; width: 100%; margin-top: 5px;">
                    <div style="float: left;width: 29%; text-align: right; margin-top: 5px;">
                        Histórico:
                    </div>
                    <div style="float: left;width: 60%; margin-left: 1%;">
                        <input type="text" name="txtHistorico" id="txtHistorico" class="input-medium" style="width:220px" value="Comissão"/>
                    </div>
                </div>
                <div style="float: left; width: 100%; margin-top: 5px; margin-bottom: 10px;">
                    <div style="float: left;width: 29%; text-align: right; margin-top: 5px;">
                        Observação:
                    </div>
                    <div style="float: left;width: 60%; margin-left: 1%;">
                        <textarea name="txtComentarios" id="txtComentarios" style="width:220px" cols="5" rows="3"></textarea>
                    </div>
                </div>                
                <div class="toolbar bottom tar" style="margin:10px; border-top:solid 1px #CCC; overflow:hidden">
                    <div class="data-fluid" style="overflow:hidden; padding-bottom:10px">
                        <div style="float: left;text-align: left">
                            Funcionário:<strong><?php echo ($id_funcionario > 0 ? formatNameCompact($nome_funcionario) : " Não encontrado"); ?></strong><br/>
                        </div>
                        <div class="btn-group">
                            <button id="bntOK" name="bntSave" type="submit" class="btn btn-success" onclick="return validaForm();" style="margin-top:20px"><span class="ico-refresh"></span> Gerar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <script type="text/javascript" src="../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../../js/plugins/select/select2.min.js"></script>
        <script type="text/javascript" src="../../../js/plugins.js"></script>
        <script type="text/javascript" src="../../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
        <script type="text/javascript" src="../../../js/plugins/datatables/fnReloadAjax.js"></script>
        <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../../js/moment.min.js"></script>
        <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../../js/jquery.priceformat.min.js"></script>    
        <script type="text/javascript" src="../../../js/util.js"></script>       
        <script type="text/javascript">
            
            $("#txtVencimento").mask(lang["dateMask"]);            
            $("#txtValor").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
            
            function validaForm() {
                if ($("#txtDestinatario").val() === "null") {
                    bootbox.alert("Selecione um Destinatário!");
                    return false;
                }                   
                if ($("#txtVencimento").val() === "") {
                    bootbox.alert("Vencimento Inválido!");
                    return false;
                }                   
                if ($("#txtValor").val() === "0,00") {
                    bootbox.alert("Valor Inválido!");
                    return false;
                }                   
                if ($("#txtHistorico").val() === "") {
                    bootbox.alert("Histórico Inválido!");
                    return false;
                }                   
                if ($("#txtGrupoConta").val() === "") {
                    bootbox.alert("Grupo Inválido!");
                    return false;
                }                   
                if ($("#txtContaConta").val() === "") {
                    bootbox.alert("Conta Inválida!");
                    return false;
                }                   
                if ($("#txtTipo").val() === "") {
                    bootbox.alert("Forma de Pagto Inválida!");
                    return false;
                }    
                localStorage.setItem('txtGrupoConta', $("#txtGrupoConta").val());
                localStorage.setItem('txtContaConta', $("#txtContaConta").val());
                localStorage.setItem('txtTipo', $("#txtTipo").val());
                return true;
            }
            
            $("#txtGrupoConta").change(function () {
                getContas("null");
            });  
            
            function getContas(id) {
                $.getJSON("./../Estoque/conta.ajax.php", {txtGrupo: $("#txtGrupoConta").val(), ajax: "true"}, function (j) {
                    var options = "<option value='null'>--Selecione--</option>";
                    for (var i = 0; i < j.length; i++) {
                        options += "<option value='" + j[i].id_contas_movimento + "'>" + j[i].descricao + "</option>";
                    }
                    $("#txtContaConta").html(options);
                    $("#txtContaConta").val(id);
                });                
            }
            
            let param = parent.getAllSelected();
            $("#txtId").val(param[0]);
            $("#txtValor").val(numberFormat(param[1]));    
            
            $("#txtGrupoConta").val(localStorage.getItem('txtGrupoConta'));
            getContas(localStorage.getItem('txtContaConta'));
            $("#txtTipo").val(localStorage.getItem('txtTipo'));            
            
        </script>
        <?php odbc_close($con); ?>                
    </body>
</html>