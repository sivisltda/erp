<?php
include "../../Connections/configini.php";
include "form/FormVistoriasServer.php";
?>
<!DOCTYPE html>
<head>
    <link rel="icon" type="image/ico" href="../../../favicon.ico"/>
    <link href="../../../css/stylesheets.css" rel="stylesheet" type="text/css" />
    <link href="../../../css/stylesheet.css" rel="stylesheet" type="text/css" />
    <link href="../../../css/main.css" rel="stylesheet">
    <link href="../../../css/bootbox.css" rel="stylesheet" type="text/css"/>
    <link href="../../../js/leaflet/leaflet.css" rel="stylesheet" type="text/css"/>     
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    <style type="text/css">
        .labcont {
            width: 100%;
            border: 1px solid transparent;
        }
        .labhead {
            padding: 0;
            border: 1px solid transparent;
        }
        .liscont {
            width: 100%;
            border: 1px solid #EEE;
        }
        .lishead {
            padding: 0;
            background: #EEE;
            border: 1px solid #FFF;
        }
        .lisbody {
            padding: 0 5px;
            line-height: 28px;
            background: #F6F6F6;
            border: 1px solid #FFF;
        }
        .lisfoot {
            padding: 0 5px;
            background: #EEE;
            text-align: right;
            line-height: 28px;
            font-weight: bold;
            border: 1px solid #FFF;
        }
        .lisbody:nth-child(3) { text-align: center; }
        .lisbody:nth-child(4), .lisbody:nth-child(5) { text-align: right; }
        .lisbody.selected { background: #acbad4 !important;}        
        .img-car {
            border: 1px solid #ddd;
            height: 50px;            
            width: 100%;                
        }
        .box {
            float: left;
            margin-left: 2%;
            margin-bottom: 2%;         
            width: 23%;            
        }
        .img-car-master {
            margin-top: 3.5%;
            width: 97%;
        }
        .left-imgs {
            float: left; 
            width: 34%; 
            overflow: hidden; 
        }
        .item-ckb {
            float:left; 
            width:24%;
            margin-left: 1%;
            display: flex;
        }
        .deleteImg {
            position: absolute; 
            margin: 0px; 
            padding: 2px 4px 3px; 
            line-height: 10px; 
            cursor: pointer;
        }
    </style>
</head>
<body>
    <div class="row-fluid">
        <form id="frmVistorias" name="frmVistorias" action="FormVistorias.php" method="POST" enctype="multipart/form-data">
            <div class="frmhead">
                <div class="frmtext">Vistoria</div>
                <div class="frmicon" style="top:13px" onClick="parent.FecharBoxVistorias()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <input id="txtId" name="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <input id="txtImgSel" name="txtImgSel" value="" type="hidden"/>
            <input id="txtIdVeiculo" name="txtIdVeiculo" value="<?php echo $id_veiculo; ?>" type="hidden"/>
            <input id="txtVeiculoTipo" name="txtVeiculoTipo" value="<?php echo $veiculo_tipo; ?>" type="hidden"/>
            <input id="txtLatitude" name="txtLatitude" value="<?php echo $latitude; ?>" type="hidden"/>
            <input id="txtLongitude" name="txtLongitude" value="<?php echo $longitude; ?>" type="hidden"/>
            <input id="txtReadOnly" value="<?php echo $ckb_adm_cli_read_; ?>" type="hidden"/>
            <div class="left-imgs">
                <div class="tabbable frmtabs">
                    <ul class="nav nav-tabs" style="margin:0">
                        <li class="active"><a href="#tab11" data-toggle="tab">Perfil do Veículo</a></li>
                        <li><a href="#tab12" data-toggle="tab">Danos, Avarias e Outros</a></li>
                    </ul>
                    <div class="tab-content frmcont" style="height:390px; font-size:11px !important; overflow: unset;">
                        <div class="tab-pane active" id="tab11">                            
                            <?php 
                            for ($i = 0; $i < 24; $i++) { 
                                $imgRead = getImageCar($Imagens, $id . "_img" . str_pad($i, 2, "0", STR_PAD_LEFT), $contrato, $id, "Vistorias"); 
                                $imgExt = substr($imgRead, -3); ?>
                            <div class="box">
                                <?php if ($ckb_adm_cli_read_ == 0) { ?>
                                <input id="del<?php echo str_pad($i, 2, "0", STR_PAD_LEFT); ?>" class="btn red deleteImg" onclick="removerImg('<?php echo end(explode("/", $imgRead)); ?>','<?php echo str_pad($i, 2, "0", STR_PAD_LEFT); ?>')" type="button" value="x" <?php echo ($imgRead == "./../../img/noimage.png" ? "style=\"display: none;\"" : ""); ?>>
                                <?php } ?>
                                <a href="javascript:void(0)">                                
                                    <img id="img<?php echo str_pad($i, 2, "0", STR_PAD_LEFT); ?>" alt="Imagem" data="<?php echo $imgRead; ?>" class="img-car" src="<?php echo ($imgExt == "3gp" || $imgExt == "mp4" ? "./../../img/novideo.png" : $imgRead); ?>"/>
                                </a>
                            </div>
                            <?php } if (is_numeric($id) && strlen($disabled) > 0) { ?>
                            <input id="file" name="file" type="file" style="display: none;"/>
                            <button id="bntSendFile" name="bntSendFile" class="btn btn-success" type="submit" style="display: none;" onClick="return validaFormImg();"><span class="ico-upload-alt"></span> Enviar Imagem</button>                
                            <?php } ?>
                            <div style="width:40%; float:left; margin-left:1%;">
                                <span>Data/Hora:</span>
                                <input id="txtData" name="txtData" class="input-xlarge" type="text" style="min-height: 28px;" value="<?php echo $data; ?>" disabled/>
                            </div>                        
                            <div style="width:45%; float:left; margin-left:1%;">
                                <span>Usuário:</span>
                                <input id="txtVistoriador" name="txtVistoriador" class="input-xlarge" type="text" style="min-height: 28px;" value="<?php echo $vistoriador; ?>" disabled/>
                            </div>                            
                            <div style="width:12%; float:left; margin-left:1%;">
                                <span>Tipo:</span>
                                <?php if (strlen($latitude) > 0 && strlen($longitude) > 0) { ?>
                                    <button class="btn btn-default" type="button" title="Lançamento Aplicativo"><span class="ico-map-marker"></span></button>
                                <?php } else { ?>
                                    <button class="btn yellow" type="button" title="Lançamento Manual"><span class="ico-laptop"></span></button>
                                <?php } ?>
                            </div>                            
                        </div>  
                        <div class="tab-pane" id="tab12">
                            <?php if (is_numeric($id) && strlen($disabled) > 0) { ?>
                            <img id="img99" alt="Imagem" style="width: 23%;" class="img-car" src="./../../img/noimage.png"/>
                            <div style="width:61%; float:left; margin-left: 1%;">
                                <span>Descrição:</span>
                                <input id="txtDanos" name="txtDanos" class="input-xlarge" type="text" style="min-height: 37px;" value=""/>
                            </div>
                            <input id="fileDanos" name="fileDanos" type="file" style="display: none;"/>                            
                            <button id="bntSendFileDanos" name="bntSendFileDanos" class="btn btn-success" type="submit" style="min-height: 37px; margin-left: 1%; margin-top: 15px;" onClick="return validaFormImgDanos();" disabled><span class="ico-upload-alt"></span></button>
                            <?php } ?>                            
                            <div style="clear:both;"></div>
                            <hr style="margin: 10px;">
                            <?php 
                            if (is_numeric($id)) {
                                $cur = odbc_exec($con, "select * from sf_vistorias_danos where id_vistoria = " . $id . " order by id");
                                while ($RFP = odbc_fetch_array($cur)) { 
                                    $imgRead = getImageCar($ImagensDanos, $RFP["id"], $contrato, $id, "Danos");
                                ?>
                                <div class="box">
                                    <?php if ($ckb_adm_cli_read_ == 0) { ?>
                                        <input id="delAv<?php echo utf8_encode($RFP["id"]); ?>" class="btn red deleteImg" onclick="removerImgDanos('<?php echo array_reverse(explode('/', $imgRead))[0]; ?>','<?php echo utf8_encode($RFP["id"]); ?>')" type="button" value="x">
                                    <?php } ?>                                    
                                    <a href="javascript:void(0)">
                                        <img alt="Imagem" ondblclick="window.open('<?php echo $imgRead; ?>', '_blank');" title="<?php echo utf8_encode($RFP["observacao"]); ?>" class="img-car" src="<?php echo $imgRead; ?>"/>
                                    </a>
                                </div>
                            <?php }} ?>
                        </div>                            
                    </div>                            
                </div>                            
            </div>
            <div style="float: left; width: 66%;">                
                <div class="tabbable frmtabs" style="padding-left: 0px;">
                    <ul class="nav nav-tabs" style="margin:0">
                        <li class="active"><a href="#tab1" data-toggle="tab">Dados Principais</a></li>
                        <li><a href="#tab2" onclick="refreshPointer();" data-toggle="tab">Localização</a></li>                        
                        <li><a href="#tab3" data-toggle="tab">Observações</a></li>
                    </ul>
                    <div class="tab-content frmcont" style="height:390px; font-size:11px !important; overflow: unset;">
                        <div class="tab-pane active" id="tab1">
                            <div style="width:12%; float:left;">
                                <span>Placa:</span>
                                <input id="txtPlaca" name="txtPlaca" class="input-xlarge" maxlength="8" type="text" style="min-height: 28px;" value="<?php echo $placa; ?>" <?php echo $disabled; ?>/>
                            </div>    
                            <div style="width:22%; float:left; margin-left:1%">
                                <span>Tipo:</span>
                                <select id="txtTipo" name="txtTipo" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>                                
                                    <option value="null">Selecione</option>
                                    <?php if (is_numeric($veiculo_tipo)) {
                                    $query = "SELECT id, descricao from sf_vistorias_tipo where (id in (select id_vistoria from sf_vistorias_veiculo_tipos where id_tipo = " . $veiculo_tipo . ") or 
                                    (select count(id_tipo) from sf_vistorias_veiculo_tipos where id_vistoria = sf_vistorias_tipo.id) = 0) and inativo = 0";
                                    $cur = odbc_exec($con, $query);
                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                        <option value="<?php echo utf8_encode($RFP['id']); ?>" <?php echo ($tipo == $RFP['id'] ? "SELECTED" : ""); ?>><?php echo utf8_encode($RFP['descricao']); ?></option>
                                    <?php }} ?>                                
                                </select>
                            </div>
                            <div style="width:30%; float:left; margin-left:1%">
                                <span>Pagamento de Serviço:</span>
                                <select id="txtVendaItem" name="txtVendaItem" class="select input-medium" style="width:100%" <?php echo $disabled; ?> <?php echo (is_numeric($id_item_venda) ? "readonly" : "");?>>                              
                                    <option value="null">Selecione</option>
                                    <?php if (is_numeric($info_id_cliente)) {
                                        $query = "select id_item_venda, data_venda, descricao, vendedor_comissao, quantidade 
                                        from sf_vendas_itens vi inner join sf_vendas v on v.id_venda = vi.id_venda
                                        inner join sf_produtos p on p.conta_produto = vi.produto
                                        where produto = 13 and cliente_venda = " . $info_id_cliente . " and (vendedor_comissao is null or id_item_venda = " . valoresNumericos2($id_item_venda) . ")";  
                                        $cur = odbc_exec($con, $query);
                                        while ($RFP = odbc_fetch_array($cur)) {
                                            if ($id_item_venda == $RFP['id_item_venda']) {
                                                $funcionario = $RFP['vendedor_comissao'];
                                            } ?>
                                    <option value="<?php echo utf8_encode($RFP['id_item_venda']); ?>" <?php echo ($id_item_venda == $RFP['id_item_venda'] ? "SELECTED" : ""); ?>><?php echo escreverDataHora($RFP['data_venda']) . " Qtd. " . escreverNumero($RFP['quantidade'], 0, 0); ?></option>
                                    <?php }} ?>   
                                </select>
                            </div>                                    
                            <div style="width:33%; float:left; margin-left:1%">
                                <span>Vistoriador:</span>
                                <select id="txtFuncionario" name="txtFuncionario" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>                              
                                    <option value="null">Selecione</option>
                                    <?php $query = "SELECT id_fornecedores_despesas, razao_social FROM sf_usuarios u 
                                    INNER JOIN sf_fornecedores_despesas f ON u.funcionario = f.id_fornecedores_despesas  
                                    WHERE f.recebe_comissao = 1 and f.inativo = 0 AND dt_demissao IS NULL AND f.tipo = 'E'";
                                    $cur = odbc_exec($con, $query);
                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                        <option value="<?php echo utf8_encode($RFP['id_fornecedores_despesas']); ?>" <?php echo ($funcionario == $RFP['id_fornecedores_despesas'] ? "SELECTED" : ""); ?>><?php echo utf8_encode($RFP['razao_social']); ?></option>
                                    <?php } ?>                                    
                                </select>
                            </div>                                    
                            <div style="clear:both;"></div>
                            <hr style="margin: 10px 0;">
                            <div id="txtInfo">
                                <p><?php echo $info_veiculo; ?></p>
                                <p><?php echo $info_cliente; 
                                if (is_numeric($id_telemarketing)) { ?>
                                    <span style="float: right;"><?php echo "Número OS: " . str_pad($id_telemarketing, 8, "0", STR_PAD_LEFT) . " de " .$data_tele; ?></span>
                                <?php } ?>
                                </p>
                            </div>    
                            <hr style="margin: 10px 0;">
                            <b>ACESSÓRIOS / EQUIPAMENTOS EXISTENTES</b>
                            <span style="float: right;">
                                <?php 
                                if ($data_aprov == "") {
                                    echo "<div id='formPQ' style=\"color:#bc9f00\" title=\"Pendente\"><center>Pendente </center></div>";
                                } else {
                                    echo "<div id='formPQ' style=\"color:#0066cc\" title=\"Concluída\"><center>Concluída (" . $data_aprov .")</center></div>";
                                } ?>
                            </span>
                            <hr style="margin: 10px 0;">                            
                            <div id="txtItens"></div>                                     
                        </div>                                     
                        <div class="tab-pane" id="tab2">
                            <div id="map" style="float: left; width: 100%; height:390px;"></div>
                        </div>                        
                        <div class="tab-pane" id="tab3">
                            <div style="width:15%; float:left;">
                                <span>Observações:</span>
                                <textarea id="txtObs" name="txtObs" style="min-width: 630px;min-height: 375px;" <?php echo $disabled; ?>><?php echo $obs; ?></textarea>
                            </div>                        
                        </div>
                    </div>
                </div>
            </div>     
            <div style="clear:both;"></div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if($ckb_adm_cli_read_ == 0) { ?>
                    <?php if ($data_aprov == "" && is_numeric($id)) { ?>
                        <div style="float:left;">
                            <button class="btn btn-success" type="submit" name="bntAprov" title="Aprovar" id="bntAprov"><span class="ico-checkmark"></span> Concluir</button>
                        </div>
                    <?php } else if (strlen($data_aprov) > 0 && is_numeric($id)) { ?>
                        <div style="float:left;">
                            <button class="btn yellow" type="submit" name="bntReprov" title="Cancelar" id="bntReprov"><span class="ico-undo"></span> Ficar Pendente</button>
                        </div>                    
                    <?php } if (is_numeric($id)) { ?>
                        <div style="float:left;">                            
                            <button onclick="window.open('https://vistoria.sivisweb.com.br/<?php echo $id; ?>/<?php echo $contrato; ?>', '_blank'); " class="btn btn-default" type="button" name="bntLink" title="Link" id="bntLink"><span class="ico-link"></span></button>
                        </div>                                        
                    <?php } if ($disabled == "") { ?>
                        <button id="bntSave" name="bntSave" type="submit" class="btn green" onClick="return validaForm();"><span class="ico-checkmark"></span> Gravar</button>
                        <?php if ($_POST["txtId"] == "") { ?>
                            <button class="btn yellow" onClick="parent.FecharBoxVistorias()" id="bntCancelar"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                            <button class="btn yellow" type="submit" name="bntAlterar" id="bntAlterar"><span class="ico-reply"></span> Cancelar</button>
                            <?php
                        }
                    } else { ?>
                        <div style="float: left; margin-top: 15px;">
                            <select id="txtContrato" class="select" style="width:100%">
                                <?php $cur = odbc_exec($con, "select cnt.id, cnt.descricao, cnt.logo, cnt.margem_d from sf_contratos cnt where cnt.inativo = 0 and cnt.tipo = 4") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP['id']; ?>" logo="<?php echo $RFP['logo']; ?>" mdi="<?php echo $RFP['margem_d']; ?>"><?php echo utf8_encode($RFP['descricao']); ?></option> 
                                <?php } ?>
                            </select>                     
                        </div> 
                        <button style="float: left; margin-top: 15px;" class="btn button-orange" type="button" name="bntAssinar" id="bntAssinar" onclick="Assinar()" title="Assinar" value="Assinar"><span class="ico-pen icon-white"></span></button>                                                                                                                                            
                        <button style="float: left; margin-top: 15px;" class="btn button-blue" type="button" name="bntPrint" id="bntPrint" onclick="Print()" title="Imprimir" value="Imprimir"><span class="icon-print icon-white"></span></button>                                                                                                                                            
                        <button class="btn green" type="submit" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" type="submit" name="bntEdit" id="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                        <button class="btn red" type="submit" name="bntDelete" id="bntDelete" value="Excluir"><span class=" ico-remove"></span> Excluir</button>
                    <?php }} ?>
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins.js"></script>
    <script type="text/javascript" src="../../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type="text/javascript" src="../../../js/plugins/datatables/fnReloadAjax.js"></script>
    <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../../js/jquery.priceformat.min.js"></script> 
    <script type="text/javascript" src="../../../js/leaflet/leaflet.js"></script>    
    <script type="text/javascript" src="../../../js/util.js"></script>                                        
    <script type="text/javascript" src="js/FormVistorias.js"></script>
    <script type="text/javascript">
        
        var pIniLat = "-22.5252";
        var pIniLog = "-44.1038";

        if($("#txtLatitude").val() !== "" && $("#txtLongitude").val() !== "") {
            pIniLat = $("#txtLatitude").val();
            pIniLog = $("#txtLongitude").val();
        }

        var map = L.map('map').setView([pIniLat, pIniLog], 17);   
        L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 17, attribution: '' }).addTo(map);            
        L.marker([pIniLat, pIniLog]).addTo(map);         
        
        function refreshPointer() {
            setTimeout(function(){ 
                map.invalidateSize();            
            }, 5);
        }    
        
        function Print() {
            window.open("../../util/ImpressaoPdf.php?id_vistoria=" + $("#txtId").val() + "&id=" + $("#txtContrato").val() + 
            "&model=S&tpImp=I&NomeArq=Contrato&PathArq=../Modulos/Comercial/modelos/ModeloContrato.php&emp=1" + 
            ($("#txtContrato option:selected").attr("logo") === "1" ? "&logo=n" : "") + 
            ($("#txtContrato option:selected").attr("mdi") !== "0" ? "&red=" + $("#txtContrato option:selected").attr("mdi") : ""), '_blank');
        }
        
        function Assinar() {
            window.open("../../Modulos/Seguro/FormAssinatura.php?id_vistoria=" + $("#txtId").val() + 
            "&idContrato=" + $("#txtContrato").val() + "&crt=" + parent.$("#txtMnContrato").val() + 
            "&idCliente=" + parent.$("#txtId").val() + "&id=" + $("#txtId").val(), '_blank');
        }            
               
    </script>      
</body>
<?php odbc_close($con); ?>
</html>