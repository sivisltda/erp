<?php
include '../../Connections/configini.php';

$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_comissao_cliente_padrao set " .
                        "descricao = " . valoresTexto('txtdescricao') . "," .
                        "inativo = " . valoresCheck('ckbInativo') . " where id = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_comissao_cliente_padrao(descricao, inativo) values (" .
                        valoresTexto('txtdescricao') . "," .
                        valoresCheck('ckbInativo') . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id from sf_comissao_cliente_padrao order by id desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
    if (isset($_POST['txtFilial'])) {
        $inValue = "0";
        for ($i = 0; $i < count($_POST['txtFilial']); $i++) {
            if ($_POST['txtFilial'][$i] != '') {
                $inValue .= "," . $_POST['txtFilial'][$i];
            }
        }
        odbc_exec($con, "delete from sf_comissao_cliente_filiais where id_comissao = " . $_POST['txtId'] . " and id_filial not in (" . $inValue . ")") or die(odbc_errormsg());        
        for ($i = 0; $i < count($_POST['txtFilial']); $i++) {        
            odbc_exec($con, "IF NOT EXISTS (SELECT id_comissao, id_filial FROM sf_comissao_cliente_filiais WHERE id_comissao = " . $_POST['txtId'] . " AND id_filial = " . $_POST['txtFilial'][$i] . ")
            BEGIN INSERT INTO sf_comissao_cliente_filiais(id_comissao, id_filial) VALUES (" . $_POST['txtId'] . "," . $_POST['txtFilial'][$i] . "); END");
        }
    }    
    if (isset($_POST['txtContatosTotal'])) {
        $max = $_POST['txtContatosTotal'];
        $notIn = '0';
        for ($i = 0; $i <= $max; $i++) {
            if (isset($_POST['txtIdItem_' . $i]) && is_numeric($_POST['txtIdItem_' . $i])) {
                $notIn .= "," . $_POST['txtIdItem_' . $i];
            }
        }
        odbc_exec($con, "delete from sf_comissao_cliente_padrao_item where id_comissao_padrao = " . $_POST['txtId'] . " and id not in (" . $notIn . ")");
        for ($i = 0; $i <= $max; $i++) {
            if (isset($_POST['txtIdItem_' . $i])) {
                if (is_numeric($_POST['txtIdItem_' . $i])) {                    
                    odbc_exec($con, "update sf_comissao_cliente_padrao_item set " . 
                    "id_comissao_padrao = " . $_POST['txtId'] . ",id_usuario = " . valoresSelect('txtResponsavel_' . $i) . "," . 
                    "tipo = " . valoresSelect('txtTipo_' . $i) . "," . 
                    "valor = " . valoresNumericos('txtValor_' . $i) . "," . 
                    "tipo_valor = " . valoresCheck('txtTipoValor_' . $i) . "," .  
                    "tipo_valor_desc = " . valoresCheck('txtTipoValorDesc_' . $i) . 
                    " WHERE id = " . $_POST['txtIdItem_' . $i]) or die(odbc_errormsg());                    
                } else {
                    odbc_exec($con, "insert into sf_comissao_cliente_padrao_item(id_comissao_padrao,id_usuario,tipo,valor,tipo_valor,tipo_valor_desc) values(" .
                    $_POST['txtId'] . "," . valoresSelect('txtResponsavel_' . $i) . "," . 
                    valoresSelect('txtTipo_' . $i) . "," . valoresNumericos('txtValor_' . $i) . "," . 
                    valoresCheck('txtTipoValor_' . $i) . "," . valoresCheck('txtTipoValorDesc_' . $i) . ")") or die(odbc_errormsg());
                }
            }
        }
    }    
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_comissao_cliente_padrao WHERE id = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_comissao_cliente_padrao where id =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id'];
        $descricao = $RFP['descricao'];
        $inativo = $RFP['inativo'];
    }
} else {
    $disabled = '';
    $id = '';
    $descricao = '';
    $inativo = '';
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}

function getTypePessoa($tp) {
    switch ($tp) :
        case  0: return ' * Usuário Responsável';
        case  -1: return ' * Indicador Responsável';
        case  -2: return ' * Gerente Responsável';
    endswitch;
}

?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../../js/plugins/select/select22.css"/>
<script type='text/javascript' src='../../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<body>
    <form action="FormComissaoClientes.php" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Comissão de Clientes</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="tabbable frmtabs">
            <ul class="nav nav-tabs" style="margin:0">
                <li class="active"><a href="#tab1" data-toggle="tab">Principal</a></li>
                <li><a href="#tab2" data-toggle="tab">Filial</a></li>
            </ul>
            <div class="tab-content frmcont" style="height:350px;">
                <div class="tab-pane active" id="tab1">
                    <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                    <div style="width: 86%;float: left">
                        <span>Descrição:</span>
                        <input name="txtdescricao" id="txtdescricao" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="128" value="<?php echo utf8_encode($descricao); ?>"/>
                    </div>
                    <div style="width: 13%;float:left;margin-left: 1%;display: flex;flex-direction: column;justify-content: center; margin-top: 20px;">
                        <div style="display:flex;justify-content: space-between;">
                            <span>Inativo:</span>
                            <input name="ckbInativo" id="ckbInativo" type="checkbox" <?php
                            if ($inativo == "1") {
                                echo "checked";
                            }
                            ?> class="input-medium" <?php echo $disabled; ?> value="1" style="opacity: 0;"/>
                        </div>                                
                    </div>                        
                    <div style="width: 100%; float: left;">
                        <input id="txtIdItemEdit" value="" type="hidden">
                        <span>Campos:</span>
                        <span style="float: right; color: gray; margin-right: 6%;">[S-N] (Dependente de desconto)</span>
                        <div style="width:100%; float:left">                  
                            <div style="background:#F1F1F1; border:1px solid #DDD; border-bottom:0; padding:10px 10px 0">
                                <input id="txtIdItem" value="" type="hidden"/>
                                <div style="float:left; width: calc(44% - 70px);">
                                    <select id="txtResponsavel" class="select" style="width:100%" <?php echo $disabled; ?>>
                                        <option value="null">Selecione</option>
                                        <?php for ($i = 0; $i > -3; $i--) { ?>
                                        <option value="<?php echo $i; ?>"><?php echo getTypePessoa($i); ?></option>
                                        <?php } $cur = odbc_exec($con, "select id_usuario, nome from sf_usuarios where inativo = 0 and id_usuario > 1 order by nome") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['id_usuario'] ?>"><?php echo formatNameCompact(strtoupper(utf8_encode($RFP['nome']))); ?></option>
                                        <?php } ?>                                 
                                    </select>
                                </div>
                                <div style="float:left; width:32%; margin-left:1%;">
                                    <select id="txtTipo" class="select" style="width:100%" <?php echo $disabled; ?>>                                
                                        <option value="null">Selecione</option>
                                        <?php for ($i = 0; $i < 8; $i++) { ?>
                                        <option value="<?php echo $i; ?>"><?php echo getTypeComissao($i, true); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div style="float:left; margin-left:1%;">
                                    <span style="width:100%; display:block;">
                                        <div class="btn-group"><button class="btn btn-primary" id="btnTpValorDesc" <?php echo $disabled; ?> type="button" style="width:35px; padding-bottom:3px; text-align:center; background:#308698 !important">
                                        <span id="tp_sinalDesc">N</span></button></div>
                                        <input id="txtSinalDesc" value="0" type="hidden">
                                    </span>
                                </div>                                
                                <div style="float:left; width:13%;">
                                    <input type="text" id="txtValor" value="0,00" class="input-medium" <?php echo $disabled; ?>>
                                </div>
                                <div style="float:left">
                                    <span style="width:100%; display:block;">
                                        <div class="btn-group"><button class="btn btn-primary" id="btnTpValor" <?php echo $disabled; ?> type="button" style="width:35px; padding-bottom:3px; text-align:center; background:#308698 !important">
                                        <span id="tp_sinal">%</span></button></div>
                                        <input id="txtSinal" value="0" type="hidden">
                                    </span>
                                </div>                                 
                                <div style="float:left; width:8%; margin-left:1%;">
                                    <button type="button" onclick="addCampos();" class="btn dblue" <?php echo $disabled; ?> style="height: 26px;background-color: #308698 !important;">
                                        <span style="margin-top: 4px;" class="ico-plus icon-white"></span>
                                    </button>                            
                                </div>                        
                                <div style="clear:both"></div>
                            </div>
                            <div style="background:#F1F1F1; border:1px solid #DDD; border-top:0; padding:5px 10px 10px">
                                <div id="divContatos" style="height:230px; background:#FFF; border:1px solid #DDD; overflow-y:scroll;">
                                <?php
                                    $i = 0;
                                    if (is_numeric($id)) {
                                        $cur = odbc_exec($con, "select i.*, nome from sf_comissao_cliente_padrao_item i 
                                        left join sf_usuarios u on u.id_usuario = i.id_usuario
                                        where i.id_comissao_padrao = " . $id . " order by id") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { $i++; ?>
                                        <div id="tabela_linha_<?php echo $i; ?>" style="padding:2px; border-bottom:1px solid #DDD; overflow: hidden;">
                                            <input id="txtIdItem_<?php echo $i; ?>" name="txtIdItem_<?php echo $i; ?>" value="<?php echo utf8_encode($RFP["id"]); ?>" type="hidden">
                                            <input id="txtResponsavel_<?php echo $i; ?>" name="txtResponsavel_<?php echo $i; ?>" value="<?php echo utf8_encode($RFP["id_usuario"]); ?>" type="hidden">
                                            <input id="txtTipo_<?php echo $i; ?>" name="txtTipo_<?php echo $i; ?>" value="<?php echo utf8_encode($RFP["tipo"]); ?>" type="hidden">
                                            <input id="txtValor_<?php echo $i; ?>" name="txtValor_<?php echo $i; ?>" value="<?php echo escreverNumero($RFP["valor"]); ?>" type="hidden">
                                            <input id="txtTipoValor_<?php echo $i; ?>" name="txtTipoValor_<?php echo $i; ?>" value="<?php echo $RFP["tipo_valor"]; ?>" type="hidden">
                                            <input id="txtTipoValorDesc_<?php echo $i; ?>" name="txtTipoValorDesc_<?php echo $i; ?>" value="<?php echo $RFP["tipo_valor_desc"]; ?>" type="hidden">
                                            <div id="tabela_desc_<?php echo $i; ?>" style="line-height:27px; float:left">[<?php echo ($RFP["id_usuario"] > 0 ? formatNameCompact(strtoupper(utf8_encode($RFP['nome']))) : getTypePessoa($RFP["id_usuario"])); ?>] [<?php echo getTypeComissao($RFP["tipo"], true); ?>] [<?php echo $RFP["tipo_valor_desc"] == "1" ? "S" : "N"; ?>] <a href="javascript:void(0)" onclick="editCampos(<?php echo $i; ?>)"><?php echo escreverNumero($RFP["valor"]); ?><?php echo $RFP["tipo_valor"] == "1" ? "$" : "%"; ?></a></div>
                                            <div style="width: 5%; padding: 4px 0px 3px; float: right; cursor: pointer;" onclick="return removeLinha('<?php echo $i; ?>');">
                                                <span class="ico-remove" style="font-size:20px"></span>
                                            </div>
                                        </div>
                                    <?php }} ?>                            
                                    <input id="txtContatosTotal" name="txtContatosTotal" type="hidden" value="<?php echo $i; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                </div>
                <div class="tab-pane" id="tab2">
                    <div style="width:100%; float: left;">
                        <span>Filial:</span>
                        <select id="txtFilial" name="txtFilial[]" multiple="multiple" class="select" style="width:100%;" class="input-medium" <?php echo $disabled; ?>>
                            <?php $cur = odbc_exec($con, "select t.id_filial,t.Descricao,ti.id_comissao from sf_filiais t 
                            left join sf_comissao_cliente_filiais ti on t.id_filial = ti.id_filial 
                            and ti.id_comissao = " . valoresSelect2($id) . " order by t.Descricao") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP['id_filial'] ?>"<?php
                                if ($RFP["id_comissao"] != null) {
                                    echo "SELECTED";
                                }
                                ?>><?php echo utf8_encode($RFP['Descricao']) ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" name="bntSave" id="bntSave" ><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntCancelar"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntAlterar" ><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else {
                    ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntEdit" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>    
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/multiselect/jquery.multi-select.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery.filter_input.js'></script>
    <script type='text/javascript' src='../../js/plugins/ckeditor/ckeditor.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script> 
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>    
    <script type="text/javascript" src="../../js/util.js"></script>
    <script type="text/javascript">
        
        $("#txtValor").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
        
        function addCampos() {
            if (validar()) {
                if($.isNumeric($("#txtIdItemEdit").val())) {
                    let id = $("#txtIdItemEdit").val();                    
                    $("#txtIdItem_" + id).val($("#txtIdItem").val());
                    $("#txtResponsavel_" + id).val($("#txtResponsavel").val());
                    $("#txtTipo_" + id).val($("#txtTipo").val());
                    $("#txtValor_" + id).val($("#txtValor").val());
                    $("#txtTipoValor_" + id).val($("#txtSinal").val());
                    $("#txtTipoValorDesc_" + id).val($("#txtSinalDesc").val());
                    $("#tabela_desc_" + id).html("[" + $("#txtResponsavel option:selected").html() + "] [" + $("#txtTipo option:selected").html() + "] " + 
                    "[" +($("#txtTipoValorDesc_" + id).val() === "0" ? "N" : "S") + "] " + 
                    "<a href=\"javascript:void(0)\" onclick=\"editCampos(" + id + ")\">" + $("#txtValor").val() + ($("#txtTipoValor_" + id).val() === "0" ? "%" : "$") + "</a>");
                } else {
                    let id = parseInt($("#txtContatosTotal").val()) + 1;                    
                    let linha = "<div id=\"tabela_linha_" + id + "\" style=\"padding:2px; border-bottom:1px solid #DDD; overflow: hidden;\">" +                        
                    "<input id=\"txtIdItem_" + id + "\" name=\"txtIdItem_" + id + "\" value=\"" + $("#txtIdItem").val() + "\" type=\"hidden\">" +
                    "<input id=\"txtResponsavel_" + id + "\" name=\"txtResponsavel_" + id + "\" value=\"" + $("#txtResponsavel").val() + "\" type=\"hidden\">" +                    
                    "<input id=\"txtTipo_" + id + "\" name=\"txtTipo_" + id + "\" value=\"" + $("#txtTipo").val() + "\" type=\"hidden\">" +
                    "<input id=\"txtValor_" + id + "\" name=\"txtValor_" + id + "\" value=\"" + $("#txtValor").val() + "\" type=\"hidden\">" +
                    "<input id=\"txtTipoValor_" + id + "\" name=\"txtTipoValor_" + id + "\" value=\"" + $("#txtSinal").val() + "\" type=\"hidden\">" +
                    "<input id=\"txtTipoValorDesc_" + id + "\" name=\"txtTipoValorDesc_" + id + "\" value=\"" + $("#txtSinalDesc").val() + "\" type=\"hidden\">" +
                    "<div id=\"tabela_desc_" + id + "\" style=\"line-height:27px; float:left\">" + "[" + $("#txtResponsavel option:selected").html() + "] [" + $("#txtTipo option:selected").html() + "] " + 
                    "[" +($("#txtSinalDesc").val() === "0" ? "N" : "S") + "] " + 
                    "<a href=\"javascript:void(0)\" onclick=\"editCampos(" + id + ")\">" + $("#txtValor").val() + ($("#txtSinal").val() === "0" ? "%" : "$") + "</a></div>" +                        
                    "<div style=\"width: 5%; padding: 4px 0px 3px; float: right; cursor: pointer;\" onclick=\"return removeLinha('" + id + "');\"><span class=\"ico-remove\" style=\"font-size:20px\"></span></div></div>";
                    $("#divContatos").append(linha);                    
                    $("#txtContatosTotal").val(id);
                }
                $("#txtIdItemEdit").val("");                
            }
        }
        
        $("#btnTpValor").click(function () {
            if ($("#tp_sinal").html() === "%") {
                $("#tp_sinal").html("$");
                $("#txtSinal").val("1");
            } else {
                $("#tp_sinal").html("%");
                $("#txtSinal").val("0");
            }
        });        
        
        $("#btnTpValorDesc").click(function () {
            if ($("#tp_sinalDesc").html() === "N") {
                $("#tp_sinalDesc").html("S");
                $("#txtSinalDesc").val("1");
            } else {
                $("#tp_sinalDesc").html("N");
                $("#txtSinalDesc").val("0");
            }
        });        
        
        function editCampos(id) {
            $("#txtIdItemEdit").val(id);            
            $("#txtIdItem").val($("#txtIdItem_" + id).val());
            $("#txtResponsavel").select2("val", $("#txtResponsavel_" + id).val());
            $("#txtTipo").select2("val", $("#txtTipo_" + id).val());
            $("#txtValor").val($("#txtValor_" + id).val());
            $("#txtSinal").val($("#txtTipoValor_" + id).val());
            $("#txtSinalDesc").val($("#txtTipoValorDesc_" + id).val());
            $("#tp_sinal").html(($("#txtTipoValor_" + id).val() === "0" ? "%" : "$"));
            $("#tp_sinalDesc").html(($("#txtTipoValorDesc_" + id).val() === "0" ? "N" : "S"));
        }

        function validar() {
            if ($('#txtResponsavel').val() === "null") {
                bootbox.alert("Preencha o campo Responsável!");
                return false;           
            }
            if ($('#txtTipo').val() === "null") {
                bootbox.alert("Preencha o campo Tipo!");
                return false;           
            }
            if ($('#txtValor').val() === "0,00") {
                bootbox.alert("Preencha o campo Valor!");
                return false;           
            }
            return true;
        }

        function removeLinha(id) {
            $("#tabela_linha_" + id).remove();
        }
            
    </script>
    <?php odbc_close($con); ?>
</body>
