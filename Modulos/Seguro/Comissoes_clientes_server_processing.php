<?php

if (isset($_GET["functionPage"]) && isset($_GET["loja"]) && is_numeric($_GET["loja"]) && in_array($_SERVER['REMOTE_ADDR'], ["148.72.177.101"])) {
    require_once(__DIR__ . '/../../Connections/funcoesAux.php');
    $hostname = getDataBaseServer(1);
    $database = "erp" . str_pad($_GET["loja"], 3, "0", STR_PAD_LEFT);
    $username = "erp" . str_pad($_GET["loja"], 3, "0", STR_PAD_LEFT);
    $password = "!Password123";
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());    
    $id_usuario = $_GET["id_usuario"];
} else {
    require_once(__DIR__ . '/../../Connections/configini.php');
    $id_usuario = $_SESSION["id_usuario"];
}

$cont = 0;
$imprimir = 0;
$sWhere = "";
$sWhere2 = "";
$id_usuario = "0";
$login = "";
$id_funcionario = "0";
$comissao_plano_pri = 0.00;
$comissao_plano = 0.00;
$comissao_servico = 0.00;
$comissao_produto = 0.00;
$grupoArray = [];
$tipos = [0, 1, 2, 3];

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (is_numeric($_GET['txtUsuario'])) {
    $cur = odbc_exec($con, "select id_usuario, login_user, isnull(funcionario,0) funcionario from sf_usuarios where id_usuario = " . $_GET['txtUsuario']);
    while ($RFP = odbc_fetch_array($cur)) {
        $id_usuario = $RFP['id_usuario'];
        $login = utf8_encode($RFP['login_user']);
        $id_funcionario = $RFP['funcionario'];
    }
    $comissao_plano_pri = "((vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio)) * isnull((select max(valor) from sf_comissao_cliente where id_cliente = c.id_fornecedores_despesas and tipo = 0 and id_usuario = " . $id_usuario . "),0)/100) / case when pp.parcela < 1 then 1 else pp.parcela end";
    $comissao_plano = "((vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio)) * isnull((select max(valor) from sf_comissao_cliente where id_cliente = c.id_fornecedores_despesas and tipo = 1 and id_usuario = " . $id_usuario . "),0)/100) / case when pp.parcela < 1 then 1 else pp.parcela end";
    $comissao_servico = "((vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio)) * isnull((select max(valor) from sf_comissao_cliente where id_cliente = c.id_fornecedores_despesas and tipo = 2 and id_usuario = " . $id_usuario . "),0)/100)";
    $comissao_produto = "((vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio)) * isnull((select max(valor) from sf_comissao_cliente where id_cliente = c.id_fornecedores_despesas and tipo = 3 and id_usuario = " . $id_usuario . "),0)/100)";
    $sWhere .= " and c.id_fornecedores_despesas in (select id_cliente from sf_comissao_cliente where id_usuario = " . $id_usuario . ")";
}

if (isset($_GET['filial']) && $_GET['filial'] != "null") {
    $sWhere .= " and c.empresa in (" . str_replace("\"", "'", str_replace(array("[", "]"), "", $_GET["filial"])) . ")";
}

if (isset($_GET['filtro']) && $_GET['filtro'] != "null") {
    $tipos = explode(",", $_GET['filtro']);
}

if (is_numeric($_GET['txtForma'])) {
    if ($_GET['txtForma'] == "1") {
        $sWhere2 .= " and sa.status = 'Aprovado' ";
    } elseif ($_GET['txtForma'] == "2") {
        $sWhere2 .= " and sa.status is null ";
    } elseif ($_GET['txtForma'] == "3") {
        $sWhere2 .= " and sa.status = 'Aguarda' ";
    } elseif ($_GET['txtForma'] == "4") {
        $sWhere2 .= " and sa.status = 'Reprovado' ";
    }
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $sWhere2 .= " and data_venda between " . valoresDataHoraUnico2(str_replace("_", "/", $_GET['dti']) . " 00:00:00") . " and " . valoresDataHoraUnico2(str_replace("_", "/", $_GET['dtf']) . " 23:59:59");
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => 0,
    "iTotalDisplayRecords" => 0,
    "aaData" => array()
);

$sQuery = "set dateformat dmy; ";
$sQuery .= "select v.id_venda, v.data_venda, c.razao_social, c.empresa,  
case when p.tipo = 'C' then vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) else 0 end preco_plano,
0 preco_servico, 0 preco_produto, 0 comissao_plano_pri, 
case when p.tipo = 'C' then ((vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio))
* isnull((select max(valor) from sf_comissao_cliente where id_cliente = c.id_fornecedores_despesas and tipo = 1 and id_usuario = " . $id_usuario . "),0)/100) / case when pp.parcela < 1 then 1 else pp.parcela end end
comissao_plano, 0 comissao_servico,0 comissao_produto,   
v.vendedor, v.indicador, c.id_user_resp, c.prof_resp, vi.vendedor_comissao comissionado, fi.descricao gc, 0 prov,  pp.parcela
into #tempProdutividade from sf_vendas v 
inner join sf_vendas_itens vi on vi.id_venda = v.id_venda
inner join sf_produtos p on p.conta_produto = vi.produto
left join sf_vendas_planos_mensalidade m on m.id_item_venda_mens = vi.id_item_venda
left join sf_vendas_planos vp on vp.id_plano = m.id_plano_mens
left join sf_produtos_parcelas pp on pp.id_parcela = m.id_parc_prod_mens
inner join sf_fornecedores_despesas c on c.id_fornecedores_despesas = v.cliente_venda
left join sf_filiais fi on fi.id_filial = c.empresa
where pp.parcela > 1 and v.cov = 'V' and v.status = 'Aprovado' and p.tipo in ('C') and dt_cancelamento is null " . $sWhere . "
and (" . valoresDataHoraUnico2(str_replace("_", "/", $_GET['dti']) . " 00:00:00") . " <= DATEADD(month,pp.parcela,data_venda)) and (data_venda <= " . valoresDataHoraUnico2(str_replace("_", "/", $_GET['dtf']) . " 23:59:59") . ") ";
//echo $sQuery; exit;
odbc_exec($con, $sQuery) or die(odbc_errormsg());

$queryX = "set dateformat dmy;insert into #tempProdutividade ";
$cur = odbc_exec($con, "select * from #tempProdutividade");
while ($RFP = odbc_fetch_array($cur)) {
    for ($i = 1; $i < $RFP['parcela']; $i++) {
        $queryX .= " select id_venda, dateadd(month," . $i . ",data_venda) data_venda, razao_social,empresa,preco_plano,
        preco_servico,preco_produto,comissao_plano_pri,comissao_plano,comissao_servico,comissao_produto,
        vendedor,indicador,id_user_resp,prof_resp,comissionado,gc, " . $i . ", parcela
        from #tempProdutividade where id_venda = " . $RFP['id_venda'] . " and cast(data_venda as date) = " . valoresData2(escreverData($RFP['data_venda'])) . " union all";
    }
}
//echo substr_replace($queryX, "", -9); exit;
if (strlen($queryX) > 50) {
    odbc_exec($con, substr_replace($queryX, "", -9));
}
//-------------------------------------------------------------------------------------------------------------------------
$sQuery = "set dateformat dmy; ";
$sQuery .= "select id_venda, data_venda, vendedor, razao_social, empresa, sum(preco_plano) preco_plano, sum(preco_servico) preco_servico, 
sum(preco_produto) preco_produto, sum(comissao_plano_pri) comissao_plano_pri, sum(comissao_plano) comissao_plano, 
sum(comissao_servico) comissao_servico, sum(comissao_produto) comissao_produto, id_solicitacao_autorizacao, status, gc, prov, parcela from (";
$sQuery .= "select v.id_venda, v.data_venda, c.razao_social, c.empresa,  
case when p.tipo = 'C' then vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) else 0 end preco_plano,
case when p.tipo = 'S' then vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) else 0 end preco_servico, 
case when p.tipo = 'P' then vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) else 0 end preco_produto,
case when p.tipo = 'C' and dbo.FU_PRIMEIRO_PAGAMENTO(vi.id_item_venda) > 0 then $comissao_plano_pri end comissao_plano_pri, 
case when p.tipo = 'C' and dbo.FU_PRIMEIRO_PAGAMENTO(vi.id_item_venda) = 0 then $comissao_plano end comissao_plano,   
case when p.tipo = 'S' and p.valor_comissao_venda > 0 then $comissao_servico else 0 end comissao_servico,
case when p.tipo = 'P' and p.valor_comissao_venda > 0 then $comissao_produto else 0 end comissao_produto,   
v.vendedor, v.indicador, c.id_user_resp, c.prof_resp, vi.vendedor_comissao comissionado, fi.descricao gc, 
0 prov, 1 parcela, sa.id_solicitacao_autorizacao, sa.status
from sf_vendas v inner join sf_vendas_itens vi on vi.id_venda = v.id_venda
inner join sf_produtos p on p.conta_produto = vi.produto
left join sf_vendas_planos_mensalidade m on m.id_item_venda_mens = vi.id_item_venda
left join sf_vendas_planos vp on vp.id_plano = m.id_plano_mens
left join sf_produtos_parcelas pp on pp.id_parcela = m.id_parc_prod_mens
inner join sf_fornecedores_despesas c on c.id_fornecedores_despesas = v.cliente_venda
left join sf_filiais fi on fi.id_filial = c.empresa
left join sf_comissao_historico ch on v.id_venda = ch.hist_venda and hist_parcela = 0 and hist_vendedor = " . $id_usuario . "
left join sf_solicitacao_autorizacao sa on sa.id_solicitacao_autorizacao = ch.hist_solicitacao_autorizacao and sa.fornecedor_despesa = $id_funcionario
where v.cov = 'V' and v.status = 'Aprovado' and p.tipo in ('C', 'P', 'S') and dt_cancelamento is null " . $sWhere . $sWhere2;

$sQuery .= " union all select distinct #tempProdutividade.*, sa.id_solicitacao_autorizacao, sa.status from #tempProdutividade 
left join sf_comissao_historico ch on id_venda = ch.hist_venda and hist_parcela = prov and hist_vendedor = " . $id_usuario . "
left join sf_solicitacao_autorizacao sa on sa.id_solicitacao_autorizacao = ch.hist_solicitacao_autorizacao and sa.fornecedor_despesa = $id_funcionario    
where prov > 0 " . $sWhere2;

$sQuery .= ") as x group by id_venda, data_venda, vendedor, razao_social, empresa, id_solicitacao_autorizacao, status, gc, prov, parcela order by data_venda";
//echo $sQuery; exit;
$cur = odbc_exec($con, $sQuery);
while ($aRow = odbc_fetch_array($cur)) {
    if (isset($_GET["functionPage"])) {
        if (($aRow["comissao_plano_pri"] > 0 && in_array("0", $tipos)) || ($aRow["comissao_plano"] > 0 && in_array("1", $tipos)) || ($aRow["comissao_servico"] > 0 && in_array("2", $tipos)) || ($aRow["comissao_produto"] > 0 && in_array("3", $tipos))) {
            $valor = ($aRow["comissao_plano_pri"] + $aRow["comissao_plano"] + $aRow["comissao_servico"] + $aRow["comissao_produto"]);            
            $row["data_venda"] = escreverDataHora($aRow["data_venda"]);
            $row["id_venda"] = $aRow["id_venda"];
            $row["nome"] = utf8_encode($aRow['razao_social']);
            $row["parcela"] = ($aRow["prov"] + 1) . "/" . $aRow["parcela"];        
            $row["preco_plano"] = escreverNumero($aRow["preco_plano"], 1);
            $row["preco_servico"] = escreverNumero($aRow["preco_servico"], 1);
            $row["preco_produto"] = escreverNumero($aRow["preco_produto"], 1);
            $row["comissao_plano_pri"] = escreverNumero($aRow["comissao_plano_pri"], 1);
            $row["comissao_plano"] = escreverNumero($aRow["comissao_plano"], 1);        
            $row["comissao_servico"] = escreverNumero($aRow["comissao_servico"], 1);
            $row["comissao_produto"] = escreverNumero($aRow["comissao_produto"], 1);
            $row["valor_comissao"] = escreverNumero($valor, 1);        
            $row["produto"] = "";
            $row["status"] = (strlen($aRow['status']) > 0 ? $aRow['status'] : "Pendente");
            $output['aaData'][] = $row;
            $cont++;
        }
    } else {      
        if (($aRow["comissao_plano_pri"] > 0 && in_array("0", $tipos)) || ($aRow["comissao_plano"] > 0 && in_array("1", $tipos)) || ($aRow["comissao_servico"] > 0 && in_array("2", $tipos)) || ($aRow["comissao_produto"] > 0 && in_array("3", $tipos))) {
            $valor = ($aRow["comissao_plano_pri"] + $aRow["comissao_plano"] + $aRow["comissao_servico"] + $aRow["comissao_produto"]);
            $row = array();
            $row[] = "<input type=\"checkbox\" name=\"items[]\" val=\"" . escreverNumero($valor) . "\" value=\"" . $aRow["id_venda"] . "-" . $aRow["prov"] . "\" " . ($valor > 0 && !is_numeric($aRow['id_solicitacao_autorizacao']) ? "" : "disabled") . ">";
            $row[] = $aRow["id_venda"] . " (" . ($aRow["prov"] + 1) . "/" . $aRow["parcela"] . ")";
            $row[] = escreverDataHora($aRow["data_venda"]);
            $row[] = utf8_encode($aRow["vendedor"]);
            $row[] = utf8_encode($aRow["razao_social"]);
            $row[] = escreverNumero(($aRow["preco_plano"] / $aRow["parcela"]), 1);
            $row[] = escreverNumero($aRow["preco_servico"], 1);
            $row[] = escreverNumero($aRow["preco_produto"], 1);
            $row[] = escreverNumero($aRow["comissao_plano_pri"], 1);
            $row[] = escreverNumero($aRow["comissao_plano"], 1);
            $row[] = escreverNumero($aRow["comissao_servico"], 1);
            $row[] = escreverNumero($aRow["comissao_produto"], 1);
            $row[] = escreverNumero($valor, 1);
            if (is_numeric($aRow['id_solicitacao_autorizacao'])) {
                $Solicitacao = "<a title=\"" . $aRow['status'] . "\" href=\"./../Contas-a-pagar/FormSolicitacao-de-Autorizacao.php?id=" . $aRow['id_solicitacao_autorizacao'] . "\">";
                $Solicitacao .= "<img src=\"../../img/";
                if ($aRow['status'] == 'Reprovado') {
                    $Solicitacao .= "inativo";
                } else if ($aRow['status'] == 'Aprovado') {
                    $Solicitacao .= "pago";
                } else if ($aRow['status'] == 'Aguarda') {
                    $Solicitacao .= "apagar";
                } else {
                    $Solicitacao .= "back";
                }
                $Solicitacao .= ".PNG\" value=\"\"></a>";
                $row[] = $Solicitacao;
            } else {
                $row[] = "<img src=\"../../img/dollar" . ($valor > 0 ? "" : "2") . ".png\" value=\"\">";
            }
            if (isset($_GET['pdf'])) {
                $row[] = $aRow['gc'];
                $grp = array(utf8_encode($aRow['gc']), ($aRow['gc'] == "" ? "SEM GRUPO" : utf8_encode($aRow['gc'])), 12);
                if (!in_array($grp, $grupoArray)) {
                    $grupoArray[] = $grp;
                }
            }
            $output['aaData'][] = $row;
            $cont++;
        }
    }
}
$output["iTotalRecords"] = $cont;
$output["iTotalDisplayRecords"] = $cont;
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
} else {
    $output['aaTotal'][] = array(4, 4, "Sub Total", "Total", 0, 0);
    $output['aaTotal'][] = array(5, 5, "", "", 0, 0);
    $output['aaTotal'][] = array(6, 6, "", "", 0, 0);
    $output['aaTotal'][] = array(7, 7, "", "", 0, 0);
    $output['aaTotal'][] = array(8, 8, "", "", 0, 0);
    $output['aaTotal'][] = array(9, 9, "", "", 0, 0);
    $output['aaTotal'][] = array(10, 10, "", "", 0, 0);
    $output['aaTotal'][] = array(11, 11, "", "", 0, 0);
    $output['aaGrupo'] = $grupoArray;
}
odbc_close($con);
