<?php

include ('../../Connections/configini.php');
$local = array();

if (isset($_GET['filial']) && $_GET['filial'] != "null") {
    $sWhere .= " AND u.id_usuario in (select id_usuario_f from sf_usuarios_filiais where id_filial_f in (" . str_replace("\"", "'", str_replace(array("[", "]"), "", $_GET["filial"])) . "))";
} else {
    $sWhere .= " AND u.id_usuario in (select uf1.id_usuario_f from sf_usuarios_filiais uf1 where uf1.id_filial_f in (select uf2.id_filial_f from sf_usuarios_filiais uf2 where uf2.id_usuario_f = '" . $_SESSION["id_usuario"] . "'))";
}

if (is_numeric($_GET["txtTipo"])) {
    if ($_GET["txtTipo"] == "0" || $_GET["txtTipo"] == "2") { //Vendedor (venda) | Usuário Responsável (Cliente)
        $query = "SELECT id_usuario, razao_social FROM sf_usuarios u 
        INNER JOIN sf_fornecedores_despesas f ON u.funcionario = f.id_fornecedores_despesas  
        WHERE f.recebe_comissao = 1 and f.inativo = 0 AND dt_demissao IS NULL AND f.tipo = 'E' " . 
        $sWhere . " order by razao_social";
    } else if ($_GET["txtTipo"] == "1") { //Indicador (venda)
        $query = "select id_usuario, razao_social from sf_usuarios u 
        inner join sf_fornecedores_despesas f on u.funcionario = f.id_fornecedores_despesas  
        where f.recebe_comissao = 1 and f.inativo = 0 and dt_demissao is null and f.tipo = 'I' " . 
        $sWhere . " order by razao_social";
    } else if ($_GET["txtTipo"] == "3") { //Funcionário Responsável (Cliente)
        $query = "select id_fornecedores_despesas id_usuario,razao_social from sf_fornecedores_despesas f
        where f.recebe_comissao = 1 and f.inativo = 0 and dt_demissao is null and f.tipo = 'E' order by razao_social";
    } else if ($_GET["txtTipo"] == "4") { //Gerência de Usuário Responsável (Cliente)
        $query = "select id_usuario, razao_social from sf_usuarios u 
        inner join sf_fornecedores_despesas f on u.funcionario = f.id_fornecedores_despesas  
        where f.recebe_comissao = 1 and f.inativo = 0 and dt_demissao is null and f.tipo = 'E' and (tbcomiss_gere > 0 or comiss_gerePri > 0) " . 
        $sWhere . " order by razao_social";
    } else if (in_array($_GET["txtTipo"], ["5", "6", "7", "8"])) { //Usuário Responsável (Cliente) | Modo Seguro
        $query = "select distinct id_usuario, razao_social from sf_usuarios u
        inner join sf_fornecedores_despesas f on u.funcionario = f.id_fornecedores_despesas
        where f.tipo='" . str_replace(["5", "6", "7", "8"], ["E", "I", "P", "C"], $_GET["txtTipo"]) . "' and u.inativo = 0         
        and id_usuario not in (select uln.id_usuario from sf_usuarios u inner join sf_usuarios_lista_negra uln on u.funcionario = uln.id_fornecedor_despesas where u.id_usuario = '" . $_SESSION["id_usuario"] . "') 
        and id_usuario > 1 " . ($comissao_forma_ > 0 ? " and id_usuario in (select id_usuario from sf_comissao_cliente)" : "") . 
        $sWhere . " order by razao_social";
    }    
    $cur = odbc_exec($con, $query) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('login_user' => $RFP['id_usuario'], 'nome' => utf8_encode($RFP['razao_social']));
    }
}
echo(json_encode($local));
odbc_close($con);