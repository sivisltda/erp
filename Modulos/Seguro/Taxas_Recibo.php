<?php

require_once(__DIR__ . '/../../Connections/configini.php');
require_once(__DIR__ . '/../../util/impressora/examples/tcpdf_include.php');
require_once(__DIR__ . '/../../util/impressora/tcpdf.php');
require_once(__DIR__ . '/../../util/impressora/examples/fpdi2/src/autoload.php');

if (isset($_GET['data']) && $_GET['data'] != "") {
    $WhereL = "data_pagamento between " . valoresDataHora2(str_replace("_", "/", $_GET['data']), "00:00:00") . " and dateadd(day,-1,dateadd(month,1," . valoresDataHora2(str_replace("_", "/", $_GET['data']), "23:59:59") . "))";
    $WhereS = "data_pagamento between " . valoresDataHora2(str_replace("_", "/", $_GET['data']), "00:00:00") . " and dateadd(day,-1,dateadd(month,1," . valoresDataHora2(str_replace("_", "/", $_GET['data']), "23:59:59") . "))";
    $WhereV = "vp.data_pagamento between " . valoresDataHora2(str_replace("_", "/", $_GET['data']), "00:00:00") . " and dateadd(day,-1,dateadd(month,1," . valoresDataHora2(str_replace("_", "/", $_GET['data']), "23:59:59") . "))";
} else if (isset($_GET['itens']) && $_GET['itens'] != "") {
    $count = explode(",", $_GET['itens']);
    if (count($count) > 0) {
        for ($i = 0; $i < count($count); $i++) {
            $item = explode("-", $count[$i]);         
            if (count($item) == 2) {                
                if ($item[0] == 'D') {
                    $WhereL = $WhereL . $item[1] . ',';
                } else if ($item[0] == 'A') {
                    $WhereS = $WhereS . $item[1] . ',';
                } else if ($item[0] == 'V') {
                    $WhereV = $WhereV . $item[1] . ',';
                }
            }
        }
    }
    $WhereL = "sf_lancamento_movimento_parcelas.id_parcela in (" . $WhereL . "0)";
    $WhereS = "sf_solicitacao_autorizacao_parcelas.id_parcela in (" . $WhereS . "0)";
    $WhereV = "vp.id_parcela in (" . $WhereV . "0)";
}

$pdf = new \setasign\Fpdi\TcpdfFpdi(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('ERP');
$pdf->SetFont('helvetica', 'B', 10);
//------------------------------------------------------------------------------------------------------------------------------------------
$query_temp = "set dateformat dmy;";
$query_temp .= "select 'V-' +cast(vp.id_parcela as VARCHAR) as pk, c.id_contas_movimento, sf_grupo_contas.descricao grupo, c.descricao, v.historico_venda historico, data_pagamento, razao_social, sf_produtos.descricao produto,
valor_pago valor_item, valor_total, (select isnull(sum(valor_total),0) from sf_vendas_itens where id_venda = venda) total_itens, data_parcela 'Vecto'
into #tempTable from sf_vendas v 
inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = v.cliente_venda
inner join sf_contas_movimento c on v.conta_movimento = c.id_contas_movimento
inner join sf_grupo_contas on id_grupo_contas = c.grupo_conta    
inner join sf_vendas_itens vi on v.id_venda = vi.id_venda
inner join sf_produtos on conta_produto = produto
inner join sf_venda_parcelas vp on venda = v.id_venda
where " . $WhereV . " and cov = 'C' and status = 'Aprovado' and vp.inativo in (0,2) and vp.somar_rel = 1";   
//echo $query_temp; exit;     
odbc_exec($con, $query_temp);    
$query = "set dateformat dmy;";
$query .= "select pk, id_contas_movimento, grupo, descricao, historico, dbo.CALC_PROPORCAO(valor_item,valor_total, total_itens) valor_pago, 
data_pagamento, razao_social collate SQL_Latin1_General_CP1_CI_AS + ' - ' + produto collate SQL_Latin1_General_CP1_CI_AS razao_social, Vecto from #tempTable
union all    
select 'D-' +cast(sf_lancamento_movimento_parcelas.id_parcela as VARCHAR) as pk, sf_contas_movimento.id_contas_movimento, sf_grupo_contas.descricao grupo, sf_contas_movimento.descricao, historico, 
sf_lancamento_movimento_parcelas.valor_pago, data_pagamento, razao_social, data_vencimento 'Vecto'
from sf_lancamento_movimento_parcelas 
inner join sf_lancamento_movimento on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on id_grupo_contas = sf_contas_movimento.grupo_conta    
inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas
where " . $WhereL . " and sf_lancamento_movimento.status = 'Aprovado' and data_pagamento is not null AND sf_lancamento_movimento_parcelas.inativo = '0'
and sf_contas_movimento.tipo = 'D' and sf_lancamento_movimento_parcelas.tipo_documento not in (12)
union all
select 'A-' +cast(sf_solicitacao_autorizacao_parcelas.id_parcela as VARCHAR) as pk, sf_contas_movimento.id_contas_movimento, sf_grupo_contas.descricao grupo, sf_contas_movimento.descricao, historico, 
sf_solicitacao_autorizacao_parcelas.valor_pago, data_pagamento, razao_social, data_parcela 'Vecto'
from sf_solicitacao_autorizacao_parcelas 
inner join sf_solicitacao_autorizacao on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on id_grupo_contas = sf_contas_movimento.grupo_conta    
inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas 
where " . $WhereS . " and sf_solicitacao_autorizacao.status = 'Aprovado' and data_pagamento is not null AND sf_solicitacao_autorizacao_parcelas.inativo = '0' 
and sf_contas_movimento.tipo = 'D' and sf_solicitacao_autorizacao_parcelas.tipo_documento not in (12) ORDER BY Vecto desc, razao_social";
//echo $query; exit;    
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $texto = escreverData($RFP["data_pagamento"]) . " - " . utf8_encode($RFP["razao_social"]); /*. " - " . escreverNumero($RFP["valor_pago"], 1);*/
    $pasta = str_replace("-", "", $RFP['pk']);
    if ($handle = opendir("./../../Pessoas/" . $_SESSION["contrato"] . "/A_Pagar/" . $pasta)) {
        while (false !== ($entry = readdir($handle))) {            
            $extension = explode(".", $entry);
            $extension = $extension[count($extension) - 1];              
            if ($entry != "." && $entry != ".." && in_array($extension, array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG"))) {
                addImg($pdf, $texto, $pasta, $entry);
            } else if ($entry != "." && $entry != ".." && in_array($extension, array("pdf"))) {
                addPdf($pdf, $texto, $pasta, $entry);
            } else if ($entry != "." && $entry != "..") {
                addSem($pdf, $texto);                 
            }
        }
        closedir($handle);
    }
}

function addImg($pdf, $texto, $pasta, $id) {
    $pdf->AddPage();
    $pdf->Cell(0, 0, $texto, 1, 1, 'C');
    $pdf->Image('./../../Pessoas/' . $_SESSION["contrato"] . '/A_Pagar/' . $pasta . '/' . $id, 11, 20, 120, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);    
}

function addPdf($pdf, $texto, $pasta, $id) {
    try {
        $pages = $pdf->setSourceFile('./../../Pessoas/' . $_SESSION["contrato"] . '/A_Pagar/' . $pasta . '/' . $id);
        for ($i = 0; $i < 1; $i++) {
            $pdf->AddPage();
            $pdf->Cell(0, 0, $texto, 1, 1, 'C');
            $tplIdx = $pdf->importPage($i + 1);
            $pdf->useTemplate($tplIdx, 11, 20, 150);
        }
    } catch (Exception $e) {

    }
}

function addSem($pdf, $texto) {
    $pdf->AddPage();
    $pdf->Cell(0, 0, $texto, 1, 1, 'C');  
    $pdf->Ln(5);
    $pdf->SetFillColor(189, 181, 176);
    $pdf->MultiCell(0, 0, 'Sem documento em anexo!', 1, 'C', 1, 1, '', '', true, 0, false, true, 0);
}

$pdf->Output('imprimir.pdf', 'I');