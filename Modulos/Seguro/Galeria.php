<?php
include '../../Connections/configini.php';
if (isset($_GET["id"]) && isset($_GET["img"]) && isset($_GET["loja"])) {
    $vistoria = $_GET["id"];
    $img = $_GET["img"];
    $empresa = $_GET["loja"];
    
    $legenda = [];
    $cur = odbc_exec($con, "select descricao from sf_vistorias v
    inner join sf_vistorias_tipo_item vi on id_vistoria_tipo = v.tipo
    where v.id = " . $vistoria . " order by etapa, vi.id ;");
    while ($RFP = odbc_fetch_array($cur)) {
        $legenda[] = utf8_encode($RFP['descricao']);
    }
} else {
    exit;
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Galeria</title>
        <link href="../../css/galeria.css" rel="stylesheet"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <script type='text/javascript' src='../../js/jquery.easing.1.3.js'></script>
    </head>
    <body>
        <div id="bg">
            <a href="#" class="nextImageBtn" title="next"></a>
            <a href="#" class="prevImageBtn" title="previous"></a>
            <img src="./../../Pessoas/<?php echo $empresa; ?>/Vistorias/<?php echo $vistoria; ?>/<?php echo $img; ?>" title="<?php echo $legenda[0]; ?> - <?php echo date("d/m/Y H:i:s", filemtime("./../../Pessoas/" . $empresa . "/Vistorias/" . $vistoria . "/" . $img)); ?>" alt="<?php echo $img; ?>" id="bgimg"/>
        </div>
        <div id="preloader"><img src="../../img/easing/ajax-loader_dark.gif" width="32" height="32" /></div>
        <div id="img_title"></div>
        <div id="toolbar">
            <a href="#" title="Maximize" onClick="ImageViewMode('full');return false">
                <img src="../../img/easing/toolbar_fs_icon.png" width="50" height="50"  />
            </a>
        </div>
        <div id="thumbnails_wrapper">
            <div id="outer_container">
                <div class="thumbScroller">
                    <div class="container">
                        <?php
                        $i = 0;
                        $scanned_directory = array_diff(scandir("./../../Pessoas/" . $empresa . "/Vistorias/" . $vistoria), array('..', '.'));
                        foreach ($scanned_directory as $value) { ?>
                            <div class="content">
                                <div>
                                    <?php if (!in_array(end(explode('.', $value)), ["3gp", "mp4"])) {?>
                                        <a href="./../../Pessoas/<?php echo $empresa; ?>/Vistorias/<?php echo $vistoria; ?>/<?php echo $value; ?>">
                                            <img src="./../../util/timthumb.php?src=<?php echo "./../Pessoas/" . $empresa . "/Vistorias/" . $vistoria . "/" . $value . "&w=200"; ?>" title="<?php echo $legenda[$i]; ?> - <?php echo date("d/m/Y H:i:s", filemtime("./../../Pessoas/" . $empresa . "/Vistorias/" . $vistoria . "/" . $value)); ?>" alt="<?php echo $value; ?>" class="thumb" />
                                        </a>
                                    <?php } else { ?>
                                        <span>
                                            <img ondblclick="window.open('<?php echo "./../../Pessoas/" . $empresa . "/Vistorias/" . $vistoria . "/" . $value; ?>','_blank');" src="./../../util/timthumb.php?src=<?php echo "./../img/novideo.png&h=111&w=200"; ?>" title="Duplo clique para abrir - <?php echo date("d/m/Y H:i:s", filemtime("./../../Pessoas/" . $empresa . "/Vistorias/" . $vistoria . "/" . $value)); ?>" alt="<?php echo $value; ?>" style="margin-top: 5px;" class="thumb" />
                                        </span>                                    
                                    <?php } ?>
                                </div>
                            </div>                        
                        <?php $i++;} ?>
                    </div>                        
                </div>
            </div>
        </div>
        <script type='text/javascript' src='js/Galeria.js'></script>
    </body>
</html>