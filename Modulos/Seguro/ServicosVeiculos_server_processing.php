<?php

if (!isset($_GET['pdf'])) {
    include '../../Connections/configini.php';
}
$aColumns = array('id_modelo', 'id_servico', 'tempo', 'valor', 's.descricao', 'codigo_interno');
$table = "sf_veiculo_modelo_servico vs
inner join sf_veiculo_modelo md on vs.id_modelo = md.id
inner join sf_produtos s on s.conta_produto = vs.id_servico";
$PageName = "ServicosVeiculos";
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = " and id_modelo > 0 ";
$sWhere = "";
$sOrder = " ORDER BY " . $aColumns[0] . " asc ";
$sOrder2 = " ORDER BY " . $aColumns[0] . " asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) < count($aColumns)) {
                $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i])], 0) . " " . $_GET['sSortDir_' . $i] . ", ";
                $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i])], 1) . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY " . $aColumns[0] . " asc";
        $sOrder2 = " ORDER BY " . $aColumns[0] . " asc";
    }
}

if (is_numeric($_GET['txtMontadoras'])) {
    $sWhereX .= " and id_marca = " . $_GET['txtMontadoras'];
}

if (is_numeric($_GET['txtModelos'])) {
    $sWhereX .= " and id_modelo = " . $_GET['txtModelos'];
}

if (is_numeric($_GET['txtClassificacao'])) {
    $sWhereX .= " and id_classificacao = " . $_GET['txtClassificacao'];
}

if ($_GET['txtBusca'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= prepareCollum($aColumns[$i], 0) . " LIKE '%" . utf8_decode($_GET['txtBusca']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

$sQuery = "SELECT COUNT(*) total from " . $table . " where " . $aColumns[0] . " > 0 $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total from " . $table . " where " . $aColumns[0] . " > 0 $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,id_modelo, id_servico, 
tempo, valor, md.descricao modelo, s.descricao, s.codigo_interno,
(select top 1 descricao from sf_veiculo_marca m where m.id = id_marca) marca,
(select top 1 descricao from sf_veiculo_classificacao c where c.id = id_classificacao) classificacao 
from " . $table . " where " . $aColumns[0] . " > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder2;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[5]]) . "'><center>" . utf8_encode($aRow[$aColumns[5]]) . "</center></div>";
    $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[$aColumns[0]]) . "," . utf8_encode($aRow[$aColumns[1]]) . ")'><div id='formPQ' title='" . utf8_encode($aRow["descricao"]) . "'>" . utf8_encode($aRow["descricao"]) . "</div></a>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["modelo"]) . "'>" . utf8_encode($aRow["modelo"]) . "</div>"; 
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["marca"]) . "'>" . utf8_encode($aRow["marca"]) . "</div>"; 
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["classificacao"]) . "'>" . utf8_encode($aRow["classificacao"]) . "</div>"; 
    $row[] = "<center>" . utf8_encode($aRow["tempo"]) . "</center>";
    $row[] = "<div id='formPQ' title='" . escreverNumero($aRow["valor"], 1) . "'>" . escreverNumero($aRow["valor"], 1) . "</div>";
    if ($imprimir == 0) {
        $row[] = "<center><a title='Excluir' onClick=\"return confirm('Deseja deletar esse registro?');\" href='" . $PageName . ".php?id_modelo=" . $aRow[$aColumns[0]] . "&id_servico=" . $aRow[$aColumns[1]] . "'><input name='' type='image' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
    }
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);