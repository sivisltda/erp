<?php

if (!isset($_GET['pdf'])) {
    include '../../Connections/configini.php';
}
$aColumns = array('id', 'placa', 'marca', 'modelo', 'ano_modelo', 'combustivel', 'valor', 'v.id_fornecedores_despesas', 
'razao_social', 'descricao', 'preco_venda', 'dt_fim', 'status', 'f.tipo', 'cnpj', 'planos_status', 'imei', 'chip');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$colunas = "";
$filter = "";
$sOrder = " ORDER BY id desc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0 && intval($_GET['iSortCol_' . $i]) < 9) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY id desc";
    }
}

if (isset($_GET['ur']) && is_numeric($_GET['ur']) && !is_numeric($_GET['idCli'])) {
    $sWhereX .= " and f.id_user_resp = '" . $_GET['ur'] . "' ";
}

if ($crm_pro_ure_ > 0 && !is_numeric($_GET['idCli'])) {
    $sWhereX .= " and (id_usuario = " . $_SESSION["id_usuario"] . " 
    or id_usuario in (select sf_usuarios_dependentes.id_usuario from sf_usuarios_dependentes 
    inner join sf_usuarios on id_fornecedor_despesas = funcionario where sf_usuarios.id_usuario = " . $_SESSION["id_usuario"] . ")) ";
}

if (!is_numeric($_GET['idCli'])) {
    if (isset($_GET['filial']) && $_GET['filial'] != "null") {
        $sWhereX .= " AND f.empresa in (" . str_replace("\"", "'", str_replace(array("[", "]"), "", $_GET["filial"])) . ") ";
    } else {
        $sWhereX .= " AND f.empresa in (select id_filial_f from sf_usuarios_filiais where id_usuario_f = " . $_SESSION["id_usuario"] . ") ";
    }
}

if (isset($_GET['tpCli'])) {
    $sWhereX .= " AND f.tipo = " . valoresTexto2($_GET['tpCli']);
} 

if (is_numeric($_GET['idCli'])) {
    $sWhereX .= " AND f.id_fornecedores_despesas = " . $_GET['idCli'];
    $filter = "and dt_cancelamento is null ";
} 

if (isset($_GET['vistorias'])) {
    if ($_GET['vistorias'] == "0") {
        $sWhereX .= " AND v.id in(select id_veiculo from sf_vistorias where data_aprov is not null) ";
    } else if ($_GET['vistorias'] == "1") {
        $sWhereX .= " AND v.id in(select id_veiculo from sf_vistorias where data_aprov is null) ";
    } else if ($_GET['vistorias'] == "2") {
        $sWhereX .= " AND v.id not in(select id_veiculo from sf_vistorias) ";
    }
}

if (isset($_GET['statusap']) && $_GET['statusap'] != 'null') {
    $where .= " and (";
    $statusap = explode(",", $_GET['statusap']);
    for ($i = 0; $i < count($statusap); $i++) {            
        $where .= ($i > 0 ? " or " : "");
        if ($statusap[$i] == "0") {
            $where .= " v.status = 'Aguarda'";
        } elseif ($statusap[$i] == "1") {
            $where .= " v.status = 'Aprovado'";
        } elseif ($statusap[$i] == "2") {
            $where .= " v.status = 'Reprovado'";
        } elseif ($statusap[$i] == "3") {
            $where .= " v.status = 'Pendente'";
        }
    }
    $where .= ") ";
    $sWhereX .= $where;    
}

if (isset($_GET['categoria'])) {
    $sWhereX .= " AND v.categoria = " . $_GET['categoria'] . " ";
}

if (isset($_GET['status']) && $_GET['status'] != 'null') {
    $sWhereX .= " and sf_vendas_planos.planos_status in ('" . str_replace(",", "','", $_GET['status']) . "') ";
}

if (isset($_GET['statusCli']) && $_GET['statusCli'] != 'null') {
    $sWhereX .= " and f.fornecedores_status in ('" . str_replace(",", "','", $_GET['statusCli']) . "') ";
}

if (isset($_GET['tpVei']) && $_GET['tpVei'] != 'null') {
    $sWhereX .= " and v.tipo_veiculo in ('" . str_replace(",", "','", $_GET['tpVei']) . "') ";
}

if (isset($_GET['tpPlano']) && $_GET['tpPlano'] != 'null') {
    $sWhereX .= " and sf_vendas_planos.id_prod_plano in ('" . str_replace(",", "','", $_GET['tpPlano']) . "') ";
}

if (isset($_GET['tpItem']) && $_GET['tpItem'] != 'null') {
    $sWhereX .= " and sf_vendas_planos.id_plano in (select id_venda_plano from sf_vendas_planos_acessorios where id_servico_acessorio in ('" . str_replace(",", "','", $_GET['tpItem']) . "')) ";
}

if (isset($_GET['assistencia'])) {
    if ($_GET['assistencia'] == "0") {
        $sWhereX .= " AND v.id_externo is not null ";
    } else if ($_GET['assistencia'] == "1") {
        $sWhereX .= " AND v.id_externo is null ";
    }
}

if (isset($_GET['rastreador'])) {
    if ($_GET['rastreador'] == "1") {
        $sWhereX .= " AND len(imei) = 0 ";
    } else if ($_GET['rastreador'] == "0") {
        $sWhereX .= " AND len(imei) > 0 ";
    }
}

if (isset($_GET['valor'])) {
    $sWhereX .= " AND v.valor >= " . valoresNumericos2($_GET['valor']) . " ";
}

if (isset($_GET['sSearch']) && strlen($_GET['sSearch']) > 0) {
    $_GET['Search'] = $_GET['sSearch'];
}

if ($_GET['Search'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        if ($i !== 6) {
            $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['Search']) . "%' OR ";
        }
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

function existsFiles($tipo, $contrato, $id_cliente, $id_plano, $id_contrato, $id_mens) {
    $pasta = __DIR__."/../../Pessoas/".$contrato."/";   
    if (is_dir($pasta)) {
        if ($tipo == 0 && ((file_exists($pasta . "Documentos/" . $id_cliente . "/" . $id_plano . "_" . $id_mens . ".png")) ||
            (file_exists($pasta . "Documentos/" . $id_cliente . "/" . $id_plano . "_" . $id_mens . ".jpeg")) ||
            (file_exists($pasta . "Documentos/" . $id_cliente . "/" . $id_plano . "_" . $id_mens . ".pdf")))) { //Pagamento
            return true;
        } else if ($tipo == 1 && ((file_exists($pasta . "Documentos/" . $id_cliente . "/" . $id_plano . "_res.png")) ||
            (file_exists($pasta . "Documentos/" . $id_cliente . "/" . $id_plano . "_res.jpeg")) ||
            (file_exists($pasta . "Documentos/" . $id_cliente . "/" . $id_plano . "_res.pdf")))) { //Residência
            return true;            
        } else if ($tipo == 2 && ((file_exists($pasta . "Documentos/" . $id_cliente . "/" . $id_plano . "_cnh.png")) ||
            (file_exists($pasta . "Documentos/" . $id_cliente . "/" . $id_plano . "_cnh.jpeg")) ||
            (file_exists($pasta . "Documentos/" . $id_cliente . "/" . $id_plano . "_cnh.pdf")))) { //Habilitação
            return true;
        } else if ($tipo == 3 && file_exists($pasta . "Assinaturas/" . $id_cliente . "/" . $id_plano . "_" . $id_contrato . ".png")) { //Assinaturas
            return true;
        } else if ($tipo == 4) { //Assinaturas
            return $pasta . "Assinaturas/" . $id_cliente . "/" . $id_plano . "_" . $id_contrato . ".png";
        } else {
            return false;            
        }
    } else {
        return false;
    }
}

if (isset($_GET['getEstatisticas'])) {       
    $lbl_contrato = 0;      
    $lbl_pagamento = 0;
    $lbl_habilitacao = 0;          
    $lbl_residencia = 0;
    $sQuery = "SELECT f.id_fornecedores_despesas, id_plano, 
    (select max(id) from sf_contratos where portal = 1 and inativo = 0 and id in (select id_contrato from sf_contratos_grupos where id_grupo = conta_movimento)) contrato,        
    (select top 1 id_mens from sf_vendas_planos_mensalidade where id_plano_mens = id_plano order by dt_inicio_mens) mensalidade
    FROM sf_fornecedores_despesas_veiculo v 
    inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = v.id_fornecedores_despesas
    left join sf_vendas_planos on id_veiculo = v.id " . $filter . "left join sf_produtos on id_prod_plano = conta_produto
    left join sf_usuarios on id_user_resp = id_usuario    
    WHERE status = 'Pendente' " . $sWhereX . " " . $sWhere;
    //echo $sQuery;
    $cur2 = odbc_exec($con, $sQuery);
    while ($RFP = odbc_fetch_array($cur2)) {
        if (!existsFiles(0, $contrato, $RFP['id_fornecedores_despesas'], $RFP['id_plano'], $RFP['contrato'], $RFP['mensalidade'])) {
            $lbl_pagamento++;            
        }
        if (!existsFiles(1, $contrato, $RFP['id_fornecedores_despesas'], $RFP['id_plano'], $RFP['contrato'], $RFP['mensalidade'])) {
            $lbl_residencia++;
        }
        if (!existsFiles(2, $contrato, $RFP['id_fornecedores_despesas'], $RFP['id_plano'], $RFP['contrato'], $RFP['mensalidade'])) {
            $lbl_habilitacao++;
        }
        if (!existsFiles(3, $contrato, $RFP['id_fornecedores_despesas'], $RFP['id_plano'], $RFP['contrato'], $RFP['mensalidade'])) {
            $lbl_contrato++;            
        }
    }    
    $row = array();    
    $row["lbl_contrato"] = $lbl_contrato;    
    $row["lbl_pagamento"] = $lbl_pagamento;
    $row["lbl_habilitacao"] = $lbl_habilitacao;    
    $row["lbl_residencia"] = $lbl_residencia;
    echo json_encode($row); exit;
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => 0,
    "iTotalDisplayRecords" => 0,
    "aaData" => array()
);

for ($i = 0; $i < count($aColumns); $i++) {
    $colunas .= $aColumns[$i] . ",";
}

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row," . $colunas . "
chassi, renavam, nome_produto_cor, ano_fab, endereco,numero,complemento,bairro,cidade_nome,estado_sigla,cep,
(select sum(case when p.tp_preco = 1 then (px.segMax * p.preco_venda)/100 
when p.tp_preco = 2 then (v.valor * p.preco_venda)/100 else p.preco_venda end)
from sf_vendas_planos vp inner join sf_vendas_planos_acessorios vpa on vp.id_plano = vpa.id_venda_plano
inner join sf_fornecedores_despesas_veiculo v on v.id = vp.id_veiculo
inner join sf_produtos p on p.conta_produto = vpa.id_servico_acessorio
inner join sf_produtos px on px.conta_produto = vp.id_prod_plano
WHERE p.tipo='A' AND vpa.id_venda_plano = sf_vendas_planos.id_plano) adicionais, id_plano,
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = f.id_fornecedores_despesas) telefone_celular,
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = f.id_fornecedores_despesas) email,
(select max(id) from sf_contratos where portal = 1 and inativo = 0 and id in (select id_contrato from sf_contratos_grupos where id_grupo = conta_movimento)) contrato,        
(select top 1 id_mens from sf_vendas_planos_mensalidade where id_plano_mens = id_plano order by dt_inicio_mens) mensalidade,
(select top 1 descricao from sf_veiculo_tipos vt where vt.id = v.tipo_veiculo) veiculo_tipos ";
if (isset($_GET['tpImp']) && $_GET['tpImp'] == "E") {
    $sQuery1 .= ",(select dbo.VALOR_REAL_MENSALIDADE(MAX(id_mens)) from sf_vendas_planos_mensalidade m1 where m1.id_plano_mens = sf_vendas_planos.id_plano) valor_mens,
    (select max(login_user) from sf_usuarios where id_usuario = f.id_user_resp) consultor ";
}
$sQuery1 .= "from sf_fornecedores_despesas_veiculo v 
inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = v.id_fornecedores_despesas
left join tb_cidades cid on cid.cidade_codigo = f.cidade
left join tb_estados est on est.estado_codigo = f.estado
left join sf_vendas_planos on id_veiculo = v.id " . $filter . " left join sf_produtos on id_prod_plano = conta_produto
left join sf_usuarios on id_user_resp = id_usuario
left join sf_produtos_cor on id_produto_cor = cor
WHERE id > 0 " . $sWhereX . $sWhere . ") as a " . $sOrder; //WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1=" . $imprimir . " 
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    if (!is_numeric($_GET['filter'])
        || ($_GET['filter'] == "0" && !existsFiles(0, $contrato, $aRow['id_fornecedores_despesas'], $aRow['id_plano'], $aRow['contrato'], $aRow['mensalidade']) && utf8_encode($aRow['status']) == 'Pendente')
        || ($_GET['filter'] == "1" && !existsFiles(1, $contrato, $aRow['id_fornecedores_despesas'], $aRow['id_plano'], $aRow['contrato'], $aRow['mensalidade']) && utf8_encode($aRow['status']) == 'Pendente')
        || ($_GET['filter'] == "2" && !existsFiles(2, $contrato, $aRow['id_fornecedores_despesas'], $aRow['id_plano'], $aRow['contrato'], $aRow['mensalidade']) && utf8_encode($aRow['status']) == 'Pendente')
        || ($_GET['filter'] == "3" && !existsFiles(3, $contrato, $aRow['id_fornecedores_despesas'], $aRow['id_plano'], $aRow['contrato'], $aRow['mensalidade']) && utf8_encode($aRow['status']) == 'Pendente')) {
        $iFilteredTotal = $iFilteredTotal + 1;            
        if ($iFilteredTotal > $sLimit && $iFilteredTotal <= ($sLimit + $sQtd) || $imprimir == 1) {         
            $backColor = (utf8_encode($aRow['status']) == 'Pendente' ? " style='color: red' " : "");    
            $row = array();
            if (isset($_GET['emailMarketing']) || isset($_GET['envioSms'])) {
                $row[] = utf8_encode($aRow["id_fornecedores_despesas"]);        
                $row[] = utf8_encode($aRow["telefone_celular"]);
                $row[] = utf8_encode($aRow["email"]);
            } else {
                $row[] = "<center><span title=\"" . (strlen($aRow[$aColumns[15]]) > 0 ? $aRow[$aColumns[15]] : "Cancelado") . "\" class=\"label-" . (strlen($aRow[$aColumns[15]]) > 0 ? $aRow[$aColumns[15]] : "Cancelado") . "\" style=\"display: inline-block;width: 13px;height: 13px;border-radius: 50%!important;\"></span></center>";            
                $row[] = "<center><a href='javascript:void(0)' onClick='AbrirBoxVeiculo(" . utf8_encode($aRow[$aColumns[0]]) . "," . utf8_encode($aRow["id_fornecedores_despesas"]) . ")'><div id='formPQ' title='" . strtoupper($aRow[$aColumns[1]]) . "'>" . ($aRow[$aColumns[1]] == "" ? "Zero Km" : strtoupper($aRow[$aColumns[1]])) . "</div></a></center>";    
                if (is_numeric($_GET['idCli'])) {    
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow[$aColumns[2]]) . "'>" . utf8_encode($aRow[$aColumns[2]]) . "</div>";       
                }
                if ($_GET['tpImp'] == "E") {
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow[$aColumns[2]]) . "'>" . utf8_encode($aRow[$aColumns[2]]) . "</div>";                    
                }                
                $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow[$aColumns[3]]) . "'>" . utf8_encode($aRow[$aColumns[3]]) . "</div>";
                if ($_GET['tpImp'] == "E") {
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow["combustivel"]) . "'>" . utf8_encode($aRow["combustivel"]) . "</div>";                    
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow["chassi"]) . "'>" . utf8_encode($aRow["chassi"]) . "</div>";                    
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow["renavam"]) . "'>" . utf8_encode($aRow["renavam"]) . "</div>";                    
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow["nome_produto_cor"]) . "'>" . utf8_encode($aRow["nome_produto_cor"]) . "</div>";                    
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow["ano_fab"]) . "'>" . ($aRow["ano_fab"] == "32000" ? "Zero Km" : $aRow["ano_fab"]) . "</div>";                    
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow["ano_modelo"]) . "'>" . utf8_encode($aRow["ano_modelo"]) . "</div>";                                        
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow["cep"]) . "'>" . utf8_encode($aRow["cep"]) . "</div>";                    
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow["endereco"]) . "'>" . utf8_encode($aRow["endereco"]) . "</div>";
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow["numero"]) . "'>" . utf8_encode($aRow["numero"]) . "</div>";
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow["complemento"]) . "'>" . utf8_encode($aRow["complemento"]) . "</div>";
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow["bairro"]) . "'>" . utf8_encode($aRow["bairro"]) . "</div>";
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow["cidade_nome"]) . "'>" . utf8_encode($aRow["cidade_nome"]) . "</div>";
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow["estado_sigla"]) . "'>" . utf8_encode($aRow["estado_sigla"]) . "</div>";                    
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow["veiculo_tipos"]) . "'>" . utf8_encode($aRow["veiculo_tipos"]) . "</div>";
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow["telefone_celular"]) . "'>" . utf8_encode($aRow["telefone_celular"]) . "</div>";
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow["email"]) . "'>" . utf8_encode($aRow["email"]) . "</div>";
                }
                if (is_numeric($_GET['idCli'])) {    
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow[$aColumns[4]]) . "'>" . ($aRow[$aColumns[4]] == "32000" ? "Zero Km" : $aRow[$aColumns[4]]) . "</div>";   
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow[$aColumns[5]]) . "'>" . utf8_encode($aRow[$aColumns[5]]) . "</div>";   
                } else {
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow[$aColumns[16]]) . "'>" . utf8_encode($aRow[$aColumns[16]]) . "</div>";       
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow[$aColumns[17]]) . "'>" . utf8_encode($aRow[$aColumns[17]]) . "</div>";       
                }
                $row[] = "<div " . $backColor . " id='formPQ' title='" . escreverNumero($aRow[$aColumns[6]]) . "'>" . escreverNumero($aRow[$aColumns[6]]) . "</div>";       
                if (!is_numeric($_GET['idCli'])) {
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow[$aColumns[14]]) . "'>" . utf8_encode($aRow[$aColumns[14]]) . "</div>";                   
                    $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow[$aColumns[8]]) . "'><a href='javascript:void(0)' onclick=\"AbrirBoxCli('" . utf8_encode($aRow["tipo"]) . "'," . utf8_encode($aRow["id_fornecedores_despesas"]) . ")\">" . utf8_encode($aRow[$aColumns[8]]) . "</a></div>";
                }        
                $row[] = "<div " . $backColor . " id='formPQ' title='" . utf8_encode($aRow[$aColumns[9]]) . "'>" . utf8_encode($aRow[$aColumns[9]]) . "</div>";      
                $row[] = ($aRow[$aColumns[10]] != "" ? "<div " . $backColor . " id='formPQ' title='" . escreverNumero($aRow[$aColumns[10]]) . "'>" . escreverNumero($aRow[$aColumns[10]]) . "</div>" : "");   
                $row[] = ($aRow["adicionais"] != "" ? "<div " . $backColor . " id='formPQ' title='" . escreverNumero($aRow["adicionais"]) . "'>" . escreverNumero($aRow["adicionais"]) . "</div>" : "");        
                $row[] = "<div " . $backColor . " id='formPQ' title='" . escreverData($aRow[$aColumns[11]]) . "'>" . escreverData($aRow[$aColumns[11]]) . "</div>";   
                $color = "style=\"color:";
                if (utf8_encode($aRow['status']) == 'Aguarda') {
                    $color = $color . "#bc9f00\"";
                } elseif (utf8_encode($aRow['status']) == 'Aprovado') {
                    $color = $color . "#0066cc\"";
                } elseif (utf8_encode($aRow['status']) == 'Reprovado') {
                    $color = $color . "#f00\"";
                } elseif (utf8_encode($aRow['status']) == 'Pendente') {
                    $color = $color . "orange\"";
                } else {
                    $color = $color . "black\"";
                }
                $titleStatus = "";
                if (utf8_encode($aRow['status']) == 'Pendente') {
                    if (!existsFiles(0, $contrato, $aRow['id_fornecedores_despesas'], $aRow['id_plano'], $aRow['contrato'], $aRow['mensalidade'])) {          
                        $titleStatus .= "* Comprovante de Pagamento \n";                    
                    }
                    if (!existsFiles(1, $contrato, $aRow['id_fornecedores_despesas'], $aRow['id_plano'], $aRow['contrato'], $aRow['mensalidade'])) {
                        $titleStatus .= "* Comprovante de Residência \n";
                    }
                    if (!existsFiles(2, $contrato, $aRow['id_fornecedores_despesas'], $aRow['id_plano'], $aRow['contrato'], $aRow['mensalidade'])) {
                        $titleStatus .= "* Comprovante de Habilitação \n";            
                    }
                    if (!existsFiles(3, $contrato, $aRow['id_fornecedores_despesas'], $aRow['id_plano'], $aRow['contrato'], $aRow['mensalidade'])) {
                        $titleStatus .= "* Assinatura do Contrato \n";  
                    }
                }
                $row[] = "<div id='formPQ' " . $color . "><center>" . utf8_encode($aRow['status']) . "</center></div>";    
                if (!is_numeric($_GET['idCli']) && $_GET['tpImp'] != "E") {
                    $row[] = "<center>" . (strlen($titleStatus) > 0 ? "<img src=\"../../img/apagar.PNG\" width='18' height='18' style=\"margin-right: 2px;\" title=\"" . $titleStatus . "\">" : "") .
                    ($ckb_pro_rastreador_ > 0 ? "<input title='Dados de Rastreador' name='' type='image' onClick='AbrirBoxRastreador(" . utf8_encode($aRow[$aColumns[0]]) . ")' src='../../img/1365123843_onebit_323 copy.PNG' width='18' height='18' value='Editar'>" : "") . 
                    ($ckb_pro_exc_vei_ > 0 && $ckb_adm_cli_read_ == 0 ? "<a title='Excluir' href='Veiculos.php?Del=" . $aRow[$aColumns[0]] . "'><input name='' type='image' onClick=\"return confirm('Deseja deletar esse registro?')\" src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a>" : "") . "</center>";
                }
                if (isset($_GET['tpImp']) && $_GET['tpImp'] == "E") {
                    $row[] = escreverNumero($aRow["valor_mens"], 1);
                    $row[] = utf8_encode($aRow["consultor"]);
                    if (existsFiles(3, $contrato, $aRow['id_fornecedores_despesas'], $aRow['id_plano'], $aRow['contrato'], $aRow['mensalidade'])) {
                        $row[] = date('d/m/Y H:i', filemtime(existsFiles(4, $contrato, $aRow['id_fornecedores_despesas'], $aRow['id_plano'], $aRow['contrato'], $aRow['mensalidade'])));
                    } else {
                        $row[] = "";
                    }
                }                
            }
            $output['aaData'][] = $row;
        }
    }
}

$output['iTotalDisplayRecords'] = $iFilteredTotal;
$output['iTotalRecords'] = $iFilteredTotal;

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
