<?php
include "./../../Connections/configini.php";
$FinalUrl = '';
if ($_GET['idx'] != '') {
    $FinalUrl = "id=" . $_GET['idx'];
}
if ($_GET['tpx'] != '') {
    $FinalUrl = $FinalUrl . "&tp=" . $_GET['tpx'];
}
if ($_GET['tdx'] != '') {
    $FinalUrl = $FinalUrl . "&td=" . $_GET['tdx'];
}
if ($_GET['dti'] != '') {
    $FinalUrl = $FinalUrl . "&dti=" . $_GET['dti'];
}
if ($_GET['dtf'] != '') {
    $FinalUrl = $FinalUrl . "&dtf=" . $_GET['dtf'];
}

$disabled = 'disabled';
$active1 = 'active';
$linhaContato = "1";

if (isset($_POST['bntSave'])) {
    $notIn = "";
    $idCliente = "";
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_fornecedores_despesas set " .
                        "razao_social = '" . utf8_decode($_POST['txtRazao']) . "'," .
                        "nome_fantasia = '" . utf8_decode($_POST['txtFantasia']) . "'," .
                        "cnpj = '" . utf8_decode($_SESSION["cpf_cnpj"]) . "'," .
                        "inscricao_estadual = '" . utf8_decode($_POST['txtInsEst']) . "'," .
                        "cnae_fiscal = '" . utf8_decode($_POST['txtCnaeFiscal']) . "'," .
                        "inscricao_municipal = '" . utf8_decode($_POST['txtInsMun']) . "'," .
                        "ie_tributario = '" . utf8_decode($_POST['txtSubTrib']) . "'," .
                        "regime_tributario = '" . utf8_decode($_POST['txtRegTrib']) . "'," .
                        "regime_tributario_especial = " . valoresSelect("txtRegTribEsp") . "," .
                        "endereco = '" . utf8_decode($_POST['txtEndereco']) . "'," .
                        "numero = '" . utf8_decode($_POST['txtNumero']) . "'," .
                        "complemento = '" . utf8_decode($_POST['txtComplemento']) . "'," .
                        "bairro = '" . utf8_decode($_POST['txtBairro']) . "'," .
                        "estado = " . valoresSelect("txtEstado") . "," .
                        "cidade = " . valoresSelect("txtCidade") . "," .
                        "comiss_gere = " . valoresNumericos("txtImposto") . "," .
                        "cep = '" . utf8_decode($_POST['txtCep']) . "' where id_fornecedores_despesas = " . $_POST['txtId']) or die(odbc_errormsg());
        $notIn = '0';
        if (isset($_POST['txtQtdContatos'])) {
            $max = $_POST['txtQtdContatos'];
            for ($i = 0; $i <= $max; $i++) {
                if (isset($_POST['txtIdContato_' . $i]) && is_numeric($_POST['txtIdContato_' . $i])) {
                    if ($_POST['txtIdContato_' . $i] != 0) {
                        $notIn = $notIn . "," . $_POST['txtIdContato_' . $i];
                    }
                }
            }
            if ($notIn != "") {
                odbc_exec($con, "delete from sf_fornecedores_despesas_contatos where fornecedores_despesas = " . $_POST['txtId'] . " and id_contatos not in (" . $notIn . ")");
            }
        }
        $idCliente = $_POST['txtId'];
    } else {
        $toSave = true;
        if ($_POST['txtCnpj'] != '') {
            $cur = odbc_exec($con, "select * from sf_fornecedores_despesas where tipo = 'M' and cnpj = '" . str_replace("'", "", $_POST['txtCnpj']) . "'");
            while ($RFP = odbc_fetch_array($cur)) {
                $toSave = false;
            }
        }
        if ($toSave == false) {
            echo "<script>alert('Registro de CPF/CNPJ já cadastrado no sistema!');</script>";
        } else {
            $query = "insert into sf_fornecedores_despesas(razao_social,nome_fantasia,cnpj,inscricao_estadual,cnae_fiscal,inscricao_municipal,ie_tributario,regime_tributario,regime_tributario_especial,endereco,numero,complemento,bairro,estado,cidade,cep,tipo,empresa,dt_cadastro,comiss_gere)values('" .
                    utf8_decode($_POST['txtRazao']) . "','" .
                    utf8_decode($_POST['txtFantasia']) . "','" .
                    utf8_decode($_SESSION["cpf_cnpj"]) . "','" .
                    utf8_decode($_POST['txtInsEst']) . "','" .
                    utf8_decode($_POST['txtCnaeFiscal']) . "','" .
                    utf8_decode($_POST['txtInsMun']) . "','" .
                    utf8_decode($_POST['txtSubTrib']) . "','" .
                    utf8_decode($_POST['txtRegTrib']) . "'," .
                    valoresSelect("txtRegTribEsp") . ",'" .
                    utf8_decode($_POST['txtEndereco']) . "','" .
                    utf8_decode($_POST['txtNumero']) . "','" .
                    utf8_decode($_POST['txtComplemento']) . "','" .
                    utf8_decode($_POST['txtBairro']) . "'," .
                    valoresSelect("txtEstado") . "," .
                    valoresSelect("txtCidade") . ",'" .
                    utf8_decode($_POST['txtCep']) . "','M','001',getDate()," .
                    valoresNumericos("txtImposto") . ")";
            odbc_exec($con, $query) or die(odbc_errormsg());
            $res = odbc_exec($con, "select top 1 id_fornecedores_despesas from sf_fornecedores_despesas order by id_fornecedores_despesas desc") or die(odbc_errormsg());
            //$_POST['txtId'] = odbc_result($res, 1);
            $idCliente = odbc_result($res, 1);
        }
    }
    if ($idCliente !== "") {
        if (isset($_POST['txtQtdContatos'])) {
            $max = $_POST['txtQtdContatos'];
            for ($i = 1; $i < $max; $i++) {
                if (isset($_POST['txtIdContato_' . $i])) {
                    if ($_POST['txtIdContato_' . $i] !== "0") {
                        odbc_exec($con, "update sf_fornecedores_despesas_contatos set tipo_contato = " . $_POST['txtTpContato_' . $i] . ",conteudo_contato = " . valoresTexto('txtTxContato_' . $i) . " where id_contatos = " . $_POST['txtIdContato_' . $i]) or die(odbc_errormsg());
                    } else {
                        odbc_exec($con, "insert into sf_fornecedores_despesas_contatos(fornecedores_despesas,tipo_contato,conteudo_contato) values(" . $idCliente . "," . $_POST['txtTpContato_' . $i] . "," . valoresTexto('txtTxContato_' . $i) . ")") or die(odbc_errormsg());
                    }
                }
            }
        }
        $_POST['txtId'] = $idCliente;
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = " . $_POST['txtId']);
        odbc_exec($con, "DELETE FROM sf_fornecedores_despesas WHERE id_fornecedores_despesas = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso');window.top.location.href = 'Emitentes.php';</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_fornecedores_despesas where id_fornecedores_despesas = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_fornecedores_despesas']);
        $razao = utf8_encode($RFP['razao_social']);
        $fantasia = utf8_encode($RFP['nome_fantasia']);
        $cnpj = utf8_encode($RFP['cnpj']);
        $ins_estadual = utf8_encode($RFP['inscricao_estadual']);
        $cnae_fiscal = utf8_encode($RFP['cnae_fiscal']);
        $ins_municipal = utf8_encode($RFP['inscricao_municipal']);
        $subst_trib = utf8_encode($RFP['ie_tributario']);
        $reg_trib = utf8_encode($RFP['regime_tributario']);
        $reg_trib_esp = utf8_encode($RFP['regime_tributario_especial']);
        $endereco = utf8_encode($RFP['endereco']);
        $numero = utf8_encode($RFP['numero']);
        $complemento = utf8_encode($RFP['complemento']);
        $bairro = utf8_encode($RFP['bairro']);
        $estado = utf8_encode($RFP['estado']);
        $cidade = utf8_encode($RFP['cidade']);
        $cep = utf8_encode($RFP['cep']);
        $imposto = escreverNumero($RFP['comiss_gere']);
    }
} else {
    $disabled = '';
    $id = '';
    $razao = '';
    $fantasia = '';
    $cnpj = '';
    $ins_estadual = '';
    $cnae_fiscal = '';
    $ins_municipal = '';
    $subst_trib = '';
    $reg_trib = '';
    $reg_trib_esp = '';
    $endereco = '';
    $numero = '';
    $complemento = '';
    $bairro = '';
    $estado = '';
    $cidade = '';
    $cep = '';
    $imposto = escreverNumero(0);
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <div class="row-fluid" style="width:720px">
        <div class="block title">                
        </div>
        <div>
            <div class="block">                            
                <form action="FormEmitentes.php" name="frmEnviaDados" method="POST" enctype="multipart/form-data">
                    <div class="frmhead">
                        <div class="frmtext">Emitentes</div>
                        <div class="frmicon" onClick="parent.FecharBox()">
                            <span class="ico-remove"></span>
                        </div>
                    </div>
                    <input name="txtFinalUrl" id="txtFinalUrl" value="<?php echo $FinalUrl; ?>" type="hidden"/>
                    <input name="txtQtdContatos" id="txtQtdContatos" value="<?php echo $linhaContato; ?>" type="hidden"/>                                                    
                    <div class="tabbable frmtabs">                    
                        <ul class="nav nav-tabs">
                            <li class="<?php echo $active1; ?>" style="margin-left:10px"><a href="#tab1" data-toggle="tab">Dados Principais</a></li>
                            <li><a href="#tab2" data-toggle="tab">Endereço</a></li>
                            <li><a href="#tab3" data-toggle="tab">Contatos</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane <?php echo $active1; ?>" id="tab1">   
                                <div style="height: 344px;">
                                <table width="680" border="0" cellpadding="3" cellspacing="0" class="textosComuns">
                                    <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                                    <tr>
                                        <td width="110" align="right">Nome/Razão Social:</td>
                                        <td colspan="3"><input type="text" name="txtRazao" id="txtRazao" maxlength="128" class="input-medium" value="<?php echo $razao; ?>" <?php echo $disabled; ?>/></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Nome Fantasia:</td>
                                        <td colspan="3"><input type="text" name="txtFantasia" id="txtFantasia" maxlength="128" class="input-medium" value="<?php echo $fantasia; ?>" <?php echo $disabled; ?>/></td>
                                    </tr>
                                    <tr>
                                        <td align="right">CNPJ:</td>
                                        <td width="210"><input type="text" name="txtCnpj" id="txtCnpj" maxlength="18" class="input-medium" value="<?php echo $_SESSION["cpf_cnpj"]; ?>" disabled /></td>
                                        <td width="110" align="right">Ins. Estadual:</td>
                                        <td><input type="text" name="txtInsEst" id="txtInsEst" class="input-medium" maxlength="25" value="<?php echo $ins_estadual; ?>" <?php echo $disabled; ?>/></td>
                                    </tr>
                                    <tr>
                                        <td align="right">CNAE Fiscal:</td>
                                        <td><input type="text" name="txtCnaeFiscal" id="txtCnaeFiscal" class="input-medium" maxlength="10" value="<?php echo $cnae_fiscal; ?>" <?php echo $disabled; ?>/></td>
                                        <td align="right">Ins. Municipal:</td>
                                        <td><input type="text" name="txtInsMun" id="txtInsMun" maxlength="20" class="input-medium" value="<?php echo $ins_municipal; ?>" <?php echo $disabled; ?>/></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Subst. Tributário:</td>
                                        <td><input type="text" name="txtSubTrib" id="txtSubTrib" maxlength="20" class="input-medium" value="<?php echo $subst_trib; ?>" <?php echo $disabled; ?>/></td>
                                        <td align="right">Reg. Tributário:</td>
                                        <td><select name="txtRegTrib" id="txtRegTrib" class="input-medium" style="width:100%" <?php echo $disabled; ?>>
                                                <option value="1" <?php
                                                if ($reg_trib == "1") {
                                                    echo "SELECTED";
                                                }
                                                ?>>Simples Nacional</option>
                                                <option value="2" <?php
                                                if ($reg_trib == "2") {
                                                    echo "SELECTED";
                                                }
                                                ?>>Simples Nacional – excesso de sublimite de receita bruta</option>
                                                <option value="3" <?php
                                                if ($reg_trib == "3") {
                                                    echo "SELECTED";
                                                }
                                                ?>>Regime Normal</option>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td align="right">(%) de Impostos:</td>
                                        <td><input type="text" name="txtImposto" id="txtImposto" maxlength="10" class="input-medium" value="<?php echo $imposto; ?>" <?php echo $disabled; ?>/></td>
                                        <td align="right">Regime especial de Tributação</td>
                                        <td>
                                            <select id="txtRegTribEsp" name="txtRegTribEsp" class="input-medium" style="width:100%" <?php echo $disabled; ?>>
                                                <option value="">Selecione</option>
                                                <option value="0" <?php
                                                if ($reg_trib_esp == "0") {
                                                    echo "SELECTED";
                                                }
                                                ?>> - </option>
                                                <option value="1" <?php
                                                if ($reg_trib_esp == "1") {
                                                    echo "SELECTED";
                                                }
                                                ?>>Microempresa Municipal</option>
                                                <option value="2" <?php
                                                if ($reg_trib_esp == "2") {
                                                    echo "SELECTED";
                                                }
                                                ?>>Estimativa</option>
                                                <option value="3" <?php
                                                if ($reg_trib_esp == "3") {
                                                    echo "SELECTED";
                                                }
                                                ?>>Sociedade de Profissionais</option>
                                                <option value="4" <?php
                                                if ($reg_trib_esp == "4") {
                                                    echo "SELECTED";
                                                }
                                                ?>>Cooperativa</option>
                                                <option value="5" <?php
                                                if ($reg_trib_esp == "5") {
                                                    echo "SELECTED";
                                                }
                                                ?>>MEI - Simples Nacional</option>
                                                <option value="6" <?php
                                                if ($reg_trib_esp == "6") {
                                                    echo "SELECTED";
                                                }
                                                ?>>ME EPP - Simples Nacional</option>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab2">
                                <div style="height: 344px;">
                                <table width="680" border="0" cellpadding="3" class="textosComuns">
                                    <tr>
                                        <td width="110" align="right">Endereço:</td>
                                        <td colspan="3"><input name="txtEndereco" id="txtEndereco" type="text" maxlength="256" class="input-medium" value="<?php echo $endereco; ?>" <?php echo $disabled; ?>/></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Numero:</td>
                                        <td width="210"><input type="text" name="txtNumero" id="txtNumero" maxlength="6" class="input-medium" style="width:100px" value="<?php echo $numero; ?>" <?php echo $disabled; ?>/></td>
                                        <td width="110" align="right">Compl.:</td>
                                        <td><input id="txtComplemento" <?php echo $disabled; ?> name="txtComplemento" maxlength="20" type="text" class="input-medium" value="<?php echo $complemento; ?>"/></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Bairro:</td>
                                        <td><input id="txtBairro" name="txtBairro" <?php echo $disabled; ?> type="text" maxlength="20" class="input-medium" value="<?php echo $bairro; ?>"/></td>
                                        <td align="right">Estado:</td>
                                        <td><select name="txtEstado" id="txtEstado" class="input-medium" style="width:100%" <?php echo $disabled; ?>>
                                                <option value="null" >Selecione o estado</option>
                                                <?php
                                                $cur = odbc_exec($con, "select estado_codigo,estado_nome from tb_estados where estado_codigo > 0 order by estado_nome") or die(odbc_errormsg());
                                                while ($RFP = odbc_fetch_array($cur)) {
                                                    ?>
                                                    <option value="<?php echo $RFP['estado_codigo'] ?>"<?php
                                                    if (!(strcmp($RFP['estado_codigo'], $estado))) {
                                                        echo "SELECTED";
                                                    }
                                                    ?>><?php echo utf8_encode($RFP['estado_nome']) ?></option>
                                                        <?php } ?>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Cidade:</td>
                                        <td><span id="carregando" name="carregando" style="color:#666; display:none">Aguarde, carregando...</span>
                                            <select name="txtCidade" id="txtCidade" class="input-medium" style="width:100%" <?php echo $disabled; ?>>
                                                <option value="null" >Selecione a cidade</option>
                                                <?php
                                                if ($estado != '') {
                                                    $sql = "select cidade_codigo,cidade_nome from tb_cidades where cidade_codigo > 0 AND cidade_codigoEstado = " . $estado . " ORDER BY cidade_nome";
                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                    <option value="<?php echo $RFP['cidade_codigo'] ?>"<?php
                                                    if (!(strcmp($RFP['cidade_codigo'], $cidade))) {
                                                        echo "SELECTED";
                                                    }
                                                    ?>><?php echo utf8_encode($RFP['cidade_nome']) ?></option>
                                                            <?php
                                                        }
                                                    } ?>
                                            </select>
                                            </span></td>
                                        <td align="right">CEP:</td>
                                        <td><input type="text" name="txtCep" id="txtCep" maxlength="9" class="input-medium" style="width:100px" value="<?php echo $cep; ?>" <?php echo $disabled; ?>/></td>
                                    </tr>
                                </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab3">    
                                <div style="height: 344px;">
                                <div class="toolbar dark" style="background-color: #DCDCDC">
                                    <div class="input-prepend input-append">
                                    </div>
                                    <table width="99%" border="0" cellpadding="3" class="textosComuns">
                                        <tr>
                                            <td>                                                                                                                                                                                                
                                                <select id="txtTipoContato" name="txtTipoContato" onChange="alterarMask()" <?php echo $disabled; ?>>
                                                    <option value="0">Telefone</option>
                                                    <option value="1">Celular</option>
                                                    <option value="2">E-mail</option>                                                        
                                                    <option value="3">Site</option>                                                        
                                                    <option value="4">Outros</option>                                                        
                                                </select>
                                            </td>
                                            <td id="sprytextfield10">
                                                <input id="txtTextoContato" type="text" <?php echo $disabled; ?>/>
                                            </td>
                                            <td width="80" align="right">                                                                    
                                                <button type="button" onclick="addContato();" <?php echo $disabled; ?> class="btn dblue">Incluir <span class="icon-arrow-next icon-white"></span></button>
                                            </td>                                                         
                                        </tr>
                                    </table>
                                </div>
                                <div class="data dark npr npb" style="overflow:hidden">                                
                                    <div id="divContatos" class="messages" style="height:305px; overflow-y:scroll"></div>                                
                                </div>                                                      
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="frmfoot">
                        <div class="frmbtn">
                            <?php if ($disabled == '') { ?>
                                <button class="btn green" type="submit" title="Gravar" name="bntSave"><span class="ico-checkmark"></span> Gravar</button>
                                <?php if ($_POST['txtId'] == '') { ?>
                                    <button class="btn yellow" type="button" title="Cancelar" onClick="parent.FecharBox();"><span class="ico-reply"></span> Cancelar</button>
                                <?php } else { ?>
                                    <button class="btn yellow" type="submit" title="Cancelar" name="bntAlterar" ><span class="ico-reply"></span> Cancelar</button>
                                    <?php
                                }
                            } else { ?>
                                <button class="btn green" type="submit" title="Novo" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"> </span>  Novo</button>
                                <button class="btn green" type="submit" title="Alterar" name="bntEdit" value="Alterar"> <span class="ico-edit"> </span>  Alterar</button>
                                <button class="btn red" type="submit" title="Excluir" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span>  Excluir</button>
                            <?php } ?>
                        </div>
                    </div>                                               
                </form>                            
            </div> 
            <div class="block"></div>
        </div>
    </div>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>    
    <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>        
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>                
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>            
    <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>            
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/charts.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>    
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script> 
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type="text/javascript">
        
        $("#txtImposto").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
        
        var idLinhaContato =<?php echo $linhaContato; ?>;
        $("#frmEnviaDados").validate({
            errorPlacement: function (error, element) {
                return true;
            },
            rules: {
                txtRazao: {required: true},
                txtCnpj: {required: true},
                txtInsEst: {required: true}
            }
        });
        $("#txtCnpj").mask("99.999.999/9999-99");
        $("#txtCep").mask("99999-999");

        $(function () {
            $('#txtEstado').change(function () {
                if ($(this).val()) {
                    $('#txtCidade').hide();
                    $('#carregando').show();
                    $.getJSON('locais.ajax.php?search=', {txtEstado: $(this).val(), ajax: 'true'}, function (j) {
                        var options = '<option value=""></option>';
                        for (var i = 0; i < j.length; i++) {
                            options += '<option value="' + j[i].cidade_codigo + '">' + j[i].cidade_nome + '</option>';
                        }
                        $('#txtCidade').html(options).show();
                        $('#carregando').hide();
                    });
                } else {
                    $('#txtCidade').html('<option value="">Selecione a cidade</option>');
                }
            });
        });

        function alterarMask() {
            $('#txtTextoContato').val("");
            var tipo = $("#txtTipoContato option:selected").val();
            if (sprytextfield10 !== null) {
                sprytextfield10.reset();
                sprytextfield10.destroy();
                sprytextfield10 = null;
            }
            if (tipo === "0") {
                sprytextfield10 = new Spry.Widget.ValidationTextField("sprytextfield10", "custom", {pattern: "(00) 0000-0000", useCharacterMasking: true, isRequired: false});
            } else if (tipo === "1") {
                sprytextfield10 = new Spry.Widget.ValidationTextField("sprytextfield10", "custom", {pattern: "(00) 00000-0000", useCharacterMasking: true, isRequired: false});
            }
        }
        
        function addContato() {
            var msg = $("#txtTextoContato").val();
            var sel = $("#txtTipoContato").val();
            if (msg !== "") {
                var arb = msg + "@";
                if (sel !== 2 || (sel === 2 && arb.match(/@/g).length === 2)) {
                    addLinhaContato($("#txtTipoContato").val(), $("#txtTipoContato option:selected").text(), $("#txtTextoContato").val(), "0");
                } else {
                    bootbox.alert("Preencha um endereço de e-mail corretamente!");
                }
            } else {
                bootbox.alert("Preencha as informações de contato corretamente!");
            }
        }
        
        function addLinhaContato(idTipo, tipo, conteudo, id) {
            var btExcluir = "";
            <?php if ($disabled != "") { ?>
                btExcluir = '<div style="width:5%; padding:4px 0 3px; float:right"></div>';
            <?php } else { ?>
                btExcluir = '<div style="width:5%; padding:4px 0 3px; float:right" onClick="return removeLinha(\'tabela_linha_' + idLinhaContato + '\');"><span class="ico-remove" style="font-size:20px"></span></div>';
            <?php } ?>
            var linha = "";
            linha = '<div id="tabela_linha_' + idLinhaContato + '" style="padding:5px; border-bottom:1px solid #DDD">\n\
            <input id="txtIdContato_' + idLinhaContato + '" name="txtIdContato_' + idLinhaContato + '" value="' + id + '" type="hidden"></input>\n\
            <input id="txtTpContato_' + idLinhaContato + '" name="txtTpContato_' + idLinhaContato + '" value="' + idTipo + '" type="hidden"></input>\n\
            <div style="width:32%; padding-left:1%; line-height:27px; float:left">' + tipo + '</div>\n\
            <input id="txtTxContato_' + idLinhaContato + '" name="txtTxContato_' + idLinhaContato + '" value="' + conteudo + '" type="hidden"></input>\n\
            ' + btExcluir + '<div style="width:60%; line-height:27px; float:left">' + conteudo + '</div></div>';
            $("#divContatos").prepend(linha);
            idLinhaContato++;
            $("#txtQtdContatos").val(idLinhaContato);
        }
        
        function removeLinha(id) {
            $("#" + id).remove();
        }

        $(document).ready(function () {
            listContatos();
        });
        
        function listContatos() {
            $.getJSON('../Comercial/FormGerenciador-Clientes.contatos.ajax.php?search=', {cod: <?php
                if (is_numeric($id)) {
                echo $id;
                } else {
                echo "0";
                }
                ?>, ajax: 'true'}, function (j) {
                for (var i = 0; i < j.length; i++) {
                    addLinhaContato(j[i].tipo_contato, j[i].texto_contatos, j[i].conteudo_contato, j[i].id);
                }
            });
        }

        var sprytextfield10 = new Spry.Widget.ValidationTextField("sprytextfield10", "custom", {pattern: "(00) 0000-0000", useCharacterMasking: true, isRequired: false});
    </script>
    <?php odbc_close($con); ?>
</body>
