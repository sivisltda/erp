<?php

$tpTela = "NFE";
$finalUrl .= "?TpTela=NFE";
$finalUrl .= "&id=" . $_GET["id"];
$finalUrl .= "&produto=" . $_GET["produto"];

if (isset($_POST['bntSave'])) {
    $idNfe = $_POST['txtId'];
    $idProduto = $_POST['txtIdProduto'];

    $query = "update sf_nfe_notas_itens set " .
            "tot_desconto = " . valoresNumericos('txtDescontos') . ", " .
            "tot_outras_despesas = " . valoresNumericos('txtOutrasDespesas') . ", " .
            "tot_seguro = " . valoresNumericos('txtSeguro') . ", " .
            "tot_frete = " . valoresNumericos('txtFrete') . ", " .
            "icms_sit_trib = " . valoresSelect('txtICMSSitTrib') . ", " .
            "icms_origem = " . valoresSelect('txtICMSOrigem') . ", " .
            "icms_alig_calc_cred = " . valoresNumericos('txtAliqCalcCred') . ", " .
            "icms_mod_det_st = " . valoresSelect('txtICMSModDetSt') . ", " .
            "icms_red_bc_st = " . valoresNumericos('txtICMSRedBcSt') . ", " .
            "icms_marg_va_st = " . valoresNumericos('txtICMSMargValAdSt') . ", " .
            "icms_aliq_st = " . valoresNumericos('txtICMSAliqSt') . ", " .
            "icms_aliq = " . valoresNumericos('txtICMSAliq') . ", " .
            "icms_mod_det = " . valoresSelect('txtICMSModDet') . ", " .
            "icms_red_bc = " . valoresNumericos('txtICMSRedBc') . ", " .
            "icms_val_deson = " . valoresNumericos('txtICMSValDes') . ", " .
            "icms_mot_deson = " . valoresNumericos('txtICMSMotDes') . ", " .
            "icms_bc_ret_st = " . valoresNumericos('txtICMSBCAnt') . ", " .
            "icms_ret_st = " . valoresNumericos('txtICMSSTAnt') . ", " .
            "icms_fcp = " . valoresNumericos('txtICMSFcp') . "," .            
            "pis_sit_trib = " . valoresSelect('txtPISSitTrib') . ", " .
            "pis_aliq_perc = " . valoresNumericos('txtPISAliqPerc') . ", " .
            "pis_quantidade = " . valoresNumericos('txtPISQtdVend') . ", " .
            "pis_aliq_real = " . valoresNumericos('txtPISAliqReal') . ", " .
            "pis_tipo_calc = " . valoresSelect('txtPISTipoCalc') . ", " .
            "cofins_sit_trib = " . valoresSelect('txtCOFINSSitTrib') . ", " .
            "cofins_aliq_perc = " . valoresNumericos('txtCOFINSAliqPerc') . ", " .
            "cofins_aliq_real = " . valoresNumericos('txtCOFINSAliqReal') . ", " .
            "cofins_quantidade = " . valoresNumericos('txtCOFINSQtdVend') . ", " .
            "cofins_tipo_calc = " . valoresSelect('txtCOFINSTipoCalc') . " where cod_nota = " . $idNfe . " and cod_prod = " . $idProduto;
    odbc_exec($con, $query) or die(odbc_errormsg());
}

if ($_GET['id'] != '') {
    $idNfe = $_GET['id'];
    $idProduto = $_GET['produto'];

    $cur = odbc_exec($con, "select * from sf_nfe_notas_itens where cod_nota = " . $idNfe . " and cod_prod = " . $idProduto);
    while ($RFP = odbc_fetch_array($cur)) {

        $baseICMSbruto = $RFP['tot_val_bruto'];
        $credicmsapro = $baseICMSbruto * $RFP['icms_alig_calc_cred'] / 100;
        $baseIcmsST = ($baseICMSbruto + 0) * ((100 + $RFP['icms_marg_va_st']) / 100);
        $icmsvalst = $baseIcmsST * $RFP['icms_aliq_st'] / 100;
        $icmsvalbcant = $baseIcmsST * $RFP['icms_aliq_st'] / 100;
        $baseICMSLiquido = ($RFP['tot_val_bruto'] + $RFP['tot_frete'] + $RFP['tot_outras_despesas']) - $RFP['tot_desconto'];
        $icmsval = $baseICMSLiquido * $RFP['icms_aliq'] / 100;

        $id = $RFP['cod_nota'];
        $desconto = escreverNumero($RFP['tot_desconto']);
        $outrasDespesas = escreverNumero($RFP['tot_outras_despesas']);
        $seguro = escreverNumero($RFP['tot_seguro']);
        $frete = escreverNumero($RFP['tot_frete']);
        $ICMSSitTrib = utf8_encode($RFP['icms_sit_trib']);
        $ICMSOrigem = utf8_encode($RFP['icms_origem']);
        //$ICMSRegime = utf8_encode($RFP['ICMSRegime']);
        $ICMSModDet = utf8_encode($RFP['icms_mod_det']);
        $ICMSModDetSt = utf8_encode($RFP['icms_mod_det_st']);
        $aliqcalccred = escreverNumero($RFP['icms_alig_calc_cred']);
        $credicmsapro = escreverNumero($credicmsapro);
        $icmsredbc = escreverNumero($RFP['icms_red_bc']);
        $icmsredbcst = escreverNumero($RFP['icms_red_bc_st']);
        $icmsvalbc = escreverNumero($baseICMSLiquido); // Calcular BC ICM escreverNumero($RFP['ICMSValBc']);
        $icmsmargvaladst = escreverNumero($RFP['icms_marg_va_st']);
        $icmsaliq = escreverNumero($RFP['icms_aliq']);
        $icmsvalbcst = escreverNumero($baseIcmsST);
        $icmsval = escreverNumero($icmsval); // Calcular escreverNumero($RFP['ICMSVal']);
        $icmsaliqst = escreverNumero($RFP['icms_aliq_st']);
        $icmsvalst = escreverNumero($icmsvalst);
        $IPISitTrib = ""; // LUGAR NENHUM utf8_encode($RFP['txtIPISitTrib']);
        $ipiclassenq = ""; // LUGAR NENHUM utf8_encode($RFP['IPIClasEnq']);
        $ipicodenq = ""; // LUGAR NENHUM  utf8_encode($RFP['IPICodEnq']);
        $ipicnpjprod = ""; // LUGAR NENHUM  utf8_encode($RFP['IPICnpjProd']);
        $ipicodselocont = ""; // LUGAR NENHUM  utf8_encode($RFP['IPICodSeloCont']);
        $ipiqtdselocont = ""; // LUGAR NENHUM  utf8_encode($RFP['IPIQtdSeloCont']);
        $IPITipoCalc = ""; // LUGAR NENHUM  utf8_encode($RFP['IPITipoCalc']);
        $ipivalbasecalc = ""; // LUGAR NENHUM  escreverNumero($RFP['IPIValBaseCalc']);
        $ipialiq = ""; // LUGAR NENHUM escreverNumero($RFP['IPIAliq']);
        $ipiqtdtotundpad = ""; // LUGAR NENHUM escreverNumero($RFP['IPIQtdTotUndPad']);
        $ipivalunid = ""; // LUGAR NENHUM escreverNumero($RFP['IPIValUnid']);
        $ipivalor = ""; // LUGAR NENHUM escreverNumero($RFP['IPIValor']);
        $PISSitTrib = utf8_encode($RFP['pis_sit_trib']);
        $PISTipoCalc = utf8_encode($RFP['pis_tipo_calc']);
        $pisvalbasecalc = ""; // Calcular  escreverNumero($RFP['PISValBaseCalc']);
        $pisaliqperc = escreverNumero($RFP['pis_aliq_perc']);
        $pisaliqreal = escreverNumero($RFP['pis_aliq_real']);
        $pisqtdvend = escreverNumero($RFP['pis_quantidade']);
        $pisvalor = ""; // Calcular escreverNumero($RFP['PISValor']);
        $PISTipoCalcST = ""; // LUGAR NENHUM utf8_encode($RFP['PISTipoCalcST']);
        $PISValBaseCalcST = ""; // LUGAR NENHUM escreverNumero($RFP['PISValBaseCalcST']);
        $PISAliqPercST = ""; // LUGAR NENHUM escreverNumero($RFP['PISAliqPercST']);
        $PISAliqRealST = ""; // LUGAR NENHUM escreverNumero($RFP['PISAliqRealST']);
        $PISQtdVendST = ""; // LUGAR NENHUM escreverNumero($RFP['PISQtdVendST']);
        $PISValorST = ""; // LUGAR NENHUM escreverNumero($RFP['PISValorST']);
        $COFINSSitTrib = utf8_encode($RFP['cofins_sit_trib']);
        $COFINSTipoCalc = utf8_encode($RFP['cofins_tipo_calc']);
        $cofinsvalbasecalc = ""; // Calcular escreverNumero($RFP['COFINSValBaseCalc']);
        $cofinsaliqperc = escreverNumero($RFP['cofins_aliq_perc']);
        $cofinsaliqreal = escreverNumero($RFP['cofins_aliq_real']);
        $cofinsqtdvend = escreverNumero($RFP['cofins_quantidade']);
        $cofinsvalor = ""; // Calcular escreverNumero($RFP['COFINSValor']);
        $COFINSTipoCalcST = ""; // LUGAR NENHUM  utf8_encode($RFP['COFINSTipoCalcST']);
        $cofinsvalbasecalcst = ""; // LUGAR NENHUM  escreverNumero($RFP['COFINSValBaseCalcST']);
        $cofinsaliqpercst = ""; // LUGAR NENHUM  escreverNumero($RFP['COFINSAliqPercST']);
        $cofinsaliqrealst = ""; // LUGAR NENHUM  escreverNumero($RFP['COFINSAliqRealST']);
        $cofinsqtdvendst = ""; // LUGAR NENHUM  escreverNumero($RFP['COFINSQtdVendST']);
        $cofinsvalorst = ""; // LUGAR NENHUM  escreverNumero($RFP['COFINSValorST']);
        $icmsvalbcant = escreverNumero($RFP['icms_bc_ret_st']);
        $icmsvalstant = escreverNumero($RFP['icms_ret_st']);
        //$icmsoperprop = escreverNumero($RFP['ICMSOperProp']);
        //$icmsufst = utf8_encode($RFP['ICMSUFSt']);
        $icmsvaldes = escreverNumero($RFP['icms_val_deson']);
        $icmsvalmot = utf8_encode($RFP['icms_mot_deson']);
        //$icmsbcstretido = escreverNumero($RFP['ICMSBcStRetido']);
        //$icmsstretido = escreverNumero($RFP['ICMSStRetido']);
        //$icmsbcstdestino = escreverNumero($RFP['ICMSBcStDestino']);
        //$icmsstdestino = escreverNumero($RFP['ICMSStDestino']);
        $icmsfcp = escreverNumero($RFP['icms_fcp']);
    }
} else {
    $disabled = '';
    $id = '';
    $nome = '';
    $modaldet = '';
    //$ICMSRegime = '';
    $ICMSSitTrib = '';
    $ICMSOrigem = '';
    $ICMSModDet = '';
    $ICMSModDetSt = '';
    $aliqcalccred = escreverNumero(0);
    $credicmsapro = escreverNumero(0);
    $icmsredbc = escreverNumero(0);
    $icmsredbcst = escreverNumero(0);
    $icmsvalbc = escreverNumero(0);
    $icmsmargvaladst = escreverNumero(0);
    $icmsaliq = escreverNumero(0);
    $icmsvalbcst = escreverNumero(0);
    $icmsval = escreverNumero(0);
    $icmsaliqst = escreverNumero(0);
    $icmsvalst = escreverNumero(0);
    $IPISitTrib = '';
    $ipiclassenq = '';
    $ipicodenq = '';
    $ipicnpjprod = '';
    $ipicodselocont = '';
    $ipiqtdselocont = '';
    $IPITipoCalc = '';
    $ipivalbasecalc = escreverNumero(0);
    $ipialiq = escreverNumero(0);
    $ipiqtdtotundpad = escreverNumero(0);
    $ipivalunid = escreverNumero(0);
    $ipivalor = escreverNumero(0);
    $PISSitTrib = '';
    $PISTipoCalc = '';
    $pisvalbasecalc = escreverNumero(0);
    $pisaliqperc = escreverNumero(0);
    $pisaliqreal = escreverNumero(0);
    $pisqtdvend = escreverNumero(0);
    $pisvalor = escreverNumero(0);
    $PISTipoCalcST = '';
    $PISValBaseCalcST = escreverNumero(0);
    $PISAliqPercST = escreverNumero(0);
    $PISAliqRealST = escreverNumero(0);
    $PISQtdVendST = escreverNumero(0);
    $PISValorST = escreverNumero(0);
    $COFINSSitTrib = '';
    $COFINSTipoCalc = '';
    $cofinsvalbasecalc = escreverNumero(0);
    $cofinsaliqperc = escreverNumero(0);
    $cofinsaliqreal = escreverNumero(0);
    $cofinsqtdvend = escreverNumero(0);
    $cofinsvalor = escreverNumero(0);
    $COFINSTipoCalcST = '';
    $cofinsvalbasecalcst = escreverNumero(0);
    $cofinsaliqpercst = escreverNumero(0);
    $cofinsaliqrealst = escreverNumero(0);
    $cofinsqtdvendst = escreverNumero(0);
    $cofinsvalorst = escreverNumero(0);
    $icmsvalstant = escreverNumero(0);
    $icmsvalbcant = escreverNumero(0);
    //$icmsoperprop = escreverNumero(0);
    //$icmsufst = "";
    $icmsvaldes = escreverNumero(0);
    $icmsvalmot = "";
    //$icmsbcstretido = escreverNumero(0);
    //$icmsstretido = escreverNumero(0);
    //$icmsbcstdestino = escreverNumero(0);
    //$icmsstdestino =  escreverNumero(0);
    $icmsfcp = escreverNumero(0);
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}