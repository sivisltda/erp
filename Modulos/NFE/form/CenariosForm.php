<?php

$tpCenario = "A";
$tpTela = "";

if (is_numeric($_GET["IdProduto"])) {
    $finalUrl .= "?idProduto=" . $_GET["IdProduto"];
    $idProduto = $_GET["IdProduto"];
}

if (isset($_GET['tpCenario'])) {
    if ($_GET['tpCenario'] !== "") {
        $tpCenario = $_GET['tpCenario'];
    }
}

if (isset($_POST['bntSave'])) {
    $tpCenario = $_POST['txtTpCenario'];
    if ($_POST['txtId'] != '' && $tpCenario === "A") {
        $query = "update sf_nfe_cenarios set " .
                "nome_cenario = " . valoresTexto('txtNome') . "," .
                "modalidade_cenario = " . valoresTexto('txtModalDet') . "," .
                "ICMSRegime = " . valoresTexto('txtICMSRegime') . "," .
                "ICMSSitTrib = " . valoresSelect('txtICMSSitTrib') . "," .
                "ICMSOrigem = " . valoresSelect('txtICMSOrigem') . "," .
                "AliqCalcCred = " . valoresNumericos('txtAliqCalcCred') . "," .
                "ICMSModDet = " . valoresSelect('txtICMSModDet') . "," .
                "ICMSModDetSt = " . valoresSelect('txtICMSModDetSt') . "," .
                "ICMSRedBc = " . valoresNumericos('txtICMSRedBc') . "," .
                "ICMSRedBcSt = " . valoresNumericos('txtICMSRedBcSt') . "," .
                "ICMSMargValAdSt = " . valoresNumericos('txtICMSMargValAdSt') . "," .
                "ICMSAliq = " . valoresNumericos('txtICMSAliq') . "," .
                "ICMSAliqSt = " . valoresNumericos('txtICMSAliqSt') . "," .
                "IPISitTrib = " . valoresSelect('txtIPISitTrib') . "," .
                "IPIClasEnq = " . valoresTexto('txtIPIClasEnq') . "," .
                "IPICodEnq = " . valoresTexto('txtIPICodEnq') . "," .
                "IPICnpjProd = " . valoresTexto('txtIPICnpjProd') . "," .
                "IPICodSeloCont = " . valoresTexto('txtIPICodSeloCont') . "," .
                "IPIQtdSeloCont = " . valoresNumericos('txtIPIQtdSeloCont') . "," .
                "IPITipoCalc = " . valoresSelect('txtIPITipoCalc') . "," .
                "IPIAliq = " . valoresNumericos('txtIPIAliq') . "," .
                "IPIQtdTotUndPad = " . valoresNumericos('txtIPIQtdTotUndPad') . "," .
                "IPIValUnid = " . valoresNumericos('txtIPIValUnid') . "," .
                "IPIValor = " . valoresNumericos('txtIPIValor') . "," .
                "PISSitTrib = " . valoresSelect('txtPISSitTrib') . "," .
                "PISTipoCalc = " . valoresSelect('txtPISTipoCalc') . "," .
                "PISAliqPerc = " . valoresNumericos('txtPISAliqPerc') . "," .
                "PISAliqReal = " . valoresNumericos('txtPISAliqReal') . "," .
                "PISQtdVend = " . valoresNumericos('txtPISQtdVend') . "," .
                "PISTipoCalcST = " . valoresSelect('txtPISTipoCalcST') . "," .
                "PISAliqPercST = " . valoresNumericos('txtPISAliqPercST') . "," .
                "PISAliqRealST = " . valoresNumericos('txtPISAliqRealST') . "," .
                "PISQtdVendST = " . valoresNumericos('txtPISQtdVendST') . "," .
                "COFINSSitTrib = " . valoresSelect('txtCOFINSSitTrib') . "," .
                "COFINSTipoCalc = " . valoresSelect('txtCOFINSTipoCalc') . "," .
                "COFINSAliqPerc = " . valoresNumericos('txtCOFINSAliqPerc') . "," .
                "COFINSAliqReal = " . valoresNumericos('txtCOFINSAliqReal') . "," .
                "COFINSQtdVend = " . valoresNumericos('txtCOFINSQtdVend') . "," .
                "COFINSTipoCalcST = " . valoresSelect('txtCOFINSTipoCalcST') . "," .
                "COFINSAliqPercST = " . valoresNumericos('txtCOFINSAliqPercST') . "," .
                "COFINSAliqRealST = " . valoresNumericos('txtCOFINSAliqRealST') . "," .
                "COFINSQtdVendST = " . valoresNumericos('txtCOFINSQtdVendST') . "," .
                "ICMSOperProp = " . valoresNumericos('txtBCOpeProp') . "," .
                "ICMSUFSt = " . valoresSelectTexto('txtUfICMS') . "," .
                "ICMSValMot = " . valoresSelect('txtICMSMotDes') . "," .
                "ICMSBcStRetido = " . valoresNumericos('txtBCICMSSTRetido') . "," .
                "ICMSStRetido = " . valoresNumericos('txtICMSSTRetido') . "," .
                "ICMSBcStDestino = " . valoresNumericos('txtBCICMSSTDestino') . "," .
                "ICMSFcp = " . valoresNumericos('txtICMSFcp') . "," .
                "ICMSStDestino = " . valoresNumericos('txtICMSSTDestino') . " where id_cenario = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
    } else {
        $idProduto = $_POST['txtIdProduto'] == "" ? "NULL" : $_POST['txtIdProduto'];
        $query = "insert into sf_nfe_cenarios(nome_cenario,modalidade_cenario,ICMSRegime,ICMSSitTrib,ICMSOrigem,AliqCalcCred,ICMSModDet,
        ICMSModDetSt,ICMSRedBc,ICMSRedBcSt,ICMSMargValAdSt,ICMSAliq,ICMSAliqSt,IPISitTrib,IPIClasEnq,IPICodEnq,IPICnpjProd,IPICodSeloCont,
        IPIQtdSeloCont,IPITipoCalc,IPIAliq,IPIQtdTotUndPad,IPIValUnid,IPIValor,PISSitTrib,PISTipoCalc,PISAliqPerc,PISAliqReal,PISQtdVend,
        PISTipoCalcST,PISAliqPercST,PISAliqRealST,PISQtdVendST,COFINSSitTrib,COFINSTipoCalc,COFINSAliqPerc,COFINSAliqReal,COFINSQtdVend,
        COFINSTipoCalcST,COFINSAliqPercST,COFINSAliqRealST,COFINSQtdVendST,id_produto,ICMSOperProp,ICMSUFSt,ICMSValMot,ICMSBcStRetido,
        ICMSStRetido,ICMSBcStDestino,ICMSFcp,ICMSStDestino)values(" .
                valoresTexto('txtNome') . "," .
                valoresTexto('txtModalDet') . "," .
                valoresTexto('txtICMSRegime') . "," .
                valoresSelect('txtICMSSitTrib') . "," .
                valoresSelect('txtICMSOrigem') . "," .
                valoresTexto('txtAliqCalcCred') . "," .
                valoresSelect('txtICMSModDet') . "," .
                valoresSelect('txtICMSModDetSt') . "," .
                valoresNumericos('txtICMSRedBc') . "," .
                valoresNumericos('txtICMSRedBcSt') . "," .
                valoresNumericos('txtICMSMargValAdSt') . "," .
                valoresNumericos('txtICMSAliq') . "," .
                valoresNumericos('txtICMSAliqSt') . "," .
                valoresSelect('txtIPISitTrib') . "," .
                valoresTexto('txtIPIClasEnq') . "," .
                valoresTexto('txtIPICodEnq') . "," .
                valoresTexto('txtIPICnpjProd') . "," .
                valoresTexto('txtIPICodSeloCont') . "," .
                valoresNumericos('txtIPIQtdSeloCont') . "," .
                valoresSelect('txtIPITipoCalc') . "," .
                valoresNumericos('txtIPIAliq') . "," .
                valoresNumericos('txtIPIQtdTotUndPad') . "," .
                valoresNumericos('txtIPIValUnid') . "," .
                valoresNumericos('txtIPIValor') . "," .
                valoresSelect('txtPISSitTrib') . "," .
                valoresSelect('txtPISTipoCalc') . "," .
                valoresNumericos('txtPISAliqPerc') . "," .
                valoresNumericos('txtPISAliqReal') . "," .
                valoresNumericos('txtPISQtdVend') . "," .
                valoresSelect('txtPISTipoCalcST') . "," .
                valoresNumericos('txtPISAliqPercST') . "," .
                valoresNumericos('txtPISAliqRealST') . "," .
                valoresNumericos('txtPISQtdVendST') . "," .
                valoresSelect('txtCOFINSSitTrib') . "," .
                valoresSelect('txtCOFINSTipoCalc') . "," .
                valoresNumericos('txtCOFINSAliqPerc') . "," .
                valoresNumericos('txtCOFINSAliqReal') . "," .
                valoresNumericos('txtCOFINSQtdVend') . "," .
                valoresSelect('txtCOFINSTipoCalcST') . "," .
                valoresNumericos('txtCOFINSAliqPercST') . "," .
                valoresNumericos('txtCOFINSAliqRealST') . "," .
                valoresNumericos('txtCOFINSQtdVendST') . "," .
                $idProduto . "," .
                valoresNumericos('txtBCOpeProp') . "," .
                valoresSelect('txtUfICMS') . "," .
                valoresSelect('txtICMSMotDes') . "," .
                valoresNumericos('txtBCICMSSTRetido') . "," .
                valoresNumericos('txtICMSSTRetido') . "," .
                valoresNumericos('txtBCICMSSTDestino') . "," .
                valoresNumericos('txtICMSFcp') . "," .
                valoresNumericos('txtICMSSTDestino') . ")";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_cenario from sf_nfe_cenarios order by id_cenario desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }

    if ($_POST['txtIdProduto'] !== "") {
        $query = "update sf_produtos set cenario = " . $_POST['txtId'] . " where conta_produto = " . $_POST['txtIdProduto'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        echo "<script>parent.FecharBox(3);</script>";
    }
}

if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_nfe_cenarios WHERE id_cenario = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $descrProd = "";
    if ($idProduto !== "" && $idProduto !== "NULL") {
        $cur = odbc_exec($con, "Select descricao from sf_produtos where conta_produto = " . $idProduto);
        while ($RFP = odbc_fetch_array($cur)) {
            $descrProd = " - " . $RFP['descricao'];
        }
        $disabled = "";
    }
    $cur = odbc_exec($con, "select * from sf_nfe_cenarios where id_cenario = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_cenario'];
        $nome = $tpCenario == "A" ? utf8_encode($RFP['nome_cenario']) : substr(utf8_encode($RFP['nome_cenario']) . $descrProd, 0, 50);
        $modaldet = utf8_encode($RFP['modalidade_cenario']);
        $ICMSSitTrib = utf8_encode($RFP['ICMSSitTrib']);
        $ICMSOrigem = utf8_encode($RFP['ICMSOrigem']);
        $ICMSRegime = utf8_encode($RFP['ICMSRegime']);
        $ICMSModDet = utf8_encode($RFP['ICMSModDet']);
        $ICMSModDetSt = utf8_encode($RFP['ICMSModDetSt']);
        $aliqcalccred = escreverNumero($RFP['AliqCalcCred']);
        $icmsredbc = escreverNumero($RFP['ICMSRedBc']);
        $icmsredbcst = escreverNumero($RFP['ICMSRedBcSt']);
        $icmsmargvaladst = escreverNumero($RFP['ICMSMargValAdSt']);
        $icmsaliq = escreverNumero($RFP['ICMSAliq']);
        $icmsaliqst = escreverNumero($RFP['ICMSAliqSt']);
        $IPISitTrib = utf8_encode($RFP['IPISitTrib']);
        $ipiclassenq = utf8_encode($RFP['IPIClasEnq']);
        $ipicodenq = utf8_encode($RFP['IPICodEnq']);
        $ipicnpjprod = utf8_encode($RFP['IPICnpjProd']);
        $ipicodselocont = utf8_encode($RFP['IPICodSeloCont']);
        $ipiqtdselocont = utf8_encode($RFP['IPIQtdSeloCont']);
        $IPITipoCalc = utf8_encode($RFP['IPITipoCalc']);
        $ipialiq = escreverNumero($RFP['IPIAliq']);
        $ipiqtdtotundpad = escreverNumero($RFP['IPIQtdTotUndPad']);
        $ipivalunid = escreverNumero($RFP['IPIValUnid']);
        $ipivalor = escreverNumero($RFP['IPIValor']);
        $PISSitTrib = utf8_encode($RFP['PISSitTrib']);
        $PISTipoCalc = utf8_encode($RFP['PISTipoCalc']);
        $pisaliqperc = escreverNumero($RFP['PISAliqPerc']);
        $pisaliqreal = escreverNumero($RFP['PISAliqReal']);
        $pisqtdvend = escreverNumero($RFP['PISQtdVend']);
        $PISTipoCalcST = utf8_encode($RFP['PISTipoCalcST']);
        $PISAliqPercST = escreverNumero($RFP['PISAliqPercST']);
        $PISAliqRealST = escreverNumero($RFP['PISAliqRealST']);
        $PISQtdVendST = escreverNumero($RFP['PISQtdVendST']);
        $COFINSSitTrib = utf8_encode($RFP['COFINSSitTrib']);
        $COFINSTipoCalc = utf8_encode($RFP['COFINSTipoCalc']);
        $cofinsaliqperc = escreverNumero($RFP['COFINSAliqPerc']);
        $cofinsaliqreal = escreverNumero($RFP['COFINSAliqReal']);
        $cofinsqtdvend = escreverNumero($RFP['COFINSQtdVend']);
        $COFINSTipoCalcST = utf8_encode($RFP['COFINSTipoCalcST']);
        $cofinsaliqpercst = escreverNumero($RFP['COFINSAliqPercST']);
        $cofinsaliqrealst = escreverNumero($RFP['COFINSAliqRealST']);
        $cofinsqtdvendst = escreverNumero($RFP['COFINSQtdVendST']);
        $icmsoperprop = escreverNumero($RFP['ICMSOperProp']);
        $icmsufst = utf8_encode($RFP['ICMSUFSt']);
        $icmsvalmot = utf8_encode($RFP['ICMSValMot']);
        $icmsbcstretido = escreverNumero($RFP['ICMSBcStRetido']);
        $icmsstretido = escreverNumero($RFP['ICMSStRetido']);
        $icmsbcstdestino = escreverNumero($RFP['ICMSBcStDestino']);
        $icmsstdestino = escreverNumero($RFP['ICMSStDestino']);
        $icmsfcp = escreverNumero($RFP['ICMSFcp']);
    }
} else {
    $disabled = '';
    $id = '';
    $nome = '';
    $modaldet = '';
    $ICMSRegime = '';
    $ICMSSitTrib = '';
    $ICMSOrigem = '';
    $ICMSModDet = '';
    $ICMSModDetSt = '';
    $aliqcalccred = escreverNumero(0);
    $icmsredbc = escreverNumero(0);
    $icmsredbcst = escreverNumero(0);
    $icmsmargvaladst = escreverNumero(0);
    $icmsaliq = escreverNumero(0);
    $icmsaliqst = escreverNumero(0);
    $IPISitTrib = '';
    $ipiclassenq = '';
    $ipicodenq = '';
    $ipicnpjprod = '';
    $ipicodselocont = '';
    $ipiqtdselocont = '';
    $IPITipoCalc = '';
    $ipialiq = escreverNumero(0);
    $ipiqtdtotundpad = escreverNumero(0);
    $ipivalunid = escreverNumero(0);
    $ipivalor = escreverNumero(0);
    $PISSitTrib = '';
    $PISTipoCalc = '';
    $pisaliqperc = escreverNumero(0);
    $pisaliqreal = escreverNumero(0);
    $pisqtdvend = escreverNumero(0);
    $PISTipoCalcST = '';
    $PISAliqPercST = escreverNumero(0);
    $PISAliqRealST = escreverNumero(0);
    $PISQtdVendST = escreverNumero(0);
    $COFINSSitTrib = '';
    $COFINSTipoCalc = '';
    $cofinsaliqperc = escreverNumero(0);
    $cofinsaliqreal = escreverNumero(0);
    $cofinsqtdvend = escreverNumero(0);
    $COFINSTipoCalcST = '';
    $cofinsaliqpercst = escreverNumero(0);
    $cofinsaliqrealst = escreverNumero(0);
    $cofinsqtdvendst = escreverNumero(0);
    $icmsoperprop = escreverNumero(0);
    $icmsufst = "";
    $icmsvalmot = "";
    $icmsbcstretido = escreverNumero(0);
    $icmsstretido = escreverNumero(0);
    $icmsbcstdestino = escreverNumero(0);
    $icmsstdestino = escreverNumero(0);
    $icmsfcp = escreverNumero(0);
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}