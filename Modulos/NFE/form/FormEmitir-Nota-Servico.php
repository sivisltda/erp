<?php

include "./../../Connections/configini.php";
$FinalUrl = '';
$active1 = "active";
$disabled = 'disabled';

if (isset($_POST['bntAprov']) && is_numeric($_POST['txtId'])) {
    odbc_exec($con, "update sf_nfe_notas set status = 'Aprovado' where cod_nfe = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
}

if (isset($_POST['bntReprov']) && is_numeric($_POST['txtId'])) {
    odbc_exec($con, "update sf_nfe_notas set status = 'Reprovado' where cod_nfe = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
}

if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        $query = "set dateformat dmy;update sf_nfe_notas set " .
                "status_nfe = '" . utf8_decode('Em Digitação') . "'," .
                "serie = " . valoresNumericos('txtSerie') . "," .
                "nnf = " . valoresNumericos('txtNumNfe') . "," .
                "demis = " . valoresData('txtDtEmissao') . "," .
                "hSaiEmi = " . valoresTexto('txtHrSaiEmi') . "," .
                "tpnf = " . utf8_decode($_POST['txtTipoDoc']) . "," .
                "dSaiEnt = " . valoresData('txtDtSaiEnt') . "," .
                "hSaiEnt = " . valoresTexto('txtHrSaiEnt') . "," .
                "indPag = " . valoresSelect('txtForPgto') . "," .
                "uf = " . utf8_decode($_POST['txtEstadoNFE']) . "," .
                "cmunfg = " . utf8_decode($_POST['txtCidadeNFE']) . "," .
                "contigencia_hora = " . valoresData('txtcontigencia_hora') . "," .
                "contingencia_motivo = '" . utf8_decode($_POST['txtcontigencia_motivo']) . "'," .
                "codigo_emitente = " . utf8_decode($_POST['txtEmitente']) . "," .
                "codigo_destinatario = " . utf8_decode($_POST['txtFonecedor']) . "," .
                "cod_transp = " . valoresSelect('txtTranNome') . "," .
                "placa_veiculo = '" . utf8_decode($_POST['txtTranPlaca']) . "'," .
                "estado_veiculo = " . valoresSelect('txtTranEst') . "," .
                "reboque_veiculo = '" . utf8_decode($_POST['txtRebNome']) . "'," .
                "reboque_veiculo_placa = '" . utf8_decode($_POST['txtRebPlaca']) . "'," .
                "reboque_veiculo_estado = " . valoresSelect('txtRebEst') . "," .
                "tipo_volume = '" . utf8_decode($_POST['txtTipoVol']) . "'," .
                "volume_transp = " . valoresNumericos('txtQntVol') . "," .
                "peso_liq_transp = " . valoresNumericos('txtPesoLiq') . "," .
                "peso_bruto_transp = " . valoresNumericos('txtPesoBru') . "," .
                "endereco_entrega = " . valoresSelect('txtEntrega') . "," .
                "info_add_fisco = '" . utf8_decode($_POST['txtInfoFis']) . "'," .
                "info_add_complementar = '" . utf8_decode($_POST['txtInfoCon']) . "'," .
                "destinatario = '" . utf8_decode($_POST['txtDestinatario']) . "'," .
                "descontop = " . valoresNumericos("txtProdDesconto") . "," .
                "descontotp = " . utf8_decode($_POST['txtProdSinal']) . "," .
                "msg_comp = " . valoresSelect('optInfoCon') . "," .
                "id_venda_nfe = " . valoresSelect('txtVenda') . "," .
                "msg_comp_fisco = " . valoresSelect('optInfoFis') . " " .
                "where cod_nfe = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        //------------------Itens-------------------------------------------------------------------
        $notIn = '0';
        if (isset($_POST['txtIdTP'])) {
            $maxP = $_POST['txtIdTP'];
            for ($i = 0; $i <= $maxP; $i++) {
                if (isset($_POST['txtIP_' . $i])) {
                    if (is_numeric($_POST['txtIP_' . $i])) {
                        $notIn = $notIn . "," . $_POST['txtIP_' . $i];
                    }
                }
            }
            odbc_exec($con, "delete from sf_nfe_notas_itens where cod_nota = " . $_POST['txtId'] . " and cod_item_nota not in (" . $notIn . ")");
            $maxP = $_POST['txtIdTP'];
            $freteParc = 0;
            $descontoParc = 0;
            if ($maxP > 0) {
                $descontoParc = valoresNumericos('txtTotDesconto') / $maxP;
            }
            for ($i = 0; $i <= $maxP; $i++) {
                if (isset($_POST['txtIP_' . $i])) {
                    if (is_numeric($_POST['txtIP_' . $i])) {
                        odbc_exec($con, "update sf_nfe_notas_itens set cod_prod = " . $_POST['txtPD_' . $i] .
                                        ",qtd_comercial = " . valoresNumericos('txtVQ_' . $i) .
                                        ",qtd_trib = " . valoresNumericos('txtVQ_' . $i) .
                                        ",tot_val_bruto = " . valoresNumericos('txtVP_' . $i) .
                                        ",cfop = '" . utf8_decode($_POST['txtVCfop_' . $i]) . "'" .
                                        ",tot_frete = " . $freteParc .
                                        ",tot_desconto = " . $descontoParc .
                                        " where cod_item_nota = " . $_POST['txtIP_' . $i]) or die(odbc_errormsg());
                    } else {
                        $query = "INSERT INTO sf_nfe_notas_itens (
                        cod_nota,cod_prod,qtd_comercial,tot_val_bruto,
                        cfop,qtd_trib,tot_seguro,tot_desconto,tot_frete,tot_outras_despesas,
                        icms_sit_trib,icms_origem,pis_sit_trib,pis_quantidade,cofins_sit_trib,cofins_quantidade,info_add, 
                        icms_alig_calc_cred, icms_val_deson, icms_red_bc,icms_aliq,icms_mod_det_st,icms_marg_va_st,icms_red_bc_st,icms_aliq_st,
                        pis_aliq_perc,pis_aliq_real,pis_tipo_calc,cofins_aliq_perc,cofins_aliq_real,cofins_tipo_calc,icms_mot_deson)
                        select " . $_POST['txtId'] . "," . $_POST['txtPD_' . $i] . "," . valoresNumericos('txtVQ_' . $i) . "," . valoresNumericos('txtVP_' . $i) . ",'" .
                        utf8_decode($_POST['txtVCfop_' . $i]) . "'," . valoresNumericos('txtVQ_' . $i) . ",0," . $descontoParc . "," . $freteParc . ",0,
                        isnull(ICMSSitTrib,0), isnull(ICMSOrigem,0), isnull(PISSitTrib,0), isnull(PISQtdVend,0), isnull(COFINSSitTrib,0), isnull(COFINSQtdVend,0), '',
                        AliqCalcCred, ICMSValDes, ICMSRedBc, ICMSAliq, ICMSModDetSt, ICMSMargValAdSt, ICMSRedBcSt, ICMSAliqSt,
                        PISAliqPerc, PISAliqReal, PISTipoCalc, COFINSAliqPerc, COFINSAliqReal, PISTipoCalc, ICMSValMot
                        from sf_produtos A left join sf_nfe_cenarios B on A.cenario = B.id_cenario
                        where conta_produto = " . $_POST['txtPD_' . $i];
                        odbc_exec($con, $query) or die(odbc_errormsg());
                    }
                }
            }
        }
    } else {
        $query = "set dateformat dmy;INSERT INTO sf_nfe_notas(status_nfe,code,versao_xml,mod,serie,nnf,demis,cnf,cdv,tpnf,dSaiEnt
            ,hSaiEnt,tpemis,indPag,finNFe,tpImp,natop,uf,cmunfg,contigencia_hora,contingencia_motivo,tpAmb,procEmi,verProc
            ,codigo_emitente,codigo_destinatario,impresso,exportado,mod_transp,cod_transp,placa_veiculo,estado_veiculo,reboque_veiculo,reboque_veiculo_placa,reboque_veiculo_estado,tipo_volume,volume_transp
            ,peso_liq_transp,peso_bruto_transp,endereco_entrega,info_add_fisco,info_add_complementar,xml,dhRecbto,nProt,destinatario,status,msg_comp,msg_comp_fisco,descontop,descontotp,id_venda_nfe,hSaiEmi,conFinal,destOper,tipoAten) VALUES('" . utf8_decode('Em Digitação') . "','" .
                $chave . "','1.0'," . valoresNumericos('txtModelo') . "," .
                valoresNumericos('txtSerie') . "," .
                valoresNumericos('txtNumNfe') . "," .
                valoresData('txtDtEmissao') . ",0,'" . $dv . "','" .
                utf8_decode($_POST['txtTipoDoc']) . "'," .
                valoresData('txtDtSaiEnt') . "," .
                valoresTexto('txtHrSaiEnt') . ",0," .
                valoresSelect('txtForPgto') . ",0,0,'" . utf8_decode("SERVIÇO") . "'," .
                utf8_decode($_POST['txtEstadoNFE']) . "," .
                utf8_decode($_POST['txtCidadeNFE']) . "," .
                valoresData('txtcontigencia_hora') . ",'" .
                utf8_decode($_POST['txtcontigencia_motivo']) . "',1,3,'1.0.0'," .
                utf8_decode($_POST['txtEmitente']) . "," .
                utf8_decode($_POST['txtFonecedor']) . ",0,0,0," .
                valoresSelect('txtTranNome') . ",'" .
                utf8_decode($_POST['txtTranPlaca']) . "'," .
                valoresSelect('txtTranEst') . ",'" .
                utf8_decode($_POST['txtRebNome']) . "','" .
                utf8_decode($_POST['txtRebPlaca']) . "'," .
                valoresSelect('txtRebEst') . ",'" .
                utf8_decode($_POST['txtTipoVol']) . "'," .
                valoresNumericos('txtQntVol') . "," .
                valoresNumericos('txtPesoLiq') . "," .
                valoresNumericos('txtPesoBru') . "," .
                valoresSelect('txtEntrega') . ",'" .
                utf8_decode($_POST['txtInfoFis']) . "','" .
                utf8_decode($_POST['txtInfoCon']) . "'," .
                "null,null,null,'" . utf8_decode($_POST['txtDestinatario']) . "','Aguarda'," .
                valoresSelect('optInfoCon') . "," . valoresSelect('optInfoFis') . "," .
                valoresNumericos("txtProdDesconto") . "," .
                utf8_decode($_POST['txtProdSinal']) . "," .
                valoresSelect('txtVenda') . "," .
                valoresTexto('txtHrSaiEmi') . ",0,0,0)";
        //echo $query; exit;
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 cod_nfe from sf_nfe_notas order by cod_nfe desc") or die(odbc_errormsg());
        $nn = odbc_result($res, 1);
        //------------------Itens-------------------------------------------------------------------
        $notIn = '0';
        if (isset($_POST['txtIdTP'])) {
            $maxP = $_POST['txtIdTP'];
            $freteParc = 0;
            $descontoParc = 0;
            if ($maxP > 0) {
                $descontoParc = valoresNumericos('txtTotDesconto') / $maxP;
            }
            for ($i = 0; $i <= $maxP; $i++) {
                if (isset($_POST['txtIP_' . $i])) {
                    $query = "INSERT INTO sf_nfe_notas_itens (
                    cod_nota,cod_prod,qtd_comercial,tot_val_bruto,
                    cfop,qtd_trib,tot_seguro,tot_desconto,tot_frete,tot_outras_despesas,
                    icms_sit_trib,icms_origem,pis_sit_trib,pis_quantidade,cofins_sit_trib,cofins_quantidade,info_add, 
                    icms_alig_calc_cred, icms_val_deson, icms_red_bc,icms_aliq,icms_mod_det_st,icms_marg_va_st,icms_red_bc_st,icms_aliq_st,
                    pis_aliq_perc,pis_aliq_real,pis_tipo_calc,cofins_aliq_perc,cofins_aliq_real,cofins_tipo_calc,icms_mot_deson)
                    select " . $nn . "," . $_POST['txtPD_' . $i] . "," . valoresNumericos('txtVQ_' . $i) . "," . valoresNumericos('txtVP_' . $i) . ",'" .
                    utf8_decode($_POST['txtVCfop_' . $i]) . "'," . valoresNumericos('txtVQ_' . $i) . ",0," . $descontoParc . "," . $freteParc . ",0,
                    isnull(ICMSSitTrib,0), isnull(ICMSOrigem,0), isnull(PISSitTrib,0), isnull(PISQtdVend,0), isnull(COFINSSitTrib,0), isnull(COFINSQtdVend,0), '',
                    AliqCalcCred, ICMSValDes, ICMSRedBc, ICMSAliq, ICMSModDetSt, ICMSMargValAdSt, ICMSRedBcSt, ICMSAliqSt,
                    PISAliqPerc, PISAliqReal, PISTipoCalc, COFINSAliqPerc, COFINSAliqReal, PISTipoCalc, ICMSValMot
                    from sf_produtos A left join sf_nfe_cenarios B on A.cenario = B.id_cenario
                    where conta_produto = " . $_POST['txtPD_' . $i];
                    odbc_exec($con, $query) or die(odbc_errormsg());
                }
            }
        }
        $_POST['txtId'] = $nn;
        echo "<script>window.top.location.href = 'FormEmitir-Nota-Servico.php?id=" . $nn . "';</script>";
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_nfe_notas_itens WHERE cod_nota = " . $_POST['txtId']);
        odbc_exec($con, "DELETE FROM sf_nfe_notas WHERE cod_nfe = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso');window.top.location.href = 'Emitir-Nota-Servico.php';</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_nfe_notas left join sf_fornecedores_despesas on codigo_emitente = id_fornecedores_despesas where cod_nfe =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['cod_nfe'];
        $status_nfe = utf8_encode($RFP['status_nfe']); //[varchar](20) NOT NULL,           
        $chave_nfe = utf8_encode($RFP['code']); //[varchar](128) NOT NULL,           
        $vxml_nfe = utf8_encode($RFP['versao_xml']); //[varchar](10) NOT NULL,                           
        $modelo_nfe = utf8_encode($RFP['mod']); //[int] NOT NULL,           
        $serie_nfe = str_pad(utf8_encode($RFP['serie']), 3, "0", STR_PAD_LEFT); //[int] NOT NULL,
        $numero_nfe = str_pad(utf8_encode($RFP['nnf']), 9, "0", STR_PAD_LEFT); //[int] NOT NULL,
        $nprot = utf8_encode($RFP['nProt']); //[int] NOT NULL,
        if ($RFP['demis'] != "") {
            $data_emissao = escreverData($RFP['demis']); //[datetime] NOT NULL,
            $data_emissaoX = escreverData($RFP['demis'], 'Y-m-d'); //[datetime] NOT NULL,
            $data_emissao_pasta = escreverData($RFP['demis'], 'Ym'); //[datetime] NOT NULL,
        }
        if ($RFP['hSaiEmi'] != "") {
            $hora_saiemi = escreverData($RFP['hSaiEmi'], 'H:i:s'); //[datetime] NULL,                       
        } else {
            $hora_saiemi = "00:00:00";
        }
        $cod_numerico = utf8_encode($RFP['cnf']); //[varchar](10) NOT NULL,
        $dv = utf8_encode($RFP['cdv']); //[int] NOT NULL,                        
        $tipo_doc = utf8_encode($RFP['tpnf']); //[int] NOT NULL,  
        if ($RFP['dSaiEnt'] != "") {
            $data_saient = escreverData($RFP['dSaiEnt']); //[datetime] NOT NULL,
            $data_saientX = escreverData($RFP['dSaiEnt'], 'Y-m-d'); //[datetime] NOT NULL,
        }
        if ($RFP['hSaiEnt'] != "") {
            $hora_saient = escreverData($RFP['hSaiEnt'], 'H:i:s'); //[datetime] NULL,                       
        } else {
            $hora_saient = "00:00:00";
        }
        $for_emis = utf8_encode($RFP['tpemis']); //[int] NULL,
        $for_pgto = utf8_encode($RFP['indPag']); //[int] NOT NULL,           
        $fin_emis = utf8_encode($RFP['finNFe']); //[int] NOT NULL,           
        $tipo_print = utf8_encode($RFP['tpImp']); //[int] NOT NULL,                        
        $nat_operacao = utf8_encode($RFP['natop']); //[varchar](128) NOT NULL,            
        $estadoNFE = utf8_encode($RFP['uf']); //[varchar](20) NOT NULL,
        $cidadeNFE = utf8_encode($RFP['cmunfg']); //[varchar](20) NOT NULL,            
        $contigencia_hora = utf8_encode($RFP['contigencia_hora']); //[datetime] NULL,
        $contigencia_motivo = utf8_encode($RFP['contingencia_motivo']); //[varchar](512) NULL,
        $ambiente = utf8_encode($RFP['tpAmb']); //[int] NOT NULL,
        $processo = utf8_encode($RFP['procEmi']); //[int] NOT NULL,
        $versaoProc = utf8_encode($RFP['verProc']); //[varchar](10) NOT NULL,
        $Cod_Emitente = utf8_encode($RFP['codigo_emitente']); //[int] NOT NULL,
        $Cod_Destinatario = utf8_encode($RFP['codigo_destinatario']); //[int] NOT NULL,
        $Mod_Transp = utf8_encode($RFP['mod_transp']); //[int] NOT NULL,
        $Cod_Transp = utf8_encode($RFP['cod_transp']); //[int] NULL,
        $placa_veiculo = utf8_encode($RFP['placa_veiculo']);
        $trans_estado = utf8_encode($RFP['estado_veiculo']);
        $reboque = utf8_encode($RFP['reboque_veiculo']);
        $placa_veiculo_reboque = utf8_encode($RFP['reboque_veiculo_placa']);
        $reboq_estado = utf8_encode($RFP['reboque_veiculo_estado']);
        $tipoVol = utf8_encode($RFP['tipo_volume']);
        $Volume_Transp = utf8_encode($RFP['volume_transp']);
        $Peso_liq_Transp = utf8_encode($RFP['peso_liq_transp']); //[decimal](12, 3) NULL,
        $Peso_bru_Transp = utf8_encode($RFP['peso_bruto_transp']); //[decimal](12, 3) NULL,
        $end_entrega = utf8_encode($RFP['endereco_entrega']);
        $Info_add_fisco = utf8_encode($RFP['info_add_fisco']); //[varchar](1024) NULL,
        $Info_add_complementar = utf8_encode($RFP['info_add_complementar']); //[varchar](1024) NULL, 
        $destinatario = utf8_encode($RFP['destinatario']);
        $status = utf8_encode($RFP['status']);
        $msg_comp = utf8_encode($RFP['msg_comp']);
        $msg_comp_fisco = utf8_encode($RFP['msg_comp_fisco']);
        $descontop = escreverNumero($RFP['descontop']);
        $descontotp = utf8_encode($RFP['descontotp']);
        $conFinal = utf8_encode($RFP['conFinal']);
        $destOper = utf8_encode($RFP['destOper']);
        $tipoAten = utf8_encode($RFP['tipoAten']);
        $Aliquota = $RFP['comiss_gere'];
        $TotNota = escreverNumero(0);
        $TotDesconto = escreverNumero(0);
        $BaseCalc = escreverNumero(0);
        $TotImposto = escreverNumero(0);
        $TotInss = escreverNumero(0);
        $TotIr = escreverNumero(0);
        $TotCsll = escreverNumero(0);
        $Pis = escreverNumero(0);
        $Cofins = escreverNumero(0);
        $OutDespesas = escreverNumero(0);
        $TotFed = escreverNumero(0);
        $TotEst = escreverNumero(0);
        $TotMun = escreverNumero(0);
        $linkDownloadPDF = utf8_encode($RFP['link_pdf']);
        $linkDownloadXML = utf8_encode($RFP['link_xml']);
        $idVenda = utf8_encode($RFP['id_venda_nfe']);
    }
} else {
    $disabled = '';
    $id = '';
    $status_nfe = 'Em Digitação';
    $chave_nfe = '';
    $numero_nfe = '000000001';
    $nprot = "";
    $cur = odbc_exec($con, "select isnull(max(nnf),0) + 1 nnf from sf_nfe_notas where mod = 0");
    while ($RFP = odbc_fetch_array($cur)) {
        $numero_nfe = str_pad(utf8_encode($RFP['nnf']), 9, "0", STR_PAD_LEFT);
    }
    $cur = odbc_exec($con, "select * from sf_fornecedores_despesas where tipo in ('M') order by razao_social") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $Cod_Emitente = $RFP['id_fornecedores_despesas'];
        $Aliquota = $RFP['comiss_gere'];
        $estadoNFE = $RFP['estado'];
        $cidadeNFE = $RFP['cidade'];
    }
    $vxml_nfe = '1.0';
    $modelo_nfe = '00';
    $serie_nfe = '001';
    $data_emissao = getData("T");
    $data_emissaoX = date("Y-m-d");
    $data_emissao_pasta = date("Ym");
    $hora_saiemi = date("H:i:s");
    $cod_numerico = rand(99999999, 11111111);
    $dv = 0;
    $tipo_doc = '1';
    $data_saient = getData("T");
    $data_saientX = date("Y-m-d");
    $hora_saient = date("H:i:s");
    $for_emis = '1';
    $for_pgto = '1';
    $fin_emis = '1';
    $tipo_print = '1';
    $nat_operacao = '';
    $contigencia_hora = '';
    $contigencia_motivo = '';
    $ambiente = '1';
    $processo = '3';
    $versaoProc = '1.0.0';
    $Cod_Destinatario = '';
    $Mod_Transp = '9';
    $Cod_Transp = 'null';
    $placa_veiculo = '';
    $trans_estado = 'null';
    $reboque = 'N';
    $placa_veiculo_reboque = '';
    $reboq_estado = 'null';
    $tipoVol = 'UN';
    $Volume_Transp = escreverNumero(0);
    $Peso_liq_Transp = escreverNumero(0);
    $Peso_bru_Transp = escreverNumero(0);
    $end_entrega = 'null';
    $Info_add_fisco = '';
    $Info_add_complementar = '';
    $destinatario = '';
    $status = '';
    $estadoNFEX = '';
    $cidadeNFEX = '';
    $msg_comp = '';
    $msg_comp_fisco = '';
    $descontop = 0;
    $descontotp = 0;
    $conFinal = 0;
    $destOper = 1;
    $tipoAten = 0;
    $TotNota = escreverNumero(0);
    $TotDesconto = escreverNumero(0);
    $BaseCalc = escreverNumero(0);
    $TotImposto = escreverNumero(0);
    $TotInss = escreverNumero(0);
    $TotIr = escreverNumero(0);
    $TotCsll = escreverNumero(0);
    $Pis = escreverNumero(0);
    $Cofins = escreverNumero(0);
    $OutDespesas = escreverNumero(0);
    $TotFed = escreverNumero(0);
    $TotEst = escreverNumero(0);
    $TotMun = escreverNumero(0);
    $linkDownloadPDF = "";
    $linkDownloadXML = "";
    $idVenda = "";
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}

if ($_GET['idx'] != '') {
    $FinalUrl = "&idx=" . $_GET['idx'];
}
if ($_GET['tpx'] != '') {
    $FinalUrl = $FinalUrl . "&tpx=" . $_GET['tpx'];
}
if ($_GET['tdx'] != '') {
    $FinalUrl = $FinalUrl . "&tdx=" . $_GET['tdx'];
}
if ($_GET['dti'] != '') {
    $FinalUrl = $FinalUrl . "&dti=" . $_GET['dti'];
}
if ($_GET['dtf'] != '') {
    $FinalUrl = $FinalUrl . "&dtf=" . $_GET['dtf'];
}
if ($_GET['id'] != '') {
    $FinalUrl = "id=" . $_GET['id'] . $FinalUrl;
}

if (isset($_GET['vd']) && is_numeric($_GET['vd'])) {
    $cur = odbc_exec($con, "select * from sf_vendas inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = sf_vendas.cliente_venda 
    left join tb_estados e on f.estado = e.estado_codigo left join tb_cidades c on f.cidade = c.cidade_codigo where id_venda = '" . $_GET['vd'] . "'");
    while ($RFP = odbc_fetch_array($cur)) {
        $idVenda = $_GET['vd'];
        $data_saient = escreverData($RFP['data_venda']);
        $hora_saient = escreverData($RFP['data_venda'], 'H:i:s');
        $nat_operacao = 'VENDA';
        $Cod_Destinatario = $RFP['cliente_venda'];
        $descontop = escreverNumero($RFP['descontop']);
        $descontotp = utf8_encode($RFP['descontotp']);
        $destinatario = utf8_encode($RFP['destinatario']);
        $for_pgto = 1;
        $hora_saiemi = date("H:i:s");
        $conFinal = 0;
        $destOper = 1;
        $tipoAten = 0;
    }
}

$tpAmb = 1;
$cur = odbc_exec($con, "select * from sf_configuracao where id = 1");
while ($RFP = odbc_fetch_array($cur)) {
    $tpAmb = $RFP['Nfs_Ambiente'];
}
