<?php

include "./../../Connections/configini.php";
include './../../util/nfe/vendor/autoload.php';
$FinalUrl = '';
$active1 = "active";
$PATH = "./../../Pessoas/" . $_SESSION["contrato"] . "/NFE/";
$disabled = 'disabled';

use NFePHP\NFe\ToolsNFe;

$filename = './../../Pessoas/' . $contrato . '/Emitentes/*.pfx';
if (is_array(glob($filename))) {
    if (file_exists('./../../Pessoas/' . $contrato . '/Emitentes/config.json')) {
        $nfeTools = new ToolsNFe('./../../Pessoas/' . $contrato . '/Emitentes/config.json');
        $tpAmb = $nfeTools->aConfig['tpAmb'];
    }
}

function modulo_11($num, $base = 9, $r = 0) {
    $soma = 0;
    $fator = 2;
    for ($i = strlen($num); $i > 0; $i--) {
        $numeros[$i] = substr($num, $i - 1, 1);
        $parcial[$i] = $numeros[$i] * $fator;
        $soma += $parcial[$i];
        if ($fator == $base) {
            $fator = 1;
        }
        $fator++;
    }
    if ($r == 0) {
        $soma *= 10;
        $digito = $soma % 11;
        if ($digito == 10) {
            $digito = 0;
        }
        return $digito;
    } elseif ($r == 1) {
        $resto = $soma % 11;
        return $resto;
    }
}

function MontaChaveAcessoNFe($cnpj, $estadoNFEX, $dtEmissao, $Modelo, $serie, $NumeroNF, $CN, $Forma) {
    $char = array(".", "/", "-");
    $clear = array("", "", "");
    $chave = $estadoNFEX . substr($dtEmissao, 9, 2) . substr($dtEmissao, 4, 2) . str_replace($char, $clear, $cnpj) .
            str_pad($Modelo, 2, "0", STR_PAD_LEFT) . str_pad($serie, 3, "0", STR_PAD_LEFT) .
            str_pad($NumeroNF, 9, "0", STR_PAD_LEFT) . $Forma . str_pad($CN, 8, "0", STR_PAD_LEFT);
    return $chave . modulo_11($chave);
}

function CharReplace($nome) {
    $caracteresBR = array('À', 'Á', 'Ã', 'Â', 'à', 'á', 'ã', 'â', 'Ê', 'É', 'Í', 'í', 'Ó', 'Õ', 'Ô', 'ó', 'õ', 'ô', 'Ú', 'Ü', 'Ç', 'ç', 'é', 'ê', 'ú', 'ü');
    $caracteresUS = array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'E', 'E', 'I', 'i', 'O', 'O', 'O', 'o', 'o', 'o', 'U', 'U', 'C', 'c', 'e', 'e', 'u', 'u');
    return str_replace($caracteresBR, $caracteresUS, $nome);
}

function CharClear($nome) {
    $caracteres = array(".", "-", "/", "(", ")", " ");
    return str_replace($caracteres, "", $nome);
}

function CharSplit($nome, $char) {
    $caracteres = explode($char, $nome);
    return $caracteres[0];
}

if (isset($_POST['bntAprov']) && is_numeric($_POST['txtId'])) {
    $res = odbc_exec($con, "select isnull(max(nnf),0) + 1 nnf from sf_nfe_notas where mod > 0") or die(odbc_errormsg());
    $numNFE = odbc_result($res, 1);
    odbc_exec($con, "update sf_nfe_notas set status = 'Aprovado', hSaiEmi = getdate(), nnf = " . $numNFE .
            " where nnf = 0 and cod_nfe = " . $_POST['txtId']);
}

if (isset($_POST['bntReprov']) && is_numeric($_POST['txtId'])) {
    odbc_exec($con, "update sf_nfe_notas set status = 'Reprovado' where cod_nfe = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
}

if (isset($_POST['bntSave'])) {
    $query = "select cnpj,cidade_codigoEstado,estado_gov from sf_fornecedores_despesas f 
    inner join tb_cidades c on f.cidade = c.cidade_codigo 
    inner join tb_estados e on e.estado_codigo = c.cidade_codigoEstado    
    where id_fornecedores_despesas = '" . utf8_decode($_POST['txtEmitente']) . "'";
    $cur = odbc_exec($con, $query) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $cnpj = $RFP['cnpj'];
        $estadoNFEX = $RFP['estado_gov'];
    }
    $chave = MontaChaveAcessoNFe($cnpj, $estadoNFEX, valoresData('txtDtEmissao'), valoresNumericos('txtModelo'), valoresNumericos('txtSerie'), valoresNumericos('txtNumNfe'), utf8_decode($_POST['txtCodNumerico']), utf8_decode($_POST['txtForPgto']));
    $dv = substr($chave, strlen($chave) - 1);
    if ($_POST['txtId'] != '') {
        $query = "set dateformat dmy;update sf_nfe_notas set " .
                "status_nfe = '" . utf8_decode('Em Digitação') . "'," .
                "code = '" . $chave . "'," .
                "mod = " . valoresNumericos('txtModelo') . "," .
                "serie = " . valoresNumericos('txtSerie') . "," .
                "nnf = " . valoresNumericos('txtNumNfe') . "," .
                "demis = " . valoresData('txtDtEmissao') . "," .
                "hSaiEmi = " . valoresTexto('txtHrSaiEmi') . "," .
                "cnf = '" . utf8_decode($_POST['txtCodNumerico']) . "'," .
                "cdv = " . $dv . "," .
                "tpnf = " . utf8_decode($_POST['txtTipoDoc']) . "," .
                "dSaiEnt = " . valoresData('txtDtSaiEnt') . "," .
                "hSaiEnt = " . valoresTexto('txtHrSaiEnt') . "," .
                "tpemis = " . utf8_decode($_POST['txtForEmis']) . "," .
                "indPag = " . utf8_decode($_POST['txtForPgto']) . "," .
                "finNFe = " . utf8_decode($_POST['txtFinEmis']) . "," .
                "tpImp = " . utf8_decode($_POST['txtTipoImp']) . "," .
                "natop = '" . utf8_decode($_POST['txtNtOpera']) . "'," .
                "uf = " . utf8_decode($_POST['txtEstadoNFE']) . "," .
                "cmunfg = " . utf8_decode($_POST['txtCidadeNFE']) . "," .
                "contigencia_hora = " . valoresData('txtcontigencia_hora') . "," .
                "contingencia_motivo = '" . utf8_decode($_POST['txtcontigencia_motivo']) . "'," .
                "codigo_emitente = " . utf8_decode($_POST['txtEmitente']) . "," .
                "codigo_destinatario = " . utf8_decode($_POST['txtFonecedor']) . "," .
                "mod_transp = " . utf8_decode($_POST['txtModFrete']) . "," .
                "cod_transp = " . valoresSelect('txtTranNome') . "," .
                "placa_veiculo = '" . utf8_decode($_POST['txtTranPlaca']) . "'," .
                "estado_veiculo = " . valoresSelect('txtTranEst') . "," .
                "reboque_veiculo = '" . utf8_decode($_POST['txtRebNome']) . "'," .
                "reboque_veiculo_placa = '" . utf8_decode($_POST['txtRebPlaca']) . "'," .
                "reboque_veiculo_estado = " . valoresSelect('txtRebEst') . "," .
                "tipo_volume = '" . utf8_decode($_POST['txtTipoVol']) . "'," .
                "volume_transp = " . valoresNumericos('txtQntVol') . "," .
                "peso_liq_transp = " . valoresNumericos('txtPesoLiq') . "," .
                "peso_bruto_transp = " . valoresNumericos('txtPesoBru') . "," .
                "endereco_entrega = " . valoresSelect('txtEntrega') . "," .
                "info_add_fisco = '" . utf8_decode($_POST['txtInfoFis']) . "'," .
                "info_add_complementar = '" . utf8_decode($_POST['txtInfoCon']) . "'," .
                "destinatario = '" . utf8_decode($_POST['txtDestinatario']) . "'," .
                "descontop = " . valoresNumericos('txtProdDesconto') . "," .
                "descontotp = " . utf8_decode($_POST['txtProdSinal']) . "," .
                "msg_comp = " . utf8_decode($_POST['optInfoCon']) . "," .
                "msg_comp_fisco = " . utf8_decode($_POST['optInfoFis']) . "," .
                "conFinal = " . valoresSelect('txtConFinal') . "," .
                "destOper = " . valoresSelect('txtDestOper') . "," .
                "tipoAten = " . valoresSelect('txtTipoAtend') . "," .
                "detPag = " . valoresNumericos('txtDetPgto') .
                " where cod_nfe = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        //------------------Itens-------------------------------------------------------------------
        $notIn = '0';
        if (isset($_POST['txtIdTP'])) {
            $maxP = $_POST['txtIdTP'];
            for ($i = 0; $i <= $maxP; $i++) {
                if (isset($_POST['txtIP_' . $i])) {
                    if (is_numeric($_POST['txtIP_' . $i])) {
                        $notIn = $notIn . "," . $_POST['txtIP_' . $i];
                    }
                }
            }
            odbc_exec($con, "delete from sf_nfe_notas_itens where cod_nota = " . $_POST['txtId'] . " and cod_item_nota not in (" . $notIn . ")");
            $maxP = $_POST['txtIdTP'];
            $freteParc = 0;
            $descontoParc = 0;
            if ($maxP > 0) {
                $freteParc = valoresNumericos('txtTotFrete') / $maxP;
                $descontoParc = valoresNumericos('txtTotDesconto') / $maxP;
            }
            for ($i = 0; $i <= $maxP; $i++) {
                if (isset($_POST['txtIP_' . $i])) {
                    if (is_numeric($_POST['txtIP_' . $i])) {
                        odbc_exec($con, "update sf_nfe_notas_itens set cod_prod = " . $_POST['txtPD_' . $i] .
                                        ",qtd_comercial = " . valoresNumericos('txtVQ_' . $i) .
                                        ",qtd_trib = " . valoresNumericos('txtVQ_' . $i) .
                                        ",tot_val_bruto = " . valoresNumericos('txtVP_' . $i) .
                                        ",cfop = '" . utf8_decode($_POST['txtVCfop_' . $i]) . "'" .
                                        ",tot_frete = " . $freteParc .
                                        ",tot_desconto = " . $descontoParc .
                                        " where cod_item_nota = " . $_POST['txtIP_' . $i]) or die(odbc_errormsg());
                    } else {
                        $query = "INSERT INTO sf_nfe_notas_itens (
                        cod_nota,cod_prod,qtd_comercial,tot_val_bruto,
                        cfop,qtd_trib,tot_seguro,tot_desconto,tot_frete,tot_outras_despesas,
                        icms_sit_trib,icms_origem,pis_sit_trib,pis_quantidade,cofins_sit_trib,cofins_quantidade,info_add, 
                        icms_alig_calc_cred, icms_val_deson, icms_red_bc,icms_aliq,icms_mod_det_st,icms_marg_va_st,icms_red_bc_st,icms_aliq_st,
                        pis_aliq_perc,pis_aliq_real,pis_tipo_calc,cofins_aliq_perc,cofins_aliq_real,cofins_tipo_calc,icms_mot_deson)
                        select " . $_POST['txtId'] . "," . $_POST['txtPD_' . $i] . "," . valoresNumericos('txtVQ_' . $i) . "," . valoresNumericos('txtVP_' . $i) . ",'" .
                                utf8_decode($_POST['txtVCfop_' . $i]) . "'," . valoresNumericos('txtVQ_' . $i) . ",0," . $descontoParc . "," . $freteParc . ",0,
                        isnull(ICMSSitTrib,0), isnull(ICMSOrigem,0), isnull(PISSitTrib,0), isnull(PISQtdVend,0), isnull(COFINSSitTrib,0), isnull(COFINSQtdVend,0), '',
                        AliqCalcCred, ICMSValDes, ICMSRedBc, ICMSAliq, ICMSModDetSt, ICMSMargValAdSt, ICMSRedBcSt, ICMSAliqSt,
                        PISAliqPerc, PISAliqReal, PISTipoCalc, COFINSAliqPerc, COFINSAliqReal, PISTipoCalc, ICMSValMot
                        from sf_produtos A left join sf_nfe_cenarios B on A.cenario = B.id_cenario
                        where conta_produto = " . $_POST['txtPD_' . $i];
                        odbc_exec($con, $query) or die(odbc_errormsg());
                    }
                }
            }
        }
    } else {
        $query = "set dateformat dmy;INSERT INTO sf_nfe_notas(status_nfe,code,versao_xml,mod,serie,nnf,demis,cnf,cdv,tpnf,dSaiEnt
            ,hSaiEnt,tpemis,indPag,finNFe,tpImp,natop,uf,cmunfg,contigencia_hora,contingencia_motivo,tpAmb,procEmi,verProc
            ,codigo_emitente,codigo_destinatario,impresso,exportado,mod_transp,cod_transp,placa_veiculo,estado_veiculo,reboque_veiculo,reboque_veiculo_placa,reboque_veiculo_estado,tipo_volume,volume_transp
            ,peso_liq_transp,peso_bruto_transp,endereco_entrega,info_add_fisco,info_add_complementar,xml,dhRecbto,nProt,destinatario,status,msg_comp,msg_comp_fisco,descontop,descontotp,hSaiEmi,conFinal,destOper,tipoAten,detPag) VALUES ('" . utf8_decode('Em Digitação') . "','" .
                $chave . "','4.00'," . valoresNumericos('txtModelo') . "," .
                valoresNumericos('txtSerie') . "," .
                valoresNumericos('txtNumNfe') . "," .
                valoresData('txtDtEmissao') . "," .
                utf8_decode($_POST['txtCodNumerico']) . "," .
                $dv . ",'" .
                utf8_decode($_POST['txtTipoDoc']) . "'," .
                valoresData('txtDtSaiEnt') . "," .
                valoresTexto('txtHrSaiEnt') . "," .
                utf8_decode($_POST['txtForEmis']) . "," .
                utf8_decode($_POST['txtForPgto']) . "," .
                utf8_decode($_POST['txtFinEmis']) . "," .
                utf8_decode($_POST['txtTipoImp']) . ",'" .
                utf8_decode($_POST['txtNtOpera']) . "'," .
                utf8_decode($_POST['txtEstadoNFE']) . "," .
                utf8_decode($_POST['txtCidadeNFE']) . "," .
                valoresData('txtcontigencia_hora') . ",'" .
                utf8_decode($_POST['txtcontigencia_motivo']) . "',1,3,'3.10.86'," .
                utf8_decode($_POST['txtEmitente']) . "," .
                utf8_decode($_POST['txtFonecedor']) . "," .
                "0,0," .
                utf8_decode($_POST['txtModFrete']) . "," .
                valoresSelect('txtTranNome') . ",'" .
                utf8_decode($_POST['txtTranPlaca']) . "'," .
                valoresSelect('txtTranEst') . ",'" .
                utf8_decode($_POST['txtRebNome']) . "','" .
                utf8_decode($_POST['txtRebPlaca']) . "'," .
                valoresSelect('txtRebEst') . ",'" .
                utf8_decode($_POST['txtTipoVol']) . "'," .
                valoresNumericos('txtQntVol') . "," .
                valoresNumericos('txtPesoLiq') . "," .
                valoresNumericos('txtPesoBru') . "," .
                valoresSelect('txtEntrega') . ",'" .
                utf8_decode($_POST['txtInfoFis']) . "','" .
                utf8_decode($_POST['txtInfoCon']) . "'," .
                "null,null,null,'" . utf8_decode($_POST['txtDestinatario']) . "','Aguarda'," .
                utf8_decode($_POST['optInfoCon']) . "," . utf8_decode($_POST['optInfoFis']) . "," .
                valoresNumericos('txtProdDesconto') . "," .
                utf8_decode($_POST['txtProdSinal']) . "," .
                valoresTexto('txtHrSaiEmi') . "," .
                valoresSelect('txtConFinal') . "," .
                valoresSelect('txtDestOper') . "," .
                valoresSelect('txtTipoAtend') . "," .
                valoresNumericos('txtDetPgto') . ")";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 cod_nfe from sf_nfe_notas order by cod_nfe desc") or die(odbc_errormsg());
        $nn = odbc_result($res, 1);
        //------------------Itens-------------------------------------------------------------------
        $notIn = '0';
        if (isset($_POST['txtIdTP'])) {
            $maxP = $_POST['txtIdTP'];
            $freteParc = 0;
            $descontoParc = 0;
            if ($maxP > 0) {
                $freteParc = valoresNumericos('txtTotFrete') / $maxP;
                $descontoParc = valoresNumericos('txtTotDesconto') / $maxP;
            }
            for ($i = 0; $i <= $maxP; $i++) {
                if (isset($_POST['txtIP_' . $i])) {
                    $query = "INSERT INTO sf_nfe_notas_itens (
                    cod_nota,cod_prod,qtd_comercial,tot_val_bruto,
                    cfop,qtd_trib,tot_seguro,tot_desconto,tot_frete,tot_outras_despesas,
                    icms_sit_trib,icms_origem,pis_sit_trib,pis_quantidade,cofins_sit_trib,cofins_quantidade,info_add, 
                    icms_alig_calc_cred, icms_val_deson, icms_red_bc,icms_aliq,icms_mod_det_st,icms_marg_va_st,icms_red_bc_st,icms_aliq_st,
                    pis_aliq_perc,pis_aliq_real,pis_tipo_calc,cofins_aliq_perc,cofins_aliq_real,cofins_tipo_calc,icms_mot_deson)
                    select " . $nn . "," . $_POST['txtPD_' . $i] . "," . valoresNumericos('txtVQ_' . $i) . "," . valoresNumericos('txtVP_' . $i) . ",'" .
                            utf8_decode($_POST['txtVCfop_' . $i]) . "'," . valoresNumericos('txtVQ_' . $i) . ",0," . $descontoParc . "," . $freteParc . ",0,
                    isnull(ICMSSitTrib,0), isnull(ICMSOrigem,0), isnull(PISSitTrib,0), isnull(PISQtdVend,0), isnull(COFINSSitTrib,0), isnull(COFINSQtdVend,0), '',
                    AliqCalcCred, ICMSValDes, ICMSRedBc, ICMSAliq, ICMSModDetSt, ICMSMargValAdSt, ICMSRedBcSt, ICMSAliqSt,
                    PISAliqPerc, PISAliqReal, PISTipoCalc, COFINSAliqPerc, COFINSAliqReal, PISTipoCalc, ICMSValMot
                    from sf_produtos A left join sf_nfe_cenarios B on A.cenario = B.id_cenario
                    where conta_produto = " . $_POST['txtPD_' . $i];
                    odbc_exec($con, $query) or die(odbc_errormsg());
                }
            }
        }
        $_POST['txtId'] = $nn;
        echo "<script>window.top.location.href = 'FormEmitir-Nota.php?id=" . $nn . "';</script>";
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_nfe_notas_itens WHERE cod_nota = " . $_POST['txtId']);
        odbc_exec($con, "DELETE FROM sf_nfe_notas WHERE cod_nfe = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso');window.top.location.href = 'Emitir-Nota.php';</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_nfe_notas where cod_nfe =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['cod_nfe'];
        $status_nfe = utf8_encode($RFP['status_nfe']); //[varchar](20) NOT NULL,           
        $chave_nfe = utf8_encode($RFP['code']); //[varchar](128) NOT NULL,           
        $vxml_nfe = utf8_encode($RFP['versao_xml']); //[varchar](10) NOT NULL,                           
        $modelo_nfe = utf8_encode($RFP['mod']); //[int] NOT NULL,           
        $serie_nfe = str_pad(utf8_encode($RFP['serie']), 3, "0", STR_PAD_LEFT); //[int] NOT NULL,
        $numero_nfe = str_pad(utf8_encode($RFP['nnf']), 9, "0", STR_PAD_LEFT); //[int] NOT NULL,
        $nprot = utf8_encode($RFP['nProt']); //[int] NOT NULL,
        if ($RFP['demis'] != "") {
            $data_emissao = escreverData($RFP['demis']); //[datetime] NOT NULL,
            $data_emissaoX = escreverData($RFP['demis'], 'Y-m-d'); //[datetime] NOT NULL,
            $data_emissao_pasta = escreverData($RFP['demis'], 'Ym'); //[datetime] NOT NULL,
        }
        if ($RFP['hSaiEmi'] != "") {
            $hora_saiemi = escreverData($RFP['hSaiEmi'], 'H:i:s'); //[datetime] NULL,                       
        } else {
            $hora_saiemi = "00:00:00";
        }
        $cod_numerico = utf8_encode($RFP['cnf']); //[varchar](10) NOT NULL,
        $dv = utf8_encode($RFP['cdv']); //[int] NOT NULL,                        
        $tipo_doc = utf8_encode($RFP['tpnf']); //[int] NOT NULL,  
        if ($RFP['dSaiEnt'] != "") {
            $data_saient = escreverData($RFP['dSaiEnt']); //[datetime] NOT NULL,
            $data_saientX = escreverData($RFP['dSaiEnt'], 'Y-m-d'); //[datetime] NOT NULL,
        }
        if ($RFP['hSaiEnt'] != "") {
            $hora_saient = escreverData($RFP['hSaiEnt'], 'H:i:s'); //[datetime] NULL,                       
        } else {
            $hora_saient = "00:00:00";
        }
        $for_emis = utf8_encode($RFP['tpemis']); //[int] NULL,
        $for_pgto = utf8_encode($RFP['indPag']); //[int] NOT NULL,           
        $fin_emis = utf8_encode($RFP['finNFe']); //[int] NOT NULL,           
        $tipo_print = utf8_encode($RFP['tpImp']); //[int] NOT NULL,                        
        $nat_operacao = utf8_encode($RFP['natop']); //[varchar](128) NOT NULL,            
        $estadoNFE = utf8_encode($RFP['uf']); //[varchar](20) NOT NULL,
        $cidadeNFE = utf8_encode($RFP['cmunfg']); //[varchar](20) NOT NULL,            
        $contigencia_hora = utf8_encode($RFP['contigencia_hora']); //[datetime] NULL,
        $contigencia_motivo = utf8_encode($RFP['contingencia_motivo']); //[varchar](512) NULL,
        $ambiente = utf8_encode($RFP['tpAmb']); //[int] NOT NULL,
        $processo = utf8_encode($RFP['procEmi']); //[int] NOT NULL,
        $versaoProc = utf8_encode($RFP['verProc']); //[varchar](10) NOT NULL,
        $Cod_Emitente = utf8_encode($RFP['codigo_emitente']); //[int] NOT NULL,
        $Cod_Destinatario = utf8_encode($RFP['codigo_destinatario']); //[int] NOT NULL,
        $Mod_Transp = utf8_encode($RFP['mod_transp']); //[int] NOT NULL,
        $Cod_Transp = utf8_encode($RFP['cod_transp']); //[int] NULL,
        $placa_veiculo = utf8_encode($RFP['placa_veiculo']);
        $trans_estado = utf8_encode($RFP['estado_veiculo']);
        $reboque = utf8_encode($RFP['reboque_veiculo']);
        $placa_veiculo_reboque = utf8_encode($RFP['reboque_veiculo_placa']);
        $reboq_estado = utf8_encode($RFP['reboque_veiculo_estado']);
        $tipoVol = utf8_encode($RFP['tipo_volume']);
        $Volume_Transp = utf8_encode($RFP['volume_transp']);
        $Peso_liq_Transp = utf8_encode($RFP['peso_liq_transp']); //[decimal](12, 3) NULL,
        $Peso_bru_Transp = utf8_encode($RFP['peso_bruto_transp']); //[decimal](12, 3) NULL,
        $end_entrega = utf8_encode($RFP['endereco_entrega']);
        $Info_add_fisco = utf8_encode($RFP['info_add_fisco']); //[varchar](1024) NULL,
        $Info_add_complementar = utf8_encode($RFP['info_add_complementar']); //[varchar](1024) NULL, 
        $destinatario = utf8_encode($RFP['destinatario']);
        $status = utf8_encode($RFP['status']);
        $msg_comp = utf8_encode($RFP['msg_comp']);
        $msg_comp_fisco = utf8_encode($RFP['msg_comp_fisco']);
        $descontop = escreverNumero($RFP['descontop']);
        $descontotp = utf8_encode($RFP['descontotp']);
        $conFinal = utf8_encode($RFP['conFinal']);
        $destOper = utf8_encode($RFP['destOper']);
        $tipoAten = utf8_encode($RFP['tipoAten']);
        $det_pgto = utf8_encode($RFP['detPag']);
    }
    $cur = odbc_exec($con, "select estado_codigo,estado_sigla,estado_gov from tb_estados where estado_codigo = '" . $estadoNFE . "' order by estado_nome") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $estadoNFEX = $RFP['estado_gov'];
    }
    if (is_numeric($cidadeNFE)) {
        $cur = odbc_exec($con, "select cidade_gov from tb_cidades 
        inner join tb_estados on tb_estados.estado_codigo = tb_cidades.cidade_codigoEstado
        where tb_cidades.cidade_codigo = " . $cidadeNFE) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $cidadeNFEX = $RFP['cidade_gov'];
        }
    }
} else {
    $disabled = '';
    $id = '';
    $status_nfe = 'Em Digitação';
    $chave_nfe = '';
    $numero_nfe = '000000000';
    $nprot = "";
    $cur = odbc_exec($con, "select * from sf_fornecedores_despesas where tipo in ('M') order by razao_social") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $Cod_Emitente = $RFP['id_fornecedores_despesas'];
        $estadoNFE = $RFP['estado'];
        $cidadeNFE = $RFP['cidade'];
    }
    $vxml_nfe = '4.00';
    $modelo_nfe = '55';
    $serie_nfe = '001';
    $data_emissao = getData("T");
    $data_emissaoX = date("Y-m-d");
    $data_emissao_pasta = date("Ym");
    $hora_saiemi = date("H:i:s");
    $cod_numerico = rand(99999999, 11111111);
    $dv = modulo_11($cod_numerico);
    if ($dv < 0) {
        $dv = 0;
    }
    $tipo_doc = '1';
    $data_saient = getData("T");
    $data_saientX = date("Y-m-d");
    $hora_saient = date("H:i:s");
    $for_emis = '1';
    $for_pgto = '1';
    $fin_emis = '1';
    $tipo_print = '1';
    $nat_operacao = '';
    $contigencia_hora = '';
    $contigencia_motivo = '';
    $ambiente = '1';
    $processo = '3';
    $versaoProc = '3.10.86';
    $Cod_Destinatario = 'null';
    $Mod_Transp = '9';
    $Cod_Transp = 'null';
    $placa_veiculo = '';
    $trans_estado = 'null';
    $reboque = 'N';
    $placa_veiculo_reboque = '';
    $reboq_estado = 'null';
    $tipoVol = 'UN';
    $Volume_Transp = escreverNumero(0);
    $Peso_liq_Transp = escreverNumero(0);
    $Peso_bru_Transp = escreverNumero(0);
    $end_entrega = 'null';
    $Info_add_fisco = '';
    $Info_add_complementar = '';
    $destinatario = '';
    $status = '';
    $estadoNFEX = '';
    $cidadeNFEX = '';
    $msg_comp = '';
    $msg_comp_fisco = '';
    $descontop = 0;
    $descontotp = 0;
    $conFinal = 0;
    $destOper = 1;
    $tipoAten = 0;
    $det_pgto = 1;
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}

if ($_GET['idx'] != '') {
    $FinalUrl = "&idx=" . $_GET['idx'];
}
if ($_GET['tpx'] != '') {
    $FinalUrl = $FinalUrl . "&tpx=" . $_GET['tpx'];
}
if ($_GET['tdx'] != '') {
    $FinalUrl = $FinalUrl . "&tdx=" . $_GET['tdx'];
}
if ($_GET['dti'] != '') {
    $FinalUrl = $FinalUrl . "&dti=" . $_GET['dti'];
}
if ($_GET['dtf'] != '') {
    $FinalUrl = $FinalUrl . "&dtf=" . $_GET['dtf'];
}
if ($_GET['id'] != '') {
    $FinalUrl = "id=" . $_GET['id'] . $FinalUrl;
}

if (isset($_POST['bntBol'])) {
    if (is_numeric($id)) {
        define("REMESSA", $PATH, true);
        $filename = REMESSA . "NFCe_" . $numero_nfe . ".txt";
        $conteudo = 'NOTA FISCAL|1|' . chr(13) . chr(10);
        $conteudo .= 'A|4.00|NFe' . $chave_nfe . '|' . chr(13) . chr(10);
        $conteudo .= 'B|' . $estadoNFEX . '|' . $cod_numerico . '|' . $nat_operacao . '|' . $for_pgto . '|' . $modelo_nfe . '|' . intval($serie_nfe) . '|' . intval($numero_nfe) . '|' . $data_emissaoX . 'T' . $hora_saiemi . '-03:00|' . $data_saientX . 'T' . $hora_saient . '-03:00|' . $tipo_doc . '|' . $destOper . '|' . $cidadeNFEX . '|' . $tipo_print . '|' . $for_emis . '|' . $dv . '|' . $ambiente . '|' . $fin_emis . '|' . $conFinal . '|' . $tipoAten . '|' . $processo . '|3.10.86|' . $contigencia_hora . '|' . $contigencia_motivo . '|' . chr(13) . chr(10);
        if (is_numeric($Cod_Emitente)) {
            $cur = odbc_exec($con, "select *,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato
            from sf_fornecedores_despesas f 
            inner join tb_cidades c on f.cidade = c.cidade_codigo
            inner join tb_estados e on e.estado_codigo = c.cidade_codigoEstado
            where id_fornecedores_despesas = " . $Cod_Emitente) or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                $conteudo .= 'C|' . utf8_encode($RFP['razao_social']) . '|' . utf8_encode($RFP['nome_fantasia']) . '|' . CharClear($RFP['inscricao_estadual']) . '|' . utf8_encode($RFP['ie_tributario']) . '|' . CharClear($RFP['inscricao_municipal']) . '|' . utf8_encode($RFP['cnae_fiscal']) . '|' . utf8_encode($RFP['regime_tributario']) . '|' . chr(13) . chr(10);
                $conteudo .= 'C02|' . CharClear(utf8_encode($RFP['cnpj'])) . '|' . chr(13) . chr(10);
                $conteudo .= 'C05|' . utf8_encode($RFP['endereco']) . '|' . utf8_encode($RFP['numero']) . '|' . utf8_encode($RFP['complemento']) . '|' . utf8_encode($RFP['bairro']) . '|' . utf8_encode($RFP['cidade_gov']) . '|' . CharReplace(utf8_encode($RFP['cidade_nome'])) . '|' . utf8_encode($RFP['estado_sigla']) . '|' . CharClear(utf8_encode($RFP['cep'])) . '|1058|BRASIL|' . substr(CharClear(utf8_encode($RFP['telefone_contato'])), 0, 10) . '|' . chr(13) . chr(10);
            }
        }
        if (is_numeric($Cod_Destinatario)) {
            $cur = odbc_exec($con, "select *,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = id_fornecedores_despesas) email_contato
            from sf_fornecedores_despesas f inner join tb_cidades c on f.cidade = c.cidade_codigo
            inner join tb_estados e on e.estado_codigo = c.cidade_codigoEstado
            where id_fornecedores_despesas = " . $Cod_Destinatario) or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                $conteudo .= 'E|' . utf8_encode($RFP['razao_social']) . '|' . utf8_encode($RFP['regime_tributario']) . '|' . CharClear($RFP['inscricao_estadual']) . '|' . CharClear($RFP['ie_tributario']) . '|' . CharClear($RFP['inscricao_municipal']) . '|' . CharSplit(utf8_encode($RFP['email_contato']), ";") . '|' . chr(13) . chr(10);
                if (strlen(CharClear(utf8_encode($RFP['cnpj']))) == 11) {
                    $conteudo .= 'E03|' . CharClear(utf8_encode($RFP['cnpj'])) . '|' . chr(13) . chr(10);
                } else {
                    $conteudo .= 'E02|' . CharClear(utf8_encode($RFP['cnpj'])) . '|' . chr(13) . chr(10);
                }
                $conteudo .= 'E05|' . utf8_encode($RFP['endereco']) . '|' . utf8_encode($RFP['numero']) . '|' . utf8_encode($RFP['complemento']) . '|' . utf8_encode($RFP['bairro']) . '|' . utf8_encode($RFP['cidade_gov']) . '|' . CharReplace(utf8_encode($RFP['cidade_nome'])) . '|' . utf8_encode($RFP['estado_sigla']) . '|' .
                        CharClear(utf8_encode($RFP['cep'])) . '|1058|BRASIL|' . substr(CharClear(utf8_encode($RFP['telefone_contato'])), 0, 10) . '|' . chr(13) . chr(10);
            }
        }
        $frete = "";
        $seguro = "";
        $desconto = "";
        $outradesp = "";
        $etotal = "1";
        $totalTributos = "";
        $val_bruto = 0;
        $i = 1;
        $cur = odbc_exec($con, "select gc.descricao grupo,codigo_interno,codigo_barra,p.descricao,ncm,unidade_comercial,
        qtd_comercial,tot_val_bruto,unidade_trib,unidade_trib,qtd_trib,valor_unitario_trib,preco_venda,cod_nota,cod_item_nota,
        origem,substituicao_trib,quantidade_comercial,nfe.*,tot_frete, i.cfop from sf_nfe_notas_itens i inner join sf_produtos p on p.conta_produto = i.cod_prod 
        inner join sf_contas_movimento gc on p.conta_movimento = gc.id_contas_movimento 
        left join sf_nfe_cenarios nfe on nfe.id_cenario = p.cenario
        where p.tipo = 'P' and cod_nota = " . $id);
        while ($RFP = odbc_fetch_array($cur)) {
            if ($RFP['tot_frete'] > 0) {
                $frt = escreverNumero($RFP['tot_frete'], 0, 2, '.', '');
            } else {
                $frt = "";
            }
            $conteudo .= 'H|' . $i . '||' . chr(13) . chr(10);
            $conteudo .= 'I|' . utf8_encode($RFP['codigo_interno']) . '|' . utf8_encode($RFP['codigo_barra']) . '|' . utf8_encode($RFP['descricao']) . '|' . utf8_encode($RFP['ncm']) .
                    '|' . utf8_encode($RFP['substituicao_trib']) . '|' . utf8_encode($RFP['cfop']) . '|' . utf8_encode($RFP['unidade_comercial']) . '|' . utf8_encode($RFP['qtd_comercial']) . '|' . escreverNumero(($RFP['tot_val_bruto'] / $RFP['qtd_comercial']), 0, 4, '.', '') . '|' . escreverNumero($RFP['tot_val_bruto'], 0, 2, '.', '') . '|' . utf8_encode($RFP['origem']) . '|' .
                    utf8_encode($RFP['unidade_trib']) . '|' . utf8_encode($RFP['qtd_trib']) . '|' . escreverNumero(($RFP['tot_val_bruto'] / $RFP['qtd_trib']), 0, 4, '.', '') . '|' . $frt . '|' . $seguro . '|' . $desconto . '|' . $outradesp . '|' . $etotal . '|||' . chr(13) . chr(10);
            $conteudo .= 'M|' . $totalTributos . '|' . chr(13) . chr(10);
            $conteudo .= 'N|' . chr(13) . chr(10);
            $conteudo .= 'N10d|' . utf8_encode($RFP['ICMSOrigem']) . '|' . utf8_encode($RFP['ICMSSitTrib']) . '|' . chr(13) . chr(10); //ICMS (Simples Nascional)
            $conteudo .= 'Q|' . chr(13) . chr(10);
            $conteudo .= 'Q05|' . $RFP['PISSitTrib'] . '|' . escreverNumero($RFP['PISValor'], 0, 2, '.', '') . '|' . chr(13) . chr(10); //PIS
            $conteudo .= 'Q10|' . escreverNumero($RFP['PISQtdVend'], 0, 4, '.', '') . '|' . escreverNumero($RFP['PISAliqReal'], 0, 4, '.', '') . '|' . chr(13) . chr(10);
            $conteudo .= 'S|' . chr(13) . chr(10);
            $conteudo .= 'S05|' . $RFP['COFINSSitTrib'] . '|' . escreverNumero($RFP['COFINSValor'], 0, 2, '.', '') . '|' . chr(13) . chr(10); //COFINS
            $conteudo .= 'S09|' . escreverNumero($RFP['COFINSQtdVend'], 0, 4, '.', '') . '|' . escreverNumero($RFP['COFINSAliqReal'], 0, 4, '.', '') . '|' . chr(13) . chr(10);
            $val_bruto = $val_bruto + $RFP['tot_val_bruto'];
            $frete = $frete + $RFP['tot_frete'];
            $i++;
        }
        $conteudo .= 'W|' . chr(13) . chr(10);
        $conteudo .= 'W02|0.00|0.00|0.00|0.00|0.00|' . escreverNumero($val_bruto, 0, 2, '.', '') . '|' . escreverNumero($frete, 0, 2, '.', '') . '|0.00|0.00|0.00|0.00|0.00|0.00|0.00|' . escreverNumero(($val_bruto + $frete), 0, 2, '.', '') . '||' . chr(13) . chr(10);
        $conteudo .= 'X|' . $Mod_Transp . '|' . chr(13) . chr(10);
        $conteudo .= 'Z|' . $Info_add_fisco . '|' . $Info_add_complementar . '|' . chr(13) . chr(10);
        if (!$handle = fopen($filename, 'w+')) {
            erro("Não foi possível abrir o arquivo ($filename)");
        }
        if (fwrite($handle, "$conteudo") === FALSE) {
            echo "Não foi possível escrever no arquivo ($filename)";
        }
        fclose($handle);
        header("Content-Type: " . "application/txt");
        header("Content-Length: " . filesize($filename));
        header("Content-Disposition: attachment; filename=" . basename($filename));
        readfile($filename);
    }
}

if (isset($_POST['btnXml'])) {
    $filename = "../../Pessoas/" . $contrato . "/NFE/" . ($tpAmb == '2' ? "homologacao" : "producao") . "/enviadas/aprovadas/" . $data_emissao_pasta . "/" . $chave_nfe . "-protNFe.xml";
    header("Content-Type: " . "application/txt");
    header("Content-Length: " . filesize($filename));
    header("Content-Disposition: attachment; filename=" . basename($filename));
    readfile($filename);
}

if (isset($_GET['vd']) && is_numeric($_GET['vd'])) {
    $for_pgto = 0;
    $det_pgto = 1;
    $tot_pgto = 0;
    $cur = odbc_exec($con, "select * from sf_vendas inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = sf_vendas.cliente_venda 
    left join tb_estados e on f.estado = e.estado_codigo left join tb_cidades c on f.cidade = c.cidade_codigo where id_venda = '" . $_GET['vd'] . "'");
    while ($RFP = odbc_fetch_array($cur)) {
        $data_saient = escreverData($RFP['data_venda']);
        $hora_saient = escreverData($RFP['data_venda'], 'H:i:s');
        $nat_operacao = 'VENDA';
        $Cod_Destinatario = $RFP['cliente_venda'];
        $descontop = escreverNumero($RFP['descontop']);
        $descontotp = utf8_encode($RFP['descontotp']);
        $destinatario = utf8_encode($RFP['destinatario']);
        $status = utf8_encode($RFP['status']);
        $hora_saiemi = date("H:i:s");
        $conFinal = 0;
        $destOper = 1;
        $tipoAten = 0;
    }
    $cur = odbc_exec($con, "select tipo_documento from sf_venda_parcelas where venda = '" . $_GET['vd'] . "'");
    while ($RFP = odbc_fetch_array($cur)) {
        switch ($RFP['tipo_documento']) {
            case 4:
                $det_pgto = 99;
                break;
            case 5:
                $det_pgto = 2;
                break;
            case 6:
                $det_pgto = 1;
                break;
            case 7:
                $det_pgto = 3;
                break;
            case 9:
                $det_pgto = 4;
                break;
            case 10:
                $det_pgto = 15;
                break;
        }
        $tot_pgto++;
    }
    if ($tot_pgto > 1) {
        $for_pgto = 1;
    }
    $cur = odbc_exec($con, "select id_mensagem,nome_mensagem,descricao_mensagem from sf_nfe_mensagens") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $msg_comp_fisco = utf8_encode($RFP['id_mensagem']);
        $Info_add_fisco = utf8_encode($RFP['descricao_mensagem']);
    }
}
