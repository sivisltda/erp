<?php
include "../../Connections/configini.php";

$disabled = "disabled";
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != "") {
        odbc_exec($con, "update sf_nfs_enquadramento set " .
        "codigo_enq = " . valoresTexto("txtCodNfe") . "," .
        "descricao_enq = " . valoresTexto("txtNomeCid") . " " .
        "where id_enq = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_nfs_enquadramento(codigo_enq,descricao_enq)values(" . 
        valoresTexto("txtCodNfe") . "," .
        valoresTexto("txtNomeCid") . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_enq from sf_nfs_enquadramento order by id_enq desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != "") {
        odbc_exec($con, "DELETE FROM sf_nfs_enquadramento WHERE id_enq = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = "";
    $_POST['txtId'] = "";
}
if ($_GET['id'] != "" || $_POST['txtId'] != "") {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != "") {
        $disabled = "disabled";
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_nfs_enquadramento where id_enq =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_enq'];
        $cod_nfe = utf8_encode($RFP['codigo_enq']);
        $nome_cid = utf8_encode($RFP['descricao_enq']);
    }
} else {
    $disabled = "";
    $id = "";
    $cod_nfe = "";
    $nome_cid = "";
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != "") {
        $disabled = "";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>        
    </head>
    <body>
        <form action="FormEnquadramento-Servico.php" name="frmEnviaDados" id="frmEnviaDados" method="POST">
            <div class="frmhead">
                <div class="frmtext">Enquadramento do Serviço</div>
                <div class="frmicon" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input name="txtId" id="txtId" type="hidden" value="<?php echo $id; ?>"/>
                <div style="float: left;width: 39%;">
                    <span>Código:</span>
                    <input type="text" name="txtCodNfe" id="txtCodNfe" class="input-medium" maxlength="20" value="<?php echo $cod_nfe; ?>" <?php echo $disabled; ?>/>
                </div>                
                <div style="clear:both"></div>                
                <div>
                    <span>Descrição:</span>
                    <input type="text" name="txtNomeCid" id="txtNomeCid" class="input-medium" maxlength="512" value="<?php echo $nome_cid; ?>" <?php echo $disabled; ?>/>
                </div>                
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == "") { ?>                        
                        <button class="btn green" type="submit" name="bntSave"><span class="ico-checkmark"></span> Gravar</button>
                        <button class="btn yellow" title="Cancelar" <?php echo ($_POST["txtId"] == "" ? "onClick=\"parent.FecharBox()\"" : "type=\"submit\" name=\"bntAlterar\""); ?>><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn green" type="submit" name="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" type="submit" name="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                        <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
        </form>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <?php odbc_close($con); ?>
    </body>
</html>