<?php

include "./../../Connections/configini.php";

if ($_GET['Del'] != '') {
    odbc_exec($con, "DELETE FROM sf_nfe_notas_itens WHERE cod_nota = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_nfe_notas WHERE cod_nfe = " . $_GET['Del']);
    echo "<script>window.top.location.href = 'Emitir-Nota.php'; </script>";
}
$imprimir = 0;
$FinalUrl = "&imp=0";

if (isset($_POST['btnPrint'])) {
    $imprimir = 1;
    $FinalUrl = "&imp=1";
}

if (isset($_GET['txtModelo']) && $_GET['txtModelo'] !== "") {
    $txtModelo = $_GET['txtModelo'];
    $FinalUrl = $FinalUrl . "&txtModelo=" . $_GET['txtModelo'];
}

if (isset($_GET['txtStatus']) && $_GET['txtStatus'] !== "") {
    $txtStatus = $_GET['txtStatus'];
    $FinalUrl = $FinalUrl . "&txtStatus=" . $_GET['txtStatus'];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>    
    </head>
    <body>
        <?php if ($imprimir == 0) { ?>
            <div id="loader"><img src="../../img/loader.gif"/></div><?php } ?>
        <div class="wrapper">
            <?php
            if ($imprimir == 0) {
                include('../../menuLateral.php');
            }
            ?>
            <div class="body">
                <?php
                if ($imprimir == 0) {
                    include("../../top.php");
                }
                ?>
                <div class="content">      
                    <?php if ($imprimir == 0) { ?>				
                        <div class="page-header">
                            <div class="icon"> <span class="ico-arrow-right"></span></div>
                            <h1>Nota Fiscal Eletrônica<small>Emissão de Notas</small></h1> 
                        </div>
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="boxfilter block">
                                    <div style="margin-top: 15px;float:left;">
                                        <div style="float:left">
                                            <form method="POST" action="Emitir-Nota.php">
                                                <a href="FormEmitir-Nota.php" title="Emitir Nota"> <button id="btnNovo" name="btnNovo" class="button button-green btn-primary" type="button"><span class="ico-file-4 icon-white"></span></button></a>
                                                <button name="btnPrint" class="button button-blue btn-primary" type="button" onclick="redirectFind(1);" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                                <button name="btnXml" class="button button-blue btn-primary" type="button" onclick="window.location = 'Xml-Arquivos.php';" title="Xml"><span class="ico-barcode-2 icon-white"></span></button>
                                            </form>
                                        </div>
                                    </div>
                                    <div style="float: right;width: 370px;">
                                        <div style="margin-left: 1%;float: left;">
                                            <span>Status:</span>
                                            <select id="txtStatus">
                                                <option value="">Todos</option>
                                                <option value="Em Digitação" <?php
                                                if ($txtStatus == "Em Digitação") {
                                                    echo "SELECTED";
                                                }
                                                ?>>Em Digitação</option>
                                                <option value="Assinada" <?php
                                                if ($txtStatus == "Assinada") {
                                                    echo "SELECTED";
                                                }
                                                ?>>Assinada</option>
                                                <option value="Autorizada" <?php
                                                if ($txtStatus == "Autorizada") {
                                                    echo "SELECTED";
                                                }
                                                ?>>Autorizada</option>
                                                <option value="Rejeitada" <?php
                                                if ($txtStatus == "Rejeitada") {
                                                    echo "SELECTED";
                                                }
                                                ?>>Rejeitada</option>
                                                <option value="Processamento" <?php
                                                if ($txtStatus == "Processamento") {
                                                    echo "SELECTED";
                                                }
                                                ?>>Processamento SEFAZ</option>
                                                <option value="Cancelada" <?php
                                                if ($txtStatus == "Cancelada") {
                                                    echo "SELECTED";
                                                }
                                                ?>>Cancelada</option>
                                            </select>
                                        </div>
                                        <div style="float:left;margin-left: 1%;">
                                            <span>Modelo:</span>
                                            <select id="txtModelo">
                                                <option value="">Todos</option>
                                                <option value="55" <?php
                                                if ($txtModelo == "55") {
                                                    echo "SELECTED";
                                                }
                                                ?>>55</option>
                                                <option value="65" <?php
                                                if ($txtModelo == "65") {
                                                    echo "SELECTED";
                                                }
                                                ?>>65</option>
                                            </select>
                                        </div>
                                        <div style="margin-top: 15px;float:left;margin-left:0.3%;">
                                            <div style="float:left">
                                                <button name="btnfind" class="button button-turquoise btn-primary" type="button" onclick="redirectFind(0);" title="Buscar"><span class="ico-search icon-white"></span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div style="clear:both"></div>
                    <div class="boxhead" <?php echo $visible; ?>>
                        <div class="boxtext">Emissão de Notas</div>
                    </div>
                    <div <?php
                    if ($imprimir == 0) {
                        echo "class=\"boxtable\"";
                    }
                    ?>>
                            <?php
                            if ($imprimir == 1) {
                                $titulo_pagina = "RELATÓRIO DE CONSULTA<br/>Emissão de Notas";
                                include "../Financeiro/Cabecalho-Impressao.php";
                            }
                            ?>
                        <table <?php
                        if ($imprimir == 0) {
                            echo "class=\"table\"";
                        } else {
                            echo "border=\"1\"";
                        }
                        ?> style="float:none" cellpadding="0" cellspacing="0" width="100%" id="example">
                            <thead>
                                <tr>
                                    <th width="5%"><center>Série</center></th>
                            <th width="12%">Número</th>
                            <th width="9%">Dt. Emissão</th>
                            <th width="20%">Destinatário</th>
                            <th width="9%">Tipo Doc.</th>
                            <th width="9%">Valor Total</th>
                            <th width="13%">Modelo</th>
                            <th width="16%">Status</th>
                            <?php if ($imprimir == 0) { ?>
                                <th width="5%"><center>Ação:</center></th>
                            <?php } ?>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>

                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>        
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            
            function redirectFind(imp) {
                var finalURL = '?imp=0';
                if (imp === 1) {
                    finalURL = '?imp=1';
                }
                if ($("#txtStatus").val() !== "") {
                    finalURL = finalURL + '&txtStatus=' + $("#txtStatus").val();
                }
                if ($("#txtModelo").val() !== "") {
                    finalURL = finalURL + '&txtModelo=' + $("#txtModelo").val();
                }
                window.location = 'Emitir-Nota.php' + finalURL.replace(/\//g, "_");
            }

            $(document).ready(function () {
                $('#example').dataTable({
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "aaSorting": [[1, "desc"]],
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": "Emitir-Nota_server_processing.php?<?php echo $FinalUrl; ?>",
                    "bFilter": false,
                    'oLanguage': {
                        'oPaginate': {
                            'sFirst': "Primeiro",
                            'sLast': "Último",
                            'sNext': "Próximo",
                            'sPrevious': "Anterior"
                        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                        'sLengthMenu': "Visualização de _MENU_ registros",
                        'sLoadingRecords': "Carregando...",
                        'sProcessing': "Processando...",
                        'sSearch': "Pesquisar:",
                        'sZeroRecords': "Não foi encontrado nenhum resultado"},
                    "sPaginationType": "full_numbers",
                    <?php if ($imprimir == 1) { ?>
                        "fnInitComplete": function (oSettings, json) {
                            window.print();
                            window.history.back();
                        }
                    <?php } ?>
                });
            });</script>        
        <script type="text/javascript">
        <?php if ($imprimir == 1) { ?>
            $(window).load(function () {
                $(".body").css("margin-left", 0);
                $("#example_length").remove();
                $("#example_filter").remove();
                $("#example_paginate").remove();
                $("#formPQ > th").css("background-image", "none");
            });
        <?php } ?>
        </script>
        <?php odbc_close($con); ?>
    </body>
</html>
