<?php
include "../../Connections/configini.php";

if ($_GET['Del'] != '') {
    odbc_exec($con, "DELETE FROM tb_cidades WHERE cidade_codigo =" . $_GET['Del']);
    echo "<script>window.top.location.href = 'Cidades.php'; </script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>                
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>        
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Nota Fiscal Eletrônica<small>Cidades</small></h1> 
                    </div>
                    <div id="parametros_busca" class="row-fluid">
                        <div class="span12">
                            <div class="block" style="font-size:13px">
                                <button id="btnNovo" class="button button-green btn-primary" type="button" onclick="AbrirBox(0)"><span class="ico-file-4 icon-white"></span></button>
                                <button id="btnPrint" class="button button-blue btn-primary" type="button" onClick="imprimir()"><span class="ico-print icon-white"></span></button>
                                <div style="float: right;width: 40%;">
                                    <div style="width: 40%; margin-right: 1%;float: left;">
                                        <span>Estado:</span>
                                        <select id="txtEstado" style="width: 100%;" class="select">
                                            <option value="null">Todos</option>
                                            <?php $cur = odbc_exec($con, "select estado_codigo, estado_nome from tb_estados where estado_codigo > 0 order by estado_nome") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['estado_codigo'] ?>"><?php echo utf8_encode($RFP['estado_nome']) ?></option>
                                            <?php } ?>                                            
                                        </select>
                                    </div>
                                    <div style="width: 48%;margin-right: 1%;float: left;">
                                        <span>Pesquisar:</span>
                                        <input name="txtBuscar"  id="txtBuscar" maxlength="100" type="text" style="width:100%;height: 28px;" onkeypress="TeclaKey(event)" value=""/>                                        
                                    </div>                                    
                                    <div style="width: 9%;margin-right: 1%;float: left; margin-top: 14px;">
                                        <button id="btnfind" class="button button-turquoise btn-primary" type="button" onclick="refresh()" id="btnPesquisar"><span class="ico-search icon-white"></span></button>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Cidades</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tblCidades">
                            <thead>
                                <tr>
                                    <th width="5%">Código</th>
                                    <th width="65%">Nome da Cidade</th>
                                    <th width="5%">Estado</th>
                                    <th width="10%">Código NFE</th>
                                    <th width="5%"><center>Ação:</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script> 
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>        
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>       
        <script type="text/javascript">
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }
            
            $(document).ready(function () {
                var columns = [
                    {"bSortable": false},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": false},
                    {"bSortable": false}];
                $('#tblCidades').dataTable({
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": finalFind(0),
                    "bFilter": false,
                    "aoColumns": columns,
                    'oLanguage': {
                        'oPaginate': {
                            'sFirst': "Primeiro",
                            'sLast': "Último",
                            'sNext': "Próximo",
                            'sPrevious': "Anterior"
                        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                        'sLengthMenu': "Visualização de _MENU_ registros",
                        'sLoadingRecords': "Carregando...",
                        'sProcessing': "Processando...",
                        'sSearch': "Pesquisar:",
                        'sZeroRecords': "Não foi encontrado nenhum resultado"},
                    "sPaginationType": "full_numbers"
                });
            });
            
            function AbrirBox(id) {
                abrirTelaBox("FormCidades.php" + (id > 0 ? "?id=" + id : ""), 235, 450);
            }

            function refresh() {
                var oTable = $('#tblCidades').dataTable();
                oTable.fnReloadAjax(finalFind(0));
            }

            function finalFind(imp) {
                var retorno = "?imp=" + imp;
                if ($('#txtEstado').val() !== "null") {
                    retorno = retorno + "&est=" + $("#txtEstado").val();
                }
                if ($('#txtBuscar').val() !== "") {
                    retorno = retorno + "&Search=" + $("#txtBuscar").val();
                }
                return "./../NFE/Cidades_server_processing.php" + retorno;
            }

            function FecharBox() {
                $("#newbox").remove();
                refresh();
            }

            function imprimir() {
                var pRel = "&NomeArq=" + "Cidades" +
                        "&lbl=" + "Código|Nome|Estado|Código NFE" +
                        "&siz=" + "50|500|50|100" +
                        "&pdf=" + "4" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + "Cidades " + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + finalFind(1).replace("?", "&").replace("./../", "../Modulos/"); // server processing            
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }
        </script>
        <?php odbc_close($con); ?>
    </body>
</html>