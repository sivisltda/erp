<?php
include "../../Connections/configini.php";

if ($_GET['Del'] != '') {
    odbc_exec($con, "DELETE FROM sf_nfe_notas_itens WHERE cod_nota = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_nfe_notas WHERE cod_nfe = " . $_GET['Del']);
    echo "<script>window.top.location.href = 'Emitir-Nota-Servico.php'; </script>";
}

$pasta = "./../../Pessoas/" . $contrato . "/Retorno/";
$nome = "";

if (isset($_POST['bntLer'])) {
    $nome = $_FILES['arquivo']['name'];
    $type = $_FILES['arquivo']['type'];
    $size = $_FILES['arquivo']['size'];
    $tmp = $_FILES['arquivo']['tmp_name'];
    if ($tmp) {
        move_uploaded_file($tmp, $pasta . "/" . $nome);
    }
}

if ($nome != "") {
    $lendo = fopen($pasta . "/" . $nome, "r");
    if (!$lendo) {
        echo "Erro ao abrir a URL."; exit;
    }
    while (!feof($lendo)) {
        $linha = fgets($lendo, 9999);
        if (substr($linha, 0, 2) == "20") {
            $codigo = substr($linha, 2, 15);
            $verificacao = substr($linha, 18, 9);
            $serie = substr($linha, 42, 5);
            $nota = substr($linha, 47, 15);
            $inscMun = substr($linha, 85, 15);            
            $query = "update sf_nfe_notas set status_nfe = " . valoresTexto2("Autorizada") . "," .
            "code = " . valoresTexto2($codigo . $verificacao) . "," .
            "link_pdf = 'https://nfse.niteroi.rj.gov.br/NFSE/contribuinte/notaPrint.aspx?inscricao=" . valoresNumericos2($inscMun) . "&nf=" . valoresNumericos2($codigo) . "&verificacao=" . str_replace(" ", "", $verificacao) . "' 
            from sf_nfe_notas where serie = " . valoresNumericos2($serie) . " and nnf = " . valoresNumericos2($nota);
            odbc_exec($con, $query);
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/bootbox.css"/>        
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>            
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">      	
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Notas de Serviços<small>Emissão de Notas</small></h1> 
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="margin-top: 15px;float:left; width: 60%;">
                                    <form action="Emitir-Nota-Servico.php" method="post" enctype="multipart/form-data">
                                        <div style="float:left">
                                            <button id="btnNovo" class="button button-green btn-primary" type="button" onclick="redirectFind(0);"><span class="ico-file-4 icon-white"></span></button>
                                            <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir();" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                            <button id="btnNfs" class="button button-green" type="button" name="btnNfs" title="Emitir Nota de Serviço" style="display: none;background: #222;border-color: #222;" disabled="true"><span class="ico-money icon-white"></span></button>
                                            <button id="btnNfsExp" class="button button-turquoise" type="button" name="btnNfsExp" title="Exportação de Notas" style="display: none;" disabled="true"><span class="ico-download-alt icon-white"></span></button>
                                            <button id="btnNfsImp2" class="button button-orange" type="button" name="btnNfsImp2" title="Importação de Notas" style="display: none;"><span class="ico-upload-alt icon-white"></span></button>                                                                                                                          
                                            <button id="btnNfsConsulta" class="button button-green btn-primary" type="button" name="btnNfsConsulta" title="Consultar Nota de Serviço" style="display: none" disabled="true"><span class="ico-refresh icon-white"></span></button>
                                            <button id="btnXml" class="button button-blue btn-primary" type="button" name="btnXml" title="Xml" style="display: none"><span class="ico-barcode-2 icon-white"></span></button>                                        
                                        </div>
                                        <div style="float:left; display: none; margin-left: 1%;" name="btnNfsImp" id="btnNfsImp">
                                            <input class="btn  btn-primary" type="file" name="arquivo" id="arquivo" style="height:23px; line-height:23px; width:250px; margin-bottom: 0px;"/>
                                            <button class="button button-turquoise btn-primary" type="submit" name="bntLer" id="bntLer" value="Ler"><span>Ler</span></button>                                                
                                        </div>                                        
                                    </form>                                        
                                </div>
                                <div style="float: right;width: 40%;">
                                    <div style="width:20%;float:left;margin-left:1%">
                                        <span>Data Inicial:</span>
                                        <input type="text" style="width:100%" id="txt_dt_begin" class="datepicker inputCenter" value="<?php echo $dt_begin; ?>" placeholder="Data inicial">
                                    </div>
                                    <div style="width:20%;float:left;margin-left:1%">
                                        <span>Data Final:</span>
                                        <input type="text" style="width:100%" class="datepicker inputCenter" id="txt_dt_end" value="<?php echo $dt_end; ?>" placeholder="Data Final">
                                    </div>
                                    <div style="width:46%;float: left;margin-left:1%">
                                        <span>Status:</span>
                                        <select id="txtStatus">
                                            <option value="">Todos</option>
                                            <option value="Em Digitação" <?php
                                            if ($txtStatus == "Em Digitação") {
                                                echo "SELECTED";
                                            }
                                            ?>>Em Digitação</option>
                                            <option value="Em Processamento" <?php
                                            if ($txtStatus == "Em Processamento") {
                                                echo "SELECTED";
                                            }
                                            ?>>Em Processamento</option>                                            
                                            <option value="Em Exportação" <?php
                                            if ($txtStatus == "Em Exportação") {
                                                echo "SELECTED";
                                            }
                                            ?>>Em Exportação</option>                                            
                                            <option value="Autorizada" <?php
                                            if ($txtStatus == "Autorizada") {
                                                echo "SELECTED";
                                            }
                                            ?>>Autorizada</option>
                                            <option value="Negada" <?php
                                            if ($txtStatus == "Negada") {
                                                echo "SELECTED";
                                            }
                                            ?>>Negada</option>
                                            <option value="Cancelada" <?php
                                            if ($txtStatus == "Cancelada") {
                                                echo "SELECTED";
                                            }
                                            ?>>Cancelada</option>                                            
                                        </select>
                                    </div>
                                    <div style="width:10%;float:left;margin-left:1%;margin-top: 15px;">
                                        <div style="float:left">
                                            <button id="btnfind" name="btnfind" class="button button-turquoise btn-primary" type="button" title="Buscar"><span class="ico-search icon-white"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Emissão de Notas de Serviços</div>
                    </div>
                    <div class="boxtable">                        
                        <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbl_servicos">
                            <thead>
                                <tr>
                                    <th width="4%" style="top:0"><input id="checkAll" type="checkbox" class="checkall"/></th>                                    <th width="5%"><center>Série</center></th>
                                    <th width="12%">Número</th>
                                    <th width="9%">Dt. Emissão</th>
                                    <th width="20%">Destinatário</th>
                                    <th width="9%">Tipo Doc.</th>
                                    <th width="9%">Valor Total</th>
                                    <th width="9%">Modelo</th>
                                    <th width="16%">Status</th>
                                    <th width="5%"><center>Ação:</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Processando">
            <div id="progressbar"></div>
            <div style="display: flex;align-items: center;justify-content: center;margin-top: 20px;">
                <div id="pgIni">0</div><div style="padding: 5px;">até</div><div id="pgFim">0</div>            
            </div>            
        </div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>        
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script> 
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            
            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
                
            $(document).ready(function () {
                $('#tbl_servicos').dataTable({
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "bSort": false,
                    "sAjaxSource": finalFind(0),
                    "bFilter": false,
                    'oLanguage': {
                        'oPaginate': {
                            'sFirst': "Primeiro",
                            'sLast': "Último",
                            'sNext': "Próximo",
                            'sPrevious': "Anterior"
                        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                        'sLengthMenu': "Visualização de _MENU_ registros",
                        'sLoadingRecords': "Carregando...",
                        'sProcessing': "Processando...",
                        'sSearch': "Pesquisar:",
                        'sZeroRecords': "Não foi encontrado nenhum resultado"},
                    "fnDrawCallback": function () {
                        refreshTotal();
                    },
                    "sPaginationType": "full_numbers"
                });
            });

            function refreshTotal() {
                $("table :checkbox").click(function (event) {
                    if ($('input:checkbox:checked:not(".checkall")').length > 0) {
                        $("#btnNfs, #btnNfsExp, #btnNfsConsulta").attr("disabled", false);
                    } else {
                        $("#btnNfs, #btnNfsExp, #btnNfsConsulta").attr("disabled", true);
                    }
                });
            }

            $("#btnfind").click(function () {
                refresh();
            });

            $("#btnNfs").click(function () {
                bootbox.confirm('Confirma o envio das Nfs Selecionadas?', function (result) {
                    if (result === true) {
                        $("#source").dialog({modal: true});
                        $("#progressbar").progressbar({value: 0});
                        $("#pgIni").html("0");
                        $("#pgFim").html($('input:checkbox:checked:not(".checkall")').length);
                        processar();
                    }
                });
            });

            $("#btnXml").click(function () {
                bootbox.confirm('Confirma a exportação dos arquivos em XML?', function (result) {
                    if (result === true) {
                        $.post(finalFind(1)).done(function (data) {
                            var response = jQuery.parseJSON(data);
                            $("#source").dialog({modal: true});
                            $("#progressbar").progressbar({value: 0});
                            $("#pgIni").html("0");
                            $("#pgFim").html(response.aaData.length);
                            processarXml(response.aaData);
                        });
                    }
                });
            });

            function processarXml(lista) {
                var PgIni = parseInt($("#pgIni").html());
                var PgFim = parseInt($("#pgFim").html());
                if ((PgIni < PgFim) && PgFim > 0 && $('#source').dialog('isOpen')) {
                    var IdChk = lista[PgIni][0].replace("<center><input type=\"checkbox\" class=\"caixa\" name=\"items[]\" value=\"", "").replace("\" disabled/></center>", "");
                    $("#progressbar").progressbar({value: ((PgIni * 100) / PgFim)});
                    $("#pgIni").html(PgIni + 1);
                    $.post("../NFE/include/XmlLoteNFse.php", "txtId=" + IdChk + "&bntSave=S&txtIni=" + PgIni).done(function (data) {
                        if (data.length > 0 && data === "YES") {                            
                        }
                        processarXml(lista);
                    });
                } else {
                    $("#source").dialog('close');
                    window.location.href = '../NFE/include/XmlLoteNFse.php?bntDownload=S';                    
                }
            }

            $("#btnNfsConsulta").click(function () {
                $("#source").dialog({modal: true});
                $("#progressbar").progressbar({value: 0});
                $("#pgIni").html("0");
                $("#pgFim").html($('input:checkbox:checked:not(".checkall")').length);
                processar_consulta();
            });

            function processar() {
                var PgIni = parseInt($("#pgIni").html());
                var PgFim = parseInt($("#pgFim").html());
                if ((PgIni < PgFim) && PgFim > 0 && $('#source').dialog('isOpen')) {
                    var IdChk = $('input:checkbox:checked:not(".checkall")')[PgIni].value;
                    $("#progressbar").progressbar({value: ((PgIni * 100) / PgFim)});
                    $("#pgIni").html(PgIni + 1);
                    $.post("../NFE/include/MakeNFse.php", "txtId=" + IdChk).done(function (data) {
                        if (data.length > 4 && data.substr(0, 4) !== "Erro") {
                            if ($("#txtStatus_" + IdChk).length) {
                                $("#txtStatus_" + IdChk).html("Em Processamento");
                            }
                        }
                        processar();
                    });
                } else {
                    $("#source").dialog('close');
                    refresh();
                }
            }

            function processar_consulta() {
                var PgIni = parseInt($("#pgIni").html());
                var PgFim = parseInt($("#pgFim").html());
                if ((PgIni < PgFim) && PgFim > 0 && $('#source').dialog('isOpen')) {
                    var IdChk = $('input:checkbox:checked:not(".checkall")')[PgIni].value;
                    $("#progressbar").progressbar({value: ((PgIni * 100) / PgFim)});
                    $("#pgIni").html(PgIni + 1);
                    $.post("../NFE/include/ConsultaNFse.php", "txtId=" + IdChk + "&txtNota=" + $("#c_nfes_" + IdChk).html()).done(function (data) {
                        if (data.status !== "Erro") {
                            if ($("#txtStatus_" + IdChk).length) {
                                $("#txtStatus_" + IdChk).html(data.status);
                            }
                        }
                        processar_consulta();
                    });
                } else {
                    $("#source").dialog('close');
                    refresh();
                }
            }

            function refresh() {
                $("#checkAll").parent().removeClass('checked');
                $("#checkAll").attr('checked', false);
                var tblServicos = $('#tbl_servicos').dataTable();
                tblServicos.fnReloadAjax(finalFind(0));
            }

            function redirectFind(id) {
                window.location = 'FormEmitir-Nota-Servico.php' + (id > 0 ? "?id=" + id : "");
            }

            function finalFind(imp) {
                $("#btnNfs, #btnNfsExp, #btnNfsImp, #btnNfsConsulta, #btnXml").hide();
                $("#btnNfs, #btnNfsExp, #btnNfsConsulta").attr("disabled", true);
                if ($("#txtStatus").val() === "Em Digitação") {
                    $("#btnNfs").show();
                    $("#btnNfsExp").show();
                } else if ($("#txtStatus").val() === "Em Exportação") {
                    $("#btnNfsExp").show();
                    $("#btnNfsImp").show();
                } else if ($("#txtStatus").val() === "Em Processamento") {
                    $("#btnNfsConsulta").show();
                } else if ($("#txtStatus").val() === "Autorizada") {
                    $("#btnXml").show();
                }
                var finalURL = '?imp=0';
                if (imp === 1) {
                    finalURL = '?imp=1';
                }
                if ($("#txtStatus").val() !== "") {
                    finalURL = finalURL + '&txtStatus=' + $("#txtStatus").val();
                }
                finalURL = finalURL + '&dti=' + $("#txt_dt_begin").val();
                finalURL = finalURL + '&dtf=' + $("#txt_dt_end").val();
                return 'Emitir-Nota-Servico_server_processing.php' + finalURL.replace(/\//g, "_");
            }

            function imprimir() {
                var pRel = "&NomeArq=" + "Notas de Serviços" +
                        "&lbl=Série|Número|Dt. Emissão|Destinatário|Tipo Doc.|Valor Total|Modelo|Status" +
                        "&siz=40|70|70|210|90|80|50|90" +
                        "&pdf=0|9" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/NFE/" + finalFind(1).replace("?", "&"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }           
            
            $("#btnNfsExp").click(function () {
                if ($('input:checkbox:checked:not(".checkall")').length > 0) {
                    setTimeout(refresh, 1000);
                    var notas = "0";
                    $('input:checkbox:checked:not(".checkall")').each(function( index, value ) {
                        notas += "," + value.value;
                    });
                    window.open("../../Modulos/NFE/include/MakeNfseText.php?id=" + notas, '_blank');
                }
            });
            
            $("#btnNfsImp").click(function () {
                
            });            
        </script>                        
    </body>
    <?php odbc_close($con); ?>
</html>
