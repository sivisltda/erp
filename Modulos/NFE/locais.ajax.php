<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include "./../../Connections/configini.php";

$id_area = $_REQUEST['txtEstado'];
$local = array();
$sql = "select cidade_codigo,cidade_nome from tb_cidades where cidade_codigo > 0 AND cidade_codigoEstado = " . $id_area . " ORDER BY cidade_nome";
$cur = odbc_exec($con, $sql) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $local[] = array('cidade_codigo' => $RFP['cidade_codigo'], 'cidade_nome' => utf8_encode($RFP['cidade_nome']));
}

echo(json_encode($local));
odbc_close($con);
