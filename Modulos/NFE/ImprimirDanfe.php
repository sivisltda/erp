<?php

require_once('../../Connections/configini.php');
//error_reporting(E_ALL);
//ini_set('display_errors', 'On');
include __DIR__ . '../../../util/nfe/vendor/autoload.php';

use NFePHP\Extras\Danfe;
use NFePHP\Extras\Danfce;
use NFePHP\Common\Files\FilesFolders;
use NFePHP\NFe\ToolsNFe;

if (is_numeric($_GET["id"])) {
    if (file_exists('../../Pessoas/' . $contrato . '/Emitentes/config.json')) {
        $pathLogo = "./img/logo-nfce.jpg";
        if (file_exists('../../Pessoas/' . $contrato . '/Empresa/logo_' . str_pad(1, 3, "0", STR_PAD_LEFT) . '.jpg')) {
            $pathLogo = '../../Pessoas/' . $contrato . '/Empresa/logo_' . str_pad(1, 3, "0", STR_PAD_LEFT) . '.jpg';
        }
        $nfeTools = new ToolsNFe('../../Pessoas/' . $contrato . '/Emitentes/config.json');
        $tpAmb = $nfeTools->aConfig['tpAmb'];

        if (isset($_GET['tipo']) && $_GET['tipo'] == "PRE") {
            $xml = "../../Pessoas/" . $contrato . "/NFE/" . ($tpAmb == '2' ? "homologacao" : "producao") . "/assinadas/" . $_GET["id"] . "-nfe.xml";
        } else {
            $xml = "../../Pessoas/" . $contrato . "/NFE/" . ($tpAmb == '2' ? "homologacao" : "producao") . "/enviadas/aprovadas/" . $_GET["dirMes"] . "/" . $_GET["id"] . "-protNFe.xml";
        }

        if (is_file($xml)) {
            if ($_GET["txtModelo"] == "55") {
                $docxml = FilesFolders::readFile($xml);
                $danfe = new Danfe($docxml, 'P', 'A4', $pathLogo, 'I', '');
                $id = $danfe->montaDANFE();
                $teste = $danfe->printDANFE($id . '.pdf', 'I');
            } else {
                $docxml = file_get_contents($xml);
                $danfe = new Danfce($docxml, $pathLogo, 2);
                $id = $danfe->montaDANFE(false);
                $teste = $danfe->printDANFE('pdf', $id . '.pdf', 'I');
            }
        }
    } else {
        echo "Json inválido!";
    }
} else {
    echo "Número inválido";
}

odbc_close($con);
