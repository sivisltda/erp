<?php include("form/FormEmitir-Nota-Servico.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Nota Fiscal Eletrônica</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>               
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>        
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header" style="display: inline-block;">
                        <div class="icon"><span class="ico-arrow-right"></span></div>
                        <h1 style="margin-top:8px">Emissão de Nota Fiscal</h1>
                        <div style="text-align: right; margin-top: 28px;<?php
                        if ($tpAmb == "0") {
                            echo "color: green";
                        } else {
                            echo "color: #ff7834";
                        }
                        ?>;font-weight: bold;"><?php
                             if ($tpAmb == "0") {
                                 echo "Ambiente de Homologação";
                             } else {
                                 echo "Ambiente de Produção";
                             }
                             ?></div>
                    </div>
                    <div class="row-fluid">
                        <form action="FormEmitir-Nota-Servico.php?<?php echo $FinalUrl; ?>" method="POST" name="frmEnviaDados" id="frmEnviaDados">
                            <div class="span12">
                                <div class="block">
                                    <div id="formulario">
                                        <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                                        <div style="width:23%; float:left">
                                            <span>Status:</span>
                                            <span style="width:100%; float:left">
                                                <input type="text" name="txtStatus" id="txtStatus" class="input-medium" value="<?php echo $status_nfe; ?>" disabled>
                                            </span>
                                        </div>
                                        <div style="width:65%; float:left; margin-left:1%">
                                            <span>Chave de Acesso:</span>
                                            <span style="width:100%; float:left">
                                                <input type="text" name="txtChave" id="txtChave" class="input-medium" value="<?php echo $chave_nfe; ?>" readonly>
                                            </span>
                                        </div>
                                        <div style="width:10%; float:left; margin-left:1%">
                                            <span>Número:</span>
                                            <span>
                                                <input type="text" name="txtNumGlobal" id="txtNumGlobal" maxlength="9" class="input-medium" value="<?php echo $numero_nfe; ?>" disabled>
                                            </span>
                                        </div>                                        
                                        <div style="clear:both"></div>
                                        <div class="data-fluid tabbable" style="margin-top:10px; margin-bottom:15px">
                                            <ul class="nav nav-tabs" style="margin-bottom:0px">
                                                <li class="<?php echo $active1; ?>"><a href="#tab1" data-toggle="tab"><b>Dados da NFS</b></a></li>
                                                <li><a href="#tab2" data-toggle="tab"><b>Emitente</b></a></li>
                                                <li><a href="#tab3" data-toggle="tab"><b>Destinatário</b></a></li>
                                                <li><a href="#tab4" data-toggle="tab"><b>Serviços</b></a></li>
                                                <li><a href="#tab5" data-toggle="tab"><b>Totais</b></a></li>
                                            </ul>
                                            <div class="tab-content" style="overflow:hidden; border:1px solid #ddd; border-top:0px; padding-top:10px; background:#F6F6F6;">
                                                <div class="tab-pane <?php echo $active1; ?>" id="tab1" style="margin-bottom:5px">
                                                    <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>                                                    
                                                    <div style="width:7%; float:left; margin-left:1%">
                                                        <span>* Série:</span>
                                                        <span style="width:100%; float:left">
                                                            <input type="text" name="txtSerie" id="txtSerie" maxlength="3" class="input-medium" style="text-align:right" value="<?php echo $serie_nfe; ?>" <?php echo $disabled; ?>>
                                                        </span>
                                                    </div>
                                                    <div style="width:16%; float:left; margin-left:1%">
                                                        <span>* Número NFs:</span>
                                                        <span style="width:100%; float:left">
                                                            <input type="text" name="txtNumNfe" id="txtNumNfe" maxlength="9" class="input-medium" style="text-align:right" value="<?php echo $numero_nfe; ?>" <?php echo $disabled; ?>>
                                                        </span>
                                                    </div>
                                                    <div style="width:13%; float:left; margin-left:1%">
                                                        <span>* Data de Emissão:</span>
                                                        <span>
                                                            <input type="text" name="txtDtEmissao" id="txtDtEmissao" class="datepicker inputCenter" value="<?php echo $data_emissao; ?>" <?php echo $disabled; ?>>
                                                        </span>
                                                    </div>
                                                    <div style="width:8%; float:left; margin-left:1%">
                                                        <span>* Estado:</span>
                                                        <span>
                                                            <select name="txtEstadoNFE" id="txtEstadoNFE" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                                <option value="">UF</option>
                                                                <?php
                                                                $query = "";
                                                                if ($disabled == 'disabled' && is_numeric($estadoNFE)) {
                                                                    $query = " and estado_codigo = " . $estadoNFE;
                                                                }
                                                                $cur = odbc_exec($con, "select estado_codigo,estado_sigla from tb_estados where estado_codigo > 0 " . $query . " order by estado_nome") or die(odbc_errormsg());
                                                                while ($RFP = odbc_fetch_array($cur)) {
                                                                    ?>
                                                                    <option value="<?php echo $RFP['estado_codigo'] ?>"<?php
                                                                    if (!(strcmp($RFP['estado_codigo'], $estadoNFE))) {
                                                                        echo "SELECTED";
                                                                    }
                                                                    ?>><?php echo utf8_encode($RFP['estado_sigla']) ?></option>
                                                                        <?php } ?>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:16%; float:left; margin-left:1%">
                                                        <span>* Cidade de Recolhimento:</span>
                                                        <span><span id="carregandoNFE" name="carregandoNFE" style="color:#666; display:none">Aguarde, carregando...</span>
                                                            <select name="txtCidadeNFE" id="txtCidadeNFE" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                                <option value="" >Selecione a Cidade</option>
                                                                <?php
                                                                $query = "";
                                                                if ($disabled == 'disabled') {
                                                                    $query = " and cidade_codigo = " . $cidadeNFE;
                                                                }
                                                                if ($estadoNFE != '') {
                                                                    $sql = "select cidade_codigo,cidade_nome from tb_cidades where cidade_codigo > 0 " . $query . " AND cidade_codigoEstado = " . $estadoNFE . " ORDER BY cidade_nome";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                        <option value="<?php echo $RFP['cidade_codigo'] ?>"<?php
                                                                        if (!(strcmp($RFP['cidade_codigo'], $cidadeNFE))) {
                                                                            echo "SELECTED";
                                                                        }
                                                                        ?>><?php echo utf8_encode($RFP['cidade_nome']) ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:100%; float:left; margin-left:1%">
                                                        <span>* Enquadramento do Serviço:</span>
                                                        <span>
                                                            <select name="txtTipoDoc" id="txtTipoDoc" class="select" style="width:98%" <?php echo $disabled; ?>>
                                                                <option value="">Selecione o Enquadramento</option>
                                                                <?php
                                                                $cur = odbc_exec($con, "select id_enq,codigo_enq,descricao_enq from sf_nfs_enquadramento order by codigo_enq") or die(odbc_errormsg());
                                                                while ($RFP = odbc_fetch_array($cur)) {
                                                                    ?>
                                                                    <option value="<?php echo $RFP['id_enq'] ?>"<?php
                                                                    if (!(strcmp($RFP['id_enq'], $tipo_doc))) {
                                                                        echo "SELECTED";
                                                                    }
                                                                    ?>><?php echo utf8_encode($RFP['codigo_enq']) . " - " . utf8_encode($RFP['descricao_enq']) ?></option>
                                                                        <?php } ?>
                                                            </select>
                                                        </span>
                                                    </div>                                                    
                                                    <div style="clear:both"></div>                                                                                                        
                                                </div>
                                                <div class="tab-pane" id="tab2" style="margin-bottom:5px">
                                                    <div style="width:49%; float:left; margin-left:0.5%">
                                                        <?php
                                                        $EmitCNPJ = '';
                                                        $EmitRazao = '';
                                                        $EmitFantasia = '';
                                                        $EmitIE = '';
                                                        $EmitIEST = '';
                                                        $EmitIM = '';
                                                        $EmitCNAE = '';
                                                        $EmitRTrib = '';
                                                        $EmitLogradouro = '';
                                                        $EmitNumero = '';
                                                        $EmitComplemento = '';
                                                        $EmitCEP = '';
                                                        $EmitBairro = '';
                                                        $EmitEstado = '';
                                                        $EmitCidade = '';
                                                        ?>
                                                        <span>Emitente:</span>
                                                        <span>
                                                            <select name="txtEmitente" id="txtEmitente" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                                <option value="">--Selecione--</option>
                                                                <?php
                                                                $cur = odbc_exec($con, "select * from sf_fornecedores_despesas where tipo in ('M') order by razao_social") or die(odbc_errormsg());
                                                                while ($RFP = odbc_fetch_array($cur)) {
                                                                    ?>
                                                                    <option value="<?php echo $RFP['id_fornecedores_despesas'] ?>"<?php
                                                                    if (!(strcmp($RFP['id_fornecedores_despesas'], $Cod_Emitente))) {
                                                                        $EmitCNPJ = utf8_encode($RFP['cnpj']);
                                                                        $EmitRazao = utf8_encode($RFP['razao_social']);
                                                                        $EmitFantasia = utf8_encode($RFP['nome_fantasia']);
                                                                        $EmitIE = utf8_encode($RFP['inscricao_estadual']);
                                                                        $EmitIEST = utf8_encode($RFP['ie_tributario']);
                                                                        $EmitIM = utf8_encode($RFP['inscricao_municipal']);
                                                                        $EmitCNAE = utf8_encode($RFP['cnae_fiscal']);
                                                                        $EmitRTrib = utf8_encode($RFP['regime_tributario']);
                                                                        $EmitLogradouro = utf8_encode($RFP['endereco']);
                                                                        $EmitNumero = utf8_encode($RFP['numero']);
                                                                        $EmitComplemento = utf8_encode($RFP['complemento']);
                                                                        $EmitCEP = utf8_encode($RFP['cep']);
                                                                        $EmitBairro = utf8_encode($RFP['bairro']);
                                                                        $EmitEstado = utf8_encode($RFP['estado']);
                                                                        $EmitCidade = utf8_encode($RFP['cidade']);
                                                                        echo "SELECTED";
                                                                    }
                                                                    ?>><?php
                                                                                echo utf8_encode($RFP['razao_social']);
                                                                                if ($RFP['cnpj'] != "") {
                                                                                    echo " (" . utf8_encode($RFP['cnpj']) . ")";
                                                                                }
                                                                                ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:49%; float:left; margin-left:1%">
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:24%; float:left; margin-left:0.5%">
                                                        <span>* CNPJ:</span>
                                                        <span><input type="text" name="txtEmitCNPJ" id="txtEmitCNPJ" value="<?php echo $EmitCNPJ; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:74%; float:left; margin-left:1%">
                                                        <span>* Razão Social:</span>
                                                        <span><input type="text" name="txtEmitRazaoSocial" id="txtEmitRazaoSocial" value="<?php echo $EmitRazao; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:54%; float:left; margin-left:0.5%">
                                                        <span>Nome Fantasia:</span>
                                                        <span><input type="text" name="txtEmitNomeFantasia" id="txtEmitNomeFantasia" value="<?php echo $EmitFantasia; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:22%; float:left; margin-left:0.5%">
                                                        <span>* Inscrição Estadual:</span>
                                                        <span><input type="text" name="txtEmitInscricaoEstadual" id="txtEmitInscricaoEstadual" value="<?php echo $EmitIE; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:22%; float:left; margin-left:0.5%">
                                                        <span>Insc. Est. do Subst. Tributário:</span>
                                                        <span><input type="text" name="txtEmitInscEstSubTrib" id="txtEmitInscEstSubTrib" value="<?php echo $EmitIEST; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:33%; float:left; margin-left:0.5%">
                                                        <span>Inscrição Municipal:</span>
                                                        <span><input type="text" name="txtEmitInscMunicipal" id="txtEmitInscMunicipal" value="<?php echo $EmitIM; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:32%; float:left; margin-left:0.5%">
                                                        <span>CNAE:</span>
                                                        <span><input type="text" name="txtEmitCNAE" id="txtEmitCNAE" value="<?php echo $EmitCNAE; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:32.5%; float:left; margin-left:1%">
                                                        <span>* Regime Tributário:</span>
                                                        <span>
                                                            <select name="txtEmitRegTrib" id="txtEmitRegTrib" class="select" style="width:100%" disabled>
                                                                <option value="S" <?php
                                                                if ($EmitRTrib == "S") {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Simples Nacional</option>
                                                                <option value="E" <?php
                                                                if ($EmitRTrib == "E") {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Simples Nacional - Excesso de Sublimite de Receita Bruta</option>
                                                                <option value="R" <?php
                                                                if ($EmitRTrib == "R") {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Regime Nacional</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:42%; float:left; margin-left:0.5%">
                                                        <span>* Logradouro:</span>
                                                        <span><input type="text" name="txtEmitEndereco" id="txtEmitEndereco" value="<?php echo $EmitLogradouro; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:7%; float:left; margin-left:1%">
                                                        <span>* Número:</span>
                                                        <span><input type="text" name="txtEmitNumero" id="txtEmitNumero" value="<?php echo $EmitNumero; ?>" class="input-medium" maxlength="6" disabled/></span>
                                                    </div>
                                                    <div style="width:34%; float:left; margin-left:1%">
                                                        <td align="right">Complemento:</td>
                                                        <td><input type="text" name="txtEmitComplemento" id="txtEmitComplemento" value="<?php echo $EmitComplemento; ?>" class="input-medium" maxlength="20" disabled/></td>
                                                    </div>
                                                    <div style="width:13%; float:left; margin-left:1%">
                                                        <span>* CEP:</span>
                                                        <span><input type="text" name="txtEmitCep" id="txtEmitCep" value="<?php echo $EmitCEP; ?>" class="input-medium" maxlength="9" disabled/></span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:32%; float:left; margin-left:0.5%">
                                                        <span>* Bairro:</span>
                                                        <span><input type="text" name="txtEmitBairro" id="txtEmitBairro" value="<?php echo $EmitBairro; ?>" class="input-medium" maxlength="20" disabled/></span>
                                                    </div>
                                                    <div style="width:32.5%; float:left; margin-left:1%">
                                                        <span>* Estado:</span>
                                                        <span>
                                                            <select name="txtEmitEstado" id="txtEmitEstado" class="select" style="width:100%" disabled>
                                                                <option value="null" >Selecione o estado</option>
                                                                <?php
                                                                $query = "";
                                                                if ($disabled == 'disabled' && is_numeric($EmitEstado)) {
                                                                    $query = " and estado_codigo = " . $EmitEstado;
                                                                }
                                                                $cur = odbc_exec($con, "select estado_codigo,estado_nome from tb_estados where estado_codigo > 0 " . $query . " order by estado_nome") or die(odbc_errormsg());
                                                                while ($RFP = odbc_fetch_array($cur)) {
                                                                    ?>
                                                                    <option value="<?php echo $RFP['estado_codigo'] ?>"<?php
                                                                    if (!(strcmp($RFP['estado_codigo'], $EmitEstado))) {
                                                                        echo "SELECTED";
                                                                    }
                                                                    ?>><?php echo utf8_encode($RFP['estado_nome']) ?></option>
                                                                        <?php } ?>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:32.5%; float:left; margin-left:1%">
                                                        <span>* Cidade:</span>
                                                        <span><span id="carregandoEmit" name="carregandoEmit" style="color:#666; display:none">Aguarde, carregando...</span>
                                                            <select name="txtEmitCidade" id="txtEmitCidade" class="select" style="width:100%" disabled>
                                                                <option value="null" >Selecione a cidade</option>
                                                                <?php
                                                                $query = "";
                                                                if ($disabled == 'disabled') {
                                                                    $query = " and cidade_codigo = " . $EmitCidade;
                                                                }
                                                                if ($EmitEstado != '') {
                                                                    $sql = "select cidade_codigo,cidade_nome from tb_cidades where cidade_codigo > 0 " . $query . " AND cidade_codigoEstado = " . $EmitEstado . " ORDER BY cidade_nome";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                                        ?>
                                                                        <option value="<?php echo $RFP['cidade_codigo'] ?>"<?php
                                                                        if (!(strcmp($RFP['cidade_codigo'], $EmitCidade))) {
                                                                            echo "SELECTED";
                                                                        }
                                                                        ?>><?php echo utf8_encode($RFP['cidade_nome']) ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                </div>
                                                <div class="tab-pane" id="tab3" style="margin-bottom:5px">
                                                    <div style="width:49%; float:left; margin-left:0.5%">
                                                        <?php
                                                        $DestCNPJ = '';
                                                        $DestRazao = '';
                                                        $DestIE = '';
                                                        $DestIsento = '';
                                                        $DestSuframa = '';
                                                        $DestLogradouro = '';
                                                        $DestNumero = '';
                                                        $DestComplemento = '';
                                                        $DestCEP = '';
                                                        $DestBairro = '';
                                                        $DestEstado = '';
                                                        $DestCidade = '';
                                                        ?>
                                                        <span>Cliente:</span>
                                                        <span>
                                                            <?php
                                                            if (is_numeric($Cod_Destinatario)) {
                                                                $cur = odbc_exec($con, "select *, 
                                                                            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato in (0,1) and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato,
                                                                            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = id_fornecedores_despesas) email_contato
                                                                         from sf_fornecedores_despesas where tipo in ('C','F','T') and id_fornecedores_despesas = " . $Cod_Destinatario . " order by razao_social") or die(odbc_errormsg());
                                                                while ($RFP = odbc_fetch_array($cur)) {
                                                                    $DestCNPJ = utf8_encode($RFP['cnpj']);
                                                                    $DestRazao = utf8_encode($RFP['razao_social']);
                                                                    $Nome_Destinatario = utf8_encode($RFP['razao_social']);
                                                                    if ($RFP['nome_fantasia'] != "") {
                                                                        $Nome_Destinatario = $Nome_Destinatario . " (" . utf8_encode($RFP['nome_fantasia']) . ")";
                                                                    }
                                                                    $DestIE = utf8_encode($RFP['inscricao_estadual']);
                                                                    $DestIsento = utf8_encode($RFP['regime_tributario']);
                                                                    $DestSuframa = utf8_encode($RFP['ie_tributario']);
                                                                    $DestMunicipal = utf8_encode($RFP['inscricao_municipal']);
                                                                    $DestEmail = utf8_encode($RFP['email_contato']);
                                                                    $DestFone = utf8_encode($RFP['telefone_contato']);
                                                                    $DestLogradouro = utf8_encode($RFP['endereco']);
                                                                    $DestNumero = utf8_encode($RFP['numero']);
                                                                    $DestComplemento = utf8_encode($RFP['complemento']);
                                                                    $DestCEP = utf8_encode($RFP['cep']);
                                                                    $DestBairro = utf8_encode($RFP['bairro']);
                                                                    $DestEstado = utf8_encode($RFP['estado']);
                                                                    $DestCidade = utf8_encode($RFP['cidade']);
                                                                }
                                                            }
                                                            ?>
                                                            <input name="txtFonecedor" id="txtFonecedor" value="<?php echo $Cod_Destinatario; ?>" type="hidden"/>
                                                            <input type="text" name="txtDesc" id="txtDesc" class="inputbox" value="<?php echo $Nome_Destinatario; ?>" size="10" <?php echo $disabled; ?> style="width: 100%"/>
                                                        </span>
                                                    </div>
                                                    <div style="width:49%; float:left; margin-left:1%">
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:24%; float:left; margin-left:0.5%">
                                                        <span>* CNPJ/CPF:</span>
                                                        <span><input type="text" name="txtCNPJ" id="txtCNPJ" value="<?php echo $DestCNPJ; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:74%; float:left; margin-left:1%">
                                                        <span>* Razão Social/Nome:</span>
                                                        <span><input type="text" name="txtRazaoSocial" id="txtRazaoSocial" value="<?php echo $DestRazao; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:18%; float:left; margin-left:0.5%">
                                                        <span>Tipo de Contribuinte:</span>
                                                        <span>
                                                            <select name="txtIsentoICMS" id="txtIsentoICMS" class="select" style="width:100%" disabled>
                                                                <option value="1" <?php
                                                                if ($DestIsento == 1) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Contribuinte do ICMS</option>
                                                                <option value="2" <?php
                                                                if ($DestIsento == 2) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Contribuinte ISENTO</option>
                                                                <option value="9" <?php
                                                                if ($DestIsento == 9) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Não Contribuinte</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:20%; float:left; margin-left:1%">
                                                        <span>Inscrição Estadual:</span>
                                                        <span><input type="text" name="txtInscricaoEstadual" id="txtInscricaoEstadual" value="<?php echo $DestIE; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:13%; float:left; margin-left:1%">
                                                        <span>Inscrição SUFRAMA:</span>
                                                        <span><input type="text" name="txtSUFRAMA" id="txtSUFRAMA" value="<?php echo $DestSuframa; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:14%; float:left; margin-left:1%">
                                                        <span>Inscrição Municipal:</span>
                                                        <span><input type="text" name="txtMunicipalDest" id="txtMunicipalDest" value="<?php echo $DestMunicipal; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:30%; float:left; margin-left:1%">
                                                        <span>E-mail:</span>
                                                        <span><input type="text" name="txtEmailDest" id="txtEmailDest" value="<?php echo $DestEmail; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:42%; float:left; margin-left:0.5%">
                                                        <span>* Logradouro:</span>
                                                        <span><input type="text" name="txtEndereco" id="txtEndereco" value="<?php echo $DestLogradouro; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:7%; float:left; margin-left:1%">
                                                        <span>* Número:</span>
                                                        <span><input type="text" name="txtNumero" id="txtNumero" value="<?php echo $DestNumero; ?>" class="input-medium" maxlength="6" disabled/></span>
                                                    </div>
                                                    <div style="width:34%; float:left; margin-left:1%">
                                                        <td align="right">Complemento:</td>
                                                        <td><input type="text" name="txtComplemento" id="txtComplemento" value="<?php echo $DestComplemento; ?>" class="input-medium" maxlength="20" disabled/></td>
                                                    </div>
                                                    <div style="width:13%; float:left; margin-left:1%">
                                                        <span>CEP:</span>
                                                        <span><input type="text" name="txtCep" id="txtCep" value="<?php echo $DestCEP; ?>" class="input-medium" maxlength="9" disabled/></span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:30%; float:left; margin-left:0.5%">
                                                        <span>* Bairro:</span>
                                                        <span><input type="text" name="txtBairro" id="txtBairro" value="<?php echo $DestBairro; ?>" class="input-medium" maxlength="20" disabled/></span>
                                                    </div>
                                                    <div style="width:19%; float:left; margin-left:1%">
                                                        <span>* Estado:</span>
                                                        <span>
                                                            <select name="txtEstado" id="txtEstado" class="select" style="width:100%" disabled>
                                                                <option value="null" >Selecione o estado</option>
                                                                <?php
                                                                $query = "";
                                                                if ($disabled == 'disabled' && is_numeric(($DestEstado))) {
                                                                    $query = " and estado_codigo = " . $DestEstado;
                                                                }
                                                                $cur = odbc_exec($con, "select estado_codigo,estado_nome from tb_estados where estado_codigo > 0 " . $query . " order by estado_nome") or die(odbc_errormsg());
                                                                while ($RFP = odbc_fetch_array($cur)) {
                                                                    ?>
                                                                    <option value="<?php echo $RFP['estado_codigo'] ?>"<?php
                                                                    if (!(strcmp($RFP['estado_codigo'], $DestEstado))) {
                                                                        echo "SELECTED";
                                                                    }
                                                                    ?>><?php echo utf8_encode($RFP['estado_nome']) ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:32.5%; float:left; margin-left:1%">
                                                        <span>* Cidade:</span>
                                                        <span><span id="carregando" name="carregando" style="color:#666; display:none">Aguarde, carregando...</span>
                                                            <select name="txtCidade" id="txtCidade" class="select" style="width:100%" disabled>
                                                                <option value="null" >Selecione a cidade</option>
                                                                <?php
                                                                $query = "";
                                                                if ($disabled == 'disabled' && is_numeric($DestCidade)) {
                                                                    $query = " and cidade_codigo = " . $DestCidade;
                                                                }
                                                                if ($DestEstado != '') {
                                                                    $sql = "select cidade_codigo,cidade_nome from tb_cidades where cidade_codigo > 0 " . $query . " AND cidade_codigoEstado = " . $DestEstado . " ORDER BY cidade_nome";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                    <option value="<?php echo $RFP['cidade_codigo'] ?>"<?php
                                                                    if (!(strcmp($RFP['cidade_codigo'], $DestCidade))) {
                                                                        echo "SELECTED";
                                                                    }
                                                                    ?>><?php echo utf8_encode($RFP['cidade_nome']) ?></option>
                                                                            <?php
                                                                        }
                                                                    } ?>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:14.5%; float:left; margin-left:1%">
                                                        <span>Fone:</span>
                                                        <span><input type="text" name="txtFone" id="txtFone" value="<?php echo $DestFone; ?>" class="input-medium" maxlength="20" disabled/></span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                </div>
                                                <div class="tab-pane" id="tab4" style="margin-bottom:5px">
                                                    <table class="table" cellpadding="0" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th width="45%">Serviços:</th>
                                                                <th width="10%">Qtd:</th>
                                                                <th width="17.5%">Valor Unitário:</th>
                                                                <th width="17.5%">Valor Total:</th>
                                                                <th width="10%">Ação:</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <input name="txtConta" id="txtConta" type="hidden"/>
                                                                    <input type="text" name="txtDescConta" id="txtDescConta" class="inputbox" size="10" <?php echo $disabled; ?> style="width: 100%;"/>
                                                                </td>
                                                                <td><input name="txtQtdProduto" id="txtQtdProduto" <?php echo $disabled; ?> type="text" class="input-medium inputCenter" value="1" onBlur="prodValor()"/></td>
                                                                <td><input name="hidValorProduto" id="hidValorProduto" <?php echo $disabled; ?> type="text" class="input-medium inputCenter" value="<?php echo escreverNumero(0); ?>" onBlur="prodValor()"/></td>
                                                                <td><input name="txtValorProduto" id="txtValorProduto" type="text" class="input-medium inputCenter" value="<?php echo escreverNumero(0); ?>" disabled /></td>                                                                
                                                                <td><center><button class="btn btn-primary" id="for-block3" <?php echo $disabled; ?> type="button" onClick="novaLinha()">+ Adicionar</button></center></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <table class="table" style="margin-top:-7px" cellpadding="0" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th width="45%"></th>
                                                                <th width="10%"></th>
                                                                <th width="17.5%"></th>
                                                                <th width="17.5%"></th>
                                                                <th width="10%"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tabela_produto">
                                                            <?php
                                                            $i = 0;
                                                            $idTP = 0;
                                                            if ($PegaURL != "") {
                                                                $cur = odbc_exec($con, "select cod_item_nota id_item_venda,conta_movimento grupo,gc.descricao grupodesc,conta_produto produto,p.descricao produtodesc,
                                                                qtd_comercial quantidade,tot_val_bruto valor_total,icms_sit_trib,icms_origem,icms_sit_trib, 
                                                                cenario,ncm,codigo_interno,tot_frete, i.cfop, cenario
                                                                from sf_nfe_notas_itens i inner join sf_produtos p 
                                                                left join sf_contas_movimento gc on p.conta_movimento = gc.id_contas_movimento
                                                                on p.conta_produto = i.cod_prod where p.tipo in ('S','C') and cod_nota = " . $PegaURL);
                                                                while ($RFP = odbc_fetch_array($cur)) {
                                                                    $TotNota = $TotNota + $RFP['valor_total'];
                                                                    $TotDesconto = $TotDesconto + $RFP['tot_desconto'];
                                                                    ?>
                                                                    <tr id='tabela_linha_<?php echo $i; ?>'>
                                                                        <td style="vertical-align: middle;">
                                                                            <input name='txtIP_<?php echo $i; ?>' id='txtIP_<?php echo $i; ?>' type='hidden' value='<?php echo $RFP['id_item_venda']; ?>'/>
                                                                            <input name='txtPD_<?php echo $i; ?>' id='txtPD_<?php echo $i; ?>' type='hidden' value='<?php echo $RFP['produto']; ?>'/>
                                                                            &nbsp;&nbsp;&nbsp;<span <?php ?>><?php echo utf8_encode($RFP['produtodesc']); ?></span>
                                                                        </td>
                                                                        <td style="vertical-align: middle;" id='myVQ_<?php echo $i; ?>'><input name='txtVQ_<?php echo $i; ?>' id='txtVQ_<?php echo $i; ?>' type='hidden' value='<?php echo escreverNumero($RFP['quantidade'],0, 0); ?>'/><center><?php echo escreverNumero($RFP['quantidade'],0, 0); ?></center></td>
                                                                <td style='text-align: center; vertical-align: middle;'>
                                                                    <input name='txtVU_<?php echo $i; ?>' id='txtVU_<?php echo $i; ?>' type='hidden' value='<?php
                                                                    if ($RFP['quantidade'] > 0) {
                                                                        $valor_unitario = $RFP['valor_total'] / $RFP['quantidade'];
                                                                    } else {
                                                                        $valor_unitario = 0;
                                                                    }
                                                                    echo escreverNumero($valor_unitario);
                                                                    ?>'/><?php echo escreverNumero($valor_unitario); ?>
                                                                </td>
                                                                <td id='myVP_<?php echo $i; ?>' style='text-align: center;    vertical-align: middle;'>
                                                                    <input name='txtVP_<?php echo $i; ?>' id='txtVP_<?php echo $i; ?>' type='hidden' value='<?php
                                                                    $totalp = $totalp + $RFP['valor_total'];
                                                                    echo escreverNumero($RFP['valor_total']);
                                                                    ?>'/><?php echo escreverNumero($RFP['valor_total']); ?>
                                                                </td>                                                                
                                                                <td><center>
                                                                    <button style="margin: 0px;padding: 2px 4px 3px 4px;line-height: 10px;" <?php echo $disabled; ?> class='btn red' type='button' onClick="removeLinha('tabela_linha_<?php echo $i; ?>')">X</button>
                                                                </center>
                                                                </td>
                                                                </tr>
                                                                <?php
                                                                $i++;
                                                            }
                                                            $idTP = $i;
                                                        } else {
                                                            if (isset($_GET['vd']) && is_numeric($_GET['vd'])) {
                                                                $cur = odbc_exec($con, "select id_item_venda,conta_movimento grupo,gc.descricao grupodesc,conta_produto produto,cm.descricao produtodesc,
                                                                quantidade,valor_total,cenario, cfop, codigo_interno, ncm from sf_vendas_itens vi left join sf_contas_movimento gc on vi.grupo = gc.id_contas_movimento inner join sf_produtos cm 
                                                                on cm.conta_produto = vi.produto where cm.tipo in ('S','C') and id_venda = '" . $_GET['vd'] . "'");
                                                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                    <tr id='tabela_linha_<?php echo $i; ?>'>
                                                                        <td style="vertical-align: middle;">
                                                                            <input name='txtIP_<?php echo $i; ?>' id='txtIP_<?php echo $i; ?>' type='hidden' value=''/>
                                                                            <input name='txtPD_<?php echo $i; ?>' id='txtPD_<?php echo $i; ?>' type='hidden' value='<?php echo $RFP['produto']; ?>'/>
                                                                            &nbsp;&nbsp;&nbsp;<span><?php echo utf8_encode($RFP['produtodesc']); ?></span>
                                                                        </td>
                                                                        <td style="vertical-align: middle;" id='myVQ_<?php echo $i; ?>'><input name='txtVQ_<?php echo $i; ?>' id='txtVQ_<?php echo $i; ?>' type='hidden' value='<?php echo escreverNumero($RFP['quantidade'],0 , 0); ?>'/><center><?php echo escreverNumero($RFP['quantidade'], 0, 0); ?></center></td>
                                                                    <td style='text-align: center;vertical-align: middle;'>
                                                                        <input name='txtVU_<?php echo $i; ?>' id='txtVU_<?php echo $i; ?>' type='hidden' value='<?php
                                                                        if ($RFP['quantidade'] > 0) {
                                                                            $valor_unitario = $RFP['valor_total'] / $RFP['quantidade'];
                                                                        } else {
                                                                            $valor_unitario = 0;
                                                                        }
                                                                        echo escreverNumero($valor_unitario);
                                                                        ?>'/><?php echo escreverNumero($valor_unitario); ?>
                                                                    </td>
                                                                    <td id='myVP_<?php echo $i; ?>' style='text-align: center;vertical-align: middle;'>
                                                                        <input name='txtVP_<?php echo $i; ?>' id='txtVP_<?php echo $i; ?>' type='hidden' value='<?php
                                                                        $totalp = $totalp + $RFP['valor_total'];
                                                                        echo escreverNumero($RFP['valor_total']);
                                                                        ?>'/><?php echo escreverNumero($RFP['valor_total']); ?>
                                                                    </td>                                                                    
                                                                    <td>
                                                                    <center>
                                                                        <button style="margin: 0px;padding: 2px 4px 3px 4px;line-height: 10px;" <?php echo $disabled; ?> class='btn red' type='button' onClick="removeLinha('tabela_linha_<?php echo $i; ?>')">X</button>
                                                                    </center>
                                                                    </td>
                                                                    </tr>
                                                                    <?php
                                                                    $i++;
                                                                }
                                                                $idTP = $i;
                                                            }
                                                        } ?>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr style="height: 20px;">
                                                                <td colspan="6" style="width:100%; background-color:#E9E9E9; margin:0; padding:8px;">
                                                                    <div id="txtTotalProd" style="width:200px; float:right; text-align:right; line-height:20px">
                                                                        Total sem Desconto: <?php echo escreverNumero($totalp, 1); ?><br>
                                                                        Valor do Desconto: <?php echo escreverNumero(($totalp * ($descontop / 100)), 1); ?><br>
                                                                        <b>Total com Desconto: <?php echo escreverNumero($totalp - ($totalp * ($descontop / 100)), 1); ?></b>
                                                                    </div>
                                                                    <div style="float:right">
                                                                        <span style="width:100%; display:block;">&nbsp;</span>
                                                                        <span style="width:100%; display:block;"><button class="btn btn-primary" id="bntProdDesconto" <?php echo $disabled; ?> type="button" style="width:35px; padding-bottom:3px; text-align:center"><span id="prod_sinal">%</span></button></span>
                                                                    </div>
                                                                    <div style="width:100px; float:right">
                                                                        <span style="width:100%; display:block;">Desconto:</span>
                                                                        <span style="width:100%; display:block;">
                                                                            <input name="txtProdDesconto" id="txtProdDesconto" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $descontop; ?>" onblur="somaTotal()"/>
                                                                            <input name="txtProdSinal" id="txtProdSinal" value="<?php echo $descontotp; ?>" type="hidden"/>
                                                                        </span>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>                                                    
                                                </div>
                                                <div class="tab-pane" id="tab5" style="margin-bottom:5px">
                                                    <div style="width:19%; float:left;">
                                                        <span>Valor do Serviço:</span>
                                                        <span><input type="text" name="txtTotNota" id="txtTotNota" value="<?php echo escreverNumero(($TotNota)); ?>" class="input-medium" style="text-align:right" readonly></span>
                                                    </div>
                                                    <div style="width:19%; float:left; margin-left:1%">
                                                        <span>Valor do Dedutível:</span>
                                                        <span><input type="text" name="txtTotDesconto" id="txtTotDesconto" value="<?php echo escreverNumero($TotDesconto); ?>" class="input-medium" style="text-align:right" readonly></span>
                                                    </div>                                                    
                                                    <div style="width:20%; float:left; margin-left:1%">
                                                        <span>Base de Cálculo:</span>
                                                        <span><input type="text" name="txtBaseCalc" id="txtBaseCalc" value="<?php echo escreverNumero($BaseCalc); ?>" class="input-medium" style="text-align:right" readonly></span>
                                                    </div>
                                                    <div style="width:19%; float:left; margin-left:1%">
                                                        <span>Alíquota(%):</span>
                                                        <span><input type="text" name="txtAliquota" id="txtAliquota" value="<?php echo escreverNumero($Aliquota); ?>" class="input-medium" style="text-align:right" readonly></span>
                                                    </div>
                                                    <div style="width:19%; float:left; margin-left:1%">
                                                        <span>Imposto:</span>
                                                        <span><input type="text" name="txtTotImposto" id="txtTotImposto" value="<?php echo escreverNumero($TotImposto); ?>" class="input-medium" style="text-align:right" readonly></span>
                                                    </div>
                                                    <div style="clear:both"></div>                                                                                                               
                                                    <div style="width:19%; float:left;">
                                                        <span>INSS(%):</span>
                                                        <span><input type="text" name="txtTotInss" id="txtTotInss" value="<?php echo escreverNumero($TotInss); ?>" class="input-medium" style="text-align:right" <?php echo $disabled; ?>></span>
                                                    </div>
                                                    <div style="width:19%; float:left; margin-left:1%">
                                                        <span>IR(%):</span>
                                                        <span><input type="text" name="txtTotIr" id="txtTotIr" value="<?php echo escreverNumero($TotIr); ?>" class="input-medium" style="text-align:right" <?php echo $disabled; ?>></span>
                                                    </div>
                                                    <div style="width:20%; float:left; margin-left:1%">
                                                        <span>CSLL(%):</span>
                                                        <span><input type="text" name="txtTotCsll" id="txtTotCsll" value="<?php echo escreverNumero($TotCsll); ?>" class="input-medium" style="text-align:right" <?php echo $disabled; ?>></span>
                                                    </div>                                                    
                                                    <div style="width:19%; float:left; margin-left:1%">
                                                        <span>PIS(%):</span>
                                                        <span><input type="text" name="txtPis" id="txtPis" value="<?php echo escreverNumero($Pis); ?>" class="input-medium" style="text-align:right" <?php echo $disabled; ?>></span>
                                                    </div>
                                                    <div style="width:19%; float:left; margin-left:1%">
                                                        <span>COFINS(%):</span>
                                                        <span><input type="text" name="txtCofins" id="txtCofins" value="<?php echo escreverNumero($Cofins); ?>" class="input-medium" style="text-align:right" <?php echo $disabled; ?>></span>
                                                    </div>
                                                    <div style="clear:both"></div>                                                    
                                                    <div style="width:19%; float:left;">
                                                        <span>Outras Retenções:</span>
                                                        <span><input type="text" name="txtOutDespesas" id="txtOutDespesas" value="<?php echo escreverNumero($OutDespesas); ?>" class="input-medium" style="text-align:right" <?php echo $disabled; ?>></span>
                                                    </div>                                                    
                                                    <div style="width:19%; float:left; margin-left:1%">
                                                        <span>Alíquota Federal(%):</span>
                                                        <span><input type="text" name="txtTotFed" id="txtTotFed" value="<?php echo escreverNumero($TotFed); ?>" class="input-medium" style="text-align:right" <?php echo $disabled; ?>></span>
                                                    </div>
                                                    <div style="width:20%; float:left; margin-left:1%">
                                                        <span>Alíquota Estadual(%):</span>
                                                        <span><input type="text" name="txtTotEst" id="txtTotEst" value="<?php echo escreverNumero($TotEst); ?>" class="input-medium" style="text-align:right" <?php echo $disabled; ?>></span>
                                                    </div>
                                                    <div style="width:19%; float:left; margin-left:1%">
                                                        <span>Alíquota Municipal(%):</span>
                                                        <span><input type="text" name="txtTotMun" id="txtTotMun" value="<?php echo escreverNumero($TotMun); ?>" class="input-medium" style="text-align:right" <?php echo $disabled; ?>></span>
                                                    </div>
                                                    <div style="width:20%; float:left; margin-left:1%">
                                                        <span style="width:100%; display:block;">&nbsp;</span>
                                                        <span style="width:100%; display:block;">&nbsp;</span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="clear:both"></div>
                                        <div style="margin-top:5px">
                                            <div style="width:49.5%; float:left">
                                                <span>* Discriminação do Serviço:</span>
                                                <span>
                                                    <textarea name="txtInfoFis" id="txtInfoFis" style="width:100%; height:100px" <?php echo $disabled; ?>><?php echo $Info_add_fisco; ?></textarea>
                                                </span>
                                            </div>
                                            <div style="width:49.5%; float:left; margin-left:1%">
                                                <span>Outras Informações:</span>
                                                <span>
                                                    <textarea name="txtInfoCon" id="txtInfoCon" style="width:100%; height:100px" <?php echo $disabled; ?>><?php echo $Info_add_complementar; ?></textarea>
                                                </span>
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span5" style="width:100%;">
                                    <div class="block">
                                        <div class="data-fluid">
                                            <table class="table" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td width="50%" colspan="2" style="background:none">
                                                            <span style="width:29%; display:block; float:left;"> * Responsável pela Autorização da NFS: </span>
                                                            <span style="width:15%; display:block; float:left; margin-left:1%;"> Status de Aprovação: </span>
                                                            <span style="width:33%; display:block; float:left; margin-left:1%;"> <?php if ($status_nfe == "Autorizada") { ?><label id="lblJust">Justificativa Cancelamento:</label><?php } else { ?> &nbsp; <?php } ?></span>
                                                            <span style="width:29%; display:block; float:left;">
                                                                <select style="width:100%" name="txtDestinatario" id="txtDestinatario" class="select" <?php echo $disabled; ?>>
                                                                    <option value="null">--Selecione--</option>
                                                                    <?php
                                                                    $query = "";
                                                                    if ($disabled == 'disabled') {
                                                                        $query = " and login_user = '" . $destinatario . "'";
                                                                    }
                                                                    $cur = odbc_exec($con, "select login_user,nome from sf_usuarios where inativo = 0 and login_user != 'Admin' " . $query . " order by nome") or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                                        ?>
                                                                        <option value="<?php echo $RFP['login_user'] ?>"<?php
                                                                        if (!(strcmp($RFP['login_user'], $destinatario))) {
                                                                            echo "SELECTED";
                                                                        }
                                                                        ?>><?php echo utf8_encode($RFP['nome']) ?></option>
                                                                            <?php } ?>
                                                                </select>
                                                            </span>
                                                            <span style="width:15%; display:block; float:left; margin-left: 1%">
                                                                <input type="text" name="txtStatusVenda" id="txtStatusVenda" class="input-medium" value="<?php echo $status; ?>" disabled />
                                                            </span>
                                                            <span style="width:33%; display:block; float:left; margin-left:1%" >
                                                                <?php if ($status_nfe == "Autorizada") { ?>
                                                                    <input type="text" name="txtJustificativa" maxlength="150" id="txtJustificativa" class="input-medium" value="<?php echo $justCancel; ?>" />
                                                                <?php } ?>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="spanBNT3" style="margin-left:10px">
                                            <div class="toolbar bottom tar">
                                                <div class="data-fluid">
                                                    <?php if ($status == 'Aprovado' && $status_nfe !== "Cancelada") { ?>
                                                        <div style="float:left">
                                                            <button id="btn_Assinar" <?php
                                                            if ($status_nfe == "Autorizada") {
                                                                echo "disabled";
                                                            }
                                                            ?> class="btn btn-success" type="button" onClick="assinar_nota();"><span class="ico-checkmark"></span> Transmitir Nota</button>
                                                            <button id="btn_Imprimir" <?php
                                                            if ($status_nfe != "Autorizada") {
                                                                echo "disabled";
                                                            }
                                                            ?> class="btn btn-primary" type="button"><span class="ico-print"></span> Imprimir</button>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if (($status == 'Aguarda') && ($_SESSION['login_usuario'] == $destinatario)) { ?>
                                                        <div style="float:left">
                                                            <button class="btn btn-success" type="submit" name="bntAprov" id="bntAprov" onClick="return validaForm();"><span class="ico-checkmark"></span> Aprovar</button>
                                                            <button class="btn red" type="submit" name="bntReprov" id="bntReprov"><span class=" ico-remove"></span> Reprovar</button>
                                                        </div>
                                                    <?php } ?>
                                                    <div class="btn-group">
                                                        <?php if ($_GET['idx'] != '') { ?>
                                                            <button class="btn  btn-warning" type="submit" name="bntVoltar" id="bntVoltar" value="Voltar"><span class="ico-backward"> </span> Voltar</button>
                                                        <?php } if ($disabled == '') { ?>
                                                            <button class="btn btn-success" type="submit" name="bntSave" id="bntSave"><span class="ico-checkmark"></span> Gravar</button>
                                                            <?php if ($_POST['txtId'] == '') { ?>
                                                                <button class="btn btn-success" onClick="history.go(-1)" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                                                            <?php } else { ?>
                                                                <button class="btn btn-success" type="submit" name="bntAlterar" id="bntAlterar" ><span class="ico-reply"></span> Cancelar</button>
                                                                <?php
                                                            }
                                                        } else { ?>
                                                            <?php if ($status == 'Aprovado' && $status_nfe !== "Cancelada") { ?>
                                                                <button <?php
                                                                if ($status_nfe !== "Autorizada" && $status_nfe !== "Cancelada") {
                                                                    echo "style=\"display: none;\"";
                                                                }
                                                                ?>class="btn btn-primary" type="button" id="btnXml" value="Imprimir"><span class="ico-barcode-2"></span> Xml</button>
                                                                <button <?php
                                                                if ($status_nfe !== "Autorizada" && $status_nfe !== "Cancelada") {
                                                                    echo "style=\"display: none;\"";
                                                                }
                                                                ?>class="btn btn-warning" type="button" onclick="cancelar_nota();" id="btnCancelarNFE"><span class="ico-remove"></span> Cancelar NFE </button>                                                           
                                                                <?php } ?>                                                                
                                                            <button class="btn btn-success" type="submit" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>                                                            
                                                            <button <?php
                                                            if ($status_nfe == "Autorizada") {
                                                                echo "style=\"display: none;\"";
                                                            }
                                                            ?> class="btn btn-success" type="submit" name="bntEdit" id="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                                                            <button <?php
                                                            if ($status_nfe == "Autorizada") {
                                                                echo "style=\"display: none;\"";
                                                            }
                                                            ?> class="btn red" type="submit" name="bntDelete" id="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                                                            <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input name="txtIdTP" id="txtIdTP" value="<?php echo $idTP; ?>" type="hidden"/>
                            <input name="txtVenda" id="txtVenda" value="<?php echo $idVenda; ?>" type="hidden"/>
                            <input name="txtLinkDownloadPDF" id="txtLinkDownloadPDF" value="<?php echo $linkDownloadPDF; ?>" type="hidden"/>
                            <input name="txtLinkDownloadXML" id="txtLinkDownloadXML" value="<?php echo $linkDownloadXML; ?>" type="hidden"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/highlight/jquery.highlight-4.js'></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/moment.min.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>     
        <script type="text/javascript" src="js/EmitirNota-Servico.js"></script>
        <script type="text/javascript" src="js/ProcessarNota-Servico.js"></script>
    </body>
    <?php odbc_close($con); ?>
</html>
