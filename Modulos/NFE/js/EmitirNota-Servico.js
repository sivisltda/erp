
$("#txtDtEmissao").mask(lang["dateMask"]);
$("#txtSerie").mask("999", {placeholder: ""});
$("#txtNumNfe").mask("9999999999", {placeholder: ""});
$("#txtCep").mask("99999-999");
$("#txtRebPlaca").mask("aaa-9999", {placeholder: ""});
$("#txtQtdProduto").mask("9999999999", {placeholder: ""});
$("#hidValorProduto, #txtValorProduto, #txtProdDesconto, #txtTotInss, #txtTotIr, " +
        "#txtTotCsll, #txtPis, #txtCofins, #txtOutDespesas, #txtTotFed, #txtTotEst, #txtTotMun").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});

var conta = $("#txtIdTP").val();
var cid_cod = 0;
var cid_nome = "";

$(function () {

    if ($("#txtProdSinal").val() === "1") {
        $("#prod_sinal").html("$");
    }
    somaTotal();

    $("#bntProdDesconto").click(function () {
        if ($("#prod_sinal").html() === "%") {
            $("#prod_sinal").html("$");
            $("#txtProdSinal").val("1");
        } else {
            $("#prod_sinal").html("%");
            $("#txtProdSinal").val("0");
        }
        somaTotal();
    });

    $('#txtEstadoNFE').change(function () {
        if ($(this).val()) {
            $('#txtCidadeNFE').hide();
            $('#carregandoNFE').show();
            $.getJSON('../Comercial/locais.ajax.php?search=', {txtEstado: $(this).val(), ajax: 'true'}, function (j) {
                var options = '<option value=""></option>';
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].cidade_codigo + '">' + j[i].cidade_nome + '</option>';
                }
                $('#txtCidadeNFE').html(options).show();
                $('#txtCidadeNFE').css("display", "none");
                $('#carregandoNFE').hide();
            });
        } else {
            $('#txtCidadeNFE').html('<option value="">Selecione a Cidade</option>');
        }
    });

    $('#txtEstado').change(function () {
        if ($(this).val()) {
            $('#txtCidade').hide();
            $('#carregando').show();
            $.getJSON('../Comercial/locais.ajax.php?search=', {txtEstado: $(this).val(), ajax: 'true'}, function (j) {
                var options = '<option value=""></option>';
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].cidade_codigo + '">' + j[i].cidade_nome + '</option>';
                }
                $('#txtCidade').html(options).show();
                $('#txtCidade').css("display", "none");
                $('#carregando').hide();
            });
        } else {
            $('#txtCidade').html('<option value="">Selecione a cidade</option>');
        }
    });

    $('#txtEmitEstado').change(function () {
        if ($(this).val()) {
            $('#txtEmitCidade').hide();
            $('#carregandoEmit').show();
            $.getJSON('../Comercial/locais.ajax.php?search=', {txtEstado: $(this).val(), ajax: 'true'}, function (j) {
                var options = '<option value=""></option>';
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].cidade_codigo + '">' + j[i].cidade_nome + '</option>';
                }
                $('#txtEmitCidade').html(options).show();
                $('#txtEmitCidade').css("display", "none");
                $('#carregandoEmit').hide();
            });
        } else {
            $('#txtEmitCidade').html('<option value="">Selecione a cidade</option>');
        }
    });

    $("#txtDescConta").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "produtos.ajax.php",
                dataType: "json",
                data: {
                    q: request.term,
                    t: 'S'
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            $('#txtConta').val(ui.item.id).show();
            $('#txtDescConta').val(ui.item.descricao).show();
            var preco = numberFormat(0);
            var qtdcom = '1';
            preco = ui.item.preco_venda;
            qtdcom = parseInt(ui.item.quantidade_comercial);
            $('#txtQtdProduto').val(qtdcom).show();
            $('#txtValorProduto').val(numberFormat(preco)).show();
            $('#hidValorProduto').val(numberFormat(preco)).show();
            prodValor();
        }
    });

    $("#txtDesc").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "./../CRM/ajax.php",
                dataType: "json",
                data: {
                    q: request.term,
                    t: "C','F','T"
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            $('#txtFonecedor').val(ui.item.id).show();
            $('#txtDesc').val(ui.item.label).show();
            if (ui.item.id !== "") {
                $.getJSON("endereco.ajax.php?search=", {txtFonecedor: ui.item.id, ajax: "true"}, function (j) {
                    for (var i = 0; i < j.length; i++) {
                        if (j[i].endereco !== "") {
                            $("#txtEndereco").val(j[i].endereco);
                        } else {
                            $("#txtEndereco").val("");
                        }
                        if (j[i].numero !== "") {
                            $("#txtNumero").val(j[i].numero);
                        } else {
                            $("#txtNumero").val("");
                        }
                        if (j[i].complemento !== "") {
                            $("#txtComplemento").val(j[i].complemento);
                        } else {
                            $("#txtComplemento").val("");
                        }
                        if (j[i].cep !== "") {
                            $("#txtCep").val(j[i].cep);
                        } else {
                            $("#txtCep").val("");
                        }
                        if (j[i].bairro !== "") {
                            $("#txtBairro").val(j[i].bairro);
                        } else {
                            $("#txtBairro").val("");
                        }
                        if (j[i].cnpj !== "") {
                            $("#txtCNPJ").val(j[i].cnpj);
                        } else {
                            $("#txtCNPJ").val("");
                        }
                        if (j[i].razao !== "") {
                            $("#txtRazaoSocial").val(j[i].razao);
                        } else {
                            $("#txtRazaoSocial").val("");
                        }
                        if (j[i].estadual !== "") {
                            $("#txtInscricaoEstadual").val(j[i].estadual);
                        } else {
                            $("#txtInscricaoEstadual").val("");
                        }
                        if (j[i].ietributo !== "") {
                            $("#txtSUFRAMA").val(j[i].ietributo);
                        } else {
                            $("#txtSUFRAMA").val("");
                        }
                        if (j[i].municipal !== "") {
                            $("#txtMunicipalDest").val(j[i].municipal);
                        } else {
                            $("#txtMunicipalDest").val("");
                        }
                        if (j[i].regime !== "") {
                            $("#txtIsentoICMS").val(j[i].regime);
                            var IsentoOpt = $("#txtIsentoICMS option[value='" + j[i].regime + "']").text();
                            $("#s2id_txtIsentoICMS > .select2-choice > span").html(IsentoOpt);
                        } else {
                            $("#txtIsentoICMS").val("9");
                            $("#s2id_txtIsentoICMS > .select2-choice > span").html("Não Contribuinte");
                        }
                        if (j[i].email !== "") {
                            $("#txtEmailDest").val(j[i].email);
                        } else {
                            $("#txtEmailDest").val("");
                        }
                        if (j[i].fone !== "") {
                            $("#txtFone").val(j[i].fone);
                        } else {
                            $("#txtFone").val("");
                        }
                        if (j[i].estado !== "") {
                            $("#txtEstado").val(j[i].estado);
                            $("#s2id_txtEstado > .select2-choice > span").html(j[i].estado_nome);
                        } else {
                            $("#txtEstado").val("");
                            $("#s2id_txtEstado > .select2-choice > span").html("Selecione o estado");
                        }
                        cid_cod = j[i].cidade;
                        cid_nome = j[i].cidade_nome;
                        $.getJSON("../Comercial/locais.ajax.php?search=", {txtEstado: $("#txtEstado").val(), ajax: "true"}, function (j) {
                            var options = "<option value=\"\"></option>";
                            for (var i = 0; i < j.length; i++) {
                                options += "<option value=\"" + j[i].cidade_codigo + "\">" + j[i].cidade_nome + "</option>";
                            }
                            $("#txtCidade").html(options).show();
                            $("#txtCidade").css("display", "none");
                        }).done(function () {
                            if (cid_cod !== "") {
                                $("#txtCidade").val(cid_cod);
                                $("#s2id_txtCidade > .select2-choice > span").html(cid_nome);
                            } else {
                                $("#txtCidade").val("");
                                $("#s2id_txtCidade > .select2-choice > span").html("Selecione a cidade");
                            }
                        });
                    }
                });
                $("#txtEntrega").hide();
                $.getJSON("entrega.ajax.php?search=", {txtEnd: ui.item.id, ajax: "true"}, function (j) {
                    var options = "<option value=\"null\">--Selecione--</option>";
                    for (var i = 0; i < j.length; i++) {
                        options += "<option value=\"" + j[i].id_endereco + "\">" + j[i].nome_end_entrega + "</option>";
                    }
                    $("#txtEntrega").html(options).show();
                });
            }
        }
    });

    $("#txtEmitente").change(function () {
        if ($(this).val()) {
            $.getJSON("endereco.ajax.php?search=", {txtFonecedor: $(this).val(), ajax: "true"}, function (j) {
                for (var i = 0; i < j.length; i++) {
                    if (j[i].endereco !== "") {
                        $("#txtEmitEndereco").val(j[i].endereco);
                    } else {
                        $("#txtEmitEndereco").val("");
                    }
                    if (j[i].numero !== "") {
                        $("#txtEmitNumero").val(j[i].numero);
                    } else {
                        $("#txtEmitNumero").val("");
                    }
                    if (j[i].complemento !== "") {
                        $("#txtEmitComplemento").val(j[i].complemento);
                    } else {
                        $("#txtEmitComplemento").val("");
                    }
                    if (j[i].cep !== "") {
                        $("#txtEmitCep").val(j[i].cep);
                    } else {
                        $("#txtEmitCep").val("");
                    }
                    if (j[i].bairro !== "") {
                        $("#txtEmitBairro").val(j[i].bairro);
                    } else {
                        $("#txtEmitBairro").val("");
                    }
                    if (j[i].cnpj !== "") {
                        $("#txtEmitCNPJ").val(j[i].cnpj);
                    } else {
                        $("#txtEmitCNPJ").val("");
                    }
                    if (j[i].razao !== "") {
                        $("#txtEmitRazaoSocial").val(j[i].razao);
                    } else {
                        $("#txtEmitRazaoSocial").val("");
                    }
                    if (j[i].fantasia !== "") {
                        $("#txtEmitNomeFantasia").val(j[i].fantasia);
                    } else {
                        $("#txtEmitNomeFantasia").val("");
                    }
                    if (j[i].estadual !== "") {
                        $("#txtEmitInscricaoEstadual").val(j[i].estadual);
                    } else {
                        $("#txtEmitInscricaoEstadual").val("");
                    }
                    if (j[i].ietributo !== "") {
                        $("#txtEmitInscEstSubTrib").val(j[i].ietributo);
                    } else {
                        $("#txtEmitInscEstSubTrib").val("");
                    }
                    if (j[i].municipal !== "") {
                        $("#txtEmitInscMunicipal").val(j[i].municipal);
                    } else {
                        $("#txtEmitInscMunicipal").val("");
                    }
                    if (j[i].cnae !== "") {
                        $("#txtEmitCNAE").val(j[i].cnae);
                    } else {
                        $("#txtEmitCNAE").val("");
                    }
                    if (j[i].regime !== "") {
                        $("#txtEmitRegTrib").val(j[i].regime);
                        var RegTribOpt = $("#txtEmitRegTrib option[value='" + j[i].regime + "']").text();
                        $("#s2id_txtEmitRegTrib > .select2-choice > span").html(RegTribOpt);
                    } else {
                        $("#txtEmitRegTrib").val("");
                        $("#s2id_txtEmitRegTrib > .select2-choice > span").html("");
                    }
                    if (j[i].estado !== "") {
                        $("#txtEmitEstado").val(j[i].estado);
                        $("#s2id_txtEmitEstado > .select2-choice > span").html(j[i].estado_nome);
                    } else {
                        $("#txtEmitEstado").val("");
                        $("#s2id_txtEmitEstado > .select2-choice > span").html("Selecione o estado");
                    }
                    cid_cod = j[i].cidade;
                    cid_nome = j[i].cidade_nome;
                    $.getJSON("../Comercial/locais.ajax.php?search=", {txtEstado: $("#txtEmitEstado").val(), ajax: "true"}, function (j) {
                        var options = "<option value=\"\"></option>";
                        for (var i = 0; i < j.length; i++) {
                            options += "<option value=\"" + j[i].cidade_codigo + "\">" + j[i].cidade_nome + "</option>";
                        }
                        $("#txtEmitCidade").html(options).show();
                        $("#txtEmitCidade").css("display", "none");
                    }).done(function () {
                        if (cid_cod !== "") {
                            $("#txtEmitCidade").val(cid_cod);
                            $("#s2id_txtEmitCidade > .select2-choice > span").html(cid_nome);
                        } else {
                            $("#txtEmitCidade").val("");
                            $("#s2id_txtEmitCidade > .select2-choice > span").html("Selecione a cidade");
                        }
                    });
                }
            });
        }
    });
});

function prodValor() {
    var UpdQtd = textToNumber($("#txtQtdProduto").val());
    var UpdVal = textToNumber($("#hidValorProduto").val());
    $("#txtValorProduto").val(numberFormat(UpdVal * UpdQtd));
}

function removeLinha(id) {
    $("#" + id).remove();
    somaTotal();
}

function somaTotal() {
    var totalVendas = 0;
    for (var i = 0; i < conta; i++) {
        if ($("#txtPD_" + i).val() !== null) {
            totalVendas = totalVendas + textToNumber($("#txtVP_" + i).val());
        }
    }
    var prod_desc = textToNumber($("#txtProdDesconto").val());
    var impostos = textToNumber($("#txtAliquota").val());
    if ($("#prod_sinal").html() === "%") {
        prod_desc = totalVendas * (prod_desc / 100);
    }
    $("#txtTotalProd").html("Total sem Desconto: " + numberFormat(totalVendas, 1) + "<br>Valor do Desconto: " + numberFormat(prod_desc, 1) + "<br><b>Total com Desconto: " + numberFormat(totalVendas - prod_desc, 1) + "</b>");
    $('#txtIdTP').val(conta);
    $('#txtBaseCalc').val(numberFormat(totalVendas - prod_desc)).show();
    $('#txtTotDesconto').val(numberFormat(prod_desc)).show();
    $('#txtTotNota').val(numberFormat(totalVendas)).show();
    $('#txtTotImposto').val(numberFormat((totalVendas - prod_desc) * (impostos / 100))).show();
}

function novaLinha() {
    if ($('#txtConta').val() !== "" && textToNumber($("#txtQtdProduto").val()) > 0 && textToNumber($("#txtValorProduto").val()) > 0) {
        var valTotal = textToNumber($("#txtValorProduto").val());
        if (valTotal > 0.00) {
            var prp = $("#txtDescConta").val();
            var prc = $("#txtConta").val();
            var qtp = $("#txtQtdProduto").val();
            var vlu = $("#hidValorProduto").val();
            var vlp = $("#txtValorProduto").val();
            var chg = 0;
            for (var i = 0; i < conta; i++) {
                if ($("#txtPD_" + i).val() !== "null") {
                    var prv = $("#txtPD_" + i).val();
                    var qtv = $("#txtVQ_" + i).val();
                    var vlv = textToNumber($("#txtVP_" + i).val());
                    if (prc === prv) {
                        qtv = parseInt(qtp);
                        $("#myVQ_" + i).html("<input name='txtVQ_" + i + "' id='txtVQ_" + i + "' type='hidden' value='" + qtv + "'/><center>" + qtv + "</center>");
                        $("#myVP_" + i).html("<input name='txtVP_" + i + "' id='txtVP_" + i + "' type='hidden' value='" + vlv + "'/>" + vlv + "");
                        chg = 1;
                    }
                }
            }
            if (chg === 0) {
                var linha = "<td style='vertical-align: middle;'><input name='txtIP_" + conta + "' id='txtIP_" + conta + "' type='hidden' value=''/><input name='txtPD_" + conta + "' id='txtPD_" + conta + "' type='hidden' value='" + prc + "'/>&nbsp;&nbsp;&nbsp;" + prp + "</td>" +
                "<td id='myVQ_" + conta + "' style='vertical-align: middle;'><input name='txtVQ_" + conta + "' id='txtVQ_" + conta + "' type='hidden' value='" + qtp + "'/><center>" + qtp + "</center></td>" +
                "<td id='myVU_" + conta + "' style='text-align: center;vertical-align: middle;'><input name='txtVU_" + conta + "' id='txtVU_" + conta + "' type='hidden' value='" + vlu + "'/>" + vlu + "</td>" +
                "<td id='myVP_" + conta + "' style='text-align: center;vertical-align: middle;'><input name='txtVP_" + conta + "' id='txtVP_" + conta + "' type='hidden' value='" + vlp + "'/>" + vlp + "</td>" +
                "<td><center><input class='btn red' type='button' value='x' onClick=\"javascript:removeLinha('tabela_linha_" + conta + "')\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\"/></center></td>";
                $("#tabela_produto").append("<tr id='tabela_linha_" + conta + "'>" + linha + "</tr>");
                conta++;
            }
            somaTotal();
            $('#txtConta').val("").show();
            $('#txtDescConta').val("").show();
            $('#txtQtdProduto').val("1").show();
            $('#txtValorProduto').val(numberFormat(0)).show();
            $('#hidValorProduto').val(numberFormat(0)).show();
        }
    } else {
        bootbox.alert("Todos os campos devem ser preenchidos.");
    }
}

function AbrirBox(produto) {
    if (produto !== "" && produto !== undefined) {
        var nfe = "&id=" + $("#txtId").val();
        nfe = nfe + "&produto=" + produto;
        $("body").append("<div id='frmCenario' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:677px; height:525px; position:absolute; top:50%; left:50%; margin-left:-338px; margin-top:-282px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:525px' src='./../NFE/FormCenarios.php?TpTela=NFE" + nfe + "'></iframe></div></div></div></div></div>");
    } else {
        bootbox.alert("Produto sem cenário.");
    }
}

function FecharBox(tela) {
    $("#frmCenario").remove();
}

$("#frmEnviaDados").validate({
    errorPlacement: function (error, element) {
        return true;
    },
    ignore: 'input[type=hidden], input[disabled],input[readonly]',
    rules: {
        txtSerie: {required: true},
        txtNumNfe: {required: true},
        txtNtOpera: {required: true},
        txtDtEmissao: {required: true},
        txtHrSaiEmi: {required: true},
        txtTipoDoc: {required: true},
        txtCodNumerico: {required: true},
        txtEstadoNFE: {required: true},
        txtCidadeNFE: {required: true},
        txtDestinatario: {required: true},
        txtEmitente: {required: true},
        txtDesc: {required: true},
        txtAliquota: {required: true}
    },
    invalidHandler: function (event, validator) { //display error alert on form submit   
        bootbox.alert("Existem campos obrigatórios não preenchidos!");
    },
    highlight: function (element, errorClass, validClass) {
        var elem = $(element);
        if (elem.hasClass("select")) {
            $("#s2id_" + elem.attr("id") + " a").css("background-color", "#FF9F9F !important;");
        } else {
            elem.addClass(errorClass);
        }
    },
    unhighlight: function (element, errorClass, validClass) {
        var elem = $(element);
        if (elem.hasClass("select")) {
            $("#s2id_" + elem.attr("id") + " a").css("background-color", "white");
        } else {
            elem.removeClass(errorClass);
        }
    }
});