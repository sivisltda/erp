
$("#btn_Imprimir").click(function (event) { //PdfNFse.php
    if ($("#txtLinkDownloadPDF").val() !== "") {
        window.open($("#txtLinkDownloadPDF").val(), '_blank');
    }
});

$("#btnXml").click(function (event) { //XmlNFse.php
    if ($("#txtLinkDownloadXML").val() !== "") {
        window.open($("#txtLinkDownloadXML").val(), '_blank');
    }
});

function assinar_nota() {
    if ($("#txtStatus").val() === "Em Processamento") {
        consulta_processamento();
    } else {
        $("#loader").show();
        $.post("../NFE/include/MakeNFse.php", "txtId=" + $("#txtId").val()).done(function (data) {
            $('#loader').hide();
            if (data.length > 4 && data.substr(0, 4) !== "Erro") {
                $("#txtChave").val(data);
                if ($("#txtStatus").length) {
                    $("#txtStatus").val("Em Processamento");
                    consulta_processamento();
                }
            } else {
                if (data !== "") {
                    bootbox.alert(data + "!");
                } else {
                    bootbox.alert("Verifique o Cadastro do Produto!");
                }
            }
        });
    }
}

function consulta_processamento() {
    $.ajax({
        type: "POST",
        url: "../NFE/include/ConsultaNFse.php",
        data: "txtId=" + $("#txtId").val() + "&txtNota=" + $("#txtNumNfe").val(),
        dataType: "json",
        success: function (data) {
            $("#txtStatus").val("Em Processamento");
            if (data.status === "Autorizada") {
                $("#txtStatus").val("Autorizada");
                $("#btn_Assinar").prop("disabled", true);
                $("#btn_Imprimir").prop("disabled", false);
                $("#txtLinkDownloadPDF").val(data.linkDownloadPDF);
                $("#bntEdit").hide();
                $("#bntDelete").hide();
                $("#btnXml").show();
                $("#txtLinkDownloadXML").val(data.linkDownloadXML);
                $("#btnCancelarNFE").show();
            } else if (data.status === "SolicitandoAutorizacao") {
                bootbox.alert("Solicitação de Autorização!");
            } else if (data.status === "EmProcessoDeAutorizacao") {
                bootbox.alert("Em Processo de Autorizacao!");
            } else if (data.status === "AutorizacaoSolicitada") {
                bootbox.alert("Autorização Solicitada!");
            } else if (data.status === "Negada") {
                if ($("#txtStatus").length) {
                    $("#txtStatus").val("Rejeitada");
                }
                bootbox.alert(" " + data.motivoStatus);
            } else {
                bootbox.alert(data.motivoStatus + "!");
            }
        },
        error: function () {
            bootbox.alert("Erro de Processamento do Arquivo!");
        }
    });
}

function cancelar_nota() {
    bootbox.confirm('Confirma o Cancelamento desta Nota?', function (result) {
        if (result === true) {
            $.ajax({
                type: "POST",
                url: "../NFE/include/CancelaNFse.php",
                data: "txtId=" + $("#txtId").val() + "&txtNota=" + $("#txtNumNfe").val(),
                success: function (data) {
                    if (data.trim() === "YES") {
                        $("#txtStatus").val("Cancelada");
                        $("#btn_Assinar").hide();
                        $("#btn_Imprimir").hide();
                        $("#btnXml").hide();
                        $("#btnCancelarNFE").hide();
                        $("#bntEdit").show();
                        $("#bntDelete").show();
                        bootbox.alert("Nota Enviada para Cancelamento!");
                    } else {
                        bootbox.alert(data + "!");
                    }
                },
                error: function () {
                    bootbox.alert("Erro de Processamento do Arquivo!");
                }
            });
        } else {
            return;
        }
    });
}