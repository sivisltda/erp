$(document).ready(function () {

    $("#txtDescontos, #txtOutrasDespesas, #txtSeguro, #txtFrete," +
    "#txtAliqCalcCred, #txtCredICMSApro, #txtICMSRedBcSt, #txtICMSMargValAdSt, #txtICMSValBcSt," +
    "#txtICMSAliqSt, #txtICMSValSt, #txtICMSBCAnt, #txtICMSSTAnt, #txtICMSRedBc," + 
    "#txtICMSValBc, #txtICMSAliq, #txtICMSVal, #txtBCOpeProp, #txtICMSValDes," +
    "#txtBCICMSSTRetido, #txtICMSSTRetido, #txtBCICMSSTDestino, #txtICMSSTDestino," +
    "#txtIPIAliq, #txtIPIValBaseCalc, #txtIPIValor," +
    "#txtPISValBaseCalc, #txtPISAliqPerc, #txtPISAliqReal, #txtPISQtdVend, #txtPISValor," +
    "#txtPISValBaseCalcST, #txtPISAliqPercST, #txtPISAliqRealST, #txtPISQtdVendST, #txtPISValorST," +
    "#txtCOFINSValBaseCalc, #txtCOFINSAliqPerc, #txtCOFINSAliqReal, #txtCOFINSQtdVend, #txtCOFINSValor," + 
    "#txtCOFINSValBaseCalcST, #txtCOFINSAliqPercST, #txtCOFINSAliqRealST, #txtCOFINSQtdVendST, #txtCOFINSValorST, #txtICMSFcp"
    ).priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
    
    trataICMRegime($('#txtICMSRegime').val());
    trataICMSitTrib($("#txtICMSSitTrib").val());
    trataPISSitTrib($("#txtPISSitTrib").val());
    trataCOFINSSitTrib($("#txtCOFINSSitTrib").val());

    $('#txtICMSRegime').change(function () {
        if ($(this).val()) {
            trataICMRegime($('#txtICMSRegime').val());
            if ($(this).val() === "1") {
                $("#txtICMSSitTrib").val("101");
            } else {
                $("#txtICMSSitTrib").val("00");
            }
            $("#txtICMSSitTrib").trigger('change');
        }
    });
    
    function trataICMRegime(valor) {
        if (valor === "1") {
            $("[id*='SN_']").show();
            $("[id*='RN_']").hide();
        } else {
            $("[id*='SN_']").hide();
            $("[id*='RN_']").show();
        }
    }
    
    $("#txtICMSSitTrib").change(function () {
        trataICMSitTrib($(this).val());
        limpaICMSitTrib();
    });
    
    function trataICMSitTrib(valor) {
        if (valor) {
            $("#titICMS").parent().html().replace("&nbsp;", '');
            switch (valor) {
                case "201":
                case "202":
                case "203":
                case "500":
                case "412":
                    $("#titICMS").parent().append("&nbsp;");
                    break;
            }
            $("#titST").hide();
            $("#titICMS").hide();
            $('#txtAliqCalcCred').hide();
            $('#AliqCalcCred').hide();
            $('#txtCredICMSApro').hide();
            $('#CredICMSApro').hide();
            $('#ICMSBCAnt').hide();
            $('#ICMSSTAnt').hide();
            $('#ICMSModDet').hide();
            $('#ICMSModDetSt').hide();
            $('#ICMSRedBc').hide();
            $('#ICMSRedBcSt').hide();
            $('#ICMSValBc').hide();
            $('#ICMSMargValAdSt').hide();
            $('#ICMSAliq').hide();
            $('#ICMSValBcSt').hide();
            $('#ICMSVal').hide();
            $('#ICMSAliqSt').hide();
            $('#ICMSValSt').hide();
            $('#BCOpeProp').hide();
            $('#UfICMS').hide();
            $('#ICMSValDes').hide();
            $('#ICMSMotDes').hide();
            $('#BCICMSSTRetido').hide();
            $('#ICMSSTRetido').hide();
            $('#BCICMSSTDestino').hide();
            $('#ICMSSTDestino').hide();

            switch (valor) {
                case "101":
                    $('#txtAliqCalcCred').show();
                    $('#AliqCalcCred').show();
                    $('#txtCredICMSApro').show();
                    $('#CredICMSApro').show();
                    break;
                case "201":
                    $("#titST").show();
                    $('#txtAliqCalcCred').show();
                    $('#AliqCalcCred').show();
                    $('#txtCredICMSApro').show();
                    $('#CredICMSApro').show();
                    $('#ICMSModDetSt').show();
                    $('#ICMSRedBcSt').show();
                    $('#ICMSMargValAdSt').show();
                    $('#ICMSValBcSt').show();
                    $('#ICMSAliqSt').show();
                    $('#ICMSValSt').show();
                    break;
                case "202":
                    $("#titST").show();
                    $('#ICMSModDetSt').show();
                    $('#ICMSRedBcSt').show();
                    $('#ICMSMargValAdSt').show();
                    $('#ICMSValBcSt').show();
                    $('#ICMSAliqSt').show();
                    $('#ICMSValSt').show();
                    break;
                case "203":
                    $("#titST").show();
                    $('#ICMSModDetSt').show();
                    $('#ICMSRedBcSt').show();
                    $('#ICMSMargValAdSt').show();
                    $('#ICMSValBcSt').show();
                    $('#ICMSAliqSt').show();
                    $('#ICMSValSt').show();
                    break;
                case "500":
                    $("#titST").show();
                    $('#ICMSBCAnt').show();
                    $('#ICMSSTAnt').show();
                    break;
                case "900":
                    $("#titICMS").show();
                    $("#titST").show();
                    $('#txtAliqCalcCred').show();
                    $('#AliqCalcCred').show();
                    $('#txtCredICMSApro').show();
                    $('#CredICMSApro').show();
                    $('#txtICMSModDet').show();
                    $('#ICMSModDet').show();
                    $('#ICMSModDetSt').show();
                    $('#ICMSRedBc').show();
                    $('#ICMSRedBcSt').show();
                    $('#ICMSValBc').show();
                    $('#ICMSMargValAdSt').show();
                    $('#txtICMSAliq').show();
                    $('#txtICMSFcp').show();
                    $('#ICMSAliq').show();
                    $('#ICMSValBcSt').show();
                    $('#ICMSVal').show();
                    $('#ICMSAliqSt').show();
                    $('#ICMSValSt').show();
                    break;
                case "00":
                    $("#titICMS").show();
                    $('#txtICMSModDet').show();
                    $('#ICMSModDet').show();
                    $('#ICMSValBc').show();
                    $('#ICMSAliq').show();
                    $('#ICMSVal').show();
                    break;
                case "1001":
                    $("#titICMS").show();
                    $("#titST").show();
                    $('#txtICMSModDet').show();
                    $('#ICMSModDet').show();
                    $('#ICMSModDetSt').show();
                    $('#ICMSRedBcSt').show();
                    $('#ICMSValBc').show();
                    $('#ICMSMargValAdSt').show();
                    $('#ICMSAliq').show();
                    $('#ICMSValBcSt').show();
                    $('#ICMSVal').show();
                    $('#ICMSAliqSt').show();
                    $('#ICMSValSt').show();
                    break;
                case "1002":
                    $("#titICMS").show();
                    $("#titST").show();
                    $('#txtICMSModDet').show();
                    $('#ICMSModDet').show();
                    $('#ICMSModDetSt').show();
                    $('#ICMSRedBc').show();
                    $('#ICMSRedBcSt').show();
                    $('#ICMSValBc').show();
                    $('#ICMSMargValAdSt').show();
                    $('#ICMSAliq').show();
                    $('#ICMSValBcSt').show();
                    $('#ICMSVal').show();
                    $('#ICMSAliqSt').show();
                    $('#ICMSValSt').show();
                    $('#BCOpeProp').show();
                    $('#UfICMS').show();
                    break;
                case "20":
                    $("#titICMS").show();
                    $('#txtICMSModDet').show();
                    $('#ICMSModDet').show();
                    $('#ICMSRedBc').show();
                    $('#ICMSValBc').show();
                    $('#ICMSAliq').show();
                    $('#ICMSVal').show();
                    $('#ICMSValDes').show();
                    $('#ICMSMotDes').show();
                    break;
                case "30":
                    $("#titICMS").show();
                    $("#titST").show();
                    $('#ICMSModDetSt').show();
                    $('#ICMSRedBcSt').show();
                    $('#ICMSMargValAdSt').show();
                    $('#ICMSValBcSt').show();
                    $('#ICMSAliqSt').show();
                    $('#ICMSValSt').show();
                    $('#ICMSValDes').show();
                    $('#ICMSMotDes').show();
                    break;
                case "40":
                    $("#titICMS").show();
                    $('#ICMSValDes').show();
                    $('#ICMSMotDes').show();
                    break;
                case "411":
                    $("#titICMS").show();
                    $('#ICMSValDes').show();
                    $('#ICMSMotDes').show();
                    break;
                case "412":
                    $("#titST").show();
                    $('#BCICMSSTRetido').show();
                    $('#ICMSSTRetido').show();
                    $('#BCICMSSTDestino').show();
                    $('#ICMSSTDestino').show();
                    break;
                case "60":
                    $("#titST").show();
                    $('#ICMSBCAnt').show();
                    $('#txtICMSBCAnt').show();
                    $('#ICMSSTAnt').show();
                    $('#txtICMSSTAnt').show();
                    break;
            }
        }
    }
    function limpaICMSitTrib() {
        $('#txtAliqCalcCred').val(numberFormat(0));
        $('#txtCredICMSApro').val(numberFormat(0));
        $('#txtICMSModDet').val("");
        $('#txtICMSRedBc').val(numberFormat(0));
        $('#txtICMSValBc').val(numberFormat(0));
        $('#txtICMSAliq').val(numberFormat(0));
        $('#txtICMSFcp').val(numberFormat(0));
        $('#txtICMSVal').val(numberFormat(0));
        $('#txtBCOpeProp').val(numberFormat(0));
        $('#txtICMSValDes').val(numberFormat(0));
        $('#txtICMSMotDes').val("");
        $('#txtICMSModDetSt').val("");
        $('#txtICMSRedBcSt').val(numberFormat(0));
        $('#txtICMSMargValAdSt').val(numberFormat(0));
        $('#txtICMSValBcSt').val(numberFormat(0));
        $('#txtICMSAliqSt').val(numberFormat(0));
        $('#txtICMSValSt').val(numberFormat(0));
        $('#txtICMSBCAnt').val(numberFormat(0));
        $('#txtICMSSTAnt').val(numberFormat(0));
        $('#txtUfICMS').val("");
        $('#txtBCICMSSTRetido').val(numberFormat(0));
        $('#txtICMSSTRetido').val(numberFormat(0));
        $('#txtBCICMSSTDestino').val(numberFormat(0));
        $('#txtICMSSTDestino').val(numberFormat(0));
    }

    $("#txtCOFINSSitTrib").change(function () {
        trataCOFINSSitTrib($(this).val());
        limpaCOFINSSitTrib("1");
    });
    function trataCOFINSSitTrib(valor) {
        if (!$("#bntNew").length) {
            $("#txtCOFINSTipoCalc").attr('disabled', 'disabled');
            $("#txtCOFINSValBaseCalc").attr('disabled', 'disabled');
            $("#txtCOFINSAliqPerc").attr('disabled', 'disabled');
            $("#txtCOFINSAliqReal").attr('disabled', 'disabled');
            $("#txtCOFINSQtdVend").attr('disabled', 'disabled');
            $("#txtCOFINSValor").attr('disabled', 'disabled');
            $("#txtCOFINSTipoCalcST").attr('disabled', 'disabled');
            $("#txtCOFINSValBaseCalcST").attr('disabled', 'disabled');
            $("#txtCOFINSAliqPercST").attr('disabled', 'disabled');
            $("#txtCOFINSAliqRealST").attr('disabled', 'disabled');
            $("#txtCOFINSQtdVendST").attr('disabled', 'disabled');
            $("#txtCOFINSValorST").attr('disabled', 'disabled');
            if (valor === "1" || valor === "2") {
                $("#txtCOFINSValBaseCalc").removeAttr('disabled');
                $("#txtCOFINSAliqPerc").removeAttr('disabled');
                $("#txtCOFINSValor").removeAttr('disabled');
            } else if (valor === "3") {
                $("#txtCOFINSAliqReal").removeAttr('disabled');
                $("#txtCOFINSQtdVend").removeAttr('disabled');
                $("#txtCOFINSValor").removeAttr('disabled');
            } else if (valor === "5") {
                $("#txtCOFINSTipoCalcST").removeAttr('disabled');
                $("#txtCOFINSTipoCalcST").trigger('change');
            } else if (valor !== "4" && valor !== "6" && valor !== "7" && valor !== "8" && valor !== "9") {
                $("#txtCOFINSTipoCalc").removeAttr('disabled');
                $("#txtCOFINSTipoCalc").trigger('change');
            }
        }
    }
    function limpaCOFINSSitTrib(tipo) {
        if (tipo === "1") {
            $("#txtCOFINSTipoCalc").val("null");
            $("#txtCOFINSTipoCalcST").val("null");
        }
        $("#txtCOFINSValBaseCalc").val(numberFormat(0));
        $("#txtCOFINSAliqPerc").val(numberFormat(0));
        $("#txtCOFINSAliqReal").val(numberFormat(0));
        $("#txtCOFINSQtdVend").val(numberFormat(0));
        $("#txtCOFINSValor").val(numberFormat(0));
        $("#txtCOFINSValBaseCalcST").val(numberFormat(0));
        $("#txtCOFINSAliqPercST").val(numberFormat(0));
        $("#txtCOFINSAliqRealST").val(numberFormat(0));
        $("#txtCOFINSQtdVendST").val(numberFormat(0));
        $("#txtCOFINSValorST").val(numberFormat(0));
    }
    $('#txtCOFINSTipoCalc').change(function () {
        if ($(this).val()) {
            switch ($(this).val()) {
                case "null":
                    $('#txtCOFINSValBaseCalc').attr('disabled', 'disabled');
                    $('#txtCOFINSAliqPerc').attr('disabled', 'disabled');
                    $('#txtCOFINSQtdVend').attr('disabled', 'disabled');
                    $('#txtCOFINSAliqReal').attr('disabled', 'disabled');
                    $('#txtCOFINSValor').attr('disabled', 'disabled');
                    break;
                case "0":
                    $('#txtCOFINSValBaseCalc').removeAttr('disabled');
                    $('#txtCOFINSAliqPerc').removeAttr('disabled');
                    $('#txtCOFINSQtdVend').attr('disabled', 'disabled');
                    $('#txtCOFINSAliqReal').attr('disabled', 'disabled');
                    $('#txtCOFINSValor').removeAttr('disabled');
                    break;
                case "1":
                    $('#txtCOFINSValBaseCalc').attr('disabled', 'disabled');
                    $('#txtCOFINSAliqPerc').attr('disabled', 'disabled');
                    $('#txtCOFINSQtdVend').removeAttr('disabled');
                    $('#txtCOFINSAliqReal').removeAttr('disabled');
                    $('#txtCOFINSValor').removeAttr('disabled');
                    break;
            }
        }
        limpaCOFINSSitTrib("2");
    });
    $('#txtCOFINSTipoCalcST').change(function () {
        if ($(this).val()) {
            switch ($(this).val()) {
                case "null":
                    $('#txtCOFINSValBaseCalcST').attr('disabled', 'disabled');
                    $('#txtCOFINSAliqPercST').attr('disabled', 'disabled');
                    $('#txtCOFINSQtdVendST').attr('disabled', 'disabled');
                    $('#txtCOFINSAliqRealST').attr('disabled', 'disabled');
                    $('#txtCOFINSValorST').attr('disabled', 'disabled');
                    break;
                case "0":
                    $('#txtCOFINSValBaseCalcST').removeAttr('disabled');
                    $('#txtCOFINSAliqPercST').removeAttr('disabled');
                    $('#txtCOFINSQtdVendST').attr('disabled', 'disabled');
                    $('#txtCOFINSAliqRealST').attr('disabled', 'disabled');
                    $('#txtCOFINSValorST').removeAttr('disabled');
                    break;
                case "1":
                    $('#txtCOFINSValBaseCalcST').attr('disabled', 'disabled');
                    $('#txtCOFINSAliqPercST').attr('disabled', 'disabled');
                    $('#txtCOFINSQtdVendST').removeAttr('disabled');
                    $('#txtCOFINSAliqRealST').removeAttr('disabled');
                    $('#txtCOFINSValorST').removeAttr('disabled');
                    break;
            }
        }
        limpaCOFINSSitTrib("2");
    });

    $("#txtPISSitTrib").change(function () {
        trataPISSitTrib($(this).val());
        limpaPISSitTrib("1");
    });
    $('#txtPISTipoCalc').change(function () {
        if ($(this).val()) {
            switch ($(this).val()) {
                case "null":
                    $('#txtPISValBaseCalc').attr('disabled', 'disabled');
                    $('#txtPISAliqPerc').attr('disabled', 'disabled');
                    $('#txtPISAliqReal').attr('disabled', 'disabled');
                    $('#txtPISQtdVend').attr('disabled', 'disabled');
                    $('#txtPISValor').attr('disabled', 'disabled');
                    break;
                case "0":
                    $('#txtPISValBaseCalc').removeAttr('disabled');
                    $('#txtPISAliqPerc').removeAttr('disabled');
                    $('#txtPISAliqReal').attr('disabled', 'disabled');
                    $('#txtPISQtdVend').attr('disabled', 'disabled');
                    $('#txtPISValor').removeAttr('disabled');
                    break;
                case "1":
                    $('#txtPISValBaseCalc').attr('disabled', 'disabled');
                    $('#txtPISAliqPerc').attr('disabled', 'disabled');
                    $('#txtPISAliqReal').removeAttr('disabled');
                    $('#txtPISQtdVend').removeAttr('disabled');
                    $('#txtPISValor').removeAttr('disabled');
                    break;
            }
        }
        limpaPISSitTrib("2");
    });
    function trataPISSitTrib(valor) {
        if (!$("#bntNew").length) {
            $("#txtPISTipoCalc").attr('disabled', 'disabled');
            $("#txtPISValBaseCalc").attr('disabled', 'disabled');
            $("#txtPISAliqPerc").attr('disabled', 'disabled');
            $("#txtPISAliqReal").attr('disabled', 'disabled');
            $("#txtPISQtdVend").attr('disabled', 'disabled');
            $("#txtPISValor").attr('disabled', 'disabled');
            $("#txtPISTipoCalcST").attr('disabled', 'disabled');
            $("#txtPISValBaseCalcST").attr('disabled', 'disabled');
            $("#txtPISAliqPercST").attr('disabled', 'disabled');
            $("#txtPISAliqRealST").attr('disabled', 'disabled');
            $("#txtPISQtdVendST").attr('disabled', 'disabled');
            $("#txtPISValorST").attr('disabled', 'disabled');
            if (valor === "1" || valor === "2") {
                $("#txtPISValBaseCalc").removeAttr('disabled');
                $("#txtPISAliqPerc").removeAttr('disabled');
                $("#txtPISValor").removeAttr('disabled');
            } else if (valor === "3") {
                $("#txtPISAliqReal").removeAttr('disabled');
                $("#txtPISQtdVend").removeAttr('disabled');
                $("#txtPISValor").removeAttr('disabled');
            } else if (valor === "5") {
                $("#txtPISTipoCalcST").removeAttr('disabled');
                $("#txtPISTipoCalcST").trigger('change');
            } else {
                $("#txtPISTipoCalc").removeAttr('disabled');
                $("#txtPISTipoCalc").trigger('change');
            }
        }
    }
    function limpaPISSitTrib(tipo) {
        if (tipo === "1") {
            $("#txtPISTipoCalc").val("null");
            $("#txtPISTipoCalcST").val("null");
        }
        $("#txtPISValBaseCalc").val(numberFormat(0));
        $("#txtPISAliqPerc").val(numberFormat(0));
        $("#txtPISAliqReal").val(numberFormat(0));
        $("#txtPISQtdVend").val(numberFormat(0));
        $("#txtPISValor").val(numberFormat(0));
        $("#txtPISValBaseCalcST").val(numberFormat(0));
        $("#txtPISAliqPercST").val(numberFormat(0));
        $("#txtPISAliqRealST").val(numberFormat(0));
        $("#txtPISQtdVendST").val(numberFormat(0));
        $("#txtPISValorST").val(numberFormat(0));
    }
    $('#txtPISTipoCalcST').change(function () {
        if ($(this).val()) {
            switch ($(this).val()) {
                case "null":
                    $('#txtPISValBaseCalcST').attr('disabled', 'disabled');
                    $('#txtPISAliqPercST').attr('disabled', 'disabled');
                    $('#txtPISAliqRealST').attr('disabled', 'disabled');
                    $('#txtPISQtdVendST').attr('disabled', 'disabled');
                    $('#txtPISValorST').attr('disabled', 'disabled');
                    break;
                case "0":
                    $('#txtPISValBaseCalcST').removeAttr('disabled');
                    $('#txtPISAliqPercST').removeAttr('disabled');
                    $('#txtPISAliqRealST').attr('disabled', 'disabled');
                    $('#txtPISQtdVendST').attr('disabled', 'disabled');
                    $('#txtPISValorST').removeAttr('disabled');
                    break;
                case "1":
                    $('#txtPISValBaseCalcST').attr('disabled', 'disabled');
                    $('#txtPISAliqPercST').attr('disabled', 'disabled');
                    $('#txtPISAliqRealST').removeAttr('disabled');
                    $('#txtPISQtdVendST').removeAttr('disabled');
                    $('#txtPISValorST').removeAttr('disabled');
                    break;
            }
        }
        limpaPISSitTrib("2");
    });

    $('#txtIPITipoCalc').change(function () {
        if ($(this).val()) {
            switch ($(this).val()) {
                case "null":
                    $('#txtIPIValBaseCalc').attr('disabled', 'disabled');
                    $('#txtIPIAliq').attr('disabled', 'disabled');
                    $('#txtIPIQtdTotUndPad').attr('disabled', 'disabled');
                    $('#txtIPIValUnid').attr('disabled', 'disabled');
                    $('#txtIPIValor').attr('disabled', 'disabled');
                    break;
                case "0":
                    $('#txtIPIValBaseCalc').removeAttr('disabled');
                    $('#txtIPIAliq').removeAttr('disabled');
                    $('#txtIPIQtdTotUndPad').attr('disabled', 'disabled');
                    $('#txtIPIValUnid').attr('disabled', 'disabled');
                    $('#txtIPIValor').removeAttr('disabled');
                    break;
                case "1":
                    $('#txtIPIValBaseCalc').attr('disabled', 'disabled');
                    $('#txtIPIAliq').attr('disabled', 'disabled');
                    $('#txtIPIQtdTotUndPad').removeAttr('disabled');
                    $('#txtIPIValUnid').removeAttr('disabled');
                    $('#txtIPIValor').removeAttr('disabled');
                    break;
            }
        }
    });
});

function validar() {
    if ($("#txtPISSitTrib").val() === "null") {
        bootbox.alert("Preencha o campo Situação Tributária de PIS!");
        return false;
    }
    if ($("#txtCOFINSSitTrib").val() === "null") {
        bootbox.alert("Preencha o campo Situação Tributária de COFINS!");
        return false;
    }
    return true;
}

$("#frmEnviaDados").validate({
    errorPlacement: function (error, element) {
        return true;
    },
    rules: {
        txtDescricao: {required: true}
    }
});
