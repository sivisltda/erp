
$(function () {
    $("#btnVisualizarDanfe").click(function () {
        $("#loader").show();
        $.post("../NFE/include/MakeNFe.php", "txtId=" + $("#txtId").val() + "&txtModelo=" + $("#txtModelo").val()).done(function (data) {
            if ($.isNumeric(data)) {
                $("#txtChave").val(data);
                $("#txtStatus").val("Assinada");
                imprimir_nota('PRE');
                $('#loader').hide();
                location.reload();
            } else {
                $('#loader').hide();
                bootbox.alert(" Erro ao gerar XML" + data);
            }
        });
    });
});

function assinar_nota() {
    $("#loader").show();
    $.post("../NFE/include/MakeNFe.php", "txtId=" + $("#txtId").val() + "&txtModelo=" + $("#txtModelo").val()).done(function (data) {
        if ($.isNumeric(data)) {
            $("#txtChave").val(data);
            if ($("#txtStatus").length) {
                $("#txtStatus").val("Assinada");
            }
            $.post("../NFE/include/EnviaLote.php", "txtChave=" + $("#txtChave").val() + "&txtId=" + $("#txtId").val() + "&txtModelo=" + $("#txtModelo").val()).done(function (dataL) {
                if ($.isNumeric(dataL)) {
                    $("#txtStatus").val("Processamento SEFAZ");
                    $.post("../NFE/include/ConsultaRecibo.php", "txtRecibo=" + dataL + "&txtId=" + $("#txtId").val() + "&txtModelo=" + $("#txtModelo").val()).done(function (datax) {
                        if (datax === "YES") {
                            $('#loader').hide();
                            if ($("#txtStatus").length) {
                                $("#txtStatus").val("Autorizada");
                            }
                            if ($("#btn_Assinar").length) {
                                $("#btn_Assinar").prop("disabled", true);
                            }
                            if ($("#btn_Imprimir").length) {
                                $("#btn_Imprimir").prop("disabled", false);
                            }
                            imprimir_nota('');
                        } else {
                            $('#loader').hide();
                            if ($("#txtStatus").length) {
                                $("#txtStatus").val("Rejeitada");
                            }
                            bootbox.alert(" " + datax);
                        }
                    });
                } else {
                    $('#loader').hide();
                    bootbox.alert(" " + dataL);
                }
            });
        } else {
            $('#loader').hide();
            if (data !== "") {
                bootbox.alert(" " + data);
            } else {
                bootbox.alert("Verifique o Cadastro do Produto!");
            }
        }
    });
}

function imprimir_nota(tipo) {
    var dtEmissao = $("#txtDtEmissao").val().substring(6, 10) + "" + $("#txtDtEmissao").val().substring(3, 5);
    var win = window.open("../NFE/ImprimirDanfe.php?id=" + $("#txtChave").val() + "&txtModelo=" + $("#txtModelo").val() + "&dirMes=" + dtEmissao + "&tipo=" + tipo, '_blank');
    if (!$("#txtStatus").length) {
        location.replace('Ponto-de-Venda.php?tp=0');
    }
    if (win) {
        win.focus();
    } else {
        bootbox.alert('Popup Bloqueado!');
    }
}
