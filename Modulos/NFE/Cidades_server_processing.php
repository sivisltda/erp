<?php

include './../../Connections/configini.php';
$aColumns = array("cidade_codigo", "estado_sigla", "cidade_nome", "cidade_gov");
$sLimit = 0;
$iTotal = 0;
$iFilteredTotal = 0;
$sWhere = "";
$sWhereX = "";
$sOrder = " ORDER BY cidade_nome asc ";
$imprimir = 0;

if (is_numeric($_GET['imp']) && $_GET['imp'] == "1") {
    $imprimir = 1;
}

if (is_numeric($_GET['est'])) {
    $sWhereX = " and cidade_codigoEstado = " . $_GET['est'];
}

$_GET['sSearch'] = $_GET['Search'];

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != "-1") {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . "" . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY cidade_nome asc";
    }
}

if ($_GET['sSearch'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['sSearch']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ")";
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != "") {
        $sWhere .= $aColumns[$i] . " AND LIKE '%" . utf8_decode($_GET['sSearch_' . $i]) . "%' ";
    }
}

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,*
FROM tb_cidades inner join tb_estados on tb_cidades.cidade_codigoEstado = tb_estados.estado_codigo
WHERE cidade_codigo > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1=" . $imprimir . " " . $sOrder;
$cur = odbc_exec($con, $sQuery1);

$sQuery = "SELECT COUNT(*) total FROM tb_cidades inner join tb_estados on tb_cidades.cidade_codigoEstado = tb_estados.estado_codigo
WHERE cidade_codigo > 0 " . $sWhereX . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total FROM tb_cidades inner join tb_estados on tb_cidades.cidade_codigoEstado = tb_estados.estado_codigo
WHERE cidade_codigo > 0 " . $sWhereX;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<center>" . utf8_encode($aRow[$aColumns[0]]) . "</center>";
    $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[$aColumns[0]]) . ")'><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[2]]) . "'>" . utf8_encode($aRow[$aColumns[2]]) . "</div></a>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[1]]) . "'>" . utf8_encode($aRow[$aColumns[1]]) . "</div></center>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[3]]) . "'>" . utf8_encode($aRow[$aColumns[3]]) . "</div></center>";
    $row[] = "<center><a title='Excluir' href='Cidades.php?Del=" . $aRow[$aColumns[0]] . "'><input name='' type='image' onClick=\"return confirm('Deseja deletar esse registro?')\" src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
