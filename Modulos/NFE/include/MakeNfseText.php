<?php

require_once(__DIR__ . '/../../../Connections/configini.php');

$content = "";
$endFile = chr(13) . chr(10);

function limparNumero($valor, $tamanho) {
    return str_pad(str_replace(array(".", ",", ";", "/", "-", " ", "(", ")"), "", $valor), $tamanho, "0", STR_PAD_LEFT);
}

function limparTexto($valor, $tamanho) {
    $caracteresBR = array('À', 'Á', 'Ã', 'Â', 'à', 'á', 'ã', 'â', 'Ê', 'É', 'Í', 'í', 'Ó', 'Õ', 'Ô', 'ó', 'õ', 'ô', 'Ú', 'Ü', 'Ç', 'ç', 'é', 'ê', 'ú', 'ü');
    $caracteresUS = array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'E', 'E', 'I', 'i', 'O', 'O', 'O', 'o', 'o', 'o', 'U', 'U', 'C', 'c', 'e', 'e', 'u', 'u');
    return str_pad(str_replace($caracteresBR, $caracteresUS, $valor), $tamanho, " ", STR_PAD_RIGHT);
}

function criarCabecalho($cnpj, $inscMun, $dtInicio, $dtFim) { //Pagina 7
    $toReturn = "10"; //Indica Cabealho
    $toReturn .= "003"; //Versão
    $toReturn .= "2"; //Tipo Contribuinte 1-cpf 2-cnpj
    $toReturn .= limparNumero($cnpj, 14); //cpf ou cnpj
    $toReturn .= limparNumero($inscMun, 15); //inscricao municipal
    $toReturn .= escreverData($dtInicio, "Ymd"); //Data INICIAL desse período
    $toReturn .= escreverData($dtFim, "Ymd"); //Data FINAL desse período
    return $toReturn;
}

function criarDetalhe($serie, $numero, $data, $cnpj, $inscMun, $inscEst, $razao_social, $logradouro, $numeroEnd, $complemento, $bairro, $cidade, $uf, $cep, $cel, $email, $cidadeCod, $codServico, $codAtivMun, $aliquota, $valor, $deducoes, $descCond, $descIncond, $cofins, $csll, $inss, $irpj, $pis, $outros, $iss, $descricao) { //Pagina 8 
    $toReturn = "20"; //Indica Detalhe
    $toReturn .= "0"; //0 – Recibo Provisório de Serviços (RPS); 1 – Recibo Provisório de Serviços proveniente
    $toReturn .= limparNumero($serie, 5); //Série
    $toReturn .= limparNumero($numero, 15); //Número
    $toReturn .= escreverData($data, "Ymd"); //Data de emissão
    $toReturn .= "1"; //1 – Normal; 2 – Cancelado.
    $toReturn .= "1"; //Tipo Contribuinte 1-cpf 2-cnpj 3-não informado
    $toReturn .= limparNumero($cnpj, 14); //cpf ou cnpj
    $toReturn .= limparNumero($inscMun, 15); //inscricao municipal    
    $toReturn .= limparNumero($inscEst, 15); //inscricao estadual    
    $toReturn .= limparTexto($razao_social, 115); //razao_social   
    $toReturn .= limparTexto("RUA", 3); //Tipo do Endereço do Tomador   
    $toReturn .= limparTexto($logradouro, 125); //razao_social
    $toReturn .= limparTexto($numeroEnd, 10); //Número do Endereço do Tomador
    $toReturn .= limparTexto($complemento, 60); //Complemento do Endereço do Tomador
    $toReturn .= limparTexto($bairro, 72); //Bairro
    $toReturn .= limparTexto($cidade, 50); //Cidade
    $toReturn .= limparTexto($uf, 2); //Uf    
    $toReturn .= limparNumero($cep, 8); //Cep
    $toReturn .= limparNumero($cel, 11); //Celular
    $toReturn .= limparTexto($email, 80); //Email
    $toReturn .= "01"; //01 – Exigível; 02 – Operação Não Incidente; 03 – Operação Isenta; 05 – Operação Imune; 06 – Operação Suspensa por Decisão Judicial; 07 – Operação Suspensa por Decisão Administrativa.    
    $toReturn .= limparNumero($cidadeCod, 7); //Cidade de incidência do ISS.
    $toReturn .= limparTexto("", 7); //Cidade onde o Serviço foi prestado.
    $toReturn .= limparTexto("", 38); //Preencher com brancos.
    $toReturn .= "00"; //(Regime Especial de Tributação da Nota) 00 – Nenhum; 01 – Microempresa Municipal; 02 – Estimativa; 03 – Sociedade de profissionais; 04 – Cooperativa.
    $toReturn .= "1"; //(Opção pelo Simples Nacional) 0 - Não-Optante pelo Simples Nacional; 1 - Optante pelo Simples Nacional (Recolhimento pelo DAS). 3 - Optante pelo Simples Nacional (Microempreendedor Individual - MEI)
    $toReturn .= "0"; //(Utilização de Incentivo Cultural) 0 – Não; 1 – Sim.    
    $toReturn .= limparNumero($codServico, 4); //Código do Serviço da lista Municipal.
    $toReturn .= limparNumero($codAtivMun, 7); //Código da Atividade da lista Municipal
    $toReturn .= limparTexto("", 9); //Código NBS
    $toReturn .= limparTexto("", 4); //Preencher com brancos.    
    $toReturn .= limparNumero($aliquota, 5); //Valor da Alíquota
    $toReturn .= limparNumero($valor, 15); //Valor dos Serviços da nota fiscal
    $toReturn .= limparNumero($deducoes, 15); //Valor das Deduções da nota fiscal    
    $toReturn .= limparNumero($descCond, 15); //Valor dos Descontos Condicionados
    $toReturn .= limparNumero($descIncond, 15); //Valor dos Descontos Incondicionados
    $toReturn .= limparNumero($cofins, 15); //Valor da retenção de COFINS
    $toReturn .= limparNumero($csll, 15); //Valor da retenção de CSLL
    $toReturn .= limparNumero($inss, 15); //Valor da retenção de INSS    
    $toReturn .= limparNumero($irpj, 15); //Valor da retenção de INSS
    $toReturn .= limparNumero($pis, 15); //Valor da retenção de INSS
    $toReturn .= limparNumero($outros, 15); //Valor da retenção de INSS
    $toReturn .= limparNumero($iss, 15); //Valor da retenção de INSS
    $toReturn .= "0"; //Retenção do ISS: 0 – Nota Fiscal sem ISS Retido; 1 – ISS Retido pelo Tomador; 2 – ISS Retido pelo Intermediário.
    $toReturn .= escreverData($data, "Ymd"); //Data de emissão
    $toReturn .= limparTexto("", 15); //Código da Obra para serviços de construção civil.
    $toReturn .= limparTexto("", 15); //Anotação de Responsabilidade Técnica para serviços de construção civil.    
    $toReturn .= limparTexto("", 5); //Série do RPS substituído caso seja um RPS substituto.
    $toReturn .= limparTexto("", 15); //Número do RPS substituído caso seja um RPS substituto.
    $toReturn .= limparTexto("", 30); //Número do Processo que suspende a exigibilidade do ISS.
    $toReturn .= limparTexto($descricao, 0); //Descritivo dos serviços
    return $toReturn;
}

function criarIntermediario() { //Pagina 13
    $toReturn = "21"; //Indica Intermediário do Serviço
    return $toReturn;
}

function criarDetalheCupons() { //Pagina 14
    $toReturn = "30"; //Indica Detalhe com Cupom
    return $toReturn;
}

function criarConvencionaisRecebida() { //Pagina 16
    $toReturn = "40"; //Convencionais Recebida
    return $toReturn;
}

function criarExportacao() { //Pagina 19
    $toReturn = "60"; //Convencionais Recebida
    return $toReturn;
}

function criarRodape($total, $totalServicos, $valDeducao, $valDescCond, $valDescInCond) { //Pagina 23
    $toReturn = "90"; //Indica Cabealho
    $toReturn .= limparNumero($total, 8); //total de linhas
    $toReturn .= limparNumero($totalServicos, 15); //soma dos valores dos serviços das linhas de detalhe
    $toReturn .= limparNumero($valDeducao, 15); //soma dos valores das deduções das linhas de detalhe
    $toReturn .= limparNumero($valDescCond, 15); //soma dos valores dos descontos condicionados das linhas de detalhe
    $toReturn .= limparNumero($valDescInCond, 15); //soma dos valores dos descontos incondicionados das linhas de detalhe
    return $toReturn;
}

if (isset($_GET["id"]) && $_GET["id"] != "0") {
    $i = 0;
    $aliquota = 0;
    $total = 0.00;
    $cnae = "";
    $cidade_gov = "";
    $notas = $_GET["id"];

    $PATH = "./../../../Pessoas/" . $_SESSION["contrato"] . "/NFSE/";
    $timestamp = mktime(date("H") - $fusohorario, date("i"), date("s"), date("m"), date("d"), date("Y"));
    $DATA['DIA'] = gmdate("d", $timestamp);
    $DATA['MES'] = gmdate("m", $timestamp);
    $DATA['ANO'] = gmdate("y", $timestamp);
    define("REMESSA", $PATH, true);
    $filename = REMESSA . "C" . $DATA['DIA'] . $DATA['MES'] . $DATA['ANO'] . "A" . ".txt";

    $query = "update sf_nfe_notas set status_nfe = " . valoresTexto2("Em Exportação") . " where cod_nfe in (" . $notas . ")";
    odbc_exec($con, $query);
    $query = "select top 1 cnpj, inscricao_municipal, comiss_gere aliquota, cnae_fiscal,
    (select min(demis) data from sf_nfe_notas where cod_nfe in (" . $notas . ")) dt_ini,
    (select max(demis) data from sf_nfe_notas where cod_nfe in (" . $notas . ")) dt_fim,
    cidade_gov from sf_fornecedores_despesas 
    left join tb_cidades on tb_cidades.cidade_codigo = sf_fornecedores_despesas.cidade 
    where tipo = 'M'";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $aliquota = $RFP['aliquota'];
        $cnae = $RFP['cnae_fiscal'];
        $cidade_gov = $RFP['cidade_gov'];        
        $content .= criarCabecalho($RFP['cnpj'], $RFP['inscricao_municipal'], $RFP['dt_ini'], $RFP['dt_fim']) . $endFile;        
    }
    $query = "select serie, nnf, demis, cnpj, inscricao_municipal, inscricao_estadual,razao_social,
    endereco, numero, complemento, bairro, cidade_nome, estado_sigla, cep,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato in (0,1) and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = id_fornecedores_despesas) email_contato, cidade_gov,
    cod_prod,codigo_enq, tot_val_bruto, info_add_fisco
    from sf_nfe_notas inner join sf_nfe_notas_itens on cod_nota = cod_nfe
    inner join sf_fornecedores_despesas on id_fornecedores_despesas = codigo_destinatario
    left join tb_cidades on tb_cidades.cidade_codigo = sf_fornecedores_despesas.cidade 
    left join tb_estados on tb_cidades.cidade_codigoEstado = tb_estados.estado_codigo
    left join sf_nfs_enquadramento on id_enq = tpnf where cod_nfe in (" . $notas . ")";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $total = $total + $RFP['tot_val_bruto'];
        $content .= criarDetalhe($RFP['serie'], $RFP['nnf'], $RFP['demis'], $RFP['cnpj'], $RFP['inscricao_municipal'], $RFP['inscricao_estadual'], utf8_encode($RFP['razao_social']), utf8_encode(str_replace("RUA ", "", $RFP['endereco'])), utf8_encode($RFP['numero']), utf8_encode($RFP['complemento']), utf8_encode($RFP['bairro']), utf8_encode($RFP['cidade_nome']), utf8_encode($RFP['estado_sigla']), utf8_encode($RFP['cep']), utf8_encode($RFP['telefone_contato']), utf8_encode($RFP['email_contato']), $cidade_gov, $RFP['codigo_enq'], $cnae, escreverNumero($aliquota), escreverNumero($RFP['tot_val_bruto']), escreverNumero(0), escreverNumero(0), escreverNumero(0), escreverNumero(0), escreverNumero(0), escreverNumero(0), escreverNumero(0), escreverNumero(0), escreverNumero(0), escreverNumero((($RFP['tot_val_bruto'] * $aliquota) / 100)), utf8_encode($RFP['info_add_fisco'])) . $endFile;
        $i++;
    }
    $content .= criarRodape($i, escreverNumero($total), "0", "0", "0") . $endFile;
    if (!file_exists($PATH)) {
        mkdir($PATH, 0777, true);
    }
    if (!$handle = fopen($filename, 'w+')) {
        erro("Não foi possível abrir o arquivo ($filename)");
    }
    if (fwrite($handle, $content) === FALSE) {
        echo "Não foi possível escrever no arquivo ($filename)";
    }
    fclose($handle);
    header("Content-Type: " . "application/txt");
    header("Content-Length: " . filesize($filename));
    header("Content-Disposition: attachment; filename=" . basename($filename));
    readfile($filename);
}

odbc_close($con);
