<?php

require_once('../../../Connections/configini.php');
error_reporting(E_ALL);
ini_set('display_errors', 'On');
include __DIR__ . '../../../../util/nfe/vendor/autoload.php';

use NFePHP\NFe\ToolsNFe;

$nfe = new ToolsNFe('../../../Pessoas/' . $contrato . '/Emitentes/config.json');
$tpAmb = $nfe->aConfig['tpAmb'];

$nfe->setModelo($_POST["txtModelo"]);

$aResposta = array();
$chave = $_POST["txtChave"];
$nProt = $_POST["txtProtocolo"];
$xJust = $_POST["txtJust"];
$retorno = $nfe->sefazCancela($chave, $tpAmb, $xJust, $nProt, $aResposta);

if ($aResposta['evento'][0]['cStat'] == "135") {
    odbc_exec($con, "update sf_nfe_notas set status_nfe = 'Cancelada',dtCancelamento = getdate() where cod_nfe = " . $_POST['txtId']) or die(odbc_errormsg());
    echo "YES";
} else {
    echo $aResposta['evento'][0]['xMotivo'];
}
odbc_close($con);
