<?php

header('Content-Type: text/html; charset=utf-8');
require(__DIR__ . '../../../../util/nfse/src/eNotasGW.php');

use eNotasGW\Api\Exceptions as Exceptions;

require_once('../../../Connections/configini.php');

if (is_numeric($_POST['txtId']) && is_numeric($_POST['txtNota'])) {
    $cur = odbc_exec($con, "select * from sf_configuracao where id = 1");
    while ($RFP = odbc_fetch_array($cur)) {
        $environmentType = $RFP['Nfs_Ambiente'];
        $txtNfsEmpresa = $RFP['Nfs_Empresa'];
        $txtNfsChave = $RFP['Nfs_Chave'];
    }
    eNotasGW::configure(array('apiKey' => $txtNfsChave));
    $idNota = $_POST['txtNota'];

    try {
        eNotasGW::$NFeApi->cancelarPorIdExterno($txtNfsEmpresa, $idNota);
        odbc_exec($con, "update sf_nfe_notas set status_nfe = 'Cancelada',dtCancelamento = getdate() where cod_nfe = " . $_POST['txtId']) or die(odbc_errormsg());
        echo 'YES';
    } catch (Exceptions\invalidApiKeyException $ex) {
        $query = "update sf_nfe_notas set status_nfe = 'Em Digitação',link_pdf = '',link_xml = '' where cod_nfe = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        echo 'Erro de autenticação: </br></br>';
        echo $ex->getMessage();
    } catch (Exceptions\unauthorizedException $ex) {
        $query = "update sf_nfe_notas set status_nfe = 'Em Digitação',link_pdf = '',link_xml = '' where cod_nfe = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        echo 'Erro Acesso negado: </br></br>';
        echo $ex->getMessage();
    } catch (Exceptions\apiException $ex) {
        $query = "update sf_nfe_notas set status_nfe = 'Em Digitação',link_pdf = '',link_xml = '' where cod_nfe = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        echo 'Erro de validação: </br></br>';
        echo $ex->getMessage();
    } catch (Exceptions\requestException $ex) {
        $query = "update sf_nfe_notas set status_nfe = 'Em Digitação',link_pdf = '',link_xml = '' where cod_nfe = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        echo 'Erro na requisição web: </br></br>';
        echo 'Requested url: ' . $ex->requestedUrl;
        echo 'Response Code: ' . $ex->getCode();
        echo 'Message: ' . $ex->getMessage();
        echo 'Response Body: ' . $ex->responseBody;
    }
}
odbc_close($con);
