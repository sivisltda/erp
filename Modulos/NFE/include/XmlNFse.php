<?php

header('Content-Type: text/html; charset=utf-8');
require(__DIR__ . '../../../../util/nfse/src/eNotasGW.php');

use eNotasGW\Api\Exceptions as Exceptions;

require_once('../../../Connections/configini.php');

if (is_numeric($_POST['txtId']) && is_numeric($_POST['txtNota'])) {
    $cur = odbc_exec($con, "select * from sf_configuracao where id = 1");
    while ($RFP = odbc_fetch_array($cur)) {
        $environmentType = $RFP['Nfs_Ambiente'];
        $txtNfsEmpresa = $RFP['Nfs_Empresa'];
        $txtNfsChave = $RFP['Nfs_Chave'];
    }
    eNotasGW::configure(array('apiKey' => $txtNfsChave));
    $idNota = $_POST['txtNota'];

    try {
        $xml = eNotasGW::$NFeApi->downloadXmlPorIdExterno($txtNfsEmpresa, $idNota);
        $folder = 'Downloads';
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }
        $xmlFileName = "{$folder}/NF-{$nfeId}.xml";
        file_put_contents($xmlFileName, $xml);
        echo "Download do xml, arquivo salvo em \"{$xmlFileName}\"";
    } catch (Exceptions\invalidApiKeyException $ex) {
        echo 'Erro de autenticação: </br></br>';
        echo $ex->getMessage();
    } catch (Exceptions\unauthorizedException $ex) {
        echo 'Erro Acesso negado: </br></br>';
        echo $ex->getMessage();
    } catch (Exceptions\apiException $ex) {
        echo 'Erro de validação: </br></br>';
        echo $ex->getMessage();
    } catch (Exceptions\requestException $ex) {
        echo 'Erro na requisição web: </br></br>';
        echo 'Requested url: ' . $ex->requestedUrl;
        echo '</br>';
        echo 'Response Code: ' . $ex->getCode();
        echo '</br>';
        echo 'Message: ' . $ex->getMessage();
        echo '</br>';
        echo 'Response Body: ' . $ex->responseBody;
    }
}
odbc_close($con);
