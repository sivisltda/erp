<?php

require_once('../../../Connections/configini.php');
error_reporting(E_ALL);
ini_set('display_errors', 'On');
include __DIR__ . '../../../../util/nfe/vendor/autoload.php';

use NFePHP\NFe\ToolsNFe;

$nfe = new ToolsNFe('../../../Pessoas/' . $contrato . '/Emitentes/config.json');

$tpAmb = $nfe->aConfig['tpAmb'];

$aResposta = array();
$saveFile = true;

if (isset($_POST["txtRecibo"]) && is_numeric($_POST["txtRecibo"])) {
    $nfe->setModelo($_POST["txtModelo"]);
    $retorno = $nfe->sefazConsultaRecibo($_POST["txtRecibo"], $tpAmb, $aResposta);
    $reposta = $aResposta["aProt"];
    if (count($reposta) > 0) {
        if (is_numeric($reposta[0]["chNFe"])) {
            $chave = $reposta[0]["chNFe"];
            if ($reposta[0]["cStat"] == 100) {
                $pathNFefile = "../../../Pessoas/" . $contrato . "/NFE/" . ($tpAmb == '2' ? "homologacao" : "producao") . "/assinadas/$chave-nfe.xml";
                $pathProtfile = "../../../Pessoas/" . $contrato . "/NFE/" . ($tpAmb == '2' ? "homologacao" : "producao") . "/temporarias/" . date("Ym") . "/" . $_POST["txtRecibo"] . "-retConsReciNFe.xml";
                $retorno = $nfe->addProtocolo($pathNFefile, $pathProtfile, $saveFile);
                odbc_exec($con, "update sf_nfe_notas set status_nfe = 'Autorizada',dhRecbto = getdate(),xml = '" . $retorno . "', nProt = '" . $reposta[0]["nProt"] . "' where cod_nfe = " . $_POST['txtId']) or die(odbc_errormsg());
                echo "YES";
            } else {
                odbc_exec($con, "update sf_nfe_notas set status_nfe = 'Rejeitada',dhRecbto = getdate() where cod_nfe = " . $_POST['txtId']) or die(odbc_errormsg());
                echo print_r($reposta);
            }
        } else {
            echo "Chave inválida!";
        }
    } else {
        odbc_exec($con, "update sf_nfe_notas set status_nfe = 'Rejeitada',dhRecbto = getdate() where cod_nfe = " . $_POST['txtId']) or die(odbc_errormsg());
        echo $retorno["xMotivo"];
    }
} else {
    echo "Recibo inválido!";
}
odbc_close($con);
