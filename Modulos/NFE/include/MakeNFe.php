<?php

require_once('../../../Connections/configini.php');
error_reporting(E_ALL);
ini_set('display_errors', 'On');
include __DIR__ . '../../../../util/nfe/vendor/autoload.php';

use NFePHP\NFe\MakeNFe;
use NFePHP\NFe\ToolsNFe;

$nfe = new MakeNFe();
$nfeX = new ToolsNFe('../../../Pessoas/' . $contrato . '/Emitentes/config.json');
$tpAmb = $nfeX->aConfig['tpAmb']; //1=Produção; 2=Homologação

if (is_numeric($_POST['txtId'])) {
    if ($_POST['txtModelo'] == '65') {
        $res = odbc_exec($con, "select isnull(max(nnf),0) + 1 nnf from sf_nfe_notas where mod > 0") or die(odbc_errormsg());
        $numNFE = odbc_result($res, 1);
        odbc_exec($con, "update sf_nfe_notas set status = 'Aprovado', hSaiEmi = getdate(), nnf = " . $numNFE .
                " where nnf = 0 and cod_nfe = " . $_POST['txtId']);
    }
    $cur = odbc_exec($con, "select *
    ,(select max(estado_sigla) from tb_estados where estado_codigo = estado_veiculo) estado_veiculo_gov 
    ,(select max(estado_sigla) from tb_estados where estado_codigo = reboque_veiculo_estado) reboque_gov 
    from sf_nfe_notas inner join tb_cidades on tb_cidades.cidade_codigo = sf_nfe_notas.cmunfg
    inner join tb_estados on tb_estados.estado_codigo = tb_cidades.cidade_codigoEstado
    inner join sf_fornecedores_despesas on sf_fornecedores_despesas.id_fornecedores_despesas = sf_nfe_notas.codigo_emitente    
    where cod_nfe = " . $_POST['txtId']);
    while ($RFP = odbc_fetch_array($cur)) {
        $cUF = $RFP['estado_gov'];
        $cNF = utf8_encode($RFP['cnf']);
        $natOp = utf8_encode($RFP['natop']);
        $indPag = utf8_encode($RFP['indPag']);
        $mod = utf8_encode($RFP['mod']);
        $nfeX->setModelo($mod);
        $serie = $RFP['serie'];
        $nNF = $RFP['nnf'];
        $dhEmi = escreverData($RFP['demis'], 'Y-m-d') . "T" . escreverData($RFP['hSaiEmi'], 'H:i:s') . "-03:00"; //não informar para NFCe
        $dhSaiEnt = escreverData($RFP['dSaiEnt'], 'Y-m-d') . "T" . escreverData($RFP['hSaiEnt'], 'H:i:s') . "-03:00";
        $tpNF = utf8_encode($RFP['tpnf']);
        $idDest = utf8_encode($RFP['destOper']);
        $cMunFG = utf8_encode($RFP['cidade_gov']);
        $tpImp = utf8_encode($RFP['tpImp']);
        $tpEmis = utf8_encode($RFP['tpemis']);
        $cDV = utf8_encode($RFP['cdv']);
        $finNFe = utf8_encode($RFP['finNFe']);
        $indFinal = utf8_encode($RFP['conFinal']);
        $indPres = utf8_encode($RFP['tipoAten']);
        $procEmi = utf8_encode($RFP['procEmi']);
        $verProc = utf8_encode($RFP['verProc']);
        $dhCont = $RFP['contigencia_hora'] != "" ? escreverData($RFP['contigencia_hora'], 'Y-m-d') . "T" . escreverData($RFP['contigencia_hora'], 'H:i:s') . "-03:00" : "";
        $xJust = utf8_encode($RFP['contingencia_motivo']);
        $tempData = explode("-", $dhEmi);
        $ano = $tempData[0] - 2000;
        $mes = $tempData[1];
        $cnpj = CharClear(utf8_encode($RFP['cnpj']));
        $chave = $nfe->montaChave($cUF, $ano, $mes, $cnpj, $mod, $serie, $nNF, $tpEmis, $cNF);
        $versao = utf8_encode($RFP['versao_xml']);
        $resp = $nfe->taginfNFe($chave, $versao);
        $cDV = substr($chave, -1);
        $resp = $nfe->tagide($cUF, $cNF, $natOp, $mod, $serie, $nNF, $dhEmi, $dhSaiEnt, $tpNF, $idDest, $cMunFG, $tpImp, $tpEmis, $cDV, $tpAmb, $finNFe, $indFinal, $indPres, $procEmi, $verProc, $dhCont, $xJust);
        $Cod_Emitente = utf8_encode($RFP['codigo_emitente']);
        $Cod_Destinatario = utf8_encode($RFP['codigo_destinatario']);
        //$cfop_padrao = utf8_encode($RFP['cfop_padrao']);
        $modFrete = utf8_encode($RFP['mod_transp']);
        $Cod_Transp = utf8_encode($RFP['cod_transp']);
        $placa_veiculo = utf8_encode($RFP['placa_veiculo']);
        $trans_estado = utf8_encode($RFP['estado_veiculo']);
        $trans_estado_sigla = utf8_encode($RFP['estado_veiculo_gov']);
        $reboque = utf8_encode($RFP['reboque_veiculo']);
        $placa_veiculo_reboque = utf8_encode($RFP['reboque_veiculo_placa']);
        $reboq_estado = utf8_encode($RFP['reboque_veiculo_estado']);
        $reboq_estado_gov = utf8_encode($RFP['reboque_gov']);
        /* $tipoVol = utf8_encode($RFP['tipo_volume']);*/
        $Volume_Transp = utf8_encode($RFP['volume_transp']);
        /*$Peso_liq_Transp = utf8_encode($RFP['peso_liq_transp']);
        $Peso_bru_Transp = utf8_encode($RFP['peso_bruto_transp']);
        $end_entrega = utf8_encode($RFP['endereco_entrega']); */
        $infAdFisco = utf8_encode($RFP['info_add_fisco']);
        $infCpl = utf8_encode($RFP['info_add_complementar']);
        $det_pgto = utf8_encode($RFP['detPag']);
    }
    if (is_numeric($Cod_Emitente)) {
        $cur = odbc_exec($con, "select *,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato
            from sf_fornecedores_despesas f 
            inner join tb_cidades c on f.cidade = c.cidade_codigo
            inner join tb_estados e on e.estado_codigo = c.cidade_codigoEstado
            where id_fornecedores_despesas = " . $Cod_Emitente) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            if (strlen(CharClear(utf8_encode($RFP['cnpj']))) == 11) {
                $CNPJ = '';
                $CPF = CharClear(utf8_encode($RFP['cnpj']));
            } else {
                $CNPJ = CharClear(utf8_encode($RFP['cnpj']));
                $CPF = '';
            }
            $xNome = utf8_encode($RFP['razao_social']);
            $xFant = utf8_encode($RFP['nome_fantasia']);
            $IE = CharClear($RFP['inscricao_estadual']);
            $IEST = utf8_encode($RFP['ie_tributario']);
            $IM = CharClear($RFP['inscricao_municipal']);
            $CNAE = utf8_encode($RFP['cnae_fiscal']);
            $CRT = utf8_encode($RFP['regime_tributario']);
            $resp = $nfe->tagemit($CNPJ, $CPF, $xNome, $xFant, $IE, $IEST, $IM, $CNAE, $CRT);
            $xLgr = utf8_encode($RFP['endereco']);
            $nro = utf8_encode($RFP['numero']);
            $xCpl = utf8_encode($RFP['complemento']);
            $xBairro = utf8_encode($RFP['bairro']);
            $cMun = utf8_encode($RFP['cidade_gov']);
            $xMun = CharReplace(utf8_encode($RFP['cidade_nome']));
            $UF = utf8_encode($RFP['estado_sigla']);
            $CEP = CharClear(utf8_encode($RFP['cep']));
            $cPais = '1058';
            $xPais = 'BRASIL';
            $fone = $RFP['telefone_contato'] !== "" ? substr(CharClear(utf8_encode($RFP['telefone_contato'])), 0, 10) : "";
            $resp = $nfe->tagenderEmit($xLgr, $nro, $xCpl, $xBairro, $cMun, $xMun, $UF, $CEP, $cPais, $xPais, $fone);
        }
    }
    if (is_numeric($Cod_Destinatario)) {
        $cur = odbc_exec($con, "select *,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = id_fornecedores_despesas) email_contato
            from sf_fornecedores_despesas f inner join tb_cidades c on f.cidade = c.cidade_codigo
            inner join tb_estados e on e.estado_codigo = c.cidade_codigoEstado
            where id_fornecedores_despesas = " . $Cod_Destinatario) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $idEstrangeiro = '';
            if (strlen(CharClear(utf8_encode($RFP['cnpj']))) == 11) {
                $CNPJ = '';
                $CPF = CharClear(utf8_encode($RFP['cnpj']));
            } else {
                $CNPJ = CharClear(utf8_encode($RFP['cnpj']));
                $CPF = '';
            }
            $xNome = utf8_encode($RFP['razao_social']);
            $indIEDest = CharClear($RFP['regime_tributario']);
            $IE = CharClear($RFP['inscricao_estadual']);
            $ISUF = CharClear($RFP['ie_tributario']);
            $IM = CharClear($RFP['inscricao_municipal']);
            $email = CharSplit(utf8_encode($RFP['email_contato']), ";");
            $resp = $nfe->tagdest($CNPJ, $CPF, $idEstrangeiro, $xNome, $indIEDest, $IE, $ISUF, $IM, $email);
            $xLgr = utf8_encode($RFP['endereco']);
            $nro = utf8_encode($RFP['numero']);
            $xCpl = utf8_encode($RFP['complemento']);
            $xBairro = utf8_encode($RFP['bairro']);
            $cMun = utf8_encode($RFP['cidade_gov']);
            $xMun = CharReplace(utf8_encode($RFP['cidade_nome']));
            $UF = utf8_encode($RFP['estado_sigla']);
            $CEP = CharClear(utf8_encode($RFP['cep']));
            $cPais = '1058';
            $xPais = 'BRASIL';
            $fone = $RFP['telefone_contato'] !== "" ? substr(CharClear(utf8_encode($RFP['telefone_contato'])), 0, 10) : "";
            $resp = $nfe->tagenderDest($xLgr, $nro, $xCpl, $xBairro, $cMun, $xMun, $UF, $CEP, $cPais, $xPais, $fone);
        }
    }
//Produtos-------------------------------------------------------------------------------------------------------    
    $frete = 0;
    $seguro = "";
    $desconto = 0;
    $outradesp = "";
    $etotal = 1;
    $totalTributos = 0;
    $val_bruto = 0;
    $i = 1;
    $vICMSDesonTotal = 0;
    $vICMSTotal = 0;
    $vFCPTotal = 0;
    $vBCTotal = 0;
    $vBCSTTotal = 0;
    $vST = 0;
    $vPISTotal = 0;
    $vCOFINSTotal = 0;
    $vICMSDeson = 0;
    $cur = odbc_exec($con, "select gc.descricao grupo,codigo_interno,codigo_barra,p.descricao,ncm,unidade_comercial,
        qtd_comercial,tot_val_bruto,unidade_trib,unidade_trib,qtd_trib,valor_unitario_trib,preco_venda,cod_nota,cod_item_nota,
        origem,substituicao_trib,quantidade_comercial, i.*, tot_frete, i.cfop 
        from sf_nfe_notas_itens i inner join sf_produtos p on p.conta_produto = i.cod_prod 
        inner join sf_contas_movimento gc on p.conta_movimento = gc.id_contas_movimento
        --left join sf_nfe_cenarios nfe on nfe.id_cenario = p.cenario
        where p.tipo = 'P' and cod_nota = " . $_POST['txtId']);
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['tot_frete'] > 0) {
            $frt = escreverNumero($RFP['tot_frete'], 0, 2, '.', '');
        } else {
            $frt = "";
        }
        $cProd = utf8_encode($RFP['codigo_interno']);
        $cEAN = utf8_encode($RFP['codigo_barra']);
        $xProd = utf8_encode($RFP['descricao']);
        $NCM = utf8_encode($RFP['ncm']);
        $cBenef = "";
        $EXTIPI = ""; //utf8_encode($RFP['substituicao_trib']);
        $CFOP = utf8_encode($RFP['cfop']);
        $uCom = utf8_encode($RFP['unidade_comercial']);
        $qCom = utf8_encode($RFP['qtd_comercial']);
        $vUnCom = escreverNumero(($RFP['tot_val_bruto'] / $RFP['qtd_comercial']), 0, 4, '.', '');
        $vProd = escreverNumero($RFP['tot_val_bruto'], 0, 2, '.', '');
        $cEANTrib = utf8_encode($RFP['codigo_barra']);
        $uTrib = utf8_encode($RFP['unidade_trib']);
        $qTrib = utf8_encode($RFP['qtd_trib']);
        $vUnTrib = escreverNumero(($RFP['tot_val_bruto'] / $RFP['qtd_trib']), 0, 4, '.', '');
        $vFrete = $frt;
        $vSeg = "";
        $vDesc = $RFP['tot_desconto'] > 0 ? escreverNumero($RFP['tot_desconto'], 0, 2, '.', '') : "";
        $vOutro = "";
        $indTot = "1";
        $xPed = "";
        $nItemPed = "";
        $nFCI = "";
        $resp = $nfe->tagprod($i, $cProd, $cEAN, $xProd, $NCM, $cBenef, $EXTIPI, $CFOP, $uCom, $qCom, $vUnCom, $vProd, $cEANTrib, $uTrib, $qTrib, $vUnTrib, $vFrete, $vSeg, $vDesc, $vOutro, $indTot, $xPed, $nItemPed, $nFCI);
//---------------------------------------------------------------------------------------------------------------        
        /* $vDesc = 'Sivis ERP';
          $resp = $nfe->taginfAdProd($i, $vDesc); */
//---------------------------------------------------------------------------------------------------------------
        $vTotTrib = '';
        $resp = $nfe->tagimposto($i, $vTotTrib);
        //INICIO ICMS-----------------------------------------------------------------------------------------------------------
        //$conteudo .= 'N10d|' . utf8_encode($RFP['ICMSOrigem']) . '|' . utf8_encode($RFP['ICMSSitTrib']) . '|' . chr(13) . chr(10); //ICMS (Simples Nascional)
        $csosn = '';
        $cst = '';
        $pCredSN = '';
        $vCredICMSSN = '';
        $orig = utf8_encode($RFP['icms_origem']);
        $vICMS = 0;
        $vFCP = 0;
        if ($CRT === '1') {
            $csosn = utf8_encode($RFP['icms_sit_trib']);
            $pCredSN = escreverNumero($RFP['icms_alig_calc_cred'], 0, 2, '.', ''); //estava '';       % 1
            $vCredICMSSN = escreverNumero((($RFP['tot_val_bruto'] * $RFP['icms_alig_calc_cred']) / 100), 0, 2, '.', ''); //estava ''; calculo            
            $infAdFisco = str_replace("||pCredSN||", escreverNumero($RFP['icms_alig_calc_cred']), $infAdFisco);
            $infAdFisco = str_replace("||vCredICMSSN||", escreverNumero((($RFP['tot_val_bruto'] * $RFP['icms_alig_calc_cred']) / 100), 1), $infAdFisco);
        } else {
            $cst = str_pad(substr(utf8_encode($RFP['icms_sit_trib']), 0, 2), 2, "0", STR_PAD_LEFT); //ESTAVA 41
            $ufST = ''; //não esta sendo usado
            $pBCOp = ''; //não esta sendo usado
            $motDesICMS = $RFP['icms_mot_deson']; //estava '';
            $vICMSDeson = escreverNumero($RFP['icms_val_deson'], 0, 2, '.', ''); //estava '';
            $vBCSTDest = ''; //não esta sendo usado;
            $vICMSSTDest = ''; //não esta sendo usado;
        }
        $vBC = 0;
        if ($RFP['icms_aliq'] > 0) {
            $vBC = escreverNumero($RFP['tot_val_bruto'], 0, 2, '.', '');  //estava ''    calculo 4
            $vICMS = escreverNumero((($RFP['tot_val_bruto'] * $RFP['icms_aliq']) / 100), 0, 2, '.', '');
        }
        if ($RFP['icms_fcp'] > 0) {
            $vFCP = escreverNumero((($RFP['tot_val_bruto'] * $RFP['icms_fcp']) / 100), 0, 2, '.', '');
        }        
        //$vBC = escreverNumero($vBC, 0, 2, '.', '');  //estava ''    calculo 4
        $modBC = utf8_encode($RFP['icms_mod_det']) !== "" ? utf8_encode($RFP['icms_mod_det']) : "3"; //estava ''
        $pRedBC = escreverNumero($RFP['icms_red_bc'], 0, 2, '.', ''); //estava ''    em cima desse 4
        $pICMS = escreverNumero($RFP['icms_aliq'], 0, 2, '.', ''); //estava ''    %5
        $vICMS = escreverNumero($vICMS, 0, 2, '.', ''); //estava '';    calcular 5
        $pFCP = escreverNumero($RFP['icms_fcp'], 0, 2, '.', ''); //estava ''    %5        
        $vFCP = escreverNumero($vFCP, 0, 2, '.', ''); //estava '';    calcular 5
        $modBCST = utf8_encode($RFP['icms_mod_det_st']) !== "" ? utf8_encode($RFP['ICMSModDetSt']) : ""; //estava ''
        $pMVAST = escreverNumero($RFP['icms_marg_va_st'], 0, 2, '.', ''); //estava '';
        $pRedBCST = escreverNumero($RFP['icms_red_bc_st'], 0, 2, '.', ''); //estava '';  % 2
        $vBCST = escreverNumero((($vBC * $RFP['icms_red_bc_st']) / 100), 0, 2, '.', ''); //estava '';     calcular 2
        $pICMSST = escreverNumero($RFP['icms_aliq_st'], 0, 2, '.', ''); //estava '';;   % 3
        $vICMSST = escreverNumero((($vBC * $RFP['icms_aliq_st']) / 100), 0, 2, '.', '');  //estava '';     calcular 3
        $vBCSTRet = escreverNumero($RFP['icms_bc_ret_st'], 0, 2, '.', ''); //estava '';; =
        $vICMSSTRet = escreverNumero($RFP['icms_ret_st'], 0, 2, '.', '');
        $pDif = '';
        $vICMSDif = '';
        $vICMSOp = '';
        if ($CRT === "1") {
            $resp = $nfe->tagICMSSN($i, $orig, $csosn, $modBC, $vBC, $pRedBC, $pICMS, $vICMS, $pCredSN, $vCredICMSSN, $modBCST, $pMVAST, $pRedBCST, $vBCST, $pICMSST, $vICMSST, $vBCSTRet, $vICMSSTRet);
        } else {
            $resp = $nfe->tagICMS($i, $orig, $cst, $modBC, $pRedBC, $vBC, $pICMS, $vICMS, $vICMSDeson, $motDesICMS, $modBCST, $pMVAST, $pRedBCST, $vBCST, $pICMSST, $vICMSST, $pDif, $vICMSDif, $vICMSOp, $vBCSTRet, $vICMSSTRet, $pFCP, $vFCP);
        }
        if ($CRT !== '1') { //qual seria a base de calculo????
            //$vBCTotal = $vBCTotal + $RFP['tot_val_bruto'];
            //$vBCSTTotal = $vBCSTTotal + 0;
        }
        //FIM ICMS-----------------------------------------------------------------------------------------------------------
        //ICMSPart
        //$resp = $nfe->tagICMSPart($i, $orig, $cst, $modBC, $vBC, $pRedBC, $pICMS, $vICMS, $modBCST, $pMVAST, $pRedBCST, $vBCST, $pICMSST, $vICMSST, $pBCOp, $ufST);
        //ICMSST
        //$resp = $nfe->tagICMSST($i, $orig, $cst, $vBCSTRet, $vICMSSTRet, $vBCSTDest, $vICMSSTDest);
        //ICMSSN
        //IPI---------------------------------------------------------------------------------------------------------------
        /* $cst = '99';
          $clEnq = '';
          $cnpjProd = '';
          $cSelo = '';
          $qSelo = '';
          $cEnq = '999';
          $vBC = '0';
          $pIPI = '0';
          $qUnid = '';
          $vUnid = '';
          $vIPI = '0.00';
          $resp = $nfe->tagIPI($i, $cst, $clEnq, $cnpjProd, $cSelo, $qSelo, $cEnq, $vBC, $pIPI, $qUnid, $vUnid, $vIPI); */
//PIS---------------------------------------------------------------------------------------------------------------
        $cst = str_pad($RFP['pis_sit_trib'], 2, "0", STR_PAD_LEFT);
        $qBCProd = "";
        $vAliqProd = "";
        $vPIS = 0;
        $vBC = 0;
        $pPIS = 0;
        if ($RFP['pis_aliq_perc'] > 0) {
            $vBC = escreverNumero($RFP['tot_val_bruto'], 0, 2, '.', '');
            $pPIS = escreverNumero($RFP['pis_aliq_perc'], 0, 2, '.', '');
            if ($RFP['pis_aliq_perc'] > 0) { // percentual
                $vPIS = escreverNumero((($RFP['tot_val_bruto'] * $RFP['pis_aliq_perc']) / 100), 0, 2, '.', '');
                $vAliqProd = escreverNumero($RFP['pis_aliq_perc'], 0, 2, '.', ''); //escreverNumero($RFP['PISAliqReal'], 0, 4, '.', '');
            } else if ($RFP['pis_aliq_real'] > 0) {
                $vPIS = escreverNumero(($RFP['tot_val_bruto'] - $RFP['pis_aliq_real']), 0, 2, '.', '');
                $qBCProd = escreverNumero($RFP['pis_quantidade'], 0, 4, '.', ''); //escreverNumero($RFP['PISQtdVend'], 0, 4, '.', '');
            }
        }
        $resp = $nfe->tagPIS($i, $cst, $vBC, $pPIS, $vPIS, $qBCProd, $vAliqProd);
//PISST
//$resp = $nfe->tagPISST($i, $vBC, $pPIS, $qBCProd, $vAliqProd, $vPIS);
//COFINS---------------------------------------------------------------------------------------------------------------    
        $cst = str_pad($RFP['cofins_sit_trib'], 2, "0", STR_PAD_LEFT);
        $qBCProd = "";
        $vAliqProd = "";
        $vCOFINS = 0;
        $vBC = 0;
        $pCOFINS = 0;
        if ($RFP['cofins_aliq_perc'] > 0) {
            $vBC = escreverNumero($RFP['tot_val_bruto'], 0, 2, '.', '');
            $pCOFINS = escreverNumero($RFP['cofins_aliq_perc'], 0, 2, '.', '');
            if ($RFP['cofins_aliq_perc'] > 0) { // percentual
                $vCOFINS = escreverNumero((($RFP['tot_val_bruto'] * $RFP['cofins_aliq_perc']) / 100), 0, 2, '.', '');
                $vAliqProd = escreverNumero($RFP['cofins_aliq_real'], 0, 4, '.', '');
            } else if ($RFP['cofins_aliq_real'] > 0) {
                $vCOFINS = escreverNumero(($RFP['tot_val_bruto'] - $RFP['cofins_aliq_real']), 0, 2, '.', '');
                $qBCProd = escreverNumero($RFP['cofins_quantidade'], 0, 4, '.', '');
            }
        }
        $resp = $nfe->tagCOFINS($i, $cst, $vBC, $pCOFINS, $vCOFINS, $qBCProd, $vAliqProd);
//COFINSST
//$resp = $nfe->tagCOFINSST($i, $vBC, $pCOFINS, $qBCProd, $vAliqProd, $vCOFINS);
//II--------------------------------------------------------------------------------------------------------------------
//$resp = $nfe->tagII($i, $vBC, $vDespAdu, $vII, $vIOF);
//ICMSTot---------------------------------------------------------------------------------------------------------------
//$resp = $nfe->tagICMSTot($vBC, $vICMS, $vICMSDeson, $vFCPUFDest, $vICMSUFDest, $vICMSUFRemet, $vFCP, $vBCST, $vST, $vFCPST, $vFCPSTRet, $vProd, $vFrete, $vSeg, $vDesc, $vII, $vIPI, $vIPIDevol, $vPIS, $vCOFINS, $vOutro, $vNF, $vTotTrib);
//ISSQNTot--------------------------------------------------------------------------------------------------------------
//$resp = $nfe->tagISSQNTot($vServ, $vBC, $vISS, $vPIS, $vCOFINS, $dCompet, $vDeducao, $vOutro, $vDescIncond, $vDescCond, $vISSRet, $cRegTrib);
//retTrib---------------------------------------------------------------------------------------------------------------
//$resp = $nfe->tagretTrib($vRetPIS, $vRetCOFINS, $vRetCSLL, $vBCIRRF, $vIRRF, $vBCRetPrev, $vRetPrev);
        $vICMSDesonTotal = $vICMSDesonTotal + $vICMSDeson;
        $vICMSTotal = $vICMSTotal + $vICMS;
        $vFCPTotal = $vFCPTotal + $vFCP;
        $vST = $vST + $vICMSST;
        $vPISTotal = $vPISTotal + $vPIS;
        $vCOFINSTotal = $vCOFINSTotal + $vCOFINS;
        $val_bruto = $val_bruto + $RFP['tot_val_bruto'];
        $frete = $frete + $RFP['tot_frete'];
        $desconto = $desconto + $RFP['tot_desconto'];
        $i++;
    }
//total-----------------------------------------------------------------------------------------------------------------
    //$vICMSDesonTotal = '0.00';
    //$vICMSTotal = '0.00';
    //$vBCTotal = '0.00';
    //$vBCSTTotal = '0.00';
    //$vST = '0.00';
    //$vPIS = '0.00';
    //$vCOFINS = '0.00';
    $vTotTrib = '0.00';
    $vProd = escreverNumero($val_bruto, 0, 2, '.', '');
    $vFrete = escreverNumero($frete, 0, 2, '.', '');
    $vSeg = '0.00';
    $vDesc = escreverNumero($desconto, 0, 2, '.', '');
    $vII = '0.00';
    $vIPI = '0.00';
    $vOutro = '0.00'; //outras despesas acessorias    
    if ($CRT === '1') {
        $vNF = '0.00';    
    } else {
        $vNF = escreverNumero(($val_bruto - $desconto), 0, 2, '.', '');
    }
    $vNF2 = escreverNumero(($val_bruto + $frete - $desconto), 0, 2, '.', '');
    //$vTotTrib = escreverNumero(($vICMSTotal + $vPISTotal + $vCOFINSTotal), 0, 2, '.', '');
    $vBCTotal = escreverNumero(($vBCTotal), 0, 2, '.', '');
    $vBCSTTotal = escreverNumero(($vBCSTTotal), 0, 2, '.', '');
    $vICMSTotal = escreverNumero(($vICMSTotal), 0, 2, '.', '');
    $vICMSDesonTotal = escreverNumero(($vICMSDesonTotal), 0, 2, '.', '');    
    $vFCPUFDest = '0.00';
    $vICMSUFDest = '0.00';
    $vICMSUFRemet = '0.00';
    $vFCPTotal = escreverNumero(($vFCPTotal), 0, 2, '.', '');
    $vFCPST = '0.00';
    $vFCPSTRet = '0.00';
    $vIPIDevol = '0.00';            
    $resp = $nfe->tagICMSTot($vNF, $vICMSTotal, $vICMSDeson, $vFCPUFDest, $vICMSUFDest, $vICMSUFRemet, $vFCPTotal, $vBCST, $vST, $vFCPST, $vFCPSTRet, $vProd, $vFrete, $vSeg, $vDesc, $vII, $vIPI, $vIPIDevol, $vPIS, $vCOFINS, $vOutro, $vNF2, $vTotTrib);
//frete---------------------------------------------------------------------------------------------------------
    $resp = $nfe->tagtransp($modFrete);
//transportadora------------------------------------------------------------------------------------------------
    if (is_numeric($Cod_Transp)) {
        $cur = odbc_exec($con, "select *,
        (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato,
        (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = id_fornecedores_despesas) email_contato
        from sf_fornecedores_despesas f inner join tb_cidades c on f.cidade = c.cidade_codigo
        inner join tb_estados e on e.estado_codigo = c.cidade_codigoEstado
        where id_fornecedores_despesas = " . $Cod_Transp) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            if (strlen(CharClear(utf8_encode($RFP['cnpj']))) == 11) {
                $CNPJ = '';
                $CPF = CharClear(utf8_encode($RFP['cnpj']));
            } else {
                $CNPJ = CharClear(utf8_encode($RFP['cnpj']));
                $CPF = '';
            }
            $xNome = utf8_encode($RFP['razao_social']);
            $IE = CharClear($RFP['inscricao_estadual']);
            $xEnder = utf8_encode($RFP['endereco']);
            $xMun = CharReplace(utf8_encode($RFP['cidade_nome']));
            $UF = utf8_encode($RFP['estado_sigla']);
            $RNTC = utf8_encode($RFP['rntc']);
            $resp = $nfe->tagtransporta($CNPJ, $CPF, $xNome, $IE, $xEnder, $xMun, $UF);
            //valores retidos para transporte
            //$vServ = '258,69'; //Valor do Serviço
            //$vBCRet = '258,69'; //BC da Retenção do ICMS
            //$pICMSRet = '0,01'; //Alíquota da Retenção
            //$vICMSRet = '25,87'; //Valor do ICMS Retido        
            //$CFOP = '5352';
            //$cMunFG = '3509502'; //Código do município de ocorrência do fato gerador do ICMS do transporte
            //$resp = $nfe->tagretTransp($vServ, $vBCRet, $pICMSRet, $vICMSRet, $CFOP, $cMunFG);                
            //dados do veículo
            if (strlen($placa_veiculo) > 0) {
                $resp = $nfe->tagveicTransp($placa_veiculo, $trans_estado_sigla, $RNTC);
            }
            //dados dos reboques
            if ($reboque == "S") {
                $aReboque = array(array($placa_veiculo_reboque, $reboq_estado_gov, '', '', ''));
                foreach ($aReboque as $reb) {
                    $placa = $reb[0];
                    $UF = $reb[1];
                    $RNTC = $reb[2];
                    $vagao = $reb[3];
                    $balsa = $reb[4];
                    $resp = $nfe->tagreboque($placa, $UF, $RNTC, $vagao, $balsa);
                }
            }
            //dados dos volumes transportados
            if ($Volume_Transp > 0) {
                $aVol = array(array(escreverNumero($Volume_Transp,0,0), 'CAIXA', '', '', '', '', ''));
                foreach ($aVol as $vol) {
                    $qVol = $vol[0]; //Quantidade de volumes transportados
                    $esp = $vol[1]; //Espécie dos volumes transportados
                    $marca = $vol[2]; //Marca dos volumes transportados
                    $nVol = $vol[3]; //Numeração dos volume
                    $pesoL = $vol[4];
                    $pesoB = $vol[5];
                    $aLacres = $vol[6];
                    $resp = $nfe->tagvol($qVol, $esp, $marca, $nVol, $pesoL, $pesoB, $aLacres);
                }
            }
        }
    }
    if ($finNFe == 4 && strlen($infCpl) > 0) {  
        $resp = $nfe->tagrefNFe($infCpl);        
        $resp = $nfe->tagpag("", 90, 0.00, 0);
    } else {
        $resp = $nfe->tagpag($indPag, str_pad($det_pgto, 2, "0", STR_PAD_LEFT), escreverNumero($vNF, 0, 2, ".", ""), 0);
    }    
    $resp = $nfe->taginfAdic($infAdFisco, $infCpl);  
    $resp = $nfe->montaNFe();
    if ($resp) {        
        //echo $nfe->getXML(); exit;
        $xml = $nfeX->assina($nfe->getXML());
        if (!$nfeX->validarXml($xml)) {
            echo "Erro no XML: ";
            foreach ($nfeX->errors as $err) {
                if (count($err) > 0) {
                    echo $err[0];
                } else {
                    echo $err;
                }
            }
            exit();
        }
        if (is_numeric($_POST['txtId'])) {
            odbc_exec($con, "update sf_nfe_notas set status_nfe = 'Assinada',code='" . $chave . "' where cod_nfe = " . $_POST['txtId']) or die(odbc_errormsg());
            $filename = "../../../Pessoas/" . $contrato . "/NFE/" . ($tpAmb == '2' ? "homologacao" : "producao") . "/assinadas/$chave-nfe.xml";
            file_put_contents($filename, $xml);
            chmod($filename, 0777);
            echo $chave;
        } else {
            echo "Número de identificação inválido!";
        }
    } else {
        foreach ($nfe->erros as $err) {
            echo 'Erro: &lt;' . $err['tag'] . '&gt; ---- ' . $err['desc'] . '<br>';
        }
    }
}

function CharReplace($nome) {
    $caracteresBR = array('À', 'Á', 'Ã', 'Â', 'à', 'á', 'ã', 'â', 'Ê', 'É', 'Í', 'í', 'Ó', 'Õ', 'Ô', 'ó', 'õ', 'ô', 'Ú', 'Ü', 'Ç', 'ç', 'é', 'ê', 'ú', 'ü');
    $caracteresUS = array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'E', 'E', 'I', 'i', 'O', 'O', 'O', 'o', 'o', 'o', 'U', 'U', 'C', 'c', 'e', 'e', 'u', 'u');
    return str_replace($caracteresBR, $caracteresUS, $nome);
}

function CharClear($nome) {
    $caracteres = array(".", "-", "/", "(", ")", " ");
    return str_replace($caracteres, "", $nome);
}

function CharSplit($nome, $char) {
    $caracteres = explode($char, $nome);
    return $caracteres[0];
}

odbc_close($con);
