<?php

include "../../../Connections/configini.php";
$dirFile = "./../../../Pessoas/" . $contrato . "/NFSE/";

function rrmdir($dir) {
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (filetype($dir . "/" . $object) == "dir") {
                    rrmdir($dir . "/" . $object);
                } else {
                    unlink($dir . "/" . $object);
                }
            }
        }
        reset($objects);
        rmdir($dir);
    }
}

if (isset($_POST['bntSave'])) {
    if ($_POST['txtIni'] == "0") {
        if (is_dir($dirFile)) {
            rrmdir($dirFile);
        }
    }
    if (!is_dir($dirFile)) {
        mkdir($dirFile, 0777);
    }
    $cur = odbc_exec($con, "select nnf,link_xml from sf_nfe_notas where cod_nfe = " . $_POST['txtId']);
    while ($RFP = odbc_fetch_array($cur)) {
        $file = $RFP['nnf'];
        $url = $RFP['link_xml'];
    }
    if (strlen($url) > 0) {
        $src = fopen($url, 'r');
        $dest = fopen($dirFile . $file . ".xml", 'w');
        stream_copy_to_stream($src, $dest);
        echo "YES";
    } else {
        echo "Erro";
    }
}

if (isset($_REQUEST['bntDownload'])) {
    if (is_dir($dirFile)) {
        try {
            $zipname = "nfse_" . date("d_m_Y") . ".zip";
            $zip = new ZipArchive;
            $zip->open($dirFile . $zipname, ZipArchive::CREATE);
            $objects = scandir($dirFile);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dirFile . $object) != "dir") {
                        $zip->addFile($dirFile . $object, $object);
                    }
                }
            }
            reset($objects);
            $zip->close();
            header('Content-Type: application/zip');
            header('Content-disposition: attachment; filename=' . $zipname);
            header('Content-Length: ' . filesize($dirFile . $zipname));
            readfile($dirFile . $zipname);
        } catch (Exception $e) {
            echo 'Exceção capturada: ', $e->getMessage(), "\n";
        }
    }
}

odbc_close($con);
