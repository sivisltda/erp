<?php

header('Content-Type: text/html; charset=utf-8');
require(__DIR__ . '../../../../util/nfse/src/eNotasGW.php');

use eNotasGW\Api\Exceptions as Exceptions;

require_once('../../../Connections/configini.php');

if (is_numeric($_POST['txtId'])) {
    $cur = odbc_exec($con, "select * from sf_configuracao where id = 1");
    while ($RFP = odbc_fetch_array($cur)) {
        $environmentType = $RFP['Nfs_Ambiente'];
        $txtNfsEmpresa = $RFP['Nfs_Empresa'];
        $txtNfsChave = $RFP['Nfs_Chave'];
    }
    eNotasGW::configure(array('apiKey' => $txtNfsChave));
    $cur = odbc_exec($con, "select * from sf_nfe_notas inner join tb_cidades on tb_cidades.cidade_codigo = sf_nfe_notas.cmunfg
    inner join tb_estados on tb_estados.estado_codigo = tb_cidades.cidade_codigoEstado
    inner join sf_fornecedores_despesas on sf_fornecedores_despesas.id_fornecedores_despesas = sf_nfe_notas.codigo_emitente    
    where cod_nfe = " . $_POST['txtId']);
    while ($RFP = odbc_fetch_array($cur)) {
        $idExterno = str_pad(utf8_encode($RFP['nnf']), 9, "0", STR_PAD_LEFT);
        $Cod_Emitente = utf8_encode($RFP['codigo_emitente']);
        $Cod_Destinatario = utf8_encode($RFP['codigo_destinatario']);
        $infAdFisco = utf8_encode($RFP['info_add_fisco']);
        $ufPreSer = utf8_encode($RFP['estado_sigla']);
        $cidPreSer = utf8_encode($RFP['cidade_nome']);
    }
    if (is_numeric($Cod_Emitente)) {
        $cur = odbc_exec($con, "select *,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato in (0,1) and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato
            from sf_fornecedores_despesas f 
            inner join tb_cidades c on f.cidade = c.cidade_codigo
            inner join tb_estados e on e.estado_codigo = c.cidade_codigoEstado
            where id_fornecedores_despesas = " . $Cod_Emitente) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $CNAE = utf8_encode($RFP['cnae_fiscal']);
            $Aliquota = escreverNumero($RFP['comiss_gere'], 0, 2, '.', '');
        }
    }
    if (is_numeric($Cod_Destinatario)) {
        $cur = odbc_exec($con, "select *,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato in (0,1) and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = id_fornecedores_despesas) email_contato
            from sf_fornecedores_despesas f inner join tb_cidades c on f.cidade = c.cidade_codigo
            inner join tb_estados e on e.estado_codigo = c.cidade_codigoEstado
            where id_fornecedores_despesas = " . $Cod_Destinatario) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            if (strlen(CharClear(utf8_encode($RFP['cnpj']))) == 11) {
                $CNPJ = '';
                $CPF = CharClear(utf8_encode($RFP['cnpj']));
            } else {
                $CNPJ = CharClear(utf8_encode($RFP['cnpj']));
                $CPF = '';
            }
            $xNome = utf8_encode($RFP['razao_social']);
            $IE = CharClear($RFP['inscricao_estadual']);
            $IM = CharClear($RFP['inscricao_municipal']);
            $email = CharSplit(utf8_encode($RFP['email_contato']), ";");
            $xLgr = utf8_encode($RFP['endereco']);
            $nro = utf8_encode($RFP['numero']);
            $xCpl = utf8_encode($RFP['complemento']);
            $xBairro = utf8_encode($RFP['bairro']);
            $xMun = CharReplace(utf8_encode($RFP['cidade_nome']));
            $UF = utf8_encode($RFP['estado_sigla']);
            $CEP = CharClear(utf8_encode($RFP['cep']));
            $fone = $RFP['telefone_contato'] !== "" ? substr(CharClear(utf8_encode($RFP['telefone_contato'])), 0, 10) : "";
        }
    }
//Produtos-------------------------------------------------------------------------------------------------------    
    $val_bruto = 0;
    $frete = 0;
    $desconto = 0;
    $cur = odbc_exec($con, "select gc.descricao grupo,codigo_interno,codigo_barra,p.descricao,ncm,unidade_comercial,
        qtd_comercial,tot_val_bruto,unidade_trib,unidade_trib,qtd_trib,valor_unitario_trib,preco_venda,cod_nota,cod_item_nota,
        origem,substituicao_trib,quantidade_comercial, i.*, tot_frete, i.cfop 
        from sf_nfe_notas_itens i inner join sf_produtos p on p.conta_produto = i.cod_prod 
        left join sf_contas_movimento gc on p.conta_movimento = gc.id_contas_movimento
        where p.tipo in ('S','C') and cod_nota = " . $_POST['txtId']);
    while ($RFP = odbc_fetch_array($cur)) {
        $xProd = utf8_encode($RFP['descricao']);
        $vProd = escreverNumero($RFP['tot_val_bruto'], 0, 2, '.', '');
        $vDesc = $RFP['tot_desconto'] > 0 ? escreverNumero($RFP['tot_desconto'], 0, 2, '.', '') : "";
        $val_bruto = $val_bruto + $RFP['tot_val_bruto'];
        $frete = $frete + $RFP['tot_frete'];
        $desconto = $desconto + $RFP['tot_desconto'];
    }
//total-----------------------------------------------------------------------------------------------------------------
    $vProd = escreverNumero($val_bruto, 0, 2, '.', '');
    $vFrete = escreverNumero($frete, 0, 2, '.', '');
    $vDesc = escreverNumero($desconto, 0, 2, '.', '');
    $vNF = escreverNumero(($val_bruto + $frete - $desconto), 0, 2, '.', '');
    try {
        $post = array(
            "tipo" => "NFS-e", //Tipo da Nota Fiscal. "NFS-e" para NFe de Serviço e "NF-e" para NFe de Produto
            "ambienteEmissao" => ($environmentType == 0 ? "Homologacao" : "Producao"),
            //"id" => "string", //Identificador único da Nota Fiscal. Usado apenas em casos de atualização de uma nota fiscal existente
            "idExterno" => $idExterno, //Identificador externo da Nota Fiscal informado pelo consumidor da API ao emitir a Nota Fiscal        
            "consumidorFinal" => true, //Utilizado somente para Nota Conjugada (ex: Brasília/DF). Indica se a nota é para consumidor final seja ele Pessoa Física ou Jurídica. Caso não seja informado será considerado o valor padrão = true        
            "enviarPorEmail" => true, //Indica se esta nota deve ser enviada para o email do cliente.
            "indicadorPresencaConsumidor" => "NaoSeAplica", //Utilizado somente para Nota Conjugada (ex: Brasília/DF). Indica a presença do consumidor para recebimento do serviço. Caso não seja informado será considerado o valor padrão = "NaoSeAplica". Possíveis valores: "NaoSeAplica", "OperacaoPresencial", "OperacaoPelaInternet", "OperacaoTeleAtendimento", "Outros".
            "cliente" => array(
                "tipoPessoa" => ($CNPJ == '' ? "F" : "J"), //"F" para pessoa física e "J" para pessoa jurídica
                "nome" => $xNome, //Nome do cliente
                "cpfCnpj" => ($CNPJ == '' ? $CPF : $CNPJ), //CPF para pessoa física ou CNPJ para pessoa jurídica
                "inscricaoEstadual" => $IE, //Inscrição estadual do cliente, deve ser preenchido apenas para pessoa Jurídica           
                "inscricaoMunicipal" => $IM, //Inscrição municipal do cliente, deve ser preenchido apenas para pessoa física ou Jurídica do mesmo município que o Prestador
                //"indicadorContribuinteICMS" => "string",            
                "email" => $email, //Endereço de email
                "telefone" => $fone, //Telefone do cliente
                "endereco" => array(
                    "logradouro" => $xLgr,
                    "numero" => $nro,
                    "cidade" => $xMun, //Nome ou código IBGE da cidade
                    "uf" => $UF,
                    "complemento" => $xCpl,
                    "bairro" => $xBairro,
                    "cep" => $CEP,
                    "pais" => "BRA" //Sigla do País com 3 dígitos. Exemplo: BRA - Brasil, ARG - Argentina. Para maiores detalhes consulte http://portal.enotasgw.com.br/article-categories/paises. Caso não seja informado será considerado como padrão o valor "BRA".               
                )
            ),
            "servico" => array(//Detalhamento do serviço prestado
                "ufPrestacaoServico" => $ufPreSer, //Sigla do Estado onde o serviço foi prestado. Opcional caso o serviço tenha sido prestado no mesmo munícipio do prestador.
                "municipioPrestacaoServico" => $cidPreSer, //Nome ou código IBGE do munícipio onde o serviço foi prestado. Opcional caso o serviço tenha sido prestado no mesmo munícipio do prestador.                         
                "descricao" => $xProd, //Descrição do serviço prestado.
                "aliquotaIss" => $Aliquota, //Valor da aliquota Iss. Caso não seja informado será considerado o valor padrão configurado no cadastro da empresa (CNPJ emissor).            
                //"cnae" => $CNAE, //Código CNAE que identifica o serviço prestado. Caso não seja informado será considerado o valor padrão configurado no cadastro da empresa (CNPJ emissor). 
                //"codigoServicoMunicipio" => "string", //Código do serviço municipal conforme cadastro na prefeitura. Caso não seja informado será considerado o valor padrão configurado no cadastro da empresa (CNPJ emissor). Este código varia de acordo com a prefeitura, para maiores detalhes consulte http://portal.enotasgw.com.br/article-categories/prefeituras 
                "descricaoServicoMunicipio" => $infAdFisco, //Descrição do serviço municipal conforme cadastro na prefeitura. Caso não seja informado será considerado o valor padrão configurado no cadastro da empresa (CNPJ emissor). Este descrição varia de acordo com a prefeitura, para maiores detalhes consulte http://portal.enotasgw.com.br/article-categories/prefeituras
                //"itemListaServicoLC116" => "string", //Item da lista de serviço conforme a Lei Complementar 116 (LC116). Para maiores detalhes consulte http://portal.enotasgw.com.br/article-categories/lista-servicos-lc116                        
                "issRetidoFonte" => false, //Indica se o Iss deverá ser retido na fonte. Retenção fonte ISSQN ? Padrão é (false), estava true
                "valorInss" => 0, //Valor do INSS
                "valorIr" => 0, //Valor do IR            
                "valorCsll" => 0, //Valor do CSLL
                "valorPis" => 0, //Valor do PIS
                "valorCofins" => 0 //Valor do COFINS
            ),
            "valorTotal" => $vNF, //Valor total da Nota Fiscal
                //"idExternoSubstituir" => "string", //Id externo da nota que deseja substituir, apenas um dos parametros devem ser informados "idExternoSubstituir" ou "nfeIdSubstitituir"
                //"nfeIdSubstitituir" => "string" //Id único da nota que deseja substituir, apenas um dos parametros devem ser informados "idExternoSubstituir" ou "nfeIdSubstitituir"
        );
        //print_r($post); exit;
        if (is_numeric($_POST['txtId'])) {
            odbc_exec($con, "update sf_nfe_notas set status_nfe = 'Em Processamento' where cod_nfe = " . $_POST['txtId']) or die(odbc_errormsg());
        }         
        $nfeId = eNotasGW::$NFeApi->emitir($txtNfsEmpresa, $post);
        if ($nfeId) {
            if (is_numeric($_POST['txtId'])) {
                odbc_exec($con, "update sf_nfe_notas set code='" . $nfeId . "' where cod_nfe = " . $_POST['txtId']) or die(odbc_errormsg());
                echo $nfeId;
            } else {
                echo "Erro Número de identificação inválido!";
            }
        }
    } catch (Exceptions\invalidApiKeyException $ex) {
        echo 'Erro de autenticação: </br></br>';
        echo $ex->getMessage();
    } catch (Exceptions\unauthorizedException $ex) {
        echo 'Erro Acesso negado: </br></br>';
        echo $ex->getMessage();
    } catch (Exceptions\apiException $ex) {
        echo 'Erro de validação: </br></br>';
        echo $ex->getMessage();
    } catch (Exceptions\requestException $ex) {
        echo 'Erro na requisição web: </br></br>';
        echo 'Requested url: ' . $ex->requestedUrl;
        echo '</br>';
        echo 'Response Code: ' . $ex->getCode();
        echo '</br>';
        echo 'Message: ' . $ex->getMessage();
        echo '</br>';
        echo 'Response Body: ' . $ex->responseBody;
    }
}

function CharReplace($nome) {
    $caracteresBR = array('À', 'Á', 'Ã', 'Â', 'à', 'á', 'ã', 'â', 'Ê', 'É', 'Í', 'í', 'Ó', 'Õ', 'Ô', 'ó', 'õ', 'ô', 'Ú', 'Ü', 'Ç', 'ç', 'é', 'ê', 'ú', 'ü');
    $caracteresUS = array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'E', 'E', 'I', 'i', 'O', 'O', 'O', 'o', 'o', 'o', 'U', 'U', 'C', 'c', 'e', 'e', 'u', 'u');
    return str_replace($caracteresBR, $caracteresUS, $nome);
}

function CharClear($nome) {
    $caracteres = array(".", "-", "/", "(", ")", " ");
    return str_replace($caracteres, "", $nome);
}

function CharSplit($nome, $char) {
    $caracteres = explode($char, $nome);
    return $caracteres[0];
}

odbc_close($con);
