<?php

require_once('../../../Connections/configini.php');
error_reporting(E_ALL);
ini_set('display_errors', 'On');
include __DIR__ . '../../../../util/nfe/vendor/autoload.php';

use NFePHP\NFe\ToolsNFe;

$nfe = new ToolsNFe('../../../Pessoas/' . $contrato . '/Emitentes/config.json');
if (isset($_POST["txtChave"]) && is_numeric($_POST["txtChave"])) {
    $nfe->setModelo($_POST["txtModelo"]);
    $tpAmb = $nfe->aConfig['tpAmb'];

    $aResposta = array();
    $aXml = file_get_contents("../../../Pessoas/" . $contrato . "/NFE/" . ($tpAmb == '2' ? "homologacao" : "producao") . "/assinadas/" . $_POST["txtChave"] . "-nfe.xml");
    $idLote = '';
    $indSinc = '0';
    $flagZip = false;
    $retorno = $nfe->sefazEnviaLote($aXml, $tpAmb, $idLote, $aResposta, $indSinc, $flagZip);
    if (is_numeric($aResposta["nRec"])) {
        odbc_exec($con, "update sf_nfe_notas set status_nfe = 'Processamento SEFAZ',nRec = '" . $aResposta["nRec"] . "' where cod_nfe = " . $_POST['txtId']) or die(odbc_errormsg());
        echo $aResposta["nRec"];
    } else {
        echo "Erro de envio para o Servidor da SEFAZ " . $retorno;
    }
} else {
    echo "Chave invalida!!";
}
odbc_close($con);
