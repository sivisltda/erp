<?php
include "./../../Connections/configini.php";

if ($_GET['Del'] != '') {
    odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_fornecedores_despesas WHERE id_fornecedores_despesas = " . $_GET['Del']);
    echo "<script>window.top.location.href = 'Emitentes.php'; </script>";
}
$imprimir = 0;

if (isset($_POST['btnPrint'])) {
    $imprimir = 1;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php
                if ($imprimir == 0) {
                    include("../../top.php");
                }
                ?>
                <div class="content">
                    <?php if ($imprimir == 0) { ?>
                        <div class="page-header">
                            <div class="icon"> <span class="ico-arrow-right"></span></div>
                            <h1>Nota Fiscal Eletrônica<small>Emitentes</small></h1>
                        </div>
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="block" style="font-size:13px">
                                    <button id="btnNovo" class="button button-green btn-primary" type="button" onclick="AbrirBox(0)"><span class="ico-file-4 icon-white"></span></button>
                                    <button id="btnPrint" class="button button-blue btn-primary" type="button" onClick="imprimir()"><span class="ico-print icon-white"></span></button>
                                    <span style="float:right">
                                        Pesquisar: <input name="txtBuscar" id="txtBuscar" type="text" value="" style="width:200px; height:31px"/>
                                        <button class="button button-turquoise btn-primary" type="button" onclick="refresh()" id="btnPesquisar"><span class="ico-search icon-white"></span></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="block">
                                <?php
                                if ($imprimir == 1) {
                                    $titulo_pagina = "RELATÓRIO DE CONSULTA<br/>Emitentes";
                                    include "../Financeiro/Cabecalho-Impressao.php";
                                } else { ?>
                                    <div class="boxhead">
                                        <div class="boxtext">Emitentes</div>
                                    </div>
                                    <div class="boxtable">
                                        <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tblEmitentes">
                                            <thead>
                                                <tr>
                                                    <th width="3%">Cod:</th>
                                                    <th width="32%">Razão Social:</th>
                                                    <th width="15%">CNPJ:</th>
                                                    <th width="15%">Ins.Estadual:</th>
                                                    <th width="15%">Cidade:</th>
                                                    <th width="5%"><center>UF:</center></th>
                                            <th width="10%">Telefone:</th>
                                            <?php if ($imprimir == 0) { ?>
                                                <th width="5%"><center>Ação:</center></th>
                                            <?php } ?>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                        <div style="clear:both"></div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="widgets"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>        
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            
            $(document).ready(function () {
                var columns = [{"bSortable": true},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false}];
                $('#tblEmitentes').dataTable({
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": finalFind(0),
                    "bFilter": false,
                    "aoColumns": columns,
                    'oLanguage': {
                        'oPaginate': {
                            'sFirst': "Primeiro",
                            'sLast': "Último",
                            'sNext': "Próximo",
                            'sPrevious': "Anterior"
                        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                        'sLengthMenu': "Visualização de _MENU_ registros",
                        'sLoadingRecords': "Carregando...",
                        'sProcessing': "Processando...",
                        'sSearch': "Pesquisar:",
                        'sZeroRecords': "Não foi encontrado nenhum resultado"},
                    "sPaginationType": "full_numbers"
                });
            });

            function AbrirBox(id) {
                abrirTelaBox("FormEmitentes.php" + (id > 0 ? "?id=" + id : ""), 525, 720);
            }

            function refresh() {
                var oTable = $('#tblEmitentes').dataTable();
                oTable.fnReloadAjax(finalFind(0));
            }

            function finalFind(imp) {
                var retorno = "?imp=" + imp;
                if ($('#txtBuscar').val() !== "") {
                    retorno = retorno + "&Search=" + $("#txtBuscar").val();
                }
                return "./../NFE/Emitentes_server_processing.php" + retorno;
            }

            function FecharBox() {
                $("#newbox").remove();
                refresh();
            }

            function imprimir() {
                var pRel = "&NomeArq=" + "Emitentes NFE" +
                        "&lbl=" + "Cod|Razão Social|CNPJ|Ins. Estadual|Cidade|UF|Telefone" +
                        "&siz=" + "30|210|100|100|100|30|130" +
                        "&pdf=" + "7" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + "Emitentes NFE " + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + finalFind(1).replace("?", "&").replace("./../", "../Modulos/"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }

        </script>
        <?php odbc_close($con); ?>
    </body>
</html>
