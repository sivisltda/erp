<?php

include "./../../Connections/configini.php";
$aColumns = array('cod_nfe', 'serie', 'nnf', 'demis', 'razao_social', 'tpNF', 'tpEmis', 'status_nfe');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = " and mod > 0";
$sWhere = "";
$sOrder = " ORDER BY cod_nfe asc ";
$sLimit = 20;

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0 && intval($_GET['iSortCol_' . $i]) < 9) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . "
                                    " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY cod_nfe asc";
    }
}

if ($_GET['sSearch'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['sSearch']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
        $sWhere .= $aColumns[$i] . " AND LIKE '%" . utf8_decode($_GET['sSearch_' . $i]) . "%' ";
    }
}

if (isset($_GET['txtStatus']) && $_GET['txtStatus'] !== "") {
    $sWhere = $sWhere . " and status_nfe = '" . utf8_decode($_GET['txtStatus']) . "'";
}

if (isset($_GET['txtModelo']) && $_GET['txtModelo'] !== "") {
    $sWhere = $sWhere . " and mod = " . $_GET['txtModelo'];
}


$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,*,(select SUM(tot_val_bruto) from sf_nfe_notas_itens where cod_nota = cod_nfe) total 
            FROM sf_nfe_notas left join sf_fornecedores_despesas f on sf_nfe_notas.codigo_destinatario = f.id_fornecedores_despesas WHERE cod_nfe > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . "" . $sOrder;
$cur = odbc_exec($con, $sQuery1);
//echo $sQuery1;

$sQuery = "SELECT COUNT(*) total FROM sf_nfe_notas left join sf_fornecedores_despesas f on sf_nfe_notas.codigo_destinatario = f.id_fornecedores_despesas WHERE cod_nfe > 0 $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total FROM sf_nfe_notas left join sf_fornecedores_despesas f on sf_nfe_notas.codigo_destinatario = f.id_fornecedores_despesas WHERE cod_nfe > 0 $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<center><div id='formPQ' title='" . str_pad(utf8_encode($aRow[$aColumns[1]]), 3, "0", STR_PAD_LEFT) . "'>" . str_pad(utf8_encode($aRow[$aColumns[1]]), 3, "0", STR_PAD_LEFT) . "</div></center>";
    $row[] = "<center><a href='FormEmitir-Nota.php?id=" . utf8_encode($aRow[$aColumns[0]]) . "'><div id='formPQ' title='" . str_pad(utf8_encode($aRow[$aColumns[2]]), 9, "0", STR_PAD_LEFT) . "'>" . str_pad(utf8_encode($aRow[$aColumns[2]]), 9, "0", STR_PAD_LEFT) . "</div></a></center>";
    $row[] = "<center><div id='formPQ' title='" . escreverData($aRow[$aColumns[3]]) . "'>" . escreverData($aRow[$aColumns[3]]) . "</div></center>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[4]]) . "'>" . utf8_encode($aRow[$aColumns[4]]) . "</div>";
    if (utf8_encode($aRow[$aColumns[5]]) == 0) {
        $row[] = "<center><div id='formPQ' title='0 - Entrada'>0 - Entrada</div></center>";
    } else {
        $row[] = "<center><div id='formPQ' title=''></div></center>";
    }
    $row[] = "<center><div id='formPQ' style=\"text-align: right\" title='" . escreverNumero($aRow['total']) . "'>" . escreverNumero($aRow['total']) . "</div></center>";
    $row[] = "<center><div>" . $aRow['mod'] . "</div></center>";
    $row[] = utf8_encode($aRow[$aColumns[7]]);
    if ($ckb_adm_cli_read_ == 0 && (utf8_encode($aRow[$aColumns[7]] !== "Autorizada") && utf8_encode($aRow[$aColumns[7]] !== "Cancelada"))) {
        $row[] = "<center><a title='Excluir' onClick=\"return confirm('Deseja deletar esse registro?')\" href='Emitir-Nota.php?Del=" . $aRow[$aColumns[0]] . "'><input name='' type='image' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
    } else {
        $row[] = "";
    }
    $output['aaData'][] = $row;
}
echo json_encode($output);
odbc_close($con);