<?php
include "./../../Connections/configini.php";
$disabled = 'disabled';
$active1 = 'active';
$linhaContato = "1";

if (isset($_POST['bntSave'])) {
    $notIn = "";
    if ($_POST['txtId'] != '') {
        $query = "update sf_fornecedores_despesas set " .
                "razao_social = '" . utf8_decode($_POST['txtRazao']) . "'," .
                "cnpj = '" . utf8_decode($_POST['txtCnpj']) . "'," .
                "inscricao_estadual = '" . utf8_decode($_POST['txtInsc']) . "'," .
                "endereco = '" . utf8_decode($_POST['txtEndereco']) . "'," .
                "numero = '" . utf8_decode($_POST['txtNumero']) . "'," .
                "complemento = '" . utf8_decode($_POST['txtComplemento']) . "'," .
                "bairro = '" . utf8_decode($_POST['txtBairro']) . "'," .
                "estado = " . utf8_decode($_POST['txtEstado']) . "," .
                "cidade = " . utf8_decode($_POST['txtCidade']) . "," .
                "cep = '" . utf8_decode($_POST['txtCep']) . "'," .
                "contato = '" . utf8_decode($_POST['txtContato']) . "'," .
                "nome_fantasia = '" . utf8_decode($_POST['txtFantasia']) . "'," .
                "juridico_tipo = '" . utf8_decode($_POST['txtTipo']) . "'," .
                "inscricao_municipal = '" . utf8_decode($_POST['txtInsc_Mun']) . "'," .
                "isento_icms = '" . utf8_decode($_POST['txtIcms']) . "'," .
                "rntc = '" . utf8_decode($_POST['txtRntc']) . "'," .
                "antt = '" . utf8_decode($_POST['txtAntt']) . "'" .
                " where id_fornecedores_despesas = " . $_POST['txtId'];
        //echo $query;
        odbc_exec($con, $query) or die(odbc_errormsg());
        $notIn = '0';
        if (isset($_POST['txtQtdContatos'])) {
            $max = $_POST['txtQtdContatos'];
            for ($i = 0; $i <= $max; $i++) {
                if (isset($_POST['txtIdContato_' . $i]) && is_numeric($_POST['txtIdContato_' . $i])) {
                    if ($_POST['txtIdContato_' . $i] != 0) {
                        $notIn = $notIn . "," . $_POST['txtIdContato_' . $i];
                    }
                }
            }
            if ($notIn != "") {
                odbc_exec($con, "delete from sf_fornecedores_despesas_contatos where fornecedores_despesas = " . $_POST['txtId'] . " and id_contatos not in (" . $notIn . ")");
            }
        }
        $id = $_POST['txtId'];
    } else {
        $toSave = true;
        if ($_POST['txtCnpj'] != '') {
            $cur = odbc_exec($con, "select * from sf_fornecedores_despesas where tipo = 'T' and cnpj = '" . str_replace("'", "", $_POST['txtCnpj']) . "'");
            while ($RFP = odbc_fetch_array($cur)) {
                $toSave = false;
            }
        }
        if ($toSave == false) {
            echo "<script>alert('Registro de CPF/CNPJ já cadastrado no sistema!');</script>";
        } else {
            $query = "insert into sf_fornecedores_despesas(razao_social,cnpj,inscricao_estadual,endereco,numero,complemento,bairro,estado,cidade,cep,contato,nome_fantasia,juridico_tipo, inscricao_municipal, isento_icms, rntc,antt,tipo,empresa,dt_cadastro)values('" .
                    utf8_decode($_POST['txtRazao']) . "','" .
                    utf8_decode($_POST['txtCnpj']) . "','" .
                    utf8_decode($_POST['txtInsc']) . "','" .
                    utf8_decode($_POST['txtEndereco']) . "','" .
                    utf8_decode($_POST['txtNumero']) . "','" .
                    utf8_decode($_POST['txtComplemento']) . "','" .
                    utf8_decode($_POST['txtBairro']) . "'," .
                    utf8_decode($_POST['txtEstado']) . "," .
                    utf8_decode($_POST['txtCidade']) . ",'" .
                    utf8_decode($_POST['txtCep']) . "','" .
                    utf8_decode($_POST['txtContato']) . "','" .
                    utf8_decode($_POST['txtFantasia']) . "','" .
                    utf8_decode($_POST['txtTipo']) . "','" .
                    utf8_decode($_POST['txtInsc_Mun']) . "','" .
                    utf8_decode($_POST['txtIcms']) . "','" .
                    utf8_decode($_POST['txtRntc']) . "','" .
                    utf8_decode($_POST['txtAntt']) . "'," .
                    "'T','001',getDate())";
            //echo $query;
            odbc_exec($con, $query) or die(odbc_errormsg());
            $res = odbc_exec($con, "select top 1 id_fornecedores_despesas from sf_fornecedores_despesas order by id_fornecedores_despesas desc") or die(odbc_errormsg());
            $id = odbc_result($res, 1);
        }
    }

    if ($id !== "") {
        if (isset($_POST['txtQtdContatos'])) {
            $max = $_POST['txtQtdContatos'];
            for ($i = 1; $i < $max; $i++) {
                if (isset($_POST['txtIdContato_' . $i])) {
                    if ($_POST['txtIdContato_' . $i] !== "0") {
                        odbc_exec($con, "update sf_fornecedores_despesas_contatos set tipo_contato = " . $_POST['txtTpContato_' . $i] . ",conteudo_contato = " . valoresTexto('txtTxContato_' . $i) . " where id_contatos = " . $_POST['txtIdContato_' . $i]) or die(odbc_errormsg());
                    } else {
                        odbc_exec($con, "insert into sf_fornecedores_despesas_contatos(fornecedores_despesas,tipo_contato,conteudo_contato) values(" . $id . "," . $_POST['txtTpContato_' . $i] . "," . valoresTexto('txtTxContato_' . $i) . ")") or die(odbc_errormsg());
                    }
                }
            }
        }
        $_POST['txtId'] = $id;
        echo "<script> location.replace('FormTransportadoras.php?id=" . $id . "'); </script>";
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = " . $_POST['txtId']);
        odbc_exec($con, "DELETE FROM sf_fornecedores_despesas WHERE id_fornecedores_despesas = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso');window.top.location.href = 'Transportadoras.php';</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_fornecedores_despesas where id_fornecedores_despesas = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_fornecedores_despesas']);
        $razao = utf8_encode($RFP['razao_social']);
        $cnpj = utf8_encode($RFP['cnpj']);
        $insc = utf8_encode($RFP['inscricao_estadual']);
        $endereco = utf8_encode($RFP['endereco']);
        $numero = utf8_encode($RFP['numero']);
        $complemento = utf8_encode($RFP['complemento']);
        $bairro = utf8_encode($RFP['bairro']);
        $estado = utf8_encode($RFP['estado']);
        $cidade = utf8_encode($RFP['cidade']);
        $cep = utf8_encode($RFP['cep']);
        $contato = utf8_encode($RFP['contato']);
        $fantasia = utf8_encode($RFP['nome_fantasia']);
        $tipo = utf8_encode($RFP['juridico_tipo']);
        $insc_munic = utf8_encode($RFP['inscricao_municipal']);
        $isentoICMS = utf8_encode($RFP['isento_icms']);
        $rntc = utf8_encode($RFP['rntc']);
        $antt = utf8_encode($RFP['antt']);
    }
} else {
    $disabled = '';
    $id = '';
    $razao = '';
    $cnpj = '';
    $insc = '';
    $endereco = '';
    $numero = '';
    $complemento = '';
    $bairro = '';
    $estado = '';
    $cidade = '';
    $cep = '';
    $contato = '';
    $fantasia = '';
    $tipo = 'J';
    $insc_munic = '';
    $isentoICMS = 'S';
    $rntc = '';
    $antt = '';
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Transportadoras</title>
        <link rel="icon" type="image/ico" href="favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet"/>
        <link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css"/>
        <script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
        <script src="../../../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    </head>
    <body>
        <div class="row-fluid" style="width:740px">
            <div class="block title">
            </div>
            <div>
                <div class="block">
                    <form action="FormTransportadoras.php" name="frmEnviaDados" method="POST">
                        <div class="frmhead">
                            <div class="frmtext">Transportadoras</div>
                            <div class="frmicon" onClick="parent.FecharBox()">
                                <span class="ico-remove"></span>
                            </div>
                        </div>                        
                        <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                        <input name="txtQtdContatos" id="txtQtdContatos" value="<?php echo $linhaContato; ?>" type="hidden"/>
                        <div class="tabbable frmtabs">
                            <ul class="nav nav-tabs">
                                <li class="<?php echo $active1; ?>" style="margin-left:10px"><a href="#tab1" data-toggle="tab">Dados principais</a></li>
                                <li><a href="#tab2" data-toggle="tab">Contatos</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane <?php echo $active1; ?>" id="tab1">
                                    <div style="height: 370px;">
                                        <table id="tblTabela" width="98%" border="0" cellpadding="3" class="textosComuns">
                                            <tr>                                    
                                                <td width="100" align="right">Tipo:</td>
                                                <td><select name="txtTipo" id="txtTipo" class="select" style="width:150px" <?php echo $disabled; ?>>
                                                        <option value="J" <?php
                                                        if ($tipo == "J") {
                                                            echo "SELECTED";
                                                        }
                                                        ?>>Jurídica</option>
                                                        <option value="F" <?php
                                                        if ($tipo == "F") {
                                                            echo "SELECTED";
                                                        }
                                                        ?>>Física</option>
                                                    </select></td>
                                                <td align="right"></td>
                                                <td width="230"></td>                                    
                                            </tr>                                
                                            <tr>
                                                <td width="122" align="right"><div id="lblNome"><?php
                                                        if ($tipo == "J") {
                                                            echo "Razão Social:";
                                                        } else {
                                                            echo "Nome:";
                                                        }
                                                        ?></div>
                                                </td>
                                                <td colspan="3"><input type="text" name="txtRazao" id="txtRazao" class="input-medium" value="<?php echo $razao; ?>" <?php echo $disabled; ?>/></td>
                                            </tr>
                                            <tr id="trFantasia"<?php if ($tipo == "F") { ?> style="display: none" <?php } ?>>
                                                <td id="tdFantasia" width="122" align="right">Nome Fantasia:</td>
                                                <td colspan="3"><input type="text" name="txtFantasia" id="txtFantasia" class="input-medium" value="<?php echo $fantasia; ?>" <?php echo $disabled; ?>/></td>
                                            </tr>
                                            <tr>
                                                <td align="right"><div id="lblDocumento"><?php
                                                        if ($tipo == "J") {
                                                            echo "CNPJ:";
                                                        } else {
                                                            echo "CPF:";
                                                        }
                                                        ?></div></td>
                                                <td width="230"><input type="text" name="txtCnpj" id="txtCnpj" maxlength="18" class="input-medium" value="<?php echo $cnpj; ?>" <?php echo $disabled; ?>/></td>
                                                <td width="100" align="right"><div id="lblDocumento2"><?php
                                                        if ($tipo == "J") {
                                                            echo "Inscrição Estadual:";
                                                        } else {
                                                            echo "RG:";
                                                        }
                                                        ?></div></td>
                                                <td><input type="text" name="txtInsc" id="txtInsc" class="input-medium" value="<?php echo $insc; ?>" <?php echo $disabled; ?>/></td>
                                            </tr>
                                            <tr id="trImpostos"<?php if ($tipo == "F") { ?> style="display: none" <?php } ?>>
                                                <td align="right">Inscrição Municipal:</td>
                                                <td width="230"><input type="text" name="txtInsc_Mun" id="txtInsc_Mun" class="input-medium" value="<?php echo $insc_munic; ?>" <?php echo $disabled; ?>/></td>
                                                <td width="100" align="right">Isento ICMS:</td>
                                                <td><select name="txtIcms" id="txtIcms" class="select" style="width:100px" <?php echo $disabled; ?>>
                                                        <option value="S" <?php
                                                        if ($isentoICMS == "S") {
                                                            echo "SELECTED";
                                                        }
                                                        ?>>Sim</option>
                                                        <option value="N" <?php
                                                        if ($isentoICMS == "N") {
                                                            echo "SELECTED";
                                                        }
                                                        ?>>Não</option>
                                                    </select></td>
                                            </tr>
                                            <tr id="trRegistros"<?php if ($tipo == "F") { ?> style="display: none" <?php } ?>>
                                                <td align="right">Registro ANTT:</td>
                                                <td width="230"><input type="text" name="txtAntt" id="txtAntt" class="input-medium" value="<?php echo $antt; ?>" <?php echo $disabled; ?>/></td>
                                                <td width="100" align="right">RNTC:</td>
                                                <td><input type="text" name="txtRntc" id="txtRntc" class="input-medium" value="<?php echo $rntc; ?>" <?php echo $disabled; ?>/></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Endereço:</td>
                                                <td colspan="3"><input type="text" name="txtEndereco" id="txtEndereco" class="input-medium" value="<?php echo $endereco; ?>" <?php echo $disabled; ?>/></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Numero:</td>
                                                <td><input type="text" name="txtNumero" id="txtNumero" class="input-medium" style="width:100px" maxlength="6" value="<?php echo $numero; ?>" <?php echo $disabled; ?>/></td>
                                                <td align="right">Complemento:</td>
                                                <td><input type="text" name="txtComplemento" id="txtComplemento" class="input-medium" maxlength="20" value="<?php echo $complemento; ?>" <?php echo $disabled; ?>/></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Bairro:</td>
                                                <td><input type="text" name="txtBairro" id="txtBairro" class="input-medium" maxlength="20" <?php echo $disabled; ?> value="<?php echo $bairro; ?>"/></td>
                                                <td align="right">Estado:</td>
                                                <td><select name="txtEstado" id="txtEstado" class="input-medium" style="width:100%" <?php echo $disabled; ?>>
                                                        <option value="null" >Selecione o estado</option>
                                                        <?php
                                                        $cur = odbc_exec($con, "select estado_codigo,estado_nome from tb_estados where estado_codigo > 0 order by estado_nome") or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) {
                                                            ?>
                                                            <option value="<?php echo $RFP['estado_codigo'] ?>"<?php
                                                            if (!(strcmp($RFP['estado_codigo'], $estado))) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>><?php echo utf8_encode($RFP['estado_nome']) ?></option>
                                                                <?php } ?>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Cidade:</td>
                                                <td><span id="carregando" name="carregando" style="color:#666;display:none">Aguarde, carregando...</span>
                                                    <select name="txtCidade" id="txtCidade" class="input-medium" style="width:100%" <?php echo $disabled; ?>>
                                                        <option value="null" >Selecione a cidade</option>
                                                        <?php
                                                        if ($estado != '') {
                                                            $sql = "select cidade_codigo,cidade_nome from tb_cidades where cidade_codigo > 0 AND cidade_codigoEstado = " . $estado . " ORDER BY cidade_nome";
                                                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) {
                                                                ?>
                                                                <option value="<?php echo $RFP['cidade_codigo'] ?>"<?php
                                                                if (!(strcmp($RFP['cidade_codigo'], $cidade))) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>><?php echo utf8_encode($RFP['cidade_nome']) ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                    </select>                                               
                                                </td>
                                                <td align="right">CEP:</td>
                                                <td><input type="text" name="txtCep" id="txtCep" class="input-medium" maxlength="9" value="<?php echo $cep; ?>" <?php echo $disabled; ?>/></td>
                                            </tr>
                                            <tr>
                                                <td align="right"><div id="lblContato"><?php
                                                        if ($tipo == "J") {
                                                            echo "Contato:";
                                                        } else {
                                                            echo "Apelido:";
                                                        }
                                                        ?></div></td>
                                                <td><input type="text" name="txtContato" id="txtContato" class="input-medium" maxlength="128" value="<?php echo $contato; ?>" <?php echo $disabled; ?>/></td>
                                                <td align="right"> </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab2">                                                                                                                                                   
                                    <div class="toolbar dark" style="background-color: #DCDCDC">
                                        <div class="input-prepend input-append">
                                        </div>
                                        <table width="99%" border="0" cellpadding="3" class="textosComuns">
                                            <tr>
                                                <td>                                                                                                                                                                                                
                                                    <select id="txtTipoContato" name="txtTipoContato" onChange="alterarMask()" <?php echo $disabled; ?>>
                                                        <option value="0">Telefone</option>
                                                        <option value="1">Celular</option>
                                                        <option value="2">E-mail</option>                                                        
                                                        <option value="3">Site</option>                                                        
                                                        <option value="4">Outros</option>                                                        
                                                    </select>
                                                </td>
                                                <td id="sprytextfield10">
                                                    <input id="txtTextoContato" type="text" <?php echo $disabled; ?>/>
                                                </td>
                                                <td width="80" align="right">                                                                    
                                                    <button type="button" onclick="addContato();" <?php echo $disabled; ?> class="btn dblue">Incluir <span class="icon-arrow-next icon-white"></span></button>
                                                </td>                                                         
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="data dark npr npb" style="overflow:hidden">                                
                                        <div id="divContatos" class="messages" style="height:326px; overflow-y:scroll"></div>                                
                                    </div>                                                      
                                </div>
                            </div>
                        </div>
                        <div class="frmfoot">
                            <div class="frmbtn">
                                <?php if ($disabled == '') { ?>
                                    <button class="btn green" type="submit" title="Gravar" name="bntSave"><span class="ico-checkmark"></span> Gravar</button>
                                    <?php if ($_POST['txtId'] == '') { ?>
                                        <button class="btn yellow" type="button" title="Cancelar" onClick="parent.FecharBox();"><span class="ico-reply"></span> Cancelar</button>
                                    <?php } else { ?>
                                        <button class="btn yellow" type="submit" title="Cancelar" name="bntAlterar" ><span class="ico-reply"></span> Cancelar</button>
                                        <?php
                                    }
                                } else { ?>
                                    <button class="btn green" type="submit" title="Novo" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"> </span>  Novo</button>
                                    <button class="btn green" type="submit" title="Alterar" name="bntEdit" value="Alterar"> <span class="ico-edit"> </span>  Alterar</button>
                                    <button class="btn red" type="submit" title="Excluir" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span>  Excluir</button>
                                <?php } ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>    
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>        
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>                
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>            
        <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>            
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/tagsinput/jquery.tagsinput.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/maskedinput/jquery.maskedinput-1.3.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/multiselect/jquery.multi-select.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>         
        <script type="text/javascript">
            var idLinhaContato =<?php echo $linhaContato; ?>;
            $(function () {
                $('#txtTipo').change(function () {
                    if ($(this).val() === "F") {
                        $('#lblNome').html('Nome:');
                        $('#lblDocumento').html('CPF:');
                        $('#lblDocumento2').html('RG:');
                        $('#trFantasia').hide();
                        $('#trImpostos').hide();
                        $('#trRegistros').hide();
                        $('#lblContato').html('Apelido:');
                        $("#txtCnpj").mask("999.999.999-99");
                        $("#txtInsc").unmask();
                    } else {
                        $('#lblNome').html('Razão Social:');
                        $('#lblDocumento').html('CNPJ:');
                        $('#lblDocumento2').html('Inscrição Estadual:');
                        $('#trFantasia').show();
                        $('#trImpostos').show();
                        $('#trRegistros').show();
                        $('#lblContato').html('Contato:');
                        $("#txtCnpj").mask("99.999.999/9999-99");
                        $("#txtInsc").mask("99.999.99-9");
                    }
                });
                $('#txtEstado').change(function () {
                    if ($(this).val()) {
                        $('#txtCidade').hide();
                        $('#carregando').show();
                        $.getJSON('locais.ajax.php?search=', {txtEstado: $(this).val(), ajax: 'true'}, function (j) {
                            var options = '<option value=""></option>';
                            for (var i = 0; i < j.length; i++) {
                                options += '<option value="' + j[i].cidade_codigo + '">' + j[i].cidade_nome + '</option>';
                            }
                            $('#txtCidade').html(options).show();
                            $('#carregando').hide();
                        });
                    } else {
                        $('#txtCidade').html('<option value="">Selecione a cidade</option>');
                    }
                });                
            });
            function alterarMask() {
                $('#txtTextoContato').val("");
                var tipo = $("#txtTipoContato option:selected").val();
                if (sprytextfield10 !== null) {
                    sprytextfield10.reset();
                    sprytextfield10.destroy();
                    sprytextfield10 = null;
                }
                if (tipo === "0") {
                    sprytextfield10 = new Spry.Widget.ValidationTextField("sprytextfield10", "custom", {pattern: "(00) 0000-0000", useCharacterMasking: true, isRequired: false});
                } else if (tipo === "1") {
                    sprytextfield10 = new Spry.Widget.ValidationTextField("sprytextfield10", "custom", {pattern: "(00) 00000-0000", useCharacterMasking: true, isRequired: false});
                }
            }
            function addContato() {
                var msg = $("#txtTextoContato").val();
                var sel = $("#txtTipoContato").val();
                if (msg !== "") {
                    var arb = msg + "@";
                    if (sel !== 2 || (sel === 2 && arb.match(/@/g).length === 2)) {
                        addLinhaContato($("#txtTipoContato").val(), $("#txtTipoContato option:selected").text(), $("#txtTextoContato").val(), "0");
                    } else {
                        bootbox.alert("Preencha um endereço de e-mail corretamente!");
                    }
                } else {
                    bootbox.alert("Preencha as informações de contato corretamente!");
                }
            }
            function addLinhaContato(idTipo, tipo, conteudo, id) {
                var btExcluir = "";
                <?php if ($disabled != "") { ?>
                    btExcluir = '<div style="width:5%; padding:4px 0 3px; float:right"></div>';
                <?php } else { ?>
                    btExcluir = '<div style="width:5%; padding:4px 0 3px; float:right" onClick="return removeLinha(\'tabela_linha_' + idLinhaContato + '\');"><span class="ico-remove" style="font-size:20px"></span></div>';
                <?php } ?>
                var linha = "";
                linha = '<div id="tabela_linha_' + idLinhaContato + '" style="padding:5px; border-bottom:1px solid #DDD">\n\
                            <input id="txtIdContato_' + idLinhaContato + '" name="txtIdContato_' + idLinhaContato + '" value="' + id + '" type="hidden"></input>\n\
                            <input id="txtTpContato_' + idLinhaContato + '" name="txtTpContato_' + idLinhaContato + '" value="' + idTipo + '" type="hidden"></input>\n\
                            <div style="width:32%; padding-left:1%; line-height:27px; float:left">' + tipo + '</div>\n\
                            <input id="txtTxContato_' + idLinhaContato + '" name="txtTxContato_' + idLinhaContato + '" value="' + conteudo + '" type="hidden"></input>\n\
                            ' + btExcluir + '<div style="width:60%; line-height:27px; float:left">' + conteudo + '</div> \n\
                        </div>';
                $("#divContatos").prepend(linha);
                idLinhaContato++;
                $("#txtQtdContatos").val(idLinhaContato);
            }
            function removeLinha(id) {
                $("#" + id).remove();
            }

            $(document).ready(function () {
                listContatos();
            });
            function listContatos() {
                $.getJSON('../Comercial/FormGerenciador-Clientes.contatos.ajax.php?search=', {cod: <?php
                if (is_numeric($id)) {
                    echo $id;
                } else {
                    echo "0";
                }
                ?>, ajax: 'true'}, function (j) {
                    for (var i = 0; i < j.length; i++) {
                        addLinhaContato(j[i].tipo_contato, j[i].texto_contatos, j[i].conteudo_contato, j[i].id);
                    }
                });
            }
        </script>        
        <script type="text/javascript">
            $("#frmEnviaDados").validate({
                errorPlacement: function (error, element) {
                    return true;
                },
                rules: {
                    txtRazao: {required: true}
                }
            });
            <?php if ($tipo == "J") { ?>
                $("#txtCnpj").mask("99.999.999/9999-99");
                $("#txtInsc").mask("99.999.99-9");
            <?php } else { ?>
                $("#txtCnpj").mask("999.999.999-99");
                $("#txtInsc").unmask();
            <?php } ?>
            $("#txtNumero").mask("9999999999", {placeholder: ""});
            $("#txtCep").mask("99999-999");
            var sprytextfield10 = new Spry.Widget.ValidationTextField("sprytextfield10", "custom", {pattern: "(00) 0000-0000", useCharacterMasking: true, isRequired: false});
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>