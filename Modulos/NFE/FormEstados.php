<?php
include "../../Connections/configini.php";

$disabled = "disabled";
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != "") {
        odbc_exec($con, "update tb_estados set " .
        "estado_nome = " . valoresTexto("txtNomeEst") . "," .
        "estado_sigla = " . valoresTexto("txtSiglaEst") . "," .
        "estado_gov = " . valoresTexto("txtCodNfe") . 
        " where estado_codigo = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into tb_estados(estado_nome,estado_sigla,estado_gov)values(" .
        valoresTexto("txtNomeEst") . "," .
        valoresTexto("txtSiglaEst") . "," .
        valoresTexto("txtCodNfe") . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 estado_codigo from tb_estados order by estado_codigo desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}

if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != "") {
        odbc_exec($con, "DELETE FROM tb_estados WHERE estado_codigo = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = "";
    $_POST['txtId'] = "";
}

if ($_GET['id'] != "" || $_POST['txtId'] != "") {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != "") {
        $disabled = "disabled";
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from tb_estados where estado_codigo = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['estado_codigo'];
        $nome_est = utf8_encode($RFP['estado_nome']);
        $sigla_est = utf8_encode($RFP['estado_sigla']);
        $cod_nfe = utf8_encode($RFP['estado_gov']);
    }
} else {
    $disabled = "";
    $id = "";
    $nome_est = "";
    $sigla_est = "";
    $cod_nfe = "";
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != "") {
        $disabled = "";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>        
    </head>
    <body>
        <form action="FormEstados.php" name="frmEnviaDados" id="frmEnviaDados" method="POST">
            <div class="frmhead">
                <div class="frmtext">Estado</div>
                <div class="frmicon" onClick="parent.FecharBox();">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input name="txtId" id="txtId" type="hidden" value="<?php echo $id; ?>"/>
                <div>
                    <span>Nome do Estado:</span>
                    <input type="text" name="txtNomeEst" id="txtNomeEst" class="input-medium" value="<?php echo $nome_est; ?>" <?php echo $disabled; ?>/>
                </div>                
                <div style="float: left;width: 50%;">
                    <span>Sigla do Estado:</span>
                    <input type="text" name="txtSiglaEst" id="txtSiglaEst" class="input-medium" maxlength="2" value="<?php echo $sigla_est; ?>" <?php echo $disabled; ?>/>
                </div>                
                <div style="float: left;width: 49%; margin-left: 1%">
                    <span>Código NFE:</span>
                    <input type="text" name="txtCodNfe" id="txtCodNfe" class="input-medium" maxlength="10" value="<?php echo $cod_nfe; ?>" <?php echo $disabled; ?>/>
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == "") { ?>
                        <button class="btn green" type="submit" name="bntSave"><span class="ico-checkmark"></span> Gravar</button>
                        <button class="btn yellow" title="Cancelar" <?php echo ($_POST["txtId"] == "" ? "onClick=\"parent.FecharBox()\"" : "type=\"submit\" name=\"bntAlterar\""); ?>><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn green" type="submit" name="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" type="submit" name="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                        <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
        </form>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>                
        <?php odbc_close($con); ?>
    </body>
</html>