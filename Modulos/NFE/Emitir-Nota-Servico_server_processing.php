<?php

require_once(__DIR__ . '/../../Connections/configini.php');

$aColumns = array('cod_nfe', 'serie', 'nnf', 'demis', 'razao_social', 'tpNF', 'tpEmis', 'status_nfe');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = " and mod = 0";
$sWhere = "";
$sOrder = " ORDER BY cod_nfe " . ($_GET['txtStatus'] == "Em Digitação" ? "asc" : "desc");
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0 && intval($_GET['iSortCol_' . $i]) < 9) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . "
                                    " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY cod_nfe asc";
    }
}

if ($_GET['sSearch'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['sSearch']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
        $sWhere .= $aColumns[$i] . " AND LIKE '%" . utf8_decode($_GET['sSearch_' . $i]) . "%' ";
    }
}

if (isset($_GET['txtStatus']) && $_GET['txtStatus'] !== "") {
    $sWhereX = $sWhereX . " and status_nfe = '" . utf8_decode($_GET['txtStatus']) . "'";
}

if (isset($_GET['txtModelo']) && $_GET['txtModelo'] !== "") {
    $sWhereX = $sWhereX . " and mod = " . $_GET['txtModelo'];
}

if ($_GET['dti'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
    $sWhere .= " AND demis >= " . valoresDataHora2($DateBegin, "00:00:00");
}

if ($_GET['dtf'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
    $sWhere .= " AND demis <= " . valoresDataHora2($DateEnd, "23:59:59");
}

odbc_exec($con, "set dateformat dmy;");
$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,*,(select SUM(tot_val_bruto) from sf_nfe_notas_itens where cod_nota = cod_nfe) total,
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato in (0,1) and len(conteudo_contato) > 8 and fornecedores_despesas = f.id_fornecedores_despesas) telefone
FROM sf_nfe_notas left join sf_fornecedores_despesas f on sf_nfe_notas.codigo_destinatario = f.id_fornecedores_despesas WHERE cod_nfe > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder;
$cur = odbc_exec($con, $sQuery1);
//echo $sQuery1;

$sQuery = "SELECT COUNT(*) total FROM sf_nfe_notas left join sf_fornecedores_despesas f on sf_nfe_notas.codigo_destinatario = f.id_fornecedores_despesas WHERE cod_nfe > 0 $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $cor = "";
    if (($aRow[$aColumns[7]] !== "Autorizada") && ($aRow["cnpj"] == "" || $aRow["endereco"] == "" || $aRow["numero"] == "" || $aRow["bairro"] == "" || $aRow["estado"] == "" || $aRow["cidade"] == "" || $aRow["cep"] == "" || $aRow["telefone"] == "")) {
        $cor = "color:red;";
    }
    $row[] = "<center><input type=\"checkbox\" class=\"caixa\" name=\"items[]\" value=\"" . $aRow[$aColumns[0]] . "\" " . (($aRow[$aColumns[7]] !== "Autorizada") ? "" : "disabled") . "/></center>";
    $row[] = "<center><div id='formPQ' style=\"" . $cor . "\" title='" . str_pad(utf8_encode($aRow[$aColumns[1]]), 3, "0", STR_PAD_LEFT) . "'>" . str_pad(utf8_encode($aRow[$aColumns[1]]), 3, "0", STR_PAD_LEFT) . "</div></center>";
    $row[] = "<center><a href='#' onClick=\"redirectFind(" . utf8_encode($aRow[$aColumns[0]]) . ");\"><div id='c_nfes_" . $aRow[$aColumns[0]] . "' title='" . str_pad(utf8_encode($aRow[$aColumns[2]]), 9, "0", STR_PAD_LEFT) . "'>" . str_pad(utf8_encode($aRow[$aColumns[2]]), 9, "0", STR_PAD_LEFT) . "</div></a></center>";
    $row[] = "<center><div id='formPQ' style=\"" . $cor . "\" title='" . escreverData($aRow[$aColumns[3]]) . "'>" . escreverData($aRow[$aColumns[3]]) . "</div></center>";
    $row[] = "<a href='./../Academia/ClientesForm.php?id=" . utf8_encode($aRow["id_fornecedores_despesas"]) . "'><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[4]]) . "'>" . utf8_encode($aRow[$aColumns[4]]) . "</div></a>";
    $row[] = "<center><div id='formPQ' style=\"" . $cor . "\" title='" . (utf8_encode($aRow[$aColumns[5]]) == 0 ? "0 - Entrada" : "") . "'>" . (utf8_encode($aRow[$aColumns[5]]) == 0 ? "0 - Entrada" : "") . "</div></center>";
    $row[] = "<center><div id='formPQ' style=\"text-align: right;" . $cor . "\" title='" . escreverNumero($aRow['total']) . "'>" . escreverNumero($aRow['total']) . "</div></center>";
    $row[] = "<center><div style=\"" . $cor . "\">" . $aRow['mod'] . "</div></center>";
    $row[] = "<div id='txtStatus_" . $aRow[$aColumns[0]] . "' style=\"" . $cor . "\">" . utf8_encode($aRow[$aColumns[7]]) . "</div>";
    if ($ckb_adm_cli_read_ == 0 && (utf8_encode($aRow[$aColumns[7]] !== "Autorizada") && utf8_encode($aRow[$aColumns[7]] !== "Cancelada"))) {
        $row[] = "<center><a title='Excluir' onClick=\"return confirm('Deseja deletar esse registro?')\" href='Emitir-Nota-Servico.php?Del=" . $aRow[$aColumns[0]] . "'><input name='' type='image' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
    } else {
        $row[] = "";
    }
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
