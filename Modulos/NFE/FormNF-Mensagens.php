<?php
include "../../Connections/configini.php";

$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_nfe_mensagens set " . 
        "nome_mensagem = " . valoresTexto('txtDescricao') . "," .
        "descricao_mensagem = " . valoresTexto('txtConteudo') . " where id_mensagem = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_nfe_mensagens(nome_mensagem,descricao_mensagem)values(" .
        valoresTexto('txtDescricao') . "," . valoresTexto('txtConteudo') . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_mensagem from sf_nfe_mensagens order by id_mensagem desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}

if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_nfe_mensagens WHERE id_mensagem = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso');window.top.location.href = 'NF-Mensagens.php';</script>";
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_nfe_mensagens where id_mensagem =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_mensagem'];
        $descricao = utf8_encode($RFP['nome_mensagem']);
        $conteudo = utf8_encode($RFP['descricao_mensagem']);
    }
} else {
    $disabled = '';
    $id = '';
    $descricao = '';
    $conteudo = '';
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    </head>
    <body>
        <form action="FormNF-Mensagens.php" name="frmEnviaDados" id="frmEnviaDados" method="POST">
            <div class="frmhead">
                <div class="frmtext">Mensagens</div>
                <div class="frmicon" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input name="txtId" id="txtId" type="hidden" value="<?php echo $id; ?>"/>
                <div>
                    <span>Descrição:</span>
                    <input name="txtDescricao" id="txtDescricao" type="text" style="width:100%" <?php echo $disabled; ?> value="<?php echo $descricao; ?>"/>
                </div>
                <div>
                    <span>Conteúdo:</span>
                    <textarea name="txtConteudo" id="txtConteudo" style="width: 358px;" <?php echo $disabled; ?> class="input-medium"><?php echo $conteudo; ?></textarea>
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == "") { ?>
                        <?php
                        if ($_POST["txtId"] == "") {
                            $btnType = "onClick=\"parent.FecharBox()\"";
                        } else {
                            $btnType = "type=\"submit\" name=\"bntAlterar\"";
                        }
                        ?>
                        <button class="btn green" type="submit" name="bntSave"><span class="ico-checkmark"></span> Gravar</button>
                        <button class="btn yellow" title="Cancelar" <?php echo $btnType; ?>><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn green" type="submit" name="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" type="submit" name="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                        <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
        </form>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>                 
        <?php odbc_close($con); ?>
    </body>
</html>