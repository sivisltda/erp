<?php

include "./../../Connections/configini.php";
$id_area = $_REQUEST['txtFonecedor'];
$local = array();

if ($_REQUEST["txtTabela"] == "entrega") {
    $sql = "select nome_end_entrega, end_entrega, numero_entrega, complemento_entrega, bairro_entrega, estado_entrega, estado_nome, cidade_entrega, cidade_nome, cep_entrega, telefone_entrega, referencia_entrega
                from dbo.sf_fornecedores_despesas_endereco_entrega f left join tb_estados e on f.estado_entrega = e.estado_codigo
                left join tb_cidades c on f.cidade_entrega = c.cidade_codigo where id_endereco = " . $id_area;
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('nome' => utf8_encode($RFP['nome_end_entrega']), 'endereco' => utf8_encode($RFP['end_entrega']), 'numero' => utf8_encode($RFP['numero_entrega']), 'complemento' => utf8_encode($RFP['complemento_entrega']), 'bairro' => utf8_encode($RFP['bairro_entrega']), 'estado' => utf8_encode($RFP['estado_entrega']), 'estado_nome' => utf8_encode($RFP['estado_nome']), 'cidade' => utf8_encode($RFP['cidade_entrega']), 'cidade_nome' => utf8_encode($RFP['cidade_nome']), 'cep' => utf8_encode($RFP['cep_entrega']), 'telefone' => utf8_encode($RFP['telefone_entrega']), 'referencia' => utf8_encode($RFP['referencia_entrega']));
    }
} else {
    $sql = "select endereco, numero, complemento, bairro, estado, estado_nome, cidade, cidade_nome, cep, cnpj, razao_social, nome_fantasia, inscricao_estadual, ie_tributario, inscricao_municipal, cnae_fiscal, regime_tributario,
                (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato,
                (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = id_fornecedores_despesas) email_contato
                from dbo.sf_fornecedores_despesas f left join tb_estados e on f.estado = e.estado_codigo
                left join tb_cidades c on f.cidade = c.cidade_codigo where id_fornecedores_despesas = " . $id_area;
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('endereco' => utf8_encode($RFP['endereco']), 'numero' => utf8_encode($RFP['numero']), 'complemento' => utf8_encode($RFP['complemento']), 'bairro' => utf8_encode($RFP['bairro']), 'estado' => utf8_encode($RFP['estado']), 'estado_nome' => utf8_encode($RFP['estado_nome']), 'cidade' => utf8_encode($RFP['cidade']), 'cidade_nome' => utf8_encode($RFP['cidade_nome']), 'cep' => utf8_encode($RFP['cep']), 'cnpj' => utf8_encode($RFP['cnpj']), 'razao' => utf8_encode($RFP['razao_social']), 'fantasia' => utf8_encode($RFP['nome_fantasia']), 'estadual' => utf8_encode($RFP['inscricao_estadual']), 'ietributo' => utf8_encode($RFP['ie_tributario']), 'municipal' => utf8_encode($RFP['inscricao_municipal']), 'cnae' => utf8_encode($RFP['cnae_fiscal']), 'regime' => utf8_encode($RFP['regime_tributario']),
            'email' => utf8_encode($RFP['email_contato']), 'fone' => utf8_encode($RFP['telefone_contato']));
    }
}

echo( json_encode($local) );
odbc_close($con);
