<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include "./../../Connections/configini.php";

$id_area = $_REQUEST['txtEnd'];
$local = array();
$sql = "select id_endereco, nome_end_entrega
from dbo.sf_fornecedores_despesas_endereco_entrega where fornecedores_despesas = " . $id_area;
$cur = odbc_exec($con, $sql) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $local[] = array('id_endereco' => $RFP['id_endereco'], 'nome_end_entrega' => utf8_encode($RFP['nome_end_entrega']));
}

echo(json_encode($local));
odbc_close($con);
