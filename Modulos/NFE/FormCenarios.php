<?php
include './../../Connections/configini.php';
$disabled = 'disabled';
$finalUrl = "";
$idProduto = "";

if (isset($_GET['TpTela']) && $_GET['TpTela'] == "NFE") {
    include "form/CenariosFormNFE.php";
} else {
    include "form/CenariosForm.php";
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <form action="FormCenarios.php<?php echo $finalUrl; ?>" name="frmEnviaDados" id="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Cenários</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <input type="hidden" name="txtIdProduto" id="txtIdProduto" value="<?php echo $idProduto; ?>"/>
        <input type="hidden" name="txtTpCenario" id="txtTpCenario" value="<?php echo $tpCenario; ?>"/>
        <div class="tabbable frmtabs">
            <ul class="nav nav-tabs" style="margin:0">                
                <li class="active"><a href="#tab1" data-toggle="tab"><?php echo ($tpTela == "" ? "Dados Principais" : "Outros Valores") ?></a></li>
                <li><a href="#tab2" data-toggle="tab">ICMS</a></li>
                <li><a href="#tab3" data-toggle="tab">IPI</a></li>
                <li><a href="#tab4" data-toggle="tab">PIS</a></li>
                <li><a href="#tab5" data-toggle="tab">COFINS</a></li>
            </ul>
            <div class="tab-content frmcont" style="height:338px; font-size:11px !important">
                <?php if ($tpTela == "") { ?>
                    <div class="tab-pane active" id="tab1">
                        <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                        <div style="width:100%; float:left">
                            <span>Nome do Cenário:</span>
                            <input name="txtNome" id="txtNome" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $nome; ?>"/>
                        </div>
                        <div style="clear:both; height:5px"></div>
                        <div style="width:100%; float:left">
                            <span>Observações:</span>
                            <textarea name="txtModalDet" id="txtModalDet" maxlength="1024" style="width:100%; height:100px" <?php echo $disabled; ?>><?php echo $modaldet; ?></textarea>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="tab-pane active" id="tab1">
                        <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                        <div style="width:50%; float:left">
                            <span>Descontos:</span>
                            <input name="txtDescontos" id="txtDescontos" type="text" class="input-medium" <?php echo $disabled; ?> value="<?php echo $desconto; ?>" maxlength="10"/>
                        </div>
                        <div style="clear:both; height:5px"></div>
                        <div style="width:50%; float:left">
                            <span>Outras Despesas:</span>
                            <input name="txtOutrasDespesas" id="txtOutrasDespesas" <?php echo $disabled; ?> maxlength="10" type="text" class="input-medium" value="<?php echo $outrasDespesas; ?>"/>
                        </div>
                        <div style="clear:both; height:5px"></div>
                        <div style="width:50%; float:left">
                            <span>Seguro:</span>
                            <input name="txtSeguro" id="txtSeguro" <?php echo $disabled; ?> maxlength="10" type="text" class="input-medium" value="<?php echo $seguro; ?>"/>
                        </div>
                        <div style="clear:both; height:5px"></div>
                        <div style="width:50%; float:left">
                            <span>Frete:</span>
                            <input name="txtFrete" id="txtFrete" <?php echo $disabled; ?> maxlength="10" type="text" class="input-medium" value="<?php echo $frete; ?>" readonly/>
                        </div>
                    </div>
                <?php } ?>
                <div class="tab-pane" id="tab2">
                    <div style="width:49%; float:left">
                        <span>Regime:</span>
                        <select name="txtICMSRegime" id="txtICMSRegime" class="select" style="width:100%" <?php echo $disabled; ?>>
                            <option value="1" <?php
                            if ($ICMSRegime == "1") {
                                echo "SELECTED";
                            }
                            ?>>Simples Nacional</option>
                            <option value="3" <?php
                            if ($ICMSRegime == "3") {
                                echo "SELECTED";
                            }
                            ?>>Tributação Normal</option>
                        </select>
                    </div>
                    <div style="width:50%; float:left; margin-left: 1%">
                        <span>Situação Tributária:</span>
                        <select name="txtICMSSitTrib" id="txtICMSSitTrib" class="select" style="width:100%" <?php echo $disabled; ?>>
                            <option id="SN_1" value="101" <?php
                            if ($ICMSSitTrib == "101") {
                                echo "SELECTED";
                            }
                            ?>>101 - Tributada com permissão de crédito</option>
                            <option id="SN_2" value="102" <?php
                            if ($ICMSSitTrib == "102") {
                                echo "SELECTED";
                            }
                            ?>>102 - Tributada sem permissão de crédito</option>
                            <option id="SN_3" value="103" <?php
                            if ($ICMSSitTrib == "103") {
                                echo "SELECTED";
                            }
                            ?>>103 - Isenção do ICMS para faixa de receita bruta</option>
                            <option id="SN_4" value="201" <?php
                            if ($ICMSSitTrib == "201") {
                                echo "SELECTED";
                            }
                            ?>>201 - Tributada com permissão de crédito e com cobrança do ICMS por ST</option>
                            <option id="SN_5" value="202" <?php
                            if ($ICMSSitTrib == "202") {
                                echo "SELECTED";
                            }
                            ?>>202 - Tributada sem permissão de crédito e com cobrança do ICMS por ST</option>
                            <option id="SN_6" value="203" <?php
                            if ($ICMSSitTrib == "203") {
                                echo "SELECTED";
                            }
                            ?>>203 - Isenção do ICMS para faixa de receita bruta e com cobrança do ICMS por ST</option>
                            <option id="SN_7" value="300" <?php
                            if ($ICMSSitTrib == "300") {
                                echo "SELECTED";
                            }
                            ?>>300 - Imune</option>
                            <option id="SN_8" value="400" <?php
                            if ($ICMSSitTrib == "400") {
                                echo "SELECTED";
                            }
                            ?>>400 - Não Tributada</option>
                            <option id="SN_9" value="500" <?php
                            if ($ICMSSitTrib == "500") {
                                echo "SELECTED";
                            }
                            ?>>500 - ICMS cobrado anteriormente por ST ou por antecipação</option>
                            <option id="SN_10" value="900" <?php
                            if ($ICMSSitTrib == "900") {
                                echo "SELECTED";
                            }
                            ?>>900 - Outros</option>
                            <option id="RN_1" value="00" <?php
                            if ($ICMSSitTrib == "00") {
                                echo "SELECTED";
                            }
                            ?>>00 - Tributada Integralmente</option>
                            <option id="RN_2" value="1001" <?php
                            if ($ICMSSitTrib == "1001") {
                                echo "SELECTED";
                            }
                            ?>>10 - Tributada com cobrança do ICMS por ST</option>
                            <option id="RN_3" value="1002" <?php
                            if ($ICMSSitTrib == "1002") {
                                echo "SELECTED";
                            }
                            ?>>10 - Tributada com cobrança do ICMS por ST (compartilha do ICMS entre a UF de origem e a UF de destino ou a UF definida  na legislação)</option>
                            <option id="RN_4" value="20" <?php
                            if ($ICMSSitTrib == "20") {
                                echo "SELECTED";
                            }
                            ?>>20 - Com redução da base de cálculo</option>
                            <option id="RN_5" value="30" <?php
                            if ($ICMSSitTrib == "30") {
                                echo "SELECTED";
                            }
                            ?>>30 - Isenta ou não tributada e com cobrança do ICMS por ST</option>
                            <option id="RN_6" value="40" <?php
                            if ($ICMSSitTrib == "40") {
                                echo "SELECTED";
                            }
                            ?>>40 - Isenta</option>
                            <option id="RN_7" value="411" <?php
                            if ($ICMSSitTrib == "411") {
                                echo "SELECTED";
                            }
                            ?>>41 - Não tributada</option>
                            <option id="RN_8" value="412" <?php
                            if ($ICMSSitTrib == "412") {
                                echo "SELECTED";
                            }
                            ?>>41 - Não tributada (ICMSST devido para a UF de destino, nas operações interestaduais de produtos que tiveram retenção antecipada dde ICMS por ST na UF do emitente)</option>
                            <option id="RN_9" value="60" <?php
                            if ($ICMSSitTrib == "60") {
                                echo "SELECTED";
                            }
                            ?>>60 - Cobrada anteriormente por ST</option>
                        </select>
                    </div>
                    <div style="clear:both; height:5px"></div>
                    <div style="width:100%; float:left">
                        <span>Origem:</span>
                        <select name="txtICMSOrigem" id="txtICMSOrigem" class="select" style="width:100%" <?php echo $disabled; ?>>
                            <option value="null">Selecione</option>
                            <option value="0" <?php
                            if ($ICMSOrigem == "0") {
                                echo "SELECTED";
                            }
                            ?>>0 - Nacional, exceto as indicadas nos códigos 3,4,5 e 8</option>
                            <option value="1" <?php
                            if ($ICMSOrigem == "1") {
                                echo "SELECTED";
                            }
                            ?>>1 - Estrangeira - Importação direta, exceto a indicada no código 6</option>
                            <option value="2" <?php
                            if ($ICMSOrigem == "2") {
                                echo "SELECTED";
                            }
                            ?>>2 - Estrangeira - Adquirida no mercado interno, exceto a indicada no código 7</option>
                            <option value="3" <?php
                            if ($ICMSOrigem == "3") {
                                echo "SELECTED";
                            }
                            ?>>3 - Nacional, mercadoria ou bem com conteúdo de Importação superior a 40% e inferior ou igual a 70%</option>
                            <option value="4" <?php
                            if ($ICMSOrigem == "4") {
                                echo "SELECTED";
                            }
                            ?>>4 - Nacional, cuja produção tenha sido feita em conformidade com os processos produtivos básicos de que tratam as legislações citadas nos Ajustes</option>
                            <option value="5" <?php
                            if ($ICMSOrigem == "5") {
                                echo "SELECTED";
                            }
                            ?>>5 - Nacional, mercadoria ou bem com conteúdo de Importação inferior ou igual a 40%</option>
                            <option value="6" <?php
                            if ($ICMSOrigem == "6") {
                                echo "SELECTED";
                            }
                            ?>>6 - Estrangeira - Importação direta, sem similar nacional, contante em lista da CAMEX e gás natural</option>
                            <option value="7" <?php
                            if ($ICMSOrigem == "7") {
                                echo "SELECTED";
                            }
                            ?>>7 - Estrangeira - Adquirida no mercado interno, sem similar nacional, contante em lista da CAMEX e gás natural</option>
                            <option value="8" <?php
                            if ($ICMSOrigem == "8") {
                                echo "SELECTED";
                            }
                            ?>>8 - Nacional, mercadoria ou bem com conteúdo de Importação superior a 70 %</option>
                        </select>
                    </div>
                    <div style="clear:both; height:5px"></div>
                    <div style="width:49%; float:left">
                        <span id="AliqCalcCred">Alíquota aplicável de cálculo de crédito:</span>
                        <input name="txtAliqCalcCred" id="txtAliqCalcCred" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $aliqcalccred; ?>"/>
                    </div>
                    <?php if ($tpTela !== "") { ?>
                        <div style="width:50%; float:left;margin-left:1%;">
                            <span id="CredICMSApro">Crédito do ICMS que pode ser aproveitado:</span>
                            <input readonly name="txtCredICMSApro" id="txtCredICMSApro" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $credicmsapro; ?>"/>
                        </div>
                    <?php } ?>
                    <div style="clear:both; height:5px"></div>
                    <div style="width:49%; display:block; float:left; ">
                        <div id="titICMS">
                            <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">ICMS</span>
                            <hr style="margin:2px 0; border-top:1px solid #ddd">
                        </div>
                        <div class="content">
                            <div id="ICMSModDet">
                                <span>Modalid. Determ. da BC ICMS:</span>
                                <select name="txtICMSModDet" id="txtICMSModDet" class="select" style="width:100%" <?php echo $disabled; ?>>
                                    <option value="null"></option>
                                    <option value="0" <?php
                                    if ($ICMSModDet == "0") {
                                        echo "SELECTED";
                                    }
                                    ?>>Margem Valor Agregado</option>
                                    <option value="1" <?php
                                    if ($ICMSModDet == "1") {
                                        echo "SELECTED";
                                    }
                                    ?>>Pauta (valor)</option>
                                    <option value="2" <?php
                                    if ($ICMSModDet == "2") {
                                        echo "SELECTED";
                                    }
                                    ?>>Preço Tabelado Máx. (valor)</option>
                                    <option value="3" <?php
                                    if ($ICMSModDet == "3") {
                                        echo "SELECTED";
                                    }
                                    ?>>Valor da Operação</option>
                                </select>
                            </div>
                            <div id="ICMSRedBc" style="margin-top: 5px;">
                                <span>% Redução da BC ICMS</span>
                                <input name="txtICMSRedBc" id="txtICMSRedBc" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $icmsredbc; ?>"/>
                            </div>
                            <?php if ($tpTela !== "") { ?>
                                <div id="ICMSValBc" style="margin-top: 5px;"> 
                                    <span>BC ICMS</span>
                                    <input readonly name="txtICMSValBc" id="txtICMSValBc" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $icmsvalbc; ?>"/>
                                </div>
                            <?php } ?>
                            <div id="ICMSAliq" style="margin-top: 5px;">
                                <span>Alíquota ICMS</span>
                                <input name="txtICMSAliq" id="txtICMSAliq" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $icmsaliq; ?>"/>
                            </div>
                            <div id="ICMSFcp" style="margin-top: 5px;">
                                <span>% relativo ao FCP</span>
                                <input name="txtICMSFcp" id="txtICMSFcp" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $icmsfcp; ?>"/>
                            </div>
                            <?php if ($tpTela !== "") { ?>
                                <div id="ICMSVal" style="margin-top: 5px;"> 
                                    <span>ICMS</span>
                                    <input readonly name="txtICMSVal" id="txtICMSVal" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $icmsval; ?>"/>
                                </div>
                            <?php } ?>
                            <?php if ($tpTela !== "") { ?>
                                <div id="ICMSValDes" style="margin-top: 5px;"> 
                                    <span>Valor ICMS Desonerado</span>
                                    <input name="txtICMSValDes" id="txtICMSValDes" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $icmsvaldes; ?>"/>
                                </div>
                            <?php } ?>
                            <div id="ICMSMotDes" style="margin-top: 5px;">
                                <span>Motivo Desoneração ICM</span>
                                <select name="txtICMSMotDes" id="txtICMSMotDes" class="select" style="width:100%" <?php echo $disabled; ?>>
                                    <option value="null"></option>
                                    <option value="3" <?php
                                    if ($icmsvalmot == "3") {
                                        echo "SELECTED";
                                    }
                                    ?>>Produtor Agropecuário</option>
                                    <option value="9" <?php
                                    if ($icmsvalmot == "9") {
                                        echo "SELECTED";
                                    }
                                    ?>>Outros</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div style="width:50%; display:block; float:left;margin-left: 1% ">
                        <div id="titST">
                            <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">ICMS ST</span>
                            <hr style="margin:2px 0; border-top:1px solid #ddd">
                        </div>
                        <div class="content">
                            <div id="ICMSModDetSt">
                                <span>Modalid. Determ. da BC ICMS ST:</span>
                                <select name="txtICMSModDetSt" id="txtICMSModDetSt" class="select" style="width:100%" <?php echo $disabled; ?>>
                                    <option value="null"></option>
                                    <option value="0" <?php
                                    if ($ICMSModDetSt == "0") {
                                        echo "SELECTED";
                                    }
                                    ?>>Preço Tabelado ou Máximo Sugerido</option>
                                    <option value="1" <?php
                                    if ($ICMSModDetSt == "1") {
                                        echo "SELECTED";
                                    }
                                    ?>>Lista Negativa (valor)</option>
                                    <option value="2" <?php
                                    if ($ICMSModDetSt == "2") {
                                        echo "SELECTED";
                                    }
                                    ?>>Lista Positiva (valor)</option>
                                    <option value="3" <?php
                                    if ($ICMSModDetSt == "3") {
                                        echo "SELECTED";
                                    }
                                    ?>>Lista Neutra (valor)</option>
                                    <option value="4" <?php
                                    if ($ICMSModDetSt == "4") {
                                        echo "SELECTED";
                                    }
                                    ?>>Margem Valor Agregado (%)</option>
                                    <option value="5" <?php
                                    if ($ICMSModDetSt == "5") {
                                        echo "SELECTED";
                                    }
                                    ?>>Pauta (valor)</option>
                                </select>
                            </div>
                            <div id="ICMSRedBcSt" style="margin-top: 5px;">
                                <span>% Redução da BC ICMS ST</span>
                                <input name="txtICMSRedBcSt" id="txtICMSRedBcSt" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $icmsredbcst; ?>"/>
                            </div>
                            <div id="ICMSMargValAdSt" style="margin-top: 5px;">
                                <span>% Margem de Valor Adicional ICMS ST</span>
                                <input name="txtICMSMargValAdSt" id="txtICMSMargValAdSt" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $icmsmargvaladst; ?>"/>
                            </div>
                            <?php if ($tpTela !== "") { ?>
                                <div id="ICMSValBcSt" style="margin-top: 5px;">
                                    <span>BC ICMS ST</span>
                                    <input readonly name="txtICMSValBcSt" id="txtICMSValBcSt" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $icmsvalbcst; ?>"/>
                                </div>
                            <?php } ?>
                            <div id="ICMSAliqSt" style="margin-top: 5px;">
                                <span>Alíquota ICMS ST</span>
                                <input name="txtICMSAliqSt" id="txtICMSAliqSt" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $icmsaliqst; ?>"/>
                            </div>
                            <?php if ($tpTela !== "") { ?>
                                <div id="ICMSValSt" style="margin-top: 5px;">
                                    <span>ICMS ST</span>
                                    <input readonly name="txtICMSValSt" id="txtICMSValSt" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $icmsvalst; ?>"/>
                                </div>
                                <div id="ICMSBCAnt" style="margin-top: 5px;">
                                    <span>BC ICMS ST retido anteriormente</span>
                                    <input name="txtICMSBCAnt" id="txtICMSBCAnt" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $icmsvalbcant; ?>"/>
                                </div>
                                <div id="ICMSSTAnt" style="margin-top: 5px;">
                                    <span> ICMS ST retido anteriormente</span>
                                    <input name="txtICMSSTAnt" id="txtICMSSTAnt" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $icmsvalstant; ?>"/>
                                </div>
                            <?php } ?>                            
                            <div id="BCICMSSTRetido" style="margin-top: 5px;">
                                <span>BC ICMS ST retido na UF remetente</span>
                                <input name="txtBCICMSSTRetido" id="txtBCICMSSTRetido" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $icmsbcstretido; ?>"/>
                            </div>
                            <div id="ICMSSTRetido" style="margin-top: 5px;">
                                <span>ICMS ST retido na UF remetente</span>
                                <input name="txtICMSSTRetido" id="txtICMSSTRetido" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $icmsstretido; ?>"/>
                            </div>
                            <div id="BCICMSSTDestino" style="margin-top: 5px;">
                                <span>BC ICMS ST da UF destino</span>
                                <input name="txtBCICMSSTDestino" id="txtBCICMSSTDestino" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $icmsbcstdestino; ?>"/>
                            </div>
                            <div id="ICMSSTDestino" style="margin-top: 5px;">
                                <span>ICMS ST da UF destino</span>
                                <input name="txtICMSSTDestino" id="txtICMSSTDestino" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $icmsstdestino; ?>"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab3">
                    <div style="width:100%; float:left;">
                        <span>Situação Tributária</span>
                        <select name="txtIPISitTrib" id="txtIPISitTrib" class="select" style="width:100%" <?php echo $disabled; ?>>
                            <option value="null">Selecione</option>
                            <option value="0" <?php
                            if ($IPISitTrib == "0") {
                                echo "SELECTED";
                            }
                            ?>>IPI 00 - Entrada com recuperação de crédito</option>
                            <option value="1" <?php
                            if ($IPISitTrib == "1") {
                                echo "SELECTED";
                            }
                            ?>>IPI 01 - Entrada tributada com alíquita zero</option>
                            <option value="2" <?php
                            if ($IPISitTrib == "2") {
                                echo "SELECTED";
                            }
                            ?>>IPI 02 - Entrada isenta</option>
                            <option value="3" <?php
                            if ($IPISitTrib == "3") {
                                echo "SELECTED";
                            }
                            ?>>IPI 03 - Entrada não-tributada</option>
                            <option value="4" <?php
                            if ($IPISitTrib == "4") {
                                echo "SELECTED";
                            }
                            ?>>IPI 04 - Entrada imune</option>
                            <option value="5" <?php
                            if ($IPISitTrib == "5") {
                                echo "SELECTED";
                            }
                            ?>>IPI 05 - Entrada com suspensão</option>
                            <option value="49" <?php
                            if ($IPISitTrib == "49") {
                                echo "SELECTED";
                            }
                            ?>>IPI 49 - Outras entradas</option>
                            <option value="50" <?php
                            if ($IPISitTrib == "50") {
                                echo "SELECTED";
                            }
                            ?>>IPI 50 - Saída tributada</option>
                            <option value="51" <?php
                            if ($IPISitTrib == "51") {
                                echo "SELECTED";
                            }
                            ?>>IPI 51 - Saída tributada com alíquita zero</option>
                            <option value="52" <?php
                            if ($IPISitTrib == "52") {
                                echo "SELECTED";
                            }
                            ?>>IPI 52 - Saída isenta</option>
                            <option value="53" <?php
                            if ($IPISitTrib == "53") {
                                echo "SELECTED";
                            }
                            ?>>IPI 53 - Saída não-tributada</option>
                            <option value="54" <?php
                            if ($IPISitTrib == "54") {
                                echo "SELECTED";
                            }
                            ?>>IPI 54 - Saída imune</option>
                            <option value="55" <?php
                            if ($IPISitTrib == "55") {
                                echo "SELECTED";
                            }
                            ?>>IPI 55 - Saída com suspensão</option>
                            <option value="99" <?php
                            if ($IPISitTrib == "99") {
                                echo "SELECTED";
                            }
                            ?>>IPI 99 - Outras saídas</option>
                        </select>
                    </div>
                    <div style="clear:both; height:5px;"></div>
                    <div style="width:49%; float:left;">
                        <span>Classe de Enquadramento</span>
                        <input name="txtIPIClasEnq" id="txtIPIClasEnq" <?php echo $disabled; ?> type="text" maxlength="5" class="input-medium" value="<?php echo $ipiclassenq; ?>"/>
                    </div>
                    <div style="width:50%; float:left;margin-left: 1%;">
                        <span>Código de Enquadramento</span>
                        <input name="txtIPICodEnq" id="txtIPICodEnq" <?php echo $disabled; ?> type="text" maxlength="3" class="input-medium" value="<?php echo $ipicodenq; ?>"/>
                    </div>
                    <div style="clear:both; height:5px;"></div>
                    <div style="width:49%; float:left;">
                        <span>CNPJ do Produtor</span>
                        <input name="txtIPICnpjProd" id="txtIPICnpjProd" <?php echo $disabled; ?> type="text" maxlength="18" class="input-medium" value="<?php echo $ipicnpjprod; ?>"/>
                    </div>
                    <div style="width:50%; float:left;margin-left: 1%;">
                        <span>Código do Selo de Controle</span>
                        <input name="txtIPICodSeloCont" id="txtIPICodSeloCont" <?php echo $disabled; ?> type="text" maxlength="60" class="input-medium" value="<?php echo $ipicodselocont; ?>"/>
                    </div>
                    <div style="clear:both; height:5px;"></div>
                    <div style="width:49%; float:left;">
                        <span>Quantidade do Selo de Controle</span>
                        <input name="txtIPIQtdSeloCont" id="txtIPIQtdSeloCont" <?php echo $disabled; ?> type="text" maxlength="8" class="input-medium" value="<?php echo $ipiqtdselocont; ?>"/>
                    </div>
                    <div style="width:50%; float:left;margin-left: 1%;">
                        <span>Tipo de Cálculo</span>
                        <select name="txtIPITipoCalc" id="txtIPITipoCalc" class="select" style="width:100%" <?php echo $disabled; ?>>
                            <option value="null" <?php
                            if ($IPITipoCalc == "null") {
                                echo "selected";
                            }
                            ?>>Selecione</option>
                            <option value="0" <?php
                            if ($IPITipoCalc == "0") {
                                echo "selected";
                            }
                            ?>>Percentual</option>
                            <option value="1" <?php
                            if ($IPITipoCalc == "1") {
                                echo "selected";
                            }
                            ?>>Em Valor</option>
                        </select>
                    </div>
                    <div style="clear:both; height:5px;"></div>
                    <div style="width:49%; float:left;">
                        <span>Alíquota</span>
                        <input name="txtIPIAliq" id="txtIPIAliq" type="text" class="input-medium" value="<?php echo $ipialiq; ?>" <?php echo $disabled; ?>/>
                    </div>
                    <?php if ($tpTela !== "") { ?>
                        <div style="width:50%; float:left;margin-left: 1%">
                            <span>Valor Base de Cálculo</span>
                            <input name="txtIPIValBaseCalc" id="txtIPIValBaseCalc" type="text" class="input-medium" value="<?php echo $ipivalbasecalc; ?>" <?php echo $disabled; ?>/>
                        </div>
                        <div style="clear:both; height:5px;"></div>
                        <div style="width:49%; float:left;">
                            <span>Valor do IPI</span>
                            <input name="txtIPIValor" id="txtIPIValor" type="text" class="input-medium" value="<?php echo $ipivalor; ?>" <?php echo $disabled; ?>/>
                        </div>
                    <?php } ?>
                </div>
                <div class="tab-pane" id="tab4">
                    <div style="width:100%; float:left;">
                        <span>Situação Tributária</span>
                        <select name="txtPISSitTrib" id="txtPISSitTrib" class="select" style="width:100%" <?php echo $disabled; ?>>
                            <option value="null">Selecione</option>
                            <option value="1" <?php
                            if ($PISSitTrib == "1") {
                                echo "SELECTED";
                            }
                            ?>>PIS 01 - Operação Tributável - Base de Cálculo = Valor da Operação Alíquota Normal (Cumulativo/Não Cumulativo)</option>
                            <option value="2" <?php
                            if ($PISSitTrib == "2") {
                                echo "SELECTED";
                            }
                            ?>>PIS 02 - Operação Tributável - Base de Cálculo = Valor da Operação (Alíquota Diferenciada)</option>
                            <option value="3" <?php
                            if ($PISSitTrib == "3") {
                                echo "SELECTED";
                            }
                            ?>>PIS 03 - Operação Tributável - Base de Cálculo = Quantidade Vendida x Alíquota por Unidade de Produto</option>
                            <option value="4" <?php
                            if ($PISSitTrib == "4") {
                                echo "SELECTED";
                            }
                            ?>>PIS 04 - Operação Tributável - Tributação Monofásica (Alíquota Zero)</option>
                            <option value="5" <?php
                            if ($PISSitTrib == "5") {
                                echo "SELECTED";
                            }
                            ?>>PIS 05 - Operação Tributável(ST)</option>
                            <option value="6" <?php
                            if ($PISSitTrib == "6") {
                                echo "SELECTED";
                            }
                            ?>>PIS 06 - Operação Tributável (Alíquota Zero)</option>
                            <option value="7" <?php
                            if ($PISSitTrib == "7") {
                                echo "SELECTED";
                            }
                            ?>>PIS 07 - Operação Isenta da Contribuição</option>
                            <option value="8" <?php
                            if ($PISSitTrib == "8") {
                                echo "SELECTED";
                            }
                            ?>>PIS 08 - Operação sem Incidência da Contribuição</option>
                            <option value="9" <?php
                            if ($PISSitTrib == "9") {
                                echo "SELECTED";
                            }
                            ?>>PIS 09 - Operação com Suspensão da Contribuição</option>
                            <option value="49" <?php
                            if ($PISSitTrib == "49") {
                                echo "SELECTED";
                            }
                            ?>>PIS 49 - Outras Operações de Saída</option>
                            <option value="50" <?php
                            if ($PISSitTrib == "50") {
                                echo "SELECTED";
                            }
                            ?>>PIS 50 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receita Tributada no Mercado Interno</option>
                            <option value="51" <?php
                            if ($PISSitTrib == "51") {
                                echo "SELECTED";
                            }
                            ?>>PIS 51 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receita Não Tributada no Mercado Interno</option>
                            <option value="52" <?php
                            if ($PISSitTrib == "52") {
                                echo "SELECTED";
                            }
                            ?>>PIS 52 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receita de Exportação</option>
                            <option value="53" <?php
                            if ($PISSitTrib == "53") {
                                echo "SELECTED";
                            }
                            ?>>PIS 53 - Operação com Direito a Crédito - Vinculada a Receitas Tributadas e Não Tributadas no Mercado Interno</option>
                            <option value="54" <?php
                            if ($PISSitTrib == "54") {
                                echo "SELECTED";
                            }
                            ?>>PIS 54 - Operação com Direito a Crédito - Vinculada a Receitas Tributadas no Mercado Interno e de Exportação</option>
                            <option value="55" <?php
                            if ($PISSitTrib == "55") {
                                echo "SELECTED";
                            }
                            ?>>PIS 55 - Operação com Direito a Crédito - Vinculada a Receitas Não-Tributadas no Mercado Interno e de Exportação</option>
                            <option value="56" <?php
                            if ($PISSitTrib == "56") {
                                echo "SELECTED";
                            }
                            ?>>PIS 56 - Operação com Direito a Crédito - Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno, e de Exportação</option>
                            <option value="60" <?php
                            if ($PISSitTrib == "60") {
                                echo "SELECTED";
                            }
                            ?>>PIS 60 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita Tributada no Mercado Interno</option>
                            <option value="61" <?php
                            if ($PISSitTrib == "61") {
                                echo "SELECTED";
                            }
                            ?>>PIS 61 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita Não-Tributada no Mercado Interno</option>
                            <option value="62" <?php
                            if ($PISSitTrib == "62") {
                                echo "SELECTED";
                            }
                            ?>>PIS 62 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita de Exportação</option>
                            <option value="63" <?php
                            if ($PISSitTrib == "63") {
                                echo "SELECTED";
                            }
                            ?>>PIS 63 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno</option>
                            <option value="64" <?php
                            if ($PISSitTrib == "64") {
                                echo "SELECTED";
                            }
                            ?>>PIS 64 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas no Mercado Interno e de Exportação</option>
                            <option value="65" <?php
                            if ($PISSitTrib == "65") {
                                echo "SELECTED";
                            }
                            ?>>PIS 65 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Não-Tributadas no Mercado Interno e de Exportação</option>
                            <option value="66" <?php
                            if ($PISSitTrib == "66") {
                                echo "SELECTED";
                            }
                            ?>>PIS 66 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno, e de Exportação</option>
                            <option value="67" <?php
                            if ($PISSitTrib == "67") {
                                echo "SELECTED";
                            }
                            ?>>PIS 67 - Crédito Presumido - Outras Operações</option>
                            <option value="70" <?php
                            if ($PISSitTrib == "70") {
                                echo "SELECTED";
                            }
                            ?>>PIS 70 - Operação de Aquisição sem Direito a Crédito</option>
                            <option value="71" <?php
                            if ($PISSitTrib == "71") {
                                echo "SELECTED";
                            }
                            ?>>PIS 71 - Operação de Aquisição com Isenção</option>
                            <option value="72" <?php
                            if ($PISSitTrib == "72") {
                                echo "SELECTED";
                            }
                            ?>>PIS 72 - Operação de Aquisição com Suspensão</option>
                            <option value="73" <?php
                            if ($PISSitTrib == "73") {
                                echo "SELECTED";
                            }
                            ?>>PIS 73 - Operação de Aquisição a Alíquota Zero</option>
                            <option value="74" <?php
                            if ($PISSitTrib == "74") {
                                echo "SELECTED";
                            }
                            ?>>PIS 74 - Operação de Aquisição sem Incidência da Contribuição</option>
                            <option value="75" <?php
                            if ($PISSitTrib == "75") {
                                echo "SELECTED";
                            }
                            ?>>PIS 75 - Operação de Aquisição por Substituição Tributária</option>
                            <option value="98" <?php
                            if ($PISSitTrib == "98") {
                                echo "SELECTED";
                            }
                            ?>>PIS 98 - Outras saídas de Entrada</option>
                            <option value="99" <?php
                            if ($PISSitTrib == "99") {
                                echo "SELECTED";
                            }
                            ?>>PIS 99 - Outras saídas</option>
                        </select>
                    </div>
                    <div style="clear:both; height:5px;"></div>
                    <div style="width:49%; float:left;">
                        <span>Tipo de Cálculo</span>
                        <select name="txtPISTipoCalc" id="txtPISTipoCalc" class="select" style="width:100%" <?php echo $disabled; ?>>
                            <option value="null" <?php
                            if ($PISTipoCalc == "null") {
                                echo "selected";
                            }
                            ?>>Selecione</option>
                            <option value="0" <?php
                            if ($PISTipoCalc == "0") {
                                echo "selected";
                            }
                            ?>>Percentual</option>
                            <option value="1" <?php
                            if ($PISTipoCalc == "1") {
                                echo "selected";
                            }
                            ?>>Em Valor</option>
                        </select>
                    </div>
                    <?php if ($tpTela !== "") { ?>
                        <div style="width:50%; float:left;margin-left: 1%">
                            <span>Valor da Base de Cálculo</span>
                            <input name="txtPISValBaseCalc" id="txtPISValBaseCalc" type="text" class="input-medium" value="<?php echo $pisvalbasecalc; ?>" <?php echo $disabled; ?>/>
                        </div>
                    <?php } ?>
                    <div style="clear:both; height:5px;"></div>
                    <div style="width:49%; float:left;">
                        <span>Alíquota(Percentual)</span>
                        <input name="txtPISAliqPerc" id="txtPISAliqPerc" type="text" class="input-medium" value="<?php echo $pisaliqperc; ?>" <?php echo $disabled; ?>/>
                    </div>
                    <div style="width:50%; float:left;margin-left: 1%">
                        <span>Alíquota(Em Reais)</span>
                        <input name="txtPISAliqReal" id="txtPISAliqReal" type="text" class="input-medium" value="<?php echo $pisaliqreal; ?>" <?php echo $disabled; ?>/>
                    </div>
                    <div style="clear:both; height:5px;"></div>
                    <div style="width:49%; float:left;">
                        <span>Quantidade Vendida</span>
                        <input name="txtPISQtdVend" id="txtPISQtdVend" type="text" class="input-medium" value="<?php echo $pisqtdvend; ?>" <?php echo $disabled; ?>/>
                    </div>
                    <?php if ($tpTela !== "") { ?>
                        <div style="width:50%; float:left;margin-left: 1%">
                            <span>Valor do PIS</span>
                            <input name="txtPISValor" id="txtPISValor" type="text" class="input-medium" value="<?php echo $pisvalor; ?>" <?php echo $disabled; ?>/>
                        </div>
                    <?php } ?>
                    <div style="clear:both; height:5px;"></div>
                    <div style="width:49%; float:left;">
                        <span>Tipo de Cálculo ST</span>
                        <select name="txtPISTipoCalcST" id="txtPISTipoCalcST" class="select" style="width:100%" <?php echo $disabled; ?>>
                            <option value="null" <?php
                            if ($PISTipoCalcST == "null") {
                                echo "selected";
                            }
                            ?>>Selecione</option>
                            <option value="0" <?php
                            if ($PISTipoCalcST == "0") {
                                echo "selected";
                            }
                            ?>>Percentual</option>
                            <option value="1" <?php
                            if ($PISTipoCalcST == "1") {
                                echo "selected";
                            }
                            ?>>Em Valor</option>
                        </select>
                    </div>
                    <?php if ($tpTela !== "") { ?>
                        <div style="width:50%; float:left;margin-left: 1%">
                            <span>Valor da Base de Cálculo ST</span>
                            <input name="txtPISValBaseCalcST" id="txtPISValBaseCalcST" type="text" class="input-medium" value="<?php echo $PISValBaseCalcST; ?>" <?php echo $disabled; ?>/>
                        </div>
                    <?php } ?>
                    <div style="clear:both; height:5px;"></div>
                    <div style="width:49%; float:left;">
                        <span>Alíquota(Percentual) ST</span>
                        <input name="txtPISAliqPercST" id="txtPISAliqPercST" type="text" class="input-medium" value="<?php echo $PISAliqPercST; ?>" <?php echo $disabled; ?>/>
                    </div>
                    <div style="width:50%; float:left;margin-left: 1%">
                        <span>Alíquota(Em Reais) ST</span>
                        <input name="txtPISAliqRealST" id="txtPISAliqRealST" type="text" class="input-medium" value="<?php echo $PISAliqRealST; ?>" <?php echo $disabled; ?>/>
                    </div>
                    <div style="clear:both; height:5px;"></div>
                    <div style="width:49%; float:left;">
                        <span>Quantidade Vendida ST</span>
                        <input name="txtPISQtdVendST" id="txtPISQtdVendST" type="text" class="input-medium" value="<?php echo $PISQtdVendST; ?>" <?php echo $disabled; ?>/>
                    </div>
                    <?php if ($tpTela !== "") { ?>
                        <div style="width:50%; float:left;margin-left: 1%">
                            <span>Valor do PIS ST</span>
                            <input name="txtPISValorST" id="txtPISValorST" type="text" class="input-medium" value="<?php echo $PISValorST; ?>" <?php echo $disabled; ?>/>
                        </div>
                    <?php } ?>
                </div>
                <div class="tab-pane" id="tab5">
                    <div style="width:100%; float:left;">
                        <span>Situação Tributária</span>
                        <select name="txtCOFINSSitTrib" id="txtCOFINSSitTrib" class="select" style="width:100%" <?php echo $disabled; ?>>
                            <option value="null">Selecione</option>
                            <option value="1" <?php
                            if ($COFINSSitTrib == "1") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 01 - Operação Tributável - Base de Cálculo = Valor da Operação Alíquota Normal (Cumulativo/Não Cumulativo)</option>
                            <option value="2" <?php
                            if ($COFINSSitTrib == "2") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 02 - Operação Tributável - Base de Cálculo = Valor da Operação (Alíquota Diferenciada)</option>
                            <option value="3" <?php
                            if ($COFINSSitTrib == "3") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 03 - Operação Tributável - Base de Cálculo = Quantidade Vendida x Alíquota por Unidade de Produto</option>
                            <option value="4" <?php
                            if ($COFINSSitTrib == "4") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 04 - Operação Tributável - Tributação Monofásica (Alíquota Zero)</option>
                            <option value="5" <?php
                            if ($COFINSSitTrib == "5") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 05 - Operação Tributável(ST)</option>
                            <option value="6" <?php
                            if ($COFINSSitTrib == "6") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 06 - Operação Tributável (Alíquota Zero)</option>
                            <option value="7" <?php
                            if ($COFINSSitTrib == "7") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 07 - Operação Isenta da Contribuição</option>
                            <option value="8" <?php
                            if ($COFINSSitTrib == "8") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 08 - Operação sem Incidência da Contribuição</option>
                            <option value="9" <?php
                            if ($COFINSSitTrib == "9") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 09 - Operação com Suspensão da Contribuição</option>
                            <option value=49" <?php
                            if ($COFINSSitTrib == "49") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 49 - Outras Operações de Saída</option>
                            <option value="50" <?php
                            if ($COFINSSitTrib == "50") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 50 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receitas Tributadas no Mercado Interno</option>
                            <option value="51" <?php
                            if ($COFINSSitTrib == "51") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 51 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receitas Não Tributadas no Mercado Interno</option>
                            <option value="52" <?php
                            if ($COFINSSitTrib == "52") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 52 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receitas de Exportação</option>
                            <option value="53" <?php
                            if ($COFINSSitTrib == "53") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 53 - Operação com Direito a Crédito - Vinculada a Receitas Tributadas e Não Tributadas no Mercado Interno</option>
                            <option value="54" <?php
                            if ($COFINSSitTrib == "54") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 54 - Operação com Direito a Crédito - Vinculada a Receitas Tributadas no Mercado Interno e de Exportação</option>
                            <option value="55" <?php
                            if ($COFINSSitTrib == "55") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 55 - Operação com Direito a Crédito - Vinculada a Receitas Não Tributadas no Mercado Interno e de Exportação</option>
                            <option value="56" <?php
                            if ($COFINSSitTrib == "56") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 56 - Operação com Direito a Crédito - Vinculada a Receitas Tributadas e Não Tributadas no Mercado Interno e de Exportação</option>
                            <option value="60" <?php
                            if ($COFINSSitTrib == "60") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 60 - Crédito Presumido - Vinculada Exclusivamente a Receitas Tributadas no Mercado Interno</option>
                            <option value="61" <?php
                            if ($COFINSSitTrib == "61") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 61 - Crédito Presumido - Vinculada Exclusivamente a Receitas Não Tributadas no Mercado Interno</option>
                            <option value="62" <?php
                            if ($COFINSSitTrib == "62") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 62 - Crédito Presumido - Vinculada Exclusivamente a Receitas de Exportação</option>
                            <option value="63" <?php
                            if ($COFINSSitTrib == "63") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 63 - Crédito Presumido - Vinculada a Receitas Tributadas e Não Tributadas no Mercado Interno</option>
                            <option value="64" <?php
                            if ($COFINSSitTrib == "64") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 64 - Crédito Presumido - Vinculada a Receitas Tributadas no Mercado Interno e de Exportação</option>
                            <option value="65" <?php
                            if ($COFINSSitTrib == "65") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 65 - Crédito Presumido - Vinculada a Receitas Não Tributadas no Mercado Interno e de Exportação</option>
                            <option value="66" <?php
                            if ($COFINSSitTrib == "66") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 66 - Crédito Presumido - Vinculada a Receitas Tributadas e Não Tributadas no Mercado Interno e de Exportação</option>
                            <option value="67" <?php
                            if ($COFINSSitTrib == "67") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 67 - Crédito Presumido - Outras Operações</option>
                            <option value="70" <?php
                            if ($COFINSSitTrib == "70") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 70 - Operação de Aquisição sem Direito a Crédito</option>
                            <option value="71" <?php
                            if ($COFINSSitTrib == "71") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 71 - Operação de Aquisição com Isenção</option>
                            <option value="72" <?php
                            if ($COFINSSitTrib == "72") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 72 - Operação de Aquisição com Suspensão</option>
                            <option value="73" <?php
                            if ($COFINSSitTrib == "73") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 73 - Operação de Aquisição a Alíquota Zero</option>
                            <option value="74" <?php
                            if ($COFINSSitTrib == "74") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 74 - Operação de Aquisição sem Incidência da Contribuição</option>
                            <option value="75" <?php
                            if ($COFINSSitTrib == "75") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 75 - Operação de Aquisição por Substituição Tributária</option>
                            <option value="98" <?php
                            if ($COFINSSitTrib == "98") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 98 - Outras Operações de Entrada</option>
                            <option value="99" <?php
                            if ($COFINSSitTrib == "99") {
                                echo "SELECTED";
                            }
                            ?>>COFINS 99 - Outras saídas</option>
                        </select>
                    </div>
                    <div style="clear:both; height:5px;"></div>
                    <div style="width:49%; float:left;">
                        <span>Tipo de Cálculo</span>
                        <select name="txtCOFINSTipoCalc" id="txtCOFINSTipoCalc" class="select" style="width:100%" <?php echo $disabled; ?>>
                            <option value="null" <?php
                            if ($COFINSTipoCalc == "null") {
                                echo "selected";
                            }
                            ?>>Selecione</option>
                            <option value="0" <?php
                            if ($COFINSTipoCalc == "0") {
                                echo "selected";
                            }
                            ?>>Percentual</option>
                            <option value="1" <?php
                            if ($COFINSTipoCalc == "1") {
                                echo "selected";
                            }
                            ?>>Em Valor</option>
                        </select>
                    </div>
                    <?php if ($tpTela !== "") { ?>
                        <div style="width:50%; float:left;margin-left: 1%">
                            <span>Valor da Base de Cálculo</span>
                            <input name="txtCOFINSValBaseCalc" id="txtCOFINSValBaseCalc" type="text" class="input-medium" value="<?php echo $cofinsvalbasecalc; ?>" <?php echo $disabled; ?>/>
                        </div>
                    <?php } ?>
                    <div style="clear:both; height:5px;"></div>
                    <div style="width:49%; float:left;">
                        <span>Alíquota(Percentual)</span>
                        <input name="txtCOFINSAliqPerc" id="txtCOFINSAliqPerc" type="text" class="input-medium" value="<?php echo $cofinsaliqperc; ?>" <?php echo $disabled; ?>/>
                    </div>
                    <div style="width:50%; float:left;margin-left: 1%">
                        <span>Alíquota(Em Reais)</span>
                        <input name="txtCOFINSAliqReal" id="txtCOFINSAliqReal" type="text" class="input-medium" value="<?php echo $cofinsaliqreal; ?>" <?php echo $disabled; ?>/>
                    </div>
                    <div style="clear:both; height:5px;"></div>
                    <div style="width:49%; float:left;">
                        <span>Quantidade Vendida</span>
                        <input name="txtCOFINSQtdVend" id="txtCOFINSQtdVend" type="text" class="input-medium" value="<?php echo $cofinsqtdvend; ?>" <?php echo $disabled; ?>/>
                    </div>
                    <?php if ($tpTela !== "") { ?>
                        <div style="width:50%; float:left;margin-left: 1%">
                            <span>Valor do COFINS</span>
                            <input name="txtCOFINSValor" id="txtCOFINSValor" type="text" class="input-medium" value="<?php echo $cofinsvalor; ?>" <?php echo $disabled; ?>/>
                        </div>
                    <?php } ?>
                    <div style="clear:both; height:5px;"></div>
                    <div style="width:49%; float:left;">
                        <span>Tipo de Cálculo ST</span>
                        <select name="txtCOFINSTipoCalcST" id="txtCOFINSTipoCalcST" class="select" style="width:100%" <?php echo $disabled; ?>>
                            <option value="null" <?php
                            if ($COFINSTipoCalcST == "null") {
                                echo "selected";
                            }
                            ?>>Selecione</option>
                            <option value="0" <?php
                            if ($COFINSTipoCalcST == "0") {
                                echo "selected";
                            }
                            ?>>Percentual</option>
                            <option value="1" <?php
                            if ($COFINSTipoCalcST == "1") {
                                echo "selected";
                            }
                            ?>>Em Valor</option>
                        </select>
                    </div>
                    <?php if ($tpTela !== "") { ?>
                        <div style="width:50%; float:left;margin-left: 1%">
                            <span>Valor da Base de Cálculo ST</span>
                            <input name="txtCOFINSValBaseCalcST" id="txtCOFINSValBaseCalcST" type="text" class="input-medium" value="<?php echo $cofinsvalbasecalcst; ?>" <?php echo $disabled; ?>/>
                        </div>
                    <?php } ?>
                    <div style="clear:both; height:5px;"></div>
                    <div style="width:49%; float:left;">
                        <span>Alíquota(Percentual) ST</span>
                        <input name="txtCOFINSAliqPercST" id="txtCOFINSAliqPercST" type="text" class="input-medium" value="<?php echo $cofinsaliqpercst; ?>" <?php echo $disabled; ?>/>
                    </div>
                    <div style="width:50%; float:left;margin-left: 1%">
                        <span>Alíquota(Em Reais) ST</span>
                        <input name="txtCOFINSAliqRealST" id="txtCOFINSAliqRealST" type="text" class="input-medium" value="<?php echo $cofinsaliqrealst; ?>" <?php echo $disabled; ?>/>
                    </div>
                    <div style="clear:both; height:5px;"></div>
                    <div style="width:49%; float:left;">
                        <span>Quantidade Vendida ST</span>
                        <input name="txtCOFINSQtdVendST" id="txtCOFINSQtdVendST" type="text" class="input-medium" value="<?php echo $cofinsqtdvendst; ?>" <?php echo $disabled; ?>/>
                    </div>
                    <?php if ($tpTela !== "") { ?>
                        <div style="width:50%; float:left;margin-left: 1%">
                            <span>Valor do COFINS ST</span>
                            <input name="txtCOFINSValorST" id="txtCOFINSValorST" type="text" class="input-medium" value="<?php echo $cofinsvalorst; ?>" <?php echo $disabled; ?>/>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" onclick="return validar()" title="Gravar" name="bntSave"><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" type="button" title="Cancelar" onClick="parent.FecharBox();"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" title="Cancelar" name="bntAlterar" ><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else { ?>
                    <button class="btn green" type="submit" title="Novo" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"> </span>  Novo</button>
                    <button class="btn green" type="submit" title="Alterar" name="bntEdit" value="Alterar"> <span class="ico-edit"> </span>  Alterar</button>
                    <button class="btn red" type="submit" title="Excluir" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span>  Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src='../../js/charts.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>    
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type='text/javascript' src='js/Cenario.js'></script>    
    <?php odbc_close($con); ?>
</body>