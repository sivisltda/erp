<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include "./../../Connections/configini.php";

$id_area = $_REQUEST['q'];
$tipo = isset($_REQUEST['t']) ? $_REQUEST['t'] : "P";
$sql = "select conta_produto,descricao,preco_venda,quantidade_comercial from sf_produtos where descricao like '%" . $id_area . "%' and conta_produto > 0 AND tipo = '" . $tipo . "' ORDER BY descricao";
$cur = odbc_exec($con, $sql) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $row['id'] = $RFP['conta_produto'];
    $row['value'] = utf8_encode($RFP['descricao']);
    $row['preco_venda'] = utf8_encode($RFP['preco_venda']);
    $row['quantidade_comercial'] = utf8_encode($RFP['quantidade_comercial']);
    $row_set[] = $row;
}

echo json_encode($row_set);
odbc_close($con);
