<?php

include "./../../Connections/configini.php";
$dir = "/NFE/producao/enviadas/aprovadas";
if (isset($_GET['patch'])) {
    $folder = "/" . $_GET['patch'];
}

if (isset($_POST['btnfind'])) {
    $arquivos = $_POST['chk_content'];
    if (count($arquivos) > 0) {
        ob_start();
        $z = new ZipArchive();
        $criou = $z->open('./../../Pessoas/' . $contrato . $dir . $folder . '/' . $_GET['patch'] . '.zip', ZipArchive::CREATE);
        if ($criou === true) {
            for ($i = 0; $i < count($arquivos); $i++) {
                $z->addFile('./../../Pessoas/' . $contrato . $dir . $folder . '/' . $arquivos[$i], $arquivos[$i]);
            }
            $z->close();
            header("Content-length: " . filesize('./../../Pessoas/' . $contrato . $dir . $folder . '/' . $_GET['patch'] . ".zip"));
            header("Content-type: application/octet-stream");
            header("Content-disposition: attachment; filename=" . $_GET['patch'] . ".zip");
            readfile('./../../Pessoas/' . $contrato . $dir . $folder . '/' . $_GET['patch'] . ".zip");
            unlink('./../../Pessoas/' . $contrato . $dir . $folder . '/' . $_GET['patch'] . ".zip");
        } else {
            echo 'Erro: ' . $criou;
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>    
        <style>
            .fancybox-custom .fancybox-skin {
                box-shadow: 0 0 50px #069;
            }
        </style>
    </head>
    <body>
        <?php if ($imprimir == 0) { ?>
            <div id="loader"><img src="../../img/loader.gif"/></div><?php } ?>
        <div class="wrapper">
            <?php
            if ($imprimir == 0) {
                include('../../menuLateral.php');
            }
            ?>
            <div class="body">
                <?php
                if ($imprimir == 0) {
                    include("../../top.php");
                }
                ?>
                <div class="content">
                    <form action="Xml-Arquivos.php<?php
                    if (isset($_GET['patch'])) {
                        echo "?patch=" . $_GET['patch'];
                    }
                    ?>" method="POST">
                              <?php if ($imprimir == 0) { ?>				
                            <div class="page-header">
                                <div class="icon"> <span class="ico-arrow-right"></span></div>
                                <h1>Nota Fiscal Eletrônica<small>Gerenciador de Xml</small></h1> 
                            </div>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="boxfilter block">                                    
                                        <div style="float: right;">                                        
                                            <div style="float:left;">
                                                <div style="float:left">
                                                    <button name="btnfind" class="button button-turquoise btn-primary" type="submit" onclick="return countChecked();" title="Baixar"><span class="ico-download-3 icon-white"></span></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div style="clear:both"></div>
                        <div class="boxhead" <?php echo $visible; ?>>
                            <div class="boxtext">Gerenciador de Xml</div>
                        </div>
                        <div <?php
                        if ($imprimir == 0) {
                            echo "class=\"boxtable\"";
                        }
                        ?>>
                                <?php
                                if ($imprimir == 1) {
                                    $titulo_pagina = "RELATÓRIO DE CONSULTA<br/>Gerenciador de Xml";
                                    include "../Financeiro/Cabecalho-Impressao.php";
                                }
                                ?>
                            <table <?php
                            if ($imprimir == 0) {
                                echo "class=\"table\"";
                            } else {
                                echo "border=\"1\"";
                            }
                            ?> style="float:none" cellpadding="0" cellspacing="0" width="100%" id="example">
                                <thead>
                                    <tr>
                                        <th width="5%"><center><input id="checkAll" type="checkbox" class="checkall"/></center></th>
                                <th width="95%"><span style="cursor: pointer" onclick="redirectFind('');"><?php echo $dir . $folder; ?></span></th>                                    
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $handle = opendir('./../../Pessoas/' . $contrato . $dir . $folder);
                                    if ($handle) {
                                        while ($entry = readdir($handle)) {
                                            $ext = strtolower(pathinfo($entry, PATHINFO_EXTENSION));
                                            if ($entry != "." && $entry != "..") {
                                                ?>                                
                                                <tr>
                                                    <td style="text-align: center"><?php if ($ext == "xml") { ?><input name="chk_content[]" type="checkbox" value="<?php echo $entry; ?>" checked/><?php } ?></td>
                                                    <td>
                                                        <span <?php if ($ext != "xml") { ?>style="cursor: pointer" onclick="redirectFind('<?php echo $entry; ?>');"<?php } ?>>
                                                            <?php echo $entry; ?>
                                                        </span>
                                                    </td>
                                                </tr>                                
                                                <?php
                                            }
                                        }
                                        closedir($handle);
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <div style="clear:both"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>        
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>         
        <script type="text/javascript">
            
            function countChecked() {
                if ($('#example tbody input:checkbox:checked').length > 0) {
                    return true;
                } else {
                    bootbox.alert("Selecione um arquivo XML para esta operação!");
                    return false;
                }
            }
            
            function redirectFind(patch) {
                var finalURL = '';
                if (patch !== '') {
                    finalURL = '?patch=' + patch;
                }
                window.location = 'Xml-Arquivos.php' + finalURL.replace(/\//g, "_");
            }
            
        <?php if ($imprimir == 1) { ?>
            $(window).load(function () {
                $(".body").css("margin-left", 0);
                $("#example_length").remove();
                $("#example_filter").remove();
                $("#example_paginate").remove();
                $("#formPQ > th").css("background-image", "none");
            });
        <?php } ?>
        </script>
        <?php odbc_close($con); ?>
    </body>
</html>