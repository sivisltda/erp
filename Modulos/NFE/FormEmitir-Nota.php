<?php include("form/FormEmitir-Nota.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Nota Fiscal Eletrônica</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">
        <link href="../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script src="../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
        <script src="../../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <style>
            .inputDireito {
                text-align:right;
            }
            #formulario {
                float: left;
                width: 100%;
            }
            .linha {
                float: left;
                width: 100%;
                display: block;
                padding-top: 10px;
                padding-bottom: 10px;
            }            
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header" style="display: inline-block;">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1 style="margin-top:8px">Emissão de Nota Fiscal</h1>
                        <div style="text-align: right; margin-top: 28px;<?php
                        if ($tpAmb == "2") {
                            echo "color: green";
                        } else {
                            echo "color: #ff7834";
                        }
                        ?>;font-weight: bold;"><?php
                             if ($tpAmb == "2") {
                                 echo "Ambiente de Homologação";
                             } else {
                                 echo "Ambiente de Produção";
                             }
                             ?></div>
                    </div>
                    <div class="row-fluid">
                        <form action="FormEmitir-Nota.php?<?php echo $FinalUrl; ?>" method="POST" name="frmEnviaDados" id="frmEnviaDados">
                            <div class="span12">
                                <div class="block">
                                    <div id="formulario">
                                        <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                                        <div style="width:23%; float:left">
                                            <span>Status:</span>
                                            <span style="width:100%; float:left">
                                                <input type="text" name="txtStatus" id="txtStatus" class="input-medium" value="<?php echo $status_nfe; ?>" disabled>
                                            </span>
                                        </div>
                                        <div style="width:43%; float:left; margin-left:1%">
                                            <span>Chave de Acesso:</span>
                                            <span style="width:100%; float:left">
                                                <input type="text" name="txtChave" id="txtChave" class="input-medium" value="<?php echo $chave_nfe; ?>" readonly>
                                            </span>
                                        </div>
                                        <div style="width:10%; float:left; margin-left:1%">
                                            <span>Número:</span>
                                            <span>
                                                <input type="text" name="txtNumGlobal" id="txtNumGlobal" maxlength="9" class="input-medium" value="<?php echo $numero_nfe; ?>" disabled>
                                            </span>
                                        </div>
                                        <div style="width:10%; float:left; margin-left:1%">
                                            <span>Protocolo:</span>
                                            <span>
                                                <input type="text" name="txtProtocolo" id="txtProtocolo" maxlength="9" class="input-medium" value="<?php echo $nprot; ?>" readonly>
                                            </span>
                                        </div>
                                        <div style="width:10%; float:left; margin-left:1%">
                                            <span>Versão XML:</span>
                                            <span>
                                                <input type="text" name="txtVerXML" id="txtVerXML" maxlength="4" class="input-medium" value="<?php echo $vxml_nfe; ?>" disabled>
                                            </span>
                                        </div>
                                        <div style="clear:both"></div>
                                        <div class="data-fluid tabbable" style="margin-top:10px; margin-bottom:15px">
                                            <ul class="nav nav-tabs" style="margin-bottom:0px">
                                                <li class="<?php echo $active1; ?>"><a href="#tab1" data-toggle="tab"><b>Dados da NFe</b></a></li>
                                                <li><a href="#tab6" data-toggle="tab"><b>Emitente</b></a></li>
                                                <li><a href="#tab2" data-toggle="tab"><b>Destinatário</b></a></li>
                                                <li><a href="#tab4" data-toggle="tab"><b>Produtos</b></a></li>
                                                <li><a href="#tab5" data-toggle="tab"><b>Totais</b></a></li>
                                                <li><a href="#tab3" data-toggle="tab"><b>Transporte</b></a></li>
                                            </ul>
                                            <div class="tab-content" style="overflow:hidden; border:1px solid #ddd; border-top:0px; padding-top:10px; background:#F6F6F6">
                                                <div class="tab-pane <?php echo $active1; ?>" id="tab1" style="margin-bottom:5px">
                                                    <div style="width:10%; float:left">
                                                        <span>Modelo:</span>
                                                        <span style="width:100%; float:left">
                                                            <select name="txtModelo" id="txtModelo" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                                <option value="55" <?php
                                                                if ($modelo_nfe == "55") {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>55</option>
                                                                <option value="65" <?php
                                                                if ($modelo_nfe == "65") {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>65</option>
                                                            </select>                                                            
                                                        </span>
                                                    </div>
                                                    <div style="width:7%; float:left; margin-left:1%">
                                                        <span>* Série:</span>
                                                        <span style="width:100%; float:left">
                                                            <input type="text" name="txtSerie" id="txtSerie" maxlength="3" class="input-medium" style="text-align:right" value="<?php echo $serie_nfe; ?>" <?php echo $disabled; ?>>
                                                        </span>
                                                    </div>
                                                    <div style="width:15%; float:left; margin-left:1%">
                                                        <span>* Número NFe:</span>
                                                        <span style="width:100%; float:left">
                                                            <input type="text" name="txtNumNfe" id="txtNumNfe" maxlength="9" class="input-medium" style="text-align:right" value="<?php echo $numero_nfe; ?>" <?php echo $disabled; ?>>
                                                        </span>
                                                    </div>
                                                    <div style="width:13%; float:left; margin-left:1%">
                                                        <span>* Data de Emissão:</span>
                                                        <span>
                                                            <input type="text" name="txtDtEmissao" id="txtDtEmissao" class="datepicker inputCenter" value="<?php echo $data_emissao; ?>" <?php echo $disabled; ?>>
                                                        </span>
                                                    </div>
                                                    <div style="width:12%; float:left; margin-left:1%">
                                                        <span>Hora Emissão:</span>
                                                        <span>
                                                            <input type="text" name="txtHrSaiEmi" id="txtHrSaiEmi" class="inputCenter" value="<?php echo $hora_saiemi; ?>" <?php echo $disabled; ?>>
                                                        </span>
                                                    </div>
                                                    <div style="width:18%; float:left; margin-left:1%">
                                                        <span>* Tipo de Documento:</span>
                                                        <span>
                                                            <select name="txtTipoDoc" id="txtTipoDoc" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                                <option value="0" <?php
                                                                if ($tipo_doc == 0) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>0 - Entrada</option>
                                                                <option value="1" <?php
                                                                if ($tipo_doc == 1) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>1 - Saída</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:13%; float:left; margin-left:1%">
                                                        <span>* Código Numérico:</span>
                                                        <span>
                                                            <input type="text" name="txtCodNumerico" id="txtCodNumerico" maxlength="8" class="input-medium" style="text-align:right" value="<?php echo $cod_numerico; ?>" <?php echo $disabled; ?>>
                                                        </span>
                                                    </div>
                                                    <div style="width:5%; float:left; margin-left:1%">
                                                        <span>DV:</span>
                                                        <span>
                                                            <input type="text" name="txtDv" id="txtDv" class="input-medium" value="<?php echo $dv; ?>" disabled>
                                                        </span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:10%; float:left">
                                                        <span>Data Saída/Entrada:</span>
                                                        <span>
                                                            <input type="text" name="txtDtSaiEnt" id="txtDtSaiEnt" class="datepicker inputCenter" value="<?php echo $data_saient; ?>" <?php echo $disabled; ?>>
                                                        </span>
                                                    </div>
                                                    <div style="width:10%; float:left; margin-left:1%">
                                                        <span>Hora Saída/Entrada:</span>
                                                        <span>
                                                            <input type="text" name="txtHrSaiEnt" id="txtHrSaiEnt" class="inputCenter" value="<?php echo $hora_saient; ?>" <?php echo $disabled; ?>>
                                                        </span>
                                                    </div>
                                                    <div style="width:12%; float:left; margin-left:1%">
                                                        <span>* Forma de Pagamento:</span>
                                                        <span>
                                                            <select name="txtForPgto" id="txtForPgto" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                                <option value="0" <?php
                                                                if ($for_pgto == 0) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>0 - À Vista</option>
                                                                <option value="1" <?php
                                                                if ($for_pgto == 1) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>1 - À Prazo</option>
                                                                <option value="2" <?php
                                                                if ($for_pgto == 2) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>2 - Outros</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:14%; float:left; margin-left:1%">
                                                        <span>* Detalhe do Pagamento:</span>
                                                        <span>
                                                            <select name="txtDetPgto" id="txtDetPgto" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                                <option value="1" <?php if ($det_pgto == 1) { echo "SELECTED";}?>>01 - Dinheiro</option>
                                                                <option value="2" <?php if ($det_pgto == 2) { echo "SELECTED";}?>>02 - Cheque</option>
                                                                <option value="3" <?php if ($det_pgto == 3) { echo "SELECTED";}?>>03 - Cartão de Crédito</option>
                                                                <option value="4" <?php if ($det_pgto == 4) { echo "SELECTED";}?>>04 - Cartão de Débito</option>
                                                                <option value="5" <?php if ($det_pgto == 5) { echo "SELECTED";}?>>05 - Crédito Loja</option>
                                                                <option value="10" <?php if ($det_pgto == 10) { echo "SELECTED";}?>>10 - Vale Alimentação</option>
                                                                <option value="11" <?php if ($det_pgto == 11) { echo "SELECTED";}?>>11 - Vale Refeição</option>
                                                                <option value="12" <?php if ($det_pgto == 12) { echo "SELECTED";}?>>12 - Vale Presente</option>
                                                                <option value="13" <?php if ($det_pgto == 13) { echo "SELECTED";}?>>13 - Vale Combustível</option>
                                                                <option value="15" <?php if ($det_pgto == 15) { echo "SELECTED";}?>>15 - Boleto Bancário</option>
                                                                <option value="90" <?php if ($det_pgto == 90) { echo "SELECTED";}?>>90 - Sem Pagamento</option>
                                                                <option value="99" <?php if ($det_pgto == 99) { echo "SELECTED";}?>>99 - Outros</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:15%; float:left; margin-left:1%">
                                                        <span>* Forma de Emissão:</span>
                                                        <span>
                                                            <select name="txtForEmis" id="txtForEmis" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                                <option value="1" <?php
                                                                if ($for_emis == 1) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Normal</option>
                                                                <option value="2" <?php
                                                                if ($for_emis == 2) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Contingência FS</option>
                                                                <option value="3" <?php
                                                                if ($for_emis == 3) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Contingência com SCAN</option>
                                                                <option value="4" <?php
                                                                if ($for_emis == 4) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Contingência via DPEC</option>
                                                                <option value="5" <?php
                                                                if ($for_emis == 5) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Contingência FS-DA</option>
                                                                <option value="6" <?php
                                                                if ($for_emis == 6) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Contingência SVC-AN</option>
                                                                <option value="7" <?php
                                                                if ($for_emis == 7) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Contingência SVC-RS</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:17%; float:left; margin-left:1%">
                                                        <span>* Finalidade de Emissão:</span>
                                                        <span>
                                                            <select name="txtFinEmis" id="txtFinEmis" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                                <option value="1" <?php
                                                                if ($fin_emis == 1) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>1 - NFe Normal</option>
                                                                <option value="2" <?php
                                                                if ($fin_emis == 2) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>2 - NFe Complementar</option>
                                                                <option value="3" <?php
                                                                if ($fin_emis == 3) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>3 - NFe de Ajuste</option>
                                                                <option value="4" <?php
                                                                if ($fin_emis == 4) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>4 - Devolução de Mercadoria</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:16%; float:left; margin-left:1%">
                                                        <span>* Tipo de Impressão:</span>
                                                        <span>
                                                            <select name="txtTipoImp" id="txtTipoImp" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                                <option value="0" <?php
                                                                if ($tipo_print == 0) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>0 - Sem Geração de DANFE</option>
                                                                <option value="1" <?php
                                                                if ($tipo_print == 1) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>1 - DANFE Normal, Retrato</option>
                                                                <option value="2" <?php
                                                                if ($tipo_print == 2) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>2 - DANFE Normal, Paisagem</option>
                                                                <option value="3" <?php
                                                                if ($tipo_print == 3) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>3 - DANFE Simplificado</option>
                                                                <option value="4" <?php
                                                                if ($tipo_print == 4) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>4 - DANFE NFC-e</option>
                                                                <option value="5" <?php
                                                                if ($tipo_print == 5) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>5 - DANFE NFC-e em mensagem eletrônica</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:10%; float:left">
                                                        <span>* Consumidor Final:</span>
                                                        <span>
                                                            <select name="txtConFinal" id="txtConFinal" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                                <option value="0" <?php
                                                                if ($conFinal == 0) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>0 - NÃO</option>
                                                                <option value="1" <?php
                                                                if ($conFinal == 1) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>1 - SIM</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:19%; float:left; margin-left:1%">
                                                        <span>* Destino da Operação:</span>
                                                        <span>
                                                            <select name="txtDestOper" id="txtDestOper" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                                <option value="1" <?php
                                                                if ($destOper == 1) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>1 - Operação Interna</option>
                                                                <option value="2" <?php
                                                                if ($destOper == 2) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>2 - Operação Interestadual</option>
                                                                <option value="3" <?php
                                                                if ($destOper == 3) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>3 - Operação com Exterior</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:18%; float:left; margin-left:1%">
                                                        <span>* Tipo Atendimento:</span>
                                                        <span>
                                                            <select name="txtTipoAtend" id="txtTipoAtend" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                                <option value="0" <?php
                                                                if ($tipoAten == 0) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>0 - Não se Aplica</option>
                                                                <option value="1" <?php
                                                                if ($tipoAten == 1) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>1 - Operação Presencial</option>
                                                                <option value="2" <?php
                                                                if ($tipoAten == 2) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>2 - Operação Não Presencial, pela Internet</option>
                                                                <option value="3" <?php
                                                                if ($tipoAten == 3) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>3 - Operação Não Presencial, pela Teleatendimento</option>                                                                
                                                                <option value="4" <?php
                                                                if ($tipoAten == 4) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>4 - NFC-e em operação com entrega a domicílio</option>
                                                                <option value="5" <?php
                                                                if ($tipoAten == 5) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>5 - Operação presencial, fora do estabelecimento (em domicílio);</option>                                                                
                                                                <option value="9" <?php
                                                                if ($tipoAten == 9) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>9 - Operação Não Presencial, Outros</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:22%; float:left; margin-left:1%">
                                                        <span>* Natureza da Operação:</span>
                                                        <span>
                                                            <input type="text" name="txtNtOpera" id="txtNtOpera" class="input-medium" value="<?php echo $nat_operacao; ?>" <?php echo $disabled; ?>>
                                                        </span>
                                                    </div>
                                                    <div style="width:10%; float:left; margin-left:1%">
                                                        <span>* Estado:</span>
                                                        <span>
                                                            <select name="txtEstadoNFE" id="txtEstadoNFE" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                                <option value="">UF</option>
                                                                <?php
                                                                $query = "";
                                                                if ($disabled == 'disabled' && is_numeric($estadoNFE)) {
                                                                    $query = " and estado_codigo = " . $estadoNFE;
                                                                }
                                                                $cur = odbc_exec($con, "select estado_codigo,estado_sigla from tb_estados where estado_codigo > 0 " . $query . " order by estado_nome") or die(odbc_errormsg());
                                                                while ($RFP = odbc_fetch_array($cur)) {
                                                                    ?>
                                                                    <option value="<?php echo $RFP['estado_codigo'] ?>"<?php
                                                                    if (!(strcmp($RFP['estado_codigo'], $estadoNFE))) {
                                                                        echo "SELECTED";
                                                                    }
                                                                    ?>><?php echo utf8_encode($RFP['estado_sigla']) ?></option>
                                                                        <?php } ?>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:16%; float:left; margin-left:1%">
                                                        <span>* Cidade:</span>
                                                        <span><span id="carregandoNFE" name="carregandoNFE" style="color:#666; display:none">Aguarde, carregando...</span>
                                                            <select name="txtCidadeNFE" id="txtCidadeNFE" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                                <option value="" >Selecione a Cidade</option>
                                                                <?php
                                                                $query = "";
                                                                if ($disabled == 'disabled') {
                                                                    $query = " and cidade_codigo = " . $cidadeNFE;
                                                                }
                                                                if ($estadoNFE != '') {
                                                                    $sql = "select cidade_codigo,cidade_nome from tb_cidades where cidade_codigo > 0 " . $query . " AND cidade_codigoEstado = " . $estadoNFE . " ORDER BY cidade_nome";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                                        ?>
                                                                        <option value="<?php echo $RFP['cidade_codigo'] ?>"<?php
                                                                        if (!(strcmp($RFP['cidade_codigo'], $cidadeNFE))) {
                                                                            echo "SELECTED";
                                                                        }
                                                                        ?>><?php echo utf8_encode($RFP['cidade_nome']) ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <input name="txtcontigencia_hora" id="txtcontigencia_hora" value="<?php echo $contigencia_hora; ?>" type="hidden"/>
                                                    <input name="txtcontigencia_motivo" id="txtcontigencia_motivo" value="<?php echo $contigencia_motivo; ?>" type="hidden"/>
                                                    <div style="clear:both"></div>
                                                </div>
                                                <div class="tab-pane" id="tab6" style="margin-bottom:5px">
                                                    <div style="width:49%; float:left; margin-left:0.5%">
                                                        <?php
                                                        $EmitCNPJ = '';
                                                        $EmitRazao = '';
                                                        $EmitFantasia = '';
                                                        $EmitIE = '';
                                                        $EmitIEST = '';
                                                        $EmitIM = '';
                                                        $EmitCNAE = '';
                                                        $EmitRTrib = '';
                                                        $EmitLogradouro = '';
                                                        $EmitNumero = '';
                                                        $EmitComplemento = '';
                                                        $EmitCEP = '';
                                                        $EmitBairro = '';
                                                        $EmitEstado = '';
                                                        $EmitCidade = '';
                                                        ?>
                                                        <span>Emitente:</span>
                                                        <span>
                                                            <select name="txtEmitente" id="txtEmitente" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                                <option value="">--Selecione--</option>
                                                                <?php
                                                                $cur = odbc_exec($con, "select * from sf_fornecedores_despesas where tipo in ('M') order by razao_social") or die(odbc_errormsg());
                                                                while ($RFP = odbc_fetch_array($cur)) {
                                                                    ?>
                                                                    <option value="<?php echo $RFP['id_fornecedores_despesas'] ?>"<?php
                                                                    if (!(strcmp($RFP['id_fornecedores_despesas'], $Cod_Emitente))) {
                                                                        $EmitCNPJ = utf8_encode($RFP['cnpj']);
                                                                        $EmitRazao = utf8_encode($RFP['razao_social']);
                                                                        $EmitFantasia = utf8_encode($RFP['nome_fantasia']);
                                                                        $EmitIE = utf8_encode($RFP['inscricao_estadual']);
                                                                        $EmitIEST = utf8_encode($RFP['ie_tributario']);
                                                                        $EmitIM = utf8_encode($RFP['inscricao_municipal']);
                                                                        $EmitCNAE = utf8_encode($RFP['cnae_fiscal']);
                                                                        $EmitRTrib = utf8_encode($RFP['regime_tributario']);
                                                                        $EmitLogradouro = utf8_encode($RFP['endereco']);
                                                                        $EmitNumero = utf8_encode($RFP['numero']);
                                                                        $EmitComplemento = utf8_encode($RFP['complemento']);
                                                                        $EmitCEP = utf8_encode($RFP['cep']);
                                                                        $EmitBairro = utf8_encode($RFP['bairro']);
                                                                        $EmitEstado = utf8_encode($RFP['estado']);
                                                                        $EmitCidade = utf8_encode($RFP['cidade']);
                                                                        echo "SELECTED";
                                                                    }
                                                                    ?>><?php
                                                                                echo utf8_encode($RFP['razao_social']);
                                                                                if ($RFP['cnpj'] != "") {
                                                                                    echo " (" . utf8_encode($RFP['cnpj']) . ")";
                                                                                }
                                                                                ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:49%; float:left; margin-left:1%">
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:24%; float:left; margin-left:0.5%">
                                                        <span>* CNPJ:</span>
                                                        <span><input type="text" name="txtEmitCNPJ" id="txtEmitCNPJ" value="<?php echo $EmitCNPJ; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:74%; float:left; margin-left:1%">
                                                        <span>* Razão Social:</span>
                                                        <span><input type="text" name="txtEmitRazaoSocial" id="txtEmitRazaoSocial" value="<?php echo $EmitRazao; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:54%; float:left; margin-left:0.5%">
                                                        <span>Nome Fantasia:</span>
                                                        <span><input type="text" name="txtEmitNomeFantasia" id="txtEmitNomeFantasia" value="<?php echo $EmitFantasia; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:22%; float:left; margin-left:0.5%">
                                                        <span>* Inscrição Estadual:</span>
                                                        <span><input type="text" name="txtEmitInscricaoEstadual" id="txtEmitInscricaoEstadual" value="<?php echo $EmitIE; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:22%; float:left; margin-left:0.5%">
                                                        <span>Insc. Est. do Subst. Tributário:</span>
                                                        <span><input type="text" name="txtEmitInscEstSubTrib" id="txtEmitInscEstSubTrib" value="<?php echo $EmitIEST; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:33%; float:left; margin-left:0.5%">
                                                        <span>Inscrição Municipal:</span>
                                                        <span><input type="text" name="txtEmitInscMunicipal" id="txtEmitInscMunicipal" value="<?php echo $EmitIM; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:32%; float:left; margin-left:0.5%">
                                                        <span>CNAE:</span>
                                                        <span><input type="text" name="txtEmitCNAE" id="txtEmitCNAE" value="<?php echo $EmitCNAE; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:32.5%; float:left; margin-left:1%">
                                                        <span>* Regime Tributário:</span>
                                                        <span>
                                                            <select name="txtEmitRegTrib" id="txtEmitRegTrib" class="select" style="width:100%" disabled>
                                                                <option value="S" <?php
                                                                if ($EmitRTrib == "1") {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Simples Nacional</option>
                                                                <option value="E" <?php
                                                                if ($EmitRTrib == "2") {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Simples Nacional - Excesso de Sublimite de Receita Bruta</option>
                                                                <option value="R" <?php
                                                                if ($EmitRTrib == "3") {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Regime Normal</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:42%; float:left; margin-left:0.5%">
                                                        <span>* Logradouro:</span>
                                                        <span><input type="text" name="txtEmitEndereco" id="txtEmitEndereco" value="<?php echo $EmitLogradouro; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:7%; float:left; margin-left:1%">
                                                        <span>* Número:</span>
                                                        <span><input type="text" name="txtEmitNumero" id="txtEmitNumero" value="<?php echo $EmitNumero; ?>" class="input-medium" maxlength="6" disabled/></span>
                                                    </div>
                                                    <div style="width:34%; float:left; margin-left:1%">
                                                        <td align="right">Complemento:</td>
                                                        <td><input type="text" name="txtEmitComplemento" id="txtEmitComplemento" value="<?php echo $EmitComplemento; ?>" class="input-medium" maxlength="20" disabled/></td>
                                                    </div>
                                                    <div style="width:13%; float:left; margin-left:1%">
                                                        <span>* CEP:</span>
                                                        <span><input type="text" name="txtEmitCep" id="txtEmitCep" value="<?php echo $EmitCEP; ?>" class="input-medium" maxlength="9" disabled/></span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:32%; float:left; margin-left:0.5%">
                                                        <span>* Bairro:</span>
                                                        <span><input type="text" name="txtEmitBairro" id="txtEmitBairro" value="<?php echo $EmitBairro; ?>" class="input-medium" maxlength="20" disabled/></span>
                                                    </div>
                                                    <div style="width:32.5%; float:left; margin-left:1%">
                                                        <span>* Estado:</span>
                                                        <span>
                                                            <select name="txtEmitEstado" id="txtEmitEstado" class="select" style="width:100%" disabled>
                                                                <option value="null" >Selecione o estado</option>
                                                                <?php
                                                                $query = "";
                                                                if ($disabled == 'disabled' && is_numeric($EmitEstado)) {
                                                                    $query = " and estado_codigo = " . $EmitEstado;
                                                                }
                                                                $cur = odbc_exec($con, "select estado_codigo,estado_nome from tb_estados where estado_codigo > 0 " . $query . " order by estado_nome") or die(odbc_errormsg());
                                                                while ($RFP = odbc_fetch_array($cur)) {
                                                                    ?>
                                                                    <option value="<?php echo $RFP['estado_codigo'] ?>"<?php
                                                                    if (!(strcmp($RFP['estado_codigo'], $EmitEstado))) {
                                                                        echo "SELECTED";
                                                                    }
                                                                    ?>><?php echo utf8_encode($RFP['estado_nome']) ?></option>
                                                                        <?php } ?>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:32.5%; float:left; margin-left:1%">
                                                        <span>* Cidade:</span>
                                                        <span><span id="carregandoEmit" name="carregandoEmit" style="color:#666; display:none">Aguarde, carregando...</span>
                                                            <select name="txtEmitCidade" id="txtEmitCidade" class="select" style="width:100%" disabled>
                                                                <option value="null" >Selecione a cidade</option>
                                                                <?php
                                                                $query = "";
                                                                if ($disabled == 'disabled') {
                                                                    $query = " and cidade_codigo = " . $EmitCidade;
                                                                }
                                                                if ($EmitEstado != '') {
                                                                    $sql = "select cidade_codigo,cidade_nome from tb_cidades where cidade_codigo > 0 " . $query . " AND cidade_codigoEstado = " . $EmitEstado . " ORDER BY cidade_nome";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                                        ?>
                                                                        <option value="<?php echo $RFP['cidade_codigo'] ?>"<?php
                                                                        if (!(strcmp($RFP['cidade_codigo'], $EmitCidade))) {
                                                                            echo "SELECTED";
                                                                        }
                                                                        ?>><?php echo utf8_encode($RFP['cidade_nome']) ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                </div>
                                                <div class="tab-pane" id="tab2" style="margin-bottom:5px">
                                                    <div style="width:49%; float:left; margin-left:0.5%">
                                                        <?php
                                                        $DestCNPJ = '';
                                                        $DestRazao = '';
                                                        $DestIE = '';
                                                        $DestIsento = '';
                                                        $DestSuframa = '';
                                                        $DestLogradouro = '';
                                                        $DestNumero = '';
                                                        $DestComplemento = '';
                                                        $DestCEP = '';
                                                        $DestBairro = '';
                                                        $DestEstado = '';
                                                        $DestCidade = '';
                                                        ?>
                                                        <span>Cliente:</span>
                                                        <span>
                                                            <?php
                                                            if (is_numeric($Cod_Destinatario)) {
                                                                $cur = odbc_exec($con, "select *, 
                                                                            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato,
                                                                            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = id_fornecedores_despesas) email_contato
                                                                         from sf_fornecedores_despesas where tipo in ('C','F','T') and id_fornecedores_despesas = " . $Cod_Destinatario . " order by razao_social") or die(odbc_errormsg());
                                                                while ($RFP = odbc_fetch_array($cur)) {
                                                                    $DestCNPJ = utf8_encode($RFP['cnpj']);
                                                                    $DestRazao = utf8_encode($RFP['razao_social']);
                                                                    $Nome_Destinatario = utf8_encode($RFP['razao_social']);
                                                                    if ($RFP['nome_fantasia'] != "") {
                                                                        $Nome_Destinatario = $Nome_Destinatario . " (" . utf8_encode($RFP['nome_fantasia']) . ")";
                                                                    }
                                                                    $DestIE = utf8_encode($RFP['inscricao_estadual']);
                                                                    $DestIsento = utf8_encode($RFP['regime_tributario']);
                                                                    $DestSuframa = utf8_encode($RFP['ie_tributario']);
                                                                    $DestMunicipal = utf8_encode($RFP['inscricao_municipal']);
                                                                    $DestEmail = utf8_encode($RFP['email_contato']);
                                                                    $DestFone = utf8_encode($RFP['telefone_contato']);
                                                                    $DestLogradouro = utf8_encode($RFP['endereco']);
                                                                    $DestNumero = utf8_encode($RFP['numero']);
                                                                    $DestComplemento = utf8_encode($RFP['complemento']);
                                                                    $DestCEP = utf8_encode($RFP['cep']);
                                                                    $DestBairro = utf8_encode($RFP['bairro']);
                                                                    $DestEstado = utf8_encode($RFP['estado']);
                                                                    $DestCidade = utf8_encode($RFP['cidade']);
                                                                }
                                                            }
                                                            ?>
                                                            <input name="txtFonecedor" id="txtFonecedor" value="<?php echo $Cod_Destinatario; ?>" type="hidden"/>
                                                            <input type="text" name="txtDesc" id="txtDesc" class="inputbox" value="<?php echo $Nome_Destinatario; ?>" size="10" <?php echo $disabled; ?> style="width: 100%"/>
                                                        </span>
                                                    </div>
                                                    <div style="width:49%; float:left; margin-left:1%">
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:24%; float:left; margin-left:0.5%">
                                                        <span>* CNPJ/CPF:</span>
                                                        <span><input type="text" name="txtCNPJ" id="txtCNPJ" value="<?php echo $DestCNPJ; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:74%; float:left; margin-left:1%">
                                                        <span>* Razão Social/Nome:</span>
                                                        <span><input type="text" name="txtRazaoSocial" id="txtRazaoSocial" value="<?php echo $DestRazao; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:18%; float:left; margin-left:0.5%">
                                                        <span>Tipo de Contribuinte:</span>
                                                        <span>
                                                            <select name="txtIsentoICMS" id="txtIsentoICMS" class="select" style="width:100%" disabled>
                                                                <option value="1" <?php
                                                                if ($DestIsento == 1) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Contribuinte do ICMS</option>
                                                                <option value="2" <?php
                                                                if ($DestIsento == 2) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Contribuinte ISENTO</option>
                                                                <option value="9" <?php
                                                                if ($DestIsento == 9) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Não Contribuinte</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:20%; float:left; margin-left:1%">
                                                        <span>Inscrição Estadual:</span>
                                                        <span><input type="text" name="txtInscricaoEstadual" id="txtInscricaoEstadual" value="<?php echo $DestIE; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:13%; float:left; margin-left:1%">
                                                        <span>Inscrição SUFRAMA:</span>
                                                        <span><input type="text" name="txtSUFRAMA" id="txtSUFRAMA" value="<?php echo $DestSuframa; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:14%; float:left; margin-left:1%">
                                                        <span>Inscrição Municipal:</span>
                                                        <span><input type="text" name="txtMunicipalDest" id="txtMunicipalDest" value="<?php echo $DestMunicipal; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:30%; float:left; margin-left:1%">
                                                        <span>E-mail:</span>
                                                        <span><input type="text" name="txtEmailDest" id="txtEmailDest" value="<?php echo $DestEmail; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:42%; float:left; margin-left:0.5%">
                                                        <span>* Logradouro:</span>
                                                        <span><input type="text" name="txtEndereco" id="txtEndereco" value="<?php echo $DestLogradouro; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:7%; float:left; margin-left:1%">
                                                        <span>* Número:</span>
                                                        <span><input type="text" name="txtNumero" id="txtNumero" value="<?php echo $DestNumero; ?>" class="input-medium" maxlength="6" disabled/></span>
                                                    </div>
                                                    <div style="width:34%; float:left; margin-left:1%">
                                                        <td align="right">Complemento:</td>
                                                        <td><input type="text" name="txtComplemento" id="txtComplemento" value="<?php echo $DestComplemento; ?>" class="input-medium" maxlength="20" disabled/></td>
                                                    </div>
                                                    <div style="width:13%; float:left; margin-left:1%">
                                                        <span>CEP:</span>
                                                        <span><input type="text" name="txtCep" id="txtCep" value="<?php echo $DestCEP; ?>" class="input-medium" maxlength="9" disabled/></span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:30%; float:left; margin-left:0.5%">
                                                        <span>* Bairro:</span>
                                                        <span><input type="text" name="txtBairro" id="txtBairro" value="<?php echo $DestBairro; ?>" class="input-medium" maxlength="20" disabled/></span>
                                                    </div>
                                                    <div style="width:19%; float:left; margin-left:1%">
                                                        <span>* Estado:</span>
                                                        <span>
                                                            <select name="txtEstado" id="txtEstado" class="select" style="width:100%" disabled>
                                                                <option value="null" >Selecione o estado</option>
                                                                <?php
                                                                $query = "";
                                                                if ($disabled == 'disabled' && is_numeric(($DestEstado))) {
                                                                    $query = " and estado_codigo = " . $DestEstado;
                                                                }
                                                                $cur = odbc_exec($con, "select estado_codigo,estado_nome from tb_estados where estado_codigo > 0 " . $query . " order by estado_nome") or die(odbc_errormsg());
                                                                while ($RFP = odbc_fetch_array($cur)) {
                                                                    ?>
                                                                    <option value="<?php echo $RFP['estado_codigo'] ?>"<?php
                                                                    if (!(strcmp($RFP['estado_codigo'], $DestEstado))) {
                                                                        echo "SELECTED";
                                                                    }
                                                                    ?>><?php echo utf8_encode($RFP['estado_nome']) ?></option>
                                                                        <?php } ?>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:32.5%; float:left; margin-left:1%">
                                                        <span>* Cidade:</span>
                                                        <span><span id="carregando" name="carregando" style="color:#666; display:none">Aguarde, carregando...</span>
                                                            <select name="txtCidade" id="txtCidade" class="select" style="width:100%" disabled>
                                                                <option value="null" >Selecione a cidade</option>
                                                                <?php
                                                                $query = "";
                                                                if ($disabled == 'disabled' && is_numeric($DestCidade)) {
                                                                    $query = " and cidade_codigo = " . $DestCidade;
                                                                }
                                                                if ($DestEstado != '') {
                                                                    $sql = "select cidade_codigo,cidade_nome from tb_cidades where cidade_codigo > 0 " . $query . " AND cidade_codigoEstado = " . $DestEstado . " ORDER BY cidade_nome";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                                        ?>
                                                                        <option value="<?php echo $RFP['cidade_codigo'] ?>"<?php
                                                                        if (!(strcmp($RFP['cidade_codigo'], $DestCidade))) {
                                                                            echo "SELECTED";
                                                                        }
                                                                        ?>><?php echo utf8_encode($RFP['cidade_nome']) ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:14.5%; float:left; margin-left:1%">
                                                        <span>Fone:</span>
                                                        <span><input type="text" name="txtFone" id="txtFone" value="<?php echo $DestFone; ?>" class="input-medium" maxlength="20" disabled/></span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                </div>
                                                <div class="tab-pane" id="tab4" style="margin-bottom:5px">
                                                    <table class="table" cellpadding="0" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th width="35%">Produtos:</th>
                                                                <th width="10%">Qtd:</th>
                                                                <th width="17.5%">Valor Unitário:</th>
                                                                <th width="17.5%">Valor Total:</th>
                                                                <th width="10%">CFOP:</th>
                                                                <th width="10%">Ação:</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <input name="txtConta" id="txtConta" type="hidden"/>
                                                                    <input type="text" name="txtDescConta" id="txtDescConta" class="inputbox" size="10" <?php echo $disabled; ?> style="width: 100%;"/>
                                                                </td>
                                                                <td><input name="txtQtdProduto" id="txtQtdProduto" <?php echo $disabled; ?> type="text" class="input-medium inputCenter" value="1" onBlur="prodValor()"/></td>
                                                                <td><input name="hidValorProduto" id="hidValorProduto" <?php echo $disabled; ?> type="text" class="input-medium inputCenter" value="<?php echo escreverNumero(0); ?>" onBlur="prodValor()"/></td>
                                                                <td><input name="txtValorProduto" id="txtValorProduto" type="text" class="input-medium inputCenter" value="<?php echo escreverNumero(0); ?>" disabled /></td>
                                                                <td>
                                                                    <select name="txtCfop" id="txtCfop" class="select" <?php echo $disabled; ?> style="width:100%" maxlength="100">
                                                                        <option value="">Selecione</option>
                                                                        <?php
                                                                        $cur = odbc_exec($con, "select id_nfe_cfop from sf_nfe_cfop") or die(odbc_errormsg());
                                                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                            <option value="<?php echo $RFP['id_nfe_cfop'] ?>"><?php echo utf8_encode($RFP['id_nfe_cfop']) ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </td>
                                                                <td><center><button class="btn btn-primary" id="for-block3" <?php echo $disabled; ?> type="button" onClick="novaLinha()">+ Adicionar</button></center></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <table class="table" style="margin-top:-7px" cellpadding="0" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th width="35%"></th>
                                                                <th width="10%"></th>
                                                                <th width="17.5%"></th>
                                                                <th width="17.5%"></th>
                                                                <th width="10%"></th>
                                                                <th width="10%"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tabela_produto">
                                                            <?php
                                                            $i = 0;
                                                            $totalp = 0;
                                                            $BaseCalc = 0;
                                                            $TotIcms = 0;
                                                            $CalcIcms = 0;
                                                            $TotIcmsSt = 0;
                                                            $TotProd = 0;
                                                            $TotFrete = 0;
                                                            $TotSeguro = 0;
                                                            $TotDesconto = 0;
                                                            $TotIi = 0;
                                                            $TotIpi = 0;
                                                            $Pis = 0;
                                                            $Cofins = 0;
                                                            $OutDespesas = 0;
                                                            $TotNota = 0;
                                                            $idTP = 0;
                                                            if ($PegaURL != "") {
                                                                $cur = odbc_exec($con, "select cod_item_nota id_item_venda,conta_movimento grupo,gc.descricao grupodesc,conta_produto produto,p.descricao produtodesc,
                                                                                        qtd_comercial quantidade,tot_val_bruto valor_total,icms_sit_trib,icms_origem,icms_sit_trib, 
                                                                                        cenario,ncm,codigo_interno,tot_frete, i.cfop, cenario
                                                                                        from sf_nfe_notas_itens i inner join sf_produtos p 
                                                                                        inner join sf_contas_movimento gc on p.conta_movimento = gc.id_contas_movimento
                                                                                        on p.conta_produto = i.cod_prod where p.tipo = 'P' and cod_nota = " . $PegaURL);
                                                                while ($RFP = odbc_fetch_array($cur)) {
                                                                    $BaseCalc = $BaseCalc + 0;
                                                                    $TotIcms = $TotIcms + $RFP['icms_sit_trib'];
                                                                    $CalcIcms = $CalcIcms + $RFP['icms_origem'];
                                                                    $TotIcmsSt = $TotIcmsSt + $RFP['icms_sit_trib'];
                                                                    $TotProd = $TotProd + $RFP['valor_total'];
                                                                    $TotFrete = $TotFrete + $RFP['tot_frete'];
                                                                    $TotSeguro = $TotSeguro + 0;
                                                                    $TotDesconto = $TotDesconto + $RFP['tot_desconto'];
                                                                    $TotIi = $TotIi + 0;
                                                                    $TotIpi = $TotIpi + 0;
                                                                    $Pis = $Pis; // + $RFP['pis_valor'];           //calcular PIS
                                                                    $Cofins = $Cofins; // + $RFP['cofins_valor'];  //calcular COFINS
                                                                    $OutDespesas = $OutDespesas + 0;
                                                                    $TotNota = $TotNota + $RFP['valor_total'];
                                                                    ?>
                                                                    <tr id='tabela_linha_<?php echo $i; ?>'>
                                                                        <td style="vertical-align: middle;">
                                                                            <input name='txtIP_<?php echo $i; ?>' id='txtIP_<?php echo $i; ?>' type='hidden' value='<?php echo $RFP['id_item_venda']; ?>'/>
                                                                            <input name='txtPD_<?php echo $i; ?>' id='txtPD_<?php echo $i; ?>' type='hidden' value='<?php echo $RFP['produto']; ?>'/>
                                                                            &nbsp;&nbsp;&nbsp;<span <?php
                                                                            if ($RFP['cenario'] == "" || $RFP['ncm'] == "" || $RFP['codigo_interno'] == "" || $RFP['cfop'] == "") {
                                                                                echo "style=\"color:red\"";
                                                                            }
                                                                            ?>><?php echo utf8_encode($RFP['produtodesc']); ?></span>
                                                                        </td>
                                                                        <td style="vertical-align: middle;" id='myVQ_<?php echo $i; ?>'><input name='txtVQ_<?php echo $i; ?>' id='txtVQ_<?php echo $i; ?>' type='hidden' value='<?php echo escreverNumero($RFP['quantidade'], 0, 0); ?>'/><center><?php echo escreverNumero($RFP['quantidade'], 0, 0); ?></center></td>
                                                                <td style='text-align: center; vertical-align: middle;'>
                                                                    <input name='txtVU_<?php echo $i; ?>' id='txtVU_<?php echo $i; ?>' type='hidden' value='<?php
                                                                    if ($RFP['quantidade'] > 0) {
                                                                        $valor_unitario = $RFP['valor_total'] / $RFP['quantidade'];
                                                                    } else {
                                                                        $valor_unitario = 0;
                                                                    }
                                                                    echo escreverNumero($valor_unitario);
                                                                    ?>'/><?php echo escreverNumero($valor_unitario); ?>
                                                                </td>
                                                                <td id='myVP_<?php echo $i; ?>' style='text-align: center;    vertical-align: middle;'>
                                                                    <input name='txtVP_<?php echo $i; ?>' id='txtVP_<?php echo $i; ?>' type='hidden' value='<?php
                                                                    $totalp = $totalp + $RFP['valor_total'];
                                                                    echo escreverNumero($RFP['valor_total']);
                                                                    ?>'/><?php echo escreverNumero($RFP['valor_total']); ?>
                                                                </td>
                                                                <td id='myCFOP_<?php echo $i; ?>' style='text-align: right;'>
                                                                    <input name='txtVCenario_<?php echo $i; ?>' id='txtVCenario_<?php echo $i; ?>' type='hidden' value='<?php echo $RFP['cenario']; ?>'/>
                                                                    <select name='txtVCfop_<?php echo $i; ?>' id='txtVCfop_<?php echo $i; ?>' class="select" <?php echo $disabled; ?> style="width:100%" maxlength="100">
                                                                        <option value="">Selecione</option>
                                                                        <?php
                                                                        $cur2 = odbc_exec($con, "select id_nfe_cfop from sf_nfe_cfop") or die(odbc_errormsg());
                                                                        while ($RFP2 = odbc_fetch_array($cur2)) {
                                                                            ?>
                                                                            <option value="<?php echo $RFP2['id_nfe_cfop'] ?>" <?php
                                                                            if (!(strcmp($RFP2['id_nfe_cfop'], $RFP['cfop']))) {
                                                                                echo "SELECTED";
                                                                            }
                                                                            ?>><?php echo utf8_encode($RFP2['id_nfe_cfop']) ?></option>
                                                                                <?php } ?>
                                                                    </select>
                                                                </td>
                                                                <td><center>
                                                                    <button class="btn btn-primary" id="for-block3" <?php echo $disabled; ?> type="button" onClick="AbrirBox(<?php echo $RFP['produto']; ?>)"><span class="ico-eye icon-white"></span></button>
                                                                    <button style="width: 39px;" <?php echo $disabled; ?> class='btn red' type='button' onClick="removeLinha('tabela_linha_<?php echo $i; ?>')">X</button>
                                                                </center>
                                                                </td>
                                                                </tr>
                                                                <?php
                                                                $i++;
                                                            }
                                                            $idTP = $i;
                                                        } else {
                                                            if (isset($_GET['vd']) && is_numeric($_GET['vd'])) {
                                                                $cur = odbc_exec($con, "select id_item_venda,conta_movimento grupo,gc.descricao grupodesc,conta_produto produto,cm.descricao produtodesc,
                                                                quantidade,valor_total,cenario, cfop, codigo_interno, ncm from sf_vendas_itens vi inner join sf_contas_movimento gc on vi.grupo = gc.id_contas_movimento inner join sf_produtos cm 
                                                                on cm.conta_produto = vi.produto where cm.tipo = 'P' and id_venda = '" . $_GET['vd'] . "'");
                                                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                    <tr id='tabela_linha_<?php echo $i; ?>'>
                                                                        <td style="vertical-align: middle;">
                                                                            <input name='txtIP_<?php echo $i; ?>' id='txtIP_<?php echo $i; ?>' type='hidden' value=''/>
                                                                            <input name='txtPD_<?php echo $i; ?>' id='txtPD_<?php echo $i; ?>' type='hidden' value='<?php echo $RFP['produto']; ?>'/>
                                                                            &nbsp;&nbsp;&nbsp;<span <?php
                                                                            if ($RFP['cenario'] == "" || $RFP['cfop'] == "" || $RFP['codigo_interno'] == "" || $RFP['ncm'] == "") {
                                                                                echo "style=\"color:red\"";
                                                                            }
                                                                            ?>><?php echo utf8_encode($RFP['produtodesc']); ?></span>
                                                                        </td>
                                                                        <td style="vertical-align: middle;" id='myVQ_<?php echo $i; ?>'><input name='txtVQ_<?php echo $i; ?>' id='txtVQ_<?php echo $i; ?>' type='hidden' value='<?php echo escreverNumero($RFP['quantidade'], 0, 0); ?>'/><center><?php echo escreverNumero($RFP['quantidade'], 0, 0); ?></center></td>
                                                                    <td style='text-align: center;vertical-align: middle;'>
                                                                        <input name='txtVU_<?php echo $i; ?>' id='txtVU_<?php echo $i; ?>' type='hidden' value='<?php
                                                                        if ($RFP['quantidade'] > 0) {
                                                                            $valor_unitario = $RFP['valor_total'] / $RFP['quantidade'];
                                                                        } else {
                                                                            $valor_unitario = 0;
                                                                        }
                                                                        echo escreverNumero($valor_unitario);
                                                                        ?>'/><?php echo escreverNumero($valor_unitario); ?>
                                                                    </td>
                                                                    <td id='myVP_<?php echo $i; ?>' style='text-align: center;vertical-align: middle;'>
                                                                        <input name='txtVP_<?php echo $i; ?>' id='txtVP_<?php echo $i; ?>' type='hidden' value='<?php
                                                                        $totalp = $totalp + $RFP['valor_total'];
                                                                        echo escreverNumero($RFP['valor_total']);
                                                                        ?>'/><?php echo escreverNumero($RFP['valor_total']); ?>
                                                                    </td>
                                                                    <td id='myCFOP_<?php echo $i; ?>' style='text-align: right;'>
                                                                        <input name='txtVCenario_<?php echo $i; ?>' id='txtVCenario_<?php echo $i; ?>' type='hidden' value='<?php echo $RFP['cenario']; ?>'/>
                                                                        <select name='txtVCfop_<?php echo $i; ?>' id='txtVCfop_<?php echo $i; ?>' class="select" <?php echo $disabled; ?> style="width:100%" maxlength="100">
                                                                            <option value="">Selecione</option>
                                                                            <?php
                                                                            $cur2 = odbc_exec($con, "select id_nfe_cfop from sf_nfe_cfop") or die(odbc_errormsg());
                                                                            while ($RFP2 = odbc_fetch_array($cur2)) {
                                                                                ?>
                                                                                <option value="<?php echo $RFP2['id_nfe_cfop'] ?>" <?php
                                                                                if (!(strcmp($RFP2['id_nfe_cfop'], $RFP['cfop']))) {
                                                                                    echo "SELECTED";
                                                                                }
                                                                                ?>><?php echo utf8_encode($RFP2['id_nfe_cfop']) ?></option>
                                                                                    <?php } ?>
                                                                        </select>
                                                                    </td>
                                                                    <td>
                                                                    <center>
                                                                        <button class="btn btn-primary" id="for-block3" <?php echo $disabled; ?> type="button" onClick="AbrirBox(<?php echo $RFP['produto']; ?>)"><span class="ico-eye icon-white"></span></button>
                                                                        <button style="width: 39px;" <?php echo $disabled; ?> class='btn red' type='button' onClick="removeLinha('tabela_linha_<?php echo $i; ?>')">X</button>
                                                                    </center>
                                                                    </td>
                                                                    </tr>
                                                                    <?php
                                                                    $i++;
                                                                }
                                                                $idTP = $i;
                                                            }
                                                        } ?>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr style="height: 20px;">
                                                                <td colspan="6" style="width:100%; background-color:#E9E9E9; margin:0; padding:8px;">
                                                                    <div id="txtTotalProd" style="width:200px; float:right; text-align:right; line-height:20px">
                                                                        Total sem Desconto: <?php echo escreverNumero($totalp, 1); ?><br>
                                                                        Valor do Desconto: <?php echo escreverNumero(($totalp * ($descontop / 100)), 1); ?><br>
                                                                        <b>Total com Desconto: <?php echo escreverNumero($totalp - ($totalp * ($descontop / 100)), 1); ?></b>
                                                                    </div>
                                                                    <div style="float:right">
                                                                        <span style="width:100%; display:block;">&nbsp;</span>
                                                                        <span style="width:100%; display:block;"><button class="btn btn-primary" id="bntProdDesconto" <?php echo $disabled; ?> type="button" style="width:35px; padding-bottom:3px; text-align:center"><span id="prod_sinal">%</span></button></span>
                                                                    </div>
                                                                    <div style="width:100px; float:right">
                                                                        <span style="width:100%; display:block;">Desconto:</span>
                                                                        <span style="width:100%; display:block;">
                                                                            <input name="txtProdDesconto" id="txtProdDesconto" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $descontop; ?>" onblur="somaTotal()"/>
                                                                            <input name="txtProdSinal" id="txtProdSinal" value="<?php echo $descontotp; ?>" type="hidden"/>
                                                                        </span>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>                                                    
                                                </div>
                                                <div class="tab-pane" id="tab5" style="margin-bottom:5px">
                                                    <div style="width:19%; float:left">
                                                        <span>* Base de Cálculo:</span>
                                                        <span><input type="text" name="txtBaseCalc" id="txtBaseCalc" value="<?php echo escreverNumero($BaseCalc); ?>" class="input-medium" style="text-align:right" disabled></span>
                                                    </div>
                                                    <div style="width:19%; float:left; margin-left:1%">
                                                        <span>* Total do ICMS:</span>
                                                        <span><input type="text" name="txtTotIcms" id="txtTotIcms" value="<?php echo escreverNumero($TotIcms); ?>" class="input-medium" style="text-align:right" disabled></span>
                                                    </div>
                                                    <div style="width:19%; float:left; margin-left:1%">
                                                        <span>* Base de Cálculo do ICMS ST:</span>
                                                        <span><input type="text" name="txtCalcIcms" id="txtCalcIcms" value="<?php echo escreverNumero($CalcIcms); ?>" class="input-medium" style="text-align:right" disabled></span>
                                                    </div>
                                                    <div style="width:19%; float:left; margin-left:1%">
                                                        <span>* Total do ICMS ST:</span>
                                                        <span><input type="text" name="txtTotIcmsSt" id="txtTotIcmsSt" value="<?php echo escreverNumero($TotIcmsSt); ?>" class="input-medium" style="text-align:right" disabled></span>
                                                    </div>
                                                    <div style="width:20%; float:left; margin-left:1%">
                                                        <span>* Total dos Produtos:</span>
                                                        <span><input type="text" name="txtTotProd" id="txtTotProd" value="<?php echo escreverNumero($TotProd); ?>" class="input-medium" style="text-align:right" disabled></span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:19%; float:left">
                                                        <span>* Total do Frete:</span>
                                                        <span><input type="text" name="txtTotFrete" id="txtTotFrete" value="<?php echo escreverNumero($TotFrete); ?>" class="input-medium" style="text-align:right" <?php echo $disabled; ?>></span>
                                                    </div>
                                                    <div style="width:19%; float:left; margin-left:1%">
                                                        <span>* Total do Seguro:</span>
                                                        <span><input type="text" name="txtTotSeguro" id="txtTotSeguro" value="<?php echo escreverNumero($TotSeguro); ?>" class="input-medium" style="text-align:right" disabled></span>
                                                    </div>
                                                    <div style="width:19%; float:left; margin-left:1%">
                                                        <span>* Total do Desconto:</span>
                                                        <span><input type="text" name="txtTotDesconto" id="txtTotDesconto" value="<?php echo escreverNumero($TotDesconto); ?>" class="input-medium" style="text-align:right" readonly></span>
                                                    </div>
                                                    <div style="width:19%; float:left; margin-left:1%">
                                                        <span>* Total do II:</span>
                                                        <span><input type="text" name="txtTotIi" id="txtTotIi" value="<?php echo escreverNumero($TotIi); ?>" class="input-medium" style="text-align:right" disabled></span>
                                                    </div>
                                                    <div style="width:20%; float:left; margin-left:1%">
                                                        <span>* Total do IPI:</span>
                                                        <span><input type="text" name="txtTotIpi" id="txtTotIpi" value="<?php echo escreverNumero($TotIpi); ?>" class="input-medium" style="text-align:right" disabled></span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:19%; float:left">
                                                        <span>* PIS:</span>
                                                        <span><input type="text" name="txtPis" id="txtPis" value="<?php echo escreverNumero($Pis); ?>" class="input-medium" style="text-align:right" disabled></span>
                                                    </div>
                                                    <div style="width:19%; float:left; margin-left:1%">
                                                        <span>* COFINS:</span>
                                                        <span><input type="text" name="txtCofins" id="txtCofins" value="<?php echo escreverNumero($Cofins); ?>" class="input-medium" style="text-align:right" disabled></span>
                                                    </div>
                                                    <div style="width:19%; float:left; margin-left:1%">
                                                        <span>* Outras Despesas:</span>
                                                        <span><input type="text" name="txtOutDespesas" id="txtOutDespesas" value="<?php echo escreverNumero($OutDespesas); ?>" class="input-medium" style="text-align:right" disabled></span>
                                                    </div>
                                                    <div style="width:19%; float:left; margin-left:1%">
                                                        <span>* Total da Nota:</span>
                                                        <span><input type="text" name="txtTotNota" id="txtTotNota" value="<?php echo escreverNumero(($TotNota + $TotFrete)); ?>" class="input-medium" style="text-align:right" <?php echo $disabled; ?>></span>
                                                    </div>
                                                    <div style="width:20%; float:left; margin-left:1%">
                                                        <span style="width:100%; display:block;">&nbsp;</span>
                                                        <span style="width:100%; display:block;">&nbsp;</span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                </div>
                                                <div class="tab-pane" id="tab3" style="margin-bottom:5px">
                                                    <div style="width:25%; float:left; margin-left:0.5%">
                                                        <span>Modalidade do Frete:</span>
                                                        <span>
                                                            <select name="txtModFrete" id="txtModFrete" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                                <option value="0" <?php
                                                                if ($Mod_Transp == 0) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>0 - Por conta do Emitente</option>
                                                                <option value="1" <?php
                                                                if ($Mod_Transp == 1) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>1 - Por conta do Destinatário</option>
                                                                <option value="2" <?php
                                                                if ($Mod_Transp == 2) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>2 - Por conta de Terceiros</option>
                                                                <option value="3" <?php
                                                                if ($Mod_Transp == 3) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>3 - Transporte próprio por conta do emitente</option>
                                                                <option value="4" <?php
                                                                if ($Mod_Transp == 4) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>4 - Transporte próprio por conta do destinatário</option>
                                                                <option value="9" <?php
                                                                if ($Mod_Transp == 9) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>9 - Sem Frete</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:41%; float:left; margin-left:1%">
                                                        <span>Transportadora:</span>
                                                        <span>
                                                            <select name="txtTranNome" id="txtTranNome"  style="width:100%" <?php
                                                            if ($Mod_Transp == 9) {
                                                                echo "disabled";
                                                            } else {
                                                                echo $disabled;
                                                            }
                                                            ?>>
                                                                <option value="null">--Selecione--</option>
                                                                <?php
                                                                $query = "";
                                                                if ($disabled == 'disabled') {
                                                                    $query = " and id_fornecedores_despesas = '" . $Cod_Transp . "' ";
                                                                }
                                                                $cur = odbc_exec($con, "select id_fornecedores_despesas,razao_social from sf_fornecedores_despesas where tipo = 'T' " . $query . "") or die(odbc_errormsg());
                                                                while ($RFP = odbc_fetch_array($cur)) {
                                                                    ?>
                                                                    <option value="<?php echo $RFP['id_fornecedores_despesas'] ?>"<?php
                                                                    if (!(strcmp($RFP['id_fornecedores_despesas'], $Cod_Transp))) {
                                                                        echo "SELECTED";
                                                                    }
                                                                    ?>><?php echo utf8_encode($RFP['razao_social']) ?></option>
                                                                        <?php } ?>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:15%; float:left; margin-left:1%">
                                                        <span>Placa do Veículo:</span>
                                                        <span>
                                                            <input type="text" name="txtTranPlaca" id="txtTranPlaca" value="<?php echo $placa_veiculo; ?>" class="input-medium" <?php
                                                            if ($Mod_Transp == 9) {
                                                                echo "disabled";
                                                            } else {
                                                                echo $disabled;
                                                            }
                                                            ?>>
                                                        </span>
                                                    </div>
                                                    <div style="width:15%; float:left; margin-left:1%">
                                                        <span>Estado do Veículo:</span>
                                                        <span>
                                                            <select name="txtTranEst" id="txtTranEst"  style="width:100%" <?php
                                                            if ($Mod_Transp == 9) {
                                                                echo "disabled";
                                                            } else {
                                                                echo $disabled;
                                                            }
                                                            ?>>
                                                                <option value="null">--Selecione--</option>
                                                                <?php
                                                                $query = "";
                                                                if ($disabled == 'disabled') {
                                                                    $query = " and estado_codigo = '" . $trans_estado . "'";
                                                                }
                                                                $cur = odbc_exec($con, "select estado_codigo,estado_nome from tb_estados where estado_codigo > 0 " . $query . " order by estado_nome") or die(odbc_errormsg());
                                                                while ($RFP = odbc_fetch_array($cur)) {
                                                                    ?>
                                                                    <option value="<?php echo $RFP['estado_codigo'] ?>"<?php
                                                                    if (!(strcmp($RFP['estado_codigo'], $trans_estado))) {
                                                                        echo "SELECTED";
                                                                    }
                                                                    ?>><?php echo utf8_encode($RFP['estado_nome']) ?></option>
                                                                        <?php } ?>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:12%; float:left; margin-left:0.5%">
                                                        <span>Reboque:</span>
                                                        <span>
                                                            <select name="txtRebNome" id="txtRebNome"  style="width:100%" <?php
                                                            if ($Mod_Transp == 9) {
                                                                echo "disabled";
                                                            } else {
                                                                echo $disabled;
                                                            }
                                                            ?>>
                                                                <option value="N" <?php
                                                                if ($reboque == "N") {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Não</option>
                                                                <option value="S" <?php
                                                                if ($reboque == "S") {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Sim</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:15%; float:left; margin-left:1%">
                                                        <span>Placa do Reboque:</span>
                                                        <span>
                                                            <input type="text" name="txtRebPlaca" id="txtRebPlaca" value="<?php echo $placa_veiculo_reboque; ?>" class="input-medium" <?php
                                                            if ($Mod_Transp == 9) {
                                                                echo "disabled";
                                                            } else {
                                                                echo $disabled;
                                                            }
                                                            ?>>
                                                        </span>
                                                    </div>
                                                    <div style="width:15%; float:left; margin-left:1%">
                                                        <span>Estado do Reboque:</span>
                                                        <span>
                                                            <select name="txtRebEst" id="txtRebEst"  style="width:100%" <?php
                                                            if ($Mod_Transp == 9) {
                                                                echo "disabled";
                                                            } else {
                                                                echo $disabled;
                                                            }
                                                            ?>>
                                                                <option value="null">--Selecione--</option>
                                                                <?php
                                                                $query = "";
                                                                if ($disabled == 'disabled') {
                                                                    $query = " and estado_codigo = '" . $reboq_estado . "'";
                                                                }
                                                                $cur = odbc_exec($con, "select estado_codigo,estado_nome from tb_estados where estado_codigo > 0 " . $query . " order by estado_nome") or die(odbc_errormsg());
                                                                while ($RFP = odbc_fetch_array($cur)) {
                                                                    ?>
                                                                    <option value="<?php echo $RFP['estado_codigo'] ?>"<?php
                                                                    if (!(strcmp($RFP['estado_codigo'], $reboq_estado))) {
                                                                        echo "SELECTED";
                                                                    }
                                                                    ?>><?php echo utf8_encode($RFP['estado_nome']) ?></option>
                                                                        <?php } ?>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:15%; float:left; margin-left:1%">
                                                        <span>Tipo de Volume:</span>
                                                        <span>
                                                            <select name="txtTipoVol" id="txtTipoVol"  style="width:100%" <?php
                                                            if ($Mod_Transp == 9) {
                                                                echo "disabled";
                                                            } else {
                                                                echo $disabled;
                                                            }
                                                            ?>>
                                                                <option value="" >Selecione</option>
                                                                <option value="UN" <?php
                                                                if ($tipoVol == "UN") {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Unidade</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:12%; float:left; margin-left:1%">
                                                        <span>Volume:</span>
                                                        <span>
                                                            <input type="text" name="txtQntVol" id="txtQntVol" class="input-medium" style="text-align:right" value="<?php echo $Volume_Transp; ?>" <?php
                                                            if ($Mod_Transp == 9) {
                                                                echo "disabled";
                                                            } else {
                                                                echo $disabled;
                                                            }
                                                            ?>>
                                                        </span>
                                                    </div>
                                                    <div style="width:12%; float:left; margin-left:1%">
                                                        <span>Peso Líquido (Kg):</span>
                                                        <span>
                                                            <input type="text" name="txtPesoLiq" id="txtPesoLiq" class="input-medium" style="text-align:right" value="<?php echo $Peso_liq_Transp; ?>" <?php
                                                            if ($Mod_Transp == 9) {
                                                                echo "disabled";
                                                            } else {
                                                                echo $disabled;
                                                            }
                                                            ?>>
                                                        </span>
                                                    </div>
                                                    <div style="width:12%; float:left; margin-left:1%">
                                                        <span>Peso Bruto (Kg):</span>
                                                        <span>
                                                            <input type="text" name="txtPesoBru" id="txtPesoBru" class="input-medium" style="text-align:right" value="<?php echo $Peso_bru_Transp; ?>" <?php
                                                            if ($Mod_Transp == 9) {
                                                                echo "disabled";
                                                            } else {
                                                                echo $disabled;
                                                            }
                                                            ?>>
                                                        </span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:49%; float:left; margin-left:0.5%">
                                                        <?php
                                                        $End_entrega_logradouro = '';
                                                        $End_entrega_numero = '';
                                                        $End_entrega_complemento = '';
                                                        $End_entrega_cep = '';
                                                        $End_entrega_bairro = '';
                                                        $End_entrega_estado = '';
                                                        $End_entrega_cidade = '';
                                                        ?>
                                                        <span>Endereço de Entrega:</span>
                                                        <span>
                                                            <select name="txtEntrega" id="txtEntrega"  style="width:100%" <?php
                                                            if ($Mod_Transp == 9) {
                                                                echo "disabled";
                                                            } else {
                                                                echo $disabled;
                                                            }
                                                            ?>>
                                                                <option value="null">--Selecione--</option>
                                                                <?php
                                                                if ($Cod_Destinatario != "") {
                                                                    $cur = odbc_exec($con, "select id_endereco,nome_end_entrega, end_entrega, numero_entrega, complemento_entrega, bairro_entrega, estado_entrega, estado_nome, cidade_entrega, cidade_nome, cep_entrega, telefone_entrega, referencia_entrega
                                                                                                from dbo.sf_fornecedores_despesas_endereco_entrega f left join tb_estados e on f.estado_entrega = e.estado_codigo
                                                                                                left join tb_cidades c on f.cidade_entrega = c.cidade_codigo where fornecedores_despesas = " . $Cod_Destinatario) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                        <option value="<?php echo $RFP['id_endereco'] ?>"<?php
                                                                        if (!(strcmp($RFP['id_endereco'], $end_entrega))) {
                                                                            $End_entrega_logradouro = utf8_encode($RFP['end_entrega']);
                                                                            $End_entrega_numero = utf8_encode($RFP['numero_entrega']);
                                                                            $End_entrega_complemento = utf8_encode($RFP['complemento_entrega']);
                                                                            $End_entrega_cep = utf8_encode($RFP['cep_entrega']);
                                                                            $End_entrega_bairro = utf8_encode($RFP['bairro_entrega']);
                                                                            $End_entrega_estado = utf8_encode($RFP['estado_entrega']);
                                                                            $End_entrega_cidade = utf8_encode($RFP['cidade_entrega']);
                                                                            echo "SELECTED";
                                                                        }
                                                                        ?>><?php echo utf8_encode($RFP['nome_end_entrega']) ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:49%; float:left; margin-left:1%">
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:42%; float:left; margin-left:0.5%">
                                                        <span>Endereço:</span>
                                                        <span><input type="text" name="txtEnderecoEnt" id="txtEnderecoEnt" value="<?php echo $End_entrega_logradouro; ?>" class="input-medium" disabled/></span>
                                                    </div>
                                                    <div style="width:7%; float:left; margin-left:1%">
                                                        <span>Numero:</span>
                                                        <span><input type="text" name="txtNumeroEnt" id="txtNumeroEnt" value="<?php echo $End_entrega_numero; ?>" class="input-medium" maxlength="6" disabled/></span>
                                                    </div>
                                                    <div style="width:34%; float:left; margin-left:1%">
                                                        <td align="right">Complemento:</td>
                                                        <td><input type="text" name="txtComplementoEnt" id="txtComplementoEnt" value="<?php echo $End_entrega_complemento; ?>" class="input-medium" maxlength="20" disabled/></td>
                                                    </div>
                                                    <div style="width:13%; float:left; margin-left:1%">
                                                        <span>CEP:</span>
                                                        <span><input type="text" name="txtCepEnt" id="txtCepEnt" value="<?php echo $End_entrega_cep; ?>" class="input-medium" maxlength="9" disabled/></span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                    <div style="width:32%; float:left; margin-left:0.5%">
                                                        <span>Bairro:</span>
                                                        <span><input type="text" name="txtBairroEnt" id="txtBairroEnt" value="<?php echo $End_entrega_bairro; ?>" class="input-medium" maxlength="20" disabled/></span>
                                                    </div>
                                                    <div style="width:32.5%; float:left; margin-left:1%">
                                                        <span>Estado:</span>
                                                        <span>
                                                            <select name="txtEstadoEnt" id="txtEstadoEnt"  style="width:100%" disabled>
                                                                <option value="null" >Selecione o estado</option>
                                                                <?php
                                                                $query = "";
                                                                if ($disabled == 'disabled') {
                                                                    $query = " and estado_codigo = '" . $End_entrega_estado . "'";
                                                                }
                                                                $cur = odbc_exec($con, "select estado_codigo,estado_nome from tb_estados where estado_codigo > 0 " . $query . " order by estado_nome") or die(odbc_errormsg());
                                                                while ($RFP = odbc_fetch_array($cur)) {
                                                                    ?>
                                                                    <option value="<?php echo $RFP['estado_codigo'] ?>"<?php
                                                                    if (!(strcmp($RFP['estado_codigo'], $End_entrega_estado))) {
                                                                        echo "SELECTED";
                                                                    }
                                                                    ?>><?php echo utf8_encode($RFP['estado_nome']) ?></option>
                                                                        <?php } ?>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="width:32.5%; float:left; margin-left:1%">
                                                        <span>Cidade:</span>
                                                        <span><span id="carregando22" name="carregando22" style="color:#666; display:none">Aguarde, carregando...</span>
                                                            <select name="txtCidadeEnt" id="txtCidadeEnt"  style="width:100%" disabled>
                                                                <option value="null" >Selecione a cidade</option>
                                                                <?php
                                                                $query = "";
                                                                if ($End_entrega_estado != '') {
                                                                    if ($disabled == 'disabled') {
                                                                        $query = " and cidade_codigo = '" . $End_entrega_cidade . "'";
                                                                    }
                                                                    $sql = "select cidade_codigo,cidade_nome from tb_cidades where cidade_codigo > 0 " . $query . " AND cidade_codigoEstado = " . $End_entrega_estado . " ORDER BY cidade_nome";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                                        ?>
                                                                        <option value="<?php echo $RFP['cidade_codigo'] ?>"<?php
                                                                        if (!(strcmp($RFP['cidade_codigo'], $End_entrega_cidade))) {
                                                                            echo "SELECTED";
                                                                        }
                                                                        ?>><?php echo utf8_encode($RFP['cidade_nome']) ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="clear:both"></div>
                                        <div class="boxhead">
                                            <div class="boxtext">Informações Adicionais</div>
                                        </div>
                                        <div>
                                            <div style="width:49.5%; float:left">
                                                <span>Informações Adicionais Fisco:</span>
                                                <span>
                                                    <select name="optInfoFis" id="optInfoFis" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                        <option value="null">Selecione</option>
                                                        <?php
                                                        $query = "";
                                                        if ($disabled == '') {
                                                            $cur = odbc_exec($con, "select id_mensagem,nome_mensagem,descricao_mensagem from sf_nfe_mensagens") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['id_mensagem'] ?>" <?php
                                                                if (!(strcmp($RFP['id_mensagem'], $msg_comp_fisco))) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>><?php echo utf8_encode($RFP['nome_mensagem']) ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                    </select>
                                                </span>
                                            </div>
                                            <div style="width:49.5%; float:left; margin-left:1%">
                                                <span>Informações Adicionais Contribuinte:</span>
                                                <span>
                                                    <select name="optInfoCon" id="optInfoCon" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                        <option value="null">Selecione</option>
                                                        <?php
                                                        $query = "";
                                                        if ($disabled == '') {
                                                            $cur = odbc_exec($con, "select id_mensagem,nome_mensagem,descricao_mensagem from sf_nfe_mensagens") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['id_mensagem'] ?>" <?php
                                                                if (!(strcmp($RFP['id_mensagem'], $msg_comp))) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>><?php echo utf8_encode($RFP['nome_mensagem']) ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                    </select>
                                                </span>
                                            </div>
                                        </div>
                                        <div style="clear:both"></div>
                                        <div style="margin-top:5px">
                                            <div style="width:49.5%; float:left">
                                                <span>
                                                    <span id="carregando4" name="carregando4" style="color:#666; display:none">Aguarde, carregando...</span>
                                                    <textarea name="txtInfoFis" id="txtInfoFis" style="width:100%; height:100px" <?php echo $disabled; ?>><?php echo $Info_add_fisco; ?></textarea>
                                                </span>
                                            </div>
                                            <div style="width:49.5%; float:left; margin-left:1%">
                                                <span>
                                                    <span id="carregando5" name="carregando5" style="color:#666; display:none">Aguarde, carregando...</span>
                                                    <textarea name="txtInfoCon" id="txtInfoCon" style="width:100%; height:100px" <?php echo $disabled; ?>><?php echo $Info_add_complementar; ?></textarea>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span5" style="width:100%;">
                                    <div class="block">
                                        <div class="data-fluid">
                                            <table class="table" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td width="50%" colspan="2" style="background:none">
                                                            <span style="width:29%; display:block; float:left;"> * Responsável pela Autorização da NFE: </span>
                                                            <span style="width:15%; display:block; float:left; margin-left:1%;"> Status de Aprovação: </span>
                                                            <span style="width:33%; display:block; float:left; margin-left:1%;"> &nbsp;
                                                                <?php if ($status_nfe == "Autorizada") { ?>
                                                                    <label id="lblJust">Justificativa Cancelamento:</label>
                                                                <?php } ?>
                                                            </span>
                                                            <span style="width:29%; display:block; float:left;">
                                                                <select style="width:100%" name="txtDestinatario" id="txtDestinatario" class="select" <?php echo $disabled; ?>>
                                                                    <option value="null">--Selecione--</option>
                                                                    <?php
                                                                    $query = "";
                                                                    if ($disabled == 'disabled') {
                                                                        $query = " and login_user = '" . $destinatario . "'";
                                                                    }
                                                                    $cur = odbc_exec($con, "select login_user,nome from sf_usuarios where inativo = 0 and login_user != 'Admin' " . $query . " order by nome") or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                                        ?>
                                                                        <option value="<?php echo $RFP['login_user'] ?>"<?php
                                                                        if (!(strcmp($RFP['login_user'], $destinatario))) {
                                                                            echo "SELECTED";
                                                                        }
                                                                        ?>><?php echo utf8_encode($RFP['nome']) ?></option>
                                                                            <?php } ?>
                                                                </select>
                                                            </span>
                                                            <span style="width:15%; display:block; float:left; margin-left: 1%">
                                                                <input type="text" name="txtStatusVenda" id="txtStatusVenda" class="input-medium" value="<?php echo $status; ?>" disabled />
                                                                </select>
                                                            </span>
                                                            <span style="width:33%; display:block; float:left; margin-left:1%" >
                                                                <?php if ($status_nfe == "Autorizada") { ?>
                                                                    <input type="text" name="txtJustificativa" maxlength="150" id="txtJustificativa" class="input-medium" value="<?php echo $justCancel; ?>" />
                                                                <?php } ?>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="spanBNT3" style="margin-left:10px">
                                            <div class="toolbar bottom tar">
                                                <div class="data-fluid">
                                                    <?php if ($status == 'Aprovado' && $status_nfe !== "Cancelada") { ?>
                                                        <div style="float:left">
                                                            <button id="btn_Assinar" <?php
                                                            if ($status_nfe == "Autorizada" ||
                                                                    file_exists("../../Pessoas/" . $contrato . "/NFE/" . ($tpAmb == '2' ? "homologacao" : "producao") . "/enviadas/aprovadas/" . $data_emissao_pasta . "/" . $chave_nfe . "-protNFe.xml")) {
                                                                echo "disabled";
                                                            }
                                                            ?> class="btn btn-success" type="button" onClick="assinar_nota();"><span class="ico-checkmark"></span> Assinar / Transmitir </button>
                                                                    <?php if ($status_nfe == "Autorizada") { ?>
                                                                <button id="btn_Imprimir" <?php
                                                                if ($status_nfe != "Autorizada" || !file_exists("../../Pessoas/" . $contrato . "/NFE/" . ($tpAmb == '2' ? "homologacao" : "producao") . "/enviadas/aprovadas/" . $data_emissao_pasta . "/" . $chave_nfe . "-protNFe.xml")) {
                                                                    echo "disabled";
                                                                }
                                                                ?> class="btn btn-primary" type="button" onClick="imprimir_nota('');"><span class="ico-print"></span> Imprimir</button>
                                                                    <?php } else { ?>
                                                                <button class="btn btn-primary" type="button" name="btnVisualizarDanfe" id="btnVisualizarDanfe"><span class="ico-print"></span> Pré-Visualizar Danfe </button>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if (($status == 'Aguarda') && ($_SESSION['login_usuario'] == $destinatario)) { ?>
                                                        <div style="float:left">
                                                            <button class="btn btn-success" type="submit" name="bntAprov" id="bntAprov" onClick="return validaForm();"><span class="ico-checkmark"></span> Aprovar</button>
                                                            <button class="btn red" type="submit" name="bntReprov" id="bntReprov"><span class="ico-remove"></span> Reprovar</button>
                                                        </div>
                                                    <?php } ?>
                                                    <div class="btn-group">
                                                        <?php if ($_GET['idx'] != '') { ?>
                                                            <button class="btn  btn-warning" type="submit" name="bntVoltar" id="bntVoltar" value="Voltar"><span class="ico-backward"> </span> Voltar</button>
                                                        <?php } if ($disabled == '') { ?>
                                                            <button class="btn btn-success" type="submit" name="bntSave" id="bntSave" onclick="return validaCFOP();"><span class="ico-checkmark"></span> Gravar</button>
                                                            <?php if ($_POST['txtId'] == '') { ?>
                                                                <button class="btn btn-success" onClick="history.go(-1)" id="bntCancelar"><span class="ico-reply"></span> Cancelar</button>
                                                            <?php } else { ?>
                                                                <button class="btn btn-success" type="submit" name="bntAlterar" id="bntAlterar"><span class="ico-reply"></span> Cancelar</button>
                                                                <?php
                                                            }
                                                        } else {
                                                            if ($status == 'Aprovado') { ?>
                                                            <?php } ?> 
                                                            <?php if ($status_nfe == "Autorizada" && file_exists("../../Pessoas/" . $contrato . "/NFE/" . ($tpAmb == '2' ? "homologacao" : "producao") . "/enviadas/aprovadas/" . $data_emissao_pasta . "/" . $chave_nfe . "-protNFe.xml")) { ?>
                                                                <button class="btn btn-primary" type="submit" name="btnXml" id="btnXml" value="Imprimir"><span class="ico-barcode-2"></span> Xml</button>
                                                                <button class="btn btn-warning" type="button" name="btnCancelarNFE" id="btnCancelarNFE"><span class="ico-remove"></span> Cancelar NFE </button>
                                                            <?php } ?>
                                                            <button class="btn  btn-success" type="submit" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                                                            <?php if ($status_nfe !== "Autorizada" && $status_nfe !== "Cancelada") { ?>
                                                                <button class="btn  btn-success" type="submit" name="bntEdit" id="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                                                                <button class="btn red" type="submit" name="bntDelete" id="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"></span> Excluir</button>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input name="txtIdTP" id="txtIdTP" value="<?php echo $idTP; ?>" type="hidden"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/highlight/jquery.highlight-4.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/faq.js'></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/moment.min.js"></script>    
        <script type="text/javascript" src="../../js/util.js"></script>                                                         
        <script type="text/javascript" src="js/EmitirNota.js"></script>
        <script type="text/javascript" src="js/ProcessarNota.js"></script>
    </body>
    <?php odbc_close($con); ?>
</html>
