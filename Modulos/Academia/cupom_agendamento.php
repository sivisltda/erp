<?php 
if(!isset($_GET["PathArq"])) {
    session_start();
    include "./../../Connections/configini.php";
}
    
$id = "";  
$id_agenda = "";
$pessoa = "";
$pessoaNome = "";
$empresa = "001";
$dt_inicio = "";
$hora_inicio = "";
$dt_fim = "";
$hora_fim = "";
$assunto = "";
$obs = "";
$concluido = 0;

$cpf_cnpj = "";
$cedente = "";
$nome_fantasia = "";    
$inscricao = "";    
$endereco = "";
$cidade_uf = "";
$cep = "";
$telefone = "";
$site = "";
$operador = "";
    
if(isset($_GET["id"])) {            
    $cur = odbc_exec($con, "SELECT * FROM sf_agendamento 
    inner join sf_fornecedores_despesas on sf_fornecedores_despesas.id_fornecedores_despesas = sf_agendamento.id_fornecedores 
    left join sf_agenda on sf_agenda.id_agenda = sf_agendamento.id_agenda
    where id_agendamento = " . $_GET["id"]);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_agendamento']);
        $id_agenda = utf8_encode($RFP['descricao_agenda']);
        if ($RFP['id_fornecedores_despesas'] > 0) {
            $pessoa = utf8_encode($RFP['id_fornecedores_despesas']);
            $pessoaNome = utf8_encode($RFP['razao_social']);
            $empresa = utf8_encode($RFP['empresa']);            
        }
        $dt_inicio = escreverData($RFP['data_inicio']);
        $hora_inicio = escreverData($RFP['data_inicio'], 'H:i');
        $dt_fim = escreverData($RFP['data_fim']);
        $hora_fim = escreverData($RFP['data_fim'], 'H:i');
        $assunto = utf8_encode($RFP['assunto']);
        $obs = utf8_encode($RFP['obs']);
        $concluido = utf8_encode($RFP['concluido']);
        $operador = utf8_encode($RFP['agendado_por']);
    }
    if (is_numeric($empresa)) {
        $cur = odbc_exec($con, "select * from sf_filiais where numero_filial = " . $empresa);
        while ($RFP = odbc_fetch_array($cur)) {
            $cpf_cnpj = utf8_encode($RFP["cnpj"]);
            $cedente = utf8_encode($RFP["razao_social_contrato"]);
            $nome_fantasia = utf8_encode($RFP["nome_fantasia_contrato"]);
            $inscricao = utf8_encode($RFP["inscricao_estadual"]);
            $endereco = utf8_encode($RFP['endereco']) . " n°" . utf8_encode($RFP['numero']) . "," . utf8_encode($RFP['bairro']);
            $cidade_uf = utf8_encode($RFP['cidade_nome']) . " " . utf8_encode($RFP['estado']);
            $cep = utf8_encode($RFP['cep']);
            $telefone = utf8_encode($RFP['telefone']);
            $site = utf8_encode($RFP['site']);
        }
    }    
} ?>      
<html> 
    <head>
        <title>SIVIS</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script> 
        <style type="text/css">
            body {
                font-size: 10px;
                font-family: Tahoma;
            }
            table {
                width: 100%;
                max-width: 302px;
                font-size: 10px;
                font-family: Tahoma;
            }
            .emp {
                font-size: 10px;
                font-weight: bold;
                text-align: center;
                padding: 5px 0px 5px 0px;
            }
            .end {
                text-align: center;
                padding: 5px 0px 5px 0px;
            }
            .doc {
                padding-top: 5px;
            }
            .lin {
                padding-top: 5px;
                border-bottom: 1px solid #000;
            }
            .spc {
                padding-bottom: 5px;
            }
            .grupo {
                padding: 3px 0px 3px 0px;
                background-color: #DDD;
                font-weight: bold;
            }
            .tit {
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                padding: 5px 0px 10px 0px;
            }
        </style>
    </head>
    <body>
        <table cellspacing="0" cellpadding="0">            
            <tr>
                <td colspan="6" class="tit"><?php echo $nome_fantasia; ?></td>
            </tr>       
            <tr>
                <td colspan="6" class="end">
                    <?php echo (strlen($cedente) > 0 ? $cedente."<br>" : "");?>
                    <?php echo (strlen($endereco) > 0 ? $endereco."<br>" : "");?>
                    <?php echo (strlen($cidade_uf) > 0 ? $cidade_uf : "");?>
                    <?php echo (strlen($cep) > 0 ? " - CEP: " . $cep : "");?>
                    <br>
                    <?php echo (strlen($telefone) > 0 ? $telefone."<br>" : "");?>
                    <?php echo (strlen($site) > 0 ? $site."<br>" : "");?>
                </td>
            </tr>
            <tr>
                <td colspan="6" class="doc"><b>CNPJ: <?php echo $cpf_cnpj; ?></b><br/>I.E.: <?php echo $inscricao; ?></td>
            </tr>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <tr>
                <td colspan="3" class="doc"><b><?php echo $data_venda; ?></b></td>
                <td colspan="3" class="doc" style="text-align:right"><b>COD: <?php echo str_pad($id, 6, "0", STR_PAD_LEFT); ?></b></td>
            </tr>
            <tr>
                <td colspan="6" class="tit">Recibo de Agendamento</td>
            </tr>            
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>                              
            <tr>
                <td colspan="4" class="doc"><b>Agenda:</b></td>
                <td colspan="2" class="doc" style="text-align:right"><b></b></td>
            </tr>  
            <tr>
                <td colspan="6">
                    <?php echo $id_agenda;?>                                                                    
                </td>
            </tr>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>            
            <tr>
                <td colspan="4" class="doc"><b>Cliente:</b></td>
                <td colspan="2" class="doc" style="text-align:right"><b></b></td>
            </tr>              
            <tr>
                <td colspan="6">
                    <?php echo $pessoa . " - " . $pessoaNome;?>                                                                     
                </td>
            </tr>  
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>            
            <tr>
                <td colspan="4" class="doc"><b>Período:</b></td>
                <td colspan="2" class="doc" style="text-align:right"><b></b></td>
            </tr>  
            <tr>
                <td colspan="6">
                    <?php echo $dt_inicio . " " . $hora_inicio . " até " . $dt_fim . " " . $hora_fim;?>                                                                    
                </td>
            </tr> 
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>            
            <tr>
                <td colspan="4" class="doc"><b>Assunto:</b></td>
                <td colspan="2" class="doc" style="text-align:right"><b></b></td>
            </tr>  
            <tr>
                <td colspan="6">
                    <?php echo $assunto;?>                                                                    
                </td>
            </tr>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>            
            <tr>
                <td colspan="4" class="doc"><b>Observação:</b></td>
                <td colspan="2" class="doc" style="text-align:right"><b></b></td>
            </tr>  
            <tr>
                <td colspan="6">
                    <?php echo $obs;?>                                                                    
                </td>
            </tr>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <tr>
                <td>Operador:</td>
                <td colspan="5"><?php echo $operador; ?></td>
            </tr>
        </table>
    </body>
    <script type="text/javascript" src="../../js/util.js"></script>     
    <?php if(!isset($_GET["PathArq"])) { ?>
        <script type="text/javascript">
            $(window).load(function () {
                window.print();
                <?php if ($cod_pedido !== "0") { ?>
                    setTimeout(function () {
                        window.print();
                    }, 100);
                <?php } ?>
                setTimeout(function () {
                    window.close();
                }, 500);
            });
        </script>    
    <?php } ?>
</html>