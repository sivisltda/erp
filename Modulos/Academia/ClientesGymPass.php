<?php
include "../../Connections/configini.php";

$id = "0";
$razao_social = "VENDA AVULSA";

if (is_numeric($_GET['id'])) {
    $cur = odbc_exec($con, "select * from sf_fornecedores_despesas
    where inativo = 0 and id_fornecedores_despesas = " . $_GET['id']) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_fornecedores_despesas']);
        $razao_social = utf8_encode(strtoupper($RFP['razao_social']));
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
<style>
    body { 
        font-family: sans-serif; 
    }    
    .selected {
        background: #acbad4 !important;
    }
</style>
<body>
    <div class="row-fluid">
        <form>
            <div class="frmhead">
                <div class="frmtext">GYM PASS</div>
                <div class="frmicon" onClick="parent.FecharBoxCliente()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input id="txtIdAluno" type="hidden" value="<?php echo $id; ?>"/>
                <input id="txtIdCod" type="hidden" value=""/>
                <input id="txtIdToken" type="hidden" value=""/>
                <div style="width:60%; float:left">
                    <span>Nome do Aluno</span>
                    <input type="text" value="<?php echo $id . " - " . $razao_social; ?>" disabled/>
                </div>
                <div style="width:33%; float:left;margin-left: 1%">
                    <span>Pass Number:</span>
                    <input id="txtPass_number" name="txtPass_number" type="text" value="" maxlength="13"/>  
                </div>
                <div style="width:5%; float:left;margin-left: 1%">
                    <button class="button button-turquoise btn-primary" type="button" onclick="buscarPass();" id="btnPesquisar" style="margin-top: 12px;"><span class="ico-search icon-white"></span></button>           
                </div>  
                <div style="clear:both; height:5px"></div>                
                <div style="width:100%;">
                    <span style="color:black; font-size:14px; text-shadow:0 1px #FFF; padding-left:5px">Atribuir Credencial:</span>
                    <hr style="margin:2px 0; border-top:1px solid #DDD">
                </div>
                <div style="width:35%; float:left">
                    <span> Modelo:</span>
                    <select id="txtCredModelo" class="select" style="width:100%">
                        <option value="null">Selecione</option>
                        <?php $cur = odbc_exec($con, "select id_credencial,descricao from sf_credenciais where inativo = 0 order by descricao") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option value="<?php echo $RFP['id_credencial']; ?>"><?php echo utf8_encode($RFP['descricao']); ?></option> 
                        <?php } ?>
                    </select>
                </div>
                <div style="width:12%; float:left;margin-left: 1%">
                    <span>Inicio:</span>
                    <input type="text" id="txtCredDtIni" class="datepicker inputCenter" value="" disabled/>
                </div>
                <div style="width:12%; float:left;margin-left: 1%">
                    <span>Fim:</span>
                     <input type="text" id="txtCredDtFim" class="datepicker inputCenter" value="" disabled/>
                </div>
                <div style="width:25%;float: left;margin-left: 1%">
                    <span>Motivo</span>
                    <input type="text" id="txtCredMotivo" class="input-medium" value="" style="width:100%" disabled/>
                </div>
                <div style="float:left; width:75px; margin-left:1%;margin-top:12px;">
                    <button type="button" id="btnIncluirCredencial" class="btn dblue" onclick="incluirCredencial();" style="line-height:19px; width:75px; margin:0;background-color: #308698 !important;">Incluir</button>
                </div>                
                <div style="clear:both; height:5px"></div>
                <div style="width:100%;">
                    <span style="color:black; font-size:14px; text-shadow:0 1px #FFF; padding-left:5px">Plano:</span>
                    <hr style="margin:2px 0; border-top:1px solid #DDD">
                </div>
                <div class="body" style="margin-left:0; padding:0">
                    <div class="content" style="min-height:0px; margin-top:0">
                        <table id="tblPlano" class="table" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="width:45%; text-align:center"><b>Academia</b></th>
                                    <th style="width:45%; text-align:center"><b>Atividade</b></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div style="clear:both; height:5px"></div>
                <div style="width:100%;">
                    <span style="color:black; font-size:14px; text-shadow:0 1px #FFF; padding-left:5px">Aluno:</span>
                    <hr style="margin:2px 0; border-top:1px solid #DDD">
                </div>
                <div class="body" style="margin-left:0; padding:0">
                    <div class="content" style="min-height:0px; margin-top:0">
                        <table id="tblAluno" class="table" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="width:20%; text-align:center"><b>Código</b></th>
                                    <th style="width:60%; text-align:center"><b>Aluno</b></th>
                                    <th style="width:20%; text-align:center"><b>Token</b></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div style="clear:both; height:5px"></div>
                <div style="width:100%;">
                    <span style="color:black; font-size:14px; text-shadow:0 1px #FFF; padding-left:5px">Estado:</span>
                    <hr style="margin:2px 0; border-top:1px solid #DDD">
                </div>
                <div class="body" style="margin-left:0; padding:0">
                    <div class="content" style="min-height:0px; margin-top:0">
                        <table id="tblStatus" class="table" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="width:100%; text-align:center"><b>Descrição</b></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div style="clear:both; height:5px"></div>                
            </div>
        </form>
    </div>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../js/plugins.js"></script>
    <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>
    <script type="text/javascript" src="../../js/moment.min.js"></script>    
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type="text/javascript">
        
        limpar();                 
        function buscarPass() {
            if($("#txtPass_number").val().length >= 10 && $("#txtPass_number").val().length <= 13) {
                limpar();            
                parent.$("#loader").show();            
                $.ajax({
                    url: "form/gympassForm.php",
                    data: {pass_number: $("#txtPass_number").val()},
                    type: 'GET', dataType: 'json',
                    success: function (data) {
                        parent.$("#loader").hide();                            
                        if(data !== null){
                            $("#tblPlano tbody").html("<tr><td>" + data.results.gym_title + "</td><td>" + data.results.pass_type_title + "</td></tr>");
                            $("#tblAluno tbody").html("<tr><td>" + data.results.unique_token + "</td><td>" + data.results.person_name + "</td><td>" + data.results.transaction_token + "</td></tr>");
                            $("#tblStatus tbody").html("<tr><td>" + retornoGymPass[data.status.code] + "</td></tr>");
                            if(data.status.code === 0) {
                                $("#txtIdCod").val(data.results.unique_token);
                                $("#txtIdToken").val(data.results.transaction_token);
                                $("#btnIncluirCredencial").attr("disabled", false);
                                $("#txtCredDtIni, #txtCredDtFim").val(moment().format(lang["dmy"]));
                                $("#txtCredMotivo").val("GYM PASS");                            
                            }
                        } else {
                            bootbox.alert("Erro no pagamento!");    
                        }
                    },
                    error: function () {
                        parent.$("#loader").hide();
                        bootbox.alert("Erro no pagamento!");
                    }
                });
            } else {
                bootbox.alert("Preencha o valor do Pass Number corretamente!");
            }
        }
        
        function incluirCredencial() {
            if($("#txtCredModelo").val() !== "null" && $("#txtIdCod").val() !== "" && $("#txtIdToken").val() !== "") {
                parent.$("#loader").show();
                $.post("form/ClientesCredenciaisServer.php", "AddCredencial=S&txtId=" + $("#txtIdAluno").val() + 
                    "&txtCredModelo=" + $("#txtCredModelo").val() + "&txtCredDtIni=" + $("#txtCredDtIni").val().replace("/", "_") +
                    "&txtCredDtFim=" + $("#txtCredDtFim").val().replace("/", "_") + "&txtCredMotivo=" + $("#txtCredMotivo").val() + 
                    "&txtIdCod=" + $("#txtIdCod").val() + "&txtIdToken=" + $("#txtIdToken").val()).done(function (data) {
                    parent.$("#loader").hide();
                    var tblCredenciais = parent.$('#tblCredenciais').dataTable();
                    tblCredenciais.fnReloadAjax("form/ClientesCredenciaisServer.php?listCredencial=" + $("#txtIdAluno").val());
                    parent.atualizaStatusCli(); 
                    parent.FecharBoxCliente();
                });
            } else {                
                bootbox.alert("Preencha o Modelo de Credencial corretamente!");
            }
        }        
        
        function limpar() {
            $("#txtCredDtIni, #txtCredDtFim, #txtCredMotivo, #txtIdCod, #txtIdToken").val("");
            $("#txtCredModelo").select2("val", "null");
            $("#btnIncluirCredencial").attr("disabled", true);
            $("#tblPlano tbody").html("<tr><td colspan=\"2\">Não foi encontrado nenhum resultado</td></tr>");
            $("#tblAluno tbody").html("<tr><td colspan=\"3\">Não foi encontrado nenhum resultado</td></tr>");
            $("#tblStatus tbody").html("<tr><td>Não foi encontrado nenhum resultado</td></tr>");    
        }                
        
        var retornoGymPass = {
        0: "Diária validada com sucesso",
        3: "Validação não efetuada por ser um teste",
        10: "Erro de comunicação, por favor tente mais tarde.",
        11: "Número de Token Diário inválido. Por favor, tente novamente.",
        12: "Validador não autorizado. Uma diária é válida apenas na academia para o qual foi comprada. Seu email não está cadastrado como validador na academia para o qual este passe é válido. Se você acha que seu email deveria estar cadastrado como validador para esta academia, contacte o seu vendedor ou nosso suporte.",
        13: "Este Token Diário já foi usado hoje. Por favor verifique que não está validando este Token Diário duas vezes no mesmo dia.",
        14: "Este Token Diário não está disponível devido a questões com o pagamento. O status atual de pagamento é:",
        15: "Este passe já foi completamente usado e não possui mais diárias disponíveis.",
        16: "Este passe já venceu e não pode ser mais usado.",
        17: "Confirme a identidade e informações do usuário antes de validar.",
        21: "Número de Token Diário inválido. Por favor tente novamente.",
        22: "Número de cartão não habilitado, favor ligar para nosso atendimento ou entrar no site e habilitá-lo.",
        23: "Por favor escolha a academia.",
        24: "Você não está autorizado a validar diárias. Se achar que deveria estar autorizado, ligue para nosso atendimento.",
        25: "Por favor escolha a diária a utilizar.",
        26: "Créditos ou limite insuficientes, e nenhum cartão de crédito registrado. Peça ao cliente para cadastrar um cartão de crédito, carregar mais créditos ou ligar para nosso atendimento.",
        27: "Pessoa bloqueada. Favor ligar para nosso atendimento para desbloquear a pessoa",
        28: "Erro ao aprovar cartão de crédito do cliente. Por favor tente novamente ou ligue para nosso atendimento.",
        29: "Cartão desabilitado, favor ligar para nosso atendimento.",
        30: "Cartão vencido, favor ligar para nosso atendimento.",
        31: "Passe não encontrado para esta academia e/ou pessoa não habilitada para emissão automática de passes. Favor ligar para nosso atendimento.",
        32: "Esta pessoa não possui passes para esta academia",
        33: "Academia bloqueada por tentativa de validação indevida. Por favor ligue para nosso telefone para desbloquear a academia.",
        34: "Token Diário desabilitado, favor ligar para nosso atendimento.",
        35: "Token Diário vencido. O Token Diário só é válido no dia em que é emitido. Por favor peça ao CLIENTE para emitir um novo Token Diário seguindo as informações em:",
        36: "Validação via cartão não é mais permitida, somente validaçoes usando Tokens Diários. Em caso de dúvidas ligue para o Gympass nos telefones disponíveis em contato. Por favor peça ao CLIENTE para emitir um novo token diário seguindo as informações em:",
        37: "Em NENHUMA HIPÓTESE serão mais aceitos cartões de 16 dígitos. Peça ao cliente que ainda possui somente o cartão para entrar em contato com o Gympass ou emitir Tokens Diários usando as informações em:",
        38: "Esta pessoa não está na lista de pessoas permitidas nesta academia.",
        39: "Esta pessoa já foi nesta academia o limite máximo de vezes desta semana.",
        40: "Esta pessoa já foi nesta academia o limite máximo de vezes deste mês.",
        41: "Não foi encontrada uma reserva para esta pessoa neste dia para este estabelecimento.",
        42: "Ainda não chegou o horário desta reserva. Por favor aguarde até mais perto do horário desta reserva para validar.",
        43: "É tarde demais para validar esta reserva.",
        44: "Academia sem permissão para validar tokens únicos de pessoas",
        45: "Checkin de usuário ainda não realizado",
        46: "Checkin de usuário realizado para outra academia",
        47: "Checkin de usuário nesta academia expirou",
        48: "Validação não pode ser feita fora do horário de abertura da academia. Por favor entre em contato com nosso atendimento para atualizar o seu horário de abertura, e valide os clientes sempre e somente no momento da entrada na academia.",
        49: "Sua academia só pode efetuar validações através de checkin",
        50: "O número de tipo de passe não é igual ao produto escolhido pelo usuário no checkin ou reserva",
        51: "O número de tipo de passe é inválido, ou o usuário não possui acesso a este tipo de passe"};
    </script>
    <?php odbc_close($con); ?>
</body>