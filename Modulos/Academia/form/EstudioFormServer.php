<?php

include "../../../Connections/configini.php";

$toAlter = "";

function returnBlock($horario, $dia, $hora) {
    $blk = "1";
    for ($i = 0; $i < count($horario); $i++) {
        if (substr($horario[$i][0], $dia, 1) == "1" && $hora >= $horario[$i][1] && $hora <= $horario[$i][2]) {
            $blk = "0";
        }
    }
    return $blk;
}

if (isset($_POST["btnSave"])) {
    $error = "";
    $limite = 0;
    $horarios = array();
    $hr = substr($_POST['txtFaixa1_ini'], 0, 2);

    $result = odbc_exec($con, "select ESTUD_LIMIT_AGND from sf_configuracao;");
    while ($RFP = odbc_fetch_array($result)) {
        $limite = $RFP['ESTUD_LIMIT_AGND'];
    }

    $query = "select sf_turnos.* from sf_fornecedores_despesas 
    inner join sf_turnos on sf_turnos.cod_turno = sf_fornecedores_despesas.fornecedores_hr
    where id_fornecedores_despesas = " . valoresSelect('txtProfessorTurmaHr');
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        for ($i = 1; $i <= 5; $i++) {
            $horarios[] = array($RFP['dia_semana' . $i], substr($RFP['faixa' . $i . '_ini'], 0, 2), substr($RFP['faixa' . $i . '_fim'], 0, 2));
        }
    }

    if ((isset($_POST['fx1_0']) && returnBlock($horarios, 0, $hr) == "1") ||
            (isset($_POST['fx1_1']) && returnBlock($horarios, 1, $hr) == "1") ||
            (isset($_POST['fx1_2']) && returnBlock($horarios, 2, $hr) == "1") ||
            (isset($_POST['fx1_3']) && returnBlock($horarios, 3, $hr) == "1") ||
            (isset($_POST['fx1_4']) && returnBlock($horarios, 4, $hr) == "1") ||
            (isset($_POST['fx1_5']) && returnBlock($horarios, 5, $hr) == "1") ||
            (isset($_POST['fx1_6']) && returnBlock($horarios, 6, $hr) == "1")) {
        echo "Horário indisponível para agendamento deste professor!";
        exit();
    }

    $query = "select substring(faixa1_ini,1,2),sum(cast(SUBSTRING(dia_semana1,1,1) as int)) dom,
    sum(cast(SUBSTRING(dia_semana1,2,1) as int)) seg,sum(cast(SUBSTRING(dia_semana1,3,1) as int)) ter,
    sum(cast(SUBSTRING(dia_semana1,4,1) as int)) qur,sum(cast(SUBSTRING(dia_semana1,5,1) as int)) qui,
    sum(cast(SUBSTRING(dia_semana1,6,1) as int)) sex,sum(cast(SUBSTRING(dia_semana1,7,1) as int)) sab
    from dbo.sf_turnos_estudio where professor = " . valoresSelect('txtProfessorTurmaHr') . "
    and substring(faixa1_ini,1,2) = '" . $hr . "' and cod_estudio not in(" . valoresNumericos("txtId") . ")
    group by substring(faixa1_ini,1,2) order by 1";
    $res = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($res)) {
        if (isset($_POST['fx1_0']) && $RFP['dom'] >= $limite) {
            $error = "Limite de " . $limite . " pessoas no Domingo ás " . $hr . ":00";
        } elseif (isset($_POST['fx1_1']) && $RFP['seg'] >= $limite) {
            $error = "Limite de " . $limite . " pessoas no Segunda ás " . $hr . ":00";
        } elseif (isset($_POST['fx1_2']) && $RFP['ter'] >= $limite) {
            $error = "Limite de " . $limite . " pessoas no Terça ás " . $hr . ":00";
        } elseif (isset($_POST['fx1_3']) && $RFP['qur'] >= $limite) {
            $error = "Limite de " . $limite . " pessoas no Quarta ás " . $hr . ":00";
        } elseif (isset($_POST['fx1_4']) && $RFP['qui'] >= $limite) {
            $error = "Limite de " . $limite . " pessoas no Quinta ás " . $hr . ":00";
        } elseif (isset($_POST['fx1_5']) && $RFP['sex'] >= $limite) {
            $error = "Limite de " . $limite . " pessoas no Sexta ás " . $hr . ":00";
        } elseif (isset($_POST['fx1_6']) && $RFP['sab'] >= $limite) {
            $error = "Limite de " . $limite . " pessoas no Sábado ás " . $hr . ":00";
        }
    }

    if ($error != "") {
        echo $error;
        exit();
    }

    $sql = "select razao_social from sf_fornecedores_despesas where id_fornecedores_despesas = " . valoresSelect('txtProfessorTurmaHr');
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $toAlter = $RFP['razao_social'] . " : " . $_POST['txtFaixa1_ini'] . " - " . (valoresCheck('fx1_0') == 1 ? "Dom " : "") .
                (valoresCheck('fx1_1') == 1 ? "Seg " : "") . (valoresCheck('fx1_2') == 1 ? "Ter " : "") .
                (valoresCheck('fx1_3') == 1 ? "Qua " : "") . (valoresCheck('fx1_4') == 1 ? "Qui " : "") .
                (valoresCheck('fx1_5') == 1 ? "Sex " : "") . (valoresCheck('fx1_6') == 1 ? "Sab " : "");
    }

    if ($_POST['txtId'] != '') {
        $sql = "insert into sf_turnos_estudio_hist(id_fornecedor,usuario,data_lanc,tipo,observacao)        
        select aluno,'" . $_SESSION["login_usuario"] . "',GETDATE(),'A',razao_social COLLATE DATABASE_DEFAULT + ' : ' + faixa1_ini COLLATE DATABASE_DEFAULT + ' - ' + 
        case when SUBSTRING(dia_semana1,1,1) = '1' then 'Dom ' else '' end + case when SUBSTRING(dia_semana1,2,1) = '1' then 'Seg ' else '' end +
        case when SUBSTRING(dia_semana1,3,1) = '1' then 'Ter ' else '' end + case when SUBSTRING(dia_semana1,4,1) = '1' then 'Qua ' else '' end + 
        case when SUBSTRING(dia_semana1,5,1) = '1' then 'Qui ' else '' end + case when SUBSTRING(dia_semana1,6,1) = '1' then 'Sex ' else '' end +
        case when SUBSTRING(dia_semana1,7,1) = '1' then 'Sab ' else '' end + ' > " . $toAlter . "' from sf_turnos_estudio inner join sf_fornecedores_despesas on id_fornecedores_despesas = professor
        where cod_estudio = " . $_POST["txtId"];
        odbc_exec($con, $sql) or die(odbc_errormsg());
        $sql = "update sf_turnos_estudio set " .
                "faixa1_ini = " . valoresHora('txtFaixa1_ini') . "," .
                "dia_semana1 = " . valoresTexto2(valoresCheck('fx1_0') . valoresCheck('fx1_1') . valoresCheck('fx1_2') . valoresCheck('fx1_3') . valoresCheck('fx1_4') . valoresCheck('fx1_5') . valoresCheck('fx1_6')) . "," .
                "professor = " . valoresSelect('txtProfessorTurmaHr') . "," .
                "aluno = " . valoresSelect('txtCodCliente') . " " .
                "where cod_estudio = " . $_POST['txtId'];
        odbc_exec($con, $sql) or die(odbc_errormsg());
        echo $_POST['txtId'];
    } else {
        $sql = "insert into sf_turnos_estudio_hist(id_fornecedor,usuario,data_lanc,tipo,observacao) values (        
        " . valoresSelect('txtCodCliente') . ",'" . $_SESSION["login_usuario"] . "',GETDATE(),'I','" . $toAlter . "')";
        odbc_exec($con, $sql) or die(odbc_errormsg());
        $sql = "insert into sf_turnos_estudio(faixa1_ini,dia_semana1,professor,aluno) values( " .
                valoresHora('txtFaixa1_ini') . "," .
                valoresTexto2(valoresCheck('fx1_0') . valoresCheck('fx1_1') . valoresCheck('fx1_2') . valoresCheck('fx1_3') . valoresCheck('fx1_4') . valoresCheck('fx1_5') . valoresCheck('fx1_6')) . "," .
                valoresSelect('txtProfessorTurmaHr') . "," .
                valoresSelect('txtCodCliente') . ") ";
        odbc_exec($con, $sql) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 cod_estudio from sf_turnos_estudio order by cod_estudio desc") or die(odbc_errormsg());
        echo odbc_result($res, 1);
    }
}

if (isset($_POST["btnExcluir"])) {
    if (is_numeric($_POST["txtId"])) {
        $sql = "insert into sf_turnos_estudio_hist(id_fornecedor,usuario,data_lanc,tipo,observacao)        
        select aluno,'" . $_SESSION["login_usuario"] . "',GETDATE(),'E',razao_social COLLATE DATABASE_DEFAULT + ' : ' + faixa1_ini COLLATE DATABASE_DEFAULT + ' - ' + 
        case when SUBSTRING(dia_semana1,1,1) = '1' then 'Dom ' else '' end + case when SUBSTRING(dia_semana1,2,1) = '1' then 'Seg ' else '' end +
        case when SUBSTRING(dia_semana1,3,1) = '1' then 'Ter ' else '' end + case when SUBSTRING(dia_semana1,4,1) = '1' then 'Qua ' else '' end + 
        case when SUBSTRING(dia_semana1,5,1) = '1' then 'Qui ' else '' end + case when SUBSTRING(dia_semana1,6,1) = '1' then 'Sex ' else '' end +
        case when SUBSTRING(dia_semana1,7,1) = '1' then 'Sab ' else '' end from sf_turnos_estudio inner join sf_fornecedores_despesas on id_fornecedores_despesas = professor
        where cod_estudio = " . $_POST["txtId"];
        odbc_exec($con, $sql) or die(odbc_errormsg());
        $sql = "delete from sf_turnos_estudio where cod_estudio = " . $_POST["txtId"];
        odbc_exec($con, $sql) or die(odbc_errormsg());
        echo "YES";
    }
}

if (is_numeric($_GET['id'])) {
    $local = array();
    $sql = "select cod_estudio,faixa1_ini,dia_semana1,id_fornecedores_despesas,razao_social,professor
    from sf_turnos_estudio e inner join sf_fornecedores_despesas a on e.aluno = a.id_fornecedores_despesas
    where cod_estudio = " . $_GET['id'];
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array(
            'cod_estudio' => utf8_encode($RFP['cod_estudio']),
            'faixa1_ini' => utf8_encode($RFP['faixa1_ini']),
            'dia_semana1' => utf8_encode($RFP['dia_semana1']),
            'id_fornecedores_despesas' => utf8_encode($RFP['id_fornecedores_despesas']),
            'razao_social' => utf8_encode($RFP['razao_social']),
            'professor' => utf8_encode($RFP['professor'])
        );
    }
    echo(json_encode($local));
}
odbc_close($con);