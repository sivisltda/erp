<?php

ini_set('max_execution_time', 300);
include "../../../Connections/configini.php";

$data = array();
$rec_id = "";
$pass_type_number = "";
$auth_token = "";

if (isset($_GET["pass_number"])) {
    $cur = odbc_exec($con, "select * from sf_configuracao where id = 1");
    while ($RFP = odbc_fetch_array($cur)) {
        $rec_id = $RFP['gyp_recid'];
        $pass_type_number = $RFP['gyp_ptype'];
        $auth_token = $RFP['gyp_token'];
    }
    if ($rec_id != "" && $rec_id != "" && $rec_id != "") {
        $url = "https://api.gympass.com/transacoes/validar_numero.json?" .
        "pass_number=" . $_GET["pass_number"] .
        "&rec_id=" . $rec_id . "&pass_type_number=" . $pass_type_number .
        "&considered_at=" . date("Y-m-d") . "%20" . date("H:i:s") .
        "&auth_token=" . $auth_token;
        $json = file_get_contents($url);
        $data = json_decode($json, true);
    }
    echo json_encode($data);
}
odbc_close($con);