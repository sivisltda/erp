<?php

include "../../../Connections/configini.php";

if (isset($_POST['btnSave'])) {
    $cur = odbc_exec($con, "select id_ferias from sf_ferias where meses = " . $_POST['txtMeses']);
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['id_ferias'] > 0) {
            echo "NO";
            exit;
        }
    }
    $query = "insert into sf_ferias(meses, dias) values (" . $_POST['txtMeses'] . "," . $_POST['txtDias'] . ")";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}

if (isset($_POST['delFerias'])) {
    $query = "delete from sf_ferias where id_ferias = " . $_POST['delFerias'];
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}

if (isset($_POST['atualizaPlanos'])) {
    $cur = odbc_exec($con, "select meses, dias from sf_ferias where id_ferias = " . $_POST['atualizaPlanos']);
    while ($RFP = odbc_fetch_array($cur)) {
        $meses = $RFP['meses'];
        $dias = utf8_encode($RFP['dias']);
    }
    $query = "update sf_vendas_planos set dias_ferias = " . $dias . " where id_plano 
    in(select id_plano_mens from sf_vendas_planos_mensalidade 
    where id_parc_prod_mens  in(select id_parcela from dbo.sf_produtos_parcelas where parcela = $meses)
    and GETDATE() between dt_inicio_mens and dt_fim_mens)
    and dt_cancelamento is null";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}