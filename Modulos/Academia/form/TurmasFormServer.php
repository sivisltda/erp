<?php

if (isset($_POST['isAjax']) || isset($_GET['isAjax'])) {
    require_once(__DIR__ . '/../../../Connections/configini.php');
    require_once(__DIR__ . '/../../../util/util.php');
}

$mdl_seg_ = returnPart($_SESSION["modulos"], 14);

if (isset($_GET['listPlanosTurma'])) {
    $where = " and B.id_turma = 0 ";
    if (isset($_GET['idTurma']) && is_numeric($_GET['idTurma'])) {
        $where = " and B.id_turma = " . $_GET['idTurma'];
    }
    $query = "select conta_produto, descricao,  CASE WHEN B.id_produto is not null then 'S' else 'N' end isCheck
              from sf_produtos A
              left join sf_turmas_produtos B on (A.conta_produto = B.id_produto" . $where . ")
              where tipo = 'C' and inativa = 0 and necessita_turma = 1 and A.conta_produto > 0";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["conta_produto"] = utf8_encode($RFP["conta_produto"]);
        $row["descricao"] = utf8_encode($RFP["descricao"]);
        $row["isCheck"] = utf8_encode($RFP["isCheck"]);
        $records[] = $row;
    }
    echo json_encode($records);
}

if (isset($_GET['getTurma'])) {
    $query = "select *, (select COUNT(id_fornecedores_despesas) from dbo.sf_fornecedores_despesas_turmas
              where id_turma = A.id_turma) alunosTurma from dbo.sf_turmas A where id_turma = " . $_GET['getTurma'];
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('id_turma' => $RFP['id_turma'],
            'descricao' => tableFormato($RFP['descricao'], 'T', '', ''),
            'dt_cadastro' => escreverData($RFP['dt_cadastro']),
            'dt_inicio' => escreverData($RFP['dt_inicio']),
            'n_alunos' => tableFormato($RFP['n_alunos'], 'N', '', ''),
            'professor' => utf8_encode($RFP['professor']),
            'horario' => tableFormato($RFP['horario'], 'T', '', ''),
            'ambiente' => utf8_encode($RFP['ambiente']),
            'desmatricular' => utf8_encode($RFP['desmatricular']),
            'dias_desmatricula' => tableFormato($RFP['dias_desmatricula'], 'N', '', ''),
            'inativo' => utf8_encode($RFP['inativo']),
            'alunosTurma' => utf8_encode($RFP['alunosTurma']));
    }
    echo(json_encode($local));
}

if (isset($_POST['btnSave'])) {
    $query = "set dateformat dmy;";
    if (is_numeric($_POST['txtId'])) {
        $query .= "update sf_turmas set " .
                "descricao = " . valoresTextoUpper('txtDescricao') . "," .
                "dt_inicio = " . valoresData('txtDtInicio') . "," .
                "desmatricular = " . valoresCheck('ckbDesmatricular') . "," .
                "dias_desmatricula = " . valoresNumericos('txDiasDesmatricula') . "," .
                "inativo = " . valoresCheck('ckbInativo') . "," .
                "professor = " . valoresSelect('txtProfessor') . "," .
                "horario = " . valoresSelect('txtHorario') . "," .
                "n_alunos = " . valoresNumericos('txtCapacidade') . "," .
                "ambiente = " . valoresSelect('txtAmbiente') . " where id_turma = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data)
        values ('sf_turmas', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'A', 'ALTERACAO TURMA', GETDATE())");
    } else {
        $query .= "INSERT INTO sf_turmas(descricao, dt_inicio,desmatricular,dias_desmatricula,inativo,professor,horario,n_alunos,ambiente,dt_cadastro)VALUES(" .
                valoresTextoUpper('txtDescricao') . "," .
                valoresData('txtDtInicio') . "," .
                valoresCheck('ckbDesmatricular') . "," .
                valoresNumericos('txDiasDesmatricula') . "," .
                valoresCheck('ckbInativo') . "," .
                valoresSelect('txtProfessor') . "," .
                valoresSelect('txtHorario') . "," .
                valoresNumericos('txtCapacidade') . "," .
                valoresSelect('txtAmbiente') . ",getDate())";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_turma from sf_turmas order by id_turma desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data)
        values ('sf_turmas', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'I', 'INCLUSAO TURMA', GETDATE())");
    }

    $query = "delete from sf_turmas_produtos where id_turma = " . $_POST['txtId'];
    odbc_exec($con, $query);
    if (isset($_POST['itemsPlanos'])) {
        for ($i = 0; $i < count($_POST['itemsPlanos']); $i++) {
            $query = "insert into sf_turmas_produtos(id_turma,id_produto) values(" . $_POST['txtId'] . "," . $_POST['itemsPlanos'][$i] . ")";
            odbc_exec($con, $query);
        }
    }
    echo $_POST['txtId'];
}

if (isset($_POST['addAlunoFila'])) {
    $query = "select id_fornecedores_despesas from sf_fornecedores_despesas_turmas where id_turma = " . $_POST['txtId'] . " and id_fornecedores_despesas = " . $_POST['txtCodAlunoFila'];
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['id_fornecedores_despesas'] !== "") {
            echo "TURMA";
            return;
        }
    }
    $query = "insert into dbo.sf_turmas_espera(id_turma,id_fornecedores_despesas,data_pedido) values(" . $_POST['txtId'] . "," . $_POST['txtCodAlunoFila'] . ",getdate())";
    odbc_exec($con, $query) or die(odbc_errormsg());
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    values ('sf_turmas_espera', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'I', 'INCLUSAO ALUNO', GETDATE(), " . $_POST['txtCodAlunoFila'] . ")");
    echo "YES";
}

if (isset($_POST['removerAlunoFila'])) {
    $query = "delete from sf_turmas_espera where id_turma = " . $_POST['txtId'] . " and id_fornecedores_despesas = " . $_POST['removerAlunoFila'];
    odbc_exec($con, $query) or die(odbc_errormsg());
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    values ('sf_turmas_espera', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'E', 'EXCLUSAO ALUNO', GETDATE(), " . $_POST['removerAlunoFila'] . ")");
    echo "YES";
}

if (isset($_POST['removerTurma'])) {
    $query = "delete from sf_turmas where id_turma = " . $_POST['removerTurma'];
    odbc_exec($con, $query) or die(odbc_errormsg());
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data)
    values ('sf_turmas', " . $_POST['removerTurma'] . ", '" . $_SESSION["login_usuario"] . "', 'E', 'EXCLUSAO TURMA', GETDATE())");
    echo "YES";
}

if (isset($_GET['listAlunosTurma'])) {
    $Where = "where A.id_turma > 0 ";
    $joinProduto = "";
    $grupoArray = [];
    
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    if (isset($_GET['txtId'])) {
        $Where .= " and T.id_turma = " . $_GET['txtId'];
    }
    if (isset($_GET['horario'])) {
        $Where .= " and horario = " . $_GET['horario'];
    }
    if (isset($_GET['professor'])) {
        $Where .= " and professor = " . $_GET['professor'];
    }
    if (isset($_GET['plano'])) {
        $joinProduto = " left join sf_turmas_produtos C on T.id_turma = C.id_turma ";
        $Where .= " and id_produto = " . $_GET['plano'];
    }                       
    $query = "select A.id_fornecedores_despesas, B.razao_social,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas = A.id_fornecedores_despesas) telefone,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = A.id_fornecedores_despesas) celular,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = A.id_fornecedores_despesas) email,
    T.id_turma, T.descricao, B.endereco, B.numero, B.complemento, B.bairro, tb_cidades.cidade_nome, tb_estados.estado_sigla, B.cep 
    from sf_turmas T left join sf_turnos TU on T.horario = TU.cod_turno " . $joinProduto . "
    inner join sf_fornecedores_despesas_turmas A on A.id_turma = T.id_turma
    inner join sf_fornecedores_despesas B on A.id_fornecedores_despesas = B.id_fornecedores_despesas  
    left join tb_cidades on tb_cidades.cidade_codigo = B.cidade 
    left join tb_estados on tb_estados.estado_codigo = B.estado
    left join sf_ambientes D on T.ambiente = D.id_ambiente
    left join sf_fornecedores_despesas E on T.professor = E.id_fornecedores_despesas " . $Where . " order by razao_social";
    //echo $query; exit;
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $cont += 1;
        $row = array();
        $row[] = "<div title='" . $RFP['id_fornecedores_despesas'] . "'>" . $RFP['id_fornecedores_despesas'] . "</div>";
        $row[] = "<a href=\"./../Academia/ClientesForm.php?id=" . $RFP["id_fornecedores_despesas"] . "\" target=\"_blank\">" . tableFormato($RFP['razao_social'], 'T', '', '') . "</a>";
        $row[] = "<div title='" . tableFormato($RFP['telefone'], 'T', '', '') . "'>" . tableFormato($RFP['telefone'], 'T', '', '') . "</div>";
        $row[] = "<div title='" . tableFormato($RFP['celular'], 'T', '', '') . "'>" . tableFormato($RFP['celular'], 'T', '', '') . "</div>";
        $row[] = "<div title='" . tableFormato($RFP['email'], 'T', '', '') . "'>" . tableFormato($RFP['email'], 'T', '', '') . "</div>";
        $row[] = $RFP['id_turma'];
        $row[] = utf8_encode($RFP['descricao']);        
        $row[] = utf8_encode($RFP['cep']);
        $row[] = utf8_encode($RFP['endereco']);
        $row[] = utf8_encode($RFP['numero']);
        $row[] = utf8_encode($RFP['bairro']);
        $row[] = utf8_encode($RFP['estado_sigla']);
        $row[] = utf8_encode($RFP['cidade_nome']);
        $row[] = utf8_encode($RFP['complemento']);
        $grp = array(utf8_encode($RFP['descricao']), (utf8_encode($RFP['descricao']) == "" ? "SEM GRUPO" : utf8_encode($RFP['descricao'])), 5);
        if (!in_array($grp, $grupoArray)) {
            $grupoArray[] = $grp;
        }        
        $output['aaData'][] = $row;
    }
    $output["iTotalRecords"] = $cont;
    $output["iTotalDisplayRecords"] = $cont;
    if (!isset($_GET['pdf'])) {
        echo json_encode($output);
    } else {
        $output['aaGrupo'] = $grupoArray;        
    }
}

if (isset($_GET['listaPresenca'])) {
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );

    $query = "select A.id_fornecedores_despesas, razao_social  from sf_fornecedores_despesas_turmas A
              inner join sf_fornecedores_despesas B on A.id_fornecedores_despesas = B.id_fornecedores_despesas
              where id_turma = " . $_GET['txtId'] . " order by razao_social";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $cont += 1;
        $row = array();
        $row[] = $RFP['id_fornecedores_despesas'];
        $row[] = tableFormato($RFP['razao_social'], 'T', '', '');
        for ($i = 0; $i <= 31; $i++) {
            $row[] = "";
        }
        $output['aaData'][] = $row;
    }
    $output["iTotalRecords"] = $cont;
    $output["iTotalDisplayRecords"] = $cont;
    if (!isset($_GET['pdf'])) {
        echo json_encode($output);
    }    
}

if (isset($_GET['listAlunosFila'])) {

    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );

    $query = "select A.id_fornecedores_despesas, B.razao_social, data_pedido from dbo.sf_turmas_espera A
              inner join sf_fornecedores_despesas B on A.id_fornecedores_despesas = B.id_fornecedores_despesas where id_turma = " . $_GET['txtId'] . " order by data_pedido asc";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row[] = "<div title='" . $RFP['id_fornecedores_despesas'] . "'>" . $RFP['id_fornecedores_despesas'] . "</div>";
        $row[] = "<a href='javascript:void(0)' onClick='AbrirCliente(" . utf8_encode($RFP['id_fornecedores_despesas']) . ")'><div id='formPQ' title='" . tableFormato($RFP['razao_social'], 'T', '', '') . "'>" . tableFormato($RFP['razao_social'], 'T', '', '') . "</div></a>";
        $row[] = "<div title='" . escreverDataHora($RFP['data_pedido']) . "'>" . escreverDataHora($RFP['data_pedido']) . "</div>";
        $row[] = "<center>
                    <button class=\"btn red\" type=\"button\" title=\"Remover da Fila\" onclick=\"RemoverAlunoFila(" . $RFP['id_fornecedores_despesas'] . ")\" style=\"padding: 1px 9px 0px 9px;\"><span class=\"ico-remove\"></span></button>
                    <button class=\"btn btn-success\" type=\"button\" title=\"Adicionar\" onclick=\"MatricularAluno(" . $RFP['id_fornecedores_despesas'] . ",'" . tableFormato($RFP['razao_social'], 'T', '', '') . "')\" style=\"padding: 1px 9px 0px 9px;\"><span class=\"ico-plus\"></span></button>
                  </center>";
        $output['aaData'][] = $row;
    }
    $output["iTotalRecords"] = $cont;
    $output["iTotalDisplayRecords"] = $cont;
    echo json_encode($output);
}

if (isset($_GET['listTurmaModalidade'])) {
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );

    $query = "select A.id_turma, descricao from sf_turmas A
            inner join sf_turmas_produtos B on A.id_turma = B.id_turma and inativo = 0 and id_produto = " . $_GET['listTurmaModalidade'];
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $cont += 1;
        $row = array();
        $row[] = "<div title='" . $RFP['id_turma'] . "'>" . $RFP['id_turma'] . "</div>";
        $row[] = "<div title='" . tableFormato($RFP['descricao'], 'T', '', '') . "'>" . tableFormato($RFP['descricao'], 'T', '', '') . "</div>";
        $row[] = "<div title='" . tableFormato($RFP['descricao'], 'T', '', '') . "'>" . tableFormato($RFP['descricao'], 'T', '', '') . "</div>";
        $output['aaData'][] = $row;
    }
    $output["iTotalRecords"] = $cont;
    $output["iTotalDisplayRecords"] = $cont;
    echo json_encode($output);
}

if (isset($_GET['listTurmaAluno'])) {
    $cur = odbc_exec($con, "select B.descricao descr_turma, F.descricao descr_plano, C.razao_social, D.nome_turno, A.id_turma, 
    A.id_fornecedores_despesas id_aluno, E.dt_inicio, E.dt_fim
    from sf_fornecedores_despesas_turmas A
    inner join sf_turmas B on A.id_turma = B.id_turma
    left join sf_vendas_planos E on A.id_plano = E.id_plano
    left join sf_produtos F on E.id_prod_plano = F.conta_produto
    left join sf_fornecedores_despesas C on B.professor = C.id_fornecedores_despesas
    left join sf_turnos D on B.horario = D.cod_turno
    where A.id_fornecedores_despesas = " . $_GET['listTurmaAluno']);
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    while ($RFP = odbc_fetch_array($cur)) {
        $cont = $cont + 1;
        $row = array();
        $row[] = tableFormato($RFP["descr_turma"], 'T', '', '');
        $row[] = tableFormato($RFP["descr_plano"], 'T', '', '') . " (" . escreverData($RFP["dt_inicio"]) . " - " . escreverData($RFP["dt_fim"]) . ")";
        $row[] = tableFormato($RFP["razao_social"], 'T', '', '');
        $row[] = tableFormato($RFP["nome_turno"], 'T', '', '');
        $row[] = "<center><button class=\"btn btn-small red\" type=\"button\" title=\"Matricular\" onClick=\"desmatricularTurma(" . $RFP["id_turma"] . "," . $RFP["id_aluno"] . ")\"><span class=\"ico-remove\" ></span></button></center>";
        $records['aaData'][] = $row;
    }
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}

if (isset($_POST['addAlunoTurma'])) {
    $query = "select id_fornecedores_despesas from sf_fornecedores_despesas_turmas where id_turma = " . $_POST['idTurma'] . " and id_fornecedores_despesas = " . $_POST['addAlunoTurma'];
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['id_fornecedores_despesas'] !== "") {
            echo "TURMA";
            return;
        }
    }
    $query = "set dateformat dmy;
                    BEGIN TRANSACTION
                        insert into sf_fornecedores_despesas_turmas(id_turma,id_fornecedores_despesas,id_plano,data_cadastro) values(" . $_POST['idTurma'] . "," . $_POST['addAlunoTurma'] . "," . $_POST['idPlano'] . ", getdate());
                        delete from sf_turmas_espera where id_fornecedores_despesas = " . $_POST['addAlunoTurma'] . " and id_turma = " . $_POST['idTurma'] . ";
                        insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values ('sf_alunos_turmas', " . $_POST['idTurma'] . ", '" . $_SESSION["login_usuario"] . "', 'I', '" . ($mdl_seg_ > 0 ? "ENTRADA NA OFICINA" : "INCLUSAO ALUNO TURMA") . " (' + (select descricao from sf_turmas where id_turma = " . $_POST['idTurma'] . ") + ')', GETDATE(), " . $_POST['addAlunoTurma'] . ");
                    IF @@ERROR = 0
                    COMMIT
                    ELSE
                    ROLLBACK;";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}

if (isset($_POST['excluirAlunoTurma'])) {
    $query = "set dateformat dmy;
                    BEGIN TRANSACTION
                        insert into sf_turmas_desmatriculas (data,id_fornecedores_despesas,id_turma,descricao_turma,motivo,login_user) values(getdate()," . $_POST['excluirAlunoTurma'] . "," . $_POST['idTurma'] . ",(select descricao from sf_turmas where id_turma = " . $_POST['idTurma'] . ")," . valoresTexto('txtMotivo') . ",'" . $_SESSION["login_usuario"] . "');
                        delete from sf_fornecedores_despesas_turmas where id_fornecedores_despesas = " . $_POST['excluirAlunoTurma'] . " and id_turma = " . $_POST['idTurma'] . ";
                        insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values ('sf_alunos_turmas', " . $_POST['idTurma'] . ", '" . $_SESSION["login_usuario"] . "', 'R', '" . ($mdl_seg_ > 0 ? "SAIDA DA OFICINA" : "EXCLUSAO ALUNO TURMA") . " (' + (select descricao from sf_turmas where id_turma = " . $_POST['idTurma'] . ") + ')', GETDATE(), " . $_POST['excluirAlunoTurma'] . ");
                    IF @@ERROR = 0
                    COMMIT
                    ELSE
                    ROLLBACK;";
    //echo $query;
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}

if (isset($_GET['listHistTurma'])) {
    $cur = odbc_exec($con, "set dateformat dmy;
    select descricao_turma, data, login_user, motivo from dbo.sf_turmas_desmatriculas
    where id_fornecedores_despesas = " . $_GET['listHistTurma'] . " order by data desc");
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );

    while ($RFP = odbc_fetch_array($cur)) {
        $cont = $cont + 1;
        $row = array();
        $row[] = tableFormato($RFP["descricao_turma"], 'T', '', '');
        $row[] = "<center>" . escreverData($RFP["data"]) . "</center>";
        $row[] = "<center>" . tableFormato($RFP["login_user"], 'T', '', '') . "</center>";
        $row[] = "<center>" . tableFormato($RFP["motivo"], 'T', '', '') . "</center>";
        $records['aaData'][] = $row;
    }
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}

if (isset($_GET['listHistSerasa'])) {
    $cur = odbc_exec($con, "set dateformat dmy;
    select tipo,data_lanc,usuario,observacao from sf_serasa where id_fornecedor = " .
            $_GET['listHistSerasa'] . " order by data_lanc desc");
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    while ($RFP = odbc_fetch_array($cur)) {
        $cont = $cont + 1;
        $row = array();
        if ($RFP["tipo"] == "I") {
            $valor = "Inclusão";
        } elseif ($RFP["tipo"] == "R") {
            $valor = "Exclusão";
        } elseif ($RFP["tipo"] == "M") {
            $valor = "Mensagem";
        } elseif ($RFP["tipo"] == "P") {
            $valor = "Liberou Pag.";
        } elseif ($RFP["tipo"] == "B") {
            $valor = "Bloqueou Pag.";
        }
        $row[] = "<center>" . $valor . "</center>";
        $row[] = "<center>" . escreverDataHora($RFP["data_lanc"]) . "</center>";
        $row[] = "<center>" . tableFormato($RFP["usuario"], 'T', '', '') . "</center>";
        $row[] = tableFormato($RFP["observacao"], 'T', '', '');
        $records['aaData'][] = $row;
    }
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}

if (isset($_GET['listTrancamento'])) {
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    $cur = odbc_exec($con, "select tipo, max(data) data, usuario, data_trancamento, trancarMotivo from sf_trancamento
    where fornecedores_despesas = " . $_GET['listTrancamento'] . " 
    group by tipo, usuario, data_trancamento, trancarMotivo order by data desc");    
    while ($RFP = odbc_fetch_array($cur)) {
        $cont = $cont + 1;
        $row = array();
        if ($RFP["tipo"] == "0") {
            $row[] = "<center>Cancelar</center>";
        } elseif ($RFP["tipo"] == "1") {
            $row[] = "<center>Incluir</center>";            
        }
        $row[] = "<center>" . escreverData($RFP["data_trancamento"]) . "</center>";
        $row[] = "<center>" . tableFormato($RFP["usuario"], 'T', '', '') . "</center>";
        $row[] = "<center>" . escreverDataHora($RFP["data"]) . "</center>";
        $row[] = "<center>" . tableFormato($RFP["trancarMotivo"], 'T', '', '') . "</center>";
        $records['aaData'][] = $row;
    }
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}