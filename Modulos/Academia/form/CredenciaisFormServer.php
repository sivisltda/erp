<?php

$disabled = 'disabled';
$id = '';
$descricao = '';
$horario = '';
$ambiente = '';
$dias = '';
$limitar_acesso = "0";
$qtd_limite_acesso = "0";
$qtd_limite_acesso_min = "0";
$inativo = "0";
$delDepVenc = "0";
$imprimir = 0;

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = 1;
}

if (isset($_POST['bntSave'])) {
    
    if (valoresCheck('txtLimitar_Acesso') == "1") {
        $qtd_limite_acesso = valoresNumericos('txtQtdLimiteAcesso');
        $qtd_limite_acesso_min = valoresNumericos('txtQtdLimiteAcessoMin');
    }
    
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_credenciais set " .
                        "descricao = UPPER(" . valoresTexto('txtDescricao') . ")," .
                        "id_turno = " . valoresSelect('txtHorario') . "," .
                        "id_ambiente_cred = " . valoresSelect('txtAmbiente') . "," .
                        "dias = " . valoresNumericos('txtDias') . "," .
                        "limitar_acesso = " . valoresCheck('txtLimitar_Acesso') . "," .
                        "qtd_limit_acesso = " . $qtd_limite_acesso . "," .
                        "qtd_limit_acesso_min = " . $qtd_limite_acesso_min . "," .
                        "inativo = " . valoresCheck('txtInativo') . "," .
                        "del_dep_venc = " . valoresCheck('txtDelDepVenc') . " " .
                        "where id_credencial = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_credenciais(descricao,id_turno,id_ambiente_cred,dias,
        limitar_acesso,qtd_limit_acesso,qtd_limit_acesso_min,inativo,del_dep_venc)values(UPPER(" .
                        valoresTexto('txtDescricao') . ")," .
                        valoresSelect('txtHorario') . "," .
                        valoresSelect('txtAmbiente') . "," .
                        valoresNumericos('txtDias') . "," .
                        valoresCheck('txtLimitar_Acesso') . "," .
                        $qtd_limite_acesso . "," .
                        $qtd_limite_acesso_min . "," .
                        valoresCheck('txtInativo') . "," .
                        valoresCheck('txtDelDepVenc') . ");") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_credencial from sf_credenciais order by id_credencial desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}

if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        if (isset($_POST['List'])) {
            include "../../../Connections/configini.php";
        }
        odbc_exec($con, "DELETE FROM sf_credenciais WHERE id_credencial = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox(1);</script>";
        exit();
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_credenciais where id_credencial =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_credencial'];
        $descricao = utf8_encode($RFP['descricao']);
        $horario = utf8_encode($RFP['id_turno']);
        $ambiente = utf8_encode($RFP['id_ambiente_cred']);
        $dias = utf8_encode($RFP['dias']);
        $limitar_acesso = $RFP['limitar_acesso'];
        $qtd_limite_acesso = $RFP['qtd_limit_acesso'];
        $qtd_limite_acesso_min = $RFP['qtd_limit_acesso_min'];  
        $inativo = $RFP['inativo'];
        $delDepVenc = $RFP['del_dep_venc'];
    }
} else {
    $disabled = '';
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
