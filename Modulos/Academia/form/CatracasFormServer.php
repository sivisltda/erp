<?php

$disabled = 'disabled';
$id = '';
$descricao = '';
$tipo = '';
$numinner = '1';
$porta = '';
$qtddigitos = '14';
$tpconexao = 3;
$tpEquip = 2;
$sentido = 'E';
$intervalo = '1';
$variante = '1';
$ip = '';
$imprimir = 0;

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = 1;
}

if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_catracas set " .
                        "nome_catraca = UPPER(" . valoresTexto('txtDescricao') . ")," .
                        "tipo_catraca = " . valoresSelect('txtTipo') . "," .
                        "num_inner_catraca = " . valoresSelect('txtNumInner') . "," .
                        "porta_catraca = " . valoresSelect('txtPorta') . "," .
                        "qtd_digitos_catraca = " . valoresSelect('txtQtdDigitos') . "," .
                        "tp_conexao_catraca = " . valoresSelect('txtTpConexao') . "," .
                        "tp_equipamento_catraca = " . valoresSelect('txtTpEquip') . "," .
                        "sentido_catraca = " . valoresTexto('txtSentido') . "," .
                        "intervalo_catraca = " . valoresNumericos('txtIntervalo') . ", " .
                        "variante_catraca = " . valoresNumericos('txtVariante') . ", " .
                        "ip_catraca = " . valoresTexto('txtIp') . " " .
                        "where id_catraca = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_catracas(nome_catraca,tipo_catraca, num_inner_catraca,porta_catraca,qtd_digitos_catraca, tp_conexao_catraca,tp_equipamento_catraca,sentido_catraca,intervalo_catraca,variante_catraca,ip_catraca)values
                        (UPPER(" . valoresTexto('txtDescricao') . ")," . valoresSelect('txtTipo') . "," . valoresSelect('txtNumInner') . "," . valoresSelect('txtPorta') . "," .
                        valoresSelect('txtQtdDigitos') . "," . valoresSelect('txtTpConexao') . "," . valoresSelect('txtTpEquip') . "," . valoresTexto('txtSentido') . ", " . valoresNumericos('txtIntervalo') . ", " . valoresNumericos('txtVariante') . ", " . valoresTexto('txtIp') . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_catraca from sf_catracas order by id_catraca desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}

if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        if (isset($_POST['List'])) {
            include "../../../Connections/configini.php";
        }
        odbc_exec($con, "DELETE FROM sf_catracas WHERE id_catraca = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox(1);</script>";
        exit();
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_catracas where id_catraca =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_catraca'];
        $descricao = utf8_encode($RFP['nome_catraca']);
        $tipo = utf8_encode($RFP['tipo_catraca']);
        $numinner = utf8_encode($RFP['num_inner_catraca']) !== "" ? utf8_encode($RFP['num_inner_catraca']) : $numinner;
        $porta = utf8_encode($RFP['porta_catraca']) !== "" ? utf8_encode($RFP['porta_catraca']) : $porta;
        $qtddigitos = utf8_encode($RFP['qtd_digitos_catraca']) !== "" ? utf8_encode($RFP['qtd_digitos_catraca']) : $qtddigitos;
        $tpconexao = utf8_encode($RFP['tp_conexao_catraca']) !== "" ? utf8_encode($RFP['tp_conexao_catraca']) : $tpconexao;
        $tpEquip = utf8_encode($RFP['tp_equipamento_catraca']) !== "" ? utf8_encode($RFP['tp_equipamento_catraca']) : $tpEquip;
        $sentido = utf8_encode($RFP['sentido_catraca']) !== "" ? utf8_encode($RFP['sentido_catraca']) : $sentido;
        $intervalo = utf8_encode($RFP['intervalo_catraca']) !== "" ? utf8_encode($RFP['intervalo_catraca']) : $sentido;
        $variante = utf8_encode($RFP['variante_catraca']) !== "" ? utf8_encode($RFP['variante_catraca']) : $sentido;
        $ip = utf8_encode($RFP['ip_catraca']);
    }
} else {
    $disabled = '';
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
