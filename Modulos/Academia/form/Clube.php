<?php

include "../../../Connections/configini.php";
require_once('../../../util/util.php');

if (isset($_POST['btnSave'])) {
    if (is_numeric($_POST['txtId'])) {
        $query = "set dateformat dmy;
        UPDATE sf_fornecedores_despesas SET " .
        "socio = " . valoresSelect("ckbClbSocio") . "," .
        "titular = " . valoresSelect("ckbClbTitular") . "," .
        "mala_direta = " . valoresSelect("ckbClbMalaDireta") . "," .
        "titularidade = " . valoresSelect("txtTitularidade") . "," .
        "tipo_socio = " . valoresSelect("ckbTipo") . "," .
        "gran_esp = " . valoresTexto("txtGradEsp") . "," .
        "nip = " . valoresTexto("txtNip") . "," .
        "depto = " . valoresTexto("txtDepto") . "," .
        "om = " . valoresTexto("txtOm") . "," .
        "tel_om = " . valoresTexto("txtTelOm") . "," .
        "ramal = " . valoresTexto("txtRamal") . "," .
        "local_cobranca = " . valoresTexto("txtLocalCobranca") . "," .
        "status_cracha = " . valoresSelect("ckbCracha") . "," .
        "validadeCarteira = " . valoresData("txtValidadeCarteira") . "," .                
        "trancar = " . valoresSelect("ckbTrancar") . "," .
        "trancarData = " . valoresData("txtDataTrancamento") . "," .
        "trancarMotivo = " . valoresTexto("txtMotivoTrancamento") . "," .                
        "funcao_contato = " . valoresTexto("txtContFunc") . " " .                
        " WHERE id_fornecedores_despesas = " . $_POST['txtId'] . ";";
        odbc_exec($con, $query) or die(odbc_errormsg());
        
        $query_dependente = "update sf_fornecedores_despesas set trancar = " . valoresSelect("ckbTrancar") . "
        where id_fornecedores_despesas in (select id_dependente from sf_fornecedores_despesas_dependentes 
        where id_titular = " . $_POST['txtId'] . ")";
        odbc_exec($con, $query_dependente) or die(odbc_errormsg()); 
        
        if (valoresSelect("ckbTrancar") != valoresSelect("ckbTrancarOld")) {
            odbc_exec($con, "set dateformat dmy; insert into sf_trancamento (fornecedores_despesas, tipo, data, usuario, data_trancamento, trancarMotivo)
            values (" . $_POST['txtId'] . ", " . valoresSelect("ckbTrancar") . ", GETDATE(), '" . $_SESSION["login_usuario"] . "', " . 
            valoresData("txtDataTrancamento") . ", " . valoresTexto("txtMotivoTrancamento") . ")");
            
            odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
            values ('sf_fornecedores_despesas', null, '" . $_SESSION["login_usuario"] . "', 'T', " . 
            valoresTexto2((valoresSelect("ckbTrancar") == 1 ? "Incluir" : "Cancelar") . " - " . $_POST['txtMotivoTrancamento']) . ", GETDATE(), " . $_POST['txtId'] . ")");            
            if (valoresSelect("ckbTrancar") == 1) {                  
                odbc_exec($con, "update sf_fornecedores_despesas set email_marketing = 0, celular_marketing = 0 where id_fornecedores_despesas = " . $_POST['txtId']);                
                odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
                select 'sf_agendamento', id_agendamento, '" . $_SESSION["login_usuario"] . "', 'E', 'EXCLUIR - AGENDAMENTO POR TRANCAMENTO', GETDATE(), id_fornecedores
                from sf_agendamento where id_fornecedores = " . $_POST['txtId'] . " and inativo = 0 and cast(getdate() as date) < data_inicio");                
                $query = "UPDATE sf_agendamento SET inativo = 1 WHERE id_fornecedores = " . $_POST['txtId'] . " and inativo = 0 and cast(getdate() as date) < data_inicio";
                odbc_exec($con, $query) or die(odbc_errormsg());
            }
        }
        echo "YES";
    }
}

odbc_close($con);
