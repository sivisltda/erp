<?php

include "../../../Connections/configini.php";
require_once('../../../util/util.php');

$mdl_aca_ = returnPart($_SESSION["modulos"], 6);
$mdl_cnt_ = returnPart($_SESSION["modulos"], 13);
$mdl_seg_ = returnPart($_SESSION["modulos"], 14);

$horario = '';
if ($_POST['sys'] == 1) {
    $horario = '0';
} else {
    $horario = $_POST['txtHorarios'];
}

if (isset($_GET['listPlanos'])) {
    if ($_GET['tpTela'] == 'A') {
        $query = "SELECT conta_produto,A.descricao, inativa, '' id_agendamento from sf_produtos A where A.tipo = 'C' and A.conta_produto > 0 and A.data_inativo is null
        and conta_produto not in (select A.id_prod_plano from sf_vendas_planos A
        inner join sf_produtos on A.id_prod_plano = sf_produtos.conta_produto
        where favorecido = " . $_GET['listPlanos'] . " and A.dt_cancelamento is null and
       ((DATEADD(day,(select ACA_PLN_DIAS_RENOVACAO from sf_configuracao),A.dt_fim) > GETDATE() and sf_produtos.parcelar = 0))) and inativa = 0 order by descricao";
    } elseif ($_GET['tpTela'] == 'T') {
        $query = "SELECT conta_produto,A.descricao, inativa, '' id_agendamento from sf_produtos A where A.tipo = 'C' and A.conta_produto > 0 and A.data_inativo is null
        and conta_produto not in (select A.id_prod_plano from sf_vendas_planos A
        inner join sf_produtos on A.id_prod_plano = sf_produtos.conta_produto
        where id_plano = " . $_GET['listPlanos'] . ") and inativa = 0 order by descricao";
    } elseif ($_GET['tpTela'] == 'R') {
        $query = "SELECT conta_produto,A.descricao, inativa, isnull((select dt_cancel_agendamento from sf_vendas_planos_dcc_agendamento where id_plano_agendamento =  " . $_GET['idItem'] . "),'') id_agendamento
        from sf_produtos A where A.tipo = 'C' and A.data_inativo is null
        and conta_produto in (select A.id_prod_plano from sf_vendas_planos A where A.id_plano =  " . $_GET['idItem'] . ") order by descricao";
    }
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["id"] = utf8_encode($RFP["conta_produto"]);
        $row["descricao"] = utf8_encode($RFP["descricao"]);
        $row["inativa"] = utf8_encode($RFP["inativa"]);
        $row["id_agendamento"] = utf8_encode($RFP["id_agendamento"]);
        $records[] = $row;
    }
    //echo $query;exit();
    echo json_encode($records);
}

if (isset($_GET['listHorarios'])) {
    $id_turno = "";
    $cur = odbc_exec($con, "select ISNULL(id_turno,0) id_turno from sf_produtos where conta_produto = " . $_GET['idPlano']);
    while ($RFP = odbc_fetch_array($cur)) {
        $id_turno = $RFP['id_turno'];
    }
    if ($id_turno !== "0") {
        $query = "select cod_turno,nome_turno from sf_turnos where cod_turno = (select id_turno from sf_produtos where conta_produto = " . $_GET['idPlano'] . ")";
    } else {
        $query = "select cod_turno,nome_turno  from sf_turnos where cod_turno > 0 ";
    }
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["cod_turno"] = utf8_encode($RFP["cod_turno"]);
        $row["nome_turno"] = utf8_encode($RFP["nome_turno"]);
        $records[] = $row;
    }
    echo json_encode($records);
}

if (isset($_GET['prazoTroca'])) {
    $cur = odbc_exec($con, "select id_parcela,parcela,valor_unico from sf_produtos_parcelas inner join sf_produtos
    on sf_produtos.conta_produto = sf_produtos_parcelas.id_produto
    where id_produto = " . $_GET['idPlano'] . " order by parcela");
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["cod_produto"] = utf8_encode($RFP["id_parcela"]);
        $row["nome_produto"] = ($RFP["parcela"] == -2 ? "PIX" : ($RFP["parcela"] == -1 ? "BOL" : ($RFP["parcela"] == 0 ? "DCC" : utf8_encode($RFP["parcela"]) .
        ($RFP["parcela"] > 1 ? " Meses" : " Mês")))) . " " . escreverNumero($RFP["valor_unico"], 1);
        $row["par_produto"] = $RFP["parcela"];
        $row["val_produto"] = escreverNumero($RFP["valor_unico"]);
        $records[] = $row;
    }
    echo json_encode($records);
}

if (isset($_POST['trocarPlano'])) {
    odbc_autocommit($con, false);
    $result = odbc_exec($con, "set dateformat dmy; insert into sf_vendas_planos (id_turno, dt_inicio, dt_fim, favorecido, dias_ferias, dt_cadastro,usuario_resp, id_prod_plano) values
    (" . valoresSelect('txtHorario') . "," . valoresData('txtDtIni') . "," . valoresData('txtDtIni') . "," . valoresNumericos('txtId') . "," .
    "(select isnull(max(dias),0) from sf_ferias where meses in (select parcela from sf_produtos_parcelas where id_parcela = " . valoresSelect('txtPrazo') . "))," .
    "getdate()," . $_SESSION["id_usuario"] . "," . valoresSelect('idPlanoNovo') . "); SELECT SCOPE_IDENTITY() ID;") or die(odbc_errormsg());
    odbc_next_result($result);
    $codplano = odbc_result($result, 1);    
    $result = odbc_exec($con, "set dateformat dmy; insert into sf_vendas_planos_mensalidade(id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens)
    values (" . $codplano . "," . valoresData('txtDtIni') . "," . valoresData('txtDtFim') . "," . valoresSelect('txtPrazo') . ",getdate(), " . valoresNumericos('txtCredito') . "); SELECT SCOPE_IDENTITY() ID;") or die(odbc_errormsg());
    odbc_next_result($result);
    $id_mens = odbc_result($result, 1);    
    //----------------------Proxima-Parcela-------------------------------------------------------------------        
    $result = odbc_exec($con, "set dateformat dmy; insert into sf_vendas_planos_mensalidade (id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens)
    select id_plano_mens, dateadd(day,1,dt_fim_mens),
    case when tp_valid_acesso = 'C' then dateadd(day, qtd_dias_acesso,dt_fim_mens) else 
        dateadd(day,-1, dateadd(month,1,dateadd(day,1,dt_fim_mens)))
    end,
    id_parc_prod_mens, GETDATE(),valor_unico valor_mens
    from sf_vendas_planos_mensalidade m1 inner join sf_produtos_parcelas pp on m1.id_parc_prod_mens = pp.id_parcela
    inner join sf_produtos p on p.conta_produto = pp.id_produto
    where p.inativar_renovacao = 0 and id_mens = " . $id_mens . " and parcela in (-2,-1,0,1) and (select COUNT(*) from sf_vendas_planos_mensalidade m2
    where m2.id_plano_mens = m1.id_plano_mens and m2.id_parc_prod_mens = m1.id_parc_prod_mens
    and ((tp_valid_acesso <> 'C' and month(m2.dt_inicio_mens) = month(dateadd(month,1,m1.dt_inicio_mens)) and year(m2.dt_inicio_mens) = year(dateadd(month,1,m1.dt_inicio_mens))
    ) or (tp_valid_acesso = 'C' and dateadd(day,1,m1.dt_fim_mens) = dt_inicio_mens))) = 0; SELECT SCOPE_IDENTITY() ID;") or die(odbc_errormsg());
    odbc_next_result($result);    
    $id_mens_renov = odbc_result($result, 1); 
    //----------------------Pro-Rata-Mensalidade--------------------------------------------------------------
    $cur = odbc_exec($con, "set dateformat dmy;select dateadd(day, isnull((select day(max(dt_fim_mens)) from sf_vendas_planos_mensalidade where id_item_venda_mens is not null and 
    id_plano_mens = " . valoresNumericos('idPlano') . "),0), dateadd(day, (-1 * day(dt_fim_mens)), dt_fim_mens)) dt_fim_mens from sf_vendas_planos_mensalidade where id_mens = " . $id_mens_renov);
    while ($RFP = odbc_fetch_array($cur)) {
        $_POST['atualizaMensalidade'] = $id_mens_renov;
        $_POST['txtValorMens'] = $_POST['txtValor'];
        $_POST['txtDataMens'] = date_format(date_create($RFP["dt_fim_mens"]), "d_m_Y");
    }
    //----------------------------------------------Baixar-Credito-----------------------------------------------------------------------------------
    $result = odbc_exec($con, "insert into sf_vendas (vendedor,cliente_venda,historico_venda,data_venda,sys_login,empresa,destinatario,status,tipo_documento,
    cov,descontop,descontos,descontotp,descontots,n_servicos,data_aprov,grupo_conta,conta_movimento,favorito,reservar_estoque, n_produtos, comentarios_venda)
    values ('" . $_SESSION["login_usuario"] . "'," . valoresNumericos('txtId') . ",'" . utf8_decode('ABONO / TROCA DE PLANO') . "',getdate(),'" . $_SESSION["login_usuario"] . "',1,'" . $_SESSION["login_usuario"] . "','Aprovado',null,
    'V',0.00,0.00,0,0,0,getdate(),14,1,0,0,0,'Troca de Plano') SELECT SCOPE_IDENTITY() ID;") or die(odbc_errormsg());
    odbc_next_result($result);
    $idVenda = odbc_result($result, 1);
    $query .= "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total,valor_bruto)
    values (" . $idVenda . ",(select max(conta_movimento) from sf_produtos where conta_produto = " . valoresSelect('idPlanoNovo') . ")," . valoresSelect('idPlanoNovo') . ",1," . valoresNumericos('txtCredito') . "," . valoresNumericos('txtCredito') . ");";
    $query .= "insert into sf_venda_parcelas (venda, numero_parcela, data_parcela, valor_parcela, valor_pago, data_pagamento,id_banco, pa, inativo, historico_baixa, valor_multa, valor_juros, syslogin, tipo_documento, valor_parcela_liquido,data_cadastro,exp_remessa, somar_rel)
    values (" . $idVenda . ",'1'," . valoresData('txtDtIni') . "," . valoresNumericos('txtCredito') . "," . valoresNumericos('txtCredito') . ",getDate(),1,'01/01',0,'" . utf8_decode('TROCA DE PLANO ' . $_POST['txtDescrPlano']) . "',0.00,0.00,'" . $_SESSION["login_usuario"] . "',12,0,getdate(), 0, 0)";
    $query .= "update sf_venda_parcelas set valor_parcela_liquido = [dbo].[FU_TAXA_FORMA_PAGAMENTO](sf_venda_parcelas.tipo_documento,sf_venda_parcelas.venda, 0, valor_parcela) where venda = " . $idVenda . ";";      
    $query .= "update sf_vendas set conta_movimento = [dbo].[FU_SUB_GRUPO_VENDA](id_venda) where cov = 'V' and id_venda = " . $idVenda . ";";    
    $query .= "insert into sf_creditos (id_fornecedor_despesa, valor, data, tipo, id_venda, historico, dt_vencimento, usuario_resp, id_venda_quitacao, inativo) values
    (" . valoresNumericos('txtId') . ", " . valoresNumericos('txtCredito') . ", getdate(), 'D'," . $idVenda . ",'" . utf8_decode('PAG PLANO/SERVICO') . "'," . valoresData('txtDtIni') . " ," . $_SESSION["id_usuario"] . "," . $idVenda . ",0);";
    $query .= "update sf_vendas_planos_mensalidade set id_item_venda_mens = (select id_item_venda from sf_vendas_itens where id_venda = " . $idVenda . "),
    dt_pagamento_mens = cast(getdate() as date) where id_mens = " . $id_mens;    
    $query .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
    ('sf_vendas', " . $idVenda . ", '" . $_SESSION["login_usuario"] . "', 'B', 'TROCA DE PLANO, ABONO: " . $idVenda . "', GETDATE()," . valoresNumericos('txtId') . ");";
    $query .= "update sf_vendas_planos set dt_fim = dbo.FU_PLANO_FIM(id_plano) where id_plano = " . $codplano;    
    //----------------------Plano-Anterior--------------------------------------------------------------------    
    $query .= "insert into sf_creditos (id_fornecedor_despesa, valor, data, tipo, id_plano, id_solicitacao_autorizacao, historico, usuario_resp, inativo) values
    (" . valoresNumericos('txtId') . ", " . valoresNumericos('txtCredito') . ", getdate(), 'C'," . valoresNumericos('idPlano') . ",null,'" . utf8_decode('TROCA DE PLANO ' . $_POST['txtDescrPlano']) . "' ," . $_SESSION["id_usuario"] . ",0);";
    $query .= "update sf_vendas_planos set dt_cancelamento = getdate(), obs_cancelamento = 'Troca de Plano', usuario_canc = " . $_SESSION["id_usuario"] . " where id_plano = " . valoresNumericos('idPlano') . ";";
    $query .= "update sf_venda_parcelas set inativo = 2 where tipo_documento = 12 and data_pagamento is null
    and venda in (select id_venda from sf_vendas_planos
    inner join sf_vendas_planos_mensalidade on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens
    inner join sf_vendas_itens on sf_vendas_planos_mensalidade.id_item_venda_mens = sf_vendas_itens.id_item_venda
    where id_plano = " . valoresNumericos('idPlano') . ");";
    $query .= "update sf_creditos set inativo = 2 where id_venda_quitacao is null and id_venda in (select id_venda from sf_vendas_planos
    inner join sf_vendas_planos_mensalidade on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens
    inner join sf_vendas_itens on sf_vendas_planos_mensalidade.id_item_venda_mens = sf_vendas_itens.id_item_venda
    where id_plano = " . valoresNumericos('idPlano') . ");";
    $query .= "delete from sf_vendas_planos_mensalidade where dt_pagamento_mens is null and id_plano_mens = " . valoresNumericos('idPlano');
    $objExec = odbc_exec($con, $query) or die(odbc_errormsg());
    if (!$objExec) {       
        odbc_rollback($con);
        echo "ERROR"; 
        exit();
    }
    if ($objExec) {       
        odbc_commit($con);
        echo "YES";                
    }
}

if (isset($_GET['listValores'])) {    
    $acess = 0;
    $rs = odbc_exec($con, "select dbo.FU_VALOR_ACESSORIOS(" . $_GET['txtIdPlano'] . ") preco_venda");
    while ($dados_acres = odbc_fetch_array($rs)) {
        $acess = $acess + $dados_acres["preco_venda"];
    }    
    $cur = odbc_exec($con, "SELECT id_parcela, parcela, valor_unico,  B.valor_desconto, (valor_unico - (valor_unico * B.valor_desconto)/100) valor_final,
    ((valor_unico - (valor_unico * B.valor_desconto)/100) /(case when parcela in (-2,-1,0) then 1 else parcela end)) valor_parcela
    from sf_produtos A left join sf_produtos_parcelas B on A.conta_produto = B.id_produto
    where id_produto > 0 and id_produto = " . $_GET['listValores'] . " order by parcela");
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    while ($RFP = odbc_fetch_array($cur)) {
        $acess = $acess * ($RFP['parcela'] == 0 || $RFP['parcela'] == -1 || $RFP['parcela'] == -2 ? 1 : $RFP['parcela']);        
        $cont = $cont + 1;
        $row = array();
        $row[] = "<div style=\"line-height: 14px;\" title='" . ($RFP['parcela'] == 0 || $RFP['parcela'] == -1 || $RFP['parcela'] == -2 ? '1' : $RFP['parcela']) . "'><center><b>" . ($RFP['parcela'] == -2 ? 'PIX' :($RFP['parcela'] == -1 ? 'BOL' : ($RFP['parcela'] == 0 ? 'DCC' : $RFP['parcela']))) . "</b></center><input type=\"hidden\" data-id=\"" . $RFP['id_parcela'] . "\"/></div>";
        $row[] = "<div title='" . escreverNumero($RFP['valor_unico']) . "' style=\"text-align:right;\">" . escreverNumero($RFP['valor_unico'] + $acess) . "</div>";
        $row[] = "<div title='" . escreverNumero($RFP['valor_final']) . "' style=\"text-align:right;\">" . escreverNumero($RFP['valor_final'] + $acess) . "</div>";
        $row[] = "<div title='" . ($RFP['parcela'] == 0 || $RFP['parcela'] == -1 || $RFP['parcela'] == -2 ? '1' : $RFP['parcela']) . " x " . escreverNumero($RFP['valor_parcela'] + $acess) . "' style=\"text-align:right;\">" . ($RFP['parcela'] == 0 || $RFP['parcela'] == -1 || $RFP['parcela'] == -2 ? '1' : $RFP['parcela']) . " x <span style=\"font-size:14px;\"><b> " . escreverNumero($RFP['valor_parcela'] + ($acess / ($RFP['parcela'] == 0 || $RFP['parcela'] == -1 || $RFP['parcela'] == -2 ? 1 : $RFP['parcela']))) . " </b></span></div>";
        $row[] = "<div title='" . escreverNumero($RFP['valor_desconto']) . "' style=\"text-align:center;\">" . escreverNumero($RFP['valor_desconto']) . "</div>";
        $records['aaData'][] = $row;
    }
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}

if (isset($_POST['renovarContrato'])) {
    $data_cancelamento = "";
    $query = "set dateformat dmy; select top 1 dateadd(day,1,dt_fim_mens) dt_fim, 
    id_plano, favorecido, id_prod_plano, id_turno, id_parc_prod_mens, dt_cancelamento
    from sf_vendas_planos inner join sf_vendas_planos_mensalidade on id_plano_mens = id_plano
    where id_plano = " . $_POST['renovarContrato'] . " order by dt_fim_mens desc";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $_POST['txtIdPlano'] = $RFP["id_plano"];
        $_POST['txtId'] = $RFP["favorecido"];
        $_POST['txtProduto'] = $RFP["id_prod_plano"];
        $_POST['txtHorarios'] = $RFP["id_turno"];
        $_POST['txtIdParc'] = $RFP["id_parc_prod_mens"];
        $_POST['txtDtIni'] = escreverData($RFP["dt_fim"]);
        $_POST['addContrato'] = "S";
        $_POST['tpTela'] = "R";
        $_POST['descr'] = "CONTRATO TAXA MANUTENÇÃO";
        $horario = $_POST['txtHorarios'];
        $data_cancelamento = escreverData($RFP["dt_cancelamento"]);        
    }
    if(strlen($data_cancelamento) > 0){
        echo "Plano cancelado em " . $data_cancelamento . ", não é possível renovar!"; exit;
    }
}

if (isset($_POST['renovarContratoAnt'])) {
    $data_cancelamento = "";
    $query = "set dateformat dmy; select top 1 dateadd(month,-1,dt_inicio_mens) dt_ini, 
    id_plano, favorecido, id_prod_plano, id_turno, id_parc_prod_mens, dt_cancelamento
    from sf_vendas_planos inner join sf_vendas_planos_mensalidade on id_plano_mens = id_plano
    where id_plano = " . $_POST['renovarContratoAnt'] . " order by dt_fim_mens";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $_POST['txtIdPlano'] = $RFP["id_plano"];
        $_POST['txtId'] = $RFP["favorecido"];
        $_POST['txtProduto'] = $RFP["id_prod_plano"];
        $_POST['txtHorarios'] = $RFP["id_turno"];
        $_POST['txtIdParc'] = $RFP["id_parc_prod_mens"];
        $_POST['txtDtIni'] = escreverData($RFP["dt_ini"]);
        $_POST['addContrato'] = "S";
        $_POST['tpTela'] = "R";
        $_POST['descr'] = "CONTRATO TAXA MANUTENÇÃO";
        $horario = $_POST['txtHorarios'];
        $data_cancelamento = escreverData($RFP["dt_cancelamento"]);        
    }
    if(strlen($data_cancelamento) > 0){
        echo "Plano cancelado em " . $data_cancelamento . ", não é possível renovar!"; exit;
    }
}

if (isset($_POST['addContrato'])) {
    odbc_autocommit($con, false);
    $parcela = 0;    
    $dtIni = str_replace('_', '/', $_POST['txtDtIni']); //escreverDataSoma($dtIni,"");
    $cur = odbc_exec($con, "select A.*, isnull((select dias from sf_ferias where meses = (case when A.parcela in (-2,-1,0) then 1 else A.parcela end)),0) dias,B.parcelar gera_parc,
    case when B.parcelar = 1 then (A.valor_unico - (A.valor_unico * A.valor_desconto)/100)/(case when parcela in (-2,-1,0) then 1 else parcela end) else (A.valor_unico - (A.valor_unico * A.valor_desconto)/100) end valor_mens,
    tp_valid_acesso, qtd_dias_acesso from sf_produtos_parcelas A inner join sf_produtos B on A.id_produto = B.conta_produto where id_parcela = " . $_POST['txtIdParc']);
    while ($RFP = odbc_fetch_array($cur)) {
        $parcela = $RFP['parcela'];        
        $valor_mens = $RFP['valor_mens'];
        if ($RFP['tp_valid_acesso'] == "C") { //DIAS CORRIDOS
            $dias = 0;
            $quantparc = 0;
            $geraparc = 0;
            $dtFim = escreverDataSoma($dtIni, " + " . ($RFP['qtd_dias_acesso'] - 1) . " day");
        } else {
            $dias = $RFP['dias'];
            $quantparc = $RFP['parcela'] == 0 || $RFP['parcela'] == -1 || $RFP['parcela'] == -2 ? 1 : $RFP['parcela'];
            $geraparc = $RFP['gera_parc'];
            $dtFim = escreverDataSoma($dtIni, " + " . ($RFP['parcela'] == 0 || $RFP['parcela'] == -1 || $RFP['parcela'] == -2 ? 1 : $RFP['parcela']) . " month -1 day");
        }
    }
    if ($_POST['tpTela'] == "A") {
        $query = "set dateformat dmy;insert into sf_vendas_planos (id_turno, dt_inicio, dt_fim, favorecido, dias_ferias, dt_cadastro, usuario_resp, id_prod_plano) values
        (" . $horario . ", " . valoresData2($dtIni) . "," . valoresData2($dtIni) . "," . $_POST['txtId'] . ", " . valoresNumericos2($dias) . ", getdate(), " . $_SESSION["id_usuario"] . ", " . $_POST['txtProduto'] . ");
        SELECT SCOPE_IDENTITY() ID;";
        $result = odbc_exec($con, $query) or die(odbc_errormsg());
        odbc_next_result($result);
        $codplano = odbc_result($result, 1);
    } else {
        $codplano = $_POST['txtIdPlano'];
        $query = "set dateformat dmy;update sf_vendas_planos set dt_fim = dbo.FU_PLANO_FIM(id_plano),dias_ferias = " . valoresNumericos2($dias) . ",
        id_turno = " . $_POST['txtHorarios'] . " where id_plano = " . $codplano;
        $objExec = odbc_exec($con, $query);
    }
    $query = "";
    if ($geraparc == "1") { //Geração de várias mensalidades em aberto
        $query = $query . "set dateformat dmy;";
        for ($i = 0; $i <= $quantparc - 1; $i++) {
            $query .= "insert into sf_vendas_planos_mensalidade(id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens) values
            (" . $codplano . ", dateadd(month," . $i . "," . valoresData2($dtIni) . "), dateadd(day,-1,dateadd(month," . ($i + 1) . "," . valoresData2($dtIni) . "))," . $_POST['txtIdParc'] . ",getdate()," . $valor_mens . ");";
        }
        $query .= "SELECT SCOPE_IDENTITY() ID;";        
    } else { //Gera 1 mensalidade            
        $query = "set dateformat dmy;
        IF NOT EXISTS (select * from sf_vendas_planos_mensalidade where id_plano_mens = " . $codplano . " and dt_inicio_mens = " . valoresData2($dtIni) . " and dt_fim_mens = " . valoresData2($dtFim) . " and id_item_venda_mens is null) BEGIN
            insert into sf_vendas_planos_mensalidade(id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens) values
            (" . $codplano . ", " . valoresData2($dtIni) . ", " . valoresData2($dtFim) . ", " . $_POST['txtIdParc'] . ",getdate()," . $valor_mens . ");
            SELECT SCOPE_IDENTITY() ID;            
        END ELSE BEGIN
            update sf_vendas_planos_mensalidade set id_parc_prod_mens = " . $_POST['txtIdParc'] . ", valor_mens = " . $valor_mens . " where id_plano_mens = " . $codplano . " and dt_inicio_mens = " . valoresData2($dtIni) . " and dt_fim_mens = " . valoresData2($dtFim) . " and id_item_venda_mens is null;
            select id_mens from sf_vendas_planos_mensalidade where id_plano_mens = " . $codplano . " and dt_inicio_mens = " . valoresData2($dtIni) . " and dt_fim_mens = " . valoresData2($dtFim) . " and id_item_venda_mens is null;
        END";                
    }
    //echo $query; exit;
    $codmensalidade = "0";
    $objExec = odbc_exec($con, $query) or die(odbc_errormsg());
    if (!$objExec) {
        odbc_rollback($con);
        echo "ERROR";
        exit();
    }
    if ($objExec) {
        odbc_next_result($objExec);
        $codmensalidade = odbc_result($objExec, 1);
        odbc_commit($con);
        echo $codplano;
    }
    if ($geraparc == "1") { //Pega a 1ª mensalidades para gerar Pro-rata
        $cur = odbc_exec($con, "select isnull(min(id_mens),0) mensalidade from sf_vendas_planos_mensalidade where dt_pagamento_mens is null and id_plano_mens = " . $codplano);
        while ($RFP = odbc_fetch_array($cur)) {
            $codmensalidade = $RFP['mensalidade'];
        }
    }
    $cur = odbc_exec($con, "select ACA_TIPO_VENCIMENTO, ACA_DIA_VENCIMENTO,
    (select max(dia) dia from sf_configuracao_vencimentos where " . substr($dtIni, 0, 2) . " between de and ate and dia = " . substr($dtIni, 0, 2) . ") DIA_CONDICIONAL
    from sf_configuracao");
    while ($RFP = odbc_fetch_array($cur)) {
        $diaVen = str_pad($RFP['ACA_DIA_VENCIMENTO'], 2, "0", STR_PAD_LEFT);
        if ($RFP['ACA_TIPO_VENCIMENTO'] == "1" && $diaVen != substr($dtIni, 0, 2) && $parcela < 2) {
            $_POST['atualizaMensalidade'] = $codmensalidade;
            $_POST['txtValorMens'] = escreverNumero($valor_mens);
            $_POST['txtDataMens'] = str_replace("/", "_", ($diaVen < substr($dtIni, 0, 2) ? escreverDataSoma(($diaVen . substr($dtIni, 2)), " +1 month -1 day ") : escreverDataSoma(($diaVen . substr($dtIni, 2)), " -1 day ")));
        } else if ($RFP['ACA_TIPO_VENCIMENTO'] == "2" && is_numeric($RFP['DIA_CONDICIONAL']) 
            && substr($RFP['DIA_CONDICIONAL'], 0, 2) != substr($dtIni, 0, 2) && $parcela < 2) {
            $_POST['atualizaMensalidade'] = $codmensalidade;
            $_POST['txtValorMens'] = escreverNumero($valor_mens);
            $_POST['txtDataMens'] = str_replace("/", "_", ($RFP['DIA_CONDICIONAL'] < substr($dtIni, 0, 2) ? escreverDataSoma(($RFP['DIA_CONDICIONAL'] . substr($dtIni, 2)), " +1 month -1 day ") : escreverDataSoma(($RFP['DIA_CONDICIONAL'] . substr($dtIni, 2)), " -1 day ")));
        }
    }
}

if (isset($_POST['atuContrato'])) {
    $query = "update sf_vendas_planos  set id_turno = " . $_POST['txtHorarioDet'] . " where id_plano = " . $_POST['atuContrato'];
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}

if (isset($_POST['removerContrato'])) {
    $queryTurma = "";
    $cur = odbc_exec($con, "select count(*) total_pago from sf_vendas_planos_mensalidade where id_plano_mens = " . $_POST['removerContrato'] . " and id_item_venda_mens is not null");
    while ($RFP = odbc_fetch_array($cur)) {
        $totalPago = $RFP['total_pago'];
    }
    if ($totalPago > 0) {
        echo "PAGAMENTO";
    } else {
        $cur = odbc_exec($con, "select favorecido, id_prod_plano, isnull(id_turma,0)id_turma from sf_vendas_planos A
        left join sf_fornecedores_despesas_turmas B on A.id_plano = B.id_plano where A.id_plano = " . $_POST['removerContrato']);
        while ($RFP = odbc_fetch_array($cur)) {
            $idCliente = $RFP['favorecido'];
            $idPlano = $RFP['id_prod_plano'];
            if ($RFP['id_turma'] !== "0") {
                $queryTurma .= "insert into sf_turmas_desmatriculas (data,id_fornecedores_despesas,id_turma,descricao_turma,motivo,login_user) values(getdate()," . $idCliente . "," . $RFP['id_turma'] . ",(select descricao from sf_turmas where id_turma = " . $RFP['id_turma'] . "),'EXCLUSAO PLANO','" . $_SESSION["login_usuario"] . "');";
            }
        }
        $cur = odbc_exec($con, "select id_turma from sf_fornecedores_despesas_turmas where id_fornecedores_despesas = " . $_POST['removerContrato'] . " and id_turma is not null");
        while ($RFP = odbc_fetch_array($cur)) {
            $totalPago = $RFP['total_pago'];
        }
        $query = "set dateformat dmy;
        BEGIN TRANSACTION " . $queryTurma . "
            insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values ('sf_vendas_planos', " . $_POST['removerContrato'] . ", '" . $_SESSION["login_usuario"] . "', 'R', 'EXCLUSAO PLANO_PRODUTO: " . $idPlano . "', GETDATE(), " . $idCliente . ");
            delete from sf_vendas_planos_mensalidade where id_plano_mens = " . $_POST['removerContrato'] . ";
            delete from sf_vendas_planos_ferias where id_plano = " . $_POST['removerContrato'] . ";
            delete from sf_vendas_planos_acessorios where id_venda_plano = " . $_POST['removerContrato'] . ";   
            delete from sf_convenios_planos where id_convenio_planos in (select id_convenio from sf_convenios where id_conv_plano = " . $_POST['removerContrato'] . ");
            delete from sf_fornecedores_despesas_convenios where convenio in (select id_convenio from sf_convenios where id_conv_plano = " . $_POST['removerContrato'] . ");                
            delete from sf_convenios where id_conv_plano = " . $_POST['removerContrato'] . ";
            delete from sf_vendas_planos where id_plano = " . $_POST['removerContrato'] . ";
        IF @@ERROR = 0
        COMMIT
        ELSE
        ROLLBACK;";
        odbc_exec($con, $query) or die(odbc_errormsg());
        echo "YES";
    }
}

if (isset($_GET['listPagMens'])) {
    $records = array();
    $query = "set dateformat dmy; select month(dt_inicio_mens) mes, year(dt_inicio_mens) ano from sf_vendas_planos A
    inner join sf_vendas_planos_mensalidade B on B.id_plano_mens = A.id_plano
    inner join sf_produtos C on A.id_prod_plano = C.conta_produto
    where A.dt_cancelamento is null and B.dt_pagamento_mens is null and 
    ((DATEADD(day,(select ACA_PLN_DIAS_RENOVACAO from sf_configuracao),A.dt_fim) > GETDATE() and C.parcelar = 0) or
    (A.dt_fim >= GETDATE() and C.parcelar = 1) or ((select COUNT(*) from sf_vendas_planos_mensalidade C where C.id_plano_mens = A.id_plano and dt_pagamento_mens is null) > 0))
    and favorecido not in (select id_dependente from sf_fornecedores_despesas_dependentes)
    AND favorecido = " . $_GET['listPagMens'] . " union select month(dt_inicio_mens) mes, year(dt_inicio_mens) ano from sf_fornecedores_despesas_dependentes D
    inner join sf_vendas_planos A on A.favorecido = D.id_dependente
    inner join sf_vendas_planos_mensalidade B on B.id_plano_mens = A.id_plano
    inner join sf_produtos C on A.id_prod_plano = C.conta_produto
    where A.dt_cancelamento is null and B.dt_pagamento_mens is null and 
    ((DATEADD(day,(select ACA_PLN_DIAS_RENOVACAO from sf_configuracao),A.dt_fim) > GETDATE() and C.parcelar = 0) or
    (A.dt_fim >= GETDATE() and C.parcelar = 1) or ((select COUNT(*) from sf_vendas_planos_mensalidade C where C.id_plano_mens = A.id_plano and dt_pagamento_mens is null) > 0))
    AND D.id_titular = " . $_GET['listPagMens'] . " order by 2, 1";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $records[] = str_pad($RFP["mes"], 2, "0", STR_PAD_LEFT) . "/" . $RFP["ano"];
    }
    echo json_encode($records);
}

if (isset($_GET['getDtRenovacao'])) {
    $query = "set dateformat dmy; select dateadd(day, 1,max(dt_fim)) dt_fim from sf_vendas_planos A
    where id_prod_plano = " . $_GET['idPlano'] . " and favorecido = " . $_GET['getDtRenovacao'] . " and dt_cancelamento is null";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["dtRenovacao"] = escreverData($RFP["dt_fim"]);
        $records[] = $row;
    }
    echo json_encode($records);
}

if (isset($_GET['listPlanosCliente']) && is_numeric($_GET['listPlanosCliente'])) {
    $whereTurma = "";
    if (isset($_GET['isTurma'])) {
        $whereTurma = " and B.necessita_turma = 1 ";
    }

    $query = "update sf_vendas_planos set planos_status = dbo.FU_STATUS_PLANO_CLI(id_plano) where favorecido = " . $_GET['listPlanosCliente'];
    $cur = odbc_exec($con, $query);

    $query = "select B.conta_produto, B.descricao, C.finito_agendamento, 
    (select top 1 parcela from sf_vendas_planos_mensalidade PM inner join sf_produtos_parcelas PP on PM.id_parc_prod_mens = PP.id_parcela where PM.id_plano_mens = A.id_plano order by PM.dt_fim_mens desc) parcela, 
    A.dt_inicio, A.dt_fim, planos_status status, A.id_plano, B.parcelar gera_parc, C.dt_cancel_agendamento, D.bloqueado, A.dt_cancelamento, B.bloq_congelamento, V.placa, V.modelo, 
    STUFF((SELECT distinct ', ' + t.descricao from sf_fornecedores_despesas_turmas ct inner join sf_turmas t on t.id_turma = ct.id_turma where ct.id_plano = A.id_plano FOR XML PATH('')), 1, 1, '') descricao_turma
    from sf_vendas_planos A inner join sf_produtos B on A.id_prod_plano = B.conta_produto
    inner join sf_fornecedores_despesas D on D.id_fornecedores_despesas = favorecido
    left join sf_vendas_planos_dcc_agendamento C on A.id_plano = C.id_plano_agendamento
    left join sf_fornecedores_despesas_veiculo V on A.id_veiculo = V.id    
    where favorecido = " . $_GET['listPlanosCliente'] . ($_GET['inv'] == "1" ? "" : " and ((DATEADD(day,(select ACA_PLN_DIAS_RENOVACAO from sf_configuracao),A.dt_fim) > GETDATE() and B.parcelar = 0) or
    (A.dt_fim >= GETDATE() and B.parcelar = 1) or
    ((select COUNT(*) from sf_vendas_planos_mensalidade C where C.id_plano_mens = A.id_plano and dt_pagamento_mens is null) > 0))") . $whereTurma . " order by A.dt_cadastro asc";
    $cur = odbc_exec($con, $query);
    if (isset($_GET['isTurma'])) {
        while ($RFP = odbc_fetch_array($cur)) {
            $row = array();
            $row["id_plano"] = utf8_encode($RFP["id_plano"]);
            $row["descricao"] = utf8_encode($RFP["descricao"]);
            $row["produto"] = utf8_encode($RFP["conta_produto"]);
            $records[] = $row;
        }
        echo json_encode($records);
    } else {
        $cont = 0;
        $records = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => 0,
            "iTotalDisplayRecords" => 0,
            "aaData" => array()
        );
        while ($RFP = odbc_fetch_array($cur)) {
            $btnImp = "";
            $renovar = "";
            $dcc = "";
            //$ckb_aca_can_plano_
            if (($mdl_aca_ == 1 && $RFP["bloqueado"] != 2 && file_exists("./../../../Modulos/Academia/modelos/Contratos/Contrato_" . $_SESSION["contrato"] . ".php")) || $mdl_cnt_ == 1) {
                $btnImp .= "<li><a class=\"impContrato\" onclick='imprimir_email(" . $RFP["id_plano"] . ",0)'>Contrato</a></li>";
            }
            if ($ckb_aca_can_plano_ > 0 && ($RFP["status"] != "Novo" && (($mdl_aca_ == 1 && $RFP["bloqueado"] != 2 && file_exists("./../../../Modulos/Academia/modelos/Contratos/Contrato_" . $_SESSION["contrato"] . "C.php")) || $mdl_cnt_ == 1))) {
                $btnImp .= "<li><a class=\"impContrato\" onclick=\"abrirTelaBox('./form/recisaoContrato.php?idplano=" . $RFP["id_plano"] . "', 400, 400);\">Rescisão de Contrato</a></li>";
            }
            if ($RFP["gera_parc"] == "0" && $RFP["bloqueado"] != 2 && $RFP["dt_cancelamento"] == "") {
                $renovar = "<li><a data-id=\"" . $RFP["id_plano"] . "\" class=\"renovar\">Renovar</a></li>";
            }
            if ($ckb_aca_can_plano_ > 0 && ($RFP["parcela"] <= 1 && $RFP["dt_cancelamento"] == "" && (($mdl_aca_ == 1 && $RFP["bloqueado"] != 2 && file_exists("./../../../Modulos/Academia/modelos/Contratos/Contrato_" . $_SESSION["contrato"] . "C.php")) || $mdl_cnt_ == 1))) {
                $dcc .= "<li><a data-id=\"" . $RFP["id_plano"] . "\" class=\"cancDcc\">Agendar Cancelamento</a></li>";
            }
            if ($RFP["bloqueado"] == 2) {
                $dcc .= "<li><a data-id=\"" . $RFP["id_plano"] . "\" class=\"cancSerasa\">Liberar Recebimento</a></li>";
            }
            $cont = $cont + 1;
            $row = array();
            $row[] = strtoupper(utf8_encode($RFP["descricao"])) . ($mdl_seg_ > 0 ?  " [" . (strlen($RFP["modelo"]) > 0 && strlen($RFP["placa"]) == 0 ? "Zero Km" : utf8_encode($RFP["placa"]))  . "] - " . utf8_encode($RFP["modelo"]) . " " : (strlen($RFP["descricao_turma"]) > 0 ? " - [" . utf8_encode($RFP["descricao_turma"]) . "]" : "")) .                    
            ($RFP["dt_cancel_agendamento"] == "" || $RFP["finito_agendamento"] == '1' ? "" : " || <span style='color: red;'>  Cancelamento em: " . escreverData($RFP["dt_cancel_agendamento"])) . " </span>";
            $row[] = "<center>" . ($RFP["parcela"] == -2 ? "PIX" : ($RFP["parcela"] == -1 ? "BOL" : ($RFP["parcela"] == 0 ? "DCC" : $RFP["parcela"]))) . "</center>";
            $row[] = escreverData($RFP["dt_inicio"]);
            $row[] = escreverData($RFP["dt_fim"]);
            $row[] = "<div data-prod=\"" . $RFP["conta_produto"] . "\" style=\"text-align: right;\"><span class=\"label label-" . str_replace(" ", "", $RFP["status"]) . "\" style=\"font-size: 11px; line-height: 13px;\">" . $RFP["status"] . "</span></div>";
            if ($ckb_adm_cli_read_ == 0 || $ckb_fin_lpg_ == 1) {
                $row[] = "<center>
                    <div style=\"text-align:left;\" class=\"btn-group\">
                        <button style=\"height: 13px;background-color: gray;\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\">
                            <span style=\"margin-top: 1px;\" class=\"caret\"></span>
                        </button>
                        <ul style=\"left:-130px;\" class=\"dropdown-menu\">" .
                        "<li style=\"" . ($mdl_aca_ == 0 || $RFP["bloqueado"] == 2 ? "display: none;" : "") . "\">
                            <a data-id=\"" . $RFP["id_plano"] . "\" class=\"detalhe\">Detalhes</a>
                        </li>" . $renovar .
                        "<li style=\"" . ($mdl_aca_ == 0 || $RFP["bloqueado"] == 2 || $RFP["bloq_congelamento"] == 1 ? "display: none;" : "") . "\">
                            <a data-id=\"" . $RFP["id_plano"] . "\" class=\"congelamento\">Congelamento</a>
                        </li>
                        <li style=\"" . ($RFP["bloqueado"] == 2 || $ckb_aca_rem_plano_ == 0 ? "display: none;" : "") . "\">
                            <a data-id=\"" . $RFP["id_plano"] . "\" class=\"remover\">Remover</a>
                        </li>" . $btnImp . $dcc . "
                        </ul>
                    </div>
                </center>";
            } else {
                $row[] = '';
            }
            $records['aaData'][] = $row;
        }
        $records["iTotalRecords"] = $cont;
        $records["iTotalDisplayRecords"] = $cont;
        echo json_encode($records);
    }
}

if (isset($_GET['listExtratoCredito'])) {
    $cur = odbc_exec($con, "select id_credito, data, valor, case when tipo = 'D' then 'DEBITO' else 'CREDITO' end tipo,
            dt_vencimento,historico collate SQL_Latin1_General_CP1_CI_AS historico,A.id_venda, id_venda_quitacao, B.status,
            (select data_venda from sf_vendas where id_venda = id_venda_quitacao) data_pagamento
            from sf_creditos A left join sf_vendas B on A.id_venda_quitacao = B.id_venda
            where A.inativo = 0 and id_fornecedor_despesa = " . $_GET['listExtratoCredito'] . "
            order by dt_vencimento");
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP["status"] !== "Reprovado") {
            $cont = $cont + 1;
            $row = array();
            $row[] = tableFormato($RFP["tipo"], 'T', '', '');
            $row[] = escreverData($RFP["data"]);
            $row[] = escreverNumero($RFP["valor"]);
            $row[] = escreverData($RFP["dt_vencimento"]);
            $row[] = tableFormato($RFP["historico"], 'T', '', '');
            $row[] = escreverData($RFP["data_pagamento"]);
            if ($RFP["tipo"] == "DEBITO" && $RFP["id_venda_quitacao"] == "") {
                $comando = "<center>
                        <div style=\"text-align:left;\" class=\"btn-group\">
                            <button type=\"button\" class=\"btn green\" style=\"line-height:10px;margin:0 0 1px;padding: 5px 5px 4px 5px;\" onClick=\"incluirItemPag('CARTEIRA',466," . $RFP["id_credito"] . ",'','S','S')\"><span>Pagar</span></button>
                        </div>";
                if ($ckb_aca_abodeb_ == 1) {
                    $comando .= "<div style=\"text-align:left;margin-left: 5px;\" class=\"btn-group\">
                    <button style=\"height: 13px;background-color: gray;\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\">
                        <span style=\"margin-top: 1px;\" class=\"caret\"></span>
                    </button><ul style=\"left:-130px;\" class=\"dropdown-menu\">
                        <li><a onclick=\"AbonarCarteira(" . $RFP["id_credito"] . ")\" class=\"Abonar\">Abonar</a></li>
                    </ul></div>";
                }
                $comando .= "</center>";
                $row[] = $comando;
            } else {
                $row[] = "";
            }
            $records['aaData'][] = $row;
        }
    }
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}

if (isset($_GET['listPagamentosServicos'])) {

    $arrayTmp = array();
    $arraySel = array();
    if (is_numeric($_GET["favorecido"])) {
        $query = "set dateformat dmy;
        select v.id_venda,COUNT(p.descricao) qtd, p.descricao, sum(valor_total) total, max(data_venda) data_venda, 
        (select id_transacao from sf_vendas_itens_dcc vc where vc.id_venda_transacao = v.id_venda) id_transacao            
        from sf_vendas v inner join sf_vendas_itens vi on v.id_venda = vi.id_venda
        inner join sf_produtos p on vi.produto = p.conta_produto
        where status = 'Aprovado' and cov = 'V' and (select count(id_item_venda) from sf_vendas_itens inner join
        sf_produtos on sf_vendas_itens.produto = sf_produtos.conta_produto
        where id_venda = v.id_venda and tipo not in ('S','P')) = 0 and cliente_venda = " . $_GET["favorecido"] . "
        group by v.id_venda, descricao order by 1 desc";
        $cur = odbc_exec($con, $query);
    }
    while ($RFP = odbc_fetch_array($cur)) {
        $arrayTmp[] = array($RFP['id_venda'], $RFP["descricao"] . ($RFP["qtd"] > 1 ? " (" . $RFP["qtd"] . ")" : ""), $RFP["total"], $RFP["data_venda"], $RFP['id_transacao']);
    }
    for ($i = 0; $i < count($arrayTmp); $i++) {
        if ($venda != $arrayTmp[$i][0]) {
            $venda = $arrayTmp[$i][0];
            $itens = "";
            $total = 0;
            for ($j = 0; $j < count($arrayTmp); $j++) {
                if ($venda == $arrayTmp[$j][0]) {
                    $itens .= ($itens != "" ? " + " : "") . $arrayTmp[$j][1];
                    $total = $total + $arrayTmp[$j][2];
                }
            }
            $arraySel[] = array($arrayTmp[$i][0], $itens, $total, $arrayTmp[$i][3], $arrayTmp[$i][4]);
        }
    }
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => count($arrayTmp),
        "iTotalDisplayRecords" => count($arrayTmp),
        "aaData" => array()
    );
    for ($i = 0; $i < count($arraySel); $i++) {
        $row = array();
        $row[] = tableFormato($arraySel[$i][1], 'T', '', '');
        $row[] = escreverNumero($arraySel[$i][2]);
        $row[] = escreverData($arraySel[$i][3]);
        if ($ckb_aca_est_mens_ == 1) {
            $row[] = "<center><div style=\"text-align:left;\" class=\"btn-group\"><button type=\"button\" class=\"btn " . ($arraySel[$i][2] == 0 ? "dblue" : "red") . "\" style=\"line-height:10px;margin:0 0 1px;padding: 5px 5px 4px 5px;\"
            onClick=\"estornarServico('" . (is_numeric($arraySel[$i][4]) ? "S" : "N") . "'," . $arraySel[$i][0] . ")\"><span>Estornar</span></button></div></center>";
        } else {
            $row[] = "<center><div title=\"Sem Acesso\" style=\"text-align:left;\" class=\"btn-group\"><button type=\"button\" class=\"btn red\" style=\"line-height:10px;margin:0 0 1px;padding: 5px 5px 4px 5px;\" onClick=\"\" disabled><span>Estornar</span></button></div></center>";
        }
        $records['aaData'][] = $row;
    }
    echo json_encode($records);
}

if (isset($_GET['listPagamentosPlano'])) {
    $cont = 0;    
    $acres = 0;
    $acess = 0;
    $tpPag = 0;
    $totBanco = 0;
    $cur = odbc_exec($con, "select ACA_ORDEM_MENS,clb_pagamento_agrupado from sf_configuracao where id = 1");
    while ($RFP = odbc_fetch_array($cur)) {
        $tpPag = $RFP["clb_pagamento_agrupado"];
        if ($RFP["ACA_ORDEM_MENS"] == 0) {
            $ordem = 'ASC';
        } else {
            $ordem = 'DESC';
        }
    }
    $cur = odbc_exec($con, "select count(id_bancos) total from sf_bancos 
    inner join sf_bancos_carteiras on sf_carteiras_bancos = id_bancos
    where tipo_bclc = 0 and ativo = 0");
    while ($RFP = odbc_fetch_array($cur)) {
        $totBanco = $RFP["total"];        
    }
    if (isset($_GET['idPlano']) && is_numeric($_GET['idPlano'])) {
        $rs = odbc_exec($con, "select dbo.FU_VALOR_ACESSORIOS(" . $_GET['idPlano'] . ") preco_venda");
        while ($dados_acres = odbc_fetch_array($rs)) {
            $acess = $acess + $dados_acres["preco_venda"];
        }
        $rs = odbc_exec($con, "select ISNULL(sum(x.qtd_total * valor), 0) acrecimo from (
        select id, count(id) qtd_total from sf_vendas_planos
        inner join sf_fornecedores_despesas_dependentes on favorecido = id_titular
        inner join sf_fornecedores_despesas on id_fornecedores_despesas = id_dependente
        left join sf_produtos_acrescimo on id_produto = id_prod_plano and
        ((datediff(year,data_nascimento,getdate()) between idade_min and idade_max_m and sexo = 'M') or
        (datediff(year,data_nascimento,getdate()) between idade_min and idade_max_f and sexo = 'F'))
        and (id_parentesco is null or id_parentesco = parentesco)
        where compartilhado = 1 and id_plano = " . $_GET['idPlano'] . " group by sf_produtos_acrescimo.id) as x 
        inner join sf_produtos_acrescimo on sf_produtos_acrescimo.id = x.id 
        and x.qtd_total between qtd_min and qtd_max");
        while ($dados_acres = odbc_fetch_array($rs)) {
            $acres = $acres + $dados_acres["acrecimo"];
        }        
        $cur = odbc_exec($con, "set dateformat dmy;
        select D.id_venda, D.id_item_venda, C.id_plano, C.id_prod_plano, A.id_mens,valor_mens, B.valor_unico,
        A.dt_inicio_mens, dt_fim_mens, D.valor_total valor_pago, A.dt_inicio_mens dt_vencimento, E.data_venda,  E.vendedor, F.desc_bloqueado, isnull(E.cliente_venda,0) cliente_venda, C.favorecido, B.parcela,
        case when (isnull((select max(id_transacao) from sf_vendas_itens_dcc where id_venda_transacao is not null and E.id_venda = id_venda_transacao and processorMessage = 'APPROVED' and status_transacao = 'A'),0)) > 0 then 'S' else 'N' end isDCC,        
        case when (select count(id_boleto) from sf_boleto where inativo = 0 and ISNUMERIC(mundi_id) = 1 and id_referencia = A.id_mens) > 0 then 'S' else 'N' end isCanc,
        isnull((n_servicos + n_produtos),0) abono, (select max(id_titular) from sf_fornecedores_despesas_dependentes where id_dependente = favorecido) id_titular, usu_alt, A.dt_pagamento_mens,
        (select count(id_boleto) total from sf_boleto where tp_referencia = 'M' and inativo = 0 and id_referencia = A.id_mens) total_boleto,         
        (select isnull(max(b.id_boleto),0) total from sf_boleto_online_itens bi inner join sf_boleto_online b on b.id_boleto = bi.id_boleto where b.inativo = 0 and bi.id_mens = A.id_mens) total_boleto_online,         
        (select isnull(max(b.bol_link),'') total from sf_boleto_online_itens bi inner join sf_boleto_online b on b.id_boleto = bi.id_boleto where b.inativo = 0 and bi.id_mens = A.id_mens) link_online    
        from sf_vendas_planos_mensalidade A inner join sf_produtos_parcelas B on A.id_parc_prod_mens = B.id_parcela
        inner join sf_vendas_planos C on A.id_plano_mens = C.id_plano
        left join sf_vendas_itens D on A.id_item_venda_mens = D.id_item_venda
        left join sf_vendas E on D.id_venda = E.id_venda
        inner join sf_produtos F on C.id_prod_plano = F.conta_produto
        where C.id_plano = " . $_GET['idPlano'] . " and C.favorecido = " . $_GET['listPagamentosPlano'] . (isset($_GET['isCota']) ? " and id_cota is not null" : " and id_cota is null") . " order by dt_vencimento " . $ordem);
    }

    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );

    while ($RFP = odbc_fetch_array($cur)) {
        $cont = $cont + 1;
        $row = array();
        $row[] = escreverData($RFP['dt_inicio_mens']) . " a " . escreverData($RFP['dt_fim_mens']);        
        $row[] = ($RFP["valor_mens"] != $RFP["valor_unico"] && strlen($RFP["usu_alt"]) > 0 ? "<span style=\"background-color: greenyellow;\">" . 
        escreverNumero(($RFP["valor_mens"] + (!isset($_GET['isCota']) ? $acres + ($RFP["parcela"] < 1 ? $acess : $acess * $RFP["parcela"]) : 0))) . "</span>" : 
        escreverNumero(($RFP["valor_mens"] + (!isset($_GET['isCota']) ? $acres + ($RFP["parcela"] < 1 ? $acess : $acess * $RFP["parcela"]) : 0))));
        $row[] = escreverNumero(($RFP["valor_pago"]));
        $row[] = escreverData($RFP['dt_vencimento']);
        $row[] = escreverData($RFP["dt_pagamento_mens"]);
        $row[] = tableFormato($RFP["vendedor"], 'T', '', '');
        if ($RFP["cliente_venda"] > 0 && ($RFP["cliente_venda"] !== $RFP["favorecido"]) && $RFP["cliente_venda"] !== $RFP["id_titular"]) {
            $row[] = "";
        } else {
            if ($RFP["data_venda"] == "") {
                $btnContent = "";
                $boletoLink = "";                
                if ($RFP["parcela"] == -1 && $RFP["total_boleto_online"] > 0 && $ckb_clb_cboleto_ > 0) {
                    $btnContent .= "<li><a onClick=\"EnviarBoleto('', " . $RFP['total_boleto_online'] . ")\" data-id=\"" . $RFP['id_mens'] . "\" class=\"Enviar\">Enviar Cobrança</a></li>";
                } else if ($RFP["parcela"] == -1 && $RFP["total_boleto"] > 0) {
                    $btnContent .= "<li><a onClick=\"EnviarBoleto(" . $RFP['id_mens'] . ", '')\" data-id=\"" . $RFP['id_mens'] . "\" class=\"Enviar\">Enviar Cobrança</a></li>";                    
                }
                if ($RFP["total_boleto_online"] > 0) {
                    $btnContent .= "<li><a onclick=\"abrirTelaBox('ClientesBoletosForm.php?id=" . $RFP["total_boleto_online"] . "', 500, 770);\" class=\"Gerar\">Itens do Boleto</a></li>";
                } else if ($RFP["parcela"] == -2 && $RFP["total_boleto"] == 0 && $ckb_clb_gboleto_ > 0) {
                    $btnContent .= "<li><a onClick=\"GerarPIX(" . $RFP['id_mens'] . ")\" data-id=\"" . $RFP['id_mens'] . "\" class=\"Gerar\">Cobrança PIX</a></li>";
                } else if ($RFP["parcela"] == -1 && $RFP["total_boleto"] == 0 && $ckb_clb_gboleto_ > 0) {
                    $btnContent .= "<li><a onClick=\"GerarBoleto(" . $RFP['id_mens'] . ")\" data-id=\"" . $RFP['id_mens'] . "\" class=\"Gerar\">Gerar Boleto</a></li>";
                } else if (($RFP["parcela"] == 0 || $RFP["parcela"] == 1 || $RFP["parcela"] > 1) && $RFP["total_boleto"] == 0 && $ckb_clb_gboleto_ > 0 && $totBanco > 0) {
                    //$btnContent .= "<li><a onClick=\"abrirTelaBox('./form/gerarBoleto.php?idMens=" . $RFP['id_mens'] . "', 350, 400);\" data-id=\"" . $RFP['id_mens'] . "\" class=\"Gerar\">Gerar Boleto</a></li>";
                }                
                if ($ckb_aca_abomens_ == 1) {
                    $btnContent .= "<li><a onClick=\"AbonarMens(" . $RFP['id_mens'] . ")\" data-id=\"" . $RFP['id_mens'] . "\" class=\"Abonar\">Abonar</a></li>";
                }
                if ($ckb_aca_alt_val_mens_ == 1) {
                    $btnContent .= "<li><a data-id=\"" . $RFP['id_mens'] . "\" id=\"btnAlt\" class=\"Alterar\">Alterar Valor</a></li>";
                }
                if ($ckb_aca_pro_rata_mens_ == 1) {
                    $btnContent .= "<li><a data-id=\"" . $RFP['id_mens'] . "\" id=\"btnProRata\" class=\"ProRata\">Pro Rata</a></li>";
                }
                if ($RFP["total_boleto_online"] > 0 && $ckb_clb_cboleto_ > 0) {
                    $btnContent .= "<li><a onclick=\"cancelarBoletoOnline(" . $RFP["total_boleto_online"] . ");\" class=\"Excluir\">Cancelar Boleto</a></li>";
                } else if ($RFP["total_boleto"] > 0 && $ckb_clb_cboleto_ > 0) {
                    $btnContent .= "<li><a onClick=\"CancelarBoleto(" . $RFP['id_mens'] . ",'" . $RFP['isCanc'] . "')\" data-id=\"" . $RFP['id_mens'] . "\" class=\"Enviar\">Cancelar " . ($RFP["parcela"] == -2 ? "PIX" : "Boleto") . "</a></li>";
                }                
                if ($RFP["total_boleto_online"] == 0 && $ckb_aca_exc_mens_ == 1) {
                    $btnContent .= "<li><a onClick=\"ExcluirItemPag(" . $RFP['id_mens'] . ")\" data-id=\"\" class=\"Excluir\">Excluir</a></li>";
                }                
                if ($RFP["total_boleto_online"] > 0) {
                    $boletoLink = "<a style=\"margin-right: 6px;\" href=\"" . $RFP['link_online'] . "\" target=\"_blank\"><img src=\"../../img/boleto.png\"></a>";
                } else if ($RFP["total_boleto"] > 0 && $RFP["parcela"] != -2) {
                    $boletoLink = "<a style=\"margin-right: 6px;\" href=\"./../../Boletos/Boleto.php?id=M-" . $RFP['id_mens'] . "&amp;crt=" . $_SESSION["contrato"] . "\" target=\"_blank\"><img src=\"../../img/boleto.png\"></a>";
                } else if ($RFP["total_boleto"] > 0 && $RFP["parcela"] == -2) {
                    $boletoLink = "<a style=\"margin-right: 6px;\" href=\"./../../Boletos/boleto_pix.php?id=M-" . $RFP['id_mens'] . "&amp;crt=" . $_SESSION["contrato"] . "\" target=\"_blank\"><img src=\"../../img/pix.png\"></a>";
                }
                $row[] = "<center>" . $boletoLink . "<div class=\"btn-group\">" .
                "<button type=\"button\" class=\"btn green\" style=\"line-height:10px;margin:0 0 1px;padding: 5px 5px 4px 5px;" . 
                ($tpPag == 1 ? "display: none;" : "") . "\" onClick=\"incluirItemPag('MENSALIDADE'," . $RFP['id_prod_plano'] . "," . $RFP['id_mens'] . ",'T'," . 
                $RFP['desc_bloqueado'] . ",'" . ($RFP['parcela'] == 0 ? 'S' : 'N') . "')\"><span>Pagar</span></button>" .                         
                ($btnContent != "" ? "<div style=\"text-align:left;margin-left: 5px;\" class=\"btn-group\">
                <button style=\"height: 13px;background-color: gray;\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\">
                <span style=\"margin-top: 1px;\" class=\"caret\"></span></button><ul style=\"left:-130px;\" class=\"dropdown-menu\">" . $btnContent . "</ul></div>" : "") . "</div></center>";
            } else {
                $row[] = "<center><div style=\"text-align:left;\" class=\"btn-group\"><button type=\"button\" class=\"btn " . ($RFP['abono'] == 2 ? "dblue" : "red") . "\" style=\"line-height:10px;margin:0 0 1px;padding: 5px 5px 4px 5px;\" " .
                "onClick=\"" . ($ckb_aca_est_mens_ == 1 ? "estornarPagPlano(" . $RFP['id_mens'] . "," . $RFP['id_plano'] . ",'" . $RFP['isDCC'] . "', " . $RFP['id_venda'] . ")" : "") . "\" " . ($ckb_aca_est_mens_ == 1 ? "" : "disabled") . "><span>Estornar</span></button></div></center>";
            }
            $row[] = "";
        }
        $records['aaData'][] = $row;
    }
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}

if (isset($_GET['listPagamentosDependentes'])) {
    $tpPag = 0;
    $cur = odbc_exec($con, "select clb_pagamento_agrupado from sf_configuracao where id = 1");
    while ($RFP = odbc_fetch_array($cur)) {
        $tpPag = $RFP["clb_pagamento_agrupado"];
    }    
    $cur = odbc_exec($con, "select F.id_item_venda_mens, F.id_mens, D.id_produto, E.descricao,
    dt_inicio_mens,dt_fim_mens, valor_mens, F.dt_inicio_mens dt_vencimento, B.razao_social, E.desc_bloqueado,
    (select count(id_boleto) total from sf_boleto where tp_referencia = 'M' and inativo = 0 and id_referencia = F.id_mens) total_boleto 
    from sf_fornecedores_despesas_dependentes A
    inner join sf_fornecedores_despesas B on A.id_dependente = B.id_fornecedores_despesas
    inner join sf_vendas_planos C on A.id_dependente = C.favorecido
    inner join sf_vendas_planos_mensalidade F on F.id_plano_mens = C.id_plano
    inner join sf_produtos_parcelas D on D.id_parcela = F.id_parc_prod_mens
    inner join sf_produtos E on D.id_produto = E.conta_produto
    where F.id_item_venda_mens is null and id_titular = " . $_GET['listPagamentosDependentes'] . 
    " order by dt_vencimento, B.razao_social, D.id_produto");
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    while ($RFP = odbc_fetch_array($cur)) {
        $boletoLink = "";        
        if ($RFP["total_boleto"] > 0) {
            $boletoLink = "<a style=\"margin-right: 6px;\" href=\"./../../Boletos/Boleto.php?id=M-" . $RFP['id_mens'] . "&amp;crt=" . $_SESSION["contrato"] . "\" target=\"_blank\"><img src=\"../../img/boleto.png\"></a>";
        }
        $cont = $cont + 1;
        $row = array();
        $row[] = tableFormato($RFP["razao_social"], 'T', '', '');
        $row[] = tableFormato($RFP["descricao"], 'T', '', '');
        $row[] = escreverData($RFP["dt_inicio_mens"]) . " a " . escreverData($RFP["dt_fim_mens"]);
        $row[] = escreverNumero($RFP["valor_mens"]);
        $row[] = escreverNumero($RFP["valor_pago"]);
        $row[] = escreverData($RFP["dt_vencimento"]);
        $row[] = "<center>" . $boletoLink . "<div style=\"text-align:left;\" class=\"btn-group\">" .                 
        "<button type=\"button\" class=\"btn green\" style=\"" . ($tpPag == 1 ? "display: none;" : "") . "line-height:10px;margin:0 0 1px;padding: 5px 5px 4px 5px;\" onClick=\"incluirItemPag('MENSALIDADE'," . $RFP['id_produto'] . "," . $RFP['id_mens'] . ",'D'," . $RFP['desc_bloqueado'] . ",'N')\"><span>Pagar</span></button>" . 
        "</div></center>";        
        $records['aaData'][] = $row;
    }
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}

if (isset($_GET['validaEstorno'])) {
    $exiteCredito = "N";
    $cur = odbc_exec($con, "select id_venda_quitacao from sf_creditos where inativo = 0 and id_venda in(
    select A.id_venda from sf_vendas A inner join sf_vendas_itens B on A.id_venda = B.id_venda
    inner join sf_vendas_planos_mensalidade C on B.id_item_venda = C.id_item_venda_mens where id_mens = " . $_GET['idMens'] . ")
    and id_venda_quitacao is not null");
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['id_venda_quitacao'] > 0) {
            $exiteCredito = "S";
        }
    }
    $cur = odbc_exec($con, "select id_credito from sf_creditos where inativo = 0 and id_plano = " . $_GET['validaEstorno']);
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['id_credito'] > 0) {
            $exiteCredito = "S";
        }
    }
    if ($ckb_aca_exc_mret_ == 0) {
        $cur = odbc_exec($con, "select case when dt_fim_mens >= (
        select max(dt_fim_mens) from sf_vendas_planos_mensalidade where id_plano_mens = " . $_GET['validaEstorno'] . " and dt_pagamento_mens is not null) then 'S' else 'N' end maior_dt from sf_vendas_planos_mensalidade A
        where id_mens = " . $_GET['idMens']);
        while ($RFP = odbc_fetch_array($cur)) {
            $maior_dt = $RFP['maior_dt'];
        }
    }
    if ($exiteCredito == 'S') {
        echo "CREDITO";
    } else if ($maior_dt === "N") {
        echo "NAOULTIMO";
    } else {
        echo "YES";
    }
    exit();
}

if (isset($_POST['estornarServico']) && is_numeric($_POST['estornarServico'])) {
    $idVenda = $_POST['estornarServico'];
    odbc_autocommit($con, false);
    $cur = odbc_exec($con, "select cliente_venda from sf_vendas where id_venda = " . $idVenda);
    while ($RFP = odbc_fetch_array($cur)) {
        $cliente_venda = $RFP['cliente_venda'];
    }
    $query = "update sf_vendas set status = 'Reprovado', dt_estorno = getdate(), user_estorno = " . $_SESSION["id_usuario"] . " where id_venda = " . $idVenda . ";
    update sf_creditos set inativo = 1 where id_venda = " . $idVenda . ";
    insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values ('sf_vendas', " . $idVenda . ", '" . $_SESSION["login_usuario"] . "', 'E', " . valoresTexto2("ESTORNO: " . $idVenda . ", " . $_POST['motivo']) . " + ', Itens:' + STUFF((SELECT distinct ', ' + descricao FROM sf_vendas_itens inner join sf_produtos on produto = conta_produto WHERE id_venda = " . $idVenda . " FOR XML PATH('')), 1, 1, ''), GETDATE(), " . $cliente_venda . ");";
    $objExec = odbc_exec($con, $query);
    if (!$objExec) {
        odbc_rollback($con);
        echo "NO";
        exit();
    }
    if ($objExec) {
        odbc_commit($con);
        echo "YES";
    }
}

if (isset($_POST['estornarPagPlano'])) {
    $queryDCC = '';
    $parcela = 0;
    odbc_autocommit($con, false);
    $cur = odbc_exec($con, "select parcela, id_item_venda_mens, id_plano_mens from sf_vendas_planos_mensalidade A
    inner join sf_produtos_parcelas B on A.id_parc_prod_mens = B.id_parcela
    where id_mens = " . $_POST['estornarPagPlano']);
    while ($RFP = odbc_fetch_array($cur)) {
        $idItemVenda = $RFP['id_item_venda_mens'];
        $parcela = $RFP['parcela'];
        $idPlanoMens = $RFP['id_plano_mens'];
    }
    $cur = odbc_exec($con, "select sf_vendas_itens.id_venda,sf_vendas.cliente_venda from sf_vendas_itens
    inner join sf_vendas on sf_vendas_itens.id_venda = sf_vendas.id_venda
    where id_item_venda = " . $idItemVenda);
    while ($RFP = odbc_fetch_array($cur)) {
        $idVenda = $RFP['id_venda'];
        $cliente_venda = $RFP['cliente_venda'];
    }
    if ($parcela == 0) {
        $queryDCC = "delete from sf_vendas_planos_mensalidade where id_plano_mens = (select id_plano_mens from sf_vendas_planos_mensalidade where id_mens = " . $_POST['estornarPagPlano'] . ") and dt_pagamento_mens is null;";
    }
    if ($idVenda !== "") {
        $id_mens = $_POST['estornarPagPlano'];
        $query = "set dateformat dmy;" . $queryDCC;
        $query .= "update sf_vendas set status = 'Reprovado', dt_estorno = getdate(), user_estorno = " . $_SESSION["id_usuario"] . " where id_venda = " . $idVenda . ";
        update sf_vendas_planos_mensalidade set id_item_venda_mens = null, dt_pagamento_mens = null where id_item_venda_mens in( select id_item_venda from sf_vendas_itens where id_venda = " . $idVenda . ");
        update sf_creditos set inativo = 1 where id_venda = " . $idVenda . ";
        insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values ('sf_vendas', " . $idVenda . ", '" . $_SESSION["login_usuario"] . "', 'E', " . valoresTexto2("ESTORNO: " . $idVenda . ", " . $_POST['motivo']) . " + ', Itens:' + STUFF((SELECT distinct ', ' + descricao FROM sf_vendas_itens inner join sf_produtos on produto = conta_produto WHERE id_venda = " . $idVenda . " FOR XML PATH('')), 1, 1, ''), GETDATE(), " . $cliente_venda . ");";
        $query .= "update sf_vendas_planos set dt_fim = dbo.FU_PLANO_FIM(id_plano) where id_plano = " . $idPlanoMens . ";";
        $objExec = odbc_exec($con, $query);
    } else {
        echo "NO";
        exit();
    }
    if (!$objExec) {
        odbc_rollback($con);
        echo "NO";
        exit();
    }
    if ($objExec) {
        odbc_commit($con);
        echo "YES";
    }
}

if (isset($_POST['abonoMensalidade'])) {
    $id_mens = $_POST['abonoMensalidade'];
    $cur = odbc_exec($con, "select favorecido,conta_movimento,id_prod_plano,valor_mens from sf_vendas_planos_mensalidade
    inner join sf_vendas_planos on sf_vendas_planos_mensalidade.id_plano_mens = sf_vendas_planos.id_plano
    left join sf_produtos on sf_produtos.conta_produto = sf_vendas_planos.id_prod_plano where id_mens = " . $id_mens);
    while ($RFP = odbc_fetch_array($cur)) {
        $idCliente = $RFP['favorecido'];
        $grupo = $RFP['conta_movimento'];
        $produto = $RFP['id_prod_plano'];
        $login_usuario = strtoupper($_SESSION["login_usuario"]);
    }
    $query = "set dateformat dmy;
    insert into sf_vendas (vendedor,cliente_venda,historico_venda,data_venda,sys_login,empresa,destinatario,status,tipo_documento,
    cov,descontop,descontos,descontotp,descontots,n_servicos,data_aprov,grupo_conta,conta_movimento,favorito,reservar_estoque, n_produtos)
    values ('" . $login_usuario . "'," . $idCliente . ",'" . utf8_decode('ABONO / PLANO') . "',getdate(),'" . $login_usuario . "',1,'" . $login_usuario . "','Aprovado',null,
    'V',0.00,0.00,0,0,1,getdate(),14,1,0,0,1) SELECT SCOPE_IDENTITY() ID;";
    $result = odbc_exec($con, $query) or die(odbc_errormsg());
    odbc_next_result($result);
    $idVenda = odbc_result($result, 1);
    $query = "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total,valor_bruto)
    values (" . $idVenda . "," . valoresSelect2($grupo) . "," . $produto . ",1,0.00,0.00);";
    $query .= "update sf_vendas_planos_mensalidade set id_item_venda_mens = (select id_item_venda from sf_vendas_itens where id_venda = " . $idVenda . "),
    dt_pagamento_mens = cast(getdate() as date) where id_mens = " . $id_mens;
    
    $query .= " insert into sf_vendas_planos_mensalidade (id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens)
    select id_plano_mens, dateadd(day,1,dt_fim_mens),
    case when tp_valid_acesso = 'C' then dateadd(day, qtd_dias_acesso,dt_fim_mens) else 
        dateadd(day,-1, dateadd(month,1,dateadd(day,1,dt_fim_mens)))
    end,
    id_parc_prod_mens, GETDATE(),valor_unico valor_mens
    from sf_vendas_planos_mensalidade m1 inner join sf_produtos_parcelas pp on m1.id_parc_prod_mens = pp.id_parcela
    inner join sf_produtos p on p.conta_produto = pp.id_produto
    inner join sf_vendas_planos on id_plano = id_plano_mens    
    where p.inativar_renovacao = 0 and id_mens = " . $id_mens . " and dt_cancelamento is null and parcela in (-2,-1,0,1) and (select COUNT(*) from sf_vendas_planos_mensalidade m2
    where m2.id_plano_mens = m1.id_plano_mens and m2.id_parc_prod_mens = m1.id_parc_prod_mens
    and ((tp_valid_acesso <> 'C' and month(m2.dt_inicio_mens) = month(dateadd(month,1,m1.dt_inicio_mens)) and year(m2.dt_inicio_mens) = year(dateadd(month,1,m1.dt_inicio_mens))
    ) or (tp_valid_acesso = 'C' and dateadd(day,1,m1.dt_fim_mens) = dt_inicio_mens))) = 0;";
    $query .= "update sf_vendas_planos set dt_fim = dbo.FU_PLANO_FIM(id_plano) where id_plano in(
    select id_plano_mens from sf_vendas_planos_mensalidade where id_mens = " . $id_mens . ");";
    $query .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
    ('sf_vendas', " . $idVenda . ", '" . $_SESSION["login_usuario"] . "', 'B', " . valoresTexto2("ABONO: " . $idVenda . ", " . $_POST['motivo']) . " + ', Itens:' + STUFF((SELECT distinct ', ' + descricao FROM sf_vendas_itens inner join sf_produtos on produto = conta_produto WHERE id_venda = " . $idVenda . " FOR XML PATH('')), 1, 1, ''), GETDATE()," . $idCliente . ");";    
    $query .= "EXEC dbo.SP_MK_VISTORIA @id_venda = " . $idVenda . ", @loja = '" . $contrato . "';";    
    $objExec = odbc_exec($con, $query);        
    if (!$objExec) {
        odbc_rollback($con);
        echo "ERROR" . $query;
        exit();
    }
    if ($objExec) {
        odbc_commit($con);
        echo "YES";
    }
}

if (isset($_POST['abonoProduto'])) {
    $idCliente = $_POST['idCliente'];
    $login_usuario = strtoupper($_SESSION["login_usuario"]);    
    $id_produto = $_POST['abonoProduto'];    
    $cur = odbc_exec($con, "select * from sf_produtos where conta_produto = " . $id_produto);
    while ($RFP = odbc_fetch_array($cur)) {
        $grupo = $RFP['conta_movimento'];
        $produto = $RFP['conta_produto'];
    }
    $query = "set dateformat dmy;
    insert into sf_vendas (vendedor,cliente_venda,historico_venda,data_venda,sys_login,empresa,destinatario,status,tipo_documento,
    cov,descontop,descontos,descontotp,descontots,n_servicos,data_aprov,grupo_conta,conta_movimento,favorito,reservar_estoque, n_produtos)
    values ('" . $login_usuario . "'," . $idCliente . ",'" . utf8_decode('ABONO / PRODUTO') . "',getdate(),'" . $login_usuario . "',1,'" . $login_usuario . "','Aprovado',null,
    'V',0.00,0.00,0,0,1,getdate(),14,1,0,0,1) SELECT SCOPE_IDENTITY() ID;";
    $result = odbc_exec($con, $query) or die(odbc_errormsg());
    odbc_next_result($result);
    $idVenda = odbc_result($result, 1);
    $query = "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total,valor_bruto)
    values (" . $idVenda . "," . valoresSelect2($grupo) . "," . $produto . ",1,0.00,0.00);";    
    $query .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
    ('sf_vendas', " . $idVenda . ", '" . $_SESSION["login_usuario"] . "', 'B', " . valoresTexto2("ABONO: " . $idVenda . ", " . $_POST['motivo']) . " + ', Itens:' + STUFF((SELECT distinct ', ' + descricao FROM sf_vendas_itens inner join sf_produtos on produto = conta_produto WHERE id_venda = " . $idVenda . " FOR XML PATH('')), 1, 1, ''), GETDATE()," . $idCliente . ");";    
    $objExec = odbc_exec($con, $query);        
    if (!$objExec) {
        odbc_rollback($con);
        echo "ERROR" . $query;
        exit();
    }
    if ($objExec) {
        odbc_commit($con);
        echo "YES";
    }
}

if (isset($_POST['abonoCredito'])) {
    $idCliente = $_POST['abonoCredito'];
    $grupo = "";
    $produto = "466";
    $login_usuario = strtoupper($_SESSION["login_usuario"]);

    $query = "set dateformat dmy;
    insert into sf_vendas (vendedor,cliente_venda,historico_venda,data_venda,sys_login,empresa,destinatario,status,tipo_documento,
    cov,descontop,descontos,descontotp,descontots,n_servicos,data_aprov,grupo_conta,conta_movimento,favorito,reservar_estoque, n_produtos)
    values ('" . $login_usuario . "'," . $idCliente . ",'" . utf8_decode('ABONO / CREDITO') . "',getdate(),'" . $login_usuario . "',1,'" . $login_usuario . "','Aprovado',null,
    'V',0.00,0.00,0,0,1,getdate(),14,1,0,0,1) SELECT SCOPE_IDENTITY() ID;";
    $result = odbc_exec($con, $query) or die(odbc_errormsg());
    odbc_next_result($result);
    $idVenda = odbc_result($result, 1);
    $query = "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
    ('sf_vendas', " . $idVenda . ", '" . $_SESSION["login_usuario"] . "', 'B', 'ABONO CREDITO, ABONO: " . $idVenda . "', GETDATE()," . $idCliente . ");";
    $query .= "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total,valor_bruto)
    values (" . $idVenda . "," . valoresSelect2($grupo) . "," . $produto . ",1,0.00,0.00)";
    $query .= "insert into sf_creditos (id_fornecedor_despesa, valor, data, tipo, id_venda, historico, usuario_resp, inativo) values 
    (" . $idCliente . ", " . valoresNumericos("valCred") . ", getdate(), 'C'," . $idVenda . ",'" . utf8_decode('AQUISIÇÃO CREDITO') . "' ," . $_SESSION["id_usuario"] . ",0);";
    $objExec = odbc_exec($con, $query);
    if (!$objExec) {
        odbc_rollback($con);
        echo "ERROR" . $query;
        exit();
    }
    if ($objExec) {
        odbc_commit($con);
        echo "YES";
    }
}

if (isset($_POST['abonoCarteira'])) {
    $id_carteira = $_POST['abonoCarteira'];
    $grupo = "";
    $produto = "466";
    $login_usuario = strtoupper($_SESSION["login_usuario"]);
    $cur = odbc_exec($con, "select id_venda, id_fornecedor_despesa from sf_creditos where id_credito = " . $id_carteira);
    while ($RFP = odbc_fetch_array($cur)) {
        $idCliente = $RFP['id_fornecedor_despesa'];
        $idVendaDivida = $RFP['id_venda'];
    }
    $query = "set dateformat dmy;
    insert into sf_vendas (vendedor,cliente_venda,historico_venda,data_venda,sys_login,empresa,destinatario,status,tipo_documento,
    cov,descontop,descontos,descontotp,descontots,n_servicos,data_aprov,grupo_conta,conta_movimento,favorito,reservar_estoque, n_produtos)
    values ('" . $login_usuario . "'," . $idCliente . ",'" . utf8_decode('ABONO / CREDITO') . "',getdate(),'" . $login_usuario . "',1,'" . $login_usuario . "','Aprovado',null,
    'V',0.00,0.00,0,0,1,getdate(),14,1,0,0,1) SELECT SCOPE_IDENTITY() ID;";
    $result = odbc_exec($con, $query) or die(odbc_errormsg());
    odbc_next_result($result);
    $idVenda = odbc_result($result, 1);
    $query = "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
    ('sf_vendas', " . $idVenda . ", '" . $_SESSION["login_usuario"] . "', 'B', 'ABONO PLANO, ABONO: " . $idVenda . "', GETDATE()," . $idCliente . ");";
    $query .= "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total,valor_bruto)
    values (" . $idVenda . "," . valoresSelect2($grupo) . "," . $produto . ",1,0.00,0.00)";
    $query .= "update sf_creditos set id_venda_quitacao = " . $idVenda . " where id_credito = " . $id_carteira;
    $query .= "update sf_venda_parcelas set data_pagamento = getdate(), valor_pago = 0.00,inativo = 2 where id_parcela in (
    select top 1 id_parcela from sf_venda_parcelas inner join sf_creditos on sf_creditos.id_venda = sf_venda_parcelas.venda
    where sf_creditos.inativo = 0 and tipo_documento = 12 and data_pagamento is null and data_parcela = dt_vencimento 
    and valor_parcela = valor and id_credito = " . $id_carteira . ");";
    $objExec = odbc_exec($con, $query);
    if (!$objExec) {
        odbc_rollback($con);
        echo "ERROR" . $query;
        exit();
    }
    if ($objExec) {
        odbc_commit($con);
        echo "YES";
    }
}

if (isset($_POST['cancelarBoleto'])) {
    $query = "update sf_boleto set inativo = 1 where tp_referencia = 'M' and id_referencia = " . $_POST['cancelarBoleto'] . ";";    
    $query .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) 
    values ('sf_boleto', " . $_POST['cancelarBoleto'] . ", '" . $_SESSION["login_usuario"] . "', 'C', 'CANCELAMENTO BOLETO: ' + 
    (select top 1 cast(sa_descricao as varchar(max)) from sf_boleto where id_referencia = " . $_POST['cancelarBoleto'] . "), GETDATE(), 
    (select isnull((select favorecido from sf_vendas_planos_mensalidade inner join sf_vendas_planos on id_plano_mens = id_plano where id_mens = " . $_POST['cancelarBoleto'] . "),0)));";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}

if (isset($_POST['excluirMensalidade'])) {
    $cur = odbc_exec($con, "select isnull(id_item_venda_mens,0) id_item_venda_mens, id_plano_mens, favorecido, dt_inicio_mens, dt_fim_mens, dt_fim from sf_vendas_planos_mensalidade
    inner join sf_vendas_planos on sf_vendas_planos_mensalidade.id_plano_mens = sf_vendas_planos.id_plano where id_mens = " . $_POST['excluirMensalidade']);
    while ($RFP = odbc_fetch_array($cur)) {
        $idPagamento = $RFP['id_item_venda_mens'];
        $idPlano = $RFP['id_plano_mens'];
        $idCliente = $RFP['favorecido'];
        $periodo = $RFP['dt_inicio_mens'] . " a " . $RFP['dt_fim_mens'];
        $time_inicial = strtotime($RFP['dt_inicio_mens']);
        $time_final = strtotime($RFP['dt_fim_mens']);
        $diferenca = $time_final - $time_inicial;
        $dias = (int) floor($diferenca / (60 * 60 * 24)) + 1;
        $dtFimPlano = escreverDataSoma(escreverData($RFP['dt_fim']), " -" . $dias . " day");
    }
    $cur = odbc_exec($con, "select isnull(count(*),0) total from sf_vendas_planos_mensalidade where
    id_plano_mens in(select id_plano_mens from sf_vendas_planos_mensalidade where id_mens = " . $_POST['excluirMensalidade'] . ")");
    while ($RFP = odbc_fetch_array($cur)) {
        $total = $RFP['total'];
    }
    if ($ckb_aca_exc_mret_ == 0) {
        $cur = odbc_exec($con, "select case when dt_fim_mens >= (
        select max(dt_fim_mens) from sf_vendas_planos_mensalidade where id_plano_mens in(
        select id_plano_mens from sf_vendas_planos_mensalidade where id_mens = A.id_mens)) then 'S' else 'N' end maior_dt from sf_vendas_planos_mensalidade A
        where id_mens = " . $_POST['excluirMensalidade']);
        while ($RFP = odbc_fetch_array($cur)) {
            $maior_dt = $RFP['maior_dt'];
        }
    }
    if ($idPagamento > 0) {
        echo "PAGAMENTO";
    } else if ($maior_dt === "N") {
        echo "NAOULTIMO";
    } else if ($total <= 1) {
        echo "UNICO";
    } else {
        $query = "set dateformat dmy;
        BEGIN TRANSACTION
            insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values ('sf_vendas_planos_mensalidade', " . $_POST['excluirMensalidade'] . ", '" . $_SESSION["login_usuario"] . "', 'R', 'EXCLUSAO MENS. PLANO " . $idPlano . ", PERIODO: " . $periodo . "', GETDATE(), " . $idCliente . ");
            delete from sf_vendas_planos_mensalidade where id_mens = " . $_POST['excluirMensalidade'] . ";
            update sf_vendas_planos set dt_fim = dbo.FU_PLANO_FIM(id_plano) where id_plano = " . $idPlano . ";
        IF @@ERROR = 0
        COMMIT TRANSACTION
        ELSE
        ROLLBACK;";
        odbc_exec($con, $query) or die(odbc_errormsg());
        echo "YES";
    }
}

if (isset($_POST['atualizaMensalidade'])) {
    $idPagamento = 0;
    odbc_autocommit($con, true);    
    $query = "set dateformat dmy;
    select isnull(id_item_venda_mens,0) id_item_venda_mens, valor_mens + dbo.FU_VALOR_ACESSORIOS(id_plano) valor_mens, 
    dbo.FU_VALOR_ACESSORIOS(id_plano) valor_acessorios, DATEDIFF(day, dt_inicio_mens, dt_fim_mens) + 1 diasdiff, dt_fim_mens, 
    DATEDIFF (day, dt_inicio_mens, " . valoresData('txtDataMens') . ") + 1 difNovaDt, favorecido from dbo.sf_vendas_planos_mensalidade
    inner join sf_vendas_planos on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens
    where id_mens = " . $_POST['atualizaMensalidade'];
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $idPagamento = $RFP['id_item_venda_mens'];
        $valAnt = $RFP['valor_mens'];
        $valAcess = $RFP['valor_acessorios'];
        $diasDif = $RFP['diasdiff'];
        $dataFim = escreverData($RFP['dt_fim_mens']);
        $difNovaDt = $RFP['difNovaDt'];
        $favorecido = $RFP['favorecido'];
    }
    if ($idPagamento > 0) {
        echo "PAGAMENTO";
        exit;
    }
    $queryProxMens = "";
    $dt_fim = "";
    $valorMens = valoresNumericos('txtValorMens');
    if (isset($_POST['txtDataMens']) && ("'" . $dataFim . "'" !== valoresData('txtDataMens'))) {
        $valorMens = ($valAnt / $diasDif) * $difNovaDt;
        $queryMens = "select id_mens from sf_vendas_planos_mensalidade where id_plano_mens in(select id_plano_mens from sf_vendas_planos_mensalidade where id_mens = " . $_POST['atualizaMensalidade'] . ") and id_mens > " . $_POST['atualizaMensalidade'] . " and dt_pagamento_mens is null order by dt_inicio_mens";
        $cur = odbc_exec($con, $queryMens);
        $dtIniParc = valoresData('txtDataMens');
        $meses = 0;
        while ($RFP = odbc_fetch_array($cur)) {
            $queryProxMens = $queryProxMens . "update sf_vendas_planos_mensalidade set dt_inicio_mens = dateadd(day,1,dateadd(month," . $meses . "," . $dtIniParc . ")), dt_fim_mens= dateadd(month," . ($meses + 1) . "," . $dtIniParc . ") where id_mens = " . $RFP['id_mens'] . ";";
            $meses = $meses + 1;
        }
        $queryProxMens = $queryProxMens . " update sf_vendas_planos set dt_fim = dbo.FU_PLANO_FIM(id_plano) where id_plano = (select id_plano_mens from sf_vendas_planos_mensalidade where id_mens = " . $_POST['atualizaMensalidade'] . ");";
        $dt_fim = " dt_fim_mens = " . valoresData('txtDataMens') . ", ";
    }
    $query = "  set dateformat dmy;
    BEGIN TRANSACTION
        update sf_vendas_planos_mensalidade set " . $dt_fim . " valor_mens = (" . $valorMens . " - " . $valAcess . "),dt_alt = getdate(), usu_alt = '" . $_SESSION["login_usuario"] . "' where id_mens = " . $_POST['atualizaMensalidade'] . ";
        " . $queryProxMens . "
        insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values ('sf_vendas_planos_mensalidade', " . $_POST['atualizaMensalidade'] . ", '" . $_SESSION["login_usuario"] . "', 'A', 'ALTERACAO MENS. VALOR ANT " . $valAnt . "', GETDATE(), " . $favorecido . ");
    IF @@ERROR = 0
    COMMIT
    ELSE
    ROLLBACK;";
    odbc_exec($con, $query) or die(odbc_errormsg());
    if (!isset($_POST['addContrato']) && !isset($_POST['trocarPlano'])) {
        echo "YES";
    }
}

if (isset($_GET['listCartoes'])) {
    $query = "select * from sf_fornecedores_despesas_cartao where id_fornecedores_despesas = " . $_GET['listCartoes'] . " order by id_cartao desc";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["id_cartao"] = utf8_encode($RFP["id_cartao"]);
        $row["legenda_cartao"] = utf8_encode($RFP["legenda_cartao"]);
        $row["validade_cartao"] = escreverData($RFP['validade_cartao'], 'm/Y');
        $records[] = $row;
    }
    echo json_encode($records);
}

if (isset($_POST['salvaPagPlano'])) {
    include "ClientesPlanosServerPagamentos.php";
}

if (isset($_GET['verificaDccPago'])) {
    $exiteDCC = "N";
    $cur = odbc_exec($con, "select id_transacao from sf_vendas_itens_dcc where id_plano_item = " . $_GET['verificaDccPago'] . " and processorMessage = 'APPROVED' and status_transacao = 'A'");
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['id_transacao'] > 0) {
            $exiteDCC = "S";
        }
    }
    echo $exiteDCC;
}

if (isset($_GET['getPlano'])) {
    $query = "Select A.id_plano, A.id_turno, A.id_prod_plano, A.dt_inicio, A.dt_fim, A.dt_cadastro, A.usuario_resp, A.dt_cancelamento,
    isnull(ROUND((((select sum(valor_total) from sf_vendas_itens where id_item_venda in(select id_item_venda_mens from sf_vendas_planos_mensalidade where id_plano_mens = A.id_plano)) /
    (select sum(DATEDIFF(day,dt_inicio_mens,dt_fim_mens)) from sf_vendas_planos_mensalidade where id_item_venda_mens is not null and id_plano_mens = A.id_plano)
    ) * DATEDIFF(day,case when A.dt_inicio > GETDATE() then A.dt_inicio  else GETDATE() end,A.dt_fim)),2),0) valorcredito,
    isnull(ROUND(case when A.dt_fim > GETDATE() then (select sum(valor_total) from sf_vendas_itens where id_item_venda in(select id_item_venda_mens from sf_vendas_planos_mensalidade where id_plano_mens = A.id_plano)) else 0 end - ((datediff(month,dt_inicio,dateadd(day,1,dt_fim)) * (select top 1  valor_unico/(case when parcela=0 then 1 else parcela end)
    from sf_produtos_parcelas where id_produto = A.id_prod_plano and parcela > 0 order by parcela)) / datediff(day,dt_inicio,dt_fim)) * (case when A.dt_inicio < GETDATE() and A.dt_fim > GETDATE() then DATEDIFF(day,A.dt_inicio,case when A.dt_inicio > GETDATE() then A.dt_fim else GETDATE() end) else 0 end),2),0) valorcreditoFinanceiro,
    (12 * (YEAR(dt_fim) - YEAR(GETDATE())) + ((MONTH(dt_fim) - MONTH(GETDATE())-1)) + SIGN(DAY(dt_fim) / DAY(GETDATE()))) mesesrest,
    C.nome nome_resp_cancel, B.nome nome_resp, A.dt_transferencia, D.nome nome_transf, E.razao_social nome_origem,
    (select count(id_planos_ferias) from sf_vendas_planos_ferias F where dt_fim > GETDATE() and dt_cancelamento is null and F.id_plano = A.id_plano) ferias
    from sf_vendas_planos A inner join sf_usuarios B on A.usuario_resp = B.id_usuario
    left join sf_usuarios C on A.usuario_canc = C.id_usuario
    left join sf_usuarios D on A.usuario_transf = D.id_usuario
    left join sf_fornecedores_despesas E on A.id_user_origem = E.id_fornecedores_despesas
    where id_plano = " . $_GET['getPlano'];
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $date = '';
        $dataTransf = '';
        if (!is_null($RFP['dt_cancelamento'])) {
            $date = escreverData($RFP['dt_cancelamento']);
        }
        if (!is_null($RFP['dt_transferencia'])) {
            $dataTransf = escreverData($RFP['dt_transferencia']);
        }
        $local[] = array('id_plano' => $RFP['id_plano'], 'id_turno' => $RFP['id_turno'], 'id_prod_parc' => $RFP['id_prod_parc'],
            'dt_inicio' => escreverData($RFP['dt_inicio']), 'dt_fim' => escreverData($RFP['dt_fim']), 'dt_cadastro' => escreverData($RFP['dt_cadastro']),
            'nome_resp' => utf8_encode($RFP['nome_resp']), 'duracao' => utf8_encode($RFP['duracao']), 'valorcredito' => escreverNumero(($RFP['valorcredito'] > 0 ? $RFP['valorcredito'] : 0)),
            'dt_cancelamento' => $date, 'nome_resp_cancel' => utf8_encode($RFP['nome_resp_cancel']), 'valorcreditoFinanceiro' => escreverNumero(($RFP['valorcreditoFinanceiro'] > 0 ? $RFP['valorcreditoFinanceiro'] : 0)), 'mesesrest' => ($RFP['mesesrest'] > 0 ? $RFP['mesesrest'] : 0),
            'id_produto' => $RFP['id_prod_plano'], 'dt_transferencia' => $dataTransf, 'nome_transf' => utf8_encode($RFP['nome_transf']), 'nome_origem' => utf8_encode($RFP['nome_origem']), 'ferias' => $RFP['ferias']);
    }
    echo(json_encode($local));
}

if (isset($_GET['listHistCreditos'])) {
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );    
    $cur = odbc_exec($con, "select id_credito, valor, data, historico, nome from sf_creditos A
    inner join sf_usuarios B on A.usuario_resp = B.id_usuario where A.inativo = 0 and id_fornecedor_despesa = " . $_GET['listHistCreditos'] . " and tipo = '" . $_GET['tipo'] . "'");
    while ($RFP = odbc_fetch_array($cur)) {
        $cont = $cont + 1;
        $row = array();
        $row[] = escreverData($RFP["data"]);
        $row[] = escreverNumero($RFP["valor"]);
        $row[] = tableFormato($RFP["historico"], 'T', '', '');
        $row[] = tableFormato($RFP["nome"], 'T', '', '');
        $records['aaData'][] = $row;
    }
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}

if (isset($_GET['listAssinaturas'])) {
    $total = 0;
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );    
    if (is_numeric($_GET['id'])) {
        $pasta = "./../../../Pessoas/" . $contrato . "/Assinaturas/" . $_GET["id"] . "/";
        if (file_exists($pasta)) {
            $cdir = scandir($pasta);
            foreach ($cdir as $key => $value) {
                if (!in_array($value, array(".", ".."))) {
                    $id_plano = explode("_", $value)[0];
                    $id_planoArray = explode(",", $id_plano);
                    if ($id_plano == $_GET['listAssinaturas'] || in_array($_GET['listAssinaturas'], $id_planoArray)) {
                        $row = array();
                        $row[] = date($lang['dmy'] . " H:i:s", filectime($pasta . $value));
                        $row[] = "Contrato Cod. " . (explode(".", explode("_", $value)[1])[0]);
                        $row[] = "<center><img src=\"" . $pasta . $value . "\" alt=\"img\" style=\"height: 70px;\"></center>";
                        if($ckb_aca_documento_ == 1) {
                            $row[] = "<center><input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\" onclick=\"RemoverAss('" . utf8_encode($value) . "')\" /></center>";
                        } else {
                            $row[] = "";
                        }
                        $output['aaData'][] = $row;
                        $total++;
                    }
                }   
            }
            $output['iTotalRecords'] = $total;
            $output['iTotalDisplayRecords'] = $total;   
        }
    }
    echo json_encode($output);
    odbc_close($con);
}

if (isset($_GET['listAssinaturasResp'])) {
    $total = 0;
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );    
    $pasta = "./../../../Pessoas/" . $contrato . "/Empresa/Assinatura/";
    if (file_exists($pasta)) {
        $cdir = scandir($pasta);
        foreach ($cdir as $key => $value) {
            if ($value == "responsavel.png") {
                $row = array();
                $row[] = date($lang['dmy'] . " H:i:s", filectime($pasta . $value));
                if($ckb_aca_documento_ == 1) {
                    $row[] = "<center><input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\" onclick=\"RemoverAssinatura('" . utf8_encode($value) . "')\" /></center>";
                } else {
                    $row[] = "";
                }
                $output['aaData'][] = $row;
                $total++;
            }
        }
        $output['iTotalRecords'] = $total;
        $output['iTotalDisplayRecords'] = $total; 
    }
    echo json_encode($output);
    odbc_close($con);
}

if (isset($_POST['btnDelAss'])) {
    $url = "./../../../Pessoas/" . $contrato . "/Assinaturas/" . $_POST["txtId"];
    if (file_exists($url . "/" . utf8_decode($_POST['txtFileName']))) {
        unlink($url . "/" . utf8_decode($_POST['txtFileName']));
        echo "YES";
    }
}

if (isset($_POST['btnDelAssResp'])) {
    $url = "./../../../Pessoas/" . $contrato . "/Empresa/Assinatura";
    if (file_exists($url . "/responsavel.png")) {
        unlink($url . "/responsavel.png");
        echo "YES";
    }
}

if (isset($_POST['addCreditoCli'])) {
    odbc_autocommit($con, false);
    $valor = valoresNumericos('txtValor');
    $query = "set dateformat dmy;";
    $query .= "insert into sf_creditos (id_fornecedor_despesa, valor, data, tipo, id_plano, id_solicitacao_autorizacao, historico, usuario_resp, inativo) values
    (" . $_POST['addCreditoCli'] . ", " . $valor . ", getdate(), 'C'," . $_POST['idPlano'] . ",null,'" . utf8_decode('CONVERSÃO CREDITO PLANO ' . $_POST['txtDescrPlano']) . "' ," . $_SESSION["id_usuario"] . ",0);";
    $query .= "update sf_vendas_planos set dt_cancelamento = getdate(), usuario_canc = " . $_SESSION["id_usuario"] . " where id_plano = " . $_POST['idPlano'] . ";";
    $query .= "update sf_venda_parcelas set inativo = 2 where tipo_documento = 12 and data_pagamento is null
    and venda in (select id_venda from sf_vendas_planos
    inner join sf_vendas_planos_mensalidade on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens
    inner join sf_vendas_itens on sf_vendas_planos_mensalidade.id_item_venda_mens = sf_vendas_itens.id_item_venda
    where id_plano = " . $_POST['idPlano'] . ");";
    $query .= "update sf_creditos set inativo = 2 where id_venda_quitacao is null and id_venda in (select id_venda from sf_vendas_planos
    inner join sf_vendas_planos_mensalidade on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens
    inner join sf_vendas_itens on sf_vendas_planos_mensalidade.id_item_venda_mens = sf_vendas_itens.id_item_venda
    where id_plano = " . $_POST['idPlano'] . ");";
    $query .= "delete from sf_vendas_planos_mensalidade where dt_pagamento_mens is null and id_plano_mens = " . $_POST['idPlano'];
    $objExec = odbc_exec($con, $query);
    if (!$objExec) {
        odbc_rollback($con);
        echo "ERROR";
        exit();
    }
    if ($objExec) {
        odbc_commit($con);
        echo "YES";
    }
}

if (isset($_GET['saldoCredito'])) {
    $cur = odbc_exec($con, "select ISNULL(SUM(case when tipo = 'C' then valor else valor * -1 end),0) saldo_credito
    from sf_creditos where inativo in (0,2) and id_fornecedor_despesa = " . $_GET['saldoCredito'] . " and (id_venda_quitacao is null or id_venda_quitacao = id_venda) and id_solicitacao_autorizacao is null");
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('saldo_credito' => escreverNumero($RFP['saldo_credito']));
    }
    echo json_encode($local);
}

if (isset($_GET['getNumParcelas'])) {
    $NumParc = 0; 
    if (is_numeric($_GET['getNumParcelas'])) {
        $cur = odbc_exec($con, "select min(pp.parcela) parcela, (select max(DCC_FORMA_PAGAMENTO) from sf_configuracao) fp 
        from sf_vendas_planos_mensalidade m inner join sf_produtos_parcelas pp on pp.id_parcela = m.id_parc_prod_mens
        where id_mens in (" . $_GET['getNumParcelas'] . ")");
        while ($RFP = odbc_fetch_array($cur)) {
            if ($RFP['parcela'] == 0) {
                $NumParc = $RFP['fp'];
            }
        }
    }
    echo $NumParc;
}

if (isset($_GET['getDiasRestFerias'])) {
    $query = "select dt_cancelamento, isnull(dias_ferias,0) - (select isnull(sum(dias),0) dias from sf_vendas_planos_ferias vf
    inner join sf_vendas_planos_mensalidade vm on vm.id_plano_mens = vf.id_plano
    and vf.dt_inicio between vm.dt_inicio_mens and vm.dt_fim_mens where vf.dt_cancelamento is null and vm.dt_pagamento_mens is not null
    and vf.id_plano = A.id_plano and vm.id_plano_mens = A.id_plano and id_mens in (select max(id_mens) from sf_vendas_planos_mensalidade
    where id_plano_mens = A.id_plano and dt_pagamento_mens is not null)) dias
    from sf_vendas_planos A where id_plano = " . $_GET['getDiasRestFerias'];
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $date = '';
        if (!is_null($RFP['dt_cancelamento'])) {
            $date = escreverData($RFP['dt_cancelamento']);
        }
        $local[] = array('dias' => $RFP['dias'], 'dt_cancelamento' => $date);
    }
    echo json_encode($local);
}

if (isset($_POST['addCongelamento'])) {
    $dtIni = date("Y-m-d", strtotime(str_replace('_', '-', $_POST['txtDtIni'])));
    $dtFim = escreverDataSoma(escreverData($dtIni), " +" . $_POST['txtDias'] . " day");
    $query = "set dateformat dmy;
    BEGIN TRANSACTION
        insert into sf_vendas_planos_ferias (id_plano, motivo, dias, dt_inicio, dt_fim, dt_inclusao, id_usuario_resp) values
        (" . $_POST['addCongelamento'] . ", '" . utf8_decode($_POST['txtMotivo']) . "'," . $_POST['txtDias'] . ", " . valoresData('txtDtIni') . ",'" . $dtFim . "',getdate()," . $_SESSION["id_usuario"] . " );
        update sf_vendas_planos set dt_fim = dbo.FU_PLANO_FIM(id_plano) where id_plano = " . $_POST['addCongelamento'] . ";
    IF @@ERROR = 0
    COMMIT
    ELSE
    ROLLBACK;";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}

if (isset($_GET['listHistCongelamento'])) {
    $cur = odbc_exec($con, "select id_planos_ferias, dias, A.dt_inicio, A.dt_fim, motivo, dt_inclusao,
    A.dt_cancelamento, P.dt_cancelamento dt_canc, case when A.dt_cancelamento is null then B.nome else C.nome end nome
    from sf_vendas_planos_ferias A inner join sf_vendas_planos P on P.id_plano = A.id_plano
    left join sf_usuarios B on A.id_usuario_resp = B.id_usuario
    left join sf_usuarios C on A.id_usuario_canc = C.id_usuario where A.id_plano = " . $_GET['listHistCongelamento'] . " order by A.dt_cancelamento");
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    while ($RFP = odbc_fetch_array($cur)) {
        $fontColor = "";
        if ($RFP["dt_cancelamento"]) {
            $fontColor = "style=\"color: red;\"";
        }
        $cont = $cont + 1;
        $row = array();
        $row[] = "<center " . $fontColor . ">" . tableFormato($RFP["dias"], 'T', '', '') . "</center>";
        $row[] = "<div " . $fontColor . ">" . escreverData($RFP["dt_inicio"]) . " a " . escreverData($RFP["dt_fim"]) . "</div>";
        $row[] = "<div " . $fontColor . ">" . tableFormato($RFP["motivo"], 'T', '', '') . "</div>";
        $row[] = "<div " . $fontColor . ">" . escreverData($RFP["dt_inclusao"]) . "</div>";
        $row[] = "<div " . $fontColor . ">" . tableFormato($RFP["nome"], 'T', '', '') . "</div>";
        if ($RFP["dt_cancelamento"] != "" || $RFP["dt_canc"] != "" || ($ckb_aca_fer_par_ == 0 && $ckb_aca_fer_exc_ == 0)) {
            $row[] = "";
        } else {
            $row[] = "<center><div style=\"text-align:left;\" class=\"btn-group\">
            <button style=\"height: 13px;background-color: gray;\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\">
                <span style=\"margin-top: 1px;\" class=\"caret\"></span>
            </button>
            <ul style=\"left:-130px;\" class=\"dropdown-menu\">" .
                    ($ckb_aca_fer_par_ == 1 ? "<li><a data-id=\"" . $RFP["id_planos_ferias"] . "\" class=\"parar\">Parar</a></li>" : "") .
                    ($ckb_aca_fer_exc_ == 1 ? "<li><a data-id=\"" . $RFP["id_planos_ferias"] . "\" class=\"excluir\">Excluir</a></li>" : "") .
                    "</ul></div></center>";
        }
        $records['aaData'][] = $row;
    }
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}

if (isset($_POST['delCongelamento'])) {
    $query = "set dateformat dmy;
    BEGIN TRANSACTION
        update sf_vendas_planos_ferias set dt_cancelamento = getdate(), id_usuario_canc = " . $_SESSION["id_usuario"] . " where id_planos_ferias = " . $_POST['delCongelamento'] . ";
        update sf_vendas_planos set dt_fim = dbo.FU_PLANO_FIM(id_plano) where id_plano = (select id_plano from sf_vendas_planos_ferias where id_planos_ferias = " . $_POST['delCongelamento'] . ");
    IF @@ERROR = 0
    COMMIT
    ELSE
    ROLLBACK;";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}

if (isset($_POST['pararCongelamento'])) {
    $cur = odbc_exec($con, "select isnull(DATEDIFF(DAY,dt_inicio,GETDATE()),0) diasIni,  isnull(DATEDIFF(DAY,GETDATE(), dt_fim),0) diasFim from sf_vendas_planos_ferias where id_planos_ferias = " . $_POST['pararCongelamento']);
    while ($RFP = odbc_fetch_array($cur)) {
        $diasIni = $RFP["diasIni"];
        $diasFim = $RFP["diasFim"];
    }
    if ($diasIni <= 0 || $diasFim <= 0) {
        echo "NO";
    } else {
        $query = "set dateformat dmy;
        BEGIN TRANSACTION
            update sf_vendas_planos_ferias set dt_fim = GETDATE(), dias = DATEDIFF(DAY,dt_inicio,GETDATE()) where id_planos_ferias = " . $_POST['pararCongelamento'] . ";
            update sf_vendas_planos set dt_fim = dbo.FU_PLANO_FIM(id_plano) where id_plano = (select id_plano from sf_vendas_planos_ferias where id_planos_ferias = " . $_POST['pararCongelamento'] . ")
        IF @@ERROR = 0
        COMMIT
        ELSE
        ROLLBACK;";
        odbc_exec($con, $query) or die(odbc_errormsg());
        echo "YES";
    }
}

if (isset($_POST['transfVeiculo'])) {
    $id_plano = "";
    $cur = odbc_exec($con, "select id_plano from sf_vendas_planos where id_veiculo = " . $_POST['transfVeiculo']);
    while ($RFP = odbc_fetch_array($cur)) {
        $id_plano = $RFP["id_plano"];
    }
    if ($id_plano == "") {        
        $query = "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
        ('sf_fornecedores_despesas_veiculo', " . $_POST['transfVeiculo'] . ", '" . $_SESSION["login_usuario"] . "', 'T',
        'TRANSFERINDO VEICULO (" . $_POST['transfVeiculo'] . ") PARA " . $_POST['txtTransfParaCod'] . " - ' + " . valoresTexto('txtMotivo') . ", GETDATE()," . $_POST['txtIdDe'] . ");";                
        $query .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
        ('sf_fornecedores_despesas_veiculo', " . $_POST['transfVeiculo'] . ", '" . $_SESSION["login_usuario"] . "', 'T',
        'TRANSFERIDO VEICULO (" . $_POST['transfVeiculo'] . ") DE " . $_POST['txtIdDe'] . " - ' + " . valoresTexto('txtMotivo') . ", GETDATE()," . $_POST['txtTransfParaCod'] . ");";        
        $query .= "update sf_fornecedores_despesas_veiculo set id_fornecedores_despesas = " . $_POST['txtTransfParaCod'] . " where id = " . $_POST['transfVeiculo'];
        odbc_exec($con, $query);
        echo "YES";
    } else {
        $_POST['transfPlano'] = $id_plano;
    }
}

if (isset($_POST['transfPlano'])) {
    $dataIni = "";
    $query = "select dbo.FU_PLANO_FIM(A.id_plano) data, id_veiculo from sf_vendas_planos A 
    inner join sf_produtos B on A.id_prod_plano = B.conta_produto
    inner join sf_fornecedores_despesas D on D.id_fornecedores_despesas = favorecido
    left join sf_vendas_planos_dcc_agendamento C on A.id_plano = C.id_plano_agendamento
    where favorecido = " . $_POST['txtTransfParaCod'] . " and B.conta_produto in (select id_prod_plano from sf_vendas_planos where id_plano = " . $_POST['transfPlano'] . ")
    and ((DATEADD(day,(select ACA_PLN_DIAS_RENOVACAO from sf_configuracao),A.dt_fim) > GETDATE() and B.parcelar = 0) or
    (A.dt_fim >= GETDATE() and B.parcelar = 1) or ((select COUNT(*) from sf_vendas_planos_mensalidade C where C.id_plano_mens = A.id_plano and dt_pagamento_mens is null) > 0))
    order by data desc";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $dataIni = escreverData($RFP["data"]);
    }
    if ($dataIni == "") {
        $query = "set dateformat dmy;update sf_vendas_planos set dt_transferencia = getdate()
        , motivo_transf = " . valoresTexto('txtMotivo') . "
        , dt_anteriortransf = dt_inicio
        , dt_inicio = getdate()
        , favorecido = " . $_POST['txtTransfParaCod'] . "
        , id_user_origem = " . $_POST['txtIdDe'] . "
        , usuario_transf = " . $_SESSION["id_usuario"] . " where id_plano = " . $_POST['transfPlano'] . ";";
    } else {
        $query = "set dateformat dmy;update sf_vendas_planos set dt_transferencia = getdate()
        , motivo_transf = " . valoresTexto('txtMotivo') . "
        , dt_anteriortransf = dt_inicio
        , dt_inicio = " . valoresData2($dataIni) . "
        , favorecido = " . $_POST['txtTransfParaCod'] . "
        , id_user_origem = " . $_POST['txtIdDe'] . "
        , usuario_transf = " . $_SESSION["id_usuario"] . " where id_plano = " . $_POST['transfPlano'] . ";";
        $query .= "update sf_vendas_planos set dt_fim = dateadd(day,datediff(day,getdate(),dt_fim),dt_inicio) where id_plano = " . $_POST['transfPlano'] . ";";
        $query .= "update sf_vendas_planos_mensalidade set dt_inicio_mens = dt_inicio, dt_fim_mens = dt_fim
        from sf_vendas_planos_mensalidade inner join sf_vendas_planos on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens
        where id_mens in (select max(id_mens) from sf_vendas_planos_mensalidade where id_item_venda_mens is not null and id_plano_mens = " . $_POST['transfPlano'] . ");";
    }    
    $id_veiculo = "";
    $cur = odbc_exec($con, "select id_veiculo from sf_vendas_planos where id_plano = " . $_POST['transfPlano'] . ";");
    while ($RFP = odbc_fetch_array($cur)) {
        $id_veiculo = $RFP["id_veiculo"];
    }
    if (is_numeric($id_veiculo)) {
        $query .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
        ('sf_fornecedores_despesas_veiculo', " . $id_veiculo . ", '" . $_SESSION["login_usuario"] . "', 'T',
        'TRANSFERINDO VEICULO (" . $id_veiculo . ") PARA " . $_POST['txtTransfParaCod'] . " - ' + " . valoresTexto('txtMotivo') . ", GETDATE()," . $_POST['txtIdDe'] . ");";                
        $query .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
        ('sf_fornecedores_despesas_veiculo', " . $id_veiculo . ", '" . $_SESSION["login_usuario"] . "', 'T',
        'TRANSFERIDO VEICULO (" . $id_veiculo . ") DE " . $_POST['txtIdDe'] . " - ' + " . valoresTexto('txtMotivo') . ", GETDATE()," . $_POST['txtTransfParaCod'] . ");";                
        $query .= "update sf_fornecedores_despesas_veiculo set id_fornecedores_despesas = " . $_POST['txtTransfParaCod'] . "
        from sf_fornecedores_despesas_veiculo inner join sf_vendas_planos on id_veiculo = id
        where id_plano = " . $_POST['transfPlano'] . ";";
    }
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}

if (isset($_GET['listPagamentosTodos'])) {
    $query = "select V.id_venda, V.cliente_venda, V.data_venda dataVenda, V.sys_login, VP.valor_parcela, VP.id_parcela, VP.tipo_documento,
    TD.descricao TpDocumento, VI.produto id_produto,  VI.valor_total valor_item, P.descricao Produto, ISNULL(PP.parcela,0) meses, VP.data_parcela,
    F.razao_social, V.status, V.dt_estorno dt_estorno, VE.login_user user_estorno, VP.data_pagamento, VI.id_item_venda, V.comentarios_venda, VM.dt_inicio_mens
    from sf_vendas V inner join sf_vendas_itens VI on V.id_venda = VI.id_venda
    left join sf_vendas_planos_mensalidade VM on VI.id_item_venda = VM.id_item_venda_mens
    left join sf_vendas_planos PL on VM.id_plano_mens = PL.id_plano
    left join sf_venda_parcelas VP on V.id_venda = VP.venda
    left join sf_tipo_documento TD on VP.tipo_documento = TD.id_tipo_documento    
    left join sf_produtos_parcelas PP on PP.id_parcela = VM.id_parc_prod_mens
    left join sf_produtos P on VI.produto = P.conta_produto
    left join sf_fornecedores_despesas F on (PL.favorecido = F.id_fornecedores_despesas and PL.favorecido <> V.cliente_venda)
    left join sf_usuarios VE on V.user_estorno = VE.id_usuario
    where cliente_venda = " . $_GET['listPagamentosTodos'] . " order by data_venda desc,  VP.id_parcela";
    $cur = odbc_exec($con, $query);
    $idVenda = 0;
    $local = "";
    $itemVenda = array();
    $produtos = array();
    $parcelas = array();
    $idParcela = "";
    while ($RFP = odbc_fetch_array($cur)) {
        if ($idVenda != $RFP['id_venda'] && $idVenda !== 0) {
            $result = montaItens($parcelas, $produtos, $dataVenda, $user, $idVenda, $status, $userestorno, $dtestorno, $comentariosVenda);
            $local .= $result[0];
            $TotalGeral += $result[1];
            $itensPag = "";
            $itensVenda = "";
            $produtos = array();
            $parcelas = array();
        }
        $nome = '';
        if (tableFormato($RFP['razao_social'], 'T', '', '') !== "") {
            $nome = " - FAVORECIDO: " . tableFormato($RFP['razao_social'], 'T', '', '');
        }
        $detalhe = (strlen($RFP['dt_inicio_mens']) > 0 ? " - Venc. " . escreverData($RFP['dt_inicio_mens']) : "");
        $item = $RFP['Produto'] . $detalhe . " - " . escreverNumero($RFP['valor_item'], 1) . ($RFP['meses'] != 0 ? " - " . strtoupper(legendaPlano($RFP['meses'])) : "") . " " . $nome;
        if (array_search($RFP['id_item_venda'], $itemVenda) === false) {
            array_push($itemVenda, $RFP['id_item_venda']);
            array_push($produtos, $item);
        }
        if ($RFP['id_parcela'] !== $idParcela) {
            array_push($parcelas, array($RFP['TpDocumento'], $RFP['valor_parcela'], escreverData($RFP['data_parcela']), $RFP['meses'], $RFP['data_pagamento']));
        }
        $dataVenda = escreverData($RFP['dataVenda']);
        $user = $RFP['sys_login'];
        $idVenda = $RFP['id_venda'];
        $idParcela = $RFP['id_parcela'];
        $status = $RFP['status'];
        $userestorno = $RFP['user_estorno'];
        $dtestorno = escreverData($RFP['dt_estorno']);
        $comentariosVenda = tableFormato($RFP['comentarios_venda'], 'T', '', '');
    }
    $result = montaItens($parcelas, $produtos, $dataVenda, $user, $idVenda, $status, $userestorno, $dtestorno, $comentariosVenda);
    $local .= $result[0];
    $TotalGeral += $result[1];
    $result[0] = $local;
    $result[1] = escreverNumero($TotalGeral);
    echo json_encode($result);
}

function montaItemGroup($parcelas) {
    $grupos = array();
    $item = "";
    $valor = 0;
    $total = 0;
    $dcc = "";
    foreach ($parcelas as $value) {
        if ($value[0] != $item && strlen($item) > 0) {
            $grupos[] = array($item, $valor, $total, $dcc);
            $item = "";
            $valor = 0;
            $total = 0;
        }
        $item = $value[0];
        $valor += $value[1];
        $dcc = $value[3];
        $total++;
    }
    if (strlen($item) > 0) {
        $grupos[] = array($item, $valor, $total, $dcc);
    }
    return $grupos;
}

function montaItens($parcelas, $produtos, $dataVenda, $user, $idVenda, $status, $userestorno, $dtestorno, $comentariosVenda) {
    $local = "";
    if (sizeof($produtos) > 0) {
        $itensPag = "";
        $itensVenda = "";
        $isDcc = 'A';
        $totalVenda = 0;
        foreach (montaItemGroup($parcelas) as $grupo) {
            $itensPag .= "<li data-jstree='{ \"icon\" :\"ico-dollar\" }'>" .
                    utf8_encode($grupo[0]) . ": " . escreverNumero($grupo[1], 1);
            if ($grupo[2] > 1) {
                foreach ($parcelas as $value) {
                    if ($grupo[0] == $value[0]) {
                        $itensPag .= "<ul><li data-jstree='{ \"icon\" :\"ico-dollar\"}'>" . $value[2] . " - Valor: " . escreverNumero($value[1]) . " - Status: " . ($value[4] == "" ? "Em Aberto" : "Baixa Em " . escreverData($value[4])) . "</li></ul>";
                    }
                }
            }
            $itensPag .= "</li>";
            $isDcc = ($status !== "Aprovado" && $grupo[3] === "0") ? 'E' : 'A';
            $totalVenda += ($grupo[0] != "CARTEIRA" ? $grupo[1] : 0);
        }
        foreach ($produtos as $value) {
            $itensVenda .= "<li data-jstree='{ \"icon\" :\"ico-caret-right\" }' onclick='parent.FecharBoxPagamento(" . $idVenda . ");'>" . utf8_encode($value) . " </li>";
        }
        $local .= "<ul><li style='color: red;' > <span " . ($status !== "Aprovado" ? "style=\"color:red;\"" : "") . ">" . $dataVenda .
                " - " . escreverNumero($totalVenda, 1) .
                " - Recebido por: " . $user . ($status !== "Aprovado" ? " - Estornado por: " . $userestorno . " Em: " . $dtestorno : "") . ($comentariosVenda != "" ? " (" . $comentariosVenda . ")" : "") . "</span>
        <ul>" . $itensPag . $itensVenda . "</ul></li></ul>";
    }
    if ($status !== "Aprovado") {
        $totalVenda = 0;
    }
    return array($local, $totalVenda);
}

if (isset($_GET['listPlanosTodos'])) {
    $cur = odbc_exec($con, "set dateformat dmy;
    select id_plano, P.descricao, dt_inicio, dt_fim, dt_cancelamento, obs_cancelamento
    from sf_vendas_planos VP inner join sf_produtos P on VP.id_prod_plano = P.conta_produto
    where favorecido = " . $_GET['listPlanosTodos'] . "order by VP.dt_cadastro desc");
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    while ($RFP = odbc_fetch_array($cur)) {
        $cont = $cont + 1;
        $row = array();
        $row[] = $RFP["id_plano"] . " - " . tableFormato($RFP["descricao"], 'T', '', '');
        $row[] = "<center>" . escreverData($RFP["dt_inicio"]) . "</center>";
        $row[] = "<center>" . escreverData($RFP["dt_fim"]) . "</center>";
        $row[] = "<center>" . escreverData($RFP["dt_cancelamento"]) . "</center>";
        $row[] = tableFormato($RFP["obs_cancelamento"], 'T', '', '');
        $records['aaData'][] = $row;
    }
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}

if (isset($_GET['listTransferencia'])) {
    $cur = odbc_exec($con, "set dateformat dmy;
    select dt_transferencia,favorecido,f1.razao_social nome_para,id_user_origem,f2.razao_social nome_de,id_prod_plano, 
    p.descricao,dt_inicio,dt_fim,motivo_transf,usuario_transf,u.login_user, placa
    from sf_vendas_planos v inner join sf_fornecedores_despesas f1 on f1.id_fornecedores_despesas = v.favorecido
    inner join sf_fornecedores_despesas f2 on f2.id_fornecedores_despesas = v.id_user_origem
    inner join sf_produtos p on p.conta_produto = v.id_prod_plano inner join sf_usuarios u on u.id_usuario = v.usuario_transf
    left join sf_fornecedores_despesas_veiculo on id = id_veiculo    
    where dt_transferencia is not null and (favorecido = " . $_GET['listTransferencia'] . " or id_user_origem = " . $_GET['listTransferencia'] . ") order by dt_transferencia desc");
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    while ($RFP = odbc_fetch_array($cur)) {
        $cont = $cont + 1;
        $row = array();
        $row[] = tableFormato($RFP["dt_transferencia"], 'D', 'C', '');
        if ($_GET['listTransferencia'] == $RFP["favorecido"]) {
            $row[] = "Rec. por";
            $row[] = $RFP["id_user_origem"] . " - " . tableFormato($RFP["nome_de"], 'T', '', '');
        } else {
            $row[] = "Transf. para";
            $row[] = $RFP["favorecido"] . " - " . tableFormato($RFP["nome_para"], 'T', '', '');
        }
        $row[] = tableFormato($RFP["descricao"], 'T', '', '');
        $row[] = "<center>" . escreverData($RFP["dt_inicio"]) . "-" . escreverData($RFP["dt_fim"]) . "</center>";
        $row[] = (strlen($RFP["placa"]) > 0 ? $RFP["placa"] . " - " : "") . tableFormato($RFP["motivo_transf"], 'T', '', '');
        $row[] = tableFormato($RFP["login_user"], 'T', 'C', '');
        $records['aaData'][] = $row;
    }
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}

if (isset($_GET['getConfiguracao'])) {
    $credencial = "0";    
    $query = "select credencial from sf_fornecedores_despesas_cartao where id_fornecedores_despesas = " . valoresNumericos("txtId");
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $credencial = $RFP["credencial"];    
    }    
    $query = "select DCC_FORMA_PAGAMENTO,DCC_FORMA_PAGAMENTO2 from sf_configuracao where id = 1";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["dcc_forma_pgt"] = ($credencial == "1" ? utf8_encode($RFP["DCC_FORMA_PAGAMENTO2"]) : utf8_encode($RFP["DCC_FORMA_PAGAMENTO"]));
        $records[] = $row;
    }
    echo json_encode($records);
}

if (isset($_GET['listFavorito'])) {
    $idFilial = 0;
    $query = "select ISNULL(filial, (SELECT id_filial FROM dbo.sf_filiais where id_filial = 1)) filial from dbo.sf_usuarios where id_usuario = '" . $_SESSION["id_usuario"] . "'";
    $cur = odbc_exec($con, $query) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $idFilial = $RFP['filial'];
    }    
    $query = "select conta_produto, descricao, preco_venda, nao_vender_sem_estoque, dbo.ESTOQUE_FILIAL(conta_produto," . $idFilial . ") estoque_atual,
            (select max(data_venda) from sf_vendas_itens A inner join sf_vendas B on A.id_venda = B.id_venda
            where produto = conta_produto and cliente_venda = " . $_GET['listFavorito'] . " and status = 'Aprovado') data_venda
            from sf_produtos where tipo = '" . ($_GET['tp'] == "P" ? "P" : "S") . "'
            and favorito = 1 and inativa = 0";
    $cur = odbc_exec($con, $query);
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    while ($RFP = odbc_fetch_array($cur)) {
        $cont = $cont + 1;
        $row = array();
        $row[] = tableFormato($RFP["descricao"], 'T', '', '');
        $row[] = escreverNumero($RFP["preco_venda"]);
        $row[] = escreverData($RFP["data_venda"]);
        $btnContent = "";
        if ($ckb_aca_abomens_ == 1) {
            $btnContent .= "<li><a onClick=\"AbonarProd(" . $RFP['conta_produto'] . ")\" data-id=\"" . $RFP['conta_produto'] . "\" class=\"Abonar\">Abonar</a></li>";
        }        
        $row[] = "<center><div style=\"text-align:left;\" class=\"btn-group\"><button type=\"button\" " . ($RFP['nao_vender_sem_estoque'] == '1' && $RFP['estoque_atual'] <= 0 ? "disabled" : "") . " class=\"btn green\" style=\"line-height:10px;margin:0 0 1px;padding: 5px 5px 4px 5px;\" onClick=\"incluirItemPag('" . ($_GET['tp'] == "P" ? "PRODUTO" : "SERVIÇO") . "'," . $RFP['conta_produto'] . ",'')\"><span>Pagar</span></button>" . 
        ($btnContent != "" ? "<div style=\"text-align:left;margin-left: 5px;\" class=\"btn-group\">
        <button style=\"height: 13px;background-color: gray;\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\">
        <span style=\"margin-top: 1px;\" class=\"caret\"></span></button><ul style=\"left:-130px;\" class=\"dropdown-menu\">" . $btnContent . "</ul></div>" : "") . "</div></center>";
        $records['aaData'][] = $row;
    }
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}

if (isset($_GET['getAgendamentoCancelDcc'])) {
    $idPlano = $_GET['getAgendamentoCancelDcc'];
    if (isset($_GET['getIdMensalidade'])) {
        $res = odbc_exec($con, "select id_plano_mens from sf_vendas_planos_mensalidade where id_mens = " . $_GET['getIdMensalidade']) or die(odbc_errormsg());
        $idPlano = odbc_result($res, 1);
    }
    $cur = odbc_exec($con, "select id_agendamento, dt_cancel_agendamento, finito_agendamento from dbo.sf_vendas_planos_dcc_agendamento where id_plano_agendamento =  " . $idPlano);
    while ($RFP = odbc_fetch_array($cur)) {
        $date = '';
        $id_cancel = '';
        if (!is_null($RFP['dt_cancel_agendamento'])) {
            $date = escreverData($RFP['dt_cancel_agendamento']);
            $id_agendamento = $RFP['id_agendamento'];
            $finito = $RFP['finito_agendamento'];
        }
        $local[] = array('id_agendamento' => $id_agendamento, 'dt_cancel_agendamento' => $date, 'finito' => $finito);
    }
    echo json_encode($local);
}

if (isset($_POST['addAgendamentoCancelDcc'])) {
    odbc_exec($con, "delete from sf_vendas_planos_dcc_agendamento where id_plano_agendamento = " . $_POST['addAgendamentoCancelDcc']) or die(odbc_errormsg());
    $query = "set dateformat dmy;
    BEGIN TRANSACTION
        insert into sf_vendas_planos_dcc_agendamento (id_plano_agendamento, dt_cancel_agendamento, dt_cadastro_agendamento, id_user_agendamento) values
        (" . $_POST['addAgendamentoCancelDcc'] . ", " . valoresData('txtDtCancelDcc') . ", getdate()," . $_SESSION["id_usuario"] . " );
        insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
        ('sf_vendas_planos_dcc_agend', " . $_POST['addAgendamentoCancelDcc'] . ", '" . $_SESSION["login_usuario"] . "', 'I', 'INCLUSAO DE AGENDAMENTO DE CANCELAMENTO', GETDATE(),
        (select favorecido from sf_vendas_planos where id_plano = " . $_POST['addAgendamentoCancelDcc'] . "));
    IF @@ERROR = 0
    COMMIT
    ELSE
    ROLLBACK;";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}

if (isset($_POST['excluirAgendamentoDcc'])) {
    $query = "set dateformat dmy;
    BEGIN TRANSACTION
        delete from sf_vendas_planos_dcc_agendamento where id_plano_agendamento = " . $_POST['excluirAgendamentoDcc'] . "
        insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
        ('sf_vendas_planos_dcc_agend', " . $_POST['excluirAgendamentoDcc'] . ", '" . $_SESSION["login_usuario"] . "', 'R', 'EXCLUSAO CANCEL DCC', GETDATE(),
        (select favorecido from sf_vendas_planos where id_plano = " . $_POST['excluirAgendamentoDcc'] . "));
    IF @@ERROR = 0
    COMMIT
    ELSE
    ROLLBACK;";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}

if (isset($_GET['getEmailBoleto'])) {
    $local = array(); 
    $cur = odbc_exec($con, "select id_email, descricao from sf_emails where mensagem like '%||M_bol_link||%' and tipo = 0");
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('value' => $RFP['id_email'], 'text' => utf8_encode($RFP['descricao']));
    }
    echo json_encode($local);
}

if (isset($_GET['listPagamentosBoleto'])) {
    $cont = 0;    
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );       
    $cur = odbc_exec($con, "set dateformat dmy;
    select id_boleto, bol_data_parcela, 
    isnull(bol_valor, (select sum(valor_bruto) total from sf_boleto_online_itens bi where bi.id_boleto = sf_boleto_online.id_boleto)) bol_valor,
    bol_link, galaxy_id, id_venda, datediff(day, bol_data_parcela, getdate()) dias, inativo 
    from sf_boleto_online where id_pessoa = " . valoresNumericos2($_GET['favorecido']) . 
    " and bol_data_parcela between " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']) . " order by id_boleto desc");
    while ($RFP = odbc_fetch_array($cur)) {
        $cont = $cont + 1;
        $row = array();
        $row[] = escreverData($RFP["bol_data_parcela"]);
        $row[] = escreverNumero($RFP["bol_valor"], 1);
        
        $row[] = (is_numeric($RFP["id_venda"]) ? "<span class=\"label label-Ativo\" style=\"font-size: 11px; line-height: 13px;\">Pago</span>" :         
        ($RFP["inativo"] == 1 ? "<span class=\"label label-Inativo\" style=\"font-size: 11px; line-height: 13px;\">Cancelado</span>" :                
        ($RFP["dias"] > 0 ? "<span class=\"label label-Suspenso\" style=\"font-size: 11px; line-height: 13px;\">Vencido</span>" :
        (strlen($RFP["galaxy_id"]) > 0 ? "<span class=\"label label-Cancelado\" style=\"font-size: 11px; line-height: 13px;\">Em Aberto</span>" :
        "<span class=\"label label-Novo\" style=\"font-size: 11px; line-height: 13px;\">Aguardando</span>"))));
        
        $row[] = "<div style=\"text-align:left;margin-left: 5px;\" class=\"btn-group\">
        <button style=\"height: 13px;background-color: gray;\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\"><span style=\"margin-top: 1px;\" class=\"caret\"></span></button>
        <ul style=\"left:-130px;\" class=\"dropdown-menu\">" .
        (is_numeric($RFP["id_venda"]) ? "" : ("<li><a onClick=\"EnviarBoleto('', " . $RFP['id_boleto'] . ")\" class=\"Enviar\">Enviar Cobrança</a></li>")) .                
        "<li><a onclick=\"abrirTelaBox('ClientesBoletosForm.php?id=" . $RFP["id_boleto"] . "', 500, 770);\" class=\"Gerar\">Itens do Boleto</a></li>" .                        
        (is_numeric($RFP["id_venda"]) ? "" : (strlen($RFP["galaxy_id"]) > 0 ? "<li><a href=\"" . $RFP["bol_link"] . "\" target=\"_blank\" class=\"Gerar\">Abrir Boleto</a></li>" .
        ($RFP["inativo"] == 0 ? "<li><a onclick=\"cancelarBoletoOnline(" . $RFP["id_boleto"] . ");\" class=\"Excluir\">Cancelar Boleto</a></li>" : "") :
        "<li><a onclick=\"processarBoletoOnline(" . $RFP["id_boleto"] . ");\" class=\"Gerar\">Gerar Boleto</a></li>")) .
        (!is_numeric($RFP["id_venda"]) && (!strlen($RFP["galaxy_id"]) > 0 || $RFP["inativo"] == 1) ? 
        "<li><a onclick=\"excluirBoletoOnline(" . $RFP["id_boleto"] . ");\" class=\"Excluir\">Excluir</a></li>" : "") . "</ul></div>";
        
        $records['aaData'][] = $row;
    }
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}

if (isset($_GET['listPagamentosLink'])) {
    $cont = 0;    
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );           
    $cur = odbc_exec($con, "set dateformat dmy;
    select id_link, data_parcela, id_venda, datediff(day, data_parcela, getdate()) dias, inativo, max_parcela,
    isnull(valor, (select sum(valor_bruto) total from sf_link_online_itens bi where bi.id_link = sf_link_online.id_link)) bol_valor,
    (select dt_estorno from sf_vendas v where v.id_venda = sf_link_online.id_venda) dt_estorno
    from sf_link_online where " . (isset($_GET['agendamento']) ? "id_link in (select id_link from sf_link_online_itens where id_agendamento = " . valoresNumericos2($_GET['agendamento']) . ")" : "id_pessoa = " . valoresNumericos2($_GET['favorecido'])) . 
    " and data_parcela between " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']) . " order by id_link desc");
    while ($RFP = odbc_fetch_array($cur)) {        
        $cont = $cont + 1;
        $row = array();
        $link = encrypt((escreverDataHora($RFP["data_parcela"]) . "|" . $RFP["id_link"] . "|" . $contrato . "|" . escreverNumero($RFP["bol_valor"])), "VipService123", true);
        $row[] = escreverData($RFP["data_parcela"]);
        $row[] = escreverNumero($RFP["bol_valor"], 1);                
        $row[] = $RFP["max_parcela"];        
        $row[] = (strlen($RFP["dt_estorno"]) > 0 ? "<span class=\"label label-Inativo\" style=\"font-size: 11px; line-height: 13px;\">Estornado</span>" :
        (is_numeric($RFP["id_venda"]) ? "<span class=\"label label-Ativo\" style=\"font-size: 11px; line-height: 13px;\">Pago</span>" :         
        ($RFP["inativo"] == 1 ? "<span class=\"label label-Inativo\" style=\"font-size: 11px; line-height: 13px;\">Cancelado</span>" :                   
        ($RFP["dias"] > 0 ? "<span class=\"label label-Suspenso\" style=\"font-size: 11px; line-height: 13px;\">Vencido</span>" :                
        "<span class=\"label label-Novo\" style=\"font-size: 11px; line-height: 13px;\">Aguardando</span>"))));        
        $row[] = "<div style=\"text-align:left;margin-left: 5px;\" class=\"btn-group\">
        <button style=\"height: 13px;background-color: gray;\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\"><span style=\"margin-top: 1px;\" class=\"caret\"></span></button>
        <ul style=\"left:-130px;\" class=\"dropdown-menu\">" .
        //(is_numeric($RFP["id_venda"]) ? "" : ("<li><a onClick=\"EnviarLink('', " . $RFP['id_link'] . ")\" class=\"Enviar\">Enviar Link</a></li>")) .                
        "<li><a onclick=\"" . (isset($_GET['agendamento']) ? "bootbox.alert('SERVIÇO DE AGENDAMENTO');" : "abrirTelaBox('ClientesBoletosForm.php?id=" . $RFP["id_link"] . "&tp=L', 500, 770);") . "\" class=\"Gerar\">Itens do Link</a></li>" .                        
        (is_numeric($RFP["id_venda"]) ? "" : ("<li><a href=\"../../Modulos/Financeiro/frmLinkPagamento.php?link=" . $link . "\" target=\"_blank\" class=\"Gerar\">Abrir Link</a></li>")) .
        (is_numeric($RFP["id_venda"]) || isset($_GET['agendamento']) ? "" : ("<li><a data-id=\"" . $RFP["id_link"] . "\" id=\"btnAltLink\" class=\"Alterar\">Editar Parcelas</a></li>")) .
        (is_numeric($RFP["id_venda"]) ? "" : ($RFP["inativo"] == 0 ? "<li><a onclick=\"cancelarLinkOnline(" . $RFP["id_link"] . ");\" class=\"Excluir\">Cancelar Link</a></li>" : 
        "<li><a onclick=\"excluirLinkOnline(" . $RFP["id_link"] . ");\" class=\"Excluir\">Excluir</a></li>")) . "</ul></div>";        
        $records['aaData'][] = $row;
    }
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}

if (isset($_GET['getDiaPadrao']) && is_numeric($_GET['idDia'])) {
    $local = array(); 
    $cur = odbc_exec($con, "select distinct dia, minimo, pro_rata from sf_configuracao_vencimentos where " . $_GET['idDia'] . " between de and ate order by 1");
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('value' => str_pad($RFP['dia'], 2, "0", STR_PAD_LEFT), 'text' => "Dia " . utf8_encode($RFP['dia']), 'minimo' => utf8_encode($RFP['minimo']), 'pro_rata' => utf8_encode($RFP['pro_rata']));
    }
    echo json_encode($local);
}

if (isset($_POST['addMensalidade'])) {
    $dtIni = str_replace('_', '/', $_POST['txtDtIni']);
    if (!is_numeric($_POST['txtIdPlano'])) {
        $result = odbc_exec($con, "set dateformat dmy; insert into sf_vendas_planos (id_turno, dt_inicio, dt_fim, favorecido, dias_ferias, dt_cadastro,usuario_resp, id_prod_plano) values
        (" . valoresSelect('txtHorarios') . ",'" . $dtIni . "','" . $dtIni . "'," . valoresNumericos('txtId') . "," .
        "(select isnull(max(dias),0) from sf_ferias where meses in (select parcela from sf_produtos_parcelas where id_parcela = " . valoresSelect('txtIdParc') . "))," .
        "getdate()," . $_SESSION["id_usuario"] . "," . valoresSelect('txtProduto') . "); SELECT SCOPE_IDENTITY() ID;") or die(odbc_errormsg());
        odbc_next_result($result);
        $_POST['txtIdPlano'] = odbc_result($result, 1);        
    }    
    if (is_numeric($_POST['txtIdParc']) && is_numeric($_POST['txtDiaPadrao'])) {
        $query = "set dateformat dmy;
        EXEC dbo.SP_MK_MENS_DIA_COND @dt_ini = '" . $dtIni . "', @id_plano = " . $_POST['txtIdPlano'] . ",
        @id_parcela = " . $_POST['txtIdParc'] . ", @dia_padrao = " . $_POST['txtDiaPadrao'] . ",
        @minimo = " . $_POST['txtAjustarPeriodo'] . ", @pro_rata = " . $_POST['txtAjustarValor'] . ", @id_mens = null;";
        $cur = odbc_exec($con, $query);
        $result = odbc_result($cur, 1);        
        echo $_POST['txtIdPlano']; exit();              
    }
}

if (isset($_POST['atualizaMaxLink'])) {
    $query = "update sf_link_online set max_parcela = " . valoresNumericos('txtValorMens') . " where id_link = " . valoresNumericos('atualizaMaxLink') . ";";    
    $query .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) 
    values ('sf_link_online', " . $_POST['atualizaMaxLink'] . ", '" . $_SESSION["login_usuario"] . "', 'A', 'ALTERACAO MAXIMO PARCELAS', GETDATE(), 
    (select isnull((select id_pessoa from sf_link_online where id_link = " . $_POST['atualizaMaxLink'] . "),0)));";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}