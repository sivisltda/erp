<?php

include "../../../Connections/configini.php";
require_once('../../../util/util.php');

if (isset($_POST['bntSave'])) {
    if (is_numeric($_POST['txtIdComissao'])) {
        odbc_exec($con, "update sf_comissao_cliente set " .
        "id_cliente = " . valoresSelect('txtId') . "," .
        "id_usuario = " . valoresSelect('txtTipoResponsavel') . "," .
        "tipo = " . valoresSelect('txtTipoComissao') . "," .
        "tipo_valor = " . valoresCheck('txtSinal') . "," .                                        
        "tipo_valor_desc = " . valoresCheck('txtSinalDesc') . "," .                                        
        "valor = " . valoresNumericos('txtValorComissao') . " " .
        "where id = " . $_POST['txtIdComissao']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_comissao_cliente (id_cliente, id_usuario, tipo, valor, tipo_valor, tipo_valor_desc) values (" . 
        valoresSelect('txtId') . "," . valoresSelect('txtTipoResponsavel') . "," . valoresSelect('txtTipoComissao') . "," . 
        valoresNumericos('txtValorComissao') . "," . valoresCheck('txtSinal') . "," . valoresCheck('txtSinalDesc') . ")");
    }
    echo "YES"; exit();
}

if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtIdComissao'])) {
        odbc_exec($con, "DELETE FROM sf_comissao_cliente WHERE id = " . $_POST['txtIdComissao']);
        echo "YES"; exit();
    }
}

if (isset($_POST['bntLinhaPadrao'])) {   
    if (is_numeric($_POST['txtId']) && is_numeric($_POST['txtIdComissao'])) {
        odbc_exec($con, "EXEC dbo.SP_MK_COMISSAO_REGRAS @id_cliente = " . $_POST['txtId'] . ", @id_regra = " . $_POST['txtIdComissao'] . ";");
        echo "YES"; exit();                
    }
}

if (isset($_POST['btnListar'])) {
    $toReturn = [];
    if (is_numeric($_POST['idUsuario'])) {
        $cur = odbc_exec($con, "select id_fornecedores_despesas from sf_fornecedores_despesas 
        where tipo = 'C' and id_user_resp = " . $_POST['idUsuario']);
        while ($RFP = odbc_fetch_array($cur)) {
            $toReturn[] = $RFP['id_fornecedores_despesas'];
        }
    }
    echo json_encode($toReturn);
}

odbc_close($con);
