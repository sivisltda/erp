<?php

include "../../../Connections/configini.php";
require_once('../../../util/util.php');

if (isset($_POST['AddCredencial'])) {
    odbc_exec($con, "update sf_fornecedores_despesas set gyp_idaluno = " . valoresTexto('txtIdCod') . " where id_fornecedores_despesas = " . $_POST['txtId']);    
    odbc_exec($con, "set dateformat dmy; insert into sf_fornecedores_despesas_credenciais(id_fornecedores_despesas,id_credencial, dt_inicio, dt_fim, motivo, usuario_resp, dt_cadastro, gyp_credenciais, id_item_venda)values
    (" . $_POST['txtId'] . "," . $_POST['txtCredModelo'] . "," . valoresData('txtCredDtIni') . "," .
    valoresData('txtCredDtFim') . "," . valoresTexto('txtCredMotivo') . "," . $_SESSION["id_usuario"] . ", GETDATE(), " . valoresTexto('txtIdToken') . ", " . valoresSelect('txtCredPagamento') . ") ") or die(odbc_errormsg());    
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    values ('sf_fornecedores_despesas_credenciais', " . $_POST['txtCredModelo'] . ", '" . $_SESSION["login_usuario"] . "', 'C', 'INCLUIR - CREDENCIAL', GETDATE(), " . $_POST['txtId'] . ")");
}

if (isset($_POST['DelCredencial'])) {
    odbc_exec($con, "UPDATE sf_fornecedores_despesas_credenciais SET dt_cancelamento = GETDATE(), usuario_canc = " . $_SESSION["id_usuario"] . " where id_fornecedores_despesas_credencial = " . $_POST['DelCredencial']) or die(odbc_errormsg());
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    values ('sf_fornecedores_despesas_credenciais', " . $_POST['DelCredencial'] . ", '" . $_SESSION["login_usuario"] . "', 'C', 'EXCLUIR - CREDENCIAL', GETDATE(), (select id_fornecedores_despesas from sf_fornecedores_despesas_credenciais where id_fornecedores_despesas_credencial = " . $_POST['DelCredencial'] . "))");    
    echo "YES";
}

if (isset($_GET['getCredPagamento'])) {
    $local = array(); 
    if (strlen($_GET['idTitular']) > 0) {
        $usado = 0;
        $limite = 0;    
        $responsavel = 1;        
        $cur = odbc_exec($con, "select responsavel from sf_fornecedores_despesas_dependentes
        where id_dependente = " . $_GET['txtId'] . " and id_titular = " . $_GET['idTitular']);
        while ($RFP = odbc_fetch_array($cur)) {
            $responsavel = $RFP['responsavel'];  
        }        
        if ($responsavel == 1) {
            $cur = odbc_exec($con, "select count(id_credencial) usado, (select clb_limite_credencial from sf_configuracao where ID = 1) limite
            from sf_fornecedores_despesas_credenciais c inner join sf_fornecedores_despesas_dependentes d on c.id_fornecedores_despesas = d.id_dependente
            where d.id_titular = " . $_GET['idTitular'] . " and id_item_venda is null and dt_cancelamento is null and d.responsavel = 1
            and CAST(GETDATE() as date) between dt_inicio and dt_fim");
            while ($RFP = odbc_fetch_array($cur)) {
                $usado = $RFP['usado'];    
                $limite = $RFP['limite'];   
            }
            if ($limite > 0) {
                $local[] = array('value' => 'null', 'usado' => $usado, 'limite' => $limite, 'texto' => 'Credenciais Gratuitas ' . $usado . "/" . $limite);        
            } 
        } else {
            $local[] = array('value' => 'null', 'usado' => $usado, 'limite' => 1, 'texto' => 'Credenciais Dependentes');
        }
        $cur = odbc_exec($con, "select id_item_venda, data_venda, descricao, vendedor_comissao, quantidade 
        from sf_vendas_itens vi inner join sf_vendas v on v.id_venda = vi.id_venda inner join sf_produtos p on p.conta_produto = vi.produto
        where p.ativar_convite = 1 and v.dt_estorno is null and cliente_venda = " . $_GET['txtId'] . " and id_item_venda not in (select id_item_venda from sf_fornecedores_despesas_credenciais where id_item_venda is not null and dt_cancelamento is null)") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $local[] = array('value' => utf8_encode($RFP['id_item_venda']), 'usado' => $usado, 'limite' => $limite, 'texto' => escreverDataHora($RFP['data_venda']) . " Qtd. " . escreverNumero($RFP['quantidade'], 0, 0));        
        }
    }
    echo json_encode($local); exit;
}

if (isset($_GET['listCredencial'])) {
    $cur = odbc_exec($con, "select A.id_fornecedores_despesas_credencial id, A.id_credencial, B.descricao, 
    dt_inicio, dt_fim, motivo, C.nome, dt_cancelamento, id_item_venda 
    from sf_fornecedores_despesas_credenciais A
    inner join sf_credenciais B on A.id_credencial = B.id_credencial
    inner join dbo.sf_usuarios C on A.usuario_resp = C.id_usuario
    where A.id_fornecedores_despesas = " . $_GET['listCredencial'] . " order by dt_inicio desc");
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    while ($RFP = odbc_fetch_array($cur)) {
        $fontColor = "";
        if ($RFP["dt_cancelamento"]) {
            $fontColor = "style=\"color: red;\"";
        }
        $cont = $cont + 1;
        $row = array();
        $row[] = "<div title='" . escreverData($RFP['dt_inicio']) . "' " . $fontColor . ">" . escreverData($RFP['dt_inicio']) . "</div>";
        $row[] = "<div title='" . escreverData($RFP['dt_fim']) . "' " . $fontColor . ">" . escreverData($RFP['dt_fim']) . "</div>";
        $row[] = "<div title='" . utf8_encode($RFP['descricao']) . "' " . $fontColor . ">" . utf8_encode($RFP['descricao']) . (is_numeric($RFP['id_item_venda']) ? " - [PAGO]" : "") . "</div>";
        $row[] = "<div title='" . utf8_encode($RFP['motivo']) . "' " . $fontColor . ">" . utf8_encode($RFP['motivo']) . "</div>";
        $row[] = "<div title='" . utf8_encode($RFP['nome']) . "' " . $fontColor . ">" . utf8_encode($RFP['nome']) . "</div>";
        if ($RFP["dt_cancelamento"]) {
            $row[] = "";
        } else if ($ckb_adm_cli_read_ == 0 && $ckb_aca_add_credencial_ > 0) {
            $row[] = "<center><input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\" onClick=\"RemoverCredencial(" . $RFP['id'] . ")\")></center>";
        } else {
            $row[] = "";
        }
        $records['aaData'][] = $row;
    }
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}