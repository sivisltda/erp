<?php

include "../../../Connections/configini.php";
require_once('../../../util/util.php');

if (isset($_POST['SalvarConvenio'])) {
    if (is_numeric($_POST['txtId']) && is_numeric($_POST['txtIdConv'])) {
        odbc_exec($con, "set dateformat dmy;") or die(odbc_errormsg());
        $query = "insert into sf_fornecedores_despesas_convenios values (" . $_POST['txtId'] . "," . $_POST['txtIdConv'] . "," . valoresData("dti") . "," . valoresData("dtf") . "," . valoresSelect("txtLider") . ")";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $cur = odbc_exec($con, "select dt_inicio, dt_fim, id_convenio, descricao from sf_fornecedores_despesas_convenios fc inner join sf_convenios c on fc.convenio = c.id_convenio where id_fornecedor = " . $_POST['txtId'] . " and convenio = " . $_POST['txtIdConv'] . "");
        while ($RFP = odbc_fetch_array($cur)) {
            odbc_exec($con, "set dateformat dmy;insert into sf_logs values('sf_fornecedores_despesas_convenios'," . $RFP['id_convenio'] . ", '" . $_SESSION["login_usuario"] . "','I',
           'INCLUSAO CONVENIO " . $RFP['id_convenio'] . " - " . $RFP['descricao'] . " - " . escreverData($RFP['dt_inicio']) . " - " . escreverData($RFP['dt_fim']) . "', getdate(), " . $_POST['txtId'] . ")");
        }
        echo "YES";
    }
}

if (isset($_POST['SalvarConvenioAdd'])) {
    if (is_numeric($_POST['txtIdPlano']) && valoresNumericos('txtValConv') > 0) {
        $convenio = 0;
        $cliente = 0;
        $plano = 0;        
        $placa = "";
        
        $cur = odbc_exec($con, "select favorecido, id_prod_plano, placa
        from sf_vendas_planos inner join sf_fornecedores_despesas_veiculo on id_veiculo = id
        where id_plano = " . $_POST['txtIdPlano']);
        while ($RFP = odbc_fetch_array($cur)) {
            $cliente = $RFP['favorecido'];    
            $plano = $RFP['id_prod_plano'];
            $placa = $RFP['placa'];        
        }                        
        
        if ($cliente > 0 && $plano > 0) {
            odbc_exec($con, "insert into sf_convenios (descricao, status, tipo, convenio_especifico, ativo, ate_vencimento, id_conv_plano) 
            values ('" . $placa . "', 0, 1, 1, 0, " . valoresNumericos('txtAteVencimento') . "," . valoresNumericos('txtIdPlano') . ");") or die(odbc_errormsg());            
            $res = odbc_exec($con, "select top 1 id_convenio from sf_convenios order by id_convenio desc") or die(odbc_errormsg());
            $convenio = odbc_result($res, 1);            
            $query = "set dateformat dmy; insert into sf_convenios_planos (id_convenio_planos, id_prod_convenio) 
            values (" . $convenio . ", " . $plano . ");";
            $query .= "insert into sf_convenios_regras (convenio, valor, tp_valor, numero_minimo) 
            values (" . $convenio . ", " . valoresNumericos('txtValConv') . ", " . valoresTexto("txtSinal") . ", 0);";
            $query .= "insert into sf_fornecedores_despesas_convenios (id_fornecedor, convenio, dt_inicio, dt_fim) 
            values (" . $cliente . ", " . $convenio . ", " . valoresData("dti") . ", " . valoresData("dtf") . ");";
            odbc_exec($con, $query) or die(odbc_errormsg());
            $cur = odbc_exec($con, "select dt_inicio, dt_fim, id_convenio, descricao from sf_fornecedores_despesas_convenios fc 
            inner join sf_convenios c on fc.convenio = c.id_convenio where id_fornecedor = " . $cliente . " and convenio = " . $convenio . "");
            while ($RFP = odbc_fetch_array($cur)) {
                odbc_exec($con, "set dateformat dmy;insert into sf_logs values('sf_fornecedores_despesas_convenios', " . $RFP['id_convenio'] . ", '" . $_SESSION["login_usuario"] . "', 'I',
               'INCLUSAO CONVENIO " . $RFP['id_convenio'] . " - " . $RFP['descricao'] . " - " . escreverData($RFP['dt_inicio']) . " - " . escreverData($RFP['dt_fim']) . "', getdate(), " . $cliente . ")");
            }
            echo "YES";
        }
    }
}

if (isset($_POST['AtualizaConvenio'])) {
    if (is_numeric($_POST['txtId']) && is_numeric($_POST['txtIdConv'])) {
        odbc_exec($con, "set dateformat dmy;") or die(odbc_errormsg());
        $query = "update sf_fornecedores_despesas_convenios set id_lider = " . valoresSelect("txtLider") . "
        where convenio = " . $_POST['txtIdConv'] . " and id_lider 
        in (select cin.id_lider from sf_fornecedores_despesas_convenios cin
        where cin.id_fornecedor = " . $_POST['txtId'] . " and cin.convenio = convenio)";
        odbc_exec($con, $query) or die(odbc_errormsg());
        echo "YES";
    }
}

if (isset($_POST['btnExcluirConvenio'])) {
    if (is_numeric($_POST['txtId']) && is_numeric($_POST['txtIdConv'])) {
        $cur = odbc_exec($con, "select dt_inicio, dt_fim, id_convenio, descricao from sf_fornecedores_despesas_convenios fc inner join sf_convenios c on fc.convenio = c.id_convenio where id_fornecedor = " . $_POST['txtId'] . " and convenio = " . $_POST['txtIdConv'] . "");
        while ($RFP = odbc_fetch_array($cur)) {
            odbc_exec($con, "set dateformat dmy;insert into sf_logs values('sf_fornecedores_despesas_convenios'," . $RFP['id_convenio'] . ", '" . $_SESSION["login_usuario"] . "','R',
           'EXCLUSAO CONVENIO " . $RFP['id_convenio'] . " - " . $RFP['descricao'] . " - " . escreverData($RFP['dt_inicio']) . " - " . escreverData($RFP['dt_fim']) . "', getdate(), " . $_POST['txtId'] . ")");
        }
        odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_convenios WHERE id_fornecedor = " . $_POST['txtId'] . " and convenio = " . $_POST['txtIdConv']);
        echo "YES";
    }
}

if (isset($_GET['listConvenios'])) {
    $cur = odbc_exec($con, "select * from sf_fornecedores_despesas_convenios                            
    inner join sf_convenios on id_convenio = sf_fornecedores_despesas_convenios.convenio
    inner join sf_convenios_regras on sf_convenios_regras.convenio = sf_fornecedores_despesas_convenios.convenio where tipo = 1 
    and id_fornecedor = " . $_GET['listConvenios'] . (is_numeric($_GET['txtIdPlano']) ? " and id_conv_plano = " . $_GET['txtIdPlano'] : ""));
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    while ($RFP = odbc_fetch_array($cur)) {
        $cont = $cont + 1;
        $row = array();
        $row[] = utf8_encode($RFP["descricao"]) . (!is_numeric($RFP["id_conv_plano"]) ? "<span style=\"color: red;\"> [Todos]</span>" : "");
        $row[] = escreverNumero($RFP['valor']) . " (" . ($RFP['tp_valor'] == "P" ? "%" : "$"). ")";
        if ($RFP["dt_inicio"] != "") {
            $row[] = escreverData($RFP['dt_inicio']);
            $row[] = escreverData($RFP['dt_fim']);
        } else {
            $row[] = "";
            $row[] = "";
        }
        $row[] = "<center>" . ($RFP['ate_vencimento'] == "1" ? "SIM" : "NÃO") . "</center>";        
        if ($ckb_aca_add_convenio_ > 0 && $ckb_adm_cli_read_ == 0) {
            $row[] = "<center><input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\" onClick=\"RemoverConvenio(" . $RFP['convenio'] . ")\")></center>";
        } else {
            $row[] = '';
        }
        $records['aaData'][] = $row;
    }
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}

if (isset($_GET['listIntegrantes'])) {
    if (is_numeric($_GET['id'])) {
        if ($_GET['search']) {
            $filtro = " and razao_social like '%" . $_GET['search'] . "%'";
        }
        $query = "select * from sf_fornecedores_despesas_convenios dc inner join sf_fornecedores_despesas on id_fornecedores_despesas = id_fornecedor 
        where dc.convenio = " . $_GET['listIntegrantes'] . " and id_lider in (select cin.id_lider from sf_fornecedores_despesas_convenios cin
        where cin.id_fornecedor = " . $_GET['id'] . " and cin.convenio = dc.convenio) " . $filtro . " order by razao_social";
        $cur = odbc_exec($con, $query);
        while ($RFP = odbc_fetch_array($cur)) {
            $row = array();
            $row["id"] = utf8_encode($RFP["id_fornecedores_despesas"]);
            $row["razao_social"] = utf8_encode($RFP["razao_social"]);
            $row["id_lider"] = utf8_encode($RFP["id_lider"]);
            $records[] = $row;
        }
        echo json_encode($records);
    }
}

if (isset($_GET['listLider'])) {
    if (is_numeric($_GET['id'])) {
        if ($_GET['search']) {
            $filtro = " and razao_social like '%" . $_GET['search'] . "%'";
        }
        $query = "select distinct id_fornecedores_despesas,razao_social,id_lider from sf_fornecedores_despesas_convenios dc 
        inner join sf_fornecedores_despesas on id_fornecedores_despesas = id_lider 
        where dc.convenio = " . $_GET['listLider'] . $filtro . "
        union select id_fornecedores_despesas,razao_social,id_fornecedores_despesas id_lider from sf_fornecedores_despesas
        where id_fornecedores_despesas = " . $_GET['id'] . $filtro . " order by razao_social";
        $cur = odbc_exec($con, $query);
        while ($RFP = odbc_fetch_array($cur)) {
            $row = array();
            $row["id"] = utf8_encode($RFP["id_fornecedores_despesas"]);
            $row["razao_social"] = utf8_encode($RFP["razao_social"]);
            $row["id_lider"] = utf8_encode($RFP["id_lider"]);
            $records[] = $row;
        }
        echo json_encode($records);
    }
}