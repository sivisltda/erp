<?php

$imprimir = 0;
$disabled = 'disabled';
$id = '';
$descricao = '';
$pesJur = '0';
$acessorios = '0';
$inativo = '0';
$cod = '';

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = 1;
}

if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_contas_movimento set 
        descricao = UPPER(" . valoresTexto('txtDescricao') . "),
        pes_jur = " . valoresCheck('txtPesJur') . ",            
        inativa = " . valoresCheck('txtInativo') . ", 
        grupo_observacao = " . valoresTexto('txtCod') . ",
        acessorios = " . valoresCheck('txtAcessorios') . " where id_contas_movimento = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_contas_movimento(descricao,tipo,pes_jur, inativa,acessorios, grupo_observacao)values(" . 
        "UPPER(" . valoresTexto('txtDescricao') . "),'L'," . valoresCheck('txtPesJur') . "," . 
        valoresCheck('txtInativo') . "," . valoresCheck('txtAcessorios') . "," . valoresTexto('txtCod') . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_contas_movimento from sf_contas_movimento order by id_contas_movimento desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}

if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        if (isset($_POST['List'])) {
            include "../../../Connections/configini.php";
        }
        odbc_exec($con, "DELETE FROM sf_contas_movimento WHERE id_contas_movimento = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox(1);</script>";
        exit();
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_contas_movimento where id_contas_movimento =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_contas_movimento'];
        $descricao = utf8_encode($RFP['descricao']);
        $pesJur = $RFP['pes_jur'];
        $acessorios = $RFP['acessorios'];
        $inativo = $RFP['inativa'];
        $cod = utf8_encode($RFP['grupo_observacao']);
    }
} else {
    $disabled = '';
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}