<?php

include "../../../Connections/configini.php";
require_once('../../../util/util.php');

function verificar_bandeira($cc_num) {
    if (preg_match("/^3[47]/", $cc_num) && strlen($cc_num) == 15) {
        return "-59"; //"amex";
    } elseif (preg_match("/^30[0-5]/", $cc_num) && strlen($cc_num) == 14) {
        return "-87"; //"diners_club_carte_blanche";
    } elseif (preg_match("/^36/", $cc_num) && strlen($cc_num) == 14) {
        return "-87"; //"diners_club_international";
    } elseif (preg_match("/^35(2[89]|[3-8][0-9])/", $cc_num) && strlen($cc_num) == 16) {
        return "-114"; //"jcb";
    } elseif (preg_match("/^(6304|670[69]|6771)/", $cc_num) && strlen($cc_num) >= 16 && strlen($cc_num) <= 19) {
        return "-114"; //"laser";
    } elseif (preg_match("/^(4026|417500|4508|4844|491(3|7))/", $cc_num) && strlen($cc_num) == 16) {
        return "-186"; //"visa_electron";
    } elseif (preg_match("/^4/", $cc_num) && strlen($cc_num) == 16) {
        return "-150"; //"visa";
    } elseif (preg_match("/^5[1-5]/", $cc_num) && strlen($cc_num) == 16) {
        return "-223"; //"mastercard";
    } elseif (preg_match("/^(5018|5020|5038|6304|6759|676[1-3])/", $cc_num) && strlen($cc_num) >= 12 && strlen($cc_num) <= 19) {
        return "-259"; //"maestro";
    } elseif (preg_match("/^(5859)/", $cc_num) && strlen($cc_num) == 16) {
        return "-355"; //"banescard";
    } elseif (preg_match("/^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)/", $cc_num) && strlen($cc_num) == 16) {
        return "-295"; //"discover";
    } elseif (preg_match("/^(636368|438935|504175|451416|636297|5067|4576|4011)\d+$/", $cc_num) && strlen($cc_num) == 16) {
        return "-30"; //"elo";
    } elseif (preg_match("/^50[0-9]/", $cc_num) && strlen($cc_num) == 16) {
        return "-1"; //"aura";
    } elseif (preg_match("/^(38|60)\d{11}(?:\d{3})?(?:\d{3})?$/", $cc_num) && strlen($cc_num) == 16) {
        return "-324"; //"hiper";
    } else {
        return "-114";
    }
}

if (isset($_POST['AddCartao'])) {
    odbc_exec($con, "set dateformat dmy;") or die(odbc_errormsg());
    $legCartao = substr($_POST['txtDCCNumCartao'], 0, 4) . "..." . substr($_POST['txtDCCNumCartao'], -4, 4);
    $numCartao = base64_encode($_POST['txtDCCNumCartao']);
    odbc_exec($con, "insert into sf_fornecedores_despesas_cartao
                    (id_fornecedores_despesas,nome_cartao, num_cartao, legenda_cartao, dv_cartao, validade_cartao, credencial, id_bandeira)values
                    (" . valoresTexto('txtId') . ",UPPER(" . valoresTexto('txtDCCNmCartao') . "),'" . $numCartao . "','" . $legCartao . "'," . valoresTexto('txtDCCCodSeg') . ",'" .
                    str_replace("_", "/", $_POST['txtDCCValidade']) . "'," . valoresNumericos("txtTipo") . "," . valoresNumericos("txtDCCBandeira") . ") ") or die(odbc_errormsg());
    $res = odbc_exec($con, "select top 1 id_cartao from sf_fornecedores_despesas_cartao order by id_cartao desc") or die(odbc_errormsg());
    $id = odbc_result($res, 1);    
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    select 'sf_fornecedores_despesas_cartao', id_cartao, '" . $_SESSION["login_usuario"] . "', 'I',
    'INCLUSAO DE CARTAO ' + legenda_cartao + ' (' + replace(dv_cartao, ' ','') + ') - ' + nome_cartao, GETDATE(), id_fornecedores_despesas
    from sf_fornecedores_despesas_cartao where id_cartao = " . $id);      
    echo $id;
}

if (isset($_POST['DelCartao'])) {
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    select 'sf_fornecedores_despesas_cartao', id_cartao, '" . $_SESSION["login_usuario"] . "', 'R',
    'EXCLUSAO DE CARTAO ' + legenda_cartao + ' (' + replace(dv_cartao, ' ','') + ') - ' + nome_cartao, GETDATE(), id_fornecedores_despesas
    from sf_fornecedores_despesas_cartao where id_cartao = " . $_POST['DelCartao']);        
    odbc_exec($con, "Delete from sf_fornecedores_despesas_cartao where id_cartao = " . $_POST['DelCartao']) or die(odbc_errormsg());
    echo "YES";
}

if (isset($_POST['AltCartao']) && isset($_POST['credencial'])) {
    if (is_numeric($_POST['AltCartao']) && is_numeric($_POST['credencial'])) {
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        select 'sf_fornecedores_despesas_cartao', id_cartao, '" . $_SESSION["login_usuario"] . "', 'A',
        'ALTERACAO DE CARTAO ' + legenda_cartao + ' (' + replace(dv_cartao, ' ','') + ') - ' + nome_cartao, GETDATE(), id_fornecedores_despesas
        from sf_fornecedores_despesas_cartao where id_cartao = " . $_POST['AltCartao']);                
        odbc_exec($con, "UPDATE sf_fornecedores_despesas_cartao SET credencial = " . $_POST['credencial'] . " where id_cartao = " . $_POST['AltCartao']) or die(odbc_errormsg());
        echo "YES";
    }
}

if (isset($_GET['listDCC'])) {
    $cur = odbc_exec($con, "select * from sf_configuracao where id = 1");
    while ($RFP = odbc_fetch_array($cur)) {
        $dcc_descricao = $RFP['DCC_DESCRICAO'];
        if (is_numeric($RFP['DCC_CHAVE2'])) {
            $dcc_descricao2 = $RFP['DCC_DESCRICAO2'];
        }
    }
    $cur = odbc_exec($con, "select *, (select count(*) from sf_fornecedores_despesas_cartao where num_cartao = A.num_cartao) qtdCartao
    from sf_fornecedores_despesas_cartao  A where id_fornecedores_despesas = " . $_GET['listDCC']);
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    while ($RFP = odbc_fetch_array($cur)) {
        $cont = $cont + 1;
        $row = array();
        if ($RFP['qtdCartao'] > 1) {
            $row[] = "<center><img src=\"./../../img/details_open.png\" data-id='" . $RFP['id_cartao'] . "'></center>";
        } else {
            $row[] = "<center><img src=\"./../../img/details_disabled.png\" data-id='" . $RFP['id_cartao'] . "'></center>";
        }        
        $row[] = "<div title='" . utf8_encode($RFP['nome_cartao']) . "'>" . utf8_encode($RFP['nome_cartao']) . "</div>";
        $row[] = "<center><div style=\"float: left;margin-top: 6px;\" title='" . utf8_encode($RFP['legenda_cartao']) . "'><img src=\"../../img/cartao/" . $RFP['id_bandeira'] . ".png\" style=\"height: 25px; margin-top: -5px;\">" . utf8_encode($RFP['legenda_cartao']) . "</div></center>";
        $dv = str_pad("", strlen(str_replace(" ", "", $RFP['dv_cartao'])), "*", STR_PAD_LEFT);
        $row[] = "<center>" . $dv . "</center>";
        $row[] = "<center><div title='" . tableFormato($RFP['validade_cartao'], 'MA', '', '') . "'>" . tableFormato($RFP['validade_cartao'], 'MA', '', '') . "</div></center>";
        $row[] = "<center><select class=\"inputCenter\" id=\"txt_" . $RFP['id_cartao'] . "\" name=\"txt_" . $RFP['id_cartao'] . "\" disabled>
        <option value=\"0\" " . ($RFP['credencial'] == 0 ? "SELECTED" : "") . ">" . $dcc_descricao . "</option>" .
                (strlen($dcc_descricao2) > 0 ? "<option value=\"1\" " . ($RFP['credencial'] == 1 ? "SELECTED" : "") . ">" . $dcc_descricao2 . "</option>" : "") .
                "</select></center>";
        if($ckb_adm_cli_read_ == 0) {
            $row[] = "<center><a id=\"btn_edit_" . $RFP['id_cartao'] . "\" onclick=\"EditarCartao(" . $RFP['id_cartao'] . ");\" href=\"javascript:;\" class=\"btn green-haze\" title=\"Editar\" style=\"padding: 0px 4px;background:#0099cc;\"><span class=\"ico-pencil\"></span></a>
            <a id=\"btn_exlr_" . $RFP['id_cartao'] . "\" onclick=\"RemoverCartao(" . $RFP['id_cartao'] . ");\" href=\"javascript:;\" class=\"btn red-haze\" title=\"Excluir\" style=\"padding: 0px 4px;background:red;\"><span class=\"ico-cancel\"></span></a>
            <a id=\"btn_save_" . $RFP['id_cartao'] . "\" onclick=\"SalvarCartao(" . $RFP['id_cartao'] . ");\" href=\"javascript:;\" class=\"btn green-haze\" title=\"Salvar\" style=\"padding: 0px 4px;background:#5bb75b;display: none;\"><span class=\"ico-checkmark\"></span></a>
            <a id=\"btn_canc_" . $RFP['id_cartao'] . "\" onclick=\"CancelarCartao();\" href=\"javascript:;\" class=\"btn red-haze\" title=\"Cancelar\" style=\"padding: 0px 4px;background:#FFAA31;display: none;\"><span class=\"ico-reply\"></span></a></center>";
        } else {
            $row[] = "";    
        }
        $records['aaData'][] = $row;
    }
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}

if (isset($_GET['listDCCDetails'])) {
    $cur = odbc_exec($con, "select B.razao_social, A.* from sf_fornecedores_despesas_cartao A
    inner join sf_fornecedores_despesas B on A.id_fornecedores_despesas = B.id_fornecedores_despesas
    where num_cartao in( select num_cartao from sf_fornecedores_despesas_cartao where id_cartao = " . $_GET['listDCCDetails'] . ") and A.id_fornecedores_despesas <> " . $_GET['idList']);
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('id' => $RFP['id_fornecedores_despesas'], 'razao_social' => utf8_encode($RFP['razao_social']));
    }
    echo(json_encode($local));
}

odbc_close($con);
