<?php

include "../../../Connections/configini.php";
require_once('../../../util/util.php');

if (isset($_POST['AddBloqueio'])) {
    odbc_exec($con, "set dateformat dmy; insert into sf_fornecedores_despesas_bloqueios(id_fornecedores_despesas, dt_inicio, dt_fim, motivo, usuario_resp, dt_cadastro)values
    (" . $_POST['txtId'] . "," . valoresData('txtBlqDtIni') . "," . valoresData('txtBlqDtFim') . "," . valoresTexto('txtBlqMotivo') . "," . $_SESSION["id_usuario"] . ", GETDATE())") or die(odbc_errormsg());    
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    values ('sf_fornecedores_despesas_bloqueios', null, '" . $_SESSION["login_usuario"] . "', 'C', 'INCLUIR - BLOQUEIO', GETDATE(), " . $_POST['txtId'] . ")");
}

if (isset($_POST['DelBloqueio'])) {
    odbc_exec($con, "UPDATE sf_fornecedores_despesas_bloqueios SET dt_cancelamento = GETDATE(), usuario_canc = " . $_SESSION["id_usuario"] . " where id_fornecedores_despesas_bloqueio = " . $_POST['DelBloqueio']) or die(odbc_errormsg());
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    values ('sf_fornecedores_despesas_bloqueios', " . $_POST['DelBloqueio'] . ", '" . $_SESSION["login_usuario"] . "', 'C', 'EXCLUIR - BLOQUEIO', GETDATE(), (select id_fornecedores_despesas from sf_fornecedores_despesas_bloqueios where id_fornecedores_despesas_bloqueio = " . $_POST['DelBloqueio'] . "))");    
    echo "YES";
}

if (isset($_GET['listBloqueio'])) {
    $cur = odbc_exec($con, "select A.id_fornecedores_despesas_bloqueio id, dt_inicio, dt_fim, motivo, C.nome, dt_cancelamento 
    from sf_fornecedores_despesas_bloqueios A inner join dbo.sf_usuarios C on A.usuario_resp = C.id_usuario
    where A.id_fornecedores_despesas = " . $_GET['listBloqueio'] . " order by dt_cancelamento");
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    while ($RFP = odbc_fetch_array($cur)) {
        $fontColor = "";
        if ($RFP["dt_cancelamento"]) {
            $fontColor = "style=\"color: red;\"";
        }
        $cont = $cont + 1;
        $row = array();
        $row[] = "<div title='" . escreverData($RFP['dt_inicio']) . "' " . $fontColor . ">" . escreverData($RFP['dt_inicio']) . "</div>";
        $row[] = "<div title='" . escreverData($RFP['dt_fim']) . "' " . $fontColor . ">" . escreverData($RFP['dt_fim']) . "</div>";
        $row[] = "<div title='" . utf8_encode($RFP['motivo']) . "' " . $fontColor . ">" . utf8_encode($RFP['motivo']) . "</div>";
        $row[] = "<div title='" . utf8_encode($RFP['nome']) . "' " . $fontColor . ">" . utf8_encode($RFP['nome']) . "</div>";
        if ($RFP["dt_cancelamento"]) {
            $row[] = "";
        } else if ($ckb_aca_add_bloqueios_ > 0) {
            $row[] = "<center><input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\" onClick=\"RemoverBloqueio(" . $RFP['id'] . ")\")></center>";
        } else {
            $row[] = "";
        }
        $records['aaData'][] = $row;
    }
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}