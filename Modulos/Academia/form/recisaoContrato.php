<?php
require_once('../../../Connections/configini.php');
require_once('../../../util/util.php');

$sQuery1 = "select *, ISNULL(dt_cancelamento, dbo.FU_PLANO_FIM(id_plano)) dt_limite from sf_vendas_planos vp
left join sf_usuarios u on vp.usuario_canc = u.id_usuario 
where id_plano = " . $_GET['idplano'];
$cur = odbc_exec($con, $sQuery1);
while ($RFP = odbc_fetch_array($cur)) {
    $dtCanc = utf8_encode($RFP["dt_cancelamento"]);
    $dtLimite = escreverData($RFP["dt_limite"]);
    $assunto = utf8_encode($RFP["obs_cancelamento"]);
    $favorecido = utf8_encode($RFP["favorecido"]);
    $usurescind = utf8_encode($RFP["login_user"]);
}
?>
<link rel="icon" type="image/ico" href="../../../favicon.ico"/>
<link rel="stylesheet" type="text/css" href="../../../css/stylesheets.css"/>
<link rel="stylesheet" type="text/css" href="../../../css/main.css"/>
<link href="../../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
<body>
    <div class="row-fluid">
        <div class="frmhead">
            <div class="frmtext">Recisão de Contrato</div>
            <div class="frmicon" onClick="parent.FecharBox(1)">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont">
            <input name="txtId" id="txtId" value="<?php echo $favorecido; ?>" type="hidden"/>
            <input name="txtDoc" id="txtDoc" value="<?php echo $_GET['idplano']; ?>" type="hidden"/>
            <input name="txtTp" id="txtTp" value="1" type="hidden"/>
            <input type="hidden" id="txtidplano" value="<?php echo $_GET['idplano']; ?>">
            <div class="cnttRescCntt" style="display: flex;">
                <div style="width:42%; margin-left:1%">
                    <label><span style="color:red;">*</span>Dt. Limite Contrato:</label>
                    <input type="text" style="width: 70%;" id="txtDtLimite" name="txtDtLimite" value="<?php echo $dtLimite; ?>"<?php echo ($ckb_aca_cntt_dtCanc_limit == 1 || $dtCanc != '' ? "disabled" : "") ?> class="datepicker inputCenter"/>
                </div>
                <div style="width:56%; margin-left:1%; display: none;">
                    <label><span style="color:red;">*</span>Modelo Credencial:</label>
                    <select id="txtCredModelo" class="select" style="width:100%" <?php echo ($ckb_aca_cntt_dtCanc_limit == 1 || $dtCanc != '' ? "disabled" : "") ?>>
                        <option value="null">Selecione</option>
                        <?php $cur = odbc_exec($con, "select id_credencial,descricao from sf_credenciais where inativo = 0 order by descricao") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option value="<?php echo $RFP['id_credencial']; ?>"><?php echo utf8_encode($RFP['descricao']); ?></option> 
                        <?php } ?>
                    </select>
                </div>                
            </div>
            <div style="width:100%;clear:both;padding-top: 1%;">
                <span class="ajustSpan" style="margin-bottom: 2%;display: block;">Motivo: </span>
                <textarea id="txtMotivo" style="height: <?php echo ($dtCanc != '' ? "150px" : "180px"); ?>;" maxlength="512" <?php echo ($ckb_aca_cntt_dtCanc_limit == 1 || $dtCanc != '' ? 'disabled' : ''); ?>><?php echo $assunto; ?></textarea>
            </div>
            <?php if ($dtCanc != '') { ?>
                <p style="margin: inherit;color: #dc5039;margin-left: 0;"><?php echo "Rescindido por: " . $usurescind; ?></p>
            <?php } ?>
        </div>
        <div class="frmfoot" style="position: absolute;width: 95%;bottom: 0;">
            <div class="frmbtn">
                <?php if ($dtCanc != '') { ?>
                    <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir();" style="float: left;margin-top: 14px;" title="Imprimir"><span class="ico-print icon-white"></span> Imprimir</button>
                    <button class="btn yellow" onClick="desfazer();" style="margin-top: 15px"><span class="ico-reply"></span> Desfazer Cancelamento</button>
                <?php } else { ?>
                    <button class="btn green" type="button" name="bntSave" onClick="verificaObgtorio()"><span class="ico-checkmark"></span> Gravar</button>
                    <button class="btn yellow" onClick="parent.FecharBox(1)"><span class="ico-reply"></span> Cancelar</button>
                <?php } ?>
            </div>
        </div>
    </div>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
    <script type='text/javascript' src='../../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../../js/plugins.js'></script>
    <script type='text/javascript' src='../../../js/actions.js'></script>
    <script type='text/javascript' src='../../../js/plugins/datatables/jquery.dataTables.min.js'></script>
    <script type='text/javascript' src='../../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../../js/util.js"></script>
    <script type="text/javascript" src="../../../js/moment.min.js"></script>
    <script type="text/javascript" src="../js/RecisaoContrato.js"></script>
</body>