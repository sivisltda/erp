<?php include "../../../Connections/configini.php"; ?>
<link rel="icon" type="image/ico" href="../../../favicon.ico"/>
<link href="../../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../../css/main.css" rel="stylesheet">
<link href="../../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' src='../../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<style>
    body { font-family: sans-serif; }
    .selected { background: #acbad4 !important;}

    input[type="text"], input[type="password"]{
        margin-bottom: 15px;
    }
</style>
<body>
    <div class="row-fluid">
        <form>
            <div class="frmhead">
                <div class="frmtext">Login</div>
                <div class="frmicon" onClick="parent.FecharBox(<?php echo $fechar; ?>)">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont" style="height: 200px;">
                <div style="clear:both; height:5px"></div>
                <input type="text" name="empresa" disabled id="empresa" value="<?php echo $_SESSION["contrato"]; ?>">
                <input type="text" name="login" id="login" value="<?php echo $_SESSION["login_usuario"]; ?>">
                <input type="password" name="password" id="password" placeholder="Digite sua senha">
                <input type="hidden" name="txtIDdesc" id="txtIDdesc" value="<?php echo $_GET['idDesc']; ?>">                
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <button class="btn btn-success" type="button" onClick="salvar()" id="bntSave" title="Gravar"><span class="ico-checkmark"></span>Logar</button>
                    <button class="btn yellow" title="Cancelar" onClick="parent.FecharBox(<?php echo $fechar; ?>)"><span class="ico-reply"></span> Cancelar</button>
                </div>
            </div>
        </form>
    </div>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../../js/plugins.js'></script>
    <script type="text/javascript" src="../../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type='text/javascript' src='../../../js/plugins/datatables/fnReloadAjax.js'></script>
    <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../../js/util.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#password").focus();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                salvar();
                return false;
            }
        });

        $(document).keyup(function (e) {
            if (e.keyCode === 27) { // escape key maps to keycode `27`
                parent.FecharBox(0, 0);
            }
        });

        function salvar() {
            var dados = {
                txtusu: $("#login").val(), //o aluno que esta sendo feito o marketing
                txtpass: $("#password").val()
            };
            var error = '';
            if ($("#login").val() === '') {
                error = 'Digite o usuario.';
            } else if ($("#password").val() === '') {
                error = 'Digite a senha.';
            }
            if (error === '') {
                $.post("/Modulos/Academia/ajax/login_desc_server_processing.php", dados)
                .done(function (data) {
                    var data = JSON.parse(data);
                    if (data[0].status === 'YES') {
                        let valorD = textToNumber(<?php echo "parent.$('#txtValor_" . $_GET['idDesc'] . "'"; ?>).val()) - textToNumber(<?php echo "parent.$('#txtValFinal_" . $_GET['idDesc'] . "'"; ?>).val());
                        let valorM = textToNumber(parent.$("#txtMulta_<?php echo $_GET['idDesc'] ?>").val());
                        <?php echo ($_GET['tp'] == "C" ? 'parent.$("#txtMulta_' . $_GET['idDesc'] . '").val(numberFormat(0));' : ""); ?>                        
                        parent.aplicarDesconto($('#txtIDdesc').val(), 0);
                        parent.FecharBox<?php echo ($_GET['tp'] == "C" ? "Canc" : ""); ?>(0, 1, <?php echo $_GET['idDesc'] . "," . $_GET['idProd']; ?>, data[0].User, <?php echo ($_GET['tp'] == "C" ? "valorM" : "valorD"); ?>);
                    } else if (data[0].status === 'SENHA') {
                        bootbox.alert("Senha incorreta.", function () {
                            parent.FecharBox<?php echo ($_GET['tp'] == "C" ? "Canc" : ""); ?>(0, 0);
                        });
                    } else {
                        bootbox.alert("Esse usuário não tem permissão para desconto.", function () {
                            parent.FecharBox<?php echo ($_GET['tp'] == "C" ? "Canc" : ""); ?>(0, 0);
                        });
                    }
                });
            } else {
                bootbox.alert(error);
            }
        }
    </script>    
    <?php odbc_close($con); ?>
</body>