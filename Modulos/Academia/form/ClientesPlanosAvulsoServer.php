<?php

include "../../../Connections/configini.php";
require_once('../../../util/util.php');

$dias = 0;
$codplano = 0;
$query = "";
$interval = 1;
$tipoInterval = '';

$dtIni = str_replace('_', '-', $_POST['txtDtIni']);
$dtFimParc = date('Y-m-d', strtotime(date('Y-m-d', strtotime(date("Y-m-d", strtotime($dtIni)) . " +" . 0 . " day")) . "-1 day"));
$dtIniParc = date("Y-m-d", strtotime($dtIni));
$dtFim = date("Y-m-d", strtotime($dtFimParc));
$dtFimPlano = strtotime($dtFimParc);
$quantparc = $_POST['qntparc'];

if ($_POST['prazo'] == 1) {
    $tipoInterval = ' month';
    $interval = $_POST['interval'];
} else {
    $tipoInterval = ' day';
    $interval = $_POST['interval'];
}

for ($i = 1; $i <= $quantparc; $i++) {
    $dtFimPlano = strtotime("+" . $interval . $tipoInterval, strtotime($dtFimParc));
}

if ($_POST['finito'] == 1) {
    $query = "set dateformat dmy;insert into sf_vendas_planos (id_turno, dt_inicio, dt_fim,favorecido, dias_ferias, dt_cadastro,usuario_resp, id_prod_plano) values
    (" . 0 . ", " . valoresData('txtDtIni') . "," . valoresData(escreverData($dtFimParc)) . "," . $_POST['txtId'] . ", " . $dias . ", getdate(), " . $_SESSION["id_usuario"] . ", " . $_POST['txtProduto'] . ");
    SELECT SCOPE_IDENTITY() ID;";
    $result = odbc_exec($con, $query) or die(odbc_errormsg());
    odbc_next_result($result);
    $codplano = odbc_result($result, 1);
    for ($i = 1; $i <= $quantparc; $i++) {
        $dtFimParc = date('Y-m-d', strtotime(date("Y-m-d", strtotime($dtFimParc)) . " +" . $interval . $tipoInterval));
        $query = "set dateformat dmy; insert into sf_vendas_planos_mensalidade(id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens) values
        (" . $codplano . ", '" . $dtIniParc . "'," . valoresData(escreverData($dtFimParc)) . "," . 0 . ",getdate(),'" . valoresNumericos2($_POST['valMens']) . "');";
        $dtIniParc = date('Y-m-d', strtotime(date("Y-m-d", strtotime($dtIniParc)) . " +" . $interval . $tipoInterval));
        odbc_exec($con, $query);
        if ($i == $quantparc) {
            odbc_exec($con, "delete from sf_vendas_planos_dcc_agendamento where id_plano_agendamento = " . $codplano) or die(odbc_errormsg());
            $query = "set dateformat dmy;
            BEGIN TRANSACTION
                insert into sf_vendas_planos_dcc_agendamento (id_plano_agendamento, dt_cancel_agendamento, dt_cadastro_agendamento, id_user_agendamento, finito_agendamento) values
                (" . $codplano . "," . valoresData(escreverData($dtFimParc)) . ", getdate()," . $_SESSION["id_usuario"] . ",1);
                insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
                ('sf_vendas_planos_dcc_agend', " . $codplano . ", '" . 'Plano Finito' . "', 'I', 'INCLUSAO DE AGENDAMENTO DE CANCELAMENTO', GETDATE(), 0);
            IF @@ERROR = 0
            COMMIT
            ELSE
            ROLLBACK;";
            odbc_exec($con, $query) or die(odbc_errormsg());
        }
    }
} else {
    $query = "set dateformat dmy;insert into sf_vendas_planos (id_turno, dt_inicio, dt_fim,favorecido, dias_ferias, dt_cadastro,usuario_resp, id_prod_plano) values
    (0, " . valoresData('txtDtIni') . "," . valoresData(escreverData($dtFimParc)) . "," . $_POST['txtId'] . ", " . $dias . ", getdate(), " . $_SESSION["id_usuario"] . ", " . $_POST['txtProduto'] . ");
    SELECT SCOPE_IDENTITY() ID;";
    $result = odbc_exec($con, $query) or die(odbc_errormsg());
    odbc_next_result($result);
    $codplano = odbc_result($result, 1);
    $dtFimParc = date('Y-m-d', strtotime(date("Y-m-d", strtotime($dtFimParc)) . " +" . $interval . $tipoInterval));
    $query = "set dateformat dmy;insert into sf_vendas_planos_mensalidade(id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens) values
    (" . $codplano . ", '" . $dtIniParc . "'," . valoresData(escreverData($dtFimParc)) . "," . 0 . ",getdate(),'" . valoresNumericos2($_POST['valMens']) . "');";
    odbc_exec($con, $query);
}
echo $codplano;
