<?php include "../../../Connections/configini.php"; ?>
<link rel="icon" type="image/ico" href="../../../favicon.ico"/>
<link href="../../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../../css/main.css" rel="stylesheet">
<link href="../../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' src='../../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<style>
    body { font-family: sans-serif; }
    .selected { background: #acbad4 !important;}
</style>
<body>
    <div class="row-fluid">
        <form>
            <div class="frmhead">
                <div class="frmtext">Liberar Recebimento</div>
                <div class="frmicon" onClick="parent.FecharBox(<?php echo $fechar; ?>)">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont" style="height: 210px;">
                <input name="txtId" id="txtId" value="" type="hidden"/>
                <div style="width:20%; float:left; margin-left:1%">
                    <span>Data Início:</span>
                    <input id="txtDataFechar" class="datepicker inputCenter" type="text" value="<?php echo getData("T"); ?>">
                </div>
                <div style="width:15%; float:left; margin-left:1%">
                    <span>Horário:</span>
                    <input id="txtHoraFechar" type="text" class="input-medium" value="<?php echo date("H:i"); ?>">
                </div>
                <div style="width:61%; float:left; margin-left:1%">
                    <span>Encaminhar Para:</span>
                    <select id="txtMotivo" style="width:100%" class="select input-medium">
                        <option value="null">Não Informado</option>
                        <?php
                        $queryX = "select id_usuario id, nome from sf_usuarios left join sf_usuarios_permissoes on sf_usuarios.master = sf_usuarios_permissoes.id_permissoes where inativo = 0 and (out_remover_serasa = 1 or master = 0) order by nome";
                        $cur = odbc_exec($con, $queryX) or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option value="<?php echo $RFP['id']; ?>"><?php echo utf8_encode($RFP['nome']); ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div style="clear:both; height:5px"></div>
                <div class="body" style="margin-left:0; padding:0">
                    <div class="content" style="min-height:120px; margin-top:0">
                        <span>Observação:</span>
                        <textarea id="txtObsFechamento" style="width:100%; height:120px"></textarea>
                    </div>
                </div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <button class="btn btn-success" type="button" onClick="salvar()" id="bntSave" title="Gravar"><span class="ico-checkmark"></span> Gravar</button>
                    <button class="btn yellow" title="Cancelar" onClick="parent.FecharBox(<?php echo $fechar; ?>)"><span class="ico-reply"></span> Cancelar</button>
                </div>
            </div>
        </form>
        <script type='text/javascript' src='../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../../js/plugins.js'></script>
        <script type="text/javascript" src="../../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
        <script type='text/javascript' src='../../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../../js/moment.min.js"></script>
        <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../../js/util.js"></script>        
        <script type="text/javascript">
            
            $("#txtDataFechar, #txtDataPrazo").mask(lang["dateMask"]);
            $("#txtHoraFechar, #txtHoraPrazo").mask("99:99");
            
            $(document).ready(function () {                
                $('#CkbTransferir').click(function () {
                    if ($(this).is(':checked')) {
                        $("#txtObsFechamento").css("height", "80");
                        $("#dvNovoChamado").show();
                    } else {
                        $("#txtObsFechamento").css("height", "120");
                        $("#dvNovoChamado").hide();
                    }
                });
            });            
                
            function salvar() {
                var dados = {
                    txtIdx: '',
                    txtId: parent.$("#txtId").val(), //o aluno que esta sendo feito o marketing
                    txtData: $("#txtDataFechar").val(),
                    txtHora: $("#txtHoraFechar").val(),
                    txtDataProximo: $("#txtDataFechar").val(),
                    txtHoraProximo: $("#txtHoraFechar").val(),
                    txtDeHistorico: <?php echo $_SESSION["id_usuario"]; ?>, //usuario logado
                    txtPara: $("#txtMotivo").val(), //usuario que vai ser o remetente
                    txtRelato: $("#txtObsFechamento").val(), //é o valor do campo observação
                    ckbAcompanhar: '0',
                    txtQualificacao: '',
                    bntSave: 'S',
                    btnSerasa: 'S'
                };
                var error = '';
                if ($("#txtMotivo").val() === 'null') {
                    error = 'Selecione o atendente que deseja encaminhar.';
                } else if ($("#txtObsFechamento").val() === '') {
                    error = 'Digite a observação sobre o atendimento.';
                }

                if (error === '') {
                    $.post("/Modulos/CRM/form/CRMFormServerTelemarketing.php", dados)
                            .done(function (data) {
                                if (data.trim() === 'YES') {
                                    bootbox.alert("Atendimento foi enviado com sucesso.", function () {
                                        parent.FecharBox(0);
                                    });
                                }
                            });
                } else {
                    bootbox.alert(error);
                }
            }            
        </script>        
    </div>
    <?php odbc_close($con); ?>
</body>