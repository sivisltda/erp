<?php

if (isset($_POST['isAjax']) || isset($_GET['isAjax'])) {
    include "../../../Connections/configini.php";
}

$disabled = 'disabled';
$id = '';
$titulo = '';
$descricao = '';
$imprimir = 0;

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = 1;
}

if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_ambientes set " .
                        "nome_ambiente = UPPER(" . valoresTexto('txtTitulo') . ")," .
                        "descricao_ambiente = " . valoresTexto('txtDescricao') . " " .
                        "where id_ambiente = " . $_POST['txtId']) or die(odbc_errormsg());
        $query = "delete from sf_ambientes_itens where ambiente = " . $_POST['txtId'];
        odbc_exec($con, $query);
        if (isset($_POST['itemsGrupo'])) {
            for ($i = 0; $i < count($_POST['itemsGrupo']); $i++) {
                $query = "insert into sf_ambientes_itens(ambiente,catraca) values(" . $_POST['txtId'] . "," . $_POST['itemsGrupo'][$i] . ")";
                odbc_exec($con, $query);
            }
        }
    } else {
        odbc_exec($con, "insert into sf_ambientes(nome_ambiente,descricao_ambiente)values(UPPER(" .
                        valoresTexto('txtTitulo') . ")," .
                        valoresTexto('txtDescricao') . ") ") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_ambiente from sf_ambientes order by id_ambiente desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
        if (isset($_POST['itemsGrupo'])) {
            for ($i = 0; $i < count($_POST['itemsGrupo']); $i++) {
                $query = "insert into sf_ambientes_itens(ambiente,catraca) values(" . $_POST['txtId'] . "," . $_POST['itemsGrupo'][$i] . ")";
                odbc_exec($con, $query);
            }
        }
    }
}

if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        if (isset($_POST['List'])) {
            include "../../../Connections/configini.php";
        }
        odbc_exec($con, "DELETE FROM sf_ambientes WHERE id_ambiente = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox(1);</script>";
        exit();
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_ambientes where id_ambiente =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_ambiente'];
        $titulo = utf8_encode($RFP['nome_ambiente']);
        $descricao = utf8_encode($RFP['descricao_ambiente']);
    }
} else {
    $disabled = '';
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}

if (isset($_GET['listAmbientes'])) {
    $query = "select id_ambiente,nome_ambiente from sf_ambientes";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["id_ambiente"] = utf8_encode($RFP["id_ambiente"]);
        $row["nome_ambiente"] = utf8_encode($RFP["nome_ambiente"]);
        $records[] = $row;
    }
    echo json_encode($records);
}
