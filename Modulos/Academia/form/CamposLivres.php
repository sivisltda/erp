<?php

include "../../../Connections/configini.php";
require_once('../../../util/util.php');

if (isset($_POST['btnSave'])) {
    if (is_numeric($_POST['txtCLPessoa'])) {
        $query = "";
        $cur = odbc_exec($con, "select id_campo from sf_configuracao_campos_livres") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            if(is_numeric($_POST["txtCLid_" . $RFP['id_campo']])) {
                $query .= "UPDATE sf_fornecedores_despesas_campos_livres SET " . 
                "campos_campos = " . $RFP['id_campo'] . "," .                        
                "fornecedores_campos = " .  valoresNumericos("txtCLPessoa") . "," .                        
                "conteudo_campo = " . valoresTexto("txtCLConteudo_" . $RFP['id_campo']) . 
                " WHERE id_fornecedores_campo = " . valoresNumericos("txtCLid_" . $RFP['id_campo']) . ";";
            } elseif ($_POST["txtCLConteudo_" . $RFP['id_campo']] != "") {
                $query .= "INSERT INTO sf_fornecedores_despesas_campos_livres (campos_campos,fornecedores_campos, conteudo_campo) VALUES (" . 
                $RFP['id_campo'] . "," . valoresNumericos("txtCLPessoa") . "," . valoresTexto("txtCLConteudo_" . $RFP['id_campo']) . ");";
            }            
        }
        odbc_exec($con, $query) or die(odbc_errormsg());
        odbc_exec($con, "delete from sf_fornecedores_despesas_campos_livres 
        where id_fornecedores_campo in (select max(id_fornecedores_campo) from sf_fornecedores_despesas_campos_livres 
        group by fornecedores_campos, campos_campos having count(id_fornecedores_campo) > 1)") or die(odbc_errormsg());
        echo "YES";
    }
}

odbc_close($con);
