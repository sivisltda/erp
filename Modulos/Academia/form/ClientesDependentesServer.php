<?php

include "../../../Connections/configini.php";
require_once('../../../util/util.php');

if (isset($_POST['SalvarDependente'])) {
    if (is_numeric($_POST['txtId']) && is_numeric($_POST['txtIdDep'])) {
        $query = "select id_fornecedores_despesas, razao_social from sf_fornecedores_despesas_dependentes 
        inner join sf_fornecedores_despesas on id_titular = id_fornecedores_despesas
        where id_dependente in ('" . $_POST['txtIdDep'] . "')";
        $cur = odbc_exec($con, $query);
        while ($RFP = odbc_fetch_array($cur)) {
            echo "Não é possível realizar esta operação, dependente de " . $RFP['id_fornecedores_despesas'] . " - " . utf8_encode($RFP['razao_social']);
            exit;
        }
        $query = "set dateformat dmy;insert into sf_fornecedores_despesas_dependentes values (" . $_POST['txtId'] . "," .
        $_POST['txtIdDep'] . ",getdate()," . valoresCheck("ckbCompartilhado") . "," . valoresSelect("txtParentesco") . "," . valoresCheck("ckbResponsavel") . ");";                
        $query .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
        ('sf_fornecedores_despesas_dependentes', " . $_POST['txtIdDep'] . ", '" . $_SESSION["login_usuario"] . "',            
        'I', 'INCLUSAO DO DEPENDENTE " . $_POST['txtIdDep'] . " - ' + (select max(razao_social) from sf_fornecedores_despesas where id_fornecedores_despesas = " . $_POST['txtIdDep'] . "), GETDATE()," . $_POST['txtId'] . ");";                
        odbc_exec($con, $query) or die(odbc_errormsg());        
        echo "YES";
    }
}

if (isset($_POST['btnExcluirDependente'])) {
    if (is_numeric($_POST['txtId']) && is_numeric($_POST['txtIdDep'])) {
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
        ('sf_fornecedores_despesas_dependentes', " . $_POST['txtIdDep'] . ", '" . $_SESSION["login_usuario"] . "',            
        'E', 'EXCLUSAO DO DEPENDENTE " . $_POST['txtIdDep'] . " - ' + (select max(razao_social) from sf_fornecedores_despesas where id_fornecedores_despesas = " . $_POST['txtIdDep'] . "), GETDATE()," . $_POST['txtId'] . ");") or die(odbc_errormsg());        
        odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_dependentes WHERE id_titular = " . $_POST['txtId'] . " and id_dependente = " . $_POST['txtIdDep']);
        echo "YES";
    }
}

if (isset($_GET['listDependentes'])) {    
    $aColumns = array('razao_social', 'nome_parentesco');
    $iTotal = 0;
    $iFilteredTotal = 0;
    $sWhereX = "";
    $sWhere = "";
    
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = $_GET['iDisplayStart'];
        $sQtd = $_GET['iDisplayLength'];
    }
    
    if ($_GET['sSearch'] != "") {
        $sWhere = "AND (";
        for ($i = 0; $i < count($aColumns); $i++) {
            $sWhere .= prepareCollum($aColumns[$i], 0) . " LIKE '%" . utf8_decode($_GET['sSearch']) . "%' OR ";
        }
        $sWhere = substr_replace($sWhere, "", -3);
        $sWhere .= ')';
    }
    
    $sQuery = "set dateformat dmy; SELECT COUNT(*) total from sf_fornecedores_despesas_dependentes 
    inner join sf_fornecedores_despesas on id_fornecedores_despesas = id_dependente 
    left join sf_parentesco on parentesco = id_parentesco
    where id_titular = " . $_GET['listDependentes'] . $sWhereX . $sWhere;
    $cur2 = odbc_exec($con, $sQuery);
    while ($aRow = odbc_fetch_array($cur2)) {
        $iFilteredTotal = $aRow['total'];
    }
    
    $sQuery = "set dateformat dmy; SELECT COUNT(*) total from sf_fornecedores_despesas_dependentes 
    inner join sf_fornecedores_despesas on id_fornecedores_despesas = id_dependente 
    left join sf_parentesco on parentesco = id_parentesco
    where id_titular = " . $_GET['listDependentes'] . $sWhereX;
    $cur2 = odbc_exec($con, $sQuery);
    while ($aRow = odbc_fetch_array($cur2)) {
        $iTotal = $aRow['total'];
    }
    
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => $iTotal,
        "iTotalDisplayRecords" => $iFilteredTotal,
        "aaData" => array()
    );
    
    $sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (order by razao_social) as row,
    responsavel, id_dependente, razao_social, nome_parentesco, data_inclusao, compartilhado,
    FLOOR(DATEDIFF(DAY, data_nascimento, dateadd(day, 1, GETDATE())) / 365.25) idade
    from sf_fornecedores_despesas_dependentes 
    inner join sf_fornecedores_despesas on id_fornecedores_despesas = id_dependente 
    left join sf_parentesco on parentesco = id_parentesco
    where id_titular = " . $_GET['listDependentes'] . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " order by razao_social";    
    //echo $sQuery1; exit;
    $cur = odbc_exec($con, $sQuery1);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row[] = "<a style=\"color: " . ($RFP["responsavel"] == "1" ? "royalblue" : "green") . ";\" href=\"ClientesForm.php?id=" . $RFP['id_dependente'] . "\">" . utf8_encode($RFP["razao_social"]) . "</a>";
        $row[] = utf8_encode($RFP["nome_parentesco"]);
        $row[] = $RFP["idade"];
        $row[] = escreverData($RFP["data_inclusao"]);
        $row[] = tableFormato($RFP["compartilhado"], "SN", "C", "");
        $row[] = tableFormato($RFP["responsavel"], "SN", "C", "");
        if ($ckb_adm_cli_read_ == 0) {
            $row[] = "<center><input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\" onClick=\"RemoverDependente(" . $RFP['id_dependente'] . ")\")></center>";
        } else {
            $row[] = "<center><input type=\"hidden\" onClick=\"RemoverDependente(" . $RFP['id_dependente'] . ")\")></center>";
        }
        $output['aaData'][] = $row;
    }    
    echo json_encode($output);
}
