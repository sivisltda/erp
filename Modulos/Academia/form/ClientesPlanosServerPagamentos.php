<?php

$login_usuario = 'SISTEMA';
$gerarAuto = 0;
$gerarAutoTotal = 0;
$tipo_documento = 0;
$somar_dias = 0;
$vendaFilial = '';
$_POST['txtHrPag'] = date('H:i:s');

if (isset($_GET["hostname"]) && isset($_GET["database"]) && isset($_GET["username"]) && isset($_GET["password"])) {
    $hostname = $_GET["hostname"];
    $database = $_GET["database"];
    $username = $_GET["username"];
    $password = $_GET["password"];
    $contrato = str_replace("ERP", "", strtoupper($_GET['database']));    
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
    $cur = odbc_exec($con, "select * from sf_configuracao where id = 1");
    while ($RFP = odbc_fetch_array($cur)) {
        $_POST['txtParcelaTipo_1'] = $RFP['DCC_FORMA_PAGAMENTO'];
        $_POST['txtTipoPag'] = $RFP['DCC_FORMA_PAGAMENTO'];
        $tipo_documento = $RFP['DCC_FORMA_PAGAMENTO'];
    }
    require_once(__DIR__ . '/../../../Connections/funcoesAux.php');
}

if (is_numeric($tipo_documento) && $tipo_documento > 0) {
    $cur = odbc_exec($con, "select dias from sf_tipo_documento where id_tipo_documento = " . $tipo_documento);
    while ($RFP = odbc_fetch_array($cur)) {
        $somar_dias = $RFP['dias'];
    }
}

if (isset($_SESSION["login_usuario"])) {
    $login_usuario = strtoupper($_SESSION["login_usuario"]);
}

if (isset($_GET["txtIdCli"]) && is_numeric($_GET["txtIdCli"])) {
    $_POST['txtIdCli'] = $_GET["txtIdCli"];
}

if (isset($_GET["txtEmpresa"]) && is_numeric($_GET["txtEmpresa"])) {
    $_POST['txtEmpresa'] = $_GET["txtEmpresa"];
}

if (isset($_GET["txtValFinal"])) {
    $_POST['txtValFinal_1'] = $_GET["txtValFinal"];
    $_POST['txtParcelaV_1'] = $_GET["txtValFinal"];
}

if (isset($_GET["txtValMens"])) {
    $_POST['txtValor_1'] = $_GET["txtValMens"];
}

if (isset($_GET["txtIdItem"])) {
    $_POST['txtIdItem_1'] = $_GET["txtIdItem"];
}

if (isset($_GET["txtIdPlano"])) {
    $_POST['txtIdPlano_1'] = $_GET["txtIdPlano"];
}

if (isset($_GET["isDcc"]) && $_GET["isDcc"] == "S") {
    $_POST['isDcc'] = "S";
    $_POST['txtQtdItens'] = 1;
    $_POST['txtQtd_1'] = 1;
    $_POST['txtQtdParc'] = 1;
    $_POST['txtParcelaD_1'] = escreverDataSoma(getData("T"), "+ " . $somar_dias . " day");
}

if (isset($_GET["idTransacao"])) {
    $_POST['idTransacao'] = $_GET["idTransacao"];
}

if (!isset($_POST['txtDtPag']) || $_POST['txtDtPag'] == "") {
    $_POST['txtDtPag'] = getData("T");
}

odbc_autocommit($con, false);
$cur = odbc_exec($con, "select * from sf_configuracao where id = 1");
while ($RFP = odbc_fetch_array($cur)) {
    $gerarAuto = $RFP['ACA_RENOVACAO_AUTO'];
    $gerarAutoTotal = $RFP['clb_mensalidades_aberto'];
    $vendaFilial = $RFP['fin_venda_filial'];
}

$descr = utf8_decode('PAG PLANO / SERVIÇO');
if (isset($_POST['isCredito']) && $_POST['isCredito'] == "S") {
    $descr = utf8_decode("AQUISIÇÃO CREDITO CARTEIRA");
}

$query = "set dateformat dmy;insert into sf_vendas (vendedor, cliente_venda, historico_venda, data_venda, sys_login, empresa, destinatario, status, tipo_documento, cov, data_aprov, grupo_conta, conta_movimento)
values('" . $login_usuario . "'," . $_POST['txtIdCli'] . ", '" . $descr . "'," . valoresDataHora("txtDtPag", "txtHrPag") . ", '" . $login_usuario . "', " . 
(is_numeric($vendaFilial) ? $vendaFilial : $_POST['txtEmpresa']) . ", '" . $login_usuario . "', 'Aprovado', " . $_POST['txtParcelaTipo_1'] . ", 'V', " . valoresDataHora("txtDtPag", "txtHrPag") . ", 14, 1)";
//echo $query; exit();
$objExec = odbc_exec($con, $query);
$res = odbc_exec($con, "select top 1 id_venda from sf_vendas order by id_venda desc") or die(odbc_errormsg());
$idVenda = odbc_result($res, 1);
$res = odbc_exec($con, "select isnull(indicador,0) indicador from sf_fornecedores_despesas where id_fornecedores_despesas = " . $_POST['txtIdCli']) or die(odbc_errormsg());
$idComissionado = odbc_result($res, 1);
$idComissionado = $idComissionado > 0 ? $idComissionado : 'NULL';

$query = "set dateformat dmy;";
$valorTotal = 0;
$valorPagoCredito = 0;
$id_mens_plano = 0;
$id_all_mens = 0;
$saldoCredito = 0;
$valCredParc = 0;
$bandeira = 0;

if (isset($_POST['txtQtdItens'])) {
    $max = $_POST['txtQtdItens'];
    $valCred = 0;
    for ($i = 1; $i <= $max; $i++) {
        $valorFinal = valoresNumericos('txtValFinal_' . $i);
        $valorMulta = valoresNumericos('txtMulta_' . $i);
        $valorBruto = valoresNumericos('txtValor_' . $i);
        if ($_POST['txtIdItem_' . $i] == 466 && $_POST['isDcc'] != "C") {
            $valCred = $valCred + $valorFinal;
        }
        $valorTotal = $valorTotal + $valorFinal;
        if (isset($_POST['txtIdItem_' . $i])) {
            $id_mens = 0;
            if ($_POST['txtIdPlano_' . $i] != "") {
                $id_mens = $_POST['txtIdPlano_' . $i];
                $id_mens_plano = $_POST['txtIdPlano_' . $i];
                $id_all_mens .= "," . $_POST['txtIdPlano_' . $i];
            }
            if ($_POST['isDcc'] == "C") {
                $query .= "insert into sf_vendas_itens(id_venda, grupo, produto, quantidade, valor_total, valor_bruto, vendedor_comissao, valor_multa) values(" . $idVenda . ", " .
                        "(select top 1 conta_movimento from sf_produtos where conta_produto = " . $_POST['txtIdItem_' . $i] . "), " . $_POST['txtIdItem_' . $i] . ", " . $_POST['txtQtd_' . $i] . "," . $valorFinal . "," . $valorBruto . "," . $idComissionado . "," . $valorMulta . ")";
            } else {
                $query .= "insert into sf_vendas_itens(id_venda, grupo, produto, quantidade, valor_total, valor_bruto, vendedor_comissao, valor_multa) values(" . $idVenda . ", " .
                        "(select top 1 conta_movimento from sf_produtos where conta_produto = " . $_POST['txtIdItem_' . $i] . "), " . $_POST['txtIdItem_' . $i] . ", " . $_POST['txtQtd_' . $i] . "," . $valorFinal . "," . $valorBruto . "," . $idComissionado . "," . $valorMulta . ")
                DECLARE @newID" . $i . " INT;SELECT @newID" . $i . " = SCOPE_IDENTITY();
                update sf_vendas_planos_mensalidade set id_item_venda_mens = @newID" . $i . ", dt_pagamento_mens = " . valoresDataHora("txtDtPag", "txtHrPag") . " where id_mens = " . $id_mens . ";";
            }
        }
    }
    if ($valCred > 0) {
        $query .= "insert into sf_creditos (id_fornecedor_despesa, valor, data, tipo, id_venda, historico, usuario_resp, inativo) values (" . $_POST['txtIdCli'] . ", " . $valCred . ", " . valoresDataHora("txtDtPag", "txtHrPag") . ", 'C'," . $idVenda . ",'" . utf8_decode('AQUISIÇÃO CREDITO') . "' ," . $_SESSION["id_usuario"] . ",0);";
    }
}

$curCredito = odbc_exec($con, "select ISNULL(SUM(case when tipo = 'C' then valor else valor * -1 end),0)  saldo_credito
from sf_creditos where inativo = 0 and id_fornecedor_despesa = " . $_POST['txtIdCli'] . " and (id_venda_quitacao is null or id_venda_quitacao = id_venda)");
while ($RFP = odbc_fetch_array($curCredito)) {
    $saldoCredito = $RFP['saldo_credito'];
}

if (isset($_POST['txtQtdParc'])) {
    $max = $_POST['txtQtdParc'];
    for ($i = 1; $i <= $max; $i++) {
        $id_banco = 1;
        $qCheque = "";
        $valorPago = 0;
        $dtPagamento = "null";
        $valorParcela = valoresNumericos('txtParcelaV_' . $i);
        $numero_doc = "";
        if ($_POST['txtParcelaTipo_' . $i] == 12) {
            $valCredParc = $valCredParc + $valorParcela;
            $saldoCreditoParc = $valorParcela;
            if ($saldoCredito > 0) {
                if ($valorParcela > $saldoCredito) {
                    $valorQuitacao = $saldoCredito;
                } else {
                    $valorQuitacao = $valorParcela;
                }
                $query .= "insert into sf_creditos (id_fornecedor_despesa, valor, data, tipo, id_venda, historico, dt_vencimento, usuario_resp, id_venda_quitacao, inativo) values
                (" . $_POST['txtIdCli'] . ", " . abs($valorQuitacao) . ", " . valoresDataHora("txtDtPag", "txtHrPag") . ", 'D'," . $idVenda . ",'PAG PLANO/SERVICO' , " . valoresDataHora("txtDtPag", "txtHrPag") . "," . $_SESSION["id_usuario"] . ", " . $idVenda . ",0);";
                $saldoCredito = $saldoCredito - $valorParcela;
                $saldoCreditoParc = abs($saldoCredito);
            }
            if ($saldoCreditoParc > 0 && ($valorParcela >= ($saldoCredito + $valorParcela))) {
                $query .= "insert into sf_creditos (id_fornecedor_despesa, valor, data, tipo, id_venda, historico, dt_vencimento, usuario_resp, inativo) values
                (" . $_POST['txtIdCli'] . ", " . $saldoCreditoParc . ", " . valoresDataHora("txtDtPag", "txtHrPag") . ", 'D'," . $idVenda . ",'PAG PLANO/SERVICO'," . valoresData('txtParcelaD_' . $i) . "," . $_SESSION["id_usuario"] . ",0);";
            }
        } else if ($_POST['txtParcelaTipo_' . $i] == 5) {
            $qCheque .= "DECLARE @IDPARC" . $i . " INT;
            SELECT @IDPARC" . $i . " = SCOPE_IDENTITY();
            insert into sf_cheques(chq_data, chq_agencia, chq_banco, chq_conta, chq_proprietario, chq_numero, chq_valor,
            chq_recebido, tipo, codigo_relacao) values (" . valoresData('txtParcelaD_' . $i) . ", " . valoresTexto('txtChAgencia_' . $i) . "," .
                    valoresTexto('txtChBanco_' . $i) . ", " . valoresTexto('txtChConta_' . $i) . "," . valoresTexto('txtChProp_' . $i) . "," .
                    valoresTexto('txtChNumero_' . $i) . ", " . $valorParcela . ",0,'V',@IDPARC" . $i . ");";
            $numero_doc = $_POST['txtChNumero_' . $i];
        } else { //inserir os campos de obs
            $sql = "select id_tipo_documento_campos,campo from sf_tipo_documento_campos where inativo = 0 AND tipo_documento = " . $_POST['txtParcelaTipo_' . $i] . " ORDER BY 1";
            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                if (strlen($_POST['txtCampo' . $RFP['id_tipo_documento_campos'] . "_" . $i]) > 0) {
                    $numero_doc = $_POST['txtCampo' . $RFP['id_tipo_documento_campos'] . "_" . $i];
                    $qCampos = "insert into sf_vendas_campos(id_sf_vendas,tipo_documento_campos,conteudo) values (" . $idVenda . "," . $RFP['id_tipo_documento_campos'] . "," . valoresTexto('txtCampo' . $RFP['id_tipo_documento_campos'] . "_" . $i) . ")";
                    $objExec = odbc_exec($con, $qCampos);
                }
            }
        }
        if ($_POST['txtParcelaTipo_' . $i] == 6) {
            $valorPago = $valorParcela;
            $dtPagamento = valoresDataHora("txtDtPag", "txtHrPag");
        }
        $query .= "insert into sf_venda_parcelas(venda, numero_parcela, data_parcela, valor_parcela, valor_pago, data_pagamento, valor_parcela_liquido, id_banco, tipo_documento,historico_baixa, pa, data_cadastro, syslogin, somar_rel, sa_descricao) values
        (" . $idVenda . "," . $i . "," . valoresData('txtParcelaD_' . $i) . ", " . $valorParcela . ", " . $valorPago . "," . $dtPagamento . ", 0, "
                . $id_banco . ", " . $_POST['txtParcelaTipo_' . $i] . ", '" . $descr . "', '" . str_pad($i, 2, "0", STR_PAD_LEFT) . "/" . str_pad($max, 2, "0", STR_PAD_LEFT)
                . "', " . valoresDataHora("txtDtPag", "txtHrPag") . ",'" . $login_usuario . "', " . ($_POST['txtParcelaTipo_' . $i] == 12 ? "0" : "1") . ", '" . substr($numero_doc, 0, 50) . "');" . $qCheque;
    }        
}

if (isset($_POST['idTransacao']) && is_numeric($_POST['idTransacao'])) {
    $cur = odbc_exec($con, "select id_bandeira from sf_vendas_itens_dcc where id_transacao = " . $_POST['idTransacao'] . ";");
    while ($RFP = odbc_fetch_array($cur)) {
        $bandeira = $RFP['id_bandeira'];
    }
}

$query .= "update sf_venda_parcelas set valor_parcela_liquido = [dbo].[FU_TAXA_FORMA_PAGAMENTO](sf_venda_parcelas.tipo_documento,sf_venda_parcelas.venda, " . $bandeira . ", valor_parcela) " . 
($_POST['isDcc'] == "C" ? ", somar_rel = 0" : "") . " where valor_parcela_liquido = 0 and venda = " . $idVenda . ";";

$query .= "update sf_vendas set conta_movimento = [dbo].[FU_SUB_GRUPO_VENDA](id_venda) where cov = 'V' and id_venda = " . $idVenda . ";";
    
$isCancelDcc = "";
if ($_POST['isDcc'] == "S") {
    $curCancelamento = odbc_exec($con, "select case when dt_cancel_agendamento < (select dateadd(month ,1,dt_fim_mens) from sf_vendas_planos_mensalidade where id_mens = " . $id_mens_plano . ")
    then 'S' else 'N' end isCancelDcc from dbo.sf_vendas_planos_dcc_agendamento
    where id_plano_agendamento = (select id_plano_mens from dbo.sf_vendas_planos_mensalidade where id_mens = " . $id_mens_plano . ")");
    while ($RFP = odbc_fetch_array($curCancelamento)) {
        $isCancelDcc = $RFP['isCancelDcc'];
    }
}

if (isset($_POST['idTransacao']) && is_numeric($_POST['idTransacao'])) {
    $query .= " update sf_vendas_itens_dcc set id_venda_transacao = " . $idVenda . " where id_transacao = " . $_POST['idTransacao'] . ";";
}

if (($_POST['isDcc'] == "S" && $isCancelDcc != "S") || $gerarAuto == 1) {
    if ($_POST['system'] == 1) {
        $valorMens = '';
        $id = $_POST['txtIdPlano_1'];
        $dtIni = '';
        $dtFim = '';
        $query2 = 'select id_plano_mens,valor_mens,dt_inicio_mens,dt_fim_mens from sf_vendas_planos_mensalidade where id_mens =' . $id;
        $diferenca = '';
        $res = odbc_exec($con, $query2);
        $valorArredondado = '';
        $tipodata = '';
        while ($RFP = odbc_fetch_array($res)) {
            $valorMens = $RFP['valor_mens'];
            $dtIni = escreverData($RFP['dt_inicio_mens'], 'Y-m-d');
            $dtFim = escreverData($RFP['dt_fim_mens'], 'Y-m-d');
            $diferenca = dataDiff($dtIni, $dtFim, 'd');
        }
        if ($diferenca > 28) {
            $valorArredondado = round($diferenca / 28);
            $tipodata = 'month';
        } else {
            $tipodata = 'day';
            $valorArredondado = $diferenca;
        }
        
        $query .= " insert into sf_vendas_planos_mensalidade (id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens)
        select id_plano_mens, dateadd(" . $tipodata . "," . $valorArredondado . ",dt_inicio_mens), dateadd(" . $tipodata . "," . $valorArredondado . ",dt_fim_mens), id_parc_prod_mens, GETDATE()," . $valorMens . "
        from sf_vendas_planos_mensalidade m1 inner join sf_produtos_parcelas on id_parc_prod_mens = id_parcela 
        inner join sf_produtos on sf_produtos.conta_produto = sf_produtos_parcelas.id_produto
        inner join sf_vendas_planos on id_plano = id_plano_mens    
        where inativar_renovacao = 0 and id_mens = " . $id_mens_plano . " and dt_cancelamento is null
        and parcela in (-2,-1,0,1) and (select COUNT(*) from sf_vendas_planos_mensalidade m2
        where m2.id_plano_mens = m1.id_plano_mens and m2.id_parc_prod_mens = m1.id_parc_prod_mens
        and month(m2.dt_inicio_mens) = month(dateadd(month,1,m1.dt_inicio_mens))
        and year(m2.dt_inicio_mens) = year(dateadd(month,1,m1.dt_inicio_mens))) = 0;";
    } else { //Ajustado para pegar final da mensalidade
        for ($i = 0; $i < $gerarAutoTotal; $i++) {
            $query .= " insert into sf_vendas_planos_mensalidade (id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens)
            select id_plano_mens, dateadd(month," . $i . ",dateadd(day,1,dt_fim_mens)),
            case when tp_valid_acesso = 'C' then dateadd(day, qtd_dias_acesso,dt_fim_mens) else 
                dateadd(day,-1, dateadd(month,1,dateadd(month," . $i . ",dateadd(day,1,dt_fim_mens))))
            end,
            id_parc_prod_mens, GETDATE(),valor_unico valor_mens
            from sf_vendas_planos_mensalidade m1 inner join sf_produtos_parcelas pp on m1.id_parc_prod_mens = pp.id_parcela
            inner join sf_produtos p on p.conta_produto = pp.id_produto
            inner join sf_vendas_planos on id_plano = id_plano_mens                
            where p.inativar_renovacao = 0 and id_mens in (" . $id_all_mens . ") and dt_cancelamento is null and parcela in (-2,-1,0,1) and (select COUNT(*) from sf_vendas_planos_mensalidade m2
            where m2.id_plano_mens = m1.id_plano_mens and m2.id_parc_prod_mens = m1.id_parc_prod_mens
            and ((tp_valid_acesso <> 'C' and month(m2.dt_inicio_mens) = month(dateadd(month," . ($i + 1) . ",m1.dt_inicio_mens)) and year(m2.dt_inicio_mens) = year(dateadd(month," . ($i + 1) . ",m1.dt_inicio_mens))
            ) or (tp_valid_acesso = 'C' and dateadd(day," . ($i + 1) . ",m1.dt_fim_mens) = dt_inicio_mens))) = 0;";
        }
    }
}

$query .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
values ('sf_vendas', " . $idVenda . ", '" . $_SESSION["login_usuario"] . "', 'V', 'VENDA " . (substr(valoresDataHora("txtDtPag", "txtHrPag"), 1, 10) != getData("T") ? "RETROATIVA" : "") . " DTPAG: " . $_POST["txtDtPag"] . "', GETDATE(), " . $_POST['txtIdCli'] . "); ";

if ($_POST['isDcc'] == "C") {
    $query .= "update sf_creditos set id_venda_quitacao = " . $idVenda . " where id_credito in (" . $id_all_mens . ") and id_venda_quitacao is null;";
    $query .= "update sf_venda_parcelas set data_pagamento = " . valoresDataHora("txtDtPag", "txtHrPag") . ", valor_pago = " . $valorParcela . ",inativo = 2 where id_parcela in (
    select top 1 id_parcela from sf_venda_parcelas inner join sf_creditos on sf_creditos.id_venda = sf_venda_parcelas.venda
    where sf_creditos.inativo = 0 and tipo_documento = 12 and data_pagamento is null and data_parcela = dt_vencimento and valor_parcela = valor and id_credito in (" . $id_all_mens . "));";
    $query .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    values ('sf_vendas', " . $idVenda . ", '" . $_SESSION["login_usuario"] . "', 'Q', 'QUITACAO CARTEIRA: " . $id_all_mens . "', GETDATE(), " . $_POST["txtIdCli"] . ");";
} else {
    $query .= "update sf_vendas_planos set dt_fim = dbo.FU_PLANO_FIM(id_plano)
    where id_plano in(select id_plano_mens from sf_vendas_planos_mensalidade where id_mens = " . $id_mens_plano . "); ";    
}

$data = json_decode(stripslashes($_POST['desct']));
if (count($data) > 0) {
    foreach ($data as $d) {
        $query .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        values('sf_vendas', " . $idVenda . ", '" . $d->usuario . "', 'D', 'DESCONTO EM ' + (select top 1 descricao from sf_produtos where conta_produto = " . $d->idprod . ") + ' DE " . $d->valor . "', GETDATE()," . $_POST["txtIdCli"] . ");";
    }
}

$dataCanc = json_decode(stripslashes($_POST['dcanc']));
if (count($dataCanc) > 0) {
    foreach ($dataCanc as $d) {
        $query .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        values('sf_vendas', " . $idVenda . ", '" . $d->usuario . "', 'M', 'CANCELAMENTO DE MULTA EM ' + (select top 1 descricao from sf_produtos where conta_produto = " . $d->idprod . ") + ' DE " . $d->valor . "', GETDATE()," . $_POST["txtIdCli"] . ");";
    }
}
$query .= "EXEC dbo.SP_MK_VISTORIA @id_venda = " . $idVenda . ", @loja = '" . $contrato . "';";
//echo $query; exit;
$objExec = odbc_exec($con, $query);
if (!$objExec) {
    odbc_rollback($con);
    echo "ERROR" . $query;
    exit();
}
if ($objExec) {
    odbc_commit($con);
    echo $idVenda;
}