<?php

include "../../../Connections/configini.php";

$grupo_conta = "";
$conta_movimento = "";

if ($_POST['txtId'] == '') {
    $cur = odbc_exec($con, "select cc.id_contas_movimento id_cred,cc.grupo_conta grup_cred,
    cd.id_contas_movimento id_deb,cd.grupo_conta grup_deb from sf_configuracao
    left join sf_contas_movimento cc on cc.id_contas_movimento = fin_ccontas_movimento and cc.inativa = 0 
    left join sf_contas_movimento cd on cd.id_contas_movimento = fin_dcontas_movimento and cd.inativa = 0") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        if ($_POST['txtOperacao'] == 'C') {
            $grupo_conta = $RFP["grup_cred"];
            $conta_movimento = $RFP["id_cred"];
        } else if ($_POST['txtOperacao'] == 'D') {
            $grupo_conta = $RFP["grup_deb"];
            $conta_movimento = $RFP["id_deb"];
        }
    }
    if (is_numeric($grupo_conta) && is_numeric($conta_movimento)) {
        odbc_exec($con, "set dateformat dmy");
        $query = "insert into sf_solicitacao_autorizacao(empresa,grupo_conta,conta_movimento,fornecedor_despesa,tipo_documento,
        historico,destinatario,comentarios,sys_login,data_lanc,status,destinatario2,data_aprov,destaprov)values(" .
        valoresSelect("txtEmpresa") . "," . valoresSelect2($grupo_conta) . "," . valoresSelect2($conta_movimento) . ",0," . valoresSelect("txtForma") . "," . valoresTexto2("Lançamento no Caixa") . "," .
        valoresTexto2($_SESSION['login_usuario']) . "," . valoresTexto("txtObs") . "," .
        valoresTexto2($_SESSION['login_usuario']) . ",cast(getdate() as date),'Aprovado','',getdate(),1)";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_solicitacao_autorizacao from sf_solicitacao_autorizacao order by id_solicitacao_autorizacao desc") or die(odbc_errormsg());
        $nn = odbc_result($res, 1);
        odbc_exec($con, "set dateformat dmy; insert into sf_solicitacao_autorizacao_parcelas (solicitacao_autorizacao,
        numero_parcela,data_parcela,valor_parcela,valor_pago,pa,sa_descricao,inativo,historico_baixa,valor_multa,valor_juros
        ,tipo_documento,data_cadastro,exp_remessa,somar_rel) values (" . $nn . ", 1, cast(GETDATE() as date), " . valoresNumericos("txtValor") . ", 0.00, '01/01',
        '', 0, '', 0.00, 0.00, " . valoresSelect("txtForma") . ",GETDATE(), 0, 1)") or die(odbc_errormsg());
        echo "YES";
    } else {
        echo "Configuração inválida para esta opção!";
    }
}

odbc_close($con);
