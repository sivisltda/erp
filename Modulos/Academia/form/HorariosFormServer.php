<?php

$imprimir = 0;
$disabled = 'disabled';
$id = '';
$descricao = '';
$faixa1_ini = '';
$faixa1_fim = '';
$faixa2_ini = '';
$faixa2_fim = '';
$faixa3_ini = '';
$faixa3_fim = '';
$faixa4_ini = '';
$faixa4_fim = '';
$faixa5_ini = '';
$faixa5_fim = '';
$dia_semana1 = '';
$dia_semana2 = '';
$dia_semana3 = '';
$dia_semana4 = '';
$dia_semana5 = '';

if ($_GET['isAjax']) {
    include "../../../Connections/configini.php";
}

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = 1;
}
if (isset($_GET['listHorarios'])) {
    $query = "select cod_turno,nome_turno  from sf_turnos where cod_turno > 0 order by nome_turno";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["cod_turno"] = utf8_encode($RFP["cod_turno"]);
        $row["nome_turno"] = utf8_encode($RFP["nome_turno"]);
        $records[] = $row;
    }
    echo json_encode($records);
}


if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_turnos set " .
                        "nome_turno = UPPER(" . valoresTexto('txtDescricao') . ")," .
                        "faixa1_ini = " . valoresHora('txtFaixa1_ini') . "," .
                        "faixa1_fim = " . valoresHora('txtFaixa1_fim') . "," .
                        "faixa2_ini = " . valoresHora('txtFaixa2_ini') . "," .
                        "faixa2_fim = " . valoresHora('txtFaixa2_fim') . "," .
                        "faixa3_ini = " . valoresHora('txtFaixa3_ini') . "," .
                        "faixa3_fim = " . valoresHora('txtFaixa3_fim') . "," .
                        "faixa4_ini = " . valoresHora('txtFaixa4_ini') . "," .
                        "faixa4_fim = " . valoresHora('txtFaixa4_fim') . "," .
                        "faixa5_ini = " . valoresHora('txtFaixa5_ini') . "," .
                        "faixa5_fim = " . valoresHora('txtFaixa5_fim') . "," .
                        "dia_semana1 = '" . valoresCheck('fx1_0') . valoresCheck('fx1_1') . valoresCheck('fx1_2') . valoresCheck('fx1_3') . valoresCheck('fx1_4') . valoresCheck('fx1_5') . valoresCheck('fx1_6') . "', " .
                        "dia_semana2 = '" . valoresCheck('fx2_0') . valoresCheck('fx2_1') . valoresCheck('fx2_2') . valoresCheck('fx2_3') . valoresCheck('fx2_4') . valoresCheck('fx2_5') . valoresCheck('fx2_6') . "', " .
                        "dia_semana3 = '" . valoresCheck('fx3_0') . valoresCheck('fx3_1') . valoresCheck('fx3_2') . valoresCheck('fx3_3') . valoresCheck('fx3_4') . valoresCheck('fx3_5') . valoresCheck('fx3_6') . "', " .
                        "dia_semana4 = '" . valoresCheck('fx4_0') . valoresCheck('fx4_1') . valoresCheck('fx4_2') . valoresCheck('fx4_3') . valoresCheck('fx4_4') . valoresCheck('fx4_5') . valoresCheck('fx4_6') . "', " .
                        "dia_semana5 = '" . valoresCheck('fx5_0') . valoresCheck('fx5_1') . valoresCheck('fx5_2') . valoresCheck('fx5_3') . valoresCheck('fx5_4') . valoresCheck('fx5_5') . valoresCheck('fx5_6') . "' " .
                        "where cod_turno = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_turnos(nome_turno,faixa1_ini,faixa1_fim,faixa2_ini,faixa2_fim,faixa3_ini
                        ,faixa3_fim,faixa4_ini,faixa4_fim,faixa5_ini,faixa5_fim,dia_semana1,dia_semana2,dia_semana3,dia_semana4,dia_semana5)values(UPPER(" .
                        valoresTexto('txtDescricao') . ")," .
                        valoresHora('txtFaixa1_ini') . "," .
                        valoresHora('txtFaixa1_fim') . "," .
                        valoresHora('txtFaixa2_ini') . "," .
                        valoresHora('txtFaixa2_fim') . "," .
                        valoresHora('txtFaixa3_ini') . "," .
                        valoresHora('txtFaixa3_fim') . "," .
                        valoresHora('txtFaixa4_ini') . "," .
                        valoresHora('txtFaixa4_fim') . "," .
                        valoresHora('txtFaixa5_ini') . "," .
                        valoresHora('txtFaixa5_fim') . "," .
                        "'" . valoresCheck('fx1_0') . valoresCheck('fx1_1') . valoresCheck('fx1_2') . valoresCheck('fx1_3') . valoresCheck('fx1_4') . valoresCheck('fx1_5') . valoresCheck('fx1_6') . "'," .
                        "'" . valoresCheck('fx2_0') . valoresCheck('fx2_1') . valoresCheck('fx2_2') . valoresCheck('fx2_3') . valoresCheck('fx2_4') . valoresCheck('fx2_5') . valoresCheck('fx2_6') . "'," .
                        "'" . valoresCheck('fx3_0') . valoresCheck('fx3_1') . valoresCheck('fx3_2') . valoresCheck('fx3_3') . valoresCheck('fx3_4') . valoresCheck('fx3_5') . valoresCheck('fx3_6') . "'," .
                        "'" . valoresCheck('fx4_0') . valoresCheck('fx4_1') . valoresCheck('fx4_2') . valoresCheck('fx4_3') . valoresCheck('fx4_4') . valoresCheck('fx4_5') . valoresCheck('fx4_6') . "'," .
                        "'" . valoresCheck('fx5_0') . valoresCheck('fx5_1') . valoresCheck('fx5_2') . valoresCheck('fx5_3') . valoresCheck('fx5_4') . valoresCheck('fx5_5') . valoresCheck('fx5_6') . "') ") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 cod_turno from sf_turnos where cod_turno > 0 order by cod_turno desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}

if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        if (isset($_POST['List'])) {
            include "../../../Connections/configini.php";
        }
        odbc_exec($con, "DELETE FROM sf_turnos WHERE cod_turno = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox(1);</script>";
        exit();
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_turnos where cod_turno =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['cod_turno'];
        $descricao = utf8_encode($RFP['nome_turno']);
        $faixa1_ini = utf8_encode($RFP['faixa1_ini']);
        $faixa1_fim = utf8_encode($RFP['faixa1_fim']);
        $faixa2_ini = utf8_encode($RFP['faixa2_ini']);
        $faixa2_fim = utf8_encode($RFP['faixa2_fim']);
        $faixa3_ini = utf8_encode($RFP['faixa3_ini']);
        $faixa3_fim = utf8_encode($RFP['faixa3_fim']);
        $faixa4_ini = utf8_encode($RFP['faixa4_ini']);
        $faixa4_fim = utf8_encode($RFP['faixa4_fim']);
        $faixa5_ini = utf8_encode($RFP['faixa5_ini']);
        $faixa5_fim = utf8_encode($RFP['faixa5_fim']);
        $dia_semana1 = utf8_encode($RFP['dia_semana1']);
        $dia_semana2 = utf8_encode($RFP['dia_semana2']);
        $dia_semana3 = utf8_encode($RFP['dia_semana3']);
        $dia_semana4 = utf8_encode($RFP['dia_semana4']);
        $dia_semana5 = utf8_encode($RFP['dia_semana5']);
    }
} else {
    $disabled = '';
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
