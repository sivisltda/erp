<?php

include "../../../Connections/configini.php";
require_once('../../../util/util.php');

if (isset($_GET['id_fornecedores_despesas'])) {

    $id_fornecedor = (int) $_GET['id_fornecedores_despesas'];

    //selecionando os veiculos desse cliente
    $query = " SELECT * FROM sf_fornecedores_despesas_veiculo WHERE id_fornecedores_despesas = $id_fornecedor  ";

    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["id"] = utf8_encode($RFP["id"]);
        $row["id_fornecedores_despesas"] = utf8_encode($RFP["id_fornecedores_despesas"]);
        $row["valor"] = utf8_encode($RFP["valor"]);
        $row["marca"] = utf8_encode($RFP["marca"]);
        $row["modelo"] = utf8_encode($RFP["modelo"]);
        $row["ano_modelo"] = utf8_encode($RFP["ano_modelo"]);
        $row["combustivel"] = utf8_encode($RFP["combustivel"]);
        $row["codigo_fipe"] = utf8_encode($RFP["codigo_fipe"]);
        $row["placa"] = utf8_encode($RFP["placa"]);
        $row["renavam"] = utf8_encode($RFP["renavam"]);
        $row["uso"] = utf8_encode($RFP["uso"]);
        $row["dirige_maior_tempo"] = utf8_encode($RFP["dirige_maior_tempo"]);
        $row["condutores_18"] = utf8_encode($RFP["condutores_18_25"]);
        $row["sinistro"] = utf8_encode($RFP["sinistro"]);
        $records[] = $row;
    }
    //echo $query;exit();
    echo json_encode($records);
}