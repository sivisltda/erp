<?php

if (isset($_POST['isAjax']) || isset($_GET['isAjax'])) {
    include "../../../Connections/configini.php";
}

$imprimir = 0;
$disabled = '';
$id = '';
$nome = '';
$dtCadastro = date("d/m/Y");
$statusAluno = '';
$cpf = '';
$rg = '';
$dtNascimento = '';
$sexo = '';
$estadoCivil = '';
$profissao = '';
$nmEmpresa = '';
$planoSaude = '';
$nmEmergencia = '';
$telEmergencia = '';
$procedencia = '';
$cep = '';
$endereco = '';
$numero = '';
$bairro = '';
$estado = '';
$cidade = '';
$complemento = '';
$user_resp = isset($_SESSION["id_usuario"]) ? $_SESSION["id_usuario"] : null;
$Obs = '';
$acessoTipo = '';
$acessoNumProv = '';
$planoEmpresaFamilia = '';
$LiderplanoEmpresaFamilia = '';
$idLiderplanoEmpresaFamilia = '';
$acessomsg = '';
$profResp = '';
$profRespNome = '';
$emailMarketing = '1';
$celularMarketing = '1';
$inativo = '0';
$dtValCartao = '';
$gobody = '';
$gymPass_id = '';
$prospectIr = '0';
$excecoes_digitais = '';
$clbSocio = '';
$clbTitular = '';
$clbMalaDireta = '';
$clbTitularidade = '';
$ckbTipo = '';
$gradEsp = '';
$nip = '';
$depto = '';
$om = '';
$telOm = '';
$ramal = '';
$localCobranca = '';
$ckbCracha = '';
$validadeCarteira = '';
$ckbTrancar = '0';
$dataTrancamento = '';
$trancarMotivo = '';
$tipoSelect = 'C';
$ckempresa = '';
$juridico_tipo = '';
$gymPassId = '';
$nmFantasia = '';
$regimetributario = '';
$usuario_cadastro = '';
$insMunicipal = '';
$numCnh = '';
$catCnh = '';
$vencCnh = '';
$priCnh = '';
$nfe_iss = '';
$id_pessoa = '';
$totSinistro = '0';

if (isset($_GET['imp']) && is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = 1;
}

if (isset($_POST['btnSerasa'])) {
    odbc_exec($con, "set dateformat dmy;") or die(odbc_errormsg());
    if (is_numeric($_POST['txtId']) && is_numeric($_POST['tipo'])) {
        $tipo = $_POST['tipo'];
        if ($tipo == 2) {
            if (isset($_POST['blockpg'])) {
                $tipo = 'B';
            } else {
                $tipo = 'I';
            }
        } elseif ($tipo == 3) {
            $tipo = 'P';
        } else {
            $tipo = 'R';
        }
        $query = "update sf_fornecedores_despesas set bloqueado = " . $_POST['tipo'] . " where id_fornecedores_despesas = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        odbc_exec($con, "insert into sf_serasa (id_fornecedor,usuario,data_lanc,tipo,observacao)
        values (" . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', GETDATE(),'" . $tipo . "','')");
        echo $_POST['txtId'];
    }
}

if (isset($_POST['btnSave'])) {
    $insert = false;    
    odbc_exec($con, "set dateformat dmy;") or die(odbc_errormsg());
    $cur = odbc_exec($con, "select id_fornecedores_despesas from sf_fornecedores_despesas 
    where len(cnpj) > 0 and cnpj = '" . $_REQUEST['txtCpf'] . "' and id_fornecedores_despesas not in (" . valoresNumericos2($_REQUEST['txtId']) . ")
    and tipo not in ('R','E','F') and inativo = 0");
    while ($RFP = odbc_fetch_array($cur)) {
        if (is_numeric($RFP['id_fornecedores_despesas'])) {
            echo "CPF inválido para cadastro!"; exit;            
        }
    }        
    if (is_numeric($_POST['txtId'])) {        
        $query = "select bloqueado, isnull((select login_user from sf_usuarios where id_usuario = id_user_resp), 'Nenhum') usuario_atual, 
        isnull((select login_user from sf_usuarios where id_usuario = " . valoresSelect('txtUserResp') . "), 'Nenhum') usuario_novo, 
        isnull((select razao_social from sf_fornecedores_despesas as y where y.id_fornecedores_despesas > 0 and y.id_fornecedores_despesas = x.prof_resp), 'Nenhum') prof_atual,
        isnull((select razao_social from sf_fornecedores_despesas as z where z.id_fornecedores_despesas > 0 and z.id_fornecedores_despesas = " . valoresTexto('txtProfResp') . "), 'Nenhum') prof_novo,
        isnull((select descricao_grupo from sf_grupo_cliente where id_grupo = grupo_pessoa), 'Nenhum') grupo_atual,
        isnull((select descricao_grupo from sf_grupo_cliente where id_grupo = " . valoresSelect('txtGrupo') . "), 'Nenhum') grupo_novo            
        from sf_fornecedores_despesas as x where id_fornecedores_despesas = " . $_POST['txtId'];
        $cur = odbc_exec($con, $query);
        while ($RFP = odbc_fetch_array($cur)) {            
            if (utf8_encode($RFP['usuario_atual']) != utf8_encode($RFP['usuario_novo'])) {
                odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
                values ('sf_fornecedores_despesas', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'P', '" . utf8_decode("ALTERAÇÃO DE USUÁRIO RESPONSÁVEL DE ") . $RFP['usuario_atual'] . " para " . $RFP['usuario_novo'] . "', GETDATE(), " . $_POST['txtId'] . ")");
            } 
            if (utf8_encode($RFP['prof_atual']) != utf8_encode($RFP['prof_novo'])) {
                odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
                values ('sf_fornecedores_despesas', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'P', '" . utf8_decode("ALTERAÇÃO DE INDICADOR RESPONSÁVEL DE ") . $RFP['prof_atual'] . " para " . $RFP['prof_novo'] . "', GETDATE(), " . $_POST['txtId'] . ")");
            }
            if ($RFP['bloqueado'] == 0 && valoresCheck('ckbInativo') == 1) {
                odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
                values ('sf_fornecedores_despesas', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'B', '" . utf8_decode("INCLUSÃO DE BLOQUEIO") . "', GETDATE(), " . $_POST['txtId'] . ")");
            }
            if ($RFP['bloqueado'] == 1 && valoresCheck('ckbInativo') == 0) {
                odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
                values ('sf_fornecedores_despesas', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'B', '" . utf8_decode("EXCLUSÃO DE BLOQUEIO") . "', GETDATE(), " . $_POST['txtId'] . ")");
            }
            if (utf8_encode($RFP['grupo_atual']) != utf8_encode($RFP['grupo_novo'])) {
                odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
                values ('sf_fornecedores_despesas', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'G', '" . utf8_decode("ALTERAÇÃO DE GRUPO DE ") . $RFP['grupo_atual'] . " para " . $RFP['grupo_novo'] . "', GETDATE(), " . $_POST['txtId'] . ")");
            } 
        }
    }    
    
    if (is_numeric($_POST['txtId'])) {
        $query = "update sf_fornecedores_despesas set " .
                "razao_social = " . strtoupper(valoresTexto('txtNome')) . "," .
                "cnpj = " . valoresTexto('txtCpf') . "," .
                "inscricao_estadual = " . valoresTexto('txtRG') . "," .
                "endereco = " . valoresTexto('txtEndereco') . "," .
                "numero = " . valoresTexto('txtNumero') . "," .
                "complemento = " . valoresTexto('txtComplemento') . "," .
                "bairro = " . valoresTexto('txtBairro') . "," .
                "org_exped_rg = " . valoresTexto('txtOrg') . "," .
                "estado = " . valoresSelect('txtEstado') . "," .
                "cidade = " . valoresSelect('txtCidade') . "," .
                "grupo_pessoa = " . valoresSelect('txtGrupo') . "," .
                "estrangeiro = " . valoresSelect('txtEstrag') . "," .
                "cep = " . valoresTexto('txtCep') . "," .
                "data_nascimento = " . valoresData('txtDtNasc') . "," .
                "profissao = " . valoresTexto('txtProfissao') . "," .
                "inscricao_municipal = " . valoresTexto('txtInsMunicipal') . "," .                
                "procedencia = " . valoresSelect('txtProcedencia') . "," .
                "trabalha_empresa = " . valoresTexto('txtNmEmpresa') . "," .
                "estado_civil = " . valoresTexto('txtEstadoCivil') . "," .
                "Observacao = " . valoresTexto('txtObs') . "," .
                "telefone_ps1 = " . valoresTexto('txtNmEmergencia') . "," .
                "telefone_ps2 = " . valoresTexto('txtTelEmerg') . "," .
                "sexo = " . valoresTexto('txtSexo') . "," .
                "id_user_resp = " . valoresSelect('txtUserResp') . "," .
                "prof_resp = " . valoresSelect('txtProfResp') . "," .
                "tipo_sang = " . valoresTexto('txtTpSang') . "," .
                "plano_saude = " . valoresTexto('txtPlanoSaude') . "," .
                "empresa = " . valoresTexto('txtFilial') . "," .
                "bloqueado = " . valoresSelect('ckbInativo') . "," .
                "validade_cartao = " . valoresData('txtDtValCartao') . "," .
                "email_marketing = " . (valoresCheck('ckbInativo') == 0 ? valoresCheck('ckbEmailMarketing') : 0) . "," .
                "celular_marketing = " . (valoresCheck('ckbInativo') == 0 ? valoresCheck('ckbCelularMarketing') : 0) . "," .
                "gyp_idaluno = " . valoresTexto('txtGymPassId') . "," .
                "nome_fantasia = " . valoresTexto('txtNmFantasia') . "," . 
                "regime_tributario = " . valoresSelect('txtRegTributario') . "," .
                "juridico_tipo = " . valoresTexto('txtJuridicoTipo') . "," .                
                "num_cnh = " . valoresTexto('txtNumCnh') . "," .
                "cat_cnh = " . valoresTexto('txtCatCnh') . "," .
                "ven_cnh = " . valoresData('txtVencCnh') . "," .
                "nfe_iss = " . valoresCheck('ckbISS') . "," .                
                "pri_cnh = " . valoresData('txtPriCnh') .                 
                " where id_fornecedores_despesas = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        values ('sf_clientes', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'E', 'EDITAR - CLIENTE', GETDATE(), " . $_POST['txtId'] . ")");        
    } else {
        $insert = true;
        $query = "insert into sf_fornecedores_despesas (razao_social,cnpj,inscricao_estadual,org_exped_rg,estrangeiro,endereco,numero,complemento,bairro,estado,cidade,cep,data_nascimento,
                profissao,tipo_sang,procedencia,trabalha_empresa,estado_civil,grupo_pessoa,Observacao,telefone_ps1,telefone_ps2,sexo,id_user_resp,prof_resp,plano_saude,bloqueado,validade_cartao,
                gyp_idaluno,usuario_cadastro,tipo,dt_convert_cliente,empresa,dt_cadastro, email_marketing, celular_marketing, fornecedores_status, acesso_tipo, nome_fantasia, juridico_tipo, 
                regime_tributario, inscricao_municipal, num_cnh, cat_cnh, ven_cnh, pri_cnh, nfe_iss) values (" .
                strtoupper(valoresTexto('txtNome')) . "," .
                valoresTexto('txtCpf') . "," .
                valoresTexto('txtRG') . "," .
                valoresTexto('txtOrg') . "," .
                valoresTexto('txtEstrag') . "," .
                valoresTexto('txtEndereco') . "," .
                valoresTexto('txtNumero') . "," .
                valoresTexto('txtComplemento') . "," .
                valoresTexto('txtBairro') . "," .
                valoresSelect('txtEstado') . "," .
                valoresSelect('txtCidade') . "," .
                valoresTexto('txtCep') . "," .
                valoresData('txtDtNasc') . "," .
                valoresTexto('txtProfissao') . "," .
                valoresTexto('txtTpSang') . "," .
                valoresSelect('txtProcedencia') . "," .
                valoresTexto('txtNmEmpresa') . "," .
                valoresTexto('txtEstadoCivil') . "," .
                valoresSelect('txtGrupo') . "," .
                valoresTexto('txtObs') . "," .
                valoresTexto('txtNmEmergencia') . "," .
                valoresTexto('txtTelEmerg') . "," .
                valoresTexto('txtSexo') . "," .
                valoresSelect('txtUserResp') . "," .
                valoresSelect('txtProfResp') . "," .
                valoresTexto('txtPlanoSaude') . "," .
                valoresCheck('ckbInativo') . "," .
                valoresData('txtDtValCartao') . "," .
                valoresTexto('txtGymPassId') . "," .
                $_SESSION["id_usuario"] . "," .
                "'C', getdate()," . valoresTexto('txtFilial') . "," . 
                valoresData('txtDtCad') . "," .
                (valoresCheck('ckbInativo') == 0 ? valoresCheck('ckbEmailMarketing') : 0) . "," .
                (valoresCheck('ckbInativo') == 0 ? valoresCheck('ckbCelularMarketing') : 0) . "," .
                "'Inativo', 1," .
                valoresTexto('txtNmFantasia') . "," . 
                valoresTexto('txtJuridicoTipo') . "," .
                valoresSelect('txtRegTributario') . "," .                
                valoresTexto('txtInsMunicipal') . "," .                
                valoresTexto('txtNumCnh') . "," .
                valoresTexto('txtCatCnh') . "," .
                valoresData('txtVencCnh') . "," .
                valoresData('txtPriCnh') . "," .
                valoresCheck('ckbISS') . ")";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_fornecedores_despesas from sf_fornecedores_despesas order by id_fornecedores_despesas desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        values ('sf_clientes', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'I', 'INCLUIR - CLIENTE', GETDATE(), " . $_POST['txtId'] . ")");                
        $cur = odbc_exec($con, "select * from sf_configuracao");
        while ($RFP = odbc_fetch_array($cur)) {
            if (is_numeric($RFP['ACA_DOC_NOVO_ALU'])) {
                odbc_exec($con, "insert into sf_fornecedores_despesas_documentos(id_fornecedor_despesas, id_documento, data_documento)
                values(" . $_POST['txtId'] . "," . $RFP['ACA_DOC_NOVO_ALU'] . "," . valoresData2(getData("T")) . ")");
            }
        }
    }
    if (isset($_POST['txtQtdContatos'])) {
        $max = $_POST['txtQtdContatos'];
        $notIn = '0';
        for ($i = 0; $i <= $max; $i++) {
            if (isset($_POST['txtIdContato_' . $i]) && is_numeric($_POST['txtIdContato_' . $i])) {
                if ($_POST['txtIdContato_' . $i] != 0) {
                    $notIn = $notIn . "," . $_POST['txtIdContato_' . $i];
                }
            }
        }
        odbc_exec($con, "delete from sf_fornecedores_despesas_contatos where fornecedores_despesas = " . $_POST['txtId'] . " and id_contatos not in (" . $notIn . ")");
        for ($i = 0; $i <= $max; $i++) {
            if (isset($_POST['txtIdContato_' . $i])) {
                if ($_POST['txtIdContato_' . $i] !== "0") {
                    
                } else {
                    odbc_exec($con, "insert into sf_fornecedores_despesas_contatos(fornecedores_despesas,tipo_contato,conteudo_contato,resp_contato) values(" .
                                    $_POST['txtId'] . "," . $_POST['txtTpContato_' . $i] . "," . valoresTexto('txtTxContato_' . $i) . "," . valoresTexto('txtTxResponsavel_' . $i) . ")") or die(odbc_errormsg());
                }
            }
        }
        echo $_POST['txtId'];
    }
    if ($insert) {
        $id_padrao = "21";        
        include "../../Sivis/ws_disparo_imediato.php";        
    }
}

if (isset($_POST['ckbClbSocio']) && is_numeric($_POST['txtId'])) {
    $query = "UPDATE sf_fornecedores_despesas SET " .
            "socio = " . valoresSelect("ckbClbSocio") . "," .
            "titular = " . valoresSelect("ckbClbTitular") . "," .
            "mala_direta = " . valoresSelect("ckbClbMalaDireta") . "," .
            "titularidade = " . valoresSelect("txtTitularidade") . "," .
            "tipo_socio = " . valoresSelect("ckbTipo") . "," .
            "gran_esp = " . valoresTexto("txtGradEsp") . "," .
            "nip = " . valoresTexto("txtNip") . "," .
            "depto = " . valoresTexto("txtDepto") . "," .
            "om = " . valoresTexto("txtOm") . "," .
            "tel_om = " . valoresTexto("txtTelOm") . "," .
            "ramal = " . valoresTexto("txtRamal") . "," .
            "local_cobranca = " . valoresTexto("txtLocalCobranca") . "," .
            "status_cracha = " . valoresSelect("ckbCracha") . "," .
            "validadeCarteira = " . valoresData("txtValidadeCarteira") . "," .
            "trancar = " . valoresSelect("ckbTrancar") . "," .
            "trancarData = " . valoresData("txtDataTrancamento") . "," .            
            "trancarMotivo = " . valoresTexto("txtMotivoTrancamento") . "," .
            "funcao_contato = " . valoresTexto("txtContFunc") . " " .
            " WHERE id_fornecedores_despesas = " . $_POST['txtId'] . ";";
    odbc_exec($con, $query) or die(odbc_errormsg());
    $query_dependente = "update sf_fornecedores_despesas set trancar = " . valoresSelect("ckbTrancar") . "
    where id_fornecedores_despesas in (select id_dependente from sf_fornecedores_despesas_dependentes 
    where id_titular = " . $_POST['txtId'] . ")";
    odbc_exec($con, $query_dependente) or die(odbc_errormsg());  
    
    if (valoresSelect("ckbTrancar") != valoresSelect("ckbTrancarOld")) {
        odbc_exec($con, "set dateformat dmy; insert into sf_trancamento (fornecedores_despesas, tipo, data, usuario, data_trancamento, trancarMotivo)
        values (" . $_POST['txtId'] . ", " . valoresSelect("ckbTrancar") . ", GETDATE(), '" . $_SESSION["login_usuario"] . "', " . 
        valoresData("txtDataTrancamento") . ", " . valoresTexto("txtMotivoTrancamento") . ")");

        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        values ('sf_fornecedores_despesas', null, '" . $_SESSION["login_usuario"] . "', 'T', " . 
        valoresTexto2((valoresSelect("ckbTrancar") == 1 ? "Incluir" : "Cancelar") . " - " . $_POST['txtMotivoTrancamento']) . ", GETDATE(), " . $_POST['txtId'] . ")");            
        if (valoresSelect("ckbTrancar") == 1) {
            odbc_exec($con, "update sf_fornecedores_despesas set email_marketing = 0, celular_marketing = 0 where id_fornecedores_despesas = " . $_POST['txtId']);                                        
            odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
            select 'sf_agendamento', id_agendamento, '" . $_SESSION["login_usuario"] . "', 'E', 'EXCLUIR - AGENDAMENTO POR TRANCAMENTO', GETDATE(), id_fornecedores
            from sf_agendamento where id_fornecedores = " . $_POST['txtId'] . " and inativo = 0 and cast(getdate() as date) < data_inicio");                
            $query = "UPDATE sf_agendamento SET inativo = 1 WHERE id_fornecedores = " . $_POST['txtId'] . " and inativo = 0 and cast(getdate() as date) < data_inicio";
            odbc_exec($con, $query) or die(odbc_errormsg());
        }
    }
}

if (isset($_POST['btnGobody'])) {
    if (is_numeric($_POST['idGobody']) && is_numeric($_POST['txtId'])) {
        odbc_exec($con, "set dateformat dmy;") or die(odbc_errormsg());
        $query = "update sf_fornecedores_despesas set id_gobody = " . $_POST['idGobody'] . " where id_fornecedores_despesas = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        echo "YES";
    }
}

if (isset($_POST['SalvarAcesso'])) {
    if (is_numeric($_POST['txtId'])) {
        $NumProvisorio = preg_replace('/[^0-9]/', '', $_POST['txtAcNumProvisorio']);
        if (valoresCheck('rAcTipo') == 0) {
            if (strlen($NumProvisorio) == 0) {
                echo "Erro, Preencha o campo Número Provisório ";
                exit;
            }
            $query = "select id_fornecedores_despesas from sf_fornecedores_despesas 
            where acesso_numero_provisorio = '" . $NumProvisorio . "' " .
            "and id_fornecedores_despesas not in ('" . $_POST['txtId'] . "')";
            $cur = odbc_exec($con, $query);
            while ($RFP = odbc_fetch_array($cur)) {
                echo "Erro, Provisório está ativo para matrícula " . $RFP['id_fornecedores_despesas'];
                exit;
            }
        }
        odbc_exec($con, "set dateformat dmy;") or die(odbc_errormsg());
        $query = "update sf_fornecedores_despesas set " .
                "acesso_tipo = " . valoresCheck('rAcTipo') . "," .
                "excecoes_digitais = " . valoresCheck('ckbEmailMarketing') . "," .
                "acesso_numero_provisorio = '" . $NumProvisorio . "'," .
                "acesso_msg = " . valoresTextoUpper('txtAcMsg') .
                " where id_fornecedores_despesas = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        echo "YES";
    }
}

if (isset($_POST['btnExcluir'])) {
    if (is_numeric($_POST['txtId'])) {
        $delTitularDependente = "";
        $cur = odbc_exec($con, "select * from sf_configuracao");
        while ($RFP = odbc_fetch_array($cur)) {
            if ($RFP['ACA_DEL_TITU_DEP'] > 0) {
                $delTitularDependente = " or id_fornecedores_despesas in (select id_dependente from sf_fornecedores_despesas_dependentes where id_titular = " . $_POST['txtId'] . ")";
            }
        }
        odbc_exec($con, "UPDATE sf_fornecedores_despesas set inativo = 1 where id_fornecedores_despesas = " . $_POST['txtId'] . $delTitularDependente);
        odbc_exec($con, "UPDATE sf_fornecedores_despesas set fornecedores_status = dbo.FU_STATUS_CLI(" . $_POST['txtId'] . ") where id_fornecedores_despesas = " . $_POST['txtId'] . $delTitularDependente);
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        values ('sf_clientes', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'E', 'EXCLUIR - CLIENTE', GETDATE(), " . $_POST['txtId'] . ")");
        echo $_POST['txtId'];
    }
}

if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    $idTitular = "";
    $idDependente = "";
    $itensTitular = "";
    $nmTitular = "";
    $ckempresa = "";
    $disabled = 'disabled';
    
    $query = "select id_titular from sf_fornecedores_despesas_dependentes where id_dependente = " . $_GET["id"];
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $idTitular = $RFP['id_titular'];
    }
    
    $query = "select max(id_dependente) id_dependente from sf_fornecedores_despesas_dependentes where id_titular = " . $_GET["id"];
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $idDependente = $RFP['id_dependente'];
    }
    
    if ($idTitular !== "") {
        $itensTitular = ", C.razao_social nmTitular";
        $queryTitular = " inner join sf_fornecedores_despesas C on C.id_fornecedores_despesas = " . $idTitular;
    }
    $cur = odbc_exec($con, "select A.*, dbo.FU_STATUS_CLI(A.id_fornecedores_despesas) statusAluno " . $itensTitular . ",A.id_gobody,
    (select razao_social from sf_fornecedores_despesas as y where y.id_fornecedores_despesas = A.prof_resp) prof_atual,
    (select COUNT(*) total from sf_telemarketing where atendimento_servico = 2 and (pessoa = A.id_fornecedores_despesas or proprietario = A.id_fornecedores_despesas)) tot_sinistro
    from sf_fornecedores_despesas A " . $queryTitular . "
    where A.inativo = 0 and A.id_fornecedores_despesas = " . $_GET['id']) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        if ($idTitular !== "") {
            $nmTitular = utf8_encode($RFP['nmTitular']);
        }
        $id = utf8_encode($RFP['id_fornecedores_despesas']);
        $nome = utf8_encode(strtoupper($RFP['razao_social']));
        $cpf = utf8_encode($RFP['cnpj']);
        $rg = utf8_encode($RFP['inscricao_estadual']);
        $dtCadastro = escreverData($RFP['dt_cadastro']);
        $statusAluno = utf8_encode($RFP['statusAluno']);
        $dtNascimento = escreverData($RFP['data_nascimento']);
        $sexo = utf8_encode($RFP['sexo']);
        $orgExp = utf8_encode($RFP['org_exped_rg']);
        $planoSaude = utf8_encode($RFP['plano_saude']);
        $estadoCivil = utf8_encode($RFP['estado_civil']);
        $profissao = utf8_encode($RFP['profissao']);
        $nmEmpresa = utf8_encode($RFP['trabalha_empresa']);
        $nmEmergencia = utf8_encode($RFP['telefone_ps1']);
        $telEmergencia = utf8_encode($RFP['telefone_ps2']);
        $procedencia = utf8_encode($RFP['procedencia']);
        $estrang = utf8_encode($RFP['estrangeiro']);
        $cep = utf8_encode($RFP['cep']);
        $endereco = utf8_encode($RFP['endereco']);
        $numero = utf8_encode($RFP['numero']);
        $complemento = utf8_encode($RFP['complemento']);
        $grupoPessoa = utf8_encode($RFP['grupo_pessoa']);
        $bairro = strtoupper(utf8_encode($RFP['bairro']));
        $estado = utf8_encode($RFP['estado']);
        $cidade = utf8_encode($RFP['cidade']);
        $user_resp = $RFP['id_user_resp'] == "" ? $_SESSION["id_usuario"] : strtoupper(utf8_encode($RFP['id_user_resp']));
        $profResp = utf8_encode($RFP['prof_resp']);
        $profRespNome = utf8_encode($RFP['prof_atual']);
        $Obs = utf8_encode($RFP['Observacao']);
        $acessoTipo = utf8_encode($RFP['acesso_tipo']);
        $acessoNumProv = utf8_encode($RFP['acesso_numero_provisorio']);
        $acessomsg = utf8_encode($RFP['acesso_msg']);
        $emailMarketing = utf8_encode($RFP['email_marketing']);
        $celularMarketing = utf8_encode($RFP['celular_marketing']);
        $inativo = utf8_encode($RFP['bloqueado']);
        $dtValCartao = escreverData($RFP['validade_cartao']);
        $gobody = utf8_encode($RFP['id_gobody']);
        $excecoes_digitais = utf8_encode($RFP['excecoes_digitais']);
        $tipoCli = utf8_encode($RFP['tipo']);
        $clbSocio = utf8_encode($RFP['socio']);
        $clbTitular = utf8_encode($RFP['titular']);
        $clbMalaDireta = utf8_encode($RFP['mala_direta']);
        $clbTitularidade = utf8_encode($RFP['titularidade']);
        $ckbTipo = utf8_encode($RFP['tipo_socio']);
        $gradEsp = utf8_encode($RFP['gran_esp']);
        $nip = utf8_encode($RFP['nip']);
        $depto = utf8_encode($RFP['depto']);
        $om = utf8_encode($RFP['om']);
        $telOm = utf8_encode($RFP['tel_om']);
        $contFunc = utf8_encode($RFP['funcao_contato']);
        $ramal = utf8_encode($RFP['ramal']);
        $tpsang = utf8_encode($RFP['tipo_sang']);
        $localCobranca = utf8_encode($RFP['local_cobranca']);
        $ckbCracha = utf8_encode($RFP['status_cracha']);
        $validadeCarteira = escreverData($RFP['validadeCarteira']);
        $ckbTrancar = utf8_encode($RFP['trancar']);
        $dataTrancamento = escreverData($RFP['trancarData']);        
        $trancarMotivo = utf8_encode($RFP['trancarMotivo']);
        $ckempresa = utf8_encode($RFP['empresa']);
        $juridico_tipo = utf8_encode($RFP['juridico_tipo']);
        $gymPassId = utf8_encode($RFP['gyp_idaluno']);
        $nmFantasia = utf8_encode($RFP['nome_fantasia']);
        $regimetributario = utf8_encode($RFP['regime_tributario']);   
        $usuario_cadastro = utf8_encode($RFP['login_user']);        
        $insMunicipal = utf8_encode($RFP['inscricao_municipal']);
        $numCnh = utf8_encode($RFP['num_cnh']);
        $catCnh = utf8_encode($RFP['cat_cnh']);
        $vencCnh = escreverData($RFP['ven_cnh']);
        $priCnh = escreverData($RFP['pri_cnh']);
        $nfe_iss = $RFP['nfe_iss'];
        $id_pessoa = utf8_encode($RFP['id_fornecedores_despesas']);
        $totSinistro = utf8_encode($RFP['tot_sinistro']);        
    }
    if ($tipoCli == "P" && substr($_SESSION["modulos"], 6, 1) == 1) {
        header('Location: ../../Modulos/CRM/ProspectsForm.php?id=' . $id);
    }
    $cur = odbc_exec($con, "select * from sf_fornecedores_despesas_convenios
    inner join sf_convenios on id_convenio = sf_fornecedores_despesas_convenios.convenio
    inner join sf_fornecedores_despesas on id_fornecedores_despesas = id_lider
    where sf_convenios.tipo = 0 and status = 0 and id_fornecedor = " . $_GET['id']);
    while ($RFP = odbc_fetch_array($cur)) {
        $planoEmpresaFamilia = utf8_encode($RFP['convenio']);
        $idLiderplanoEmpresaFamilia = utf8_encode($RFP['id_fornecedores_despesas']);
        $LiderplanoEmpresaFamilia = utf8_encode($RFP['razao_social']);
    }
    $cur = odbc_exec($con, "select gyp_recid, gyp_ptype, gyp_token, cat_topdata_lista, ACA_PROSP_CLI from sf_configuracao where id = 1");
    while ($RFP = odbc_fetch_array($cur)) {
        $gymPass_id = $RFP['gyp_recid'];
        $prospectIr = $RFP['ACA_PROSP_CLI'];
        $cat_topdata_lista = $RFP['cat_topdata_lista'];        
    }
    if (strlen($statusAluno) > 0) {
        odbc_exec($con, "update sf_fornecedores_despesas set fornecedores_status = " .
                valoresTexto2($statusAluno) . " where id_fornecedores_despesas = " . $_GET['id']);
    }
    odbc_exec($con, "update sf_vendas_planos set dt_inicio = dbo.FU_PLANO_INI(id_plano), dt_fim = dbo.FU_PLANO_FIM(id_plano) where favorecido = " . $_GET['id']);
    odbc_exec($con, "update sf_fornecedores_despesas set tipo = 'C' where tipo = 'P' and id_fornecedores_despesas in (select id_dependente from sf_fornecedores_despesas_dependentes where id_titular = " . $_GET['id'] . ")");
    if (!is_numeric($ckempresa)) {
        $cur = odbc_exec($con, "select filial from sf_usuarios where id_usuario = '" . $_SESSION["id_usuario"] . "'");
        while ($RFP = odbc_fetch_array($cur)) {
            $ckempresa = $RFP['filial'];
        }
        odbc_exec($con, "update sf_fornecedores_despesas set empresa = " . (is_numeric($ckempresa) ? $ckempresa : "1") .
                " where len(isnull(empresa,'')) = 0 and id_fornecedores_despesas = " . $_GET['id']);
    }
    $query = "select count(id_planos_ferias) total from sf_vendas_planos_ferias f inner join sf_vendas_planos p on p.id_plano = f.id_plano
    where f.dt_cancelamento is null and p.dt_cancelamento is null and GETDATE() between f.dt_inicio and f.dt_fim and favorecido = '" . $_GET['id'] . "'";
    $cur = odbc_exec($con, $query);
    while ($aRow = odbc_fetch_array($cur)) {
        $statusAluno = $aRow["total"] > 0 ? "Ferias" : $statusAluno;
    }
}

if (isset($_POST['img'])) {
    define('UPLOAD_DIR', '../../../Pessoas/' . $contrato . "/Clientes/");
    $img = $_POST['img'];
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = UPLOAD_DIR . $_POST['txtId'] . '.png';
    $success = file_put_contents($file, $data);
    echo $success ? "YES" : 'NO';
}

if (isset($_POST['RemoverImg']) && is_numeric($_POST['RemoverImg'])) {
    $diretorio = '../../../Pessoas/' . $contrato . "/Clientes/" . $_POST['RemoverImg'] . ".png";
    if (file_exists($diretorio)) {
        unlink($diretorio);
    }
    echo "YES";
}

if (isset($_GET['existeCPF'])) {
    require_once(__DIR__ . '/../../../Connections/configini.php');
    $inat = 0;
    $isCPF = "";
    $pessoa = "";
    $cur = odbc_exec($con, "select cnpj, inativo, id_fornecedores_despesas, razao_social from sf_fornecedores_despesas 
    where cnpj = '" . $_GET['txtCPF'] . "' and id_fornecedores_despesas not in (" . valoresNumericos2($_GET['txtId']) . ") and tipo in ('C','P')");
    while ($RFP = odbc_fetch_array($cur)) {
        $inat = $RFP['inativo'];
        $isCPF = $RFP['cnpj'];
        $pessoa = utf8_encode($RFP["id_fornecedores_despesas"]) . " - " . utf8_encode($RFP["razao_social"]);
    }
    if ($isCPF !== "") {
        if ($inat === '0') {
            echo 'ATIVO|' . $pessoa;
        } else {
            echo 'INATIVO|' . $pessoa;
        }
    } else {
        echo 'NO|' . $pessoa;
    }
}

if (isset($_POST['reativarCadastro'])) {
    if (isset($_POST['txtCPF'])) {
        $cur = odbc_exec($con, "select id_fornecedores_despesas from sf_fornecedores_despesas where cnpj = '" . $_POST['txtCPF'] . "' and inativo = 1");
        while ($RFP = odbc_fetch_array($cur)) {
            $idCliente = $RFP['id_fornecedores_despesas'];
        }
    } else {
        $idCliente = $_POST['idCli'];
    }
    $reatTitularDependente = "";
    $cur = odbc_exec($con, "select * from sf_configuracao");
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['ACA_DEL_TITU_DEP'] > 0) {
            $reatTitularDependente = " or id_fornecedores_despesas in (select id_dependente from sf_fornecedores_despesas_dependentes where id_titular = " . $idCliente . ")";
        }
    }
    odbc_exec($con, "UPDATE sf_fornecedores_despesas set inativo = 0 where id_fornecedores_despesas = " . $idCliente . $reatTitularDependente);
    odbc_exec($con, "UPDATE sf_fornecedores_despesas set fornecedores_status = case when tipo = 'C' then dbo.FU_STATUS_CLI(" . $idCliente . ") else dbo.FU_STATUS_PRO(" . $idCliente . ") end where id_fornecedores_despesas = " . $idCliente . $reatTitularDependente);
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    values ('sf_clientes', " . $idCliente . ", '" . $_SESSION["login_usuario"] . "', 'E', 'EDITAR - REATIVAR CLIENTE', GETDATE(), " . $idCliente . ")");
    echo $idCliente;
}

if (isset($_GET['verificaConvenio'])) {
    if (isset($_GET['isAjax']) && $_GET['isAjax']) {
        require_once(__DIR__ . '/../../../Connections/configini.php');
    }
    if (isset($_GET["hostname"]) && isset($_GET["database"]) && isset($_GET["username"]) && isset($_GET["password"])) {
        $hostname = $_GET["hostname"];
        $database = $_GET["database"];
        $username = $_GET["username"];
        $password = $_GET["password"];
        $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
        require_once(__DIR__ . '/../../../Connections/funcoesAux.php');
    }    
    $valorProduto = $_GET['valorProduto'];
    $totalDesconto = 0;    
    $diasMens = 0;
    $id_pessoa = 0;
    $desconto_decimal = 2;
    
    $cur = odbc_exec($con, "select desconto_decimal from sf_configuracao where id = 1");
    while ($RFP = odbc_fetch_array($cur)) {
        $desconto_decimal = $RFP['desconto_decimal'];
    }
    
    $cur = odbc_exec($con, "select datediff(day, getdate(),dt_inicio_mens) dias, favorecido from sf_vendas_planos_mensalidade x1 
    inner join sf_vendas_planos x2 on x1.id_plano_mens = x2.id_plano where id_mens = " . valoresNumericos2($_GET['idPlano']));
    while ($RFP = odbc_fetch_array($cur)) {
        $diasMens = $RFP['dias'];
        $id_pessoa = $RFP['favorecido'];
    }
    if (valoresNumericos2($_GET['verificaConvenio']) != $id_pessoa) {
        $cur = odbc_exec($con, "select id_titular from sf_fornecedores_despesas_dependentes 
        where id_titular = " . valoresNumericos2($_GET['verificaConvenio']) . " and id_dependente = " . $id_pessoa . " and compartilhado = 1");
        while ($RFP = odbc_fetch_array($cur)) {
            $id_pessoa = $RFP['id_titular'];
        }
    }
    $query = "select tp_valor, valor, ate_vencimento from sf_convenios_regras A INNER JOIN sf_convenios B ON A.convenio = B.id_convenio
    INNER JOIN sf_configuracao_convenios C ON C.id_convenio = A.convenio
    where B.status = 0 and B.tipo = 1 and B.convenio_especifico = 1 and (select count(distinct id_prod_plano) total from sf_convenios_planos
    inner join sf_vendas_planos on id_prod_plano = id_prod_convenio where id_convenio_planos = B.id_convenio and dt_cancelamento is null 
    and favorecido in (select favorecido from sf_vendas_planos_mensalidade x1 inner join sf_vendas_planos x2 on x1.id_plano_mens = x2.id_plano
    where id_mens = " . valoresNumericos2($_GET['idPlano']) . ") and planos_status in ('NOVO', 'ATIVO')) 
    between min_dep and max_dep and B.id_convenio in (select id_convenio_planos from sf_convenios_planos 
    where id_prod_convenio = " . valoresNumericos2($_GET['idProd']) . ")";
    //echo $query; exit;
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        if (!($RFP['ate_vencimento'] == '1' && $diasMens < 0)) {
            if ($RFP['tp_valor'] == 'P') {
                $totalDesconto = $totalDesconto + ($valorProduto * $RFP['valor'] / 100);
            } else {
                $totalDesconto = $totalDesconto + $RFP['valor'];
            }
        }
    }    
    $query = "SELECT * FROM sf_convenios_regras INNER JOIN sf_convenios ON sf_convenios_regras.convenio = sf_convenios.id_convenio        
    WHERE id_regra IN(SELECT CASE WHEN B.tipo = 1 THEN
    (SELECT id_regra FROM sf_convenios_regras WHERE convenio = B.id_convenio) ELSE
    (SELECT TOP 1 id_regra FROM sf_convenios_regras
    WHERE numero_minimo <=(
        select count(distinct fc.id_fornecedor)
        from sf_fornecedores_despesas_convenios fc
        left join sf_convenios_planos cc on fc.convenio = cc.id_convenio_planos
        left join sf_vendas_planos vp on cc.id_prod_convenio = vp.id_prod_plano and fc.id_fornecedor = vp.favorecido
        where ((cc.id_prod_convenio = " . valoresNumericos2($_GET['idProd']) . " and B.convenio_especifico = 1) or B.convenio_especifico = 0)
        and fc.id_lider = A.id_lider and fc.convenio = B.id_convenio
        and ((planos_status in ('NOVO', 'ATIVO') and ativo = 1) or ativo = 0)
    ) AND convenio = B.id_convenio ORDER BY numero_minimo DESC) END
    valor FROM sf_fornecedores_despesas_convenios A
    INNER JOIN sf_convenios B ON A.convenio = B.id_convenio
    left join sf_convenios_planos C on B.id_convenio = C.id_convenio_planos
    WHERE id_fornecedor = " . (!is_numeric($_GET['idPlano']) ? valoresNumericos2($_GET['verificaConvenio']) : $id_pessoa) . 
    " and ((C.id_prod_convenio = " . valoresNumericos2($_GET['idProd']) . " and B.convenio_especifico = 1) or B.convenio_especifico = 0) 
    and B.status = 0 and (B.id_conv_plano is null or id_conv_plano = " . valoresNumericos2($_GET['idConvProd']) . ")
    AND (GETDATE() BETWEEN dt_inicio AND dt_fim OR (dt_inicio IS NULL AND dt_fim IS NULL)))";
    //echo $query; exit;
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        if (!($RFP['ate_vencimento'] == '1' && $diasMens < 0)) {
            if ($RFP['tp_valor'] === 'P') {
                $totalDesconto = $totalDesconto + ($valorProduto * $RFP['valor'] / 100);
            } else {
                $totalDesconto = $totalDesconto + $RFP['valor'];
            }
        }
    }
    echo escreverNumero($totalDesconto, 0, $desconto_decimal);
}

if (isset($_GET['listProfessor'])) {
    $query = "select id_fornecedores_despesas,razao_social from sf_fornecedores_despesas
    where tipo = 'E' and dt_demissao is null order by razao_social";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["id_fornecedores_despesas"] = utf8_encode($RFP["id_fornecedores_despesas"]);
        $row["razao_social"] = utf8_encode($RFP["razao_social"]);
        $row["cor"] = getCollor($i);
        $records[] = $row;
    }
    echo json_encode($records);
}

if (isset($_GET['listAmbiente'])) {
    $query = "select id_ambiente, nome_ambiente from sf_ambientes order by nome_ambiente";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["id_ambiente"] = utf8_encode($RFP["id_ambiente"]);
        $row["nome_ambiente"] = utf8_encode($RFP["nome_ambiente"]);
        $records[] = $row;
    }
    echo json_encode($records);
}

if (isset($_GET['verificaTaxaAdesao'])) {
    $id_servico = 0;
    $cur = odbc_exec($con, "select top 1 id_servico from sf_vendas_planos_mensalidade m
    inner join sf_vendas_planos vp on vp.id_plano = m.id_plano_mens
    inner join sf_produtos_acrescimo_servicos pa on pa.id_produto = vp.id_prod_plano
    and (select count(id_dependente) from sf_fornecedores_despesas_dependentes where id_titular = favorecido) between qtd_min and qtd_max
    where id_mens = " . valoresNumericos2($_GET['verificaTaxaAdesao']) . " and (select count(m1.id_mens) from sf_vendas_planos_mensalidade m1 where m1.dt_pagamento_mens is not null and m1.id_plano_mens = vp.id_plano) = 0 order by qtd_max desc");
    while ($RFP = odbc_fetch_array($cur)) {
        $id_servico = $RFP['id_servico'];
    }
    echo $id_servico;
}

if (isset($_GET['verificaMulta'])) {
    $total = 0;
    $valor = $_GET['valMens'];
    $cur = odbc_exec($con, "set dateformat dmy;
    select ACA_PLN_DIAS_TOLERANCIA, aca_tipo_multa, aca_forma_multa, aca_taxa_multa, aca_taxa_multa_dias,
    aca_mora_multa, aca_tolerancia_multa, datediff(day, " . valoresData2($_GET['dtMens']) . ", GETDATE()) dias 
    from sf_configuracao where id = 1 ");
    while ($RFP = odbc_fetch_array($cur)) {
        $mul_tipo = $RFP['aca_tipo_multa'];
        if ($RFP['aca_forma_multa'] == 0) {
            $mul_taxa = ($RFP['aca_taxa_multa_dias'] <= $RFP['dias'] ? (($valor * $RFP['aca_taxa_multa']) / 100) : 0);
            $mul_mora = (($valor * $RFP['aca_mora_multa']) / 100);
        } else {
            $mul_taxa = ($RFP['aca_taxa_multa_dias'] <= $RFP['dias'] ? $RFP['aca_taxa_multa'] : 0);
            $mul_mora = $RFP['aca_mora_multa'];
        }
        if ($RFP['aca_tolerancia_multa'] > 0) {
            $dias = $RFP['dias'] - $RFP['aca_tolerancia_multa'];
        } else {
            $dias = $RFP['dias'];
        }
    }
    $cur = odbc_exec($con, "set dateformat dmy;
    select dbo.CALC_DIA_UTIL(" . valoresData2($_GET['dtMens']) . ") total;");
    while ($RFP = odbc_fetch_array($cur)) {
        $dias = $dias - $RFP['total'];
    }
    if ($dias > 0) {
        if ($mul_tipo == 1) {
            $total = ($mul_mora * $dias);
        } else if ($mul_tipo == 2) {
            $total = (($mul_mora * $dias) + $mul_taxa);
        } else if ($mul_tipo == 3) {
            $total = $mul_taxa;
        }
    }
    echo escreverNumero($total);
}

if (isset($_GET['verificaAdesao'])) {
    $total = 0;
    $produto = 0;
    if (isset($_GET['isAjax']) && $_GET['isAjax']) {
        require_once(__DIR__ . '/../../../Connections/configini.php');
    }
    if (isset($_GET["hostname"]) && isset($_GET["database"]) && isset($_GET["username"]) && isset($_GET["password"])) {
        $hostname = $_GET["hostname"];
        $database = $_GET["database"];
        $username = $_GET["username"];
        $password = $_GET["password"];
        $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
        require_once(__DIR__ . '/../../../Connections/funcoesAux.php');
    }    
    $query = "select conta_produto, preco_venda from sf_produtos where inativa = 0 and conta_produto in 
    (select conta_produto_adesao from sf_produtos where conta_produto = " . $_GET['idProd'] . ")";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $produto = $RFP['conta_produto'];        
        $total = $RFP['preco_venda'];
    }        
    $query = "select tp_valor, valor from sf_fornecedores_despesas_convenios fc
    inner join sf_convenios_planos cp on fc.convenio = cp.id_convenio_planos
    inner join sf_convenios_regras cr on fc.convenio = cr.convenio
    where id_fornecedor = " . $_GET['verificaAdesao'] . " and id_prod_convenio = " . $produto . "
    and getdate() between dt_inicio and dt_fim";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $total = $total - ($RFP['tp_valor'] == "D" ? $RFP['valor'] : (($RFP['valor'] * $total)/100));
    }    
    echo escreverNumero($total);
}

if (isset($_GET['verificaAcrescimo'])) {
    $idProd = $_GET['idProd'];
    $totalAcrescimo = 0;
    $favorecido = $_GET['verificaAcrescimo'];
    $servico_adesao = 0;    

    if (isset($_GET['isAjax']) && $_GET['isAjax']) {
        require_once(__DIR__ . '/../../../Connections/configini.php');
    }
    if (isset($_GET["hostname"]) && isset($_GET["database"]) && isset($_GET["username"]) && isset($_GET["password"])) {
        $hostname = $_GET["hostname"];
        $database = $_GET["database"];
        $username = $_GET["username"];
        $password = $_GET["password"];
        $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password) or die(odbc_errormsg());
        require_once(__DIR__ . '/../../../Connections/funcoesAux.php');
    }
    $cur = odbc_exec($con, "select ACA_SERVICO_ADESAO from sf_configuracao");
    while ($RFP = odbc_fetch_array($cur)) {
        $servico_adesao = $RFP['ACA_SERVICO_ADESAO'];
    }    
    $query = "select p.conta_produto, p.descricao, 
    case when p.tp_preco = 1 then (px.segMax * p.preco_venda)/100 
    when p.tp_preco = 2 then (v.valor * p.preco_venda)/100 else p.preco_venda end preco_venda
    from sf_vendas_planos vp inner join sf_vendas_planos_acessorios vpa on vp.id_plano = vpa.id_venda_plano
    inner join sf_fornecedores_despesas_veiculo v on v.id = vp.id_veiculo
    inner join sf_produtos p on p.conta_produto = vpa.id_servico_acessorio
    inner join sf_produtos px on px.conta_produto = vp.id_prod_plano
    WHERE p.tipo='A' AND vp.id_plano = " . $idProd . " AND vp.favorecido = " . $favorecido;    
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $totalAcrescimo = $totalAcrescimo + $RFP['preco_venda'];
    }
    if ($servico_adesao > 0) {
        $query = " select preco_venda from sf_produtos where tipo = 'S' and inativa = 0 and conta_produto in (
        select top 1 id_servico from sf_vendas_planos vp inner join sf_produtos_acrescimo_servicos pa on pa.id_produto = vp.id_prod_plano
        and (select count(id_dependente) from sf_fornecedores_despesas_dependentes where id_titular = favorecido) between qtd_min and qtd_max
        inner join sf_produtos on conta_produto = vp.id_prod_plano where favorecido = " . $favorecido . " and id_plano = " . $idProd . " 
        and adesao_tipo = 1 and adesao_intervalo = month(getdate()) order by qtd_max desc)";
        $cur = odbc_exec($con, $query);
        while ($RFP = odbc_fetch_array($cur)) {
            $totalAcrescimo = $totalAcrescimo + $RFP['preco_venda'];
        }
    }
    $query = "select ISNULL(sum(x.qtd_total * valor), 0) acrecimo from (
    select id, count(id) qtd_total from sf_vendas_planos
    inner join sf_fornecedores_despesas_dependentes on favorecido = id_titular
    inner join sf_fornecedores_despesas on id_fornecedores_despesas = id_dependente
    left join sf_produtos_acrescimo on id_produto = id_prod_plano and
    ((datediff(year,data_nascimento,getdate()) between idade_min and idade_max_m and sexo = 'M') or
    (datediff(year,data_nascimento,getdate()) between idade_min and idade_max_f and sexo = 'F'))
    and (id_parentesco is null or id_parentesco = parentesco)
    where compartilhado = 1 and id_plano = " . $idProd . " group by sf_produtos_acrescimo.id) as x 
    inner join sf_produtos_acrescimo on sf_produtos_acrescimo.id = x.id 
    and x.qtd_total between qtd_min and qtd_max";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $totalAcrescimo = $totalAcrescimo + $RFP['acrecimo'];
    }
    echo escreverNumero($totalAcrescimo);
}