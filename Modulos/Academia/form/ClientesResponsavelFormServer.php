<?php

include "../../../Connections/configini.php";
require_once('../../../util/util.php');

if (isset($_POST['SaveRespLegal'])) {
    if (is_numeric($_POST['idRespLegal']) && $_POST['TipoRespLegal'] == "R") {
        $query = "update sf_fornecedores_despesas set
        razao_social = " . valoresTextoUpper('NmRespLegal') . ",
        telefone_ps1 = " . valoresTexto('CelRespLeg') . ",
        telefone_ps2 = " . valoresTexto('EmailRespLeg') . ",   
        cnpj = " . valoresTexto('CpfRespLegal') . ",
        inscricao_estadual = " . valoresTexto('RgRespLegal') . ",
        cep = " . valoresTexto('CepRespLegal') . ",
        endereco = " . valoresTexto('EndRespLegal') . ",
        numero = " . valoresTexto('NumRespLegal') . ",
        bairro = " . valoresTexto('BairroRespLegal') . ",
        estado = " . valoresSelect('EstRespLegal') . ",
        cidade = " . valoresSelect('CidRespLegal') . ",
        complemento = " . valoresTexto('ComplRespLegal') . ",
        estado_civil = " . valoresTexto('CivilRespLegal') . ",
        profissao = " . valoresTexto('ProfRespLegal') . "                
        where id_fornecedores_despesas = " . $_POST['idRespLegal'];
        odbc_exec($con, $query) or die(odbc_errormsg());            
    } else if (!is_numeric($_POST['idRespLegal'])) {
        $query = "insert into sf_fornecedores_despesas(empresa,razao_social,cnpj,telefone_ps1,telefone_ps2,inscricao_estadual,
        cep, endereco, numero, bairro, estado, cidade, complemento, estado_civil,profissao,tipo, dt_cadastro) values
        ('001'," . valoresTextoUpper('NmRespLegal') . "," . valoresTexto('CpfRespLegal') . "," . valoresTexto('CelRespLeg') . "," .
        valoresTexto('EmailRespLeg') . "," . valoresTexto('RgRespLegal') . "," . valoresTexto('CepRespLegal') . "," .
        valoresTexto('EndRespLegal') . "," . valoresTexto('NumRespLegal') . "," . valoresTexto('BairroRespLegal') . "," .
        valoresSelect('EstRespLegal') . "," . valoresSelect('CidRespLegal') . "," . valoresTexto('ComplRespLegal') . "," .
        valoresTexto('CivilRespLegal') . "," . valoresTexto('ProfRespLegal') . ", 'R', getdate())";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_fornecedores_despesas from sf_fornecedores_despesas order by id_fornecedores_despesas desc") or die(odbc_errormsg());
        $_POST['idRespLegal'] = odbc_result($res, 1);
    }
    $query = "IF NOT EXISTS (SELECT * FROM sf_fornecedores_despesas_responsavel 
    WHERE id_responsavel = " . valoresSelect('idRespLegal') . " AND id_dependente = " . valoresSelect('SaveRespLegal') . ")
        BEGIN
            INSERT INTO sf_fornecedores_despesas_responsavel (id_responsavel, id_dependente, data_inclusao) 
            VALUES (" . valoresSelect('idRespLegal') . ", " . valoresSelect('SaveRespLegal') . ", getdate());
        END";
    odbc_exec($con, $query) or die(odbc_errormsg());    
    echo $_POST['idRespLegal'];
}

if (isset($_POST['ExcluirRespLegal'])) {
    $query = "delete from sf_fornecedores_despesas_responsavel
    where id_responsavel = " . $_POST['ExcluirRespLegal'] . " and id_dependente = " . $_POST['IdRespLegal'];
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}

if (isset($_GET['ListRespLegal'])) {
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    $cur = odbc_exec($con, "select *,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = id_fornecedores_despesas) telefone_contato,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = id_fornecedores_despesas) email_contato 
    from sf_fornecedores_despesas_responsavel inner join sf_fornecedores_despesas on id_responsavel = id_fornecedores_despesas
    where id_dependente = " . $_GET['ListRespLegal']);    
    while ($RFP = odbc_fetch_array($cur)) {
        $cont = $cont + 1;
        $row = array();
        $row[] = utf8_encode($RFP["id_fornecedores_despesas"]);
        $row[] = "<a title=\"" . utf8_encode($RFP["razao_social"]) . "\" href=\"javascript:void(0)\" onclick=\"AbrirRespLegal(" . utf8_encode($RFP["id_fornecedores_despesas"]) . ")\"><div id=\"formPQ\" title=\"" . utf8_encode($RFP["razao_social"]) . "\">" . utf8_encode($RFP["razao_social"]) . "</div></a>";
        $row[] = ($RFP['tipo'] == "R" ? utf8_encode($RFP['telefone_ps1']) : utf8_encode($RFP['telefone_contato']));
        $row[] = ($RFP['tipo'] == "R" ? utf8_encode($RFP['telefone_ps2']) : utf8_encode($RFP['email_contato']));
        $row[] = utf8_encode($RFP["cnpj"]);
        $row[] = utf8_encode($RFP["profissao"]);
        $row[] = "<center><input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\" onClick=\"RemoverRespLegal(" . $RFP['id_fornecedores_despesas'] . ",'" . $RFP['tipo'] . "')\")></center>";
        $records['aaData'][] = $row;
    }    
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;    
    echo json_encode($records);
}

if (isset($_GET['ListRespLegalDe'])) {
    $records = [];
    $cur = odbc_exec($con, "select * from sf_fornecedores_despesas_responsavel 
    inner join sf_fornecedores_despesas on id_dependente = id_fornecedores_despesas
    where id_responsavel = " . $_GET['ListRespLegalDe']);    
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row[] = utf8_encode($RFP["id_fornecedores_despesas"]);
        $row[] = utf8_encode($RFP["razao_social"]);
        $records[] = $row;
    }
    echo json_encode($records);
}

if (isset($_GET['GetRespLegal'])) {
    $cur = odbc_exec($con, "select *,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = id_fornecedores_despesas) telefone_contato,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = id_fornecedores_despesas) email_contato
    from sf_fornecedores_despesas where id_fornecedores_despesas = " . $_GET['GetRespLegal']);
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('id_fornecedores_despesas' => utf8_encode($RFP['id_fornecedores_despesas']),
            'tipo' => utf8_encode($RFP['tipo']),
            'razao_social' => utf8_encode($RFP['razao_social']),
            'cel_resp' => ($RFP['tipo'] == "R" ? utf8_encode($RFP['telefone_ps1']) : utf8_encode($RFP['telefone_contato'])),
            'email_resp' => ($RFP['tipo'] == "R" ? utf8_encode($RFP['telefone_ps2']) : utf8_encode($RFP['email_contato'])),
            'cnpj' => utf8_encode($RFP['cnpj']),
            'inscricao_estadual' => utf8_encode($RFP['inscricao_estadual']),
            'cep' => utf8_encode($RFP['cep']),
            'endereco' => utf8_encode($RFP['endereco']),
            'numero' => utf8_encode($RFP['numero']),
            'bairro' => utf8_encode($RFP['bairro']),
            'estado' => utf8_encode($RFP['estado']),
            'cidade' => utf8_encode($RFP['cidade']),
            'complemento' => utf8_encode($RFP['complemento']),
            'profissao' => utf8_encode($RFP['profissao']),
            'estado_civil' => utf8_encode($RFP['estado_civil']));
    }
    echo(json_encode($local));
}

odbc_close($con);