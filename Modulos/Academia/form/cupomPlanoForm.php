<?php

$id = "";
$PegaURL = "";
$frete_total = 0;
$comentario = "";
$clinte = 0;
$cedente = "";
$nome_fantasia = "";
$cpf_cnpj = "";
$inscricao = "";
$endereco = "";
$cidade_uf = "";
$cep = "";
$telefone = "";
$site = "";
$data_venda = "";
$descontop = "";
$descontotp = "";
$cod_pedido = "0";
$empresa = "0";
$loop = 1;
$sub_total = 0;
$desc_valor = 0;
$multa_valor = 0;
$titulo = "";
$desconto = "$";
$valorPagoDinheiro = 0;
$dt_estorno = "";
$login_user = "";

if (is_numeric($_GET["id"])) {
    $PegaURL = $_GET["id"];
}

if (is_numeric($PegaURL)) {
    $cur = odbc_exec($con, "select vendedor, id_venda,data_venda,descontop,descontotp,
    isnull(cod_pedido,0) cod_pedido,empresa,cliente_venda,comentarios_venda, dt_estorno, login_user
    from sf_vendas left join sf_usuarios on id_usuario = user_estorno
    where id_venda = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP["id_venda"];
        $data_venda = escreverData($RFP["data_venda"]) . " " . date("H:i");
        $descontop = $RFP["descontop"];
        $descontotp = $RFP["descontotp"];
        $cod_pedido = $RFP["cod_pedido"];
        $operador = utf8_encode($RFP["vendedor"]);
        $empresa = utf8_encode($RFP["empresa"]);
        $clinte = utf8_encode($RFP['cliente_venda']);
        $comentario = utf8_encode($RFP['comentarios_venda']);
        $dt_estorno = escreverData($RFP["dt_estorno"]) . " " . date("H:i");
        $login_user = utf8_encode($RFP['login_user']);
    }
    if (is_numeric($empresa)) {
        $cur = odbc_exec($con, "select * from sf_filiais where numero_filial = " . $empresa);
        while ($RFP = odbc_fetch_array($cur)) {
            $cpf_cnpj = utf8_encode($RFP["cnpj"]);
            $cedente = utf8_encode($RFP["razao_social_contrato"]);
            $nome_fantasia = utf8_encode($RFP["nome_fantasia_contrato"]);
            $inscricao = utf8_encode($RFP["inscricao_estadual"]);
            $endereco = utf8_encode($RFP['endereco']) . " n°" . utf8_encode($RFP['numero']) . "," . utf8_encode($RFP['bairro']);
            $cidade_uf = utf8_encode($RFP['cidade_nome']) . " " . utf8_encode($RFP['estado']);
            $cep = utf8_encode($RFP['cep']);
            $telefone = utf8_encode($RFP['telefone']);
            $site = utf8_encode($RFP['site']);
        }
    }
    $arrProduto = array();
    $cur = odbc_exec($con, "select id_item_venda,cm.conta_movimento grupo,gc.descricao grupodesc,conta_produto produto,cm.descricao produtodesc,
    quantidade,valor_total,descontop,descontos,descontotp,descontots,valor_multa from sf_vendas_itens vi 
    inner join sf_contas_movimento gc on vi.grupo = gc.id_contas_movimento inner join sf_produtos cm 
    on cm.conta_produto = vi.produto inner join sf_vendas v on v.id_venda = vi.id_venda where cm.tipo = 'P' and vi.id_venda = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $arrProduto[] = array($RFP["produto"],
            utf8_encode($RFP["grupodesc"]) . "-" . utf8_encode($RFP["produtodesc"]),
            $RFP["quantidade"],
            escreverNumero($RFP["valor_total"] / $RFP["quantidade"]),
            escreverNumero($RFP["valor_total"]));
        if ($RFP["descontotp"] == 0) {
            $desconto = "%";
            $desc_valor = $RFP["valor_total"] * ($RFP["descontop"] / 100);
        } else {
            $desc_valor = $RFP["descontop"];
        }
        $sub_total = $sub_total + $RFP["valor_total"];
    }
    $arrServicos = array();
    $cur = odbc_exec($con, "select id_item_venda,cm.conta_movimento grupo,gc.descricao grupodesc,conta_produto produto,cm.descricao produtodesc,
    quantidade,valor_total,isnull(valor_bruto, valor_total) valor_bruto,descontop,descontos,descontotp,descontots,valor_multa 
    from sf_vendas_itens vi left join sf_contas_movimento gc on vi.grupo = gc.id_contas_movimento inner join sf_produtos cm 
    on cm.conta_produto = vi.produto inner join sf_vendas v on v.id_venda = vi.id_venda where cm.tipo in ('S','A') and vi.id_venda = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $arrServicos[] = array($RFP["produto"],
            utf8_encode($RFP["grupodesc"]) . "-" . utf8_encode($RFP["produtodesc"]),
            $RFP["quantidade"], escreverNumero($RFP["valor_bruto"] / $RFP["quantidade"]),
            escreverNumero($RFP["valor_bruto"]));
        if ($RFP["valor_total"] < $RFP["valor_bruto"]) {
            $desc_valor = $desc_valor + $RFP["valor_bruto"] - $RFP["valor_total"];
        }
        $sub_total = $sub_total + $RFP["valor_bruto"];
    }    
    $arrPlano = array();
    $cur = odbc_exec($con, "select id_item_venda,cm.conta_movimento grupo,gc.descricao grupodesc,conta_produto produto,cm.descricao produtodesc,
    quantidade,valor_total,isnull(valor_bruto, valor_total) valor_bruto,descontop,descontos,descontotp,descontots,pp.parcela, pm.dt_inicio_mens, pm.dt_fim_mens, valor_multa
    from sf_vendas_itens vi left join sf_contas_movimento gc on vi.grupo = gc.id_contas_movimento 
    inner join sf_produtos cm on cm.conta_produto = vi.produto 
    inner join sf_vendas v on v.id_venda = vi.id_venda
    left join sf_vendas_planos_mensalidade pm on  pm.id_item_venda_mens = vi.id_item_venda 
    left join sf_produtos_parcelas pp on pm.id_parc_prod_mens = pp.id_parcela
    where cm.tipo = 'C' and vi.id_venda = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $arrPlano[] = array($RFP["produto"],
            utf8_encode($RFP["produtodesc"]) . " (QTD. MESES: " . $RFP["parcela"] . " | Período: " . escreverData($RFP["dt_inicio_mens"]) . " a " . escreverData($RFP["dt_fim_mens"]) . ")",
            $RFP["quantidade"],
            escreverNumero($RFP["valor_bruto"] / $RFP["quantidade"]),
            escreverNumero($RFP["valor_bruto"]));
        if ($RFP["valor_total"] < ($RFP["valor_bruto"] + $RFP["valor_multa"])) {
            $desc_valor = $desc_valor + (($RFP["valor_bruto"] + $RFP["valor_multa"]) - $RFP["valor_total"]);
        }
        $sub_total = $sub_total + $RFP["valor_bruto"];        
        $multa_valor = $multa_valor + $RFP["valor_multa"];        
    }
    $arrOutro = array();
    $cur = odbc_exec($con, "select id_item_venda,cm.conta_movimento grupo,gc.descricao grupodesc,conta_produto produto,cm.descricao produtodesc,
    quantidade,valor_total,isnull(valor_bruto, valor_total) valor_bruto,descontop,descontos,descontotp,descontots,valor_multa from sf_vendas_itens vi left join sf_contas_movimento gc on vi.grupo = gc.id_contas_movimento inner join sf_produtos cm 
    on cm.conta_produto = vi.produto inner join sf_vendas v on v.id_venda = vi.id_venda where cm.tipo = 'R' and vi.id_venda = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $arrOutro[] = array($RFP["produto"],
            utf8_encode($RFP["produtodesc"]) . " (QTD. MESES: " . $RFP["parcela"] . " | Período: " . escreverData($RFP["dt_inicio_mens"]) . " a " . escreverData($RFP["dt_fim_mens"]) . ")",
            $RFP["quantidade"], escreverNumero($RFP["valor_total"] / $RFP["quantidade"]),
            escreverNumero($RFP["valor_total"]));
        if ($RFP["valor_total"] < $RFP["valor_bruto"]) {
            $desc_valor = $desc_valor + $RFP["valor_bruto"] - $RFP["valor_total"];
        }
        $sub_total = $sub_total + $RFP["valor_total"];        
    }
    $arrFinanceiro = array();
    $sql = "select * from sf_venda_parcelas v left join sf_tipo_documento t on v.tipo_documento = t.id_tipo_documento where v.inativo = 0 and venda = " . $PegaURL;
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $arrFinanceiro[] = array(escreverData($RFP["data_parcela"]),
        utf8_encode($RFP["descricao"]), escreverNumero($RFP["valor_parcela"]));
        if (is_numeric($RFP["bol_valor"])) {
            $valorPagoDinheiro = $valorPagoDinheiro + $RFP["bol_valor"];
        }
    }
    $arrFrete = array();
    $cur = odbc_exec($con, "select razao_social,(select sum(valor_parcela) 
    from sf_solicitacao_autorizacao_parcelas where solicitacao_autorizacao = frete_solicitacao_autorizacao) parcelas 
    from sf_frete_historico inner join sf_fornecedores_despesas on id_fornecedores_despesas = frete_transportadora
    where tipo = 'T' and frete_venda = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $arrFrete[] = array(utf8_encode($RFP['razao_social']), escreverNumero($RFP['parcelas']));
        $frete_total = $RFP['parcelas'];
    }
    $arrCliente = array();
    if (is_numeric($clinte) && $clinte > 0) {
        $cur = odbc_exec($con, "select id_fornecedores_despesas,razao_social,cnpj,nome_fantasia,endereco,numero,complemento,bairro,tb_cidades.cidade_nome,tb_estados.estado_sigla,cep 
        from sf_fornecedores_despesas left join tb_cidades on tb_cidades.cidade_codigo = cidade left join tb_estados on tb_estados.estado_codigo = estado
        where id_fornecedores_despesas = " . $clinte);
        while ($RFP = odbc_fetch_array($cur)) {
            $arrCliente[] = array(utf8_encode($RFP['id_fornecedores_despesas']) . " - " . utf8_encode($RFP['razao_social']) . 
            (strlen($RFP['nome_fantasia']) > 0 ? " (" . utf8_encode($RFP['nome_fantasia']) . ")" : "") .
            (strlen($RFP['cnpj']) > 0 ? " - " . utf8_encode($RFP['cnpj']) . "" : ""),
            utf8_encode($RFP['endereco']) . (strlen($RFP['numero']) > 0 ? ",N° " . utf8_encode($RFP['numero']) : "") . " " . utf8_encode($RFP['complemento']),
            utf8_encode($RFP['bairro']) . " " . utf8_encode($RFP['cidade_nome']) . (strlen($RFP['estado_sigla']) > 0 ? "-" . utf8_encode($RFP['estado_sigla']) : "") . " " . utf8_encode($RFP['cep']));
        }
    }
}