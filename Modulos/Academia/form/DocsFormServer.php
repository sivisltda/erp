<?php

$disabled = 'disabled';
$id = '';
$imprimir = 0;

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = 1;
}

if (isset($_POST['bntSave'])) {
    $bloquear = 0;
    if (isset($_POST["txtBloquear"])) {
        $bloquear = 1;
    }    
    if ($_POST['txtId'] != '') {
        $id = $_POST['txtId'];
        $query = "update sf_documentos set " .
                "descricao_doc =" . valoresTexto('txtNome') . "," .
                "obs_doc = " . valoresTexto('txtDesc') . "," .
                "validade_doc = " . valoresTexto('txtdias') . "," .
                "servico_doc = " . valoresSelect('txtServico') . "," .
                "inativo_doc = " . $bloquear .
                " where id_doc = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        if (isset($_POST["listCatracas"])) {
            odbc_exec($con, "delete from sf_documentos_catracas where id_documento=" . $_POST['txtId']) or die(odbc_errormsg());
            $catraca = $_POST["listCatracas"];
            for ($i = 0; $i < count($catraca); $i++) {
                odbc_exec($con, "insert into sf_documentos_catracas(id_documento, id_catraca) values ($id,$catraca[$i])") or die(odbc_errormsg());
            }
        } else {
            odbc_exec($con, "delete from sf_documentos_catracas where id_documento=" . $_POST['txtId']) or die(odbc_errormsg());
        }
    } else {
        odbc_exec($con, "insert into sf_documentos(descricao_doc, obs_doc, validade_doc, servico_doc, inativo_doc) values (" . valoresTexto('txtNome') . "," . valoresTexto('txtDesc') . "," . valoresTexto('txtdias') . "," . valoresSelect('txtServico') . ", $bloquear)") or die(odbc_errormsg());
        if (isset($_POST["listCatracas"])) {
            $catraca = $_POST["listCatracas"];
            for ($i = 0; $i < count($catraca); $i++) {
                odbc_exec($con, "insert into sf_documentos_catracas(id_documento, id_catraca) values (IDENT_CURRENT('sf_documentos'),$catraca[$i])") or die(odbc_errormsg());
            }
        }
        $res = odbc_exec($con, "select top 1 id_doc from sf_documentos order by id_doc desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}

if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        if (isset($_POST['List'])) {
            include "../../../Connections/configini.php";
        }
        odbc_exec($con, "DELETE FROM sf_documentos WHERE id_doc = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox(1);</script>";
        exit();
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from dbo.sf_documentos where id_doc = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_doc']);
        $nome_doc = utf8_encode($RFP['descricao_doc']);
        $obs_doc = utf8_encode($RFP['obs_doc']);        
        $validade_doc = $RFP['validade_doc'];
        $bloqueio_doc = $RFP['inativo_doc'];
        $servico_doc = $RFP['servico_doc'];        
    }
} else {
    $disabled = '';
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}

if (isset($_POST['bntCancel'])) {
    $disabled = 'disabled';
}
