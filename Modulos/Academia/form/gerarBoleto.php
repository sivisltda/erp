<?php
require_once('../../../Connections/configini.php');

$plano = "";
$dtInicio = "";
$mensalidade = "";
$valor = escreverNumero(0);
$id_bancos = "";
$sf_carteiras_id = "";

$sQuery1 = "select id_mens, id_parc_prod_mens, dt_inicio_mens, dbo.VALOR_REAL_MENSALIDADE(id_mens) valor_mens 
from sf_vendas_planos_mensalidade where id_mens = " . valoresNumericos2($_GET['idMens']);
$cur = odbc_exec($con, $sQuery1);
while ($RFP = odbc_fetch_array($cur)) {
    $plano = utf8_encode($RFP["id_parc_prod_mens"]);
    $dtInicio = escreverData($RFP["dt_inicio_mens"]);
    $mensalidade = utf8_encode($RFP["id_mens"]);
    $valor = escreverNumero($RFP["valor_mens"]);
}

$sQuery2 = "select top 1 bol_id_banco, carteira_id from sf_boleto order by id_boleto desc";
$cur = odbc_exec($con, $sQuery2);
while ($RFP = odbc_fetch_array($cur)) {
    $id_bancos = $RFP["bol_id_banco"];
    $sf_carteiras_id = $RFP["carteira_id"];
}

?>
<link rel="icon" type="image/ico" href="../../../favicon.ico"/>
<link rel="stylesheet" type="text/css" href="../../../css/stylesheets.css"/>
<link rel="stylesheet" type="text/css" href="../../../css/main.css"/>
<link href="../../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
<body>
    <div class="row-fluid">
        <div class="frmhead">
            <div class="frmtext">Gerar Boletos</div>
            <div class="frmicon" onClick="parent.FecharBox(1)">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div style="padding: 10px;">
            <input id="txtPlanos" value="<?php echo $plano;?>" type="hidden"/>
            <input id="txtValor" value="<?php echo $valor;?>" type="hidden"/>
            <input id="txtMensalidade" value="<?php echo $mensalidade;?>" type="hidden"/>            
            <div class="data-fluid tabbable" style="overflow-x: auto;">
                <ul class="nav nav-tabs" style="margin-bottom: -1px;">
                    <li class="active"><a href="#tab1" data-toggle="tab">Dados do Boleto</a></li>
                    <li><a href="#tab2" data-toggle="tab">Adicionais</a></li>
                </ul>
                <div class="tab-content frmcont" style="margin: 0px;min-height: 165px;">
                    <div class="tab-pane active" id="tab1">                        
                        <div style="float: left; width:100%;">
                            <span>Banco:</span>
                            <select id="txtBanco" class="input-medium" style="width:100%">
                                <option value="null">Selecione</option>
                                <?php $cur = odbc_exec($con, "select id_bancos,razao_social from sf_bancos where ativo = 0 order by razao_social") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP['id_bancos']; ?>" <?php echo ($id_bancos == $RFP['id_bancos'] ? "selected" : "");?>><?php echo utf8_encode($RFP['razao_social']) ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div style="float: left; width:100%; margin-top:5px">
                            <span>Carteira:</span>
                            <select id="txtCarteira" class="input-medium" style="width:100%">
                                <option value="null">Selecione</option>
                                <?php if (is_numeric($id_bancos)) {
                                $cur = odbc_exec($con, "select sf_carteiras_id, sf_carteira_descricao from sf_bancos_carteiras where sf_carteiras_bancos = " . $id_bancos) or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP['sf_carteiras_id']; ?>" <?php echo ($sf_carteiras_id == $RFP['sf_carteiras_id'] ? "selected" : "");?>><?php echo utf8_encode($RFP['sf_carteira_descricao']) ?></option>
                                <?php }} ?>
                            </select>
                        </div>
                        <div style="float: left; width:33%; margin-top:5px">
                            <span>Data de Vencimento:</span>            
                            <input id="txt_dt_begin" type="text" class="datepicker inputCenter" value="<?php echo $dtInicio;?>"/>
                        </div>                         
                        <div style="float: left; width:33%; margin-top:5px; margin-left: 1%;">
                            <span>Valor Total:</span>            
                            <input id="txt_valor" type="text" class="inputCenter" value="0,00" disabled/>
                        </div>                         
                    </div>                    
                    <div class="tab-pane" id="tab2">
                        <div style="float:left; width:88%;">
                            <span>Serviços:</span>
                            <select id="txtServico" class="input-medium" style="width:100%">
                                <option value="null">Selecione</option>
                                <?php $cur = odbc_exec($con, "select conta_produto,descricao,preco_venda from sf_produtos where conta_produto > 0 AND tipo = 'S' and inativa = 0  ORDER BY descricao") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP['conta_produto'] ?>" valor="<?php echo $RFP['preco_venda']; ?>"><?php echo utf8_encode($RFP['descricao']) ?></option>
                                <?php } ?> 
                            </select>
                        </div>                            
                        <div style="float:left; width:5%; margin-left:1%; margin-top: 4.7%;">
                            <button type="button" onclick="addCampos();" class="btn dblue" <?php echo $disabled; ?>>
                                <span style="margin-top: 4px;" class="ico-plus icon-white"></span>
                            </button>                            
                        </div>                        
                        <div style="clear:both"></div>                        
                        <div id="divContatos" style="height:114px; background:#FFF; border:1px solid #DDD; overflow-y:scroll;">
                            <?php
                            $i = 0;
                            $cur = odbc_exec($con, "select top 1 id_servico, descricao, preco_venda from sf_vendas_planos_mensalidade m
                            inner join sf_vendas_planos vp on vp.id_plano = m.id_plano_mens
                            inner join sf_produtos_acrescimo_servicos pa on pa.id_produto = vp.id_prod_plano
                            inner join sf_produtos p on p.conta_produto = pa.id_servico
                            and (select count(id_dependente) from sf_fornecedores_despesas_dependentes where id_titular = favorecido) between qtd_min and qtd_max
                            where id_mens = " . valoresNumericos2($_GET['idMens']) . " and (select count(m1.id_mens) from sf_vendas_planos_mensalidade m1 where m1.dt_pagamento_mens is not null and m1.id_plano_mens = vp.id_plano) = 0 order by qtd_max desc");
                            while ($RFP = odbc_fetch_array($cur)) { $i++; ?>
                                <div id="tabela_linha_<?php echo $i; ?>" style="padding:2px; border-bottom:1px solid #DDD; overflow: hidden;">
                                    <input id="txtSer_<?php echo $i; ?>" name="txtSer_<?php echo $i; ?>" value="<?php echo $RFP['id_servico']; ?>" type="hidden">
                                    <input id="txtVal_<?php echo $i; ?>" name="txtVal_<?php echo $i; ?>" value="<?php echo $RFP['preco_venda']; ?>" type="hidden">
                                    <div id="tabela_desc_<?php echo $i; ?>" style="line-height:27px; float:left"><?php echo utf8_encode($RFP['descricao']); ?></div>
                                    <div style="width: 5%; padding: 4px 0px 3px; float: right; cursor: pointer;" onclick="return removeLinha('<?php echo $i; ?>');"><span class="ico-remove" style="font-size:20px"></span></div>
                                </div>
                            <?php } ?>
                            <input id="txtContatosTotal" name="txtContatosTotal" type="hidden" value="<?php echo $i; ?>"/>
                        </div>                        
                    </div>                    
                </div>                    
            </div>                                               
        </div>
        <div class="frmfoot" style="position: absolute;width: 95%;bottom: 0;">
            <div class="frmbtn">
                <button class="btn green" type="button" name="bntSave" onClick="GerarBol()"><span class="ico-checkmark"></span> Gravar</button>
                <button class="btn yellow" onClick="parent.FecharBox(1)"><span class="ico-reply"></span> Cancelar</button>
            </div>
        </div>
    </div>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
    <script type='text/javascript' src='../../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../../js/plugins.js'></script>
    <script type='text/javascript' src='../../../js/actions.js'></script>
    <script type='text/javascript' src='../../../js/plugins/datatables/jquery.dataTables.min.js'></script>
    <script type='text/javascript' src='../../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../../js/util.js"></script>
    <script type="text/javascript" src="../../../js/moment.min.js"></script>
    <script type="text/javascript">                
                
        function validar() {
            if ($("#txtBanco").val() === "null") {
                bootbox.alert('Selecione um Banco!');
                return false;
            } else if ($("#txtCarteira").val() === "null") {
                bootbox.alert('Selecione uma Carteira!');
                return false;
            }else if(!moment($('#txt_dt_begin').val(), lang["dmy"], true).isValid()) {
                bootbox.alert("Informar uma data válida!");                
                return false;
            }
            return true;
        }
        
        $('#txtBanco').change(function () {
            $("txtCarteira").attr('disabled', true);
            if ($(this).val()) {
                $('#txtBancoCarteira').hide();
                $.getJSON('./../../Financeiro/bancoCarteira.ajax.php?search=', {txtBanco: $(this).val(), ajax: 'true'}, function (j) {
                    options = '<option value="null">Selecione</option>';
                    for (var i = 0; i < j.length; i++) {
                        options += '<option value="' + j[i].sf_carteiras_id + '">' + j[i].sf_carteira_descricao + '</option>';
                    }
                    $('#txtCarteira').html(options).show();
                    $("txtCarteira").attr('disabled', false);
                });
            } else {
                $('#txtCarteira').html('<option value="null">Selecione</option>');
                $("txtCarteira").attr('disabled', false);
            }
        });
        
        function addCampos() {
            if ($('#txtServico').val() === "null") {
                bootbox.alert("Preencha o campo Serviço!");
            } else {
                let id = parseInt($("#txtContatosTotal").val()) + 1;                
                $("#divContatos").append("<div id=\"tabela_linha_" + id + "\" style=\"padding:2px; border-bottom:1px solid #DDD; overflow: hidden;\">" +                        
                "<input id=\"txtSer_" + id + "\" name=\"txtSer_" + id + "\" value=\"" + $("#txtServico").val() + "\" type=\"hidden\">" +                    
                "<input id=\"txtVal_" + id + "\" name=\"txtVal_" + id + "\" value=\"" + $('#txtServico option:selected').attr("valor") + "\" type=\"hidden\">" +                    
                "<div id=\"tabela_desc_" + id + "\" style=\"line-height:27px; float:left\">" +$("#txtServico option:selected").html() + "</div>" +                    
                "<div style=\"width: 5%; padding: 4px 0px 3px; float: right; cursor: pointer;\" onclick=\"return removeLinha('" + id + "');\">" +
                "<span class=\"ico-remove\" style=\"font-size:20px\"></span></div></div>");
                $("#txtContatosTotal").val(id);
                $('#txtServico').val("null");
                calcTotal();                
            }
        }
        
        function removeLinha(id) {
            $("#tabela_linha_" + id).remove();
            calcTotal();
        }
        
        function calcTotal() {
            let total = textToNumber($("#txtValor").val());
            $.each($('[id^=txtVal_]'), function (index, value) {
                total += parseFloat($(value).val());
            });
            $("#txt_valor").val(numberFormat(total));
        }
        
        function GerarBol() {
            if (validar()) {
                bootbox.confirm('Confirma a geração dos boletos selecionados?', function (result) {
                    if (result === true) {
                        $.post("./../../Clube/form/gerar-BoletosForm.php", {bntSave: "S", txtTipo: 1, 
                            txtBanco: $("#txtBanco").val(), txtPlanos: $("#txtPlanos").val(), txtCarteira: $("#txtCarteira").val(), 
                            txtDtBegin: $("#txt_dt_begin").val().replace(/\//g, "/"), 
                            txtVencimento: $("#txt_dt_begin").val().replace(/\//g, "/"),
                            txtServicos: $('[id^=txtSer_]').serializeArray().map(function (value, key) { return value.value; }).join(','),
                            txtMens: "P|0,M|" + $("#txtMensalidade").val()
                        }).done(function (data) {
                            if (data.trim() === "YES") {
                                AtualizarPagina();
                            } else {
                                bootbox.alert("Erro na geração dos boletos!");
                            }
                        });
                    } else {
                        return;
                    }
                });
            }
        }
        
        function AtualizarPagina() {            
            parent.AtualizaPagPlanosClientes($("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id'));
            parent.AtualizaListPlanosClientes();            
            parent.FecharBox(1);
        }
        
        calcTotal();
        
    </script>
</body>