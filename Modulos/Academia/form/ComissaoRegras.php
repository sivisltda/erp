<?php

function execRegras($con, $id_cliente, $id_regra) {
    $query = "";
    $indicador = "";
    $nivel_priramide = [];    
    $cur = odbc_exec($con, "select id_user_resp, isnull((select max(id_usuario) from sf_fornecedores_despesas i
    inner join sf_usuarios u on u.funcionario = i.prof_resp where i.id_fornecedores_despesas = c.id_fornecedores_despesas), id_user_resp) prof_resp 
    from sf_fornecedores_despesas c where id_fornecedores_despesas = " . $id_cliente);
    while ($RFP = odbc_fetch_array($cur)) {
        $indicador = $RFP['prof_resp'];
        $nivel_priramide[0] = $RFP['id_user_resp'];
    }
    for($i = 0; $i < 4; $i++) {
        $nivel_priramide[$i + 1] = getResUser($con, $nivel_priramide[$i]);    
    }  
    $cur = odbc_exec($con, "select id_usuario, tipo, valor, tipo_valor from sf_comissao_cliente_padrao_item where id_comissao_padrao = " . $id_regra);
    while ($RFP = odbc_fetch_array($cur)) {
        $query .= "insert into sf_comissao_cliente (id_cliente, id_usuario, tipo, valor) values (" . $id_cliente . "," . 
        ($RFP['id_usuario'] == -1 ? $indicador : 
            ($RFP['id_usuario'] == -2 ? $nivel_priramide[1] :         
                ($RFP['id_usuario'] > 0 ? $RFP['id_usuario'] : 
                    ($RFP['tipo'] > 3 ? (is_numeric($nivel_priramide[$RFP['tipo'] - 3]) ? $nivel_priramide[$RFP['tipo'] - 3] : $nivel_priramide[0]) : $nivel_priramide[0])))) . "," . 
        ($RFP['tipo'] > 3 ? 1 : $RFP['tipo']) . "," . $RFP['valor'] . "," . $RFP['tipo_valor'] . ");";
    }   
    odbc_exec($con, $query);
}

function deleteRegras($con, $id_cliente) {
    $query = "delete from sf_comissao_cliente where id_cliente = " . $id_cliente;
    odbc_exec($con, $query);
}

function getResUser($con, $id_usuario) {
    $toReturn = "";
    if (is_numeric($id_usuario)) {
        $cur = odbc_exec($con, "select u.id_usuario pai from sf_usuarios_dependentes ud
        inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = ud.id_fornecedor_despesas
        inner join sf_usuarios u on u.funcionario = f.id_fornecedores_despesas
        where ud.id_usuario = " . $id_usuario . " and u.inativo = 0");
        while ($RFP = odbc_fetch_array($cur)) {
            $toReturn = $RFP['pai'];
        }
    }
    return $toReturn;
}
