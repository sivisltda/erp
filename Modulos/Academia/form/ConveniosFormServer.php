<?php

$imprimir = 0;
$disabled = "disabled";
$id = "";
$descricao = "";
$cupom = "";
$cupomMeses = "";
$status = "";
$convAtivos = "";
$convEspecifico = "";
$tipo = "1";
$ateVencimento = "";

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = 1;
}

if (isset($_POST['btnSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_convenios set " .
                        "descricao = UPPER(" . valoresTexto('txtDescricao') . ")," .
                        "cupom = UPPER(" . valoresTexto('txtCupom') . ")," .
                        "cupom_meses = " . valoresNumericos('txtCupomMeses') . ", " .
                        "status = " . valoresCheck('txtStatus') . ", " .
                        "ativo = " . valoresCheck('txtConvAtivos') . ", " .
                        "convenio_especifico = " . valoresCheck('txtConvEspecifico') . "," .
                        "ate_vencimento = " . valoresCheck('txtAteVencimento') . " " .
                        "where id_convenio = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_convenios(descricao,cupom,cupom_meses,status,ativo,convenio_especifico,tipo,ate_vencimento)values(UPPER(" .
                        valoresTexto('txtDescricao') . "),UPPER(" .
                        valoresTexto('txtCupom') . ")," .
                        valoresNumericos('txtCupomMeses') . "," .
                        valoresCheck('txtStatus') . "," .
                        valoresCheck('txtConvAtivos') . "," .
                        valoresCheck('txtConvEspecifico') . "," .
                        valoresCheck('rConvTipo') . "," .
                        valoresCheck('txtAteVencimento') . ") ") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_convenio from sf_convenios order by id_convenio desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }

    $query = "delete from sf_convenios_planos where id_convenio_planos = " . $_POST['txtId'];
    odbc_exec($con, $query);
    if (valoresCheck('txtConvEspecifico') == "1") {
        if (isset($_POST['itemsPlanos'])) {
            for ($i = 0; $i < count($_POST['itemsPlanos']); $i++) {
                $query = "insert into sf_convenios_planos(id_convenio_planos,id_prod_convenio) values(" . $_POST['txtId'] . "," . $_POST['itemsPlanos'][$i] . ")";
                odbc_exec($con, $query);
            }
        }
        if (isset($_POST['itemsServicos'])) {
            for ($i = 0; $i < count($_POST['itemsServicos']); $i++) {
                $query = "insert into sf_convenios_planos(id_convenio_planos,id_prod_convenio) values(" . $_POST['txtId'] . "," . $_POST['itemsServicos'][$i] . ")";
                odbc_exec($con, $query);
            }
        }
    }

    if (isset($_POST['txtQtdRegra'])) {
        $max = $_POST['txtQtdRegra'];
        $notIn = '0';
        for ($i = 0; $i <= $max; $i++) {
            if (isset($_POST['txtIdRegra_' . $i]) && is_numeric($_POST['txtIdRegra_' . $i])) {
                if ($_POST['txtIdRegra_' . $i] != 0) {
                    $notIn = $notIn . "," . $_POST['txtIdRegra_' . $i];
                }
            }
        }
        odbc_exec($con, "delete from sf_convenios_regras where convenio = " . $_POST['txtId'] . " and id_regra not in (" . $notIn . ")");
        for ($i = 0; $i <= $max; $i++) {
            if (isset($_POST['txtIdRegra_' . $i])) {
                if ($_POST['txtIdRegra_' . $i] !== "0") {
                    
                } else {
                    odbc_exec($con, "insert into sf_convenios_regras(convenio,valor,tp_valor,numero_minimo) values(" . $_POST['txtId'] . "," . valoresNumericos('txtValRegra_' . $i) . ",'" . $_POST['txtTpValor_' . $i] . "'," . valoresNumericos('txtMinRegra_' . $i) . ")") or die(odbc_errormsg());
                }
            }
        }
    }
}

if (isset($_POST['btnDelete'])) {
    if ($_POST['txtId'] != '') {
        if (isset($_POST['List'])) {
            include "../../../Connections/configini.php";
        }
        odbc_exec($con, " delete from sf_convenios_planos where id_convenio_planos = " . $_POST['txtId'] . " 
                          delete from sf_convenios WHERE id_convenio = " . $_POST['txtId']);
        echo "YES";
    }
}

if (isset($_POST['btnNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_convenios where id_convenio =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_convenio'];
        $descricao = utf8_encode($RFP['descricao']);
        $cupom = utf8_encode($RFP['cupom']);
        $cupomMeses = utf8_encode($RFP['cupom_meses']);
        $tipo = utf8_encode($RFP['tipo']);
        $convAtivos = utf8_encode($RFP['ativo']);
        $status = utf8_encode($RFP['status']);
        $convEspecifico = utf8_encode($RFP['convenio_especifico']);
        $ateVencimento = utf8_encode($RFP['ate_vencimento']);        
    }
} else {
    $disabled = '';
}

if (isset($_POST['btnEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}

if (isset($_GET['listRegras'])) {
    include "../../../Connections/configini.php";
    $cur = odbc_exec($con, "select * from sf_convenios_regras inner join sf_convenios on sf_convenios.id_convenio = sf_convenios_regras.convenio where convenio = " . $_GET['listRegras']);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["id"] = utf8_encode($RFP["id_regra"]);
        $row["valor"] = escreverNumero($RFP["valor"], 0, $desconto_decimal_);
        $row["tipo"] = utf8_encode($RFP["tp_valor"]);
        if ($RFP["tipo"] == "0") {
            $row["numero_minimo"] = utf8_encode($RFP["numero_minimo"]);
        } else {
            $row["numero_minimo"] = "";
        }
        $row["ate_vencimento"] = utf8_encode($RFP["ate_vencimento"]);
        $records[] = $row;
    }
    echo json_encode($records);
}

if (isset($_GET['listConveniados'])) {
    include "../../../Connections/configini.php";
    $cur = odbc_exec($con, "select id_fornecedores_despesas,razao_social from sf_convenios 
    inner join sf_fornecedores_despesas_convenios on id_convenio = convenio
    inner join sf_fornecedores_despesas on id_fornecedor = id_fornecedores_despesas
    where id_convenio = " . $_GET['listConveniados']);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["id"] = utf8_encode($RFP["id_fornecedores_despesas"]);
        $row["valor"] = utf8_encode($RFP["razao_social"]);
        $records[] = $row;
    }
    echo json_encode($records);
}