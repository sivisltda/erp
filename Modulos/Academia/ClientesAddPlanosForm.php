<?php 
include "../../Connections/configini.php"; 
$tipo_venc = 0;
$mdl_seg_ = returnPart($_SESSION["modulos"],14);

$sql = "select ACA_TIPO_VENCIMENTO from sf_configuracao";   
$cur = odbc_exec($con, $sql) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {    
    $tipo_venc = $RFP['ACA_TIPO_VENCIMENTO'];
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
<style>
    body { 
        font-family: sans-serif; 
    }    
    .selected {
        background: #acbad4 !important;
    }
</style>
<body onload="iniciaAddPlanoForm(<?php echo $_GET["idForm"]; ?>);">
    <div class="row-fluid">
        <form>
            <input type="hidden" id="txtModEmp" value="<?php echo $_SESSION["mod_emp"]; ?>">
            <div class="frmhead">
                <div class="frmtext">Adicionar Contrato</div>
                <div class="frmicon" onClick="parent.FecharBoxCliente()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont" style="min-height: 283px;">
                <input id="tpTela" type="hidden" value="<?php echo $_GET["tp"]; ?>"/>
                <div style="width:46%; float:left">
                    <span>Plano:</span>
                    <select id="txtPlanos" class="select" style="width:100%"></select>
                </div>
                <?php if ($_SESSION["mod_emp"] != 1 && $mdl_seg_ == 0) { ?>
                    <div style="width:35%; float:left; margin-left:1%">
                        <span>Horário:</span>
                        <select id="txtHorarios" class="select" style="width:100%"></select>
                    </div>
                <?php } ?>
                <div style="width:16%; float:left; margin-left:1%">
                    <span>Data Início:</span>
                    <input type="text" id="txtDtInicio" class="datepicker inputCenter" <?php echo ($ckb_aca_data_inicio_ == 0 ? "disabled" : "");?>/>
                </div>
                <div style="clear:both; height:5px"></div>
                <?php if ($tipo_venc == "2") { ?>
                    <div style="width:16%; float:left;">
                        <span>Escolher dia Padrão:</span>
                        <select id="txtDiaPadrao" class="select" style="width:100%"></select>
                    </div>
                    <div style="width:19%; float:left; margin-left:1%">
                        <span>Período:</span>                
                        <select id="txtAjustarPeriodo" class="select" style="width:100%">
                            <option value="0">Máximo</option>
                            <option value="1">Mínimo</option>                            
                        </select>                    
                    </div>
                    <div style="width:19%; float:left; margin-left:1%">
                        <span>Ajustar Valor:</span>                
                        <select id="txtAjustarValor" class="select" style="width:100%">
                            <option value="0">Manter Valor</option>
                            <option value="1">Pro-Rata</option>                            
                        </select>                    
                    </div>
                    <div style="width:16%; float:left; margin-left:1%">
                        <span>Data Fim:</span>
                        <input type="text" id="txtDtFim" value="" class="datepicker inputCenter" disabled/>
                    </div>
                    <div style="width:9%; float:left; margin-left:1%">
                        <span>Dias:</span>
                        <input type="text" id="txtDias" value="" class="datepicker inputCenter" disabled/>
                    </div>                
                    <div style="width:16%; float:left; margin-left:1%">
                        <span>Valor:</span>
                        <input type="text" id="txtValor" value="" class="datepicker inputCenter" disabled/>
                    </div>                
                    <div style="clear:both; height:5px"></div>
                <?php } ?>
                <div style="width:100%; margin:10px 0 0">
                    <span style="color:black; font-size:14px; text-shadow:0 1px #FFF; padding-left:5px">Prazo</span>
                    <span id="lblCancelamento" style="color:red;padding-right:10px; float: right;"></span>
                    <input id="txtCancelamento" type="hidden" value=""/>
                    <hr style="margin:2px 0; border-top:1px solid #DDD">
                </div>
                <div class="body" style="margin-left:0; padding:0">
                    <div class="content" style="min-height:153px; margin-top:0">
                        <table id="tblValores" class="table" style="cursor: pointer;" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr style="cursor: default;">
                                    <th style="width:30px; text-align:center"><b>Meses</b></th>
                                    <th style="width:127px; text-align:center"><b>Valor</b></th>
                                    <th style="width:150px; text-align:center"><b>Valor c/ Desconto</b></th>
                                    <th style="width:180px; text-align:center"><b>Pagamento</b></th>
                                    <th style="width:100px; text-align:center"><b>Desconto %</b></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <button class="btn green" type="button" id="btnGravarPlano" title="Gravar"><span class="ico-checkmark"></span> Gravar</button>
                    <button class="btn yellow" title="Cancelar" onClick="parent.FecharBoxCliente()"><span class="ico-reply"></span> Cancelar</button>
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../js/plugins.js"></script>
    <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type="text/javascript" src="js/ClientesAddPlanoForm.js"></script>
    <script type="text/javascript">
        
        function calcularData() {
            let tipo_planos = ["BOL", "DCC", "PIX", "1"];            
            let data_fim_ajuste = "";
            let dias_diferenca = "";
            let valor_pag = "";            
            
            let tabela = $('>td', $('tr:has(td.selected)')[0]);
            if (tabela.length > 0 && tipo_planos.includes($(tabela[0].innerHTML).text())) {
                data_fim_ajuste = moment($("#txtDiaPadrao").val() + $("#txtDtInicio").val().substring(2), lang["dmy"]).add(-1, 'day');
                if (moment(data_fim_ajuste).diff(moment($("#txtDtInicio").val(), lang["dmy"]), 'days') < 0) {
                    data_fim_ajuste = data_fim_ajuste.add(1, 'month');
                }
                if ($("#txtAjustarPeriodo").val() === "0") {
                    data_fim_ajuste = data_fim_ajuste.add(1, 'month');
                }
                dias_diferenca = moment(data_fim_ajuste).diff(moment($("#txtDtInicio").val(), lang["dmy"]), 'days') + 1;
                valor_pag = $(tabela[2].innerHTML).text();
                if ($("#txtAjustarValor").val() === "1") {
                    let data_fim = moment($("#txtDtInicio").val(), lang["dmy"]).add(1, 'month').add(-1, 'day');
                    let dias_dif_normal = moment(data_fim).diff(moment($("#txtDtInicio").val(), lang["dmy"]), 'days') + 1;                    
                    let valor_dia = (textToNumber($(tabela[2].innerHTML).text()) / dias_dif_normal);
                    valor_pag = numberFormat((valor_dia * dias_diferenca));
                }
                data_fim_ajuste = data_fim_ajuste.format(lang["dmy"]);                
            } else if (tabela.length > 0) {
                data_fim_ajuste = moment($("#txtDtInicio").val(), lang["dmy"]).add($(tabela[0].innerHTML).text(), 'month').add(-1, 'day');
                dias_diferenca = moment(data_fim_ajuste).diff(moment($("#txtDtInicio").val(), lang["dmy"]), 'days');
                valor_pag = $(tabela[2].innerHTML).text();                
                data_fim_ajuste = data_fim_ajuste.format(lang["dmy"]);
            }            
            $("#txtDtFim").val(data_fim_ajuste);                        
            $("#txtDias").val(dias_diferenca);
            $("#txtValor").val(valor_pag);
        }
        
        function refreshDias() {
            $.getJSON("form/ClientesPlanosServer.php", {getDiaPadrao: "S", idDia: $("#txtDtInicio").val().substring(0,2)}, function (j) {
                if (j !== null && j.length > 0) {
                    let options = "";
                    for (var i = 0; i < j.length; i++) {
                        options = options + "<option value=\"" + j[i].value + "\" minimo=\"" + j[i].minimo + "\" pro_rata=\"" + j[i].pro_rata + "\">" + j[i].text + "</option>";
                    }
                    $("#txtDiaPadrao").html(options);
                    $("#txtDiaPadrao").select2("val", j[0].value);                    
                    $("#txtAjustarPeriodo").select2("val", $("#txtDiaPadrao option:selected").attr("minimo"));
                    $("#txtAjustarValor").select2("val", $("#txtDiaPadrao option:selected").attr("pro_rata"));
                    calcularData();                    
                }
            });            
        }
        
        $('#txtDtInicio').change(function () {
            refreshDias();
        });
        
        $('#txtDiaPadrao').change(function () {
            $("#txtAjustarPeriodo").select2("val", $("#txtDiaPadrao option:selected").attr("minimo"));
            $("#txtAjustarValor").select2("val", $("#txtDiaPadrao option:selected").attr("pro_rata"));
            calcularData();
        });        
        
        $('#txtAjustarPeriodo, #txtAjustarValor').change(function () {
            calcularData();
        });        
        
    </script>
    <?php odbc_close($con); ?>
</body>
