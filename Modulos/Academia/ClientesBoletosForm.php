<?php
include "../../Connections/configini.php";
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<link href="../../js/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <div class="row-fluid">
        <form>
            <div class="frmhead">
                <div class="frmtext">Itens do <?php echo (isset($_GET['tp']) && $_GET['tp'] == "L" ? "Link" : "Boleto");?></div>
                <div class="frmicon" onClick="parent.FecharBoxCliente()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <div class="body" style="margin-left:0; padding:0">
                    <div class="content" style="min-height:400px; margin-top:0">
                        <table id="tblHistTurmas" class="table" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="12%">Tipo</th>
                                    <th width="35%">Descrição</th>
                                    <th width="5%">Qtd</th>
                                    <th width="12%">Valor</th>
                                    <th width="12%">V.Juros</th>                                    
                                    <th width="12%">Desconto</th>
                                    <th width="12%">V.Final</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                $valor_total = 0;
                                $valor_multa = 0;
                                $valor_desc = 0;
                                $valor_bruto = 0;
                                if (is_numeric($_GET["id"])) {
                                    if (isset($_GET['tp']) && $_GET['tp'] == "L") {
                                        $query = "select dt_inicio_mens, descricao, placa, quantidade, tipo, valor_total, valor_bruto, 
                                        valor_multa from sf_link_online_itens bi inner join sf_produtos on conta_produto = id_produto
                                        left join sf_vendas_planos_mensalidade m on m.id_mens = bi.id_mens
                                        left join sf_vendas_planos vp on vp.id_plano = m.id_plano_mens
                                        left join sf_fornecedores_despesas_veiculo v on v.id = vp.id_veiculo
                                        where id_link = " . $_GET["id"];                                        
                                    } else {
                                        $query = "select dt_inicio_mens, descricao, placa, quantidade, tipo, valor_total, valor_bruto, 
                                        valor_multa from sf_boleto_online_itens bi inner join sf_produtos on conta_produto = id_produto
                                        left join sf_vendas_planos_mensalidade m on m.id_mens = bi.id_mens
                                        left join sf_vendas_planos vp on vp.id_plano = m.id_plano_mens
                                        left join sf_fornecedores_despesas_veiculo v on v.id = vp.id_veiculo
                                        where id_boleto = " . $_GET["id"];
                                    }
                                    $cur = odbc_exec($con, $query);
                                    while ($RFP = odbc_fetch_array($cur)) { 
                                        $valor_total += $RFP["valor_total"];
                                        $valor_multa += $RFP["valor_multa"];
                                        $valor_desc += ($RFP["valor_total"] - $RFP["valor_bruto"]);
                                        $valor_bruto += $RFP["valor_bruto"];    
                                    ?>
                                <tr>
                                    <td><?php echo ($RFP["tipo"] == "C" ? "MENSALIDADE" : ($RFP["tipo"] == "S" ? "SERVIÇO" : "PRODUTO")); ?></td>
                                    <td><?php echo ($RFP["tipo"] == "C" ? escreverData($RFP["dt_inicio_mens"]) . " - " . utf8_encode($RFP["descricao"]) . " [" . utf8_encode($RFP["placa"]) . "]" : utf8_encode($RFP["descricao"])); ?></td>
                                    <td><?php echo escreverNumero($RFP["quantidade"], 0, 0); ?></td>
                                    <td><?php echo escreverNumero($RFP["valor_total"], 1); ?></td>
                                    <td><?php echo escreverNumero($RFP["valor_multa"], 1); ?></td>                                    
                                    <td><?php echo escreverNumero(($RFP["valor_total"] - $RFP["valor_bruto"]), 1); ?></td>
                                    <td><?php echo escreverNumero($RFP["valor_bruto"], 1); ?></td>
                                </tr>
                            <?php }} ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="3"></th>
                                    <th><?php echo escreverNumero($valor_total, 1); ?></th>
                                    <th><?php echo escreverNumero($valor_multa, 1); ?></th>
                                    <th><?php echo escreverNumero($valor_desc, 1); ?></th>
                                    <th><b><?php echo escreverNumero($valor_bruto, 1); ?></b></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>            
            </div>
        </form>
    </div>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
    <script type='text/javascript' src="../../js/plugins/jstree/dist/jstree.min.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
</body>