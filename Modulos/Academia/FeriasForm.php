<?php include "../../Connections/configini.php"; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Configuração</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <style>
            .inputDireito {
                text-align:right;
            }
            .fancybox-wrap, .fancybox-skin, .fancybox-outer, .fancybox-inner, .fancybox-image, .fancybox-wrap object, .fancybox-nav, .fancybox-nav span, .fancybox-tmp {
                padding: 0;
                margin: 0;
                border: 0;
                outline: none;
                vertical-align: top;
                background:url(../../img/fundosForms/formBancos.PNG);
                border:solid 1 #cfcfcf;
            }
            #formulario {
                float: left;
                width: 100%;
            }
            .linha {
                float: left;
                width: 100%;
                display: block;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            #tblFerias td {
                padding: 5px 10px;
                line-height: 16px;
            }
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1><?php echo ($mdl_clb_ == 1 ? "Configuração" : "Academia")?><small>Congelamento</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="block" style="font-size:13px">
                                Meses: <input name="txtMeses" id="txtMeses" type="text" class="input-medium" style="width:80px; height:31px" maxlength="3"/>
                                Quantidade de Dias: <input name="txtDias" id="txtDias" type="text" class="input-medium" style="width:80px; height:31px" maxlength="3"/>
                                <button class="button button-green btn-primary" type="button" onclick="salvaFerias()"><span class="icon-plus icon-white"></span></button>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Congelamento</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tblFerias">
                            <thead>
                                <tr>
                                    <th width="30%">Meses</th>
                                    <th width="55%">Quantidade</th>
                                    <th width="10%"><center>Ação</center></th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="6" class="dataTables_empty">Carregando dados ...</td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <div class="dialog" id="source" title="Source"></div>    
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../js/plugins.js"></script>
    <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/actions.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type="text/javascript" src="js/FeriasForm.js"></script>    
    <?php odbc_close($con); ?>
</html>