<?php
include "../../Connections/configini.php";
include "../../util/util.php";
include "form/ConveniosFormServer.php";
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
<body>
    <div class="row-fluid">
        <form action="ConveniosForm.php" name="frmConveniosForm" id="frmConveniosForm" method="POST">
            <div class="frmhead">
                <div class="frmtext">Convênios</div>
                <div class="frmicon" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="tabbable frmtabs">
                <ul class="nav nav-tabs" style="margin:0">
                    <li class="active"><a href="#tab1" data-toggle="tab">Dados principais</a></li>
                    <li><a href="#tab2" data-toggle="tab">Especificar Modalidades</a></li>
                    <li><a href="#tab3" data-toggle="tab">Especificar Serviços</a></li>                    
                </ul>
                <div class="tab-content frmcont" style="height:260px">
                    <div class="tab-pane active" id="tab1" style="overflow:hidden; height:260px">
                        <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                        <div style="float:left; width:100%;">
                            <span style="margin-right:10px"><input type="radio" id="rConvTipo" <?php
                                if ($tipo == 1) {
                                    echo "checked";
                                }
                                ?> <?php
                                if ($id !== "") {
                                    echo "disabled";
                                }
                                ?> name="rConvTipo" value="1" style="opacity:0">Convênio</span>
                            <?php if ($_SESSION["mod_emp"] != 1) { ?><span style="margin-right:10px"><input type="radio" id="rConvTipo" <?php
                                if ($tipo == 0) {
                                    echo "checked";
                                }
                                ?> <?php
                                if ($id !== "") {
                                    echo "disabled";
                                }
                                ?> name="rConvTipo" value="0" style="opacity:0">Plano Empresa/Família</span> <?php } ?>
                        </div>
                        <div style="width:60%; float:left">
                            <span>Descrição:</span>
                            <input type="text" class="input-medium" style="width:100%" id="txtDescricao" name="txtDescricao" <?php echo $disabled; ?> value="<?php echo $descricao; ?>" maxlength="255"/>
                        </div>
                        <div style="width:29%; float:left; margin-left: 1%;">
                            <span>Cupom (Online):</span>
                            <input type="text" class="input-medium" style="width:100%" id="txtCupom" name="txtCupom" <?php echo $disabled; ?> value="<?php echo $cupom; ?>" maxlength="128"/>
                        </div>                        
                       <div style="width:9%; float:left; margin-left: 1%;">
                            <span>Meses:</span>
                            <input type="text" class="input-medium" style="width:100%" id="txtCupomMeses" name="txtCupomMeses" <?php echo $disabled; ?> value="<?php echo $cupomMeses; ?>" maxlength="2"/>
                        </div>                        
                        <div style="width:100%; float:left;margin-top: 5px;">
                            <div style="float:left; margin-top:0">
                                <input type="checkbox" class="input-medium" id="txtStatus" name="txtStatus" <?php echo $disabled; ?> <?php
                                if (substr($status, 0, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1"/>Bloqueado
                            </div>
                            <div style="float:left; margin-top:0; margin-left:1%;">
                                <input type="checkbox" class="input-medium" id="txtConvEspecifico" name="txtConvEspecifico" <?php echo $disabled; ?> <?php
                                if (substr($convEspecifico, 0, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1"/>Especificar Modalidades
                            </div>                    
                            <div style="float:left; margin-top:0; margin-left:1%;">
                                <input type="checkbox" class="input-medium" id="txtConvAtivos" name="txtConvAtivos" <?php echo $disabled; ?> <?php
                                if (substr($convAtivos, 0, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1"/>Somente Ativos
                            </div>                    
                            <div style="float:left; margin-top:0; margin-left:1%;">
                                <input type="checkbox" class="input-medium" id="txtAteVencimento" name="txtAteVencimento" <?php echo $disabled; ?> <?php
                                if (substr($ateVencimento, 0, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1"/>Até o Vencimento
                            </div>                    
                        </div>
                        <div style="width:100%; float:left">
                            <input type="hidden" value="" id="txtQtdRegra" name="txtQtdRegra" value="0" />
                            <div style="background:#F1F1F1; border:1px solid #DDD; border-bottom:0; padding:5px 5px 0">
                                <div style="float:left; width:30%">
                                    <span>Valor:</span>
                                    <input type="text" class="input-medium" id="txtValor" maxlength="10" name="txtValor" <?php echo $disabled; ?>/>
                                </div>
                                <div style="float:left">
                                    <span style="width:100%; display:block;">&nbsp;</span>
                                    <span style="width:100%; display:block;">
                                        <div class="btn-group"><button class="btn btn-primary" id="btnTpValor" type="button" <?php echo $disabled; ?> style="width:35px; padding-bottom:3px; text-align:center; background:#308698 !important"><span id="tp_sinal">%</span></button></div>
                                        <input name="txtSinal" id="txtSinal" value="P" type="hidden"/>
                                    </span>
                                </div>
                                <div id="divQtdMin" style="float:left; width:24% ;margin-left:1%" <?php
                                if ($tipo == 1) {
                                    echo "hidden";
                                }
                                ?>>
                                    <span>Qtd Mínima:</span>
                                    <input type="text" class="input-medium" id="txtQtdMin" maxlength="4" name="txtQtdMin" <?php echo $disabled; ?> value="<?php echo $valor; ?>" />
                                </div>
                                <div style="float:left; width:75px; margin-left:1%; margin-top:15px">
                                    <button type="button" onclick="addRegra()" class="btn dblue" <?php echo $disabled; ?> style="line-height:19px; width:75px; margin:0; background:#308698 !important">Incluir <span class="icon-arrow-next icon-white"></span></button>
                                </div>
                                <div style="clear:both"></div>
                            </div>
                            <div style="background:#F1F1F1; border:1px solid #DDD; border-top:0; padding:5px">
                                <div id="divRegras" style="height:100px; background:#FFF; border:1px solid #DDD; overflow-y:scroll"></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2" style="overflow:hidden; height:260px">
                        <div style="width:100%; margin-top:10px">
                            <select name="itemsPlanos[]" multiple="multiple" id="mscPlanos">
                                <?php
                                $idUser = $id == "" ? "null" : $id;
                                $cur = odbc_exec($con, "select conta_produto, B.descricao, case when conta_produto 
                                in(select id_prod_convenio from sf_convenios_planos where id_convenio_planos = " . $idUser . ") then 'S' else 'N' end isParte
                                from sf_produtos B where tipo = 'C' and b.conta_produto > 0 order by B.descricao") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP["conta_produto"] ?>"<?php
                                    if ($RFP["isParte"] == "S") {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                <?php } ?>
                            </select>
                        </div>                        
                    </div>
                    <div class="tab-pane" id="tab3" style="overflow:hidden; height:260px">
                        <div style="width:100%; margin-top:10px">
                            <select name="itemsServicos[]" multiple="multiple" id="mscServicos">
                                <?php
                                $idUser = $id == "" ? "null" : $id;
                                $cur = odbc_exec($con, "select conta_produto, B.descricao, case when conta_produto 
                                in(select id_prod_convenio from sf_convenios_planos where id_convenio_planos = " . $idUser . ") then 'S' else 'N' end isParte
                                from sf_produtos B where tipo = 'S' and b.conta_produto > 0 order by B.descricao") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP["conta_produto"] ?>"<?php
                                    if ($RFP["isParte"] == "S") {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>            
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == "") { ?>
                        <button class="btn green" type="button" onClick="validaForm()"><span class="ico-checkmark"></span> Gravar</button>
                        <input name="btnSave" id="btnSave" type="submit" value="btnSave" style="display:none"/>
                        <?php if ($_POST["txtId"] == "") { ?>
                            <button class="btn yellow" onClick="parent.FecharBox()"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                            <button class="btn yellow" type="submit" name="btnAlterar"><span class="ico-reply"></span> Cancelar</button>
                        <?php
                        }
                    } else { ?>
                        <button class="btn green" type="submit" name="btnNew"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" type="submit" name="btnEdit"><span class="ico-edit"></span> Alterar</button>
                        <button class="btn red" type="button" name="btnDelete" onClick="parent.RemoverItem(document.getElementById('txtId').value)"><span class="ico-remove"></span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
    <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
    <script type="text/javascript" src="../../js/plugins/other/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/animatedprogressbar/animated_progressbar.js"></script>
    <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../js/plugins.js"></script>
    <script type="text/javascript" src="../../js/charts.js"></script>
    <script type="text/javascript" src="../../js/actions.js"></script>
    <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../js/jquery.priceformat.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>      
    <script type='text/javascript' src='../../js/plugins/multiselect/jquery.multi-select.min.js'></script>    
    <script type="text/javascript" src="js/ConveniosForm.js"></script>        
    <?php odbc_close($con); ?>
</body>
