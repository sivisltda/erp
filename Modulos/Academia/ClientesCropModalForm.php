<?php include "../../Connections/configini.php"; ?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="../../js/plugins/cropper/cropper.min.css">
<link rel="stylesheet" href="../../css/CropModal.css">
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <div class="row-fluid">
        <form class="" id="crop-avatar">
            <div id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                <div class="avatar-form">
                    <div class="frmhead">
                        <div class="frmtext">Upload de Foto</div>
                        <div class="frmicon" onClick="parent.FecharBox()">
                            <span class="ico-remove"></span>
                        </div>
                    </div>
                    <div class="frmcont">
                        <div class="modal-body" style="padding: 0px !important;max-height: 450px !important;">
                            <input id="txtContrato" name="txtContrato" value="<?php echo $_SESSION["contrato"]; ?>" type="hidden"/>
                            <input id="txtId" name="txtId" type="hidden"/>
                            <div class="avatar-body">
                                <div class="avatar-upload">
                                    <input type="hidden" class="avatar-src"  name="avatar_src" id="avatar_src">
                                    <input type="hidden" class="avatar-data" name="avatar_data" id="avatar_data">
                                    <label for="avatarInput">Selecionar:</label>
                                    <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
                                </div>
                                <div class="row">
                                    <div style="float: left;width: 58%">
                                        <div class="avatar-wrapper"></div>
                                    </div>
                                    <div style="float: left;margin-left: 10%;">
                                        <div class="avatar-preview preview-lg"></div>
                                        <div style="clear:both; height:5px;"></div>
                                        <div style="width: 93%">
                                            <div class="avatar-btns" >
                                                <button type="button" class="btn btn-primary btn-block" data-method="rotate" data-option="-90" title="Rotate -90 degrees">Girar Esquerda</button>
                                                <button type="button" class="btn btn-primary btn-block" data-method="rotate" data-option="90" title="Rotate 90 degrees">Girar Direita</button>
                                            </div>
                                            <div class="col-md-3 recortar-btns">
                                                <button type="button" data-method="recortar" class="btn btn-primary btn-block avatar-save">Recortar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>-->
        <!--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
        <script type="text/javascript" src="js/ClientesCropModalForm.js"></script>
        <script src="../../JS/plugins/cropper/cropper.min.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
    </div>
</body>
