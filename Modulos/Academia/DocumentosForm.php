<?php
include "../../Connections/configini.php";
include "form/DocsFormServer.php";
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
<style>
    .selected {
        background: #acbad4 !important;
    }
</style>
<body>
    <div class="row-fluid">
        <form action="DocumentosForm.php<?php
        if (isset($_GET['id'])) {
            echo "?id=" . $_GET['id'];
        }
        ?>" name="frmDocumentosForm" id="frmDocumentosForm" method="POST">
            <div class="frmhead">
                <div class="frmtext">Documentos</div>
                <div class="frmicon" onClick="parent.FecharBox(1)">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <div class="formDocs">
                    <input type="hidden" name="txtId" id="txtId" value="<?php echo $id; ?>">
                    <p>Nome: </p>
                    <input type="text" name="txtNome" id="txtNome" value="<?php echo $nome_doc; ?>" <?php echo $disabled; ?>>
                    <p>Serviço (Pagamento Obrigatório): </p>                    
                    <select name="txtServico" id="txtServico" <?php echo $disabled; ?>>
                        <option value="null">Selecione</option>
                        <?php $cur = odbc_exec($con, "select conta_produto, descricao from sf_produtos where tipo = 'S' and inativa = 0 order by descricao") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option value="<?php echo $RFP["conta_produto"]; ?>" <?php echo ($RFP["conta_produto"] == $servico_doc ? "SELECTED" : ""); ?>><?php echo utf8_encode($RFP["descricao"]); ?></option>
                        <?php } ?>
                    </select>                    
                    <p>Descrição: </p>
                    <textarea name="txtDesc" id="txtDesc" rows="8" cols="80" <?php echo $disabled; ?>><?php echo $obs_doc; ?></textarea>
                    <div class="ajustEmLinha">
                        <div class="validdDoc">
                            <p>Validade: </p>
                            <input type="number" name="txtdias" id="txtdias" value="<?php echo $validade_doc; ?>" <?php echo $disabled; ?>>
                            <p>dias</p>
                        </div>
                        <div class="bloqueioDoc" style="flex: 3;">
                            <input type="checkbox" name="txtBloquear" id="txtBloquear" <?php echo ($bloqueio_doc == 1 ? "checked" : ""); ?> value="" <?php echo $disabled; ?>>
                            <p>Bloquear Aluno</p>
                        </div>
                    </div>
                </div>
                <div style="clear:both;"></div>
                <div style="width:100%; margin-top:10px" class="formCatracas">
                    <fieldset>
                        <legend>Catracas:</legend>
                        <select name="listCatracas[]" multiple="multiple" id="mscPlanos">
                            <?php $cur = odbc_exec($con, "select id_catraca, nome_catraca, case when id_catraca in(
                            select id_catraca from sf_documentos_catracas where id_documento ='" . $id . "') then 'S' else 'N' end grp
                            from sf_catracas") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP["id_catraca"]; ?>" <?php echo ($RFP["grp"] == "S" ? "SELECTED" : ""); ?>><?php echo utf8_encode($RFP["nome_catraca"]); ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == "") { ?>
                        <button class="btn green" type="submit" name="bntSave" onClick="return validaForm()"><span class="ico-checkmark"></span> Gravar</button>
                        <?php if ($_POST["txtId"] == "") { ?>
                            <button class="btn yellow" onClick="parent.FecharBox(1)"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                            <button class="btn yellow" type="submit" name="bntCancel"><span class="ico-reply"></span> Cancelar</button>
                            <?php
                        }
                    } else {
                        ?>
                        <button class="btn green" type="submit" name="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" type="submit" name="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                        <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../js/jquery.priceformat.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/plugins/multiselect/jquery.multi-select.min.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>
    <script type="text/javascript" src="js/DocumentosForm.js"></script>    
    <?php odbc_close($con); ?>
</body>
