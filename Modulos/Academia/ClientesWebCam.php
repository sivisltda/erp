<?php
if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") {
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $redirect);
    exit();
}
include "../../Connections/configini.php";
$urlReturn = "ClientesForm.php";
if (is_numeric($_GET["id"])) {
    $cur = odbc_exec($con, "select tipo from sf_fornecedores_despesas where id_fornecedores_despesas = " . $_GET["id"]);
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['tipo'] == "F") {
            $urlReturn = "FuncionariosForm.php";
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1><?php echo ($mdl_clb_ == 1 ? "Configuração" : "Academia")?><small>WebCam</small></h1>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">WebCam</div>
                    </div>
                    <div class="boxtable">
                        <div style="float: left">
                            <input id="txtId" type="hidden" value="<?php echo $_GET["id"]; ?>"/>
                            <video id="video" width="512" height="384" style="border:1px solid gray;" autoplay></video>    
                        </div>
                        <div style="float: left;margin-left: 10%;">
                            <canvas id="canvas" width="256" height="256" style="border:1px solid gray;"></canvas>                            
                            <div style="clear:both; height:5px"></div>
                            <button type="button" class="btn btn-default" id="snap" style="width: 100%">Capturar</button>
                            <button type="button" class="btn btn-success" id="btnSalvarImagem" onclick="saveImagem()" style="width: 100%" disabled>Confirmar</button>
                        </div>                        
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>
        <script type="text/javascript" src="../../fancyapps/source/jquery.fancybox.js?v=2.1.4"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>  
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            var video = document.getElementById('video');
            var canvas = document.getElementById('canvas');
            var context = canvas.getContext('2d');
            var errBack = function (e) {
                console.log('An error has occurred!', e);
            };

            if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                navigator.mediaDevices.getUserMedia({video: true
                }).then(function (stream) {
                    //video.src = window.URL.createObjectURL(stream);
                    video.srcObject = stream;
                    video.play();
                });
            }

            var mediaConfig = {video: true};
            if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                navigator.mediaDevices.getUserMedia(mediaConfig).then(function (stream) {
                    //video.src = window.URL.createObjectURL(stream);
                    //video.srcObject = stream;
                    video.play();
                });
            } else if (navigator.getUserMedia) { // Standard
                navigator.getUserMedia(mediaConfig, function (stream) {
                    video.src = stream;
                    video.play();
                }, errBack);
            } else if (navigator.webkitGetUserMedia) { // WebKit-prefixed
                navigator.webkitGetUserMedia(mediaConfig, function (stream) {
                    video.src = window.webkitURL.createObjectURL(stream);
                    video.play();
                }, errBack);
            } else if (navigator.mozGetUserMedia) { // Mozilla-prefixed
                navigator.mozGetUserMedia(mediaConfig, function (stream) {
                    //video.src = window.URL.createObjectURL(stream);
                    video.srcObject = stream;                    
                    video.play();
                }, errBack);
            }

            $('#snap').click(function (e) {
                context.drawImage(video, 0, 0, 256, 256);
                $('#btnSalvarImagem').attr("disabled", false);
            });

            function saveImagem() {
                $("#loader").show();
                $.post("form/ClientesFormServer.php", "isAjax=S&uploadImage=S&img=" +
                        canvas.toDataURL() + "&txtId=" + $("#txtId").val()).done(function (data) {
                    if (data.trim() === "YES") {
                        $("#loader").hide();
                        var dir = window.location.href;
                        window.location.href = dir.replace("ClientesWebCam.php", "<?php echo $urlReturn; ?>");
                    } else {
                        $("#loader").hide();
                        alert(data);
                    }
                });
            }
        </script>
        <?php odbc_close($con); ?>
    </body>
</html>