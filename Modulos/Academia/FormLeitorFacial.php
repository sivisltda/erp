<?php
include '../../Connections/configini.php';

$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_biometria_facial set " .
        "descricao = " . valoresTexto("txtDescricao") . "," . 
        "ip_leitor = " . valoresTexto("txtIp") . "," . 
        "id_catraca = " . valoresSelect("txtCatraca") . "," . 
        "mac_leitor = " . valoresTexto("txtMac") . " where id_leitor = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {        
        odbc_exec($con, "insert into sf_biometria_facial(descricao, ip_leitor, id_catraca, mac_leitor) values (" .
        valoresTexto("txtDescricao") . "," . valoresTexto("txtIp") . "," . 
        valoresSelect("txtCatraca") . "," . valoresTexto("txtMac") . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_leitor from sf_biometria_facial order by id_leitor desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_biometria_facial WHERE id_leitor = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_biometria_facial where id_leitor = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id_leitor = utf8_encode($RFP['id_leitor']);
        $descricao = utf8_encode($RFP['descricao']);        
        $ip_leitor = utf8_encode($RFP['ip_leitor']);
        $mac_leitor = utf8_encode($RFP['mac_leitor']);        
        $id_catraca = utf8_encode($RFP['id_catraca']);
    }
} else {
    $disabled = '';
    $id_leitor = '';
    $descricao = '';
    $ip_leitor = '';
    $mac_leitor = '';    
    $id_catraca = '';
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script> 
<script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<body>
    <form action="FormLeitorFacial.php" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Leitor Facial</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont">
            <input name="txtId" id="txtId" value="<?php echo $id_leitor; ?>" type="hidden"/>
            <div style="width: 100%;float: left">
                <span>Catraca:</span>
                <select name="txtCatraca" id="txtCatraca" <?php echo $disabled; ?> class="select" style="width:100%" class="input-medium">
                    <option value="null">Selecione</option>
                    <?php $cur = odbc_exec($con, "select id_catraca, nome_catraca from sf_catracas order by id_catraca") or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) { ?>
                        <option value="<?php echo $RFP["id_catraca"]; ?>" <?php echo ($id_catraca == $RFP["id_catraca"] ? "SELECTED" : "");?>><?php echo utf8_encode($RFP["nome_catraca"]); ?></option>
                    <?php } ?>
                </select>
            </div>
            <div style="width: 100%;float: left">
                <span>Descrição:</span>
                <input name="txtDescricao" id="txtDescricao" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="256" value="<?php echo utf8_encode($descricao); ?>"/>
            </div>            
            <div style="width: 100%;float: left">
                <span>Ip (Leitor):</span>
                <input name="txtIp" id="txtIp" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="256" value="<?php echo utf8_encode($ip_leitor); ?>"/>
            </div>
            <div style="width: 100%;float: left">
                <span>Mac (Leitor):</span>
                <input name="txtMac" id="txtMac" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="256" value="<?php echo utf8_encode($mac_leitor); ?>"/>
            </div>            
            <div style="clear:both;"></div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" name="bntSave" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK" ><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else {
                    ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../../js/plugins.js'></script>
    <script type="text/javascript" src="../../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type='text/javascript' src='../../../js/plugins/datatables/fnReloadAjax.js'></script>
    <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script> 
    <script type='text/javascript' src='../../../js/jquery.priceformat.min.js'></script>    
    <script type="text/javascript" src="../../js/util.js"></script>
    <?php odbc_close($con); ?>
</body>