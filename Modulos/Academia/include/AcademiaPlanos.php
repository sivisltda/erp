<input type="hidden" id="txtIdSel"/>
<input type="hidden" id="txtAnoSel"/>
<input type="hidden" id="txtCancelMulta" value="<?php echo $ckb_fin_cancel_multa_; ?>"/>
<input type="hidden" id="txtCancelItemSelect" value="<?php echo $ckb_aca_can_item_; ?>"/>
<input type="hidden" id="txtAcaOneItem" value="<?php echo $ckb_aca_one_item_; ?>"/>
<input type="hidden" id="dcc_multa" value="<?php echo $dcc_multa_; ?>"/> 
<div style="width:100%; margin:10px 0">
    <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Contratos</span>
    <hr style="margin:2px 0; border-top:1px solid #ddd">
</div>
<div style="background:#F1F1F1; border:1px solid #DDD; padding:10px 10px 12px">
    <?php if ($typePage != "Prospect") { ?>
    <div style="float:left; width:calc(86% - 2px); min-height:173px; background:#FFF; border:1px solid #DDD;">
        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblPlanosClientes">
            <thead>
                <tr>
                    <th width="618px">Plano <div style="float: right;">Exibir Ocultos <input id="ckb_planos_ocultos" value="1" type="checkbox" class="input-medium" style="opacity: 0;"></div></th>
                    <th width="20px">Duração(Mês)</th>
                    <th width="187px">Inicio</th>
                    <th width="188px">Fim</th>
                    <th width="139px">Status</th>
                    <th width="70px"><center>Ações</center></th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <?php } ?>
    <div style="float:left;<?php echo ($typePage != "Prospect" ? "width:13%; margin-left:1%;" : ""); ?>">
        <?php if ($ckb_adm_cli_read_ == 0 && $inativo != "2" && $typePage != "Prospect") { ?>
        <button type="button" class="btn dblue" style="width:100%; margin:0 0 5px;background-color: #308698!important" onclick="AbrirBox(2)">
            <span class="ico-plus icon-white"></span> Adicionar
        </button>
        <?php } if ($_SESSION["mod_emp"] == 1 && $typePage != "Prospect") { ?>
        <button type="button" class="btn dblue" style="width:100%; margin:0 0 5px;background-color: #308698!important" onclick="AbrirBox(15)">
            <span class="ico-plus icon-white"></span> Adicionar Avulso
        </button>
        <?php } ?>
        <button type="button" class="btn dblue" style="width:100%; margin:0 0 5px;background-color: #308698!important" onclick="AbrirBox(9)">
            <span class="ico-refresh icon-white"></span> Histórico
        </button>
        <?php if($ckb_adm_cli_read_ == 0){ ?>
        <?php if ($mdl_aca_ == 1) {?>
        <?php if ($inativo != "2") { 
        if ($typePage != "Prospect") { ?>
        <button type="button" class="btn dblue" style="width:100%; margin:0 0 5px;background-color: #308698!important" id="btnCredito">
            <span class="ico-eye-open icon-white"></span> Crédito
        </button>        
        <?php if ($mdl_seg_ == 0) { ?>
            <button type="button" class="btn dblue" style="width:100%; margin:0 0 5px;background-color: #308698!important" id="btnTurma"
            <?php if ($mdl_tur_ > 0) { ?>
                onclick='AbrirBox(15);'
            <?php } else { ?>
                onclick='bootbox.alert("Sem Acesso ao Modulo Turmas");'
            <?php } ?>>
                <span class="ico-file-alt icon-white"></span> Turma
            </button>         
        <?php }}}}} if($ckb_fin_lpg_ == 1) { ?>
        <button id="btnPag" type="button" class="btn dblue" style="height: 40px; width:100%; margin:0 0 5px;background-color: #308698!important">
            <span class="ico-time icon-white"></span> Pagamentos
        </button>
        <?php } ?>
    </div>
    <div style="clear:both; height:5px"></div>
</div>
<div style="clear:both; height:5px"></div>
<div id="divPagamentos" hidden>
    <div style="width:100%; margin:10px 0">
        <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Pagamentos</span>
        <hr style="margin:2px 0; border-top:1px solid #ddd">
    </div>
    <?php if ($typePage != "Prospect") { ?>    
    <div style="width:<?php echo ($mdl_aca_ == 1 && $ckb_aca_pg_prod_ == 1 ? "63" : "100"); ?>%;float: left">
        <button id="btnRenovarMens" onclick="RenovarMens();" disabled title="Renovar Mensalidade Posterior" type="button" 
        class="button button-blue btn-primary" style="float: right;<?php echo ($clb_pagamento_agrupado_ > 0 ? "height: 28px;" : ""); ?>">
            <span class="icon-plus icon-white" style="<?php echo ($clb_pagamento_agrupado_ > 0 ? "background-position: -408px -98px;" : ""); ?>"></span>
        </button>        
        <button id="btnRenovarMensAnt" onclick="RenovarMensAnt();" disabled title="Renovar Mensalidade Anterior" type="button" 
        class="button button-red btn-primary" style="float: right;<?php echo ($clb_pagamento_agrupado_ > 0 ? "height: 28px;" : ""); ?>">
            <span class="icon-minus icon-white" style="<?php echo ($clb_pagamento_agrupado_ > 0 ? "background-position: -408px -98px;" : ""); ?>"></span>
        </button>
        <button id="btnRenovarMensRen" onclick="AbrirBox(3);" disabled title="Renovar Mensalidade" type="button" 
        class="button button-green btn-primary" style="float: right;<?php echo ($clb_pagamento_agrupado_ > 0 ? "height: 28px;" : ""); ?>">
            <span class="icon-refresh icon-white" style="<?php echo ($clb_pagamento_agrupado_ > 0 ? "background-position: -408px -98px;" : ""); ?>"></span>
        </button>
        <?php if ($clb_pagamento_agrupado_ > 0) { ?>
        <div style="float: right; margin-right: 1%;">            
            <select class="select" id="txtPagarTudo"></select>
            <button id="btnPagarTudo" onclick="PagarTudo();" title="Pagar" type="button" class="btn green">Pagar</button>            
        </div>
        <?php } ?>        
        <div class="data-fluid tabbable" style="margin-top:5px;">
            <ul class="nav nav-tabs" style="margin-bottom:0px;width: 100%;">
                <li id="li_pag_1" class="active"><a href="#tab10" data-toggle="tab"><b>Mensalidades</b></a></li>
                <?php if ($mdl_seg_ > 0) { ?>
                <li id="li_pag_3"><a href="#tab19" data-toggle="tab"><b>Cotas</b></a></li>
                <?php } if ($mdl_aca_ == 1) { ?>
                <li id="li_pag_2"><a href="#tab11" data-toggle="tab"><b>Dependentes</b></a></li>
                <li id="li_credito_1" hidden><a href="#tab12" data-toggle="tab"><b>Compra Crédito</b></a></li>
                <li id="li_credito_2" hidden><a href="#tab13" data-toggle="tab"><b>Quitação Carteira</b></a></li>
                <?php } ?>
            </ul>
            <div class="tab-content" style="overflow:initial; border:1px solid #ddd; border-top:0px; padding-top:10px; background:#F6F6F6;min-height: 200px;width: 98.5%;">
                <div class="tab-pane active" id="tab10" style="margin-bottom:5px">
                    <table class="table" style="width: 100%;" cellpadding="0" cellspacing="0" id="tblPagamentosPlano">
                        <thead>
                            <th style="width: 30%">Periodo</th>
                            <th style="width: 16%">a Pagar</th>
                            <th style="width: 16%">Pago</th>
                            <th style="width: 10%">Dt.Venc.</th>
                            <th style="width: 10%">Dt.Pag.</th>
                            <th style="width: 10%">Rec.Por</th>
                            <th style="width: 10%">Ação</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                    <div style="clear:both; height:5px"></div>
                </div>
                <div class="tab-pane" id="tab19" style="margin-bottom:5px">
                    <table class="table" style="width: 100%;" cellpadding="0" cellspacing="0" id="tblPagamentosCotas">
                        <thead>
                            <th style="width: 30%">Periodo</th>
                            <th style="width: 16%">a Pagar</th>
                            <th style="width: 16%">Pago</th>
                            <th style="width: 10%">Dt.Venc.</th>
                            <th style="width: 10%">Dt.Pag.</th>
                            <th style="width: 10%">Rec.Por</th>
                            <th style="width: 10%">Ação</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                    <div style="clear:both; height:5px"></div>
                </div>
                <div class="tab-pane" id="tab11" style="margin-bottom:5px">
                    <table class="table" style="width: 100%;" cellpadding="0" cellspacing="0" id="tblPagamentosDependentes">
                        <thead>
                            <th style="width: 25%"> Dependente </th>
                            <th style="width: 18%"> Plano </th>
                            <th style="width: 19%"> Periodo </th>
                            <th style="width: 10%"> a Pagar </th>
                            <th style="width: 10%"> Pago </th>
                            <th style="width: 10%"> Dt Venc </th>
                            <th style="width: 8%"> Ação </th>
                        </thead>
                        <tbody></tbody>
                    </table>
                    <div style="clear:both; height:5px"></div>
                </div>
                <div class="tab-pane" id="tab12" style="margin-bottom:5px" hidden>
                    <table class="table" cellpadding="0" cellspacing="0" width="50%" style="width: 30%">
                        <thead>
                            <tr>
                                <th width="10%">Valor Total:</th>
                                <th width="5%">Ação:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input id="txtValCredito" type="text" class="input-xlarge inputCenter" value="<?php echo escreverNumero(0); ?>"/></td>
                                <td style="vertical-align: middle;">
                                    <center>
                                        <button type="button" class="btn green" style="line-height:10px;margin:0 0 1px;padding: 5px 5px 4px 5px;" onClick="validaCredito()"/>
                                        <span>Pagar</span></button>
                                        <div style="text-align:left;margin-left: 5px;" class="btn-group">
                                            <button style="height: 13px;background-color: gray;" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                            <span style="margin-top: 1px;" class="caret"></span>
                                            </button>
                                            <?php if ($ckb_aca_abocre_ == 1) { ?>
                                            <ul style="left:-130px;" class="dropdown-menu">
                                                <li><a onclick="AbonarCredito()" class="Abonar">Abonar</a></li>
                                            </ul>
                                            <?php } ?>
                                        </div>                                                        
                                    </center>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab13" style="margin-bottom:5px;height: 192px; overflow: auto;">
                    <table class="table" style="width: 100%;" cellpadding="0" cellspacing="0" id="tblExtratoCreditos">
                        <thead>
                        <th style="width: 10%"> Tipo </th>
                        <th style="width: 10%"> Data Movimentação</th>
                        <th style="width: 5%"> Valor </th>
                        <th style="width: 10%"> Data Vencimento </th>
                        <th style="width: 40%"> Histórico </th>
                        <th style="width: 10%"> Data Pagamento </th>
                        <th style="width: 5%"> Ação </th>
                        </thead>
                        <tbody >
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php } if ($mdl_aca_ == 1) { ?>
    <div style="<?php echo ($ckb_aca_pg_prod_ == 1 ? "" : "display: none;");?><?php echo ($typePage != "Prospect" ? "float: left;margin-left: 1.5%;width: 34.5%;" : ""); ?>">
        <div class="data-fluid tabbable" style="margin-top:10px;">
            <ul class="nav nav-tabs" style="margin-bottom:0px;width: 102.6%;">
                <li id="li_pag_8" class="active"><a href="#tab14" data-toggle="tab"><b>Serviços</b></a></li>
                <li id="li_pag_9"><a href="#tab15" data-toggle="tab"><b>Produtos</b></a></li>
                <li id="li_pag_10"><a href="#tab16" data-toggle="tab"><b>Histórico</b></a></li>
                <li id="li_pag_11"><a href="#tab27" data-toggle="tab"><b>Boletos</b></a></li>
                <li id="li_pag_12"><a href="#tab28" data-toggle="tab"><b>Links</b></a></li>
            </ul>
            <div class="tab-content" style="overflow:initial; border:1px solid #ddd; border-top:0px; padding-top:10px; background:#F6F6F6;min-height: 195px;width: 98%;">
                <div class="tab-pane active" id="tab14" visible="<?php echo $ckb_aca_pg_prod_;?>" style="margin-bottom:5px">
                    <div>
                        <table class="table" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="48%">Serviços</th>
                                    <th width="10%">Qtd</th>
                                    <th width="16%">Unit</th>
                                    <th width="16%">Total</th>
                                    <th width="10%">Ação</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div style="width:100%; float:left">
                                            <select class="select" style="width:100%" id="txtServico">
                                                <option value="null" avl="0">--Selecione--</option>
                                                <?php
                                                $sql = "select conta_produto,descricao,alterar_valor from sf_produtos where conta_produto > 0 AND tipo = 'S' and inativa = 0  ORDER BY descricao";
                                                $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                                    <option value="<?php echo $RFP['conta_produto'] ?>" avl="<?php echo $RFP['alterar_valor']; ?>"><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td><input id="txtQtdServ" type="text" class="input-xlarge inputCenter" value="1" onchange="atValServ()" /></td>
                                    <td><input id="txtValServ" type="text" class="input-xlarge inputCenter" value="<?php echo escreverNumero(0); ?>" onchange="atValServ()" /></td>
                                    <td><input id="txtValServFinal" type="text" class="input-xlarge inputCenter" value="<?php echo escreverNumero(0); ?>" disabled="" /></td>
                                    <td style="vertical-align: middle;">
                                        <center>
                                            <button type="button" class="btn green pgserv" style="line-height:10px;margin:0 0 1px;padding: 5px 5px 4px 5px;" onClick="incluirItemPag('SERVIÇO', '', '')" />
                                            <span>Pagar</span>
                                            </button>
                                        </center>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table" style="width: 100%;" cellpadding="0" cellspacing="0" id="tblServicoFavorito">
                            <thead>
                            <th style="width: 15%"> Serviço </th>
                            <th style="width: 15%"> Valor </th>
                            <th style="width: 15%"> Dt Pag. </th>
                            <th width="5%">Ação</th>
                            </thead>
                            <tbody >
                            </tbody>
                        </table>
                        <div style="clear:both; height:5px"></div>
                    </div>
                </div>                
                <div class="tab-pane" id="tab15" style="margin-bottom:5px">
                    <div>
                        <table class="table" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="44%">Produtos</th>
                                    <th width="10%">Qtd</th>
                                    <th width="16%">Unit</th>
                                    <th width="16%">Total</th>
                                    <th width="10%">Ação</th>                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div style="width:100%; float:left">
                                            <select class="select" style="width:100%" id="txtProduto" >
                                                <option value="null" blk="0" avl="0">--Selecione--</option>
                                                <?php
                                                $idFilial = 0;
                                                $query = "select ISNULL(filial, (SELECT id_filial FROM dbo.sf_filiais where id_filial = 1)) filial from dbo.sf_usuarios where id_usuario = '" . $_SESSION["id_usuario"] . "'";
                                                $cur = odbc_exec($con, $query) or die(odbc_errormsg());
                                                while ($RFP = odbc_fetch_array($cur)) {
                                                    $idFilial = $RFP['filial'];
                                                }                                                 
                                                //dbo.ESTOQUE_FILIAL(conta_produto," . $idFilial . ")
                                                $sql = "select conta_produto,descricao,alterar_valor, nao_vender_sem_estoque, 1 estoque_atual from sf_produtos where conta_produto > 0 AND tipo = 'P' and inativa = 0  ORDER BY descricao";
                                                $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                                    <option value="<?php echo $RFP['conta_produto'] ?>" blk="<?php echo ($RFP['nao_vender_sem_estoque'] == '1' && $RFP['estoque_atual'] <= 0 ? "1" : "0"); ?>" avl="<?php echo $RFP['alterar_valor']; ?>"><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td><input id="txtQtdProd" type="text" class="input-xlarge inputCenter" value="1" onchange="atValProd();" /></td>
                                    <td><input id="txtValProd" type="text" class="input-xlarge inputCenter" value="<?php echo escreverNumero(0); ?>" onchange="atValProd();" /></td>
                                    <td><input id="txtValProdFinal" type="text" class="input-xlarge inputCenter" value="<?php echo escreverNumero(0); ?>" disabled="" /></td>
                                    <td style="vertical-align: middle;">
                                        <center>
                                            <button id="btnPagarProduto" type="button" class="btn green pgserv" style="line-height:10px;margin:0 0 1px;padding: 5px 5px 4px 5px;" onClick="incluirItemPag('PRODUTO', '', '');"/>
                                                <span>Pagar</span>
                                            </button>
                                        </center>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table" style="width: 100%;" cellpadding="0" cellspacing="0" id="tblProdutoFavorito">
                            <thead>
                                <th style="width: 15%"> Produto </th>
                                <th style="width: 15%"> Valor </th>
                                <th style="width: 15%"> Dt Pag. </th>
                                <th width="5%">Ação</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both; height:5px"></div>
                    </div>
                </div>                
                <div class="tab-pane" id="tab16" style="margin-bottom:5px">
                    <div>                        
                        <table class="table" style="width: 100%;" cellpadding="0" cellspacing="0" id="tblServicoHistorico">
                            <thead>
                            <th style="width: 15%"> Item </th>
                            <th style="width: 15%"> Valor </th>
                            <th style="width: 15%"> Dt Pag. </th>
                            <th width="5%">Ação</th>
                            </thead>
                            <tbody >
                            </tbody>
                        </table>
                        <div style="clear:both; height:5px"></div>
                    </div>
                </div>
                <div class="tab-pane" id="tab27" style="margin-bottom:5px">
                    <div>                                            
                        <button style="float: left;" type="button" id="btnNovoBoleto" title="Gerar boleto" class="button button-green btn-primary"><span class="ico-barcode icon-white"></span> Gerar boleto</button>
                        <span style="float: right;">                                           
                            De <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin_boleto" value="<?php echo getData("T"); ?>" placeholder="Data inicial">
                            até <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end_boleto" value="<?php echo getData("E"); ?>" placeholder="Data Final">
                            <button id="btnfindBoleto" class="button button-turquoise btn-primary buttonX" type="button" title="Buscar"><span class="ico-search icon-white"></span></button>
                        </span>                                        
                        <table class="table" style="width: 100%;" cellpadding="0" cellspacing="0" id="tblBoletoHistorico">
                            <thead>
                            <th style="width: 15%"> Dt. Venc. </th>
                            <th style="width: 15%"> Valor </th>
                            <th style="width: 15%"> Status. </th>
                            <th width="5%">Ação</th>
                            </thead>
                            <tbody >
                            </tbody>
                        </table>
                        <div style="clear:both; height:5px"></div>
                    </div>
                </div>
                <div class="tab-pane" id="tab28" style="margin-bottom:5px">
                    <div>                                            
                        <button style="float: left;" type="button" id="btnNovoLink" title="Gerar Link" class="button button-green btn-primary"><span class="ico-barcode icon-white"></span> Gerar Link</button>
                        <span style="float: right;">                                           
                            De <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin_link" value="<?php echo getData("T"); ?>" placeholder="Data inicial">
                            até <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end_link" value="<?php echo getData("E"); ?>" placeholder="Data Final">
                            <button id="btnfindLink" class="button button-turquoise btn-primary buttonX" type="button" title="Buscar"><span class="ico-search icon-white"></span></button>
                        </span>                                        
                        <table class="table" style="width: 100%;" cellpadding="0" cellspacing="0" id="tblLinkHistorico">
                            <thead>
                                <th style="width: 15%">Dt. Venc.</th>
                                <th style="width: 15%">Valor</th>
                                <th style="width: 15%">Nº Max.</th>
                                <th style="width: 15%">Status.</th>
                                <th width="5%">Ação</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both; height:5px"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>            
    <?php } ?>
    <div style="clear:both; height:5px"></div>
    <div style="width:100%; margin:10px 0">
        <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Itens a serem pagos</span>
        <hr style="margin:2px 0; border-top:1px solid #ddd">
    </div>
    <table style="width: 100%;">
        <tr>
            <td width="65%" style="vertical-align: top;">
                <input type="hidden" value="" id="txtQtdItensPagar"></input>
                <table class="table" style="margin-top:-7px;" cellpadding="0" cellspacing="0" width="50%" id="tblItensPag">
                    <thead>
                        <tr>
                            <th width="12%">Tipo</th>
                            <th width="30%">Descrição</th>
                            <th width="5%">Qtd</th>
                            <th width="10%">Valor</th>
                            <th width="20%">Desconto</th>
                            <th width="15%">V. Final</th>
                            <th width="5%"></th>
                        </tr>
                    </thead>
                    <tbody id="tabela_Itens">
                    </tbody>
                </table>
            </td>
            <td width="40%" style="padding-left: 3%;vertical-align: bottom;">
                <div id="txtTotalServ" style="width:100%;">
                    <table>
                        <tr>
                            <td style="width: 150px;">
                                <span style="font-size: 20px;">Sub Total:</span>
                            </td>
                            <td style="padding-left: 5px; font-size: 16px;">
                                <span style="font-size: 25px;" id="lblTotalSemDesconto"><?php echo escreverNumero(0,1); ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span style="font-size: 20px;">Desconto:</span>
                            </td>
                            <td style="padding-left: 5px; font-size: 16px;">
                                <span style="font-size: 25px;" id="lblTotalDesconto"><?php echo escreverNumero(0,1); ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span style="font-size: 20px;">Acréscimo:</span>
                            </td>
                            <td style="padding-left: 5px; font-size: 16px;">
                                <span style="font-size: 25px;" id="lblTotalAcrescimo"><?php echo escreverNumero(0,1); ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span style="font-size: 20px;">Total a Pagar:</span>
                            </td>
                            <td style="padding-left: 5px;font-size: 16px;color:red;">
                                <span style="font-size: 30px;font-weight: bold;" id="lblTotal"><?php echo escreverNumero(0,1); ?><br></span>
                                <input type="hidden" id="txtTotal"/>
                            </td>
                        </tr>
                    </table>
                </div>
                </br>
                <div>
                    <button type="button" class="btn green" style="line-height:50px; width:85%; margin:0 0 5px;font-size: 24px;" id="btnEfetuarPag" onclick="AbrirBox(1)" disabled>
                        <span class="ico-refresh icon-white"></span> &nbsp;Efetuar Pagamento
                    </button>
                </div>
            </td>
        </tr>
    </table>    
</div>