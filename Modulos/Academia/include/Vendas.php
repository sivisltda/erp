<div class="span12" style="text-align: right;">      
    <button style="float: left;" type="button" name="btnNovo_vendas" id="btnNovo_vendas" title="Novo" class="button button-green btn-primary" onClick="window.open('../../../Modulos/Estoque/FormVendas.php?com=<?php echo $id_comanda; ?>&ps=<?php echo $id_pessoa; ?>', '_blank');"><span class="ico-file-4 icon-white"></span></button>                                            
    <span style="padding-left:0px">                                           
        De <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin_vendas" name="txt_dt_begin_vendas" value="<?php echo getData("B"); ?>" placeholder="Data inicial">
        até <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end_vendas" name="txt_dt_end_vendas" value="<?php echo getData("E"); ?>" placeholder="Data Final">
        <button id="btnfind_vendas" name="btnfind_vendas" class="button button-turquoise btn-primary buttonX" type="button" title="Buscar"><span class="ico-search icon-white"></span></button>
    </span>                                        
</div>
<div style="clear:both;"></div>
<table class="table" cellpadding="0" style="margin-top: 15px;" cellspacing="0" width="100%" id="tbVendas">
    <thead>
        <tr>      
            <th width="10%"><center>Data</center></th>
<th width="25%">Cliente</th>
<th width="25%"><center>Histórico</center></th>
<th width="10%" style="text-align:right">Tot.Produtos</th>
<th width="10%" style="text-align:right">Tot.Serviços</th>
<th width="10%" style="text-align:right">Tot.Geral</th>
<th width="10%"><center>Status:</center></th>
</tr>
</thead>
<tbody>  
    <tr>
        <td colspan="11" class="dataTables_empty">Carregando dados do Cliente</td>
    </tr>   
</tbody>
</table>
<table id="lblTotParc_vendas" name="lblTotParc_vendas" class="table" cellpadding="0" cellspacing="0" width="100%">                                            
    <tr>
        <td style="text-align: right">
            <div id="lblTotParc2_vendas" name="lblTotParc2_vendas">
                Número de Vendas :<strong>   0</strong>
            </div>
        </td>
    </tr>
    <tr>
        <td style="text-align: right">
            <div id="lblTotParc3_vendas" name="lblTotParc3_vendas">
                Total em Vendas :<strong><?php echo escreverNumero(0, 1); ?></strong>
            </div>
        </td>
    </tr>
    <tr>
        <td style="text-align: right">
            <div id="lblTotParc4_vendas" name="lblTotParc4_vendas">
                Ticket Médio :<strong><?php echo escreverNumero(0, 1); ?></strong>
            </div>            
        </td>
    </tr>
</table>