<span style="color:#000; font-size:14px; text-shadow:0 1px #FFF; padding-left:5px">Veículos</span>
<hr style="margin:2px 0; border-top:1px solid #DDD">
<div style="width:100%;">    
    <div style="background:#F1F1F1; border:1px solid #DDD; padding:10px;vertical-align: top;">
        <?php if($ckb_adm_cli_read_ == 0 || $ckb_pro_cad_veic_ == 1) { ?>
        <button class="button button-green btn-primary" type="button" onClick="AbrirBoxVeiculo(0, <?php echo $id; ?>);"><span class="ico-file-4 icon-white"></span></button>
        <?php } ?>
    </div>
</div>
<table class="table" cellpadding="0" cellspacing="0" width="100%" id="tbListaVeiculo">
    <thead>
        <tr>
            <th width="2%"></th>
            <th width="7%">Placa:</th>
            <th width="16%">Marca:</th>                                               
            <th width="21%">Modelo:</th>                                                
            <th width="7%">Ano:</th>                                                
            <th width="10%">Comb.:</th>                                                
            <th width="7%">Valor</th>                                    
            <th width="6%">Plano:</th>
            <th width="6%">V.Plano:</th>
            <th width="6%">V.Add.:</th>                                    
            <th width="6%">Validade:</th>
            <th width="6%">Status:</th>
        </tr>
    </thead>
    <tbody></tbody>
</table> 
<div style="clear:both; height:5px"></div>
<hr>
<span style="color:#000; font-size:14px; text-shadow:0 1px #FFF; padding-left:5px">Vistorias</span>
<hr style="margin:2px 0; border-top:1px solid #DDD">
<div style="width:100%;">
    <div style="background:#F1F1F1; border:1px solid #DDD; padding:10px;vertical-align: top;">
        <?php if($ckb_adm_cli_read_ == 0) { ?>
        <button class="button button-green btn-primary" type="button" onClick="AbrirBoxVistorias(0)"><span class="ico-file-4 icon-white"></span></button>
        <?php } ?>        
    </div>
</div>
<table class="table" cellpadding="0" cellspacing="0" width="100%" id="tbListaVistorias">
    <thead>
        <tr>
            <th width="11%">Data:</th>    
            <th width="10%">Tipo:</th>    
            <th width="8%">Usuário:</th>                                
            <th width="9%">Placa:</th>
            <th width="11%">Marca:</th>                                               
            <th width="21%">Modelo:</th>                                                
            <th width="7%">Ano:</th>                                                
            <th width="9%">Comb.:</th>                                                
            <th width="8%">Valor</th>  
            <th width="6%">Status:</th>            
        </tr>
    </thead>
    <tbody></tbody>
</table> 
<div style="clear:both; height:5px"></div>