<div style="width:100%; float:left" id="textoProv">
    <div class="alert alert-warning" style="cursor: text; height: 42px;color: #8a6d3b !important; background: #fcf8e3 !important;border: 1px solid #FBEEC5 !important;">
        <p>- O provisório cancela o acesso do cartão definitivo.</p>
        <p>- Os códigos de identificação (cartões ou impressões digitais) serão detectados automaticamente pelo sistema.</p>
    </div>
</div>
<div style="width:100%; float:left">
    <div style="padding:19px 0 0; float:left">
        <div class="radio"><input type="radio" <?php if ($acessoTipo == 1) { echo "checked";} ?> name="rAcTipo" value="1" style="opacity: 0;"></div>Definitivo
        <div class="radio"><input type="radio" <?php if ($acessoTipo == 0) { echo "checked";} ?> name="rAcTipo" value="0" style="opacity: 0;"></div>Provisório
    </div>
    <?php if ($cat_topdata_lista > 0) { ?>
        <div style="float:left; width:19%;margin-left: 1%;margin-top: 19px;">
            <input id="ckb_excecoes_digitais" name="ckb_excecoes_digitais" type="checkbox" value="1" <?php if ($excecoes_digitais == 1) { echo "checked"; } ?> disabled/> Lista de exceções de Digitais
        </div>
    <?php } ?>
    <div style="clear:both; height:5px"></div>
    <div style="width:12%; float:left;">
        <span>Número Provisório:</span>
        <input type="text" id="txtAcNumProvisorio" class="input-medium" maxlength="16" value="<?php echo $acessoNumProv; ?>" style="width:100%"/>
    </div>
    <div style="width:80%; float:left;margin-left:1%">
        <span>Mensagem para tela de acesso:</span>
        <input type="text" id="txtAcMsg" class="input-medium" maxlength="256" value="<?php echo $acessomsg; ?>" style="width:100%"/>
    </div>
    <div style="width:6%; float:left; margin-left:1%">
        <?php if($ckb_adm_cli_read_ == 0){?>        
            <button class="btn btn-success" type="button" id="btnSalvarAcesso" style="margin-top: 16px;height: 26px"><span class="ico-checkmark"></span> Gravar</button>
        <?php } ?>            
    </div>
</div>
<div style="clear:both; height:5px"></div>
<div style="width:100%; margin:10px 0">
    <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Atribuir Credencial</span>
    <hr style="margin:2px 0; border-top:1px solid #ddd">
</div>
<?php if ($clb_pagamento_credencial_ > 0 && ($idTitular == "" && $idDependente == "")) { ?>
<div class="alert alert-warning" style="cursor: text; height: 42px;color: #8a6d3b !important; background: #fcf8e3 !important;border: 1px solid #FBEEC5 !important;">
    <p>- A Obrigatoriedade de Pagamento para Credencial está ativa.</p>
    <p>- É necessário vincular este cliente a um responsável ou adicionar seus dependentes caso seja o titular.</p>
</div>
<?php } else { ?>
<?php if ($ckb_aca_add_credencial_ > 0) { ?>
    <div id="divCredencial" style="border-style: solid;border-width: 1px; padding: 7px;border-color:#c8c8c8;">       
        <div style="width:20%; float:left">
            <span> Modelo:</span>
            <select id="txtCredModelo" class="select" style="width:100%">
                <option value="">Selecione</option>
                <?php $cur = odbc_exec($con, "select id_credencial,descricao,dias from sf_credenciais where inativo = 0 order by descricao") or die(odbc_errormsg());
                while ($RFP = odbc_fetch_array($cur)) { ?>
                    <option value="<?php echo $RFP['id_credencial']; ?>" dias="<?php echo $RFP['dias']; ?>"><?php echo utf8_encode($RFP['descricao']); ?></option> 
                <?php } ?>
            </select>
        </div>
        <div style="width:12%; float:left;margin-left: 1%">
            <span>Inicio:</span>
            <input type="text" id="txtCredDtIni" class="datepicker inputCenter"/>
        </div>
        <div style="width:12%; float:left;margin-left: 1%">
            <span>Fim:</span>
            <input type="text" id="txtCredDtFim" class="<?php echo ($ckb_aca_dtf_credencial_ == 0 ? "" : "datepicker");?> inputCenter" <?php echo ($ckb_aca_dtf_credencial_ == 0 ? "readonly" : "");?>/>
        </div>
        <div style="width:<?php echo ($clb_pagamento_credencial_ > 0 ? "23%" : "43%"); ?>;float:left;margin-left: 1%">
            <span>Motivo</span>
            <input type="text" id="txtCredMotivo" class="input-medium" style="width:100%"/>
        </div>        
        <?php if ($clb_pagamento_credencial_ > 0) { ?>
        <div style="width:20%; float:left;margin-left: 1%">
            <span> Pagamento:</span>
            <input name="txtIdTitular" id="txtIdTitular" value="<?php echo $idTitular; ?>" type="hidden"/>
            <select id="txtCredPagamento" class="select" style="width:100%"></select>
        </div>        
        <?php } else { ?>
            <input name="txtCredPagamento" id="txtCredPagamento" value="" type="hidden"/>
        <?php } ?>        
        <div style="width:75px; float:left;margin-left:1%;margin-top:15px;">
            <?php if($ckb_adm_cli_read_ == 0){?>        
                <button type="button" id="btnIncluirCredencial" class="btn dblue" style="line-height:19px; width:75px; margin:0;background-color: #308698 !important;">Incluir <span class="icon-arrow-next icon-white"></span></button>
            <?php } ?>                
        </div>
        <div style="clear:both; height:5px"></div>
    </div>
<?php } ?>
<div style="clear:both; height:5px"></div>
<div style="position: relative;">
    <div style="max-height: 150px;overflow: auto;width: 100%;float: left;">
        <table  id="tblCredenciais" class="table" cellpadding="0" cellspacing="0" width="100%" style="display: table;">
            <thead>
                <tr>
                    <th style="width: 15%"> De </th>
                    <th style="width: 15%"> Até </th>
                    <th style="width: 20%"> Tipo </th>
                    <th style="width: 30%"> Motivo </th>
                    <th style="width: 15%"> Criado/Cancelado por </th>
                    <th style="width: 10%"> Ação</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
<?php } ?>
<div style="clear:both; height:5px"></div>
<div style="width:100%; margin:10px 0">
    <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Atribuir Bloqueios</span>
    <hr style="margin:2px 0; border-top:1px solid #ddd">
</div>
<?php if ($ckb_aca_add_bloqueios_ > 0) { ?>
    <div id="divBloqueios" style="border-style: solid;border-width: 1px; padding: 7px;border-color:#c8c8c8;">
        <div style="width:12%; float:left;margin-left: 1%">
            <span>Inicio:</span>
            <input type="text" id="txtBlqDtIni" class="datepicker inputCenter"/>
        </div>
        <div style="width:12%; float:left;margin-left: 1%">
            <span>Fim:</span>
            <input type="text" id="txtBlqDtFim" class="datepicker inputCenter"/>
        </div>
        <div style="width:63%; float:left;margin-left: 1%">
            <span>Motivo</span>
            <input type="text" id="txtBlqMotivo" class="input-medium" style="width:100%"/>
        </div>
        <div style="width:75px; float:left;margin-left:1%;margin-top:15px;">
            <?php if($ckb_adm_cli_read_ == 0){?>
                <button type="button" id="btnIncluirBloqueios" class="btn dblue" style="line-height:19px; width:75px; margin:0;background-color: #308698 !important;">Incluir <span class="icon-arrow-next icon-white"></span></button>
            <?php } ?>
        </div>
        <div style="clear:both; height:5px"></div>
    </div>
<?php } ?>
<div style="clear:both; height:5px"></div>
<div style="position: relative;">
    <div style="max-height: 150px;overflow: auto;width: 100%;float: left;">
        <table  id="tblBloqueios" class="table" cellpadding="0" cellspacing="0" width="100%" style="display: table;">
            <thead>
                <tr>
                    <th style="width: 15%"> De </th>
                    <th style="width: 15%"> Até </th>
                    <th style="width: 35%"> Motivo </th>
                    <th style="width: 25%"> Criado/Cancelado por </th>
                    <th style="width: 10%"> Ação</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
<div style="clear:both; height:5px"></div>
<div style="width:100%; margin:10px 0">
    <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Documentos</span>
    <hr style="margin:2px 0; border-top:1px solid #ddd">
</div>
<?php if ($ckb_aca_add_credencial_ > 0) { ?>
    <div id="divCredencial" style="border-style: solid;border-width: 1px; padding: 7px;border-color:#c8c8c8;">
        <div style="width:20%; float:left">
            <span> Documento:</span>
            <select id="txtDocModelo" class="select" style="width:100%"  >
                <option value="">Selecione</option>
            </select>
        </div>
        <div style="width:12%; float:left;margin-left: 1%">
            <span>Inicio:</span>
            <input type="text" id="txtDocDtIni" class="datepicker inputCenter"/>
        </div>
        <div style="float:left; width:75px; margin-left:1%;margin-top:15px;">
            <?php if($ckb_adm_cli_read_ == 0) { ?>
                <button type="button" id="btnIncluirDocumento" class="btn dblue" style="line-height:19px; width:75px; margin:0;background-color: #308698 !important;">Incluir <span class="icon-arrow-next icon-white"></span></button>
            <?php } ?>
        </div>
        <div style="clear:both; height:5px"></div>
    </div>
<?php } ?>
<div style="clear:both; height:5px"></div>
<div style="position: relative;">
    <div style="max-height: 150px;overflow: auto;width: 100%;float: left;">
        <table  id="tblDocumentos" class="table" cellpadding="0" cellspacing="0" width="100%" style="display: table;">
            <thead>
                <tr>
                    <th style="width: 22.5%;"> Descrição </th>
                    <th style="width: 22.5%;"> Dias </th>
                    <th style="width: 22.5%;"> Data Inserido </th>
                    <th style="width: 22.5%;"> Validade </th>
                    <th style="width: 10%;"> Ações </th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
<div style="clear:both; height:5px"></div>
<div style="width:100%; margin:10px 0">
    <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Listagem de Acessos</span>
    <hr style="margin:2px 0; border-top:1px solid #ddd">
</div>
<div style="width:50%; float:left;">
    <span>De</span>
    <input type="text" class="datepicker" style="width:118px; margin-bottom:2px" id="txtDtIni" name="txtDtIni" value="<?php echo $dtIni; ?>" placeholder="Data inicial"/>
    <span>até</span>
    <input type="text" class="datepicker" style="width:118px; margin-bottom:2px" id="txtDtFim" name="txtDtFim" value="<?php echo $dtFim; ?>" placeholder="Data Final"/>
    <input type="button" title="Buscar" class="btn btn-primary" style="height:26px;margin-left: 1%" onclick="listAcesso();" value="Buscar">
</div>
<div style="clear:both;    height: 5px;"></div>
<div style="position: relative;">
    <div style="width: 50%;float: left;">
        <table class="table" id="tblAcesso">
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Hora</th>
                    <th>Status</th>
                    <th>Ambiente</th>
                    <th>Catraca</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <div id="chartTurno" style="width: 45%;height: 300px;font-size: 11px;"></div>
</div>
<div style="clear:both"></div>
<div style="width:100%; margin:10px 0">
    <div style="width:100%; margin:10px 0">
        <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Últimos Acessos</span>
        <hr style="margin:2px 0; border-top:1px solid #ddd">
    </div>
    <div id="chartDiario"  style="width: 100%;height: 300px;font-size: 11px;"></div>
</div>