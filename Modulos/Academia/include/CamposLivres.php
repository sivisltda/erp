<div style="float: left; width: 50%;">
    <div style="width:100%; margin:10px 0">
        <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Cadastro</span>
        <hr style="margin:2px 0; border-top:1px solid #ddd">
    </div>
    <div style="margin-top:5px;float: left;width: calc(100% - 14px); border-style: solid;border-width: 1px; padding: 7px;border-color:#c8c8c8;">
        <?php
        $cur = odbc_exec($con, "select * from sf_configuracao_campos_livres left join 
        sf_fornecedores_despesas_campos_livres on id_campo = campos_campos and fornecedores_campos = " . valoresNumericos2($id)) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) { ?>
            <div style="float: left;width: 100%;margin-bottom: 5px;">
                <span><b><?php echo $RFP['descricao_campo'] ?></b></span>
                <input id="txtCLid_<?php echo $RFP['id_campo'] ?>" name="txtCLid" type="hidden" value="<?php echo $RFP['id_fornecedores_campo'] ?>"/>
                <input id="txtCLConteudo_<?php echo $RFP['id_campo'] ?>" name="txtCLConteudo" value="<?php echo $RFP['conteudo_campo'] ?>" <?php echo ($RFP['ativo_campo'] == "1" ? "" : "readonly"); ?> type="text" maxlength="256"/>
            </div>
        <?php } ?>
        <div style="clear:both;height: 5px;"></div>
        <?php if ($ckb_adm_cli_read_ == 0) { ?>
            <button id="btnSaveCLivres" type="button" class="btn dblue" style="float: right;line-height:19px; width:75px;margin-top: 1px;background-color: #308698 !important;"> Salvar <span class="icon-arrow-next icon-white"></span></button> 
        <?php } ?>
    </div>     
</div>
<?php if ($comissao_forma_ > 0 && $ckb_com_forma_ > 0) { ?>
<div style="float: left; width: 49%; margin-left: 1%;">
    <div style="width:100%; margin:10px 0">
        <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Comissão por Cliente</span>
        <hr style="margin:2px 0; border-top:1px solid #ddd">
    </div>
    <div style="margin-top:5px;float: left;width: calc(100% - 14px); border-style: solid;border-width: 1px; padding: 7px;border-color:#c8c8c8;">
        <div style="width:87%; float:left;">
            <span style="width: 100%">Comissão padrão por Cliente:</span>
            <select id="txtTipoComissaoPadrao" class="select" style="width: 100%">
                <option value="null">Selecione</option>
                <?php $cur = odbc_exec($con, "select * from sf_comissao_cliente_padrao where inativo = 0 order by descricao") or die(odbc_errormsg());
                while ($RFP = odbc_fetch_array($cur)) { ?>
                    <option value="<?php echo $RFP['id'] ?>"><?php echo utf8_encode($RFP['descricao']); ?></option>
                <?php } ?>                
            </select>
        </div>
        <div style="width:12%; float:left; margin-left:1%">
            <button class="btn green" type="button" onclick="novaLinhaPadrao()" style="width:100%; height:27px; margin-top: 15px;"><span class="ico-plus icon-white"></span></button>
        </div>
    </div>
    <div style="margin-top:5px;float: left;width: calc(100% - 14px); border-style: solid;border-width: 1px; padding: 7px;border-color:#c8c8c8;">
        <input id="txtIdComissao" value="" type="hidden"/>
        <div style="width: calc(46% - 70px); float:left;">
            <span style="width: 100%">Responsável:</span>
            <select id="txtTipoResponsavel" class="select" style="width: 100%">
                <option value="null">Selecione</option>
                <?php $cur = odbc_exec($con, "select id_usuario, nome from sf_usuarios where inativo = 0 and id_usuario > 1 order by nome") or die(odbc_errormsg());
                while ($RFP = odbc_fetch_array($cur)) { ?>
                    <option value="<?php echo $RFP['id_usuario'] ?>"><?php echo formatNameCompact(strtoupper(utf8_encode($RFP['nome']))); ?></option>
                <?php } ?>                
            </select>
        </div>
        <div style="width:25%; float:left; margin-left:1%">
            <span>Tipo:</span>
            <select id="txtTipoComissao" class="select" style="width: 100%">
                <option value="null">Selecione</option>
                <?php for ($i = 0; $i < 4; $i++) { ?>
                <option value="<?php echo $i; ?>"><?php echo getTypeComissao($i, true); ?></option>
                <?php } ?>
            </select>
        </div>
        <div style="float:left; margin-left:1%;">
            <span style="width:100%; display:block;">&nbsp;</span>            
            <span style="width:100%; display:block;">
                <div class="btn-group"><button class="btn btn-primary" id="btnTpValorDesc" type="button" style="width:35px; padding-bottom:3px; text-align:center; background:#308698 !important">
                <span id="tp_sinalDesc">N</span></button></div>
                <input id="txtSinalDesc" value="0" type="hidden">
            </span>
        </div>         
        <div style="width:14%; float:left;">
            <span style="width: 100%">Valor(%):</span>
            <input type="text" id="txtValorComissao" value="0,00" class="input-medium">
        </div> 
        <div style="float:left">
            <span style="width:100%; display:block;">&nbsp;</span>
            <span style="width:100%; display:block;">
                <div class="btn-group"><button class="btn btn-primary" id="btnTpValor" type="button" style="width:35px; padding-bottom:3px; text-align:center; background:#308698 !important">
                <span id="tp_sinal">%</span></button></div>
                <input id="txtSinal" value="0" type="hidden">
            </span>
        </div>        
        <div style="width:12%; float:left; margin-left:1%">
            <button class="btn green" type="button" onclick="novaLinhaComissao()" style="width:100%; height:27px; margin-top: 15px;"><span class="ico-plus icon-white"></span></button>
        </div>
    </div>
    <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblComissao" cobg="<?php echo $ckb_adm_comissao_; ?>">
        <thead>
            <tr>
                <th width="46%">Responsável:</th>
                <th width="25%">Tipo:</th>
                <th width="16%">Valor:</th>
                <th width="13%">Ação:</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table> 
    <div style="clear:both; height:5px"></div>
</div>
<?php } ?>
<div style="clear:both;height: 5px;"></div>