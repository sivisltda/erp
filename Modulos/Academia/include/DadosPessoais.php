<div id="divDadosPessoais">
    <div style="width:11%; float:left">
        <span>Tipo</span>
        <select class="select" style="width:100%;" id="txtJuridicoTipo" name="txtJuridicoTipo">
            <option value="F" <?php echo ($juridico_tipo == "F" ? "SELECTED" : ""); ?>>Física</option>
            <?php //if ($mdl_seg_ == 0 || $juridico_tipo == "J") { ?>
            <option value="J" <?php echo ($juridico_tipo == "J" ? "SELECTED" : ""); ?>>Jurídica</option>
            <?php //} ?>
        </select>
    </div>                                                        
    <div style="width:18%; float:left; margin-left:1%">
        <span>Documento CPF:</span>
        <input type="text" id="txtCpf" cobg="<?php echo $ckb_adm_cpf_obrig_; ?>" cobgLimit="<?php echo $txt_adm_cpf_limite_; ?>" name="txtCpf" value="<?php echo $cpf; ?>" class="input-medium"/>
    </div>
    <div style="width:15%; float:left; margin-left:1%">
        <span><?php echo ($estrang == "1" ? "Passaporte/ID" : "Documento RG:"); ?></span>
        <input type="text" id="txtRG" name="txtRG" maxlength="25" value="<?php echo $rg; ?>" class="input-medium"/>
    </div>                                                        
    <div style="width:7%; float:left; margin-left:1%">
        <span>Expedidor:</span>
        <input type="text" id="txtOrg" name="txtOrg" maxlength="10" value="<?php echo $orgExp; ?>" class="input-medium"/>
    </div>
    <div style="width:8%; float:left; margin-left:1%">
        <span>Estrangeiro</span>
        <select class="select" style="width:100%;" id="txtEstrag" name="txtEstrag">
            <option value="0" <?php
            if ($estrang == "0") {
                echo "SELECTED";
            }
            ?>>NÃO</option>
            <option value="1" <?php
            if ($estrang == "1") {
                echo "SELECTED";
            }
            ?>>SIM</option>
        </select>
    </div>
    <div style="width:12%; float:left; margin-left:1%">
        <span> <span style="color:red;">*</span> Nascimento:</span>
        <input type="text" id="txtDtNasc" name="txtDtNasc" value="<?php echo $dtNascimento; ?>" class="datepicker inputCenter"/>
    </div>
    <div style="width:5%; float:left; margin-left:1%;margin-top">
        <span> Anos</span>
        <input type="text" id="txtIdade" name="txtIdade" value="" readonly/>
    </div>
    <div style="width:17%; float:left;margin-left:1%">
        <span>Profissão:</span>
        <input type="text" class="input-medium" id="txtProfissao" name="txtProfissao" value="<?php echo $profissao; ?>"/>
    </div>    
    <div style="width:17%; float:left;margin-left:1%; display: none;">
        <span>Inscrição Municipal:</span>
        <input type="text" class="input-medium" id="txtInsMunicipal" name="txtInsMunicipal" value="<?php echo $insMunicipal; ?>"/>
    </div>    
    <div style="clear:both; height:5px"></div>
    <div style="width:63%; float:left; display: none;">
        <span id="lblNmFantasia">Nome Fantasia:</span>
        <input type="text" id="txtNmFantasia" name="txtNmFantasia" value="<?php echo $nmFantasia; ?>" class="input-medium"/>
    </div>    
    <div style="width:17%; float:left;">
        <span>Gênero:</span>
        <select id="txtSexo" name="txtSexo" sexobg="<?php echo $ckb_adm_sexoobg_; ?>" class="select" style="width:100%">
            <option value="" <?php
            if ($sexo == "") {
                echo "SELECTED";
            }
            ?>>SELECIONE</option>            
            <option value="M" <?php
            if ($sexo == "M") {
                echo "SELECTED";
            }
            ?>>MASCULINO</option>
            <option value="F" <?php
            if ($sexo == "F") {
                echo "SELECTED";
            }
            ?>>FEMININO</option>
        </select>
    </div>
    <div style="width:7%; float:left; margin-left:1%">
        <span>Sangue:</span>
        <select class="select" style="width:100%" id="txtTpSang" name="txtTpSang">
            <option value="" <?php
            if ($tpsang == "") {
                echo "SELECTED";
            }
            ?>></option>
            <option value="A +" <?php
            if ($tpsang == "A +") {
                echo "SELECTED";
            }
            ?>>A+</option>
            <option value="A -" <?php
            if ($tpsang == "A -") {
                echo "SELECTED";
            }
            ?>>A-</option>
            <option value="B +" <?php
            if ($tpsang == "B +") {
                echo "SELECTED";
            }
            ?>>B+</option>
            <option value="B -" <?php
            if ($tpsang == "B -") {
                echo "SELECTED";
            }
            ?>>B-</option>
            <option value="AB+" <?php
            if ($tpsang == "AB+") {
                echo "SELECTED";
            }
            ?>>AB+</option>
            <option value="AB-" <?php
            if ($tpsang == "AB-") {
                echo "SELECTED";
            }
            ?>>AB-</option>
            <option value="O +" <?php
            if ($tpsang == "O +") {
                echo "SELECTED";
            }
            ?>>O+</option>
            <option value="O -" <?php
            if ($tpsang == "O -") {
                echo "SELECTED";
            }
            ?>>O-</option>
        </select>
    </div>
    <div style="width:20%; float:left; margin-left:1%">
        <span>Estado Civil:</span>
        <select class="select" style="width:100%;tex" id="txtEstadoCivil" name="txtEstadoCivil">
            <option value="SOLTEIRO(A)" <?php
            if ($estadoCivil == "SOLTEIRO(A)") {
                echo "SELECTED";
            }
            ?>>SOLTEIRO(A)</option>
            <option value="CASADO(A)" <?php
            if ($estadoCivil == "CASADO(A)") {
                echo "SELECTED";
            }
            ?>>CASADO(A)</option>
            <option value="SEPARADO(A)" <?php
            if ($estadoCivil == "SEPARADO(A)") {
                echo "SELECTED";
            }
            ?>>SEPARADO(A)</option>
            <option value="VIÚVO(A)" <?php
            if ($estadoCivil == "VIÚVO(A)") {
                echo "SELECTED";
            }
            ?>>VIÚVO(A)</option>
            <option value="DIVORCIADO(A)" <?php
            if ($estadoCivil == "DIVORCIADO(A)") {
                echo "SELECTED";
            }
            ?>>DIVORCIADO(A)</option>
        </select>
    </div>
    <div style="width:16%; float:left; margin-left:1%">
        <span>Nome da Empresa:</span>
        <input type="text" class="input-medium" id="txtNmEmpresa" name="txtNmEmpresa" value="<?php echo $nmEmpresa; ?>"/>
    </div>
    <div style="width:18%; float:left; margin-left:1%">
        <span>Contato (Emergência):</span>
        <input type="text" class="input-medium" id="txtNmEmergencia" name="txtNmEmergencia" value="<?php echo $nmEmergencia; ?>"/>
    </div>
    <div style="width:17%; float:left; margin-left:1%">
        <span>Telefone (Emergência):</span>
        <input type="text" class="input-medium" id="txtTelEmerg" name="txtTelEmerg" value="<?php echo $telEmergencia; ?>"  />
    </div>
    <div style="clear:both; height:5px"></div> 
    <?php if ($typePage == "Funcionario") { ?>
        <div style="width:17%; float:left">
            <span>PIS:</span>
            <input type="text" id="txtContato" name="txtContato" value="<?php echo $contato; ?>" class="input-medium"/>
        </div>    
        <div style="width:12%; float:left;margin-left: 1%">
            <span> <span style="color:red;">*</span> Data de Admissão:</span>
            <input type="text" id="txtDtAdmissao" name="txtDtAdmissao" value="<?php echo $data_admissao; ?>" class="datepicker inputCenter"/>
        </div>
        <div style="width:12%; float:left; margin-left:1%">
            Data de Demissão:</span>
            <input type="text" id="txtDtDemissao" name="txtDtDemissao" value="<?php echo $data_demissao; ?>" class="datepicker inputCenter"/>
        </div> 
        <div style="width:19%; float:left; margin-left:1%">
            <span>Horário:</span>
            <select class="select" style="width:100%;tex" id="txtfornecedorHR" name="txtfornecedorHR">
                <option value="null">Selecione o horário</option>
                <?php
                    $cur = odbc_exec($con, "select cod_turno, nome_turno from dbo.sf_turnos where cod_turno > 0") or die(odbc_errormsg());
                while ($RFP = odbc_fetch_array($cur)) {
                    ?>
                    <option value="<?php echo $RFP['cod_turno'] ?>"<?php
                    if (!(strcmp($RFP['cod_turno'], $fornecedores_hr))) {
                        echo "SELECTED";
                    }
                    ?>><?php echo utf8_encode($RFP['nome_turno']); ?>
                    </option>
                <?php } ?>
            </select>
        </div>    
        <div style="width:18%; float:left; margin-left:1%">
            <span>Departamento:</span>
            <select class="select" style="width:100%" name="txtDepartamento" id="txtDepartamento">
                <option value="null">Selecione o departamento:</option>
                <?php $cur = odbc_exec($con, "select id_departamento,nome_departamento from sf_departamentos ") or die(odbc_errormsg());
                while ($RFP = odbc_fetch_array($cur)) { ?>
                    <option value="<?php echo $RFP['id_departamento'] ?>"<?php
                    if (!(strcmp($RFP['id_departamento'], $departamento))) {
                        echo "SELECTED";
                    }
                    ?>><?php echo utf8_encode($RFP['nome_departamento']) ?></option>
                <?php } ?>
            </select>
        </div>
        <div style="width:17%; float:left; margin-left:1%">
            <span>Matrícula de Registro:</span>
            <input type="text" class="input-medium" id="txtMatriculaRegistro" name="txtMatriculaRegistro" value="<?php echo $referencia_ps1; ?>"/>
        </div>      
        <div style="clear:both; height:5px"></div>                                                                                                                                                                                                                                
    <?php } ?>
    <div style="width:25%;float:left;">
        <span>Como conheceu a Empresa?</span>
        <select class="select" id="txtProcedencia" name="txtProcedencia" probg="<?php echo $ckb_adm_procobg_; ?>" style="width:100%;float: left;">
            <option value="null">Selecione</option>
            <?php $cur = odbc_exec($con, "select id_procedencia,nome_procedencia from sf_procedencia where tipo_procedencia = 0 order by nome_procedencia") or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) { ?>
                <option value="<?php echo $RFP['id_procedencia'] ?>"<?php
                if (!(strcmp($RFP['id_procedencia'], $procedencia))) {
                    echo "SELECTED";
                }
                ?>><?php echo utf8_encode($RFP['nome_procedencia']); ?>
                </option> 
            <?php } ?>
        </select>
    </div>
    <div style="width:20%;float:left; margin-left:1%">
        <span>Usuário Responsável</span>
        <?php if ($ckb_aca_usu_resp_ == 1 || $id == "") { ?>
            <select class="select" id="txtUserResp" name="txtUserResp" style="width:100%;float: left;">
                <option value="">Selecione</option>
                <?php $cur = odbc_exec($con, "select id_usuario, nome from sf_usuarios where (inativo = 0 or id_usuario = " . valoresNumericos2($user_resp) . ") and id_usuario > 1") or die(odbc_errormsg());
                while ($RFP = odbc_fetch_array($cur)) { ?>
                    <option value="<?php echo $RFP['id_usuario'] ?>"<?php
                    if ($RFP['id_usuario'] == $user_resp) {
                        echo "SELECTED";
                    } ?>><?php echo formatNameCompact(strtoupper(utf8_encode($RFP['nome']))); ?></option>
                <?php } ?>
            </select>
            <?php
        } else {
            $name_user_resp_sel = "";
            $cur = odbc_exec($con, "select id_usuario, nome from sf_usuarios where inativo = 0 and id_usuario > 1") or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                if ($RFP['id_usuario'] == $user_resp) {
                    $name_user_resp_sel = formatNameCompact(strtoupper(utf8_encode($RFP['nome'])));
                }
            } ?>
            <input name="txtUserResp" id="txtUserResp" type="hidden" value="<?php echo $user_resp; ?>"/>
            <input type="text" value="<?php echo $name_user_resp_sel; ?>" readonly/>
        <?php } ?>
    </div>
    <div style="width:16%;float:left; margin-left:1%; <?php echo ($_SESSION["mod_emp"] == 3 ? "display: none;" : ""); ?>">
        <span>Indicador:</span>
        <input type="hidden" id="txtProfResp" name="txtProfResp" value="<?php echo $profResp;?>">      
        <input type="text" id="txtProfRespNome" name="txtProfRespNome" style="width:100%;" value="<?php echo $profRespNome;?>"/>
    </div>                                                        
    <div style="width:16%; float:left; margin-left:1%">
        <span>Grupo:</span>
        <select class="select" id="txtGrupo" name="txtGrupo" style="width:100%; float:left">
            <option value="">Selecione</option>
            <?php $cur = odbc_exec($con, "select id_grupo,descricao_grupo from dbo.sf_grupo_cliente 
            where tipo_grupo = '" . ($typePage == "Funcionario" ? "E" : "C") . "' and inativo_grupo = 0 order by descricao_grupo") or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) { ?>
                <option value="<?php echo $RFP['id_grupo']; ?>"<?php
                if (!(strcmp($RFP['id_grupo'], $grupoPessoa))) {
                    echo "SELECTED";
                }
                ?>><?php echo utf8_encode($RFP['descricao_grupo']); ?>
                </option>
            <?php } ?>
        </select>
    </div>                                                        
    <div style="width:9%; float:left; margin-left:1%">
        <span style="color: <?php echo ($emailMarketing == "0" && $contrato != "007" ? "#dc5039" : "#000") ?> ;">Email Marketing:</span>
        <select class="select" id="ckbEmailMarketing" name="ckbEmailMarketing" style="width:100%;">
            <option value="1" <?php
            if ($emailMarketing == "1") {
                echo "selected";
            }
            ?>>SIM</option>
            <option value="0" <?php
            if ($emailMarketing == "0") {
                echo "selected";
            }
            ?>>NÃO</option>
        </select>
    </div>
    <div style="width:9%; float:left; margin-left:1%">
        <span style="color: <?php echo ($celularMarketing == "0" && $contrato != "007" ? "#dc5039" : "#000") ?> ;">Cel. Marketing:</span>
        <select class="select" id="ckbCelularMarketing" name="ckbCelularMarketing" style="width:100%;">
            <option value="1" <?php
            if ($celularMarketing == "1") {
                echo "selected";
            }
            ?>>SIM</option>
            <option value="0" <?php
            if ($celularMarketing == "0") {
                echo "selected";
            }
            ?>>NÃO</option>
        </select>
    </div>
    <?php if ($_SESSION["mod_emp"] == 2) { ?>
        <div style="width:20%; float:left;margin-right: 1%;">
            <span> Validade Cartão de Vantagens</span>
            <input type="text" id="txtDtValCartao" name="txtDtValCartao" value="<?php echo $dtValCartao; ?>" class="datepicker inputCenter"/>
        </div>
    <?php } if ($mdl_gbo_ == 1 && is_numeric($id)) { ?>
        <div style="width:20%;float:left;">
            <span>GoBody (Sincronizar):</span>
            <button id="btnGobody" type="button" <?php if ($gobody == "") { ?>onclick="addGobody();"<?php } ?> class="btn dblue" style="width:100%; background-color: rgba(20,173,163,1) !important;"><?php if ($gobody == "") { ?>Cadastrar <span class="icon-plus icon-white"></span> <?php } else { ?> Sincronizado<?php } ?></button>
        </div>
    <?php } ?>                                                                                                                    
    <div style="width:17%; float:left;">
        <span id="lblDocumento2">Tipo de Contribuinte:</span>
        <select name="txtRegTributario" id="txtRegTributario" class="select" style="width:100%" disabled>
            <option value="1" <?php
            if ($regimetributario == 1) {
                echo "SELECTED";
            }
            ?>>Contribuinte do ICMS</option>
            <option value="2" <?php
            if ($regimetributario == 2) {
                echo "SELECTED";
            }
            ?>>Contribuinte ISENTO</option>
            <option value="9" <?php
            if ($regimetributario == 9) {
                echo "SELECTED";
            }
            ?>>Não Contribuinte</option>
        </select>
    </div>
    <div style="width:7%; float:left; margin-left:1%">
        <span id="lblISS">ISS Retido:</span>
        <select class="select" id="ckbISS" name="ckbISS" style="width:100%;">
            <option value="1" <?php
            if ($nfe_iss == "1") {
                echo "selected";
            }
            ?>>SIM</option>
            <option value="0" <?php
            if ($nfe_iss == "0") {
                echo "selected";
            }
            ?>>NÃO</option>
        </select>
    </div>        
    <div style="width:20%; float:left; margin-left:1%">
        <span>Plano de Saúde:</span>
        <select class="select" style="width:100%" id="txtPlanoSaude" name="txtPlanoSaude">
            <option value="NULL" <?php
            if ($planoSaude == "OUTRO") {
                echo "SELECTED";
            }
            ?>>OUTRO</option>
            <option value="UNIMED" <?php
            if ($planoSaude == "UNIMED") {
                echo "SELECTED";
            }
            ?>>UNIMED</option>
            <option value="AMIL" <?php
            if ($planoSaude == "AMIL") {
                echo "SELECTED";
            }
            ?>>AMIL</option>
            <option value="CONMEDH" <?php
            if ($planoSaude == "CONMEDH") {
                echo "SELECTED";
            }
            ?>>CONMEDH</option>
            <option value="ASSIM" <?php
            if ($planoSaude == "ASSIM") {
                echo "SELECTED";
            }
            ?>>ASSIM</option>
            <option value="SAÚDE BRADESCO" <?php
            if ($planoSaude == "SAÚDE BRADESCO") {
                echo "SELECTED";
            }
            ?>>SAÚDE BRADESCO</option>
            <option value="SUL AMÉRICA" <?php
            if ($planoSaude == "SUL AMÉRICA") {
                echo "SELECTED";
            }
            ?>>SUL AMÉRICA</option>
            <option value="DIX AMICO" <?php
            if ($planoSaude == "DIX AMICO") {
                echo "SELECTED";
            }
            ?>>DIX AMICO</option>
            <option value="MARÍTIMA SAÚDE" <?php
            if ($planoSaude == "MARÍTIMA SAÚDE") {
                echo "SELECTED";
            }
            ?>>MARÍTIMA SAÚDE</option>
            <option value="OMINT SAÚDE" <?php
            if ($planoSaude == "OMINT SAÚDE") {
                echo "SELECTED";
            }
            ?>>OMINT SAÚDE</option>
            <option value="MEDIAL SAÚDE" <?php
            if ($planoSaude == "MEDIAL SAÚDE") {
                echo "SELECTED";
            }
            ?>>MEDIAL SAÚDE</option>
            <option value="PETROBRAS" <?php
            if ($planoSaude == "PETROBRAS") {
                echo "SELECTED";
            }
            ?>>PETROBRAS</option>
        </select>
    </div>
    <div style="width:16%; float:left; margin-left:1%">
        <span>Número da CNH:</span>
        <input type="text" class="input-medium" id="txtNumCnh" name="txtNumCnh" value="<?php echo $numCnh; ?>"/>
    </div>
    <div style="width:10%; float:left; margin-left:1%">
        <span>Categoria:</span>
        <input type="text" class="input-medium" id="txtCatCnh" name="txtCatCnh" value="<?php echo $catCnh; ?>"/>
    </div>
    <div style="width:12%; float:left; margin-left:1%">
        <span>Vencimento CNH:</span>
        <input type="text" class="datepicker inputCenter" id="txtVencCnh" name="txtVencCnh" value="<?php echo $vencCnh; ?>"/>
    </div>
    <div style="width:12%; float:left; margin-left:1%">
        <span>1ª Habilitação:</span>
        <input type="text" class="datepicker inputCenter" id="txtPriCnh" name="txtPriCnh" value="<?php echo $priCnh; ?>"/>
    </div>           
    <div style="width:15%;float:right;">                                                           
        <div style="width:100%;">
            <span style="color: <?php echo ($inativo == "1" ? "#dc5039" : "#000") ?> ;">Bloqueado:</span>
            <select class="select" id="ckbInativo" name="ckbInativo" style="width:100%;">
                <?php if ($inativo == "2" || $inativo == "3") { ?>
                    <option value="<?php echo $inativo; ?>" <?php
                    if ($inativo == "2" || $inativo == "3") {
                        echo "selected";
                    }
                    ?>>Sim</option>
                <?php } else { ?>
                    <option value="1" <?php
                    if ($inativo == "1") {
                        echo "selected";
                    }
                    ?>>Sim</option>
                    <option value="0" <?php
                    if ($inativo == "0") {
                        echo "selected";
                    }
                    ?>>Não</option>
                <?php } ?>
            </select>
        </div>
    </div>                               
    <?php
    $total = 0;
    $ckempresa = (!is_numeric($ckempresa) ? $filial : $ckempresa);
    $cur = odbc_exec($con, "select count(id_filial) total from sf_filiais where ativo = 0");
    while ($RFP = odbc_fetch_array($cur)) {
        $total = $RFP['total'];
    }
    if ($total > 1) { ?>
        <div style="width:20%;float:right; margin-right: 1%;">
            <label>Selecione a Loja: </label>          
            <select class="select" id="txtFilial" name="txtFilial" style="width:100%;float: left;">
                <?php $cur = odbc_exec($con, "select distinct id_filial, numero_filial, Descricao from sf_filiais 
                inner join sf_usuarios_filiais on id_filial_f = id_filial
                inner join sf_usuarios on id_usuario_f = id_usuario
                where ativo = 0 and (id_usuario_f = '" . $_SESSION["id_usuario"] . "' or id_filial = " . valoresNumericos2($ckempresa) . ");");
                while ($RFP = odbc_fetch_array($cur)) { ?>
                    <option value="<?php echo utf8_encode($RFP['id_filial']); ?>"<?php
                    if (!(strcmp($RFP['id_filial'], $ckempresa))) {
                        echo "SELECTED";
                    }
                    ?>><?php echo utf8_encode($RFP['numero_filial']) . " - " . utf8_encode($RFP['Descricao']); ?></option>
                <?php } ?>
            </select>
        </div>                                                                        
    <?php } else { ?>
        <input name="txtFilial" id="txtFilial" type="hidden" value="<?php echo $ckempresa; ?>"/>                                                            
    <?php } if ($gymPass_id > 0) { ?> 
        <div style="width:20%;float:right; margin-right: 1%;">
            <label>GymPass Id: </label>          
            <input type="text" class="input-medium" id="txtGymPassId" name="txtGymPassId" value="<?php echo $gymPassId; ?>"  />
        </div>
    <?php } ?>        
</div>
<div style="clear:both; height:5px"></div>
<div style="width:100%; margin:10px 0">
    <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Endereço</span>
    <hr style="margin:2px 0; border-top:1px solid #ddd">
</div>
<div style="clear:both; height:5px"></div>
<div style="width:16%; float:left">
    <span>CEP:</span>
    <input type="text" id="txtCep" name="txtCep" eobg="<?php echo $ckb_adm_endobg_; ?>" class="input-medium" value="<?php echo $cep; ?>"/>
</div>
<div style="width:41%; float:left; margin-left:1%">
    <span>Logradouro:</span>
    <input type="text" id="txtEndereco" name="txtEndereco" maxlength="256" class="input-medium" value="<?php echo $endereco; ?>" readonly/>
</div>
<div style="width:10%; float:left; margin-left:1%">
    <span>Número:</span>
    <input type="text" id="txtNumero" name="txtNumero" maxlength="6" class="input-medium" value="<?php echo $numero; ?>"/>
</div>
<div style="width:30%; float:left; margin-left:1%">
    <span>Bairro:</span>
    <input type="text" id="txtBairro" name="txtBairro" maxlength="40" class="input-medium" value="<?php echo $bairro; ?>" readonly/>
</div>
<div style="clear:both; height:5px"></div>
<div style="width:28.5%; float:left; ">
    <span>Estado:</span>
    <select class="select" id="txtEstado" name="txtEstado" style="width:100%" readonly>
        <option uf="" value="">Selecione o estado</option>
        <?php $cur = odbc_exec($con, "select estado_codigo,estado_nome,estado_sigla from tb_estados where estado_codigo > 0 order by estado_nome") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) { ?>
            <option uf="<?php echo strtoupper(utf8_encode($RFP['estado_sigla'])); ?>" value="<?php echo $RFP['estado_codigo'] ?>"<?php
            if (!(strcmp($RFP['estado_codigo'], $estado))) {
                echo "SELECTED";
            }
            ?>><?php echo strtoupper(utf8_encode($RFP['estado_nome'])); ?></option>
        <?php } ?>
    </select>
</div>
<div style="width:28.5%; float:left;margin-left:1%">
    <span>Cidade:</span>
    <div id="carregando" style=" margin-top: 10px;;display:none">
        <span style="color:#666;">Aguarde, carregando...</span>
    </div>
    <select class="select" id="txtCidade" name="txtCidade" style="width:100%;text-transform: uppercase" readonly>
        <option value="">Selecione a cidade</option>
        <?php
        if ($estado != "") {
            $sql = "select cidade_codigo,cidade_nome from tb_cidades where cidade_codigo > 0 AND cidade_codigoEstado = " . $estado . " ORDER BY cidade_nome";
            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                ?>
                <option value="<?php echo $RFP['cidade_codigo'] ?>"<?php
                if (!(strcmp($RFP['cidade_codigo'], $cidade))) {
                    echo "SELECTED";
                }
                ?>><?php echo strtoupper(utf8_encode($RFP['cidade_nome'])) ?>
                </option>
                <?php
            }
        }
        ?>
    </select>
</div>
<div style="width:41%; float:left; margin-left:1%">
    <span>Complemento:</span>
    <input type="text" id="txtComplemento" name="txtComplemento" class="input-medium" value="<?php echo $complemento; ?>"/>
</div>
<div style="clear:both; height:5px"></div>
<div style="width:100%; margin:10px 0">
    <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Informações</span>
    <hr style="margin:2px 0; border-top:1px solid #ddd">
</div>
<div style="clear:both; height:5px"></div>
<div style="width:55%; float:left">
    <input type="hidden" value="" id="txtQtdContatos" name="txtQtdContatos" value="0"></input>
    <span>Contatos:</span>
    <div style="background:#F1F1F1; border:1px solid #DDD; border-bottom:0; padding:10px 10px 0">
        <div style="float:left; width:19%">
            <select id="txtTipoContato" onChange="alterarMask()" >
                <option value="1">Celular</option>
                <option value="0">Telefone</option>
                <option value="2">E-mail</option>
                <option value="3">Site</option>
                <option value="4">Outros</option>
            </select>
        </div>
        <div style="float:left; width:calc(53% - 35px); margin-left:1%">
            <input type="text" class="input-medium" id="txtTextoContato" />
        </div>
        <div style="float:left; width:25%; margin-left:1%">
            <input type="text" class="input-medium" id="txtRespContato" />
        </div>
        <div style="float:left; width:35px; margin-left:1%">
            <button type="button" onclick="addContato();" class="btn dblue" style="line-height:19px;margin:0;background-color: #308698 !important;"><span class="ico-plus icon-white"></span></button>
        </div>
        <div style="clear:both"></div>
    </div>
    <div style="background:#F1F1F1; border:1px solid #DDD; border-top:0; padding:5px 10px 10px">
        <div id="divContatos" eobg="<?php echo $ckb_adm_emlobg_; ?>" tobg="<?php echo $ckb_adm_telobg_; ?>" cobg="<?php echo $ckb_adm_celobg_; ?>" style="height:108px; background:#FFF; border:1px solid #DDD; overflow-y:scroll"></div>
    </div>
</div>
<div style="width:44%; float:left; margin-left:1%">
    <span>Observações:</span>
    <textarea style="width:100%; height:164px; resize:none" id="txtObs" name="txtObs" bloq="<?php echo ($typePage == "Prospect" ? "" : $ckb_adm_cli_read_obs_); ?>"><?php echo $Obs; ?></textarea>
</div>
<div style="clear:both"></div>
<div class="toolbar bottom tar">
    <div class="data-fluid">
        <div class="btn-group" style="margin-top: 10px;">
            <?php if ($ckb_adm_cli_read_ == 0 || ($typePage == "Prospect" && $user_resp == $_SESSION["id_usuario"])) { ?> 
                <button id="btnSave" name="btnSave" class="btn btn-success" style="display: none;" type="button"><span class="ico-checkmark"></span> Gravar</button>
                <button id="btnCancelar" name="btnCancelar" class="btn yellow" style="display: none;" type="button"><span class="ico-reply"></span> Cancelar</button>
                <button id="btnNovo" name="btnNovo" class="btn  btn-success" type="button" title="Novo" hidden><span class="ico-plus-sign"> </span> Novo</button>
                <button id="btnAlterar" name="btnAlterar" class="btn  btn-success" type="button" title="Alterar" hidden> <span class="ico-edit"> </span> Alterar</button>
                <button id="btnExcluir" name="btnExcluir" class="btn red" type="button" title="Excluir" hidden><span class=" ico-remove"> </span> Excluir</button>
            <?php } ?>
        </div>
    </div>
</div>
<div style="clear:both"></div>
<div class="dialog" id="source" title="Processando" style="display: none;">
    <input id="txtTotalSucesso" type="hidden" value="0"/>
    <input id="txtTotalErro" type="hidden" value="0"/>
    <div id="progressbar"></div>
    <div style="display: flex;align-items: center;justify-content: center;margin-top: 20px;">
        <div id="pgIni">0</div><div style="padding: 5px;">até</div><div id="pgFim">0</div>
    </div>
</div>