<span style="color:#000; font-size:14px; text-shadow:0 1px #FFF; padding-left:5px">Agendamentos</span>
<hr style="margin:2px 0; border-top:1px solid #DDD">
<div style="width:100%;">
    <div style="background:#F1F1F1; border:1px solid #DDD; padding:10px;vertical-align: top;">
        <?php if ($ckb_adm_cli_read_ == 0) { ?>
        <button class="button button-green btn-primary" type="button" onClick="AbrirBoxAgenda(0)"><span class="ico-file-4 icon-white"></span></button>
        <?php } ?>
    </div>
</div>
<table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblAgenda">
    <thead>
        <tr>
            <th width="8%">Data Início:</th>
            <th width="8%">Data Fim:</th>
            <th width="10%">Agenda:</th>                                                                                                                                    
            <th width="25%">Cliente:</th>                                               
            <th width="10%">Assunto:</th>                                                                                                
            <th width="15%">Observação:</th>                                                                                                                                            
            <th width="8%"><center>Concluído:</center></th> 
            <th width="10%"><center>Operador:</center></th> 
            <th width="6%"><center>Ação:</center></th> 
        </tr>
    </thead>
    <tbody></tbody>
</table> 
<div style="clear:both; height:5px"></div>                                                        