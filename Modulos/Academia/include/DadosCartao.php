<div style="width:26%; float:left">
    <span>Nome Impresso no Cartão:</span>
    <input type="text" class="input-medium" autocomplete="off" id="txtDCCNmCartao" name="txtDCCNmCartao"/>
</div>
<div style="width: calc(37% - 105px); float:left; margin-left:1%">
    <span>Número do Cartão:</span>
    <input type="text" class="input-medium" autocomplete="off" id="txtDCCNumCartao" name="txtDCCNumCartao" maxlength="20" style="width:100%;float: left"/>
</div>
<div style="width:105px; float:left; margin-left:1%">
    <span>Bandeira:</span>
    <select class="selectCr" id="txtDCCBandeira" name="txtDCCBandeira" style="width: 105px;">                                        
        <?php for($j = 0; $j < 12; $j++) { ?>            
            <option value="<?php echo $j; ?>"></option>
        <?php } ?>                                        
    </select>
</div>
<div style="width:8%; float:left; margin-left:1%">
    <span>CVV:</span>
    <input type="text" class="input-medium" autocomplete="off" id="txtDCCCodSeg" name="txtDCCCodSeg" maxlength="4" style="width:100%"/>
</div>
<div style="width:12%; float:left; margin-left:1%">
    <span>Validade:</span>
    <input type="text" class="inputCenter" autocomplete="off" id="txtDCCValidade" name="txtDCCValidade"/>
</div>
<div style="width:12%; float:left; margin-left:1%">
    <span>Tipo:</span>
    <select class="inputCenter" id="txtTipo" name="txtTipo">
        <?php $cur = odbc_exec($con, "select DCC_CHAVE, DCC_FILIAL, DCC_DESCRICAO, DCC_CHAVE2, DCC_FILIAL2, DCC_DESCRICAO2, adm_cad_car_fil from sf_configuracao where id = 1");
        while ($RFP = odbc_fetch_array($cur)) { ?>
            <?php if (strlen($RFP['DCC_CHAVE']) > 0 && ($RFP['DCC_FILIAL'] == $filial || $RFP['adm_cad_car_fil'] == 0)){ ?>
                <option value="0"><?php echo $RFP['DCC_DESCRICAO']; ?></option>
            <?php } if (strlen($RFP['DCC_CHAVE2']) > 0 && ($RFP['DCC_FILIAL2'] == $filial || $RFP['adm_cad_car_fil'] == 0)){ ?>
                <option value="1"><?php echo $RFP['DCC_DESCRICAO2']; ?></option>
            <?php } ?>
        <?php } ?>
    </select>
</div>