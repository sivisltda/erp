<?php if($_SESSION["mod_emp"] != 1){ ?>
<div style="width:calc(53% - 101px);  float:left;">
    <span>Plano Empresa/Família:</span>
    <select name="txtPlanoEF" class="select" style="width:100%" id="txtPlanoEF">
        <option value="">Selecione</option>
        <?php $cur = odbc_exec($con, "select id_convenio,descricao from sf_convenios where tipo = 0 and status = 0 and id_conv_plano is null order by descricao") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) { ?>
        <option value="<?php echo $RFP['id_convenio']; ?>"
                <?php if($planoEmpresaFamilia == $RFP['id_convenio']){echo "selected";} ?>>
                <?php echo utf8_encode($RFP['descricao']); ?>
        </option> <?php } ?>
    </select>
</div>
<div style="float:left;">
    <?php if($ckb_adm_cli_read_ == 0){?>
    <button type="button" class="btn dblue" id="btnAddPlanoEmpresaFamilia" hidden onclick="AbrirBox(7)" style="line-height:19px; width:75px;margin-top: 16px;background-color: #308698 !important;"> Incluir <span class="icon-arrow-next icon-white"></span></button>
    <input class="btn red" type="button" id="btnRemPlanoEmpresaFamilia" hidden value="x" style="margin:0px; padding:4px 8px 2px 10px; margin-top: 16px;font-size: 16px; line-height:22px;" onclick="RemoverPlanoEF()">
    <?php } ?>    
</div>
<div style="width:44%; float:left;margin-left: 1%;">
    <span id="lblLider" onclick="goToLider();" <?php echo is_numeric($idLiderplanoEmpresaFamilia) ? "style=\"color: rgb(0, 136, 204);cursor: pointer;\"" : ""; ?>>Lider:</span>
    <input id="txtIdLider" type="hidden" value="<?php echo $idLiderplanoEmpresaFamilia; ?>"/>    
    <input id="txtDescrLider" type="text" value="<?php echo $LiderplanoEmpresaFamilia; ?>" readonly/>
</div>
<div style="float:left;margin-left: 1%;">
    <?php if($ckb_adm_cli_read_ == 0){?>
    <button type="button" class="btn dblue" id="btnTrocaLider" hidden onclick="AbrirBox(8)" style="line-height:19px; width:75px;margin-top: 16px;background-color: #308698 !important;"> Trocar</button>
    <?php } ?>
</div>
<?php } ?>
<div style="clear:both; height:5px"></div>
<div style="width:100%; margin:10px 0">
    <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Convênios</span>
    <hr style="margin:2px 0; border-top:1px solid #ddd">
</div>
<?php if($ckb_aca_add_convenio_ > 0) { ?>
<div id="divConvenio" style="border-style: solid;border-width: 1px; padding: 7px;border-color:#c8c8c8;">
    <div style="float:left; width:64%">
        <span>Convênio:</span>
        <select id="txtConvenios" class="select" style="width:100%"  >
            <option value="">Selecione</option>
            <?php $cur = odbc_exec($con, "select id_convenio,descricao from sf_convenios where tipo = 1 and status = 0 and id_conv_plano is null order by descricao") or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) { ?>
            <option value="<?php echo $RFP['id_convenio']; ?>"><?php echo utf8_encode($RFP['descricao']); ?></option> 
            <?php } ?>
        </select>
    </div>
    <div style="width:12%; float:left;margin-left: 1%">
        <span>Inicio:</span>
         <input type="text" id="txtConvDtIni" class="datepicker inputCenter"/>
    </div>
    <div style="width:12%; float:left;margin-left: 1%">
        <span>Fim:</span>
        <input type="text" id="txtConvDtFim" class="datepicker inputCenter"/>
    </div>
    <div style="float:left;margin-left: 1%;">
        <?php if($ckb_adm_cli_read_ == 0){?>
            <button type="button" class="btn dblue" id="btnIncluirConv" style="line-height:19px; width:75px;margin-top: 16px;background-color: #308698 !important;"> Incluir <span class="icon-arrow-next icon-white"></span></button>
        <?php } ?>
    </div>
    <div style="clear:both; height:5px"></div>
</div>
<?php }?>
<div style="clear:both; height:5px"></div>
<div class="body" style=" margin-left: 0px; padding: 0px 0px 0px 0px;">
    <div class="content" style="min-height: 0px;margin-top: 0px;margin-bottom: 0px;">
        <table  id="tblConvenio" class="table" cellpadding="0" cellspacing="0" width="100%" style="display: table;">
            <thead>            
                <tr><th width="513px"><b>Convênio</b></th>
                    <th width="150px"><b>Desconto</b></th>
                    <th width="150px"><b>Inicio</b></th>
                    <th width="150px"><b>Fim</b></th>
                    <th width="150px"><b>Até Vencimento</b></th>
                    <th width="43px"><b>Ação</b></th>
                </tr></thead>            
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<div style="clear:both; height:5px"></div>
<?php if($_SESSION["mod_emp"] != 1){ ?>
<div style="width:100%; margin:10px 0">
    <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Dependentes</span>
    <hr style="margin:2px 0; border-top:1px solid #ddd">
</div>
<div id="divDependente" style="border-style: solid;border-width: 1px; padding: 7px;border-color:#c8c8c8;">
    <div style="float:left; width:39%">
        <span>Dependente:</span>
        <input type="text" id="txtDependente" style="width:100%" />
        <input type="hidden" id="txtCodDep">        
    </div>
    <div style="float:left; width:20%;margin-left: 1%;">
        <span>Parentesco:</span>
        <select id="txtParentesco" class="select" style="width:100%"  >
            <option value="null">Selecione</option>
            <?php $cur = odbc_exec($con, "select * from sf_parentesco order by nome_parentesco") or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) { ?>
            <option value="<?php echo $RFP['id_parentesco']; ?>"><?php echo utf8_encode($RFP['nome_parentesco']); ?></option> 
            <?php } ?>
        </select>
    </div>        
    <div style="float:left; width:15%;margin-left: 1%;margin-top: 19px;">
        <input id="ckb_compartilhado" type="checkbox" value="1" checked/> Plano Compartilhado
    </div>
    <div style="float:left; width:15%;margin-left: 1%;margin-top: 19px;">
        <input id="ckb_responsavel" type="checkbox" value="1"/> Dependente legal
    </div>
    <div style="float:left;margin-left: 1%;">
        <?php if($ckb_adm_cli_read_ == 0){?>
            <button type="button" class="btn dblue" id="btnIncluirDep" style="line-height:19px; width:75px;margin-top: 16px;background-color: #308698 !important;"> Incluir <span class="icon-arrow-next icon-white"></span></button>
        <?php } ?>
    </div>
    <div style="clear:both;height:5px"></div>
</div>
<div style="clear:both; height:5px"></div>
<div class="body" style="margin-left: 0px; padding: 0px 0px 0px 0px;">
    <div class="content" style="min-height: 0px;margin-top: 0px;margin-bottom: 0px;">
        <table id="tblDependente" class="table" cellpadding="0" cellspacing="0" width="100%" style="display: table;">
            <thead>
                <tr>
                    <th width="250%"><b>Nome</b></th>
                    <th width="10%"><b>Parentesco</b></th>
                    <th width="10%"><b>Idade</b></th>                    
                    <th width="10%"><b>Data Inclusão</b></th>
                    <th width="10%"><b>Compartilhado</b></th>
                    <th width="10%"><b>Dependente legal</b></th>
                    <th width="10%"><b>Ação</b></th>
                </tr></thead>
            <tbody></tbody>
        </table>
    </div>
</div>
<?php } ?>
<div style="clear:both; height:5px"></div>
<?php if($_SESSION["mod_emp"] != 1){ ?>
<div style="width:100%; margin:10px 0">
    <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Responsável Legal</span>
    <hr style="margin:2px 0; border-top:1px solid #ddd">
</div>
<div style="clear:both;height: 5px;"></div>
<div id="divRespLegal" style="border-style: solid;border-width: 1px; padding: 7px;border-color:#c8c8c8;">
    <input type="hidden" id="txtIdRespLegal" value="">
    <input type="hidden" id="txtTipoRespLegal" value="">
    <div style="float:left; width: 40%;">
        <span>Nome:</span>
        <input type="text" id="txtNmRespLegal" style="width:100%" value="" maxlength="128"/>
    </div>
    <div style="float:left; width: 29%;margin-left: 1%">
        <span>Celular:</span>
        <input type="text" id="txtCelRespLeg" style="width:100%" value="" maxlength="128"/>
    </div>
    <div style="float:left; width: 29%;margin-left: 1%">
        <span>E-mail:</span>
        <input type="text" id="txtEmailRespLeg" style="width:100%" value="" maxlength="128"/>
    </div>
    <div style="clear:both; height:5px"></div>
    <div style="float:left; width:18%;">
        <span>CPF:</span>
        <input type="text" id="txtCpfRespLegal" style="width:100%" value=""/>
    </div>
    <div style="float:left; width:14%; margin-left: 1%">
        <span>RG:</span>
        <input type="text" id="txtRgRespLegal" style="width:100%" value="" maxlength="25"/>
    </div>
    <div style="float:left; width:20%; margin-left: 1%">
        <span>Estado Civil:</span>
        <select class="select" style="width:100%;tex" id="txtCivilRespLegal" name="txtCivilRespLegal">
            <option value="SOLTEIRO(A)">SOLTEIRO(A)</option>
            <option value="CASADO(A)">CASADO(A)</option>
            <option value="SEPARADO(A)">SEPARADO(A)</option>
            <option value="VIÚVO(A)">VIÚVO(A)</option>
            <option value="DIVORCIADO(A)">DIVORCIADO(A)</option>
        </select>
    </div>
    <div style="float:left; width:15%; margin-left: 1%">
        <span>Profissão:</span>
        <input type="text" id="txtProfRespLegal" style="width:100%" value="" maxlength="50"/>
    </div>
    <div style="clear:both; height:5px"></div>
    <div style="width:16%; float:left">
        <span>CEP:</span>
        <input type="text" id="txtCepRespLegal" name="txtCepRespLegal" class="input-medium" value=""/>
    </div>
    <div style="width:41%; float:left; margin-left:1%">
        <span>Logradouro:</span>
        <input type="text" id="txtEndRespLegal" name="txtEndRespLegal" maxlength="256" class="input-medium" value="" readonly/>
    </div>
    <div style="width:10%; float:left; margin-left:1%">
        <span>Número:</span>
        <input type="text" id="txtNumRespLegal" name="txtNumRespLegal" maxlength="6" class="input-medium" value=""/>
    </div>
    <div style="width:30%; float:left; margin-left:1%">
        <span>Bairro:</span>
        <input type="text" id="txtBairroRespLegal" name="txtBairroRespLegal" maxlength="40" class="input-medium" value="" readonly/>
    </div>
    <div style="clear:both; height:5px"></div>
    <div style="width:19%; float:left; ">
        <span>Estado:</span>
        <select class="select" id="txtEstRespLegal" name="txtEstRespLegal" style="width:100%" readonly>
            <option value="null">Selecione o estado</option>
            <?php $cur = odbc_exec($con, "select estado_codigo,estado_nome from tb_estados where estado_codigo > 0 order by estado_nome") or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) { ?>
                <option value="<?php echo $RFP['estado_codigo'] ?>"><?php echo strtoupper(utf8_encode($RFP['estado_nome'])); ?></option> 
            <?php } ?>
        </select>
    </div>
    <div style="width:29%; float:left;margin-left:1%">
        <span>Cidade:</span>
        <div id="carregando2" style=" margin-top: 10px;;display:none">
            <span style="color:#666;">Aguarde, carregando...</span>
        </div>
        <select class="select" id="txtCidRespLegal" name="txtCidRespLegal" style="width:100%;text-transform: uppercase" readonly>
            <option value="null">Selecione a cidade</option>            
        </select>
    </div>
    <div style="width:39%; float:left; margin-left:1%">
        <span>Complemento:</span>
        <input type="text" id="txtComplRespLegal" name="txtComplRespLegal" class="input-medium" value="" maxlength="100"/>
    </div>
    <?php if($ckb_adm_cli_read_ == 0){?>
    <div style="width:10%; float:left; margin-left:1%;margin-top: 14px;">
        <?php if($ckb_adm_cli_read_ == 0){?>
            <button type="button" class="btn dblue" id="btnSaveRespLegal" style="line-height:19px; width:75px;margin-top: 1px;background-color: #308698 !important;"> Salvar <span class="icon-arrow-next icon-white"></span></button>
        <?php } ?>
    </div>
    <?php }?>
    <div style="clear:both; height:5px"></div>
</div>
<div style="clear:both; height:5px"></div>
<div class="body" style=" margin-left: 0px; padding: 0px 0px 0px 0px;">
    <div class="content" style="min-height: 0px;margin-top: 0px;margin-bottom: 0px;">
        <table id="tblListRespLegal" class="table" cellpadding="0" cellspacing="0" width="100%" style="display: table;">
            <thead>
                <tr>
                    <th width="100px"><b>Código</b></th>
                    <th width="645px"><b>Nome</b></th>
                    <th width="200px"><b>Celular</b></th>
                    <th width="200px"><b>E-mail</b></th>
                    <th width="200px"><b>CPF</b></th>
                    <th width="219px"><b>Profissão</b></th>
                    <th width="43px"><b>Ação</b></th>
                </tr></thead>
            <tbody></tbody>
        </table>
    </div>
</div>
<?php } ?>
<div style="clear:both;height: 5px;"></div>
