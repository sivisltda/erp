<div style="clear:both; height:5px"></div>
<div style="width:100%; margin:10px 0">
    <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Titularidade</span>
    <hr style="margin:2px 0; border-top:1px solid #ddd">
</div>
<div id="divConvenio" style="border-style: solid;border-width: 1px; padding: 7px;border-color:#c8c8c8;">
    <div style="float:left; width:18%">
        <span>Sócio:</span>
        <select class="select" id="ckbClbSocio" name="ckbClbSocio" style="width:100%;">
            <option value="0" <?php if($clbSocio == "0"){ echo "selected";} ?>>NÃO</option>            
            <option value="1" <?php if($clbSocio == "1"){ echo "selected";} ?>>SIM</option>
        </select>
    </div>    
    <div style="float:left; width:18%; margin-left: 1%;">
        <span>Titular:</span>
        <select class="select" id="ckbClbTitular" name="ckbClbTitular" style="width:100%;">
            <option value="0" <?php if($clbTitular == "0"){ echo "selected";} ?>>NÃO</option>            
            <option value="1" <?php if($clbTitular == "1"){ echo "selected";} ?>>SIM</option>
        </select>
    </div>    
    <div style="float:left; width:18%; margin-left: 1%;">
        <span>Mala Direta:</span>
        <select class="select" id="ckbClbMalaDireta" name="ckbClbMalaDireta" style="width:100%;">           
            <option value="0" <?php if($clbMalaDireta == "0"){ echo "selected";} ?>>NÃO</option> 
            <option value="1" <?php if($clbMalaDireta == "1"){ echo "selected";} ?>>SIM</option>            
        </select>
    </div>    
    <div id="divTitularidade" etit="<?php echo $ckb_clb_titobg_; ?>" style="float:left; width:24%; margin-left: 1%;">
        <span>Titularidade:</span>
        <select id="txtTitularidade" name="txtTitularidade" class="select" style="width:100%"  >
            <option value="null">Selecione</option>
            <?php $cur = odbc_exec($con, "select id_cat, nome_cat from sf_categoria_socio order by nome_cat") or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) { ?>
            <option value="<?php echo $RFP['id_cat']; ?>" <?php if($clbTitularidade == $RFP['id_cat']){ echo "selected";} ?>><?php echo utf8_encode($RFP['nome_cat']); ?></option> 
            <?php } ?>
        </select>
    </div> 
    <div style="float:left; width:18%; margin-left: 1%;">
        <span>Tipo:</span>
        <select id="ckbTipo" name="ckbTipo" class="select" style="width:100%"  >
            <option value="null">Selecione</option>
            <?php $cur = odbc_exec($con, "select id_tipo, descricao_tipo from sf_tipo_socio order by descricao_tipo") or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) { ?>
            <option value="<?php echo $RFP['id_tipo']; ?>" <?php if($ckbTipo == $RFP['id_tipo']){ echo "selected";} ?>><?php echo utf8_encode($RFP['descricao_tipo']); ?></option> 
            <?php } ?>
        </select>        
    </div>     
    <div style="clear:both; height:5px"></div>
</div>
<div style="clear:both; height:5px"></div>
<div style="width:100%; margin:10px 0">
    <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Dados Complementares</span>
    <hr style="margin:2px 0; border-top:1px solid #ddd">
</div>
<div id="divConvenio" style="border-style: solid;border-width: 1px; padding: 7px;border-color:#c8c8c8;">    
    <div style="float:left; width:22%">
        <span>GRAD/ESP:</span>
        <input id="txtGradEsp" name="txtGradEsp" maxlength="64" type="text" value="<?php echo $gradEsp; ?>" style="width:100%" />
    </div>    
    <div style="float:left; width:32%; margin-left: 1%;">
        <span>NIP:</span>
        <input id="txtNip" name="txtNip" maxlength="64" type="text" value="<?php echo $nip; ?>" style="width:100%" />
    </div>    
    <div style="float:left; width:44%; margin-left: 1%;">
        <span>Depto:</span>
        <input id="txtDepto" name="txtDepto" maxlength="64" type="text" value="<?php echo $depto; ?>" style="width:100%" />
    </div>    
    <div style="float:left; width:44%">
        <span>OM:</span>
        <input id="txtOm" name="txtOm" maxlength="64" type="text" value="<?php echo $om; ?>" style="width:100%" />
    </div>    
    <div style="float:left; width:32%; margin-left: 1%;">
        <span>TEL. OM:</span>
        <input id="txtTelOm" name="txtTelOm" maxlength="64" type="text" value="<?php echo $telOm; ?>" style="width:100%" />
    </div>    
    <div style="float:left; width:22%; margin-left: 1%;">
        <span>Ramal:</span>
        <input id="txtRamal" name="txtRamal" maxlength="16" type="text" value="<?php echo $ramal; ?>" style="width:100%" />
    </div>    
    <div style="float:left; width:44%">
        <span>Local Cobrança:</span>
        <input id="txtLocalCobranca" name="txtLocalCobranca" maxlength="64" type="text" value="<?php echo $localCobranca; ?>" style="width:100%" />
    </div>  
    <div style="float:left; width:32%; margin-left: 1%;">
        <span>Conta Corrente:</span>
        <input id="txtContFunc" name="txtContFunc" maxlength="70" type="text" value="<?php echo $contFunc; ?>" style="width:100%" />
    </div>    
    <div style="float:left; width:22%; margin-left: 1%; margin-top: 12px; text-align: right;">
        <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir()" title="Imprimir"><span class="ico-print icon-white"></span></button>
    </div>
    <div style="clear:both; height:5px"></div>    
</div> 
<div style="clear:both; height:5px"></div>
<div style="width:100%; margin:10px 0">
    <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Outros</span>
    <hr style="margin:2px 0; border-top:1px solid #ddd">
</div>
<div id="divConvenio" style="border-style: solid;border-width: 1px; padding: 7px;border-color:#c8c8c8;">
    <div style="float:left; width:18%">
        <span>Crachá Entregue:</span>
        <select class="select" id="ckbCracha" name="ckbCracha" style="width:100%;">
            <option value="null">Selecione</option>
            <option value="1" <?php if($ckbCracha == "1"){ echo "selected";} ?>>CONFECÇÃO</option>            
            <option value="2" <?php if($ckbCracha == "2"){ echo "selected";} ?>>SOLICITAÇÃO</option>
            <option value="3" <?php if($ckbCracha == "3"){ echo "selected";} ?>>RECEBIMENTO</option>            
            <option value="4" <?php if($ckbCracha == "4"){ echo "selected";} ?>>CRACHÁ ENTREGUE</option>
        </select>
    </div>
    <div style="float:left; width:15%; margin-left: 1%;">
        <span>Validade da Carteira:</span>
        <input id="txtValidadeCarteira" name="txtValidadeCarteira" maxlength="10" type="text" value="<?php echo $validadeCarteira; ?>" style="width:100%" type="text" class="input-medium datepicker"/>
    </div>    
    <div style="float:left; width:18%; margin-left: 1%;">
        <span>Desligar Titularidade:</span>
        <input name="ckbTrancarOld" id="ckbTrancarOld" value="<?php echo $ckbTrancar; ?>" type="hidden"/>
        <select class="select" old="<?php echo $ckbTrancar; ?>" id="ckbTrancar" name="ckbTrancar" style="width:100%;" <?php echo ($ckb_clb_trancamento_ == 0 ? "readonly" : "");?>>
            <option value="0" <?php if($ckbTrancar == "0"){ echo "selected";} ?>>NÃO</option>            
            <option value="1" <?php if($ckbTrancar == "1"){ echo "selected";} ?>>SIM</option>
        </select>
    </div>
    <div style="float:left; width:15%; margin-left: 1%;<?php if($ckbTrancar == "0"){ echo "display: none;";} ?>">
        <span>Data de Desligamento:</span>
        <input id="txtDataTrancamento" name="txtDataTrancamento" maxlength="10" type="text" value="<?php echo $dataTrancamento; ?>" style="width:100%" type="text" class="input-medium datepicker"/>
    </div>    
    <div style="float:left; width:20%; margin-left: 1%;<?php if($ckbTrancar == "0"){ echo "display: none;";} ?>">
        <span>Motivo de Desligamento:</span>
        <input id="txtMotivoTrancamento" name="txtMotivoTrancamento" type="text" value="<?php echo $trancarMotivo; ?>" style="width:100%" type="text" class="input-medium"/>
    </div>    
    <div style="float:left; margin-left: 1%; margin-top: 12px;">
        <button id="btnPrintTrancar" class="button button-blue btn-primary" type="button" onclick="imprimirTrancar()" title="Imprimir Desligar Titularidade" <?php if($ckbTrancar == "0"){ echo "disabled";} ?>><span class="ico-print icon-white"></span></button>
    </div>    
    <div style="float:left; margin-left: 1%; margin-top: 12px;">
        <button id="btnPrintPermanente" class="button button-green btn-primary" type="button" onclick="imprimirPermanente()" title="Imprimir Permanente"><span class="ico-print icon-white"></span></button>
    </div>    
    <div style="clear:both; height:5px"></div>
</div>     
<?php if ($ckb_adm_cli_read_ == 0 && $id != "") { ?>
<div style="clear:both; height:5px"></div>
<button id="btnSaveClube" type="button" class="btn dblue" style="float: right;line-height:19px; width:75px;margin-top: 1px;background-color: #308698 !important;"> Salvar <span class="icon-arrow-next icon-white"></span></button>
<?php } ?>
<div style="clear:both;height: 5px;"></div>