$("#btnfind").click(function () {
    refresh_orcamentos();
});

$('#tbCompras').dataTable({
    "iDisplayLength": -1,
    "ordering": false,
    "bFilter": false,
    "bInfo": false,
    "bLengthChange": false,
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": finalFind_orcamentos(),
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "fnDrawCallback": function () {
        refreshTotal_orcamentos();
    },
    "sPaginationType": "full_numbers"
});

function refresh_orcamentos() {
    var tblCompras = $('#tbCompras').dataTable();
    tblCompras.fnReloadAjax(finalFind_orcamentos());
}

function finalFind_orcamentos() {
    return "../../../Modulos/Estoque/Orcamentos_server_processing.php?filial=" +
    parent.$("#txtLojaSel").val() + "&imp=1&id=5&td=null&tp=null" + 
    ($("#txtNome").length > 0 ? "&cli=" : "&com=") + $("#txtId").val() +
    "&dti=" + $("#txt_dt_begin_orcamentos").val() + '&dtf=' + $("#txt_dt_end_orcamentos").val();
}

function refreshTotal_orcamentos() {
    $('#lblTotParc2').html('Número de Orçamentos :<strong>' + ($('#qtdparc').val() !== undefined ? $('#qtdparc').val() : "0") + '</strong>').show('slow');
    $('#lblTotParc3').html('Total em Orçamentos :<strong>' + numberFormat($('#totparc').val(), 1) + '</strong>').show('slow');
    $('#lblTotParc4').html('Ticket Médio :<strong>' + numberFormat(($('#totparc').val() / $('#qtdparc').val()), 1) + '</strong>').show('slow');
}