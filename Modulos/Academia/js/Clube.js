
$("#btnSaveClube").click(function (e) {
    if ($("#divTitularidade").attr("etit") === "1" && $("#txtTitularidade").val() === "null") {
        bootbox.alert("O campo Titularidade é Obrigatório!");
    } else if ($("#ckbTrancar").val() === "1" && $("#txtDataTrancamento").val() === "") {
        bootbox.alert("O campo Data de Desligamento é Obrigatório!");
    } else if ($("#ckbTrancar").val() === "1" && $("#txtMotivoTrancamento").val() === "") {
        bootbox.alert("O campo Motivo de Desligamento é Obrigatório!");
    } else if ($("#ckbTrancar").attr("old") === "0" && $("#ckbTrancar").val() === "1") {
        bootbox.confirm('Deseja trancar a titularidade desse associado? Ao trancar TODOS OS AGENDAMENTO FUTUROS serão excluídos de forma irreversível!', function (result) {
            if (result === true) {
                salvarCampos();      
            }
        });
    } else {
        salvarCampos();
    }
});

function salvarCampos() {
    var send = "btnSave=S" +
            "&txtId=" + $("#txtId").val() +
            "&ckbClbSocio=" + $("#ckbClbSocio").val() +
            "&ckbClbTitular=" + $("#ckbClbTitular").val() +
            "&ckbClbMalaDireta=" + $("#ckbClbMalaDireta").val() +
            "&txtTitularidade=" + $("#txtTitularidade").val() +
            "&ckbTipo=" + $("#ckbTipo").val() +
            "&txtGradEsp=" + $("#txtGradEsp").val() +
            "&txtNip=" + $("#txtNip").val() +
            "&txtDepto=" + $("#txtDepto").val() +
            "&txtOm=" + $("#txtOm").val() +
            "&txtTelOm=" + $("#txtTelOm").val() +
            "&txtRamal=" + $("#txtRamal").val() +
            "&txtLocalCobranca=" + $("#txtLocalCobranca").val() +
            "&ckbCracha=" + $("#ckbCracha").val() +
            "&txtValidadeCarteira=" + $("#txtValidadeCarteira").val() +
            "&ckbTrancar=" + $("#ckbTrancar").val() +
            "&ckbTrancarOld=" + $("#ckbTrancarOld").val() +
            "&txtDataTrancamento=" + $("#txtDataTrancamento").val() +
            "&txtMotivoTrancamento=" + $("#txtMotivoTrancamento").val() +
            "&txtContFunc=" + $("#txtContFunc").val();
    $.post("../Academia/form/Clube.php", send).done(function (data) {
        if (data.trim() === "YES") {
            atualizaStatusCli();
            FecharBoxF();
            if ($("#ckbTrancar").attr("old") === "0" && $("#ckbTrancar").val() === "1") {
                $("#btnPrintTrancar").attr("disabled", false);
                $("#ckbTrancar").attr("old", "1");                   
                $("#ckbTrancarOld").val("1");
                bootbox.confirm('Deseja imprimir o recibo de Trancamento de Titularidade?', function (result) {
                    if (result === true) {
                        imprimirTrancar();
                    } else {
                        bootbox.alert("Registros salvos com sucesso!", function () {
                            location.reload();
                        });
                    }
                });
            } else if ($("#ckbTrancar").attr("old") === "1" && $("#ckbTrancar").val() === "0") {
                $("#btnPrintTrancar").attr("disabled", true);
                $("#ckbTrancar").attr("old", "0");  
                $("#ckbTrancarOld").val("0");
                bootbox.alert("Registros salvos com sucesso!");                        
            } else {
                bootbox.alert("Registros salvos com sucesso!");    
            }
            checkTrancar();
        } else {
            bootbox.alert("Erro ao salvar os campos!");
        }
    });
}

function imprimir() {
    window.open("../../util/ImpressaoPdf.php?id=" + $("#txtId").val() + "&tpImp=I&NomeArq=Relat%C3%B3rio%20NIP&PathArq=../Modulos/Clube/modelos/RelatorioNip.php&emp=" + $("#txtMnFilial").val(), '_blank');
}

function imprimirPermanente() {
    window.open("../../Modulos/Academia/cupom_status.php?id=" + $("#txtId").val(), '_blank');
}

function imprimirTrancar() {
    window.open("../../util/ImpressaoPdf.php?pAlt=90&pLar=140&id=" + $("#txtId").val() + "&tpImp=I&NomeArq=CancelamentoDCC&PathArq=../Modulos/Academia/modelos/Recibo_Canc_Trancar.php", '_blank');
}

$('#ckbTrancar').change(function () {
    checkTrancar();
});

function checkTrancar() {
    if ($("#ckbTrancar").val() === "1") {
        $("#txtDataTrancamento, #txtMotivoTrancamento").parent().show();
    } else {
        $("#txtDataTrancamento, #txtMotivoTrancamento").parent().hide();
    }    
}