
$("#txtValorComissao").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});

function novaLinhaComissao() {
    if (validar()) {
        $.post("./../Academia/form/ComissaoCliente.php", {
            bntSave: true, txtIdComissao: $("#txtIdComissao").val(),
            txtId: $("#txtId").val(), txtTipoResponsavel: $("#txtTipoResponsavel").val(),
            txtTipoComissao: $("#txtTipoComissao").val(), txtValorComissao: $("#txtValorComissao").val(),
            txtSinal: $("#txtSinal").val(), txtSinalDesc: $("#txtSinalDesc").val()
        }).done(function (data) {
            if (data.trim() === "YES") {
                $("#txtIdComissao").val("");
                refreshComissao();
            } else {
                bootbox.alert("Erro ao efetuar esta operação!");
            }
        });
    }
}

$("#btnTpValor").click(function () {
    if ($("#tp_sinal").html() === "%") {
        $("#tp_sinal").html("$");
        $("#txtSinal").val("1");
    } else {
        $("#tp_sinal").html("%");
        $("#txtSinal").val("0");
    }
});

$("#btnTpValorDesc").click(function () {
    if ($("#tp_sinalDesc").html() === "N") {
        $("#tp_sinalDesc").html("S");
        $("#txtSinalDesc").val("1");
    } else {
        $("#tp_sinalDesc").html("N");
        $("#txtSinalDesc").val("0");
    }
});  

function validar() {
    if ($('#txtTipoResponsavel').val() === "null") {
        bootbox.alert("Preencha o campo Responsável!");
        return false;
    }
    if ($('#txtTipoComissao').val() === "null") {
        bootbox.alert("Preencha o campo Tipo!");
        return false;
    }
    if ($('#txtValorComissao').val() === "0,00") {
        bootbox.alert("Preencha o campo Valor!");
        return false;
    }
    return true;
}

function editLinhaComissao(id, responsavel, tipo, valor, tipo_valor, tipo_valor_desc) {
    $("#txtIdComissao").val(id);
    $("#txtTipoResponsavel").select2("val", responsavel);
    $("#txtTipoComissao").select2("val", tipo);
    $("#txtValorComissao").val(valor);
    $("#txtSinal").val(tipo_valor);
    $("#tp_sinal").html((tipo_valor === 1 ? "$" : "%"));    
    $("#txtSinalDesc").val(tipo_valor_desc);
    $("#tp_sinalDesc").html((tipo_valor_desc === 1 ? "S" : "N"));    
}

function delLinhaComissao(id) {
    bootbox.confirm('Confirma a exclusão?', function (result) {
        if (result === true) {
            $.post("./../Academia/form/ComissaoCliente.php", {bntDelete: true, txtIdComissao: id}).done(function (data) {
                if (data.trim() === "YES") {
                    refreshComissao();
                } else {
                    bootbox.alert("Erro ao efetuar esta operação!" + data);
                }
            });
        } else {
            return;
        }
    });
}

function novaLinhaPadrao() {
    if ($('#txtTipoComissaoPadrao').val() !== "null") {
        bootbox.confirm('Confirma a adição destas regras?', function (result) {
            if (result === true) {
                $.post("./../Academia/form/ComissaoCliente.php", {bntLinhaPadrao: true, txtId: $("#txtId").val(), txtIdComissao: $("#txtTipoComissaoPadrao").val()}).done(function (data) {
                    if (data.trim() === "YES") {
                        $("#txtTipoComissaoPadrao").select2("val", "null");
                        refreshComissao();
                    } else {
                        bootbox.alert("Erro ao efetuar esta operação!" + data);
                    }
                });
            } else {
                return;
            }
        });
    } else { 
        bootbox.alert("Selecione uma Comissão padrão por Cliente!");
    }
}

function finalFindComissao() {
    return "./../Academia/ajax/Comissao_server_processing.php?cli=" + $("#txtId").val();
}

var tbComissao = $('#tblComissao').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bFilter": false,
    "aoColumns": [
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false}],
    "sAjaxSource": finalFindComissao(),
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

function refreshComissao() {
    tbComissao.fnReloadAjax(finalFindComissao());
}