$(document).ready(function () {

    $('#txtDataIni, #txtDataFim').datetimepicker({
        dateFormat: lang["dmy"].toLowerCase().substring(0, 8),
        hourText: 'Hora',
        minuteText: 'Minutos',
        timeText: 'Horas',
        closeText: 'Feito',
        currentText: 'Hoje',
        timeInput: true,
        timeFormat: "HH:mm"
    });

    $('#txtDataIni').datetimepicker('setDate', moment().add(-1, 'days').format(lang["dmy"]));
    $('#txtDataFim').datetimepicker('setDate', moment().add(1, 'days').format(lang["dmy"]));

    $("#txtTpUsuario").change(function (event) {
        $("#txtDesc, #txtIdUsuario").val("");
        $("#txtDesc").parent().hide();
        if ($("#txtTpUsuario").val() === "C" || $("#txtTpUsuario").val() === "E" || $("#txtTpUsuario").val() === "P") {
            $("#txtDesc").parent().show();
        }
    });

    $("#txtTpUsuario").trigger("change");

    $(function () {
        $("#txtDesc").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "./../CRM/ajax.php",
                    dataType: "json",
                    data: {
                        q: request.term,
                        t: $("#txtTpUsuario").val()
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            minLength: 3,
            select: function (event, ui) {
                $("#txtIdUsuario").val(ui.item.id);
            }
        });
    });

    $("#btnfind").click(function (event) {
        if ($("#txtDataIni").val() === "" && $("#txtDataFim").val() === "") {
            bootbox.alert("É necessário definir ao menos uma data!");
            return;
        }
        $("#loader").show();
        $("#tblAcessos").dataTable().fnDestroy();
        var tblAcessos = $('#tblAcessos').dataTable({
            "iDisplayLength": 20,
            "aLengthMenu": [20, 30, 40, 50, 100],
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": montaUrl("N"),
            "bFilter": false,
            "bSort": false,
            "bPaginate": false,
            "fnInitComplete": function (oSettings, json) {
                trataImprimir();
                resumoAcessos();
            },
            'oLanguage': {
                'sInfo': "",
                'sInfoEmpty': "",
                'sInfoFiltered': "",
                'sLengthMenu': "",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sSearch': "Pesquisar:",
                'sZeroRecords': "Não foi encontrado nenhum resultado"},
            "sPaginationType": "full_numbers"
        });

    });

    function trataImprimir() {
        if ($('#tblAcessos').dataTable().fnGetData().length > 0) {
            $("#btnPrint").removeAttr('disabled');
        }
        $("#loader").hide();
    }

    function resumoAcessos() {
        $("#tblResumoAcessos").dataTable().fnDestroy();
        $('#tblResumoAcessos').dataTable({
            "aoColumnDefs": [{
                    "aTargets": [0], fnCreatedCell: function (nTd) {
                        $(nTd).css({"width": "200px"});
                    }
                }],
            "iDisplayLength": 20,
            "aLengthMenu": [20, 30, 40, 50, 100],
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": montaUrl("R"),
            "bFilter": false,
            "bSort": false,
            "bPaginate": false,
            'oLanguage': {
                'sInfo': "",
                'sInfoEmpty': "",
                'sInfoFiltered': "",
                'sLengthMenu': "",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sSearch': "Pesquisar:",
                'sZeroRecords': "Não foi encontrado nenhum resultado"},
            "sPaginationType": "full_numbers"
        });

    }

    $("#btnPrint").click(function (event) {
        window.open(montaUrl('S'));
    });

    function montaUrl(tipo) {
        var url = "ajax/Acessos_server_processing.php";
        url += "?txtTpUser=" + $("#txtTpUsuario").val();
        url += "&txtTpAcesso=" + $("#txtTipoAcesso").val();
        url += "&txtFilial=" + $("#txtLojaSel").val();
        if ($("#txtTpUsuario").val() !== "T" && $("#txtIdUsuario").val() !== "") {
            url += "&txtIdUser=" + $("#txtIdUsuario").val();
        }
        if ($("#txtDataIni").val() !== "") {
            url += "&txtDtHrInicial=" + $("#txtDataIni").val().replace(/\//g, "_");
        }
        if ($("#txtDataFim").val() !== "") {
            url += "&txtDtHrFinal=" + $("#txtDataFim").val().replace(/\//g, "_");
        }
        if ($("#mscAmbiente").val() !== null) {
            url += "&txtAmbiente=" + $("#mscAmbiente").val();
        }
        if ($("#mscCatraca").val() !== null) {
            url += "&txtCatraca=" + $("#mscCatraca").val();
        }
        if ($("#mscGrupoAce").val() !== null) {
            url += "&txtGrupo=" + $("#mscGrupoAce").val();
        }
        if ($("#txtCredencial").length > 0 && $("#txtCredencial").val() !== "null") {
            url += "&txtCredencial=" + $("#txtCredencial").val();
        }        
        if (tipo === "S") {
            url += "&ex=1";
        }
        if (tipo === "R") {
            url += "&isResumo=S";
        }
        return url;
    }
});