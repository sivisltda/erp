
$("#btnSaveCLivres").click(function (e) {
    var send = "btnSave=S&";
    $("[name='txtCLid']").each(function() {
        send += this.id + "=" + this.value + "&";
    });
    $("[name='txtCLConteudo']").each(function() {
        send += this.id + "=" + this.value + "&";
    });
    send += "txtCLPessoa=" + $("#txtId").val();
    $.post("../Academia/form/CamposLivres.php", send).done(function (data) {
        if (data.trim() === "YES") {
            bootbox.alert("Registros salvos com sucesso!");
        } else {
            bootbox.alert("Erro ao salvar os campos!");
        }
    });        
});