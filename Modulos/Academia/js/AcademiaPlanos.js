var descontos = [];
var cancelamentos = [];
var descAccept = 0;
var setTaxaAdesao = false;

$("#txtValServ").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});

function atualizaTotCredito() {
    $("#lblTotalDisp span").remove();
    $.getJSON('./../Academia/form/ClientesPlanosServer.php', {saldoCredito: $("#txtId", window.parent.document).val()}, function (j) {
        if (j !== null) {
            var color = "color: green;";
            if (textToNumber(j[0].saldo_credito) < 0) {
                color = "color: red;";
            }
            var totDisp = "<span style=\"display:block;font-size: 11px;\"> <span style=\"font-size: 16px;padding-top: 6px;\"> Saldo: </span>" + lang["prefix"] + "<span style=\"font-size: 17px;font-weight: bold;" + color + "\"> " + j[0].saldo_credito + "</span></span>";
            $("#lblTotalDisp").append(totDisp);
        }
    });
}

function listPlanosClientes() {
    $('#tblPlanosClientes').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "./../Academia/form/ClientesPlanosServer.php?listPlanosCliente=" + $("#txtId").val() + "&inv=" + ($("#ckb_planos_ocultos").is(':checked') ? 1 : 0),
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });
    if (localStorage.getItem("ckb_planos_ocultos") === "1") {
        $("#ckb_planos_ocultos").click();
    }
}

function AtualizaListPlanosClientes() {
    var tblPlanosClientes = $("#tblPlanosClientes").dataTable();
    tblPlanosClientes.fnReloadAjax("./../Academia/form/ClientesPlanosServer.php?listPlanosCliente=" + $("#txtId").val() + "&inv=" + ($("#ckb_planos_ocultos").is(':checked') ? 1 : 0), selecionaLinha);
    PagarTudoItens();
}

$("#ckb_planos_ocultos").change(function () {
    localStorage.setItem("ckb_planos_ocultos", ($("#ckb_planos_ocultos").is(':checked') ? 1 : 0));
    AtualizaListPlanosClientes();
});

function listHistoricoServicos() {
    $('#tblServicoHistorico').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "./../Academia/form/ClientesPlanosServer.php?listPagamentosServicos=S&favorecido=" + $("#txtId").val(),
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });
}

function AtualizaListHistoricoServicos() {
    var tblPlanosClientes = $("#tblServicoHistorico").dataTable();
    tblPlanosClientes.fnReloadAjax("./../Academia/form/ClientesPlanosServer.php?listPagamentosServicos=S&favorecido=" + $("#txtId").val());
}

function listHistoricoLink() {
    $('#tblLinkHistorico').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "./../Academia/form/ClientesPlanosServer.php?listPagamentosLink=S" + 
        "&favorecido=" + $("#txtId").val() + "&dti=" + $("#txt_dt_begin_link").val() + "&dtf=" + $("#txt_dt_end_link").val(),
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });
}

function AtualizaListHistoricoLink() {
    var tblPlanosBoleto = $("#tblLinkHistorico").dataTable();
    tblPlanosBoleto.fnReloadAjax("./../Academia/form/ClientesPlanosServer.php?listPagamentosLink=S" + 
    "&favorecido=" + $("#txtId").val() + "&dti=" + $("#txt_dt_begin_link").val() + "&dtf=" + $("#txt_dt_end_link").val());
}

function makeLinkOnline() {
    if ($("#tblItensPag tbody > tr").length > 0) {
        bootbox.confirm('Confirma a geração do Link com os itens selecionados?', function (value) {
            if (value === true) {
                $("#loader").show();
                $.post("../../../util/dcc/makeLinkOnline.php", "salvaLink=S" + 
                        "&txtIdCli=" + $("#txtId").val() +
                        "&txtQtdItens=" + $("#txtQtdItensPagar").val() +
                        "&" + $("#tblItensPag :input").serialize()).done(function (data) {
                    if (!isNaN(data.trim())) {
                        console.log(data.trim());
                        limpaItensPag();
                        AtualizaListPlanosClientes();
                        AtualizaListHistoricoLink();                                                
                    } else {
                        bootbox.alert("Erro ao gerar o Link!<br/>" + data.trim());
                    }
                    $("#loader").hide();                                                
                });
            } else {
                return;
            }
        });
    } else {
        bootbox.alert("Selecione pelo menos um item para geração do Link!");
    }
}

function EnviarLink(idParc, tipo) {
    /*$.ajax({
        type: "GET", url: "./../Academia/form/ClientesPlanosServer.php",
        data: {getEmailBoleto: 'S'}, dataType: "json",
        success: function (data) {
            bootbox.prompt({
                title: "Selecione o modelo de E-mail",
                inputType: 'select',
                inputOptions: data,
                callback: function (result) {
                    if (result) {
                        let email = $("#" + $(Array.from($('[id*="txtTpContato_"]')).filter(function (item) {
                            return item.value === "2";
                        })[0]).attr("id").replace("txtTpContato_", "txtTxContato_")).val();
                        let send = [["", "", "", "", "", "", "", "", "", email, $("#txtId").val(), idParc, tipo]];
                        $("#source").dialog({modal: true});
                        $("#progressbar").progressbar({value: 0});
                        $("#pgIni").html("0");
                        $("#pgFim").html("1");
                        $("#txtTotalSucesso, #txtTotalErro").val(0);
                        sendEmailrow(result, send);
                    }
                }
            });
        },
        error: function (error) {
            bootbox.alert(error.responseText + "!");
        }
    });*/
}

function cancelarLinkOnline(id_boleto) {
    bootbox.confirm('Confirma o cancelamento do Link?', function (value) {
        if (value === true) {
            $("#loader").show();
            $.post("../../../util/dcc/makeLinkOnline.php", "cancelarLink=S" + 
                    "&txtIdLink=" + id_boleto).done(function (data) {
                if (!isNaN(data.trim())) {
                    console.log(data.trim());
                    AtualizaListPlanosClientes();
                    AtualizaListHistoricoLink();                                                
                } else {
                    bootbox.alert("Erro ao cancelar o Link!<br/>" + data.trim());
                }
                $("#loader").hide();                                                
            });
        } else {
            return;
        }
    });
}

function excluirLinkOnline(id_boleto) {
    bootbox.confirm('Confirma a exclusão do Link?', function (value) {
        if (value === true) {
            $("#loader").show();
            $.post("../../../util/dcc/makeLinkOnline.php", "excluirLink=S" + 
                    "&txtIdLink=" + id_boleto).done(function (data) {
                if (!isNaN(data.trim())) {
                    console.log(data.trim());
                    AtualizaListPlanosClientes();
                    AtualizaListHistoricoLink();                                                
                } else {
                    bootbox.alert("Erro ao excluir o Link!<br/>" + data.trim());
                }
                $("#loader").hide();                                                
            });
        } else {
            return;
        }
    });
}

$("#btnNovoLink").click(function (event) {
    makeLinkOnline();    
});

$("#btnfindLink").click(function (event) {
    AtualizaListHistoricoLink();
});

function listHistoricoBoleto() {
    $('#tblBoletoHistorico').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "./../Academia/form/ClientesPlanosServer.php?listPagamentosBoleto=S" + 
        "&favorecido=" + $("#txtId").val() + "&dti=" + $("#txt_dt_begin_boleto").val() + "&dtf=" + $("#txt_dt_end_boleto").val(),
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });
}

function AtualizaListHistoricoBoleto() {
    var tblPlanosBoleto = $("#tblBoletoHistorico").dataTable();
    tblPlanosBoleto.fnReloadAjax("./../Academia/form/ClientesPlanosServer.php?listPagamentosBoleto=S" + 
    "&favorecido=" + $("#txtId").val() + "&dti=" + $("#txt_dt_begin_boleto").val() + "&dtf=" + $("#txt_dt_end_boleto").val());
}

function makeBoletoOnline(data) {
    if ($("#tblItensPag tbody > tr").length > 0) {
        bootbox.confirm('Confirma a geração do boleto com os itens selecionados?', function (value) {
            if (value === true) {
                $("#loader").show();
                $.post("../../../util/dcc/makeBoletoOnline.php", "salvaBoleto=S" + 
                        "&txtIdCli=" + $("#txtId").val() + 
                        "&txtDataVencimento=" + data +
                        "&txtQtdItens=" + $("#txtQtdItensPagar").val() +
                        "&" + $("#tblItensPag :input").serialize()).done(function (data) {
                    if (!isNaN(data.trim())) {
                        console.log(data.trim());
                        limpaItensPag();
                        AtualizaListPlanosClientes();
                        AtualizaListHistoricoBoleto();                                                
                    } else {
                        bootbox.alert("Erro ao gerar o boleto!<br/>" + data.trim());
                    }
                    $("#loader").hide();                                                
                });
            } else {
                return;
            }
        });
    } else {
        bootbox.alert("Selecione pelo menos um item para geração do boleto!");
    }
}

function processarBoletoOnline(id_boleto) {
    bootbox.confirm('Confirma o processamento do boleto?', function (value) {
        if (value === true) {
            $("#loader").show();
            $.post("../../../util/dcc/makeBoletoOnline.php", "processarBoleto=S" + 
                    "&txtIdBol=" + id_boleto).done(function (data) {
                if (!isNaN(data.trim())) {
                    console.log(data.trim());
                    limpaItensPag();
                    AtualizaListPlanosClientes();
                    AtualizaListHistoricoBoleto();                                                
                } else {
                    bootbox.alert("Erro ao gerar o boleto!<br/>" + data.trim());
                }
                $("#loader").hide();                                                
            });
        } else {
            return;
        }
    });
}

function cancelarBoletoOnline(id_boleto) {
    bootbox.confirm('Confirma o cancelamento do boleto?', function (value) {
        if (value === true) {
            $("#loader").show();
            $.post("../../../util/dcc/makeBoletoOnline.php", "cancelarBoleto=S" + 
                    "&txtIdBol=" + id_boleto).done(function (data) {
                if (!isNaN(data.trim())) {
                    console.log(data.trim());
                    AtualizaListPlanosClientes();
                    AtualizaListHistoricoBoleto();                                                
                } else {
                    bootbox.alert("Erro ao cancelar o boleto!<br/>" + data.trim());
                }
                $("#loader").hide();                                                
            });
        } else {
            return;
        }
    });
}

function excluirBoletoOnline(id_boleto) {
    bootbox.confirm('Confirma a exclusão do boleto?', function (value) {
        if (value === true) {
            $("#loader").show();
            $.post("../../../util/dcc/makeBoletoOnline.php", "excluirBoleto=S" + 
                    "&txtIdBol=" + id_boleto).done(function (data) {
                if (!isNaN(data.trim())) {
                    console.log(data.trim());
                    AtualizaListPlanosClientes();
                    AtualizaListHistoricoBoleto();                                                
                } else {
                    bootbox.alert("Erro ao excluir o boleto!<br/>" + data.trim());
                }
                $("#loader").hide();                                                
            });
        } else {
            return;
        }
    });
}

$("#btnNovoBoleto").click(function (event) {
    bootbox.prompt({title: "Selecione a data de vencimento do boleto:", inputType: 'date', callback: function (data) {
        if (moment(data).isValid()) {
            makeBoletoOnline(moment(data).format(lang["dmy"]));    
        } else {
            bootbox.alert("Informar uma data válida!");                
        }
    }});    
});

$("#btnfindBoleto").click(function (event) {
    AtualizaListHistoricoBoleto();
});

function FecharBoxPagamento(recibo) {
    parent.descontos = [];
    parent.cancelamentos = [];
    $("#newbox").remove();
    AtualizaListPlanosClientes();
    AtualizaListHistoricoServicos(0);
    if (recibo > 0 && $("#txtId").val() !== "") {
        abrirTelaBox("../CRM/ImprimirEmail.php?id=" + $("#txtId").val() + "&doc=" + recibo + "&tp=2", 280, 400);
    }
}

function AtualizaPagPlanosClientes(idPlano) {
    if (idPlano === undefined) {
        idPlano = localStorage.getItem("idPlan");
    } else {
        localStorage.setItem("idPlan", idPlano);
    }
    var tblPagamentosPlano = $('#tblPagamentosPlano').dataTable();
    tblPagamentosPlano.fnReloadAjax("./../Academia/form/ClientesPlanosServer.php?listPagamentosPlano=" + $("#txtId").val() + "&idPlano=" + idPlano + "&serasa=" + $("#ckbInativo").val());
    var tblPagamentosCotas = $('#tblPagamentosCotas').dataTable();
    tblPagamentosCotas.fnReloadAjax("./../Academia/form/ClientesPlanosServer.php?isCota=S&listPagamentosPlano=" + $("#txtId").val() + "&idPlano=" + idPlano + "&serasa=" + $("#ckbInativo").val());
    PagarTudoItens();
}

function PagarTudo() {
    $.getJSON("./../Academia/form/ClientesPlanosServer.php?listPagamentosDependentes=" + $("#txtId").val(), function (data) {
        $.each(data["aaData"], function (key, val) {
            if (!moment(val[5], "DD/MM/YYYY").isAfter(moment("01/" + $("#txtPagarTudo").val(), "DD/MM/YYYY").endOf('month'))) {
                $(val[6]).find("button.green").click();
            }
        });
    }).done(function () {
        $("#tblPlanosClientes").find('.detalhe').each(function (index) {
            let i = 0;
            let chartData = [];
            let idPlano = $(this).attr('data-id');
            $.getJSON("./../Academia/form/ClientesPlanosServer.php?listPagamentosPlano=" + $("#txtId").val() + "&idPlano=" + idPlano + "&serasa=" + $("#ckbInativo").val(), function (data) {
                $("#tblPagamentosPlano tbody").html("");
                $.each(data["aaData"], function (key, val) {
                    $("#tblPagamentosPlano tbody").append("<tr><td>" + val[0] + "</td><td>" + val[1] + "</td><td>" + val[2] + "</td><td>" + val[3] + "</td><td>" + val[4] + "</td><td>" + val[5] + "</td><td>" + val[6] + "</td></td>");
                    $('#tblPlanosClientes').find('td').removeClass('selected');
                    $("#txtIdSel").val(idPlano);
                    chartData[i] = {plano: idPlano, vencimento: val[3], acao: val[6]};
                    i++;
                });
            }).done(function () {
                $.each(chartData, function (key, val) {
                    if (!moment(val.vencimento, "DD/MM/YYYY").isAfter(moment("01/" + $("#txtPagarTudo").val(), "DD/MM/YYYY").endOf('month'))) {
                        $(val.acao).find("button.green").click();
                    }
                });
            });
        });
    });
}

function PagarTudoItens() {
    if ($("#txtPagarTudo").length > 0) {
        $.getJSON("./../Academia/form/ClientesPlanosServer.php?listPagMens=" + $("#txtId").val(), function (data) {
            let options = "";
            $.each(data, function (key, val) {
                options += "<option value=\"" + val + "\">" + val + "</option>";
            });
            $("#txtPagarTudo").select2().html(options);
            $("#txtPagarTudo").val(moment().format('MM/YYYY')).trigger('change');
        });
    }
}

function selecionaLinha() {
    var idPlano = $("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id');
    if ($("#txtIdSel").val() > 0 && idPlano === undefined) {
        $('#tblPlanosClientes').find('a[data-id="' + $("#txtIdSel").val() + '"]').parent().parent().parent().parent().parent().parent().find('td').addClass('selected');
    }
}

$('#tblPlanosClientes tbody').on('click', 'tr', function () {
    if ($(this).find('td').hasClass('selected')) {
        $(this).find('td').removeClass('selected');
        if ($('#divPagamentos').is(':visible')) {
            AtualizaPagPlanosClientes(0);
        }
        $("#btnRenovarMens, #btnRenovarMensAnt, #btnRenovarMensRen").attr("disabled", true);
    } else {
        $('#tblPlanosClientes').find('td.selected').removeClass('selected');
        $(this).find('td').addClass('selected');
        if ($('#divPagamentos').is(':visible')) {
            AtualizaPagPlanosClientes($("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id'));
        }
        $("#btnRenovarMens, #btnRenovarMensAnt, #btnRenovarMensRen").attr("disabled", false);
    }
    $("#txtIdSel").val($("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id'));
});

$('#tblPlanosClientes').on('click', '.detalhe', function (e) {
    e.preventDefault();
    $('#tblPlanosClientes').find('td.selected').removeClass('selected');
    $(this).parents('tr').find('td').addClass('selected');
    AbrirBox(5);
});

$('#tblPlanosClientes').on('click', '.renovar', function (e) {
    e.preventDefault();
    $('#tblPlanosClientes').find('td.selected').removeClass('selected');
    $(this).parents('tr').find('td').addClass('selected');
    AbrirBox(3);
});

$('#tblPlanosClientes').on('click', '.congelamento', function (e) {
    e.preventDefault();
    $('#tblPlanosClientes').find('td.selected').removeClass('selected');
    $(this).parents('tr').find('td').addClass('selected');
    AbrirBox(4);
});

$('#tblPlanosClientes').on('click', '.remover', function (e) {
    var idPlano = $(this).attr('data-id');
    $('#tblPlanosClientes').find('td.selected').removeClass('selected');
    $(this).parents('tr').find('td').addClass('selected');
    bootbox.confirm('Todas as operações em andamento serão canceladas, confirma o exclusão da modalidade ?', function (result) {
        if (result === true) {
            $("#tblItensPag tbody > tr").remove();
            $.post("./../Academia/form/ClientesPlanosServer.php", "removerContrato=" + idPlano).done(function (data) {
                if (data.trim() === "PAGAMENTO") {
                    bootbox.alert("Não é possível excluir pois existe um pagamento efetuado para esta modalidade!");
                } else if (data.trim() === "YES") {
                    AtualizaListPlanosClientes();
                    limpaItensPag();
                } else {
                    bootbox.alert("Erro ao excluir modalidade!");
                }
            });
        } else {
            return;
        }
    });
});

$('#tblPlanosClientes').on('click', '.cancDcc', function (e) {
    e.preventDefault();
    $('#tblPlanosClientes').find('td.selected').removeClass('selected');
    $(this).parents('tr').find('td').addClass('selected');
    AbrirBox(14);
});

function trataDivPagamento(tipo) {
    if (tipo === "P" && $("li[id*='li_pag_']").is(':visible')) {
        $("#divPagamentos").hide();
        return;
    } else if (tipo === "C" && $("#li_credito").is(':visible')) {
        $("#divPagamentos").hide();
        return;
    } else if (tipo === "P") {
        $("#li_credito_1").parent().parent().parent().css("width", ($("#tab14").attr("visible") === "1" ? "63%" : "100%"));
        $("li[id*='li_pag_']").show();
        $("#li_pag_8").parent().parent().show();
        $("li[id*='li_credito_']").hide();
        $('a[href="#tab10"]').click();
        $("#tblPagamentosPlano").dataTable().fnDestroy();
        listPagamentosPlano($("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id') === undefined ? "0" : $("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id'));
        listPagamentosCotas($("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id') === undefined ? "0" : $("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id'));
        $("#txtServico").select2("val", "");
        $("#txtProduto").select2("val", "");
        $("#btnRenovarMens, #btnRenovarMensAnt, #btnRenovarMensRen").show();
    } else {
        $("#li_credito_1").parent().parent().parent().css("width", "100%");
        $("li[id*='li_pag_']").hide();
        $("#li_pag_8").parent().parent().hide();
        $("li[id*='li_credito_']").show();
        $('a[href="#tab12"]').click();
        $("#btnRenovarMens, #btnRenovarMensAnt, #btnRenovarMensRen").hide();
        listExtratoCredito();
    }
    $("#txtServico").trigger("change");
    $("#txtProduto").trigger("change");
    $("#divPagamentos").show();
}

function listPagamentosPlano(idPlano) {
    $('#tblPagamentosPlano').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "./../Academia/form/ClientesPlanosServer.php?listPagamentosPlano=" + $("#txtId").val() + "&idPlano=" + idPlano + "&serasa=" + $("#ckbInativo").val(),
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        "fnDrawCallback": function (data) {
            makeFilters();
        },
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });
}

function listPagamentosCotas(idPlano) {
    if (!$.fn.DataTable.isDataTable('#tblPagamentosCotas')) {
        $('#tblPagamentosCotas').dataTable({
            "iDisplayLength": 20,
            "aLengthMenu": [20, 30, 40, 50, 100],
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "./../Academia/form/ClientesPlanosServer.php?isCota=S&listPagamentosPlano=" + $("#txtId").val() + "&idPlano=" + idPlano + "&serasa=" + $("#ckbInativo").val(),
            "bFilter": false,
            "bSort": false,
            "bPaginate": false,
            'oLanguage': {
                'sInfo': "",
                'sInfoEmpty': "",
                'sInfoFiltered': "",
                'sLengthMenu': "",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sSearch': "Pesquisar:",
                'sZeroRecords': "Não foi encontrado nenhum resultado"},
            "sPaginationType": "full_numbers"
        });
    }
}

function limpaItensPag() {
    $("#tblItensPag tbody > tr").remove();
    $("#txtQtdItensPagar").val("0");
    if ($("li[id*='li_pag_']").is(':visible')) {
        AtualizaPagPlanosClientes($("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id'));
        AtualizaPagPlanosDependentes();
    }
    atualizaTotal();
}

function incluirItemPag(tipo, idProd, idPlano, tpAluno, Dbloq, isDCC) {    
    if (validaItemPag(tipo, idPlano, idProd)) {
        /*if (isDCC === 'S') {
            limpaItensPag();
        }*/
        var descr = "";
        var disDesc = "";
        var dataMulta = "";
        var valor = 0;
        var multa = numberFormat(0);
        var valordesc = numberFormat(0);
        var dataMulta = "";
        var qtd = "1";
        if (tipo === "MENSALIDADE") {
            if (tpAluno === "T") {
                dataMulta = $('button[onclick="incluirItemPag(\'MENSALIDADE\',' + idProd + ',' + idPlano + ',\'T\',' + Dbloq + ',\'' + isDCC + '\')"]').parent().parent().parent().parent().find("td:nth-child(4)").text().trim();
                descr = $("#tblPlanosClientes").find('tr:has(td.selected)').find('td:first').text() + " - " + dataMulta;
                valor = $('button[onclick="incluirItemPag(\'MENSALIDADE\',' + idProd + ',' + idPlano + ',\'T\',' + Dbloq + ',\'' + isDCC + '\')"]').parent().parent().parent().parent().find("td:nth-child(2)").text().trim();
            } else {
                dataMulta = $('button[onclick="incluirItemPag(\'MENSALIDADE\',' + idProd + ',' + idPlano + ',\'D\',' + Dbloq + ',\'' + isDCC + '\')"]').parent().parent().parent().parent().find("td:nth-child(6)").text().trim();
                descr = $('button[onclick="incluirItemPag(\'MENSALIDADE\',' + idProd + ',' + idPlano + ',\'D\',' + Dbloq + ',\'' + isDCC + '\')"]').parent().parent().parent().parent().find("td:nth-child(2)").text().trim() + " - " + $('button[onclick="incluirItemPag(\'MENSALIDADE\',' + idProd + ',' + idPlano + ',\'D\',' + Dbloq + ',\'' + isDCC + '\')"]').parent().parent().parent().parent().find("td:nth-child(1)").text().trim();
                valor = $('button[onclick="incluirItemPag(\'MENSALIDADE\',' + idProd + ',' + idPlano + ',\'D\',' + Dbloq + ',\'' + isDCC + '\')"]').parent().parent().parent().parent().find("td:nth-child(4)").text().trim();
            }
            valordesc = verificaConvenio(textToNumber(valor), idProd, idPlano, $("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id'));
            if (!(isDCC === 'S' && $("#dcc_multa").val() === "1")) {
                multa = verificaMulta(textToNumber(valor), dataMulta);
            }
            taxaAdesao(idPlano);
            if (Dbloq === '1') {
                disDesc = "disabled";
            }
        } else if (tipo === "SERVIÇO") {
            if (idProd === "") {
                idProd = $("#txtServico").val();
                descr = $("#txtServico").select2('data').text;
                valor = $("#txtValServFinal").val();
                qtd = $("#txtQtdServ").val();
            } else {
                descr = $('button[onclick="incluirItemPag(\'SERVIÇO\',' + idProd + ',\'\')"]').parent().parent().parent().parent().find("td:nth-child(1)").text().trim();
                valor = $('button[onclick="incluirItemPag(\'SERVIÇO\',' + idProd + ',\'\')"]').parent().parent().parent().parent().find("td:nth-child(2)").text().trim();
            }
            valordesc = verificaConvenio(textToNumber(valor), idProd, idPlano, null);
        } else if (tipo === "PRODUTO") {
            if (idProd === "") {
                idProd = $("#txtProduto").val();
                descr = $("#txtProduto").select2('data').text;
                valor = $("#txtValProdFinal").val();
                qtd = $("#txtQtdProd").val();
            } else {
                descr = $('button[onclick="incluirItemPag(\'PRODUTO\',' + idProd + ',\'\')"]').parent().parent().parent().parent().find("td:nth-child(1)").text().trim();
                valor = $('button[onclick="incluirItemPag(\'PRODUTO\',' + idProd + ',\'\')"]').parent().parent().parent().parent().find("td:nth-child(2)").text().trim();
            }
            valordesc = verificaConvenio(textToNumber(valor), idProd, idPlano, null);
        } else if (tipo === 'CREDITO') {
            descr = 'AQUISIÇÃO CREDITO CARTEIRA';
            valor = $("#txtValCredito").val();
            disDesc = "disabled";
        } else if (tipo === 'CARTEIRA') {
            descr = 'PAGAMENTO DEBITO CARTEIRA';
            valor = $('button[onclick="incluirItemPag(\'CARTEIRA\',466,' + idPlano + ',\'\',\'S\',\'S\')"]').parent().parent().parent().parent().find("td:nth-child(3)").text().trim();
            disDesc = "disabled";
        }
        if ($("#txtAcessoDesc").val() !== "1") {
            disDesc = "disabled";
        }
        var nItens = eval($('#txtQtdItensPagar').val()) + 1;
        var btDesc = "  <input " + disDesc + " id=\"txtDesconto_" + nItens + "\" type=\"text\" maxlength=\"9\"  class=\"input-xlarge\" value=\"" + valordesc + "\" style=\"width: 57%;\">\n\
                        <input " + disDesc + " id=\"txtSinal_" + nItens + "\" value=\"P\" type=\"hidden\">\n\
                        <button " + disDesc + " onclick=\"trocaSinal(" + nItens + ")\" class=\"btn btn-primary\" type=\"button\" style=\"padding:2px 7px;background-color: #308698 !important;width:20%;height: 27px;margin-top: 3px;margin-left: -3px;\"><span id=\"tp_sinal_" + nItens + "\">%</span></button> \n\
                        <button " + disDesc + " onclick=\"aplicarDesconto(" + nItens + ',1,' + idProd + ")\" class=\"btn green\" type=\"button\" style=\"padding:2px 5px;width:20%;margin-left: -3px;height: 27px;margin-top: 3px;\"><span class=\"ico-arrow-right icon-white\"></span></button>";
        var tbody = "";
        tbody += "<tr id='linhaItem_" + nItens + "' data-id=\"" + nItens + "\" data-tp=\"" + tipo + "\"> " +
                "<input type=\"hidden\" id=\"txtIdItem_" + nItens + "\" name=\"txtIdItem_" + nItens + "\" value=\"" + idProd + "\"/> " +
                "<input type=\"hidden\" id=\"txtIdPlano_" + nItens + "\" name=\"txtIdPlano_" + nItens + "\" value=\"" + idPlano + "\"/> " +
                "<td><input type=\"hidden\" id=\"txtTipo_" + nItens + "\" name=\"txtTipo_" + nItens + "\" value=\"" + tipo + "\"/><center> " + tipo + "</center></td> " +
                "<td> " + descr + "</td> " +
                "<td><input type=\"hidden\" id=\"txtQtd_" + nItens + "\" name=\"txtQtd_" + nItens + "\" value=\"" + qtd + "\"/><center>" + qtd + "</center></td> " +
                "<td><input type=\"hidden\" id=\"txtValor_" + nItens + "\" name=\"txtValor_" + nItens + "\" value=\"" + valor + "\"/>" +
                "<input type=\"hidden\" id=\"txtMulta_" + nItens + "\" name=\"txtMulta_" + nItens + "\" value=\"" + multa + "\"/>" + valor + "</td> " +
                "<td>" + btDesc + "</td> " +
                "<td><input id=\"txtValFinal_" + nItens + "\" name=\"txtValFinal_" + nItens + "\" type=\"text\" class=\"input-xlarge\" value=\"" + valor + "\" readonly></td> " +
                "<td><center>" + ($("#txtCancelItemSelect").val() === "1" || textToNumber(multa) > 0 ? "<div style=\"text-align:left;\" class=\"btn-group\"> " +
                        "<button style=\"height: 13px;background-color: gray;\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\"> " +
                        "<span style=\"margin-top: 1px;\" class=\"caret\"></span></button><ul style=\"left:-130px;\" class=\"dropdown-menu\"> " +
                        (textToNumber(multa) > 0 ? "<li><a onclick=\"cancelarMultaPag(" + nItens + "," + idProd + ")\" data-id=\"" + nItens + "\" class=\"Abonar\">Cancelar Multa</a></li> " : "") +
                        ($("#txtCancelItemSelect").val() === "1" ? "<li><a onclick=\"removerLinhaPag(" + nItens + ")\" data-id=\"\" class=\"Excluir\">Excluir</a></li> " : "") +
                        "</ul></div>" : "") + "</center></td></tr>";
        $("#tblItensPag tbody").append(tbody);
        $("#txtQtdItensPagar").val(nItens);
        trocaSinal(nItens);
        aplicarDesconto(nItens);        
        $("#txtDesconto_" + nItens).priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"], centsLimit: parseInt($("#txtMnDesconDecimal").val())});
        /*if (isDCC === "S") {
            if (tipo === "CARTEIRA") {
                AbrirBox(13);
            } else {
                AbrirBox(12);
            }
        }*/
    }
    $("html, body").scrollTop($("#btnPag").offset().top);
}

function trocaSinal(id) {
    if ($("#tp_sinal_" + id).html() === "%") {
        $("#tp_sinal_" + id).html("$");
        $("#txtSinal_" + id).val("D");
    } else {
        $("#tp_sinal_" + id).html("%");
        $("#txtSinal_" + id).val("P");
    }
}

function taxaAdesao(idPlano) {
    if ($("#txtServicoAdesao").val() === "1") {
        $.ajax({
            type: "GET", data: {isAjax: 'S', verificaTaxaAdesao: idPlano},
            url: "./../Academia/form/ClientesFormServer.php", async: false
        }).done(function (data) {
            if ($.isNumeric(data) && data !== "0") {
                let result = true;
                $('#tabela_Itens  > tr').each(function () {
                    var dtitem = $(this).find("input[name*='txtIdItem_']").val();
                    if (eval(dtitem) === eval(data)) {
                        result = false;
                    }
                });
                if (result) {
                    $("#txtServico").val(data).trigger('change');
                    setTaxaAdesao = true;
                }
            }
        });
    }
}

function formatMoney(number, decPlaces, decSep, thouSep) {
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSep = typeof decSep === "undefined" ? "." : decSep;
    thouSep = typeof thouSep === "undefined" ? "," : thouSep;
    var sign = number < 0 ? "-" : "";
    var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
    var j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSep : "") +
    i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
    (decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
}

function aplicarDesconto(id, tp, idprod) {
    if (tp === 1) {
        AbrirBox(17, id, idprod);
    } else if (tp === 2) {
        AbrirBox(18, id, idprod);
    } else {
        if ($("#txtDesconto_" + id).val() === "") {
            $("#txtDesconto_" + id).val("0");
        }
        var valorat = textToNumber($("#txtValor_" + id).val());
        var valormt = textToNumber($("#txtMulta_" + id).val());
        var valdesc = textToNumber($("#txtDesconto_" + id).val());
        valorat = valorat + valormt;
        if ($("#txtSinal_" + id).val() === "P") {
            valdesc = valorat * (valdesc / 100);
        }
        if (valorat - valdesc > 0) {
            $("#txtValFinal_" + id).val(formatMoney((valorat - valdesc), $("#txtMnDesconDecimal").val(), ",", "."));
        } else {
            $("#txtValFinal_" + id).val(formatMoney(0, $("#txtMnDesconDecimal").val(), ",", "."));
        }
        atualizaTotal();
    }
}

function atualizaTotal() {
    var valSemDesc = 0;
    var valDesc = 0;
    var valAcre = 0;
    var valTotal = 0;
    $('#tabela_Itens > tr').each(function () {
        var item = $(this).attr('data-id');
        valSemDesc = valSemDesc + textToNumber($('#txtValor_' + item, $(this)).val());
        valAcre = valAcre + textToNumber($('#txtMulta_' + item, $(this)).val());
        valTotal = valTotal + textToNumber($('>td', $(this)).find("#txtValFinal_" + item).val());
    });
    valDesc = (valTotal - (valAcre + valSemDesc));
    $("#lblTotalSemDesconto").text(numberFormat(valSemDesc, 1));
    $("#lblTotalDesconto").text(numberFormat(valDesc, 1));
    $("#lblTotalAcrescimo").text(numberFormat(valAcre, 1));
    $("#lblTotal").text(numberFormat(valTotal, 1));
    $("#btnEfetuarPag").attr("disabled", true);
    if (valTotal > 0 && ($("#ckbInativo").val() > 2 || $("#ckbInativo").val() === "0")) {
        $("#btnEfetuarPag").attr("disabled", false);
    }
}

function AbonarMens(idParc) {   
    let bb = bootbox.prompt({title: "Confirma o abono desta mensalidade? <br>Preencha o motivo abaixo:", inputType: 'textarea', callback: function (result) {
        if (result === null) {
            return;            
        } else if (result.length > 0) {
            $.post("./../Academia/form/ClientesPlanosServer.php", "abonoMensalidade=" + idParc + "&motivo=" + result).done(function (data) {
                if (data.trim() === "YES") {
                    AtualizaPagPlanosClientes($("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id'));
                    AtualizaListPlanosClientes();
                    atualizaStatusCli();
                } else {
                    bootbox.alert("Erro ao abonar pagamento!" + data);
                }
            });
        } else {            
            bb.find('.bootbox-input-textarea').css("border-color", "red");
            return false;
        }
    }});        
}

function AbonarProd(idParc) {   
    let bb = bootbox.prompt({title: "Confirma o abono deste item? <br>Preencha o motivo abaixo:", inputType: 'textarea', callback: function (result) {
        if (result === null) {
            return;            
        } else if (result.length > 0) {
            $.post("./../Academia/form/ClientesPlanosServer.php", "abonoProduto=" + idParc + "&idCliente=" + $("#txtId").val() + "&motivo=" + result).done(function (data) {
                if (data.trim() === "YES") {
                    AtualizaServicoFavorito();
                    AtualizaListHistoricoServicos();
                    bootbox.alert("Abono realizado com sucesso!");
                } else {
                    bootbox.alert("Erro ao abonar pagamento!" + data);
                }
            });
        } else {            
            bb.find('.bootbox-input-textarea').css("border-color", "red");
            return false;
        }
    }});        
}

function GerarBoleto(idParc) {
    bootbox.confirm('Confirma a geração do boleto desta mensalidade?', function (result) {
        if (result === true) {
            $("#loader").show();
            $.ajax({
                type: "GET", data: {onLineBoletoDCC: 'S', IdMens: idParc}, url: "./../Sivis/ws_atualizabanco.php"
            }).done(function (data) {
                $("#loader").hide();
                AtualizaPagPlanosClientes($("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id'));
                AtualizaListPlanosClientes();
            });
        } else {
            return;
        }
    });
}

function GerarPIX(idParc) {
    bootbox.confirm('Confirma a geração de cobrança desta mensalidade?', function (result) {
        if (result === true) {
            $("#loader").show();
            $.ajax({
                type: "GET", data: {onLinePIX: 'S', IdMens: idParc}, url: "./../Sivis/ws_atualizabanco.php"
            }).done(function (data) {
                $("#loader").hide();
                AtualizaPagPlanosClientes($("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id'));
                AtualizaListPlanosClientes();
            });
        } else {
            return;
        }
    });
}

function EnviarBoleto(idParc, tipo) {
    $.ajax({
        type: "GET", url: "./../Academia/form/ClientesPlanosServer.php",
        data: {getEmailBoleto: 'S'}, dataType: "json",
        success: function (data) {
            bootbox.prompt({
                title: "Selecione o modelo de E-mail",
                inputType: 'select',
                inputOptions: data,
                callback: function (result) {
                    if (result) {
                        let email = $("#" + $(Array.from($('[id*="txtTpContato_"]')).filter(function (item) {
                            return item.value === "2";
                        })[0]).attr("id").replace("txtTpContato_", "txtTxContato_")).val();
                        let send = [["", "", "", "", "", "", "", "", "", email, $("#txtId").val(), idParc, tipo]];
                        $("#source").dialog({modal: true});
                        $("#progressbar").progressbar({value: 0});
                        $("#pgIni").html("0");
                        $("#pgFim").html("1");
                        $("#txtTotalSucesso, #txtTotalErro").val(0);
                        sendEmailrow(result, send);
                    }
                }
            });
        },
        error: function (error) {
            bootbox.alert(error.responseText + "!");
        }
    });
}

function refreshTableBol() {
    //atualização após finalizar
}

function CancelarBoleto(idParc, isDcc) {
    bootbox.confirm('Confirma o cancelamento?', function (result) {
        if (result === true) {
            $("#loader").show();
            $.post("./../Academia/form/ClientesPlanosServer.php", "cancelarBoleto=" + idParc).done(function (data) {
                if (data.trim() === "YES") {
                    AtualizaPagPlanosClientes($("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id'));
                    if (isDcc === 'S') {
                        $("#loader").show();
                        $.post("../../../util/dcc/cancelDCC.php", "btnSalvar=S&txtPlanoItem=" + idParc).done(function (data) {
                            if (data === '{"type":true}') {
                                bootbox.alert("Sucesso ao cancelar!");
                            } else {
                                bootbox.alert("Erro ao cancelar!");
                                console.log(data);                                                        
                            }
                            $("#loader").hide();
                        });
                    }                    
                } else {
                    console.log(data);            
                    bootbox.alert("Erro ao cancelamento!");
                }
                $("#loader").hide();
            });                        
        } else {
            return;
        }
    });
}   

function ExcluirItemPag(idParc) {
    bootbox.confirm('Confirma a exclusão?', function (result) {
        if (result === true) {
            $("#tblItensPag tbody > tr").remove();
            $.post("./../Academia/form/ClientesPlanosServer.php", "excluirMensalidade=" + idParc).done(function (data) {
                if (data.trim() === 'PAGAMENTO') {
                    bootbox.alert("Não é possível excluir pois existe um pagamento!");
                } else if (data.trim() === 'NAOULTIMO') {
                    bootbox.alert("Somente é possivel excluir a data mais proxima do vencimento do plano!");
                } else if (data.trim() === 'UNICO') {
                    bootbox.alert("Não é possivel excluir. Remova o plano!");
                } else if (data.trim() === "YES") {
                    AtualizaPagPlanosClientes($("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id'));
                    AtualizaListPlanosClientes();
                    atualizaTotal();
                } else {
                    bootbox.alert("Erro ao estornar pagamento!" + data);
                }
            });
        } else {
            return;
        }
    });
}

function estornarPagPlano(idParc, idPlano, isDcc, idVenda) {
    let bb = bootbox.prompt({title: "Todos os pagamento efetuados juntos serão estornados. <br>Confirma o estorno? Preencha o motivo abaixo:", inputType: 'textarea', callback: function (result) {
        if (result === null) {
            return;            
        } else if (result.length > 0) {
            $("#loader").show();
            $.get('./../Academia/form/ClientesPlanosServer.php', {validaEstorno: idPlano, idMens: idParc}, function (data) {
                if (data.trim() === "CREDITO") {
                    $("#loader").hide();
                    bootbox.alert('Não é possível estornar pois foi gerado um crédito para este plano!');
                } else if (data.trim() === "NAOULTIMO") {
                    $("#loader").hide();
                    bootbox.alert("Somente é possivel estornar o último pagamento!");
                } else {
                    if (isDcc === 'S') {
                        $.post("../../../util/dcc/refundDCC.php", "btnSalvar=S&txtVendaItem=" + idVenda).done(function (data) {
                            if (!isNaN(data.trim())) {
                                geraEstorno(idParc, isDcc, idVenda, result);
                            } else {
                                $("#loader").hide();
                                bootbox.alert("Erro ao estornar pagamento!" + data);
                            }
                        });
                    } else {
                        geraEstorno(idParc, isDcc, idVenda, result);
                    }
                }
                atualizaStatusCli();
            });
        } else {
            bb.find('.bootbox-input-textarea').css("border-color", "red");
            return false;
        }
    }});
}

function estornarServico(isDcc, idVenda) {
    let bb = bootbox.prompt({title: "Todos os pagamento efetuados juntos serão estornados. <br>Confirma o estorno? Preencha o motivo abaixo:", inputType: 'textarea', callback: function (result) {
        if (result === null) {
            return;
        } else if (result.length > 0) {
            $("#loader").show();
            if (isDcc === 'S') {
                $.post("../../../util/dcc/refundDCC.php", "btnSalvar=S&txtVendaItem=" + idVenda).done(function (data) {
                    if (!isNaN(data.trim())) {
                        geraEstornoServico(isDcc, idVenda, result);
                        AtualizaServicoFavorito();
                    } else {
                        $("#loader").hide();
                        bootbox.alert("Erro ao estornar pagamento!" + data);
                    }
                });
            } else {
                geraEstornoServico(isDcc, idVenda, result);
                AtualizaServicoFavorito();                
            }                                    
        } else {            
            bb.find('.bootbox-input-textarea').css("border-color", "red");
            return false;
        }
    }});
}

function geraEstornoServico(isDcc, idVenda, result) {
    $.post("./../Academia/form/ClientesPlanosServer.php", "estornarServico=" + idVenda + "&motivo=" + result).done(function (data) {
        if (data.trim() === "YES") {
            if (isDcc === 'S') {
                bootbox.confirm('Estorno efetuado com sucesso! Imprimir Recibo?', function (result) {
                    if (result === true) {
                        window.open("../../util/ImpressaoPdf.php?pAlt=90&pLar=140&id=" + idVenda + "&tpImp=I&NomeArq=CancelamentoDCC&PathArq=../Modulos/Academia/modelos/Recibo_Estorno_Dcc.php", '_blank');
                    }
                    atualizaTela();
                });
            } else {
                atualizaTela();
            }
        } else {
            bootbox.alert("Erro ao estornar pagamento!");
        }
        $("#loader").hide();
    });    
}

function geraEstorno(idParc, isDcc, idVenda, result) {
    $.post("./../Academia/form/ClientesPlanosServer.php", "estornarPagPlano=" + idParc + "&isDcc=" + isDcc + "&motivo=" + result).done(function (data) {
        if (data.trim() === "YES") {
            atualizaStatusCli();
            if (isDcc === 'S') {
                bootbox.confirm('Estorno efetuado com sucesso! Imprimir Recibo?', function (result) {
                    if (result === true) {
                        window.open("../../util/ImpressaoPdf.php?pAlt=90&pLar=140&id=" + idVenda + "&tpImp=I&NomeArq=CancelamentoDCC&PathArq=../Modulos/Academia/modelos/Recibo_Estorno_Dcc.php", '_blank');
                    }
                    atualizaTela();
                });
            } else {
                atualizaTela();
            }
        } else {
            bootbox.alert("Erro ao estornar pagamento!");
        }
        $("#loader").hide();
    });
}

function atualizaTela() {
    AtualizaListPlanosClientes();
    AtualizaListHistoricoServicos();
    AtualizaPagPlanosClientes($("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id'));
    AtualizaPagPlanosDependentes();
    atualizaTotCredito();
    atualizatbListaDCC();
}

function validaItemPag(tipo, idPlano, idProd) {
    var result = true;
    if ($("#txtAcaOneItem").val() === "1" && $('#tabela_Itens  > tr').length > 0) {
        result = false;
        bootbox.alert("Itens excedido, o pagamento está limitado a apenas 1 item!");            
    } else if (tipo === "MENSALIDADE") {
        $('#tabela_Itens  > tr').each(function () {
            var dtitem = $(this).find("input[name*='txtIdPlano_']").val();
            var dttipo = $(this).attr('data-tp');
            if (eval(dtitem) === eval(idPlano) && dttipo === tipo) {
                result = false;
                bootbox.alert("Registro já incluido para pagamento!");
            }
        });
    } else if (tipo === "SERVIÇO" && idProd === "") {
        if ($("#txtServico").val() === "null") {
            result = false;
            bootbox.alert("Escolha um serviço!");
        }
    } else if (tipo === "PRODUTO" && idProd === "") {
        if ($("#txtProduto").val() === "null") {
            result = false;
            bootbox.alert("Escolha um Produto!");
        }
    }
    if (tipo !== "CREDITO") {
        $('#tabela_Itens  > tr').each(function () {
            var dttipo = $(this).attr('data-tp');
            if (dttipo === 'CREDITO') {
                result = false;
                bootbox.alert("Não é possivel incluir outro pagamento com um pagamento de crédito!");
            }
        });
    }
    return result;
}

function validaCredito() {
    if (textToNumber($("#txtValCredito").val()) > 0) {
        if ($("#tblItensPag tbody > tr").length > 0) {
            bootbox.confirm('Ao adicionar um item de crédito todos os itens na lista serão retirados, Confirma?', function (value) {
                if (value === true) {
                    $("#tblItensPag tbody > tr").remove();
                    incluirItemPag('CREDITO', '466', '', '', '');
                } else {
                    return;
                }
            });
        } else {
            incluirItemPag('CREDITO', '466', '', '', '');
        }
    } else {
        bootbox.alert("Preencha um valor válido para crédito!");
    }
}

function AbonarCredito() {
    if (textToNumber($("#txtValCredito").val()) > 0) {
        bootbox.confirm('Confirma o abono deste crédito?', function (result) {
            if (result === true) {
                $.post("./../Academia/form/ClientesPlanosServer.php",
                        "abonoCredito=" + $("#txtId").val() + "&valCred=" + $("#txtValCredito").val()).done(function (data) {
                    if (data.trim() === "YES") {
                        bootbox.alert("Lançamento efetuado com sucesso!");
                        $("#txtValCredito").val("0,00");
                        atualizaTotCredito();
                        listExtratoCredito();
                    } else {
                        bootbox.alert("Erro ao abonar crédito!" + data);
                    }
                });
            } else {
                return;
            }
        });
    } else {
        bootbox.alert("Preencha um valor válido para crédito!");
    }
}

function AbonarCarteira(id) {
    bootbox.confirm('Confirma o abono desta Carteira?', function (result) {
        if (result === true) {
            $.post("./../Academia/form/ClientesPlanosServer.php",
                    "abonoCarteira=" + id).done(function (data) {
                if (data.trim() === "YES") {
                    bootbox.alert("Lançamento efetuado com sucesso!");
                    atualizaTotCredito();
                    listExtratoCredito();
                } else {
                    bootbox.alert("Erro ao abonar Carteira!" + data);
                }
            });
        } else {
            return;
        }
    });
}

function verificaConvenio(valor, idProd, idPlano, idConvProd) {
    var toReturn;
    $.ajax({
        type: "GET", data: {isAjax: 'S', verificaConvenio: $("#txtId").val(), valorProduto: valor, 
        idProd: idProd, idPlano: idPlano, idConvProd: idConvProd},
        url: "./../Academia/form/ClientesFormServer.php", async: false
    }).done(function (data) {
        if (data !== null) {
            toReturn = data;
        }
    });
    return toReturn;
}

function verificaMulta(valor, dataVencimento) {
    var toReturn = numberFormat(0);
    $.ajax({
        type: "GET", data: {isAjax: 'S', verificaMulta: $("#txtId").val(), valMens: valor, dtMens: dataVencimento},
        url: "./../Academia/form/ClientesFormServer.php", async: false
    }).done(function (data) {
        if (data !== null) {
            toReturn = data;
        }
    });
    return toReturn;
}

function FecharBoxCliente(opc) {
    parent.descontos = [];
    parent.cancelamentos = [];
    $("#newbox").remove();
    if ($("#tab2").hasClass("active")) {
        var tblCRM = $('#tblCRM').dataTable();
        tblCRM.fnReloadAjax("./../CRM/Crm_Atendimentos_server_processing.php?Al=1&Cli=" + $("#txtId").val());
    }
    if (opc === 'PAG') {
        limpaItensPag();
    }
}

function listPagamentosDependentes() {
    $('#tblPagamentosDependentes').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "./../Academia/form/ClientesPlanosServer.php?listPagamentosDependentes=" + $("#txtId").val(),
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });
}

function imprimirContrato(idMens) {
    window.open("../../util/ImpressaoPdf.php?id=" + idMens + "&tpImp=I&NomeArq=Contrato&PathArq=ContratoAcademia", '_blank');
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Servicos">
$('#txtServico').change(function () {
    $('#txtValServ').attr('readonly', ($('option:selected', $(this)).attr('avl') === "0"));
    $('#txtQtdServ').attr("disabled", false);
    if ($(this).val()) {
        var idServico = $(this).val();
        $.getJSON('../../Modulos/Estoque/conta_a.ajax.php', {txtContaS: $(this).val(), ajax: 'true'}, function (j) {
            var preco = numberFormat(0);
            var qtdcom = '1';
            for (var i = 0; i < j.length; i++) {
                if (j[i].preco_venda !== '') {
                    preco = j[i].preco_venda;
                }
                if (j[i].quantidade_comercial !== '') {
                    if (parseFloat(j[i].quantidade_comercial) > 0) {
                        qtdcom = parseInt(j[i].quantidade_comercial);
                    } else {
                        qtdcom = 1;
                    }
                }
            }
            $('#txtQtdServ').val(qtdcom).show();
            $('#txtValServ').val(numberFormat(preco)).show();
            $('#txtValServFinal').val(numberFormat(preco)).show();
            if (idServico === "466") {
                $('#txtQtdServ').attr("disabled", true);
            }
            if (setTaxaAdesao) {
                setTaxaAdesao = false;
                incluirItemPag('SERVIÇO', '', '');
            }
        });
    } else {
        $('#txtQtdServ').val('1');
        $('#txtValServ').val(numberFormat(0));
        $('#txtValServFinal').val(numberFormat(0));
    }
});

$('#txtProduto').change(function () {
    $('#txtValProd').attr('readonly', ($('option:selected', $(this)).attr('avl') === "0"));
    $('#txtQtdProd').attr("disabled", false);
    $('#btnPagarProduto').attr("disabled", ($('option:selected', $(this)).attr('blk') === "1"));
    if ($(this).val()) {
        var idServico = $(this).val();
        $.getJSON('../../Modulos/Estoque/conta_a.ajax.php', {txtContaS: $(this).val(), ajax: 'true'}, function (j) {
            var preco = numberFormat(0);
            var qtdcom = '1';
            for (var i = 0; i < j.length; i++) {
                if (j[i].preco_venda !== '') {
                    preco = j[i].preco_venda;
                }
                if (j[i].quantidade_comercial !== '') {
                    if (parseFloat(j[i].quantidade_comercial) > 0) {
                        qtdcom = parseInt(j[i].quantidade_comercial);
                    } else {
                        qtdcom = 1;
                    }
                }
            }
            $('#txtQtdProd').val(qtdcom).show();
            $('#txtValProd').val(numberFormat(preco)).show();
            $('#txtValProdFinal').val(numberFormat(preco)).show();
        });
    } else {
        $('#txtQtdProd').val('1');
        $('#txtValProd').val(numberFormat(0));
        $('#txtValProdFinal').val(numberFormat(0));
    }
});

function atValServ() {
    var qtd = textToNumber($("#txtQtdServ").val());
    var val = textToNumber($("#txtValServ").val());
    $('#txtValServFinal').val(numberFormat(val * qtd));
}

function atValProd() {
    var qtd = textToNumber($("#txtQtdProd").val());
    var val = textToNumber($("#txtValProd").val());
    $('#txtValProdFinal').val(numberFormat(val * qtd));
}
// </editor-fold>

$("#btnPag").click(function (event) {
    trataDivPagamento("P");
});

function listExtratoCredito() {
    $("#tblExtratoCreditos").dataTable().fnDestroy();
    $('#tblExtratoCreditos').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "./../Academia/form/ClientesPlanosServer.php?listExtratoCredito=" + $("#txtId").val(),
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });
}

var tblValores = $('#tblValores').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    bPaginate: false, bFilter: false, bInfo: false, bSort: false,
    "sAjaxSource": "./../Academia/form/ClientesPlanosServer.php?listValores=0",
    "scrollY": "120px",
    "scrollCollapse": true,
    'oLanguage': {
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sZeroRecords': "Não foi encontrado nenhum resultado"
    }
});

function AtualizaPagPlanosDependentes() {
    var tblPagamentosDependentes = $('#tblPagamentosDependentes').dataTable();
    tblPagamentosDependentes.fnReloadAjax("./../Academia/form/ClientesPlanosServer.php?listPagamentosDependentes=" + $("#txtId").val());
}

function removerLinhaPag(idLinha) {
    rmDec(idLinha);
    rmCan(idLinha);
    $("#linhaItem_" + idLinha).remove();
    atualizaTotal();
}

function cancelarMultaPag(nItens, idprod) {
    if ($("#txtCancelMulta").val() === "1") {
        rmCan(nItens);
        cancelamentos.push({idVariavel: nItens, usuario: $("#txtUserLog").val(), valor: $("#txtMulta_" + nItens).val(), idprod: idprod});
        $("#txtMulta_" + nItens).val(numberFormat(0));
        aplicarDesconto(nItens, 0, idprod);
    } else {
        aplicarDesconto(nItens, 2, idprod);
    }
}

function rmCan(idLinha) {
    if (cancelamentos.length > 0) {
        for (var x = 0; x < cancelamentos.length; x++) {
            if (cancelamentos[x].idVariavel === idLinha) {
                cancelamentos.splice(x, 1);
            }
        }
    }
}

function rmDec(idLinha) {
    if (descontos.length > 0) {
        for (var x = 0; x < descontos.length; x++) {
            if (descontos[x].idVariavel === idLinha) {
                descontos.splice(x, 1);
            }
        }
    }
}

function listServicoFavorito() {
    $('#tblServicoFavorito').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "./../Academia/form/ClientesPlanosServer.php?tp=S&listFavorito=" + $("#txtId").val(),
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });

    $('#tblProdutoFavorito').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "./../Academia/form/ClientesPlanosServer.php?tp=P&listFavorito=" + $("#txtId").val(),
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });
}

function AtualizaServicoFavorito() {
    var tblServicoFavorito = $('#tblServicoFavorito').dataTable();
    tblServicoFavorito.fnReloadAjax("./../Academia/form/ClientesPlanosServer.php?tp=S&listFavorito=" + $("#txtId").val());
    
    var tblProdutoFavorito = $('#tblProdutoFavorito').dataTable();
    tblProdutoFavorito.fnReloadAjax("./../Academia/form/ClientesPlanosServer.php?tp=P&listFavorito=" + $("#txtId").val());
}

var nEditing = null;
var idEditing = "";
var regEdit = "";
var regData = "";
var btnMens = "";

$("body").on("click", "#tblLinkHistorico #btnAltLink", function () {
    var nRow = $(this).parents('tr')[0];
    if (nEditing !== null) {
        restoreRowLink(nEditing, idEditing);
    } else {
        var id = $(this).attr("data-id");
        nEditing = nRow;
        idEditing = id;
        var jqTds = $('>td', nRow);
        regEdit = (jqTds[2].innerHTML.indexOf("span") > 0 ? $(jqTds[2].innerHTML).text() : jqTds[2].innerHTML);
        btnMens = jqTds[4].innerHTML;
        jqTds[2].innerHTML = '<input id=\"txValorMens_' + id + '\" type="text" maxlength="2" class="form-control input" value="' + regEdit + '">';
        jqTds[4].innerHTML = '<center><a href="javascript:;" data-id="' + id + '" class="btn green-haze saveLink" title="Salvar" style=\"padding: 0px 4px;background:#5bb75b;\"><span class="ico-checkmark"></span></a> \n\
        <a href="javascript:;" data-id="' + id + '" class="btn red-haze cancelLink" title="Cancelar" style=\"padding: 0px 4px;background:red;\"><span class="ico-reply"></span></a> <center>';
    }
});

$("body").on('click', '.cancelLink', function (e) {
    nEditing = null;
    AtualizaListHistoricoLink();
});

$("body").on('click', '.saveLink', function (e) {
    var id = $(this).attr("data-id");
    if ($("#txValorMens_" + id).val() === "") {
        bootbox.alert("Valor Inválido!");
        return;
    }    
    bootbox.confirm('Confirma a alteração do número máximo de parcelas?', function (result) {
        if (result === true) {
            $.post("./../Academia/form/ClientesPlanosServer.php", "atualizaMaxLink=" + id + 
            "&txtValorMens=" + $("#txValorMens_" + id).val()).done(function (data) {
                if (data.trim() === "YES") {
                    nEditing = null;
                    AtualizaListHistoricoLink();
                }
            });
        }
    });
});

function restoreRowLink(nRow, id) {
    var jqTds = $('>td', nRow);
    jqTds[2].innerHTML = regEdit;
    jqTds[4].innerHTML = btnMens;
    nEditing = null;
    regEdit = "";
}

$("body").on("click", "#tblPagamentosPlano #btnAlt", function () {
    var nRow = $(this).parents('tr')[0];
    if (nEditing !== null) {
        restoreRow(nEditing, idEditing);
    } else {
        var id = $(this).attr("data-id");
        nEditing = nRow;
        idEditing = id;
        var jqTds = $('>td', nRow);
        regData = jqTds[0].innerHTML;
        regEdit = (jqTds[1].innerHTML.indexOf("span") > 0 ? $(jqTds[1].innerHTML).text() : jqTds[1].innerHTML);
        btnMens = jqTds[6].innerHTML;
        jqTds[1].innerHTML = '<input id=\"txValorMens_' + id + '\" type="text" class="form-control input" value="' + regEdit + '">';
        jqTds[6].innerHTML = '<center><a href="javascript:;" data-id="' + id + '" class="btn green-haze save" title="Salvar" style=\"padding: 0px 4px;background:#5bb75b;\"><span class="ico-checkmark"></span></a> \n\
        <a href="javascript:;" data-id="' + id + '" class="btn red-haze cancel" title="Cancelar" style=\"padding: 0px 4px;background:red;\"><span class="ico-reply"></span></a> <center>';
        if ($("#tblPlanosClientes").find('tr:has(td.selected) >td:nth-child(2)').text() === "1") {
            jqTds[0].innerHTML = regData.substring(0, 12) + " " + regData.substring(13, 23) + '<input id=\"txDataMens_' + id + '\" type="hidden" data-mask="00/00/0000" data-mask-reverse="true" class="form-control input" value="' + regData.substring(13, 23) + '" style=\"width:78px;\">';
        }
    }
});

$("body").on("click", "#tblPagamentosPlano #btnProRata", function () {
    var nRow = $(this).parents('tr')[0];
    if (nEditing !== null) {
        restoreRow(nEditing, idEditing);
    } else if ($("#tblPlanosClientes").find('tr:has(td.selected)>td:nth-child(2)').text() === "1" ||
            $("#tblPlanosClientes").find('tr:has(td.selected)>td:nth-child(2)').text() === "DCC" ||
            $("#tblPlanosClientes").find('tr:has(td.selected)>td:nth-child(2)').text() === "BOL" ||
            $("#tblPlanosClientes").find('tr:has(td.selected)>td:nth-child(2)').text() === "PIX") {
        var id = $(this).attr("data-id");
        nEditing = nRow;
        idEditing = id;
        var jqTds = $('>td', nRow);
        regData = jqTds[0].innerHTML;
        regEdit = jqTds[1].innerHTML;
        btnMens = jqTds[6].innerHTML;
        jqTds[1].innerHTML = regEdit + '<input id=\"txValorMens_' + id + '\" type="hidden" class="form-control input" value="' + (regEdit.indexOf("span") > 0 ? $(regEdit).text() : regEdit) + '">';
        jqTds[6].innerHTML = '<center><a href="javascript:;" data-id="' + id + '" class="btn green-haze save" title="Salvar" style=\"padding: 0px 4px;background:#5bb75b;\"><span class="ico-checkmark"></span></a> \n\
        <a href="javascript:;" data-id="' + id + '" class="btn red-haze cancel" title="Cancelar" style=\"padding: 0px 4px;background:red;\"><span class="ico-reply"></span></a> <center>';
        jqTds[0].innerHTML = regData.substring(0, 12) + '<input id=\"txDataMens_' + id + '\" type="text" data-mask="00/00/0000" data-mask-reverse="true" class="form-control input" value="' + regData.substring(13, 23) + '" style=\"width:78px;\">';
    } else {
        bootbox.alert("Esta operação é apenas para plano mensal!");
        return;
    }
});

$("body").on('click', '.cancel', function (e) {
    nEditing = null;
    AtualizaPagPlanosClientes($("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id'));
});

$("body").on('click', '.save', function (e) {
    var id = $(this).attr("data-id");
    if ($("#txValorMens_" + id).val() === "") {
        bootbox.alert("Valor Inválido!");
        return;
    }
    if ($("#txDataMens_" + id).length > 0 && $("#tblPlanosClientes").find('tr:has(td.selected) >td:nth-child(2)').text() === "1") {
        if ($("#txDataMens_" + id).val() === "") {
            bootbox.alert("Data Inválida!");
            return;
        }
    }
    bootbox.confirm('Confirma o alteração da mensalidade?', function (result) {
        if (result === true) {
            $("#txtIdSel").val($("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id'));
            var dataMens = "";
            if ($("#txDataMens_" + id).length > 0) {
                dataMens = "&txtDataMens=" + $("#txDataMens_" + id).val().replace(/\//g, "_");
            }
            $.post("./../Academia/form/ClientesPlanosServer.php", "atualizaMensalidade=" + id + "&txtValorMens=" + $("#txValorMens_" + id).val() + dataMens).done(function (data) {
                if (data.trim() === "YES") {
                    nEditing = null;
                    AtualizaPagPlanosClientes($("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id'));
                    AtualizaListPlanosClientes();
                }
            });
        }
    });
});

function restoreRow(nRow, id) {
    var jqTds = $('>td', nRow);
    jqTds[0].innerHTML = regData;
    jqTds[1].innerHTML = regEdit;
    jqTds[6].innerHTML = btnMens;
    nEditing = null;
    regEdit = "";
}

$("#btnCredito").click(function (event) {
    trataDivPagamento("C");
});

$(document).on('click', '.cancSerasa', function () {
    AbrirBox(16);
});

function RenovarMens() {
    var idPlano = $("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id');
    if (idPlano !== undefined) {
        $.post("./../Academia/form/ClientesPlanosServer.php", "renovarContrato=" + idPlano).done(function (data) {
            if ($.isNumeric(data.trim())) {
                AtualizaPagPlanosClientes($("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id'));
            } else {
                bootbox.alert(data.trim() + "!");
            }
        });
    } else {
        bootbox.alert("Selecione um Contrato para esta operação!");
    }
}

function RenovarMensAnt() {
    var idPlano = $("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id');
    if (idPlano !== undefined) {
        $.post("./../Academia/form/ClientesPlanosServer.php", "renovarContratoAnt=" + idPlano).done(function (data) {
            if ($.isNumeric(data.trim())) {
                AtualizaPagPlanosClientes($("#tblPlanosClientes").find('tr:has(td.selected)').find('.detalhe').attr('data-id'));
            } else {
                bootbox.alert(data.trim() + "!");
            }
        });
    } else {
        bootbox.alert("Selecione um Contrato para esta operação!");
    }
}

function getYears() {
    let array = [];
    $("#tblPagamentosPlano tbody tr").each(function (l, x) {
        let item = $(x).find("td:eq(3)").html() ? $(x).find("td:eq(3)").html().substring(6, 10) : "";
        if (array.indexOf(item) === -1 && item.length > 0)
            array.push(item);
    });
    if (array.indexOf(moment().year() + "") === -1)
        array.push(moment().year() + "");
    return array.reverse();
}

function filterYears(year) {    
    $("#txtAnoSel").val(year);
    $(".filterMens").each(function (l, x) {
        $(x).removeClass("dblue").removeClass("green").addClass($(x).find("span").html() === year ? "green" : "dblue");
    });
    $("#tblPagamentosPlano tbody tr").each(function (l, x) {
        if ($(x).find("td:eq(3)").html() && year === $(x).find("td:eq(3)").html().substring(6, 10)) {
            $(x).show();
        } else {
            $(x).hide();
        }
    });
}

function makeFilters() {
    $(".filterMens").remove();
    $.each(getYears(), function (index, value) {
        $("#tblPagamentosPlano").before(`<button type="button" class="btn dblue filterMens" style="line-height:10px;margin: 0px 5px 5px 0px;padding: 5px 5px 4px 5px;" onclick="filterYears('${value}');"><span>${value}</span></button>`);
    });
    filterYears(($("#txtAnoSel").val() === "" ? moment().year() + "" : $("#txtAnoSel").val()));
}