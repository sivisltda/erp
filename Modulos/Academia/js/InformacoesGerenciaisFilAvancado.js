var clickTabAvancados = 0;

$(document).ready(function () {
    $('#txtCidade').change(function () {
        if ($(this).val()) {
            $('#txtBairro').hide();
            $.getJSON('./ajax/locais.ajax.php', {cidade: $(this).val(), ajax: 'true'}, function (j) {
                var options = '<option value="null">[ TODOS ]</option>';
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].bairro + '">' + j[i].bairro + '</option>';
                }
                $('#txtBairro').html(options).show();
            });
        } else {
            $('#txtBairro').html('<option value="null">[ TODOS ]</option>');
        }
    });
});

function carregaTabAvancado() {
    if (clickTabAvancados === 1) {
        return;
    }
    clickTabAvancados = 1;
    $("#loader").show();

    $('#tbFiltroAv').DataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "ordering": false,
        "bProcessing": true,
        "bServerSide": true,
        "bFilter": false,
        "sAjaxSource": final_url_FilAvancado(0),
        "bLengthChange": false,
        "bPaginate": false,
        "bInfo": false,
        'oLanguage': {
            'sEmptyTable': "Não foi encontrado nenhum resultado",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "fnInitComplete": function (oSettings, json) {
            $("#loader").hide();
        },
        "footerCallback": function (row, data, start, end, display) {
            var api = this.api(), data;
            $(api.column(7).footer()).html(end);
        }
    });
}

function final_url_FilAvancado(id) {
    return "ajax/InformacoesGerenciaisFilAvancado_server_processing.php?id=" + id +
            "&loj=" + $('#txtLojaSel').val() +
            "&sta=" + $("#mscStat").val() +
            "&con=" + $("#mscConv").val() +
            "&pla=" + $("#mscPlano").val() +
            "&dti=" + $("#txtAniversarioDe").val() + "&dtf=" + $("#txtAniversarioAte").val() +
            "&dsa=" + $("#txtDiasSemAcessoDe").val() +
            "&dsf=" + $("#txtDiasSemAcessoAte").val() +
            "&pef=" + $("#txtPlanoEmpFam").val() +
            "&pro=" + $("#txtProcedencia").val() +
            "&adi=" + $("#txtFxSemAtendDe").val() + "&adf=" + $("#txtFxSemAtendAte").val() +
            "&idi=" + $("#txtFxEtariaDe").val() + "&idf=" + $("#txtFxEtariaAte").val() +
            "&sex=" + $("#txtSexo").val() +
            "&prf=" + $("#txtProfissao").val() +
            "&eci=" + $("#txtEstCivil").val() +
            "&par=" + $("#txtParentesco").val() +
            "&crd=" + $("#txtCredenciais").val() +
            "&cid=" + $("#txtCidade").val() +
            "&bai=" + $("#txtBairro").val() +
            "&grp=" + $("#txtGrupo").val() +
            "&tit=" + $("#txtTitularFiltro").val() +            
            "&com=" + $("#txtComissoes").val() +            
            "&dep=" + $("#txtRelacionamento").val();
}

function BuscarFilAvancado() {
    $("#loader").show();
    var tblrefresh = $("#tbFiltroAv").dataTable();
    tblrefresh.fnReloadAjax(final_url_FilAvancado(1), function () {
        $("#loader").hide();
    }, null);
}

function abrirTelaSMS(sms) {
    abrirTelaBox("../CRM/SMSEnvioForm.php", 400, 400);
    $("#txtSendSms").val(sms);
}

function abrirTelaWha(sms) {
    abrirTelaBox("../CRM/SMSEnvioForm.php?zap=s", 400, 400);
    $("#txtSendSms").val(sms);
}

function abrirTelaEmail(email) {
    abrirTelaBox("../CRM/EmailsEnvioForm.php", 650, 720);
    $("#txtSendEmail").val(email);
}
function FecharBox() {
    $("#newbox").remove();
}

function printFilAvancado(tipo) {
    var pRel = "&NomeArq=" + "Filtro Avançado" +
            "&lbl=" + "Mat.|Nome|Dt.Nasc|Sexo|Status|Telefone|Celular|Email" + (tipo === "E" ? "|CPF/CNPJ|Convênios|Profissão|Estado Civil" : "") + 
            "&siz=" + "30|200|50|30|80|80|80|150" + (tipo === "E" ? "|100|150|100|100" : "") + 
            "&pdf=" + "20" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + "Alunos " + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + "../Modulos/Academia/" + final_url_FilAvancado(1).replace("?", "&"); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tipo + "&PathArq=GenericModelPDF.php", '_blank');
}
