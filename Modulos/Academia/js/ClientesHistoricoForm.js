function iniciaHistoricoForm() {

    $.getJSON("form/ClientesPlanosServer.php?listPagamentosTodos=" + $("#txtId", window.parent.document).val(), function (j) {
        if (j !== "") {
            $("#histPag").append(j[0]);
            $("#lblTotalPag").text(j[1]);
            $("#txtAlert").hide();
        } else {
            $("#txtAlert").show();
        }
    }).done(function (data) {
        $('#histPag').jstree();
    });

    $('#histPag').on('select_node.jstree', function (e, data) {
        var link = $('#' + data.selected).find('a');
        if (link.attr("href") !== "#" && link.attr("href") !== "javascript:;" && link.attr("href") !== "") {
            window.open(link.attr("href"), '_blank');
            return false;
        }
    });

    var tblCongHist = $('#tblHistPlanos').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        bPaginate: false, bFilter: false, bInfo: false, bSort: false,
        "sAjaxSource": "form/ClientesPlanosServer.php?listPlanosTodos=" + $("#txtId", window.parent.document).val(),
        'oLanguage': {
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sZeroRecords': "Não foi encontrado nenhum resultado"
        }
    });

    var tblHistTurmas = $('#tblHistTurmas').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        bPaginate: false, bFilter: false, bInfo: false, bSort: false,
        "sAjaxSource": "form/TurmasFormServer.php?isAjax=S&listHistTurma=" + $("#txtId", window.parent.document).val(),
        'oLanguage': {
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sZeroRecords': "Não foi encontrado nenhum resultado"
        }
    });

    var tblHistTransferencia = $('#tblHistTransferencia').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        bPaginate: false, bFilter: false, bInfo: false, bSort: false,
        "sAjaxSource": "form/ClientesPlanosServer.php?listTransferencia=" + $("#txtId", window.parent.document).val(),
        'oLanguage': {
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sZeroRecords': "Não foi encontrado nenhum resultado"
        }
    });

    var tblHistSerasa = $('#tblHistSerasa').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        bPaginate: false, bFilter: false, bInfo: false, bSort: false,
        "sAjaxSource": "form/TurmasFormServer.php?isAjax=S&listHistSerasa=" + $("#txtId", window.parent.document).val(),
        'oLanguage': {
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sZeroRecords': "Não foi encontrado nenhum resultado"
        }
    });
    
    var tblTrancamento = $('#tblTrancamento').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        bPaginate: false, bFilter: false, bInfo: false, bSort: false,
        "sAjaxSource": "form/TurmasFormServer.php?isAjax=S&listTrancamento=" + $("#txtId", window.parent.document).val(),
        'oLanguage': {
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sZeroRecords': "Não foi encontrado nenhum resultado"
        }
    });
}

function trataFoot(tab) {
    if (tab === "tab1") {
        $("#TotPag").show();
    } else {
        $("#TotPag").hide();
    }
}