
function validaForm() { 
    
    if($("#txtDescricao").val() === ""){
       bootbox.alert("Descrição é Obrigatória!");
       return false;
    }
    
    if($("#txtHorario").val() === "null"){
       bootbox.alert("Horário é Obrigatório!");
       return false;
    }
    
    return true;
}

