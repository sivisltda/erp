function iniciaAgendamentoDccForm() {
    $("#txtDtCancelDcc").mask(lang["dateMask"]);
    $("#txtIdPlano").val($("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected)').find('.detalhe').attr('data-id'));
    $("#txtPlano").val($("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected) >td:nth-child(1) ').text());
    $("#txtDtVencPlano").val($("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected) >td:nth-child(4) ').text());
    $.getJSON("form/ClientesPlanosServer.php?getAgendamentoCancelDcc=" + $("#txtIdPlano").val(), function (j) {
        $("#btnExcluirAgendDcc, #btnReciboDcc").hide();
        if (j !== null) {
            if (j.length > 0) {
                if (j[0].dt_cancel_agendamento !== "") {
                    $("#txtDtCancelDcc").val(j[0].dt_cancel_agendamento);
                    bootbox.alert('Existe um agendamento programado para este plano!');
                    $("#btnExcluirAgendDcc, #btnReciboDcc").show();
                }
            }
        }
    });

    $('#btnGravarAgendDcc').click(function (e) {
        var dtCancelDcc = moment($("#txtDtCancelDcc").val(), lang["dmy"]).format('YYYYMMDD');
        var dtAtual = moment().format('YYYYMMDD');

        if ($("#txtDtCancelDcc").val() === "") {
            bootbox.alert('Data é obrigatória!');
        } else if (dtCancelDcc < dtAtual) {
            bootbox.alert('Data inválida!');
        } else {
            $.post("form/ClientesPlanosServer.php", "addAgendamentoCancelDcc=" + $("#txtIdPlano").val() + "&txtDtCancelDcc=" + $("#txtDtCancelDcc").val().replace(/\//g, "_")).done(function (data) {
                if (data.trim() === "YES") {
                    bootbox.alert("Cancelamento Adicionado com Sucesso!", function () {
                        parent.AtualizaListPlanosClientes();
                        parent.FecharBox(0);
                    });
                } else {
                    bootbox.alert("Erro ao adicionar cancelamento!");
                }
            });
        }
    });

    $('#btnExcluirAgendDcc').click(function (e) {
        bootbox.confirm('Confirma a exclusão do agendamento?', function (result) {
            if (result === true) {
                $.post("form/ClientesPlanosServer.php", {excluirAgendamentoDcc: $("#txtIdPlano").val()}).done(function (data) {
                    if (data.trim() === "YES") {
                        bootbox.alert("Agendamento excluido com sucesso, caso nao tenha nenhuma parcela em aberto renove o Plano!", function () {
                            parent.AtualizaListPlanosClientes();
                            parent.FecharBox(0);
                        });
                    } else {
                        bootbox.alert("Erro ao atualizar registro!");
                    }
                });
            }
        });
    });

    $('#btnReciboDcc').click(function (e) {
        window.open("../../util/ImpressaoPdf.php?pAlt=90&pLar=140&id=" + $("#txtIdPlano").val() + "&tpImp=I&NomeArq=CancelamentoDCC&PathArq=../Modulos/Academia/modelos/Recibo_Canc_Dcc.php", '_blank');
    });

}