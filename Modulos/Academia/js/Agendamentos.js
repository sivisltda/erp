
function AbrirBoxAgenda(id) {
    abrirTelaBox("./../Comercial/FormAgenda.php?al=" + $("#txtId").val() + (id > 0 ? "&id=" + id : ""), 465, 500);
}

function finalFindAgenda(imp) {
    var retPrint = (imp === 1 ? '&imp=1' : '?imp=0');
    retPrint += "&al=S";
    retPrint += "&ps=" + $("#txtId").val();
    return "./../Comercial/Gerenciador-Agenda-Compromissos_server_processing.php" + retPrint.replace(/\//g, "_");
}

var tbLista = $('#tblAgenda').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bFilter": false,
    "aaSorting": [[0, 'desc']],
    "aoColumns": [
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true}],
    "sAjaxSource": finalFindAgenda(0),
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

function FecharBoxF() {
    $("#newbox").remove();
    tbLista.fnReloadAjax(finalFindAgenda(0));
}

function abrirContrato(id) {
    $.getJSON('./../CRM/contratos.ajax.php', {txtTipo: "2"}, function (itens) {
        let arrayOptions = [];
        arrayOptions.push({text: 'Selecione', value: 'null'});
        for (var i = 0; i < itens.length; i++) {
            arrayOptions.push({text: itens[i].descricao, value: itens[i].id});
        }
        bootbox.prompt({
            title: "Selecione um modelo e contrato:",
            inputType: 'select',
            inputOptions: arrayOptions,
            callback: function (result) {
                if (result !== 'null') {
                    window.open("../../util/ImpressaoPdf.php?id_plan=" + id + "&idContrato=" + result + "&model=A&tpImp=I&NomeArq=Contrato&PathArq=../Modulos/Comercial/modelos/ModeloContrato.php&emp=1", '_blank');
                } else {
                    bootbox.alert("É necessário selecionar um contrato válido!");
                }
            }
        });
    });
}