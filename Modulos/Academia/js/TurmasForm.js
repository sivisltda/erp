$(document).ready(function () {
    $("#loader").show();
    totalFimCarregamento = 0;    
    $("#txtDtInicio, #txtDtCriacao").mask(lang["dateMask"]);
    $.getJSON("../Academia/form/HorariosFormServer.php", {isAjax:"S", listHorarios: "S"}, function (j) {
        var options = "<option value=\"\">Selecione</option>";
        if(j !== null ){
            if(j.length > 0){
                for (var i = 0; i < j.length; i++) {
                    options = options + "<option value=\""+ j[i].cod_turno+"\">" + j[i].nome_turno + "</option>";
                }
            }
            $("#txtHorario").html(options);
        }
    }).done(function(){
        trataCarregamentoTela();
    });

    $.getJSON("../Academia/form/ClientesFormServer.php", {isAjax:"S", listProfessor: "S"}, function (j) {
        var options = "<option value=\"\">Selecione</option>";
        if(j !== null ){
            if(j.length > 0){
                for (var i = 0; i < j.length; i++) {
                    options = options + "<option value=\""+ j[i].id_fornecedores_despesas+"\">" + j[i].razao_social + "</option>";
                }
            }
            $("#txtProfessor").html(options);
        }
    }).done(function(){
        trataCarregamentoTela();
    });

    $.getJSON("../Academia/form/AmbientesFormServer.php", {isAjax:"S", listAmbientes: "S"}, function (j) {
        var options = "<option value=\"\">Selecione</option>";
        if(j !== null ){
            if(j.length > 0){
                for (var i = 0; i < j.length; i++) {
                    options = options + "<option value=\""+ j[i].id_ambiente+"\">" + j[i].nome_ambiente + "</option>";
                }
            }
            $("#txtAmbiente").html(options);
        }
    }).done(function(){
        trataCarregamentoTela();
    });

    $.getJSON("../Academia/form/TurmasFormServer.php", {isAjax:"S", listPlanosTurma: "S", idTurma: $("#txtId").val()}, function (j) {
        var options = "";
        if(j !== null ){
            if(j.length > 0){
                for (var i = 0; i < j.length; i++) {
                    var selecionado = j[i].isCheck === 'S' ? "SELECTED" : "";
                    options = options + "<option value=\""+ j[i].conta_produto+"\" "+selecionado+" >" + j[i].descricao + "</option>";
                }
            }
            $("#mscPlanos").html(options);
        }
    }).done(function() {
        $('#mscPlanos').multiSelect({            
            selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder=''>",
            selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder=''>",
            afterInit: function(ms){
              var that = this,
                  $selectableSearch = that.$selectableUl.prev(),
                  $selectionSearch = that.$selectionUl.prev(),
                  selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                  selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

              that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
              .on('keydown', function(e){
                if (e.which === 40){
                  that.$selectableUl.focus();
                  return false;
                }
              });

              that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
              .on('keydown', function(e){
                if (e.which == 40){
                  that.$selectionUl.focus();
                  return false;
                }
              });
            },
            afterSelect: function(){
              this.qs1.cache();
              this.qs2.cache();
            },
            afterDeselect: function(){
              this.qs1.cache();
              this.qs2.cache();
            }
        });        
    });

    function trataCarregamentoTela(){
        totalFimCarregamento = totalFimCarregamento + 1;
        if(totalFimCarregamento === 3){
            if($("#txtId").val() !== ""){
                $.getJSON("../Academia/form/TurmasFormServer.php", {isAjax:"S", getTurma: $("#txtId").val()}, function (j) {
                    if(j !== null ){
                        $("#txtDescricao").val(j[0].descricao);
                        $("#txtDtInicio").val(j[0].dt_inicio);
                        $("#txtDtCriacao").val(j[0].dt_cadastro);
                        $('#ckbDesmatricular').parents('span').removeClass("checked").end().removeAttr("checked");
                        if (j[0].desmatricular === "1") { $('#ckbDesmatricular').click(); }
                        $('#ckbInativo').parents('span').removeClass("checked").end().removeAttr("checked");
                        if (j[0].inativo === "1") { $('#ckbInativo').click(); }
                        $("#txtProfessor").select2("val", j[0].professor);
                        $("#txtHorario").select2("val", j[0].horario);
                        $("#txtCapacidade").val(j[0].n_alunos);
                        $("#txtAmbiente").select2("val", j[0].ambiente);
                        $("#txDiasDesmatricula").val(j[0].dias_desmatricula);
                    }
                    listAlunosTurma();
                    listAlunosFila();
                    $("#loader").hide();
                    TrataEnableCampos(false);
                });
            }else{
                $("#loader").hide();
                TrataEnableCampos(true);
            }
        }
    }
});

function listAlunosTurma(){
    $('#tblAlunosTurma').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "form/TurmasFormServer.php?isAjax=S&listAlunosTurma=S&txtId="+$("#txtId").val(),
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        "scrollY": "120px",
        "scrollCollapse": true,
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers",
        "fnInitComplete": function (oSettings, json) {
            $("#qtdAluno").text($('#tblAlunosTurma').dataTable().fnGetData().length);
        }
    });
}

function listAlunosFila(){
    $('#tblAlunosFila').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "form/TurmasFormServer.php?isAjax=S&listAlunosFila=S&txtId="+$("#txtId").val(),
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        "scrollY": "445px",
        "scrollCollapse": true,
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers",
        "fnInitComplete": function (oSettings, json) {
            $("#qtdAlunoFila").text($('#tblAlunosFila').dataTable().fnGetData().length);
        }
    });
}

function TrataEnableCampos(enable) {
    if (!enable) {
        $("#divDadosTurma :input").attr("disabled", true);
        $('#txtProfessor, #txtHorario, #txtAmbiente').select2('disable');
        $('#ms-mscPlanos').find('li').addClass("disabled");
        $("#btnNovo, #btnAlterar, #btnExcluir").show();
        $("#btnSave, #btnCancelar").hide();
        $('.btnExcluir').hide();
    } else {
        $("#divDadosTurma :input").attr("disabled", false);
        $('#txtProfessor, #txtHorario, #txtAmbiente').select2('enable');
        $('#ms-mscPlanos').find('li').removeClass("disabled");
        $("#btnNovo, #btnAlterar, #btnExcluir").hide();
        $("#btnSave, #btnCancelar").show();
        $('.btnExcluir').show();
    }
    $("#btnPrint").attr("disabled", false);
    $("#btnPrintPresenca").attr("disabled", false);
    $("#btnPrintPresencaExcel").attr("disabled", false);
}

$(function () {
    $("#txtAlunoFila").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "../../Modulos/CRM/ajax.php",
                dataType: "json",
                data: {
                    q: request.term,
                    t: 'C'
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            $("#txtCodAlunoFila").val(ui.item.id);
        }
    });
});

$("#txtCapacidade").change(function(e){
    if($("#txtCapacidade").val() < $("#qtdAluno").text()){
        bootbox.alert("Valor de capacidade menor que o número de matriculados!");
    }
});

// <editor-fold defaultstate="collapsed" desc="Botoes">
function validaForm(){
    if($("#txtDescricao").val() === "" || $("#txtDtInicio").val() === "" || $("#txtCapacidade").val() === "" || $("#txtHorario").val() === ""){
        bootbox.alert("Os campos descriçao, data de inicio, Capacidade e Horario são obrigatórios!");
        return false;
    }
    return true;
}

$('#btnSave').click(function (e) {
    if (validaForm()) {
        $.post("form/TurmasFormServer.php", "isAjax=S&btnSave=S&txtId=" + $("#txtId").val() + "&" + $(".tab-content :input").serialize()).done(function (data) {
            if(!isNaN(data)){
                window.location = "TurmasForm.php?id=" + data;
            }else{
                bootbox.alert(data);
            }
        });
    }
});

$('#btnAlterar').click(function (e) {
    TrataEnableCampos(true);
});

$('#btnCancelar').click(function (e) {
    if($("#txtId").val()!== ""){
        window.location = "TurmasForm.php?id=" + $("#txtId").val();
    }else{
        window.location = "TurmasForm.php";
    }
});

$('#btnNovo').click(function (e) {
    window.location = "TurmasForm.php";
});

$('#btnExcluir').click(function (e) {
    bootbox.confirm('Confirma a exclusão da turma?', function (result) {
        if (result === true) {
            $("#loader").show();
            $.post("form/TurmasFormServer.php", "isAjax=S&removerTurma="+$("#txtId").val()).done(function (data) {
                if(data.trim() === "YES"){
                    window.location = "TurmasForm.php";
                } else {
                    bootbox.alert("Erro ao excluir. Remova todos os alunos cadastrados na turma e da lista de espera!");
                }
                $("#loader").hide();
            });
        }
    });
});

function AtualizaListaFila(){
    var tblAlunosFila = $('#tblAlunosFila').dataTable();
    tblAlunosFila.fnReloadAjax("form/TurmasFormServer.php?isAjax=S&listAlunosFila=S&txtId="+$("#txtId").val(), function(){
        $("#qtdAlunoFila").text($('#tblAlunosFila').dataTable().fnGetData().length);
    }, null);
}

function AtualizaListaTurma(){
    var tblAlunosFila = $('#tblAlunosTurma').dataTable();
    tblAlunosFila.fnReloadAjax("form/TurmasFormServer.php?isAjax=S&listAlunosTurma=S&txtId="+$("#txtId").val(), function(){
        $("#qtdAluno").text($('#tblAlunosTurma').dataTable().fnGetData().length);
    }, null);
}

$('#btnIncluirAlunoFila').click(function(e){
    if($("#txtCodAlunoFila").val() === "" || $("#txtAlunoFila").val() === "" ){
        bootbox.alert("Selecione um aluno corretamente!");
    }else{
        $.post( "form/TurmasFormServer.php", "isAjax=S&addAlunoFila=S&txtId="+$("#txtId").val()+"&txtCodAlunoFila="+$("#txtCodAlunoFila").val()).done(function( data ) {
            if(data.trim() === "TURMA"){
                bootbox.alert("Aluno já esta matriculado na turma!");
            }else if(data.trim() === "YES"){
                AtualizaListaFila();
                $("#txtCodAlunoFila, #txtAlunoFila").val("");
            }else{
                bootbox.alert("Erro ao adicionar o aluno!"+data.trim());
            }
        });
    }
});

function RemoverAlunoFila(idAluno){
    bootbox.confirm('Confirma a exclusão do aluno da fila de espera?', function (result) {
        if (result === true) {
            $("#loader").show();
            $.post("form/TurmasFormServer.php", "isAjax=S&removerAlunoFila="+idAluno+"&txtId=" + $("#txtId").val()).done(function (data) {
                if(data.trim() === "YES"){
                    AtualizaListaFila();
                } else {
                    bootbox.alert("Erro ao excluir");
                }
                $("#loader").hide();
            });
        }
    });
}

function MatricularAluno(idAluno,nmAluno){
    abrirTelaBox("ClientesTurmasForm.php?idAluno="+idAluno+"&nmAluno="+nmAluno, 531, 770);
}

function imprimirListaAlunos(){
    var pRel = "&NomeArq=" + "Alunos Turmas" +
        "&lbl=Cod Aluno|Nome Aluno|Telefone|Celular|E-mail" +
        "&siz=80|300|90|90|140" +
        "&pdf=5" + // Colunas do server processing que não irão aparecer no pdf
        "&filter=" + "Turma: " + $("#txtDescricao").val() + ", Professor: " + $("#txtProfessor option:selected").text() + //Label que irá aparecer os parametros de filtro
        "&PathArqInclude=" + "../Modulos/Academia/form/TurmasFormServer.php&isAjax=S&listAlunosTurma=S&txtId="+$("#txtId").val(); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel+ "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
}

function imprimirListaPresenca(tp){
    var pRel = "&Fonte=7" +
        "&NomeArq=" + "Turma" +
        "&lbl=Mat|Aluno|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31" +
        "&siz=35|200|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15" +
        "&pdf=32" + // Colunas do server processing que não irão aparecer no pdf
        "&filter=" + "Turma: " + $("#txtDescricao").val()+ 
        ", Professor: " + $("#txtProfessor option:selected").text() + 
        ", Horário: " + $("#txtHorario option:selected").text() + 
        ", Capacidade: " + $("#txtCapacidade").val() +         
        "&PathArqInclude=" + "../Modulos/Academia/form/TurmasFormServer.php&isAjax=S&listaPresenca=S&txtId="+$("#txtId").val(); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel+ "&tpImp=" + tp + "&PathArq=GenericModelPDF.php", '_blank');

}
// </editor-fold>

function AbrirCliente(id){
    window.location = './../Academia/ClientesForm.php?id='+id;
}

function FecharBoxCliente (){
    $("#newbox").remove();
    AtualizaListaFila();
    AtualizaListaTurma();
}

