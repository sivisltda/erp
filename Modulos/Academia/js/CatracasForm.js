$(document).ready(function () {
    $('#txtTipo').change(function () {
        ajustarLabel($('#txtTipo').val());
    });
    $('#txtSelIntervalo').change(function () {
        $('#txtIntervalo').val($('#txtSelIntervalo').val());
    });
    ajustarLabel($('#txtTipo').val());
});

function validaForm() {
    if ($("#txtDescricao").val() === "") {
        bootbox.alert("Descrição é Obrigatória!");
        return false;
    }
    if ($("#txtTipo").val() === "null") {
        bootbox.alert("Tipo é Obrigatório!");
        return false;
    }
    return true;
}

function ajustarLabel(tipo) {
    $("#txtIntervalo, #txtVariante, #txtPorta, #txtNumInner, #txtQtdDigitos, #txtSentido, #txtIp, #txtTpConexao, #txtTpEquip, #txtSelIntervalo").parent().hide();
    $("#Intervalo").parent().find("span").text("Intervalo:");
    $("#txtVariante").parent().find("span").text("Relé:");
    $("#txtPorta").parent().find("span").text("Porta:");
    $("#txtNumInner").parent().find("span").text("Número Inner:");
    $("#txtQtdDigitos").parent().find("span").text("Qtd. Digitos:");
    $("#txtTpConexao").parent().find("span").text("Tipo de Conexão:");
    $("#txtTpEquip").parent().find("span").text("Tipo Equipamento:");
    $("#txtSentido").parent().find("span").text("Catraca está instalada à sua:");
    $("#txtIp").parent().find("span").text("IP:");
    $("#txtIntervalo, #txtVariante, #txtPorta, #txtNumInner, #txtQtdDigitos").parent().css("width", "20%");
    $("#txtSentido, #txtIp, #txtTpConexao").parent().css("width", "36%");
    $("#txtTpEquip").parent().css("width", "62%");
    if (tipo === "3") {
        $("#txtNumInner, #txtQtdDigitos, #txtTpConexao, #txtTpEquip, #txtSentido, #txtVariante, #txtSelIntervalo, #txtIp").parent().show();
        $("#txtVariante").parent().find("span").text("Teclado:");
        $("#txtNumInner").parent().find("span").text("Endereço:");
        $("#txtTpConexao").parent().find("span").text("Biometria:");
        $("#txtTpConexao, #txtVariante").html("<option value=\"1\">Sim</option>" +
        "<option value=\"0\">Não</option>");
        $("#txtNumInner").parent().css("width", "13%");
        $("#txtQtdDigitos").parent().css("width", "15%");        
        $("#txtTpConexao").parent().css("width", "15%");
        $("#txtVariante").parent().css("width", "15%");        
        $("#txtSentido").parent().css("width", "22%");        
        $("#txtTpEquip").parent().css("width", "37%");        
        $("#txtTpEquip").html("<option value=\"0\">Não utilizado</option>" +
        "<option value=\"1\">Catraca Entrada/Saída</option>" +
        "<option value=\"2\">Catraca Entrada</option>" +
        "<option value=\"3\">Catraca Saída</option>" +
        "<option value=\"4\">Catraca Saída Liberada</option>" +
        "<option value=\"5\">Catraca Entrada Liberada</option>" +
        "<option value=\"6\">Catraca Liberada 2 Sentidos</option>" +
        "<option value=\"7\">Catraca Liberada 2 Sentidos(Sentido Giro)</option>" +
        "<option value=\"8\">Catraca com Urna</option>");
        $("#txtSentido").parent().find("span").text("Instalada à sua:");
        $("#txtSentido").parent().css("width", "22%");
        $("#txtSentido").html("<option value=\"E\">Esquerda</option>" +
        "<option value=\"D\">Direita</option>");        
        $("#txtSelIntervalo").parent().css("width", "39%");
        $("#txtSelIntervalo").parent().css("margin-left", "1%");        
        $("#txtSelIntervalo").parent().find("span").text("Leitor:");
        $("#txtSelIntervalo").html("<option value=\"0\">Código de Barras</option>" +
        "<option value=\"1\">Magnético</option>" +
        "<option value=\"2\">Prox.Abatrack/Smart Card</option>" +
        "<option value=\"3\">Prox.Wiegand/Smart Card</option>" +
        "<option value=\"4\">Prox.Smart Card Serial</option>" +
        "<option value=\"5\">Código de barras serial</option>" +
        "<option value=\"6\">Prox. Wiegand FC Sem Separador</option>" + 
        "<option value=\"7\">Prox. Wiegand FC Com Separador</option>" +
        "<option value=\"8\">Barras, Prox, QR Code c/ letras</option>");                
    } else if (tipo === "4" || tipo === "5" || tipo === "11" || tipo === "13") {
        $("#txtPorta, #txtNumInner, #txtQtdDigitos, #txtTpConexao, #txtTpEquip, #txtIp").parent().show();
        $("#txtNumInner").parent().find("span").text("Equipamento:");
        $("#txtQtdDigitos").parent().find("span").text("Duração:");
        if (tipo === "13") {
            $("#txtTpConexao").parent().find("span").text("Leitor Biométrico:");
            $("#txtTpConexao").html("<option value=\"1\">Interno</option><option value=\"0\">Externo</option>");
            $("#txtTpEquip").parent().css("width", "35%");
            $("#txtSentido").parent().show();
            $("#txtSentido").parent().css("width", "25%");
            $("#txtSentido").parent().find("span").text("Manter o braço:");
            $("#txtSentido").html("<option value=\"B\">Bloqueado</option><option value=\"E\">Anti Hor.</option><option value=\"D\">Horário</option><option value=\"A\">Liberado</option>");
        } else {
            $("#txtTpConexao").html((tipo === "4" ? "<option value=\"1\">Serial</option>" : "") +
            "<option value=\"2\">TCP IP</option>");
        }
        $("#txtTpEquip").html("<option value=\"0\">Bloqueada</option>" +
        "<option value=\"1\">Liberada Entrada</option>" +
        "<option value=\"2\">Liberada Saída</option>" +
        "<option value=\"3\">Libera os 2 Lados</option>");
    } else if (tipo === "6") {
        $("#txtNumInner, #txtIntervalo, #txtTpConexao, #txtTpEquip, #txtSentido, #txtVariante, #txtIp").parent().show();
        $("#txtNumInner").parent().find("span").text("N°.Relógio:");
        $("#txtIntervalo").parent().find("span").text("Duração:");
        $("#txtTpEquip").parent().css("width", "40%");
        $("#txtTpEquip").parent().find("span").text("Acionamento:");
        $("#txtTpEquip").html("<option value=\"0\">Relógio Simples sem Acionamento</option>" +
        "<option value=\"8\">Catraca Entrada</option>" +
        "<option value=\"9\">Catraca Saída</option>" +
        "<option value=\"11\">Catraca Entrada e Saída Livre</option>" +
        "<option value=\"13\">Catraca Bidirecional com Sentido Independente do Leitor</option>" +
        "<option value=\"2\">Fechadura sem sensor</option>" +
        "<option value=\"7\">Fechadura com sensor</option>" +
        "<option value=\"17\">Fechadura dupla sem sensor</option>");
        $("#txtTpConexao").parent().find("span").text("Tipo:");
        $("#txtTpConexao").html("<option value=\"0\">Horário</option>" +
        "<option value=\"1\">Anti Horário</option>");
        $("#txtVariante").parent().find("span").text("Entrada:");
        $("#txtSentido").parent().find("span").text("Saída:");
        $("#txtVariante, #txtSentido").html("<option value=\"0\">Libera</option>" +
        "<option value=\"1\">Bloqueia</option>");
        $("#txtTpConexao").parent().css("width", "25%");
        $("#txtNumInner").parent().css("width", "15%");
        $("#txtIntervalo").parent().css("width", "13%");
        $("#txtVariante, #txtSentido").parent().css("width", "21%");
        $("#txtIntervalo").parent().css("margin-left", "1%");
        //$("#txtSentido").parent().insertAfter($("#txtVariante").parent());
        if(!$.isNumeric($("#txtId").val())) {
            $("#txtVariante").attr("val", "0");
            $("#txtIntervalo").val("5");
        }        
    } else if (tipo === "8") {
        $("#txtVariante, #txtSentido, #txtIntervalo, #txtPorta").parent().show();        
        $("#txtVariante").html("<option value=\"1\">1</option>" +
        "<option value=\"2\">2</option>" +
        "<option value=\"3\">3</option>" +
        "<option value=\"4\">4</option>");
        $("#txtIntervalo").parent().css("margin-left", "1%");
        $("#txtSentido").parent().find("span").text("Modelo:");
        $("#txtSentido").html("<option value=\"0\">PADRÃO</option>" +
        "<option value=\"1\">MUSB2E2S</option>");
        $("#txtSentido").parent().css("width", "25%");        
        $("#txtPorta").parent().find("span").text("Porta COM (MUSB2E2S):");        
        $("#txtPorta").parent().css("width", "32%");
        $("#txtPorta").parent().css("margin-left", "0%");                
    } else if (tipo === "10") {
        $("#txtPorta, #txtTpConexao, #txtTpEquip, #txtIp").parent().show();
        $("#txtTpConexao").html("<option value=\"2\">TCP IP</option>");
        $("#txtTpEquip").html("<option value=\"1\">Liberada Entrada</option>" +
        "<option value=\"2\">Liberada Saída</option>" +
        "<option value=\"3\">Libera Ambos os Lados</option>");
        $("#txtTpEquip").parent().css("width", "41%");
    } else if (tipo === "12") {
        $("#txtTpEquip, #txtSentido, #txtQtdDigitos, #txtIp").parent().show();
        $("#txtQtdDigitos").parent().find("span").text("Duração:");
        $("#txtSentido").html("<option value=\"B\">Bloqueado</option><option value=\"E\">Anti Hor.</option><option value=\"D\">Horário</option><option value=\"A\">Liberado</option>");        
        $("#txtTpEquip").html("<option value=\"0\">Bloqueada</option>" +
        "<option value=\"1\">Liberada Entrada</option>" +
        "<option value=\"2\">Liberada Saída</option>" +
        "<option value=\"3\">Libera Ambos os Lados</option>");
        $("#txtTpEquip").parent().css("width", "41%");
    } else if (tipo === "14") {
        $("#txtNumInner, #txtIntervalo, #txtTpConexao, #txtTpEquip, #txtIp").parent().show();
        $("#txtNumInner").parent().find("span").text("N°.Relógio:");
        $("#txtIntervalo").parent().find("span").text("Duração:");
        $("#txtTpEquip").parent().css("width", "40%");
        $("#txtTpEquip").parent().find("span").text("Acionamento:");        
        $("#txtTpEquip").html("<option value=\"0\">Sem Acionamento</option>" +
        "<option value=\"1\">Acionar uma porta</option>" +
        "<option value=\"2\">Catraca unidimensional</option>" +
        "<option value=\"3\">Catraca bidimensional</option>");
        $("#txtTpConexao").parent().find("span").text("Tipo:");
        $("#txtTpConexao").html("<option value=\"0\">Direta Esquerda</option>" +
        "<option value=\"1\">Esquerda Direita</option>");
        $("#txtTpConexao").parent().css("width", "25%");
        $("#txtNumInner").parent().css("width", "15%");
        $("#txtIntervalo").parent().css("width", "13%");
        $("#txtIntervalo").parent().css("margin-left", "1%");
        if(!$.isNumeric($("#txtId").val())) {
            $("#txtIntervalo").val("5");
        }                
    }
    $("#txtVariante").val($("#txtVariante").attr("val"));
    $("#txtTpConexao").val($("#txtTpConexao").attr("val"));
    $("#txtTpEquip").val($("#txtTpEquip").attr("val"));
    $("#txtSentido").val($("#txtSentido").attr("val"));
    $("#txtSelIntervalo").val($("#txtSelIntervalo").attr("val"));
}