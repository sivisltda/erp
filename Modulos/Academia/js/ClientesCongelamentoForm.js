function iniciaCongelamentoForm() {
    $("#spinner").mask("999");
    $("#txtDtInicio").mask(lang["dateMask"]);

    $("#txtIdPlano").val($("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected)').find('.detalhe').attr('data-id'));
    $("#txtPlano").val($("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected) >td:nth-child(1) ').text());

    $('#spinner').blur(function () {
        var dtFimContr = $("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected) >td:nth-child(3) ').text();
        $("#txtDtTermino").val(moment(dtFimContr, lang["dmy"]).add($('#spinner').val(), 'days').format(lang["dmy"]));
    });

    $.getJSON("form/ClientesPlanosServer.php?getDiasRestFerias=" + $("#txtIdPlano").val(), function (j) {
        if (j.length > 0) {
            if (!j[0].dt_cancelamento) {
                $("#txtDias").val(j[0].dias);
                $("#btnAddCongelamento").attr("disabled", false);
            } else {
                $("#btnAddCongelamento").attr("disabled", true);
            }
        }
    });

    var tblCongHist = $('#tblCongHist').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        bPaginate: false, bFilter: false, bInfo: false, bSort: false,
        "sAjaxSource": "form/ClientesPlanosServer.php?listHistCongelamento=" + $("#txtIdPlano").val(),
        'oLanguage': {
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sZeroRecords': "Não foi encontrado nenhum resultado"
        }
    });

    $('#btnAddCongelamento').click(function (e) {
        var dtInicioCont = moment($("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected) >td:nth-child(3) ').text(), lang["dmy"]);
        var dtInicioCong = moment($("#txtDtInicio").val(), lang["dmy"]);
        if (dtInicioCont >= dtInicioCong) {
            bootbox.alert("Data de início de congelamento inválido!");
        } else if ($("#txtDtInicio").val() === "" || $("#spinner").val() === "" || $("#spinner").val() <= 0 || $("#txtMotivo").val() === "null") {
            bootbox.alert("Todos os campos são obrigatórios");
        } else if (eval($('#spinner').val()) > eval($("#txtDias").val())) {
            bootbox.alert("Quantidade de dias maior que dias disponíveis!");
        } else {
            $.post("form/ClientesPlanosServer.php", "addCongelamento=" + $("#txtIdPlano").val() + "&txtMotivo=" + $("#txtMotivo").val() + "&txtDias=" + $("#spinner").val() + "&txtDtIni=" + $("#txtDtInicio").val().replace(/\//g, "_")).done(function (data) {
                if (data.trim() === "YES") {
                    parent.AtualizaListPlanosClientes();
                    parent.atualizaStatusCli();                      
                    parent.FecharBox(0);
                } else {
                    bootbox.alert("Erro ao adicionar congelamento!");
                }
            });
        }
    });

    $('#tblCongHist').on('click', '.excluir', function (e) {
        var idPlano = $(this).attr('data-id');
        bootbox.confirm('Confirma a exclusão do registro?', function (result) {
            if (result === true) {
                $.post("form/ClientesPlanosServer.php", {delCongelamento: idPlano}).done(function (data) {
                    if (data.trim() === "YES") {
                        parent.AtualizaListPlanosClientes();
                        parent.atualizaStatusCli();                        
                        parent.FecharBox(0);
                    } else {
                        bootbox.alert("Erro ao excluir registro!");
                    }
                });
            } else {
                return;
            }
        });
    });

    $('#tblCongHist').on('click', '.parar', function (e) {
        var idPlano = $(this).attr('data-id');
        bootbox.confirm('Confirma o paralisação do congelamento?', function (result) {
            if (result === true) {
                $.post("form/ClientesPlanosServer.php", {pararCongelamento: idPlano}).done(function (data) {
                    if (data.trim() === "YES") {
                        parent.AtualizaListPlanosClientes();
                        parent.atualizaStatusCli();
                        parent.FecharBox(0);
                    } else if (data.trim() === "NO") {
                        bootbox.alert("Data de atual maior/menor que o período!");
                    } else {
                        bootbox.alert("Erro ao atualizar registro!");
                    }
                });
            } else {
                return;
            }
        });
    });
}

