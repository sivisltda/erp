var columns = [{ "bSortable": true },
               { "bSortable": false },
               { "bSortable": false },
               { "bSortable": false },
               { "bSortable": false },
               { "bSortable": false }];

 if($("#imprimir").val() !== "1"){
     columns.push({ "bSortable": false});
 }

 var tbLista = $("#tblHorarios").dataTable({
     "iDisplayLength": 20,
     "aLengthMenu": [20, 30, 40, 50, 100],
     "bProcessing": true,
     "bServerSide": true,
     "sAjaxSource": finalFind(0),
     "bFilter": false,
     "aoColumns": columns, 
     "oLanguage": {
         "oPaginate": {
             "sFirst": "Primeiro",
             "sLast": "Último",
             "sNext": "Próximo",
             "sPrevious": "Anterior"
         }, "sEmptyTable": "Não foi encontrado nenhum resultado",
         "sInfo": "Visualização do registro _START_ ao _END_ de _TOTAL_",
         "sInfoEmpty": "Visualização do registro 0 ao 0 de 0",
         "sInfoFiltered": "(filtrado de _MAX_ registros totais)",
         "sLengthMenu": "Visualização de _MENU_ registros",
         "sLoadingRecords": "Carregando...",
         "sProcessing": "Processando...",
         "sSearch": "Pesquisar:",
         "sZeroRecords": "Não foi encontrado nenhum resultado"},
     "sPaginationType": "full_numbers",
     "fnInitComplete": function (oSettings, json) {
         if($("#imprimir").val() === "1"){
             $(".body").css("margin-left", 0);
             $("#parametros_busca").hide();
             $("#tblHorarios_length").remove();
             $("#tblHorarios_filter").remove();
             $("#tblHorarios_paginate").remove();
             $("#formPQ > th").css("background-image", "none");
             window.print();
             window.history.back();
         }
     }
 });

$(document).ready(function () {        
    $("#btnPesquisar").click(function() {
        tbLista.fnReloadAjax(finalFind(0));        
    });
});

function AbrirBox(id,opc) {
    if (opc === 1) { 
        var myId = "";
        if (id > 0){ myId = "?id="+ id; } 
        abrirTelaBox("HorariosForm.php"+ myId, 415, 460);
    } else if (opc === 0) { 
        window.location = finalFind(1);
    }
}

function FecharBox() {
    var oTable = $("#tblHorarios").dataTable();
    oTable.fnDraw(false);
    $("#newbox").remove();
}

function RemoverItem(id) {
    if (confirm("Deseja deletar esse registro?")) {
        $.post("form/HorariosFormServer.php", { List: true, bntDelete: true, txtId: id }).done(function(data) {
            tbLista.fnReloadAjax(finalFind(0)); 
        });
    }
} 

function finalFind(op) {
    var retPrint = "&Search="+ $("#txtBuscar").val();
    if (op === 0) {
        return "ajax/Horarios_server_processing.php?imp="+ $("#imprimir").val() + retPrint;
    } else {
        return "HorariosList.php?imp=1"+ retPrint;
    }    
}