function iniciaDetalhesForm() {
    $("#textoFin").hide();
    $("#txtDtIniDet, #txtDtFimDet").mask(lang["dateMask"]);
    $("#txtCreditoConvFin").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});

    var idPlano = $("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected)').find('.detalhe').attr('data-id');
    $.getJSON("form/ClientesPlanosServer.php", {listPlanos: $("#txtId", window.parent.document).val(), tpTela: "R", idItem: $("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected)').find('.detalhe').attr('data-id')}, function (j) {
        var idsel = "0";
        var options = "<option value=\"0\">Selecione o Plano</option>";
        if (j.length > 0) {
            for (var i = 0; i < j.length; i++) {
                options = options + "<option value=\"" + j[i].id + "\">" + j[i].descricao + "</option>";
                idsel = j[i].id;
            }
        }
        $("#txtPlanoDet").html(options);
        $("#txtPlanoDet").select2("val", idsel);
    });
    var idTurno, idProd = "";
    $.getJSON("form/ClientesPlanosServer.php", {getPlano: $("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected)').find('.detalhe').attr('data-id')}, function (j) {
        if (j !== null) {
            $("#txtDtIniDet").val(j[0].dt_inicio);
            $("#txtDtFimDet").val(j[0].dt_fim);
            $("#txtCompradoEm").val(j[0].dt_cadastro);
            $("#txtUserResp").val(j[0].nome_resp);
            $("#txtMesesRest").val(j[0].mesesrest);
            $("#txtMesesRestTra").val(j[0].mesesrest);
            $("#txtCreditoConvCar").val(j[0].valorcredito);
            $("#txtCreditoConvCarTra").val(j[0].valorcredito);
            $("#txtCreditoConvFin").val(j[0].valorcreditoFinanceiro);
            if (textToNumber(j[0].valorcredito) > 0) {
                $("#btnConversao").attr("disabled", false);
            }
            if (j[0].dt_cancelamento) {
                $("#divCancel").show();
                $("#txtDtCancelamento").val(j[0].dt_cancelamento);
                $("#txtUserRespCancel").val(j[0].nome_resp_cancel);
                $("#btnSalvaDet, #btnSalvaTra, #btnSalvaTransf, #btnConversao").hide();
            } else {
                $("#divCancel").hide();
            }
            if (j[0].dt_transferencia) {
                $("#divTransf").show();
                $("#txtDtTransferencia").val(j[0].dt_transferencia);
                $("#txtTransfOrigem").val(j[0].nome_origem);
                $("#btnSalvaTransf").hide();
                $('a[href="#tab4"], a[href="#tab2"]').hide();
            } else {
                $("#divTransf").hide();
            }
            $("#txtFerias").val(j[0].ferias);            
            idTurno = j[0].id_turno;
            idProd = j[0].id_produto;
        }
    }).done(function ( ) {
        $.getJSON("form/ClientesPlanosServer.php", {listHorarios: "S", idPlano: idProd}, function (j) {
            var options = "<option value=\"\">Selecione o Horário</option>";
            if (j) {
                for (var i = 0; i < j.length; i++) {
                    options = options + "<option value=\"" + j[i].cod_turno + "\">" + j[i].nome_turno + "</option>";
                }
            }
            $("#txtHorarioDet").html(options);
            $("#txtHorarioDet").select2("val", idTurno);
        });
    });

    listCreditos();

    $('#btnSalvaDet').click(function (e) {
        $.post("form/ClientesPlanosServer.php", "atuContrato=" + $("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected)').find('.detalhe').attr('data-id') + "&txtHorarioDet=" + $("#txtHorarioDet").val()).done(function (data) {
            if (data.trim() === "YES") {
                bootbox.alert("Alteração efetuada com sucesso!");
            } else {
                bootbox.alert("Erro ao salvar dados!");
            }
        });
    });

    $('input[name="rCredTipo"]').click(function (e) {
        if ($('input[name="rCredTipo"]:checked').val() === "0") {
            $("#txtCreditoConvFin").hide();
            $("#txtCreditoConvCar").show();
            $("#textoFin").hide();
        } else {
            $("#txtCreditoConvFin").show();
            $("#txtCreditoConvCar").hide();
            $("#textoFin").show();
        }
    });

    $('#btnConversao').click(function (e) {
        if (($("#txtCreditoConvFin").val() === 0 || $("#txtCreditoConvFin").val() === '') && $('input[name="rCredTipo"]:checked').val() === "1") {
            bootbox.alert("Valor inválido!");
        } else if ($("#txtFerias").val() !== "0") {
            bootbox.alert("Plano em férias, não é possível realizar esta operação!");
        } else if (textToNumber($("#txtCreditoConvFin").val()) > textToNumber($("#txtCreditoConvCar").val())) {
            bootbox.alert("Valor máximo permitido de " + $("#txtCreditoConvCar").val() + "!");
        } else {
            bootbox.confirm('Confirma a conversão dos créditos? Essa ação não poderá ser revertida.', function (result) {
                if (result === true) {
                    var valor = $("#txtCreditoConvCar").val();
                    var tipo = "C";
                    if ($('input[name="rCredTipo"]:checked').val() === "1") {
                        valor = $("#txtCreditoConvFin").val();
                        tipo = "F";
                    }
                    $.post("form/ClientesPlanosServer.php", "addCreditoCli=" + $("#txtId", window.parent.document).val() + "&txtValor=" + valor + "&idPlano=" + idPlano + "&txtDescrPlano=" + $("#txtPlanoDet").select2('data').text + "&tipoCred=" + tipo).done(function (data) {
                        if (data.trim() === "YES") {
                            atualizaListCreditos();
                            $("#btnConversao").attr("disabled", true);
                            $("#txtCreditoConvCar, #txtCreditoConvFin").val(numberFormat(0));
                            parent.AtualizaListPlanosClientes();
                        } else {
                            bootbox.alert("Erro ao gerar crédito!");
                        }
                    });
                } else {
                    return;
                }
            });
        }
    });

    $('#btnSalvaTransf').click(function (e) {
        if ($("#txtDtCancelamento").val() === "" && $("#txtDtTransferencia").val() === "" &&
                ((textToNumber($("#txtCreditoConvCar").val()) > 0) || (textToNumber($("#txtCreditoConvFin").val()) > 0))) {
            if ($("#txtTransfParaCod").val() === "" || $("#txtMotivo").val() === "") {
                bootbox.alert("Destinatário e Motivo são obrigatórios!");
            } else if ($("#txtFerias").val() !== "0") {
                bootbox.alert("Plano em férias, não é possível realizar esta operação!");                
            } else {
                bootbox.confirm('Confirma a Transferêcia? Essa ação não poderá ser revertida.', function (result) {
                    if (result === true) {
                        $.post("form/ClientesPlanosServer.php", "transfPlano=" + $("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected)').find('.detalhe').attr('data-id') +
                                "&txtTransfParaCod=" + $("#txtTransfParaCod").val() +
                                "&txtIdDe=" + $("#txtId", window.parent.document).val() +
                                "&txtMotivo=" + $("#txtMotivo").val()).done(function (data) {
                            if (data.trim() === "YES") {
                                bootbox.alert("Transferência efetuada com sucesso!");
                                parent.StatusCliEspecific($("#txtTransfParaCod").val());
                                parent.AtualizaListPlanosClientes();
                                parent.atualizaStatusCli();
                                if (parent.$("#tbListaVeiculo").length > 0) {
                                    parent.tbListaVeiculo.fnReloadAjax(parent.finalFindVeiculo(0));
                                    parent.tbListaVistorias.fnReloadAjax(parent.finalFindVistorias(0));
                                }
                                parent.FecharBoxCliente();                                
                            } else {
                                bootbox.alert("Erro ao salvar dados!");
                            }
                        });
                    }
                });
            }
        } else {
            bootbox.alert("Impossivel Transferir!");
        }
    });

    $("#txtTransfPara").change(function (e) {
        if ($("#txtTransfPara").val() === "") {
            $("#txtTransfParaCod").val("");
        }
    });
}

$(function () {
    $("#txtTransfPara").autocomplete({
        source: function (request, response) {
            $("#txtTransfParaCod").val("");
            $.ajax({
                url: "../../Modulos/CRM/ajax.php",
                dataType: "json",
                data: {
                    q: request.term,
                    t: 'C',
                    n: $("#txtId", window.parent.document).val()
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            $("#txtTransfParaCod").val(ui.item.id);
        }
    });
});

function atualizaListCreditos() {
    var tblPagamentosPlano = $('#tblHistCredito').dataTable();
    tblPagamentosPlano.fnReloadAjax("form/ClientesPlanosServer.php?listHistCreditos=" + $("#txtId", window.parent.document).val() + "&tipo=C");
}

function listCreditos() {
    $('#tblHistCredito').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "form/ClientesPlanosServer.php?listHistCreditos=" + $("#txtId", window.parent.document).val() + "&tipo=C",
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });
}