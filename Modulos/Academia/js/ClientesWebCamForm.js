$(document).ready(function () {
    $("#webcam").scriptcam({
        showMicrophoneErrors: false,
        onError: onError,
        cornerRadius: 20,
        cornerColor: 'e3e5e2',
        onWebcamReady: onWebcamReady,
        uploadImage: "",
        onPictureAsBase64: base64_tofield_and_image,
        width: 400,
        height: 400
    });
});

function base64_toimage() {
    $('#image').attr("src", "data:image/png;base64," + $.scriptcam.getFrameAsBase64());
}
;

function saveImagem() {
    if ($("#image").attr("src") !== undefined && $("#image").attr("src") !== "") {
        $.post("form/ClientesFormServer.php", "isAjax=S&uploadImage=S&img=" + $("#image").attr("src") + "&txtId=" + $("#txtId", window.parent.document).val()).done(function (data) {
            if (data.trim() === "YES") {
                var d = new Date();
                $("#ImgCliente", window.parent.document).attr("src", "./../../Pessoas/" + $("#txtContrato").val() + "/Clientes/" + $("#txtId", window.parent.document).val() + ".png?" + d);
                $('#btnRemImg', window.parent.document).show();
                parent.FecharBox(0);
            } else {
                alert(data);
            }
        });
    } else {
        bootbox.alert("Para confirmar você deve capturar uma imagem!");
    }
}
function base64_tofield_and_image(b64) {
    $('#formfield').val(b64);
    $('#image').attr("src", "data:image/png;base64," + b64);
}
;
function onError(errorId, errorMsg) {
    $("#btn1").attr("disabled", true);
    $("#btn2").attr("disabled", true);
    alert(errorMsg);
}
function onWebcamReady(cameraNames, camera, microphoneNames, microphone, volume) {
    $.each(cameraNames, function (index, text) {
        $('#cameraNames').append($('<option></option>').val(index).html(text))
    });
    $('#cameraNames').val(camera);
}