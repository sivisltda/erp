$(document).ready(function () {
    var columns = [{"bSortable": true}, {"bSortable": true}, {"bSortable": true}];

    if ($("#imprimir").val() === "0") {
        columns.push({"bSortable": false});
    }

    var tbLista = $("#tblConvenios").dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": finalFind(0),
        "bFilter": false,
        "aoColumns": columns,
        "oLanguage": {
            "oPaginate": {
                "sFirst": "Primeiro",
                "sLast": "Último",
                "sNext": "Próximo",
                "sPrevious": "Anterior"
            }, "sEmptyTable": "Não foi encontrado nenhum resultado",
            "sInfo": "Visualização do registro _START_ ao _END_ de _TOTAL_",
            "sInfoEmpty": "Visualização do registro 0 ao 0 de 0",
            "sInfoFiltered": "(filtrado de _MAX_ registros totais)",
            "sLengthMenu": "Visualização de _MENU_ registros",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sSearch": "Pesquisar:",
            "sZeroRecords": "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers",
        "fnInitComplete": function (oSettings, json) {
            if ($("#imprimir").val() === "1") {
                $(".body").css("margin-left", 0);
                $("#parametros_busca").hide();
                $("#tblConvenios_length").remove();
                $("#tblConvenioss_filter").remove();
                $("#tblConvenios_paginate").remove();
                $("#formPQ > th").css("background-image", "none");
                window.print();
                window.history.back();
            }
        }
    });

    $("#btnPesquisar").click(function () {
        tbLista.fnReloadAjax(finalFind(0));
    });
});

function AbrirBox(id, opc) {
    if (opc === 1) {
        var myId = "";
        if (id > 0) {
            myId = "?id=" + id;
        }
        abrirTelaBox("ConveniosForm.php" + myId, 448, 550);
    } else if (opc === 0) {
        window.location = finalFind(1);
    }
}

function FecharBox() {
    var oTable = $("#tblConvenios").dataTable();
    oTable.fnDraw(false);
    $("#newbox").remove();
}

function RemoverItem(id) {
    if (confirm("Confirma a exclusão do registro?")) {
        $.post("form/ConveniosFormServer.php", {List: true, btnDelete: true, txtId: id}).done(function (data) {
            if (data.trim() === "YES") {
                var tbLista = $("#tblConvenios").dataTable();
                tbLista.fnDraw(false);
            } else {
                FecharBox();
                bootbox.alert("Erro ao excluir registro!");
            }
        });
    }
}

function finalFind(op) {
    var retPrint = "&Search=" + $("#txtBuscar").val() + '&at=' + $("#txtStatus").val();
    if (op === 0) {
        return "ajax/Convenios_server_processing.php?imp=" + $("#imprimir").val() + retPrint;
    } else {
        return "ConveniosList.php?imp=1" + retPrint;
    }
}