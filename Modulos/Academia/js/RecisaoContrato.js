
$("#txtDtLimite").mask(lang["dateMask"]);
VerificarData();

function verificaObgtorio() {
    var error = '';
    if ($('#txtDtLimite').val() === "") {
        error = "Seleciona a data de Rescisão de Contrato.";
    } else if ($('#txtMotivo').val() === "") {
        error = "Digite o motivo da Rescisão do Contrato.";
    } else if ($('#txtCredModelo').val() === "null" && $('#txtCredModelo').parent().is(":visible")) {
        error = "Selecione o Modelo Credencial.";
    }
    if (error === '') {
        gravar();
    } else {
        bootbox.alert(error);
    }
}

$('#txtDtLimite').change(function() {
    VerificarData();
});

function VerificarData() {
    let given = moment($("#txtDtLimite").val(), "DD/MM/YYYY");
    let current = moment().startOf('day');
    if (moment.duration(given.diff(current)).asDays() > 0) {
        $("#txtCredModelo").parent().show(); 
    } else {
        $("#txtCredModelo").parent().hide();
    }
    $('#txtCredModelo').select2("val", "null");
}

function gravar() {
    $.ajax({
        url: "../ajax/recisao_contrato_server.php",
        dataType: "json",
        data: {
            credModelo: $('#txtCredModelo').val(),
            dtLimite: $('#txtDtLimite').val(),
            motiv: $('#txtMotivo').val(),
            idplano: $("#txtidplano").val(),
            favoredico: $("#txtId").val(),
            oneday: $("#txtOneDay").is(':checked')
        },
        success: function (data) {
            parent.atualizaCredenciais();
            parent.AtualizaListPlanosClientes();
            var tblPagamentosPlano = parent.$('#tblPagamentosPlano').dataTable();
            tblPagamentosPlano.fnReloadAjax("./../Academia/form/ClientesPlanosServer.php?listPagamentosPlano=" + parent.$("#txtId").val() + "&idPlano=" + $("#txtidplano").val() + "&serasa=" + parent.$("#ckbInativo").val());
            window.location.href = "../../CRM/ImprimirEmail.php?id=" + $("#txtId", window.parent.document).val() + "&doc=" + $("#txtidplano").val() + "&tp=" + 1 + "&minHeight=" + 250;
        }
    });
}

function imprimir() {
    window.location.href = "../../CRM/ImprimirEmail.php?id=" + $("#txtId", window.parent.document).val() + "&doc=" + $("#txtidplano").val() + "&tp=" + 1 + "&minHeight=" + 250;
}

function desfazer() {
    bootbox.confirm('Confirma a ação de desfazer o cancelamento deste plano?', function (result) {
        if (result === true) {
            $.ajax({
                url: "../ajax/recisao_contrato_desfazer_server.php",
                dataType: "json",
                data: {
                    idplano: $("#txtidplano").val()
                },
                success: function (data) {
                    parent.atualizaCredenciais();
                    parent.AtualizaListPlanosClientes();
                    var tblPagamentosPlano = parent.$('#tblPagamentosPlano').dataTable();
                    tblPagamentosPlano.fnReloadAjax("./../Academia/form/ClientesPlanosServer.php?listPagamentosPlano=" + parent.$("#txtId").val() + "&idPlano=" + $("#txtidplano").val() + "&serasa=" + parent.$("#ckbInativo").val());
                    parent.FecharBox(1);                    
                }
            });
        }
    });
}