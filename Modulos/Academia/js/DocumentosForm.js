$(document).ready(function () {
    if ($("#txtID").val() > 0) {
        documento();
    }

    $("#mscPlanos").multiSelect({
        selectableHeader: "<div class='multipleselect-header'>Todas</div>",
        selectionHeader: "<div class='multipleselect-header'>Permitidas</div>"
    });

    if ($('#txtNome').attr("disabled") === "disabled") {
        $('#ms-mscPlanos').find('li').addClass("disabled");
    }

});

function removeLinha(id) {
    $("#" + id).remove();
}

function validaForm() {
    if ($("#txtNome").val() === "") {
        bootbox.alert("Nome é Obrigatória!");
        return false;
    }
    if ($("#txtdias").val() === "" || $("#txtdias").val() < 0) {
        bootbox.alert("Quantidade de dias tem que ser maior que zero ou zero!");
        return false;
    }
    return true;
}

function documento() {
    var idDoc = '';
    if ($("#txtID").val() > 0) {
        idDoc = "?id=" + $("#txtID").val();
    }
    $.getJSON('ajax/DocsList_server_processing.php' + idDoc, function (j) {
    });
}
