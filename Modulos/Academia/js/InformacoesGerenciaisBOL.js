var clickTabBOL = 0;
$(document).ready(function () {
    $("#txtDescBol").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "./../CRM/ajax.php",
                dataType: "json",
                data: {
                    q: request.term,
                    t: "C"
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            $('#txtIdNotaBol').val(ui.item.id).show();
            $('#txtDescBol').val(ui.item.label).show();
            getPlanos(ui.item.id);
        }
    });
    $("#txtDescBol, #txtClientePlanoBol").prop('disabled', true);
    carregaTabBoleto();
    $("#txtTipoClienteBol").change(function () {
        if ($(this).val() === "1") {
            $("#txtDescBol, #txtClientePlanoBol").prop('disabled', false);
        } else {
            $("#txtDescBol, #txtClientePlanoBol").prop('disabled', true);
            $("#txtIdNotaBol").val("");
            $("#txtDescBol").val("");
            $("#txtClientePlanoBol").val("null");
            getPlanos("null");
        }
    });

    $("#ckb_data_pgtoBol").change(function () {
        if ($(this).is(":checked")) {
            $("#txtTipoPgtoBol").prop('disabled', false);
            $("#txtDataIniBol").prop('disabled', false);
            $("#txtDataFinBol").prop('disabled', false);
            $("#txtDataIniBol").val(moment().startOf('month').format(lang["dmy"]));
            $("#txtDataFinBol").val(moment().endOf('month').format(lang["dmy"]));
        } else {
            $("#txtTipoPgtoBol").prop('disabled', true);
            $("#txtDataIniBol").prop('disabled', true);
            $("#txtDataFinBol").prop('disabled', true);
            $("#txtDataIniBol").val("");
            $("#txtDataFinBol").val("");
        }
    });
    
    $("#ckb_grupoFin").change(function () {
        refreshTableBol();
    });
});

function getPlanos(id) {
    let options = '<option value="null">TODOS</option>';
    if (id === "null") { 
        $('#txtClientePlanoBol').html(options).show();
    } else {
        $.getJSON('planos.ajax.php', {txtPessoa: id}, function (j) {
            for (var i = 0; i < j.length; i++) {
                options += '<option value="' + j[i].id_plano + '">' + j[i].descricao + '</option>';
            }
            $('#txtClientePlanoBol').html(options).show();
        });
    }
}

function carregaTabBoleto() {
    if (clickTabBOL === 1) {
        return;
    }
    clickTabBOL = 1;
    $("#loader").show();
    $('#tbListaBOL').dataTable({
        "iDisplayLength": 20,
        "bProcessing": true,
        "bServerSide": true,
        "ordering": false,
        "bFilter": false,
        "bLengthChange": false,
        "sAjaxSource": final_url_bol(0),
        'oLanguage': {
            'oPaginate': {
                'sFirst': "Primeiro",
                'sLast': "Último",
                'sNext': "Próximo",
                'sPrevious': "Anterior"
            }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
            'sLengthMenu': "Visualização de _MENU_ registros",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers",
        "fnDrawCallback": function () {
            $("#loader").hide();
        }
    });
}

function refreshTableBol() {
    $("#loader").show();
    var tblrefresh = $("#tbListaBOL").dataTable();
    tblrefresh.fnReloadAjax(final_url_bol(0));
}

function final_url_bol(imp) {
    return "./../../util/dcc/BOL_server_processing.php?imp=" + imp +
            "&fil=" + $('#txtLojaSel').val() +            
            "&tcl=" + $("#txtTipoClienteBol").val() +
            "&cli=" + $("#txtIdNotaBol").val() +            
            "&pln=" + $("#txtClientePlanoBol").val() +
            "&dck=" + ($("#ckb_data_pgtoBol").is(':checked') ? 1 : 0) +            
            "&grp=" + ($("#ckb_grupoFin").is(':checked') ? 1 : 0) +            
            "&dtp=" + $("#txtTipoPgtoBol").val() +
            "&din=" + $("#txtDataIniBol").val() +
            "&dfn=" + $("#txtDataFinBol").val() +
            "&sts=" + $("#txtTipoBol").val();
}

function printBol(tp) {
    var pRel = "&NomeArq=" + "Transações em BOL" +
            "&lbl=" + "Dt.Mens.|Mat.|Cliente|Pedido|Transação|Valor Mens.|Dt.Pgto." +
            "&siz=" + "55|45|190|170|120|60|60" +
            "&pdf=" + "7|8" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + "" + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + final_url_bol(1).replace("./../../util", "./").replace("?", "&"); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tp + "&PathArq=GenericModelPDF.php", '_blank');
}

function ExportarExcelBol() {
    printBol("E");
}

function ler_saldoBol() {
    $.getJSON("./../../util/dcc/saldoDCC.php", {ajax: "true"}, function (j) {
        $('#saldoImpBol').val(j);
        $('#saldoBtnBol').hide();
        $('#saldoImpBol').show();
    });
}

function sendTableBol() {
    if ($("#txtTipoBol").val() !== "2") {
        bootbox.alert("Opção válida apenas para os itens NÃO GERADOS!");
    } else {
        bootbox.confirm('Confirma a geração dos boletos?', function (result) {
            if (result === true) {
                $.post(final_url_bol(1)).done(function (data) {
                    var response = jQuery.parseJSON(data);
                    $("#source").dialog({modal: true});
                    $("#progressbar").progressbar({value: 0});
                    $("#pgIni").html("0");
                    $("#pgFim").html(response.aaData.length);
                    $("#txtTotalSucesso, #txtTotalErro").val(0);  
                    if ($("#txtTipoBoletoEnvio").val() === "0") {
                        processarBol(response.aaData);                        
                    } else {
                        processarBolOnline(response.aaData);
                    }
                });
            }
        });
    }
}

function printBolCar() {
    if ($("#txtTipoBol").val() !== "1") {
        bootbox.alert("Opção válida apenas para os itens GERADOS!");
    } else if ($("#txtTipoClienteBol").val() !== "1") {
        bootbox.alert("Opção válida apenas para o tipo ESPECÍFICO!");
    } else {
        bootbox.confirm('Confirma a impressão do carnê?', function (result) {
            if (result === true) {
                $("#loader").show();
                $.post(final_url_bol(1)).done(function (data) {
                    let lista = jQuery.parseJSON(data);
                    let lote = "";
                    for(i = lista.aaData.length; i >= 0; i--) {
                        if($.isArray(lista.aaData[i])) {
                            if($.isNumeric(lista.aaData[i][4])) {
                                lote = lote + lista.aaData[i][4] + ",";
                            }
                        }
                    }
                    $.ajax({
                        type: "POST", url: "../../util/dcc/makeBoletoOnline.php",
                        data: {imprimirBoleto: "S", txtIdBol: lote.slice(0,-1)},
                        success: function (data) {
                            $("#loader").hide();
                            window.open(data,'_blank');
                        },
                        error: function (error) {
                            $("#loader").hide();                            
                            bootbox.alert(error);
                        }
                    });
                });
            }
        });
    }
}

function processarBolOnline(lista) {
    var PgIni = parseInt($("#pgIni").html());
    var PgFim = parseInt($("#pgFim").html());
    var data_itens = "";
    if ((PgIni < PgFim) && PgFim > 0 && $('#source').dialog('isOpen')) {                    
        $("#progressbar").progressbar({value: (((PgIni + 1) * 100) / PgFim)});
        $("#pgIni").html((PgIni + 1));
        if($.isArray(lista[PgIni])){
            var IdChk = lista[PgIni][0].split("<input name=\"nEnviadoB[]\" type=\"hidden\" value=\"");                
            IdChk = IdChk[1].split("\"/>");
            data_itens = {salvaBoletoMens: "S", id_mens: IdChk[0]};
        }
        $.ajax({
            type: "POST", url: "../../util/dcc/makeBoletoOnline.php", dataType: "json",
            data: data_itens,
            success: function (data) {
                $("#txtTotalSucesso").val(parseInt($("#txtTotalSucesso").val()) + 1);
                processarBolOnline(lista);
            },
            error: function (error) {
                $("#txtTotalErro").val(parseInt($("#txtTotalErro").val()) + 1);
                processarBolOnline(lista);    
            }
        });
    } else {
        $("#source").dialog('close');                    
        bootbox.alert("Transações: " + $("#txtTotalSucesso").val() + " Aprovada(s) e " + $("#txtTotalErro").val() + " com erro(s)!");
        refreshTableBol();
    }
}

function processarBol(lista) {
    var PgIni = parseInt($("#pgIni").html());
    var PgFim = parseInt($("#pgFim").html());
    var lote = "";
    if ((PgIni < PgFim) && PgFim > 0 && $('#source').dialog('isOpen')) {
        for(i = PgIni; i < PgIni + 1; i++) {
            if($.isArray(lista[i])){
                var IdChk = lista[i][0].split("<input name=\"nEnviadoB[]\" type=\"hidden\" value=\"");                
                IdChk = IdChk[1].split("\"/>");
                if($.isNumeric(IdChk[0])) {
                    lote = lote + IdChk[0] + ",";
                }
            }
        }
        lote = lote.substring(0,(lote.length - 1));
        $("#progressbar").progressbar({value: (((PgIni + lote.split(",").length) * 100) / PgFim)});
        $("#pgIni").html(PgIni + lote.split(",").length);        
        $.ajax({
            type: "GET", url: "./../Sivis/ws_atualizabanco.php", 
            data: {onLineBoletoDCC: 'S', IdMens: lote}, dataType: "json",
            success: function (data) {
                if (data.MensagemDCC === "") {
                    $("#txtTotalSucesso").val(parseInt($("#txtTotalSucesso").val()) + parseInt(data.TotalSucesso));
                    $("#txtTotalErro").val(parseInt($("#txtTotalErro").val()) + parseInt(data.TotalErro));
                    processarBol(lista);
                } else {
                    bootbox.alert(data.MensagemDCC + "!");
                    refreshTableBol();
                }
            },
            error: function (error) {
                bootbox.alert(error.responseText + "!");
                refreshTableBol();
            }                        
        });        
    } else {
        $("#source").dialog('close');
        bootbox.alert("Transações: " + $("#txtTotalSucesso").val() + " Aprovada(s) e " + $("#txtTotalErro").val() + " com erro(s)!");
        refreshTableBol();
    }
}

function sendEmail() {
    if ($("#txtTipoBol").val() !== "1" && $("#txtTipoBol").val() !== "4") {
        bootbox.alert("Opção válida apenas para os itens GERADOS ou NÃO ENVIADOS!");
    } else {    
        $.ajax({
            type: "GET", url: "./../Academia/form/ClientesPlanosServer.php", 
            data: {getEmailBoleto: 'S'}, dataType: "json",
            success: function (data) {
                bootbox.prompt({
                    title: "Selecione o modelo de E-mail",
                    inputType: 'select',
                    inputOptions: data,
                    callback: function (result) {
                        if (result) {                        
                            $.post(final_url_bol(1)).done(function (data) {
                                var response = jQuery.parseJSON(data);
                                $("#source").dialog({modal: true});
                                $("#progressbar").progressbar({value: 0});
                                $("#pgIni").html("0");
                                $("#pgFim").html(response.aaData.length);
                                $("#txtTotalSucesso, #txtTotalErro").val(0);
                                sendEmailrow(result, response.aaData);
                            });
                        }
                    }
                }); 
            },
            error: function (error) {
                bootbox.alert(error.responseText + "!");
            }                        
        });     
    }
}