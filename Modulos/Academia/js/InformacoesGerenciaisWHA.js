var clickTabWHA = 0;
$(document).ready(function () {
    $("#txtDescWha").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "./../CRM/ajax.php",
                dataType: "json",
                data: {
                    q: request.term,
                    t: "C"
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            $('#txtIdNotaWha').val(ui.item.id).show();
            $('#txtDescWha').val(ui.item.label).show();
        }
    });
    $("#txtDescWha").prop('disabled', true);
    carregaWhatsapp();
    $("#txtTipoClienteWha").change(function () {
        if ($(this).val() === "1") {
            $("#txtDescWha").prop('disabled', false);
        } else {
            $("#txtDescWha").prop('disabled', true);
            $("#txtIdNotaWha").val("");
            $("#txtDescWha").val("");
        }
    });

    $("#ckb_data_pgtoWha").change(function () {
        if ($(this).is(":checked")) {
            $("#txtTipoPgtoWha").prop('disabled', false);
            $("#txtDataIniWha").prop('disabled', false);
            $("#txtDataFinWha").prop('disabled', false);
            $("#txtDataIniWha").val(moment().startOf('month').format(lang["dmy"]));
            $("#txtDataFinWha").val(moment().endOf('month').format(lang["dmy"]));
        } else {
            $("#txtTipoPgtoWha").prop('disabled', true);
            $("#txtDataIniWha").prop('disabled', true);
            $("#txtDataFinWha").prop('disabled', true);
            $("#txtDataIniWha").val("");
            $("#txtDataFinWha").val("");
        }
    });
});

function carregaWhatsapp() {
    if (clickTabWHA === 1) {
        return;
    }
    clickTabWHA = 1;
    $("#loader").show();
    $('#tbListaWHA').dataTable({
        "iDisplayLength": 20,
        "bProcessing": true,
        "bServerSide": true,
        "ordering": false,
        "bFilter": false,
        "bLengthChange": false,
        "sAjaxSource": final_url_wha(0),
        'oLanguage': {
            'oPaginate': {
                'sFirst': "Primeiro",
                'sLast': "Último",
                'sNext': "Próximo",
                'sPrevious': "Anterior"
            }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
            'sLengthMenu': "Visualização de _MENU_ registros",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers",
        "fnDrawCallback": function () {
            $("#loader").hide();
        }
    });
}

function refreshTableWha() {
    $("#loader").show();
    $("#bntInativoWha").attr("disabled", $("#txtTipoWha").val() !== "0");    
    var tblrefresh = $("#tbListaWHA").dataTable();
    tblrefresh.fnReloadAjax(final_url_wha(0));
}

function final_url_wha(imp) {
    return "./../../util/dcc/WHA_server_processing.php?imp=" + imp +
            "&fil=" + $('#txtLojaSel').val() +            
            "&tcl=" + $("#txtTipoClienteWha").val() +
            "&cli=" + $("#txtIdNotaWha").val() +            
            "&dck=" + ($("#ckb_data_pgtoWha").is(':checked') ? 1 : 0) +            
            "&dtp=" + $("#txtTipoPgtoWha").val() +
            "&din=" + $("#txtDataIniWha").val() +
            "&dfn=" + $("#txtDataFinWha").val() +
            "&sts=" + $("#txtTipoWha").val();
}

function printWha(tp) {
    var pRel = "&NomeArq=" + "Transações Whatsapp" +
            "&lbl=" + "Dt.Envio|Mat.|Cliente|Grupo|Telefone|Mensagem|Usuário|Status" +
            "&siz=" + "75|55|180|65|55|150|60|60" +
            "&pdf=" + "0" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + "" + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + final_url_wha(1).replace("./../../util", "./").replace("?", "&"); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tp + "&PathArq=GenericModelPDF.php", '_blank');
}

function ExportarExcelWha() {
    printWha("E");
}

function cancelarWha() {
    bootbox.confirm('Confirma o cancelamento do envio destas transações?', function (result) {
        if (result === true) {
            $.post(final_url_wha(0), {cancelTrans: "S"}).done(function (data) {
                if (data.trim() === "YES") {
                    refreshTableWha();
                } else {
                    bootbox.alert("Erro ao cancelar transações!");
                }
            });
        }
    });
}