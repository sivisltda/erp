
$(document).ready(function () {    
    $('#txtUsuario').select2('disable');
    $("#lblTotalPag").text(parent.$("#lblTotal").text());
    $("#txtDtVencimento, #txtDtPagamento").mask(lang["dateMask"]);
    $("#txtDCCValidade").mask("99/9999");
    $("#txtDCCNumCartao").mask("99999999999999999999");
    startTime();
        
    new jBox('Tooltip', { attach: '.informacao', animation: 'pulse', theme: 'TooltipBorder', addClass: 'tooltipMU', content: ''});    
    $('#txtDCCNumCartao').change(function () {
        if ($("#txtDCCNumCartao").val() !== "") {
            $('#txtDCCNumCartao').validateCreditCard(function (result) {
                $("#txtDCCNumCartao").removeClass();
                if (result.card_type !== null) {
                    $("#txtDCCBandeira").val(result.card_type.code).trigger("change");
                }
            });
        }
    });
        
    $(".selectCr").select2({
        placeholder: "",
        formatResult: function(item) { 
             return '<img src="../../img/cartao/' + item.id + '.png" style="height: 25px;"/>';
        },
        formatSelection: function(item) { 
             return '<img src="../../img/cartao/' + item.id + '.png" style="height: 25px;"/>';
        }
    });
    
    var itensPlano = "";
    var itensCarteira = "";
    var itensOutros = "";
    for (var i = 1; i <= $("#tblItensPag tbody > tr", window.parent.document).length; i++) {
        if ($("#tblItensPag tr:nth-child(" + i + ")", window.parent.document).find("input[name*='txtTipo']").val() === "MENSALIDADE") {
            var item = $("#tblItensPag tr:nth-child(" + i + ")", window.parent.document).find("input[name*='txtIdPlano_']").val();
            itensPlano = itensPlano === "" ? item : itensPlano + "," + item;
        } else if ($("#tblItensPag tr:nth-child(" + i + ")", window.parent.document).find("input[name*='txtTipo']").val() === "CARTEIRA") {
            var item = $("#tblItensPag tr:nth-child(" + i + ")", window.parent.document).find("input[name*='txtIdPlano_']").val();
            itensCarteira = itensCarteira === "" ? item : itensCarteira + "," + item;
        } else {
            var item = $("#tblItensPag tr:nth-child(" + i + ")", window.parent.document).find("input[name*='txtIdItem_']").val();
            itensOutros = itensOutros === "" ? item : itensOutros + "," + item;
        }
    }
    
    if (itensCarteira.length > 0 && (itensPlano.length > 0 || itensOutros.length > 0)) {
        bootbox.alert("Quitação dos itens em carteira não podem ser associados a pagamentos de outros itens!", function() {
            parent.FecharBoxCliente('PAG'); 
        });
    } else if (itensCarteira.length > 0) {        
        $('#telaDcc').val("C");        
    } else if (itensPlano.length > 0) {    
        $.getJSON('form/ClientesPlanosServer.php', {getNumParcelas: itensPlano}, function (j) {
            if (j > 0) {
                $('#txtTipoPag').select2("val", j);
                $('#telaDcc').val("S");
            }
            $("#spinner").val(1);
            gerarParc("");
            atualizar_parcelas();
            showCard();        
            getCartoes();        
        });  
    }
    
    $('#txtAcaoCartao').change(function () {
        acaoCartao();
    });
        
    $('#txtTipoPag').change(function () {
        atualizar_parcelas();
        sistemaTroco();
        showCard();
    });
    
    $('.ui-spinner-button').click(function () {
        sistemaTroco();
    });
    
    $('#txtPagamentoPago').change(function () {
        let valTt = textToNumber($("#txtPagamentoValor").val());
        let valPg = textToNumber($("#txtPagamentoPago").val());
        $("#txtPagamentoTroco").val(numberFormat((valPg - valTt)));        
    });
    
    $("#txtDtPagamento").change(function () {
        var now = moment();
        if ($("#txtDtPagamento").val() !== now.format(lang["dmy"])) {
            bootbox.confirm('Confirma a alteração da Data atual?', function (result) {
                if (result === false) {
                    $("#txtDtPagamento").val(now.format(lang["dmy"]));
                }
                $("#data_click").html($("#txtDtPagamento").val());
                atualizar_parcelas();
            });
        } else {
            $("#data_click").html(now.format(lang["dmy"]));
            atualizar_parcelas();
        }
    });
    
    $("#txtDtPagamento").click(function () {
        $(".ui-datepicker-current").remove();
    });
    
    $.getJSON('form/ClientesPlanosServer.php', {saldoCredito: $("#txtId", window.parent.document).val()}, function (j) {
        if (j !== null) {
            $("#lblTotalDisp").text(j[0].saldo_credito);
            $("#txtTotalDisp").val(j[0].saldo_credito);
        }
    });
    
    gerarParc('');    
});

function getCartoes() {
    $.getJSON('form/ClientesPlanosServer.php', {listCartoes: $("#txtId", window.parent.document).val()}, function (j) {
        var options = "<option value=\"null\">Selecione</option>";
        if (j !== null) {
            if (j.length > 0) {
                for (var i = 0; i < j.length; i++) {
                    options = options + "<option value=\"" + j[i].id_cartao + "\">" + j[i].legenda_cartao + " / Vencimento " + j[i].validade_cartao + "</option>";
                }
            }
            $("#txtCartao").html(options);
        } else {
            $('#txtAcaoCartao').select2("val", 1);
            acaoCartao();            
        }
        $('#txtCartao').select2().select2('val', $('#txtCartao option:eq(1)').val());
    });
}

function acaoCartao() {
    if ($('#txtAcaoCartao').val() === "0") {
        $("#divCartao").show();
        $("#divCartaoDados").hide();
    } else {
        $("#divCartao").hide();
        $("#divCartaoDados").show();
    }
}

function atualizar_parcelas() {
    if ($('#telaDcc').val() === "S") {
        $("#tblParcelas").find("select[name*='txtParcelaT']").val($("#txtTipoPag").val());
        if ($("#txtTipoPag option:selected").attr('cartao_dcc') === "0") {
            bootbox.alert("Tipo de Pagamento escolhido diferente de DCC!");
        }
    }
    var data = $("#txtDtPagamento").val();
    if ($('#txtTipoPag option:selected').attr("dias") > 0) {
        data = moment(data, lang["dmy"]).add($('#txtTipoPag option:selected').attr("dias"), 'days').format(lang["dmy"]);
        $("#txtDtVencimento").attr('disabled', true);        
    } else {
        $("#txtDtVencimento").attr('disabled', false);
    }
    $("#txtDtVencimento").val(data);
    gerarParc('B');
}

function linhaCampos(ln, tipo) {
    var tbody = "";
    $.ajax({url: "../../Modulos/Estoque/campos.ajax.php",
        data: {txtCampos: tipo, ajax: 'true', async: false},
        async: false, dataType: "json",
        success: function (j) {
            if (j.length > 0) {
                var size = parseInt(100 / j.length);
                for (var i = 0; i < j.length; i++) {
                    let content = ($("#txtCampo" + j[i].id_tipo_documento_campos + "_1").length > 0 ? $("#txtCampo" + j[i].id_tipo_documento_campos + "_1").val() : "");
                    tbody += "<input id=\"txtCampo" + j[i].id_tipo_documento_campos + "_" + ln + "\" name=\"txtCampo" + j[i].id_tipo_documento_campos + "_" + ln + "\" style=\"width:" + size + "%;\" title=\"" + j[i].campo + "\" placeholder=\"" + j[i].campo + "\" type=\"text\" class=\"input-xlarge\" value='" + content + "'/>";
                }
            }
        }
    });
    return tbody;
}

function linhaCheque(i) {
    var tbody = "";
    var txtbanco = "", txtagencia = "", txtconta = "", txtnumero = "", txtprop = "";
    if ($('#txtChBanco_1').length) {
        txtbanco = $("#txtChBanco_1").val();
        txtagencia = $("#txtChAgencia_1").val();
        txtconta = $("#txtChConta_1").val();
        txtnumero = $("#txtChNumero_1").val();
        txtprop = $("#txtChProp_1").val();
    }
    if (i !== 1 && $('#txtChBanco_1').length) {
        txtnumero = $("#txtChNumero_1").val() !== "" ? parseInt($("#txtChNumero_1").val()) + (i - 1) : '';
    }
    tbody += "<input id=\"txtChBanco_" + i + "\" name=\"txtChBanco_" + i + "\" style=\"width:20%;\" title=\"Banco\" placeholder=\"Banco\" type=\"text\" class=\"input-xlarge\" value='" + txtbanco + "' />" +
            "<input id=\"txtChAgencia_" + i + "\" name=\"txtChAgencia_" + i + "\" style=\"width:20%;\" title=\"Agência\" placeholder=\"Agência\" type=\"text\" class=\"input-xlarge\" value='" + txtagencia + "'/>" +
            "<input id=\"txtChConta_" + i + "\" name=\"txtChConta_" + i + "\" style=\"width:20%;\" title=\"Conta\" placeholder=\"Conta\" type=\"text\" class=\"input-xlarge\" value='" + txtconta + "'/>" +
            "<input id=\"txtChNumero_" + i + "\" name=\"txtChNumero_" + i + "\" style=\"width:20%;\" title=\"Número\" placeholder=\"Número\" type=\"text\" class=\"input-xlarge\" value='" + txtnumero + "'/>" +
            "<input id=\"txtChProp_" + i + "\" name=\"txtChProp_" + i + "\" style=\"width:20%;\" title=\"Propr.\" placeholder=\"Propr.\" type=\"text\" class=\"input-xlarge\" value='" + txtprop + "'/>";
    return tbody;
}

function gerarParc(tipo) {
    var date = $("#txtDtVencimento").val();
    var qtde = parseInt($("#spinner").val());
    if ((date === "" || (qtde <= 0 || $("#spinner").val() === "")) && tipo === "B") {
        bootbox.alert("Vencimento e quantidade de parcelas é obrigatorio!");
        return;
        $("#btnPag").attr("disabled", true);
    } else {
        var val = textToNumber($("#lblTotalPag").text());
        if (date !== "" && qtde >= 0 && val >= 0) {
            var tbody = "";
            var campos = linhaCampos("|", $('#txtTipoPag').val());
            var parc = val / qtde;
            for (var i = 1; i <= qtde; i++) {
                var dtVenc = moment(date, lang["dmy"]).add(i - 1, 'month').format(lang["dmy"]);
                tbody += geraLinhaParcela(i, dtVenc, parc, ($('#txtTipoPag').val() !== "5" ? campos.replace(/\|/g, i) : linhaCheque(i)));
            }
            $("#tblParcelas tbody > tr").remove();
            $("#tblParcelas tbody").append(tbody);
            $("#tblParcelas").find("select[name*='txtParcelaT']").val($("#txtTipoPag").val());
            $("#tblParcelas").find("input[name*='txtParcelaD_']").mask(lang["dateMask"]);
            $("#tblParcelas").find("input[name*='txtParcelaV_']").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
            $("#btnPag").attr("disabled", false);
            if ($('#telaDcc').val() === "S") {
                $("#tblParcelas :input").attr("readonly", true);
            }
            changeParc(1,1);
        }
    }
}

function geraLinhaParcela(i, dtVenc, parc, campos) { // gerar as linha
    var tlinha = "";
    tlinha += "<tr id='linhaParcela_" + i + "'><td valign='middle'><center><span style='margin-top:8px; display:block;width: 37px;'>" + i + "</span></center></td>";
    tlinha += "<td>" + campos + "</td>";
    tlinha += "<td style='width:97px;'><input style='text-align: center;width:100%' class='datepicker inputDireito' name='txtParcelaD_" + i + "' id='txtParcelaD_" + i + "' type='text' value='" + dtVenc + "' /></td>";
    tlinha += "<td style='width:75px;'><input style='text-align: center;width:100%' class='input-xxlarge inputDireito' name='txtParcelaV_" + i + "' id='txtParcelaV_" + i + "' onBlur='changeParc(" + i + ",0)' type='text' value='" + numberFormat(parc) + "' /></td>";
    tlinha += "<td><select name='txtParcelaTipo_" + i + "' id='txtParcelaTipo_" + i + "' style='width:100%;' onchange='changeParc(" + i + ",1);'>>" + $('#txtTipoPag').html() + "</select> </td>";
    tlinha += "</tr>";
    return tlinha;
}

function changeParc(obj, alt) {
    if ($('#telaDcc').val() !== "S") {
        recalcular(obj);
        var campos = linhaCampos("|", $('#txtParcelaTipo_' + obj).val());
        $("#linhaParcela_" + obj + " td:nth-child(2)").html(($('#txtParcelaTipo_' + obj).val() !== "5" ? campos.replace(/\|/g, obj) : linhaCheque(obj)));
        ajustesDatas(alt);
    } else {        
        var valorParcelas = 0.00;
        for (var i = 1; i <= $('#tblParcelas >tbody >tr').length; i++) {
            valorParcelas = valorParcelas + textToNumber($("#txtParcelaV_" + i).val());
        }
        $("#lblTotalPag").text(numberFormat(valorParcelas));
    }
}

function recalcular(idLinha) {
    var valorTotal = textToNumber($("#lblTotalPag").text());
    var valorAlterar = textToNumber($("#txtParcelaV_" + idLinha).val());
    var valorSemAlterar = 0.00;
    var linhas = parseInt($('#tblParcelas >tbody >tr').length);
    for (var i = 1; i <= linhas; i++) {
        if (idLinha !== i) {
            valorSemAlterar = valorSemAlterar + textToNumber($("#txtParcelaV_" + i).val());
        }
    }
    let valorDiferenca = (valorTotal - (valorSemAlterar + valorAlterar));
    let p_rest = (linhas - idLinha);    
    if (valorDiferenca !== 0.00 && idLinha !== linhas && p_rest > 0) {
        let valorDiferencaParc = valorDiferenca / p_rest;
        if (valorDiferencaParc < 0.01) {
            $("#txtParcelaV_" + linhas).val(numberFormat(textToNumber($("#txtParcelaV_" + linhas).val()) + valorDiferenca));
        } else {
            for (var i = (idLinha + 1); i <= linhas; i++) {
                $("#txtParcelaV_" + i).val(numberFormat(textToNumber($("#txtParcelaV_" + i).val()) + valorDiferencaParc));
            }
        }
    } else if (valorDiferenca !== 0.00 && idLinha !== 1) {
        $("#txtParcelaV_1").val(numberFormat(textToNumber($("#txtParcelaV_1").val()) + valorDiferenca));
    }
}

function ajustesDatas(alt) {
    if(alt > 0) {
        var tipos = [];
        for (var i = 1; i <= $('#tblParcelas >tbody >tr').length; i++) {
            if ($.inArray($("#txtParcelaTipo_" + (i)).val(), tipos) === -1) {
                tipos.push($("#txtParcelaTipo_" + (i)).val());
            }
        }
        for (var i = 0; i < tipos.length; i++) {
            var data = "";
            for (var j = 1; j <= $('#tblParcelas >tbody >tr').length; j++) {
                $("#txtParcelaD_" + j + ", #txtParcelaV_" + j + ", #txtParcelaTipo_"+ j).attr('readonly', ($('#txtParcelaTipo_' + j + ' option:selected').attr("dias") > 0 ? true : false));
                if (tipos[i] === $("#txtParcelaTipo_" + j).val()) {                
                    if (data === "") {                                    
                        if ($('#txtParcelaTipo_' + j + ' option:selected').attr("dias") > 0) {
                            data = $("#txtDtPagamento").val(); 
                            data = moment(data, lang["dmy"]).add($('#txtParcelaTipo_' + j + ' option:selected').attr("dias"), 'days').format(lang["dmy"]);
                        } else {
                            data = $("#txtDtVencimento").val();                    
                        }
                    } else {
                        data = moment(data, lang["dmy"]).add(1, 'month').format(lang["dmy"]);
                    }
                    $("#txtParcelaD_" + j).val(data);                
                }
            }
        }
    }
}

function salvaPagCliente() {
    if ($("#txtTipoPag").val() === "6" && $("#spinner").val() === "1") {
        gerarParc('B');
    }
    var date = $("#txtDtVencimento").val();
    var qtde = parseInt($("#spinner").val());
    var valorParcelas = 0.00;
    var tpNaoParc = [];
    var formaPag = true;
    for (var i = 1; i <= $('#tblParcelas >tbody >tr').length; i++) {
        valorParcelas = valorParcelas + textToNumber($("#txtParcelaV_" + i).val());
        if ($("#txtParcelaTipo_" + (i)).val() === "null") {
            formaPag = false;
        } else if (!moment($("#txtParcelaD_" + i).val(), 'DD/MM/YYYY', true).isValid()) {
            bootbox.alert("Vencimento " + $("#txtParcelaD_" + i).val() + " inválido!");
            return;
        } else if (textToNumber($("#txtParcelaV_" + i).val()) <= 0.00) {
            bootbox.alert("Valor " + $("#txtParcelaV_" + i).val() + " inválido!");
            return;
        } else {
            if ($("#txtParcelaTipo_" + (i) + " option:selected").attr('data-parc') === "0") {
                if (tpNaoParc.indexOf($("#txtParcelaTipo_" + (i) + " option:selected").val()) > -1) {
                    bootbox.alert("Forma de pagamento escolhida inválida (Parcelamento)!");
                    return;
                } else {
                    tpNaoParc.push($("#txtParcelaTipo_" + (i) + " option:selected").val());
                }
            }
            if ($("#txtParcelaTipo_" + (i) + " option:selected").attr('id_baixa') === "0" && $("#txtBanco").val() === "null") {
                bootbox.alert("Banco é obrigatorio!");
                return;
            }
            if ($("#txtAcaoCartao").val() === "0" && $("#txtCartao").val() === "null" && $.inArray($("#txtTipoPag option:selected").attr('cartao_dcc'), ["1", "2"]) >=0) {
                bootbox.alert("Cartão é obrigatorio!");
                return;
            }
        }
    }
    if (textToNumber($("#lblTotalPag").text()) !== textToNumber(numberFormat(valorParcelas))) {
        bootbox.alert("Valor total é diferente do total em parcelas!");
        return;
    } else if (!formaPag || date === "" || (qtde <= 0 || $("#spinner").val() === "")) {
        bootbox.alert("Forma de pagamento, vencimento e quantidade de parcelas é obrigatorio!");
        return;
        $("#btnPag").attr("disabled", true);
    } else if ($.inArray($("#txtAcaoCartao").val(), ["1", "2"]) >=0 && 
        $("#dcc_tef").val() === "1" && ($.inArray($("#txtTipoPag option:selected").attr('cartao_dcc'), ["1", "2"]) >=0 && 
        ($('#txtDCCNmCartao').val() === "" || $('#txtDCCNumCartao').val() === "" || $('#txtDCCCodSeg').val() === "" || $('#txtDCCValidade').val() === "" || $('#txtTipo').val() === ""))) {
        bootbox.alert("Os dados de pagamento para cartão são obrigatórios!");
        return;        
    } else {
        $("#loader", window.parent.document).show();
        if ($("#txtAcaoCartao").val() === "0" && $.inArray($("#txtTipoPag option:selected").attr('cartao_dcc'), ["1", "2"]) >=0) {
            let total = 0.00;
            let parcelas = $('[id^=txtParcelaTipo_]').filter(function (i, value) {
                return $(value).val() === $("#txtTipoPag").val();
            });            
            parcelas.each(function(i, value) {
                total += textToNumber($("#" + $(value).attr("id").replace("txtParcelaTipo_", "txtParcelaV_")).val());
            });  
            $.post("../../../util/dcc/makeDCC.php", "btnSalvar=S" +
                    "&txtCartao=" + $("#txtCartao").val() +
                    "&txtPlanoItem=" + $("#txtIdPlano_1", window.parent.document).val() +                    
                    "&txtValor=" + numberFormat(total) +
                    "&txtParcelas=" + parcelas.length +
                    "&txtTipoPag=" + $("#txtTipoPag").val() +
                    "&txtId=" + $("#txtId", window.parent.document).val()).done(function (data) {
                if (!isNaN(data.trim()) && $.isNumeric(data.trim())) {
                    geraVenda(data.trim());
                } else {
                    bootbox.alert("Atenção ao Pagamento! " + "<br/>" + data.trim().replace("DECLINED", "NEGADA"));
                    $("#loader", window.parent.document).hide();
                }
            });
        } else if ($.inArray($("#txtAcaoCartao").val(), ["1", "2"]) >=0 && $("#dcc_tef").val() === "1" && $.inArray($("#txtTipoPag option:selected").attr('cartao_dcc'), ["1", "2"]) >=0) {
            let total = 0.00;
            let parcelas = $('[id^=txtParcelaTipo_]').filter(function (i, value) {
                return $(value).val() === $("#txtTipoPag").val();
            });            
            parcelas.each(function(i, value) {
                total += textToNumber($("#" + $(value).attr("id").replace("txtParcelaTipo_", "txtParcelaV_")).val());
            });            
            $.post("../../../util/dcc/makeDCC.php", "btnSalvar=S" + 
                    "&txtPlanoItem=" + $("#txtIdPlano_1", window.parent.document).val() +
                    "&txtCartaoNome=" + $("#txtDCCNmCartao").val() +
                    "&txtCartaoNumero=" + $("#txtDCCNumCartao").val() +
                    "&txtCartaoCodSeg=" + $("#txtDCCCodSeg").val() +
                    "&txtCartaoValidade=" + $("#txtDCCValidade").val() +
                    "&txtCartaoBandeira=" + $("#txtDCCBandeira").val() +
                    "&txtCartaoTipo=" + $("#txtTipo").val() +                    
                    "&txtTpPayment=" + $("#txtTipoPag option:selected").attr('cartao_dcc') +                       
                    "&txtValor=" + numberFormat(total) +
                    "&txtParcelas=" + parcelas.length +
                    "&txtTipoPag=" + $("#txtTipoPag").val() +                    
                    "&txtSaveCard=" + $("#txtAcaoCartao").val() +
                    "&txtId=" + $("#txtId", window.parent.document).val()).done(function (data) {
                if (!isNaN(data.trim()) && $.isNumeric(data.trim())) {
                    geraVenda(data.trim());
                } else {
                    bootbox.alert("Atenção ao Pagamento! " + "<br/>" + data.trim().replace("DECLINED", "NEGADA"));
                    $("#loader", window.parent.document).hide();
                }
            });           
        } else {
            geraVenda(0);
        }
    }
}

function geraVenda(idTransacao) {
    var isCredito = "N";
    if ($("#tblItensPag tr", window.parent.document).find('td:first').text().trim() === "CREDITO") {
        isCredito = "S";
    }
    $.post("form/ClientesPlanosServer.php", "salvaPagPlano=S&btnSave=S&txtIdCli=" + $("#txtId", window.parent.document).val() +
            "&system=" + parent.$('#system').val() +
            "&idTransacao=" + idTransacao +
            "&txtEmpresa=" + $("#txtLojaSel", window.parent.document).val() +
            "&txtTipoPag=" + $("#txtTipoPag").val() +
            "&txtQtdItens=" + $("#txtQtdItensPagar", window.parent.document).val() +
            "&txtQtdParc=" + $('#tblParcelas >tbody >tr').length +
            "&txtBanco=" + $("#txtBanco").val() +
            "&isCredito=" + isCredito +
            "&isDcc=" + $('#telaDcc').val() +
            "&desct=" + JSON.stringify(parent.descontos) +
            "&dcanc=" + JSON.stringify(parent.cancelamentos) +
            "&txtDtPag=" + $('#txtDtPagamento').val() +
            "&" + $("#tblParcelas :input").serialize() +
            "&" + $("#tblItensPag :input", window.parent.document).serialize()).done(function (data) {
        if (!isNaN(data.trim())) {
            atualizaStatusCli(1, data.trim());
        } else {
            bootbox.alert("Erro ao efetuar pagamento!" + "<br/>" + data.trim());
        }
    });
}

function atualizaStatusCli(tp, id_pg) {
    $.post("ajax/Clientes_Status_serve_processing.php", {idCli: $("#txtId", window.parent.document).val()}).done(function (data) {
        parent.$("#statusCli .button").attr("class", " ");
        parent.$("#statusCli div").first().addClass("button");
        parent.$(".name center").html(data.toUpperCase());
        if (data.trim().toUpperCase() === "SUSPENSO") {
            parent.$("#statusCli div").first().addClass("label-Suspenso");            
        } else if (data.trim().toUpperCase() === "CANCELADO") {
            parent.$("#statusCli div").first().addClass("label-Cancelado");
        } else if (data.trim().toUpperCase() === "INATIVO") {
            parent.$("#statusCli div").first().addClass("label-Inativo");
        } else if (data.trim().toUpperCase() === "ATIVOEMABERTO") {
            parent.$("#statusCli div").first().addClass("label-AtivoEmAberto");
            parent.$(".name center").html("ATIVO EM ABERTO");            
        } else if (data.trim().toUpperCase() === "ATIVOABONO") {
            parent.$("#statusCli div").first().addClass("label-AtivoAbono");
            parent.$(".name center").html("ATIVO ABONO");            
        } else if (data.trim().toUpperCase().substring(0, 5) === "ATIVO") {
            parent.$("#statusCli div").first().addClass("label-Ativo");
        } else if (data.trim().toUpperCase() === "FERIAS") {
            parent.$("#statusCli div").first().addClass("label-Ferias");
            parent.$(".name center").html("EM FÉRIAS");
        } else if (data.trim().toUpperCase() === "DEPENDENTE") {
            parent.$("#statusCli div").first().addClass("label-Dependente");
            parent.$(".name center").html("DEPENDENTE");
        } else if (data.trim().toUpperCase() === "DESLIGADO") {
            parent.$("#statusCli div").first().addClass("label-Desligado");
            parent.$(".name center").html("DESLIGADO");
        } else if (data.trim().toUpperCase() === "DESLIGADOEMABERTO") {
            parent.$("#statusCli div").first().addClass("label-DesligadoEmAberto");
            parent.$(".name center").html("DESLIGADO EM ABERTO");
        }
        if (tp === 1) {
            $("#loader", window.parent.document).hide();
            bootbox.confirm('Pagamento efetuado com sucesso! Imprimir Recibo?', function (result) {
                var id = 0;
                if (result === true) {
                    id = id_pg;
                }
                parent.limpaItensPag();
                parent.atualizaTotCredito();
                parent.listExtratoCredito();
                parent.AtualizaListPlanosClientes();
                parent.atualizaCredenciais();
                parent.FecharBoxPagamento(id);
            });
        }
    });
}

function pegaValorAvulso() {
    var totlinha = parent.$("#tblPagamentosPlano tr").length;
    var ultimaLinha = parent.$("#tblPagamentosPlano tr")[totlinha - 2];
    return ultimaLinha.innerHTML.split('</td>')[1].substring(4);
}

function preencheZeros(valor, valor2, tamanho) {
    var qtd = parseInt(valor) + parseInt(valor2);
    if (qtd.toString().length < tamanho.length) {
        var limite = tamanho.length - qtd.toString().length;
        for (i = 0; i < limite; i++) {
            qtd = '0' + qtd;
        }
    }
    return qtd;
}

function startTime() {
    var myDay = new Date();
    if (myDay.getHours() < 10) {
        var h = "0" + myDay.getHours();
    } else {
        var h = myDay.getHours();
    }
    if (myDay.getMinutes() < 10) {
        var m = "0" + myDay.getMinutes();
    } else {
        var m = myDay.getMinutes();
    }
    if (myDay.getSeconds() < 10) {
        var s = "0" + myDay.getSeconds();
    } else {
        var s = myDay.getSeconds();
    }
    document.getElementById("time").innerHTML = h + ":" + m + ":" + s;
    t = setTimeout(function () {
        startTime();
    }, 1000);
}

function sistemaTroco() {
    if ($("#txtTipoPag").val() === "6" && $("#spinner").val() === "1") {
        var val = textToNumber($("#lblTotalPag").text());
        $("#txtPagamentoValor, #txtPagamentoPago, #txtPagamentoTroco").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});        
        $("#txtPagamentoValor, #txtPagamentoPago").val(numberFormat(val));
        $("#tblParcelasDinheiro").show();
        $("#tblParcelas").hide();
    } else {
        $("#tblParcelasDinheiro").hide();
        $("#tblParcelas").show();
    }
}

function showCard() {
    if ($("#dcc_tef").val() === "1" && $.inArray($("#txtTipoPag option:selected").attr('cartao_dcc'), ["1", "2"]) >=0 && !$("#divCartao").is(":visible")) {
        $("#mnuCartao").show();
        $("#tblParcelas").parent().parent().height(235);
    } else {        
        $("#mnuCartao").hide();
        $("#tblParcelas").parent().parent().height(283);
    }
}