$("#txtDtIni, #txtDtFim, #txtCredDtIni, #txtCredDtFim, #txtDocDtIni").mask(lang["dateMask"]);

var tblAcesso;

function atualizaStatusCli() {
    $.post("../Academia/ajax/Clientes_Status_serve_processing.php", {idCli: $("#txtId", window.parent.document).val()}).done(function (data) {
        $("#statusCli .button").attr("class", " ");
        $("#statusCli div").first().addClass("button");
        $(".name center").html(data.toUpperCase());
        if (data.trim().toUpperCase() === "SUSPENSO") {
            $("#statusCli div").first().addClass("label-Suspenso");
        } else if (data.trim().toUpperCase() === "CANCELADO") {
            $("#statusCli div").first().addClass("label-Cancelado");
        } else if (data.trim().toUpperCase() === "INATIVO") {
            $("#statusCli div").first().addClass("label-Inativo");
        } else if (data.trim().toUpperCase() === "ATIVOEMABERTO") {
            $("#statusCli div").first().addClass("label-AtivoEmAberto");    
            $(".name center").html("ATIVO EM ABERTO");              
        } else if (data.trim().toUpperCase() === "ATIVOABONO") {
            $("#statusCli div").first().addClass("label-AtivoAbono");    
            $(".name center").html("ATIVO ABONO");              
        } else if (data.trim().toUpperCase().substring(0, 5) === "ATIVO") {
            $("#statusCli div").first().addClass("label-Ativo");
        } else if (data.trim().toUpperCase() === "FERIAS") {
            $("#statusCli div").first().addClass("label-Ferias");
            $(".name center").html("EM FÉRIAS");
        } else if (data.trim().toUpperCase() === "DEPENDENTE") {
            $("#statusCli div").first().addClass("label-Dependente");
            $(".name center").html("DEPENDENTE");            
        } else if (data.trim().toUpperCase() === "DESLIGADO") {
            $("#statusCli div").first().addClass("label-Desligado");
            $(".name center").html("DESLIGADO");            
        } else if (data.trim().toUpperCase() === "DESLIGADOEMABERTO") {
            $("#statusCli div").first().addClass("label-DesligadoEmAberto");
            $(".name center").html("DESLIGADO EM ABERTO");            
        }
    });
    refreshCredPagamento();
}

function StatusCliEspecific(id) {
    $.post("../Academia/ajax/Clientes_Status_serve_processing.php", 
    {idCli: id}).done(
            function (data) {
    });
}

if ($('#txtId').val() !== "") {
    listCredenciais();
    refreshCredPagamento();    
    listBloqueios();
    listDocumentos();
    chartData("T");
    chartData("D");
    trataMsgAcesso();
    listAllDocs();
    carregaAcessosGraf();
}

function carregaAcessosGraf(){
  tblAcesso = $('#tblAcesso').dataTable({
      "iDisplayLength": 15,
      "aLengthMenu": [20, 30, 40, 50, 100],
      "bProcessing": true,
      "bServerSide": true,
      "sAjaxSource": "../Academia/ajax/Clientes_acessos_server_processing.php?id=" + $("#txtId").val() + "&dti=" + $("#txtDtIni").val().replace(/\//g, "_") + "&dtf=" + $("#txtDtFim").val().replace(/\//g, "_"),
      "bFilter": false,
      "bSort": false,
      "scrollY": "209px",
      "scrollCollapse": true,
      "paging": false,
      'oLanguage': {
          'oPaginate': {
              'sFirst': "Primeiro",
              'sLast': "Último",
              'sNext': "Próximo",
              'sPrevious': "Anterior"
          }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
          'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
          'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
          'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
          'sLengthMenu': "Visualização de _MENU_ registros",
          'sLoadingRecords': "Carregando...",
          'sProcessing': "Processando...",
          'sSearch': "Pesquisar:",
          'sZeroRecords': "Não foi encontrado nenhum resultado"},
      "sPaginationType": "full_numbers"
  });
}

$('#btnIncluirCredencial').click(function (e) {
    var data1 = moment($("#txtCredDtIni").val(), lang["dmy"]);
    var data2 = moment($("#txtCredDtFim").val(), lang["dmy"]);
    if ($("#txtCredModelo").val() === "" || $("#txtCredDtIni").val() === "" || $("#txtCredDtFim").val() === "" || $("#txtCredMotivo").val() === "") {
        bootbox.alert("Todos os campos são obrigatórios!");
    } else if (data2 < data1) {
        bootbox.alert("Periodo inválido!");
    } else if ($("#txtCredPagamento").length > 0 && $("#txtCredPagamento").val() === "null" && parseInt($("#txtCredPagamento option:selected").attr("usado")) >= parseInt($("#txtCredPagamento option:selected").attr("limite"))) {
        bootbox.alert("Limite de Credenciais Gratuitas!");
    } else {
        $.post("../Academia/form/ClientesCredenciaisServer.php", "AddCredencial=S&txtId=" + $("#txtId").val() + "&txtCredModelo=" + $("#txtCredModelo").val() + 
                "&txtCredDtIni=" + $("#txtCredDtIni").val().replace("/", "_") + "&txtCredDtFim=" + $("#txtCredDtFim").val().replace("/", "_") + 
                "&txtCredMotivo=" + $("#txtCredMotivo").val() + "&txtCredPagamento=" + $("#txtCredPagamento").val()).done(function (data) {
            var tblCredenciais = $('#tblCredenciais').dataTable();
            tblCredenciais.fnReloadAjax("../Academia/form/ClientesCredenciaisServer.php?listCredencial=" + $("#txtId").val());
            $("#txtCredDtIni, #txtCredDtFim, #txtCredMotivo").val("");
            $("#txtCredModelo").select2("val", "");
            atualizaStatusCli();            
        });
    }
});

function RemoverCredencial(id) {
    $.post("../Academia/form/ClientesCredenciaisServer.php", "DelCredencial=" + id).done(function (data) {
        if (data.trim() === "YES") {
            atualizaStatusCli();
            var tblCredenciais = $('#tblCredenciais').dataTable();
            tblCredenciais.fnReloadAjax("../Academia/form/ClientesCredenciaisServer.php?listCredencial=" + $("#txtId").val());
        } else {
            bootbox.alert("Erro ao efetuar o cancelamento!");
        }
    });
}

function refreshCredPagamento() {
    if ($("#txtCredPagamento").length > 0) {
        $.getJSON("form/ClientesCredenciaisServer.php", {getCredPagamento: "S", idTitular: $("#txtIdTitular").val(), txtId: $("#txtId").val()}, function (j) {
            if (j !== null && j.length > 0) {
                let options = "";
                for (var i = 0; i < j.length; i++) {
                    options = options + "<option value=\"" + j[i].value + "\" usado=\"" + j[i].usado + "\" limite=\"" + j[i].limite + "\">" + j[i].texto + "</option>";
                }
                $("#txtCredPagamento").html(options);
                $("#txtCredPagamento").select2("val", "null");                    
            }
        });            
    }
}

function listCredenciais() {
    $('#tblCredenciais').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "../Academia/form/ClientesCredenciaisServer.php?listCredencial=" + $("#txtId").val(),
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });
}

$('#btnIncluirBloqueios').click(function (e) {
    var data1 = moment($("#txtBlqDtIni").val(), lang["dmy"]);
    var data2 = moment($("#txtBlqDtFim").val(), lang["dmy"]);
    if ($("#txtBlqDtIni").val() === "" || $("#txtBlqDtFim").val() === "" || $("#txtBlqMotivo").val() === "") {
        bootbox.alert("Todos os campos são obrigatórios!");
    } else if (data2 < data1) {
        bootbox.alert("Periodo inválido!");
    } else {
        $.post("../Academia/form/ClientesBloqueioServe.php", "AddBloqueio=S&txtId=" + $("#txtId").val() +
                "&txtBlqDtIni=" + $("#txtBlqDtIni").val().replace("/", "_") +
                "&txtBlqDtFim=" + $("#txtBlqDtFim").val().replace("/", "_") +
                "&txtBlqMotivo=" + $("#txtBlqMotivo").val()).done(function (data) {
            var tblBloqueios = $('#tblBloqueios').dataTable();
            tblBloqueios.fnReloadAjax("../Academia/form/ClientesBloqueioServe.php?listBloqueio=" + $("#txtId").val());
            $("#txtBlqDtIni, #txtBlqDtFim, #txtBlqMotivo").val("");
            atualizaStatusCli();
        });
    }
});

function RemoverBloqueio(id) {
    $.post("../Academia/form/ClientesBloqueioServe.php", "DelBloqueio=" + id).done(function (data) {
        if (data.trim() === "YES") {
            atualizaStatusCli();
            var tblBloqueios = $('#tblBloqueios').dataTable();
            tblBloqueios.fnReloadAjax("../Academia/form/ClientesBloqueioServe.php?listBloqueio=" + $("#txtId").val());
        } else {
            bootbox.alert("Erro ao efetuar o cancelamento!");
        }
    });
}

function listBloqueios() {
    $('#tblBloqueios').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "../Academia/form/ClientesBloqueioServe.php?listBloqueio=" + $("#txtId").val(),
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });
}

function listDocumentos() {
    $('#tblDocumentos').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "../Academia/ajax/Clientes_docs_server_processing.php?op=r&id_forn=" + $("#txtId").val(),
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        "aoColumns": [
            {'sWidth': "22.5%"},
            {'sWidth': "22.5%"},
            {'sWidth': "22.5%"},
            {'sWidth': "22.5%"},
            {'sWidth': "10%"}
        ],
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });
}

function trataMsgAcesso() {    
    if ($('input[name="rAcTipo"]:checked').val() === "1") {
        $("#textoProv").hide();
        $("#txtAcNumProvisorio").attr("disabled", true);
    } else {
        $("#textoProv").show();
        $("#txtAcNumProvisorio").attr("disabled", false);
    }
}

$('input[name="rAcTipo"]').click(function (e) {
    if ($('input[name="rAcTipo"]:checked').val() === "1") {
        $("#txtAcNumProvisorio").val("");
    }
    trataMsgAcesso();
});

$('#btnSalvarAcesso').click(function (e) {
    if ($.isNumeric($("#txtAcNumProvisorio").val()) || $("#txtAcNumProvisorio").val() === "") {
        $.post("../Academia/form/ClientesFormServer.php", "isAjax=S&SalvarAcesso=S"
                + "&txtId=" + $("#txtId").val()
                + "&rAcTipo=" + $('input[name="rAcTipo"]:checked').val()
                + "&txtAcNumProvisorio=" + $("#txtAcNumProvisorio").val()
                + "&txtAcMsg=" + $("#txtAcMsg").val()
                + "&ckbEmailMarketing=" + ($('#ckb_excecoes_digitais').is(":checked") ? "1" : "0")
                ).done(function (data) {
            if (data.trim() === "YES") {
                bootbox.alert("Alteração efetuada com sucesso!");
            } else {
                bootbox.alert(data.trim() + "!");
            }
        });
    } else {
        bootbox.alert("O Campo Número Provisório deve ser numérico!");        
    }
});

function chartData(tipo) {
    if (tipo === 'T') {
        $.getJSON("../Academia/ajax/Clientes_acessos_server_processing.php?tipo=T&id=" + $("#txtId").val() + "&dti=" + $("#txtDtIni").val().replace(/\//g, "_") + "&dtf=" + $("#txtDtFim").val().replace(/\//g, "_"), function (data) {
            chartTurno.dataProvider = data;
            chartTurno.validateData();
        });
    } else if (tipo === 'D') {
        $.getJSON("../Academia/ajax/Clientes_acessos_server_processing.php?tipo=D&id=" + $("#txtId").val() + "&dti=" + $("#txtDtIni").val().replace(/\//g, "_") + "&dtf=" + $("#txtDtFim").val().replace(/\//g, "_"), function (data) {
            var status = 0;
            if (data !== null) {
                for (var x = 0; x < data.length; x++) {
                    if (data[x].value !== 0) {
                        status = 1;
                    }
                }
            }
            if (status === 1) {
                chartDiario.dataProvider = data;
                chartDiario.validateData();
            } else {
                $("#chartDiario").hide();
            }
        });
    }
}

var chartTurno = AmCharts.makeChart("chartTurno", {
    "type": "pie",
    "theme": "light",
    autoMargins: false,
    marginTop: 0,
    marginBottom: 0,
    marginLeft: 0,
    marginRight: 0,
    pullOutRadius: 0,
    "valueField": "acesso",
    "titleField": "turno",
    "labelText": "[[percents]]%",
    "balloon": {
        "fixedPosition": true
    },
    "export": {
        "enabled": true
    }
});

var chartDiario = AmCharts.makeChart("chartDiario", {
    "type": "serial",
    "theme": "light",
    "marginRight": 40,
    "marginLeft": 40,
    "autoMarginOffset": 20,
    "valueAxes": [{
            "id": "v1",
            "axisAlpha": 0,
            "position": "left",
            "ignoreAxisWidth": true
        }],
    "balloon": {
        "borderThickness": 1,
        "shadowAlpha": 0
    },
    "graphs": [{
            "id": "g1",
            "balloon": {
                "drop": true,
                "adjustBorderColor": false,
                "color": "#ffffff"
            },
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "bulletSize": 5,
            "hideBulletsCount": 50,
            "lineThickness": 2,
            "useLineColorForBulletBorder": true,
            "valueField": "value",
            "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
        }],
    "categoryField": "date",
    "categoryAxis": {
        "dashLength": 1,
        "minorGridEnabled": true,
        "labelRotation": 45
    },
    "export": {
        "enabled": true
    }
});

function listAcesso() {
    var data1 = moment($("#txtDtIni").val(), lang["dmy"]);
    var data2 = moment($("#txtDtFim").val(), lang["dmy"]);
    var diferenca = data2.diff(data1, 'months');
    if (diferenca > 12) {
        bootbox.alert("Intervalo não pode ser mair que um ano!");
    } else if (data2 < data1) {
        bootbox.alert("Periodo inválido!");
    } else {
        tblAcesso.fnReloadAjax("../Academia/ajax/Clientes_acessos_server_processing.php?id=" + $("#txtId").val() + "&dti=" + $("#txtDtIni").val().replace(/\//g, "_") + "&dtf=" + $("#txtDtFim").val().replace(/\//g, "_"));
        chartData("T");
        chartData("D");
    }
}

$("#btnIncluirDocumento").click(function (event) {
    var erro = "";
    if ($("#txtDocModelo").val() === "") {
        erro = 'Selecione um documento!';
    } else if ($("#txtDocDtIni").val() === "") {
        erro = 'Selecione uma data de inicio!';
    }
    if (erro === "") {
        $.get("../Academia/ajax/Clientes_docs_server_processing.php?op=c&id_forn=" + $("#txtId").val() + "&id_doc=" + $("#txtDocModelo").val() + "&txtIdCliMU="+$("#txtIdCliMU").val()+"&dt_doc=" + $("#txtDocDtIni").val()).done(
                function (data) {
                    var json = JSON.parse(data);
                    if (json.result === "YES") {
                        var tblDocumentos = $('#tblDocumentos').dataTable();
                        tblDocumentos.fnReloadAjax("../Academia/ajax/Clientes_docs_server_processing.php?op=r&id_forn=" + $("#txtId").val());
                        $("#txtDocDtIni").val("");
                        $("#txtDocModelo").select2("val", "");
                        $("#txtDocModelo").children().remove();
                        $("#txtDocModelo").append(new Option('Selecione', ''));
                        listAllDocs();
                    } else {
                        bootbox.alert(json.result);
                    }
                });
    } else {
        bootbox.alert(erro);
    }
});

function RemoverDocumentos(idForn, idDoc) {
    bootbox.confirm('Confirma a exclusão do documento?', function (result) {
        if (result === true) {
            $.get("../Academia/ajax/Clientes_docs_server_processing.php?op=d" + "&txtIdCliMU="+$("#txtIdCliMU").val()+"&id_forn=" + idForn + "&id_doc=" + idDoc).done(
                    function (data) {
                        var json = JSON.parse(data);
                        if (json.result === "YES") {
                            var tblDocumentos = $('#tblDocumentos').dataTable();
                            tblDocumentos.fnReloadAjax("../Academia/ajax/Clientes_docs_server_processing.php?op=r&id_forn=" + $("#txtId").val());
                            $("#txtDocModelo").children().remove();
                            $("#txtDocModelo").append(new Option('Selecione', ''));
                            listAllDocs();
                        } else {
                            bootbox.alert("Erro ao efetuar o cancelamento!");
                        }
                    });
        }
    });
}

function listAllDocs() {
    $.get("../Academia/ajax/Clientes_docs_server_processing.php?op=rs" + "&id_forn=" + $("#txtId").val()).done(
            function (data) {
                var json = JSON.parse(data);
                if (json !== null) {
                    for (var x = 0; x < json.length; x++) {
                        $("#txtDocModelo").append(new Option(json[x].desc, json[x].id));
                    }
                }
            });
}

function RenovarDoc(idForn, idDoc) {
    bootbox.confirm('Confirma renovar o documento?', function (result) {
        if (result === true) {
            $.get("../Academia/ajax/Clientes_docs_server_processing.php?op=u" + "&id_forn=" + idForn + "&id_doc=" + idDoc).done(
                    function (data) {
                        var json = JSON.parse(data);
                        if (json.result === "YES") {
                            var tblDocumentos = $('#tblDocumentos').dataTable();
                            tblDocumentos.fnReloadAjax("../Academia/ajax/Clientes_docs_server_processing.php?op=r&id_forn=" + $("#txtId").val());
                        } else {
                            bootbox.alert("Erro ao efetuar a Renovação!");
                        }
                    });
        }
    });
}

$("#txtCredModelo").change(function () {
    calcDateEnd();
    refreshCredPagamento();
});

$("#txtCredDtIni").change(function () {
    calcDateEnd();
});

function calcDateEnd() {
    if (moment($("#txtCredDtIni").val(), lang["dmy"], true).isValid() && textToNumber($("#txtCredModelo option:selected").attr('dias')) > 0) {
        var date = moment($("#txtCredDtIni").val(), lang["dmy"], true);
        $("#txtCredDtFim").val(date.add(textToNumber($("#txtCredModelo option:selected").attr('dias')), 'days').format(lang["dmy"]));
    } else {
        $("#txtCredDtFim").val("");
    }
}

function atualizaCredenciais(){
  var tblCredenciais = $('#tblCredenciais').dataTable();
  tblCredenciais.fnReloadAjax("../Academia/form/ClientesCredenciaisServer.php?listCredencial=" + $("#txtId").val());
}
