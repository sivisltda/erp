$(document).ready(function () {
    $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
});

function finalFind(imp) {
    var retPrint = "";
    retPrint = '&tp=1';
    retPrint = retPrint + '&dt_begin=' + $('#txt_dt_begin').val() + '&dt_end=' + $('#txt_dt_end').val() + '&status=' + $("#mscStat").val() + '&tipo=' + $("#multiTipo").val();
    return retPrint.replace(/\//g, "_");
}

function imprimir() {
    var error = '';
    if ($("#txt_dt_begin").val() === '' || $("#txt_dt_end").val() === '') {
        error = 'Parar gerar o relatório é necessário data de inicio e fim.';
    }
    if (error === '') {
        var pRel = "&NomeArq=" + "Cartões Vencidos" +
                "&lbl= Cod|Nome Aluno|Nome do Cartão|Nº Cartão|Validade" +
                "&siz= 50|250|250|75|75" +
                "&pdf=" + // Colunas do server processing que não irão aparecer no pdf
                "&filter=" + //Label que irá aparecer os parametros de filtro
                "&PathArqInclude=" + "../Modulos/Academia/ajax/CartoesVencidos_server_processing.php" + finalFind(1).replace("?", "&"); // server processing
        window.open("/util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
    } else {
        bootbox.alert(error);
    }
}
