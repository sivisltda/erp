$(document).ready(function () { 
    var tblFerias = $('#tblFerias').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "ajax/Ferias_server_processing.php",
        "bFilter": false,    
        "aoColumns": [{ "bSortable": true },
               { "bSortable": true },
               { "bSortable": false }],
        'oLanguage': {
            'oPaginate': {
                'sFirst': "Primeiro",
                'sLast': "Último",
                'sNext': "Próximo",
                'sPrevious': "Anterior"
            }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
            'sLengthMenu': "Visualização de _MENU_ registros",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });
});

    
function salvaFerias (){
    if($("#txtMeses").val() === "" || $("#txtDias").val() === ""){
        bootbox.alert("Todos os campos são obrigatórios!");
    }else{
        $.post( "form/FeriasFormServer.php", "btnSave=S&txtMeses="+$("#txtMeses").val()+"&txtDias="+$("#txtDias").val()).done(function( data ) {
            if(data.trim() === "NO"){
                bootbox.alert("Meses já existentes!");
            }else if(data.trim() === "YES"){
                var oTable = $('#tblFerias').dataTable();
                oTable.fnDraw(false);
            }else{
                bootbox.alert("Erro ao salvar registro!");
            }
        }); 
    }
}

function RemoverItem(id){
    bootbox.confirm('Confirma a exclusão do registro?', function(result){
        if(result === true){
            $.post( "form/FeriasFormServer.php", {delFerias: id }).done(function( data ) {
                if(data.trim() === "YES"){
                    var oTable = $('#tblFerias').dataTable();
                    oTable.fnDraw(false);
                    $("#txtMeses, #txtDias").val("");
                }else{
                    bootbox.alert("Erro ao excluir registro!");
                }

            });
        }else{
            return;
        }
    });
}

function AtualizaPlanos(id){
    bootbox.confirm('Todos os planos ativos com dias de ferias zerados serão atualizados. Confirma a atualização?', function(result){
        if(result === true){
            $.post( "form/FeriasFormServer.php", {atualizaPlanos: id }).done(function( data ) {
                if(data.trim() === "YES"){
                    bootbox.alert("Planos atualizados com sucesso!");
                }else{
                    bootbox.alert(data.trim()+"Erro ao atualizar registro!");
                }

            });
        }else{
            return;
        }
    });
}