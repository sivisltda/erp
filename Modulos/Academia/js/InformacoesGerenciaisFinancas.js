var now = new Date();
var totalFimCarregamento = 0;
var clickTabFinanca = 0;

$(document).ready(function () {
    $("#txt_dt_begin, #txt_dt_end, #txt_dt_begin, #txtDtInicioAcesso, #txtDtFimAcesso").mask(lang["dateMask"]);
});

function carregaTabFinancas() {
    if (clickTabFinanca === 1) {
        return;
    }
    clickTabFinanca = 1;
    totalFimCarregamento = 0;
    botaoHoje();
    if ($("#tbFinancas").length > 0) {
        $('#tbFinancas').DataTable({
            "iDisplayLength": -1,
            "ordering": false,
            "bFilter": false,
            "bInfo": false,
            "sAjaxSource": final_url(0, 1),
            "bLengthChange": false,
            "bPaginate": false,
            'oLanguage': {
                'sEmptyTable': "Não foi encontrado nenhum resultado",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sZeroRecords': "Não foi encontrado nenhum resultado"},
            "fnDrawCallback": function (data) {
                makeChartFinance(data);
                trataCarregamentoPaginaFinancas('#tbFinancas');            
            }
        });
    } if ($("#tbTmedio").length > 0) {
        $('#tbTmedio').DataTable({
            "iDisplayLength": -1,
            "ordering": false,
            "bFilter": false,
            "bInfo": false,
            "sAjaxSource": final_url(2, 1),
            "bLengthChange": false,
            "bPaginate": false,
            'oLanguage': {
                'sEmptyTable': "Não foi encontrado nenhum resultado",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sZeroRecords': "Não foi encontrado nenhum resultado"},        
            "fnDrawCallback": function (data) {        
                trataCarregamentoPaginaFinancas('#tbTmedio');
            }
        });
    } if ($("#tbModalidades").length > 0) {    
        $('#tbModalidades').DataTable({
            "iDisplayLength": -1,
            "ordering": false,
            "bFilter": false,
            "bInfo": false,
            "sAjaxSource": final_url(3, 1),
            "bLengthChange": false,
            "bPaginate": false,
            'oLanguage': {
                'sEmptyTable': "Não foi encontrado nenhum resultado",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sZeroRecords': "Não foi encontrado nenhum resultado"},
            "fnDrawCallback": function (data) {
                makeChartFinanceMod(data);
                trataCarregamentoPaginaFinancas('#tbModalidades');            
            }
        });
    } if ($("#tbMatriculas").length > 0) {    
        $('#tbMatriculas').DataTable({
            "iDisplayLength": -1,
            "ordering": false,
            "bFilter": false,
            "bInfo": false,
            "sAjaxSource": final_url(4, 1),
            "bLengthChange": false,
            "bPaginate": false,
            'oLanguage': {
                'sEmptyTable': "Não foi encontrado nenhum resultado",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sZeroRecords': "Não foi encontrado nenhum resultado"},        
            "fnDrawCallback": function (data) {         
                trataCarregamentoPaginaFinancas('#tbMatriculas');
            }
        });
    } if ($("#tbReceita").length > 0) {    
        $('#tbReceita').DataTable({
            "iDisplayLength": -1,
            "ordering": false,
            "bFilter": false,
            "bInfo": false,
            "destroy": true,
            "sAjaxSource": final_url_Contratos(2, '-1'),
            "bLengthChange": false,
            "bPaginate": false,
            'oLanguage': {
                'sEmptyTable': "Não foi encontrado nenhum resultado",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sZeroRecords': "Não foi encontrado nenhum resultado"},
            "fnDrawCallback": function (data) {        
                trataCarregamentoPaginaFinancas('#tbReceita');
                let api = this.api();
                $(api.column(7).footer()).html(data["aoData"].length);
            }
        });
    } if ($("#tbResumo").length > 0) {    
        $('#tbResumo').DataTable({
            "iDisplayLength": -1,
            "ordering": false,
            "bFilter": false,
            "bInfo": false,
            "sAjaxSource": final_url(1, 1),
            "bLengthChange": false,
            "bPaginate": false,
            'oLanguage': {
                'sEmptyTable': "Não foi encontrado nenhum resultado",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sZeroRecords': "Não foi encontrado nenhum resultado"},        
            "aoColumnDefs": [{"bSortable": false, "aTargets": [0]}],
            "fnDrawCallback": function (data) {
                trataCarregamentoPaginaFinancas('#tbResumo');
            }
        });
    }
}

function trataCarregamentoPaginaFinancas(tipo) {
    totalFimCarregamento = totalFimCarregamento + 1;
    console.log(totalFimCarregamento + "|" + tipo);
    if (totalFimCarregamento === ($("#tbFinancas").length + $("#tbTmedio").length + $("#tbModalidades").length + 
    $("#tbMatriculas").length + $("#tbReceita").length + $("#tbResumo").length)) {     
        totalFimCarregamento = 0;        
        console.log("end");
        $("#loader").hide();
    } else {
        console.log("ini");        
        $("#loader").show();
    }
}

function botaoHoje() {
    $("#txt_dt_begin, #txt_dt_end").val(moment().format(lang["dmy"]));
}

function botaoMes() {
    $("#txt_dt_begin").val(moment().startOf('month').format(lang["dmy"]));
    $("#txt_dt_end").val(moment().endOf('month').format(lang["dmy"]));
}

function BuscarFinancas() {
    if ($("#tbFinancas").length > 0) {
        var tblrefresh = $("#tbFinancas").dataTable();
        tblrefresh.fnReloadAjax(final_url(0, 1));
    } if ($("#tbTmedio").length > 0) {        
        var tblrefreshMod = $("#tbTmedio").dataTable();
        tblrefreshMod.fnReloadAjax(final_url(2, 1));
    } if ($("#tbModalidades").length > 0) {        
        var tblrefreshMod = $("#tbModalidades").dataTable();
        tblrefreshMod.fnReloadAjax(final_url(3, 1));
    } if ($("#tbMatriculas").length > 0) {        
        var tblrefreshMod = $("#tbMatriculas").dataTable();
        tblrefreshMod.fnReloadAjax(final_url(4, 1));    
    } if ($("#tbReceita").length > 0) {        
        var tblrefreshReceita = $("#tbReceita").dataTable();
        tblrefreshReceita.fnReloadAjax(final_url_Contratos(2, '-1'));     
    } if ($("#tbResumo").length > 0) {        
        var tblrefreshResumo = $("#tbResumo").dataTable();
        tblrefreshResumo.fnReloadAjax(final_url(1, 1));    
    }
}

function makeChartFinance(data) {
    let o = 0;
    let val1 = 0;
    let val2 = 0;
    let val3 = 0;
    let val4 = 0;
    let val5 = 0;        
    let chartData7 = [];    
    
    for (var i = 0; i < data["aoData"].length; i++) {
        let val = data["aoData"][i]["_aData"];        
        chartData7[o] = {desc: val[0], valor: val[1]};
        val1 = val1 + textToNumber(val[1]);
        val2 = val2 + textToNumber(val[2]);
        val3 = val3 + textToNumber(val[3]);
        val4 = val4 + textToNumber(val[4]);
        val5 = val5 + textToNumber(val[5]);            
        o++;        
    }

    $('#tbFinancas tfoot th:eq(1)').html(numberFormat(val1, 1));
    $('#tbFinancas tfoot th:eq(2)').html(numberFormat(val2, 1));
    $('#tbFinancas tfoot th:eq(3)').html(numberFormat(val3, 1));
    $('#tbFinancas tfoot th:eq(4)').html(numberFormat(val4, 1));
    $('#tbFinancas tfoot th:eq(5)').html(numberFormat(val5, 1));
    AmCharts.makeChart("chartdiv7", {
        "type": "pie",
        "theme": "light",
        "dataProvider": chartData7,
        "valueField": "valor",
        "titleField": "desc",
        "marginTop": 0,
        "marginBottom": 0,
        "pullOutRadius": 10,
        "autoMargins": false,
        "labelText": "",
        "balloonText": "[[title]]: [[percents]]%"
    });
}

function makeChartFinanceMod(data) {
    let p = 0;
    let chartData8 = [];
    let val1 = 0;
    
    for (var i = 0; i < data["aoData"].length; i++) {
        let val = data["aoData"][i]["_aData"];
        chartData8[p] = {category: val[0], value: textToNumber(val[1]), legend: ""};
        val1 = val1 + textToNumber(val[1]);
        p++;
    }
    
    $('#tbModalidades tfoot th:eq(1)').html(numberFormat(val1, 1));
    AmCharts.makeChart("chartdiv8", {
        "type": "serial",
        "theme": "light",
        "dataProvider": chartData8,
        "startDuration": 1,
        "graphs": [{
                "balloonText": "<b>" + lang["prefix"] + " [[value]]</b>",
                "fillColorsField": "color",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "value"
            }],
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "numberFormatter": {"precision": 2, "decimalSeparator": lang["centsSeparator"], "thousandsSeparator": lang["thousandsSeparator"]},
        "categoryField": "legend"
    });   
}

function fnFormatDetails(nTr) {
    var aData = $('#tbResumo').dataTable().fnGetData(nTr);
    if (aData[9]) {
        $.getJSON('../Academia/ajax/InformacoesGerenciaisFinancas_parcelas.php', {
            id_venda: aData[9],
            tipo: $("#txtTpData").val(),
            dt_begin: $("#txt_dt_begin").val().replace(/\//g, "_"),
            dt_end: $("#txt_dt_end").val().replace(/\//g, "_")
        }, function (j) {
            var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="width: 100%;">';
            for (var i = 0; i < j.length; i++) {
                sOut += '<tr><td style="width: 29px;"></td>' +
                        '<td style="width: 65px;"> ' + j[i].pa + '</td>' +
                        '<td style="width: 98px; color: #08c;">' + j[i].data_parcela + '</td>' +
                        '<td style="width: 90px;text-align: center"> ' + j[i].valor_parcela + '</td>' +
                        '<td>' + j[i].descricao + '</td></tr>';
            }
            sOut += '</table>';
            $('#tbResumo').dataTable().fnOpen(nTr, sOut, 'details');
        });
    }
}

$('#tbResumo tbody td img').live('click', function () {
    var nTr = $(this).parents('tr')[0];
    var aData = $('#tbResumo').dataTable().fnGetData(nTr);
    if (aData[7].length > 0) {
        if ($('#tbResumo').dataTable().fnIsOpen(nTr)) {
            this.src = "./../../img/details_open.png";
            $('#tbResumo').dataTable().fnClose(nTr);
        } else {
            this.src = "./../../img/details_close.png";
            fnFormatDetails(nTr);
        }
    }
});

function ExportarExcelCaixa() {
    window.open(final_url(1, 1) + "&ex=1");
}

function carregaCxs(loja) {
    $("#loader").show();
    $.ajax({
        type: "GET",
        url: "./../Academia/ajax/InformacoesGerenciaisFinancas_server_processing.php?fr=5&loj=" + loja,
        success: function (data) {
            $("#loader").hide();
            var items = JSON.parse(data).aaData;
            $("#txtOperador").find('option').remove().end().append('<option value="">TODOS</option>').val('');
            $.each(items, function (i, item) {
                $('#txtOperador').append($('<option>', {
                    value: item.usuario,
                    text: item.nome
                }));
            });
        }
    });
}

function final_url(forma, tipo) {
    let retPrint = "";
    if ($("#mscPagamento").val() !== null) {
        retPrint += "&grp=" + $("#mscPagamento").val();
    }    
    if ($("#mscFilialCli").val() !== null) {
        retPrint += "&gfi=" + $("#mscFilialCli").val();
    }    
    if ($("#mscGrupoCli").val() !== null) {
        retPrint += "&gpc=" + $("#mscGrupoCli").val();
    }    
    return "./../Academia/ajax/InformacoesGerenciaisFinancas_server_processing.php?fr=" + forma +
    "&tp=" + tipo +
    "&emp=" + $("#txtLojaSel").val() +
    "&ban=" + $("#txtBanco").val() +
    "&opr=" + $("#txtOperador").val() +
    "&dtt=" + $("#txtTpData").val() +
    "&dti=" + $("#txt_dt_begin").val() +
    "&dtf=" + $("#txt_dt_end").val() + retPrint;
}

function imprimirCaixa(tipo) {
    var tblrefreshResumo = $("#tbResumo").dataTable();
    if (tblrefreshResumo.fnGetData().length > 0) {
        let retPrint = "";
        if ($("#mscPagamento").val() !== null) {
            retPrint += "&grp=" + $("#mscPagamento").val();
        }           
        if ($("#mscFilialCli").val() !== null) {
            retPrint += "&gfi=" + $("#mscFilialCli").val();
        }           
        if ($("#mscGrupoCli").val() !== null) {
            retPrint += "&gpc=" + $("#mscGrupoCli").val();
        }           
        var pRel = "&emp=" + $("#txtLojaSel").val() +
                "&ban=" + $("#txtBanco").val() +
                "&opr=" + $("#txtOperador").val() +
                "&dtt=" + $("#txtTpData").val() +
                "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "_") +
                "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "_") + retPrint;
        window.open("../../util/ImpressaoPdf.php?id=" + (tipo === 2 ? "&pAlt=80&pLar=160&pMrg=0&pFon=8" : "") +
        "&mdl=" + tipo + "&tpImp=I&NomeArq=ResumoCaixa&PathArq=../Modulos/Academia/modelos/ResumoCaixa.php" + pRel, '_blank');
    }
}

function AbrirBox(opc, id) {
    if (opc === 2) {
        window.location = './../Academia/ClientesForm.php?id=' + id;
    }
}
