 $(document).ready(function() {
     $("#tbodyAmbiente").css("width",$( window ).width()-313+"px");
     $('#tbodyAmbiente').scroll(function(e) { //detect a scroll event on the tbody
         $('thead').css("left", -$("#tbodyAmbiente").scrollLeft()); //fix the thead relative to the body scrolling
         $('thead th:nth-child(1)').css("left", $("#tbodyAmbiente").scrollLeft()); //fix the first cell of the header
         $('tbody td:nth-child(1)').css("left", $("#tbodyAmbiente").scrollLeft()); //fix the first column of tdbody
     });

     $( "#dtCalendario" ).datepicker({
         dateFormat: lang["dPickerFormat"]
     });

     $.getJSON("../Academia/form/AmbientesFormServer.php", {isAjax:"S", listAmbientes: "S"}, function (j) {
         var options = "<option value=\"-1\">[ TODOS ]</option>";
         if(j !== null ){
             if(j.length > 0){
                 for (var i = 0; i < j.length; i++) {
                     options = options + "<option value=\""+ j[i].id_ambiente+"\">" + j[i].nome_ambiente + "</option>";
                 }
             }
             $("#txtAmbienteTurma").html(options);
         }
     }).done(function(){
         //trataCarregamentoTela();
     });

     $.getJSON("../Academia/form/TurmasFormServer.php", {isAjax:"S", listPlanosTurma: "S", idTurma: "0"}, function (j) {
         var options = "";
         if(j !== null ){
             if(j.length > 0){
                 for (var i = 0; i < j.length; i++) {
                     var selecionado = j[i].isCheck === 'S' ? "SELECTED" : "";
                     options = options + "<option value=\""+ j[i].conta_produto+"\" "+selecionado+" >" + j[i].descricao + "</option>";
                 }
             }
             $("#mscPlanosTurma").html(options);
         }
     }).done(function() {
         //trataCarregamentoTela();
     });

     $.getJSON("../Academia/form/ClientesFormServer.php", {isAjax:"S", listProfessor: "S"}, function (j) {
         var options = "<option value=\"-1\">[TODOS]</option>";
         if(j !== null ){
             if(j.length > 0){
                 for (var i = 0; i < j.length; i++) {
                     options = options + "<option value=\""+ j[i].id_fornecedores_despesas+"\">" + j[i].razao_social + "</option>";
                 }
             }
             $("#txtProfessorTurma").html(options);
         }
     }).done(function(){
         //trataCarregamentoTela();
     });

     $.getJSON("../Academia/form/HorariosFormServer.php", {isAjax:"S", listHorarios: "S"}, function (j) {
         var options = "<option value=\"-1\">[TODOS]</option>";
         if(j !== null ){
             if(j.length > 0){
                 for (var i = 0; i < j.length; i++) {
                     options = options + "<option value=\""+ j[i].cod_turno+"\">" + j[i].nome_turno + "</option>";
                 }
             }
             $("#txtHorarioTurma").html(options);
         }
     }).done(function(){
         //trataCarregamentoTela();
     });

     $('#tbAgendamentos').dataTable({
         "iDisplayLength": 20,
         "bProcessing": true,
         "bServerSide": true,
         "bSort": false,
         "bFilter": false,
         "bPaginate": false,
         "scrollY": "240px",
         "scrollCollapse": true,
         "sAjaxSource": final_url_turmas(1,0),
         "bInfo": false,
         'oLanguage': {
             'oPaginate': {
                 'sFirst': "Primeiro",
                 'sLast': "Último",
                 'sNext': "Próximo",
                 'sPrevious': "Anterior"
             }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
             'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
             'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
             'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
             'sLengthMenu': "Visualização de _MENU_ registros",
             'sLoadingRecords': "Carregando...",
             'sProcessing': "Processando...",
             'sSearch': "Pesquisar:",
             'sZeroRecords': "Não foi encontrado nenhum resultado"},
         "sPaginationType": "full_numbers",
         "fnDrawCallback":function(){
             //pos_processamento();
         }
     });
     montaListaAmbientes();
     $(function () {
         $("#txtClienteTurma").autocomplete({
             source: function (request, response) {
                 $.ajax({
                     url: "../../Modulos/CRM/ajax.php",
                     dataType: "json",
                     data: {
                         q: request.term,
                         t: 'C'
                     },
                     success: function (data) {
                         response(data);
                     }
                 });
             },
             minLength: 3,
             select: function (event, ui) {
                 $("#txtCodClienteTurma").val(ui.item.id);
             }
         });
     });
 });

function BuscarTurma(){
    var tblTurmas = $('#tbAgendamentos').dataTable();
    tblTurmas.fnReloadAjax(final_url_turmas(1,0));
    montaListaAmbientes();
}

function final_url_turmas(tptable,imp) {
    var ret = "";
    ret = '?imp=0';
    if(imp === 1){ ret = '?imp=1'; }
    if($("#txtAmbienteTurma").val() > 0){ ret = ret + '&idAmbiente='+$("#txtAmbienteTurma").val(); }
    if($("#txtHorarioTurma").val() > 0){ ret = ret + '&idHorario='+$("#txtHorarioTurma").val(); }
    if($("#txtProfessorTurma").val()> 0){ ret = ret + '&idProfessor='+$("#txtProfessorTurma").val(); }
    if($("#txtCodClienteTurma").val()!== ""){ ret = ret + '&idCliente='+$("#txtCodClienteTurma").val(); }
    if($("#mscPlanosTurma").val()!== null){ ret = ret + '&idPlano='+$("#mscPlanosTurma").val(); }
    return "ajax/MapaTurmas_server_processing.php" + ret + "&tptable=" + tptable + "&dtAgenda=" + $("#dtCalendario").val().replace(/\//g, "_");
}

function montaListaAmbientes(){
    $("#tbAmbiente thead tr th").remove();
    $("#tbAmbiente tbody tr").remove();
    $.getJSON(final_url_turmas(2,0), {}, function (data) {
        if(data !== null ){
            var cabecalho = "<tr><th></th>";
            $.each(data[1], function (key, val) {
                $("#tbAmbiente thead tr").append("<th>"+ val +"</th>");
            });
            var conteudo = "";
            $.each(data[0],function(index, value){
                conteudo += "<tr><td>"+value['hr']+"</td>";
                $.each(value,function(k,v){
                    if(k > 0){
                        conteudo += "<td>"+v.replace("|","<br>")+"</td>";
                    }
                });
                conteudo += "</tr>";
            });
            $("#tbodyAmbiente").append(conteudo);
        }
    }).done(function(){
        //trataCarregamentoTela();
    });
}

function listaFiltroTurma(){
    var filtros = "";
    if($("#txtAmbienteTurma").val() >= "0"){
     filtros += "   Ambiente: "+$("#txtAmbienteTurma option:selected").text();
    }
    if($("#txtHorarioTurma").val() >= "0"){
        filtros += "   Horário: "+$("#txtHorarioTurma option:selected").text();
    }
    if($("#txtProfessorTurma").val() >= "0"){
     filtros += "   Professor: " + $("#txtProfessorTurma option:selected").text();
    }
    if($("#txtClienteTurma").val() >= "0"){
     filtros += "   Cliente: "+$("#txtClienteTurma").val();
    }
    var itenspl = "";
    $("#mscPlanosTurma option:selected").each(function (i,sel) {
        itenspl += itenspl !== "" ? "|"+$(sel).text() : $(sel).text();
    });
    if(itenspl!== ""){ filtros += "   Plano: "+itenspl;}
    return filtros;
}

function imprimirTbAgendamento(){
    var pRel = "&NomeArq=" + "Agendamentos" +
        "&lbl=Cliente|Modalidade|Horário|Professor|Ambiente" +
        "&siz=200|140|100|140|120" +
        "&pdf=5" + // Colunas do server processing que não irão aparecer no pdf
        "&filter=" + listaFiltroTurma() + //Label que irá aparecer os parametros de filtro
        "&PathArqInclude=../Modulos/Academia/" + final_url_turmas(1,1).replace("?","&"); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel+ "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
}

function imprimirMapaAmbiente(){
    var pRel = "&NomeArq=" + "Agendamentos" +
        "&pdf=4" + // Colunas do server processing que não irão aparecer no pdf
        "&filter=" + listaFiltroTurma() + //Label que irá aparecer os parametros de filtro
        "&PathArqInclude=../Modulos/Academia/" + final_url_turmas(2,1).replace("?","&"); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel+ "&pOri=L&tpImp=I&PathArq=../Modulos/Academia/modelos/Lista_Turmas_Ambiente.php", '_blank');
 }

function AgendarAlunoTurma(){
    if($('#txtCodClienteTurma').val() !== "" && $('#txtClienteTurma').val()){
        window.location = 'ClientesForm.php?id=' + $('#txtCodClienteTurma').val()+"#tab3";
    }else{
        bootbox.alert("Selecione um Cliente");
    }
}



