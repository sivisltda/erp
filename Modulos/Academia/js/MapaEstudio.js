$(document).ready(function () {
    $("#txtFaixa1_ini").mask("99:99");

    $('#tbodyAmbiente').scroll(function (e) {
        $('thead').css("left", -$("#tbodyAmbiente").scrollLeft());
        $('thead th:nth-child(1)').css("left", $("#tbodyAmbiente").scrollLeft());
        $('tbody td:nth-child(1)').css("left", $("#tbodyAmbiente").scrollLeft());
    });

    $.getJSON("../Academia/form/ClientesFormServer.php", {isAjax: "S", listProfessor: "S"}, function (j) {
        var options = "</option>";
        if (j !== null) {
            if (j.length > 0) {
                for (var i = 0; i < j.length; i++) {
                    options = options + "<option value=\"" + j[i].id_fornecedores_despesas + "\" style=\"color:" + j[i].cor + ";\" >" + j[i].razao_social + "</option>";
                }
            }
            $("#txtProfessorTurma").html("<option value=\"null\">TODOS" + options);
            $("#txtProfessorTurmaHr").html("<option value=\"null\">SELECIONE" + options);
        }
    });

    $("#txtCliente").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "../../Modulos/CRM/ajax.php",
                dataType: "json",
                data: {q: request.term, t: 'C', s: 'Ativo'},
                success: function (data) {
                    response(data);
                }
            });
        }, minLength: 3,
        select: function (event, ui) {
            $("#txtCodCliente").val(ui.item.id);
        }
    });

    $("#txtProfessorTurma").change(function (event) {
        montaListaAmbientes();
    });
    montaListaAmbientes();
});

var dias = ["", "DOMINGO", "SEGUNDA-FEIRA", "TERÇA-FEIRA", "QUARTA-FEIRA", "QUINTA-FEIRA", "SEXTA-FEIRA", "SÁBADO"];

function listaFiltroTurma() {
    var filtros = "";
    if ($("#txtProfessorTurma").val() >= "0") {
        filtros += "   Professor: " + $("#txtProfessorTurma option:selected").text();
    }
    return filtros;
}

function final_url_turmas(imp) {
    var ret = "?idProfessor=" + $("#txtProfessorTurma").val();
    ret = ret + '&imp=0';
    if (imp === 1) {
        ret = ret + '&imp=1';
    }
    return "ajax/MapaEstudio_server_processing.php" + ret;
}

function montaListaAmbientes() {
    $("#tbAmbiente thead tr th").remove();
    $("#tbAmbiente tbody tr").remove();
    $.getJSON(final_url_turmas(0), {}, function (data) {
        if (data !== null) {
            $.each(dias, function (key, val) {
                $("#tbAmbiente thead tr").append("<th>" + val + "</th>");
            });
            var conteudo = "";
            $.each(data["aaData"], function (index, value) {
                conteudo += "<tr><td>" + value[0] + "</td>";
                $.each(value, function (k, v) {
                    if (k > 0) {
                        conteudo += "<td class=\"" + (v.substring(0, 1) === "1" ? "cell-disabled" : "") + "\">" + v.replace(/\|/g, '<br>').substring(1) + "</td>";
                    }
                });
                conteudo += "</tr>";
            });
            $("#tbodyAmbiente").append(conteudo);
        }
    });
}

function imprimirMapaAmbiente() {
    var label = "";
    var tamanho = "";
    $.each(dias, function (key, val) {
        if (key > 0) {
            label = label + "|" + val;
            tamanho = tamanho + "|130";
        }
    });
    var pRel = "&NomeArq=" + "Agendamentos" +
            "&lbl=Horário" + label +
            "&siz=97" + tamanho +
            "&pdf=8" + //Colunas do server processing que não irão aparecer no pdf
            "&filter=" + listaFiltroTurma() + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + "../Modulos/Academia/" + final_url_turmas(1).replace("?", "&"); //server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&pOri=L&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
}

function salvar() {
    if (validaForm()) {
        $.post("form/EstudioFormServer.php", "btnSave=S&" + $("#frmMain").serialize()).done(function (data) {
            if ($.isNumeric(data.trim())) {
                limparTela();
                montaListaAmbientes();
            } else {
                bootbox.alert(data);
            }
        });
    }
}

function editar() {
    disabledCampos(false);
    $("#btn_new").hide();
    $("#btn_edt").hide();
    $("#btn_exr").hide();
    $("#btn_sav").show();
    $("#btn_cnl").show();
}

function cancelar() {
    limparTela();
}

function excluir() {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            if ($.isNumeric($("#txtId").val())) {
                $.post("form/EstudioFormServer.php", {
                    btnExcluir: "S", txtId: $("#txtId").val()}).done(function (data) {
                    if (data.trim() === "YES") {
                        limparTela();
                        montaListaAmbientes();
                    } else {
                        bootbox.alert("Erro ao excluir registro!");
                    }
                });
            }
        } else {
            return;
        }
    });
}

function validaForm() {
    if (!$.isNumeric($('#txtProfessorTurmaHr').val())) {
        bootbox.alert("Professor é Obrigatório!");
        return false;
    }
    if (!$.isNumeric($('#txtCodCliente').val())) {
        bootbox.alert("Cliente é Obrigatório!");
        return false;
    }
    if (!verificaHora($("#txtFaixa1_ini").val())) {
        bootbox.alert("Horário inválido!");
        return false;
    }
    if ($("#fx1_0").is(':checked') === false && $("#fx1_1").is(':checked') === false && $("#fx1_2").is(':checked') === false
            && $("#fx1_3").is(':checked') === false && $("#fx1_4").is(':checked') === false && $("#fx1_5").is(':checked') === false
            && $("#fx1_6").is(':checked') === false) {
        bootbox.alert("Selecione os dias da semana!");
        return false;
    }
    return true;
}

function limparTela() {
    $("#txtId").val("");
    $("#txtCliente").val("");
    $("#txtCodCliente").val("");
    $("#txtFaixa1_ini").val("");
    for (var i = 0; i <= 6; i++) {
        $("#fx1_" + i).attr('checked', false);
    }
    $("#txtProfessorTurmaHr").val("null");
    disabledCampos(false);
    $("#btn_new").show();
    $("#btn_edt").hide();
    $("#btn_exr").hide();
    $("#btn_sav").hide();
    $("#btn_cnl").hide();
}

function preencherTela(id) {
    $.getJSON("form/EstudioFormServer.php", {id: id}, function (j) {
        if (j !== null && j.length > 0) {
            for (var i = 0; i < j.length; i++) {
                $("#txtId").val(j[i].cod_estudio);
                $("#txtCliente").val(j[i].razao_social);
                $("#txtCodCliente").val(j[i].id_fornecedores_despesas);
                $("#txtFaixa1_ini").val(j[i].faixa1_ini);
                for (var k = 0; k <= 6; k++) {
                    $("#fx1_" + k).attr('checked', (j[i].dia_semana1.substring(k, k + 1) === "1" ? true : false));
                }
                $("#txtProfessorTurmaHr").val(j[i].professor);
                disabledCampos(true);
                $("#btn_new").hide();
                $("#btn_edt").show();
                $("#btn_exr").show();
                $("#btn_sav").hide();
                $("#btn_cnl").hide();
            }
        }
    });
}

function disabledCampos(tipo) {
    $("#txtCliente").attr("disabled", tipo);
    $("#txtFaixa1_ini").attr("disabled", tipo);
    for (var k = 0; k <= 6; k++) {
        $("#fx1_" + k).attr("disabled", tipo);
    }
    $("#txtProfessorTurmaHr").attr("disabled", tipo);
}

function historico() {
    if ($.isNumeric($('#txtCodCliente').val())) {
        abrirTelaBox("MapaEstudioHistorico.php?id=" + $('#txtCodCliente').val(), 436, 600);
    } else {
        bootbox.alert("Cliente é Obrigatório!");
        return false;
    }
}

function FecharBox(opc){
    $("#newbox").remove();
}

function verificaHora(hora) {
    if (hora !== "") {
        var hrs = (hora.substring(0, 2));
        var min = (hora.substring(3, 5));
        if (hrs === "" || min === "") {
            return false;
        }
        if ((hrs < 00) || (hrs > 23) || (min < 00) || (min > 59)) {
            return false;
        } else {
            return true;
        }
    }
    return true;
}
