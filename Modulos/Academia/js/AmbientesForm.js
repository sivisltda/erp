
$(document).ready(function () {
    $("#mscGrupos").multiSelect({
        selectableHeader: "<div class='multipleselect-header'>Selecione</div>",
        selectionHeader: "<div class='multipleselect-header'>Selecionados</div>"
    });

    if ($('#txtTitulo').attr("disabled") === "disabled") {
        $('#ms-mscGrupos').find('li').addClass("disabled");
    }
});

function validaForm() {
    if ($("#txtTitulo").val() === "") {
        bootbox.alert("Título é Obrigatório!");
        return false;
    }
    return true;
}

