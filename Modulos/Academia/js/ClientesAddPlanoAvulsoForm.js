function iniciaAddPlanoAvulsoForm() {
    $("#txtpreco").priceFormat({prefix: "", centsSeparator: ",", thousandsSeparator: "."});

    $('#txtTempo').change(function () {
        if ($('#txtTempo').val() === 1) {
            $('#Cnttqnt').show();
            $("#txtPrazo").parent().css({'width': '19%'});
        } else {
            $("#txtPrazo").parent().css({'width': '30%'});
            $('#Cnttqnt').hide();
        }
    });

    var tblValores = $('#tblValores').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        bPaginate: false, bFilter: false, bInfo: false, bSort: false,
        "sAjaxSource": "form/ClientesPlanosServer.php?listValores=0",
        "scrollY": "120px",
        "scrollCollapse": true,
        'oLanguage': {
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sZeroRecords': "Não foi encontrado nenhum resultado"
        }
    });

    function atualizaValores(id) {
        tblValores.fnReloadAjax("form/ClientesPlanosServer.php?listValores=" + id);
        $("#txtHorarios").select2("val", "");
        $.getJSON("form/ClientesPlanosServer.php", {listHorarios: "S", idPlano: id}, function (j) {
            var options = "<option value=\"\">Selecione o Horário</option>";
            if (j !== null) {
                if (j.length > 0) {
                    for (var i = 0; i < j.length; i++) {
                        options = options + "<option value=\"" + j[i].cod_turno + "\">" + j[i].nome_turno + "</option>";
                    }
                }
                $("#txtHorarios").html(options);
                if (j.length === 1) {
                    $("#txtHorarios").select2("val", j[0].cod_turno);
                }
            }

        });
    }

    $('#tblValores tbody').on('click', 'tr', function () {
        if ($(this).find('td').hasClass('selected')) {
            $(this).find('td').removeClass('selected');
        } else {
            $('#tblValores').find('td.selected').removeClass('selected');
            $(this).find('td').addClass('selected');
        }
    });

    $('#btnGravarPlano').click(function (e) {
        var error = '';
        if ($("#txtpreco").val() === numberFormat(0) || $("#txtpreco").val() === '') {
            error = 'O campo preço é obrigatório ser preenchido e o valor tem que ser maior que 0!';
        } else if ($('#txtDtInicio').val() === '') {
            error = 'O campo data de inicío é um campo obrigatório!';
        }
        if ($('#txtTempo').val() === 1) {
            if ($('#qnt').val() <= 0 || $('#qnt').val() === '') {
                error = 'O campo quantidade deve um numero inteiro maior que 0';
            } else if ($('#txtInterval').val() <= 0 || $('#txtInterval').val() === '') {
                error = 'O campo intervalo deve um numero inteiro maior que 0';
            }
        } else {
            if ($('#txtInterval').val() <= 0 || $('#txtInterval').val() === '') {
                error = 'O campo intervalo deve um numero inteiro maior que 0';
            }
        }
        if (error === '') {
            salvaPlano();
        } else {
            bootbox.alert(error);
        }
    });

    function salvaPlano() {
        $.post("form/ClientesPlanosAvulsoServer.php", "addContrato=S&avulso=S&txtId=" + $("#txtId", window.parent.document).val() +
                "&tpTela=" + 'A' +
                "&qntparc=" + $("#txtqnt").val() +
                "&finito=" + $("#txtTempo").val() +
                "&prazo=" + $("#txtPrazo").val() +
                "&interval=" + $("#txtInterval").val() +
                "&descr=CONTRATO " + $("#txtPlanos").select2('data').text +
                "&txtProduto=" + $("#txtPlanos").val() +
                "&txtIdPlano=" + $("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected)').find('.renovar').attr('data-id') +
                "&txtHorarios=" + $("#txtHorarios").val() + "&txtIdParc=" + $('tr:has(td.selected)').find('input').attr('data-id') +
                "&valMens=" + $("#txtpreco").val() +
                "&txtDtIni=" + $("#txtDtInicio").val().replace(/\//g, "_")).done(function (data) {
            if (!isNaN(data.trim())) {
                $('#txtIdSel', window.parent.document).val(data.trim());
                parent.AtualizaListPlanosClientes();
                $("#txtDtInicio").val("");
                $("#txtPlanos, #txtIdParc").select2("val", "");
                $('#tblValores').find('td.selected').removeClass('selected');
                parent.FecharBox(0);
            }
        });
    }
}
