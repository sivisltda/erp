var clickTabStatus = 0;
var selecionadoStatus;
function carregaTabStatus() {
    if (clickTabStatus === 1) {
        return;
    }
    clickTabStatus = 1;
    $("#loader").show();
    totalFimCarregamento = 0;

    if (!$.fn.DataTable.isDataTable('#tbReceita')) {
        $('#tbReceita').DataTable({
            "iDisplayLength": -1,
            "ordering": false,
            "bFilter": false,
            "bInfo": false,
            "sAjaxSource": final_url_Contratos(2, '-1'),
            "bLengthChange": false,
            "bPaginate": false,
            'oLanguage': {
                'sEmptyTable': "Não foi encontrado nenhum resultado",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sZeroRecords': "Não foi encontrado nenhum resultado"},
            "fnDrawCallback": function (data) {
                $("#tbReceita tfoot tr th")[1].innerText = data["aoData"].length; 
            },            
            "fnInitComplete": function (oSettings, json) {
                trataCarregamentoPaginaContratos();
            }
        });
    }

    $('#tbInfoAdd').DataTable({
        "iDisplayLength": -1,
        "ordering": false,
        "bFilter": false,
        "bInfo": false,
        "sAjaxSource": final_url_Status(4, ''),
        "bLengthChange": false,
        "bPaginate": false,
        "fnInitComplete": function (oSettings, json) {
            trataCarregamentoPaginaStatus();
        }
    });
    makeChartStatus();
    makeChartStatusNRR();
    makeChartStatusVisitantes();
    makeChartStatusAtivos();
}

function trataCarregamentoPaginaStatus() {
    totalFimCarregamento = totalFimCarregamento + 1;
    if (totalFimCarregamento === 9) {
        $("#loader").hide();
    }
}

function final_url_Status(forma, filtro) {
    return "ajax/InformacoesGerenciaisStatus_server_processing.php?fr=" + forma + "&fil=" + filtro + "&filial=" + $('#txtLojaSel').val();
}

function filterStatus(forma, estado) {
    $("#loader").show();
    selecionadoStatus = forma + '/' + estado;
    var tblrefresh = $("#tbReceita").dataTable();
    tblrefresh.fnReloadAjax(final_url_Status(forma, estado), function () {
        $("#loader").hide();
    }, null);
    $("#tbReceita").attr("filtro", final_url_Status(forma, estado));
}

function filterStatusNRR(forma, estado) {
    $("#tbRec").show();
    $("#loader").show();
    selecionadoStatus = forma + '/' + estado;
    var tblrefresh = $("#tbReceita").dataTable();
    tblrefresh.fnReloadAjax("ajax/InformacoesGerenciaisStatus_server_processing.php?fr=" + forma +
            "&fil=" + estado + "&filial=" + $('#txtLojaSel').val() +
            "&dti=" + $('#txt_dt_begin').val() + "&dtf=" + $('#txt_dt_end').val(), function () {
        $("#loader").hide();
    }, null);
    $("#tbReceita").attr("filtro", final_url_Status(forma, estado));
}

function filterStatusVis(forma, estado) {
    $("#loader").show();
    selecionadoStatus = forma + '/' + estado;
    var tblrefresh = $("#tbReceita").dataTable();
    tblrefresh.fnReloadAjax(final_url_Status(forma, estado), function () {
        $("#loader").hide();
    }, null);
    $("#tbReceita").attr("filtro", final_url_Status(forma, estado));
}

function filterStatusBol(forma, estado) {
    $("#loader").show();
    selecionadoStatus = forma + '/' + estado;
    var tblrefresh = $("#tbReceita").dataTable();
    tblrefresh.fnReloadAjax(final_url_Status(forma, estado), function () {
        $("#loader").hide();
    }, null);
    $("#tbReceita").attr("filtro", final_url_Status(forma, estado));
}

function filterAddInfo(forma, estado) {
    $("#loader").show();
    selecionadoStatus = forma + '/' + estado;
    var tblrefresh = $("#tbReceita").dataTable();
    tblrefresh.fnReloadAjax(final_url_Status(forma, estado), function () {
        $("#loader").hide();
    }, null);
    $("#tbReceita").attr("filtro", final_url_Status(forma, estado));
}

function makeChartStatus() {
    var i = 0;
    var chartData = [];
    $.getJSON(final_url_Status(0, ''), function (data) {
        novoStatus(data);
        $.each(data["aaData"], function (key, val) {
            chartData[i] = {desc: val[0], valor: val[1], color: val[3]};
            i++;
        });
    }).done(function () {
        AmCharts.makeChart("chartdiv", {
            "type": "pie",
            "theme": "light",
            "dataProvider": chartData,
            "valueField": "valor",
            "titleField": "desc",
            "colorField": "color",
            "marginTop": 0,
            "marginBottom": 0,
            "pullOutRadius": 10,
            "autoMargins": false,
            "labelText": "",
            "balloonText": "[[title]]: [[percents]]%", "export": {
                "enabled": true,
                "libs": {"path": "http://www.amcharts.com/lib/3/plugins/export/libs/"}
            }
        });
        trataCarregamentoPaginaStatus();
    });
}

function makeChartStatusNRR() {
    var j = 0;
    var chartData2 = [];
    $.getJSON(final_url_Status(1, ''), function (data) {
        statusAlunos(data);
        $.each(data["aaData"], function (key, val) {
            chartData2[j] = {desc: val[3], valor: val[1]};
            j++;
        });
    }).done(function () {
        AmCharts.makeChart("chartdiv2", {
            "type": "serial",
            "theme": "light",
            "dataProvider": chartData2,
            "graphs": [{
                    "balloonText": "[[category]]: [[value]]",
                    "fillAlphas": 1,
                    "lineAlpha": 0.2,
                    "title": "Alunos Novos",
                    "type": "column",
                    "valueField": "valor"
                }],
            "rotate": false,
            "categoryField": "desc",
            "categoryAxis": {
                "gridPosition": "start",
                "labelRotation": 20,
                "fillAlpha": 0.05,
                "position": "left"
            }, "export": {
                "enabled": true,
                "libs": {"path": "http://www.amcharts.com/lib/3/plugins/export/libs/"}
            }
        });
        trataCarregamentoPaginaStatus();
    });
}

function makeChartStatusVisitantes() {
    var k = 0;
    var chartData3 = [];
    $.getJSON(final_url_Status(2, ''), function (data) {
        statusConversao(data);
        $.each(data["aaData"], function (key, val) {
            if (k < 2) {
                chartData3[k] = {desc: val[0], valor: val[1]};
                k++;
            }
        });
    }).done(function () {
        AmCharts.makeChart("chartdiv3", {
            "type": "pie",
            "theme": "light",
            "dataProvider": chartData3,
            "valueField": "valor",
            "titleField": "desc",
            "marginTop": 0,
            "marginBottom": 0,
            "pullOutRadius": 10,
            "autoMargins": false,
            "labelText": "",
            "balloonText": "[[title]]: [[percents]]%", "export": {
                "enabled": true,
                "libs": {"path": "http://www.amcharts.com/lib/3/plugins/export/libs/"}
            }
        });
        trataCarregamentoPaginaStatus();
    });
}

function makeChartStatusAtivos() {
    var l = 0;
    var chartData4 = [];
    $.getJSON(final_url_Status(3, ''), function (data) {
        statusBolsasEativos(data);
        $.each(data["aaData"], function (key, val) {
            chartData4[l] = {desc: val[0], valor: val[1], color: val[3]};
            l++;
        });
    }).done(function () {
        AmCharts.makeChart("chartdiv4", {
            "type": "pie",
            "theme": "light",
            "dataProvider": chartData4,
            "valueField": "valor",
            "titleField": "desc",
            "colorField": "color",
            "marginTop": 0,
            "marginBottom": 0,
            "pullOutRadius": 10,
            "autoMargins": false,
            "labelText": "",
            "balloonText": "[[title]]: [[percents]]%", "export": {
                "enabled": true,
                "libs": {"path": "http://www.amcharts.com/lib/3/plugins/export/libs/"}
            }
        });
        trataCarregamentoPaginaStatus();
    });
}

function listaEmails() {
    var emails = [];
    if ($("#tab1").hasClass("active") || $("#tab2").hasClass("active")) {
        $('#tbReceita > tbody  > tr').each(function () {
            if ($('>td:nth-child(8)', $(this)).html() !== "" && $.isNumeric($('>td:nth-child(1)', $(this)).html())) {
                emails.push($('>td:nth-child(8)', $(this)).html() + "|" + $('>td:nth-child(1)', $(this)).html());
            }
        });
    } else if ($("#tab4").hasClass("active")) {
        $('#tbFiltroAv > tbody  > tr').each(function () {
            if ($('>td:nth-child(8)', $(this)).html() !== "" && $.isNumeric($('>td:nth-child(1)', $(this)).html())) {
                emails.push($('>td:nth-child(8)', $(this)).html() + "|" + $('>td:nth-child(1)', $(this)).html());
            }
        });
    } else if ($("#tab5").hasClass("active")) {
        $('#tbRenovacao > tbody  > tr').each(function () {
            if ($('>td:nth-child(8)', $(this)).html() !== "" && $.isNumeric($('>td:nth-child(1)', $(this)).html())) {
                emails.push($('>td:nth-child(8)', $(this)).html() + "|" + $('>td:nth-child(1)', $(this)).html());
            }
        });
    }
    return emails;
}

function listaSMS() {
    var sms = [];
    if ($("#tab1").hasClass("active") || $("#tab2").hasClass("active")) {
        $('#tbReceita > tbody  > tr').each(function () {
            if ($('>td:nth-child(7)', $(this)).html() !== "" && $.isNumeric($('>td:nth-child(1)', $(this)).html())) {
                sms.push($('>td:nth-child(7)', $(this)).html() + "|" + $('>td:nth-child(1)', $(this)).html());
            }
        });
    } else if ($("#tab4").hasClass("active")) {
        $('#tbFiltroAv > tbody  > tr').each(function () {
            if ($('>td:nth-child(7)', $(this)).html() !== "" && $.isNumeric($('>td:nth-child(1)', $(this)).html())) {
                sms.push($('>td:nth-child(7)', $(this)).html() + "|" + $('>td:nth-child(1)', $(this)).html());
            }
        });
    } else if ($("#tab5").hasClass("active")) {
        $('#tbRenovacao > tbody  > tr').each(function () {
            if ($('>td:nth-child(7)', $(this)).html() !== "" && $.isNumeric($('>td:nth-child(1)', $(this)).html())) {
                sms.push($('>td:nth-child(7)', $(this)).html() + "|" + $('>td:nth-child(1)', $(this)).html());
            }
        });
    }
    return sms;
}

function statusConversao(result) {
    $('#tbStatusVis').DataTable({
        "aaData": result.aaData,
        "iDisplayLength": -1,
        "ordering": false,
        "bFilter": false,
        "bInfo": false,
        "bLengthChange": false,
        "bPaginate": false,
        "fnInitComplete": function (oSettings, json) {
            trataCarregamentoPaginaStatus();
        }
    });
}

function statusBolsasEativos(result) {
    $('#tbStatusAtivo').DataTable({
        "aaData": result.aaData,
        "iDisplayLength": -1,
        "ordering": false,
        "bFilter": false,
        "bInfo": false,
        "bLengthChange": false,
        "bPaginate": false,
        "fnInitComplete": function (oSettings, json) {
            trataCarregamentoPaginaStatus();
        }
    });
}

function statusAlunos(result) {
    $('#tbStatusNRR').DataTable({
        "aaData": result.aaData,
        "iDisplayLength": -1,
        "ordering": false,
        "bFilter": false,
        "bInfo": false,
        "bLengthChange": false,
        "bPaginate": false,
        "fnInitComplete": function (oSettings, json) {
            trataCarregamentoPaginaStatus();
        }
    });
}

function novoStatus(result) {
    $('#tbStatusAlunos').DataTable({
        "aaData": result.aaData,
        "iDisplayLength": -1,
        "ordering": false,
        "bFilter": false,
        "bInfo": false,
        "bLengthChange": false,
        "bPaginate": false,
        "fnDrawCallback": function () {
            var txtTotal = 0;
            $('#tbStatusAlunos > tbody  > tr').each(function () {
                txtTotal = txtTotal + parseFloat($('>td:nth-child(2)', $(this)).html());
            });
            $("#lblTotalStatusAlunos").html(txtTotal);
        },
        "fnInitComplete": function (oSettings, json) {
            trataCarregamentoPaginaStatus();
        }
    });
}