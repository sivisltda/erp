$("#btnfind_vendas").click(function () {
    refresh_vendas();
});

$('#tbVendas').dataTable({
    "iDisplayLength": -1,
    "ordering": false,
    "bFilter": false,
    "bInfo": false,
    "bLengthChange": false,
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": finalFind_vendas(),
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "fnDrawCallback": function () {
        refreshTotal_vendas();
    },
    "sPaginationType": "full_numbers"
});

function refresh_vendas() {
    var tblVendas = $('#tbVendas').dataTable();
    tblVendas.fnReloadAjax(finalFind_vendas());
}

function finalFind_vendas() {
    return "../../../Modulos/Estoque/Vendas_server_processing.php?filial=" +
    parent.$("#txtLojaSel").val() + "&imp=1&" + 
    ($("#txtNome").length > 0 ? "tp=0&cli=&td=" : "id=5&tp=null&com=") + $("#txtId").val() +
    "&dti=" + $("#txt_dt_begin_vendas").val() + '&dtf=' + $("#txt_dt_end_vendas").val();
}

function refreshTotal_vendas() {
    $('#lblTotParc2_vendas').html('Número de Vendas :<strong>' + ($('#qtdparc').val() !== undefined ? $('#qtdparc').val() : "0") + '</strong>').show('slow');
    $('#lblTotParc3_vendas').html('Total em Vendas :<strong>' + numberFormat($('#totparc').val(), 1) + '</strong>').show('slow');
    $('#lblTotParc4_vendas').html('Ticket Médio :<strong>' + numberFormat(($('#totparc').val() / $('#qtdparc').val()), 1) + '</strong>').show('slow');
}