$("#btnfind").click(function () {
    tbLista.fnReloadAjax(finalFind(0));
});

function finalFind(imp) {
    var retPrint = (imp === 1 ? '&imp=1' : '?imp=0');
    if ($("#txtStatus").val() !== "null") {
        retPrint = retPrint + '&at=' + $("#txtStatus").val();
    }
    if ($("#txtBusca").val() !== "") {
        retPrint = retPrint + '&txtBusca=' + $("#txtBusca").val();
    }  
    return "ajax/Credenciais_server_processing.php" + retPrint.replace(/\//g, "_");
}

function AbrirBox(id) {
    abrirTelaBox("CredenciaisForm.php" + (id > 0 ? "?id=" + id : ""), 342, 450);
}

function FecharBox() {
    $("#newbox").remove();
    tbLista.fnReloadAjax(finalFind(0));
}

function imprimir() {
    var pRel = "&NomeArq=" + "Credenciais" +
            "&lbl=Código|Descrição|Horário|Ambiente|Dias" +
            "&siz=50|245|175|180|50" +
            "&pdf=5" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + $("#txtStatus option:selected").text() + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + "../Modulos/Academia/" + finalFind(1); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
}

var tbLista = $('#tblLista').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bFilter": false,
    "aaSorting": [[1, 'asc']],
    "aoColumns": [
        {"bSortable": false},        
        {"bSortable": true},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false}],
    "sAjaxSource": finalFind(0),
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

function TeclaKey(event) {
    if (event.keyCode === 13) {
        $("#btnfind").click();
    }
}

function RemoverItem(id) {
    if (confirm("Deseja deletar esse registro?")) {
        $.post("form/CredenciaisFormServer.php", {List: true, bntDelete: true, txtId: id}).done(function (data) {
            tbLista.fnReloadAjax(finalFind(0));
        });
    }
}

function atualizarLista(){
  tbLista.fnReloadAjax(finalFind(0));
}
