var clickTabPIX = 0;
$(document).ready(function () {
    $("#txtDescPix").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "./../CRM/ajax.php",
                dataType: "json",
                data: {
                    q: request.term,
                    t: "C"
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            $('#txtIdNotaPix').val(ui.item.id).show();
            $('#txtDescPix').val(ui.item.label).show();
        }
    });
    $("#txtDescPix").prop('disabled', true);
    carregaTabPix();
    $("#txtTipoClientePix").change(function () {
        if ($(this).val() === "1") {
            $("#txtDescPix").prop('disabled', false);
        } else {
            $("#txtDescPix").prop('disabled', true);
            $("#txtIdNotaPix").val("");
            $("#txtDescPix").val("");
        }
    });

    $("#ckb_data_pgtoPix").change(function () {
        if ($(this).is(":checked")) {
            $("#txtTipoPgtoPix").prop('disabled', false);
            $("#txtDataIniPix").prop('disabled', false);
            $("#txtDataFinPix").prop('disabled', false);
            $("#txtDataIniPix").val(moment().startOf('month').format(lang["dmy"]));
            $("#txtDataFinPix").val(moment().endOf('month').format(lang["dmy"]));
        } else {
            $("#txtTipoPgtoPix").prop('disabled', true);
            $("#txtDataIniPix").prop('disabled', true);
            $("#txtDataFinPix").prop('disabled', true);
            $("#txtDataIniPix").val("");
            $("#txtDataFinPix").val("");
        }
    });
});

function carregaTabPix() {
    if (clickTabPIX === 1) {
        return;
    }
    clickTabPIX = 1;
    $("#loader").show();
    $('#tbListaPIX').dataTable({
        "iDisplayLength": 20,
        "bProcessing": true,
        "bServerSide": true,
        "ordering": false,
        "bFilter": false,
        "bLengthChange": false,
        "sAjaxSource": final_url_Pix(0),
        'oLanguage': {
            'oPaginate': {
                'sFirst': "Primeiro",
                'sLast': "Último",
                'sNext': "Próximo",
                'sPrevious': "Anterior"
            }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
            'sLengthMenu': "Visualização de _MENU_ registros",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers",
        "fnDrawCallback": function () {
            $("#loader").hide();
        }
    });
}

function refreshTablePix() {
    $("#loader").show();
    var tblrefresh = $("#tbListaPIX").dataTable();
    tblrefresh.fnReloadAjax(final_url_Pix(0));
}

function final_url_Pix(imp) {
    return "./../../util/dcc/PIX_server_processing.php?imp=" + imp +
            "&fil=" + $('#txtLojaSel').val() +            
            "&tcl=" + $("#txtTipoClientePix").val() +
            "&cli=" + $("#txtIdNotaPix").val() +            
            "&dck=" + ($("#ckb_data_pgtoPix").is(':checked') ? 1 : 0) +            
            "&dtp=" + $("#txtTipoPgtoPix").val() +
            "&din=" + $("#txtDataIniPix").val() +
            "&dfn=" + $("#txtDataFinPix").val() +
            "&sts=" + $("#txtTipoPix").val();
}

function printPix(tp) {
    var pRel = "&NomeArq=" + "Transações em PIX" +
            "&lbl=" + "Dt.Mens.|Mat.|Cliente|Pedido|Transação|Valor Mens.|Dt.Pgto." +
            "&siz=" + "55|45|190|170|120|60|60" +
            "&pdf=" + "7|8" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + "" + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + final_url_Pix(1).replace("./../../util", "./").replace("?", "&"); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tp + "&PathArq=GenericModelPDF.php", '_blank');
}

function ExportarExcelPix() {
    printPix("E");
}

function ler_saldoPix() {
    $.getJSON("./../../util/dcc/saldoDCC.php", {ajax: "true"}, function (j) {
        $('#saldoImpPix').val(j);
        $('#saldoBtnPix').hide();
        $('#saldoImpPix').show();
    });
}

function sendTablePix() {
    if ($("#txtTipoPix").val() !== "2") {
        bootbox.alert("Opção válida apenas para os itens NÃO GERADOS!");
    } else {
        bootbox.confirm('Confirma o envio dos Pix não envidos?', function (result) {
            if (result === true) {
                $.post(final_url_Pix(1)).done(function (data) {
                    var response = jQuery.parseJSON(data);
                    $("#source").dialog({modal: true});
                    $("#progressbar").progressbar({value: 0});
                    $("#pgIni").html("0");
                    $("#pgFim").html(response.aaData.length);
                    $("#txtTotalSucesso, #txtTotalErro").val(0);
                    processarPix(response.aaData);
                });
            }
        });
    }
}

function processarPix(lista) {
    var PgIni = parseInt($("#pgIni").html());
    var PgFim = parseInt($("#pgFim").html());
    var lote = "";
    if ((PgIni < PgFim) && PgFim > 0 && $('#source').dialog('isOpen')) {
        for(i = PgIni; i < PgIni + 1; i++) {
            if($.isArray(lista[i])){
                var IdChk = lista[i][0].split("<input name=\"nEnviadoB[]\" type=\"hidden\" value=\"");                
                IdChk = IdChk[1].split("\"/>");
                if($.isNumeric(IdChk[0])) {
                    lote = lote + IdChk[0] + ",";
                }
            }
        }
        lote = lote.substring(0,(lote.length - 1));
        $("#progressbar").progressbar({value: (((PgIni + lote.split(",").length) * 100) / PgFim)});
        $("#pgIni").html(PgIni + lote.split(",").length);
        $.ajax({
            type: "GET", url: "./../Sivis/ws_atualizabanco.php", 
            data: {onLinePix: 'S', IdMens: lote}, dataType: "json",
            success: function (data) {
                if (data.MensagemDCC === "") {
                    $("#txtTotalSucesso").val(parseInt($("#txtTotalSucesso").val()) + parseInt(data.TotalSucesso));
                    $("#txtTotalErro").val(parseInt($("#txtTotalErro").val()) + parseInt(data.TotalErro));
                    processarPix(lista);
                } else {
                    bootbox.alert(data.MensagemDCC + "!");
                    refreshTablePix();
                }
            },
            error: function (error) {
                bootbox.alert(error.responseText + "!");
                refreshTablePix();
            }                        
        });
    } else {
        $("#source").dialog('close');
        bootbox.alert("Transações: " + $("#txtTotalSucesso").val() + " Aprovada(s) e " + $("#txtTotalErro").val() + " com erro(s)!");
        refreshTablePix();
    }
}

function sendEmailPix() {
    if ($("#txtTipoPix").val() !== "1") {
        bootbox.alert("Opção válida apenas para os itens GERADOS!");
    } else {    
        $.ajax({
            type: "GET", url: "./../Academia/form/ClientesPlanosServer.php", 
            data: {getEmailBoleto: 'S'}, dataType: "json",
            success: function (data) {
                bootbox.prompt({
                    title: "Selecione o modelo de E-mail",
                    inputType: 'select',
                    inputOptions: data,
                    callback: function (result) {
                        if (result) {                        
                            $.post(final_url_Pix(1)).done(function (data) {
                                var response = jQuery.parseJSON(data);
                                $("#source").dialog({modal: true});
                                $("#progressbar").progressbar({value: 0});
                                $("#pgIni").html("0");
                                $("#pgFim").html(response.aaData.length);
                                $("#txtTotalSucesso, #txtTotalErro").val(0);
                                sendEmailrow(result, response.aaData);
                            });
                        }
                    }
                }); 
            },
            error: function (error) {
                bootbox.alert(error.responseText + "!");
            }                        
        });     
    }
}