var clickTabAcesso = 0;

function carregaTabAcesso() {
    if (clickTabAcesso === 1) {
        return;
    }
    clickTabAcesso = 1;
    $("#loader").show();
    totalFimCarregamento = 0;
    $("#txtTpUsuario").trigger("change");
    AtualizaGraficosAcesso();
}

function trataCarregamentoPaginaAcessos() {
    totalFimCarregamento = totalFimCarregamento + 1;
    if (totalFimCarregamento === 4) {
        $("#loader").hide();
    }
}

function montaUrl(tipo) {
    var url = "ajax/InformacoesGerenciaisAcessos_server_processing.php";
    if (tipo === 'N') {
        url = "ajax/Acessos_server_processing.php";
    }
    url += "?tipo=" + tipo;
    url += "&txtTpUser=" + $("#txtTpUsuario").val();
    url += "&txtTpAcesso=" + $("#txtTipoAcesso").val();
    url += "&txtFilial=" + $("#txtLojaSel").val();    
    if ($("#txtTpUsuario").val() !== "T" && $("#txtIdUsuario").val() !== "") {
        url += "&txtIdUser=" + $("#txtIdUsuario").val();
    }
    if ($("#txtDtInicioAcesso").val() !== "") {
        url += "&txtDtHrInicial=" + $("#txtDtInicioAcesso").val().replace(/\//g, "_") + " 00:00";
    }
    if ($("#txtDtFimAcesso").val() !== "") {
        url += "&txtDtHrFinal=" + $("#txtDtFimAcesso").val().replace(/\//g, "_") + " 23:59";
    }
    if ($("#mscAmbiente").val() !== null) {
        url += "&txtAmbiente=" + $("#mscAmbiente").val();
    }
    if ($("#mscCatraca").val() !== null) {
        url += "&txtCatraca=" + $("#mscCatraca").val();
    }
    if ($("#mscGrupoAce").val() !== null) {
        url += "&txtGrupo=" + $("#mscGrupoAce").val();
    }
    return url;
}

function AtualizaGraficosAcesso() {
    $.getJSON(montaUrl('T'), function (data) {
        chartTurnoAcesso.dataProvider = data;
        chartTurnoAcesso.validateData();
        trataCarregamentoPaginaAcessos();
    });

    $.getJSON(montaUrl('S'), function (data) {
        chartStatusAcesso.dataProvider = data;
        chartStatusAcesso.validateData();
        trataCarregamentoPaginaAcessos();
    });

    $.getJSON(montaUrl('H'), function (data) {
        chartHorario.dataProvider = data;
        chartHorario.validateData();
        trataCarregamentoPaginaAcessos();
    });

    $("#tblAcessos").dataTable().fnDestroy();
    var tblAcessos = $('#tblAcessos').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": montaUrl("N"),
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers",
        "fnInitComplete": function (oSettings, json) {
            trataCarregamentoPaginaAcessos();
        }
    });
}

$("#txtTpUsuario").change(function (event) {
    $("#txtAcessoDesc, #txtIdUsuario").val("");
    $("#txtAcessoDesc").parent().hide();
    if ($("#txtTpUsuario").val() === "C" || $("#txtTpUsuario").val() === "E" || $("#txtTpUsuario").val() === "P") {
        $("#txtAcessoDesc").parent().show();
    }
});

$(function () {
    $("#txtAcessoDesc").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "./../CRM/ajax.php",
                dataType: "json",
                data: {
                    q: request.term,
                    t: $("#txtTpUsuario").val()
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            $("#txtIdUsuario").val(ui.item.id);
        }
    });
});

var chartHorario = AmCharts.makeChart("chartHorario", {
    "type": "serial",
    "theme": "light",
    "marginRight": 40,
    "marginLeft": 40,
    "autoMarginOffset": 20,
    "valueAxes": [{
            "id": "v1",
            "axisAlpha": 0,
            "position": "left",
            "ignoreAxisWidth": true
        }],
    "balloon": {
        "borderThickness": 1,
        "shadowAlpha": 0
    },
    "graphs": [{
            "id": "g1",
            "balloon": {
                "drop": true,
                "adjustBorderColor": false,
                "color": "#ffffff"
            },
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "bulletSize": 5,
            "hideBulletsCount": 50,
            "lineThickness": 2,
            "useLineColorForBulletBorder": true,
            "valueField": "valor",
            "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
        }],
    "categoryField": "hora",
    "categoryAxis": {
        "dashLength": 1,
        "minorGridEnabled": true,
        "labelRotation": 45
    },
    "export": {
        "enabled": true
    }
});

var chartTurnoAcesso = AmCharts.makeChart("chartTurnoAcesso", {
    "type": "pie",
    "theme": "light",
    "valueField": "acesso",
    "titleField": "turno",
    "legend": {
        "align": "center",
        "position": "bottom",
        "markerType": "circle",
        "labelText": "[[title]]",
        "valueText": "",
        "fontSize": 12
    },
    "marginBottom": 0,
    "pullOutRadius": 10,
    "autoMargins": false,
    "labelText": "",
    "balloonText": "[[title]]: [[percents]]% Total:[[acesso]]", "export": {
        "enabled": true,
        "libs": {"path": "http://www.amcharts.com/lib/3/plugins/export/libs/"}
    }
});

var chartStatusAcesso = AmCharts.makeChart("chartStatusAcesso", {
    "type": "pie",
    "theme": "light",
    "valueField": "acesso",
    "titleField": "status",
    "legend": {
        "align": "center",
        "position": "bottom",
        "markerType": "circle",
        "labelText": "[[title]]",
        "valueText": "",
        "fontSize": 12
    },
    "marginBottom": 0,
    "pullOutRadius": 10,
    "autoMargins": false,
    "labelText": "",
    "balloonText": "[[title]]: [[percents]]% Total:[[acesso]]", "export": {
        "enabled": true,
        "libs": {"path": "http://www.amcharts.com/lib/3/plugins/export/libs/"}
    }
});

function printAcessos() {
    var pRel = "&NomeArq=" + "Lista de Acessos" +
            "&lbl=" + "Mat.|Nome|Data/Hora|Tipo Acesso|Motivo|Ambiente|Catraca" +
            "&siz=" + "50|150|100|100|100|100|100" +
            "&pdf=" + "20" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + "Alunos " + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + "../Modulos/Academia/" + montaUrl("N").replace("?", "&"); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
}

function ExportarExcelAcesso() {
    var link = montaUrl('N');
    window.open(link + "&ex=1");
}
