$(document).ready(function () {
    $("#txtValor").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"], centsLimit: parseInt(parent.$("#txtMnDesconDecimal").val())});
    $("#txtQtdRegra").val("0");

    $("#mscPlanos").multiSelect({
        selectableHeader: "<div class='multipleselect-header'>Selecione Modalidades</div>",
        selectionHeader: "<div class='multipleselect-header'>Modalidades Selecionadas</div>"
    });
    
    $("#mscServicos").multiSelect({
        selectableHeader: "<div class='multipleselect-header'>Selecione Servicos</div>",
        selectionHeader: "<div class='multipleselect-header'>Servicos Selecionadas</div>"
    });

    if ($('#txtDescricao').attr("disabled") === "disabled" || !$("#txtConvEspecifico").is(":checked")) {
        $('#ms-mscPlanos, #ms-mscServicos').find('li').addClass("disabled");
    }

    $("#txtDescricao").focus();

    if ($('#txtId').val() !== '') {
        listRegras($('#txtId').val());
        $('[id="rConvTipo"]').trigger("change");
    }

    $("#btnTpValor").click(function () {
        if ($("#tp_sinal").html() === "%") {
            $("#tp_sinal").html("$");
            $("#txtSinal").val("D");
        } else {
            $("#tp_sinal").html("%");
            $("#txtSinal").val("P");
        }
    });

    if ($("#txtSinal").val() === "D") {
        $("#tp_sinal").html("$");
    } else {
        $("#tp_sinal").html("%");
    }

    $('[id="rConvTipo"]').change(function () {
        if ($("#rConvTipo:checked").val() === "1") {
            $("#divQtdMin").hide();
        } else {
            $("#divQtdMin").show();
        }
    });

    $('#txtConvEspecifico').change(function () {
        if ($("#txtConvEspecifico").is(":checked")) {
            $('#ms-mscPlanos, #ms-mscServicos').find('li').removeClass("disabled");
        } else {
            $('#ms-mscPlanos, #ms-mscServicos').find('li').addClass("disabled");
        }
    });
});

function addRegra() {
    if (($("#rConvTipo:checked").val() === "1" && $("#txtValor").val() !== "") ||
            ($("#rConvTipo:checked").val() === "0" && ($("#txtValor").val() !== "" && $("#txtQtdMin").val() !== ""))) {
        if (($("#rConvTipo:checked").val() === "1" && $("#divRegras").find('div').text().length <= 0) || $("#rConvTipo:checked").val() === "0") {
            addLinhaRegra($("#txtValor").val(), $("#txtSinal").val(), $("#txtQtdMin").val(), "0");
        } else {
            bootbox.alert("Para o tipo convênio somente é possível adicionar um valor!");
        }
    } else {
        bootbox.alert("Preencha as informações corretamente!");
    }
}

function addLinhaRegra(valor, tpValor, qtdMin, id) {
    var idLinhaRegra = eval($('#txtQtdRegra').val()) + 1;
    var btExcluir = "";
    if ($('#txtDescricao').attr("disabled") !== "disabled") {
        btExcluir = '<div class="btnExcluir" style="width:5%; padding:4px 0 3px; float:right;cursor: pointer;" onClick="return removeLinha(\'tabela_linha_' + idLinhaRegra + '\');"><span class="ico-remove" style="font-size:20px"></span></div>';
    }
    var tpSimb = '%';
    if (tpValor === 'D') {
        tpSimb = '$';
    }
    var linha = "";
    linha = '<div id="tabela_linha_' + idLinhaRegra + '" style="padding:5px; border-bottom:1px solid #DDD;height: 25px;overflow: hidden;">\n\
                <input id="txtIdRegra_' + idLinhaRegra + '" name="txtIdRegra_' + idLinhaRegra + '" value="' + id + '" type="hidden"></input>\n\
                <input id="txtValRegra_' + idLinhaRegra + '" name="txtValRegra_' + idLinhaRegra + '" value="' + valor + '" type="hidden"></input>\n\
                <div style="width:38%; padding-left:1%; line-height:27px; float:left">' + valor + " - " + tpSimb + '</div>\n\
                <input id="txtTpValor_' + idLinhaRegra + '" name="txtTpValor_' + idLinhaRegra + '" value="' + tpValor + '" type="hidden"></input>\n\
                <input id="txtMinRegra_' + idLinhaRegra + '" name="txtMinRegra_' + idLinhaRegra + '" value="' + qtdMin + '" type="hidden"></input>\n\
                <div style="width:32%; padding-left:1%; line-height:27px; float:left">' + qtdMin + '</div>\n\
                ' + btExcluir + '\n\
            </div>';
    $("#divRegras").prepend(linha);
    $("#txtQtdRegra").val(idLinhaRegra);
    $("#txtValor, #txtQtdMin").val("0");
}

function removeLinha(id) {
    $("#" + id).remove();
}

function listRegras(id) {
    $("#divRegras").html("");
    $.getJSON('form/ConveniosFormServer.php', {listRegras: id}, function (j) {
        for (var i = 0; i < j.length; i++) {
            addLinhaRegra(j[i].valor, j[i].tipo, j[i].numero_minimo, j[i].id);
        }
    });
}

function validaForm() {
    var retorno = true;
    if ($("#txtDescricao").val() === "") {
        bootbox.alert("Descrição é Obrigatória!");
        retorno = false;
    }
    if ($("#divRegras").find('div').text().length <= 0) {
        bootbox.alert("Valor é Obrigatório!");
        retorno = false;
    }
    if (retorno) {
        $("#btnSave").click();
    }
}

