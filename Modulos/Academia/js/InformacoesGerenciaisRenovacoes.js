var clickTabRenovacoes = 0;

function carregaTabRenovacoes() {
    if (clickTabRenovacoes === 1) {
        return;
    }
    clickTabRenovacoes = 1;
    $("#loader").show();
    totalFimCarregamento = 0;
    $('#tbRenovacao').DataTable({
        "iDisplayLength": -1,
        "ordering": false,
        "bFilter": false,
        "bInfo": false,
        "sAjaxSource": final_url_Renovacoes(0),
        "bLengthChange": false,
        "bPaginate": false,
        "oLanguage": {
        'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "fnInitComplete": function (oSettings, json) {
            trataCarregamentoRenovacoes();
        },
        "footerCallback": function (row, data, start, end, display) {
            var api = this.api(), data;
            $(api.column(8).footer()).html(end);
        }
    });
    makeChartRenovacoes();
}

function trataCarregamentoRenovacoes() {
    totalFimCarregamento = totalFimCarregamento + 1;
    if (totalFimCarregamento === 2) {
        $("#loader").hide();
    }
}

function final_url_Renovacoes(id) {
    return "ajax/InformacoesGerenciaisRenovacoes_server_processing.php?id=" + id +
            "&tpd=" + $("#txtTipoData").val() +
            "&loj=" + $("#txtLojaSel").val() +
            "&sta=" + $("#mscStatRen").val() +
            "&dti=" + $("#txtInicioContDe").val() + "&dtf=" + $("#txtInicioContAte").val() +
            "&idi=" + $("#txtFimContDe").val() + "&idf=" + $("#txtFimContAte").val() +            
            "&grp=" + $("#txtGrupoRen").val() +            
            "&rep=" + $("#txtConsulResp").val() +
            "&prf=" + $("#txtProfResp").val() +
            "&tip=" + $("#txtTipoCont").val() +
            "&pla=" + $("#txtPlano").val() +
            "&crd=" + $("#txtCredenciaisRen").val() +            
            "&vli=" + $("#txtValPlanoDe").val() +
            "&vlf=" + $("#txtValPlanoAte").val() +
            "&per=" + $("#txtPeriodo").val() +
            "&car=" + ($('#ckbValidadeCartao').attr('checked') ? "1" : "0") +
            "&mpl=" + ($('#ckbMaisPlanos').attr('checked') ? "1" : "0") +
            "&tms=" + $("#txtTipoMens").val() +
            "&tmd=" + $("#txtTipoMensData").val() +
            "&mdi=" + $("#txtFimMensDe").val() +
            "&mdf=" + $("#txtFimMensAte").val() +
            "&tit=" + $("#txtTitular").val() +
            "&status=" + $("#txtStatus").val();
}

function BuscarFilRenovacoes() {
    $("#loader").show();
    var tblrefresh = $("#tbRenovacao").dataTable();
    tblrefresh.fnReloadAjax(final_url_Renovacoes(1), function () {
        $("#loader").hide();
    }, null);
}

function makeChartRenovacoes() {
    var q = 0;
    var chartData9 = [];
    $.getJSON(final_url_Renovacoes(2), function (data) {
        $.each(data["aaData"], function (key, val) {
            chartData9[q] = {desc: val[0], valor: val[1]};
            q++;
        });
    }).done(function () {
        AmCharts.makeChart("chartdiv9", {
            "type": "serial",
            "theme": "light",
            "dataProvider": chartData9,
            "graphs": [{
                    "balloonText": "Contratos em [[category]]: [[value]]",
                    "fillAlphas": 1,
                    "lineAlpha": 0.2,
                    "title": "Contratos",
                    "type": "column",
                    "valueField": "valor"
                }],
            "rotate": false,
            "categoryField": "desc",
            "categoryAxis": {
                "gridPosition": "start",
                "labelRotation": 20,
                "fillAlpha": 0.05,
                "position": "left"
            }, "export": {
                "enabled": true,
                "libs": {"path": "http://www.amcharts.com/lib/3/plugins/export/libs/"}
            }
        });
        trataCarregamentoRenovacoes();
    });
}

function printRenovacoes(tp) {
    var pRel = "&NomeArq=" + "Renovações" + "&pOri=L" +
            "&lbl=" + "Mat.|Nome|Período|Status|Plano|Telefone|Celular|Email|Consultor" + (tp === "E" ? "|CPF/CNPJ|Valor|Dt.Nascimento|Estado Civil|Dt.Inicio|ISS Retido|Grupo|Convênio" : "") +
            "&siz=" + "40|200|150|95|100|80|80|160|100" + (tp === "E" ? "|80|80|80|80|80|80|100|100" : "") +
            "&pdf=" + "17" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + "Alunos " + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + "../Modulos/Academia/" + final_url_Renovacoes(1).replace("?", "&"); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tp + "&PathArq=GenericModelPDF.php", '_blank');
}

$("#txtTipoCont").change(function () {
    if ($('#txtTipoCont').val() === "4") {
        ajusteTela(true, numberFormat(0), '');
        $('#divPlano').hide();
        $('#divCredenciais').show();
    } else {
        ajusteTela(false, numberFormat(0));
        $('#divPlano').show();
        $('#divCredenciais').hide();
    }
});

$("#txtTipoData").change(function () {
    if ($('#txtTipoData').val() === "1") {
        $("#txtFimContDe").prop('disabled', true);
        $("#txtFimContAte").prop('disabled', true);
        $("#txtPeriodoTipo").html("Período de Compra (de ~ até):");
    } else if ($('#txtTipoData').val() === "2") {
        $("#txtFimContDe").prop('disabled', true);
        $("#txtFimContAte").prop('disabled', true);
        $("#txtPeriodoTipo").html("Período de Cadastro (de ~ até):");        
    } else {
        $("#txtFimContDe").prop('disabled', false);
        $("#txtFimContAte").prop('disabled', false);
        $("#txtPeriodoTipo").html("Início do Contrato (de ~ até):");
    }
});

$("#txtTipoMensData").change(function () {
    if ($('#txtTipoMensData').val() === "2") {
        $('#txtTipoMens').val("2");
        $('#txtTipoMens').attr("disabled", true);
    } else {        
        $('#txtTipoMens').val("null");
        $('#txtTipoMens').attr("disabled", false);
    }
});

function ajusteTela(disable, val, valimpo) {
    $("#txtValPlanoDe").prop('disabled', disable);
    $("#txtValPlanoAte").prop('disabled', disable);
    $("#txtTipoMensData").prop('disabled', disable);
    $("#txtTipoMens").prop('disabled', disable);
    $("#txtFimMensDe").prop('disabled', disable);
    $("#txtFimMensAte").prop('disabled', disable);
    $("#txtPlano").prop('disabled', disable);
    $("#txtPeriodo").prop('disabled', disable);
    $("#txtTitular").prop('disabled', disable);    
    $('#txtPeriodo').select2().enable = !disable;
    $("#txtValPlanoDe").val(val);
    $("#txtValPlanoAte").val(val);
}
