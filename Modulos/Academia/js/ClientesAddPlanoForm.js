$(document).ready(function () {
    $("#txtDtInicio").mask(lang["dateMask"]);
});

function iniciaAddPlanoForm() {

    if ($("#tpTela").val() === 'A') {
        $(".frmtext").text("Adicionar Contrato");
        $("#txtPlanos").select2('enable');
        $('#tblPlanosClientes', window.parent.document).find('td.selected').removeClass('selected');
        $("#txtDtInicio").val(moment().format(lang["dmy"]));
    } else {
        $(".frmtext").text("Renovar Contrato");
        $("#txtPlanos").select2('disable');
    }

    $.getJSON("form/ClientesPlanosServer.php", {listPlanos: $("#txtId", window.parent.document).val(), tpTela: $("#tpTela").val(), idItem: $("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected)').find('.renovar').attr('data-id')}, function (j) {
        var idsel = "0";
        var options = "<option value=\"0\">Selecione o Plano</option>";
        if (j !== null && j.length > 0) {
            for (var i = 0; i < j.length; i++) {
                options = options + "<option value=\"" + j[i].id + "\">" + j[i].descricao + "</option>";
                if ($("#tpTela").val() === "R") {
                    idsel = j[i].id;
                    if (j[i].inativa === '1') {
                        bootbox.alert('Não é possivel renovar pois o plano está inativo.', function () {
                            parent.FecharBox(0);
                        });
                    }
                    if (j[i].id_agendamento !== "1900-01-01" && j[i].id_agendamento.length === 10) {
                        $("#lblCancelamento").html("Cancelamento agendado para: " + moment(j[i].id_agendamento, "YYYY-MM-DD").format(lang["dmy"]));
                        $("#txtCancelamento").val(j[i].id_agendamento.substring(0, 4) + j[i].id_agendamento.substring(5, 7) + j[i].id_agendamento.substring(8, 10));
                    }
                }
            }
        }
        $("#txtPlanos").html(options);
        $("#txtPlanos").select2("val", idsel);
        atualizaValores($('#txtPlanos').val());
    }).done(function (data) {
        if ($("#tpTela").val() !== 'A') {
            $.getJSON("form/ClientesPlanosServer.php", {getDtRenovacao: $("#txtId", window.parent.document).val(), idPlano: $("#txtPlanos").val()}, function (j) {
                if (j.length > 0) {
                    $("#txtDtInicio").val(j[0].dtRenovacao);
                } else {
                    $("#txtDtInicio").val(moment().format(lang["dmy"]));
                }
                refreshDias();                
            });
        } else {
            $("#txtDtInicio").val(moment().format(lang["dmy"]));
            refreshDias();            
        }
    });

    $('#txtPlanos').change(function () {
        atualizaValores($('#txtPlanos').val());
    });

    var tblValores = $('#tblValores').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        bPaginate: false, bFilter: false, bInfo: false, bSort: false,
        "sAjaxSource": "form/ClientesPlanosServer.php?listValores=0",
        "scrollY": "120px",
        "scrollCollapse": true,
        'oLanguage': {
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sZeroRecords': "Não foi encontrado nenhum resultado"
        }
    });

    function atualizaValores(id) {
        tblValores.fnReloadAjax("form/ClientesPlanosServer.php?listValores=" + id + 
        "&txtIdPlano=" + $("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected)').find('.renovar').attr('data-id'));
        $("#txtHorarios").select2("val", "");
        $.getJSON("form/ClientesPlanosServer.php", {listHorarios: "S", idPlano: id}, function (j) {
            var options = "<option value=\"\">Selecione o Horário</option>";
            if (j !== null) {
                if (j.length > 0) {
                    for (var i = 0; i < j.length; i++) {
                        options = options + "<option value=\"" + j[i].cod_turno + "\">" + j[i].nome_turno + "</option>";
                    }
                }
                $("#txtHorarios").html(options);
                if (j.length === 1) {
                    $("#txtHorarios").select2("val", j[0].cod_turno);
                }
            }
        });
    }

    $('#tblValores tbody').on('click', 'tr', function () {
        if ($(this).find('td').hasClass('selected')) {
            $(this).find('td').removeClass('selected');
        } else {
            $('#tblValores').find('td.selected').removeClass('selected');
            $(this).find('td').addClass('selected');
        }
        calcularData();
    });

    $('#btnGravarPlano').click(function (e) {
        var dtAntPlano = "";
        var dtAtual = moment().format('YYYYMMDD');
        var dtInicio = moment($("#txtDtInicio").val(), lang["dmy"]).format('YYYYMMDD');
        var dtFim = moment($("#txtDtInicio").val(), lang["dmy"]).add(1, 'month').format('YYYYMMDD');
        if ($("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected) >td:nth-child(4) ').text() !== "") {
            var dtAntPlano = moment($("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected) >td:nth-child(4) ').text(), lang["dmy"]).format('L');
        }
        if ($('tr:has(td.selected)').find('input').attr('data-id') === undefined || $("#txtPlanos").val() === "0" || $("#txtHorarios").val() === "" || $("#txtDtInicio").val() === "") {
            $("iFrame").contents().find(".bootbox-alert").css("top", "100%");
            bootbox.alert(" Todos os campos são obrigatórios!");
        } else if (dtAntPlano !== "" && (dtInicio < dtAntPlano)) {
            bootbox.alert("Data de Inicio não pode ser inferior ao término da data da última renovação!");
        } else if ($("#txtCancelamento").val() !== "" && ($("#txtCancelamento").val() < dtFim)) {
            bootbox.alert("Não é possivel renovar pois o plano possui um agendamento de cancelamento ativo neste período!");
        } else {
            if (dtInicio < dtAtual) {
                bootbox.confirm('Data de ínicio informada é anterior a data atual, Confirma a inclusão?', function (result) {
                    if (result === true) {
                        $("#btnGravarPlano").attr("disabled", true);
                        salvaPlano();
                    } else {
                        return;
                    }
                });
            } else {
                $("#btnGravarPlano").attr("disabled", true);
                salvaPlano();
            }
        }
    });

    function salvaPlano() {        
        $.post("form/ClientesPlanosServer.php", ($("#txtAjustarPeriodo").length > 0 ? "addMensalidade=S" : "addContrato=S") + 
                "&txtId=" + $("#txtId", window.parent.document).val() +
                "&tpTela=" + $("#tpTela").val() +
                "&descr=CONTRATO " + $("#txtPlanos").select2('data').text +
                "&txtProduto=" + $("#txtPlanos").val() +
                "&sys=" + parent.$("#system").val() +
                "&txtDiaPadrao=" + $("#txtDiaPadrao").val() +                
                "&txtAjustarPeriodo=" + $("#txtAjustarPeriodo").val() +
                "&txtAjustarValor=" + $("#txtAjustarValor").val() +
                "&txtIdPlano=" + $("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected)').find('.renovar').attr('data-id') +
                "&txtHorarios=" + $("#txtHorarios").val() + "&txtIdParc=" + $('tr:has(td.selected)').find('input').attr('data-id') +
                "&txtDtIni=" + $("#txtDtInicio").val().replace(/\//g, "_")).done(function (data) {
            if (!isNaN(data.trim())) {
                $('#txtIdSel', window.parent.document).val(data.trim());
                parent.AtualizaListPlanosClientes();
                $("#txtDtInicio").val("");
                $("#txtPlanos, #txtIdParc").select2("val", "");
                $('#tblValores').find('td.selected').removeClass('selected');
                parent.FecharBox(0);
            }
        });
    }
}
