
function TeclaKey(event) {
    if (event.keyCode === 13) {
        $("#btnfind").click();
    }
}

function finalFind(imp) {
    var retPrint = "";
    retPrint = '?imp=0&list=true';
    if (imp === 1) {
        retPrint = '?imp=1&list=true';
    }
    if ($("#txtBuscar").val() !== "") {
        retPrint = retPrint + '&Search=' + $("#txtBuscar").val();
    }
    return retPrint.replace(/\//g, "_");
}

function AbrirBox(id, tp) {
    if (id > 0) {
        var myId = "?id=" + id;
    } else {
        var myId = "";
    }
    abrirTelaBox("DocumentosForm.php" + myId, 640, 550);
}

function imprimir() {
    var pRel = "&NomeArq=" + "Documentos" +
            "&lbl=Descrição|Validade|Serviço|Bloqueio" +
            "&siz=280|100|220|100" +
            "&pdf=4" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + "../Modulos/Academia/ajax/DocsList_server_processing.php" + finalFind(1).replace("?", "&"); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
}

$(document).ready(function () {
    listaTable();
    $("#btnfind").click(function () {
        $("#tblDocumentos").dataTable().fnDestroy();
        listaTable();
    });


});

function listaTable() {
    $("#tblDocumentos").dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "bFilter": false,
        "aoColumns": [{"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": false},
            {"bSortable": false}],
        "sAjaxSource": "ajax/DocsList_server_processing.php" + finalFind(0),
        "oLanguage": {
            "oPaginate": {
                "sFirst": "Primeiro",
                "sLast": "Último",
                "sNext": "Próximo",
                "sPrevious": "Anterior"
            }, "sEmptyTable": "Não foi encontrado nenhum resultado",
            "sInfo": "Visualização do registro _START_ ao _END_ de _TOTAL_",
            "sInfoEmpty": "Visualização do registro 0 ao 0 de 0",
            "sInfoFiltered": "(filtrado de _MAX_ registros totais)",
            "sLengthMenu": "Visualização de _MENU_ registros",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sSearch": "Pesquisar:",
            "sZeroRecords": "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });
}


function FecharBox() {
    var oTable = $("#tblDocumentos").dataTable();
    oTable.fnDraw(false);
    $("#newbox").remove();
}

function RemoverItem(id) {
    bootbox.confirm('Deseja deletar esse registro?', function (result) {
        if (result === true) {
            $.post("ajax/DocsList_server_processing.php",
                    {bntDelete: true, txtId: id}).done(function (data) {
                $("#tblDocumentos").dataTable().fnDestroy();
                listaTable();
            });
        } else {
            return;
        }
    });
}

function buscar() {
    $.get("ajax/DocsList_server_processing.php",
            {txtBusca: $('#txtBuscar').val(), Search: $('#txtBuscar').val()}).done(function (data) {
        $("#tblDocumentos").dataTable().fnDestroy();
        listaTable();
    });
}
