var clickTabContratos = 0;
var selecionadoContrato;
var tblrefresh;

function BuscarContratos() {
    clickTabContratos = 0;
    carregaTabContratos();
}

function carregaTabContratos() {
    if (clickTabContratos === 1) {
        return;
    }
    clickTabContratos = 1;
    totalFimCarregamento = 0;        
    if ($("#tbReceita").length > 0) {
        $('#tbReceita').DataTable({
            "iDisplayLength": -1,
            "ordering": false,
            "bFilter": false,
            "bInfo": false,
            "destroy": true,
            "sAjaxSource": final_url_Contratos(2, "-1"),
            "bLengthChange": false,
            "bPaginate": false,
            'oLanguage': {
                'sEmptyTable': "Não foi encontrado nenhum resultado",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sZeroRecords': "Não foi encontrado nenhum resultado"},
            "fnDrawCallback": function (data) {
                $("#tbReceita tfoot tr th")[1].innerText = data["aoData"].length; 
            },            
            "fnInitComplete": function (oSettings, json) {
                trataCarregamentoPaginaContratos('#tbReceita');
            }
        });
    }
    makeChartContratoM();
    handleLegendClickM("");
}

function trataCarregamentoPaginaContratos(tipo) {
    totalFimCarregamento = totalFimCarregamento + 1;
    console.log(totalFimCarregamento + "|" + tipo);
    if (totalFimCarregamento === ($("#tbReceita").length + 2)) {     
        totalFimCarregamento = 0;        
        console.log("end");
        $("#loader").hide();
    } else {
        console.log("ini");        
        $("#loader").show();
    }    
}

function final_url_Contratos(forma, filtro) {
    var retPrint = "?fr=" + forma + "&fil=" + filtro + "&filial=" + $('#txtLojaSel').val();
    if ($("#statusContratos").val() === '1') {
        retPrint = retPrint + '&sttus=S';
    }
    if ($("#mscGrupo").val() !== null) {
        var grupos = JSON.stringify($("#mscGrupo").val());
        retPrint = retPrint + "&grp=" + grupos;
    }
    return "ajax/InformacoesGerenciaisContratos_server_processing.php" + retPrint;
}

function makeChartContratoM() {
    var m = 0;
    var chartData5 = [];
    $.getJSON(final_url_Contratos(0, ''), function (data) {
        $.each(data["aaData"], function (key, val) {
            chartData5[m] = {desc: val[0], valor: val[1]};
            m++;
        });
    }).done(function () {
        var chart = AmCharts.makeChart("chartdiv5", {
            "type": "serial",
            "theme": "light",
            "dataProvider": chartData5,
            "titles": [{"text": "Contratos","size": 15}],
            "graphs": [{
                "balloonText": "Contratos em [[category]]: [[value]]",
                "fillAlphas": 1,
                "lineAlpha": 0.2,
                "title": "Contratos",
                "type": "column",
                "valueField": "valor"
            }],
            "rotate": true,
            "categoryField": "desc",
            "categoryAxis": {
                "gridPosition": "start",
                "fillAlpha": 0.05,
                "position": "left",
                "listeners": [{
                    "event": "clickItem",
                    "method": function (item) {
                        handleLegendClickM(item.value);
                    }
                }]
            }, "export": {
                "enabled": true,
                "libs": {"path": "http://www.amcharts.com/lib/3/plugins/export/libs/"}
            }
        });
        trataCarregamentoPaginaContratos('#makeChartContratoM');
    });
}

function makeChartContratoP(tipo) {
    var n = 0;
    var chartData6 = [];
    $.getJSON(final_url_Contratos(1, encodeURIComponent(tipo)), function (data) {
        $.each(data["aaData"], function (key, val) {
            chartData6[n] = {desc: val[0], valor: val[1]};
            n++;
        });
    }).done(function () {
        AmCharts.makeChart("chartdiv6", {
            "type": "pie",
            "theme": "light",
            "dataProvider": chartData6,
            "valueField": "valor",
            "titleField": "desc",
            "titles": [{"text": "Prazo " + (tipo === "" ? " dos Contratos" : " de " + tipo),"size": 15}],
            "legend": {
                "align": "center",
                "position": "bottom",
                "markerType": "circle",
                "labelText": "[[title]]",
                "valueText": "",
                "fontSize": 12,
                "clickLabel": handleLegendClickP
            },
            "marginBottom": 0,
            "pullOutRadius": 10,
            "autoMargins": false,
            "labelText": "",
            "balloonText": "[[title]]: [[percents]]% Total:[[value]]", "export": {
                "enabled": true,
                "libs": {"path": "http://www.amcharts.com/lib/3/plugins/export/libs/"}
            }
        });
        if (tipo === "") {
            trataCarregamentoPaginaContratos('#makeChartContratoP');
        }
    });
}

function handleLegendClickM(graph) { //Quando clicar no Nome do Plano
    makeChartContratoP(graph);
    if ($("#mscGrupo").val() === null && graph === "") {
        graph = "-1";    
    }
    tblrefresh = $("#tbReceita").dataTable();
    tblrefresh.fnReloadAjax(final_url_Contratos(2, encodeURIComponent(graph)));
    selecionadoContrato = 2 + '/' + encodeURIComponent(graph);    
    $("#tbReceita").attr("filtro", final_url_Contratos(2, encodeURIComponent(graph)));
}

function handleLegendClickP(graph) { //Quando clicar no Gráfico de Pizza
    tblrefresh = $("#tbReceita").dataTable();
    tblrefresh.fnReloadAjax(final_url_Contratos(3, graph.title));
    selecionadoContrato = 3 + '/' + graph.title;
    $("#tbReceita").attr("filtro", final_url_Contratos(3, graph.title));
}

function printStatus() {
    var pRel = "&NomeArq=" + "Contratos de Alunos" +
            "&lbl=" + "Mat.|Nome|Dt.Nasc|Sexo|Status|Telefone|Celular|Email" +
            "&siz=" + "30|200|50|30|80|80|80|150" +
            "&pdf=" + "20" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + "Alunos " + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + "../Modulos/Academia/" + $("#tbReceita").attr("filtro").replace("?", "&"); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
}

function ExportarExcelCnttSts() {
    if (($("#tab2").hasClass('active')) === true) {
        var quebrado = selecionadoContrato.split("/");
        window.open(final_url_Contratos(quebrado[0], quebrado[1]) + "&ex=1");
    } else {
        var quebrado = selecionadoStatus.split("/");
        window.open(final_url_Status(quebrado[0], quebrado[1]) + "&ex=1");
    }
}
