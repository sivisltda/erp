var clickTabDCC = 0;
$(document).ready(function () {
    $("#txtDesc").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "./../CRM/ajax.php",
                dataType: "json",
                data: {
                    q: request.term,
                    t: "C"
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            $('#txtIdNota').val(ui.item.id).show();
            $('#txtDesc').val(ui.item.label).show();
        }
    });
    $("#txtDesc").prop('disabled', true);
    carregaTabDCC();
    $("#txtTipoCliente").change(function () {
        if ($(this).val() === "1") {
            $("#txtDesc").prop('disabled', false);
        } else {
            $("#txtDesc").prop('disabled', true);
            $("#txtIdNota").val("");
            $("#txtDesc").val("");
        }
    });

    $("#ckb_data_pgto").change(function () {
        if ($(this).is(":checked")) {
            $("#txtTipoPgto").prop('disabled', false);
            $("#txtDataIniDcc").prop('disabled', false);
            $("#txtDataFinDcc").prop('disabled', false);
            $("#txtDataIniDcc").val(moment().format(lang["dmy"]));
            $("#txtDataFinDcc").val(moment().format(lang["dmy"]));
        } else {
            $("#txtTipoPgto").prop('disabled', true);
            $("#txtDataIniDcc").prop('disabled', true);
            $("#txtDataFinDcc").prop('disabled', true);
            $("#txtDataIniDcc").val("");
            $("#txtDataFinDcc").val("");
        }
    });
});

function carregaTabDCC() {
    if (clickTabDCC === 1) {
        return;
    }
    clickTabDCC = 1;
    $("#loader").show();
    $('#tbListaDCC').dataTable({
        "iDisplayLength": 20,
        "bProcessing": true,
        "bServerSide": true,
        "ordering": false,
        "bFilter": false,
        "bLengthChange": false,
        "sAjaxSource": final_url_dcc(0),
        'oLanguage': {
            'oPaginate': {
                'sFirst': "Primeiro",
                'sLast': "Último",
                'sNext': "Próximo",
                'sPrevious': "Anterior"
            }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
            'sLengthMenu': "Visualização de _MENU_ registros",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers",
        "fnDrawCallback": function () {
            pos_processamento();
        }
    });
}

function refreshTableDcc() {
    $("#loader").show();
    var tblrefresh = $("#tbListaDCC").dataTable();
    tblrefresh.fnReloadAjax(final_url_dcc(0));
}

function sendTableDcc() {
    var comparingDate = false;
    var strData = moment($("#txtDataFinDcc").val(), lang["dmy"]);
    if (strData.diff(moment(), 'days') > 0) {
        comparingDate = true;
    }
    if ($("#txtTipo").val() !== "2" || comparingDate === true) {
        bootbox.alert("Opção válida apenas para os itens Não Enviados e com Data final menor ou igual a data Atual!");
    } else {
        bootbox.confirm('Confirma o envio das transações não envidas?', function (result) {
            if (result === true) {
                $.post(final_url_dcc(1)).done(function (data) {
                    var response = jQuery.parseJSON(data);
                    $("#source").dialog({modal: true});
                    $("#progressbar").progressbar({value: 0});
                    $("#pgIni").html("0");
                    $("#pgFim").html(response.aaData.length);
                    $("#txtTotalSucesso, #txtTotalErro").val(0);
                    processarDcc(response.aaData);
                });
            }
        });
    }
}

function final_url_dcc(imp) {
    return "./../../util/dcc/DCC_server_processing.php?imp=" + imp +
            "&fil=" + $('#txtLojaSel').val() +
            "&tcl=" + $("#txtTipoCliente").val() +
            "&cli=" + $("#txtIdNota").val() +
            "&car=" + $("#txtCartaoDCC").val() +
            "&dck=" + ($("#ckb_data_pgto").is(':checked') ? 1 : 0) +
            "&dtp=" + $("#txtTipoPgto").val() +
            "&din=" + $("#txtDataIniDcc").val() +
            "&dfn=" + $("#txtDataFinDcc").val() +
            "&cri=" + $("#txtNumCart01").val() +
            "&crf=" + $("#txtNumCart04").val() +
            "&sts=" + $("#txtTipo").val();
}

function ler_saldo() {
    $.getJSON("./../../util/dcc/saldoDCC.php", {ajax: "true"}, function (j) {
        $('#saldoImp').val(j);
        $('#saldoBtn').hide();
        $('#saldoImp').show();
    });
}

function pos_processamento() {
    $("#loader").hide();
    $('#lbl_dcc_total').html("0");
    $('#lbl_dcc_vbruto').html(numberFormat(0));
    $('#lbl_dcc_vliquido').html(numberFormat(0));
    $('#lbl_dcc_total').html($('#txt_dcc_total').val());
    $('#lbl_dcc_vbruto').html($('#txt_dcc_vbruto').val());
    $('#lbl_dcc_vliquido').html($('#txt_dcc_vliquido').val());
}

function printDCC() {
    var pRel = "&NomeArq=" + "Transações em DCC" +
            "&lbl=" + "Dt.Mens.|Matricula|Cartão|Transação|Cartão|Status|Valor|Usuário" +
            "&siz=" + "55|55|190|120|60|80|70|70" +
            "&pdf=" + "3|8|9|10" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + "Alunos " + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + final_url_dcc(1).replace("./../../util", "./").replace("?", "&"); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
}

function AbrirBoxDCC(id) {
    $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:420px; height:400px; position:absolute; top:39%; left:58%; margin-left:-358px; margin-top:-106px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:400px' src='/modulos/academia/FormCCVencidos.php?id=" + id + "'></iframe></div></div></div></div></div>");
}

function ExportarExcelDCC() {
    window.open(final_url_dcc(1) + "&ex=1");
}

function processarDcc(lista) {
    var PgIni = parseInt($("#pgIni").html());
    var PgFim = parseInt($("#pgFim").html());
    var lote = "";
    if ((PgIni < PgFim) && PgFim > 0 && $('#source').dialog('isOpen')) {
        for(i = PgIni; i < PgIni + 10; i++) {
            if($.isArray(lista[i])){
                var IdChk = lista[i][0];
                IdChk = IdChk.substring(0,(IdChk.length - 10)).replace("<input name=\"nEnviado[]\" type=\"hidden\" value=\"","").replace("\"/>","");
                if($.isNumeric(IdChk)) {
                    lote = lote + IdChk + ",";
                }
            }
        }
        lote = lote.substring(0,(lote.length - 1));
        $("#progressbar").progressbar({value: (((PgIni + lote.split(",").length) * 100) / PgFim)});
        $("#pgIni").html(PgIni + lote.split(",").length);
        $.ajax({
            type: "POST", url: "../../../util/dcc/makeLoteDCC.php",
            data: "GeraDCC=S&Mens=" + lote, dataType: "json",
            success: function (data) {
                if (data.MensagemDCC === "") {
                    $("#txtTotalSucesso").val(parseInt($("#txtTotalSucesso").val()) + parseInt(data.TotalSucesso));
                    $("#txtTotalErro").val(parseInt($("#txtTotalErro").val()) + parseInt(data.TotalErro));
                    processarDcc(lista);
                } else {
                    bootbox.alert(data.MensagemDCC + "!");
                    refreshTableDcc();
                }
            },
            error: function (error) {
                bootbox.alert(error.responseText + "!");
                refreshTableDcc();
            }
        });
    } else {
        $("#source").dialog('close');
        bootbox.alert("Transações: " + $("#txtTotalSucesso").val() + " Aprovada(s) e " + $("#txtTotalErro").val() + " com erro(s)!");
        refreshTableDcc();
    }
}
