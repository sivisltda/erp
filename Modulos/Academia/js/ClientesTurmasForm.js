function iniciaTurmasForm() {
    $.getJSON("../Academia/form/ClientesPlanosServer.php", {listPlanosCliente: $("#txtIdAluno").val(), isTurma: 'S'}, function (j) {
        var options = "";
        if (j !== null) {
            if (j.length > 0) {
                for (var i = 0; i < j.length; i++) {
                    options = options + "<option value=\"" + j[i].id_plano + "\" id_prod='" + j[i].produto + "'>" + j[i].descricao + "</option>";
                }
            }
        }
        $("#txtPlano").html(options);
        let plano = $("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected)').find('.detalhe').attr('data-id');
        //console.log(plano);
        $("#txtPlano").select2("val", plano); //$('#txtPlano option:nth-child(1)').val()
        $('#tblTurmas').dataTable({
            "iDisplayLength": 20,
            "aLengthMenu": [20, 30, 40, 50, 100],
            "bProcessing": true,
            "bServerSide": true,
            bPaginate: false, bFilter: false, bInfo: false, bSort: false,
            "sAjaxSource": "ajax/Turmas_server_processing.php?imp=1&isCliente=S&plano=" + $('#txtPlano option:selected').attr('id_prod'),
            "scrollY": "120px",
            "scrollCollapse": true,
            'oLanguage': {
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sZeroRecords': "Não foi encontrado nenhum resultado"
            }
        });
    });

    $('#txtPlano').change(function () {
        atualizaTurmas();
    });
    listTurmaAluno();
}

function matricularAluno(idTurma, vagas) {
    if (vagas === 0) {
        bootbox.alert("Turma não possui vaga!");
        return;
    } else if ($('#txtTurma').val() === "") {
        bootbox.alert("Selecione uma turma!");
        return;
    }
    bootbox.confirm('Confirma a inclusão do aluno na turma?', function (result) {
        if (result === true) {
            $("#loader").show();
            $.post("../Academia/form/TurmasFormServer.php", "isAjax=S&addAlunoTurma=" + $("#txtIdAluno").val() + "&idTurma=" + idTurma + "&idPlano=" + $('#txtPlano').val()).done(function (data) {
                if (data.trim() === "TURMA") {
                    bootbox.alert("Aluno já esta matriculado na turma!");
                } else if (data.trim() === "YES") {
                    atualizaTurmaAluno();
                    $("#txtTurma").trigger("change");
                } else {
                    bootbox.alert("Erro ao adicionar o aluno!" + data.trim());
                }
                atualizaTurmas();
                $("#loader").hide();
            });
        }
    });
}

function atualizaTurmas() {
    var tblTurmas = $('#tblTurmas').dataTable();
    tblTurmas.fnReloadAjax("ajax/Turmas_server_processing.php?imp=1&isCliente=S&plano=" + $('#txtPlano option:selected').attr('id_prod'));
}

function atualizaTurmaAluno() {
    var tblTurmasAluno = $('#tblTurmasAluno').dataTable();
    tblTurmasAluno.fnReloadAjax("../Academia/form/TurmasFormServer.php?isAjax=S&listTurmaAluno=" + $("#txtIdAluno").val());
}

function listTurmaAluno() {
    $('#tblTurmasAluno').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        bPaginate: false, bFilter: false, bInfo: false, bSort: false,
        "sAjaxSource": "../Academia/form/TurmasFormServer.php?isAjax=S&listTurmaAluno=" + $("#txtIdAluno").val(),
        "scrollY": "120px",
        "scrollCollapse": true,
        'oLanguage': {
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sZeroRecords': "Não foi encontrado nenhum resultado"
        }
    });
}

function desmatricularTurma(idTurma, idAluno) {
    bootbox.confirm('Confirma a exclusão do aluno desta turma?', function (result) {
        if (result === true) {
            bootbox.prompt("Informe o motivo da desmatrícula!", function (result) {
                if (result === "" || result === null) {
                    result = "SEM MOTIVO";
                }
                $.post("../Academia/form/TurmasFormServer.php", "isAjax=S&excluirAlunoTurma=" + idAluno + "&idTurma=" + idTurma + "&txtMotivo=" + result).done(function (data) {
                    if (data.trim() === "YES") {
                        atualizaTurmaAluno();
                        $("#txtTurma").trigger("change");
                    } else {
                        bootbox.alert("Erro ao remover aluno!" + data.trim());
                    }
                    atualizaTurmas();
                    $("#loader").hide();
                });
            });
        }
    });
}