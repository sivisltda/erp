var validado = false;
$(document).ready(function () {
    $('#txtNome').ajaxComboBox('../../Modulos/CRM/ClientesAjaxCombobox.php', {
        plugin_type: 'simple',
        lang: 'pt-br',
        primary_key: 'id',
        bind_to: 'foo'
    }).bind('foo', function () {
        window.location = 'ClientesForm.php?id=' + $('#txtNome_primary_key').val();
    });       

    $("#txtValCredito").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});    
    $("#txtQtdItensPagar").val("0");
    $("#txtTotalSemDesconto").val("0");
    
    if ($('#txtId').val() !== "") {
        listContatos($('#txtId').val());
        listDCC($('#txtId').val());
        tbListaDCC();        
        listPlanosClientes();
        listHistoricoServicos();
        listHistoricoBoleto();
        listHistoricoLink();
        listPagamentosDependentes();
        listServicoFavorito();
        atualizaTotCredito();
        PagarTudoItens();        
    }
});

function AbrirBox(opc, iddesc, idprpd) {
    if (opc === 1) {
        abrirTelaBox("ClientesPagamentoForm.php", 536, 800);
    }
    if (opc === 2) {
        if (document.querySelectorAll("a[href='#tab0']").length > 0) {
            $(document.querySelectorAll("a[href='#tab0']")).click();
            AbrirBoxVeiculo(0, $('#txtId').val());
        } else {
            $("#divPagamentos").hide();
            abrirTelaBox("ClientesAddPlanosForm.php?tp=A", 435, 770);    
        }
    }
    if (opc === 3) {
        $("#divPagamentos").hide();
        abrirTelaBox("ClientesAddPlanosForm.php?tp=R", 435, 770);
    }
    if (opc === 4) {
        abrirTelaBox("ClientesCongelamentoForm.php", 438, 770);
    }
    if (opc === 5) {
        AtualizaPagPlanosClientes(0);
        abrirTelaBox("ClientesPlanosDetForm.php", 410, 670);
    }
    if (opc === 6) {
        $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:540px; height:210px; position:absolute; top:50%; left:50%; margin-left:-290px; margin-top:-282px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:210px' src='ClientesTransferenciaForm.php'></iframe></div></div></div></div></div>");
    }
    if (opc === 7 && $("#txtPlanoEF").val() !== "") {
        abrirTelaBox("ClientesLiderForm.php?idForm=" + opc, 438, 580);
    }
    if (opc === 8 && $("#txtPlanoEF").val() !== "") {
        abrirTelaBox("ClientesLiderForm.php?idForm=" + opc, 438, 580);
    }
    if (opc === 9) {
        abrirTelaBox("ClientesHistoricoForm.php", 500, 770);
    }
    if (opc === 10) {
        document.location = "ClientesWebCam.php?id=" + $("#txtId").val();
    }
    if (opc === 11) {
        abrirTelaBox("ClientesCropModalForm.php", 539, 770);
    }
    /*if (opc === 12) {
        abrirTelaBox("ClientesPagamentoForm.php?isDCC=S", 536, 800);
    }
    if (opc === 13) {
        abrirTelaBox("ClientesPagamentoForm.php?isDCC=C", 536, 800);
    }*/
    if (opc === 14) {
        abrirTelaBox("ClientesAgendamentoDccForm.php", 400, 430);
    }
    if (opc === 15) {
        abrirTelaBox("ClientesTurmasForm.php?idAluno=" + $("#txtId").val() + "&nmAluno=" + $("#txtNome").val(), 531, 770);
    }
    if (opc === 16) {
        abrirTelaBox("/modulos/academia/form/formLiberaRecebimento.php?txtIdx=8&bntFind=S&txtQualificacao=", 360, 600);
    }
    if (opc === 17) {
        abrirTelaBox("/modulos/academia/form/login_desc.php?idDesc=" + iddesc + "&idProd=" + idprpd + "&tp=D", 350, 400);
    }
    if (opc === 18) {
        abrirTelaBox("/modulos/academia/form/login_desc.php?idDesc=" + iddesc + "&idProd=" + idprpd + "&tp=C", 350, 400);
    }
}

$('#btnNovo').click(function (e) {
    if($("#txtProspectIr").val() === "1") {
        window.location = './../CRM/ProspectsForm.php';
    } else {
        window.location = "ClientesForm.php";
    }
});

$('#btnSave').click(function (e) {   
    if (validaForm()) {
        $("#loader").show();
        $("#btnSave").attr("disabled", true); 
        $("#btnSave").hide();
        $.post("form/ClientesFormServer.php", "isAjax=S" + 
            "&btnSave=S" + 
            "&txtId=" + $("#txtId").val() + 
            "&txtNome=" + $("#txtNome").val() + 
            "&txtDtCad=" + $("#txtDtCad").val() + 
            "&" + $("#tab1 :input").serialize() + 
            "&" + $("#tab8 :input").serialize()
        ).done(function (data) {            
            if(!isNaN(data)){
                window.location = "ClientesForm.php?id=" + data;
            }else{
                bootbox.alert(data);
                $("#btnSave").attr("disabled", false);        
                $("#btnSave").show();
                $("#loader").hide();
            }
        });
    }    
});

$('#btnExcluir').click(function (e) {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {            
            $.post("form/ClientesFormServer.php", "isAjax=S&btnExcluir=S&txtId=" + $("#txtId").val()).done(function (data) {
                window.location = "../Comercial/Gerenciador-Clientes.php";
            });            
        } else {
            return;
        }
    });
});