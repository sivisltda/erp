
function TeclaKey(event) {
    if (event.keyCode === 13) {
        $("#btnfind").click();
    }
}

$(document).ready(function () {
    $.getJSON("../Academia/form/HorariosFormServer.php", {isAjax:"S", listHorarios: "S"}, function (j) {
        var options = "<option value=\"\">Selecione</option>";
        if(j !== null ){
            if(j.length > 0){
                for (var i = 0; i < j.length; i++) {
                    options = options + "<option value=\""+ j[i].cod_turno+"\">" + j[i].nome_turno + "</option>";
                }
            }
            $("#txtHorario").html(options);
        }
    });

    $.getJSON("../Academia/form/ClientesFormServer.php", {isAjax:"S", listProfessor: "S"}, function (j) {
        var options = "<option value=\"\">Selecione</option>";
        if(j !== null ){
            if(j.length > 0){
                for (var i = 0; i < j.length; i++) {
                    options = options + "<option value=\""+ j[i].id_fornecedores_despesas+"\">" + j[i].razao_social + "</option>";
                }
            }
            $("#txtProfessor").html(options);
        }
    });

    $.getJSON("../Estoque/form/PlanosFormServer.php", {isAjax:"S", listPlanos: "S"}, function (j) {
        var options = "<option value=\"\">Selecione</option>";
        if(j !== null ){
            if(j.length > 0){
                for (var i = 0; i < j.length; i++) {
                    options = options + "<option value=\""+ j[i].conta_produto+"\">" + j[i].conta_produto + " - " + j[i].descricao + "</option>";
                }
            }
            $("#txtPlanos").html(options);
        }
    });

    $('#tblTurmas').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "bFilter": false,
        "aoColumns": [{"bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": false }] ,
        "sAjaxSource": "ajax/Turmas_server_processing.php"+finalFind(0),
        'oLanguage': {
            'oPaginate': {
                'sFirst': "Primeiro",
                'sLast': "Último",
                'sNext': "Próximo",
                'sPrevious': "Anterior"
            }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
            'sLengthMenu': "Visualização de _MENU_ registros",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });

    $("#btnfind").click(function(){
        var tblTurmas = $('#tblTurmas').dataTable();
        tblTurmas.fnReloadAjax("ajax/Turmas_server_processing.php"+finalFind(0));
    });
});

function listaFiltro() {
    var filtros = "";
    if($("#txtHorario").val() >= "0"){
        filtros += "   Horário: "+$("#txtHorario option:selected").text();
    }
    if($("#txtProfessor").val() >= "0"){
        filtros += "   Professor: " + $("#txtProfessor option:selected").text();
    }
    if($("#txtPlanos").val() >= "0"){
        filtros += "   Plano: "+$("#txtPlanos option:selected").text();
    }
    if($("#txtStatus").val() !== ""){
        filtros += "   Status: "+$("#txtStatus option:selected").text();
    }
    return filtros;
}

function imprimir(tipo) {
    var pRel = "&NomeArq=" + "Turmas" +
        "&lbl=Codigo|Nome da Turma|Max Alunos|Vagas|Matriculados|Inicio Em|Horário" + 
        (tipo === "E" ? "|Professor|Ambiente|Status|Criada Em|Desmatricular Automático|Em Dias|Planos" : "") +
        "&siz=50|195|75|75|75|80|150" + (tipo === "E" ? "|150|100|50|50|50|50|150" : "") +
        "&pdf=" + (tipo === "E" ? "15" : "8") + // Colunas do server processing que não irão aparecer no pdf
        "&filter=" + listaFiltro() + //Label que irá aparecer os parametros de filtro
        "&PathArqInclude=" + "../Modulos/Academia/ajax/Turmas_server_processing.php" + finalFind(1).replace("?","&"); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel+ "&tpImp=" + tipo + "&PathArq=GenericModelPDF.php", '_blank');
}

function finalFind(imp) {
    var retPrint = "";
    retPrint = '?imp=0';
    if(imp === 1){ retPrint = '?imp=1'; }
    if($("#txtBusca").val() !== "") { retPrint = retPrint + '&txtBusca=' + $("#txtBusca").val();}
    if($("#txtHorario").val()!== ""){ retPrint = retPrint + '&horario='+$("#txtHorario").val(); }
    if($("#txtProfessor").val()!== ""){ retPrint = retPrint + '&professor='+$("#txtProfessor").val(); }
    if($("#txtPlanos").val()!== ""){ retPrint = retPrint + '&plano='+$("#txtPlanos").val(); }
    if($("#txtStatus").val()!== ""){ retPrint = retPrint + '&status='+$("#txtStatus").val(); }
    return retPrint.replace(/\//g, "_");
}

function RemoverItem(idTurma){
    bootbox.confirm('Confirma a exclusão da turma?', function (result) {
        if (result === true) {
            $("#loader").show();
            $.post("form/TurmasFormServer.php", "isAjax=S&removerTurma="+idTurma).done(function (data) {
                if(data.trim() === "YES"){
                    var tblTurmas = $('#tblTurmas').dataTable();
                    tblTurmas.fnReloadAjax("ajax/Turmas_server_processing.php"+finalFind(0));
                } else {
                    bootbox.alert("Erro ao excluir. Remova todos os alunos cadastrados na turma e da lista de espera!");
                }
                $("#loader").hide();
            });
        }
    });
}

function AbrirBox(id){
    if(id === -1){
        window.location = './../Academia/MapaTurmas.php';
    }else if(id > 0){
        window.open('./../Academia/TurmasForm.php?id='+id, '_blank');
    }else{
        window.location = './../Academia/TurmasForm.php';
    }
}

function imprimirListaAlunos(tipo){
    var pRel = "&NomeArq=" + "Alunos Turmas" +
        "&lbl=Cod Aluno|Nome Aluno|Contato" + (tipo === "E" ? "|Email|Cod Turma|Turma|Cep|Logradouro|Numero|Bairro|Estado|Cidade|Complemento": "") +
        "&siz=100|400|200" + (tipo === "E" ? "|200|50|200|50|200|50|100|50|100|50" : "") +
        "&pdf=3" + // Colunas do server processing que não irão aparecer no pdf
        "&filter=" + "Turmas" +  //Label que irá aparecer os parametros de filtro
        "&PathArqInclude=" + "../Modulos/Academia/form/TurmasFormServer.php&isAjax=S&listAlunosTurma=S" + finalFind(1).replace("?","&");
    window.open("../../util/ImpressaoPdf.php?id=" + pRel+ "&tpImp=" + tipo + "&PathArq=GenericModelPDF.php", '_blank');
}

function imprimirChamadas() {
    $("#tblTurmas tbody tr").each(function(index, tr) { 
        let c = $(tr).find("td");
        let id = $(c[1])[0].innerHTML.split("AbrirBox(")[1].split(")")[0];
        let link = "../../util/ImpressaoPdf.php?id=&Fonte=7&NomeArq=Turma&" + 
        "lbl=Mat|Aluno|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31&" + 
        "siz=35|200|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15|15&pdf=32&" + 
        "filter=Turma: " + $(c[1])[0].innerText + " , " +
        "Professor: " + $(c[7])[0].innerText + ", " +
        "Horário: " + $(c[6])[0].innerText + ", " +
        "Capacidade: " + $(c[2])[0].innerText + 
        "&PathArqInclude=../Modulos/Academia/form/TurmasFormServer.php&isAjax=S&listaPresenca=S&txtId=" + id +
        "&tpImp=I&PathArq=GenericModelPDF.php";
        window.open(link, '_blank');
    });
}