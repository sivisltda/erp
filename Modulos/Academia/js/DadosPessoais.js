var mousedownHappened;

$(document).ready(function () {
    $("#txtDtCad, #txtDtNasc, #txtDtValCartao, #txtDtAdmissao, #txtDtDemissao, #txtVencCnh, #txtPriCnh").mask(lang["dateMask"]);
    $("#txtTelEmerg").mask("(99) 99999-9999");
    $("#txtCep").mask("99999-999");
    $("#txtTextoContato").mask("(99) 99999-9999");
    $('#txtQtdContatos').val("0");
    if ($("#txtDtNasc").val() !== '') {
        $("#txtIdade").val(moment().diff(moment($("#txtDtNasc").val(), lang["dmy"]), 'years'));
    } else {
        $("#txtIdade").val('');
    }
    TrataEnableCampos();
    cpfCnpj();    
    $('#txtTextoContato').blur(function () {
        $("#txtTextoContato").css("background-color", "white");
        if ($("#txtTextoContato").val() !== "") {
            $.getJSON("../../Modulos/CRM/valida_contatos.ajax.php", {txtContatos: $("#txtTextoContato").val(), txtTipo: $("#txtTipoContato").val(), ajax: "true"}, function (j) {
                if (j[0].existe > 0) {
                    $("#txtTextoContato").css("background-color", "#efff02");
                }
            });
        }
    });    
});

$('#btnAlterar').click(function (e) {
    $("#txtDisabled").val("");
    TrataEnableCampos();
});

$('#btnCancelar').click(function (e) { 
    $("#txtDisabled").val("disabled");
    TrataEnableCampos();
});

function TrataEnableCampos() {
    if ($("#txtDisabled").val() !== "") {                
        $("#divDadosPessoais :input, #txtCep, #txtNumero, #txtComplemento, #txtTextoContato, #txtRespContato, #txtObs, #txtTipoContato").attr("disabled", true);
        $("#textoArea, #txtDataHistorico, #txtHoraHistorico, #txtDataHistoricoPrev, #txtHoraHistoricoPrev, #txtComProd, #txtComServ, #txtComPlano, #txtComGere, #txtComPlanoPri, #txtComGerePri, #btnTransfPendencias, #btnTransfResponsavel, #btnTransfComissaoCliente, #btnTransfResponsavelLead").attr("disabled", true);
        $('#txtRecebeComissao, #selComProd, #selComProdTipo, #selComServ, #selComSerTipo, #selComPlano, #selComGere, #txtTransfPendencias, #txtTransfResponsavel, #txtDeHistorico, #txtTransfLead, #txtTransfComissaoCliente').select2('disable');        
        $('#txtSexo, #txtEstadoCivil, #txtPlanoSaude, #txtRegTributario, #ckbISS, #txtFilial, #txtProcedencia, #txtTipoContato, #txtUserResp, #txtProfResp, #ckbEmailMarketing, #ckbCelularMarketing, #ckbInativo, #txtGrupo, #txtTpSang, #txtEstrag, #txtJuridicoTipo, #txtfornecedorHR, #txtDepartamento').select2('disable');
        $('#ms-mscGrupos, #ms-mscEquipe, #ms-mscEquipeBlack').find('li').addClass("disabled");
        $("#txtTipoChamado, #txtParaHistorico").select2('disable');                
        $("#btnNovo, #btnAlterar, #btnExcluir").show();
        $("#btnSave, #btnCancelar").hide();
        $('.btnExcluir').hide();
    } else {
        $("#divDadosPessoais :input, #txtCep, #txtNumero, #txtComplemento, #txtTextoContato, #txtRespContato, #txtObs, #txtTipoContato").attr("disabled", false);
        $("#textoArea, #txtDataHistorico, #txtHoraHistorico, #txtDataHistoricoPrev, #txtHoraHistoricoPrev, #txtComProd, #txtComServ, #txtComPlano, #txtComGere, #txtComPlanoPri, #txtComGerePri, #btnTransfPendencias, #btnTransfResponsavel, #btnTransfComissaoCliente, #btnTransfResponsavelLead").attr("disabled", false);
        $('#txtRecebeComissao, #selComProd, #selComProdTipo, #selComServ, #selComSerTipo, #selComPlano, #selComGere, #txtTransfPendencias, #txtTransfResponsavel, #txtDeHistorico, #txtTransfLead, #txtTransfComissaoCliente').select2('enable');        
        if ($("#txtAcessoAlunBlock").val() === "1") {
            $('#ckbInativo').select2('enable');
        } else {
            $('#ckbInativo').select2('disable');
        }
        $('#txtSexo, #txtEstadoCivil, #txtPlanoSaude, #txtRegTributario, #ckbISS, #txtFilial, #txtProcedencia, #txtTipoContato, #txtUserResp, #txtProfResp, #ckbEmailMarketing, #ckbCelularMarketing, #txtGrupo, #txtTpSang, #txtEstrag, #txtJuridicoTipo, #txtfornecedorHR, #txtDepartamento').select2('enable');
        $('#ms-mscGrupos, #ms-mscEquipe, #ms-mscEquipeBlack').find('li').removeClass("disabled");
        $("#txtTipoChamado, #txtParaHistorico").select2('enable');                
        $("#btnNovo, #btnAlterar, #btnExcluir").hide();
        $("#btnSave, #btnCancelar").show();
        $('.btnExcluir').show();
    }
    if ($("#txtObs").attr("bloq") === "1") {
        $("#txtObs").attr("readonly", true);
    }
}

$('#txtJuridicoTipo').change(function () {
    cpfCnpj();
});

function cpfCnpj() {
    let d = $("#txtCpf").val().replace(/\./g, '').replace(/\-/g, '').replace(/\//g, '');
    if ($('#txtJuridicoTipo').val() === "F") {
        $("#txtCpf").mask("999.999.999-99");
        $("#lblNome").html("Nome Completo");        
        $("#txtCpf").parent().find("span").text("Documento CPF:");
        $("#txtRG").parent().find("span").text("Documento RG:");
        $("#txtDtNasc").parent().find("span").text("* Nascimento:");
        $("#txtCpf").val(d.length > 0 ? d.substring(0, 3) + "." + d.substring(3, 6) + "." + d.substring(6, 9) + "-" + d.substring(9, 11) : '');        
        $("#txtSexo, #txtTpSang, #txtEstadoCivil, #txtNmEmpresa, #txtPlanoSaude, #txtProfissao").parent().show();
        $("#txtNmFantasia, #txtInsMunicipal").parent().hide();
    } else {
        $("#txtCpf").mask("99.999.999/9999-99");
        $("#lblNome").html("Razão Social");        
        $("#txtCpf").parent().find("span").text("Documento CNPJ:");
        $("#txtRG").parent().find("span").text("Inscrição Estadual:");
        $("#txtDtNasc").parent().find("span").text("* Fundação:");        
        $("#txtCpf").val(d.length > 0 ? d.substring(0, 2) + "." + d.substring(2, 5) + "." + d.substring(5, 8) + "/" + d.substring(8, 12) + "-" + d.substring(12, 14) : '');        
        $("#txtSexo, #txtTpSang, #txtEstadoCivil, #txtNmEmpresa, #txtPlanoSaude, #txtProfissao").parent().hide();
        $("#txtNmFantasia, #txtInsMunicipal").parent().show();
    }
}

$("#txtDtNasc").change(function () {
    if ($("#txtDtNasc").val() !== '') {
        $("#txtIdade").val(moment().diff(moment($("#txtDtNasc").val(), lang["dmy"]), 'years'));
    } else {
        $("#txtIdade").val('');
    }
});

$('#txtCpf').blur(function () {
    if (mousedownHappened && validado == true) { // cancel the blur event{
        mousedownHappened = false;
    } else if (validarCPF()) {
        $("#loader").show();
        validado = false;
        $.get("../../Modulos/Academia/form/ClientesFormServer.php", "existeCPF=S&txtCPF=" + $("#txtCpf").val() + "&txtId=" + $("#txtId").val()).done(function (data) {
            var result = data.trim().split('|');
            if (result.length > 0 && result[0] === "ATIVO") {
                $("#loader").hide();
                bootbox.alert("CPF ja cadastrado para " + result[1] + "!");
            } else if (result.length > 0 && result[1] === "INATIVO") {
                $("#loader").hide();
                bootbox.confirm("CPF encontrado para " + result[1] + " um usuário excluido, reativar cadastro ?", function (result) {
                    if (result === true) {
                        $("#loader").show();
                        $.post("../../Modulos/Academia/form/ClientesFormServer.php", "isAjax=S&reativarCadastro=S&txtCPF=" + $("#txtCpf").val()).done(function (data) {
                            if (!isNaN(data.trim())) {
                                window.location = "../../Modulos/Academia/ClientesForm.php?id=" + data.trim();
                            } else {
                                bootbox.alert("Erro ao reativar cadastro");
                                $('#txtCpf').val("");
                            }
                            $("#loader").hide();
                        });
                    } else {
                        validado = true;
                        $('#txtCpf').val("");
                    }
                });
            } else {
                $("#loader").hide();
            }
        });
    }
});

function validarCPF() {
    if ($("#txtCpf").val().length >= 14 && (!validate_cpf($("#txtCpf").val()) && !validate_cnpj($("#txtCpf").val()))) {
        bootbox.alert("Documento inválido!");
        return false;
    }
    return true;
}

function validaForm() {
    if ($('#txtNome').val() === "") {
        bootbox.alert("Nome é Obrigatório!");
        return false;
    }
    if ($("#txtCep").attr("eobg") === "1" && ($("#txtEndereco").val() === "" || $("#txtNumero").val() === "" || $("#txtBairro").val() === "" || $("#txtEstado").val() === "" || $("#txtCidade").val() === "")) {
        bootbox.alert("Endereço completo é Obrigatório!");
        return false;
    }
    if ($("#txtCpf").val() !== "" && (!validate_cpf($("#txtCpf").val()) && !validate_cnpj($("#txtCpf").val()))) {
        bootbox.alert("Documento inválido!");
        return false;
    }    
    if ($("#txtDtNasc").val() === "" && $("#txtJuridicoTipo").val() === "F" && $("#txtTermometro").length === 0) {
        bootbox.alert("Data de Nascimento é Obrigatório!");
        return false;
    }
    if ($("#txtEstrag").val() === "0" && $("#txtDtNasc").val() !== "") {
        if (moment().diff(moment($("#txtDtNasc").val(), lang["dmy"]), 'years') >= textToNumber($("#txtCpf").attr("cobgLimit")) && $("#txtCpf").val() === "" && $("#txtCpf").attr("cobg") === "1") {
            bootbox.alert("Cpf Obrigatório!");
            return false;
        }
    }
    
    let tblCom = $('#tblComissao').DataTable();
    if ($("#tblComissao").attr("cobg") === "1" && $("#tblComissao").is(":visible") && tblCom.rows()[0].length === 0) {
        bootbox.alert("Preencha o comissionamento!");
        return false;
    }
    
    var email = "";
    var telefone = "";
    var celular = "";
    $("#divContatos input[name^='txtTp']").each(function (i, selected) {
        if (selected.defaultValue === "0") {
            telefone = "S";
        } else if (selected.defaultValue === "1") {
            celular = "S";
        } else if (selected.defaultValue === "2") {
            email = "S";
        }
    });
    if ($("#divContatos").attr("eobg") === "1" && email === "") {
        bootbox.alert("O campo E-mail é Obrigatório!");
        $("#loader").hide();
        $('#btnSave').show();
        return false;
    }
    if ($("#divContatos").attr("tobg") === "1" && telefone === "") {
        bootbox.alert("O campo Telefone é Obrigatório!");
        return false;
    }
    if ($("#divContatos").attr("cobg") === "1" && celular === "") {
        bootbox.alert("O campo Celular é Obrigatório!");
        return false;
    }
    if ($("#divTitularidade").attr("etit") === "1" && $("#txtTitularidade").val() === "null") {
        bootbox.alert("O campo Titularidade é Obrigatório!");
        return false;
    }
    if ($("#txtProcedencia").attr("probg") === "1" && $("#txtProcedencia").val() === "null") {
        bootbox.alert("O campo Procedência é obrigatório!");
        return false;
    }
    if ($("#txtSexo").attr("sexobg") === "1" && $("#txtSexo").val() === "" && $("#txtJuridicoTipo").val() === "F") {
        bootbox.alert("O campo Gênero é obrigatório!");
        return false;
    }
    if ($("#ckbTrancarOld").val() !== $("#ckbTrancar").val()) {
        bootbox.alert("Ao alterar o campo Desligar Titularidade, Salvar pela aba Clube!");
        return false;
    }
    return true;
}

$('#txtEstado, #txtEstRespLegal').change(function () {
    var inputNm = $(this).attr("name");
    if ($("#txtEstado").val() !== "" || $("#txtEstRespLegal").val() !== "") {
        carregaCidade($(this).val(), "", inputNm);
        if (inputNm === "txtEstado") {
            $("#txtCidade").select2("val", "");
        } else {
            $("#txtCidRespLegal").select2("val", "");
        }
    } else {
        if (inputNm === "txtEstado") {
            $('#txtCidade').html('<option value="">Selecione a cidade</option>');
        } else {
            $('#txtCidRespLegal').html('<option value="">Selecione a cidade</option>');
        }
    }
});

$("#txtCep, #txtCepRespLegal").change(function () {
    if ($("#txtCep").val() !== "" || $("#txtCepRespLegal").val() !== "") {
        var inputNm = $(this).attr("name");
        var cidade = '';
        $("#loader").show();
        $.get("https://viacep.com.br/ws/" + $(this).val() + "/json/",
                function (data) {
                    var str = data;
                    if ($(this).attr("name") === "txtCep") {
                        $("#txtEndereco, #txtBairro").attr("readonly", true);
                        $('#txtCidade, #txtEstado').select2('disable');
                    } else {
                        $("#txtEndRespLegal, #txtBairroRespLegal").attr("readonly", true);
                        $('#txtCidRespLegal, #txtEstRespLegal').select2('disable');
                    }
                    if (str.logradouro === undefined) {
                        $("#" + inputNm).val("");
                        bootbox.alert("CEP inválido!");
                    } else if (str.logradouro !== "") {
                        var estado = $("#txtEstado option[uf='" + str.uf.toUpperCase() + "']").val();
                        cidade = decodeURIComponent(str.localidade);
                        if (inputNm === "txtCep") {
                            $("#txtEndereco").val(decodeURIComponent(str.logradouro));
                            $("#txtBairro").val(decodeURIComponent(str.bairro));
                            $("#txtCidade").val(decodeURIComponent(str.localidade));
                            $("#txtEstado").select2("val", estado);
                            $("#txtNumero").focus();
                        } else {
                            $("#txtEndRespLegal").val(decodeURIComponent(str.logradouro));
                            $("#txtBairroRespLegal").val(decodeURIComponent(str.bairro));
                            $("#txtCidRespLegal").val(decodeURIComponent(str.localidade));
                            $("#txtEstRespLegal").select2("val", estado);
                            $("#txtNumRespLegal").focus();
                        }
                        carregaCidade(estado, cidade, inputNm);
                    } else if (str.logradouro === "") {
                        if (inputNm === "txtCep") {
                            $("#txtEndereco, #txtBairro, #txtNumero").val("");
                            $("#txtCidade, #txtEstado").select2("val", "");
                            $("#txtEndereco, #txtBairro").attr("readonly", false);
                            $('#txtCidade, #txtEstado').select2('enable');
                            $("#txtEndereco").focus();
                        } else {
                            $("#txtEndRespLegal, #txtBairroRespLegal, #txtNumRespLegal").val("");
                            $("#txtCidRespLegal, #txtEstRespLegal").select2("val", "");
                            $("#txtEndRespLegal, #txtBairroRespLegal").attr("readonly", false);
                            $('#txtCidRespLegal, #txtEstRespLegal').select2('enable');
                            $("#txtEndRespLegal").focus();
                        }
                    }
                }).always(function () {
            $("#loader").hide();
        });
    } else {
        if ($(this).attr("name") === "txtCep") {
            $("#txtEndereco, #txtBairro").val("");
            $("#txtCidade, #txtEstado").select2("val", "");
            $("#txtEndereco, #txtBairro").attr("readonly", false);
            $('#txtCidade, #txtEstado').select2('enable');
        } else {
            $("#txtEndRespLegal, #txtBairroRespLegal").val("");
            $("#txtCidRespLegal, #txtEstRespLegal").select2("val", "");
            $("#txtEndRespLegal, #txtBairroRespLegal").attr("readonly", false);
            $('#txtCidRespLegal, #txtEstRespLegal').select2('enable');
        }
    }
});

function carregaCidade(estado, cidade, inputName) {
    if (inputName === "txtCep" || inputName === "txtEstado") {
        $("#s2id_txtCidade").hide();
        $('#carregando').show();
    } else {
        $("#s2id_txtCidRespLegal").hide();
        $('#carregando2').show();
    }

    var idCidade = ($.isNumeric(cidade) ? cidade : "");
    $.getJSON("../../Modulos/Comercial/locais.ajax.php?search=", {txtEstado: estado, ajax: "true"}, function (j) {
        var options = "<option value=\"\">Selecione a Cidade</option>";
        if (j.length > 0) {
            for (var i = 0; i < j.length; i++) {
                if (cidade === j[i].cidade_nome) {
                    idCidade = j[i].cidade_codigo;
                }
                options = options + "<option value=\"" + j[i].cidade_codigo + "\">" + j[i].cidade_nome + "</option>";
            }
            if (inputName === "txtCep" || inputName === "txtEstado") {
                $("#txtCidade").html(options);
            } else {
                $("#txtCidRespLegal").html(options);
            }
        }
    }).always(function () {
        if (inputName === "txtCep" || inputName === "txtEstado") {
            $("#txtCidade").select2("val", idCidade);
            $("#s2id_txtCidade").show();
            $('#carregando').hide();
        } else {
            $("#txtCidRespLegal").select2("val", idCidade);
            $("#s2id_txtCidRespLegal").show();
            $('#carregando2').hide();
        }

    });
}

function alterarMask() {
    $('#txtTextoContato').val("");
    var tipo = $("#txtTipoContato option:selected").val();
    $("#txtTextoContato").unmask();
    if (tipo === "0") {
        $("#txtTextoContato").mask("(99) 9999-9999");
    } else if (tipo === "1") {
        $("#txtTextoContato").mask("(99) 99999-9999");
    }
}

function addContato() {
    if ($("#txtDisabled").val() === '') {
        var msg = $("#txtTextoContato").val();
        var sel = $("#txtTipoContato").val();
        if (msg !== "") {
            if (sel !== "2" || (sel === "2" && validateEmail($("#txtTextoContato").val()))) {
                addLinhaContato($("#txtTipoContato").val(), $("#txtTipoContato option:selected").text(), $("#txtTextoContato").val(), "0", $("#txtRespContato").val());
            } else {
                bootbox.alert("Preencha um endereço de e-mail corretamente!");
            }
        } else {
            bootbox.alert("Preencha as informações de contato corretamente!");
        }
    }
}

function abrirTelaSMS(sms, zap) {
    var link = '';
    if (zap === true) {
        link = "../CRM/SMSEnvioForm.php?zap=s";
    } else {
        link = "../CRM/SMSEnvioForm.php";
    }
    abrirTelaBox(link, 400, 400);
    $("#txtSendSms").val(sms);
}

function addLinhaContato(idTipo, tipo, conteudo, id, responsavel) {
    var idLinhaContato = eval($('#txtQtdContatos').val()) + 1;
    var btExcluir = '<div class="btnExcluir" style="width:5%; padding:4px 0 3px; float:right;cursor: pointer;" onClick="return removeLinha(\'tabela_linha_' + idLinhaContato + '\');"><span class="ico-remove" style="font-size:20px"></span></div>';
    var linha = "";
    var icoMail = "";
    if (idTipo === "2" && $("#txtReadOnly").val() === "0") {
        var pointer = ($("#txtId").val() !== "" ? "auto" : "none");
        icoMail = "<span id=\"icoEmail\" title=\"Enviar Email\" class=\"ico-envelope-3\" style=\"font-size: 18px; margin-left: 10px;vertical-align: middle;pointer-events:" + pointer + ";\" onclick=\"abrirTelaEmail('" + conteudo + "');\"></span>";
    } else if (idTipo === "3" && $("#txtReadOnly").val() === "0") {
        icoMail = "<a id=\"icoEmail\" title=\"Abrir Site\" class=\"ico-globe-3\" style=\"margin-left: 25px !important;font-size: 18px;vertical-align: middle;cursor: pointer;\" href=\"http://" + conteudo + "\" target=\"window\"></a>";
    } else if (idTipo === "1" && $("#txtId").val() !== "" && $("#txtReadOnly").val() === "0") {
        icoMail = "<span id=\"icoSms\" title=\"Enviar SMS\" class=\"ico-comment-alt\" style=\"font-size: 19px; margin-left: 7px;vertical-align: middle;cursor: pointer;\" onclick=\"abrirTelaSMS('" + conteudo + "');\"></span>";
        if ($("#txtId").val() !== "" && $("#txtId").val() !== undefined) {
            icoMail += "<img title=\"Enviar Whatsapp\" src=\"../../img/icon_zap.png\" class=\"ico-comment-alt\" style=\"font-size: 19px; width:20px;margin-left: 7px;vertical-align: middle;cursor: pointer;\" onclick=\"abrirTelaSMS('" + conteudo + "',true);\">";
        }
    }
    linha = '<div id="tabela_linha_' + idLinhaContato + '" style="padding:5px; border-bottom:1px solid #DDD;height: 25px;overflow: hidden;">\n\
                <input id="txtIdContato_' + idLinhaContato + '" name="txtIdContato_' + idLinhaContato + '" value="' + id + '" type="hidden" />\n\
                <input id="txtTpContato_' + idLinhaContato + '" name="txtTpContato_' + idLinhaContato + '" value="' + idTipo + '" type="hidden" />\n\
                <div style="width:22%; padding-left:1%; line-height:27px; float:left">' + tipo + icoMail + '</div>\n\
                <input id="txtTxContato_' + idLinhaContato + '" name="txtTxContato_' + idLinhaContato + '" value="' + conteudo + '" type="hidden" />\n\
                ' + btExcluir + '<div style="width:48%; line-height:27px; float:left">' + conteudo + '</div> \n\
                <input id="txtTxResponsavel_' + idLinhaContato + '" name="txtTxResponsavel_' + idLinhaContato + '" value="' + responsavel + '" type="hidden" />\n\
                <div style="width:22%; line-height:27px; float:left">' + responsavel + '</div> \n\
            </div>';
    $("#divContatos").prepend(linha);
    $("#txtQtdContatos").val(idLinhaContato);
    $("#txtTextoContato").val("");
    $("#txtRespContato").val("");
}

function abrirTelaEmail(email) {
    abrirTelaBox("../CRM/EmailsEnvioForm.php", 650, 720);
    $("#txtSendEmail").val(email);
}

function removeLinha(id) {
    $("#" + id).remove();
}

function listContatos(id) {
    $("#divContatos").html("");
    $.getJSON('../../Modulos/Comercial/FormGerenciador-Clientes.contatos.ajax.php?search=', {cod: id, ajax: 'true'}, function (j) {
        for (var i = 0; i < j.length; i++) {
            addLinhaContato(j[i].tipo_contato, j[i].texto_contatos, j[i].conteudo_contato, j[i].id, j[i].responsavel);
        }
    }).done(function () {
        $('.btnExcluir').hide();
    });
}

function listaEmails() {
    var emails = [];
    emails.push($("#txtSendEmail").val() + "|" + $("#txtId").val());
    return emails;
}

function listaSMS() {
    var sms = [];
    sms.push($("#txtSendSms").val() + "|" + $("#txtId").val());
    return sms;
}

$("#btnSave").mousedown(function() {
    mousedownHappened = true;
});

$("#txtProfRespNome").autocomplete({ 
    source: function (request, response) {
        $.ajax({
            url: "../../Modulos/CRM/ajax.php",
            dataType: "json",
            data: {q: request.term, t: "E','I"},
            success: function (data) {
                response(data);
            }
        });
    }, minLength: 3,
    select: function (event, ui) {
        $("#txtProfResp").val(ui.item.id);
        $("#txtProfRespNome").val(ui.item.value);
    }
});

$("#txtProfRespNome").blur(function() {
    if ($("#txtProfRespNome").val() === "") {
        $("#txtProfResp").val("");
    }
});

function addGobody() {
    bootbox.confirm('Confirma a inclusão do cadastro no sistema GOBODY?', function (result) {
        if (result === true) {
            if (validarGobody()) {
                $("#loader").show();
                var linhas = $('[id*="tabela_linha_"]').size();
                var email = "";
                var telefone = "";
                for (i = 1; i <= linhas; i++) {
                    if ($("#txtTpContato_" + i).val() === "0" || $("#txtTpContato_" + i).val() === "1") {
                        telefone = $("#txtTxContato_" + i).val().replace("(", "").replace(")", "").replace(" ", "").replace("-", "");
                    } else if ($("#txtTpContato_" + i).val() === "2") {
                        email = $("#txtTxContato_" + i).val();
                    }
                }
                $.ajax({
                    url: "https://www.gobody.com.br/Api/Perfil/PerfilAjax.php", dataType: "jsonp",
                    data: {
                        bntNovoPerfil: 0, txtNome: $("#txtNome").val(), txtCpf: $("#txtCpf").val(), txtEmail: email, txtTelefone: telefone,
                        txtDtnasc: $("#txtDtNasc").val().replace(/\//g, ""), txtSexo: ($("#txtSexo").val() === "M" ? 1 : 0), txtSenha: "123456", erp: "S"
                    }, success: function (data) {
                        var result = data.split('|');
                        if (result[0] === "YES") {
                            bootbox.alert("Uma conta no GoBODY, para foi criada para o email: <b>" + email + "</b>" +
                                    " com a senha temporária:<b>123456</b>,<br> é necessário confirmação do email!");
                        } else if (result[0] === "EMAIL") {
                            bootbox.alert("Este Email: <b>" + email + "</b>, já está associado a uma conta no GOBODY, perfil associado!");
                        }
                        $.post("../../Modulos/Academia/form/ClientesFormServer.php", "isAjax=S&btnGobody=S&idGobody=" + result[1] + "&txtId=" + $("#txtId").val()).done(function (dt) {
                            if (dt.trim() === "YES") {
                                $("#btnGobody").attr("disabled", true);
                                $("#btnGobody").html("Sincronizado");
                            } else {
                                bootbox.alert("Erro: " + dt);
                            }
                        });
                    }
                }).always(function () {
                    $("#loader").hide();
                });
            }
        }
    });
}