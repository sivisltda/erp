$(document).ready(function () {
    $("#txtDescricao").focus();
    
    $("#txtFaixa1_ini").mask("99:99");
    $("#txtFaixa1_fim").mask("99:99");
    $("#txtFaixa2_ini").mask("99:99");
    $("#txtFaixa2_fim").mask("99:99");
    $("#txtFaixa3_ini").mask("99:99");
    $("#txtFaixa3_fim").mask("99:99");
    $("#txtFaixa4_ini").mask("99:99");
    $("#txtFaixa4_fim").mask("99:99");
    $("#txtFaixa5_ini").mask("99:99");
    $("#txtFaixa5_fim").mask("99:99");
});

function validaForm(){
    if($("#txtDescricao").val() === ""){
        bootbox.alert("Descrição é Obrigatória!");
        return false; 
    }
    if(!verificaHora($("#txtFaixa1_ini").val()) || !verificaHora($("#txtFaixa1_fim").val()) ||
       !verificaHora($("#txtFaixa2_ini").val()) || !verificaHora($("#txtFaixa2_fim").val()) ||
       !verificaHora($("#txtFaixa3_ini").val()) || !verificaHora($("#txtFaixa3_fim").val()) ||
       !verificaHora($("#txtFaixa4_ini").val()) || !verificaHora($("#txtFaixa4_fim").val()) ||
       !verificaHora($("#txtFaixa5_ini").val()) || !verificaHora($("#txtFaixa5_fim").val()) )
    {
        bootbox.alert("Horário inválido!");
        return false;
    }
    return true;
}

function verificaHora(hora){
    if(hora !== ""){
        var hrs = (hora.substring(0,2));
        var min = (hora.substring(3,5));
        
        if(hrs === "" || min === ""){
           return false; 
        }
        if ((hrs < 00 ) || (hrs > 23) || ( min < 00) ||( min > 59))
        { 
            return false;
        }else{
            return true;
        }
    }
    return true;
}