<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body onload="iniciaDetalhesForm();">
    <form>
        <div class="frmhead">
            <div class="frmtext">Detalhes do Plano</div>
            <div class="frmicon" onClick="parent.FecharBoxCliente(1)">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="tabbable frmtabs">
            <input id="txtFerias" type="hidden"/>
            <ul class="nav nav-tabs" style="margin-bottom:0px">
                <li class="active"><a href="#tab1" data-toggle="tab"><b>Informações do Plano</b></a></li>
                <li><a href="#tab2" data-toggle="tab"><b>Gerar Crédito Plano</b></a></li>
                <li><a href="#tab3" data-toggle="tab"><b>Trocar Plano</b></a></li>
                <li><a href="#tab4" data-toggle="tab"><b>Transferir Plano</b></a></li>
                <li><a href="#tab5" data-toggle="tab"><b>Assinatura</b></a></li>
            </ul>
            <div class="tab-content frmcont" style="min-height:221px">
                <div class="tab-pane active" id="tab1" style="margin-bottom:5px">
                    <div style="width:50%; float:left;">
                        <span>Plano:</span>
                        <select id="txtPlanoDet" name="txtPlanoDet" class="select" style="width:100%" disabled></select>
                    </div>
                    <div style="width:49%; float:left;margin-left: 1%;">
                        <span>Horário:</span>
                        <select id="txtHorarioDet" name="txtHorarioDet" class="select" style="width:100%"></select>
                    </div>
                    <div style="clear:both; height:7px"></div>
                    <div style="width:18%; float:left;">
                        <span>Inicio:</span>
                        <input type="text" id="txtDtIniDet" name="txtDtIniDet" class="inputCenter" disabled/>
                    </div>
                    <div style="width:19%; float:left;margin-left: 1%">
                        <span>Fim:</span>
                        <input type="text" id="txtDtFimDet" name="txtDtFimDet" class="inputCenter" disabled/>
                    </div>
                    <div style="clear:both; height:2px"></div>
                    <div style="width:100%; margin:10px 0">
                        <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Dados da Compra</span>
                        <hr style="margin:2px 0; border-top:1px solid #ddd">
                    </div>
                    <div style="width:18%; float:left">
                        <span>Comprado Em:</span>
                        <input class="input-xlarge inputCenter" id="txtCompradoEm" type="text" value="" disabled>
                    </div>
                    <div style="width:81%; float:left;margin-left: 1%">
                        <span>Responsável pela Venda:</span>
                        <input class="input-xlarge" id="txtUserResp" type="text" value="" disabled>
                    </div>
                    <div style="clear:both; height:5px"></div>
                    <div style="min-height:19px;">
                        <div id="divTransf" hidden >
                            <div style="width:18%; float:left">
                                <span>Transferido Em:</span>
                                <input class="input-xlarge inputCenter" id="txtDtTransferencia" type="text" value="" disabled>
                            </div>
                            <div style="width:81%; float:left;margin-left: 1%">
                                <span>Transferido de:</span>
                                <input class="input-xlarge" id="txtTransfOrigem" type="text" value="" disabled>
                            </div>
                        </div>
                        <div id="divCancel" hidden >
                            <div style="width:18%; float:left">
                                <span>Cancelamento:</span>
                                <input class="input-xlarge inputCenter" id="txtDtCancelamento" type="text" value="" disabled>
                            </div>
                            <div style="width:81%; float:left;margin-left: 1%">
                                <span>Responsável Cancelamento:</span>
                                <input class="input-xlarge" id="txtUserRespCancel" type="text" value="" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="spanBNT" style="left: 0px;margin: 0px;margin-top: 34px;">
                        <div class="toolbar bottom tar" style="margin-right: 8px;">
                            <div class="data-fluid" style="overflow:hidden; margin-top: 5px;">
                                <div class="btn-group">
                                    <button class="btn btn-success" type="button" id="btnSalvaDet" title="Salvar" ><span class="ico-checkmark"></span> Salvar</button>
                                    <button class="btn yellow" title="Cancelar" onClick="parent.FecharBoxCliente()"><span class="ico-reply"></span> Cancelar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                </div>
                <div class="tab-pane" id="tab2">
                    <div style="width:36%; padding:5px 0 0; float:left">
                        <div class="radio"><input type="radio" name="rCredTipo" checked value="0" style="opacity: 0;"></div>Crédito
                        <div class="radio"><input type="radio" name="rCredTipo" value="1" style="opacity: 0;"></div>Financeiro
                    </div>
                    <div style="clear:both;height: 10px;"></div>
                    <div style="width:15%; float:left;">
                        <span>Meses Restantes:</span>
                        <input class="input-xlarge inputCenter" id="txtMesesRest" type="text" disabled>
                    </div>
                    <div style="width:15%; float:left;margin-left: 1%">
                        <span>Cred. Disponível:</span>
                        <input class="input-xlarge inputCenter" id="txtCreditoConvCar" type="text" disabled>
                        <input class="input-xlarge inputCenter" id="txtCreditoConvFin" type="text" style="display: none;">
                    </div>
                    <div style="display:block; float:left;margin-left:1%;width: 26%;padding-top: 15px;">
                        <button type="button" class="btn green" id="btnConversao" disabled>
                            <span class="ico-plus icon-white icon-white"></span> &nbsp;Confirmar Conversão
                        </button>
                    </div>
                    <div id="textoFin" style="width: 38%; float: left; display: block;margin-top: -35px;" hidden>
                        <div class="alert alert-warning" style="cursor: text;height: 30px;color: #8a6d3b !important; background: #fcf8e3 !important;border: 1px solid #FBEEC5 !important;">
                            <p>Cálculo de crédito financeiro é baseado no valor mensal do plano.</p>
                        </div>
                    </div>
                    <div style="width:100%; display:block; float:left; ">
                        <div style="width:100%; margin:7px 0">
                            <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Histórico</span>
                            <hr style="margin:2px 0; border-top:1px solid #ddd">
                        </div>
                        <div class="body" style=" margin-left: 0px; padding: 0px 0px 0px 0px;">
                            <div class="content" style="min-height: 0px;margin-top: 0px;height: 134px;">
                                <table id="tblHistCredito" class="table" cellpadding="0" cellspacing="0" width="100%">
                                    <thead>
                                        <tr><th style="width: 20%"><b>Data</b></th>
                                            <th style="width: 20%"><b>Valor</b></th>
                                            <th><b>Resposável</b></th>
                                        </tr></thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                </div>
                <div class="tab-pane" id="tab3" style="margin-bottom:5px">                    
                    <div style="width:50%; float:left;">
                        <span>Novo Plano:</span>
                        <select id="txtPlanoTra" name="txtPlanoTra" class="select" style="width:100%">
                            <option value="null">Selecione o Plano</option>
                        </select>
                    </div>
                    <div style="width:49%; float:left;margin-left: 1%;">
                        <span>Prazo:</span>
                        <select id="txtPrazoTra" name="txtPrazoTra" class="select" style="width:100%">
                            <option value="null" par="null" valor="null">Selecione um Prazo</option>    
                        </select>                        
                    </div>
                    <div style="clear:both; height:7px"></div>
                    <div style="width:50%; float:left;">
                        <span>Horário:</span>
                        <select id="txtHorarioTra" name="txtHorarioTra" class="select" style="width:100%">
                            <option value="null">Selecione o Horário</option>
                        </select>                        
                    </div>
                    <div style="width:18%; float:left;margin-left: 1%;">
                        <span>Inicio:</span>
                        <input type="text" id="txtDtIniTra" name="txtDtIniTra" class="datepicker inputCenter"/>
                    </div>
                    <div style="width:10%; float:left;margin-left: 1%">
                        <span>Dias Cred:</span>
                        <input type="text" id="txtDiasTra" name="txtDiasTra" class="inputCenter" disabled/>
                    </div>
                    <div style="width:19%; float:left;margin-left: 1%">
                        <span>Fim:</span>
                        <input type="text" id="txtDtFimTra" name="txtDtFimTra" class="inputCenter" disabled/>
                    </div>
                    <div style="clear:both; height:2px"></div>
                    <div style="width:100%; margin:10px 0">
                        <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Detalhes</span>
                        <hr style="margin:2px 0; border-top:1px solid #ddd; margin-bottom: 12px;">
                        <div style="width:15%; float:left;">
                            <span>Meses Restantes:</span>
                            <input class="input-xlarge inputCenter" id="txtMesesRestTra" type="text" disabled>
                        </div>
                        <div style="width:15%; float:left;margin-left: 1%">
                            <span>Cred. Disponível:</span>
                            <input class="input-xlarge inputCenter" id="txtCreditoConvCarTra" type="text" disabled>
                        </div>
                    </div>                    
                    <div style="clear:both; height:7px"></div>                    
                    <div class="spanBNT" style="left: 0px;margin: 0px;margin-top: 48px;">
                        <div class="toolbar bottom tar" style="margin-right: 8px;">
                            <div class="data-fluid" style="overflow:hidden;margin-top: 5px;">
                                <div class="btn-group">
                                    <button class="btn btn-success" type="button" id="btnSalvaTra" title="Salvar" ><span class="ico-checkmark"></span> Salvar</button>
                                    <button class="btn yellow" title="Cancelar" onClick="parent.FecharBoxCliente()"><span class="ico-reply"></span> Cancelar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                </div>
                <div class="tab-pane" id="tab4" style="margin-bottom:5px">
                    <div style="width:100%; float:left;">
                        <span>Destinatário Transferência :</span>
                        <input type="text" id="txtTransfPara" style="width:100%" />
                        <input id="txtTransfParaCod" type="hidden"/>
                    </div>
                    <div style="clear:both; height:5px"></div>
                    <div style="width:100%; float:left;">
                        <span>Motivo :</span>
                        <textarea id="txtMotivo" style="width:100%; height:127px; resize:none"></textarea>
                    </div>
                    <div style="clear:both; height:25px"></div>
                    <div class="spanBNT" style="left: 0px;margin: 0px;margin-top: 27px;">
                        <div class="toolbar bottom tar" style="margin-right: 8px;">
                            <div class="data-fluid" style="overflow:hidden;margin-top: 5px;">
                                <div class="btn-group">
                                    <button class="btn btn-success" type="button" id="btnSalvaTransf" title="Salvar" ><span class="ico-checkmark"></span> Salvar</button>
                                    <button class="btn yellow" title="Cancelar" onClick="parent.FecharBoxCliente()"><span class="ico-reply"></span> Cancelar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab5" style="margin-bottom:5px">
                    <div style="width:100%; display:block; float:left;">
                        <div class="body" style=" margin-left: 0px; padding: 0px 0px 0px 0px;">
                            <div class="content" style="min-height: 0px;margin-top: 0px;height: 134px;">
                                <table id="tblAssinatura" class="table" cellpadding="0" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width: 20%"><b>Data</b></th>
                                            <th style="width: 30%"><b>Contrato</b></th>
                                            <th style="width: 35%"><b>Assinatura</b></th>
                                            <th style="width: 15%"><b>Ação</b></th>
                                        </tr></thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>   
                </div>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>                
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
    <script type="text/javascript" src="../../js/moment.min.js"></script> 
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type='text/javascript' src='js/ClientesPlanosDetForm.js'></script>
    <script type='text/javascript'>
        
        $("#txtDtIniTra, #txtDtFimTra").mask(lang["dateMask"]);
        $("#txtCreditoConvCarTra").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
        
        LimparTra();

        $.getJSON("form/ClientesPlanosServer.php", {listPlanos: $("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected)').find('.detalhe').attr('data-id'), tpTela: "T", idItem: $("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected)').find('.detalhe').attr('data-id')}, function (j) {
            var options = "<option value=\"null\">Selecione o Plano</option>";
            if (j !== null && j.length > 0) {
                for (var i = 0; i < j.length; i++) {
                    options = options + "<option value=\"" + j[i].id + "\">" + j[i].descricao + "</option>";
                }
            }
            $("#txtPlanoTra").html(options);
            $("#txtPlanoTra").select2("val", "null");
        });

        $('#txtPlanoTra').change(function () {
            atualizaHorario($('#txtPlanoTra').val());
            atualizaPrazo($('#txtPlanoTra').val());  
            LimparTra();
        });
        
        $('#txtPrazoTra').change(function () {
            atualizaDataFim();
        });
        
        $('#txtDtIniTra').change(function () {
            atualizaDataFim();
        });
        
        function atualizaHorario(id) {
            $.getJSON("form/ClientesPlanosServer.php", {listHorarios: "S", idPlano: id}, function (j) {
                var options = "<option value=\"null\">Selecione o Horário</option>";
                if (j !== null && j.length > 0) {
                    for (var i = 0; i < j.length; i++) {
                        options = options + "<option value=\"" + j[i].cod_turno + "\">" + j[i].nome_turno + "</option>";
                    }
                }                    
                $("#txtHorarioTra").html(options);
                $("#txtHorarioTra").select2("val", "null");                                
            });
        }                

        function atualizaPrazo(id) {
            $.getJSON("form/ClientesPlanosServer.php", {prazoTroca: "S", idPlano: id, valor: $("#txtCreditoConvCarTra").val()}, function (j) {
                var options = "<option value=\"null\" par=\"null\" valor=\"null\">Selecione um Prazo</option>";
                if (j !== null && j.length > 0) {
                    for (var i = 0; i < j.length; i++) {
                        options = options + "<option value=\"" + j[i].cod_produto + "\" par=\"" + j[i].par_produto + "\" valor=\"" + j[i].val_produto + "\">" + j[i].nome_produto + "</option>";
                    }
                }                    
                $("#txtPrazoTra").html(options);
                $("#txtPrazoTra").select2("val", "null");                                
            });
        }
        
        function atualizaDataFim() {
            let day = 0;                                                        
            let diasMes = moment($('#txtDtIniTra').val(), lang["dmy"]).add(1, "month").diff(moment($('#txtDtIniTra').val(), lang["dmy"]), 'days');                
            let valorDia = textToNumber($("#txtPrazoTra option:selected").attr("valor")) / diasMes;
            if (valorDia > 0) {
                day = Math.round(textToNumber($("#txtCreditoConvCarTra").val()) / valorDia);
            }
            $("#txtDiasTra").val(day);                
            $('#txtDtFimTra').val(moment($('#txtDtIniTra').val(), lang["dmy"]).add(day, 'days').format(lang["dmy"]));            
        }
        
        function trocarPlano() {
            $("#btnSalvaTra").attr("disabled", true);
            $.post("form/ClientesPlanosServer.php", 
                "trocarPlano=S&idPlano=" + $("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected)').find('.detalhe').attr('data-id') +
                "&idPlanoNovo=" + $("#txtPlanoTra").val() + "&txtDescrPlano=" + $("#txtPlanoTra option:selected").text() + "&txtHorario=" + $("#txtHorarioTra").val() +
                "&txtPrazo=" + $("#txtPrazoTra").val() + "&txtDtIni=" + $("#txtDtIniTra").val() + "&txtDtFim=" + $("#txtDtFimTra").val() +
                "&txtId=" + $("#txtId", window.parent.document).val() + "&txtCredito=" + $("#txtCreditoConvCarTra").val() + "&txtValor=" + $("#txtPrazoTra option:selected").attr("valor")
                ).done(function(data) {
                if(data.trim() === "YES"){
                    parent.AtualizaListPlanosClientes();
                    parent.atualizaStatusCli();
                    parent.FecharBoxCliente();

                }else{
                    bootbox.alert("Erro ao Trocar de Plano!");
                }
            });  
        }               

        $('#btnSalvaTra').click(function (e) {
            if(validaTra()) {
                bootbox.confirm('Confirma a Troca de Plano? Essa ação não poderá ser revertida.', function (result) {
                    if (result === true) {
                        trocarPlano();
                    }
                });
            }
        });                                      
        
        function validaTra() {
            if($('#txtPlanoTra').val() === "null") {
                bootbox.alert("Selecione um novo plano!");
                return false;
            }else if($('#txtHorarioTra').val() === "null") {
                bootbox.alert("Selecione um horário!");
                return false;
            }else if($('#txtPrazoTra').val() === "null") {
                bootbox.alert("Selecione um prazo válido para troca!");
                return false;
            }else if(!moment($('#txtDtIniTra').val(), lang["dmy"], true).isValid()) {
                bootbox.alert("Informa uma data de Início válida!");
                return false;        
            } else if ($("#txtFerias").val() !== "0") {
                bootbox.alert("Plano em férias, não é possível realizar esta operação!");  
                return false;                
            }
            return true;
        }     
        
        function LimparTra() {
            $("#txtDtIniTra, #txtDtFimTra").val(moment().format(lang["dmy"]));        
            $("#txtDiasTra").val("0");        
        }
        
        function RemoverAss(file_name) {
            bootbox.confirm('Confirma a exclusão do registro?', function (result) {
                if (result === true) {
                    $.post("form/ClientesPlanosServer.php", "btnDelAss=S&txtId=" + 
                        $("#txtId", window.parent.document).val() + "&txtFileName=" + file_name).done(function (data) {
                        if (data === "YES") {
                            refreshAss();
                        } else {
                            bootbox.alert(data);
                        }
                    });
                } else {
                    return;
                }
            });            
        }
        
        function refreshAss() {
            var tblPagamentosPlano = $('#tblAssinatura').dataTable();
            tblPagamentosPlano.fnReloadAjax("form/ClientesPlanosServer.php?listAssinaturas=" + 
            $("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected)').find('.detalhe').attr('data-id') +
            "&id=" + $("#txtId", window.parent.document).val());
        }
        
        $('#tblAssinatura').dataTable({
            "iDisplayLength": 20,
            "aLengthMenu": [20, 30, 40, 50, 100],
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "form/ClientesPlanosServer.php?listAssinaturas=" + 
            $("#tblPlanosClientes", window.parent.document).find('tr:has(td.selected)').find('.detalhe').attr('data-id') +
            "&id=" + $("#txtId", window.parent.document).val(),
            "bFilter": false,
            "bSort": false,
            "bPaginate": false,
            'oLanguage': {
                'sInfo': "",
                'sInfoEmpty': "",
                'sInfoFiltered': "",
                'sLengthMenu': "",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sSearch': "Pesquisar:",
                'sZeroRecords': "Não foi encontrado nenhum resultado"},
            "sPaginationType": "full_numbers"
        });

    </script>
</body>
