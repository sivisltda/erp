<?php


    $dicionario[] = array("A_nome", "<b>".$nmAluno."</b>", "Nome");
    $dicionario[] = array("A_prof", "<b>".$profissaoAluno."</b>", "Profissão");
    $dicionario[] = array("A_estaCivil", "<b>".$estadoCivilAluno."</b>", "Estado Civil");
    $dicionario[] = array("A_rg", "<b>".$rgAluno."</b>", "RG");
    $dicionario[] = array("A_cpf", "<b>".$rgAluno."</b>", "CPF");
    $dicionario[] = array("A_end", "<b>".$endAluno."</b>", "Endereco");
    $dicionario[] = array("A_NumEnd", "<b>".$numAluno."</b>", "Numero End.");
    $dicionario[] = array("A_bairro", "<b>".$bairroAluno."</b>", "Bairro");
    $dicionario[] = array("A_cidade", "<b>".$cidadeAluno."</b>", "Cidade");
    $dicionario[] = array("A_uf", "<b>".$estadoAluno."</b>", "Estado");
    $dicionario[] = array("A_responsavel", "<b>".$NmResp."</b>", "Responsavel");

    $dicionario[] = array("P_dtInic", "<b>".$dtInicioPlano."</b>", "Dt Inicio Plano");
    $dicionario[] = array("P_dtFim", "<b>".$dtFimPlano."</b>", "Dt Fim Plano");
    $dicionario[] = array("P_qntParcel", "<b>".$parcela."</b>", "Qnt Parcela Plano");
    $dicionario[] = array("P_descGrup", "<b>".$descrGrupo."</b>", "Nome Grupo Plano");
    $dicionario[] = array("P_val01", "<b>".$vl_01."</b>", "Valor Plano 1 Mês");
    $dicionario[] = array("P_val06", "<b>".$vl_06."</b>", "Valor Plano 6 Mêses");
    $dicionario[] = array("P_val12", "<b>".$vl_12."</b>", "Valor Plano 12 Mêses");

    $dicionario[] = array("F_nomIndicador", "<b>".$nmIndicador."</b>", "Funcionario que indico");
    $dicionario[] = array("F_cpfIndicador", "<b>".$cpfIndicador."</b>", "CPF que indico");

    $dicionario[] = array("E_idFilial", "<b>".$id_filial."</b>", "ID sivis Filial");
    $dicionario[] = array("E_cnpj", "<b>".$cpf_cnpj."</b>", "CNPJ");
    $dicionario[] = array("E_razao_social", "<b>".$cedente."</b>", "Razão Social");
    $dicionario[] = array("E_nome_fantasia", "<b>".$nome_fantasia."</b>", "Nome Fantasia");
    $dicionario[] = array("E_inscr_estadual", "<b>".$inscricao."</b>", "Inscrição Estadual");
    $dicionario[] = array("E_end_cidade", "<b>".$cidade_filial."</b>", "Cidade");
    $dicionario[] = array("E_end_estado", "<b>".$estado_filial."</b>", "Estado");
    $dicionario[] = array("E_end_bairro", "<b>".$bairro_filial."</b>", "Bairro");
    $dicionario[] = array("E_endereco", "<b>".$endereco_filial."</b>", "Endereço");
    $dicionario[] = array("E_numEnd", "<b>".$numEnd_filal."</b>", "Endereço Num");


    //$dicionario[] = array("E_num_filial", $numero_filial, "Endereço Num");
