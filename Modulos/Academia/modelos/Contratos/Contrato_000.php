<?php

if (file_exists('../Connections/configini.php')) {
    include_once "../util/util.php";
}elseif (file_exists('../../Connections/configini.php')) {
    include_once "../../util/util.php";
}

if ($_GET["id"] != "") {

    $idTitular = "A.favorecido";
    $query = "select id_titular from sf_fornecedores_despesas_dependentes where id_dependente = (select favorecido from sf_vendas_planos where id_plano = ".$_GET["id"].")";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $idTitular = $RFP['id_titular'];
    }
    $query = "select favorecido, B.razao_social, B.estado_civil, B.data_nascimento,B.profissao, B.inscricao_estadual, B.cnpj, B.endereco,
                B.bairro, B.numero, B.complemento, C.cidade_nome, D.estado_sigla, B.cep,
                (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where fornecedores_despesas = A.favorecido and tipo_contato = 1 order by id_contatos) telefone_cel,
                (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where fornecedores_despesas = A.favorecido and tipo_contato = 2 order by id_contatos) email,
                E.descricao,F.razao_social nm_resp_legal,F.inscricao_estadual rg_resp_legal,F.cnpj cpf_resp_legal,F.estado_civil estcivil_resp_legal,F.profissao profissao_resp_legal,F.cep cep_resp_legal,F.endereco end_resp_legal,F.numero num_resp_legal,F.bairro bairro_resp_legal,F.complemento compl_resp_legal, F.estado est_resp_legal, F.cidade cid_resp_legal, G.cidade_nome cidade_resp_legal, H.estado_sigla estado_resp_legal
                from sf_vendas_planos A
                inner join sf_fornecedores_despesas B on ".$idTitular." = B.id_fornecedores_despesas
                left join tb_cidades C on B.cidade = C.cidade_codigo
                left join tb_estados D on B.estado = D.estado_codigo
                inner join sf_produtos E on A.id_prod_plano = E.conta_produto
                left join sf_fornecedores_despesas F on A.favorecido = F.id_fornecedores_despesas
                left join tb_cidades G on F.cidade = G.cidade_codigo
                left join tb_estados H on F.estado = H.estado_codigo
               where id_plano = ".$_GET["id"];

    //echo $query;exit();
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $id_resp = $RFP['favorecido'];
        $nm_resp = $RFP['nm_resp_legal'] != '' ? tableFormato($RFP['nm_resp_legal'],'T','','') : tableFormato($RFP['razao_social'],'T','','');
        $dtNascimento = tableFormato($RFP['data_nascimento'],'D','','');
        $profissao = $RFP['nm_resp_legal'] != '' ? tableFormato($RFP['profissao_resp_legal'],'T','','') : tableFormato($RFP['profissao'],'T','','');
        $estadoCivil = $RFP['nm_resp_legal'] != '' ? tableFormato($RFP['estcivil_resp_legal'],'T','','') : tableFormato($RFP['estado_civil'],'T','','');
        $rg = $RFP['nm_resp_legal'] != '' ? tableFormato($RFP['rg_resp_legal'],'T','','') : tableFormato($RFP['rg'],'T','','');
        $cpf = $RFP['nm_resp_legal'] != '' ? tableFormato($RFP['cpf_resp_legal'],'T','','') : tableFormato($RFP['cnpj'],'T','','');
        if($RFP['nm_resp_legal'] !== "" && $RFP['nm_resp_legal'] !== null){
            $endereco_resp = tableFormato($RFP['end_resp_legal'],'T','','').", ".tableFormato($RFP['bairro_resp_legal'],'T','','').", ".tableFormato($RFP['num_resp_legal'],'T','','').", ".tableFormato($RFP['compl_resp_legal'],'T','','').", ".
                 tableFormato($RFP['cidade_resp_legal'],'T','','').", ".tableFormato($RFP['estado_resp_legal'],'T','','').", ".tableFormato($RFP['cep_resp_legal'],'T','','');
        }else{
            $endereco_resp = tableFormato($RFP['endereco'],'T','','').", ".tableFormato($RFP['bairro'],'T','','').", ".tableFormato($RFP['numero'],'T','','').", ".tableFormato($RFP['complemento'],'T','','').", ".
                tableFormato($RFP['cidade_nome'],'T','','').", ".tableFormato($RFP['estado_sigla'],'T','','').", ".tableFormato($RFP['cep'],'T','','');
        }
        $cel_resp = tableFormato($RFP['telefone_cel'],'T','','');
        $email = tableFormato($RFP['email'],'T','','');
        $descr_plano = tableFormato($RFP['descricao'],'T','','');
    }

    $cnpj = $_SESSION["cpf_cnpj"];
    $razao_social = $_SESSION["razao_social_contrato"];
    $nome_fantasia = $_SESSION["nome_fantasia_contrato"];
    $query = " select A.endereco, A.numero, A.bairro, A.cidade_nome, A.estado, A.cep, A.cnpj, A.razao_social_contrato, A.nome_fantasia_contrato from sf_filiais A
               inner join sf_fornecedores_despesas B on A.id_filial = B.empresa
               where id_fornecedores_despesas = ".$id_resp;
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $endereco_empresa = tableFormato($RFP['endereco'],'T','','').", ".tableFormato($RFP['bairro'],'T','','').", ".tableFormato($RFP['numero'],'T','','').", ".
            tableFormato($RFP['cidade_nome'],'T','','').", ".tableFormato($RFP['estado'],'T','','').", CEP: ".tableFormato($RFP['cep'],'T','','');
        $cidade_empresa = tableFormato($RFP['cidade_nome'],'T','','');
        $cnpj = $RFP['cnpj'] !== "" ? tableFormato($RFP['cnpj'],'T','',''): $cnpj;
        $razao_social = $RFP['razao_social_contrato'] !== "" ? tableFormato($RFP['razao_social_contrato'],'T','','') : $razao_social;
        $nome_fantasia = $RFP['nome_fantasia_contrato'] !== "" ? tableFormato($RFP['nome_fantasia_contrato'],'T','','') : $nome_fantasia;
    }

    $query = " select valor_mens, parcela from sf_vendas_planos_mensalidade A
               inner join sf_produtos_parcelas B on A.id_parc_prod_mens = B.id_parcela
               where id_plano_mens = ".$_GET["id"];
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $valor_plano = escreverNumero($RFP['valor_mens'], 1) . " (".valorPorExtenso($RFP['valor_mens'],true,false).")" ;
        $tp_plano = "MENSAL";
        $parcela_plano = "1 MES ";
        if($RFP['parcela'] > 1){
            $tp_plano = "PLANO";
            $parcela_plano = $RFP['parcela']." MESES";
        }else if($RFP['parcela'] == 0) {
            $tp_plano = "DCC";
        }
    }
}

?>
<html>

<head>
    <meta http-equiv=Content-Type content="text/html; charset=windows-1252">
    <meta name=Generator content="Microsoft Word 15 (filtered)">
    <link href="https://fonts.googleapis.com/css?family=PT+Serif+Caption" rel="stylesheet">
    <title> </title>
    <style></style>
</head>

<body lang=PT-BR link=blue style="font-size: 9pt;">
<font >
    <table class="semborda" width="100%">
        <tr style="line-height: 5px;">
            <td style="width:25%;vertical-align: top;">
                <?php if(file_exists("./../Pessoas/".$_SESSION["contrato"]."/Empresa/logo_001.jpg")){ ?>
                    <div style="line-height: 0px;margin-top: -70px;">&nbsp;</div><img src ="./../Pessoas/<?php echo $_SESSION["contrato"]; ?>/Empresa/logo_001.jpg" width="160" height="38" align="middle" />
                <?php } else { ?>
                    <div style="line-height: 5px;">&nbsp;</div>
                <?php } ?>
            </td>
            <td style="width:72%;text-align:right;line-height: 8px;margin-top: 10px;">
                <span style="font-size:14pt;font-weight:bold;"> CONTRATO DE PRESTAÇÃO DE SERVIÇOS</span>
            </td>
        </tr>
        <tr>
            <td>

            </td>
        </tr>
    </table>
    <table style="line-height: 17px;">
        <tr>
            <td style="width: 46%;text-align:justify;text-indent:-9pt;">
                Por este Contrato entre as partes abaixo assinadas e adiante qualificadas, ora denominadas <b>CONTRATADA, <?php echo $razao_social; ?> ,</b> CNPJ <?php echo $cnpj; ?>, com sede na <?php echo $endereco_empresa; ?>, <b><?php echo $nome_fantasia; ?></b> e <b>CONTRATANTE, <?php echo $nm_resp; ?></b>, Brasileiro(a), <?php echo $estadoCivil; ?>, Nascido em <?php echo $dtNascimento; ?>, <?php echo $profissao; ?>, Carteira de Identidade nº <?php echo $rg; ?>, e CPF nº <?php echo $cpf; ?>, residente e domiciliado na <?php echo $endereco_resp; ?>, Telefone: <?php echo $cel_resp; ?>, E-mail: <?php echo $email; ?>, de comum acordo firmam o presente Contrato de Prestação de Serviços, que se regerá pelas cláusulas e condições descritas no presente.
                <div class="espacop">&nbsp;</div>
                <b>§1º - OBJETO: </b> Este contrato tem como objeto a prestação de serviços relacionados à prática de atividades físicas, dentro do horário de funcionamento de cada unidade.
                <div class="espacop">&nbsp;</div>
                <b>§2º - DIAS E HORÁRIOS: </b> O CONTRATANTE poderá frequentar as instalações da CONTRATADA nos dias e horários que melhor lhe convier, respeitando, contudo, a modalidade do Plano contratado, os horários e as turmas nas atividades em grupo oferecidas pela CONTRATADA.
                <br/>
                <table>
                    <tr>
                        <td style="width: 5%;">I.</td>
                        <td style="width: 95%;">A <b>CONTRATADA</b> se reserva o direito de alterar, acrescentar ou extinguir, sem prévio aviso, os horários, modalidades de atividades oferecidas, bem como o quadro de professores sem prejuízo do presente contrato.</td>
                    </tr>
                </table>
                <div class="espacop">&nbsp;</div>
                <b>§3º - RESPONSABILIDADES:</b> A CONTRATADA é responsável pela segurança dos serviços, manutenção dos equipamentos e instalações, bem como manter nas suas instalações instrutores para orientação da realização de exercícios físicos.
                <br/>
                <table>
                    <tr>
                        <td style="width: 5%;">I.</td>
                        <td style="width: 95%;">Danos de qualquer natureza decorrentes de atividades executadas sem a solicitação de orientação, ou com inobservância desta, não será de responsabilidade da <b>CONTRATADA</b> e caracterizará culpa exclusiva do <b>CONTRATANTE</b>.</td>
                    </tr>
                    <tr>
                        <td style="width: 5%;">II.</td>
                        <td style="width: 95%;">A responsabilidade sobre a guarda de objetos de uso pessoal tais como telefones celulares, relógios, chaveiros, carteiras e outros, será de responsabilidade exclusiva do <b>CONTRATANTE</b>, não podendo a <b>CONTRATADA</b>, em momento algum, ser responsabilizada por danos ou extravio dos mesmos, ocorridos em suas dependências.</td>
                    </tr>
                </table>
                <div class="espacop">&nbsp;</div>
                <b>§4º - DECLARAÇÃO DE SAÚDE: </b> O <b>CONTRATANTE</b> deverá responder e assinar o questionário que segue
            </td>
            <td style="width: 5%"></td>
            <td style="width: 46%;text-align:justify;text-indent:-9px;">
                ANEXO, cuja validade estará condicionada exclusivamente à exigência legal em cada praça. Caso haja alguma resposta positiva ao questionário, deverá o <b>CONTRATANTE</b> apresentar atestado médico, autorizando a prática da atividade física.
                <div class="espacop">&nbsp;</div>
                <b>§5º - BOA CONDUTA:</b> O <b>CONTRATANTE</b> se obriga a acatar e respeitar as normas de boas condutas da academia para o perfeito relacionamento entre as partes, sob pena do cancelamento deste contrato sem direito a qualquer restituição de quantias pagas.
                <div class="espacop">&nbsp;</div>
                <b>§6º - MENORES:</b> Os menores de 18 (dezoito) anos assinam este instrumento juntamente com seu responsável legal, respondendo este, solidariamente, por seus atos, omissões e obrigações.
                <div class="espacop">&nbsp;</div>
                <b>§7º - PLANOS E VALORES:</b> Segue as condições oferecidas:
                <br/>
                <b>MENSAL:</b> Entende-se como o contrato celebrado por período igual a 01 (um) mês;
                <br/>
                <b>PLANO:</b> Entende-se como o contrato celebrado por período igual ou superior a 03 (três) meses, em que o CONTRATANTE recebe como benefício principal o desconto em relação à condição MENSAL.
                <br/>
                <b>DCC - DÉBITO NO CARTÃO DE CRÉDITO:</b> Entende-se como Plano contratado pelo período de 12 (doze) meses, com RENOVAÇÃO AUTOMÁTICA, debito no cartão de crédito e desconto em relação à condição MENSAL.
                <br/>
                <table>
                    <tr>
                        <td style="width: 5%;">I.</td>
                        <td style="width: 95%;">Na hipótese da administradora do cartão de crédito, não autorizar a liberação da quantia devida, ou por quaisquer outros motivos, o débito não for efetuado, o <b>CONTRATANTE</b> deverá comparecer a recepção da <b>CONTRATADA</b>, imediatamente, a fim de realizar o pagamento do débito existente, mediante outra modalidade de pagamento (estando sujeito ao valor vigente do plano mensal), sob pena de suspensão da prática de atividades físicas até o efetivo pagamento.</td>
                    </tr>
                    <tr>
                        <td style="width: 5%;">II.</td>
                        <td style="width: 95%;">Em caso de reincidência da hipótese mencionada no parágrafo 4º acima, a <b>CONTRATADA</b> estará autorizada a cancelar automaticamente o referido plano, passando a ser exigido o valor estipulado para o plano mensal, sem a incidência de qualquer tipo de desconto. Podendo, inclusive, a <b>CONTRATADA</b> optar pela rescisão do contrato.</td>
                    </tr>
                    <tr>
                        <td style="width: 5%;">III.</td>
                        <td style="width: 95%;">O valor contratado para o Plano DCC será reajustados a cada 12 (doze) meses, a contar da data de início da vigência do plano pela variação do IGP-M acumulado no ano anterior.</td>
                    </tr>
                    <tr>
                        <td style="width: 5%;">IV.</td>
                        <td style="width: 95%;">Sua renovação é automática após o período contratado, devendo o <b>CONTRATANTE</b> manifestar sua discordância 30 (trinta) dias antes do vencimento.</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: center;">
                <div style="line-height: 30px;">Página 1/2</div>
            </td>
        </tr>
    </table>
    <div style="page-break-before: always;"></div>
    <table style="line-height: 17px;">
        <tr>
            <td style="width: 46%;text-align:justify;text-indent:-16px;">
                <span>
                    Em todas as condições acima apresentadas, a forma de cobrança ocorre de forma antecipada, com o pagamento e posterior prestação do serviço. Dessa maneira, o <b>CONTRATANTE</b> pagará a <b>CONTRATADA</b> à importância de <b><?php echo $valor_plano; ?></b>, referente ao Plano <b><?php echo $descr_plano; ?></b>, condição <b><?php echo $tp_plano; ?></b>, validade <b><?php echo $parcela_plano; ?></b>. Ocorrendo atraso na liquidação das parcelas, o valor devido na forma pactuada será acrescido de multa de 2%, além de juros legais de 1% ao mês.
                    <div class="espacop">&nbsp;</div>
                    <b>§8º - PRAZO:</b> Este contrato tem como prazo a data de contratação somada a validade indicada na escolha do Plano.
                    <div class="espacop">&nbsp;</div>
                    <b>§9º - CONGELAMENTO:</b> O <b>CONTRATANTE</b> pode interromper a prática da atividade contratada, por um período máximo de 30 (trinta) dias no Plano contratado, recebendo um bônus de dias equivalente ao interrompido, imediatamente após o final do período de vigência do contrato. Para usufruir deste benefício, o <b>CONTRATANTE</b> deverá comunicar por escrito a <b>CONTRATADA</b>, com uma antecedência de 03 (três) dias. Obs.: Os períodos de interrupção não poderão ser menores do que 07 (sete) dias.
                    <br/><br/>
                    <b>§10º - DESISTÊNCIA:</b> Na hipótese de o <b>CONTRATANTE</b> desistir da realização da atividade contratada, o mesmo terá a opção:
                    <br/>
                    <table>
                        <tr>
                            <td style="width: 5%;">I.</td>
                            <td style="width: 95%;">De transferência do Plano contratado para um terceiro indicado, sem cobrança de Taxas e Multa.</td>
                        </tr>
                        <tr>
                            <td style="width: 5%;">II.</td>
                            <td style="width: 95%;">De conversão do saldo residual do Plano contratado em Crédito, que poderá ser utilizado na contratação de um novo Plano, dentro do período de até 24 (vinte e quatro) meses.</td>
                        </tr>
                        <tr>
                            <td style="width: 5%;">III.</td>
                            <td style="width: 95%;">De Rescisão do Plano, em que o <b>CONTRATANTE</b> deverá comunicar por escrito a <b>CONTRATADA</b> com antecedência de 30 (trinta) dias. A <b>CONTRATADA</b> restituirá o valor residual do seu contrato, subtraindo-se o desconto promocional oferecido pela <b>CONTRATADA</b>, e no caso de cartões de crédito, descontando-se a taxa de cartão cobrada pela administradora. Obs.: Os valores pagos referente a taxa de matrícula, serviços de avaliação funcional e orientação nutricional se contratadas não serão restituídos.</td>
                        </tr>
                    </table>
                    <div class="espacop">&nbsp;</div>
                    Por estarem justos, certos e contratados, assinam o presente instrumento, em duas vias, de igual teor e forma, elegendo o foro da Comarca de <?php echo $cidade_empresa; ?> para dirimir quaisquer dúvidas provenientes deste contrato.
                </span>
            </td>
            <td style="width: 5%"></td>
            <td style="width: 46%;text-align:justify;text-indent:-9px;">
                <div style="text-align: center"><b>PAR-Q QUESTIONÁRIO DE PRONTIDÃO PARA A ATIVIDADE FÍSICA</b></div>
                <div class="espacop">&nbsp;</div>
                <table border="1" style="padding: 3px;">
                    <tr>
                        <td style="width: 80%"></td>
                        <td style="width: 10%;text-align: center;">Sim</td>
                        <td style="width: 10%;text-align: center;">Não</td>
                    </tr>
                    <tr>
                        <td>Necessita de supervisão médica devido a problemas cardíacos?</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Sente dor torácica durante a prática de Atividade Física?</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Sentiu dor no tórax no último mês?</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Você tende a perder a consciência ou cair, como resultado de tonteira ou desmaio?</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Você tem algum problema ósseo ou muscular que poderia ser agravado com a prática de atividade física?</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Algum médico já lhe recomendou o uso de medicamentos para a sua pressão arterial, para circulação ou coração?</td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <div class="espacop">&nbsp;</div>
                <div class="espacop">&nbsp;</div>
                <?php
                setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                date_default_timezone_set('America/Sao_Paulo');
                ?>
                <div align=right style="text-align:center">
                    <span style="font-size:8.0pt;color:black"><?php echo $cidade_empresa; ?>,</span><b>
                    <span style="font-size:8.0pt;"><?php
                        echo utf8_encode(strtoupper(strftime('%d de %B de %Y', strtotime('today'))));
                        ?></span></b>
                </div>
                <div class="espacop">&nbsp;</div>
                <div class="espacop">&nbsp;</div>
                <div style="text-align: center;font-size:8.0pt">
                    _________________________________<br/>
                    <b><?php echo $nm_resp; ?></b><br/>
                    CPF: <?php echo $cpf; ?><br/>
                    CONTRATANTE
                </div>
                <div style="text-align: center;font-size:8.0pt">
                    _________________________________<br/>
                    <b><?php echo $nome_fantasia; ?></b><br/>
                    CNPJ:<?php echo $cnpj; ?><br/>
                    CONTRATADA
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: center;">
                <div style="line-height: 70px;">Página 2/2</div>
            </td>
        </tr>
    </table>
    <div style="line-height: 7px;">&nbsp;</div>
</font>
</body>

</html>
