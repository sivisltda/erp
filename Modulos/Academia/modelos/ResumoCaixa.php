<?php
$_GET['tp'] = 1;
$_GET['isPdf'] = 'S';

$_GET['dx'] = 1;
$_GET['fr'] = 1;
include './../Modulos/Academia/ajax/InformacoesGerenciaisFinancas_server_processing.php';
$arrayDetalhes = $output['aaData'];

$_GET['dx'] = 0;
$_GET['fr'] = 1;
include './../Modulos/Academia/ajax/InformacoesGerenciaisFinancas_server_processing.php';
$arrayDetalhesCarteira = $output['aaData'];

$_GET['dx'] = 1;
$_GET['fr'] = 0;
include './../Modulos/Academia/ajax/InformacoesGerenciaisFinancas_server_processing.php';
$arrayTotais = $output['aaData'];

$_GET['dx'] = 0;
$_GET['fr'] = 0;
include './../Modulos/Academia/ajax/InformacoesGerenciaisFinancas_server_processing.php';
$arrayCarteira = $output['aaData'];
//echo $arrayCarteira;exit();

function spaceEcho($total) {
    $return = "";
    for ($i = 0; $i < $total; $i++) {
        $return .= "&nbsp;";
    }
    return $return;
}
$filial_orc = ($filial_orc == "" ? "001" : $filial_orc);
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
            body {
                margin:0px;
                padding:0px;
            }
            table {
                font: 10px Calibri;
                margin-bottom: 10px;
                border: 1px solid #999;
                border-collapse: collapse;
                vertical-align: middle;
            }
            .titulo {
                font: 14px Calibri;
                font-weight: Bold;
                padding: 2px;
                padding-left: 5px;
            }
            .tabela {
                height: 18px;
                padding-left: 5px;
                padding-right: 5px;
            }
            .assinatura {
                font: 12px Arial;
                bottom:0;
                width:700px;
            }
            .assinatura p{
                line-height: 1px;
            }
        </style>
    </head>
    <body>
        <div id="main" style="width: 700px; overflow:hidden;">
            <table width="100%" border="1" cellpadding="2" cellspacing="0">
                <tr>
                    <?php if ($_GET['mdl'] != 2) { ?>
                        <td width="500px" rowspan="2">
                            <table width="100%" cellpadding="0" border="0" cellspacing="0" style="border:0px solid white;padding-bottom: 15px !important;">
                                <tr>
                                    <td width="200px">
                                        <?php if (file_exists("./../Pessoas/" . $_SESSION["contrato"] . "/Empresa/logo_" . $filial_orc . ".jpg")) { ?>
                                            <div style="line-height: 5px;">&nbsp;</div><img src ="./../Pessoas/<?php echo $_SESSION["contrato"]; ?>/Empresa/logo_<?php echo $filial_orc; ?>.jpg" width="150" height="50" align="middle" />
                                        <?php } else { ?>
                                            <div style="line-height: 5px;">&nbsp;</div>
                                        <?php } ?>
                                    </td>                        
                                    <td width="265px" >
                                        <div style="text-align:right;font-size: 8px;"><?php echo $_SESSION["cabecalho"]; ?></div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    <?php } ?>            
                    <td width="<?php echo ($_GET['mdl'] != 2 ? "200" : "250"); ?>px" colspan="2" align="center" style="line-height: 40px;font-size:17px;"><b>RESUMO DO CAIXA</b></td>
                </tr>
                <tr>
                    <td style="padding-left:5px">Data:</td>
                    <td align="center" ><?php echo getData("T"); ?></td>
                </tr>
            </table>
            <?php if ($_GET['mdl'] == 2 && $_GET['opr'] !== "") { ?>             
            <table border="1" style="font-size: 10px;">
                <tr>
                    <td width="250px"><?php echo "Operador: " . utf8_encode($_GET['opr']); ?></td>
                </tr>
            </table>
            <?php } else { ?>
            <br><br>            
            <table border="1" style="font-size: 10px;">
                <tr>
                    <?php if ($_GET['emp'] !== '') { ?>
                        <td>
                            Loja : <?php echo $_GET['emp']; ?>
                        </td>
                    <?php } ?>
                    <?php
                    if ($_GET['ban'] !== '') {
                        $cur = odbc_exec($con, "select razao_social from sf_bancos where id_bancos = " . $_GET['ban']) or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) {
                            ?>
                            <td><?php echo "Banco: " . utf8_encode($RFP["razao_social"]); ?></td>
                            <?php
                        }
                    }
                    ?>
                    <?php if ($_GET['opr'] !== "") { ?>
                        <td><?php echo "Operador: " . utf8_encode($_GET['opr']); ?></td>
                    <?php } ?>
                    <td><?php echo "Periodo de " . ($_GET['dtt'] === "1" ? 'Venda' : 'Recebimento') . ": " . str_replace('_', '/', $_GET['dti']) . " até " . str_replace('_', '/', $_GET['dtf']); ?></td>
                </tr>
            </table>
            <?php } if ($_GET['mdl'] != 2) { ?>                
                <br><br>
                <table width="700px" border="1" style="font-size: 10px;">
                    <tr style="background-color: lightgrey;">
                        <td class="titulo" style="width: 45px;">Código</td>
                        <td class="titulo" style="width: 90px;">Data/Hora</td>
                        <td class="titulo" style="width: 70px;">Valor</td>
                        <td class="titulo" style="width: 55px;">Tipo</td>
                        <td class="titulo" style="width: 60px;">Matrícula</td>
                        <td class="titulo" style="width: 240px;">Cliente</td>
                        <td class="titulo" style="width: 60px;">Parcelas</td>
                        <td class="titulo" style="width: 80px;">Operador</td>
                    </tr>
                    <?php foreach ($arrayDetalhes as $itensPag) { ?>
                        <tr style="vertical-align: middle;line-height: 20px;">
                            <td style="text-align: center;"><br><?php echo $itensPag[0]; ?></td>
                            <td><?php echo $itensPag[1]; ?></td>
                            <td><?php echo $itensPag[2]; ?></td>
                            <td align="center"><?php echo $itensPag[3]; ?></td>
                            <td align="center"><?php echo $itensPag[4]; ?></td>
                            <td><?php echo strtoupper($itensPag[5]); ?></td>
                            <td align="center"><?php echo $itensPag[6]; ?></td>
                            <td><?php echo $itensPag[7]; ?></td>
                        </tr>
                        <?php
                        $iten = explode("-", strip_tags($itensPag[0]));
                        if (count($iten) > 1 && $iten[0] == "V" && is_numeric($iten[1]) && $_GET['mdl'] == 1) {
                            $cur = odbc_exec($con, "select conta_produto produto,cm.descricao produtodesc,quantidade,valor_total,valor_bruto
                            from sf_vendas_itens vi inner join sf_produtos cm on cm.conta_produto = vi.produto where vi.id_venda = " . $iten[1]);
                            while ($RFP = odbc_fetch_array($cur)) {
                                ?>
                                <tr style="vertical-align: middle;line-height: 20px;">
                                    <td colspan="8"><?php echo spaceEcho(80); ?>
                                        * <?php echo utf8_encode($RFP['produtodesc']) . " " . escreverNumero($RFP['quantidade'], 0, 0) . " x " . escreverNumero($RFP['valor_total']); ?></td>
                                </tr>        
                                <?php
                            }
                        }
                    }
                    ?>
                    <tr style="line-height: 25px;">
                        <td colspan="2" style="border-color: white;"><b>Total de Registros:</b></td>
                        <td colspan="6" style="border-color: white;"><?php echo sizeof($arrayDetalhes); ?></td>
                    </tr>
                </table>
            <?php } ?>    
            <br><br>
            <?php if ($_GET['mdl'] != 2) { ?>      
                <table border="0" cellspacing="0" cellpadding="0" width="700px" style="border:0px solid white;font-size: 11px;">
                    <tr>
                        <td style="width: 25px;"></td>            
                        <td>
                        <?php } ?>                
                        <table width="550px" border="1">
                            <tr style="background-color: lightgrey;text-align: center;">
                                <td class="titulo" style="width: <?php echo ($_GET['mdl'] != 2 ? "210" : "38"); ?>px;">Totais</td>
                                <td class="titulo" style="width: <?php echo ($_GET['mdl'] != 2 ? "105" : "53"); ?>px;">Entrada</td>
                                <td class="titulo" style="width: <?php echo ($_GET['mdl'] != 2 ? "105" : "53"); ?>px;">Saida</td>
                                <td class="titulo" style="width: <?php echo ($_GET['mdl'] != 2 ? "105" : "53"); ?>px;">Estorno</td>
                                <td class="titulo" style="width: <?php echo ($_GET['mdl'] != 2 ? "105" : "53"); ?>px;">Saldo</td>
                            </tr>
                            <?php
                            $totEntrada = 0;
                            $totSaida = 0;
                            $totEstorno = 0;
                            $totSaldo = 0;
                            foreach ($arrayTotais as $tpPag) {
                                $totEntrada = $totEntrada + valoresNumericos2($tpPag[1]);
                                $totSaida = $totSaida + valoresNumericos2($tpPag[2]);
                                $totEstorno = $totEstorno + valoresNumericos2($tpPag[3]);
                                $totSaldo = $totSaldo + valoresNumericos2($tpPag[4]);
                                ?>
                                <tr style="vertical-align: middle;line-height: 20px;">
                                    <td><?php echo $tpPag[0]; ?></td>
                                    <td><?php echo $tpPag[1]; ?></td>
                                    <td><?php echo $tpPag[2]; ?></td>                       
                                    <td><?php echo $tpPag[3]; ?></td>
                                    <td><?php echo $tpPag[4]; ?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td style="padding-left:5px;"><b>Total:</b></td>
                                <td style="padding-left:5px;"><b><?php echo escreverNumero($totEntrada, ($_GET['mdl'] == 2 ? 0 : 1)); ?></b></td>
                                <td style="padding-left:5px;"><b><?php echo escreverNumero($totSaida, ($_GET['mdl'] == 2 ? 0 : 1)); ?></b></td>
                                <td style="padding-left:5px;"><b><?php echo escreverNumero($totEstorno, ($_GET['mdl'] == 2 ? 0 : 1)); ?></b></td>
                                <td style="padding-left:5px;"><b><?php echo escreverNumero($totSaldo, ($_GET['mdl'] == 2 ? 0 : 1)); ?></b></td>
                            </tr>
                        </table>
                        <?php if ($_GET['mdl'] != 2) { ?>                  
                        </td>
                        <td style="width: 25px;"></td>
                    </tr>                    
                </table>                
            <?php } if ($_GET['dtt'] == 1) { ?>      
            <br><br>
            <?php if ($_GET['mdl'] != 2) { ?>
            <br><br>
                <table width="700px" border="1" style="font-size: 10px;">
                    <tr style="background-color: lightgrey;">
                        <td class="titulo" style="text-align: center;" colspan="8">
                            Demonstrativo de Pagamento em Carteira:
                        </td>
                    </tr>
                    <tr style="background-color: lightgrey;">
                        <td class="titulo" style="width: 45px;">Código</td>
                        <td class="titulo" style="width: 90px;">Data/Hora</td>
                        <td class="titulo" style="width: 70px;">Valor</td>
                        <td class="titulo" style="width: 55px;">Tipo</td>
                        <td class="titulo" style="width: 60px;">Matrícula</td>
                        <td class="titulo" style="width: 240px;">Cliente</td>
                        <td class="titulo" style="width: 60px;">Parcelas</td>
                        <td class="titulo" style="width: 80px;">Operador</td>
                    </tr>
                    <?php foreach ($arrayDetalhesCarteira as $itensPag) { ?>
                        <tr style="vertical-align: middle;line-height: 20px;">
                            <td style="text-align: center;"><br><?php echo $itensPag[0]; ?></td>
                            <td><?php echo $itensPag[1]; ?></td>
                            <td><?php echo $itensPag[2]; ?></td>
                            <td align="center"><?php echo $itensPag[3]; ?></td>
                            <td align="center"><?php echo $itensPag[4]; ?></td>
                            <td><?php echo strtoupper($itensPag[5]); ?></td>
                            <td align="center"><?php echo $itensPag[6]; ?></td>
                            <td><?php echo $itensPag[7]; ?></td>
                        </tr>
                        <?php
                        $iten = explode("-", strip_tags($itensPag[0]));
                        if (count($iten) > 1 && $iten[0] == "V" && is_numeric($iten[1]) && $_GET['mdl'] == 1) {
                            $cur = odbc_exec($con, "select conta_produto produto,cm.descricao produtodesc,quantidade,valor_total,valor_bruto
                            from sf_vendas_itens vi inner join sf_produtos cm on cm.conta_produto = vi.produto where vi.id_venda = " . $iten[1]);
                            while ($RFP = odbc_fetch_array($cur)) {
                                ?>
                                <tr style="vertical-align: middle;line-height: 20px;">
                                    <td colspan="8"><?php echo spaceEcho(80); ?>
                                        * <?php echo utf8_encode($RFP['produtodesc']) . " " . escreverNumero($RFP['quantidade'], 0, 0) . " x " . escreverNumero($RFP['valor_total']); ?></td>
                                </tr>        
                                <?php
                            }
                        }
                    }
                    ?>
                    <tr style="line-height: 25px;">
                        <td colspan="2" style="border-color: white;"><b>Total de Registros:</b></td>
                        <td colspan="6" style="border-color: white;"><?php echo sizeof($arrayDetalhes); ?></td>
                    </tr>
                </table>
            <br><br>
            <?php } if ($_GET['mdl'] != 2) { ?>      
                <table border="0" cellspacing="0" cellpadding="0" width="700px" style="border:0px solid white;font-size: 11px;">
                    <tr>
                        <td style="width: 25px;"></td>            
                        <td>
                        <?php } ?>                
                        <table width="550px" border="1">
                            <tr style="background-color: lightgrey;text-align: center;">
                                <td class="titulo" style="width: <?php echo ($_GET['mdl'] != 2 ? "525" : "197"); ?>px;">Demonstrativo Pag. Carteira:</td>
                                <td class="titulo" style="width: <?php echo ($_GET['mdl'] != 2 ? "105" : "53"); ?>px;">Valor</td>
                            </tr>
                            <?php
                            $totCarteira = 0;
                            foreach ($arrayCarteira as $tpPag) {
                                $totCarteira = $totCarteira + valoresNumericos2($tpPag[1]);
                                ?>
                                <tr style="vertical-align: middle;line-height: 20px;">
                                    <td><?php echo $tpPag[0]; ?></td>
                                    <td><?php echo $tpPag[1]; ?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td style="padding-left:5px;"><b>Total:</b></td>
                                <td style="padding-left:5px;"><b><?php echo escreverNumero($totCarteira, ($_GET['mdl'] == 2 ? 0 : 1)); ?></b></td>
                            </tr>
                        </table>
                        <?php if ($_GET['mdl'] != 2) { ?>                  
                        </td>
                        <td style="width: 25px;"></td>
                    </tr>                    
                </table>                            
            <?php }} ?>                   
        </div>
        <div id="foot_0" class="assinatura" >
            <table width="<?php echo ($_GET['mdl'] != 2 ? "708" : "510"); ?>px" border="1" cellspacing="0">
                <tr>
                    <td style="height:50px; width:49%"></td>
                    <?php if ($_GET['mdl'] != 2) { ?>
                        <td style="height:50px; width:50%"></td>
                    <?php } ?> 
                </tr>
                <tr>
                    <td style="text-align:center">Visto Operador</td>
                    <?php if ($_GET['mdl'] != 2) { ?>
                        <td style="text-align:center">Visto Superior</td>
                    <?php } ?> 
                </tr>
                <tr>
                    <td style="height:25px; text-align:center; vertical-align:bottom">____/____/________</td>
                    <?php if ($_GET['mdl'] != 2) { ?>
                        <td style="height:25px; text-align:center; vertical-align:bottom">____/____/________</td>
                    <?php } ?> 
                </tr>
            </table>
            <div style="font-size: 10px;margin-top: 15px;">&nbsp;&nbsp;Gerado pelo sistema SIVIS ERP - www.sivis.com.br</div>                
        </div>
    </body>
</html>
