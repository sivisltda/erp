<?php
session_start();
include "./../../Connections/configini.php";

if (is_numeric($_GET["id"])) {
    $query = "select * from sf_fornecedores_despesas where id_fornecedores_despesas = " . $_GET["id"];
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $empresa = utf8_encode($RFP["empresa"]);
        $cliente = utf8_encode($RFP["id_fornecedores_despesas"]);
        $razao_social_cli = utf8_encode($RFP['razao_social']);
        $nome_fantasia_cli = utf8_encode($RFP['nome_fantasia']);
        $status = utf8_encode($RFP['fornecedores_status']);
        $trancarData = escreverData($RFP['trancarData']);
        $trancarMotivo = utf8_encode($RFP['trancarMotivo']);        
        $endereco_cli = utf8_encode($RFP['endereco']) . ",N° " . utf8_encode($RFP['numero']) . " " . utf8_encode($RFP['complemento']) . "<br>" . utf8_encode($RFP['bairro']) . " " . utf8_encode($RFP['cidade_nome']) . "-" . utf8_encode($RFP['estado_sigla']) . " " . utf8_encode($RFP['cep']);
    }
    if (is_numeric($empresa)) {
        $cur = odbc_exec($con, "select * from sf_filiais where numero_filial = " . $empresa);
        while ($RFP = odbc_fetch_array($cur)) {
            $cpf_cnpj = utf8_encode($RFP["cnpj"]);
            $cedente = utf8_encode($RFP["razao_social_contrato"]);
            $nome_fantasia = utf8_encode($RFP["nome_fantasia_contrato"]);
            $inscricao = utf8_encode($RFP["inscricao_estadual"]);
            $endereco = utf8_encode($RFP['endereco']) . " n°" . utf8_encode($RFP['numero']) . "," . utf8_encode($RFP['bairro']);
            $cidade_uf = utf8_encode($RFP['cidade_nome']) . " " . utf8_encode($RFP['estado']);
            $cep = utf8_encode($RFP['cep']);
            $telefone = utf8_encode($RFP['telefone']);
            $site = utf8_encode($RFP['site']);
        }
    }
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
            body {
                font-size: 10px;
                font-family: Tahoma;
            }
            table {
                width: 100%;
                max-width: 302px;
                font-size: 10px;
                font-family: Tahoma;
            }
            .emp {
                font-size: 10px;
                font-weight: bold;
                text-align: center;
                padding: 5px 0px 5px 0px;
            }
            .end {
                text-align: center;
                padding: 5px 0px 5px 0px;
                font-size: 9px !important;
            }
            .doc {
                padding-top: 5px;
            }
            .lin {
                padding-top: 5px;
                border-bottom: 1px solid #000;
            }
            .spc {
                padding-bottom: 5px;
            }
            .grupo {
                padding: 3px 0px 3px 0px;
                background-color: #DDD;
                font-weight: bold;
            }
            .tit {
                font-size: 12px;
                font-weight: bold;
                text-align: center;
            }
        </style>
    </head>

    <body>
        <table cellspacing="0" cellpadding="0" style="padding: 10px 10px 10px 10px;">
            <tr>
                <td colspan="6">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="6" class="tit"><?php echo $nome_fantasia; ?></td>
            </tr>
            <tr>
                <td colspan="6">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="6" class="end">
                    <?php echo (strlen($cedente) > 0 ? $cedente . "<br>" : ""); ?>
                    <?php echo (strlen($endereco) > 0 ? $endereco . "<br>" : ""); ?>
                    <?php echo (strlen($cidade_uf) > 0 ? $cidade_uf : ""); ?>
                    <?php echo (strlen($cep) > 0 ? " - CEP: " . $cep : ""); ?>
                    <br>
                    <?php echo (strlen($telefone) > 0 ? $telefone . "<br>" : ""); ?>
                    <?php echo (strlen($site) > 0 ? $site . "<br>" : ""); ?>
                </td>
            </tr>
            <tr>
                <td colspan="6" class="doc"><b>CNPJ: <?php echo $cpf_cnpj; ?></b><br/>I.E.: <?php echo $inscricao; ?></td>
            </tr>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <tr> <td colspan="6" style="line-height: 5px;">&nbsp;</td> </tr>
            <tr> <td colspan="6" style="line-height: 5px;">&nbsp;</td> </tr>
            <tr>
                <td colspan="6" class="tit">RECIBO DESLIGAMENTO DE TITULARIDADE</td>
            </tr>
            <tr> <td colspan="6" class="lin"></td> </tr>
            <tr> <td colspan="6" style="line-height: 5px;">&nbsp;</td> </tr>
            <tr>
                <td colspan="4" class="doc"><b>Cliente:</b></td>
                <td colspan="2" class="doc" style="text-align:right"><b></b></td>
            </tr>
            <tr>
                <td colspan="6" class="end" style="text-align: left!important;"><?php
                    echo $cliente . " - " . $razao_social_cli;
                    if ($nome_fantasia_cli !== "") {
                        echo "(" . $nome_fantasia_cli . ")";
                    }
                    echo "<br>" . $endereco_cli;
                    ?>
                </td>
            </tr>
            <tr> <td colspan="6" class="lin"></td> </tr>
            <tr> <td colspan="6" style="line-height: 5px;">&nbsp;</td> </tr>
            <tr>
                <td colspan="4" class="doc"><b>Status do Cliente:</b></td>
                <td colspan="2" class="doc" style="text-align:right"><b></b></td>
            </tr>
            <tr>
                <td colspan="6" class="end" style="text-align: left!important;"><?php echo $status. "<br>" . $trancarData; ?></td>
            </tr>            
            <tr>
                <td colspan="6" class="end" style="text-align: left!important;"><?php echo $status. "<br>" . $trancarMotivo; ?></td>
            </tr>            
            <tr><td colspan="6" class="lin"></td></tr>
            <tr>
                <td colspan="2" class="doc"><b>Operador</b></td>
                <td colspan="4" class="doc" style="text-align:right"><?php echo $_SESSION["login_usuario"]; ?></td>
            </tr>           
            <tr><td colspan="6" class="doc"></td></tr>
            <tr><td colspan="6" class="doc"></td></tr>
            <tr><td colspan="6" class="doc"></td></tr>
            <tr>
                <td colspan="1" class="doc"></td>
                <td colspan="4" class="lin"></td>
                <td colspan="1" class="doc"></td>                
            </tr>
            <tr>
                <td colspan="1" class="doc"></td>
                <td colspan="4" class="doc" style="text-align:center"><?php echo $razao_social_cli; ?></td>
                <td colspan="1" class="doc"></td>                
            </tr>                       
        </table>
    </body>
</html>