<?php
include $_GET['PathArqInclude'];
$arrayDetalhes = array();
$title = $_GET['NomeArq'];
$filtro = $_GET['filter'];
$filial = '001';
$cur = odbc_exec($con, "select TOP 1 id_filial_f from sf_usuarios_filiais where id_usuario_f = '".$_SESSION["id_usuario"]."'");
while ($RFP = odbc_fetch_array($cur)){
    $filial = $RFP['id_filial_f'];
}
$filial = str_pad($filial, 3, '0', STR_PAD_LEFT);
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
        body {
            margin:0px;
            padding:0px;
        }
        table {
            font: 8px Calibri;
            margin-bottom: 5px;
            border: 1px solid #999;
            border-collapse: collapse;
            vertical-align: middle;
        }
        .titulo {
            font: 14px Calibri;
            font-weight: Bold;
            padding: 2px;
            padding-left: 5px;
            background-color: #eee;
        }
        .tabela {
            height: 18px;
            padding-left: 5px;
            padding-right: 5px;
        }
    </style>
</head>
<body>
<div id="main" style="width: 700px; overflow:hidden;">

    <?php
        $qtdAmbAtual = 1; $qtdItemAtual = 1;
        $qtdAmbiente = sizeof($output[1]) - 1;
        $qtdtbl = $qtdAmbiente/5;
        for($tbl=0;$tbl<$qtdtbl;$tbl++){ ?>
        <table width="100%" border="1" cellpadding="2" cellspacing="0">
            <tr>
                <td width="<?php if(isset($_GET['pOri']) && $_GET['pOri'] == "L"){echo "807px";}else{echo "500px";}?>" rowspan="2">
                    <table width="100%" cellpadding="0" border="0" cellspacing="0" style="border:0px solid white;padding-bottom: 15px !important;">
                        <tr>
                            <td width="200px">
                                <?php if (file_exists("./../Pessoas/" . $_SESSION["contrato"] . "/Empresa/logo_" . $filial . ".jpg")) { ?>
                                    <div style="line-height: 5px;">&nbsp;</div><img src ="./../Pessoas/<?php echo $_SESSION["contrato"]; ?>/Empresa/logo_<?php echo $filial; ?>.jpg" width="150" height="50" align="middle" />
                                <?php } else { ?>
                                    <div style="line-height: 5px;">&nbsp;</div>
                                <?php } ?>
                            </td>
                            <td width="<?php if(isset($_GET['pOri']) && $_GET['pOri'] == "L"){echo "572px";}else{echo "265px";}?>" >
                                <div style="text-align:right;font-size: 8px;"><?php echo $_SESSION["cabecalho"]; ?></div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="200px" colspan="2" align="center" style="line-height: 40px;font-size:17px;"><b><?php echo $title; ?></b></td>
            </tr>
            <tr>
                <td style="padding-left:5px">Data:</td>
                <td align="center" ><?php echo getData("T"); ?></td>
            </tr>
        </table>
        <br><br>
        <table>
            <tr>
                <td><?php echo $filtro; ?></td>
            </tr>
        </table>
        <br><br>
        <table width="700px" border="1" style="font-size: 8px;">
            <tr>
                <td class="titulo" style="width: 50px;vertical-align: middle;"></td>
            <?php
            for($ambiente=$qtdAmbAtual;$ambiente<=$qtdAmbAtual+5;$ambiente++) {
                if($ambiente<=$qtdAmbiente){ ?>
                <td class="titulo" style="width: 160px;">
                    <?php  echo $output[1][$ambiente]; ?>
                </td>
            <?php }
            } $qtdAmbAtual +=5;
            ?>
            </tr>
            <?php
            for ($hr=0;$hr<=23;$hr++){ ?>
                <tr>
                    <td style="height: 21.5px;background-color: #eee;">
                        <?php  echo $output[0][$hr]['hr']; ?>
                    </td>
                    <?php
                    for($ambiente=$qtdItemAtual;$ambiente<=$qtdItemAtual+5;$ambiente++) {
                        if($ambiente<=$qtdAmbiente){ ?>
                        <td style="font-size: 7px;"><?php echo str_replace('|','<br>',$output[0][$hr][$ambiente]); ?></td>
                    <?php }
                    }
                    ?>
                </tr>
            <?php } $qtdItemAtual +=5;
            ?>
        </table>
        <?php if($tbl <($qtdtbl-1) && $qtdtbl > 1) { ?>
        <span style="page-break-before: always;"></span>
        <?php } ?>
    <?php  }?>
</div>
</body>
</html>