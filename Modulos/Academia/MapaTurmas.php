<?php include "./../../Connections/configini.php"; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/informacoesGerenciais.css"/>
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <style type="text/css">
            .horarios-th{
                border-style: solid;
                border-width: 1px;
                border-color: gray;
                width: 150px;
            }
            .horarios td{
                border-style: dotted;
                border-width: 1px;
                border-color: gray;
                width: 150px;
            }

            table#tbAmbiente {
                position: relative;
                width: 700px;
                overflow: hidden;
                border-collapse: collapse;
            }
            /*thead*/
            #tbAmbiente thead {
                position: relative;
                display: block; /*seperates the header from the body allowing it to be positioned*/
                width: 700px;
                overflow: visible;
            }
            #tbAmbiente thead tr {
                border: 1px solid gray;
            }

            #tbAmbiente thead th {
                min-width: 155px;
                height: 12px;
                border: 1px solid gray;
            }

            #tbAmbiente thead th:nth-child(1) {/*first cell in the header*/
                min-width: 80px !important;
                position: relative;
                display: grid; /*seperates the first cell in the header from the header*/
                background-color: #eee;
            }

            /*tbody*/
            #tbodyAmbiente {
                position: relative;
                display: block; /*seperates the tbody from the header*/
                height: 400px;
                overflow: scroll;
            }

            #tbodyAmbiente td {
                width: 155px;
                border: 1px dotted gray;
                color: #005580;
                text-align: center;
                background-color: white;
                margin-top: 7px;
            }
            #tbodyAmbiente tr td:nth-child(1) {  /*the first cell in each tr*/
                width: 80px !important;
                position: relative;
                display: grid; /*seperates the first column from the tbody*/
                height: 20px;
                background-color: #eee;
            }
        </style>        
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"><span class="ico-arrow-right"></span></div>
                        <h1>Academia<small>Informações Gerenciais</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="block">
                                <div id="formulario">
                                    <div class="boxhead">
                                        <div class="boxtext">Controle de Turmas</div>
                                        <div class="boxtable" style="margin-bottom:15px; background:#FFF">
                                            <div style="float:left;">
                                                <div id="dtCalendario" style="border: 1px solid #c5c5c5;margin-left: 10px;margin-right: 27px;"></div>
                                                <div style="clear:both; height:5px"></div>
                                            </div>
                                            <div id="tablediv9" style="width: calc(100% - 300px);float:left;">
                                                <div style="float:left; width:32.7%;">
                                                    <span>Ambiente:</span>
                                                    <select name="txtAmbienteTurma" id="txtAmbienteTurma" class="input-medium" style="width:100%"></select>
                                                </div>
                                                <div style="float:left; width:66%;margin-left: 1%">
                                                    <span>Plano:</span>
                                                    <select name="itemsPlanoTurma[]" id="mscPlanosTurma" multiple="multiple" class="select" style="width:100%" class="input-medium"></select>
                                                </div>
                                                <div style="clear:both; height:5px"></div>
                                                <div style="float:left; width:32.7%;">
                                                    <span>Horario:</span>
                                                    <select name="txtHorarioTurma" id="txtHorarioTurma" class="input-medium" style="width:100%"></select>
                                                </div>
                                                <div style="float:left; width:66%;margin-left: 1%">
                                                    <span>Professor:</span>
                                                    <select name="txtProfessorTurma" id="txtProfessorTurma" class="input-medium" style="width:100%"></select>
                                                </div>
                                                <div style="clear:both; height:5px"></div>
                                                <div style="float:left; width:69%;">
                                                    <span>Cliente:</span>
                                                    <input type="text" id="txtClienteTurma" style="width:100%" />
                                                    <input type="hidden" id="txtCodClienteTurma">
                                                </div>
                                                <div style="float:left; width:30%; margin-left:1%; padding-top:15px">
                                                    <input type="button" class="button button-blue btn-primary" style="line-height:17px" onclick="AgendarAlunoTurma()" name="btnAgendarTurma" id="btnAgendarTurma" value="Agendar"/>
                                                    <input type="button" onclick="BuscarTurma()" title="Agenda" class="button button-turquoise btn-primary" style="line-height:17px" value="Buscar"/>
                                                </div>
                                                <div style="clear:both; height:5px"></div>
                                            </div>
                                            <div style="clear:both"></div>
                                        </div>
                                    </div>
                                    <span id="tbTurma">
                                        <div class="boxhead">
                                            <div class="boxtext" style="position:relative">
                                                Agendamento
                                                <div style="top:4px; right:2px; line-height:1; position:absolute">
                                                    <button type="button" class="button button-blue btn-primary" style="margin:0; line-height:17px;margin-right: 11px;" onclick="imprimirTbAgendamento();">
                                                        <span class="ico-print icon-white"></span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="boxtable">
                                            <div class="body" style="margin-left:0; padding:0">
                                                <div class="content" style="min-height:30px; margin-top:0">
                                                    <table filtro="" class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbAgendamentos">
                                                        <thead>
                                                            <tr>
                                                                <th width="35%"><center>Cliente</center></th>
                                                        <th width="15%"><center>Modalidade</center></th>
                                                        <th width="15%"><center>Horário</center></th>
                                                        <th width="25%"><center>Professor</center></th>
                                                        <th width="10%"><center>Ambiente</center></th>
                                                        </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                            </div>
                                            <div style="clear:both;height: 5px;"></div>                                            
                                            <div class="boxhead">
                                                <div class="boxtext" style="position:relative">
                                                    Ambientes
                                                    <div style="top:4px; right:2px; line-height:1; position:absolute">
                                                        <button type="button" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="imprimirMapaAmbiente();">
                                                            <span class="ico-print icon-white"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>

                                            <table id="tbAmbiente">
                                                <thead><tr></tr></thead>
                                                <tbody id="tbodyAmbiente"> </tbody>
                                            </table>
                                            <div style="clear:both;height: 5px;"></div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="data-fluid tabbable" style="margin-top:10px; margin-bottom:15px">
                        <div class="tab-content" style="border:1px solid #DDD; border-top:0; padding-top:10px; background:#F6F6F6">
                            <div class="tab-pane" id="tab7">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../js/graficos.js"></script>    
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/jquery.priceformat.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/multiselect/jquery.multi-select.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/charts.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type='text/javascript' src="../../js/numeral.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript" src="js/MapaTurmas.js"></script>
    </body>
    <?php odbc_close($con); ?>
</html>