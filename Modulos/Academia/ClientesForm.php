<?php
    include "../../Connections/configini.php";
    include "form/ClientesFormServer.php";
    require_once('../../util/util.php');
    $dtIni = getData("B");
    $dtFim = getData("T");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
        <title>Cadastro de Clientes</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <link href="../../js/plugins/jquery.ajax-combobox/dist/jquery.ajax-combobox.css" rel="stylesheet">        
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    </head>
    <style>
        input {text-transform: uppercase;}
        #tblItensPag td{
            vertical-align: middle;
        }
        .dataTables_scrollHeadInner{
            width: 100% !important;
        }
        .dataTables_scrollHeadInner table.dataTable.no-footer{
            width: 97% !important;
        }        
        #tbListaDCC td { padding: 5px; }
        #tbListaDCC td:nth-child(7) { text-align: right; }
    </style>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php");?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1 style="margin-top:8px">Clientes</h1>
                    </div>
                    <div class="row-fluid">
                        <form name="frmClientesForm" id="frmClientesForm">
                            <div class="span12">
                                <div class="block">
                                    <div id="formulario">
                                        <div style="background:#F6F6F6; border:1px solid #DDD; padding:10px">
                                            <div style="width:150px;height:150px; padding:0 2% 0 1%; float:left">
                                                <input id="btnRemImg" class="btn red" type="button" value="x" style="<?php if(!file_exists("./../../Pessoas/".$contrato."/Clientes/".$id.".png")){ echo "display:none;"; } ?>margin:0px; padding:2px 4px 3px 4px; line-height:10px;position:absolute; left:24px; z-index:700; cursor:pointer">
                                                <img onclick="UploadForm()" id="ImgCliente" src="
                                                <?php
                                                    if($id != "" and file_exists("./../../Pessoas/".$contrato."/Clientes/".$id.".png")){
                                                        $source = file_get_contents("./../../Pessoas/".$contrato."/Clientes/".$id.".png");
                                                        echo "data:image/png;base64,".base64_encode($source);
                                                    } else {
                                                        echo "./../../img/img.png";
                                                    } ?>"
                                                 style="width:100%; height:150px; border-radius:50%; cursor:pointer"/>
                                            </div>
                                            <div style="width:calc(97% - 150px); float:right; padding:8px 0">
                                                <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                                                <input name="txtAt" id="txtAt" value="<?php echo $_GET['at']; ?>" type="hidden"/>
                                                <input name="txtDisabled" id="txtDisabled" value="<?php echo $disabled; ?>" type="hidden"/>
                                                <input name="txtSendEmail" id="txtSendEmail" type="hidden"/>
                                                <input name="txtSendSms" id="txtSendSms" type="hidden"/>
                                                <input name="txtAcessoDesc" id="txtAcessoDesc" type="hidden" value="<?php echo $ckb_out_desconto_; ?>"/>
                                                <input name="txtAcessoAlunBlock" id="txtAcessoAlunBlock" type="hidden" value="<?php echo $ckb_aca_cli_block; ?>"/>
                                                <input name="txtProspectIr" id="txtProspectIr" value="<?php echo $prospectIr; ?>" type="hidden"/>
                                                <input name="txtServicoAdesao" id="txtServicoAdesao" value="<?php echo $servico_adesao; ?>" type="hidden"/>                                                
                                                <input id="txtReadOnly" value="<?php echo $ckb_adm_cli_read_; ?>" type="hidden"/>                                                
                                                <div style="width:70%; float:left">
                                                    <span><span style="color:red;">*</span> Nome Completo: </span>
                                                    <div style="display: flex;">
                                                        <input id="txtNome" name="txtNome" type="text" value="<?php echo $nome; ?>" style="height: 40px;font-size:18px; font-weight:500; font-family:'Segoe UI', arial, sans-serif;"/>
                                                    </div>
                                                </div>
                                                <div style="width:16%; float:left; margin-left:1%">
                                                    <span>Cadastro:</span>
                                                    <input id="txtDtCad" name="txtDtCad" type="text" value="<?php echo $dtCadastro; ?>" class="<?php echo ($id == "" && $mdl_clb_ > 0 ? "datepicker" : ""); ?> inputCenter" style="height: 40px;font-size:18px; background-color: #eee; font-weight:500; font-family:'Segoe UI', arial, sans-serif" <?php echo ($id == "" && $mdl_clb_ > 0 ? "" : "readonly"); ?> />
                                                </div>
                                                <div style="width:12%; float:left; margin-left:1%">
                                                    <span>Matricula:</span>
                                                    <input type="text" value="<?php echo $id; ?>" style="height: 40px;font-size:18px; font-weight:500; font-family:'Segoe UI', arial, sans-serif" readonly />
                                                </div>
                                                <div style="height:10px; clear:both"></div>
                                                <div style="float:left;">
                                                    <div style="float:left; color:green; width: 100%;">
                                                        <span id="lblTitTitle" style="display: none;"> Titular: </span>
                                                        <span id="lblTitular" style="font-size: 16px;padding-top: 6px; display: none;"><?php if (strlen($idTitular) > 0) { ?><a style="color: green;" href="ClientesForm.php?id=<?php echo $idTitular; ?>"><?php echo strtoupper($nmTitular); ?></a><?php } ?></span>
                                                    </div>
                                                    <div style="float:left; color:green; width: 100%;">
                                                        <span id="lblDepTitle" style="display: none;"> Dependente(s): </span>
                                                        <span id="lblDependentes" style="font-size: 16px;padding-top: 6px; display: none;"></span>
                                                        <img id="imgDepPlus" style="display: none;" src="./../../../img/details_open.png" data-id='0'>
                                                        <div id="lblDependentesMais" style="display: none;"></div>
                                                    </div>
                                                    <div style="float:left; color:royalblue; width: 100%;">
                                                        <span id="lblRespTitle" style="display: none;"> Responsável Legal: </span>
                                                        <span id="lblResponsavel" style="font-size: 16px; padding-top: 6px; display: none;"></span>                                                        
                                                    </div>
                                                    <div style="float:left; color:royalblue; width: 100%;">
                                                        <span id="lblRespDeTitle" style="display: none;"> Sócio(s) vinculado(s): </span>
                                                        <span id="lblResponsavelDe" style="font-size: 16px; padding-top: 6px; display: none;"></span>                                                        
                                                    </div>
                                                </div>                                                
                                                <div id="statusCli" style="float:right;text-align: right;">
                                                    <div class="button label-<?php echo $statusAluno; ?>" style="width:130px; margin:0">
                                                        <div class="name"><center><?php echo strpos($statusAluno, 'Cred') ? 'ATIVO' : ($statusAluno == 'Ferias' ? 'CONGELADO' : ($statusAluno == 'AtivoEmAberto' ? 'ATIVO EM ABERTO' : ($statusAluno == 'AtivoAbono' ? 'ATIVO ABONO' : ($statusAluno == 'DesligadoEmAberto' ? 'DESLIGADO EM ABERTO' : strtoupper($statusAluno))))); ?></center></div>
                                                    </div>
                                                </div>
                                                <div style="height:10px; clear:both"></div>                                                
                                                <div style="float:right;">
                                                    <div id="lblTotalDisp"> </div>
                                                </div>
                                            </div>
                                            <div style="clear:both"></div>
                                        </div>
                                        <div class="data-fluid tabbable" style="margin-top:10px; margin-bottom:15px">
                                            <ul class="nav nav-tabs" style="margin-bottom:0px">
                                                <li <?php echo (isset($_GET["at"]) ? "" : "class=\"active\"") ?>><a href="#tab1" data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Pessoais</b></a></li>
                                                <?php if ($id != "") { 
                                                    if ($mdl_seg_ > 0) { ?>
                                                        <li><a href="#tab0" data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Veículos</b></a></li>                                                
                                                    <?php } ?>                                                    
                                                <li <?php echo (isset($_GET["at"]) ? "class=\"active\"" : "") ?>><a href="#tab2" data-toggle="tab" onClick="$('#tbDCC').hide(); limparChamado();"><b>Atendimentos</b></a></li>
                                                <?php if ($ckb_adm_cli_read_ == 0) { ?>
                                                <li><a href="#tab3" data-toggle="tab" onClick="$('#tbDCC').show();"><b>DCC</b></a></li>
                                                <li><a href="#tab4" data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Planos</b></a></li>                                                
                                                <?php if ($mdl_ser_ > 0) { ?>
                                                    <li><a href="#tab18" data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Serviços</b></a></li>
                                                <?php } if ($ckb_est_lorc_ == 1 && $mdl_clb_ == 0) { ?>                                                    
                                                    <li><a href="#tab21" data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Orçamentos</b></a></li>
                                                    <li><a href="#tab22" data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Vendas</b></a></li>
                                                <?php } if ($mdl_seg_ > 0) { ?>
                                                    <li><a href="#tab20" <?php echo ($totSinistro > 0 ? "style=\"color: red;\"" : ""); ?> data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Eventos</b></a></li>
                                                <?php } ?>                                                    
                                                <li><a href="#tab5" data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Documentos</b></a></li>
                                                <li><a href="#tab17" data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Agenda</b></a></li>
                                                <li><a href="#tab6" data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Acesso</b></a></li>
                                                <li><a href="#tab7" data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Convênios</b></a></li>                                                
                                                <?php }} if ($mdl_clb_ > 0 && $ckb_adm_cli_read_ == 0) { ?>
                                                <li><a href="#tab8" data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Clube</b></a></li>
                                                <?php } if ($id != "" && $ckb_adm_cli_read_ == 0) { ?>
                                                <li><a href="#tab9" data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Campos Livres</b></a></li>
                                                <?php } ?>
                                            </ul>
                                            <div class="tab-content" style="overflow:initial; border:1px solid #ddd; border-top:0px; padding-top:10px; background:#F6F6F6">
                                                <div class="tab-pane <?php echo (isset($_GET["at"]) ? "" : "active") ?>" id="tab1" style="margin-bottom:5px">
                                                    <?php include "include/DadosPessoais.php";?>                                                                                                        
                                                </div>
                                                <div class="tab-pane <?php echo (isset($_GET["at"]) ? "active" : "") ?>" id="tab2" style="margin-bottom:5px">
                                                    <?php include "../CRM/include/CRMFormTelemarketing.php";?>
                                                </div>
                                                <div class="tab-pane" id="tab18" style="margin-bottom:5px">
                                                    <?php include "../CRM/include/CRMFormServicos.php"; ?>
                                                </div>
                                                <div class="tab-pane" id="tab21" style="margin-bottom:5px">
                                                    <?php include "include/Orcamentos.php"; ?>
                                                </div>
                                                <div class="tab-pane" id="tab22" style="margin-bottom:5px">
                                                    <?php include "include/Vendas.php"; ?>
                                                </div>
                                                <div class="tab-pane" id="tab20" style="margin-bottom:5px">
                                                    <?php include "../CRM/include/CRMFormSinistros.php"; ?>
                                                </div>
                                                <div class="tab-pane" id="tab3" style="margin-bottom:5px">
                                                    <?php include "include/CartaoDcc.php";?>
                                                </div>
                                                <div class="tab-pane" id="tab4" style="margin-bottom:5px">
                                                    <?php include "include/AcademiaPlanos.php"; ?>
                                                </div>
                                                <div class="tab-pane" id="tab5" style="margin-bottom:5px">
                                                    <?php include "../CRM/include/Documentos.php";?>
                                                </div>
                                                <div class="tab-pane" id="tab6" style="margin-bottom:5px" >
                                                    <?php include "include/AcademiaAcesso.php";?>
                                                </div>
                                                <div class="tab-pane" id="tab7" style="margin-bottom:5px">
                                                    <?php include "include/Convenios.php";?>
                                                </div>
                                                <div class="tab-pane" id="tab8" style="margin-bottom:5px">
                                                    <?php include "include/Clube.php";?>
                                                </div>
                                                <div class="tab-pane" id="tab9" style="margin-bottom:5px">
                                                    <?php include "include/CamposLivres.php";?>
                                                </div>
                                                <div class="tab-pane" id="tab17" style="margin-bottom:5px">
                                                    <?php include "include/Agendamentos.php";?>
                                                </div>
                                                <?php if ($mdl_seg_ > 0) { ?>
                                                    <div class="tab-pane" id="tab0" style="margin-bottom:5px">
                                                        <?php include "include/Veiculos.php";?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <span id="tbDCC" style="display:none">
                                            <div class="boxhead">
                                                <div class="boxtext">Transações em DCC
                                                    <div style="font-size: 11px;display: inline-block;float: right;color: #666;padding: 0 10px;font-weight: bold;line-height: 30px;">Exibir Inválidos
                                                        <input name="ckb_dcc_invalidos" id="ckb_dcc_invalidos" value="1" type="checkbox" class="input-medium">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="boxtable" style="margin-bottom:0">
                                                <table id="tbListaDCC" class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th><center>Dt.Mens.</center></th>
                                                            <th>Mat.</th>
                                                            <th>Nm.Cartao</th>
                                                            <th><center>Pedido</center></th>
                                                            <th><center>Transação</center></th>
                                                            <th><center>Cartão</center></th>
                                                            <th><center>Estado</center></th>
                                                            <th><center>Valor</center></th>
                                                            <th><center>Mod.</center></th>
                                                            <th><center>Rec.</center></th>
                                                            <th><center>Dt.Pgto.</center></th>
                                                            <th><center>Usuário</center></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                                <div style="clear:both"></div>
                                            </div>
                                            <div class="boxfoot">
                                                <div class="boxtext">
                                                    <div style="float:left; width:170px">Total de <span id="lbl_dcc_total">0</span> Registros</div>
                                                    <div style="float:left; width:calc(50% - 170px); text-align:center; color:#c22439; font-size:14px">Total Bruto: <span id="lbl_dcc_vbruto"><?php echo escreverNumero(0,1); ?></span></div>
                                                    <div style="float:left; width:calc(50% - 170px); text-align:center; color:#c22439; font-size:14px">Total Líquido: <span id="lbl_dcc_vliquido"><?php echo escreverNumero(0,1); ?></span></div>
                                                    <div style="float:right; width:170px; text-align:right">
                                                        Saldo de Transações:&nbsp;<input type="text" id="saldoImp" class="input-medium" style="width:40px; padding:2px; margin:-3px 0 0 3px; display:none" readonly/>
                                                        <button type="button" id="saldoBtn" class="btn dblue" style="line-height:19px; width:40px; margin:-3px 0 0; background-color:#308698 !important" onClick="ler_saldo()">
                                                            <span class="icon-search icon-white"></span>
                                                        </button>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>        
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>        
        <script type='text/javascript' src='../../js/plugins/highlight/jquery.highlight-4.js'></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/jquery.creditCardValidator.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/amcharts.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/pie.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/serial.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/light.js"></script>
        <script type="text/javascript" src="../../js/moment.min.js"></script>
        <script type="text/javascript" src="../../js/jquery.cpfcnpj.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.ajax-combobox/dist/jquery.ajax-combobox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript" src="js/DadosPessoais.js"></script>        
        <script type="text/javascript" src="js/ClientesForm.js"></script>
        <script type="text/javascript" src="js/AcademiaPlanos.js"></script>
        <script type="text/javascript" src="js/AcademiaAcesso.js"></script>
        <script type="text/javascript" src="js/Clube.js"></script>        
        <script type="text/javascript" src="js/Agendamentos.js"></script>        
        <script type="text/javascript" src="js/CamposLivres.js"></script>
        <script type="text/javascript" src="js/ComissaoCliente.js"></script>
        <?php if ($ckb_est_lorc_ == 1 && $_SESSION["mod_emp"] == 0) { ?>
        <script type="text/javascript" src="js/Orcamentos.js"></script>        
        <script type="text/javascript" src="js/Vendas.js"></script>        
        <?php } ?>
        <script type="text/javascript" src="../CRM/js/CRMFormTelemarketing.js"></script>
        <script type="text/javascript" src="../CRM/js/CRMFormServicos.js"></script>
        <script type="text/javascript" src="../CRM/js/CRMDcc.js"></script>
        <script type="text/javascript" src="../CRM/js/CRMConvenios.js"></script>
        <script type="text/javascript" src="../CRM/js/CRMDocumentos.js"></script>        
        <script type="text/javascript" src="../CRM/js/CRMContratos.js"></script>        
        <script type="text/javascript" src="../Seguro/js/Veiculo.js"></script>
        <script type="text/javascript" src="../Seguro/js/Vistoria.js"></script>
        <script type="text/javascript" src="../Seguro/js/Sinistro.js"></script>        
        <script type="text/javascript" src="../Comercial/js/DisparosEmails.js"></script>
    </body>
    <?php odbc_close($con); ?>
</html>
