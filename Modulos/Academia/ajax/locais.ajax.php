<?php

require_once('../../../Connections/configini.php');
$id_area = $_REQUEST['cidade'];
$local = array();
if (is_numeric($id_area)) {
    $sql = "SELECT distinct bairro from sf_fornecedores_despesas where bairro is not null and len(bairro) > 0 and cidade = " . $id_area . " ORDER BY 1";
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('bairro' => utf8_encode($RFP['bairro']));
    }
}

echo(json_encode($local));
odbc_close($con);
