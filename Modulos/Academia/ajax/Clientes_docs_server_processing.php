<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');
require_once(__DIR__ . '/../../../util/excel/exportaExcel.php');

$idForn = $_GET['id_forn'];
$idDoc = $_GET['id_doc'];
$dt_doc = $_GET['dt_doc'];

if ($_GET['op'] == 'c') {    
    $disponivel = 1;
    $cur = odbc_exec($con, "select (select count(*) from sf_fornecedores_despesas_documentos where id_fornecedor_despesas = " . $idForn . " and id_documento = id_doc) usados,
    (select isnull(sum(quantidade),0) from sf_vendas inner join sf_vendas_itens on sf_vendas.id_venda = sf_vendas_itens.id_venda where cliente_venda = " . $idForn . " and produto = servico_doc) comprado
    from sf_documentos where servico_doc is not null and id_doc = " . $idDoc);
    while ($aRow = odbc_fetch_array($cur)) {
        $disponivel = $aRow['comprado'] - $aRow['usados'];
    }
    if ($disponivel > 0) {
        $query = "set dateformat dmy;
        insert into sf_fornecedores_despesas_documentos(id_fornecedor_despesas, id_documento, data_documento)
        values(" . $idForn . "," . $idDoc . ",'" . $dt_doc . "')";
        $cur = odbc_exec($con, $query);
        $output = array("result" => "YES");
    } else {
        $output = array("result" => "Não é possível efetuar essa operação!");        
    } 
}

if ($_GET['op'] == 'u') {
    $disponivel = 1;    
    $cur = odbc_exec($con, "select (select count(*) from sf_fornecedores_despesas_documentos where id_fornecedor_despesas = " . $idForn . " and id_documento = id_doc) usados,
    (select isnull(sum(quantidade),0) from sf_vendas inner join sf_vendas_itens on sf_vendas.id_venda = sf_vendas_itens.id_venda where cliente_venda = " . $idForn . " and produto = servico_doc) comprado
    from sf_documentos where servico_doc is not null and id_doc = " . (isset($_GET['id_docx']) ? $_GET['id_docx'] : $idDoc));
    while ($aRow = odbc_fetch_array($cur)) {
        $disponivel = $aRow['comprado'] - $aRow['usados'];
    }
    if ($disponivel > 0) {    
        $query = "set dateformat dmy;
        update sf_fornecedores_despesas_documentos set data_documento = " . (isset($_GET['dt_docx']) ? "'" . $_GET['dt_docx'] . "'" : "GETDATE()") . 
        (isset($_GET['id_docx']) ? ",id_documento = " . $_GET['id_docx'] : "") . " where id_fornecedor_despesas = " . $idForn . " and id_documento = " . $idDoc;
        $cur = odbc_exec($con, $query);
        $output = array("result" => "YES");
    } else {
        $output = array("result" => "Não é possível efetuar essa operação!");        
    }        
}

if ($_GET['op'] == 'd') {
    $query = "delete from sf_fornecedores_despesas_documentos where id_fornecedor_despesas= $idForn and id_documento =$idDoc";
    $cur = odbc_exec($con, $query);
    $output = array("result" => "YES");
}

if ($_GET['op'] == 'r') {
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => $iTotal,
        "iTotalDisplayRecords" => $iFilteredTotal,
        "aaData" => array()
    );
    $query = "set dateformat dmy;
    SELECT DATEADD(day,validade_doc,data_documento) vencimento,* FROM sf_fornecedores_despesas_documentos as F
    inner join dbo.sf_documentos as D on F.id_documento = D.id_doc
    where F.id_fornecedor_despesas = " . $idForn;
    $cur = odbc_exec($con, $query);
    while ($aRow = odbc_fetch_array($cur)) {
        $row = array();
        $cont = $cont + 1;
        $datetoday = new DateTime();
        $datetoday->format('Y-m-d');
        $dateVenc = new DateTime($aRow['vencimento']);
        $dateVenc->format('Y-m-d');
        $clas = "";
        if ($dateVenc < $datetoday) {
            $clas = "red";
        }
        $row[] = "<div title='" . tableFormato($aRow['descricao_doc'], 'T', '', '') . "'>" . tableFormato($aRow['descricao_doc'], 'T', '', '') . "</div>";
        $row[] = "<div title='" . tableFormato($aRow['validade_doc'], 'T', '', '') . "'>" . tableFormato($aRow['validade_doc'], 'T', '', '') . "</div>";
        $row[] = "<div title='" . escreverData($aRow['data_documento']) . "'>" . escreverData($aRow['data_documento']) . "</div>";
        $row[] = "<div style=\"color:$clas\" title='" . escreverData($aRow['vencimento']) . "'>" . escreverData($aRow['vencimento']) . "</div>";
        if ($ckb_adm_cli_read_ == 0) {
            $row[] = "<center><div class=\"btn btn-success\" type=\"button\" value=\"x\" style=\"margin:0px; padding:5px 30px 5px 30px; line-height:10px;\" onClick=\"RenovarDoc(" . $aRow['id_fornecedor_despesas'] . "," . $aRow['id_documento'] . ")\")><img width='15px' src='/img/reload.png' alt='Renovar Documento' title='Renovar Documento'></div>
            <input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:7px 20px 8px 20px; line-height:10px;\" onClick=\"RemoverDocumentos(" . $aRow['id_fornecedor_despesas'] . "," . $aRow['id_documento'] . ")\")></center>";
        } else {
            $row[] = "";
        }
        $output["iTotalRecords"] = $cont;
        $output["iTotalDisplayRecords"] = $cont;
        $output['aaData'][] = $row;
    }
}

if ($_GET['op'] == 'rs') {
    $idCli = $_GET["id_forn"];
    $query = "set dateformat dmy;
    select id_doc, descricao_doc, case when id_doc in(
    select id_documento from sf_fornecedores_despesas_documentos where id_fornecedor_despesas = $idCli
    ) then 'S' else 'N' end as grp from sf_documentos where id_doc > 0" . 
    (isset($_GET["servico_doc"]) ? " and servico_doc is not null" : "");
    $cur = odbc_exec($con, $query) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['grp'] == 'N') {
            $row[] = array("id" => $RFP['id_doc'], "desc" => utf8_encode($RFP['descricao_doc']));
        }
    }
    $output = $row;
}

echo json_encode($output);
odbc_close($con);
