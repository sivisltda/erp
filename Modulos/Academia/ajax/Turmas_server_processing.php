<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');
$aColumns = array('A.id_turma', 'descricao', 'n_alunos',
    'n_alunos - isnull((select count(id_fornecedores_despesas) from sf_fornecedores_despesas_turmas where id_turma = A.id_turma),0)',
    'dt_inicio', 'B.nome_turno', 'E.razao_social nome_prof', 'D.nome_ambiente descricao_ambiente', 'A.inativo');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhere = "";
$sOrder = " ORDER BY " . $aColumns[0] . " asc ";
$sLimit = 20;
$imprimir = 0;
$grupoArray = [];

if (isset($_GET['isCliente'])) {
    $Where .= " and A.inativo = 0 ";
}

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['horario'])) {
    $Where .= " and horario = " . $_GET['horario'];
}

if (isset($_GET['professor'])) {
    $Where .= " and professor = " . $_GET['professor'];
}

if (isset($_GET['status']) && is_numeric($_GET['status'])) {
    $Where .= " and A.inativo = " . $_GET['status'];
}

if (isset($_GET['plano']) && is_numeric($_GET['plano'])) {
    $joinProduto = " left join sf_turmas_produtos C on A.id_turma = C.id_turma ";
    $Where .= " and id_produto = " . $_GET['plano'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $sOrder = " order by " . $aColumns[intval($_GET['iSortCol_0'])] . " " . $_GET['sSortDir_0'] . ", ";
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " order by conta_produto ";
    }
}

if ($_GET['txtBusca'] != "") {
    $Where .= " and (descricao like '%" . utf8_decode($_GET['txtBusca']) . "%' or B.nome_turno like '%" . utf8_decode($_GET['txtBusca']) . "%' or E.razao_social like '%" . utf8_decode($_GET['txtBusca']) . "%')";
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($i == 0) {
        $colunas = $aColumns[$i];
    } else {
        if ($i == 3) {
            $colunas = $colunas . "," . $aColumns[$i] . " vagas";
        } else {
            $colunas = $colunas . "," . $aColumns[$i];
        }
    }
}

$sQuery = "SELECT COUNT(*) total from sf_turmas A 
          left join sf_turnos B on A.horario = B.cod_turno " . $joinProduto . "
          left join sf_ambientes D on A.ambiente = D.id_ambiente 
          left join sf_fornecedores_despesas E on A.professor = E.id_fornecedores_despesas 
          where A.id_turma > 0 " . $Where;
//echo $sQuery; exit;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row," . $colunas . ",    
STUFF((SELECT distinct ',' + p.descricao from sf_turmas_produtos tp
inner join sf_produtos p on p.conta_produto = tp.id_produto where tp.id_turma = A.id_turma
FOR XML PATH('')), 1, 1, '') as produtos, A.dt_cadastro, desmatricular, dias_desmatricula
from sf_turmas A left join sf_turnos B on A.horario = B.cod_turno " . $joinProduto . "
left join sf_ambientes D on A.ambiente = D.id_ambiente
left join sf_fornecedores_despesas E on A.professor = E.id_fornecedores_despesas
where A.id_turma > 0 " . $Where . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir;
//echo $sQuery1;exit;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    if (!isset($_GET['isCliente'])) {
        $row[] = "<center><span class='label-" . ($aRow["inativo"] == 0 ? 'Ativo' : 'Inativo') . "' style='display: inline-block;width: 13px;height: 13px;border-radius: 50%!important;'></span></center>";
    }
    $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow['id_turma']) . ")'><div id='formPQ' title='" . utf8_encode($aRow['produtos']) . "'>" . utf8_encode($aRow['descricao']) . "</div></a>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["n_alunos"]) . "'>" . utf8_encode($aRow["n_alunos"]) . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["vagas"]) . "'>" . utf8_encode($aRow["vagas"]) . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["n_alunos"] - $aRow["vagas"]) . "'>" . utf8_encode($aRow["n_alunos"] - $aRow["vagas"]) . "</div>";
    if (!isset($_GET['isCliente'])) {
        $row[] = "<div id='formPQ' title='" . escreverData($aRow["dt_inicio"]) . "'>" . escreverData($aRow["dt_inicio"]) . "</div>";
    }
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["nome_turno"]) . "'>" . utf8_encode($aRow["nome_turno"]) . "</div>";    
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["nome_prof"]) . "'>" . utf8_encode($aRow["nome_prof"]) . "</div>";
    if (isset($_GET['pdf'])) {                
        $grp = array(utf8_encode($aRow['nome_prof']), ($aRow['nome_prof'] == "" ? "SEM GRUPO" : utf8_encode($aRow['nome_prof'])), 7);
        if (!in_array($grp, $grupoArray)) {
            $grupoArray[] = $grp;
        }
    }        
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["descricao_ambiente"]) . "'>" . utf8_encode($aRow["descricao_ambiente"]) . "</div>";
    if (!isset($_GET['isCliente'])) {
        $row[] = "<div id='formPQ' title='" . ($aRow["inativo"] == 0 ? 'Ativo' : 'Inativo') . "'>" . ($aRow["inativo"] == 0 ? 'Ativo' : 'Inativo') . "</div>";
        if ($imprimir == 0) {
            $row[] = "<center><input name='' type='image' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' onClick=\"RemoverItem(" . $aRow['id_turma'] . ")\" value='Enviar'></center>";
        }        
    } else {
        $row[] = "<center><button class=\"btn btn-small btn-success\" type=\"button\" title=\"Adicionar\" onClick=\"matricularAluno(" . $aRow['id_turma'] . "," . utf8_encode($aRow["vagas"]) . ")\"><span class=\"ico-plus\" ></span></button></center>";
    }
    if (isset($_GET['tpImp']) && $_GET['tpImp'] == "E") {
        $row[] = "<div id='formPQ' title='" . escreverData($aRow["dt_cadastro"]) . "'>" . escreverData($aRow["dt_cadastro"]) . "</div>";
        $row[] = ($aRow['desmatricular'] == "1" ? "SIM" : "NÃO");
        $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["dias_desmatricula"]) . "'>" . utf8_encode($aRow["dias_desmatricula"]) . "</div>";
        $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["produtos"]) . "'>" . utf8_encode($aRow["produtos"]) . "</div>";
    }
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
} else {
    $output['aaTotal'][] = array(2, 2, "", "", 0, 0, 0);
    $output['aaTotal'][] = array(3, 3, "", "", 0, 0, 0);
    $output['aaTotal'][] = array(4, 4, "", "", 0, 0, 0);
    $output['aaGrupo'] = $grupoArray;    
}
odbc_close($con);
