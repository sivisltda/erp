<?php

include_once ('../../../Connections/configini.php');
if (!isset($_GET['isPdf'])) {
    include_once ('../../../util/util.php');
}

$agenda = array();
$horarios = array();
$professores = array();

function returnBlock($horario, $dia, $hora) {
    $blk = "1";
    for ($i = 0; $i < count($horario); $i++) {
        if (substr($horario[$i][0], $dia, 1) == "1" && $hora >= $horario[$i][1] && $hora <= $horario[$i][2]) {
            $blk = "0";
        }
    }
    return $blk;
}

$query = "select id_fornecedores_despesas,razao_social from sf_fornecedores_despesas where tipo = 'E' and grupo_pessoa = 16 and dt_demissao is null order by razao_social";
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $professores[] = $RFP["id_fornecedores_despesas"];
}

$query = "select sf_turnos.* from sf_fornecedores_despesas 
inner join sf_turnos on sf_turnos.cod_turno = sf_fornecedores_despesas.fornecedores_hr
where tipo = 'E' and grupo_pessoa = 16 and dt_demissao is null ";
if (isset($_GET['idProfessor']) && is_numeric($_GET['idProfessor'])) {
    $query .= "and id_fornecedores_despesas = " . $_GET['idProfessor'];
    $where .= "where professor = " . $_GET['idProfessor'];
}
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $professores[] = $RFP['dia_semana'];
    for ($i = 1; $i <= 5; $i++) {
        $horarios[] = array($RFP['dia_semana' . $i], substr($RFP['faixa' . $i . '_ini'], 0, 2), substr($RFP['faixa' . $i . '_fim'], 0, 2));
    }
}

$query = "select * from sf_turnos_estudio e 
inner join sf_fornecedores_despesas a on e.aluno = a.id_fornecedores_despesas " . $where;
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $agenda[] = array(utf8_encode($RFP['faixa1_ini']), utf8_encode($RFP['dia_semana1']),
        "<a href=\"javascript:void(0)\" style=\"color:" . getCollor(array_search($RFP['professor'], $professores)) . ";\" onclick=\"preencherTela(" . utf8_encode($RFP['cod_estudio']) . ")\">" . utf8_encode($RFP['razao_social']) . "</a>|"
    );
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => 24,
    "iTotalDisplayRecords" => 24,
    "aaData" => array()
);

for ($hr = 0; $hr <= 23; $hr++) {
    $row = array();
    $row[] = str_pad($hr, 2, "0", STR_PAD_LEFT) . ":00";
    for ($i = 0; $i < 7; $i++) {
        $val = (!isset($_GET['pdf']) ? returnBlock($horarios, $i, $hr) : "");
        for ($j = 0; $j <= count($agenda); $j++) {
            if (substr($agenda[$j][0], 0, 2) == str_pad($hr, 2, "0", STR_PAD_LEFT) && substr($agenda[$j][1], $i, 1) == "1") {
                $val .= $agenda[$j][2];
            }
        }
        $row[] = $val;
    }
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
