<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');
require_once(__DIR__ . '/../../../util/excel/exportaExcel.php');

$aColumns = array('id_doc', 'descricao_doc', 'validade_doc', 'validade_doc', 'descricao');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY " . $aColumns[0] . " asc ";
$sOrder2 = " ORDER BY " . $aColumns[0] . " asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) < 1) {
                $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 0) . " " . $_GET['sSortDir_' . $i] . ", ";
                $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 1) . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY " . $aColumns[0] . " asc";
        $sOrder2 = " ORDER BY " . $aColumns[0] . " asc";
    }
}

if ($_GET['Search'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= prepareCollum($aColumns[$i], 0) . " LIKE '%" . utf8_decode($_GET['Search']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}


$sQuery = "SELECT COUNT(*) total from sf_documentos p where p." . $aColumns[0] . " > 0 $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total from sf_documentos p where p." . $aColumns[0] . " > 0 $sWhereX";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,*
            from sf_documentos p left join sf_produtos pd on pd.conta_produto = p.servico_doc
            where p." . $aColumns[0] . " > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder2;
$cur2 = odbc_exec($con, $sQuery1);

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ($aRow = odbc_fetch_array($cur2)) {
    $row = array();
    $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow['id_doc']) . ",1)'><div id='formPQ' title='" . utf8_encode($aRow['descricao_doc']) . "'>" . utf8_encode($aRow['descricao_doc']) . "</div></a>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow['validade_doc']) . "'>" . utf8_encode($aRow['validade_doc']) . " dias" . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow['descricao']) . "'>" . utf8_encode($aRow['descricao']) . "</div>";    
    $row[] = "<center>" . ($aRow['inativo_doc'] == 1 ? "SIM" : "NÃO") . "</center>";    
    if ($imprimir == 0) {
        $row[] = "<center><input name='' type='image' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' onClick=\"RemoverItem(" . $aRow['id_doc'] . ")\" value='Enviar'></center>";
    }
    $output['aaData'][] = $row;
}

if (isset($_POST['bntDelete'])) {
    $sQuery2 = "delete from sf_documentos where id_doc=" . $_POST['txtId'];
    $cur1 = odbc_exec($con, $sQuery2);
    echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox(1);</script>";
    exit();
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
