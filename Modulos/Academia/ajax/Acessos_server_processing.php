<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');
require_once(__DIR__ . '/../../../util/excel/exportaExcel.php');

$where = "";
if (isset($_GET['txtTpUser']) && $_GET['txtTpUser'] !== "T") {
    $where = " and B.tipo = '" . $_GET['txtTpUser'] . "'";
}

if (isset($_GET['txtDtHrInicial'])) {
    $where .= " and A.data_acesso > " . valoresDataHoraUnico('txtDtHrInicial') . "";
}

if (isset($_GET['txtDtHrFinal'])) {
    $where .= " and A.data_acesso < " . valoresDataHoraUnico('txtDtHrFinal') . "";
}

if (isset($_GET['txtTpAcesso']) && $_GET['txtTpAcesso'] !== "T") {
    $where .= " and A.status_acesso = '" . $_GET['txtTpAcesso'] . "'";
}

if (isset($_GET['txtAmbiente'])) {
    $where .= " and A.ambiente in(" . $_GET['txtAmbiente'] . ")";
}

if (isset($_GET['txtCatraca'])) {
    $where .= " and A.catraca in(" . $_GET['txtCatraca'] . ")";
}

if (isset($_GET['txtGrupo'])) {
    $where .= " and B.grupo_pessoa in(" . $_GET['txtGrupo'] . ")";
}

if (isset($_GET['txtCredencial']) && $_GET['txtCredencial'] == "1") {
    $where .= "and (select COUNT(*) from sf_fornecedores_despesas_credenciais CR where CR.id_fornecedores_despesas = A.id_fonecedor
    and A.data_acesso between dt_inicio and dateadd(day,1,dt_fim) and dt_cancelamento is null and id_item_venda is not null) > 0";
} else if (isset($_GET['txtCredencial']) && $_GET['txtCredencial'] == "2") {
    $where .= "and (select COUNT(*) from sf_fornecedores_despesas_credenciais CR where CR.id_fornecedores_despesas = A.id_fonecedor
    and A.data_acesso between dt_inicio and dateadd(day,1,dt_fim) and dt_cancelamento is null and id_item_venda is null) > 0";
}

if (isset($_GET['txtIdUser'])) {
    $where .= " and A.id_fonecedor = " . $_GET['txtIdUser'];
}

if (isset($_GET['txtFilial'])) {
    $where .= " and B.empresa  = " . $_GET['txtFilial'];
}

$group = "order by data_acesso";
$columns = " id_fornecedores_despesas,  case when id_fornecedores_despesas = 0 then 'LIBERACAO MANUAL' else B.razao_social end razao_social, data_acesso, status_acesso, motivo_liberacao, C.nome_ambiente, U.nome, CA.nome_catraca ";
if (isset($_GET['isResumo'])) {
    $columns = " status_acesso, count(*) total ";
    $group = " group by status_acesso ";
}

$itens_id = [];
$itens_text = [];
$columns_livres = "";

if (isset($_GET['ex']) && $_GET['ex'] == 1) {
    $cur = odbc_exec($con, "select id_campo, descricao_campo from sf_configuracao_campos_livres where ativo_campo = 1");
    while ($RFP = odbc_fetch_array($cur)) {
        $itens_id[] = utf8_encode($RFP['id_campo']);
        $itens_text[] = utf8_encode($RFP['descricao_campo']);
        $columns_livres .= ",(select max(conteudo_campo) from sf_fornecedores_despesas_campos_livres where campos_campos = " . $RFP['id_campo'] . 
        " and fornecedores_campos = B.id_fornecedores_despesas) campo_" . $RFP['id_campo'];
    }
}

$query = "set dateformat dmy;
          select " . $columns . $columns_livres . " from sf_acessos A
          left join sf_fornecedores_despesas B on A.id_fonecedor = B.id_fornecedores_despesas
          left join sf_ambientes C on A.ambiente = C.id_ambiente
          left join sf_catracas CA on CA.id_catraca = A.catraca
          left join sf_usuarios U on A.liberado_por = U.id_usuario
          where id_fornecedores_despesas >= 0 " . $where . $group;
$cur = odbc_exec($con, $query);
//echo $query; exit;

if (isset($_GET['isResumo'])) {
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );

    while ($RFP = odbc_fetch_array($cur)) {
        $totalGeral = $totalGeral + $RFP["total"];
        $row = array();
        $row[] = trataAcesso($RFP["status_acesso"]);
        $row[] = $RFP["total"];
        $output['aaData'][] = $row;
    }
    $row = array();
    $row[] = "TOTAL GERAL";
    $row[] = $totalGeral;
    $output['aaData'][] = $row;
} else {
    $cont = 0;
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    while ($RFP = odbc_fetch_array($cur)) {
        $cont = $cont + 1;
        $row = array();
        $row[] = "<center>" . tableFormato($RFP["id_fornecedores_despesas"], 'T', '', '') . "</center>";
        $row[] = tableFormato($RFP["razao_social"], 'T', '', '');
        $row[] = "<center>" . escreverDataHora($RFP["data_acesso"]) . "</center>";
        $row[] = "<center>" . trataAcesso($RFP["status_acesso"]) . "</center>";
        $row[] = tableFormato($RFP["motivo_liberacao"], 'T', '', '');
        $row[] = tableFormato($RFP["nome_ambiente"], 'T', '', '');
        $row[] = tableFormato($RFP["nome_catraca"], 'T', '', '');
        $row[] = "<center>" . formatNameCompact(utf8_encode($RFP["nome"])) . "</center>";
        foreach ($itens_id as $item) {
            $row[] = tableFormato($RFP["campo_" . $item], 'T', '', '');
        }
        $output['aaData'][] = $row;
    }
    $output["iTotalRecords"] = $cont;
    $output["iTotalDisplayRecords"] = $cont;
}

if (isset($_GET['ex']) && $_GET['ex'] == 1) {
    $topo = '<tr>' . 
            '<td><b>Matricula </b></td><td><b>Nome </b></td><td><b>Data/Hora </b></td>' . 
            '<td><b>Tipo Acesso </b></td><td><b>Motivo </b></td><td><b>Ambiente </b></td>' .
            '<td><b>Catraca </b></td><td><b>Usuário </b></td>';
    foreach ($itens_text as $item) {
        $topo .= '<td><b>' . $item . '</b></td>';
    }
    $topo .= '</tr>';
    exportaExcel($output['aaData'], $topo);
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
