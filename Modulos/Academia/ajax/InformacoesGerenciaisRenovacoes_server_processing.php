<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');

$iTotal = 0;
$where = " and sf_fornecedores_despesas.tipo = 'C'";
$where2 = "";
$op = ">";
$semCancelamento = " and dt_cancelamento is null";
$innerjoin = "";

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iTotal,
    "aaData" => array()
);

if ($_GET['id'] == 0) {
    $where .= " and id_fornecedores_despesas < 0";
}
if ($_GET['status'] != 'null') {
    $ajustReplace = str_replace(",", "','", $_GET['status']);
    if (strpos($_GET['status'], "lado") > 0) {
        $semCancelamento = "";
    }
    $where .= " and sf_vendas_planos.planos_status in ('" . $ajustReplace . "') ";
}
if ($_GET['loj'] != 0 && $_GET['loj'] != 'null') {
    $where .= " and empresa=" . $_GET['loj'];
}
if ($_GET['sta'] != 'null') {
    $where .= " and (";
    $dataAux = explode(",", $_GET['sta']);
    for ($i = 0; $i < count($dataAux); $i++) {
        $where .= ($i > 0 ? " or " : "") . " fornecedores_status = '" . getStatus($dataAux[$i]) . "'";
    }
    $where .= ")";
    $where2 .= $where;
}
if (isset($_GET['grp']) && $_GET['grp'] != 'null') {
    $where .= " and (grupo_pessoa in (" . $_GET['grp'] . ")" . (strpos($_GET['grp'], "-1") !== false ? " or grupo_pessoa is null" : "") . ")";
    $where2 .= $where;    
}
if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    if ($_GET['tpd'] == '1') {
        $where .= " and cast(sf_vendas_planos.dt_cadastro as date) between " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']) . "";
    } else if ($_GET['tpd'] == '2') {
        $where .= " and cast(sf_fornecedores_despesas.dt_cadastro as date) between " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']) . "";
    } else {
        $where .= " and dt_inicio between " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']) . "";
    }
}
if ($_GET['idi'] != '' && $_GET['idf'] != '') {
    $where .= " and dt_fim between " . valoresData2($_GET['idi']) . " and " . valoresData2($_GET['idf']) . "";
}
if (is_numeric($_GET['rep'])) {
    $where .= " and id_user_resp = " . $_GET['rep'];
}
if (is_numeric($_GET['prf'])) {
    $where .= " and (prof_resp = " . $_GET['prf']. " or id_fornecedores_despesas in (select B.id_fornecedores_despesas from sf_turmas A inner join sf_fornecedores_despesas_turmas B on B.id_turma = A.id_turma where A.professor = " . $_GET['prf']. "))";
}
if (is_numeric($_GET['tip'])) {
    if ($_GET['tip'] == 1) {
        $op = ">";
    } elseif ($_GET['tip'] == 2) {
        $op = "<";
    } elseif ($_GET['tip'] == 3) {
        $op = "=";
    }
    $where .= " and (select COUNT(*) from sf_vendas_planos_mensalidade vm where vm.id_plano_mens = id_plano and id_item_venda_mens is not null) " . $op . " 1";
    if ($_GET['tip'] == 4) {
        if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
            $where2 .= " and dt_inicio between " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']) . "";
        }
        if ($_GET['idi'] != '' && $_GET['idf'] != '') {
            $where2 .= " and dt_fim between " . valoresData2($_GET['idi']) . " and " . valoresData2($_GET['idf']) . "";
        }
        if (is_numeric($_GET['prf'])) {
            $where2 .= " and (prof_resp = " . $_GET['prf']. " or id_fornecedores_despesas in (select B.id_fornecedores_despesas from sf_turmas A inner join sf_fornecedores_despesas_turmas B on B.id_turma = A.id_turma where A.professor = " . $_GET['prf']. "))";
        }
        if (is_numeric($_GET['rep'])) {
            $where2 .= " and id_user_resp = " . $_GET['rep'];
        }
        if ($_GET['crd'] != 'null') {
            $where2 .= " and C.id_credencial in (" . $_GET['crd'] . ")";
        }
        if ($_GET['loj'] != 0 && $_GET['loj'] != 'null') {
            $where2 .= " and empresa = " . $_GET['loj'];
        }
        $where2 .= " and sf_fornecedores_despesas.inativo = 0";
    }
}
if ($_GET['pla'] != 'null') {
    $where .= " and id_prod_plano in (" . $_GET['pla'] . ")";
}
if ($_GET['per'] != 'null' && strlen($_GET['per']) > 0) {
    $where .= " and id_plano in (select id_plano_mens
    from sf_vendas_planos_mensalidade inner join sf_produtos_parcelas on id_parcela = id_parc_prod_mens
    where parcela in (" . $_GET['per'] . "))";
}
if ($_GET['mdi'] != '' && $_GET['mdf'] != '') {
    $tipo = "";
    if ($_GET['tms'] == 1) {
        $tipo = "dt_pagamento_mens is null and ";
    } elseif ($_GET['tms'] == 2) {
        $tipo = "dt_pagamento_mens is not null and ";
    } elseif ($_GET['tms'] == 3) {
        $tipo = "id_mens in (select top 1 id_mens from sf_vendas_planos_mensalidade mx where mx.id_plano_mens = sf_vendas_planos_mensalidade.id_plano_mens order by dt_fim_mens desc) and 
        ((DATEADD(day,(select ACA_PLN_DIAS_RENOVACAO from sf_configuracao),sf_vendas_planos.dt_fim) > GETDATE() and sf_produtos.parcelar = 0) or
        (sf_vendas_planos.dt_fim >= GETDATE() and sf_produtos.parcelar = 1) or
        ((select COUNT(*) from sf_vendas_planos_mensalidade C where C.id_plano_mens = sf_vendas_planos.id_plano and dt_pagamento_mens is null) > 0)) and ";               
    }
    if ($_GET['tmd'] == 0) {
        $tipo .= "dt_inicio_mens";
    } elseif ($_GET['tmd'] == 1) {
        $tipo .= "dt_fim_mens";
    } else {
        $tipo .= "dt_pagamento_mens";
    }
    $where .= " and id_plano in (select id_plano_mens from sf_vendas_planos_mensalidade
    where " . $tipo . " between " . valoresData2($_GET['mdi']) . " and " . valoresData2($_GET['mdf']) . ")";
}
if (valoresNumericos2($_GET['vli']) > 0 && valoresNumericos2($_GET['vlf']) == 0) {
    $where .= " and id_plano in (select id_plano_mens from sf_vendas_planos_mensalidade
    inner join sf_vendas_itens on sf_vendas_planos_mensalidade.id_item_venda_mens = sf_vendas_itens.id_item_venda
    where valor_total >= " . valoresNumericos2($_GET['vli']) . ")";
} else if (valoresNumericos2($_GET['vlf']) > 0 && valoresNumericos2($_GET['vli']) == 0) {
    $where .= " and id_plano in (select id_plano_mens from sf_vendas_planos_mensalidade
    inner join sf_vendas_itens on sf_vendas_planos_mensalidade.id_item_venda_mens = sf_vendas_itens.id_item_venda
    where valor_total <= " . valoresNumericos2($_GET['vlf']) . ")";
} else if (valoresNumericos2($_GET['vlf']) > 0 && valoresNumericos2($_GET['vli']) > 0) {
    $where .= " and id_plano in (select id_plano_mens from sf_vendas_planos_mensalidade
    inner join sf_vendas_itens on sf_vendas_planos_mensalidade.id_item_venda_mens = sf_vendas_itens.id_item_venda
    where valor_total between " . valoresNumericos2($_GET['vli']) . " and " . valoresNumericos2($_GET['vlf']) . ")";    
}
if (is_numeric($_GET['car']) && $_GET['car'] == 1) {
    $where .= " and (validade_cartao is null or validade_cartao < GETDATE()) ";
}
if (is_numeric($_GET['mpl']) && $_GET['mpl'] == 1) {
    $where .= " and (select count(id_plano) total from sf_vendas_planos where favorecido = id_fornecedores_despesas and dateadd(day,90,dt_fim) > cast(GETDATE() as DATE)) > 1 ";
}
if (is_numeric($_GET['tit']) && $_GET['tit'] == 1) {
    $innerjoin = "inner join sf_fornecedores_despesas_dependentes on id_dependente = id_fornecedores_despesas";
}

if ($_GET['tip'] == 4) {
    $sQuery1 .= "set dateformat dmy;select sf_fornecedores_despesas.id_fornecedores_despesas,sf_fornecedores_despesas.razao_social, data_nascimento,estado_civil, FC.dt_inicio, FC.dt_fim, C.descricao,U.nome estado,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'telefone',
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'celular',    
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'email'
    from sf_fornecedores_despesas
    inner join sf_fornecedores_despesas_credenciais as FC on sf_fornecedores_despesas.id_fornecedores_despesas = fc.id_fornecedores_despesas
    inner join sf_credenciais as C on FC.id_credencial = C.id_credencial
    inner join sf_usuarios as U on FC.usuario_resp = U.id_usuario" . $where2;
} else {
    if ($_GET['id'] == 0 || $_GET['id'] == 1) {
        $sQuery1 .= "set dateformat dmy;select id_fornecedores_despesas,razao_social,data_nascimento,estado_civil,
        dt_inicio, dt_fim, sf_produtos.descricao,planos_status status,
        (select top 1 case when nome = '' then login_user else nome end login_user from sf_usuarios c where c.inativo = 0 and c.id_usuario = sf_fornecedores_despesas.id_user_resp) estado,
        (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'telefone',
        (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'celular',        
        (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'email',cnpj";
        if (isset($_GET['tpImp']) && $_GET['tpImp'] == "E") {
            $sQuery1 .= ",nfe_iss,
            (select top 1 dbo.VALOR_REAL_MENSALIDADE(id_mens) valor_mens from sf_vendas_planos_mensalidade where id_plano_mens = sf_vendas_planos.id_plano order by dt_inicio_mens desc) Valor,
            (select descricao_grupo from sf_grupo_cliente where id_grupo = grupo_pessoa) grupo_cliente,
            (select descricao from sf_fornecedores_despesas_convenios inner join sf_convenios on convenio = id_convenio where id_fornecedor = id_fornecedores_despesas) convenios";
        }        
        $sQuery1 .= " from sf_fornecedores_despesas $innerjoin
        inner join sf_vendas_planos on favorecido = " . ($_GET['tit'] == 1 ? "id_titular" : "id_fornecedores_despesas") . "
        inner join sf_produtos on sf_produtos.conta_produto = id_prod_plano
        where id_fornecedores_despesas > 0 and inativo = 0 " . $semCancelamento . $where . " order by 2";
    } elseif ($_GET['id'] == 2) {
        $sQuery1 .= "select SUM(x.nRenovados) nRenovados, SUM(x.renovados) renovados from (
        select id_plano,case when (select COUNT(*) from sf_vendas_planos_mensalidade vm where vm.id_plano_mens = id_plano and id_item_venda_mens is not null) <= 1 then 1 else 0 end 'nRenovados',
        case when (select COUNT(*) from sf_vendas_planos_mensalidade vm where vm.id_plano_mens = id_plano and id_item_venda_mens is not null) > 1 then 1 else 0 end 'renovados'
        from sf_vendas_planos inner join sf_fornecedores_despesas on id_fornecedores_despesas = favorecido
        where id_plano > 0 and inativo = 0 " . $semCancelamento . $where . ") as x";
    }
}
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($RFP = odbc_fetch_array($cur)) {
    $row = array();
    if ($_GET['id'] == 0 || $_GET['id'] == 1) {
        $row[] = utf8_encode($RFP['id_fornecedores_despesas']);
        $row[] = "<a href='javascript:void(0)' onClick='AbrirCli(" . utf8_encode($RFP['id_fornecedores_despesas']) . ")'><div id='formPQ' title='" . utf8_encode($RFP['razao_social']) . "'>" . utf8_encode($RFP['razao_social']) . "</div></a>";        
        $row[] = escreverData($RFP['dt_inicio']) . " até " . escreverData($RFP['dt_fim']);
        $row[] = utf8_encode($RFP['status']);
        $row[] = utf8_encode($RFP['descricao']);
        $row[] = utf8_encode($RFP['telefone']);
        $row[] = utf8_encode($RFP['celular']);        
        $row[] = utf8_encode($RFP['email']);
        $row[] = formatNameCompact(utf8_encode($RFP['estado']));        
        $row[] = utf8_encode($RFP['cnpj']);
        $row[] = escreverNumero($RFP['Valor'], 1);
        if (isset($_GET['tpImp']) && $_GET['tpImp'] == "E") {
            $row[] = escreverData($RFP['data_nascimento']);
            $row[] = utf8_encode($RFP['estado_civil']);
            $row[] = escreverData($RFP['dt_inicio']);
            $row[] = ($RFP['nfe_iss'] == 1 ? "SIM" : "NÃO");            
            $row[] = utf8_encode($RFP['grupo_cliente']);
            $row[] = utf8_encode($RFP['convenios']);
        }
    } elseif ($_GET['id'] == 2) {
        $row[] = "Não Renov.";
        $row[] = utf8_encode($RFP['nRenovados']);
        $output['aaData'][] = $row;
        $row = array();
        $row[] = "Renovados";
        $row[] = utf8_encode($RFP['renovados']);
    }
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
