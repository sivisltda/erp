<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');
require_once(__DIR__ . '/../../../util/excel/exportaExcel.php');

$sQuery1 = "set dateformat dmy;";
$tpCli = "'C'";
$where = "";
$iTotal = 0;

if ($_GET['fil'] != '') {
    if ($_GET["fr"] == 1 || $_GET["fr"] == 2) {
        $where .= " and sf_produtos.descricao = '" . utf8_decode($_GET["fil"]) . "'";
    } elseif ($_GET["fr"] == 3) {
        $where .= " and id_plano in (select id_plano_mens from sf_vendas_planos_mensalidade
        inner join sf_produtos_parcelas on id_parc_prod_mens = id_parcela
        where parcela = " . planoLegenda(utf8_decode($_GET["fil"])) . ")";
    }
}

if (isset($_GET['filial'])) {
    $where .= " and sf_fornecedores_despesas.empresa = " . $_GET['filial'];
}

if (isset($_GET['grp']) && $_GET['grp'] != "undefined") {
    $where .= " and sf_produtos.conta_movimento in (" . str_replace("\"", "'", str_replace(array("[", "]"), "", $_GET["grp"])) . ")";
}

if ($_GET["tpCli"] == 2) {
    $tpCli = "'P'";
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $where .= " and dt_convert_cliente BETWEEN " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']);
}

if (isset($_GET['ur'])) {
    $where .= " and id_user_resp = '" . $_GET['ur'] . "'";
}

if ($crm_pro_ure_ > 0) {
    $where .= " and (id_user_resp = " . $_SESSION["id_usuario"] . " 
    or id_user_resp in (select sf_usuarios_dependentes.id_usuario from sf_usuarios_dependentes 
    inner join sf_usuarios on id_fornecedor_despesas = funcionario where sf_usuarios.id_usuario = " . $_SESSION["id_usuario"] . "))";
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iTotal,
    "aaData" => array()
);

if ($_GET["fr"] == 0) {
    $sQuery1 .= "select sf_produtos.conta_produto,sf_produtos.descricao,count(distinct favorecido) total ";
} elseif ($_GET["fr"] == 1) {
    $sQuery1 .= "select parcela,count(distinct favorecido) total ";
} elseif ($_GET["fr"] == 2 || $_GET["fr"] == 3) {
    $sQuery1 .= "select distinct id_fornecedores_despesas,razao_social,data_nascimento,sexo,fornecedores_status 'estado',
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'telefone',        
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'celular',    
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'email' ";
}
if ($_GET["fr"] == 1) {
    $sQuery1 .= "from sf_vendas_planos_mensalidade
    inner join sf_vendas_planos on sf_vendas_planos_mensalidade.id_plano_mens = sf_vendas_planos.id_plano
    inner join sf_fornecedores_despesas on sf_fornecedores_despesas.id_fornecedores_despesas = sf_vendas_planos.favorecido
    inner join sf_produtos_parcelas on sf_vendas_planos_mensalidade.id_parc_prod_mens = sf_produtos_parcelas.id_parcela
    inner join sf_produtos on sf_produtos_parcelas.id_produto = sf_produtos.conta_produto
    where dt_cancelamento is null and sf_produtos.tipo = " . $tpCli . " and inativa = 0 and sf_fornecedores_despesas.inativo = 0 " . $where . "";
} else {
    $sQuery1 .= "from sf_vendas_planos
    inner join sf_fornecedores_despesas on sf_fornecedores_despesas.id_fornecedores_despesas = sf_vendas_planos.favorecido 
    inner join sf_produtos on sf_vendas_planos.id_prod_plano = sf_produtos.conta_produto 
    where dt_cancelamento is null and sf_produtos.tipo = " . $tpCli . " and inativa = 0 and sf_fornecedores_despesas.inativo = 0 " . $where . "";
}
if (isset($_GET['sttus'])) {
    $sQuery1 .= " and sf_vendas_planos.planos_status = 'Ativo' ";
}
if ($_GET["fr"] == 0) {
    $sQuery1 .= "group by sf_produtos.conta_produto,sf_produtos.descricao order by 2";
} elseif ($_GET["fr"] == 1) {
    $sQuery1 .= "group by parcela order by 1";
}
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($RFP = odbc_fetch_array($cur)) {
    $row = array();
    if ($_GET["fr"] == 0) {
        $row[] = utf8_encode($RFP['descricao']);
        $row[] = utf8_encode($RFP['total']);
        $row[] = utf8_encode($RFP['conta_produto']);
    } elseif ($_GET["fr"] == 1) {
        $row[] = utf8_encode(legendaPlano($RFP['parcela']));
        $row[] = utf8_encode($RFP['total']);
    } elseif ($_GET["fr"] == 2 || $_GET["fr"] == 3) {
        $row[] = utf8_encode($RFP['id_fornecedores_despesas']);
        $row[] = "<a href='javascript:void(0)' onClick='AbrirCli(" . utf8_encode($RFP['id_fornecedores_despesas']) . ")'><div id='formPQ' title='" . utf8_encode($RFP['razao_social']) . "'>" . utf8_encode($RFP['razao_social']) . "</div></a>";        
        $row[] = escreverData($RFP['data_nascimento']);
        $row[] = utf8_encode($RFP['sexo']);
        $row[] = utf8_encode($RFP['estado']);
        $row[] = utf8_encode($RFP['telefone']);
        $row[] = utf8_encode($RFP['celular']);        
        $row[] = utf8_encode($RFP['email']);
    }
    $output['aaData'][] = $row;
}

if (isset($_GET['ex']) && $_GET['ex'] == 1) {
    $topo = '<tr><td><b>Código </b></td><td><b>Nome </b></td><td><b>Dt. Nasci </b></td><td><b>Sexo </b><td><b>Status </b></td><td><b>Telefone </b></td><td><b>Celular </b></td><td><b>Email </b></td></tr>';
    exportaExcel($output['aaData'], $topo);
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);