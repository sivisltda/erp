<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');
require_once(__DIR__ . '/../../../util/excel/exportaExcel.php');

$iTotal = 0;
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iTotal,
    "aaData" => array()
);

$sQuery1 .= "set dateformat dmy;";
if ($_GET['fil'] != '') {
    $sQuery1 .= "select id_fornecedores_despesas,razao_social,data_nascimento,sexo,fornecedores_status 'estado',
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'telefone',
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'celular',    
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'email'
    from sf_fornecedores_despesas where id_fornecedores_despesas > 0 and inativo = 0 and id_fornecedores_despesas in (";
}
if ($_GET['fr'] == 0) {
    if ($_GET['fil'] == '') {
        $sQuery1 .= "select x.estado,count(distinct id_fornecedores_despesas) alunos, sum(id_plano) planos from (
        select case when SUBSTRING(fornecedores_status,1,5) = 'Ativo' and fornecedores_status not in ('AtivoEmAberto') then 'Ativo' else fornecedores_status end estado,
        id_fornecedores_despesas,case when id_plano is null then 0
        when ((DATEADD(day,(select ACA_PLN_DIAS_RENOVACAO from sf_configuracao),A.dt_fim) > GETDATE() and B.parcelar = 0) or
        (A.dt_fim >= GETDATE() and B.parcelar = 1) or
        ((select COUNT(*) from sf_vendas_planos_mensalidade C where C.id_plano_mens = A.id_plano and dt_pagamento_mens is null) > 0)) then 1
        else 0 end id_plano from sf_fornecedores_despesas F
        left join sf_vendas_planos A on A.favorecido = id_fornecedores_despesas  left join sf_produtos B on A.id_prod_plano = B.conta_produto
        where F.tipo in ('C') and F.id_fornecedores_despesas > 0 and F.inativo = 0 and F.empresa = " . $_GET['filial'] . ") as x group by x.estado";
    } else {
        $sQuery1 .= "select id_fornecedores_despesas from sf_fornecedores_despesas
        left join sf_vendas_planos on favorecido = id_fornecedores_despesas where tipo in ('C')
        and id_fornecedores_despesas > 0 and inativo = 0 and fornecedores_status like '" . $_GET['fil'] . "%' " . 
        ($_GET['fil'] == "Ativo" ? "and fornecedores_status not in ('AtivoEmAberto')" : "") .
        ($_GET['fil'] == "Desligado" ? "and fornecedores_status not in ('DesligadoEmAberto')" : "");
    }
} elseif ($_GET['fr'] == 1) {
    if ($_GET['fil'] == '' || $_GET['fil'] == '0') {
        if ($_GET['fil'] == '') {
            $sQuery1 .= "select 'Hoje' estado, '0' filtro, count(id_plano) 'planos',count(distinct id_fornecedores_despesas) 'alunos' ";
        } else {
            $sQuery1 .= "select id_fornecedores_despesas ";
        }
        $sQuery1 .= "from sf_fornecedores_despesas
        left join sf_vendas_planos on id_fornecedores_despesas = sf_vendas_planos.favorecido and dt_cancelamento is null and dt_fim > getdate()
        where id_fornecedores_despesas > 0 and tipo = 'C' and empresa =" . $_GET['filial'] . "
        and inativo = 0 and cast(sf_fornecedores_despesas.dt_cadastro as date) = cast(GETDATE() as date)";
    }
    if ($_GET['fil'] == '' || $_GET['fil'] == '7') {
        if ($_GET['fil'] == '') {
            $sQuery1 .= " union all ";
            $sQuery1 .= "select '07 dias' estado, '7' filtro, count(id_plano) 'planos',count(distinct id_fornecedores_despesas) 'alunos' ";
        } else {
            $sQuery1 .= "select id_fornecedores_despesas ";
        }
        $sQuery1 .= "from sf_fornecedores_despesas
        left join sf_vendas_planos on id_fornecedores_despesas = sf_vendas_planos.favorecido and dt_cancelamento is null and dt_fim > getdate()
        where id_fornecedores_despesas > 0 and tipo = 'C' and empresa =" . $_GET['filial'] . "
        and inativo = 0 and cast(sf_fornecedores_despesas.dt_cadastro as date) > dateadd(day,-7,cast(GETDATE() as date))";
    }
    if ($_GET['fil'] == '' || $_GET['fil'] == '15') {
        if ($_GET['fil'] == '') {
            $sQuery1 .= " union all ";
            $sQuery1 .= "select '15 dias' estado, '15' filtro, count(id_plano) 'planos',count(distinct id_fornecedores_despesas) 'alunos' ";
        } else {
            $sQuery1 .= "select id_fornecedores_despesas ";
        }
        $sQuery1 .= "from sf_fornecedores_despesas
        left join sf_vendas_planos on id_fornecedores_despesas = sf_vendas_planos.favorecido and dt_cancelamento is null and dt_fim > getdate()
        where id_fornecedores_despesas > 0 and tipo = 'C' and empresa =" . $_GET['filial'] . "
        and inativo = 0 and cast(sf_fornecedores_despesas.dt_cadastro as date) > dateadd(day,-15,cast(GETDATE() as date))";
    }
    if ($_GET['fil'] == '' || $_GET['fil'] == '30') {
        if ($_GET['fil'] == '') {
            $sQuery1 .= " union all ";
            $sQuery1 .= "select '30 dias' estado, '30' filtro, count(id_plano) 'planos',count(distinct id_fornecedores_despesas) 'alunos' ";
        } else {
            $sQuery1 .= "select id_fornecedores_despesas ";
        }
        $sQuery1 .= "from sf_fornecedores_despesas
        left join sf_vendas_planos on id_fornecedores_despesas = sf_vendas_planos.favorecido and dt_cancelamento is null and dt_fim > getdate()
        where id_fornecedores_despesas > 0 and tipo = 'C' and empresa =" . $_GET['filial'] . "
        and inativo = 0 and cast(sf_fornecedores_despesas.dt_cadastro as date) > dateadd(day,-30,cast(GETDATE() as date))";
    }
    if ($_GET['fil'] == '' || $_GET['fil'] == '-1') {
        if ($_GET['fil'] == '') {
            $sQuery1 .= " union all ";
            $sQuery1 .= "select 'Mês Atual' estado, '-1' filtro, count(id_plano) 'planos',count(distinct id_fornecedores_despesas) 'alunos' ";
        } else {
            $sQuery1 .= "select id_fornecedores_despesas ";
        }
        $sQuery1 .= "from sf_fornecedores_despesas
        left join sf_vendas_planos on id_fornecedores_despesas = sf_vendas_planos.favorecido and dt_cancelamento is null and dt_fim > getdate()
        where id_fornecedores_despesas > 0 and tipo = 'C' and empresa =" . $_GET['filial'] . "
        and inativo = 0 and month(sf_fornecedores_despesas.dt_cadastro) = month(GETDATE()) and year(sf_fornecedores_despesas.dt_cadastro) = year(GETDATE())";
    }
    if ($_GET['fil'] == 'novas' && $_GET['dti'] != "" && $_GET['dtf'] != "") {
        $sQuery1 .= "select x.favorecido from(select P.favorecido,(select max(dt_pagamento_mens) from sf_vendas_planos_mensalidade B where B.id_plano_mens = A.id_plano_mens and B.id_mens < A.id_mens) anterior,
        dt_pagamento_mens atual, (select ACA_PLN_DIAS_RENOVACAO from sf_configuracao) dias
        from sf_vendas_planos_mensalidade A inner join sf_vendas_planos P on A.id_plano_mens = P.id_plano
        where id_item_venda_mens is not null and dt_pagamento_mens between " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']) . ") as x
        where x.anterior is null";
    } elseif ($_GET['fil'] == 'renovacoes' && $_GET['dti'] != "" && $_GET['dtf'] != "") {
        $sQuery1 .= "select x.favorecido from(select P.favorecido,(select max(dt_pagamento_mens) from sf_vendas_planos_mensalidade B where B.id_plano_mens = A.id_plano_mens and B.id_mens < A.id_mens) anterior,
        dt_pagamento_mens atual, (select ACA_PLN_DIAS_RENOVACAO from sf_configuracao) dias
        from sf_vendas_planos_mensalidade A inner join sf_vendas_planos P on A.id_plano_mens = P.id_plano
        where id_item_venda_mens is not null and dt_pagamento_mens between " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']) . ") as x
        where x.anterior is not null and datediff(day,x.anterior,x.atual) <= x.dias";
    } elseif ($_GET['fil'] == 'retornos' && $_GET['dti'] != "" && $_GET['dtf'] != "") {
        $sQuery1 .= "select x.favorecido from(select P.favorecido,(select max(dt_pagamento_mens) from sf_vendas_planos_mensalidade B where B.id_plano_mens = A.id_plano_mens and B.id_mens < A.id_mens) anterior,
        dt_pagamento_mens atual, (select ACA_PLN_DIAS_RENOVACAO from sf_configuracao) dias
        from sf_vendas_planos_mensalidade A inner join sf_vendas_planos P on A.id_plano_mens = P.id_plano
        where id_item_venda_mens is not null and dt_pagamento_mens between " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']) . ") as x
        where x.anterior is not null and datediff(day,x.anterior,x.atual) > x.dias";
    } elseif ($_GET['fil'] == 'cancelados' && $_GET['dti'] != "" && $_GET['dtf'] != "") {
        $sQuery1 .= "select favorecido from sf_vendas_planos 
        inner join sf_fornecedores_despesas on id_fornecedores_despesas = favorecido            
        where dt_cancelamento is not null and empresa =" . $_GET['filial'] . " and cast(dt_cancelamento as date) BETWEEN " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']) . "";
    }
} elseif ($_GET['fr'] == 2) {
    if ($_GET['fil'] == '') {
        $sQuery1 .= "select sum(case when dt_convert_cliente is null then 1 else 0 end) nconvertidos, sum(case when dt_convert_cliente is not null then 1 else 0 end) convertidos ";
    } elseif ($_GET['fil'] == 'nconvertidos') {
        $sQuery1 .= "select case when dt_convert_cliente is null then id_fornecedores_despesas else 0 end ";
    } elseif ($_GET['fil'] == 'retornos') {
        $sQuery1 .= "select case when dt_convert_cliente is not null then id_fornecedores_despesas else 0 end ";
    } else {
        $sQuery1 .= "select id_fornecedores_despesas ";
    }
    $sQuery1 .= "from sf_fornecedores_despesas where id_fornecedores_despesas > 0 and empresa =" . $_GET['filial'] . "  and ((dt_convert_cliente is null and tipo = 'P') or (dt_convert_cliente is not null and tipo = 'C'))";
} elseif ($_GET['fr'] == 3) {
    if ($_GET['fil'] == '') {
        $sQuery1 .= "select x.estado,count(distinct id_fornecedores_despesas) alunos, sum(id_plano) planos from (
        select case when SUBSTRING(fornecedores_status,1,6) = 'Ativo' and fornecedores_status not in ('AtivoEmAberto') then 'Ativo' else fornecedores_status end estado,
        id_fornecedores_despesas,case when id_plano is null then 0
        when ((DATEADD(day,(select ACA_PLN_DIAS_RENOVACAO from sf_configuracao),A.dt_fim) > GETDATE() and B.parcelar = 0) or
        (A.dt_fim >= GETDATE() and B.parcelar = 1) or
        ((select COUNT(*) from sf_vendas_planos_mensalidade C where C.id_plano_mens = A.id_plano and dt_pagamento_mens is null) > 0)) then 1
        else 0 end id_plano from sf_fornecedores_despesas F left join sf_vendas_planos A on A.favorecido = id_fornecedores_despesas left join sf_produtos B on A.id_prod_plano = B.conta_produto
        where F.tipo in ('C') and F.id_fornecedores_despesas > 0 and F.inativo = 0 and F.empresa = " . $_GET['filial'] . "
        and SUBSTRING(fornecedores_status,1,5) = 'Ativo' and fornecedores_status not in ('AtivoEmAberto')) as x group by x.estado";
    } else {
        $sQuery1 .= "select id_fornecedores_despesas from sf_fornecedores_despesas left join sf_vendas_planos on favorecido = id_fornecedores_despesas where tipo in ('C') and id_fornecedores_despesas > 0 and inativo = 0 and (fornecedores_status = '" . $_GET['fil'] . "' " . ($_GET['fil'] != "AtivoCred" ? " or fornecedores_status like '" . $_GET['fil'] . " %'" : "") . ")";
    }
} elseif ($_GET['fr'] == 4) {
    if ($_GET['fil'] == '' || $_GET['fil'] == 'troca') {
        if ($_GET['fil'] == '') {
            $sQuery1 .= "select 'Trocas de Planos' estado, 'troca' filtro, count(id_fornecedor_despesa) 'planos',count(distinct id_fornecedor_despesa) 'alunos' ";
        } else {
            $sQuery1 .= "select id_fornecedor_despesa ";
        }
        $sQuery1 .= "from sf_creditos inner join sf_fornecedores_despesas on sf_creditos.id_fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where id_plano is not null and historico LIKE 'TROCA DE PLANO%' and sf_creditos.inativo = 0 and sf_fornecedores_despesas.empresa = " . $_GET['filial'] . "";
    }
    if ($_GET['fil'] == '' || $_GET['fil'] == 'transferencia') {
        if ($_GET['fil'] == '') {
            $sQuery1 .= "union all select 'Transferências' estado, 'transferencia' filtro, count(favorecido) 'planos',count(distinct favorecido) 'alunos' ";
        } else {
            $sQuery1 .= "select favorecido ";
        }
        $sQuery1 .= "from sf_vendas_planos inner join sf_fornecedores_despesas on sf_vendas_planos.favorecido = sf_fornecedores_despesas.id_fornecedores_despesas where dt_transferencia is not null and dt_cancelamento is null and sf_fornecedores_despesas.empresa =" . $_GET['filial'] . "";
    }
    if ($_GET['fil'] == '' || $_GET['fil'] == 'congelamento') {
        if ($_GET['fil'] == '') {
            $sQuery1 .= "union all select 'Congelamentos' estado,'congelamento' filtro, count(favorecido) 'planos',count(distinct favorecido) 'alunos' ";
        } else {
            $sQuery1 .= "select favorecido ";
        }
        $sQuery1 .= "from sf_vendas_planos_ferias
        inner join sf_vendas_planos on sf_vendas_planos.id_plano = sf_vendas_planos_ferias.id_plano
        inner join sf_fornecedores_despesas  on sf_vendas_planos.favorecido = sf_fornecedores_despesas.id_fornecedores_despesas
        where sf_vendas_planos_ferias.dt_cancelamento is null and sf_vendas_planos_ferias.dt_fim > GETDATE() and sf_fornecedores_despesas.empresa =" . $_GET['filial'] . "";
    }
    if ($_GET['fil'] == '' || $_GET['fil'] == 'credito') {
        if ($_GET['fil'] == '') {
            $sQuery1 .= "union all select 'Conversões em Crédito' estado, 'credito' filtro, count(id_fornecedor_despesa) 'planos',count(distinct id_fornecedor_despesa) 'alunos' ";
        } else {
            $sQuery1 .= "select id_fornecedor_despesa ";
        }
        $sQuery1 .= "from sf_creditos inner join sf_fornecedores_despesas on sf_creditos.id_fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where id_plano is not null and historico NOT LIKE 'TROCA DE PLANO%' and sf_creditos.inativo = 0 and sf_fornecedores_despesas.empresa = " . $_GET['filial'] . "";
    }
}

if ($_GET['fil'] != '') {
    $sQuery1 .= ")";
}
//echo $sQuery1; exit;

$cur = odbc_exec($con, $sQuery1);
while ($RFP = odbc_fetch_array($cur)) {
    $row = array();
    if ($_GET['fil'] != '') {
        $row[] = utf8_encode($RFP['id_fornecedores_despesas']);
        $row[] = "<a href='javascript:void(0)' onClick='AbrirCli(" . utf8_encode($RFP['id_fornecedores_despesas']) . ")'><div id='formPQ' title='" . utf8_encode($RFP['razao_social']) . "'>" . utf8_encode($RFP['razao_social']) . "</div></a>";
        $row[] = escreverData($RFP['data_nascimento']);
        $row[] = utf8_encode($RFP['sexo']);
        $row[] = utf8_encode($RFP['estado']);
        $row[] = utf8_encode($RFP['telefone']);        
        $row[] = utf8_encode($RFP['celular']);
        $row[] = utf8_encode($RFP['email']);
    } else {
        if ($_GET['fr'] == 0) {
            $row[] = "<div style=\"background:" . getColorStatusAl(utf8_encode($RFP['estado'])) . "\"></div><span style='cursor:pointer;' onclick=\"filterStatus(0,'" . utf8_encode($RFP['estado']) . "')\"> &nbsp; " . 
            (strpos($RFP['estado'], 'Cred') ? 'Ativo' : ($RFP['estado'] == 'Ferias' ? 'Congelado' : 
            ($RFP['estado'] == 'AtivoEmAberto' ? 'Ativo em Aberto' : 
            ($RFP['estado'] == 'AtivoAbono' ? 'Ativo Abono' : 
            ($RFP['estado'] == 'DesligadoEmAberto' ? 'Desligado Em Aberto' : $RFP['estado']))))) . "</span>";
            $row[] = $RFP['alunos'];
            $row[] = $RFP['planos'];
            $row[] = getColorStatusAl($RFP['estado']);
        } elseif ($_GET['fr'] == 1) {
            $row[] = "<span style='cursor:pointer;' onclick=\"filterAddInfo(1,'" . $RFP['filtro'] . "')\">" . $RFP['estado'] . "</span>";
            $row[] = $RFP['alunos'];
            $row[] = $RFP['planos'];
            $row[] = $RFP['estado'];
        } elseif ($_GET['fr'] == 2) {
            $row[] = "<div style=\"background:" . getColor($iTotal) . "\"></div><span style='cursor:pointer;' onclick=\"filterStatusVis(2,'retornos')\"> &nbsp; Visitantes Convertidos</span>";
            $row[] = $RFP['convertidos'];
            $output['aaData'][] = $row;
            $iTotal++;
            $row = array();
            $row[] = "<div style=\"background:" . getColor($iTotal) . "\"></div><span style='cursor:pointer;' onclick=\"filterStatusVis(2,'nconvertidos')\"> &nbsp; Visitantes Não Convertidos" . "</span>";
            $row[] = $RFP['nconvertidos'];
            $output['aaData'][] = $row;
            $iTotal++;
            $row = array();
            $row[] = "<div style=\"background:" . getColor($iTotal) . "\"></div><span style='cursor:pointer;' onclick=\"filterStatusVis(2,'tconvertidos')\"> &nbsp; Taxa de Conversão</span>";
            $row[] = escreverNumero((($RFP['convertidos'] + $RFP['nconvertidos']) > 0 ? ($RFP['convertidos'] * 100) / ($RFP['convertidos'] + $RFP['nconvertidos']) : 0));
        } elseif ($_GET['fr'] == 3) {
            $row[] = "<div style=\"background:" . getColorStatusAl(utf8_encode($RFP['estado'])) . "\"></div><span style='cursor:pointer;' onclick=\"filterStatus(3,'" . utf8_encode($RFP['estado']) . "')\"> &nbsp; " . utf8_encode(($RFP['estado'] == "AtivoCred" ? "Bolsistas" : ($RFP['estado'] == "AtivoAbono" ? "Ativo Abono" : $RFP['estado']))) . "</span>";
            $row[] = $RFP['alunos'];
            $row[] = $RFP['planos'];
            $row[] = getColorStatusAl($RFP['estado']);
        } elseif ($_GET['fr'] == 4) {
            $row[] = "<div style=\"background:" . getColor($iTotal) . "\"></div><span style='cursor:pointer;' onclick=\"filterAddInfo(4,'" . $RFP['filtro'] . "')\"> &nbsp; " . $RFP['estado'] . "</span>";
            $row[] = $RFP['alunos'];
            $row[] = $RFP['planos'];
        }
    }
    $output['aaData'][] = $row;
    $iTotal++;
}

if (isset($_GET['ex']) && $_GET['ex'] == 1) {
    $topo = '<tr><td><b>Código </b></td><td><b>Nome </b></td><td><b>Dt. Nasci </b></td><td><b>Sexo </b><td><b>Status </b></td><td><b>Telefone </b></td><td><b>Celular </b></td><td><b>Email </b></td></tr>';
    exportaExcel($output['aaData'], $topo);
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
