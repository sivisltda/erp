<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');
require_once(__DIR__ . '/../../../util/excel/exportaExcel.php');

$F1 = '0';
$F2 = 'L';
$DateBegin = '';
$DateEnd = '';
$comando = "";
$myArray = [];

if (is_numeric($_GET['id'])) {
    $F1 = $_GET['id'];
}

//L=Lista, T=Turno, D=Diário
if ($_GET['tipo'] == "L" || $_GET['tipo'] == "T" || $_GET['tipo'] == "D") {
    $F2 = $_GET['tipo'];
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
    $comando .= " and data_acesso between " . valoresDataHora2($DateBegin, "00:00:00") . " and " . valoresDataHora2($DateEnd, "23:59:59");
    if ($F2 == "D") {
        $dias = (int) floor((geraTimestamp($DateEnd) - geraTimestamp($DateBegin)) / (60 * 60 * 24));
        if ($dias > 0) {
            for ($i = 0; $i <= $dias; $i++) {
                $aux[$i] = escreverDataSoma($DateBegin, " + " . $i . " days");
                $myArray[$i]['date'] = escreverDataSoma($DateBegin, " + " . $i . " days");
                $myArray[$i]['value'] = 0;
            }
        }
    }
}

$query = "set dateformat dmy;";
if ($F2 == "T") {
    $query .= "select isnull(sum(x.cmm),0) as Manha,isnull(sum(x.cmt),0) as Tarde,isnull(sum(x.cmn),0) as Noite from(
    select case when isnull(DATEPART(HOUR,data_acesso),0) between 0 and 12 then 1 else 0 end cmm,
    case when isnull(DATEPART(HOUR,data_acesso),0) between 13 and 18 then 1 else 0 end cmt,
    case when isnull(DATEPART(HOUR,data_acesso),0) between 19 and 23 then 1 else 0 end cmn ";
} elseif ($F2 == "L") {
    $query .= "select data_acesso,status_acesso,nome_ambiente,nome_catraca ";
} elseif ($F2 == "D") {
    $query .= "select cast(data_acesso as DATE) data_acesso,min(cast(data_acesso as time)) hora_acesso ";
}
$query .= " from sf_acessos 
left join sf_ambientes on sf_ambientes.id_ambiente = sf_acessos.ambiente 
left join sf_catracas on sf_catracas.id_catraca = sf_acessos.catraca 
where id_fonecedor = " . $F1 . $comando;
if ($F2 == "T") {
    $query .= ") as x";
} elseif ($F2 == "L") {
    $query .= "order by data_acesso desc";
} elseif ($F2 == "D") {
    $query .= "group by cast(data_acesso as DATE)";
}

if ($F2 == "L") {
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
}
//echo $query; exit;
$cur = odbc_exec($con, $query);
$i = 0;
while ($RFP = odbc_fetch_array($cur)) {
    $row = array();
    if ($F2 == "T") {
        $output[] = array("turno" => "Manha", "acesso" => $RFP['Manha']);
        $output[] = array("turno" => "Tarde", "acesso" => $RFP['Tarde']);
        $output[] = array("turno" => "Noite", "acesso" => $RFP['Noite']);
        $i++;
    } elseif ($F2 == "L") {
        $row[] = escreverData($RFP['data_acesso']);
        $row[] = escreverData($RFP['data_acesso'], 'H:i');
        $row[] = trataAcesso(utf8_encode($RFP['status_acesso']));
        $row[] = utf8_encode($RFP['nome_ambiente']);
        $row[] = utf8_encode($RFP['nome_catraca']);
        $output['aaData'][] = $row;
        $i++;
    } elseif ($F2 == "D") {
        $key = array_search(escreverData($RFP['data_acesso']), $aux);
        if (is_numeric($key)) {
            $myArray[$key]['value'] = escreverData($RFP['hora_acesso'], 'H');
        }
    }
}
if ($F2 == "D") {
    $count = count($myArray);
    for ($i = 0; $i < $count; $i++) {
        $row = array();
        $row['date'] = substr($myArray[$i]['date'], 0, 5);
        $row['value'] = $myArray[$i]['value'];
        $output[] = $row;
    }
} elseif ($F2 == "L") {
    $output['iTotalRecords'] = $i;
    $output['iTotalDisplayRecords'] = $i;
}
echo json_encode($output);
odbc_close($con);
