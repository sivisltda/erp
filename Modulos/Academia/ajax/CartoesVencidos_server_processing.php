<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');
require_once(__DIR__ . '/../../../util/excel/exportaExcel.php');

$iTotal = 0;
$iFilteredTotal = 0;
$where = '';

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if ($_GET['dt_begin'] != '' && $_GET['dt_end'] != '') {
    $dt_ini = str_replace("_", "/", $_GET['dt_begin']);
    $dt_end = str_replace("_", "/", $_GET['dt_end']);
    $where = " cc.validade_cartao BETWEEN " . valoresData2($dt_ini) . " and " . valoresData2($dt_end) . "";
}

if ($_GET['status'] != 'null') {
    $allStatus = str_replace(",", "','", $_GET['status']);
    $where = $where . " and A.fornecedores_status in ('" . $allStatus . "')";
}

if ($_GET['tipo'] != 'null') {
    $allTipo = str_replace(",", "','", $_GET['tipo']);
    $where = $where . " and credencial in ('" . $allTipo . "')";
}

$sQuery1 = "set dateformat dmy;select A.id_fornecedores_despesas,A.razao_social,CC.nome_cartao, cc.legenda_cartao, cc.validade_cartao from dbo.sf_fornecedores_despesas_cartao as CC inner join dbo.sf_fornecedores_despesas as A on cc.id_fornecedores_despesas = A.id_fornecedores_despesas where" . $where;
//echo $sQuery1;exit();
$cur2 = odbc_exec($con, $sQuery1);
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ($aRow = odbc_fetch_array($cur2)) {
    $row = array();
    $row[] = "<div id='formPQ' title=''>" . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow['id_fornecedores_despesas']) . "'>" . utf8_encode($aRow['id_fornecedores_despesas']) . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow['razao_social']) . "'>" . utf8_encode($aRow['razao_social']) . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow['nome_cartao']) . "'>" . utf8_encode($aRow['nome_cartao']) . "</div>";
    $row[] = "<div id='formPQ' title='" . $aRow['legenda_cartao'] . "'>" . $aRow['legenda_cartao'] . "</div>";
    $row[] = "<div id='formPQ' title='" . escreverData($aRow['validade_cartao']) . "'>" . escreverData($aRow['validade_cartao']) . "</div>";
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}

odbc_close($con);
