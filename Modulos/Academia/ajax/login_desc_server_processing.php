<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');
require_once(__DIR__ . '/../../../util/excel/exportaExcel.php');
$output = array();

$query = "select login_user,master,out_desconto from sf_usuarios u
 left join sf_usuarios_permissoes up on u.master = up.id_permissoes where login_user = " . valoresTexto('txtusu') . " and senha = " . valoresTexto2(encrypt($_POST['txtpass'], "VipService123", true)) . " and inativo = 0";
//echo $query;exit();
$cur = odbc_exec($con, $query);
if (odbc_num_rows($cur) == 0) {
    $row = array();
    $row["status"] = "SENHA";
    $output[] = $row;
} else {
    while ($aRow = odbc_fetch_array($cur)) {
        $row = array();
        if ($aRow['master'] == 0 || $aRow['out_desconto'] == 1) {
            $row["status"] = "YES";
            $row["User"] = $aRow['login_user'];
        } elseif ($aRow['out_desconto'] == 0) {
            $row["status"] = "SEM PERMISSÃO PARA DESCONTO";
            $row["User"] = $aRow['login_user'];
        }
        $output[] = $row;
    }
}

echo json_encode($output);
odbc_close($con);
