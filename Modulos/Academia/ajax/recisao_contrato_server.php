<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');
require_once(__DIR__ . '/../../../util/excel/exportaExcel.php');

$idPlano = $_GET['idplano'];
$motiv = $_GET['motiv'];
$where = "";
$mdl_seg_ = returnPart($_SESSION["modulos"], 14);

$sQuery1 = "set dateformat dmy;
update sf_vendas_planos set 
dt_cancelamento = " . valoresData2($_GET['dtLimite']) . ",
usuario_canc = " . $_SESSION["id_usuario"] . ",
obs_cancelamento ='" . utf8_decode($motiv) . "'
where id_plano = " . $idPlano;
odbc_exec($con, $sQuery1);

$sQuery1 = "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
select 'sf_vendas_planos', id_plano, '" . $_SESSION["login_usuario"] . "', 'C', 'CANCELAMENTO DE PLANO (' + (select descricao from sf_produtos t where t.conta_produto = ft.id_prod_plano) + ')', GETDATE(), favorecido 
from sf_vendas_planos ft where id_plano = " . $idPlano;
odbc_exec($con, $sQuery1);

$sQuery2 = "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
select 'sf_alunos_turmas', id_turma, '" . $_SESSION["login_usuario"] . "', 'R', '" . ($mdl_seg_ > 0 ? "SAIDA DA OFICINA" : "EXCLUSAO ALUNO TURMA") . " (' + (select descricao from sf_turmas t where t.id_turma = ft.id_turma) + ')', GETDATE(), id_fornecedores_despesas 
from sf_fornecedores_despesas_turmas ft where id_plano = " . $idPlano;
odbc_exec($con, $sQuery2);

$sQuery2 = "delete from sf_fornecedores_despesas_turmas where id_plano = " . $idPlano;
odbc_exec($con, $sQuery2);

$filial = $_SESSION["filial"];
if (is_numeric($_GET['credModelo'])) {
    $query = "insert into sf_fornecedores_despesas_credenciais (id_fornecedores_despesas, id_credencial, dt_inicio, dt_fim, motivo, dt_cadastro, usuario_resp) 
    values (" . $_GET["favoredico"] . "," . valoresSelect2($_GET['credModelo']) . ",getdate(), " . valoresData2($_GET['dtLimite']) . ", 'Cancelamento de plano', getdate()," . $_SESSION["id_usuario"] . ")";
    $cur = odbc_exec($con, $query);
}
odbc_close($con);