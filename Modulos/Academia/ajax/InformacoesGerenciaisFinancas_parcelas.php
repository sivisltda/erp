<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');
require_once(__DIR__ . '/../../../util/excel/exportaExcel.php');

$local = array();
$item = explode("-", $_REQUEST['id_venda']);
$sql = "set dateformat dmy;";
$where1 = ($_REQUEST['carteira'] == 1 ? " and B.descricao != 'CARTEIRA'" : "");
$where2 = ($_REQUEST['carteira'] == 1 ? " and B.descricao != 'CARTEIRA'" : "");
$where3 = ($_REQUEST['carteira'] == 1 ? " and B.descricao != 'CARTEIRA'" : "");

if ($_REQUEST['tipo'] == 0 && isset($_REQUEST['dt_begin']) && isset($_REQUEST['dt_end'])) {
    $where1 .= " and data_parcela between " . valoresData2($_REQUEST['dt_begin']) . " and " . valoresData2($_REQUEST['dt_end']) . "";
    $where2 .= " and data_vencimento between " . valoresData2($_REQUEST['dt_begin']) . " and " . valoresData2($_REQUEST['dt_end']) . "";
    $where3 .= " and data_parcela between " . valoresData2($_REQUEST['dt_begin']) . " and " . valoresData2($_REQUEST['dt_end']) . "";
}
if (count($item) == 2) {
    $tipo_venda = $item[0];
    $id_venda = $item[1];
    if (is_numeric($id_venda)) {
        if ($tipo_venda == "V") {
            $sql .= "select data_parcela, valor_parcela, pa, B.descricao from sf_venda_parcelas A
                inner join sf_tipo_documento B on A.tipo_documento = B.id_tipo_documento
                where venda = " . $id_venda . $where1 . " order by data_parcela";
        } else if ($tipo_venda == "C") {
            $sql .= "select data_vencimento data_parcela, valor_parcela, pa, B.descricao,id_lancamento_movimento from sf_lancamento_movimento_parcelas A
                inner join sf_tipo_documento B on A.tipo_documento = B.id_tipo_documento
                where id_lancamento_movimento = " . $id_venda . $where2 . " order by data_vencimento";
        } else if ($tipo_venda == "S") {
            $sql .= "select data_parcela, valor_parcela, pa, B.descricao,solicitacao_autorizacao from sf_solicitacao_autorizacao_parcelas A
                inner join sf_tipo_documento B on A.tipo_documento = B.id_tipo_documento
                where solicitacao_autorizacao = " . $id_venda . $where3 . " order by data_parcela";
        }
        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $local[] = array('data_parcela' => escreverData($RFP['data_parcela']),
                'valor_parcela' => escreverNumero($RFP['valor_parcela'], 1), 'pa' => utf8_encode($RFP['pa']),
                'descricao' => utf8_encode($RFP['descricao']));
        }
    }
}
echo(json_encode($local));
odbc_close($con);
