<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');

$sQuery = "set dateformat dmy;";
$DateFiltro = str_replace("_", "/", $_GET['dtAgenda']);
$i = 0;

if (isset($_GET['idAmbiente'])) {
    $where .= " and id_ambiente = " . $_GET['idAmbiente'];
}

if (isset($_GET['idHorario'])) {
    $where .= " and cod_turno = " . $_GET['idHorario'];
}

if (isset($_GET['idProfessor'])) {
    $where .= " and professor = " . $_GET['idProfessor'];
}

if (isset($_GET['idCliente'])) {
    $wheretp1 = "and A.id_fornecedores_despesas in(" . $_GET['idCliente'] . ")";
    $wheretp2 = "and B.id_turma in (select id_turma from dbo.sf_fornecedores_despesas_turmas where id_fornecedores_despesas = " . $_GET['idCliente'] . ") ";
}

if (isset($_GET['idPlano'])) {
    $wheretp1 = "and F.conta_produto in(" . $_GET['idPlano'] . ")";
    $wheretp2 = "and B.id_turma in(select id_turma from sf_turmas_produtos where id_produto in(" . $_GET['idPlano'] . ")) ";
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => 0,
    "iTotalDisplayRecords" => 0,
    "aaData" => array()
);

//agendamento
if ($_GET['tptable'] == 1) {
    $sQuery .= "select (CAST(A.id_fornecedores_despesas AS VARCHAR(10))+' - '+ D.razao_social) nm_cliente, 
    (CAST(F.conta_produto AS VARCHAR(128)) +' - '+ F.descricao) nm_produto, 
    G.razao_social nm_prof, H.nome_ambiente descricao_ambiente, C.nome_turno 
    from sf_fornecedores_despesas_turmas A 
      inner join sf_turmas B on A.id_turma = B.id_turma
      inner join sf_turnos C on B.horario = C.cod_turno
      inner join sf_fornecedores_despesas D on A.id_fornecedores_despesas = D.id_fornecedores_despesas
      inner join sf_vendas_planos E on A.id_plano = E.id_plano
      inner join sf_produtos F on E.id_prod_plano = F.conta_produto
      left join sf_fornecedores_despesas G on B.professor = G.id_fornecedores_despesas
      left join sf_ambientes H on B.ambiente = H.id_ambiente       
      where (SUBSTRING(dia_semana1,DATEPART(WEEKDAY," . valoresData2($DateFiltro) . "),1) = 1 
      or SUBSTRING(dia_semana2,DATEPART(WEEKDAY," . valoresData2($DateFiltro) . "),1) = 1 
      or SUBSTRING(dia_semana3,DATEPART(WEEKDAY," . valoresData2($DateFiltro) . "),1) = 1 
      or SUBSTRING(dia_semana4,DATEPART(WEEKDAY," . valoresData2($DateFiltro) . "),1) = 1 
      or SUBSTRING(dia_semana5,DATEPART(WEEKDAY," . valoresData2($DateFiltro) . "),1) = 1) " . $where . " " . $wheretp1;

    //echo $sQuery; exit;
    $cur = odbc_exec($con, $sQuery);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row[] = tableFormato($RFP['nm_cliente'], 'T', '', '');
        $row[] = tableFormato($RFP['nm_produto'], 'T', '', '');
        $row[] = tableFormato($RFP['nome_turno'], 'T', '', '');
        $row[] = tableFormato($RFP['nm_prof'], 'T', '', '');
        $row[] = tableFormato($RFP['descricao_ambiente'], 'T', '', '');
        $output['aaData'][] = $row;
        $i++;
    }

    $output['iTotalRecords'] = $i;
    $output['iTotalDisplayRecords'] = $i;
    if (!isset($_GET['pdf'])) {
        echo json_encode($output);
    }
} else { //Horarios
    $sQuery = "select COUNT(distinct(id_ambiente)) total from sf_ambientes A 
    inner join sf_turmas B on A.id_ambiente= B.ambiente 
    left join sf_turnos C on B.horario = C.cod_turno
    left join sf_fornecedores_despesas D on B.professor = D.id_fornecedores_despesas 
    where id_ambiente > 0 " . $where . " " . $wheretp2;
    $cur = odbc_exec($con, $sQuery);
    while ($RFP = odbc_fetch_array($cur)) {
        $qtdAmbiente = $RFP['total'];
    }
    $hr = 0;
    while ($hr <= 23) {
        $row = array();
        $row["hr"] = str_pad($hr, 2, "0", STR_PAD_LEFT) . ":00";
        for ($i = 0; $i <= $qtdAmbiente; $i++) {
            $row[] = "";
        }
        $hr += 1;
        $horas[] = $row;
    }

    $nmambientes[] = "";
    $idambientes[] = "";
    $sQuery = "set dateformat dmy;
    select id_ambiente, nome_ambiente, D.razao_social, B.descricao,
    isnull(faixa1_ini,0) faixa1_ini, isnull(faixa2_ini,0) faixa2_ini, isnull(faixa3_ini,0) faixa3_ini, isnull(faixa4_ini,0) faixa4_ini, isnull(faixa5_ini,0) faixa5_ini,
    isnull(faixa1_fim,0) faixa1_fim, isnull(faixa2_fim,0) faixa2_fim, isnull(faixa3_fim,0) faixa3_fim, isnull(faixa4_fim,0) faixa4_fim, isnull(faixa5_fim,0) faixa5_fim,
    case when (SUBSTRING(dia_semana1,DATEPART(WEEKDAY," . valoresData2($DateFiltro) . "),1) = 1) then 'S' else 'N' end diasemana1,
    case when (SUBSTRING(dia_semana2,DATEPART(WEEKDAY," . valoresData2($DateFiltro) . "),1) = 1) then 'S' else 'N' end diasemana2,
    case when (SUBSTRING(dia_semana3,DATEPART(WEEKDAY," . valoresData2($DateFiltro) . "),1) = 1) then 'S' else 'N' end diasemana3,
    case when (SUBSTRING(dia_semana4,DATEPART(WEEKDAY," . valoresData2($DateFiltro) . "),1) = 1) then 'S' else 'N' end diasemana4,
    case when (SUBSTRING(dia_semana5,DATEPART(WEEKDAY," . valoresData2($DateFiltro) . "),1) = 1) then 'S' else 'N' end diasemana5
    from sf_ambientes A 
    inner join sf_turmas B on A.id_ambiente= B.ambiente 
    left join sf_turnos C on B.horario = C.cod_turno
    left join sf_fornecedores_despesas D on B.professor = D.id_fornecedores_despesas 
    where id_ambiente > 0 " . $where . " " . $wheretp2 . "
    order by id_ambiente";
    //echo $sQuery; exit;
    $cur = odbc_exec($con, $sQuery);
    while ($RFP = odbc_fetch_array($cur)) {
        $keyAmb = array_search($RFP['id_ambiente'], $idambientes);
        if (!is_numeric($keyAmb)) {
            $nmambientes[] = tableFormato($RFP['nome_ambiente'], 'T', '', '');
            $idambientes[] = $RFP['id_ambiente'];
        }
        $keyAmb = array_search($RFP['id_ambiente'], $idambientes);
        if ($RFP['faixa1_ini'] !== "0" && $RFP['diasemana1'] == 'S') {
            $hr = substr(str_pad(str_replace(":", "", $RFP['faixa1_ini']), 4, "0", STR_PAD_LEFT), 0, 2);
            while ($hr <= substr(str_pad(str_replace(":", "", $RFP['faixa1_fim']), 4, "0", STR_PAD_LEFT), 0, 2)) {
                $keyHr = array_search($hr . ':00', array_column($horas, 'hr'));
                $horas[$keyHr][$keyAmb] .= retornaString(tableFormato($RFP['razao_social'], 'T', '', ''), tableFormato($RFP['descricao'], 'T', '', ''), $horas[$keyHr][$keyAmb]);
                $hr = str_pad($hr + 1, 2, "0", STR_PAD_LEFT);
            }
        }
        if ($RFP['faixa2_ini'] !== "0" && $RFP['diasemana2'] == 'S') {
            $hr = substr(str_pad(str_replace(":", "", $RFP['faixa2_ini']), 4, "0", STR_PAD_LEFT), 0, 2);
            while ($hr <= substr(str_pad(str_replace(":", "", $RFP['faixa2_fim']), 4, "0", STR_PAD_LEFT), 0, 2)) {
                $keyHr = array_search($hr . ':00', array_column($horas, 'hr'));
                $horas[$keyHr][$keyAmb] .= retornaString(tableFormato($RFP['razao_social'], 'T', '', ''), tableFormato($RFP['descricao'], 'T', '', ''), $horas[$keyHr][$keyAmb]);
                $hr = str_pad($hr + 1, 2, "0", STR_PAD_LEFT);
            }
        }
        if ($RFP['faixa3_ini'] !== "0" && $RFP['diasemana3'] == 'S') {
            $hr = substr(str_pad(str_replace(":", "", $RFP['faixa3_ini']), 4, "0", STR_PAD_LEFT), 0, 2);
            while ($hr <= substr(str_pad(str_replace(":", "", $RFP['faixa3_fim']), 4, "0", STR_PAD_LEFT), 0, 2)) {
                $keyHr = array_search($hr . ':00', array_column($horas, 'hr'));
                $horas[$keyHr][$keyAmb] .= retornaString(tableFormato($RFP['razao_social'], 'T', '', ''), tableFormato($RFP['descricao'], 'T', '', ''), $horas[$keyHr][$keyAmb]);
                $hr = str_pad($hr + 1, 2, "0", STR_PAD_LEFT);
            }
        }
        if ($RFP['faixa4_ini'] !== "0" && $RFP['diasemana4'] == 'S') {
            $hr = substr(str_pad(str_replace(":", "", $RFP['faixa4_ini']), 4, "0", STR_PAD_LEFT), 0, 2);
            while ($hr <= substr(str_pad(str_replace(":", "", $RFP['faixa4_fim']), 4, "0", STR_PAD_LEFT), 0, 2)) {
                $keyHr = array_search($hr . ':00', array_column($horas, 'hr'));
                $horas[$keyHr][$keyAmb] .= retornaString(tableFormato($RFP['razao_social'], 'T', '', ''), tableFormato($RFP['descricao'], 'T', '', ''), $horas[$keyHr][$keyAmb]);
                $hr = str_pad($hr + 1, 2, "0", STR_PAD_LEFT);
            }
        }
        if ($RFP['faixa5_ini'] !== "0" && $RFP['diasemana5'] == 'S') {
            $hr = substr(str_pad(str_replace(":", "", $RFP['faixa5_ini']), 4, "0", STR_PAD_LEFT), 0, 2);
            while ($hr <= substr(str_pad(str_replace(":", "", $RFP['faixa5_fim']), 4, "0", STR_PAD_LEFT), 0, 2)) {
                $keyHr = array_search($hr . ':00', array_column($horas, 'hr'));
                $horas[$keyHr][$keyAmb] .= retornaString(tableFormato($RFP['razao_social'], 'T', '', ''), tableFormato($RFP['descricao'], 'T', '', ''), $horas[$keyHr][$keyAmb]);
                $hr = str_pad($hr + 1, 2, "0", STR_PAD_LEFT);
            }
        }
    }
    $output[0] = $horas;
    $output[1] = $nmambientes;
    if (!isset($_GET['pdf'])) {
        echo json_encode($output);
    }
    //echo json_encode($myArray);
}

function retornaString($razao, $turma, $vlAtual) {
    $texto = ($razao !== "") ? $turma . "(" . $razao . ")" : $turma;
    return ($vlAtual !== "") ? ("|" . $texto) : $texto;
}

odbc_close($con);
