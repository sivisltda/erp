<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');
require_once(__DIR__ . '/../../../util/excel/exportaExcel.php');

$imprimir = 0;
$where = "";
$tipoData = 2;
$DateBegin = getData("B");
$DateEnd = getData("E");
$iTotal = 0;
$sLimit = 20;

if (is_numeric($_GET['ban'])) {
    $where .= " and id_banco = " . $_GET['ban'];
}
if (isset($_GET['grp'])) {
    $where .= " and sf_tipo_documento.id_tipo_documento in (" . $_GET['grp'] . ")";
}
if (isset($_GET['gfi'])) {
    $where .= " and sf_fornecedores_despesas.empresa in (" . $_GET['gfi'] . ")";
}
if (isset($_GET['gpc'])) {
    $where .= " and sf_fornecedores_despesas.grupo_pessoa in (" . $_GET['gpc'] . ")";
}
if (isset($_GET['gpc']) && $_GET['gpc'] != 'null') {
    $comando .= " and (sf_fornecedores_despesas.grupo_pessoa in (" . $_GET['gpc'] . ")" . (strpos($_GET['gpc'], "-1") !== false ? " or sf_fornecedores_despesas.grupo_pessoa is null" : "") . ")";
}
if (is_numeric($_GET['dtt'])) {
    $tipoData = $_GET['dtt'];
}
if ($_GET['dti'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
}
if ($_GET['dtf'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
}
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iTotal,
    "aaData" => array()
);
if (is_numeric($_GET['emp'])) {
    $whereX1 = " and sf_lancamento_movimento.empresa = " . $_GET['emp'];
    $whereX2 = " and sf_solicitacao_autorizacao.empresa = " . $_GET['emp'];
    $whereX3 = " and sf_vendas.empresa = " . $_GET['emp'];
}
if ($_GET['opr'] != "") {
    $whereX1 .= " and sf_lancamento_movimento_parcelas.syslogin = '" . $_GET['opr'] . "'";
    $whereX2 .= " and sf_solicitacao_autorizacao.sys_login = '" . $_GET['opr'] . "'";
    $whereX3 .= " and sf_vendas.vendedor = '" . $_GET['opr'] . "'";
}
if ($tipoData == "0") { //RECEBIMENTO    
    $where .= " and sf_tipo_documento.id_tipo_documento not in (12)";
} elseif ($tipoData == "1") { //VENDA   
    $where .= " and somar_rel = " . (isset($_GET['dx']) ? $_GET['dx'] : "1");
} elseif ($tipoData == "2") { //LANÇAMENTO   
    $where .= " and sf_tipo_documento.id_tipo_documento not in (12)";
}
$sQuery1 = "set dateformat dmy;";
if ($_GET['fr'] == 0 || $_GET['fr'] == 1) {
    if ($_GET['fr'] == 0) {
        $sQuery1 .= "select y.descricao,SUM(y.credito) credito, SUM(y.debito) debito, SUM(y.estorno) estorno from (
        select x.descricao,case when x.tipo = 'C' then sum(x.valor_pago) else 0 end credito,
        case when x.tipo = 'D' then sum(x.valor_pago) else 0 end debito,
        case when x.tipo = 'E' then sum(x.valor_pago) else 0 end estorno
        from (select sf_tipo_documento." . ($_GET['mdl'] == 2 ? "abreviacao" : "descricao") . " descricao,sum(valor_pago) valor_pago,sf_contas_movimento.tipo ";
    } elseif ($_GET['fr'] == 1) {
        $sQuery1 .= "select sf_lancamento_movimento.id_lancamento_movimento id, data_cadastro, sum(valor_pago) valor_parcela, sf_contas_movimento.tipo,id_fornecedores_despesas, razao_social,COUNT(*) qtd_parcela,sys_login operador,'C' tabela,
        STUFF((SELECT distinct '|' + abreviacao FROM sf_lancamento_movimento_parcelas vp inner join sf_tipo_documento td on vp.tipo_documento = td.id_tipo_documento WHERE vp.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento FOR XML PATH('')), 1, 1, '') as abreviacao,historico ";
    }
    $sQuery1 .= "from sf_lancamento_movimento_parcelas
    inner join sf_lancamento_movimento on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
    inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
    inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas
    inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas    
    left join sf_tipo_documento on sf_lancamento_movimento_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento
    where valor_pago > 0 " . $where . " " . $whereX1 . " AND data_pagamento >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND data_pagamento <= " . valoresDataHora2($DateEnd, "23:59:59") . 
    " AND sf_lancamento_movimento_parcelas.inativo in (0,2) and sf_lancamento_movimento.status = 'Aprovado' ";
    if ($_GET['fr'] == 0) {
        $sQuery1 .= " group by sf_tipo_documento." . ($_GET['mdl'] == 2 ? "abreviacao" : "descricao") . ",sf_contas_movimento.tipo ";
    } else {
        $sQuery1 .= " group by sf_lancamento_movimento.id_lancamento_movimento,data_cadastro,sf_contas_movimento.tipo,id_fornecedores_despesas,razao_social,sys_login,historico ";
    }
    $sQuery1 .= " union all ";
    if ($_GET['fr'] == 0) {
        $sQuery1 .= "select sf_tipo_documento." . ($_GET['mdl'] == 2 ? "abreviacao" : "descricao") . " descricao,sum(" . ($tipoData == 0 ? "valor_pago" : "valor_parcela") . ") valor_parcela,sf_contas_movimento.tipo ";
    } elseif ($_GET['fr'] == 1) {
        $sQuery1 .= "select sf_solicitacao_autorizacao.id_solicitacao_autorizacao id, data_lanc, sum(" . ($tipoData == 0 ? "valor_pago" : "valor_parcela") . ") valor_parcela,sf_contas_movimento.tipo,id_fornecedores_despesas, razao_social,COUNT(*) qtd_parcela,sys_login operador,'S' tabela,
        STUFF((SELECT distinct '|' + abreviacao FROM sf_solicitacao_autorizacao_parcelas vp inner join sf_tipo_documento td on vp.tipo_documento = td.id_tipo_documento WHERE vp.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao FOR XML PATH('')), 1, 1, '') as abreviacao, comentarios ";
    }
    $sQuery1 .= "from sf_solicitacao_autorizacao_parcelas
    inner join sf_solicitacao_autorizacao on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
    inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
    inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas
    inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas    
    left join sf_tipo_documento on sf_solicitacao_autorizacao_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento
    where status = 'Aprovado' " . $where . " AND " . ($tipoData == 0 ? "valor_pago" : "valor_parcela") . " > 0 " . $whereX2 . " AND " . ($tipoData == 0 ? "data_pagamento" : "data_lanc") . " >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND " . ($tipoData == 0 ? "data_pagamento" : "data_lanc") . " <= " . valoresDataHora2($DateEnd, "23:59:59") . " AND sf_solicitacao_autorizacao_parcelas.inativo in (0,2) ";
    if ($_GET['fr'] == 0) {
        $sQuery1 .= " group by sf_tipo_documento." . ($_GET['mdl'] == 2 ? "abreviacao" : "descricao") . ",sf_contas_movimento.tipo ";
    } else {
        $sQuery1 .= " group by sf_solicitacao_autorizacao.id_solicitacao_autorizacao,data_lanc,sf_contas_movimento.tipo,id_fornecedores_despesas,razao_social,sys_login,comentarios ";
    }
    $sQuery1 .= "union all ";
    if ($_GET['fr'] == 0) {
        $sQuery1 .= "select sf_tipo_documento." . ($_GET['mdl'] == 2 ? "abreviacao" : "descricao") . " descricao,sum(" . ($tipoData == 0 ? "valor_pago" : "valor_parcela") . ") valor_parcela, case when sf_vendas.cov = 'V' then 'C' else 'D' end ";
    } elseif ($_GET['fr'] == 1) {
        $sQuery1 .= "select sf_vendas.id_venda id, data_venda, sum(" . ($tipoData == 0 ? "valor_pago" : "valor_parcela") . ") valor_parcela, case when sf_vendas.cov = 'V' then (case when status = 'Aprovado' then 'C' else 'D' end) else 'D' end,id_fornecedores_despesas, razao_social,COUNT(*) qtd_parcela,case when status = 'Aprovado' then sf_vendas.vendedor else sf_usuarios.login_user end operador,'V' tabela,
        STUFF((SELECT distinct '|' + abreviacao FROM sf_venda_parcelas vp inner join sf_tipo_documento td on vp.tipo_documento = td.id_tipo_documento WHERE vp.venda = sf_vendas.id_venda FOR XML PATH('')), 1, 1, '') as abreviacao,
        case when historico_venda = 'VENDA AVULSA' then 'VENDA AVULSA (' + STUFF((SELECT distinct ',' + descricao FROM sf_vendas_itens vi inner join sf_produtos p on p.conta_produto = vi.produto WHERE vi.id_venda = sf_vendas.id_venda FOR XML PATH('')), 1, 1, '') + ')' else historico_venda end historico_venda ";
    }
    $sQuery1 .= "from sf_venda_parcelas
    inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda
    inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas    
    left join sf_tipo_documento on sf_venda_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento
    left join sf_usuarios on sf_vendas.user_estorno = sf_usuarios.id_usuario
    where status not in ('Aguarda') and cov in ('C','V') " . $where . " AND " . ($tipoData == 0 ? "valor_pago" : "valor_parcela") . " > 0 " . $whereX3 . " AND " . ($tipoData == 0 ? "data_pagamento" : "data_venda") . " >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND " . ($tipoData == 0 ? "data_pagamento" : "data_venda") . " <= " . valoresDataHora2($DateEnd, "23:59:59") . " AND sf_venda_parcelas.inativo in (0,2) ";
    if ($_GET['fr'] == 0) {
        $sQuery1 .= " group by sf_tipo_documento." . ($_GET['mdl'] == 2 ? "abreviacao" : "descricao") . ",sf_vendas.cov ";
    } else {
        $sQuery1 .= " group by sf_vendas.id_venda, cov, data_venda,id_fornecedores_despesas, razao_social, status,sf_usuarios.login_user, sf_vendas.vendedor, historico_venda ";
    }
    $sQuery1 .= " union all ";
    if ($_GET['fr'] == 0) {
        $sQuery1 .= "select sf_tipo_documento." . ($_GET['mdl'] == 2 ? "abreviacao" : "descricao") . " descricao,sum(" . ($tipoData == 0 ? "valor_pago" : "valor_parcela") . ") valor_parcela, 'E' ";
    } elseif ($_GET['fr'] == 1) {
        $sQuery1 .= "select sf_vendas.id_venda id, data_venda, sum(" . ($tipoData == 0 ? "valor_pago" : "valor_parcela") . ") valor_parcela, 'C',id_fornecedores_despesas, razao_social,COUNT(*) qtd_parcela,syslogin operador,'V' tabela,
        STUFF((SELECT distinct '|' + abreviacao FROM sf_venda_parcelas vp inner join sf_tipo_documento td on vp.tipo_documento = td.id_tipo_documento WHERE vp.venda = sf_vendas.id_venda FOR XML PATH('')), 1, 1, '') as abreviacao, historico_venda ";
    }
    $sQuery1 .= "from sf_venda_parcelas
    inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda
    inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas    
    left join sf_tipo_documento on sf_venda_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento
    where status = 'Reprovado' and cov = 'V' " . $where . " AND " . ($tipoData == 0 ? "valor_pago" : "valor_parcela") . " > 0 " . $whereX3 . " AND " . ($tipoData == 0 ? "data_pagamento" : "data_venda") . " >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND " . ($tipoData == 0 ? "data_pagamento" : "data_venda") . " <= " . valoresDataHora2($DateEnd, "23:59:59") . " AND sf_venda_parcelas.inativo in (0,2) ";
    if ($_GET['fr'] == 0) {
        $sQuery1 .= " group by sf_tipo_documento." . ($_GET['mdl'] == 2 ? "abreviacao" : "descricao") . ", sf_vendas.cov) as x
        group by x.descricao, x.tipo) as y group by y.descricao order by y.descricao";
    } else {
        $sQuery1 .= "group by sf_vendas.id_venda, cov, data_venda,status,id_fornecedores_despesas, razao_social, syslogin, historico_venda ";
        $sQuery1 .= 'order by 2,6,4 ';
    }
} elseif ($_GET['fr'] == 2) {       
    $sQuery1 .= "select count(sf_vendas.id_venda) id_venda,count(distinct sf_vendas.cliente_venda) cliente,
    isnull(sum(case when sf_produtos.tipo = 'C' then 1 else 0 end),0) total_contrato,
    isnull(sum(case when sf_produtos.tipo = 'C' then sf_vendas_itens.valor_total / (case when isnull(sf_produtos_parcelas.parcela, 1) <= 0 then 1 else isnull(sf_produtos_parcelas.parcela, 1) end) end),0) valor_contrato,
    isnull(sum(sf_vendas_itens.valor_total),0) valor_total
    from sf_vendas inner join sf_vendas_itens on sf_vendas.id_venda = sf_vendas_itens.id_venda
    inner join sf_produtos on sf_produtos.conta_produto = sf_vendas_itens.produto
    left join sf_vendas_planos_mensalidade on sf_vendas_planos_mensalidade.id_item_venda_mens = sf_vendas_itens.id_item_venda
    left join sf_produtos_parcelas on sf_produtos_parcelas.id_parcela = sf_vendas_planos_mensalidade.id_parc_prod_mens
    where sf_vendas.status = 'Aprovado' and cov = 'V' " . $whereX3 . "
    and data_venda between " . valoresDataHora2($DateBegin, "00:00:00") . " and " . valoresDataHora2($DateEnd, "23:59:59") . "" . 
    ($tipoData == "1" ? "" : ""); //" and (select count(id_parcela) from sf_venda_parcelas where venda = sf_vendas.id_venda and inativo = 0 and somar_rel = 1) > 0"
} elseif ($_GET['fr'] == 3) {
    $sQuery1 .= "select SUM(case when sf_produtos.tipo = 'P' then valor_total else 0 end) 'Produtos',
    SUM(case when sf_produtos.tipo = 'S' then valor_total else 0 end) 'Servicos',
    SUM(case when sf_produtos.tipo = 'C' then valor_total else 0 end) 'Contratos'
    from sf_vendas_itens inner join sf_produtos on sf_produtos.conta_produto = sf_vendas_itens.produto
    inner join sf_vendas on sf_vendas_itens.id_venda = sf_vendas.id_venda
    where sf_vendas.status = 'Aprovado' and cov = 'V' " . $whereX3 . " and data_venda between " . valoresDataHora2($DateBegin, "00:00:00") . " and " . valoresDataHora2($DateEnd, "23:59:59") . "";
} elseif ($_GET['fr'] == 4) {
    $sQuery1 .= "select sum(case when x.anterior is null then 1 else 0 end) 'Novos',
    sum(case when x.anterior is not null and datediff(day,x.anterior,x.atual) > x.dias then 1 else 0 end) 'Retornos',
    sum(case when x.anterior is not null and datediff(day,x.anterior,x.atual) <= x.dias then 1 else 0 end) 'Renovacoes',
    (select count(*) from sf_vendas_planos inner join sf_fornecedores_despesas on sf_vendas_planos.favorecido = sf_fornecedores_despesas.id_fornecedores_despesas    
    where dt_cancelamento is not null " . (is_numeric($_GET['emp']) ? " and sf_fornecedores_despesas.empresa = " . $_GET['emp'] : "") . " 
    and cast(dt_cancelamento as date) BETWEEN " . valoresDataHora2($DateBegin, "00:00:00") . " and " . valoresDataHora2($DateEnd, "23:59:59") . ") 'Cancelados'    
    from(select (select max(dt_pagamento_mens) from sf_vendas_planos_mensalidade B
    where B.id_plano_mens = A.id_plano_mens and B.id_mens < A.id_mens) anterior, dt_pagamento_mens atual,
    (select ACA_PLN_DIAS_RENOVACAO from sf_configuracao) dias
    from sf_vendas_planos_mensalidade A inner join sf_vendas_planos P on P.id_plano = A.id_plano_mens
    inner join sf_fornecedores_despesas F on P.favorecido = F.id_fornecedores_despesas
    where id_item_venda_mens is not null " . (is_numeric($_GET['emp']) ? " and F.empresa = " . $_GET['emp'] : "") . "
    and dt_pagamento_mens between " . valoresDataHora2($DateBegin, "00:00:00") . " and " . valoresDataHora2($DateEnd, "23:59:59") . ") as x";
} elseif ($_GET['fr'] == 5) {
    $where3 = '';
    if ($_GET['loj'] != 'null') {
        $sQuery1 .= "select u.login_user,u.nome, uf.id_usuario_f,uf.id_filial_f
        from dbo.sf_usuarios as u 
        inner join dbo.sf_usuarios_filiais as uf on u.id_usuario = uf.id_usuario_f 
        where u.id_usuario > 1 and u.inativo = 0 and id_filial_f =" . $_GET['loj'];
    } else {
        $sQuery1 .= "select nome, login_user from sf_usuarios where inativo = 0 and id_usuario > 1";
    }
}
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($RFP = odbc_fetch_array($cur)) {
    $row = array();
    if ($_GET['fr'] == 0) {
        $row[] = utf8_encode($RFP['descricao']);
        $row[] = escreverNumero($RFP['credito'], ($_GET['mdl'] == 2 ? 0 : 1));
        $row[] = escreverNumero($RFP['debito'], ($_GET['mdl'] == 2 ? 0 : 1));
        $row[] = escreverNumero($RFP['estorno'], ($_GET['mdl'] == 2 ? 0 : 1));
        $row[] = escreverNumero(($RFP['credito'] - $RFP['debito'] - $RFP['estorno']), ($_GET['mdl'] == 2 ? 0 : 1));
    } else if ($_GET['fr'] == 1) {
        if (!isset($_GET['isPdf']) && !isset($_GET['ex'])) {
            $row[] = "<center><img src=\"./../../img/details_open.png\"></center>";
        }
        $row[] = "<center>" . utf8_encode($RFP['tabela']) . "-" . utf8_encode($RFP['id']) . "</center>";
        $row[] = "<center>" . escreverDataHora($RFP['data_cadastro']) . "</center>";
        if (utf8_encode($RFP["tipo"]) == "D") {
            $row[] = "<span style='color:#900'>" . str_replace(" ", " -", escreverNumero($RFP['valor_parcela'], 1)) . "</span>";
        } else {
            $row[] = "<span style='color:#090'>" . escreverNumero($RFP['valor_parcela'], 1) . "</span>";
        }
        $row[] = "<center>" . utf8_encode($RFP['abreviacao']) . "</center>";
        $row[] = utf8_encode($RFP['id_fornecedores_despesas']);
        if (!isset($_GET['isPdf']) && $RFP['id_fornecedores_despesas'] > 0) {
            $row[] = "<a href='javascript:void(0)' onClick='AbrirCli(" . utf8_encode($RFP['id_fornecedores_despesas']) . ")'><div id='formPQ' title='" . utf8_encode($RFP['razao_social']) . "'>" . utf8_encode($RFP['razao_social']) . "</div></a>";
        } else {
            $row[] = ($RFP['id_fornecedores_despesas'] == 0 && strlen($RFP['historico']) > 0 ? utf8_encode($RFP['historico']) : utf8_encode($RFP['razao_social']));
        }
        $row[] = "<center>" . utf8_encode($RFP['qtd_parcela']) . "</center>";
        $row[] = utf8_encode($RFP['operador']);
        $row[] = utf8_encode($RFP['tabela']) . "-" . utf8_encode($RFP['id']);
    } else if ($_GET['fr'] == 2) {
        $row[] = "Faturamento por Contrato";
        $row[] = escreverNumero(($RFP['total_contrato'] > 0 ? $RFP['valor_contrato'] / $RFP['total_contrato'] : 0), 1);
        $output['aaData'][] = $row;
        $row = array();
        $row[] = "Faturamento Bruto";
        $row[] = escreverNumero(($RFP['cliente'] > 0 ? $RFP['valor_total'] / $RFP['cliente'] : 0), 1);
        $output['aaData'][] = $row;
        $row = array();
        $row[] = "Por Venda";
        $row[] = escreverNumero(($RFP['id_venda'] > 0 ? $RFP['valor_total'] / $RFP['id_venda'] : 0), 1);
    } else if ($_GET['fr'] == 3) {
        $row[] = "Produtos";
        $row[] = escreverNumero($RFP['Produtos'], 1);
        $output['aaData'][] = $row;
        $row = array();
        $row[] = "Serviços";
        $row[] = escreverNumero($RFP['Servicos'], 1);
        $output['aaData'][] = $row;
        $row = array();
        $row[] = "Contratos";
        $row[] = escreverNumero($RFP['Contratos'], 1);
    } elseif ($_GET['fr'] == 4) {
        $row[] = "<div style=\"background:" . getColor(0) . "\"></div><span onclick=\"filterStatusNRR(1,'novas')\"> &nbsp; Novas Matrículas</span>";
        $row[] = (is_numeric($RFP['Novos']) ? $RFP['Novos'] : 0);
        $output['aaData'][] = $row;
        $row = array();
        $row[] = "<div style=\"background:" . getColor(1) . "\"></div><span onclick=\"filterStatusNRR(1,'renovacoes')\"> &nbsp; Renovações</span>";
        $row[] = (is_numeric($RFP['Renovacoes']) ? $RFP['Renovacoes'] : 0);
        $output['aaData'][] = $row;
        $row = array();
        $row[] = "<div style=\"background:" . getColor(2) . "\"></div><span onclick=\"filterStatusNRR(1,'retornos')\"> &nbsp; Retornos</span>";
        $row[] = (is_numeric($RFP['Retornos']) ? $RFP['Retornos'] : 0);
        $output['aaData'][] = $row;
        $row = array();
        $row[] = "<div style=\"background:" . getColor(3) . "\"></div><span onclick=\"filterStatusNRR(1,'cancelados')\"> &nbsp; Cancelados</span>";
        $row[] = (is_numeric($RFP['Cancelados']) ? $RFP['Cancelados'] : 0);
    } elseif ($_GET['fr'] == 5) {
        $row["nome"] = utf8_encode($RFP['nome']);
        $row["usuario"] = utf8_encode($RFP['login_user']);
    }
    $output['aaData'][] = $row;
    $iTotal++;
}

if (isset($_GET['ex']) && $_GET['ex'] == 1) {
    $topo = '<tr><td><b>Código </b></td><td><b>Data/Hora </b></td><td><b>Valor </b></td><td><b>Matricula </b><td><b>Cliente </b></td><td><b>Parcelas </b></td><td><b>Operador(a) </b></td></tr>';
    exportaExcel($output['aaData'], $topo);
}

if (!isset($_GET['isPdf'])) {
    echo json_encode($output);
}