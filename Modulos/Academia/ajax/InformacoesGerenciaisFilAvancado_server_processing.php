<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');

$imprimir = 0;
$where = "";
$distinct = "distinct";
$orderBy = " order by id_fornecedores_despesas";
$iTotal = 0;

if ($_GET['id'] == 0) {
    $where .= " and id_fornecedores_despesas < 0";
}

if ($_GET['loj'] != 0 && $_GET['loj'] != 'null') {
    $where .= " and empresa=" . $_GET['loj'];
}

if ($_GET['sta'] != 'null') {
    $where .= " and (";
    $dataAux = explode(",", $_GET['sta']);
    for ($i = 0; $i < count($dataAux); $i++) {
        $where .= ($i > 0 ? " or " : "") . " fornecedores_status = '" . getStatus($dataAux[$i]) . "'";
    }
    $where .= ")";
}

if ($_GET['con'] != 'null') {
    $where .= " and id_fornecedores_despesas in (";
    $where .= "select id_fornecedor from sf_fornecedores_despesas_convenios where convenio in (" . $_GET['con'] . ")";
    $where .= ")";
}

if ($_GET['pla'] != 'null') {
    $where .= " and id_fornecedores_despesas in (";
    $where .= "select favorecido from sf_vendas_planos where id_prod_plano in (" . $_GET['pla'] . ")";
    $where .= ")";
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $dataAux = explode("/", $_GET['dti']);
    $dtini = $dataAux[1] . $dataAux[0];
    $dataAux = explode("/", $_GET['dtf']);
    $dtfim = $dataAux[1] . $dataAux[0];
    $distinct = "";
    $where .= " AND SUBSTRING(REPLACE(CONVERT(VARCHAR(10), data_nascimento, 1), '/', ''),1,4) BETWEEN '" . $dtini . "' and '" . $dtfim . "' ";
    $orderBy = " order by month(data_nascimento), day(data_nascimento)";
}
if (is_numeric($_GET['dsa']) && is_numeric($_GET['dsf'])) {
    $where .= " and isnull((select datediff(day,MAX(data_acesso),getdate()) from sf_acessos where id_fonecedor = id_fornecedores_despesas),0) between " . $_GET['dsa'] . " and " . $_GET['dsf'];
}
if ($_GET['pef'] != 'null') {
    $where .= " and id_fornecedores_despesas in (";
    $where .= "select id_fornecedor from sf_fornecedores_despesas_convenios where convenio in (" . $_GET['pef'] . ")";
    $where .= ")";
}
if ($_GET['pro'] != 'null') {
    $nDefinido = "";
    $dataAux = explode(",", $_GET['pro']);
    for ($i = 0; $i < count($dataAux); $i++) {
        if($dataAux[$i] == "-1") {
            $nDefinido = " or procedencia is null ";            
        }
    }    
    $where .= " and (procedencia in (" . $_GET['pro'] . ") " . $nDefinido . ")";
}
if (is_numeric($_GET['idi']) && is_numeric($_GET['idf'])) {
    $where .= " and datediff(year,data_nascimento,GETDATE()) between " . $_GET['idi'] . " and " . $_GET['idf'];    
}
if (is_numeric($_GET['adi']) && is_numeric($_GET['adf'])) {
    $where .= " and id_fornecedores_despesas not in (select pessoa from sf_telemarketing where data_tele between dateadd(day, -" . $_GET['adf'] . ", getdate()) and dateadd(day, -" . $_GET['adi'] . ", getdate()) and de_pessoa not in (1))";    
}
if ($_GET['sex'] == "M" || $_GET['sex'] == "F") {
    $where .= " and sexo = '" . $_GET['sex'] . "'";
}
if ($_GET['prf'] != 'null') {
    $where .= " and profissao in (";
    $dataAux = explode(",", $_GET['prf']);
    for ($i = 0; $i < count($dataAux); $i++) {
        $where .= ($i > 0 ? "," : "") . "'" . $dataAux[$i] . "'";
    }
    $where .= ")";
}
if (isset($_GET['grp']) && $_GET['grp'] != 'null') {
    $where .= " and (grupo_pessoa in (" . $_GET['grp'] . ")" . (strpos($_GET['grp'], "-1") !== false ? " or grupo_pessoa is null" : "") . ")";
    $where2 .= $where;    
}
if ($_GET['eci'] != 'null') {
    $where .= " and estado_civil in (";
    $dataAux = explode(",", $_GET['eci']);
    for ($i = 0; $i < count($dataAux); $i++) {
        $where .= ($i > 0 ? "," : "") . "'" . $dataAux[$i] . "'";
    }
    $where .= ")";
}
if (is_numeric($_GET['cid'])) {
    $where .= " and cidade = " . $_GET['cid'] . "";
}
if (is_numeric($_GET['com'])) {
    $where .= " and id_fornecedores_despesas " . ($_GET['com'] == 0 ? "not" : "") . " in (select id_cliente from sf_comissao_cliente)";
}
if ($_GET['bai'] != 'null') {
    $where .= " and bairro = '" . $_GET['bai'] . "'";
}
if (is_numeric($_GET['dep'])) {
    $where .= " and (select count(id_dependente) from sf_fornecedores_despesas_dependentes where id_titular = id_fornecedores_despesas) = " . $_GET['dep'];
}
if (is_numeric($_GET['par'])) {
    $where .= " and id_fornecedores_despesas in (select id_dependente from sf_fornecedores_despesas_dependentes where parentesco = " . $_GET['par'] . ")";
}
if ($_GET['crd'] != 'null') {
    $where .= " and id_fornecedores_despesas in (select id_fornecedores_despesas from sf_fornecedores_despesas_credenciais
    where dt_cancelamento is null and id_credencial in ("; //and cast(GETDATE() as date) <= dt_fim 
    $dataAux = explode(",", $_GET['crd']);
    for ($i = 0; $i < count($dataAux); $i++) {
        $where .= ($i > 0 ? "," : "") . "'" . $dataAux[$i] . "'";
    }
    $where .= "))";
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iTotal,
    "aaData" => array()
);

$sQuery1 = "select " . $distinct . " id_fornecedores_despesas,razao_social,data_nascimento,sexo,fornecedores_status 'estado',profissao,estado_civil,cnpj,
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'telefone',
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'celular',
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'email',
STUFF((SELECT distinct '|' + descricao from sf_fornecedores_despesas_convenios fc inner join sf_convenios cc on fc.convenio = cc.id_convenio where id_fornecedor = id_fornecedores_despesas FOR XML PATH('')), 1, 1, '') as convenios
from sf_fornecedores_despesas ";
if (is_numeric($_GET['tit'])) {
    $sQuery1 .= "inner join sf_fornecedores_despesas_dependentes on id_fornecedores_despesas = " . ($_GET['tit'] == 0 ? "id_titular" : "id_dependente") . " ";
}
$sQuery1 .= "where id_fornecedores_despesas > 0 and tipo = 'C' and inativo = 0 " . $where . $orderBy;
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($RFP = odbc_fetch_array($cur)) {
    $iTotal = $iTotal + 1;
    $row = array();
    $row[] = utf8_encode($RFP['id_fornecedores_despesas']);
    $row[] = "<a href='javascript:void(0)' onClick='AbrirCli(" . utf8_encode($RFP['id_fornecedores_despesas']) . ")'><div id='formPQ' title='" . utf8_encode($RFP['razao_social']) . "'>" . utf8_encode($RFP['razao_social']) . "</div></a>";            
    $row[] = escreverData($RFP['data_nascimento']);
    $row[] = utf8_encode($RFP['sexo']);
    $row[] = strpos($RFP['estado'], 'Cred') ? 'ATIVO' : ($RFP['estado'] == 'Ferias' ? 'CONGELADO' : 
    ($RFP['estado'] == 'AtivoEmAberto' ? 'ATIVO EM ABERTO' : 
    ($RFP['estado'] == 'AtivoAbono' ? 'ATIVO ABONO' : 
    ($RFP['estado'] == 'DesligadoEmAberto' ? 'DESLIGADO EM ABERTO' : strtoupper($RFP['estado'])))));
    $row[] = utf8_encode($RFP['telefone']);
    $row[] = utf8_encode($RFP['celular']);    
    $row[] = utf8_encode($RFP['email']);
    $row[] = utf8_encode($RFP['cnpj']);    
    $row[] = utf8_encode($RFP['convenios']);
    $row[] = utf8_encode($RFP['profissao']);
    $row[] = utf8_encode($RFP['estado_civil']);
    $output['aaData'][] = $row;
}

$output['iTotalRecords'] = $iTotal;
$output['iTotalDisplayRecords'] = $iTotal;

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
