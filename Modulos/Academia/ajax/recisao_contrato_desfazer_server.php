<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');
require_once(__DIR__ . '/../../../util/excel/exportaExcel.php');

$idPlano = $_GET['idplano'];
$where = "";

$sQuery1 = "set dateformat dmy;
update sf_vendas_planos set 
dt_cancelamento = null,
usuario_canc = null,
obs_cancelamento = ''
where id_plano = " . $idPlano;
odbc_exec($con, $sQuery1);

$sQuery1 = "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
select 'sf_vendas_planos', id_plano, '" . $_SESSION["login_usuario"] . "', 'C', 'DESFAZER CANCELAMENTO DE PLANO (' + (select descricao from sf_produtos t where t.conta_produto = ft.id_prod_plano) + ')', GETDATE(), favorecido 
from sf_vendas_planos ft where id_plano = " . $idPlano;
odbc_exec($con, $sQuery1);

odbc_close($con);