<?php

require_once(__DIR__ . '/../../../Connections/configini.php');

$aColumns = array('id', 'nome', 'tipo', 'valor');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = " WHERE id_cliente = 0 ";
$sWhere = "";
$imprimir = 0;
$sLimit = 20;
$sOrder = " ORDER BY tipo, id_usuario desc ";

if (is_numeric($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['cli']) && $_GET['cli'] > 0) {
    $sWhereX = " WHERE id_cliente = " . $_GET['cli'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . " " . $_GET['sSortDir_' . $i] . ", ";
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY id desc";
    }
}

if ($_GET['Search'] != "") {
    $sWhere = " AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['Search']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
        $sWhere .= $aColumns[$i] . " AND LIKE '%" . utf8_decode($_GET['sSearch_' . $i]) . "%' ";
    }
}

$sQuery = "SELECT COUNT(*) total from sf_comissao_cliente
inner join sf_usuarios on sf_usuarios.id_usuario = sf_comissao_cliente.id_usuario " . $sWhereX . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total from sf_comissao_cliente
inner join sf_usuarios on sf_usuarios.id_usuario = sf_comissao_cliente.id_usuario " . $sWhereX;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SET DATEFORMAT DMY;SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row, 
id, nome, tipo, valor, sf_usuarios.id_usuario, tipo_valor, tipo_valor_desc 
from sf_comissao_cliente inner join sf_usuarios on sf_usuarios.id_usuario = sf_comissao_cliente.id_usuario " . $sWhereX . $sWhere . 
") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder;
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();    
    $row[] = "<a href=\"javascript:void(0)\" onclick=\"editLinhaComissao(" . $aRow[$aColumns[0]] . "," . $aRow["id_usuario"] . "," . $aRow[$aColumns[2]] . ",'" . escreverNumero($aRow[$aColumns[3]]) . "'," . $aRow["tipo_valor"] . "," . $aRow["tipo_valor_desc"] . ");\"><div id=\"formPQ\" title=\"" . formatNameCompact(strtoupper(utf8_encode($aRow[$aColumns[1]]))) . "\">" . formatNameCompact(strtoupper(utf8_encode($aRow[$aColumns[1]]))) . "</div></a>";
    $row[] = getTypeComissao($aRow[$aColumns[2]], false);
    $row[] = "[" . ($aRow["tipo_valor_desc"] == "1" ? "S" : "N") . "] " . escreverNumero($aRow[$aColumns[3]]) . " (" . ($aRow["tipo_valor"] == "1" ? "$" : "%") . ")";
    $row[] = "<center><input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\" onclick=\"delLinhaComissao(" . $aRow[$aColumns[0]] . ");\"></center>";
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);