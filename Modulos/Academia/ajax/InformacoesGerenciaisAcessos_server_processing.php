<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');
require_once(__DIR__ . '/../../../util/excel/exportaExcel.php');

$dados = array();

if (isset($_GET['txtTpUser']) && $_GET['txtTpUser'] !== "T") {
    $where = " and B.tipo = '" . $_GET['txtTpUser'] . "'";
}
if (isset($_GET['txtDtHrInicial'])) {
    $where .= " and A.data_acesso  > " . valoresData2(str_replace("_", "/", $_GET['txtDtHrInicial']));
}

if (isset($_GET['txtDtHrFinal'])) {
    $where .= " and A.data_acesso  < " . valoresData2(str_replace("_", "/", $_GET['txtDtHrFinal']));
}

if (isset($_GET['txtTpAcesso']) && $_GET['txtTpAcesso'] !== "T") {
    $where .= " and A.status_acesso = '" . $_GET['txtTpAcesso'] . "'";
}

if (isset($_GET['txtAmbiente'])) {
    $where .= " and A.ambiente in(" . $_GET['txtAmbiente'] . ")";
}

if (isset($_GET['txtCatraca'])) {
    $where .= " and A.catraca in(" . $_GET['txtCatraca'] . ")";
}

if (isset($_GET['txtGrupo'])) {
    $where .= " and B.grupo_pessoa in(" . $_GET['txtGrupo'] . ")";
}

if (isset($_GET['txtIdUser'])) {
    $where .= " and A.id_fonecedor = " . $_GET['txtIdUser'];
}

if (isset($_GET['txtFilial'])) {
    $where .= " and B.empresa  = " . $_GET['txtFilial'];
}

$query = "set dateformat dmy;";
if ($_GET['tipo'] === 'H') {
    $query .= "select DATEPART(HOUR, cast(data_acesso as time)) hora_acesso, count(id_acesso) total from sf_acessos A
    left join sf_fornecedores_despesas B on A.id_fonecedor = B.id_fornecedores_despesas
    where id_acesso > 0  " . $where . " group by DATEPART(HOUR, cast(data_acesso as time))";
    for ($i = 0; $i < 24; $i++) {
        $arr = array();
        $arr['hora'] = str_pad($i, 2, 0, STR_PAD_LEFT) . ":00";
        $arr['valor'] = 0;
        $dados[$i] = $arr;
    }
} else if ($_GET['tipo'] === 'T') {
    $query .= "select isnull(sum(x.cmm),0) as Manha,isnull(sum(x.cmt),0) as Tarde,isnull(sum(x.cmn),0) as Noite from(
    select case when isnull(DATEPART(HOUR,data_acesso),0) between 0 and 12 then 1 else 0 end cmm,
    case when isnull(DATEPART(HOUR,data_acesso),0) between 13 and 18 then 1 else 0 end cmt,
    case when isnull(DATEPART(HOUR,data_acesso),0) between 19 and 23 then 1 else 0 end cmn from sf_acessos A
    left join sf_fornecedores_despesas B on A.id_fonecedor = B.id_fornecedores_despesas
    where id_acesso > 0 " . $where . " ) as x";
} else if ($_GET['tipo'] === 'S') {
    $query .= "select status_acesso, count(id_acesso) total from sf_acessos A
    left join sf_fornecedores_despesas B on A.id_fonecedor = B.id_fornecedores_despesas
    where id_acesso > 0 " . $where . " group by status_acesso";
}
//echo $query;exit;
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    if ($_GET['tipo'] == 'H') {
        $dados[$RFP['hora_acesso']]['valor'] = $RFP['total'];
    } else if ($_GET['tipo'] == 'T') {
        $dados[] = array("turno" => "Manha", "acesso" => $RFP['Manha']);
        $dados[] = array("turno" => "Tarde", "acesso" => $RFP['Tarde']);
        $dados[] = array("turno" => "Noite", "acesso" => $RFP['Noite']);
    } else if ($_GET['tipo'] == 'S') {
        $dados[] = array("status" => trataAcesso($RFP['status_acesso']), "acesso" => $RFP['total']);
    }
}

echo json_encode($dados);
odbc_close($con);