<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');
require_once(__DIR__ . '/../../../util/excel/exportaExcel.php');

$aColumns = array('id_ferias', 'meses', 'dias');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY " . $aColumns[0] . " asc ";
$sOrder2 = " ORDER BY " . $aColumns[0] . " asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) < 2) {
                $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 0) . " " . $_GET['sSortDir_' . $i] . ", ";
                $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 1) . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY " . $aColumns[0] . " asc";
        $sOrder2 = " ORDER BY " . $aColumns[0] . " asc";
    }
}

if ($_GET['Search'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= prepareCollum($aColumns[$i], 0) . " LIKE '%" . utf8_decode(str_replace("'", "", $_GET['Search'])) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,*
            from sf_ferias where " . $aColumns[0] . " > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . $sOrder2;

$cur = odbc_exec($con, $sQuery1);

$sQuery = "SELECT COUNT(*) total from sf_ferias where " . $aColumns[0] . " > 0 $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total from sf_ferias where " . $aColumns[0] . " > 0 $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[1]]) . "'>" . utf8_encode($aRow[$aColumns[1]]) . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[2]]) . "'>" . utf8_encode($aRow[$aColumns[2]]) . "</div>";
    if ($imprimir == 0) {
        $row[] = "<center><a title='Excluir'><input name='' type='image' onClick='RemoverItem(" . $aRow[$aColumns[0]] . ")' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Excluir'></a>
        <a title='Atualizar Planos'><input name='' type='image' onClick='AtualizaPlanos(" . $aRow[$aColumns[0]] . ")' src='../../img/refresh.PNG' width='18' height='18' value='Atualizar'></a></center>";
    }
    $output['aaData'][] = $row;
}
echo json_encode($output);
odbc_close($con);
