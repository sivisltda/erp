<?php include "../../Connections/configini.php"; ?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<style>
    body { font-family: sans-serif; }
</style>
<body onload="iniciaCongelamentoForm();">
    <div class="row-fluid">
        <form>
            <div class="frmhead">
                <div class="frmtext">Adicionar Congelamento</div>
                <div class="frmicon" onClick="parent.FecharBoxCliente()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <div style="width:28%; float:left;">
                    <span>Plano:</span>
                    <input type="hidden" id="txtIdPlano"/>
                    <input type="text" id="txtPlano" disabled/>
                </div>
                <div style="width:16%; float:left; margin-left:1%">
                    <span>Dias Restantes:</span>
                    <input type="text" id="txtDias" class="inputCenter" value="0" disabled/>
                </div>
                <div style="width:16%; float:right; margin-left:1%">
                    <span>Nova data término:</span>
                    <input type="text" id="txtDtTermino" class="inputCenter" disabled/>
                </div>
                <div style="clear:both; height:5px"></div>
                <?php if ($ckb_aca_fer_ == 1) { ?>
                    <div style="width:28%; float:left;">
                        <span>Iníciar Em:</span>
                        <input type="text" id="txtDtInicio" class="datepicker inputCenter"/>
                    </div>
                    <div style="width:16%; float:left; margin-left:1%;"> 
                        <span> Qtd. dias: </span>
                        <input type="text" class="input-xlarge" id="spinner" maxlength="2"/>
                    </div> 
                    <div style="width:43%; float:left;margin-left: 1%;">
                        <span>Motivo:</span>
                        <select id="txtMotivo" class="select" style="width:100%">
                            <option value="null">Selecione</option>
                            <option value="FÉRIAS">FÉRIAS</option>
                            <option value="AFASTAMENTO">AFASTAMENTO</option>
                            <option value="MUDANÇA">MUDANÇA</option>
                            <option value="VIAGEM">VIAGEM</option>
                            <option value="SAÚDE">SAÚDE</option>
                            <option value="OUTRO">OUTRO</option>
                        </select>
                    </div>
                    <span style="width:10%; display:block; float:left; margin-left:1%;">
                        <span style="width:100%; display:block;">&nbsp;</span>
                        <span style="width:100%; display:block; text-align:right;">	
                            <button class="btn green" id="btnAddCongelamento" type="button" style="width:100%;">Adicionar</button>
                        </span> 
                    </span>
                <?php } ?>
                <div style="clear:both; height:5px"></div>
                <div style="width:100%; margin:10px 0 0">
                    <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Histórico</span>
                    <hr style="margin:2px 0; border-top:1px solid #ddd">
                </div>
                <div class="body" style=" margin-left: 0px; padding: 0px 0px 0px 0px;width: 100%">
                    <div class="body" style="margin-left:0; padding:0;overflow-y: scroll;height: <?php echo ($ckb_aca_fer_ == 1 ? "160" : "205") ?>px;">
                        <div class="content" style="min-height:149px; margin-top:0;">
                            <table id="tblCongHist" class="table" cellpadding="0" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th style="width:5%;">Dias</th>
                                        <th style="width:20%">Período</th>
                                        <th style="width:30%">Motivo</th>
                                        <th style="width:17%">Data inclusão</th>
                                        <th style="width:20%">Criado/Cancelado por</th>
                                        <th style="width:8%">Ação</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div style="clear:both; height:5px"></div>
                </div>
            </div>
            <div class="spanBNT" style="left: 0px;margin: 0px;">
                <div class="toolbar bottom tar">
                    <div class="data-fluid" style="overflow:hidden;margin-top: 4px;">
                        <div class="btn-group">
                            <button class="btn yellow" title="Cancelar" onClick="parent.FecharBoxCliente()"><span class="ico-reply"></span> Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>                
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type="text/javascript" src="../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type='text/javascript' src='js/ClientesCongelamentoForm.js'></script>
    <?php odbc_close($con); ?>
</body>