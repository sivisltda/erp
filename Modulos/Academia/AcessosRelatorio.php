<?php
include "../../Connections/configini.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery/ui.css" />
        <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-timepicker-addon.css" />
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <style type="text/css">
            #s2id_mscAmbiente ul{
                min-height: 29px;
            }
            #tblResumoAcessos_wrapper table thead{
                display:none;
            }
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"><span class="ico-arrow-right"></span></div>
                        <h1>Gestão de Acessos<small>Relatorio de Acessos</small></h1>
                    </div>
                    <div id="parametros_busca" class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="width:8%; float:left;">
                                    <span>Usuário:</span>
                                    <select id="txtTpUsuario">
                                        <option value="T">TODOS</option>
                                        <option value="C">Cliente</option>
                                        <option value="E">Funcionário</option>
                                        <option value="P">Prospects</option>
                                    </select>
                                </div>
                                <div style="width:11.8%; float:left;margin-left:0.5%">
                                    <span>Nome</span>
                                    <input name="txtIdUsuario" id="txtIdUsuario" type="hidden"/>
                                    <input name="txtDesc" id="txtDesc" type="text" style="height:31px" size="10"/>
                                </div>
                                <div style="width:9%; float:left;margin-left:0.5%">
                                    <span>Data e Hora Inicial:</span>
                                    <input type="text" style="width:100%" id="txtDataIni" />
                                </div>
                                <div style="width:9%; float:left;margin-left:0.5%">
                                    <span>Data e Hora Final:</span>
                                    <input type="text" style="width:100%" id="txtDataFim" />
                                </div>
                                <div style="width:10%; float:left;margin-left:0.5%">
                                    <span>Tipo:</span>
                                    <select id="txtTipoAcesso">
                                        <option value="T">TODOS</option>
                                        <option value="AP">Acesso permitido</option>
                                        <option value="ANP">Acesso não permitido</option>
                                        <option value="FA">Fora Ambiente</option>
                                        <option value="FH">Fora Horário</option>
                                        <option value="LB">Liberação Manual</option>
                                        <option value="SM">Sem Matrícula</option>
                                        <option value="BL">Bloqueado</option>
                                        <option value="CV">Débito em Carteira</option>
                                        <option value="MV">Mensalidade em Aberto</option>
                                        <option value="CVD">Carteira Vencida</option>                                        
                                    </select>
                                </div>
                                <div style="float:left; width:15%;margin-left:0.5%">
                                    <span>Ambiente:</span>
                                    <select id="mscAmbiente" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                        <?php
                                        $cur = odbc_exec($con, "SELECT * from sf_ambientes") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['id_ambiente'] ?>"><?php echo utf8_encode($RFP['nome_ambiente']) ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div style="float:left; width:15%;margin-left:0.5%">
                                    <span>Catraca:</span>
                                    <select id="mscCatraca" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                        <?php $cur = odbc_exec($con, "SELECT * from sf_catracas") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['id_catraca'] ?>"><?php echo utf8_encode($RFP['nome_catraca']) ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div style="float:left; width:15%;margin-left:0.5%">
                                    <span>Grupo de Clientes:</span>
                                    <select id="mscGrupoAce" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                        <?php $cur = odbc_exec($con, "select id_grupo,descricao_grupo from dbo.sf_grupo_cliente
                                            where tipo_grupo = 'C' and inativo_grupo = 0") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['id_grupo'] ?>"><?php echo utf8_encode($RFP['descricao_grupo']) ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <?php if ($clb_pagamento_credencial_ > 0) { ?>
                                <div style="width:8%; float:left;margin-left:0.5%">
                                    <span>Credencial:</span>
                                    <select id="txtCredencial">
                                        <option value="null">TODOS</option>
                                        <option value="1">Com Pagamento</option>
                                        <option value="2">Sem Pagamento</option>
                                    </select>
                                </div> 
                                <?php } ?>
                                <div style="margin-top: 15px;float:left;margin-left:0.5%">
                                    <div style="float:left">
                                        <button id="btnfind" class="button button-turquoise btn-primary" type="button" title="Buscar"><span class="ico-search icon-white"></span></button>
                                        <button id="btnPrint" class="button button-blue btn-primary" type="button" title="Exportar Excel"><span class="ico-download-3"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="block">
                                <div class="boxhead">
                                    <div class="boxtext">Resumo</div>
                                </div>
                                <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblResumoAcessos">
                                    <thead hidden>
                                        <tr>
                                            <th width="6%"  style="text-align: center;">Matricula</th>
                                            <th width="40%" style="text-align: center;">Nome</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                                <br/>
                                <div style="clear:both"></div>
                                <div class="boxhead">
                                    <div class="boxtext">Acessos</div>
                                </div>
                                <div class="data-fluid">
                                    <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblAcessos">
                                        <thead>
                                            <tr>
                                                <th width="4%"  style="text-align: center;">Mat.</th>
                                                <th width="22%" style="text-align: center;">Nome</th>
                                                <th width="8%" style="text-align: center;">Data/Hora</th>
                                                <th width="12%" style="text-align: center;">Tipo Acesso</th>
                                                <th width="18%" style="text-align: center;">Motivo</th>
                                                <th width="12%" style="text-align: center;">Ambiente</th>
                                                <th width="12%" style="text-align: center;">Catraca</th>
                                                <th width="12%" style="text-align: center;">Usuário</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/multiselect/jquery.multi-select.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/moment.min.js"></script>
        <script type="text/javascript" src="../../js/jquery-ui-timepicker-addon.js"></script>
        <script type="text/javascript" src="../../js/jquery-ui-timepicker-addon-i18n.min.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>              
        <script type="text/javascript" src="js/AcessosRelatorio.js"></script>          
    </body>
    <?php odbc_close($con); ?>
</html>