<?php include './../../Connections/configini.php'; ?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<title>Sivis Business</title>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
<link rel="stylesheet" type="text/css" href="../../css/main.css"/>
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
<div class="row-fluid">
    <div class="block title">
        <div class="boxhead" style="margin-bottom: 15px;">
            <div class="boxtext">Histórico de Agendamento
                <button style="border:none; float:right; width:25px;margin-right: 12px;margin-top: 7px; color:#FFFFFF; background-color:#333333" onClick="parent.FecharBox()" id="bntOK"><b>x</b></button></h2>                                                 
            </div>
        </div>
        <div style="clear:both"></div>
        <div style="margin:0 10px">
            <div class="data-fluid" style="height:355px; overflow:auto">
                <div>
                    <table width="100%" bordercolor="black" border="1" cellpadding="5" cellspacing="2">           
                        <thead>
                            <tr class="txtPq">
                                <th width="20%" bgcolor="#e9e9e9" align="left" >Data:</th>                        
                                <th width="10%" bgcolor="#e9e9e9" align="left" >Usuário:</th>
                                <th width="8%" bgcolor="#e9e9e9" align="left" >Ação:</th>
                                <th width="62%" bgcolor="#e9e9e9" align="left" >Observação:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($_GET['id'] != '') {
                                $cur = odbc_exec($con, "select * from sf_turnos_estudio_hist where id_fornecedor = " . $_GET['id'] . " order by data_lanc desc");
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                <tr>
                                    <td><?php echo escreverDataHora($RFP['data_lanc']);?></td>
                                    <td><?php echo utf8_encode($RFP['usuario']); ?></td>                                        
                                    <td><?php echo ($RFP['tipo'] == "E" ? "Exclusão" : ($RFP['tipo'] == "I" ? "Inclusão" : "Alteração")); ?></td>                                        
                                    <td><?php echo utf8_encode($RFP['observacao']); ?></td>                                        
                                <?php }}?>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../js/util.js"></script> 
    </div>
<?php odbc_close($con); ?>
</div>