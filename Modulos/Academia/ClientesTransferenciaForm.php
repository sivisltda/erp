<?php include "../../Connections/configini.php"; ?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<style>
    .selected {
        background: #acbad4 !important;
    }
</style>
<body>
    <div class="row-fluid">
        <div id="inline1" style="height:160px">
            <div class="block title">
                <div class="head">
                    <div id="topBG" style="margin-bottom:20px">
                        <h2 class="head"><font color="#FFFFFF"> Transferir Plano </font>
                            <button style="border:none; float:right; width:25px; margin-right:25px; color:#FFFFFF; background-color: #333333" onClick="parent.FecharBox(1)" id="bntOK"><b>x</b></button>  
                        </h2>
                    </div>
                </div>
            </div>
            <div>
                <div class="block">
                    <form>
                        <div class="tabbable">
                            <input id="tpTela" type="hidden" value="<?php echo $_GET['tp']; ?>"/>
                            <div style="background:#F6F6F6; border:1px solid #DDD; padding: 0px 10px 0px 10px;">
                                <div style="width:60%; float:left;">
                                    <span>Transferir para:</span>
                                    <select id="txtDestinatario" class="select" style="width:100%">
                                        <option value="1">ALUNO NOVO</option>
                                    </select>
                                </div>
                                <div style="clear:both; height:7px"></div>
                                <div style="width:84%; float:left;">
                                    <span>Motivo:</span>
                                    <textarea name="txtMotivo" style="width:100%;min-height: 61px" rows="8"></textarea>
                                </div>
                                <span style="width:15%; display:block; float:left; margin-left:1%;">
                                    <span style="width:100%; display:block;">&nbsp;</span>
                                    <span style="width:100%; display:block; text-align:right;">	
                                        <button class="btn btn-success" id="btnConfirmar" type="button" style="width:100%;">Confirmar</button>
                                        <button class="btn btn-warning" id="btnCancelar" type="button" style="width:100%;">Cancelar</button>
                                    </span> 
                                </span> 
                                <div style="clear:both; height:12px"></div>
                            </div>    
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>    
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>                
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>  
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script src="js/CongelamentoForm.js" type="text/javascript"></script>
    <?php odbc_close($con); ?>
</body>
