<?php 

if (!isset($_GET["PathArq"]) && is_numeric($_GET['crt'])) {
    include("./../../Connections/configSivis.php");
    include("./../../Connections/funcoesAux.php");  
    $cur2 = odbc_exec($con2, "select *,(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = id_fornecedores_despesas) email
    from ca_contratos left join sf_fornecedores_despesas on id_fornecedores_despesas = id_cliente 
    left join tb_estados on estado_codigo = estado
    left join tb_cidades on cidade_codigo = cidade 
    where inativa = 0 and numero_contrato = '" . $_GET['crt'] . "'");
    while ($RFP = odbc_fetch_array($cur2)) {
        $cedente = utf8_encode($RFP['razao_social']);
        $cpf_cnpj = utf8_encode($RFP['cnpj']);
        $endereco = utf8_encode($RFP['endereco']) . " n°" . utf8_encode($RFP['numero']) . "," . utf8_encode($RFP['bairro']);
        $cidade_uf = utf8_encode($RFP['cidade_nome']) . " " . utf8_encode($RFP['estado_sigla']);
        $email = utf8_encode($RFP['email']);
        $site = utf8_encode($RFP['contato']);
        $contrato = utf8_encode($RFP['numero_contrato']);
        $hostname = utf8_encode($RFP['local']);
        $username = utf8_encode($RFP['login']);
        $password = utf8_encode($RFP['senha']);
        $database = utf8_encode($RFP['banco']);
    }
    odbc_close($con2);
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
} else if(!isset($_GET["PathArq"])) {
    session_start();
    include "./../../Connections/configini.php";
}
include "form/cupomPlanoForm.php"; 

$cur = odbc_exec($con, "select recibo_tipo, recibo_vias from sf_configuracao");
while ($RFP = odbc_fetch_array($cur)) {
    $tipo = ($RFP['recibo_tipo'] == 1 ? 1 : 0);
    $vias = $RFP['recibo_vias'];
    $modelo = $RFP['recibo_tipo'];        
}
?>      
<html> 
    <head>
        <title>SIVIS - <?php echo (strlen($login_user) > 0 ? "RECIBO DE ESTORNO" : "CUPOM NÃO FISCAL"); ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script> 
        <style type="text/css">
            body {
                font-size: 10px;
                font-family: Tahoma;
            }
            table {
                width: 100%;
                font-size: 10px;
                font-family: Tahoma;
            }
            .emp {
                font-size: 10px;
                font-weight: bold;
                text-align: center;
                padding: 5px 0px 5px 0px;
            }
            .end {
                text-align: center;
                padding: 5px 0px 5px 0px;
            }
            .doc {
                padding-top: 5px;
            }
            .lin {
                padding-top: 5px;
                border-bottom: 1px solid #000;
            }
            .spc {
                padding-bottom: 5px;
            }
            .grupo {
                padding: 3px 0px 3px 0px;
                background-color: #DDD;
                font-weight: bold;
            }
            .tit {
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                padding: 5px 0px 10px 0px;
            }
        </style>
    </head>
    <body>
        <?php for ($j = 0; $j < $vias; $j++) {
        for ($k = 0; $k < ($modelo == 2 ? 2 : 1); $k++) { ?>
        <div style="<?php echo ($modelo == 2 ? "float: left;margin-left: 5%;" : "") . ($tipo == 0 ? "" : "border-bottom: 1px dashed black; min-height: 500px; break-inside: avoid;"); ?>">
        <table cellspacing="0" cellpadding="0" style="<?php echo ($tipo == 0 ? "max-width: 302px;" : ""); ?>">
            <tr>
                <td colspan="6" class="tit"><?php echo $nome_fantasia; ?></td>
            </tr>       
            <tr>
                <td colspan="6" class="end">
                    <?php echo (strlen($cedente) > 0 ? $cedente. ($tipo == 0 ? "<br>" : "") : "");?>
                    <?php echo (strlen($endereco) > 0 ? $endereco. ($tipo == 0 ? "<br>" : "") : "");?>
                    <?php echo (strlen($cidade_uf) > 0 ? $cidade_uf : "");?>
                    <?php echo (strlen($cep) > 0 ? " - CEP: " . $cep : "");?>
                    <?php echo ($tipo == 0 ? "<br>" : "");?>
                    <?php echo (strlen($telefone) > 0 ? $telefone. ($tipo == 0 ? "<br>" : "") : "");?>
                    <?php echo (strlen($site) > 0 ? $site. ($tipo == 0 ? "<br>" : "") : "");?>
                </td>
            </tr>
            <tr>
                <td colspan="6" class="doc"><b>CNPJ: <?php echo $cpf_cnpj; ?></b><?php echo ($tipo == 0 ? "<br>" : "");?> I.E.: <?php echo $inscricao; ?></td>
            </tr>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <tr>
                <td colspan="3" class="doc"><b><?php echo $data_venda; ?></b></td>
                <td colspan="3" class="doc" style="text-align:right"><b>COD: <?php echo str_pad($id, 6, "0", STR_PAD_LEFT); ?></b></td>
            </tr>
            <?php if ($tipo == 0) { ?>
            <tr>
                <td colspan="6" class="tit"><?php echo (strlen($login_user) > 0 ? "RECIBO DE ESTORNO" : "CUPOM NÃO FISCAL"); ?></td>
            </tr>
            <?php } ?>
            <tr>
                <td>ITEM</td>
                <td style="text-align:center;padding-left: 1px;">CÓDIGO</td>
                <td style="padding-left: 1px;">DESCRIÇÃO</td>
                <td style="text-align:center;padding-left: 1px;">QTD</td>
                <td style="text-align:right;padding-left: 1px;">VALOR</td>
                <td style="text-align:right;padding-left: 2px;">TOTAL</td>
            </tr>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <?php
            if ($PegaURL != "") {                
                if(count($arrProduto) > 0) {
                    if ($tipo == 0) { ?>
                    <tr>
                        <td colspan="6" class="grupo"> PRODUTO </td>
                    </tr>            
                    <?php } for ($i = 0; $i < count($arrProduto); $i++) { ?>
                    <tr>
                        <td style="vertical-align: top;vertical-align: top;"><?php echo str_pad($loop, 3, "0", STR_PAD_LEFT); ?></td>
                        <td style="text-align:center;vertical-align: top;"><?php echo str_pad($arrProduto[$i][0], 6, "0", STR_PAD_LEFT); ?></td>                        
                        <td style="vertical-align: top;"><?php echo $arrProduto[$i][1]; ?></td>
                        <td style="text-align:center;vertical-align: top;"><?php echo $arrProduto[$i][2]; ?> X</td>                        
                        <td style="text-align:right;vertical-align: top;"><?php echo $arrProduto[$i][3]; ?></td>                        
                        <td style="text-align:right;vertical-align: top;"><?php echo $arrProduto[$i][4]; ?></td>
                    </tr>
                    <?php
                        $loop++;
                    }
                } if(count($arrPlano) > 0) {
                    if ($tipo == 0) { ?>
                    <tr>
                        <td colspan="6" class="grupo"> PLANO </td>
                    </tr>
                    <?php } for ($i = 0; $i < count($arrPlano); $i++) { ?>
                    <tr>
                        <td style="vertical-align: top;vertical-align: top;"><?php echo str_pad($loop, 3, "0", STR_PAD_LEFT); ?></td>
                        <td style="text-align:center;vertical-align: top;"><?php echo str_pad($arrPlano[$i][0], 6, "0", STR_PAD_LEFT); ?></td>
                        <td style="vertical-align: top;"><?php echo $arrPlano[$i][1]; ?></td>
                        <td style="text-align:center;vertical-align: top;"><?php echo $arrPlano[$i][2]; ?> X</td>                        
                        <td style="text-align:right;vertical-align: top;"><?php echo $arrPlano[$i][3]; ?></td>                        
                        <td style="text-align:right;vertical-align: top;"><?php echo $arrPlano[$i][4]; ?></td>
                    </tr>
                    <?php                    
                        $loop++;                                                
                    }                    
                } if(count($arrOutro) > 0) {
                    if ($tipo == 0) { ?>
                    <tr>
                        <td colspan="6" class="grupo"> OUTROS </td>
                    </tr>      
                    <?php } for ($i = 0; $i < count($arrOutro); $i++) { ?>
                    <tr>
                        <td style="vertical-align: top;vertical-align: top;"><?php echo str_pad($loop, 3, "0", STR_PAD_LEFT); ?></td>
                        <td style="text-align:center;vertical-align: top;"><?php echo str_pad($arrOutro[$i][0], 6, "0", STR_PAD_LEFT); ?></td>
                        <td style="vertical-align: top;"><?php echo $arrOutro[$i][1]; ?></td>
                        <td style="text-align:center;vertical-align: top;"><?php echo $arrOutro[$i][2]; ?> X</td>
                        <td style="text-align:right;vertical-align: top;"><?php echo $arrOutro[$i][3]; ?></td>
                        <td style="text-align:right;vertical-align: top;"><?php echo $arrOutro[$i][4]; ?></td>
                    </tr>
                    <?php
                        $loop++;
                    }
                } if(count($arrServicos) > 0) { 
                    if ($tipo == 0) { ?>
                    <tr>
                        <td colspan="6" class="grupo"> SERVIÇO </td>
                    </tr>  
                    <?php } for ($i = 0; $i < count($arrServicos); $i++) { ?>
                    <tr>
                        <td style="vertical-align: top;vertical-align: top;"><?php echo str_pad($loop, 3, "0", STR_PAD_LEFT); ?></td>
                        <td style="text-align:center;vertical-align: top;"><?php echo str_pad($arrServicos[$i][0], 6, "0", STR_PAD_LEFT); ?></td>
                        <td style="vertical-align: top;"><?php echo $arrServicos[$i][1]; ?></td>
                        <td style="text-align:center;vertical-align: top;"><?php echo $arrServicos[$i][2]; ?> X</td>
                        <td style="text-align:right;vertical-align: top;"><?php echo $arrServicos[$i][3]; ?></td>
                        <td style="text-align:right;vertical-align: top;"><?php echo $arrServicos[$i][4]; ?></td>
                    </tr>
                    <?php
                        $loop++;
                    }
                }
            } ?>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <tr>
                <td colspan="4" class="doc"><b>Parcelamento:</b></td>
                <td colspan="2" class="doc" style="text-align:right"><b></b></td>
            </tr>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <tr>
                <td colspan="2" class="doc"><b>Data:</b></td>
                <td colspan="2" class="doc"><b>Tipo:</b></td>
                <td style="text-align:right;padding-left: 2px;" colspan="2" class="doc"><b>Valor:</b></td>                
            </tr>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <?php if(count($arrFinanceiro) > 0) {
                for ($i = 0; $i < count($arrFinanceiro); $i++) { ?>
                <tr>
                    <td colspan="2" class="doc"><?php echo $arrFinanceiro[$i][0]; ?></td>
                    <td colspan="2" class="doc"><?php echo $arrFinanceiro[$i][1]; ?></td>
                    <td style="text-align:right;padding-left: 2px;" colspan="2" class="doc"><?php echo $lang['prefix'] . $arrFinanceiro[$i][2]; ?></td>                
                </tr>            
            <?php }
            } if(count($arrFrete) > 0) { ?>
                <tr>
                    <td colspan="6" class="lin"></td>
                </tr>                
                <tr>
                    <td colspan="4" class="doc"><b>Frete a Pagar:</b></td>
                    <td colspan="2" class="doc" style="text-align:right"><b></b></td>
                </tr>  
                <tr>
                    <td colspan="6" class="lin"></td>
                </tr>    
            <?php for ($i = 0; $i < count($arrFrete); $i++) { ?>
                <tr>
                    <td colspan="4"><?php echo $arrFrete[$i][0]; ?></td>
                    <td colspan="2"><?php echo $lang['prefix'] . $arrFrete[$i][1]; ?></td>                    
                </tr>                    
            <?php }
            }?>            
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <?php if(strlen($login_user) > 0) { ?>
            <tr>
                <td colspan="3" class="doc">Data de Estorno:</td>
                <td colspan="3" class="doc" style="text-align:right"><?php echo $dt_estorno; ?></td>
            </tr>
            <tr>
                <td colspan="3" class="doc">Estornado por:</td>
                <td colspan="3" class="doc" style="text-align:right"><?php echo $login_user; ?></td>
            </tr>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>            
            <?php } ?>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <tr>
                <td colspan="4" class="doc"><b>SUB TOTAL:</b></td>
                <td colspan="2" class="doc" style="text-align:right"><b><?php echo escreverNumero($sub_total); ?></b></td>
            </tr>
            <?php if($desc_valor > 0) { ?>
            <tr>
                <td colspan="4" class="doc"><b>DESCONTO:<?php echo ($desconto != "" ? " (" . $desconto . ")" : ""); ?></b></td>
                <td colspan="2" class="doc" style="text-align:right"><b><?php echo escreverNumero($desc_valor); ?></b></td>
            </tr>
            <?php } if($multa_valor > 0) { ?>
            <tr>
                <td colspan="4" class="doc"><b>ACRÉSCIMO:</b></td>
                <td colspan="2" class="doc" style="text-align:right"><b><?php echo escreverNumero($multa_valor); ?></b></td>
            </tr>
            <?php } if($frete_total > 0) { ?>
            <tr>
                <td colspan="4" class="doc"><b>FRETE:</b></td>
                <td colspan="2" class="doc" style="text-align:right"><b><?php echo escreverNumero($frete_total); ?></b></td>
            </tr>
            <?php } ?>
            <tr>
                <td colspan="4" class="doc"><b>TOTAL:</b></td>
                <td colspan="2" class="doc" style="text-align:right"><b><?php echo escreverNumero(($sub_total + $frete_total + $multa_valor - $desc_valor)); ?></b></td>
            </tr>  
            <?php if ($valorPagoDinheiro > 0) { ?>
                <tr>
                    <td colspan="4" class="doc"><b>PAGO:</b></td>
                    <td colspan="2" class="doc" style="text-align:right"><b><?php echo escreverNumero($valorPagoDinheiro); ?></b></td>
                </tr>  
                <tr>
                    <td colspan="4" class="doc"><b>TROCO:</b></td>
                    <td colspan="2" class="doc" style="text-align:right"><b><?php echo escreverNumero(($valorPagoDinheiro - ($sub_total + $frete_total + $multa_valor - $desc_valor))); ?></b></td>
                </tr>  
            <?php } if ($comentario != "") { ?>
                <tr>
                    <td colspan="6" class="lin"></td>
                </tr>                
                <tr>
                    <td colspan="4" class="doc"><b>Comentários:</b></td>
                    <td colspan="2" class="doc" style="text-align:right"><b></b></td>
                </tr>  
                <tr>
                    <td colspan="6" class="lin"></td>
                </tr>                
                <tr>
                    <td colspan="6"><?php echo $comentario; ?></td>
                </tr>
            <?php } if(count($arrCliente) > 0) { ?>
                <tr>
                    <td colspan="6" class="lin"></td>
                </tr> 
                <tr>
                    <td colspan="4" class="doc"><b>Cliente:</b></td>
                    <td colspan="2" class="doc" style="text-align:right"><b></b></td>
                </tr>  
                <tr>
                    <td colspan="6" style="text-align:center;">
                        <?php echo $arrCliente[0][0]; ?>                                                                     
                    </td>
                </tr>
            <?php } ?>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <tr>
                <td>Operador:</td>
                <td colspan="5"> <?php echo $operador; ?></td>
            </tr>
            <?php if ($cod_pedido !== "0") { ?>
                <tr>
                    <td colspan="6" class="tit">SENHA: <?php echo $cod_pedido; ?></td>
                </tr>
            <?php } ?>
        </table>
        </div>            
        <?php }} ?>
    </body>
    <script type="text/javascript" src="../../js/util.js"></script>     
    <?php if(!isset($_GET["PathArq"])) { ?>
        <script type="text/javascript">
            $(window).load(function () {
                window.print();
                <?php if ($cod_pedido !== "0") { ?>
                    setTimeout(function () {
                        window.print();
                    }, 100);
                <?php } ?>                
                /*setTimeout(function () {
                    window.close();
                }, 500);*/
            });
        </script>    
    <?php } ?>
</html>