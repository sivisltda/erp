<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include "../../Connections/configini.php";

$pessoa = $_REQUEST['txtPessoa'];
$local = array();

if (is_numeric($pessoa) && $pessoa > 0) {
    $sql = "select distinct vp.id_plano, p.conta_produto, p.descricao, v.placa 
    from sf_vendas_planos vp 
    inner join sf_produtos p on vp.id_prod_plano = p.conta_produto
    left join sf_fornecedores_despesas_veiculo v on v.id = vp.id_veiculo
    where dt_cancelamento is null and p.inativa = 0   
    and favorecido in (" . $pessoa . ") order by descricao";
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('id_plano' => utf8_encode($RFP['id_plano']), 
        'descricao' => (strlen($RFP['placa']) > 0 ? $RFP['placa'] . " - " : "") . utf8_encode($RFP['descricao']));
    }
}

echo(json_encode($local));
odbc_close($con);
