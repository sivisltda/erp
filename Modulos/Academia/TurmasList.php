<?php
    include "../../Connections/configini.php";
    if($ckb_aca_turmas_ == 0){
        header('Location: ../../Index.php');
    }
    $mdl_esd_ = returnPart($_SESSION["modulos"],10);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Sivis Business</title>
    <link rel="icon" type="image/ico" href="../../favicon.ico"/>
    <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
    <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
    <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    <style>
        #tblConvenios td {
            padding: 5px;
            line-height: 16px;
        }
    </style>
</head>
<body>
    <div id="loader"><img src="../../img/loader.gif"/></div>
    <div class="wrapper">
    <?php include("../../menuLateral.php"); ?>
    <div class="body">
        <?php include("../../top.php"); ?>
        <div class="content">
            <div class="page-header">
                <div class="icon"> <span class="ico-arrow-right"></span></div>
                <h1><?php echo ($mdl_clb_ == 1 ? "Configuração" : "Academia")?><small>Turmas</small></h1>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="boxfilter block">
                        <div style="margin-top: 15px;float:left;">
                            <div style="float:left">
                                <form method="POST">
                                    <button class="button button-green btn-primary" type="button" onclick="AbrirBox(0)"><span class="ico-file-4 icon-white"></span></button>
                                    <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir('I')" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                    <button id="btnExcel" class="button button-blue btn-primary" type="button" onclick="imprimir('E')" title="Exportar Excel"><span class="ico-download-3"></span></button>
                                    <?php if ($mdl_esd_ > 0) { ?>
                                    <button id="btnMapa" class="button button-blue btn-primary" type="button" onclick="AbrirBox(-1)" title="Mapa de Turmas">Mapa</button>
                                    <?php } ?>
                                    <button id="btnLista" class="button button-blue btn-primary" type="button" onclick="imprimirListaAlunos('I')" title="Imprimir"><span class="ico-print icon-white"></span> Lista</button>                                    
                                    <button id="btnLista" class="button button-blue btn-primary" type="button" onclick="imprimirListaAlunos('E')" title="Exportar Excel"><span class="ico-download-3"></span> Lista</button>                                    
                                    <button id="btnChamada" class="button button-blue btn-primary" type="button" onclick="imprimirChamadas()" title="Imprimir">Chamada</button>                                    
                                </form>
                            </div>
                        </div>
                        <div style="float: right;width: 60%;">
                            <div style="float: left;width: 20%;">
                                <span>Horário:</span>
                                <select name="txtHorario" id="txtHorario" class="select input-medium" style="width: 100%;">
                                    <option value="">Selecione</option>
                                </select>
                            </div>
                            <div style="float: left;width: 20%;margin-left: 1%;">
                                <span>Professor:</span>
                                <select name="txtProfessor" id="txtProfessor" class="select input-medium" style="width: 100%;">
                                    <option value="">Selecione</option>
                                </select>
                            </div>
                            <div style="float: left;width: 20%;margin-left: 1%;">
                                <span>Plano:</span>
                                <select name="txtPlanos" id="txtPlanos" class="select input-medium" style="width:100%">
                                    <option value="">Selecione:</option>
                                </select>
                            </div>
                            <div style="float: left;width: 11%;margin-left: 1%;">
                                <span>Status:</span>
                                <select name="txtStatus" id="txtStatus" class="select input-medium" style="width:100%">
                                    <option value="">Selecione:</option>
                                    <option value="0">Ativo</option>
                                    <option value="1">Inativo</option>
                                </select>
                            </div>
                            <div style="float:left;width: calc(21% - 17px);margin-left: 1%;">
                                <div width="100%">Busca:</div>
                                <input id="txtBusca" maxlength="100" type="text" onkeypress="TeclaKey(event)" value="" title=""/>
                            </div>                            
                            <div style="float:left; margin-top: 15px; width: 17px;margin-left:1%;">
                                <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear:both"></div>
            <div class="boxhead">
                <div class="boxtext">Turmas</div>
            </div>
            <div class="boxtable">
                <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tblTurmas">
                    <thead>
                        <tr>
                            <th width="2%"></th>
                            <th width="26%">Nome da Turma</th>
                            <th width="8%">Max Alu.</th>
                            <th width="8%">Vagas</th>
                            <th width="8%">Matriculados</th>
                            <th width="8%">Inicio Em</th>
                            <th width="13%">Horário</th>                            
                            <th width="15%">Professor</th>
                            <th width="13%">Ambiente</th>
                            <th width="10%">Status</th>
                            <th width="5%"><center>Ação</center></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                <div style="clear:both"></div>
            </div>
        </div>
    </div>
</div>
<div class="dialog" id="source" title="Source"></div>
<script type="text/javascript" src="../../fancyapps/source/jquery.fancybox.js?v=2.1.4"></script>
<script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
<script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
<script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
<script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
<script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
<script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
<script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
<script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="../../js/actions.js"></script>
<script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>
<script type="text/javascript" src="../../js/graficos.js"></script>
<script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
<script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
<script type="text/javascript" src="../../js/util.js"></script>
<script type="text/javascript" src="js/TurmasList.js"></script>
<?php odbc_close($con); ?>
</body>
</html>
