<?php
include "../../Connections/configini.php";
include "form/CredenciaisFormServer.php";
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
<style>
    .selected {
        background: #acbad4 !important;
    }
</style>
<body>
    <div class="row-fluid">
        <form action="CredenciaisForm.php" name="frmCredenciais" id="frmCredenciais" method="POST">
            <div class="frmhead">
                <div class="frmtext">Credenciais</div>
                <div class="frmicon" onClick="parent.FecharBox(1)">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input id="txtId" name="txtId" type="hidden" value="<?php echo $id; ?>"/>
                <div style="float:left; width:100%">
                    <span>Descrição:</span>
                    <input id="txtDescricao" name="txtDescricao" <?php echo $disabled; ?> type="text" value="<?php echo $descricao; ?>" class="input-medium" style="width:100%"/>
                </div>
                <div style="float:left; width:100%; margin-top:5px">
                    <span>Horário:</span>
                    <select id="txtHorario" name="txtHorario" <?php echo $disabled; ?> class="select" style="width:100%">
                        <option value="null">Selecione</option>
                        <?php
                        $cur = odbc_exec($con, "select cod_turno,nome_turno from sf_turnos where cod_turno > 0 order by nome_turno ") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) {
                            ?>
                            <option value="<?php echo $RFP["cod_turno"] ?>"<?php
                            if (!(strcmp($RFP["cod_turno"], $horario))) {
                                echo "SELECTED";
                            }
                            ?>><?php echo utf8_encode($RFP["nome_turno"]) ?></option>
                                <?php } ?>
                    </select>
                </div>
                <div style="float:left; width:70%; margin-top:5px">
                    <span>Ambiente:</span>
                    <select id="txtAmbiente" name="txtAmbiente" <?php echo $disabled; ?> class="select" style="width:100%">
                        <option value="null">Selecione</option>
                        <?php
                        $cur = odbc_exec($con, "select id_ambiente,nome_ambiente from sf_ambientes order by nome_ambiente ") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) {
                            ?>
                            <option value="<?php echo $RFP["id_ambiente"] ?>"<?php
                            if (!(strcmp($RFP["id_ambiente"], $ambiente))) {
                                echo "SELECTED";
                            }
                            ?>><?php echo utf8_encode($RFP["nome_ambiente"]) ?></option>
                                <?php } ?>
                    </select>
                </div>
                <div style="float:left; width:29%; margin-top:5px; margin-left: 1%;">
                    <span>Dias:</span>
                    <input id="txtDias" name="txtDias" <?php echo $disabled; ?> type="text" value="<?php echo $dias; ?>" class="input-medium" style="width:100%"/>
                </div>  
                <div style="float:left; width:100%; margin-top:10px;">
                    <input type="checkbox" class="input-medium" id="txtLimitar_Acesso" name="txtLimitar_Acesso" <?php echo $disabled; ?> <?php
                    if (substr($limitar_acesso, 0, 1) == 1) {
                        echo "checked";
                    }
                    ?> value="1"/>
                    Limitar
                    <input style="width: 16%;" name="txtQtdLimiteAcesso" id="txtQtdLimiteAcesso" maxlength="2" type="text" class="input-medium" value="<?php echo $qtd_limite_acesso; ?>" <?php echo $disabled; ?>>
                    Acesso(s) a cada
                    <input style="width: 16%;" name="txtQtdLimiteAcessoMin" id="txtQtdLimiteAcessoMin" maxlength="4" type="text" class="input-medium" value="<?php echo $qtd_limite_acesso_min; ?>" <?php echo $disabled; ?>>
                    Min <br>
                    <input type="checkbox" class="input-medium" id="txtInativo" name="txtInativo" <?php echo $disabled; ?> <?php
                    if (substr($inativo, 0, 1) == 1) {
                        echo "checked";
                    }
                    ?> value="1"/>
                    Inativo                    
                    <input type="checkbox" class="input-medium" id="txtDelDepVenc" name="txtDelDepVenc" <?php echo $disabled; ?> <?php
                    if (substr($delDepVenc, 0, 1) == 1) {
                        echo "checked";
                    }
                    ?> value="1"/>
                    Excluir Dependente com a Credenciais Vencida
                </div>                
                <div style="clear:both"></div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == "") { ?>
                        <button class="btn green" type="submit" name="bntSave" onClick="return validaForm()"><span class="ico-checkmark"></span> Gravar</button>
                        <?php if ($_POST["txtId"] == "") { ?>
                            <button class="btn yellow" onClick="parent.FecharBox(1)"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                            <button class="btn yellow" type="submit" name="bntAlterar"><span class="ico-reply"></span> Cancelar</button>
                            <?php
                        }
                    } else {
                        ?>
                        <button class="btn green" type="submit" name="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" type="submit" name="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                        <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../js/jquery.priceformat.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>        
    <script type="text/javascript" src="js/CredenciaisForm.js"></script>    
    <?php odbc_close($con); ?>
</body>
