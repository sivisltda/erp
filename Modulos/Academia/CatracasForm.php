<?php
include "../../Connections/configini.php";
include "form/CatracasFormServer.php";
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
<style>
    .selected {
        background: #acbad4 !important;
    }
</style>
<body>
    <div class="row-fluid">
        <form action="CatracasForm.php" name="frmCatracas" id="frmCatracas" method="POST">
            <div class="frmhead">
                <div class="frmtext">Catracas</div>
                <div class="frmicon" onClick="parent.FecharBox(1)">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input id="txtId" name="txtId" type="hidden" value="<?php echo $id; ?>"/>
                <div style="width:100%">
                    <span>Descrição:</span>
                    <input id="txtDescricao" name="txtDescricao" <?php echo $disabled; ?> type="text" value="<?php echo $descricao; ?>" class="input-medium" style="width:100%"/>
                </div>
                <div style="width:100%; margin-top:5px">
                    <span>Tipo:</span>
                    <select id="txtTipo" name="txtTipo" <?php echo $disabled; ?> class="select" style="width:100%">
                        <option value="null">Selecione</option>
                        <option value="1" <?php
                        if ($tipo == 1) {
                            echo "SELECTED";
                        }
                        ?>>Rodbel Serial (RBC2801)</option>
                        <option value="2" <?php
                        if ($tipo == 2) {
                            echo "SELECTED";
                        }
                        ?>>Rodbel Paralela (RBC3)</option>
                        <option value="3" <?php
                        if ($tipo == 3) {
                            echo "SELECTED";
                        }
                        ?>>Topdata TCP-IP (Inner Revolution)</option>
                        <option value="4" <?php
                        if ($tipo == 4) {
                            echo "SELECTED";
                        }
                        ?>>Henry 7x TCP-IP</option>
                        <option value="5" <?php
                        if ($tipo == 5) {
                            echo "SELECTED";
                        }
                        ?>>Henry 8x TCP-IP</option>
                        <option value="6" <?php
                        if ($tipo == 6) {
                            echo "SELECTED";
                        }
                        ?>>Rodbel TCP-IP (2900) | Dimep Micropoint XP</option>
                        <option value="7" <?php
                        if ($tipo == 7) {
                            echo "SELECTED";
                        }
                        ?>>Tecnibra (Eletromecânica NC)</option>
                        <option value="8" <?php
                        if ($tipo == 8) {
                            echo "SELECTED";
                        }
                        ?>>Usb Universal</option>
                        <option value="9" <?php
                        if ($tipo == 9) {
                            echo "SELECTED";
                        }
                        ?>>Rodbel TCP-IP (5707)</option>
                        <option value="10" <?php
                        if ($tipo == 10) {
                            echo "SELECTED";
                        }
                        ?>>ControlID TCP-IP</option>
                        <option value="11" <?php
                        if ($tipo == 11) {
                            echo "SELECTED";
                        }
                        ?>>Proveu TCP-IP</option>
                        <option value="12" <?php
                        if ($tipo == 12) {
                            echo "SELECTED";
                        }
                        ?>>Trix Standard</option>
                        <option value="13" <?php
                        if ($tipo == 13) {
                            echo "SELECTED";
                        }
                        ?>>Tecnibra IHM TCP-IP</option>
                        <option value="14" <?php
                        if ($tipo == 14) {
                            echo "SELECTED";
                        }
                        ?>>Dimep Fancy (apenas código de barras)</option>
                    </select>
                </div>
                <div style="width:100%; margin:15px 0 5px">
                    <hr style="margin:2px 0; border-top:1px solid #DDD">
                </div>
                <div style="min-height: 90px;">
                    <div>                        
                        <div style="width:20%;float: left;margin-left: 1%;display: none;">
                            <span>Porta:</span>
                            <input id="txtPorta" name="txtPorta" <?php echo $disabled; ?> type="text" value="<?php echo $porta; ?>" class="input-medium" style="width:100%"/>
                        </div>                        
                        <div style="width:20%;float: left;margin-left: 1%;display: none;">
                            <span>Número Inner:</span>
                            <input id="txtNumInner" name="txtNumInner" <?php echo $disabled; ?> type="text" value="<?php echo $numinner; ?>" class="input-medium" style="width:100%"/>
                        </div>                        
                        <div style="width:20%;float: left;margin-left: 1%;display: none;">
                            <span>Qtd. Digitos:</span>
                            <input id="txtQtdDigitos" name="txtQtdDigitos" <?php echo $disabled; ?> type="text" value="<?php echo $qtddigitos; ?>" class="input-medium" style="width:100%"/>
                        </div>
                        <div style="width:36%;float: left;margin-left: 1%;display: none;">
                            <span>Tipo de Conexão:</span>
                            <select id="txtTpConexao" val="<?php echo $tpconexao; ?>" name="txtTpConexao" <?php echo $disabled; ?> class="select" style="width:100%"></select>
                        </div>
                        <div style="width:20%;float: left;display: none;">
                            <span>Intervalo:</span>
                            <input id="txtIntervalo" name="txtIntervalo" <?php echo $disabled; ?> type="text" value="<?php echo $intervalo; ?>" class="input-medium" style="width:100%"/>
                        </div>
                        <div style="width:20%;float: left;margin-left: 1%;display: none;">
                            <span>Relé:</span>
                            <select id="txtVariante" val="<?php echo $variante; ?>" name="txtVariante" <?php echo $disabled; ?> class="select" style="width:100%"></select>
                        </div>                        
                        <div style="width:62%;float: left;margin-left: 1%;display: none;">
                            <span>Tipo Equipamento:</span>
                            <select id="txtTpEquip" val="<?php echo $tpEquip; ?>" name="txtTpEquip" <?php echo $disabled; ?> class="select" style="width:100%"></select>
                        </div>
                        <div style="width:36%;float: left;margin-left: 1%;display: none;">
                            <span>Catraca está instalada à sua:</span>
                            <select id="txtSentido" val="<?php echo $sentido; ?>" name="txtSentido" <?php echo $disabled; ?> class="select" style="width:100%"></select>
                        </div>
                        <div style="width:36%;float: left;margin-left: 1%;display: none;">
                            <span>IP:</span>
                            <input id="txtIp" name="txtIp" <?php echo $disabled; ?> type="text" value="<?php echo $ip; ?>" class="input-medium" style="width:100%"/>
                        </div>
                        <div style="width:20%;float: left;display: none;">
                            <span>Intervalo:</span>
                            <select id="txtSelIntervalo" val="<?php echo $intervalo; ?>" name="txtSelIntervalo" <?php echo $disabled; ?> class="select" style="width:100%"></select>
                        </div>                        
                    </div>
                </div>
                <div style="clear: both"></div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == "") { ?>
                        <button class="btn green" type="submit" name="bntSave" onClick="return validaForm()"><span class="ico-checkmark"></span> Gravar</button>
                        <?php if ($_POST["txtId"] == "") { ?>
                            <button class="btn yellow" onClick="parent.FecharBox(1)"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                            <button class="btn yellow" type="submit" name="bntAlterar"><span class="ico-reply"></span> Cancelar</button>
                            <?php
                        }
                    } else { ?>
                        <button class="btn green" type="submit" name="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" type="submit" name="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                        <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
        </form>
        <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/jquery.priceformat.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>
        <script type="text/javascript" src="js/CatracasForm.js"></script>        
    </div>
    <?php odbc_close($con); ?>
</body>