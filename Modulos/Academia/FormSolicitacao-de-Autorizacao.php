<?php include '../../Connections/configini.php'; ?>
<link rel="icon" type="image/ico" href="../../../favicon.ico"/>
<link href="../../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../../css/main.css" rel="stylesheet">
<link href="../../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' src='../../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <div class="row-fluid">
        <form method="POST" id="frmEnviaDados">
            <div class="frmhead">
                <div class="frmtext">Contas Variáveis</div>
                <div class="frmicon" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input id="txtId" name="txtId" value="" type="hidden"/>
                <div style="float: left; width: 50%;">
                    <span>Operação:</span>
                    <select id="txtOperacao" name="txtOperacao">
                        <option value="null">Selecione</option>
                        <option value="C">ENTRADA</option>
                        <option value="D">SAÍDA</option>                       
                    </select>
                </div>                
                <div style="float: left; width: 69%;">
                    <span>Formas de Pagamento:</span>
                    <select id="txtForma" name="txtForma">
                        <option value="null">Selecione</option>
                    </select>
                </div>                
                <div style="float: left; width: 30%; margin-left: 1%;">
                    <span>Valor:</span>
                    <input id="txtValor" name="txtValor" type="text" style="text-align: right;" class="input-medium" value="<?php echo escreverNumero(0); ?>"/>
                </div>                
                <div style="float: left; width: 100%;">
                    <span>Observação:</span>
                    <div class="content" style="min-height:70px; margin-top:0">
                        <textarea id="txtObs" name="txtObs" style="width:100%; height:70px"></textarea>
                    </div>
                </div>
                <div style="clear:both;"></div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <button class="btn btn-success" type="button" onClick="salvar();" id="btnGravarPlano" title="Gravar"><span class="ico-checkmark"></span> Gravar</button>
                    <button class="btn yellow" title="Cancelar" onClick="parent.FecharBox()"><span class="ico-reply"></span> Cancelar</button>
                </div>
            </div>
        </form>
    </div>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../../js/plugins.js'></script>
    <script type="text/javascript" src="../../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type='text/javascript' src='../../../js/plugins/datatables/fnReloadAjax.js'></script>
    <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script> 
    <script type='text/javascript' src='../../../js/jquery.priceformat.min.js'></script>
    <script type="text/javascript" src="../../js/util.js"></script>
    <script type="text/javascript">

        $("#txtValor").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});

        $('#txtOperacao').change(function () {
            $.getJSON('./../Contas-a-pagar/tipo.ajax.php', {txtTipo: $(this).val(), ajax: 'true'}, function (j) {
                var options = '<option value="null">Selecione</option>';
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].id_grupo_contas + '">' + j[i].descricao + '</option>';
                }
                $('#txtForma').html(options).show();
            });
        });

        function validar() {
            if ($('#txtOperacao').val() === "null") {
                bootbox.alert("Preencha o campo Operação!");
                return false;
            } else if ($('#txtForma').val() === "null") {
                bootbox.alert("Preencha o campo Formas de Pagamento!");
                return false;
            } else if ($('#txtValor').val() === "0,00") {
                bootbox.alert("Preencha o campo Valor!");
                return false;
            } else if ($('#txtObs').val() === "") {
                bootbox.alert("Preencha o campo Observação!");
                return false;
            }
            return true;
        }

        function salvar() {
            if (validar()) {
                $.post("form/FormSolicitacao-de-Autorizacao.php",
                "btnSave=S&txtEmpresa=" + parent.$("#txtMnFilial").val() + "&" + 
                $("#frmEnviaDados").serialize()).done(function (data) {
                    if (data === "YES") {
                        parent.FecharBox();
                    } else {
                        bootbox.alert("Erro: " + data);
                    }
                });
            }
        }

    </script>
    <?php odbc_close($con); ?>
</body>
