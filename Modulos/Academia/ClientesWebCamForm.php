<?php
include "../../Connections/configini.php";
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <div class="row-fluid">
        <form>
            <div class="frmhead">
                <div class="frmtext">WebCam</div>
                <div class="frmicon" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input id="txtContrato" value="<?php echo $_SESSION["contrato"]; ?>" type="hidden"/>
                <div style="float: left">
                    <div id="webcam"> </div>
                </div>
                <div style="float: left;margin-left: 10%;">
                    <img id="image" style="width:210px;height:210px;"/>
                    <div style="clear:both; height:5px"></div>
                    <button type="button" class="btn btn-default" id="btnCapturar" onclick="base64_toimage()" style="width: 100%">Capturar</button>
                    <button type="button" class="btn btn-success" id="btnSalvarImagem" onclick="saveImagem()" style="width: 100%">Confirmar</button>
                </div>

                <div style="clear:both; height:5px"></div>
            </div>
        </form>
    </div>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
    <script language="JavaScript" src="../../js/swfobject.js"></script>
    <script language="JavaScript" src="../../js/plugins/ScriptCam-master/scriptcam.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script language="JavaScript" src="js/ClientesWebCamForm.js"></script>
</body>
