<?php
include "../../Connections/configini.php";
$mdl_srs_ = returnPart($_SESSION["modulos"], 9);
$mdl_clb_ = returnPart($_SESSION["modulos"],12);
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<link href="../../js/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<style>
    .jstree-themeicon-custom {
        font-size: 15px;
        margin-top: 5px !important;;
    }
</style>
<body onload="iniciaHistoricoForm();">
    <div class="row-fluid">
        <form>
            <div class="frmhead">
                <div class="frmtext">Histórico</div>
                <div class="frmicon" onClick="parent.FecharBoxCliente()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="tabbable frmtabs">
                <ul class="nav nav-tabs" style="margin-bottom:0px">
                    <li class="active" onclick="trataFoot('tab1')"><a href="#tab1" data-toggle="tab"><b>Pagamentos</b></a></li>
                    <li onclick="trataFoot('tab2')"><a href="#tab2" data-toggle="tab"><b>Modalidades</b></a></li>
                    <li onclick="trataFoot('tab3')"><a href="#tab3" data-toggle="tab"><b>Transferências</b></a></li>                    
                    <li onclick="trataFoot('tab4')"><a href="#tab4" data-toggle="tab"><b>Turmas</b></a></li>
                    <?php if ($mdl_srs_ == 1) { ?>
                        <li onclick="trataFoot('tab5')"><a href="#tab5" data-toggle="tab"><b>Serasa</b></a></li>
                    <?php } if ($mdl_clb_ == 1) { ?>
                        <li onclick="trataFoot('tab6')"><a href="#tab6" data-toggle="tab"><b>Trancamento</b></a></li>
                    <?php } ?>                      
                </ul>
                <div class="tab-content frmcont" style="overflow:initial; border:1px solid #ddd; border-top:0px; padding-top:10px;height: 312px;overflow-y: auto">
                    <div class="tab-pane active" id="tab1" style="margin-bottom:5px">
                        <div style="width: 100%; float: left; display: none;" id="txtAlert">
                            <div class="alert alert-warning" style="cursor: text;color: #8a6d3b !important; background: #fcf8e3 !important;border: 1px solid #FBEEC5 !important;">
                                <p>Não foram encontrados histórico de pagamentos.</p>
                            </div>
                        </div>
                        <div id="histPag"> </div>
                        <div style="clear:both; height:2px"></div>
                    </div>
                    <div class="tab-pane" id="tab2">
                        <div class="body" style="margin-left:0; padding:0">
                            <div class="content" style="min-height:153px; margin-top:0">
                                <table id="tblHistPlanos" class="table" cellpadding="0" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width:180px;text-align:center;"><b>Modalidade</b></th>
                                            <th style="width:59px;text-align:center;"><b>Inicio</b></th>
                                            <th style="width:59px;text-align:center;"><b>Fim</b></th>
                                            <th style="width:59px;text-align:center;"><b>Cancelamento</b></th>
                                            <th style="width:100px;text-align:center;"><b>Motivo</b></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                        <div style="clear:both;height: 10px;"></div>
                    </div>
                    <div class="tab-pane" id="tab4">
                        <div class="body" style="margin-left:0; padding:0">
                            <div class="content" style="min-height:153px; margin-top:0">
                                <table id="tblHistTurmas" class="table" cellpadding="0" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width:150px;text-align:center;"><b>Turma</b></th>
                                            <th style="width:50px;text-align:center;"><b>Desmatr.Em</b></th>
                                            <th style="width:100px;text-align:center;"><b>Usuario</b></th>
                                            <th style="width:150px;text-align:center;"><b>Motivo</b></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                        <div style="clear:both;height: 10px;"></div>
                    </div>
                    <?php if ($mdl_srs_ == 1) { ?>
                        <div class="tab-pane" id="tab5">
                            <div class="body" style="margin-left:0; padding:0">
                                <div class="content" style="min-height:153px; margin-top:0">
                                    <table id="tblHistSerasa" class="table" cellpadding="0" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th style="width:50px;text-align:center;"><b>Tipo</b></th>
                                                <th style="width:50px;text-align:center;"><b>Data</b></th>
                                                <th style="width:100px;text-align:center;"><b>Usuario</b></th>
                                                <th style="width:250px;text-align:center;"><b>Motivo</b></th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                            <div style="clear:both;height: 10px;"></div>
                        </div>
                    <?php } if ($mdl_clb_ == 1) { ?>
                        <div class="tab-pane" id="tab6">
                            <div class="body" style="margin-left:0; padding:0">
                                <div class="content" style="min-height:153px; margin-top:0">
                                    <table id="tblTrancamento" class="table" cellpadding="0" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th style="width:100px;text-align:center;"><b>Tipo</b></th>
                                                <th style="width:100px;text-align:center;"><b>Data</b></th>
                                                <th style="width:100px;text-align:center;"><b>Usuario</b></th>
                                                <th style="width:150px;text-align:center;"><b>Lançamento</b></th>
                                                <th style="width:150px;text-align:center;"><b>Motivo</b></th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                            <div style="clear:both;height: 10px;"></div>
                        </div>
                    <?php } ?>                                       
                    <div class="tab-pane" id="tab3">
                        <div class="body" style="margin-left:0; padding:0">
                            <div class="content" style="min-height:153px; margin-top:0">
                                <table id="tblHistTransferencia" class="table" cellpadding="0" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width:45px;text-align:center;"><b>Data</b></th>
                                            <th style="width:40px;text-align:center;"><b>Ação</b></th>
                                            <th style="width:120px;text-align:center;"><b>Aluno</b></th>
                                            <th style="width:80px;text-align:center;"><b>Modalidade</b></th>
                                            <th style="width:85px;text-align:center;"><b>Período</b></th>                                        
                                            <th style="width:150px;text-align:center;"><b>Motivo</b></th>
                                            <th style="width:50px;text-align:center;"><b>Usuario</b></th>                                        
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                        <div style="clear:both;height: 10px;"></div>
                    </div>                    
                </div>
            </div>
            <div class="frmfoot" style="text-align: left;">
                <div style="width:100%; display:block; float:left; margin-left:1%;" id="TotPag">
                    <span style="display:block;float:left;font-size: 24px;"> Total Recebido: </span>
                    <span style="display:block;font-size: 16px;margin-top: -3px;margin-left: 1%;float: left"><span style="font-size: 32px;font-weight: bold;" id="lblTotalPag"><?php echo escreverNumero(0,1); ?></span></span>
                </div>
            </div>
        </form>
    </div>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
    <script type='text/javascript' src="../../js/plugins/jstree/dist/jstree.min.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script src="js/ClientesHistoricoForm.js" type="text/javascript"></script>    
</body>
