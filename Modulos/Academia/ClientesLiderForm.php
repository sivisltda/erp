<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/formularios.css" rel="stylesheet" type="text/css" />

<body onload="parent.preencheConvParticipantes(<?php echo $_GET['idForm']; ?>);">
    <div class="row-fluid">
        <form>
        <div class="frmhead">
            <div class="frmtext">Definir Líder</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont">
            <input type="hidden" value="" id="txtQtdRegra" name="txtQtdRegra" value="0"></input>
            <input name="txtIdLiderFrame" id="txtIdLiderFrame" value="" type="hidden"/>
            <div class="body" style=" margin-left: 0px; padding: 0px 0px 0px 0px;">
                <div style="float:left; width:87%">
                    <span>Líder:</span>
                    <input type="text" class="input-medium" id="txtNomeLider" name="txtNomeLider" <?php echo $disabled; ?>/>
                </div>
                <div  style="float:left;margin-left: 1%;padding-top: 14px;">
                    <button type="button" class="btn btn-success" title="Buscar" onClick="parent.preencheConvParticipantes(<?php echo $_GET['idForm']; ?>);">Buscar </button>
                </div>
                <div style="clear:both;height: 5px;"></div>
                <div class="content" style="min-height: 0px;margin-top: 0px;margin-bottom: 0px;">
                    <table id="tblConvParticipantes" class="table" cellpadding="0" cellspacing="0" width="100%" style="display: table;">
                        <thead style="display: block;">
                        <tr>
                            <th width="415px">Cliente</th>
                            <th width="80px">Definir Lider</th>
                        </tr>
                        </thead>
                        <tbody style="    height: 204px;overflow-y: auto;overflow-x: hidden;display: block;">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <button class="btn btn-success" type="button" id="btnSaveConvPart" title="Gravar" onclick="parent.SalvaConvLider(<?php echo $_GET['idForm']; ?>)"><span class="ico-checkmark"></span> Gravar</button>
                <button class="btn yellow" title="Cancelar" onClick="parent.FecharBox()"><span class="ico-reply"></span> Cancelar</button>
            </div>
        </div>
    </div>
</body>
