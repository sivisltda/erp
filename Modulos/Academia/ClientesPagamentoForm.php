<?php include "../../Connections/configini.php";

$dcc_tef = 0;

$cur = odbc_exec($con, "select A.id_filial_f from sf_usuarios_filiais A
inner join sf_usuarios B on (A.id_usuario_f = B.id_usuario and A.id_filial_f = B.filial)
where A.id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
while ($RFP = odbc_fetch_array($cur)) {
    $filial = $RFP['id_filial_f'];
}

$cur = odbc_exec($con, "select dcc_multa, dcc_tef from sf_configuracao where id = 1");
while ($RFP = odbc_fetch_array($cur)) {
    $dcc_tef = $RFP['dcc_tef'];
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<link href="../../js/jBox/jBox.css" rel="stylesheet" type="text/css"/> 
<link href="../../js/jBox/themes/TooltipBorder.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<style>
    .daterange__picker {
        position: relative;
    }
    .daterange__picker input {
        position: absolute;
        left: 0;
        bottom: 0;
        opacity: 0;
        pointer-events: none;
    }
    .clickable-text {
        display: inline-block;
        border-bottom: 2px dotted #ccc;
        cursor: pointer;
        padding: 0 2px;
        color: #000;
        &:hover {
             color:blue;
             background: #f5f5f5;
             border-bottom-color: blue;
         }
    }
    .body .content .table td {
        vertical-align: top;
    }
    #txtDtPagamento{
      margin-top: 24px;
      background-color: #e2e2e2;
      border: none;
      font-size: 16px;
      font-weight: bold;
      text-align: right;
      padding-right: 42px;
    }
</style>
<body>
    <div class="row-fluid">
        <form>
            <div class="frmhead">
                <div class="frmtext">Recebimento
                    <div style="float: right; line-height: 23px; width: 144px;">
                        <div id="time" style="font-size: 19px;font-weight: bold;text-align: center;"></div>
                        <div style="position: absolute;top: 0px;right: 6px;">
                            <input type="text" id="txtDtPagamento" name="txtDtPagamento" class="<?php if($ckb_aca_pgr_ == 1){ echo "datepicker";}?> inputCenter" <?php if($ckb_aca_pgr_ == 0){ echo "readonly";}?> value="<?php echo getData("T");?>"/>
                        </div>
                    </div>
                </div>
                <div class="frmicon" onClick="parent.FecharBoxCliente('PAG')">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input id="telaDcc" type="hidden" value="<?php echo $_GET["isDCC"]; ?>"/>
                <input id="dcc_tef" type="hidden" value="<?php echo $dcc_tef;?>"/>
                <div style="width:22%; float:left;">
                    <span>Usuário:</span>
                    <select id="txtUsuario" name="txtUsuario" class="select input-medium" style="width:100%">
                        <?php if($ckb_out_todos_operadores_ == 1){ ?>
                            <option value="null">Selecione</option>
                        <?php } $cur = odbc_exec($con, "select id_usuario,login_user,nome from sf_usuarios where " . ($ckb_out_todos_operadores_ != 1 ? " id_usuario = " . $_SESSION["id_usuario"] . " AND " : "") . " inativo = 0 order by nome") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option value="<?php echo $RFP['id_usuario'] ?>"<?php
                            if (!(strcmp($RFP['id_usuario'], $_SESSION["id_usuario"]))) {
                                echo "SELECTED";
                            } ?>><?php echo utf8_encode($RFP['nome']); ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div style="width:36%; float:left;margin-left: 1%">
                    <span>Forma de Pagto:</span>
                    <select class="select"  style="width: 100%;" name="txtTipoPag" id="txtTipoPag">
                        <option value="null">--Selecione--</option>
                        <?php
                        $isNotdcc = "";
                        if($ckb_fin_crt_ == 0) {
                            $isNotdcc .= " and id_tipo_documento <> 12";                            
                        }
                        $cur = odbc_exec($con, "select id_tipo_documento,descricao, parcelar, isnull(id_banco_baixa,0) id_banco_baixa, cartao_dcc,isnull(dias,0) dias from sf_tipo_documento where s_tipo in ('A','C') and inativo = 0 " . $isNotdcc . " order by descricao") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option  value="<?php echo $RFP['id_tipo_documento']; ?>" data-parc="<?php echo $RFP['parcelar']; ?>" cartao_dcc="<?php echo $RFP['cartao_dcc']; ?>" id_baixa="<?php echo $RFP['id_banco_baixa']; ?>" dias="<?php echo $RFP['dias']; ?>"><?php echo utf8_encode($RFP['descricao']) ?></option>
                        <?php } ?>
                    </select>
                </div>                
                <div style="width:16%; float:left;margin-left: 1%">
                    <span>Vencimento Inicial:</span>
                    <input class="<?php if($ckb_aca_pgr_ == 1){ echo "datepicker";}?> inputCenter" <?php if($ckb_aca_pgr_ == 0){ echo "readonly";}?> name="txtDtVencimento" id="txtDtVencimento" type="text" value="<?php echo getData("T");?>">
                </div>                
                <div style="width:13%; display:block; float:left; margin-left:1%;" id="parcelas">
                    <span> Qtd. de Parcelas: </span>
                    <input type="text" class="input-xlarge" id="spinner" value="1" maxlength="2" data-mask="00"/>
                </div>
                <button class="btn btn-success" type="button" style="width:8%; margin-top: 13px;margin-left: 1%;" onclick="gerarParc('B')" id="btnGerarParc">Gerar</button>
                <div style="clear:both; height:10px"></div>
                <div id="mnuCartao" style="width:100%; float:left; display: none;">
                    <div style="width:100%; float:left;">
                        <hr style="margin:2px 0; border-top:1px solid #ddd">
                    </div>
                    <div style="width:16%; float:left;">
                        <span>Ação do Cartão:</span>
                        <select style="width: 100%;" name="txtAcaoCartao" id="txtAcaoCartao">
                            <option value="0">Meus Cartões</option>
                            <option value="1">Transac.|Salvar</option>
                            <option value="2">Transacionar</option>
                        </select>
                    </div>
                    <div style="width: 83%; float: left;margin-left: 1%;" id="divCartao">
                        <span>Cartão:</span>
                        <select class="select" style="width: 100%;" name="txtCartao" id="txtCartao">
                            <option value="null">--Selecione--</option>
                        </select>
                    </div>
                    <div style="width:83%; float:left; margin-left: 1%; display: none;" id="divCartaoDados">
                        <?php include "include/DadosCartao.php";?>                        
                    </div>
                </div>                
                <div>
                    <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Parcelamento</span>
                    <hr style="margin:2px 0; border-top:1px solid #ddd">
                </div>
                <div class="body" style="overflow-y: scroll; height: 283px; padding: 0px 0px 0px 0px; margin-top: -6px; margin-left: 1px;">
                    <div class="content" style="min-height: 0px; margin: 0px;">
                        <table id="tblParcelas" class="table" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="width: 3%" width="3%" align="center">Nr:</th>
                                    <th style="width: 51%" width="51%" align="right">Documento:</th>                                    
                                    <th style="width: 4%" width="4%" align="center"><div style="width: 78px;">Vencimento:
                                    <span class="ico-question-sign informacao" title="O campo 'vencimento' em caso de pagamento
                                    <br> em cartões mostra a data de recebimento futuro,<br> respeitando para o cálculo a data do pagamento."></span></div>
                                    </th>
                                    <th style="width: 13%" width="13%" align="right">Valor:</th>
                                    <th style="width: 29%" width="29%" align="right">Forma Pg:</th>
                                </tr>
                            </thead>
                            <tbody style="height: 240px;overflow-y: auto;overflow-x: hidden;"></tbody>
                        </table>
                        <table id="tblParcelasDinheiro" class="table" style="display: none;" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th colspan="3" style="text-align: center;">Detalhamento do Pagamento</th>
                                </tr>
                            </thead>
                            <tbody style="height: 240px;overflow-y: auto;overflow-x: hidden;">
                                <tr>
                                    <td style="width: 40%; text-align: right;">TOTAL</td>                                    
                                    <td style="width: 4%"></td>
                                    <td style="width: 56%">
                                        <input id="txtPagamentoValor" style="text-align: center;width:40%" class="input-xxlarge inputDireito" type="text" value="0,00" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 40%; text-align: right;">PAGTO</td>                                    
                                    <td style="width: 4%"></td>                                    
                                    <td style="width: 56%">
                                        <input id="txtPagamentoPago" style="text-align: center;width:40%" class="input-xxlarge inputDireito" type="text" value="0,00">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 40%; text-align: right; font-weight: bold;">TROCO</td>                                    
                                    <td style="width: 4%"></td>                                    
                                    <td style="width: 56%">
                                        <input id="txtPagamentoTroco" style="text-align: center;width:40%; font-size: 20px; font-weight: bold;" class="input-xxlarge inputDireito" type="text" value="0,00" disabled>
                                    </td>
                                </tr>
                            </tbody>
                        </table>                                   
                    </div>
                </div>
                <div style="clear:both; height:5px"></div>
            </div>
            <div class="frmfoot" style="line-height: 24px !important;height: 80px;">
                <div class="data-fluid" style="overflow:hidden;">
                    <div style="width:58%; display:block; float:left; margin-left:1%;padding-top: 14px;">
                        <span style="display:block;float:left;font-size: 24px;padding-top: 3px;"> Total: </span>
                        <span style="display:block;font-size: 16px;color: red;float:left;margin-left: 1%;">
                            <span style="font-size: 32px;font-weight: bold;" id="lblTotalPag"><?php echo escreverNumero(0,1); ?></span>
                        </span>
                        <div style="clear:both; height:6px"></div>
                        <span style="display:block;float:left;font-size: 16px;"> Saldo Disponível: </span>
                        <span style="display:block;font-size: 11px;float: left;margin-left: 2%;">
                            <input type="hidden" id="txtTotalDisp" />
                            <span style="font-size: 16px;font-weight: bold;" id="lblTotalDisp"><?php echo escreverNumero(0,1); ?></span>
                        </span>
                    </div>
                    <div style="display:block;margin-right:1%;width: 40%;padding-top: 14px;float: left;">
                        <div style="float:left;margin-left: 2%;width: 100%;padding-top: 3px;">
                            <button type="button" class="btn green" style="line-height:40px; width:100%; margin:0 0 5px;font-size: 24px;" onclick="salvaPagCliente();" id="btnPag" disabled>
                                <span class="ico-shopping-cart icon-white"></span> &nbsp;Concluir
                            </button>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                </div>
            </div>
        </form>
    </div>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>    
    <script type="text/javascript" src="../../js/jquery.creditCardValidator.js"></script>    
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>    
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/moment.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/jBox/jBox.min.js"></script>    
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type='text/javascript' src='js/ClientesPagamentoForm.js'></script>    
    <?php odbc_close($con); ?>
</body>
