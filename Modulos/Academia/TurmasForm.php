<?php
include "../../Connections/configini.php";
if ($ckb_aca_turmas_ == 0) {
    header('Location: ../../Index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
        <title>Cadastro de Turmas</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <link rel="stylesheet" href="../../js/plugins/jquery.ajax-combobox/dist/jquery.ajax-combobox.css">
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>   
    </head>
    <style>
        input {text-transform: uppercase;}
        #tblItensPag td{
            vertical-align: middle;
        }
        .dataTables_scrollHeadInner{
            width: 100% !important;
        }
        .dataTables_scrollHeadInner table.dataTable.no-footer{
            width: 100% !important;
        }
        #tblAlunosTurma_wrapper div.dataTables_scrollHeadInner table.dataTable.no-footer{
            width: 99% !important;
        }
    </style>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1 style="margin-top:8px">Turmas</h1>
                    </div>
                    <div class="row-fluid">
                        <form name="frmTurmasForm" id="frmTurmasForm">
                            <div class="span12">
                                <div class="block">
                                    <div id="formulario">
                                        <div class="data-fluid tabbable" style="margin-top:10px; margin-bottom:15px">
                                            <ul class="nav nav-tabs" style="margin-bottom:0px">
                                                <li class="active"><a href="#tab1" data-toggle="tab"><b>Dados Turma</b></a></li>
                                                <?php if ($_GET['id'] != "") { ?>
                                                    <li><a href="#tab2" data-toggle="tab"><b>Lista de Espera</b></a></li>
                                                <?php } ?>
                                            </ul>
                                            <div class="tab-content" style="overflow:initial; border:1px solid #ddd; border-top:0px; padding-top:10px; background:#F6F6F6;min-height: 616px;">
                                                <div class="tab-pane active" id="tab1" style="margin-bottom:5px">
                                                    <div id="divDadosTurma">
                                                        <div style="width:19%; float:left">
                                                            <input name="txtId" id="txtId" value="<?php echo $_GET['id']; ?>" type="hidden"/>
                                                            <span><span style="color:red;">*</span>Descrição:</span>
                                                            <input type="text" id="txtDescricao" name="txtDescricao" class="input-medium"/>
                                                        </div>
                                                        <div style="width:12%; float:left;margin-left: 1%;">
                                                            <span><span style="color:red;">*</span>Início Em:</span>
                                                            <input type="text" id="txtDtInicio" name="txtDtInicio" class="datepicker inputCenter"/>
                                                        </div>
                                                        <div style="width:12%; float:left;margin-left: 1%;">
                                                            <span>Criada Em:</span>
                                                            <input type="text" id="txtDtCriacao" name="txtDtCriacao" value="<?php echo getData("T"); ?>" class="datepicker inputCenter" readonly/>
                                                        </div>
                                                        <div style="float: left;width: 176px;margin-left: 1%;margin-top: 20px;">
                                                            <input name="ckbDesmatricular" id="ckbDesmatricular" type="checkbox" value="1" class="input-medium"/> Desmatricular Automático
                                                        </div>
                                                        <div style="width:8%; float:left;margin-left: 1%;">
                                                            <span>Em:</span><br>
                                                            <input type="text" id="txDiasDesmatricula" name="txDiasDesmatricula" class="input-medium" style="width: 50px;" maxlength="3"/> Dias
                                                        </div>
                                                        <div style="float: left;width: 139px;margin-left: 1%;margin-top: 20px;text-align: right;">
                                                            <input name="ckbInativo" id="ckbInativo" type="checkbox" value="1" class="input-medium"/> Desativar Turma
                                                        </div>
                                                        <div style="clear:both; height:5px"></div>
                                                        <div style="width:30%; float:left;">
                                                            <span> Professor:</span>
                                                            <select class="select" style="width:100%" id="txtProfessor" name="txtProfessor"> </select>
                                                        </div>
                                                        <div style="width:33%; float:left; margin-left:1%">
                                                            <span><span style="color:red;">*</span>Horário:</span>
                                                            <select class="select" style="width:100%" id="txtHorario" name="txtHorario"> </select>
                                                        </div>
                                                        <div style="width:14%; float:left;margin-left: 1%;">
                                                            <span><span style="color:red;">*</span>Capacidade:</span>
                                                            <input type="text" id="txtCapacidade" maxlength="6" name="txtCapacidade" class="input-medium"/>
                                                        </div>
                                                        <div style="width:20%; float:left; margin-left:1%">
                                                            <span>Ambiente:</span>
                                                            <select class="select" style="width:100%" id="txtAmbiente" name="txtAmbiente"> </select>
                                                        </div>
                                                        <div style="clear:both; height:5px"></div>
                                                        <div style="width:50%; float:left;">
                                                            <div style="width:100%; margin:10px 0">
                                                                <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Planos</span>
                                                                <hr style="margin:2px 0; border-top:1px solid #ddd">
                                                            </div>
                                                            <select name="itemsPlanos[]" multiple="multiple" id="mscPlanos">
                                                                <option value=""></option>
                                                            </select>
                                                        </div>
                                                        <div style="clear:both; height:5px"></div>
                                                        <div style="width:100%; margin:10px 0">
                                                            <div style="width: 70%;margin-top: 12px;float: left;">
                                                                <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Alunos na Turma</span>
                                                                <span><b>Qtd.: <span id="qtdAluno">0</span></b></span>
                                                            </div>
                                                            <div style="width: 28%;text-align: right;float: left;margin-right: 10px">
                                                                <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimirListaAlunos()" title="Imprimir"><span class="ico-print icon-white"></span> Lista</button>
                                                                <button id="btnPrintPresenca" class="button button-blue btn-primary" type="button" onclick="imprimirListaPresenca('I')" title="Imprimir"><span class="ico-print icon-white"></span> Chamada</button>
                                                                <button id="btnPrintPresencaExcel" class="button button-green btn-primary" type="button" onclick="imprimirListaPresenca('E')" title="Excel"><span class="ico-download-3 icon-white"></span> Chamada</button>
                                                            </div>
                                                        </div>
                                                        <div style="clear:both; height:5px"></div>
                                                        <div class="body" style="margin-left:0; padding:0">
                                                            <div class="content" style="min-height:153px; margin-top:0">
                                                                <table id="tblAlunosTurma" class="table" cellpadding="0" cellspacing="0" width="100%" style="display: table;">
                                                                    <thead>
                                                                        <tr>
                                                                            <th style="width:30px !important;"><b>Cod.Aluno</b></th>
                                                                            <th style="width:550px !important;"><b>Nome</b></th>
                                                                            <th style="width:100px !important;"><b>Telefone</b></th>
                                                                            <th style="width:100px !important;"><b>Celular</b></th>
                                                                            <th style="width:100px !important;"><b>E-mail</b></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    </tbody>
                                                                    <tfoot></tfoot>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div style="clear:both; height:5px"></div>
                                                    </div>
                                                    <div class="toolbar bottom tar" style="background-color: #f6f6f6;border: 1px solid #DDD;">
                                                        <div class="data-fluid" style="margin: 0 9px 8px 0;">
                                                            <div class="btn-group" style="margin-top: 10px;">
                                                                <button class="btn btn-success" type="button" id="btnSave" name="btnSave"><span class="ico-checkmark"></span> Gravar</button>
                                                                <button class="btn yellow" type="button" id="btnCancelar" name="btnCancelar"><span class="ico-reply"></span> Cancelar</button>
                                                                <button class="btn  btn-success" type="button" title="Novo" name="btnNovo" id="btnNovo" hidden><span class="ico-plus-sign"> </span> Novo</button>
                                                                <button class="btn  btn-success" type="button" title="Alterar" name="btnAlterar" id="btnAlterar" hidden> <span class="ico-edit"> </span> Alterar</button>
                                                                <button class="btn red" type="button" title="Excluir" name="btnExcluir" id="btnExcluir" hidden><span class=" ico-remove"> </span> Excluir</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab2" style="margin-bottom:5px;min-height: 524px;">
                                                    <div style="width:100%; margin:10px 0">
                                                        <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Fila</span>
                                                        <span><b>Qtd.: <span id="qtdAlunoFila">0</span></b></span>
                                                        <hr style="margin:2px 0; border-top:1px solid #ddd">
                                                    </div>
                                                    <div style="border-style: solid;border-width: 1px; padding: 7px;border-color:#c8c8c8;">
                                                        <div style="float:left; width:93%">
                                                            <span>Aluno:</span>
                                                            <input type="text" id="txtAlunoFila" style="width:100%" />
                                                            <input type="hidden" id="txtCodAlunoFila">
                                                        </div>
                                                        <div style="float:left;margin-left: 1%;">
                                                            <button type="button" class="btn dblue" id="btnIncluirAlunoFila" style="line-height:19px; width:75px;margin-top: 16px;background-color: #308698 !important;"> Incluir <span class="icon-arrow-next icon-white"></span></button>
                                                        </div>
                                                        <div style="clear:both;height:5px"></div>
                                                    </div>
                                                    <div style="clear:both; height:5px"></div>
                                                    <div class="body" style="margin-left:0; padding:0">
                                                        <table id="tblAlunosFila" class="table" cellpadding="0" cellspacing="0" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width:30px !important;"><b>Cod.Aluno</b></th>
                                                                    <th style="width:600px !important;"><b>Nome</b></th>
                                                                    <th style="width:200px !important;"><b>Data Pedido</b></th>
                                                                    <th style="width:50px !important;"><b>Ação</b></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div style="clear:both; height:5px"></div>
                                                </div>
                                            </div>
                                            <div style="clear:both"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/highlight/jquery.highlight-4.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/faq.js'></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/jquery.creditCardValidator.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/amcharts.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/pie.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/serial.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/light.js"></script>
        <script type="text/javascript" src="../../js/moment.min.js"></script>
        <script type="text/javascript" src="../../js/jquery.cpfcnpj.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.ajax-combobox/dist/jquery.ajax-combobox.js"></script>
        <script type='text/javascript' src='../../js/plugins/multiselect/jquery.quicksearch.js'></script>        
        <script type='text/javascript' src='../../js/plugins/multiselect/jquery.multi-select.min.js'></script>        
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script src="js/TurmasForm.js" type="text/javascript"></script>
    </body>
    <?php odbc_close($con); ?>
</html>