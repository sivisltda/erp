<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
<style>
    body { font-family: sans-serif; }    
    .selected {
        background: #acbad4 !important;
    }
</style>
<body onload="iniciaTurmasForm();">
    <div class="row-fluid">
        <form>
            <div class="frmhead">
                <div class="frmtext">Turmas</div>
                <div class="frmicon" onClick="parent.FecharBoxCliente()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input id="txtIdAluno" type="hidden" value="<?php echo $_GET["idAluno"]; ?>"/>
                <div style="width:40%; float:left">
                    <span>Nome do Aluno</span>
                    <div style="color: darkblue;margin-top: 7px;font-size: 13px;"><?php echo $_GET['nmAluno']; ?></div>
                </div>
                <div style="width:59%; float:left;margin-left: 1%">
                    <span>Plano:</span>
                    <select id="txtPlano" class="select" style="width:100%"></select>
                </div>
                <div style="clear:both; height:5px"></div>
                <div style="width:100%; margin:10px 0 0">
                    <span style="color:black; font-size:14px; text-shadow:0 1px #FFF; padding-left:5px">Lista de Turmas</span>
                    <hr style="margin:2px 0; border-top:1px solid #DDD">
                </div>
                <div class="body" style="margin-left:0; padding:0">
                    <div class="content" style="min-height:153px; margin-top:0">
                        <table id="tblTurmas" class="table" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="width:140px; text-align:center"><b>Turma</b></th>
                                    <th style="width:22px; text-align:center"><b>Cap.</b></th>
                                    <th style="width:30px; text-align:center"><b>Vagas</b></th>
                                    <th style="width:22px; text-align:center"><b>Mat.</b></th>
                                    <th style="width:100px; text-align:center"><b>Horário</b></th>
                                    <th style="width:100px; text-align:center"><b>Professor</b></th>
                                    <th style="width:93px; text-align:center"><b>Ambiente</b></th>
                                    <th style="width:10px"><b>Ação</b></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div style="clear:both; height:5px"></div>
                <div style="width:100%; margin:10px 0 0">
                    <span style="color:black; font-size:14px; text-shadow:0 1px #FFF; padding-left:5px">Em que o aluno esta matriculado</span>
                    <hr style="margin:2px 0; border-top:1px solid #DDD">
                </div>
                <div class="body" style="margin-left:0; padding:0">
                    <div class="content" style="min-height:153px; margin-top:0">
                        <table id="tblTurmasAluno" class="table" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="width:130px; text-align:center"><b>Turma</b></th>
                                    <th style="width:100px"><b>Plano</b></th>
                                    <th style="width:100px"><b>Professor</b></th>
                                    <th style="width:100px"><b>Horário</b></th>
                                    <th style="width:10px"><b>Ação</b></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../js/plugins.js"></script>
    <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type="text/javascript" src="js/ClientesTurmasForm.js"></script>
    <?php odbc_close($con); ?>
</body>