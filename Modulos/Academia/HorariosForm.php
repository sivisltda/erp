<?php
include "../../Connections/configini.php";
include "form/HorariosFormServer.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <style>
            body { font-family: sans-serif; }
            div label input {
                margin-right: 100px;
            }
            label {
                float: left;
                width: 26px;
                cursor: pointer;
                background: #EFEFEF;
                margin: 2px 0 2px 1px;
                border: 1px solid #D0D0D0;
            }
            label span {
                display: block;
                padding: 3px 0;
                text-align: center;
            }
            label input {
                top: -20px;
                position: absolute;
            }
            input:checked + span {
                color: #FFF;
                background: #186554;
            }
        </style>
    </head>
    <body>
        <form action="HorariosForm.php" name="frmEnviaDados" id="frmEnviaDados" method="POST">
            <div class="frmhead">
                <div class="frmtext">Horários</div>
                <div class="frmicon" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input name="txtId" id="txtId" type="hidden" value="<?php echo $id; ?>"/>
                <div style="height:42px">
                    <span>Descrição:</span>
                    <input name="txtDescricao" id="txtDescricao" type="text" style="width:100%" <?php echo $disabled; ?> value="<?php echo $descricao; ?>"/>
                </div>
                <div style="width:100%; margin:15px 0 5px">
                    <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Faixas</span>
                    <hr style="margin:2px 0; border-top:1px solid #DDD">
                </div>
                <table border="0" cellpadding="3" cellspacing="0" width="100%" class="textosComuns">                
                    <tr>
                        <td>&nbsp;</td>
                        <td><center>Início</center></td>
                    <td><center>Final</center></td>
                    <td style="width:203px"><center>Dia da Semana</center></td>
                    </tr>
                    <tr>
                        <td align="right">1</td>
                        <td><input id="txtFaixa1_ini" name="txtFaixa1_ini" type="text" <?php echo $disabled; ?> value="<?php echo $faixa1_ini; ?>"/></td>
                        <td><input id="txtFaixa1_fim" name="txtFaixa1_fim" type="text" <?php echo $disabled; ?> value="<?php echo $faixa1_fim; ?>"/></td>
                        <td>
                            <label><input id="fx1_0" name="fx1_0" type="checkbox" <?php
                                if (substr($dia_semana1, 0, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>D</span></label>
                            <label><input id="fx1_1" name="fx1_1" type="checkbox" <?php
                                if (substr($dia_semana1, 1, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>S</span></label>
                            <label><input id="fx1_2" name="fx1_2" type="checkbox" <?php
                                if (substr($dia_semana1, 2, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>T</span></label>
                            <label><input id="fx1_3" name="fx1_3" type="checkbox" <?php
                                if (substr($dia_semana1, 3, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>Q</span></label>
                            <label><input id="fx1_4" name="fx1_4" type="checkbox" <?php
                                if (substr($dia_semana1, 4, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>Q</span></label>
                            <label><input id="fx1_5" name="fx1_5" type="checkbox" <?php
                                if (substr($dia_semana1, 5, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>S</span></label>
                            <label><input id="fx1_6" name="fx1_6" type="checkbox" <?php
                                if (substr($dia_semana1, 6, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>S</span></label>
                        </td>
                    </tr>                
                    <tr>
                        <td align="right">2</td>
                        <td><input id="txtFaixa2_ini" name="txtFaixa2_ini" type="text" <?php echo $disabled; ?> value="<?php echo $faixa2_ini; ?>"/></td>
                        <td><input id="txtFaixa2_fim" name="txtFaixa2_fim" type="text" <?php echo $disabled; ?> value="<?php echo $faixa2_fim; ?>"/></td>
                        <td>
                            <label><input id="fx2_0" name="fx2_0" type="checkbox" <?php
                                if (substr($dia_semana2, 0, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>D</span></label>
                            <label><input id="fx2_1" name="fx2_1" type="checkbox" <?php
                                if (substr($dia_semana2, 1, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>S</span></label>
                            <label><input id="fx2_2" name="fx2_2" type="checkbox" <?php
                                if (substr($dia_semana2, 2, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>T</span></label>
                            <label><input id="fx2_3" name="fx2_3" type="checkbox" <?php
                                if (substr($dia_semana2, 3, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>Q</span></label>
                            <label><input id="fx2_4" name="fx2_4" type="checkbox" <?php
                                if (substr($dia_semana2, 4, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>Q</span></label>
                            <label><input id="fx2_5" name="fx2_5" type="checkbox" <?php
                                if (substr($dia_semana2, 5, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>S</span></label>
                            <label><input id="fx2_6" name="fx2_6" type="checkbox" <?php
                                if (substr($dia_semana2, 6, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>S</span></label>
                        </td>                    
                    </tr>                
                    <tr>
                        <td align="right">3</td>
                        <td><input id="txtFaixa3_ini" name="txtFaixa3_ini" type="text" <?php echo $disabled; ?> value="<?php echo $faixa3_ini; ?>"/></td>
                        <td><input id="txtFaixa3_fim" name="txtFaixa3_fim" type="text" <?php echo $disabled; ?> value="<?php echo $faixa3_fim; ?>"/></td>
                        <td>
                            <label><input id="fx3_0" name="fx3_0" type="checkbox" <?php
                                if (substr($dia_semana3, 0, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>D</span></label>
                            <label><input id="fx3_1" name="fx3_1" type="checkbox" <?php
                                if (substr($dia_semana3, 1, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>S</span></label>
                            <label><input id="fx3_2" name="fx3_2" type="checkbox" <?php
                                if (substr($dia_semana3, 2, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>T</span></label>
                            <label><input id="fx3_3" name="fx3_3" type="checkbox" <?php
                                if (substr($dia_semana3, 3, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>Q</span></label>
                            <label><input id="fx3_4" name="fx3_4" type="checkbox" <?php
                                if (substr($dia_semana3, 4, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>Q</span></label>
                            <label><input id="fx3_5" name="fx3_5" type="checkbox" <?php
                                if (substr($dia_semana3, 5, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>S</span></label>
                            <label><input id="fx3_6" name="fx3_6" type="checkbox" <?php
                                if (substr($dia_semana3, 6, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>S</span></label>
                        </td>                    
                    </tr>                
                    <tr>
                        <td align="right">4</td>
                        <td><input id="txtFaixa4_ini" name="txtFaixa4_ini" type="text" <?php echo $disabled; ?> value="<?php echo $faixa4_ini; ?>"/></td>
                        <td><input id="txtFaixa4_fim" name="txtFaixa4_fim" type="text" <?php echo $disabled; ?> value="<?php echo $faixa4_fim; ?>"/></td>
                        <td>
                            <label><input id="fx4_0" name="fx4_0" type="checkbox" <?php
                                if (substr($dia_semana4, 0, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>D</span></label>
                            <label><input id="fx4_1" name="fx4_1" type="checkbox" <?php
                                if (substr($dia_semana4, 1, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>S</span></label>
                            <label><input id="fx4_2" name="fx4_2" type="checkbox" <?php
                                if (substr($dia_semana4, 2, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>T</span></label>
                            <label><input id="fx4_3" name="fx4_3" type="checkbox" <?php
                                if (substr($dia_semana4, 3, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>Q</span></label>
                            <label><input id="fx4_4" name="fx4_4" type="checkbox" <?php
                                if (substr($dia_semana4, 4, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>Q</span></label>
                            <label><input id="fx4_5" name="fx4_5" type="checkbox" <?php
                                if (substr($dia_semana4, 5, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>S</span></label>
                            <label><input id="fx4_6" name="fx4_6" type="checkbox" <?php
                                if (substr($dia_semana4, 6, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>S</span></label>
                        </td>                                        
                    </tr>                
                    <tr>
                        <td align="right">5</td>
                        <td><input id="txtFaixa5_ini" name="txtFaixa5_ini" type="text" <?php echo $disabled; ?> value="<?php echo $faixa5_ini; ?>"/></td>
                        <td><input id="txtFaixa5_fim" name="txtFaixa5_fim" type="text" <?php echo $disabled; ?> value="<?php echo $faixa5_fim; ?>"/></td>
                        <td>
                            <label><input id="fx5_0" name="fx5_0" type="checkbox" <?php
                                if (substr($dia_semana5, 0, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>D</span></label>
                            <label><input id="fx5_1" name="fx5_1" type="checkbox" <?php
                                if (substr($dia_semana5, 1, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>S</span></label>
                            <label><input id="fx5_2" name="fx5_2" type="checkbox" <?php
                                if (substr($dia_semana5, 2, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>T</span></label>
                            <label><input id="fx5_3" name="fx5_3" type="checkbox" <?php
                                if (substr($dia_semana5, 3, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>Q</span></label>
                            <label><input id="fx5_4" name="fx5_4" type="checkbox" <?php
                                if (substr($dia_semana5, 4, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>Q</span></label>
                            <label><input id="fx5_5" name="fx5_5" type="checkbox" <?php
                                if (substr($dia_semana5, 5, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>S</span></label>
                            <label><input id="fx5_6" name="fx5_6" type="checkbox" <?php
                                if (substr($dia_semana5, 6, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/><span>S</span></label>
                        </td>                                        
                    </tr>                
                </table>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == "") { ?>
                        <?php
                        if ($_POST["txtId"] == "") {
                            $btnType = "onClick=\"parent.FecharBox()\"";
                        } else {
                            $btnType = "type=\"submit\" name=\"bntAlterar\"";
                        }
                        ?>
                        <button class="btn green" type="submit" name="bntSave" onClick="return validaForm()"><span class="ico-checkmark"></span> Gravar</button>
                        <button class="btn yellow" title="Cancelar" <?php echo $btnType; ?>><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn green" type="submit" name="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" type="submit" name="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                        <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
        </form>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>                
        <script type='text/javascript' src='js/HorariosForm.js'></script>        
        <?php odbc_close($con); ?>
    </body>
</html>