<?php include "../../Connections/configini.php"; ?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<style>
    body { font-family: sans-serif; }    
</style>
<body onload="iniciaAgendamentoDccForm();">
    <div class="row-fluid">
        <form>
            <div class="frmhead">
                <div class="frmtext">Agendamento de Cancelamento</div>
                <div class="frmicon" onClick="parent.FecharBoxCliente()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont" style="height:255px;">
                <div style="width:65%; float:left;">
                    <span>Data de cancelamento desta ocorrência:</span>
                    <input type="text" id="txtDtCancelDcc" class="datepicker inputCenter" style="width: 80%;"/>
                </div>
                <div style="width:35%; float:left;">
                    <span>Data de vencimento:</span>
                    <input type="text" id="txtDtVencPlano" class="inputCenter" disabled/>
                </div>
                <div style="clear:both; height:13px"></div>
                <div style="width:100%; float:left;">
                    <span>Plano:</span>
                    <input type="hidden" id="txtIdPlano"/>
                    <input type="text" id="txtPlano" disabled/>
                </div>
                <div style="clear:both; height:27px"></div>
            </div>
            <div class="spanBNT" style="left: 0px;margin: 0px;">
                <div class="toolbar bottom tar">
                    <div class="data-fluid" style="overflow:hidden;margin-top: 4px;">
                        <div class="btn-group">
                            <?php if ($ckb_aca_cancelar_dcc_ > 0) { ?>
                                <button class="btn green" type="button" id="btnGravarAgendDcc" title="Gravar"><span class="ico-checkmark"></span> Gravar</button>
                                <button class="btn red" type="button" id="btnExcluirAgendDcc" title="Excluir" hidden> <span class="ico-remove"></span> Excluir</button>
                            <?php } ?>
                            <button class="btn yellow" title="Cancelar" onClick="parent.FecharBoxCliente()"><span class="ico-reply"></span> Cancelar</button>
                            <button class="btn blue" type="button" id="btnReciboDcc"  title="Imprimir Recibo" hidden><span style="font-size: 14px; padding: 0px 0 0px 1px;" class="ico-print"></span> </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type="text/javascript" src="../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type='text/javascript' src='js/ClientesAgendamentoDccForm.js'></script>
    <?php odbc_close($con); ?>
</body>
