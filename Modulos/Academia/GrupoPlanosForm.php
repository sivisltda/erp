<?php
include "../../Connections/configini.php";
include "form/GrupoPlanosFormServer.php";
$mdl_seg_ = returnPart($_SESSION["modulos"], 14);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <style>
            body { font-family: sans-serif; }
            div label input {
                margin-right: 100px;
            }
            label {
                float: left;
                width: 26px;
                cursor: pointer;
                background: #EFEFEF;
                margin: 2px 0 2px 1px;
                border: 1px solid #D0D0D0;
            }
            label span {
                display: block;
                padding: 3px 0;
                text-align: center;
            }
            label input {
                top: -20px;
                position: absolute;
            }
            input:checked + span {
                color: #FFF;
                background: #186554;
            }
        </style>
    </head>
    <body>
        <form action="GrupoPlanosForm.php" name="frmEnviaDados" id="frmEnviaDados" method="POST">
            <div class="frmhead">
                <div class="frmtext">Grupo de Planos</div>
                <div class="frmicon" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input name="txtId" id="txtId" type="hidden" value="<?php echo $id; ?>"/>
                <div style="height:42px; width: 70%; float: left;">
                    <span>Descrição:</span>
                    <input name="txtDescricao" id="txtDescricao" type="text" style="width:100%" <?php echo $disabled; ?> value="<?php echo $descricao; ?>"/>
                </div>
                <div style="height:42px; width: 29%; margin-left: 1%; float: left;">
                    <span>Código:</span>
                    <input name="txtCod" id="txtCod" type="text" style="width:100%" <?php echo $disabled; ?> value="<?php echo $cod; ?>"/>
                </div>
                <div style="clear:both;"></div>
                <div style="display: flex; align-items: center;">
                    <input type="checkbox" class="input-medium" id="txtInativo" name="txtInativo" <?php echo $disabled; ?> <?php echo ($inativo == 1 ? "checked" : ""); ?> value="1"/> <div style="margin-left: 5px;">Inativo</div>                     
                </div>                    
                <div style="display: flex; align-items: center;">                    
                    <input type="checkbox" class="input-medium" id="txtPesJur" name="txtPesJur" <?php echo $disabled; ?> <?php echo ($pesJur == 1 ? "checked" : ""); ?> value="1"/> <div style="margin-left: 5px;">Planos dedicados às pessoas jurídicas</div>                    
                    <?php if ($mdl_seg_ > 0) {?>
                    <input type="checkbox" class="input-medium" id="txtAcessorios" name="txtAcessorios" <?php echo $disabled; ?> style="margin-left: 10px;" <?php echo ($acessorios == 1 ? "checked" : ""); ?> value="1"/> <div style="margin-left: 5px;">Personalização de Acessórios </div>                    
                    <?php } ?>
                </div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == "") { ?>
                        <?php
                        if ($_POST["txtId"] == "") {
                            $btnType = "onClick=\"parent.FecharBox()\"";
                        } else {
                            $btnType = "type=\"submit\" name=\"bntAlterar\"";
                        }
                        ?>
                        <button class="btn green" type="submit" name="bntSave" onClick="return validaForm()"><span class="ico-checkmark"></span> Gravar</button>
                        <button class="btn yellow" title="Cancelar" <?php echo $btnType; ?>><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn green" type="submit" name="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" type="submit" name="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                        <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
        </form>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>             
        <script type='text/javascript' src='js/GrupoPlanosForm.js'></script>        
        <?php odbc_close($con); ?>
    </body>
</html>