<?php 
    include "../../Connections/configini.php";
    include "form/ConveniosFormServer.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>        
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <style>            
            #tblConvenios td {
                padding: 5px;
                line-height: 16px;
            }
        </style>
    </head>
    <body>
        <?php if ($imprimir == 0) { ?>
            <div id="loader"><img src="../../img/loader.gif"/></div>
        <?php } ?>
        <div class="wrapper">
            <?php if ($imprimir == 0) {
                include("../../menuLateral.php");
            } ?>
            <div class="body">
                <?php if ($imprimir == 0) {
                    include("../../top.php");
                } ?>
                <div class="content">
                    <?php if ($imprimir !== 0) { $visible = "hidden"; } ?>
                    <div class="page-header" <?php echo $visible; ?>>
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1><?php echo ($mdl_clb_ == 1 ? "Configuração" : "Academia")?><small>Convênios</small></h1>
                    </div>                                       
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="margin-top: 15px;float:left;">
                                    <div style="float:left">
                                        <form method="POST">
                                            <button id="btnNovo" class="button button-green btn-primary" type="button" onclick="AbrirBox(0,1)"><span class="ico-file-4 icon-white"></span></button>
                                            <button id="btnPrint" class="button button-blue btn-primary" type="button" onClick="AbrirBox(0,0)"><span class="ico-print icon-white"></span></button>
                                            <input id="imprimir" name="imprimir" type="hidden" value="<?php echo $imprimir; ?>"/>
                                        </form>
                                    </div>
                                </div>
                                <div style="float:right;width: 40%;display: flex;align-items: center;justify-content: flex-end;">
                                    <div style="float:left;margin-left: 1%;">                                    
                                        <div width="100%">Estado:</div>
                                        <select id="txtStatus" class="select">
                                            <option value="0" selected>Ativos</option>                                        
                                            <option value="1">Inativos</option>                                                                            
                                        </select>                                           
                                    </div>                                                                           
                                    <div style="float:left;margin-left: 1%;width: 56%;">
                                        <div width="100%">Busca:</div>
                                        <input name="txtBuscar" id="txtBuscar" type="text" value="<?php echo $_GET['Search']; ?>" style="width:100%; height:31px"/>
                                    </div>
                                    <div style="float:left;margin-left: 1%;width: 7%;">
                                        <div width="100%" style="opacity:0;">Busca:</div>
                                        <button class="button button-turquoise btn-primary" type="button" name="btnPesquisar" id="btnPesquisar" title="Buscar"><span class="ico-search icon-white"></span></button>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead" <?php echo $visible; ?>>
                        <div class="boxtext">Convênios</div>
                    </div>
                    <div <?php if ($imprimir == 0) { echo "class=\"boxtable\""; } ?>>
                        <?php if ($imprimir == 1) {
                            $titulo_pagina = "RELATÓRIO DE CONSULTA<br/>CONVÊNIOS";
                            include "../Financeiro/Cabecalho-Impressao.php";
                        } ?>
                        <table <?php if ($imprimir == 0) { echo "class=\"table\""; } else { echo "border=\"1\""; } ?> style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tblConvenios">
                            <thead>
                                <tr>
                                    <th>Descrição</th>
                                    <th width="20%">Tipo</th>
                                    <th width="20%">Status</th>
                                    <?php if($imprimir == 0){ ?>
                                        <th width="5%"><center>Ação:</center></th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script> 
        <script type="text/javascript" src="../../js/util.js"></script>       
        <script src="js/ConveniosList.js" type="text/javascript"></script>
        <?php odbc_close($con); ?>
    </body>
</html>
