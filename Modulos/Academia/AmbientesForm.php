<?php
include "../../Connections/configini.php";
include "form/AmbientesFormServer.php";
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<link href="../../css/formularios.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
<style>
    .selected {
        background: #acbad4 !important;
    }
</style>
<body>
    <div class="row-fluid">
        <form action="AmbientesForm.php" name="frmAmbientes" id="frmAmbientes" method="POST">
            <div class="frmhead">
                <div class="frmtext">Ambientes</div>
                <div class="frmicon" onClick="parent.FecharBox(1)">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input id="txtId" name="txtId" type="hidden" value="<?php echo $id; ?>"/>
                <div style="width:100%">
                    <span>Título:</span>
                    <input id="txtTitulo" name="txtTitulo" <?php echo $disabled; ?> type="text" value="<?php echo $titulo; ?>" class="input-medium" style="width:100%"/>
                </div>
                <div style="width:100%; margin-top:5px">
                    <span>Descrição:</span>
                    <input id="txtDescricao" name="txtDescricao" <?php echo $disabled; ?> type="text" value="<?php echo $descricao; ?>" class="input-medium" style="width:100%"/>
                </div>
                <div style="width:100%; margin-top:10px">                
                    <select name="itemsGrupo[]" multiple="multiple" id="mscGrupos">
                        <?php
                        $idUser = $id == "" ? "null" : $id;
                        $cur = odbc_exec($con, "select id_catraca, nome_catraca
                        ,case when id_catraca in(select catraca from sf_ambientes_itens where ambiente = " . $idUser . ") then 'S' else 'N' end Grp
                        from sf_catracas order by 2 asc") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) {
                            ?>
                            <option value="<?php echo $RFP["id_catraca"] ?>"<?php
                            if ($RFP["Grp"] == "S") {
                                echo "SELECTED";
                            }
                            ?>><?php echo utf8_encode($RFP["nome_catraca"]) ?></option>
                                <?php } ?>                                                                                    
                    </select>
                </div>                         
                <div style="clear:both"></div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == "") { ?>
                        <button class="btn green" type="submit" name="bntSave" onClick="return validaForm()"><span class="ico-checkmark"></span> Gravar</button>
                        <?php if ($_POST["txtId"] == "") { ?>
                            <button class="btn yellow" onClick="parent.FecharBox(1)"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                            <button class="btn yellow" type="submit" name="bntAlterar"><span class="ico-reply"></span> Cancelar</button>
                            <?php
                        }
                    } else {
                        ?>
                        <button class="btn green" type="submit" name="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" type="submit" name="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                        <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
        </form>
        <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/jquery.priceformat.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/plugins/multiselect/jquery.multi-select.min.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>
        <script type="text/javascript" src="js/AmbientesForm.js"></script>        
    </div>
    <?php odbc_close($con); ?>
</body>