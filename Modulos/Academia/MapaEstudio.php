<?php include "./../../Connections/configini.php"; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/informacoesGerenciais.css"/>
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <style type="text/css">
            #tbAmbiente thead {
                position: relative;
                display: block; /*seperates the header from the body allowing it to be positioned*/
                overflow: hidden;
                width: 100%;
            }
            #tbAmbiente thead tr {
                border: 1px solid gray;
                width: calc(100% - 49px);
            }
            #tbAmbiente thead th {
                width: 14.28%;
                height: 12px;
                border: 1px solid gray;
            }
            #tbAmbiente thead th:nth-child(1) {/*first cell in the header*/
                width: 40px!important;
                position: relative;
                display: grid; /*seperates the first cell in the header from the header*/
                background-color: #eee;
            }
            /*tbody*/
            #tbodyAmbiente {
                position: relative;
                display: block; /*seperates the tbody from the header*/
                width: 100%;
            }
            /*calc(100% - 49px)*/
            #tbodyAmbiente tr{
              width: calc(100% - 49px);
            }
            #tbodyAmbiente td {
                width: 14.28%;
                border: 1px dotted gray;
                color: #005580;
                text-align: center;
                margin-top: 7px;
                margin-right: 5px;
            }
            #tbodyAmbiente tr td:nth-child(1) {  /*the first cell in each tr*/
                width: 40px !important;
                position: relative;
                display: grid; /*seperates the first column from the tbody*/
                height: 20px;
                background-color: #eee;
                padding-right: 0px;
            }

            div label input {
                margin-right: 100px;
            }
            label {
                float: left;
                width: 26px;
                cursor: pointer;
                background: #EFEFEF;
                margin: 2px 0 2px 1px;
                border: 1px solid #D0D0D0;
            }
            label span {
                display: block;
                padding: 3px 0;
                text-align: center;
            }
            label input {
                top: -20px;
                position: absolute;
            }
            input:checked + span {
                color: #FFF;
                background: #186554;
            }
            .ibtn{
                display: none;
            }
            .cell-disabled{
                background-color: #EEEEEE;
            }
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"><span class="ico-arrow-right"></span></div>
                        <h1>Academia<small>Informações Gerenciais</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="block">
                                <div id="formulario">
                                    <div class="boxhead">
                                        <div class="boxtext">Controle de Estúdio</div>
                                        <div class="boxtable" style="margin-bottom:15px; background:#FFF">
                                            <form id="frmMain" name="frmMain">
                                                <div style="float:left;width: 39%;">
                                                    <div style="float:left; width:100%;">
                                                        <span>Professor:</span>
                                                        <select id="txtProfessorTurma" name="txtProfessorTurma" class="input-medium" style="width:100%"></select>
                                                    </div>
                                                </div>
                                                <div style="float:left;width: 60%;margin-left: 1%;">
                                                        <input type="hidden" id="txtId" name="txtId" value="">
                                                        <div style="float:left; width:90%;">
                                                            <span>Cliente:</span>
                                                            <input type="text" id="txtCliente" style="width:100%"/>
                                                            <input type="hidden" id="txtCodCliente" name="txtCodCliente" value="">
                                                        </div>
                                                        <div style="float:left; width:9%; margin-left:1%;margin-top: 15px;">
                                                            <a id="btn_new" onclick="salvar();" href="javascript:;" class="btn green-haze save" title="Salvar" style="padding: 2px 8px;background:#005683;"><span class="ico-plus"></span></a>
                                                            <a id="btn_edt" onclick="editar();" href="javascript:;" class="btn green-haze save" title="Editar" style="padding: 2px 6px;background:#0099cc;display: none;"><span class="ico-pencil"></span></a>
                                                            <a id="btn_exr" onclick="excluir();" href="javascript:;" class="btn red-haze cancel" title="Excluir" style="padding: 2px 6px;background:red;display: none;"><span class="ico-cancel"></span></a>
                                                            <a id="btn_sav" onclick="salvar();" href="javascript:;" class="btn green-haze save" title="Salvar" style="padding: 2px 6px;background:#5bb75b;display: none;"><span class="ico-checkmark"></span></a>
                                                            <a id="btn_cnl" onclick="cancelar();" href="javascript:;" class="btn red-haze cancel" title="Cancelar" style="padding: 2px 6px;background:#FFAA31;display: none;"><span class="ico-reply"></span></a>
                                                        </div>
                                                        <div style="float:left; width:10%; margin-top: 4px;">
                                                            <input id="txtFaixa1_ini" name="txtFaixa1_ini" type="text" value=""/>
                                                        </div>
                                                        <div style="float:left; width:36%; margin-left:1%; margin-top: 4px;">
                                                            <label><input id="fx1_0" name="fx1_0" class="ibtn" type="checkbox" value="1" /><span>D</span></label>
                                                            <label><input id="fx1_1" name="fx1_1" class="ibtn" type="checkbox" value="1" /><span>S</span></label>
                                                            <label><input id="fx1_2" name="fx1_2" class="ibtn" type="checkbox" value="1" /><span>T</span></label>
                                                            <label><input id="fx1_3" name="fx1_3" class="ibtn" type="checkbox" value="1" /><span>Q</span></label>
                                                            <label><input id="fx1_4" name="fx1_4" class="ibtn" type="checkbox" value="1" /><span>Q</span></label>
                                                            <label><input id="fx1_5" name="fx1_5" class="ibtn" type="checkbox" value="1" /><span>S</span></label>
                                                            <label><input id="fx1_6" name="fx1_6" class="ibtn" type="checkbox" value="1" /><span>S</span></label>
                                                        </div>
                                                        <div style="float:left; width:42%; margin-left:1%; margin-top: 4px;">                                                            
                                                            <select id="txtProfessorTurmaHr" name="txtProfessorTurmaHr" class="input-medium" style="width:100%"></select>
                                                        </div>
                                                        <div style="float:left; width:9%; margin-left:1%; margin-top: 5px;">
                                                            <a id="btn_hst" onclick="historico();" href="javascript:;" class="btn red-haze cancel" title="Histórico" style="padding: 2px 8px;background:#186554;"><span class="ico-file-4"></span></a>
                                                        </div>
                                                    </div>
                                                <div style="clear:both"></div>
                                            </form>
                                        </div>
                                    </div>
                                    <span id="tbTurma">
                                        <div class="boxhead">
                                            <div class="boxtext" style="position:relative">
                                                Agendamento
                                                <div style="top:4px; right:2px; line-height:1; position:absolute">
                                                    <button type="button" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="imprimirMapaAmbiente();">
                                                        <span class="ico-print icon-white"></span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="boxtable">
                                            <table id="tbAmbiente" style="width:100%;">
                                                <thead><tr></tr></thead>
                                                <tbody id="tbodyAmbiente"> </tbody>
                                            </table>
                                            <div style="clear:both;height: 5px;"></div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="data-fluid tabbable" style="margin-top:10px; margin-bottom:15px">
                        <div class="tab-content" style="border:1px solid #DDD; border-top:0; padding-top:10px; background:#F6F6F6">
                            <div class="tab-pane" id="tab7"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../js/graficos.js"></script>        
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/jquery.priceformat.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/multiselect/jquery.multi-select.min.js'></script>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/charts.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type='text/javascript' src="../../js/numeral.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>         
        <script type="text/javascript" src="js/MapaEstudio.js"></script>
    </body>
    <?php odbc_close($con); ?>
</html>
