<?php include "./../../Connections/configini.php";
if($ckb_aca_inf_gerenciais_ == 0){
    header('Location: ../../Index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/informacoesGerenciais.css"/>
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>                
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php");
                if($ckb_aca_igCaixa_ == 1) {
                    $tab = 3;
                }elseif($ckb_aca_igStatus_ == 1){
                    $tab = 1;
                }elseif($ckb_aca_Contrato_ == 1){
                    $tab = 2;
                }elseif($ckb_aca_filAvn_ == 1){
                    $tab = 4;
                }elseif($ckb_aca_renovacoes_ == 1){
                    $tab = 5;
                }elseif($ckb_aca_dcc_ == 1){
                    $tab = 6;
                }elseif($ckb_aca_acesso_ == 1){
                    $tab = 7;
                }elseif($ckb_aca_bol_ == 1){
                    $tab = 8;                    
                }elseif($ckb_aca_pix_ == 1){
                    $tab = 10;                    
                }?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"><span class="ico-arrow-right"></span></div>
                        <h1>Informações Gerenciais<small>Gestão dos Dados</small></h1>
                    </div>
                    <div class="data-fluid tabbable" style="margin-top:10px; margin-bottom:15px">
                        <ul class="nav nav-tabs" style="margin-bottom:0px" id="tabGerenciais">
                            <?php if($ckb_aca_igCaixa_ == 1){ ?>
                            <li class="<?php echo ($tab == 3 && !isset($_GET['dash']) ? "active" : "")?>"><a href="#tab3" data-toggle="tab" onClick="showTab(3)"><b>CAIXA</b></a></li>
                            <?php } if ($mdl_aca_ == 1 || $_SESSION["mod_emp"] == 1) {
                            if($mdl_aca_ == 1 && $ckb_aca_igStatus_ == 1){ ?>
                            <li class="<?php echo ($tab == 1 ? "active" : "")?>"><a href="#tab1" data-toggle="tab" onClick="showTab(1)"><b>STATUS DOS CLIENTES</b></a></li>
                            <?php } if($ckb_aca_Contrato_ == 1) { ?>
                            <li class="<?php echo ($tab == 2 ? "active" : "")?>"><a href="#tab2" data-toggle="tab" onClick="showTab(2)"><b>CONTRATO</b></a></li>
                            <?php } if($ckb_aca_filAvn_ == 1) { ?>
                            <li class="<?php echo ($tab == 4 ? "active" : "")?>"><a href="#tab4" data-toggle="tab" onClick="showTab(4)"><b>FILTRO AVANÇADO</b></a></li>
                            <?php } if($ckb_aca_renovacoes_ == 1) { ?>
                            <li class="<?php echo ($tab == 5 ? "active" : "")?>"><a href="#tab5" data-toggle="tab" onClick="showTab(5)"><b>RENOVAÇÕES</b></a></li>
                            <?php } if($ckb_aca_dcc_ == 1) { ?>
                            <li class="<?php echo ($tab == 6 || isset($_GET['dash']) ? "active" : "")?>"><a href="#tab6" data-toggle="tab" onClick="showTab(6)"><b>DCC</b></a></li>
                            <?php } if($ckb_aca_bol_ == 1) { ?>                            
                            <li class="<?php echo ($tab == 8 ? "active" : "")?>"><a href="#tab8" data-toggle="tab" onClick="showTab(8)"><b>BOL</b></a></li>
                            <?php } if($ckb_aca_pix_ == 1) { ?>                            
                            <li class="<?php echo ($tab == 10 ? "active" : "")?>"><a href="#tab10" data-toggle="tab" onClick="showTab(10)"><b>PIX</b></a></li>
                            <?php } if($mdl_wha_ == 1) { ?>                                                        
                            <li class="<?php echo ($tab == 9 ? "active" : "")?>"><a href="#tab9" data-toggle="tab" onClick="showTab(9)"><b>WHATSAPP</b></a></li>
                            <?php }} if ($mdl_seg_ == 0 && $mdl_aca_ == 1 && $ckb_aca_acesso_ == 1) { ?>
                            <li class="<?php echo ($tab == 7 ? "active" : "")?>"><a href="#tab7" data-toggle="tab" onClick="showTab(7)"><b>ACESSO</b></a></li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content" style="border:1px solid #DDD; border-top:0; padding-top:10px; background:#F6F6F6">
                            <?php if ($mdl_aca_ == 1 || $_SESSION["mod_emp"] == 1) {
                            if($ckb_aca_igStatus_ == 1) { ?>
                            <div class="tab-pane <?php echo ($tab == 1 ? "active" : "")?>" id="tab1">
                                <div style="width:100%">
                                    <div class="boxhead">
                                        <div class="boxtext">Status dos Clientes</div>
                                    </div>
                                    <div class="boxchart">
                                        <div style="float:left; width:50%">
                                            <div style="float:left; background-color: #f9f9f9; width:calc(100% - 17px); margin:10px 5px 0 10px; border:1px solid #D8D8D8">
                                                <div id="tablediv" style="position:relative; height:270px">
                                                    <table id="tbStatusAlunos" class="table mtStatus" style="float:none" cellpadding="0" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>Status</th>
                                                                <th style="width: 10%"><center>Clientes</center></th>
                                                                <th style="width: 10%"><center>Contratos</center></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                    <div style="width:calc(100% - 22px); border:1px solid #FFF; line-height:30px; position:absolute; bottom:0; padding:0px 10px; background:#E9E9E9; font-size:16px; color:#09C; text-align:right"><b>Total:
                                                    <span id="lblTotalStatusAlunos">0</span></b></div>
                                                </div>
                                                <div id="chartdiv"></div>
                                            </div>
                                        </div>
                                        <div style="float:left; width:50%">
                                            <div style="float:left; background-color: #f9f9f9; width:calc(100% - 17px); margin:10px 10px 0 5px; border:1px solid #D8D8D8">
                                                <div id="tablediv2" style="width:calc(100% - 251px); height:270px;">
                                                    <table id="tbStatusNRR" class="table mtStatus" style="float:none;" cellpadding="0" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>Clientes Cadastrados:</th>
                                                                <th style="width: 10%"><center>Clientes</center></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </div>
                                                <div id="chartdiv2" style="width: 250px;"></div>
                                            </div>
                                        </div>
                                        <div style="float:left; width:50%">
                                            <div style="float:left; width:calc(100% - 17px); margin:10px 5px 0 10px; border:1px solid #D8D8D8">
                                                <div id="tablediv3">
                                                    <table id="tbStatusVis" class="table mtStatus" style="float:none" cellpadding="0" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>Status</th>
                                                                <th style="width: 10%"><center>Clientes</center></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </div>
                                                <div id="chartdiv3"></div>
                                            </div>
                                        </div>
                                        <div style="float:left; width:50%">
                                            <div style="float:left; width:calc(100% - 17px); margin:10px 10px 0 5px; border:1px solid #D8D8D8">
                                                <div id="tablediv4">
                                                    <table id="tbStatusAtivo" class="table mtStatus" style="float:none" cellpadding="0" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>Status</th>
                                                                <th style="width: 10%"><center>Clientes</center></th>
                                                                <th style="width: 10%"><center>Contratos</center></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </div>
                                                <div id="chartdiv4"></div>
                                            </div>
                                        </div>
                                        <div style="float:left; width:50%">
                                            <div style="float:left; width:calc(100% - 17px); margin:10px 5px 10px 10px; border:1px solid #D8D8D8">
                                                <table id="tbInfoAdd" class="table mtStatus" style="float:none" cellpadding="0" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Status</th>
                                                            <th style="width: 10%"><center>Clientes</center></th>
                                                            <th style="width: 10%"><center>Contratos</center></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div style="clear:both"></div>
                                    </div>
                                </div>
                            </div>
                            <?php } if($ckb_aca_Contrato_ == 1) { ?>
                            <div class="tab-pane <?php echo ($tab == 2 ? "active" : "")?>" id="tab2">
                                <div style="width:100%">
                                    <div class="boxhead">
                                        <div class="boxtext">Contratos</div>
                                    </div>
                                    <div class="boxtable" style="margin-bottom:5px; background:#FFF">
                                        <div style="float:left; width:100%">
                                            <div style="width:10%; float:left;">
                                                <span>Tipo:</span>
                                                <select id="statusContratos">
                                                  <option value="0">Todos</option>
                                                  <option value="1">Ativos</option>
                                                </select>
                                            </div>
                                            <div style="float: left;margin-left: 1%;">
                                                <label for="mscGrupo">Grupo</label>
                                                <select name="itemsGrupo[]" id="mscGrupo" multiple="multiple" class="select" style="min-width:160px;min-height:31px!important;" class="input-medium">
                                                    <?php $query = "select id_contas_movimento,descricao from dbo.sf_contas_movimento where tipo = 'L' and inativa = 0";
                                                    $cur = odbc_exec($con, $query) or die(odbc_errormsg());
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo $RFP['id_contas_movimento']; ?>"><?php echo utf8_encode($RFP['descricao']); ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div style="float:left; width:8.7%; margin-left:1%; padding-top:15px">
                                                <input type="button" title="Buscar" class="button button-turquoise btn-primary" style="line-height:17px" onclick="BuscarContratos()" value="Buscar"/>
                                            </div>
                                            <div style="clear:both;height: 10px;"></div>
                                            <div style="float:left; width:100%; border:1px solid #D8D8D8">
                                                <div class="boxhead">
                                                    <div class="boxtext" style="position:relative">Resultados</div>
                                                </div>
                                                <div style="float:left; width:60%; height:400px" id="chartdiv5"></div>
                                                <div style="float:right; width:40%; height:400px" id="chartdiv6"></div>
                                            </div>
                                        </div>
                                        <div style="clear:both"></div>
                                    </div>
                                </div>
                            </div>
                            <?php } if($ckb_aca_igCaixa_ == 1){ ?>
                            <div class="tab-pane <?php echo ($tab == 3 && !isset($_GET['dash']) ? "active" : "")?>" id="tab3">
                                <div style="width:100%">
                                    <div class="boxhead">
                                        <div style="width:50%;float: left;text-align: right;" class="boxtext">
                                            Caixa
                                        </div>
                                        <div style="float:right; padding: 2px;">
                                            <input type="button" title="Hoje" class="button button-blue btn-primary" onclick="botaoHoje()" name="btnhoje" id="btnhoje" value="Hoje"/>
                                            <input type="button" title="Mês Atual" class="button button-blue btn-primary" onclick="botaoMes()" name="btnmes" id="btnmes" value="Mês"/>
                                            <input type="button" title="Buscar" class="button button-turquoise btn-primary" onclick="BuscarFinancas()" name="btnfind" id="btnfind" value="Buscar"/>
                                        </div>
                                        <div style="clear:both"></div>
                                    </div>
                                    <div id="box04" class="boxchart">
                                        <div style="padding:10px 10px 0">
                                            <div style="float:left; width:100%">                                                
                                                <div style="width:11%; float:left;">
                                                    <span>Banco:</span>
                                                    <select id="txtBanco" style="height: 31px;">
                                                        <option value="">TODOS</option>
                                                        <?php $cur = odbc_exec($con, "select id_bancos,razao_social from sf_bancos where ativo = 0 and tipo_bclc = 1 order by razao_social") or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                                            <option value="<?php echo utf8_encode($RFP['id_bancos']); ?>"><?php echo utf8_encode($RFP['razao_social']) ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div style="width:15%; float:left; margin-left:0.5%">
                                                    <span>Operador:</span>
                                                    <select id="txtOperador" style="height: 31px;">
                                                        <?php if($ckb_out_todos_operadores_ == 1){ ?>
                                                            <option value="">TODOS</option>
                                                        <?php
                                                      } $cur = odbc_exec($con, "select id_usuario,UPPER(nome) nome,login_user from sf_usuarios where " . ($ckb_out_todos_operadores_ != 1 ? " id_usuario = " . $_SESSION["id_usuario"] . " AND " : "") . " inativo = 0 and id_usuario > 1 order by nome") or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                                            <option value="<?php echo utf8_encode($RFP['login_user']); ?>"><?php echo formatNameCompact(utf8_encode($RFP['nome'])); ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div style="width:17%; float: left;margin-left: 0.5%;">
                                                    <span>Forma de Pagamento:</span>
                                                    <select id="mscPagamento" multiple="multiple" class="select" style="height: 31px; width: 100%;">
                                                        <?php $query = "select id_tipo_documento, descricao
                                                        from sf_tipo_documento where inativo = 0 and s_tipo in  ('A','C') order by descricao";
                                                        $cur = odbc_exec($con, $query) or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                                            <option value="<?php echo $RFP['id_tipo_documento']; ?>"><?php echo utf8_encode($RFP['descricao']); ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div style="width:14%; float:left;margin-left: 0.5%;">
                                                    <span>Filial de Clientes:</span>
                                                    <select id="mscFilialCli" multiple="multiple" class="select" style="height: 31px; width: 100%;">
                                                        <?php $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'") or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                                            <option value="<?php echo utf8_encode($RFP['id_filial']); ?>"><?php echo utf8_encode($RFP['numero_filial']) . " - " . utf8_encode($RFP['Descricao']); ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>                                                
                                                <div style="width:12%; float:left;margin-left: 0.5%;">
                                                    <span>Grupo de Clientes:</span>
                                                    <select id="mscGrupoCli" multiple="multiple" class="select" style="height: 31px; width: 100%;">
                                                        <?php $cur = odbc_exec($con, "select id_grupo,descricao_grupo from dbo.sf_grupo_cliente
                                                        where tipo_grupo = 'C' and inativo_grupo = 0 order by descricao_grupo") or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                                            <option value="<?php echo utf8_encode($RFP['id_grupo']); ?>"><?php echo utf8_encode($RFP['descricao_grupo']) ?></option>
                                                        <?php } ?>
                                                        <option value="-1">Sem Grupo</option>                                                            
                                                    </select>
                                                </div>                                                
                                                <div style="width:11%; float:left; margin-left:0.5%">
                                                    <span>Tipo Data:</span>
                                                    <select id="txtTpData" style="height: 31px;">
                                                        <option value="1">VENDA</option>
                                                        <option value="0">RECEBIMENTO</option>
                                                        <option value="2">LANÇAMENTO</option>
                                                    </select>
                                                </div>
                                                <div style="width:8%; float:left; margin-left:0.5%">
                                                    <div>Período De:</div>
                                                    <input type="text" class="datepicker" style="height: 31px; margin-bottom:2px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo getData("T");?>" placeholder="Data inicial"/>
                                                </div>
                                                <div style="width:8%; float:left; margin-left:0.5%">
                                                    <div>Período Até:</div>
                                                    <input type="text" class="datepicker" style="height: 31px; margin-bottom:2px" id="txt_dt_end" name="txt_dt_end" value="<?php echo getData("T");?>" placeholder="Data Final"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="float:left; width:calc(100% - 22px); margin:10px; border:1px solid #D8D8D8">
                                            <style type="text/css">#tbFinancas tfoot th { line-height: 40px }</style>
                                            <table id="tbFinancas" class="table mtResumoCx" style="float:none" cellpadding="0" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th width="32%">TOTAIS</th>
                                                        <th width="16%">Entrada</th>
                                                        <th width="16%">Saída</th>
                                                        <th width="16%">Estornos</th>
                                                        <th width="20%">Saldo</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th style="text-align:right">Total Geral</th>
                                                        <th style="text-align:right"></th>
                                                        <th style="text-align:right"></th>
                                                        <th style="text-align:right"></th>
                                                        <th style="text-align:right; padding-left:0; font-size:20px; color:#09C; background:#CCC"><b></b></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                        <div style="float:left; width:calc(50% - 100px)">
                                            <div style="float:left; width:calc(100% - 17px); margin:0 5px 10px 10px; border:1px solid #D8D8D8">
                                              <?php if($ckb_fin_cx_ticket == 1 || $ckb_fin_cx_mat == 1){ ?>
                                                <?php if($ckb_fin_cx_ticket == 1){ ?>
                                                  <table id="tbTmedio" class="table mtModalid" style="float:none" cellpadding="0" cellspacing="0" width="100%">
                                                      <thead>
                                                          <tr>
                                                              <th style="width: 70%"><center>TICKET MÉDIO</center></th>
                                                              <th style="width: 30%"><center>VALOR</center></th>
                                                          </tr>
                                                      </thead>
                                                      <tbody></tbody>
                                                  </table>
                                                <?php } if($ckb_fin_cx_mat == 1){ ?>
                                                <table id="tbMatriculas" class="table mtModalid" style="float:none" cellpadding="0" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 70%"><center>MATRÍCULAS</center></th>
                                                            <th style="width: 30%"><center>VALOR</center></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                                <?php }} ?>
                                            </div>
                                        </div>
                                        <?php if($ckb_fin_cx_mod == 1){ ?>
                                        <div style="float:left; width:calc(50% + 100px)">
                                            <div style="float:left; width:calc(100% - 17px); margin:0 10px 10px 5px; border:1px solid #D8D8D8">
                                                <div id="tablediv8">
                                                    <table id="tbModalidades" class="table mtModalid" style="float:none" cellpadding="0" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th><center>MODALIDADES</center></th>
                                                                <th><center>VALOR</center></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <th>TOTAL</th>
                                                                <th style="text-align:right"></th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                                <div id="chartdiv8"></div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div style="clear:both"></div>
                                    </div>
                                </div>
                            </div>
                            <?php } if($ckb_aca_filAvn_ == 1) { ?>
                            <div class="tab-pane <?php echo ($tab == 4 ? "active" : "")?>" id="tab4">
                                <div style="width:100%">
                                    <div class="boxhead">
                                        <div class="boxtext">Filtro Avançado</div>
                                    </div>
                                    <div class="boxtable" id="filAvan" style="margin-bottom:5px; background:#FFF">
                                        <div style="width:100%">
                                            <div style="float:left; width:32.7%">
                                              <div class="" style="display:flex;">
                                                <div style="flex:1;">
                                                  <span style="display:block;">Status do Cliente:</span>
                                                  <select name="itemsStat[]" id="mscStat" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                                    <option value="1">Ativo</option>
                                                    <option value="4">Ativo Credencial</option>
                                                    <option value="14">Ativo Abono</option>
                                                    <?php if ($mdl_seg_ == 0) { ?>
                                                    <option value="5">Ativo Ausente 1</option>
                                                    <option value="6">Ativo Ausente 2</option>
                                                    <option value="7">Ativo Ausente 3</option>
                                                    <option value="8">Ativo Ausente 4</option>
                                                    <option value="9">Ativo Ausente 5</option>
                                                    <?php } ?>
                                                    <option value="12">Ativo Em Aberto</option>
                                                    <option value="2">Suspenso</option>
                                                    <option value="16">Cancelado</option>
                                                    <option value="3">Inativo</option>
                                                    <?php if ($mdl_srs_ == 1) { ?>
                                                    <option value="10">Serasa</option>
                                                    <?php } if ($mdl_clb_ == 1) { ?>
                                                    <option value="11">Dependente</option>
                                                    <option value="13">Desligado</option>
                                                    <option value="15">Desligado Em Aberto</option>
                                                    <?php } ?>
                                                  </select>
                                                </div>
                                              </div>
                                            </div>
                                            <div style="float:left; width:32.6%;margin-left:1%">
                                                <span>Seleção de Convênios:</span>
                                                <select name="itemsConv[]" id="mscConv" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                                    <?php $cur = odbc_exec($con, "SELECT id_convenio,descricao from sf_convenios where id_conv_plano is null and tipo = 1 and status = 0 order by 2");
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo utf8_encode($RFP["id_convenio"]); ?>"><?php echo utf8_encode($RFP["descricao"]); ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div style="float:left; width:32.7%;margin-left:1%">
                                                <span>Plano:</span>
                                                <select name="itemsPlano[]" id="mscPlano" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                                    <?php $cur = odbc_exec($con, "SELECT p.conta_produto,p.descricao from sf_produtos p left join sf_contas_movimento on p.conta_movimento = sf_contas_movimento.id_contas_movimento where p.tipo = 'C' and p.conta_produto > 0") or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo $RFP['conta_produto'] ?>"><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div style="clear:both; height:5px"></div>
                                            <div style="float:left; width:32.7%">
                                                <div>Aniversariantes (de ~ até):</div>
                                                <div style="float:left; width:46%">
                                                    <input id="txtAniversarioDe" name="txtAniversarioDe" type="text" value="" class="input-medium" style="width:100%" <?php echo $disabled; ?>/>
                                                </div>
                                                <div style="float:left; width:8%; margin-top:4px">
                                                    <center>~</center>
                                                </div>
                                                <div style="float:right; width:46%">
                                                    <input id="txtAniversarioAte" name="txtAniversarioAte" type="text" value="" class="input-medium" style="width:100%" <?php echo $disabled; ?>/>
                                                </div>
                                            </div>
                                            <div style="float:left; width:16.6%; margin-left:1%">
                                                <div>Dias sem Acesso:</div>
                                                <div style="float:left; width:46%">
                                                    <input id="txtDiasSemAcessoDe" name="txtDiasSemAcessoDe" maxlength="3" type="text" value="" class="input-medium" style="width:100%" <?php echo $disabled; ?>/>
                                                </div>
                                                <div style="float:left; width:8%; margin-top:4px">
                                                    <center>~</center>
                                                </div>
                                                <div style="float:left; width:46%">
                                                    <input id="txtDiasSemAcessoAte" name="txtDiasSemAcessoAte" maxlength="3" type="text" value="" class="input-medium" style="width:100%" <?php echo $disabled; ?>/>
                                                </div>
                                            </div>
                                            <div style="float:left; width:15%; margin-left:1%">
                                                <div>Dias sem Atendimento (de ~ até):</div>
                                                <div style="float:left; width:46%">
                                                    <input id="txtFxSemAtendDe" name="txtFxSemAtendDe" type="text" value="" class="input-medium" style="width:100%" <?php echo $disabled; ?>/>
                                                </div>
                                                <div style="float:left; width:8%; margin-top:4px">
                                                    <center>~</center>
                                                </div>
                                                <div style="float:left; width:46%">
                                                    <input id="txtFxSemAtendAte" name="txtFxSemAtendAte" type="text" value="" class="input-medium" style="width:100%" <?php echo $disabled; ?>/>
                                                </div>
                                            </div>                                            
                                            <div style="float:left; width:32.7%; margin-left:1%">
                                                <span>Plano Empresa/Família:</span>
                                                <select name="txtPlanoEmpFam[]" id="txtPlanoEmpFam" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                                    <?php $cur = odbc_exec($con, "SELECT id_convenio,descricao from sf_convenios where id_conv_plano is null and tipo = 0 and status = 0 order by 2");
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo utf8_encode($RFP["id_convenio"]); ?>"><?php echo utf8_encode($RFP["descricao"]); ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div style="clear:both; height:5px"></div>
                                            <div style="float:left; width:32.7%">
                                                <span>Procedência:</span>
                                                <select name="txtProcedencia[]" id="txtProcedencia" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                                    <option value="-1">NÃO DEFINIDO</option>
                                                    <?php $cur = odbc_exec($con, "SELECT id_procedencia,nome_procedencia from sf_procedencia where tipo_procedencia = 0 order by 2");
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo utf8_encode($RFP["id_procedencia"]); ?>"><?php echo utf8_encode($RFP["nome_procedencia"]); ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div style="float:left; width:16.6%; margin-left:1%">
                                                <span>Proprietário:</span>
                                                <select name="txtTitularFiltro" id="txtTitularFiltro" class="input-medium" style="width:100%">
                                                    <option value="null">[ TODOS ]</option>
                                                    <option value="0">Titular</option>
                                                    <option value="1">Dependentes</option>
                                                </select>
                                            </div>
                                            <div style="float:left; width:15%; margin-left:1%">
                                                <span>Comissões:</span>
                                                <select name="txtComissoes" id="txtComissoes" class="input-medium" style="width:100%">
                                                    <option value="null">[ TODOS ]</option>
                                                    <option value="0">Sem Regra</option>
                                                    <option value="1">Com Regra</option>
                                                </select>
                                            </div>                                            
                                            <div style="float:left; width:16.6%; margin-left:1%">
                                                <div>Faixa Etária (de ~ até):</div>
                                                <div style="float:left; width:46%">
                                                    <input id="txtFxEtariaDe" name="txtFxEtariaDe" type="text" value="" class="input-medium" style="width:100%" <?php echo $disabled; ?>/>
                                                </div>
                                                <div style="float:left; width:8%; margin-top:4px">
                                                    <center>~</center>
                                                </div>
                                                <div style="float:left; width:46%">
                                                    <input id="txtFxEtariaAte" name="txtFxEtariaAte" type="text" value="" class="input-medium" style="width:100%" <?php echo $disabled; ?>/>
                                                </div>
                                            </div>
                                            <div style="float:left; width:15%; margin-left:1%">
                                                <span>Sexo:</span>
                                                <select name="txtSexo" id="txtSexo" class="input-medium" style="width:100%">
                                                    <option value="null">[ TODOS ]</option>
                                                    <option value="M">MASCULINO</option>
                                                    <option value="F">FEMININO</option>
                                                </select>
                                            </div>
                                            <div style="clear:both; height:5px"></div>
                                            <div style="float:left; width:32.7%">
                                                <span>Profissão:</span>
                                                <select name="txtProfissao[]" id="txtProfissao" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                                    <?php $cur = odbc_exec($con, "SELECT distinct profissao from sf_fornecedores_despesas where profissao is not null and len(profissao) > 0 order by 1");
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo utf8_encode($RFP["profissao"]); ?>"><?php echo utf8_encode($RFP["profissao"]); ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div style="float:left; width:16.6%; margin-left:1%">
                                                <span>Estado Civil:</span>
                                                <select name="txtEstCivil[]" id="txtEstCivil" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                                    <option value="SOLTEIRO(A)">SOLTEIRO(A)</option>
                                                    <option value="CASADO(A)">CASADO(A)</option>
                                                    <option value="SEPARADO(A)">SEPARADO(A)</option>
                                                    <option value="VIÚVO(A)">VIÚVO(A)</option>
                                                    <option value="DIVORCIADO(A)">DIVORCIADO(A)</option>
                                                </select>
                                            </div>
                                            <div style="float:left; width:15%; margin-left:1%">
                                                <span>Parentesco:</span>
                                                <select name="txtParentesco[]" id="txtParentesco" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                                    <option value="null">Selecione</option>
                                                    <?php $cur = odbc_exec($con, "select * from sf_parentesco order by nome_parentesco") or die(odbc_errormsg());
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                    <option value="<?php echo $RFP['id_parentesco']; ?>"><?php echo utf8_encode($RFP['nome_parentesco']); ?></option> 
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div style="float:left; width:16.5%; margin-left:1%">
                                                <span>Cidade:</span>
                                                <select name="txtCidade" id="txtCidade" class="input-medium" style="width:100%">
                                                    <option value="null">[ TODOS ]</option>
                                                    <?php $cur = odbc_exec($con, "SELECT distinct tb_cidades.cidade_codigo, tb_cidades.cidade_nome,tb_estados.estado_sigla from sf_fornecedores_despesas inner join tb_cidades on tb_cidades.cidade_codigo = sf_fornecedores_despesas.cidade inner join tb_estados on tb_cidades.cidade_codigoEstado = tb_estados.estado_codigo where cidade is not null order by 2");
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo utf8_encode($RFP["cidade_codigo"]); ?>"><?php echo utf8_encode($RFP["cidade_nome"]) . " - " . utf8_encode($RFP["estado_sigla"]); ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div style="float:left; width:15.2%; margin-left:1%">
                                                <span>Bairro:</span>
                                                <select name="txtBairro" id="txtBairro" class="input-medium" style="width:100%">
                                                    <option value="null">[ TODOS ]</option>
                                                </select>
                                            </div>
                                            <div style="clear:both; height:5px"></div>                                            
                                            <div style="float:left; width:32.6%">
                                                <span>Grupo de Clientes:</span>
                                                <select name="txtGrupo[]" id="txtGrupo" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                                    <?php $cur = odbc_exec($con, "select id_grupo,descricao_grupo from dbo.sf_grupo_cliente where tipo_grupo = 'C' and inativo_grupo = 0 order by descricao_grupo");
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo utf8_encode($RFP["id_grupo"]); ?>"><?php echo utf8_encode($RFP["descricao_grupo"]); ?></option>
                                                    <?php } ?>
                                                    <option value="-1">Sem Grupo</option>
                                                </select>
                                            </div>
                                            <div style="float:left; width:32.6%; margin-left:1%">
                                                <span>Credenciais:</span>
                                                <select name="txtCredenciais[]" id="txtCredenciais" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                                    <?php $cur = odbc_exec($con, "SELECT id_credencial,descricao from sf_credenciais where inativo = 0 order by 2");
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo utf8_encode($RFP["id_credencial"]); ?>"><?php echo utf8_encode($RFP["descricao"]); ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div style="float:left; width:23.0%; margin-left:1%;">
                                                <span>Relacionamento (número dependentes):</span>
                                                <input id="txtRelacionamento" name="txtRelacionamento" type="text" value="" class="input-medium" style="width:100%" <?php echo $disabled; ?>/>
                                            </div>
                                            <div style="float:left; width:8.7%; margin-left:1%; padding-top:15px">
                                                <input type="button" title="Buscar" class="button button-turquoise btn-primary" style="line-height:17px" onclick="BuscarFilAvancado()" value="Buscar"/>
                                            </div>
                                            <div style="clear:both"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } if($ckb_aca_renovacoes_ == 1) { ?>
                            <div class="tab-pane <?php echo ($tab == 5 ? "active" : "")?>" id="tab5">
                                <div style="width:100%">
                                    <div class="boxhead">
                                        <div class="boxtext">Renovações</div>
                                    </div>
                                    <div class="boxtable" style="margin-bottom:5px; background:#FFF">
                                        <div id="tablediv9" style="width:100%">
                                            <div style="float:left; width:32.7%">
                                                <span>Data:</span>
                                                <select name="txtTipoData" id="txtTipoData" class="input-medium" style="width:100%">
                                                    <option value="0">PERÍODO CONTRATO</option>
                                                    <option value="1">PERÍODO DE COMPRA</option>
                                                    <option value="2">PERÍODO DE CADASTRO</option>
                                                </select>
                                            </div>
                                            <div style="float:left; width:32.6%; margin-left:1%;">
                                              <div class="" style="display:flex;">
                                                <div style="flex:1;">
                                                  <span style="display:block;">Status do Cliente:</span>
                                                  <select name="itemsStatRen[]" id="mscStatRen" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                                    <option value="1">Ativo</option>
                                                    <option value="4">Ativo Credencial</option>
                                                    <option value="14">Ativo Abono</option>
                                                    <?php if ($mdl_seg_ == 0) { ?>
                                                    <option value="5">Ativo Ausente 1</option>
                                                    <option value="6">Ativo Ausente 2</option>
                                                    <option value="7">Ativo Ausente 3</option>
                                                    <option value="8">Ativo Ausente 4</option>
                                                    <option value="9">Ativo Ausente 5</option>
                                                    <?php } ?>
                                                    <option value="12">Ativo Em Aberto</option>
                                                    <option value="2">Suspenso</option>
                                                    <option value="16">Cancelado</option>
                                                    <option value="3">Inativo</option>
                                                    <?php if ($mdl_srs_ == 1) { ?>
                                                    <option value="10">Serasa</option>
                                                    <?php } if ($mdl_clb_ == 1 && $mdl_seg_ == 0) { ?>
                                                    <option value="11">Dependente</option>
                                                    <option value="13">Desligado</option>
                                                    <option value="15">Desligado Em Aberto</option>
                                                    <?php } ?>
                                                  </select>
                                                </div>
                                              </div>
                                            </div>
                                            <div style="float:left; width:15.2%; margin-left:1%">
                                                <span>Proprietário:</span>
                                                <select name="txtTitular" id="txtTitular" class="input-medium" style="width:100%">
                                                    <option value="0">Titular</option>
                                                    <option value="1">Seus Dependentes</option>
                                                </select>
                                            </div>                                            
                                            <div class="lojas" style="float:left; width:16.5%; margin-left:1%;">
                                            </div>
                                            <div style="clear:both; height:5px"></div>
                                            <div style="float:left; width:32.7%">
                                                <div id="txtPeriodoTipo">Início do Contrato (de ~ até):</div>
                                                <div style="float:left; width:47%">
                                                    <input id="txtInicioContDe" name="txtInicioContDe" type="text" value="" class="input-medium datepicker" style="width:100%" <?php echo $disabled; ?>/>
                                                </div>
                                                <div style="float:left; width:6%; margin-top:4px">
                                                    <center>~</center>
                                                </div>
                                                <div style="float:right; width:47%">
                                                    <input id="txtInicioContAte" name="txtInicioContAte" type="text" value="" class="input-medium datepicker" style="width:100%" <?php echo $disabled; ?>/>
                                                </div>
                                            </div>
                                            <div style="float:left; width:32.6%; margin-left:1%">
                                                <div>Fim do Contrato (de ~ até):</div>
                                                <div style="float:left; width:47%">
                                                    <input id="txtFimContDe" name="txtFimContDe" type="text" value="" class="input-medium datepicker" style="width:100%" <?php echo $disabled; ?>/>
                                                </div>
                                                <div style="float:left; width:6%; margin-top:4px">
                                                    <center>~</center>
                                                </div>
                                                <div style="float:right; width:47%">
                                                    <input id="txtFimContAte" name="txtFimContAte" type="text" value="" class="input-medium datepicker" style="width:100%" <?php echo $disabled; ?>/>
                                                </div>
                                            </div>
                                            <div style="float:left; width:32.6%; margin-left:1%">
                                                <span>Grupo de Clientes:</span>
                                                <select name="txtGrupoRen[]" id="txtGrupoRen" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                                    <?php $cur = odbc_exec($con, "select id_grupo,descricao_grupo from dbo.sf_grupo_cliente where tipo_grupo = 'C' and inativo_grupo = 0 order by descricao_grupo");
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo utf8_encode($RFP["id_grupo"]); ?>"><?php echo utf8_encode($RFP["descricao_grupo"]); ?></option>
                                                    <?php } ?>
                                                    <option value="-1">Sem Grupo</option>
                                                </select>
                                            </div>
                                            <div style="clear:both; height:5px"></div>
                                            <div style="float:left; width:32.7%">
                                                <span><?php echo ($mdl_seg_ > 0 ? "Indicador" : "Funcionário");?> Responsável:</span>
                                                <select name="txtProfResp" id="txtProfResp" class="input-medium" style="width:100%">
                                                    <option value="null">[ TODOS ]</option>
                                                    <?php $cur = odbc_exec($con, "select id_fornecedores_despesas, razao_social 
                                                        from sf_fornecedores_despesas where tipo = '" . ($mdl_seg_ > 0 ? "I" : "E") . "' 
                                                        and dt_demissao is null order by razao_social ") or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo $RFP['id_fornecedores_despesas'] ?>"><?php echo utf8_encode($RFP['razao_social']) ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div style="float:left; width:16.4%; margin-left:1%">
                                                <span>Usúario Responsável:</span>
                                                <select name="txtConsulResp" id="txtConsulResp" class="input-medium" style="width:100%">
                                                    <option value="null">[ TODOS ]</option>
                                                    <?php $cur = odbc_exec($con, "select id_usuario, nome, login_user from sf_usuarios where inativo = 0 and id_usuario > 1") or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo $RFP['id_usuario'] ?>"><?php echo formatNameCompact(($RFP['nome'] != ''? utf8_encode($RFP['nome']) : utf8_encode($RFP['login_user']))); ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>                                            
                                            <div style="float:left; width:15.2%; margin-left:1%">
                                                <span>Contrato:</span>
                                                <select name="txtTipoCont" id="txtTipoCont" class="input-medium" style="width:100%">
                                                    <option value="null">TODOS</option>
                                                    <option value="1">RENOVADOS</option>
                                                    <option value="2">NÃO RENOVADOS</option>
                                                    <option value="3">NOVOS</option>
                                                    <option value="4">CREDENCIAIS</option>
                                                </select>
                                            </div>
                                            <div id="divPlano" style="float:left; width:32.7%; margin-left:1%">
                                                <span>Plano:</span>
                                                <select  name="txtPlano[]" id="txtPlano" multiple="multiple" class="select"  style="width:100%" class="input-medium">
                                                    <?php $cur = odbc_exec($con, "SELECT p.conta_produto,p.descricao from sf_produtos p left join sf_contas_movimento on p.conta_movimento = sf_contas_movimento.id_contas_movimento where p.tipo = 'C' and p.conta_produto > 0") or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo $RFP['conta_produto'] ?>"><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div id="divCredenciais" style="float:left; width:32.7%; margin-left:1%;display: none;">
                                                <span>Credenciais:</span>
                                                <select name="txtCredenciaisRen[]" id="txtCredenciaisRen" multiple="multiple" class="select"  style="width:100%" class="input-medium">
                                                    <?php $cur = odbc_exec($con, "select id_credencial,descricao from sf_credenciais where inativo = 0") or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo $RFP['id_credencial'] ?>"><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div style="clear:both; height:5px"></div>
                                            <div style="float:left; width:32.7%">
                                                <div>Valor do Plano (de ~ até):</div>
                                                <div style="float:left; width:47%">
                                                    <input id="txtValPlanoDe" name="txtValPlanoDe" type="text" value="<?php echo escreverNumero(0); ?>" class="input-medium" style="width:100%" <?php echo $disabled; ?>/>
                                                </div>
                                                <div style="float:left; width:6%; margin-top:4px">
                                                    <center>~</center>
                                                </div>
                                                <div style="float:right; width:47%">
                                                    <input id="txtValPlanoAte" name="txtValPlanoAte" type="text" value="<?php echo escreverNumero(0); ?>" class="input-medium" style="width:100%" <?php echo $disabled; ?>/>
                                                </div>
                                            </div>
                                            <div style="float:left; width:32.7%; margin-left:1%">
                                                <span>Período:</span>
                                                <select name="txtPeriodo[]" id="txtPeriodo" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                                    <?php $cur = odbc_exec($con, "select distinct parcela from sf_produtos_parcelas where id_parcela > 0 order by parcela");
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo utf8_encode($RFP["parcela"]); ?>"><?php echo ($RFP["parcela"] == "0" ? "DCC" : ($RFP["parcela"] == "-1" ? "BOL" : ($RFP["parcela"] == "-2" ? "PIX" : $RFP["parcela"]))); ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>                                            
                                            <div style="float:left; width:32.6%; margin-left:1%; padding-top:15px">
                                                <?php if($_SESSION["mod_emp"] != 1){ ?>
                                                  <input type="checkbox" name="ckbValidadeCartao" value="1" id="ckbValidadeCartao"/> Sem Cartões de Vantagem/Vencidos
                                                  <div style="clear:both;"></div>
                                                <?php } ?>
                                                <input type="checkbox" name="ckbMaisPlanos" value="1" id="ckbMaisPlanos"/> Com mais de 1 plano
                                            </div>
                                            <div style="float:left; width:15.8%; clear:both;">
                                                <span>Mensalidades Data:</span>
                                                <select name="txtTipoMensData" id="txtTipoMensData" class="input-medium" style="width:100%">
                                                    <option value="0">INICIO</option>
                                                    <option value="1">FIM</option>
                                                    <option value="2">PAGAMENTO</option>
                                                </select>
                                            </div>
                                            <div style="float:left; width:15.8%; margin-left:1%;">
                                                <span>Mensalidades Tipo:</span>
                                                <select name="txtTipoMens" id="txtTipoMens" class="input-medium" style="width:100%">
                                                    <option value="null">TODAS</option>
                                                    <option value="1">EM ABERTO</option>
                                                    <option value="2">PAGAS</option>
                                                    <option value="3">ÚLTIMAS</option>
                                                </select>
                                            </div>
                                            <div style="float:left; width:32.6%; margin-left:1%">
                                                <div>Mensalidade Período (de ~ até):</div>
                                                <div style="float:left; width:47%">
                                                    <input id="txtFimMensDe" name="txtFimMensDe" type="text" value="" class="input-medium datepicker" style="width:100%" <?php echo $disabled; ?>/>
                                                </div>
                                                <div style="float:left; width:6%; margin-top:4px">
                                                    <center>~</center>
                                                </div>
                                                <div style="float:right; width:47%">
                                                    <input id="txtFimMensAte" name="txtFimMensAte" type="text" value="" class="input-medium datepicker" style="width:100%" <?php echo $disabled; ?>/>
                                                </div>
                                            </div>
                                            <div style="float:left; width:32.6%; margin-left:1%;">
                                                <span style="display: block;">Status do Plano:</span>
                                                <select name="txtStatus[]" id="txtStatus" multiple="multiple" class="select" style="width:74%" class="input-medium">
                                                    <option value="Ativo">Ativo</option>
                                                    <option value="Inativo">Inativo</option>
                                                    <option value="Suspenso">Suspenso</option>
                                                    <option value="Novo">Novo</option>
                                                    <option value="Cancelado">Cancelado</option>
                                                </select>
                                                <input type="button" onclick="BuscarFilRenovacoes()" title="Buscar" class="button button-turquoise btn-primary" style="line-height:17px" value="Buscar"/>
                                            </div>
                                        </div>
                                        <div style="float:right; display:none">
                                            <div id="chartdiv9"></div>
                                        </div>
                                        <div style="clear:both"></div>
                                    </div>
                                </div>
                            </div>
                            <?php }} if($ckb_aca_dcc_ == 1) { ?>
                            <div class="tab-pane <?php echo ($tab == 6 || isset($_GET['dash']) ? "active" : "")?>" id="tab6">
                                <div style="width:100%">
                                    <div class="boxhead">
                                        <div class="boxtext">Controle de Recorrências</div>
                                    </div>
                                    <div class="boxtable" style="margin-bottom:5px; background:#FFF">
                                        <div style="float:left; width:49.5%">
                                            <span style="display:block">Cliente:</span>
                                            <select name="txtTipoCliente" id="txtTipoCliente" style="height:27px; width:110px">
                                                <option value="0">TODOS</option>
                                                <option value="1">ESPECÍFICO</option>
                                            </select>
                                            <input name="txtIdNota" id="txtIdNota" value="" type="hidden"/>
                                            <input type="text" style="float:right; width:calc(100% - 118px)" name="txtDesc" id="txtDesc" class="inputbox" value="" size="10" />
                                            <div style="clear:both; height:5px"></div>
                                            <span>Cartão:</span>
                                            <select name="txtCartaoDCC[]" <?php echo $disabled;?> multiple="multiple" id="txtCartaoDCC">
                                                <?php $cur = odbc_exec($con, "select id_tipo_documento,descricao,abreviacao from sf_tipo_documento where cartao_dcc in (1,2) order by descricao") or die(odbc_errormsg());
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                    <option SELECTED value="<?php echo $RFP['id_tipo_documento']; ?>"><?php echo utf8_encode($RFP['descricao']) . " - " . utf8_encode($RFP['abreviacao']); ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="mynewbox">
                                            <div class="mynewtxt">Filtro por Data <input name="ckb_data_pgto" id="ckb_data_pgto" value="1" type="checkbox" class="input-medium" checked></div>
                                            <div class="mynewcnt">
                                                <div style="float:left; width:40%">
                                                    <span style="display:block">Tipo de Pagamento:</span>
                                                    <select name="txtTipoPgto" id="txtTipoPgto" style="width:100%">
                                                        <option value="0">MENSALIDADE</option>
                                                        <option value="1" <?php echo (isset($_GET['dashativos']) ? "selected" : ""); ?>>PAGAMENTO</option>
                                                    </select>
                                                </div>
                                                <div style="float:left; width:26%; margin-left:2%">
                                                    <div>Data Inicial:</div>
                                                    <input id="txtDataIniDcc" name="txtDataIniDcc" value="<?php echo (isset($_GET['dash']) ? getData("B") : getData("T")); ?>" type="text" class="input-medium datepicker" style="width:100%" <?php echo $disabled; ?>/>
                                                </div>
                                                <div style="float:left; width:6%; margin-top:20px">
                                                    <center>~</center>
                                                </div>
                                                <div style="float:right; width:26%">
                                                    <div>Data Final:</div>
                                                    <input id="txtDataFinDcc" name="txtDataFinDcc" type="text" value="<?php echo (isset($_GET['dash']) && !isset($_GET['dashativos']) ? escreverDataSoma(getData("T"), " -1 days") :  getData("T")); ?>" class="input-medium datepicker" style="width:100%" <?php echo $disabled; ?>/>
                                                </div>
                                                <div style="clear:both"></div>
                                            </div>
                                        </div>
                                        <div class="mynewbox">
                                            <div class="mynewtxt">Número do Cartão</div>
                                            <div class="mynewcnt">
                                                <div style="float:left; width:20.5%">
                                                    <input id="txtNumCart01" name="txtNumCart01" type="text" maxlength="4" class="input-medium" style="width:100%; text-align:center" <?php echo $disabled; ?>/>
                                                </div>
                                                <div style="float:left; width:6%; margin-top:4px">
                                                    <center>-</center>
                                                </div>
                                                <div style="float:left; width:20.5%">
                                                    <input id="txtNumCart02" name="txtNumCart02" type="text" value="XXXX" class="input-medium" style="width:100%; text-align:center" disabled/>
                                                </div>
                                                <div style="float:left; width:6%; margin-top:4px">
                                                    <center>-</center>
                                                </div>
                                                <div style="float:left; width:20.5%">
                                                    <input id="txtNumCart03" name="txtNumCart03" type="text" value="XXXX" class="input-medium" style="width:100%; text-align:center" disabled/>
                                                </div>
                                                <div style="float:left; width:6%; margin-top:4px">
                                                    <center>-</center>
                                                </div>
                                                <div style="float:right; width:20.5%">
                                                    <input id="txtNumCart04" name="txtNumCart04" type="text" maxlength="4" class="input-medium" style="width:100%; text-align:center" <?php echo $disabled; ?>/>
                                                </div>
                                                <div style="clear:both"></div>
                                            </div>
                                        </div>
                                        <div class="mynewbox" style="border:0">
                                            <span style="display:block">Remessa:</span>
                                            <select name="txtTipo" id="txtTipo" style="width:calc(100% - 260px); height:28px">
                                                <option value="1" <?php echo (isset($_GET['dash']) && isset($_GET['dashativos']) ? 'selected' : '') ?>>APROVADO</option>
                                                <option value="2" <?php echo (isset($_GET['dash']) && !isset($_GET['dashativos']) ? 'selected' : '') ?>>NÃO ENVIADO</option>
                                                <option value="3">TODOS</option>
                                                <option value="4">ESTORNADO</option>
                                                <option value="5">NEGADO</option>
                                                <option value="6">CANCELADO</option>
                                            </select>
                                            <div style="float:right">
                                                <button type="button" class="button button-turquoise btn-primary" onclick="refreshTableDcc()" style="margin:0; line-height:17px" title="Buscar">
                                                    <span class="ico-search icon-white"></span>
                                                </button>
                                                <button type="button" class="button button-green btn-primary" onclick="sendTableDcc()" style="margin:0; line-height:17px" title="Enviar">
                                                    <span class="ico-ok icon-white"></span>
                                                </button>
                                                <button type="button" class="button button-red btn-primary" style="margin:0; line-height:17px" title="Cancelar">
                                                    <span class="ico-blocked icon-white"></span>
                                                </button>
                                                <button type="button" class="button button-yellow btn-primary"  onclick="AbrirBoxDCC()" style="margin:0; line-height:17px" title="Cartões Vencidos">
                                                    <span class="ico-retweet-2 icon-white"></span>
                                                </button>
                                                <button type="button" class="button button-blue btn-primary" onclick="printDCC()" style="margin:0; line-height:17px" title="Imprimir">
                                                    <span class="ico-print icon-white"></span>
                                                </button>
                                                <button type="button" title="Exportar Excel" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="ExportarExcelDCC();">
                                                    <span class="ico-download-3 icon-white"></span>
                                                </button>
                                            </div>
                                        </div>
                                        <div style="clear:both"></div>                                        
                                    </div>
                                </div>
                            </div>
                            <?php } if($ckb_aca_bol_ == 1) { ?>
                            <div class="tab-pane <?php echo ($tab == 8 ? "active" : "")?>" id="tab8">
                                <div style="width:100%">
                                    <div class="boxhead">                                        
                                        <div class="boxtext">Controle de Recorrências</div>
                                    </div>
                                    <div class="boxtable" style="margin-bottom:5px; background:#FFF">
                                        <div style="float:left; width:50%">                                            
                                            <div class="mynewbox">
                                                <div class="mynewtxt">
                                                    Filtro por Data <input name="ckb_data_pgtoBol" id="ckb_data_pgtoBol" value="1" type="checkbox" class="input-medium" checked>
                                                    Agrupar boleto <input id="ckb_grupoFin" value="1" type="checkbox" class="input-medium" <?php echo ($grupoFin == "1" ? "checked" :""); ?>>
                                                </div>
                                                <div class="mynewcnt">
                                                    <div style="float:left; width:40%">
                                                        <span style="display:block">Tipo de Pagamento:</span>
                                                        <select name="txtTipoPgtoBol" id="txtTipoPgtoBol" style="width:100%">
                                                            <option value="0">MENSALIDADE</option>
                                                            <option value="1" <?php echo (isset($_GET['dashativos']) ? "selected" : ""); ?>>PAGAMENTO</option>
                                                        </select>
                                                    </div>
                                                    <div style="float:left; width:26%; margin-left:2%">
                                                        <div>Data Inicial:</div>
                                                        <input id="txtDataIniBol" name="txtDataIniBol" value="<?php echo getData("B"); ?>" type="text" class="input-medium datepicker" style="width:100%" <?php echo $disabled; ?>/>
                                                    </div>
                                                    <div style="float:left; width:6%; margin-top:20px">
                                                        <center>~</center>
                                                    </div>
                                                    <div style="float:right; width:26%">
                                                        <div>Data Final:</div>
                                                        <input id="txtDataFinBol" name="txtDataFinBol" type="text" value="<?php echo getData("E"); ?>" class="input-medium datepicker" style="width:100%" <?php echo $disabled; ?>/>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                </div>
                                            </div>                                                                                                                        
                                        </div>                                        
                                        <div style="float:left; width:48%; margin-left: 1%;">
                                            <div class="mynewbox" style="border:0">                                            
                                                <input id="txtIdNotaBol" name="txtIdNotaBol" value="" type="hidden"/> 
                                                <input id="txtTipoBoletoEnvio" value="<?php echo $dcc_boleto_tipo_; ?>" type="hidden"/> 
                                                <div style="float:left; width:20%">
                                                    <span style="display:block">Tipo:</span>    
                                                    <select name="txtTipoClienteBol" id="txtTipoClienteBol" style="width: 100%;">
                                                        <option value="0">TODOS</option>
                                                        <option value="1">ESPECÍFICO</option>
                                                    </select>                                                    
                                                </div>
                                                <div style="float:left; width:40%; margin-left: 1%;">
                                                    <span style="display:block">Nome:</span>
                                                    <input type="text" style="width: 100%;" name="txtDescBol" id="txtDescBol" class="inputbox" value="" size="10" />                                            
                                                </div>
                                                <div style="float:left; width:38%; margin-left: 1%;">
                                                    <span style="display:block">Planos:</span>
                                                    <select name="txtClientePlanoBol" id="txtClientePlanoBol" style="width: 100%;">
                                                        <option value="null">TODOS</option>
                                                    </select>                                                    
                                                </div>                                                
                                            </div>
                                            <div class="mynewbox" style="border:0">
                                                <span style="display:block">Remessa:</span>
                                                <select name="txtTipoBol" id="txtTipoBol" style="width:calc(100% - 260px); height:28px">
                                                    <option value="null">TODOS</option>                                                    
                                                    <option value="1">GERADOS</option>
                                                    <option value="2">NÃO GERADOS</option>
                                                    <option value="3">ENVIADOS</option>
                                                    <option value="4">NÃO ENVIADOS</option>
                                                    <option value="5">CANCELADOS</option>
                                                </select>
                                                <div style="float:right">
                                                    <button type="button" class="button button-turquoise btn-primary" onclick="refreshTableBol()" title="Buscar">
                                                        <span class="ico-search icon-white"></span>
                                                    </button>
                                                    <button type="button" class="button button-green btn-primary" onclick="sendTableBol()" title="Gerar">
                                                        <span class="ico-ok icon-white"></span>
                                                    </button>
                                                    <button type="button" class="button button-blue btn-primary" onclick="printBol('I')" title="Imprimir">
                                                        <span class="ico-print icon-white"></span>
                                                    </button>
                                                    <button type="button" class="button button-turquoise btn-primary" onclick="printBolCar()" title="Imprimir Carnê">
                                                        <span class="ico-barcode-3 icon-white"></span>
                                                    </button>
                                                    <button type="button" class="button button-blue btn-primary" onclick="ExportarExcelBol();" title="Exportar Excel">
                                                        <span class="ico-download-3 icon-white"></span>
                                                    </button>
                                                    <button type="button" class="button button-turquoise btn-primary" onclick="sendEmail()" title="Enviar E-mail">
                                                        <span class="ico-envelope-3 icon-white"></span>
                                                    </button>                                                    
                                                </div>
                                            </div>                                        
                                        </div>
                                        <div style="clear:both"></div>
                                    </div>
                                </div>
                            </div>
                            <?php } if($ckb_aca_pix_ == 1) { ?>
                            <div class="tab-pane <?php echo ($tab == 10 ? "active" : "")?>" id="tab10">
                                <div style="width:100%">
                                    <div class="boxhead">
                                        <div class="boxtext">Controle de Recorrências</div>
                                    </div>
                                    <div class="boxtable" style="margin-bottom:5px; background:#FFF">
                                        <div style="float:left; width:50%">                                            
                                            <div class="mynewbox">
                                                <div class="mynewtxt">Filtro por Data <input name="ckb_data_pgtoPix" id="ckb_data_pgtoPix" value="1" type="checkbox" class="input-medium" checked></div>
                                                <div class="mynewcnt">
                                                    <div style="float:left; width:40%">
                                                        <span style="display:block">Tipo de Pagamento:</span>
                                                        <select name="txtTipoPgtoPix" id="txtTipoPgtoPix" style="width:100%">
                                                            <option value="0">MENSALIDADE</option>
                                                            <option value="1" <?php echo (isset($_GET['dashativos']) ? "selected" : ""); ?>>PAGAMENTO</option>
                                                        </select>
                                                    </div>
                                                    <div style="float:left; width:26%; margin-left:2%">
                                                        <div>Data Inicial:</div>
                                                        <input id="txtDataIniPix" name="txtDataIniPix" value="<?php echo getData("B"); ?>" type="text" class="input-medium datepicker" style="width:100%" <?php echo $disabled; ?>/>
                                                    </div>
                                                    <div style="float:left; width:6%; margin-top:20px">
                                                        <center>~</center>
                                                    </div>
                                                    <div style="float:right; width:26%">
                                                        <div>Data Final:</div>
                                                        <input id="txtDataFinPix" name="txtDataFinPix" type="text" value="<?php echo getData("E"); ?>" class="input-medium datepicker" style="width:100%" <?php echo $disabled; ?>/>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                </div>
                                            </div>                                                                                                                        
                                        </div>                                        
                                        <div style="float:left; width:48%; margin-left: 1%;">
                                            <div class="mynewbox" style="border:0">                                            
                                                <span style="display:block">Cliente:</span>
                                                <select name="txtTipoClientePix" id="txtTipoClientePix" style="height:27px; width:110px">
                                                    <option value="0">TODOS</option>
                                                    <option value="1">ESPECÍFICO</option>
                                                </select>
                                                <input name="txtIdNotaPix" id="txtIdNotaPix" value="" type="hidden"/>
                                                <input type="text" style="float:right; width:calc(100% - 118px)" name="txtDescPix" id="txtDescPix" class="inputbox" value="" size="10" />                                            
                                            </div>
                                            <div class="mynewbox" style="border:0">
                                                <span style="display:block">Remessa:</span>
                                                <select name="txtTipoPix" id="txtTipoPix" style="width:calc(100% - 260px); height:28px">
                                                    <option value="null">TODOS</option>                                                    
                                                    <option value="1">GERADOS</option>
                                                    <option value="2">NÃO GERADOS</option>
                                                    <option value="3">ENVIADOS</option>
                                                    <option value="4">NÃO ENVIADOS</option>
                                                    <option value="5">CANCELADOS</option>
                                                </select>
                                                <div style="float:right">
                                                    <button type="button" class="button button-turquoise btn-primary" onclick="refreshTablePix()" title="Buscar">
                                                        <span class="ico-search icon-white"></span>
                                                    </button>
                                                    <button type="button" class="button button-green btn-primary" onclick="sendTablePix()" title="Enviar">
                                                        <span class="ico-ok icon-white"></span>
                                                    </button>
                                                    <button type="button" class="button button-blue btn-primary" onclick="printPix('I')" title="Imprimir">
                                                        <span class="ico-print icon-white"></span>
                                                    </button>
                                                    <button type="button" class="button button-blue btn-primary" onclick="ExportarExcelPix();" title="Exportar Excel">
                                                        <span class="ico-download-3 icon-white"></span>
                                                    </button>
                                                    <button type="button" class="button button-turquoise btn-primary" onclick="sendEmailPix()" title="Enviar E-mail">
                                                        <span class="ico-envelope-3 icon-white"></span>
                                                    </button>                                                    
                                                </div>
                                            </div>                                        
                                        </div>
                                        <div style="clear:both"></div>
                                    </div>
                                </div> 
                            </div>                                
                            <?php } if($mdl_wha_ == 1) { ?>
                            <div class="tab-pane <?php echo ($tab == 9 ? "active" : "")?>" id="tab9">
                                <div style="width:100%">
                                    <div class="boxhead">
                                        <div class="boxtext">Controle de Whatsapp</div>
                                    </div>
                                    <div class="boxtable" style="margin-bottom:5px; background:#FFF">
                                        <div style="float:left; width:50%">                                            
                                            <div class="mynewbox">
                                                <div class="mynewtxt">Filtro por Data <input name="ckb_data_pgtoWha" id="ckb_data_pgtoWha" value="1" type="checkbox" class="input-medium" checked></div>
                                                <div class="mynewcnt">
                                                    <div style="float:left; width:40%">
                                                        <span style="display:block">Tipo de Data:</span>
                                                        <select name="txtTipoPgtoWha" id="txtTipoPgtoWha" style="width:100%">
                                                            <option value="0">LANÇAMENTO</option>
                                                        </select>
                                                    </div>
                                                    <div style="float:left; width:26%; margin-left:2%">
                                                        <div>Data Inicial:</div>
                                                        <input id="txtDataIniWha" name="txtDataIniWha" value="<?php echo getData("B"); ?>" type="text" class="input-medium datepicker" style="width:100%" <?php echo $disabled; ?>/>
                                                    </div>
                                                    <div style="float:left; width:6%; margin-top:20px">
                                                        <center>~</center>
                                                    </div>
                                                    <div style="float:right; width:26%">
                                                        <div>Data Final:</div>
                                                        <input id="txtDataFinWha" name="txtDataFinWha" type="text" value="<?php echo getData("E"); ?>" class="input-medium datepicker" style="width:100%" <?php echo $disabled; ?>/>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                </div>
                                            </div>                                                                                                                        
                                        </div>                                        
                                        <div style="float:left; width:48%; margin-left: 1%;">
                                            <div class="mynewbox" style="border:0">                                            
                                                <span style="display:block">Cliente:</span>
                                                <select name="txtTipoClienteWha" id="txtTipoClienteWha" style="height:27px; width:110px">
                                                    <option value="0">TODOS</option>
                                                    <option value="1">ESPECÍFICO</option>
                                                </select>
                                                <input name="txtIdNotaWha" id="txtIdNotaWha" value="" type="hidden"/>
                                                <input type="text" style="float:right; width:calc(100% - 118px)" name="txtDescWha" id="txtDescWha" class="inputbox" value="" size="10" />                                            
                                            </div>
                                            <div class="mynewbox" style="border:0">
                                                <span style="display:block">Remessa:</span>
                                                <select name="txtTipoWha" id="txtTipoWha" style="width:calc(100% - 260px); height:28px">
                                                    <option value="null">TODOS</option>
                                                    <option value="0">NÃO ENVIADOS</option>                                                    
                                                    <option value="1">ENVIADOS</option>
                                                    <option value="2">CANCELADOS</option>
                                                    <option value="3">INVÁLIDOS</option>
                                                    <option value="4">DUPLICADOS</option>
                                                </select>
                                                <div style="float:right">                                                                                                  
                                                    <button type="button" class="button button-turquoise btn-primary" onclick="refreshTableWha()" title="Buscar">
                                                        <span class="ico-search icon-white"></span>
                                                    </button>
                                                    <button type="button" class="button button-blue btn-primary" onclick="printWha('I')" title="Imprimir">
                                                        <span class="ico-print icon-white"></span>
                                                    </button>
                                                    <button type="button" class="button button-blue btn-primary" onclick="ExportarExcelWha();" title="Exportar Excel">
                                                        <span class="ico-download-3 icon-white"></span>
                                                    </button>
                                                    <?php if ($_SESSION["login_usuario"] == "ADMIN") { ?>
                                                    <button type="button" class="button button-red btn-primary" onclick="cancelarWha();" title="Cancelar" id="bntInativoWha" disabled>
                                                        <span class="ico-ban-circle"> </span>
                                                    </button>
                                                    <?php } ?>
                                                    <button type="button" class="button button-orange btn-primary" onclick="abrirTelaBox('./../Comercial/WhatsappForm.php', 380, 480);" title="Whatsapp Login">
                                                        <span class="ico-off icon-white"></span>
                                                    </button>                                                    
                                                </div>
                                            </div>                                        
                                        </div>
                                        <div style="clear:both"></div>
                                    </div>
                                </div>
                            </div>
                            <?php } if ($mdl_aca_ == 1 && $ckb_aca_acesso_ == 1) { ?>
                            <div class="tab-pane <?php echo ($tab == 7 ? "active" : "")?>" id="tab7">
                                <div style="width:100%">
                                    <div class="boxhead">
                                        <div class="boxtext">Acesso</div>
                                    </div>
                                    <div class="boxtable" style="margin-bottom:5px; background:#FFF">
                                        <div style="float:left; width:100%">
                                            <div style="width:7%; float:left;">
                                                <span>Usuário:</span>
                                                <select id="txtTpUsuario">
                                                    <option value="T">TODOS</option>
                                                    <option value="C">Cliente</option>
                                                    <option value="E">Funcionário</option>
                                                    <option value="P">Prospects</option>                                                    
                                                </select>
                                            </div>
                                            <div style="width:13.8%; float:left;margin-left: 0.5%">
                                                <span>Nome:</span>
                                                <input name="txtIdUsuario" id="txtIdUsuario" type="hidden"/>
                                                <input name="txtAcessoDesc" id="txtAcessoDesc" type="text" size="10"/>
                                            </div>
                                            <div style="width:7%; float:left; margin-left:0.5%">
                                                <div>Período De:</div>
                                                <input type="text" class="datepicker" style="margin-bottom:2px" id="txtDtInicioAcesso" name="txtDtInicioAcesso" value="<?php echo getData("T");?>" placeholder="Data inicial"/>
                                            </div>
                                            <div style="width:7%; float:left; margin-left:0.5%">
                                                <div>Período Até:</div>
                                                <input type="text" class="datepicker" style="margin-bottom:2px" id="txtDtFimAcesso" name="txtDtFimAcesso" value="<?php echo getData("T");?>" placeholder="Data Final"/>
                                            </div>
                                            <div style="width:10%; float:left;margin-left:0.5%">
                                                <span>Tipo:</span>
                                                <select id="txtTipoAcesso">
                                                    <option value="T">TODOS</option>
                                                    <option value="AP">Acesso permitido</option>
                                                    <option value="ANP">Acesso não permitido</option>
                                                    <option value="FA">Fora Ambiente</option>
                                                    <option value="FH">Fora Horário</option>
                                                    <option value="LB">Liberação Manual</option>
                                                    <option value="SM">Sem Matrícula</option>
                                                    <option value="BL">Bloqueado</option>
                                                    <option value="CV">Débito em Carteira</option>
                                                    <option value="MV">Mensalidade em Aberto</option>
                                                    <option value="CVD">Carteira Vencida</option>
                                                </select>
                                            </div>
                                            <div style="float:left; width:15%;margin-left:0.5%">
                                                <span>Ambiente:</span>
                                                <select id="mscAmbiente" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                                    <?php $cur = odbc_exec($con, "SELECT * from sf_ambientes") or die(odbc_errormsg());
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo $RFP['id_ambiente'] ?>"><?php echo utf8_encode($RFP['nome_ambiente']) ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div style="float:left; width:15%;margin-left:0.5%">
                                                <span>Catraca:</span>
                                                <select id="mscCatraca" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                                    <?php $cur = odbc_exec($con, "SELECT * from sf_catracas") or die(odbc_errormsg());
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo $RFP['id_catraca'] ?>"><?php echo utf8_encode($RFP['nome_catraca']) ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div style="float:left; width:15%;margin-left:0.5%">
                                                <span>Grupo de Clientes:</span>
                                                <select id="mscGrupoAce" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                                    <?php $cur = odbc_exec($con, "select id_grupo,descricao_grupo from dbo.sf_grupo_cliente
                                                    where tipo_grupo = 'C' and inativo_grupo = 0") or die(odbc_errormsg());
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo $RFP['id_grupo'] ?>"><?php echo utf8_encode($RFP['descricao_grupo']) ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div style="float:left; margin-top:15px;margin-left: 0.5%">
                                                <input type="button" title="Buscar" class="button button-turquoise btn-primary" style="line-height:17px" onclick="AtualizaGraficosAcesso()" name="btnfind" id="btnfind" value="Buscar"/>
                                            </div>
                                        </div>
                                        <div style="clear:both;height: 10px;"></div>
                                        <div style="float:left; width:99%;border:1px solid #D8D8D8">
                                            <div class="boxhead">
                                                <div class="boxtext" style="position:relative">Horários</div>
                                            </div>
                                            <div id="chartHorario" style="width: 100%;height: 330px;font-size: 11px;"></div>
                                        </div>
                                        <div style="clear:both;height: 10px;"></div>
                                        <div style="float:left; width:49%; border:1px solid #D8D8D8">
                                            <div class="boxhead">
                                                <div class="boxtext" style="position:relative"> Turnos</div>
                                            </div>
                                            <div style="float:left; width:100%; height:400px" id="chartTurnoAcesso"></div>
                                        </div>
                                        <div style="float:left; width:49%; border:1px solid #D8D8D8;margin-left: 1%">
                                            <div class="boxhead">
                                                <div class="boxtext" style="position:relative">Status</div>
                                            </div>
                                            <div style="float:right; width:100%; height:400px" id="chartStatusAcesso"></div>
                                        </div>
                                        <div style="clear:both"></div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php if ($mdl_aca_ == 1 || $_SESSION["mod_emp"] == 1) {
                        if($ckb_aca_igCaixa_ == 1) {?>
                    <span id="tbRes">
                        <div class="boxhead">
                            <div class="boxtext" style="position:relative">
                                <?php if ($ckb_fin_lsol_ == 1 && $_SESSION["mod_emp"] != 1) { ?>
                                <div style="margin-top: -3px; margin-left :5px; float: left">
                                    <button onclick="AbrirBoxX(0);" title="Lançamentos do Caixa" type="button" class="button button-blue btn-primary" style="margin:0; line-height:17px">
                                        <span class="icon-plus icon-white"></span>
                                    </button>
                                </div>
                                <?php } ?>                                
                                Resumo de Caixa
                                <div style="top:3px; right:2px; line-height:1; position:absolute">
                                    <button onclick="imprimirCaixa(0);" title="Imprimir Caixa" type="button" class="button button-blue btn-primary" style="margin:0; line-height:17px">
                                        <span class="ico-print icon-white"></span>
                                    </button>
                                    <div style="text-align:left; margin-right: 10px;" class="btn-group">
                                        <button style="height: 13px;background-color: gray;" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                            <span style="margin-top: 1px;" class="caret"></span>
                                        </button>
                                        <ul style="left:-130px;" class="dropdown-menu">
                                            <li><a onclick="imprimirCaixa(1);">Caixa Detalhado</a></li>
                                            <li><a onclick="imprimirCaixa(2);">Caixa Resumido</a></li>
                                            <li><a onclick="ExportarExcelCaixa();">Exportar Excel</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="boxtable">
                            <table id="tbResumo" class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="1%"></th>
                                        <th width="5%"><center>Código</center></th>
                                        <th width="15%"><center>Data/Hora</center></th>
                                        <th width="10%">Valor</th>
                                        <th width="7%">Tipo</th>
                                        <th width="6%">Matrícula</th>
                                        <th width="37%">Cliente</th>
                                        <th width="7%"><center>Parcelas</center></th>
                                        <th width="10%">Operador(a)</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </span>
                    <?php } ?>
                    <span id="tbRec" style="display:none">
                        <div class="boxhead">
                            <div class="boxtext" style="position:relative">
                                Contratos de Clientes
                                <div style="top:4px; right:2px; line-height:1; position:absolute">
                                    <button type="button" title="Enviar E-mail" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="abrirTelaEmail();">
                                        <span class="ico-envelope-3 icon-white"></span>
                                    </button>
                                    <button type="button" title="Enviar SMS" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="abrirTelaSMS();">
                                        <span class="ico-comment icon-white"></span>
                                    </button>
                                    <?php if ($mdl_wha_ > 0) { ?>
                                    <button type="button" title="Enviar Whatsapp" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="abrirTelaWha();">
                                        <span class="ico-phone-4 icon-white"></span>
                                    </button>
                                    <?php } ?>
                                    <button type="button" title="Imprimir" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printStatus();">
                                        <span class="ico-print icon-white"></span>
                                    </button>
                                    <button type="button" title="Exportar Excel" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="ExportarExcelCnttSts();">
                                        <span class="ico-download-3 icon-white"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="boxtable">
                            <input id="tbReceitaFiltro" type="hidden" value="">
                            <table filtro="" class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbReceita">
                                <thead>
                                    <tr>
                                        <th width="5%"><center>Código</center></th>
                                        <th width="20%"><center>Nome</center></th>
                                        <th width="10%">Dt.Nasc.</th>
                                        <th width="5%"><center>Sexo</center></th>
                                        <th width="10%">Status</th>
                                        <th width="16%"><center>Telefone</center></th>
                                        <th width="17%"><center>Celular</center></th>
                                        <th width="17%"><center>Email</center></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th colspan="7" style="text-align:right">Total:</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </span>
                    <span id="tbFil" style="display:none">
                        <div class="boxhead">
                            <div class="boxtext" style="position:relative">
                                Lista do Filtro Avançado
                                <div style="top:4px; right:2px; line-height:1; position:absolute">
                                    <button type="button" title="Enviar E-mail" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="abrirTelaEmail();">
                                        <span class="ico-envelope-3 icon-white"></span>
                                    </button>
                                    <button type="button" title="Enviar SMS" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="abrirTelaSMS('');">
                                        <span class="ico-comment icon-white"></span>
                                    </button>
                                    <?php if ($mdl_wha_ > 0) { ?>
                                    <button type="button" title="Enviar Whatsapp" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="abrirTelaWha('');">
                                        <span class="ico-phone-4 icon-white"></span>
                                    </button>
                                    <?php } ?>                                    
                                    <button type="button" title="Imprimir" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printFilAvancado('I')">
                                        <span class="ico-print icon-white"></span>
                                    </button>
                                    <button type="button" title="Exportar Excel" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printFilAvancado('E');">
                                        <span class="ico-download-3 icon-white"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="boxtable">
                            <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbFiltroAv">
                                <thead>
                                    <tr>
                                        <th width="5%"><center>Código</center></th>
                                        <th width="20%"><center>Nome</center></th>
                                        <th width="10%">Dt.Nasc.</th>
                                        <th width="5%"><center>Sexo</center></th>
                                        <th width="10%">Status</th>
                                        <th width="16%"><center>Telefone</center></th>
                                        <th width="17%"><center>Celular</center></th>
                                        <th width="17%"><center>Email</center></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th colspan="7" style="text-align:right">Total:</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </span>
                    <span id="tbRen" style="display:none">
                        <div class="boxhead">
                            <div class="boxtext" style="position:relative">
                                Lista de Renovações
                                <div style="top:4px; right:2px; line-height:1; position:absolute">
                                    <button type="button" title="Enviar E-mail" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="abrirTelaEmail();">
                                        <span class="ico-envelope-3 icon-white"></span>
                                    </button>
                                    <button type="button" title="Enviar SMS" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="abrirTelaSMS('');">
                                        <span class="ico-comment icon-white"></span>
                                    </button>
                                    <?php if ($mdl_wha_ > 0) { ?>
                                    <button type="button" title="Enviar Whatsapp" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="abrirTelaWha('');">
                                        <span class="ico-phone-4 icon-white"></span>
                                    </button>
                                    <?php } ?>
                                    <button type="button" title="Imprimir" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printRenovacoes('I');">
                                        <span class="ico-print icon-white"></span>
                                    </button>
                                    <button type="button" title="Exportar Excel" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printRenovacoes('E');">
                                        <span class="ico-download-3 icon-white"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="boxtable">
                            <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbRenovacao">
                                <thead>
                                    <tr>
                                        <th width="5%"><center>Cód</center></th>
                                        <th width="20%"><center>Nome</center></th>
                                        <th width="12%">Período.</th>
                                        <th width="8%">Sts.Plano</th>
                                        <th width="15%"><center>Plano</center></th>
                                        <th width="8%"><center>Telefone</center></th>
                                        <th width="8%"><center>Celular</center></th>
                                        <th width="14%"><center>Email</center></th>
                                        <th width="10%">Consultor</th>                            
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th colspan="8" style="text-align:right">Total:</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </span>
                    <?php } ?>
                    <span id="tbDCC" <?php if ($mdl_aca_ == 1) { echo "style=\"display:none\""; } ?>>
                        <div class="boxhead">
                            <div class="boxtext">Transações em DCC</div>
                        </div>
                        <div class="boxtable" style="margin-bottom:0">
                            <table id="tbListaDCC" class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th><center>Dt.Mens.</center></th>
                                        <th>Mat.</th>
                                        <th>Nm.Cartão</th>
                                        <th><center>Pedido</center></th>
                                        <th><center>Transação</center></th>
                                        <th><center>Cartão</center></th>
                                        <th><center>Estado</center></th>
                                        <th><center>Valor</center></th>
                                        <th><center>Mod.</center></th>
                                        <th><center>Recor.</center></th>
                                        <th><center>Dt.Pgto.</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <div style="clear:both"></div>
                        </div>
                        <div class="boxfoot">
                            <div class="boxtext">
                                <div style="float:left; width:170px">Total de <span id="lbl_dcc_total">0</span> Registros</div>
                                <div style="float:left; width:calc(50% - 170px); text-align:center; color:#c22439; font-size:14px">Total Bruto: <span id="lbl_dcc_vbruto"><?php echo escreverNumero(0,1); ?></span></div>
                                <div style="float:left; width:calc(50% - 170px); text-align:center; color:#c22439; font-size:14px">Total Líquido: <span id="lbl_dcc_vliquido"><?php echo escreverNumero(0,1); ?></span></div>
                                <div style="float:right; width:170px; text-align:right;font-size:12px;">
                                    Saldo de Transações:&nbsp;<input type="text" id="saldoImp" class="input-medium" style="width:46px; padding:2px; margin:-3px 0 0 3px; display:none;font-size:16px;" readonly/>
                                    <button type="button" id="saldoBtn" class="btn dblue" style="line-height:19px;font-size:14px; width:46px; margin:-3px 0 0; background-color:#308698 !important" onClick="ler_saldo()">
                                        <span class="icon-search icon-white"></span>
                                    </button>
                                </div>
                                <div style="clear:both"></div>
                            </div>
                        </div>
                    </span>
                    <span id="tbBOL" <?php if ($mdl_aca_ == 1) { echo "style=\"display:none\""; } ?>>
                        <div class="boxhead">
                            <div class="boxtext">Transações em BOL</div>
                        </div>
                        <div class="boxtable" style="margin-bottom:0">
                            <table id="tbListaBOL" class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th><center>Dt.Mens.</center></th>
                                        <th>Mat.</th>
                                        <th>Cliente</th>
                                        <th><center>Pedido</center></th>
                                        <th><center>Transação</center></th>
                                        <th><center>Valor Mens.</center></th>
                                        <th><center>Dt.Pgto.</center></th>                            
                                        <th><center>Bol.</center></th>
                                        <th><center>Env.</center></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            <div style="clear:both"></div>
                        </div>                        
                    </span>
                    <span id="tbPIX" <?php if ($mdl_aca_ == 1) { echo "style=\"display:none\""; } ?>>
                        <div class="boxhead">
                            <div class="boxtext">Transações em PIX</div>
                        </div>
                        <div class="boxtable" style="margin-bottom:0">
                            <table id="tbListaPIX" class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th><center>Dt.Mens.</center></th>
                                        <th>Mat.</th>
                                        <th>Cliente</th>
                                        <th><center>Pedido</center></th>
                                        <th><center>Transação</center></th>
                                        <th><center>Valor Mens.</center></th>
                                        <th><center>Dt.Pgto.</center></th>                            
                                        <th><center>PIX.</center></th>
                                        <th><center>Env.</center></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            <div style="clear:both"></div>
                        </div>                        
                    </span>
                    <span id="tbWha" <?php if ($mdl_aca_ == 1) { echo "style=\"display:none\""; } ?>>
                        <div class="boxhead">
                            <div class="boxtext">Transações em Whatsapp</div>
                        </div>
                        <div class="boxtable" style="margin-bottom:0">
                            <table id="tbListaWHA" class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Dt.Envio</th>
                                        <th>Mat.</th>
                                        <th>Cliente</th>
                                        <th>Grupo</th>
                                        <th>Telefone</th>
                                        <th>Mensagem</th>
                                        <th>Usuário</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            <div style="clear:both"></div>
                        </div>                        
                    </span>
                    <?php if ($mdl_aca_ == 1) { ?>
                    <span id="tbAce" style="display: none;">
                        <div class="boxhead">
                            <div class="boxtext" style="position:relative">
                                Lista de Acessos
                                <div style="top:4px; right:2px; line-height:1; position:absolute">
                                    <button type="button" title="Imprimir" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printAcessos()">
                                        <span class="ico-print icon-white"></span>
                                    </button>
                                    <button type="button" title="Exportar Excel" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="ExportarExcelAcesso();">
                                        <span class="ico-download-3 icon-white"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="boxtable">
                            <table id="tblAcessos" class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th width="5%"  style="text-align: center;">Mat.</th>
                                    <th width="30%" style="text-align: center;">Nome</th>
                                    <th width="8%" style="text-align: center;">Data/Hora</th>
                                    <th width="9%" style="text-align: center;">Tipo Acesso</th>
                                    <th width="16%" style="text-align: center;">Motivo</th>
                                    <th width="16%" style="text-align: center;">Ambiente</th>
                                    <th width="16%" style="text-align: center;">Catraca</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </span>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Processando" style="display: none;">
            <input id="txtTotalSucesso" type="hidden" value="0"/>
            <input id="txtTotalErro" type="hidden" value="0"/>
            <div id="progressbar"></div>
            <div style="display: flex;align-items: center;justify-content: center;margin-top: 20px;">
                <div id="pgIni">0</div><div style="padding: 5px;">até</div><div id="pgFim">0</div>
            </div>
        </div>        
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/charts.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type='text/javascript' src="../../js/numeral.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/jquery.priceformat.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/multiselect/jquery.multi-select.min.js'></script>
        <script type="text/javascript" src="../../js/moment.min.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>
        <script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
        <script type="text/javascript" src="https://www.amcharts.com/lib/3/pie.js"></script>
        <script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script>
        <script type="text/javascript" src="https://www.amcharts.com/lib/3/themes/light.js"></script>        
        <script type="text/javascript" src="../Comercial/js/DisparosEmails.js"></script>        
        <script type="text/javascript" src="js/InformacoesGerenciaisStatus.js"></script>
        <script type="text/javascript" src="js/InformacoesGerenciaisContratos.js"></script>
        <script type="text/javascript" src="js/InformacoesGerenciaisFinancas.js"></script>
        <script type="text/javascript" src="js/InformacoesGerenciaisFilAvancado.js"></script>
        <script type="text/javascript" src="js/InformacoesGerenciaisRenovacoes.js"></script>
        <script type="text/javascript" src="js/InformacoesGerenciaisDCC.js"></script>
        <script type="text/javascript" src="js/InformacoesGerenciaisBOL.js"></script>
        <script type="text/javascript" src="js/InformacoesGerenciaisPIX.js"></script>
        <script type="text/javascript" src="js/InformacoesGerenciaisWHA.js"></script>
        <script type="text/javascript" src="js/InformacoesGerenciaisAcessos.js"></script>
        <script type="text/javascript">

            $("#txtRelacionamento").mask("99", {placeholder: ""});
            $("#txtFxEtariaDe, #txtFxEtariaAte, #txtDiasSemAcesso, #txtFxSemAtendDe, #txtFxSemAtendAte").mask("999", {placeholder: ""});
            $("#txtValPlanoDe, #txtValPlanoAte").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
            $("#txtInicioContDe, #txtInicioContAte, #txtFimContDe, #txtFimContAte, #txtDataIniDcc, #txtDataFinDcc, #txtFimMensDe, #txtFimMensAte").mask(lang["dateMask"]);
            $("#txtAniversarioDe, #txtAniversarioAte").mask("99/99");
            
            function AbrirCli(id) {
                window.open("./../Academia/ClientesForm.php?id=" + id, '_blank');         
            }
            
            function AbrirBoxX(id) {
                abrirTelaBox("FormSolicitacao-de-Autorizacao.php" + (id > 0 ? "?id=" + id : ""), 320, 400);
            }

            function FecharBox() {
                $("#newbox").remove();
                BuscarFinancas();
            }
           
            function showTab(id) {
                <?php if ($mdl_aca_ == 1 || $_SESSION["mod_emp"] == 1) { ?>
                $("#tbAce").hide();
                $("#tbRec").hide();
                $("#tbRes").hide();
                $("#tbFil").hide();
                $("#tbRen").hide();
                $("#tbDCC").hide();
                $("#tbAce").hide();
                $("#tbBOL").hide();                
                $("#tbPIX").hide();                
                $("#tbWha").hide();                
                if(id === 1){
                    $("#tbRec").show();
                    carregaTabStatus();
                } else if(id === 2){
                    $("#tbRec").show();
                    carregaTabContratos();
                } else if(id === 3){
                    $("#tbRes").show();
                    carregaTabFinancas();
                } else if (id === 4) {
                    carregaTabAvancado();
                    $("#tbFil").show();
                } else if (id === 5) {
                    carregaTabRenovacoes();
                    $("#tbRen").show();
                } else if (id === 6) {
                    carregaTabDCC();
                    $("#tbDCC").show();
                } else if (id === 7) {
                    carregaTabAcesso();
                    $("#tbAce").show();
                } else if (id === 8) {
                    carregaTabBoleto();
                    $("#tbBOL").show();
                } else if (id === 10) {
                    carregaTabPix();
                    $("#tbPIX").show();
                } else if (id === 9) {
                    carregaWhatsapp();
                    $("#tbWha").show();
                }
                <?php } ?>
            }
            
            $("#txtCartaoDCC").multiSelect({
                selectableHeader: "<div class='multipleselect-header'>Cartões Disponíveis</div>",
                selectionHeader: "<div class='multipleselect-header'>Cartões Selecionados</div>"
            });
            
            $(document).ready(function () {
                showTab(<?php echo (isset($_GET['dash']) ? 6 : $tab); ?>);                
                $("#chartdiv9").parent().css("display", "block");
                $("#tablediv9").css("width", "calc(100% - 200px)");
            });
            
            function FecharTermos() {
                $("#newbox").remove();
            }
            
            <?php if ($mdl_aca_ == 1) { ?>
            var retPrint = "";
            if ($("#txt_dt_begin").val() !== "" && $("#txt_dt_begin").val() !== undefined) {
                retPrint = retPrint + "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "_");
            }
            if ($("#txt_dt_end").val() !== "" && $("#txt_dt_end").val() !== undefined) {
                retPrint = retPrint + "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "_");
            }
            <?php } ?>
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>