<?php
session_start();
include "./../../Connections/configini.php";

$cpf_cnpj = "";
$cedente = "";
$nome_fantasia = "";
$inscricao = "";
$endereco = "";
$cidade_uf = "";
$cep = "";
$telefone = "";
$site = "";
$empresa = "";
$cliente = "";
$exame = "";
$plano = "";
$categoria = "";
$credencial = "";

$cur = odbc_exec($con, "select A.id_filial_f from sf_usuarios_filiais A
inner join sf_usuarios B on (A.id_usuario_f = B.id_usuario and A.id_filial_f = B.filial)
where A.id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
while ($RFP = odbc_fetch_array($cur)) {
    $empresa = $RFP['id_filial_f'];
}
if (is_numeric($empresa)) {
    $cur = odbc_exec($con, "select * from sf_filiais where numero_filial = " . $empresa);
    while ($RFP = odbc_fetch_array($cur)) {
        $cpf_cnpj = utf8_encode($RFP["cnpj"]);
        $cedente = utf8_encode($RFP["razao_social_contrato"]);
        $nome_fantasia = utf8_encode($RFP["nome_fantasia_contrato"]);
        $inscricao = utf8_encode($RFP["inscricao_estadual"]);
        $endereco = utf8_encode($RFP['endereco']) . " n°" . utf8_encode($RFP['numero']) . "," . utf8_encode($RFP['bairro']);
        $cidade_uf = utf8_encode($RFP['cidade_nome']) . " " . utf8_encode($RFP['estado']);
        $cep = utf8_encode($RFP['cep']);
        $telefone = utf8_encode($RFP['telefone']);
        $site = utf8_encode($RFP['site']);
    }

    if (is_numeric($_GET["id"])) {
        $cur = odbc_exec($con, "select id_fornecedores_despesas, razao_social, data_nascimento, nome_cat 
        from sf_fornecedores_despesas left join sf_categoria_socio on titularidade = id_cat            
        where id_fornecedores_despesas = " . $_GET["id"]);
        while ($RFP = odbc_fetch_array($cur)) {
            $cliente = utf8_encode($RFP["id_fornecedores_despesas"]) . " - " . utf8_encode($RFP["razao_social"]) . " - " . escreverData($RFP["data_nascimento"]);
            $categoria = utf8_encode($RFP["nome_cat"]);
        }
        $cur = odbc_exec($con, "select descricao_doc, dateadd(day,validade_doc,data_documento) validade_doc from sf_fornecedores_despesas_documentos
        inner join sf_documentos on id_doc = id_documento
        where inativo_doc = 0 and dateadd(day,validade_doc,data_documento) > getdate() and id_fornecedor_despesas = " . $_GET["id"]);
        while ($RFP = odbc_fetch_array($cur)) {
            $exame .= utf8_encode($RFP["descricao_doc"]) . " - " . escreverData($RFP["validade_doc"]) . "<br>";
        }
        $cur = odbc_exec($con, "select descricao, dbo.FU_PLANO_FIM(id_plano) dt_fim, dbo.FU_STATUS_PLANO_CLI(id_plano) status from sf_vendas_planos A
        inner join sf_produtos C on conta_produto = id_prod_plano where A.dt_cancelamento is null and 
        ((DATEADD(day,(select ACA_PLN_DIAS_RENOVACAO from sf_configuracao),A.dt_fim) > GETDATE() and C.parcelar = 0) or
        (A.dt_fim >= GETDATE() and C.parcelar = 1) or ((select COUNT(*) from sf_vendas_planos_mensalidade C where C.id_plano_mens = A.id_plano and dt_pagamento_mens is null) > 0))
        and favorecido = " . $_GET["id"]);
        while ($RFP = odbc_fetch_array($cur)) {
            $plano .= utf8_encode($RFP["descricao"]) . " - " . escreverData($RFP["dt_fim"]) . " - " . utf8_encode($RFP["status"]) . "<br>";
        }
        $cur = odbc_exec($con, "select descricao, dt_inicio, dt_fim from sf_fornecedores_despesas_credenciais a
        inner join sf_credenciais b on a.id_credencial = b.id_credencial
        where dt_cancelamento is null and dt_fim > getdate() and id_fornecedores_despesas = " . $_GET["id"]);
        while ($RFP = odbc_fetch_array($cur)) {
            $credencial .= utf8_encode($RFP["descricao"]) . " - " . escreverData($RFP["dt_inicio"]) . " até " . escreverData($RFP["dt_fim"]) . "<br>";
        }
    }
}
?>      
<html> 
    <head>
        <title>SIVIS - Cupom Fiscal</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script> 
        <style type="text/css">
            body {
                font-size: 10px;
                font-family: Tahoma;
            }
            table {
                width: 100%;
                max-width: 302px;
                font-size: 10px;
                font-family: Tahoma;
            }
            .emp {
                font-size: 10px;
                font-weight: bold;
                text-align: center;
                padding: 5px 0px 5px 0px;
            }
            .end {
                text-align: center;
                padding: 5px 0px 5px 0px;
            }
            .doc {
                padding-top: 5px;
            }
            .lin {
                padding-top: 5px;
                border-bottom: 1px solid #000;
            }
            .spc {
                padding-bottom: 5px;
            }
            .grupo {
                padding: 3px 0px 3px 0px;
                background-color: #DDD;
                font-weight: bold;
            }
            .tit {
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                padding: 5px 0px 10px 0px;
            }
        </style>
    </head>
    <body>
        <table cellspacing="0" cellpadding="0">            
            <tr>
                <td colspan="6" class="tit"><?php echo $nome_fantasia; ?></td>
            </tr>       
            <tr>
                <td colspan="6" class="end">
                    <?php echo (strlen($cedente) > 0 ? $cedente . "<br>" : ""); ?>
                    <?php echo (strlen($endereco) > 0 ? $endereco . "<br>" : ""); ?>
                    <?php echo (strlen($cidade_uf) > 0 ? $cidade_uf : ""); ?>
                    <?php echo (strlen($cep) > 0 ? " - CEP: " . $cep : ""); ?>
                    <br>
                    <?php echo (strlen($telefone) > 0 ? $telefone . "<br>" : ""); ?>
                    <?php echo (strlen($site) > 0 ? $site . "<br>" : ""); ?>
                </td>
            </tr>
            <tr>
                <td colspan="6" class="doc"><b>CNPJ: <?php echo $cpf_cnpj; ?></b><br/>I.E.: <?php echo $inscricao; ?></td>
            </tr>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <tr>
                <td colspan="3" class="doc"><b><?php echo date("d/m/Y H:i"); ?></b></td>
                <td colspan="3" class="doc" style="text-align:right"><?php echo $categoria; ?></td>
            </tr>
            <tr>
                <td colspan="6" class="tit">PERMANENTE</td>
            </tr>            
            <tr>
                <td colspan="6" class="lin"></td>
            </tr> 
            <tr>
                <td colspan="4" class="doc"><b>Cliente:</b></td>
                <td colspan="2" class="doc" style="text-align:right"><b></b></td>
            </tr>  
            <tr>
                <td colspan="6">
                    <?php echo $cliente; ?>                                                                     
                </td>
            </tr>           
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <?php if (strlen($exame) > 0) { ?>
                <tr>
                    <td colspan="4" class="doc"><b>Exames:</b></td>
                    <td colspan="2" class="doc" style="text-align:right"><b></b></td>
                </tr>  
                <tr>
                    <td colspan="6">
                        <?php echo $exame; ?>                                                                     
                    </td>
                </tr>           
                <tr>
                    <td colspan="6" class="lin"></td>
                </tr> 
            <?php } if (strlen($plano) > 0) { ?>
                <tr>
                    <td colspan="4" class="doc"><b>Pagamentos:</b></td>
                    <td colspan="2" class="doc" style="text-align:right"><b></b></td>
                </tr>  
                <tr>
                    <td colspan="6">
                        <?php echo $plano; ?>                                                                     
                    </td>
                </tr>           
                <tr>
                    <td colspan="6" class="lin"></td>
                </tr>
            <?php } if (strlen($credencial) > 0) { ?>
                <tr>
                    <td colspan="4" class="doc"><b>Autorização de Entrada:</b></td>
                    <td colspan="2" class="doc" style="text-align:right"><b></b></td>
                </tr>  
                <tr>
                    <td colspan="6">
                        <?php echo $credencial; ?>                                                                     
                    </td>
                </tr>           
                <tr>
                    <td colspan="6" class="lin"></td>
                </tr>
            <?php } ?>
            <tr>
                <td colspan="2" class="doc"><b>Operador</b></td>
                <td colspan="4" class="doc" style="text-align:right"><?php echo $_SESSION["login_usuario"]; ?></td>
            </tr>
        </table>
    </body>
    <script type="text/javascript" src="../../js/util.js"></script>     
    <script type="text/javascript">
        $(window).load(function () {
            window.print();
            setTimeout(function () {
                window.close();
            }, 500);
        });
    </script>    
</html>