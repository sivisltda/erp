<?php include "../../Connections/configini.php"; ?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
<style>
    body { font-family: sans-serif; }    
    .selected {
        background: #acbad4 !important;
    }
</style>
<body onload="iniciaAddPlanoAvulsoForm(<?php echo $_GET["idForm"]; ?>);">
    <div class="row-fluid">
        <form>
            <input type="hidden" id="txtModEmp" value="<?php echo $_SESSION["mod_emp"]; ?>">
            <div class="frmhead">
                <div class="frmtext">Adicionar Contrato</div>
                <div class="frmicon" onClick="parent.FecharBoxCliente()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont" style="height:190px;">
                <input id="tpTela" type="hidden" value="<?php echo $_GET["tp"]; ?>"/>
                <input type="hidden" id="txtPlanos" value="0" />
                <div style="width:40%;margin:0 auto;display: flex;align-items: center;margin-top: 12px;">
                    <h2 style="margin:0;width:45%;"><?php echo $lang['prefix']; ?>:</h2>
                    <input type="text" name="txtpreco" id="txtpreco" value="<?php echo escreverNumero(0); ?>" style="height: 40px;font-size: 28px;">
                </div>
                <div style="clear:both; height:5px"></div>
                <hr>
                <div style="width:23%; float:left; margin-right:1%">
                    <span>Data Início:</span>
                    <input type="text" id="txtDtInicio" class="datepicker inputCenter" value="<?php
                    date_default_timezone_set('Brazil/Brazilia');
                    echo getData("T");
                    ?>"/>
                </div>
                <div style="width:31%; float:left">
                    <span>Duração:</span>
                    <select id="txtTempo" class="select" style="width:100%">
                        <option value="0">Infinito</option>
                        <option value="1">Finito</option>
                    </select>
                </div>
                <div id="Cnttqnt" style="width:10%; float:left;margin-left:1%;display:none;">
                    <span>Quantidade:</span>
                    <input type="text" id="txtqnt" name="txtqnt" value="">
                </div>
                <div style="width:30%; float:left;margin-left:1%;">
                    <span>Tempo:</span>
                    <select id="txtPrazo" class="select" style="width:100%">
                        <option value="0">dias</option>
                        <option value="1" selected>mês</option>
                    </select>
                </div>
                <div style="width:13%; float:left;margin-left:1%">
                    <span>Intervalo:</span>
                    <input type="text" id="txtInterval" name="txtInterval" value="1">
                </div>
                <div style="clear:both; height:5px"></div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <button class="btn green" type="button" id="btnGravarPlano" title="Gravar"><span class="ico-checkmark"></span> Gravar</button>
                    <button class="btn yellow" title="Cancelar" onClick="parent.FecharBoxCliente()"><span class="ico-reply"></span> Cancelar</button>
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../js/plugins.js"></script>
    <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/moment.min.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type="text/javascript" src="js/ClientesAddPlanoAvulsoForm.js"></script>    
    <?php odbc_close($con); ?>
</body>
