<?php

include '../../../Connections/configini.php';

if (isset($_GET['getTotal'])) {
    $totalCR = 0;
    $query = "set dateformat dmy;
    select 'VD' tipo, id_parcela from sf_venda_parcelas A
    inner join sf_vendas B on A.venda = B.id_venda
    inner join sf_tipo_documento C on (A.tipo_documento = C.id_tipo_documento and id_banco_baixa is not null)
    where data_parcela+dias_baixa <= getdate() and B.status = 'Aprovado' AND A.valor_pago = 0 and B.cov = 'V'
    union all
    select 'LM' tipo, id_parcela from sf_lancamento_movimento_parcelas A
    inner join sf_lancamento_movimento B on A.id_lancamento_movimento = B.id_lancamento_movimento
    inner join sf_contas_movimento C on B.conta_movimento = C.id_contas_movimento
    inner join sf_tipo_documento D on (A.tipo_documento = D.id_tipo_documento and id_banco_baixa is not null)
    and data_vencimento+dias_baixa <= getdate() and B.status = 'Aprovado' and B.inativo = 0 and valor_pago = 0 and C.tipo = 'C'";
    //echo $query;exit();
    $cur = odbc_exec($con, $query) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('tipo' => $RFP['tipo'], 'id_parcela' => utf8_encode($RFP['id_parcela']));
    }
    echo json_encode($local);
}

if (isset($_POST['baixaItem'])) {    
    $excecao_baixa = 0;
    $cur = odbc_exec($con, "select * from sf_configuracao where id = 1");
    while ($RFP = odbc_fetch_array($cur)) {
        $excecao_baixa = $RFP['adm_excecao_baixa'];
    }    
    if ($_POST['tpItem'] == 'VD') {
        $query = "select case when valor_parcela_liquido > 0 then valor_parcela_liquido else valor_parcela end valor_parcela, id_banco_baixa, data_parcela from sf_venda_parcelas A
        inner join sf_tipo_documento B on A.tipo_documento = B.id_tipo_documento inner join sf_bancos C on B.id_banco_baixa = C.id_bancos
        where id_parcela = " . $_POST['idItem'];
        $cur = odbc_exec($con, $query);
        while ($RFP = odbc_fetch_array($cur)) {
            $valor = $RFP['valor_parcela'];
            $banco = $RFP['id_banco_baixa'];
            $dataVenc = escreverData($RFP['data_parcela']);
        }
        $query = "set dateformat dmy;
        update sf_venda_parcelas set obs_baixa = '" . utf8_decode('AUTOMATICA') . "', id_banco = " . $banco . ", valor_pago = " . $valor . ", " . 
        "data_pagamento = " . ($excecao_baixa == 1 ? "dateadd(day,dbo.CALC_DIA_UTIL('" . $dataVenc . "'),'" . $dataVenc . "')" : "'" . $dataVenc . "'") . " where id_parcela = " . $_POST['idItem'] . ";
        UPDATE sf_cheques SET sf_cheques.chq_recebido = 1 FROM sf_cheques where tipo = 'V' and codigo_relacao = " . $_POST['idItem'];
        odbc_exec($con, $query) or die(odbc_errormsg());
    } else if ($_POST['tpItem'] == 'LM') {
        $query = "select valor_parcela, id_banco_baixa, data_vencimento data_parcela from sf_lancamento_movimento_parcelas A
        inner join sf_tipo_documento B on A.tipo_documento = B.id_tipo_documento inner join sf_bancos C on B.id_banco_baixa = C.id_bancos
        where id_parcela = " . $_POST['idItem'];
        $cur = odbc_exec($con, $query);
        while ($RFP = odbc_fetch_array($cur)) {
            $valor = $RFP['valor_parcela'];
            $banco = $RFP['id_banco_baixa'];
            $dataVenc = escreverData($RFP['data_parcela']);
        }
        $query = "set dateformat dmy;
        update sf_lancamento_movimento_parcelas set obs_baixa = '" . utf8_decode('AUTOMATICA') . "', id_banco = " . $banco . ", valor_pago = " . $valor . ", " . 
        "data_pagamento = " . ($excecao_baixa == 1 ? "dateadd(day,dbo.CALC_DIA_UTIL('" . $dataVenc . "'),'" . $dataVenc . "')" : "'" . $dataVenc . "'") . " where id_parcela = " . $_POST['idItem'] . ";
        UPDATE sf_cheques SET sf_cheques.chq_recebido = 1 FROM sf_cheques where tipo = 'C' and codigo_relacao = " . $_POST['idItem'];
        odbc_exec($con, $query) or die(odbc_errormsg());
    }
}
odbc_close($con);
