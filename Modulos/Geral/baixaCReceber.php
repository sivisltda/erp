<?php include '../../Connections/configini.php'; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Login - SIVIS Finance</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" href="../../css/bootstrap/bootstrap3.3.7.min.css">
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <script src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <script src="../../js/plugins/bootstrap/bootstrap3.3.7.min.js"></script>
        <script src="js/baixaCReceber.js"></script>
        <style type="text/css">
            #fundo img {
                overflow:hidden;
                max-height:800px;
                width:100%;
            }
            * {
                margin: 0;
                padding:0;
            }
            body, html {
                width: 100%;
                height: 100%;
                font-family: "Segoe UI", arial, sans-serif;
            }
            #fundo img {
                width: 750px;
                position: absolute;
            }
        </style>
    </head>
    <body class="erp-login">
        <div class="erp-logo">
            <img src="../../img/logo_login_sivis.png" width="20%">
        </div>
        <div style="max-width:500px; margin:20px auto">
            <div class="erp-box" style="width: 100%">
                <form>
                    <input id="mainPageGo" name="mainPageGo" type="hidden" value="<?php echo $mainPageGo;?>"/>
                    <h3>Atualizando registros...</h3><span>Não feche a página</span>
                    <div class="progress" id="prgProgresso">
                        <div class="progress-bar progress-bar-striped active" role="progressbar"
                             aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </body>
    <?php odbc_close($con); ?>
</html>