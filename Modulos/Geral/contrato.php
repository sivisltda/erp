<?php
  require_once('contrato_server_processing.php');
?>
<!DOCTYPE html>
<html lang="en">

    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
      <title>Contrato Sivis</title>
      <link rel="icon" type="image/ico" href="../../favicon.ico"/>
      <link href="./../../css/stylesheet.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" type="text/css" href="../../css/main.css">
      <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css">
      <link rel="stylesheet" type="text/css" href="../../css/styleLeo.css">
      <link rel="stylesheet" type="text/css" href="../../css/bootstrap/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="../../assets/plugins/bootstrap/css/bootstrap.css">
    </head>

    <style>
      .contentLoader{
        display: flex;
        align-items: center;
        justify-content: center;
        height: 100%;
        flex-direction: column;
      }

      .erp-box {
        padding: 30px;
        margin: 0 auto;
        margin-bottom: 0;
        padding-top: 20px;
        padding-bottom: 15px;
        width: calc(100% - 60px);
        font-family: "Open Sans", sans-serif;
        background-color: rgba(255,255,255,0.75);
      }

      .erp-box button {
        border: 0;
        color: #EEE;
        float: right;
        font-size: 14px;
        background: #666;
        border-radius: 0;
        text-shadow: none;
        padding: 7px 14px;
        font-family: inherit;
      }

      #loader img {
          position: initial;
          left: initial;
          top: initial;
      }
    </style>

    <body class="erp-login" style="height:100%;">
      <div id="loader"><div class="contentLoader"> <p >Enviando o contrato para <?php echo $emailCli; ?></p> <img src="/img/loader.gif"/> </div></div>
      <div class="erp-logo" style="margin:0;">
        <img src="/img/logo_login.png" width="20%">
      </div>

      <div style="max-width:70%; margin:20px auto;height: 65%;">

        <div class="erp-box" style="height:100%;">

            <p style="color: #006568;font-size: 15px;margin-top: 10px;margin-bottom: 20px;text-align: center;">Você deve ler e aceitar as clausulas do contrato para proseguir.</p>

            <form style="height:80%;" action="contrato.php" method="post">
              <input type="hidden" id="idContrato" value="<?php echo $_GET['bd']; ?>">
              <input type="hidden" name="" id="emailCli" value="<?php echo $emailCli;?>">
              <textarea id="txtcntrato" rows="8" cols="80" style="height:100%;">
                DADOS DO FORNECEDOR:  <?php echo utf8_encode($nomeVip); ?>&#13;
                ENDEREÇO: 		        <?php echo "       ".utf8_encode($endVip).",".$numEndVip.",".utf8_encode($bairroVip).",".utf8_encode($cidadeVip)."-".utf8_encode($ufVip); ?> &#13;
                CNPJ: 			          <?php echo "     ".$cnpjVip; ?> &#13;
                RESPONSÁVEL: 		      <?php echo " ".utf8_encode($contatoVip); ?> &#13;

                DADOS DO CLIENTE: 	  <?php echo "     ".utf8_encode($nomeCli); ?>&#13;
                ENDEREÇO: 		        <?php echo "       ".utf8_encode($endCli).",".$numEndCli.",".utf8_encode($bairroCli).",".utf8_encode($cidadeCli)."-".$ufCli; ?> &#13;
                CNPJ: 			          <?php echo "     ".$cnpjCli; ?> &#13;
                RESPONSÁVEL: 		      <?php echo " ".utf8_encode($contatoCli); ?> &#13;

                Pelo presente instrumento, de um lado Micro University LTDA, pessoa jurídica sediada no Rio de Janeiro, estado do Rio de Janeiro, à Av. Nossa Senhora de Copacabana nº 647, salas 701, 702, 703, 705 e 707 - Copacabana, inscrita no CNPJ SOB n. º. 01.740.699/0001-99, neste ato representada em conformidade com seus atos constitutivos, doravante designada simplesmente CONTRATADA e de outro lado, a CONTRATANTE, indicada e caracterizada no demonstrativo Anexo I anexo a este instrumento, têm, entre si, por justo e combinado o que se segue:

                CLÁUSULA 1ª – OBJETO: 
                O objeto deste Contrato é a Cessão do Direito de Uso dos Sistemas de Gerenciamento e Controle de Acesso, simplesmente denominado Micro, e as respectivas implantações. Também será objeto do presente contrato o atendimento ao cliente para suporte, no que diz respeito exclusivamente ao Software Micro.
                Parágrafo único: Todas as definições,estão descritas neste contrato, princípios e regras que norteiam a cessão do direito de uso dos sistemas de gerenciamento e controle de acesso, a respectivas implantações, bem como o atendimento ao cliente. 

                CLÁUSULA 2ª – PRAZO: 
                “O prazo do presente Contrato é de 1 (um) ano a partir da data de sua assinatura, podendo ser renovado por igual período, automaticamente, caso não seja solicitado o cancelamento.”

                CLÁUSULA 3ª – PREÇO: 
                A CONTRATANTE pagará a CONTRATADA os valores especificados no ANEXO I, que é parte integrante do presente instrumento.

                CLÁUSULA 4ª – PAGAMENTO:
                4.1 - O valor da Cessão do Direito de uso do sistema será cobrado, em data de vencimento a ser definida. Toda cobrança se efetivará mediante emissão de boleto bancário pela CONTRATADA.
                4.2 - O pagamento fora da data de vencimento, implicará na cobrança de multa de 2% (dois por cento) ao mês e mora diária de 0,03% (zero vírgula zero três por cento), conforme especificado no boleto, a contar do dia imediatamente posterior à data do vencimento até o dia de sua efetiva quitação, salvo se o dia seguinte ao vencimento cair em feriados, sábados ou domingos.
                4.3 - No caso do não recebimento do boleto em até 03 (três) dias úteis antes da data de vencimento, a CONTRATANTE poderá imprimir seu boleto pelo Sistema da CONTRATADA (Micro Fitness).  Somente será considerado quitado todo e qualquer valor devido pela CONTRATANTE à CONTRATADA, se o pagamento for efetuado através do boleto bancário emitido pela CONTRATADA, obedecendo-se todas as instruções constantes do boleto como: juros, correções, acréscimos, descontos e/ou outras instruções.

                CLÁUSULA 5ª – OBRIGAÇÕES DA CONTRATADA:
                5.1 – Proceder a instalação do sistema de gerenciamento e controle de acesso escolhido pela CONTRATANTE, .
                5.2 - Disponibilizar à CONTRATANTE atualização(ões) opcionais do(s) software(s) objeto deste contrato, sempre que houver necessidade de atualização, através do portal na internet e sem ônus adicionais, e que tais alterações sejam informadas por escrito a mesma. 
                5.3 – Oferecer o atendimento ao cliente, como suporte e auxílio na utilização dos sistemas de gerenciamento e controle de acessos,.
                5.4 – Oferecer os treinamentos na quantidade e de acordo com o combinado.

                CLÁUSULA 6ª- OBRIGAÇÕES DA CONTRATANTE:
                6.1 –  A CONTRATANTE deve possuir a infra-estrutura necessária para o bom funcionamento dos sistemas, observados nos requisitos mínimos de hardware para o(s) software(s) escolhido(s);
                6.3 – Garantir que somente os técnicos designados pela CONTRATADA realizem os serviços ou manutenção no(s) software(s) escolhido(s);
                6.4 – Manter seus dados cadastrais atualizados junto a CONTRATADA;
                6.5 - Garantir acesso à internet aos computadores no(s) qual(is) o(s) software(s) escolhido(s) forem instalados para garantir utilização de eventuais funções e acesso remoto para manutenção e/ou atualização;

                CLÁUSULA 7ª-CONFIDENCIALIDADE: 
                A CONTRATADA compromete-se a manter confidencialidade sobre as informações e dados da CONTRATANTE que tramitarem através do(s) software(s) escolhido(s).

                CLÁUSULA 8 ª– RESCISÃO:
                8.1 - A rescisão poderá ser feita por ambas às partes, devendo haver uma comunicação formal e por escrito com um prazo mínimo de 30 (trinta) dias de antecedência. 
                8.2 - Fica assegurado à CONTRATADA ou CONTRATANTE o direito de rescindir o Contrato, sem prejuízo de apuração de perdas e danos, caso ocorra o descumprimento de qualquer cláusula deste contrato.
                8.3 - O Atraso ou inadimplência no pagamento deste Contrato, assegura a CONTRATADA o direito de desativar o software, mesmo antes do prazo estipulado, sem a necessidade de comunicação formal.

                CLÁUSULA 9ª – RESTRIÇÕES AO USO DOS SOFTWARES: 
                A CONTRATADA não se responsabiliza pelos resultados produzidos pelo(s) software(s), caso esse seja afetado por algum tipo de programa externo, como aqueles conhecidos popularmente como “vírus”, ou por falha de operação. A CONTRATADA não se responsabiliza ainda, por: integração do(s) software(s), escolhido(s) pela CONTRATANTE, com qualquer outro software de terceiros ou do CONTRATANTE. Operação do software por pessoas não autorizadas; qualquer defeito decorrente 
                de mal uso exclusivo do CONTRATANTE; pelos danos ou prejuízos decorrentes de decisões administrativas, gerenciais ou comerciais tomadas com base nas informações fornecidas pelo sistema; por eventos definidos na legislação civil como caso fortuito ou força maior.

                CLÁUSULA 10ª – DISPOSIÇÕES GERAIS
                10.1 - Ao assinar o presente contrato, o CONTRATANTE tem ciência de todos os recursos disponíveis no(s) software(s) escolhido(s), que lhe foram apresentados por ocasião da Proposta
                Comercial.
                10.2 – O CONTRATANTE está de acordo que o(s) software(s) escolhido(s) foi(ram) desenvolvido(s) de forma a atender ao público em geral, não estando sujeita a CONTRATADA a providenciar alterações exclusivas para a CONTRATANTE e nem a posteriores reclamações. 
                10.3	- Caso haja opção por alterações/adaptações no sistema de gerenciamento e controle de acessos, estes serão objetos de outro contrato sujeito a um novo orçamento e com prazos a serem definidos pela CONTRATADA.
                10.4 - A simples aquisição da licença de uso do sistema de gerenciamento e controle de acesso, objeto deste contrato, vincula a CONTRATANTE a todos seus termos, independentemente do seu uso.  
                10.5 – A CONTRATANTE fica ciente de que a não utilização de quaisquer Sistema(s) cedido(s) não implica em redução de custos.

                CLÁUSULA 11ª – FORO: 
                Fica eleito o foro da Comarca Central da Cidade do Rio de Janeiro - Rj para dirimir quaisquer questões relativas às obrigações assumidas neste Contrato.

                E, por estarem assim certos e ajustados, firmam os signatários o presente contrato, com suas partes integrantes, especificamente o Anexo I em 2 (duas) vias de igual teor e forma, e para um único fim de direito diante de 2 (duas) testemunhas abaixo, que também o subscrevem.

                <?php date_default_timezone_set('America/Sao_Paulo'); ?>                                                                                                                                                          
                <?php  echo "Li e concordei em ". getData("T") . " " . date("H:i:s"); ?>
              </textarea>

              <div class="boxButtons" style="width:68%;">
                    <input type="checkbox" id="readCntrato" value="" disabled style="margin:0;"> <span>Li e concordo com os termos acima.</span>
                    <input type="hidden" name="txtbd" value="<?php echo $_GET['bd']; ?>">
              </div>

              <div class="boxButtons" style="width:32%;justify-content: flex-end;">
                <button type="button" name="button" onclick="window.open('../../Pessoas/' + $('#idContrato').val() + '/Contrato/MuContrato.pdf', '_blank');" title="Salvar" class="ico-download-3 icon-white" disabled></button>
                <button type="submit" name="txtNext" title="Proseguir" onClick="return enviarEmail()" class="glyphicon glyphicon-arrow-right" disabled style="font-family: 'Glyphicons Halflings';"></button>
              </div>

            </form>

          </div>
      </div>

    </body>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    <script type='text/javascript' src='./js/contrato.js'></script>
</html>
