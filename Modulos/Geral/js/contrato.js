var number;
var total;

$("#loader").hide();
$(document).ready(function () {
    $("#txtcntrato").scrollTop($("#txtcntrato").prop("scrollHeight"));
    total = $('#txtcntrato').scrollTop();
    $('#txtcntrato').scrollTop(0);
    number = 0;
    $('#readCntrato').val($(this).is(':checked'));
    $('#readCntrato').change(function () {
        if ($(this).is(":checked")) {
            $("button").removeAttr('disabled');
        } else {
            $("button").attr("disabled", true);
        }
    });
});

$('#txtcntrato').scroll(function () {
    if ($('#txtcntrato').scrollTop() === total) {
        number++;
        if (number > 0) {
            $("#readCntrato").removeAttr('disabled');
        }
    }
});

function enviarEmail() {
    $("#loader").show();
    $.post('../CRM/email.ajax.php', {txtTipo: "F", Modelo: "0", txtId: "005", contratoSivis: "S", idContrato: $('#idContrato').val()}, function (data) {
        if (data === "YES") {
            $.post('../CRM/form/EmailsEnvioFormServer.php', {
                btnEnviar: "S",
                txtId: "",
                contrato: "S",
                ckeditor: "Segue em Anexo",
                idContrato: $('#idContrato').val(),
                emailcli: $('#emailCli').val()
            }, function (d) {
                $.post('contrato_server_processing.php', {
                    txtNext: "S",
                    txtbd: $('#idContrato').val()
                }, function (d) {
                    redirect();
                });
            });
        }
    });

    function redirect() {
        $("#loader").hide();
        window.location.href = "baixaCReceber.php";
    }
    return false;
}
