$(document).ready(function () {
    var total = 100;
    var atual = 0;
    var status = 0;
    $.getJSON("form/baixaCReceber.php?getTotal=S", function (j) {
        if (j !== null) {
            if (j.length > 0) {
                processaItens(j);
            }
        } else {
            $(".progress-bar").html('100%').css('width', "100%");
            window.location = "../../" + $("#mainPageGo").val();
        }
    });
    function processaItens(itens) {
        total = itens.length;
        for (i = 0; i < total; i++) {
            $.ajax({
                type: "POST",
                data: {baixaItem: 'S', idItem: itens[i].id_parcela, tpItem: itens[i].tipo},
                url: "form/baixaCReceber.php",
                async: false
            }).done(function (data) {
                atual += 1;
                status = Math.round(atual * 100 / total);
                $(".progress-bar").html(status + '%').css('width', status + "%");
            });
        }
        window.location = "../../" + $("#mainPageGo").val();
    }
});
