<?php

if (!isset($_GET['Trn'])) {
    echo "Erro Parâmetros!"; exit;
} else {
    $empresa = str_pad($_GET["Emp"], 3, '0', STR_PAD_LEFT);
    $transacao = $_GET["Trn"];
}

$login_usuario = 'SISTEMA';
$id_mens = 0;
$tipo_documento = 0;
$somar_dias = 0;
$valorAdesao = 0;
$valorAcrecimo = 0;
$totalDCC = 0;
$idServicoAcrecimo = 0;
$servico_adesao = 0;

$hostname = $_GET['hostname'];
$database = $_GET['database'];
$username = $_GET['username'];
$password = $_GET['password'];
$contrato = str_replace("ERP", "", strtoupper($_GET['database']));

$dataPagamento = $_GET['dataPagto'];
$adesao_soma = isset($_GET['adesaoSoma']) ? $_GET['adesaoSoma'] : false;

$con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
require_once(__DIR__ . '/../../Connections/funcoesAux.php');
require_once(__DIR__ . '/../../util/util.php');

$cur = odbc_exec($con, "select ACA_SERVICO_ADESAO from sf_configuracao");
while ($RFP = odbc_fetch_array($cur)) {
    $servico_adesao = $RFP['ACA_SERVICO_ADESAO'];
}

if (isset($_SESSION["login_usuario"]) && strlen($_SESSION["login_usuario"]) > 0) {
    $login_usuario = strtoupper($_SESSION["login_usuario"]);
} elseif (isset($_GET["login_usuario"]) && strlen($_GET["login_usuario"]) > 0) {
    $login_usuario = strtoupper($_GET["login_usuario"]);
}

$queryListaDCC = "set dateformat dmy;
select * from sf_vendas_planos_mensalidade A
inner join sf_produtos_parcelas B on (A.id_parc_prod_mens = B.id_parcela and parcela = 
(select top 1 parcela from sf_produtos_parcelas where id_parcela = A.id_parc_prod_mens))
inner join sf_vendas_planos C on A.id_plano_mens = C.id_plano
inner join sf_fornecedores_despesas D on C.favorecido = D.id_fornecedores_despesas
inner join sf_boleto BO ON BO.id_referencia = A.id_mens AND BO.tp_referencia = 'M' 
and dt_pagamento_mens is null and D.inativo = 0 AND id_boleto = " . valoresSelect2($transacao);
$colSql = "count(*) as total";
$cur = odbc_exec($con, str_replace('*', $colSql, $queryListaDCC)) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $totalDCC = $RFP['total'];
}

if ($totalDCC > 0) {
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
    $id_mens = 0;
    $tipo_documento = 0;
    $somar_dias = 0;
    
    if (isset($_GET['txtValorAde']) && valoresNumericos2($_GET['txtValorAde']) > 0) {        
        $valorAdesao = valoresNumericos2($_GET['txtValorAde']);
    }
    $cur = odbc_exec($con, "select top 1 id_referencia as id_plano_item, id_tipo_documento, dias from sf_boleto b inner join sf_tipo_documento td ON td.id_tipo_documento = b.tipo_documento
    where id_venda is null AND B.tp_referencia = 'M' AND id_boleto = " . valoresSelect2($transacao));
    while ($RFP = odbc_fetch_array($cur)) {
        $id_mens = $RFP['id_plano_item'];
        $tipo_documento = $RFP['id_tipo_documento'];
        $somar_dias = $RFP['dias'];
    }
    
    if (is_numeric($id_mens) && $servico_adesao > 0 && !$adesao_soma) {
        $cur = odbc_exec($con, "set dateformat dmy; select conta_produto,preco_venda from sf_produtos where tipo = 'S' and inativa = 0 and conta_produto in (
        select top 1 id_servico from sf_vendas_planos vp
        inner join sf_produtos_acrescimo_servicos pa on pa.id_produto = vp.id_prod_plano
        and (select count(id_dependente) from sf_fornecedores_despesas_dependentes where id_titular = favorecido) between qtd_min and qtd_max
        inner join sf_produtos on conta_produto = vp.id_prod_plano
        inner join sf_vendas_planos_mensalidade on id_plano = id_plano_mens
        where id_mens = " . valoresSelect2($id_mens) . " and adesao_tipo = 1 and 
        adesao_intervalo = month(". valoresData2($dataPagamento) .") order by qtd_max desc)");
        while ($RFP = odbc_fetch_array($cur)) {
            $idServicoAcrecimo = $RFP['conta_produto'];
            $valorAcrecimo = $RFP['preco_venda'];
        }
    }

    if (is_numeric($id_mens) && is_numeric($transacao)) {
        odbc_autocommit($con, false);
        $historico = $adesao_soma ?  utf8_decode('ABONO / PLANO') : utf8_decode('PAG PLANO / SERVIÇO');
        $query = "set dateformat dmy; insert into sf_vendas (vendedor,cliente_venda,historico_venda,data_venda,sys_login,empresa,destinatario,status,tipo_documento,
        cov,descontop,descontos,descontotp,descontots,n_servicos,data_aprov,grupo_conta,conta_movimento,favorito,reservar_estoque, n_produtos)
        select 'SISTEMA' as vendedor, favorecido as cliente_venda, '" . $historico . "' as historico_venda, ". valoresData2($dataPagamento) ." as data_venda,
        'SISTEMA' as sys_login , 1 as empresa, 'SISTEMA' as destinatario, 'Aprovado' as status, tipo_documento,
        'V' as cov, 0.00 as descontop, 0.00 as descontos, 0 as descontotp, 0 as descontots, 0 as n_servicos, ". valoresData2($dataPagamento) ." as data_aprov,
        14 as grupo_conta, 1 as conta_movimento, 0 as favorito, 0 as reservar_estoque, 0 as n_produtos
        from sf_boleto b
        inner join sf_vendas_planos_mensalidade vpm ON vpm.id_mens = b.id_referencia AND b.tp_referencia = 'M'
        inner join sf_vendas_planos vp ON vp.id_plano = vpm.id_plano_mens
        where b.id_venda is null AND vpm.dt_pagamento_mens is null AND id_boleto = " . valoresSelect2($transacao) . "; SELECT SCOPE_IDENTITY() ID;";
        $result = odbc_exec($con, $query);
        odbc_next_result($result);
        $idVenda = odbc_result($result, 1);
        if (!($idVenda && is_numeric($idVenda))) {
            odbc_rollback($con);
            echo "ERROR" . $query;
            exit();
        }
        
        $valorTransacao = $adesao_soma ? '0.00' : "(bol_valor - " . $valorAdesao . ")";
        $query = "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total,valor_bruto)
        select " . valoresSelect2($idVenda) . ", conta_movimento,id_prod_plano,1, $valorTransacao valor_transacao,
        $valorTransacao valor_transacao from sf_boleto B
        inner join sf_vendas_planos_mensalidade A ON A.id_mens = B.id_referencia AND B.tp_referencia = 'M'
        inner join sf_vendas_planos VP ON VP.id_plano = A.id_plano_mens
        left join sf_produtos P ON P.conta_produto = VP.id_prod_plano 
        where id_venda is null AND id_boleto = " . valoresSelect2($transacao) . ";";
        if ($valorAdesao > 0 || !$adesao_soma) {
            $query .= "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total,valor_bruto)
            select " . valoresSelect2($idVenda) . ",conta_movimento,conta_produto,1, " . $valorAdesao . ", " . $valorAdesao . " from sf_produtos where inativa = 0 
            and conta_produto in (select conta_produto_adesao from sf_boleto B
            inner join sf_vendas_planos_mensalidade A ON A.id_mens = B.id_referencia AND B.tp_referencia = 'M'
            inner join sf_vendas_planos VP ON VP.id_plano = A.id_plano_mens
            left join sf_produtos P ON P.conta_produto = VP.id_prod_plano where id_venda is null AND id_boleto =" . valoresSelect2($transacao) . ");";
        }

        if ($valorAcrecimo > 0 && !$adesao_soma) {
            $query .= "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total,valor_bruto)
            select " . valoresSelect2($idVenda) . ",conta_movimento,conta_produto,1,preco_venda,preco_venda from sf_produtos where inativa = 0 
            and conta_produto in (" . $idServicoAcrecimo . ") and conta_produto > 0 and preco_venda > 0;";
        }

        if ($valorAdesao > 0 || !$adesao_soma) {
            $query .= "insert into sf_venda_parcelas(venda,numero_parcela,data_parcela,valor_parcela,valor_pago,data_pagamento,
            id_banco,pa,inativo,historico_baixa,obs_baixa,valor_multa,valor_juros,syslogin,tipo_documento,valor_parcela_liquido, data_cadastro, exp_remessa)                        
            select " . valoresSelect2($idVenda) . ",1,dateadd(day," . $somar_dias . ", ". valoresData2($dataPagamento) ."), 
            bol_valor, 0.00, dateadd(day," . $somar_dias . ", ". valoresData2($dataPagamento) ."),
            (select isnull(id_banco_baixa, 1) from sf_tipo_documento where id_tipo_documento = tipo_documento),           
            '01/01',0,'AUTOMATICA', null, 0.00, 0.00, 'SISTEMA', tipo_documento, 0.00, ". valoresData2($dataPagamento) .", 0
            from sf_boleto where id_venda is null AND id_boleto = " . valoresSelect2($transacao) . ";";            
            
            $query .= "update sf_vendas set conta_movimento = [dbo].[FU_SUB_GRUPO_VENDA](id_venda) where cov = 'V' and id_venda = " . valoresSelect2($idVenda) . ";";
        }

        $query .= "update sf_vendas_planos_mensalidade set
        id_item_venda_mens = id_item_venda,dt_pagamento_mens = ". valoresData2($dataPagamento) ."
        from sf_boleto b inner join sf_vendas_planos_mensalidade vpm ON vpm.id_mens = b.id_referencia AND b.tp_referencia = 'M'
        inner join sf_vendas_planos vp ON vp.id_plano = vpm.id_plano_mens
        inner join sf_vendas_itens vi ON vi.produto = vp.id_prod_plano 
        where b.id_venda is null AND id_boleto = " . valoresSelect2($transacao) . " and vi.id_venda = " . valoresSelect2($idVenda) . ";";

        $query .= "insert into sf_vendas_planos_mensalidade (id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens)
        select id_plano_mens, dateadd(month, (select case when parcela < 1 then 1 else parcela end), dt_inicio_mens),
        dateadd(day,-1, dateadd(month, 2 * (select case when parcela < 1 then 1 else parcela end), dt_inicio_mens)), 
        id_parc_prod_mens, ". valoresData2($dataPagamento) .",valor_unico valor_mens 
        from sf_vendas_planos_mensalidade m1 inner join sf_produtos_parcelas on id_parc_prod_mens = id_parcela 
        inner join sf_produtos on sf_produtos.conta_produto = sf_produtos_parcelas.id_produto
        inner join sf_vendas_planos on id_plano = id_plano_mens        
        where inativar_renovacao = 0 and id_mens = " . valoresSelect2($id_mens) . " and dt_cancelamento is null  
        and (select COUNT(*) from sf_vendas_planos_mensalidade m2
        where m2.id_plano_mens = m1.id_plano_mens and m2.id_parc_prod_mens = m1.id_parc_prod_mens
        and month(m2.dt_inicio_mens) = month(dateadd(month ,(select case when parcela < 1 then 1 else parcela end),m1.dt_inicio_mens))
        and year(m2.dt_inicio_mens) = year(dateadd(month ,(select case when parcela < 1 then 1 else parcela end) ,m1.dt_inicio_mens))) = 0;";

        $query .= "update sf_boleto set id_venda = " . valoresSelect2($idVenda) . " where id_boleto = " . valoresSelect2($transacao) . ";";

        $query .= "update sf_vendas_planos set dt_fim = dbo.FU_PLANO_FIM(id_plano) where id_plano in (
        select id_plano_mens from sf_vendas_planos_mensalidade where id_mens = " . valoresSelect2($id_mens) . ");";

        if ($indicador = filter_input(INPUT_GET, 'ind')) {
            $indicador = $indicador ? (int) encrypt($indicador, "VipService123", false) : null;
            if (is_numeric($indicador)) {
                $sql_query = "select TOP 1 fd.tipo, login_user from sf_fornecedores_despesas fd inner join sf_usuarios u  on fd.id_fornecedores_despesas = u.funcionario"
                    . " where fd.id_fornecedores_despesas = " . valoresSelect2($indicador) . " and fd.tipo = 'E' ";
                $rs_fornecedor = odbc_exec($con, $sql_query) or die(odbc_errormsg());
                if (odbc_num_rows($rs_fornecedor) > 0) {
                    $fornecedor = odbc_fetch_array($rs_fornecedor);
                    $query .= "update sf_vendas set vendedor = '" . $fornecedor['login_user'] . "' where id_venda =" . valoresSelect2($idVenda) . ";";
                } else {
                    $query .= "update sf_vendas set indicador = " . $indicador . " where id_venda =" . valoresSelect2($idVenda) . ";";
                }
            }
        }

        $query .= "EXEC dbo.SP_MK_VISTORIA @id_venda = " . $idVenda . ", @loja = '" . $contrato . "';";
        
        $objExec = odbc_exec($con, $query);
        if (!$objExec) {
            odbc_rollback($con);
            echo "ERROR" . $query;
            exit();
        }
        if ($objExec) {
            odbc_commit($con);
            odbc_autocommit($con, true);
            echo $idVenda;
            exit;
        }
    }

}
echo "Erro, dados não encontrados!";
exit;