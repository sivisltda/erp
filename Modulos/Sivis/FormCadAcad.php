<?php
include './../../Connections/configini.php';
$disabled = 'disabled';

function valoresCheckX($nome) {
    if ($_POST[$nome] == "") {
        return "-";
    } else {
        return $_POST[$nome];
    }
}

if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update ca_clientes set " .
                        "sf_cliente = " . utf8_decode($_POST['txtFonecedor']) . "," .
                        "num_provs = " . valoresNumericos('txtNumProvs') . "," .
                        "tipo_cli = " . valoresTexto('txtTipoCliente') . "," .
                        "opc_cli = '" . valoresCheckX('ckb_Acesso') . valoresCheckX('ckb_Financeiro') . valoresCheckX('ckb_Marketing') . valoresCheckX('ckb_Turmas') . valoresCheckX('ckb_Vendas') . valoresCheckX('ckb_Clube') . valoresCheckX('ckb_GymWeb') . valoresCheckX('ckb_DCC') . "'," .
                        "possui_tk = " . valoresCheck('txtToken') . "," .
                        "versao = '" . valoresNumericos('txtVersao1') . "." . valoresNumericos('txtVersao2') . "." . valoresNumericos('txtVersao3') . "'," .
                        "cod_plugue = " . valoresTexto('txtPlugue') . "," .
                        "numLicencas = " . valoresNumericos('txtLicencas') . "," .
                        "suporte_token = " . valoresCheck('ckb_Token02') . "," .
                        "suporte_token3 = " . valoresCheck('ckb_Token03') . "," .
                        "dcc_tp_operacao = " . valoresTexto('txtDccTpOperacao') . "," .
                        "dcc_bloqueio = " . valoresTexto('txtDccBloqueio') . "," .
                        "dcc_loja = " . valoresTexto('txtDccLoja') . "," .
                        "dcc_chave = " . valoresTexto('txtDccChave') . "," .
                        "dcc_tipo = " . valoresNumericos('txtDccTipo') . "," .
                        "dcc_operadora = " . valoresNumericos('txtDccOperadora') . "," .
                        "dcc_usuario = " . valoresTexto('txtDccUsuario') . "," .
                        "dcc_senha = " . valoresTexto('txtDccSenha') . "," .
                        "dcc_loja2 = " . valoresTexto('txtDccLoja2') . "," .
                        "dcc_chave2 = " . valoresTexto('txtDccChave2') . "," .
                        "dcc_tipo2 = " . valoresNumericos('txtDccTipo2') . "," .
                        "dcc_operadora2 = " . valoresNumericos('txtDccOperadora2') . "," .
                        "dcc_usuario2 = " . valoresTexto('txtDccUsuario2') . "," .
                        "dcc_senha2 = " . valoresTexto('txtDccSenha2') . "," .
                        "opc_body = " . valoresTexto('ckb_body') . "," .
                        "opc_gobody = " . valoresTexto('ckb_gobody') . "," .
                        "opc_training = " . valoresTexto('ckb_training') . "," .
                        "opc_biometrico = " . valoresTexto('ckb_biometrico') . "," .
                        "opc_sms = " . valoresTexto('ckb_sms') . "," .
                        "opc_cartao = " . valoresTexto('ckb_cartao') . "," .
                        "opc_adaptadorusb = " . valoresCheck('txtUSB') . "," .
                        "opc_catraca = " . valoresTexto('txtCatraca') . "," .
                        "sms_usuario = " . valoresTexto('txtSmsUsuario') . "," .
                        "sms_senha = " . valoresTexto('txtSmsSenha') . ", " .
                        "sms_custo = " . valoresNumericos('txtSmsCusto') . "," .
                        "tp_franquia = " . valoresTexto('txtTpFranquia') . "" .
                        "where cod_acad = " . $_POST['txtId']) or die(odbc_errormsg());
        $totalReg = 0;
        $cur = odbc_exec($con, "Select count(*) total from ca_cliplugue where COD_ACAD = " . $_POST['txtId'] . " AND COD_PLUGUE = " . valoresTexto('txtPlugue'));
        while ($RFP = odbc_fetch_array($cur)) {
            $totalReg = $RFP['total'];
        }
        if ($totalReg == 0) {
            odbc_exec($con, "INSERT INTO ca_cliplugue VALUES(" . $_POST['txtId'] . "," . valoresTexto('txtPlugue') . ",getDate())");
        }
    } else {
        $query = "SET IDENTITY_INSERT ca_clientes ON;INSERT INTO ca_clientes(cod_acad,sf_cliente,num_provs,tipo_cli,opc_cli,possui_tk,versao,cod_plugue,numLicencas,dcc_tp_operacao, dcc_bloqueio
                ,dcc_loja,dcc_chave,dcc_tipo,dcc_operadora,dcc_usuario,dcc_senha,dcc_loja2,dcc_chave2,dcc_tipo2,dcc_operadora2,dcc_usuario2,dcc_senha2,suporte_token,suporte_token3,opc_body,
                opc_gobody,opc_training,opc_biometrico,opc_sms,opc_cartao,opc_adaptadorusb,opc_catraca,sms_usuario,sms_senha,sms_custo,tp_franquia)values(" .
                valoresNumericos('txtCodigo') . "," .
                utf8_decode($_POST['txtFonecedor']) . "," .
                valoresNumericos('txtNumProvs') . "," .
                valoresTexto('txtTipoCliente') . ",'" .
                valoresCheckX('ckb_Acesso') . valoresCheckX('ckb_Financeiro') . valoresCheckX('ckb_Marketing') . valoresCheckX('ckb_Turmas') . valoresCheckX('ckb_Vendas') . valoresCheckX('ckb_Clube') . valoresCheckX('ckb_GymWeb') . valoresCheckX('ckb_DCC') . "'," .
                valoresCheck('txtToken') . ",'" .
                valoresNumericos('txtVersao1') . "." . valoresNumericos('txtVersao2') . "." . valoresNumericos('txtVersao3') . "'," .
                valoresTexto('txtPlugue') . "," .
                valoresNumericos('txtLicencas') . "," .
                valoresTexto('txtDccTpOperacao') . "," .
                valoresTexto('txtDccBloqueio') . "," .
                valoresTexto('txtDccLoja') . "," .
                valoresTexto('txtDccChave') . "," .
                valoresNumericos('txtDccTipo') . "," .
                valoresNumericos('txtDccOperadora') . "," .
                valoresTexto('txtDccUsuario') . "," .
                valoresTexto('txtDccSenha') . "," .
                valoresTexto('txtDccLoja2') . "," .
                valoresTexto('txtDccChave2') . "," .
                valoresNumericos('txtDccTipo2') . "," .
                valoresNumericos('txtDccOperadora2') . "," .
                valoresTexto('txtDccUsuario2') . "," .
                valoresTexto('txtDccSenha2') . "," .
                valoresCheck('ckb_Token02') . "," .
                valoresCheck('ckb_Token03') . "," .
                valoresCheck('ckb_body') . "," .
                valoresCheck('ckb_gobody') . "," .
                valoresCheck('ckb_training') . "," .
                valoresCheck('ckb_biometrico') . "," .
                valoresCheck('ckb_sms') . "," .
                valoresCheck('ckb_cartao') . "," . valoresCheck('txtUSB') . "," . valoresTexto('txtCatraca') . "," .
                valoresTexto('txtSmsUsuario') . "," .
                valoresTexto('txtSmsSenha') . "," .
                valoresNumericos('txtSmsCusto') . "," .
                valoresTexto('txtTpFranquia') . ");SET IDENTITY_INSERT ca_clientes OFF;";
        odbc_exec($con, $query) or die(odbc_errormsg());
        odbc_exec($con, "INSERT INTO ca_cliplugue VALUES(" . valoresNumericos('txtCodigo') . "," . valoresTexto('txtPlugue') . ",getDate())");
        $_POST['txtId'] = valoresNumericos('txtCodigo');
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM ca_cliplugue where COD_ACAD = " . $_POST['txtId']);
        odbc_exec($con, "DELETE FROM ca_clisenha where cod_acad = " . $_POST['txtId']);
        odbc_exec($con, "DELETE FROM ca_clientes WHERE cod_acad = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso');parent.FecharBox(0);</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "SELECT *,
                            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = fd.id_fornecedores_despesas) telefone_contato,
                            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = fd.id_fornecedores_despesas) email_contato
                            FROM ca_clientes inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = ca_clientes.sf_cliente
                            LEFT JOIN tb_estados e on f.estado = e.estado_codigo left join tb_cidades c on f.cidade = c.cidade_codigo
                            LEFT JOIN sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = sf_cliente where cod_acad = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['cod_acad'];
        $numprovs = utf8_encode($RFP['num_provs']);
        $cliente_venda = utf8_encode($RFP['id_fornecedores_despesas']);
        $nomecliente_venda = utf8_encode($RFP['razao_social']);
        $tipoCliente = utf8_encode($RFP['tipo_cli']);
        $licencas = utf8_encode($RFP['numLicencas']);
        $item = explode(".", $RFP['versao']);
        if (count($item) == 3) {
            $versao1 = $item[0];
            $versao2 = $item[1];
            $versao3 = $item[2];
        }
        $plugue = utf8_encode($RFP['cod_plugue']);
        if (substr($RFP['opc_cli'], 0, 1) == "A") {
            $mod_acesso = "1";
        } else {
            $mod_acesso = "0";
        }
        if (substr($RFP['opc_cli'], 1, 1) == "F") {
            $mod_financeiro = "1";
        } else {
            $mod_financeiro = "0";
        }
        if (substr($RFP['opc_cli'], 2, 1) == "M") {
            $mod_marketing = "1";
        } else {
            $mod_marketing = "0";
        }
        if (substr($RFP['opc_cli'], 3, 1) == "T") {
            $mod_turmas = "1";
        } else {
            $mod_turmas = "0";
        }
        if (substr($RFP['opc_cli'], 4, 1) == "V") {
            $mod_vendas = "1";
        } else {
            $mod_vendas = "0";
        }
        if (substr($RFP['opc_cli'], 5, 1) == "C") {
            $mod_clube = "1";
        } else {
            $mod_clube = "0";
        }
        if (substr($RFP['opc_cli'], 6, 1) == "W") {
            $mod_gymweb = "1";
        } else {
            $mod_gymweb = "0";
        }
        if (substr($RFP['opc_cli'], 7, 1) == "D") {
            $mod_dcc = "1";
        } else {
            $mod_dcc = "0";
        }
        $token = utf8_encode($RFP['possui_tk']);
        $tks02 = $RFP['suporte_token'];
        $tks03 = $RFP['suporte_token3'];
        $contato = utf8_encode($RFP['contato']);
        if ($RFP['endereco'] != '') {
            $dados1 = utf8_encode($RFP['endereco']);
        }
        if ($RFP['numero'] != '') {
            $dados1 = $dados1 . ' N° ' . utf8_encode($RFP['numero']);
        }
        if ($RFP['complemento'] != '') {
            $dados1 = $dados1 . ' ' . utf8_encode($RFP['complemento']);
        }
        if ($RFP['bairro'] != '') {
            $dados1 = $dados1 . ',' . utf8_encode($RFP['bairro']);
        }
        if ($RFP['cidade_nome'] != '') {
            $dados1 = $dados1 . "\n" . utf8_encode($RFP['cidade_nome']);
        }
        if ($RFP['estado_sigla'] != '') {
            $dados1 = $dados1 . '-' . utf8_encode($RFP['estado_sigla']);
        }
        if ($RFP['cep'] != '') {
            $dados1 = $dados1 . ', Cep: ' . utf8_encode($RFP['cep']);
        }
        if ($RFP['cnpj'] != '') {
            $dados2 = $dados2 . 'CNPJ/CPF:' . utf8_encode($RFP['cnpj']);
        }
        if ($RFP['telefone_contato'] != '') {
            $dados2 = $dados2 . "\n" . 'Tel:' . utf8_encode($RFP['telefone_contato']);
        }
        if ($RFP['email_contato'] != '') {
            $dados2 = $dados2 . ' E-mail:' . utf8_encode($RFP['email_contato']);
        }
        $dccBloqueio = utf8_encode($RFP['dcc_bloqueio']);
        $dccTpOperacao = utf8_encode($RFP['dcc_tp_operacao']);
        $dccLoja = utf8_encode($RFP['dcc_loja']);
        $dccChave = utf8_encode($RFP['dcc_chave']);
        $dccTipo = utf8_encode($RFP['dcc_tipo']);
        $dccOperadora = utf8_encode($RFP['dcc_operadora']);
        $dccUsuario = utf8_encode($RFP['dcc_usuario']);
        $dccSenha = utf8_encode($RFP['dcc_senha']);
        $dccLoja2 = utf8_encode($RFP['dcc_loja2']);
        $dccChave2 = utf8_encode($RFP['dcc_chave2']);
        $dccTipo2 = utf8_encode($RFP['dcc_tipo2']);
        $dccOperadora2 = utf8_encode($RFP['dcc_operadora2']);
        $dccUsuario2 = utf8_encode($RFP['dcc_usuario2']);
        $dccSenha2 = utf8_encode($RFP['dcc_senha2']);
        $tks_body = utf8_encode($RFP['opc_body']);
        $tks_gobody = utf8_encode($RFP['opc_gobody']);
        $tks_training = utf8_encode($RFP['opc_training']);
        $tksbiometrico = utf8_encode($RFP['opc_biometrico']);
        $tkssms = utf8_encode($RFP['opc_sms']);
        $tkscartao = utf8_encode($RFP['opc_cartao']);
        $catraca = utf8_encode($RFP['opc_catraca']);
        $usb = utf8_encode($RFP['opc_adaptadorusb']);
        $smsUsuario = utf8_encode($RFP['sms_usuario']);
        $smsSenha = utf8_encode($RFP['sms_senha']);
        $smsCusto = escreverNumero($RFP['sms_custo'], 0, 4);
        $tpFranquia = utf8_encode($RFP['tp_franquia']);
    }
} else {
    $disabled = '';
    $id = '';
    $res = odbc_exec($con, "select top 1 cod_acad from ca_clientes order by cod_acad desc") or die(odbc_errormsg());
    $idSugere = odbc_result($res, 1) + 1;
    $numprovs = '32000';
    $cliente_venda = '';
    $nomecliente_venda = '';
    $tipoCliente = 'A';
    $licencas = '3';
    $contato = '';
    $versao1 = '';
    $versao2 = '';
    $versao3 = '';
    $plugue = '';
    $token = '1';
    $mod_acesso = '0';
    $mod_financeiro = '0';
    $mod_marketing = '0';
    $mod_turmas = '0';
    $mod_vendas = '0';
    $mod_clube = '0';
    $mod_gymweb = '0';
    $mod_dcc = '0';
    $tks02 = '';
    $tks03 = '1';
    $dados1 = '';
    $dados2 = '';
    $dccTpOperacao = '0';
    $dccBloqueio = '0';
    $dccLoja = '';
    $dccChave = '';
    $dccTipo = '0';
    $dccOperadora = '1';
    $dccUsuario = '';
    $dccSenha = '';
    $dccLoja2 = '';
    $dccChave2 = '';
    $dccTipo2 = '0';
    $dccOperadora2 = '1';
    $dccUsuario2 = '';
    $dccSenha2 = '';
    $tks_body = '';
    $tks_gobody = '';
    $tks_training = '';
    $tksbiometrico = '';
    $tkssms = '';
    $tkscartao = '';
    $catraca = '';
    $usb = '';
    $smsUsuario = '';
    $smsSenha = '';
    $tpFranquia = '';
    $smsCusto = escreverNumero(0, 0, 4);
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <form action="FormCadAcad.php" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Cad Acad</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont">
            <div class="data-fluid tabbable" style="overflow:hidden; height:275px">
                <ul class="nav nav-tabs">
                    <li class="active" style="margin-left:10px"><a href="#tab1" data-toggle="tab">Dados Cliente</a></li>
                    <li><a href="#tab2" data-toggle="tab">Tipo Cliente</a></li>
                    <li><a href="#tab3" data-toggle="tab">Tipo Licença</a></li>
                    <li><a href="#tab4" data-toggle="tab">Histórico Plugue</a></li>
                    <li><a href="#tab5" data-toggle="tab">Parâmetros DCC</a></li>
                    <li><a href="#tab6" data-toggle="tab">Parâmetros SMS</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                        <table width="680" border="0" cellpadding="3" cellspacing="0" class="textosComuns">
                            <tr>
                                <td width="100" align="right">Código:</td>
                            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                            <td width="234"><input name="txtCodigo" id="txtCodigo" <?php
                                if (is_numeric($cliente_venda)) {
                                    echo "disabled";
                                }
                                ?> type="text" class="input-medium" value="<?php
                                if (is_numeric($id)) {
                                    echo str_pad($id, 3, "0", STR_PAD_LEFT);
                                } else {
                                    echo str_pad($idSugere, 3, "0", STR_PAD_LEFT);
                                }
                                ?>"/></td>
                            <td width="100" align="right">Num. Provs:</td>
                            <td><input name="txtNumProvs" id="txtNumProvs" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $numprovs; ?>"/></td>
                            </tr>
                            <tr>
                                <td width="100" align="right">Cliente:</td>
                                <td colspan="3">
                                    <input name="txtFonecedor" id="txtFonecedor" value="<?php echo $cliente_venda; ?>" type="hidden"/>
                                    <input type="text" style="width:100%" <?php echo $disabled; ?> name="txtDesc" id="txtDesc" class="inputbox" value="<?php echo $nomecliente_venda; ?>" size="10" />
                                </td>
                            </tr>
                            <tr>
                                <td width="100" align="right">Dados do Cliente:</td>
                                <td colspan="3"><textarea disabled="true" name="txtDados" id="txtDados" style="width:100%" rows="10"><?php echo $dados2; ?></textarea></td>
                            </tr>
                            <tr>
                                <td width="100" align="right">Endereço:</td>
                                <td colspan="3"><textarea disabled="true" name="txtEntrega" id="txtEntrega" style="width:100%" rows="10"><?php echo $dados1; ?></textarea></td>
                            </tr>
                        </table>
                    </div>
                    <div class="tab-pane" id="tab2">
                        <table width="680" style=" overflow: hidden;" border="0" cellpadding="3" cellspacing="0" class="textosComuns">
                            <tr>
                                <td width="100" align="right">Tipo de Cliente:</td>
                                <td width="234"><select name="txtTipoCliente" id="txtTipoCliente" class="select" style="width:100%" <?php echo $disabled; ?>>
                                        <option value="A" <?php
                                        if ($tipoCliente == "A") {
                                            echo "SELECTED";
                                        }
                                        ?>>Aluguel</option>
                                        <option value="C" <?php
                                        if ($tipoCliente == "C") {
                                            echo "SELECTED";
                                        }
                                        ?>>Compra</option>
                                    </select></td>
                                <td width="100" align="right">Nº Licenças:</td>
                                <td><input name="txtLicencas" id="txtLicencas" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $licencas; ?>"/></td>
                            </tr>
                            <tr>
                                <td align="right">Contato:</td>
                                <td><input disabled type="text" id="txtContato" class="input-medium" value="<?php echo $contato; ?>"/></td>
                                <td align="right">Versão:</td>
                                <td>
                                    <input name="txtVersao1" id="txtVersao1" <?php echo $disabled; ?> type="text" class="input-medium" style="width:67px" value="<?php echo $versao1; ?>"/>
                                    <span style="width:3px; text-align:center"><b>.</b></span>
                                    <input name="txtVersao2" id="txtVersao2" <?php echo $disabled; ?> type="text" class="input-medium" style="width:68px" value="<?php echo $versao2; ?>"/>
                                    <span style="width:3px; text-align:center"><b>.</b></span>
                                    <input name="txtVersao3" id="txtVersao3" <?php echo $disabled; ?> type="text" class="input-medium" style="width:68px" value="<?php echo $versao3; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">Plugue:</td>
                                <td><input name="txtPlugue" id="txtPlugue" <?php echo $disabled; ?> type="text" class="input-medium" style="width: 126px;" value="<?php echo $plugue; ?>"/>
                                    <button id="btnNewPlug" name="btnNewPlug" <?php echo $disabled; ?> class="btn btn-primary" type="button" title="Novo Plugue"><span class="ico-file-4 icon-white"></span> Novo Plugue</button></td>
                                <td align="right">Possui Token:</td>
                                <td><input name="txtToken" id="ckb_Token20" <?php
                                    echo $disabled;
                                    if ($token == 1) {
                                        echo " CHECKED";
                                    }
                                    ?> value="1" type="checkbox" class="input-medium"/></td>
                            </tr>
                            <tr>
                                <td align="right">Catraca:</td>
                                <td><input name="txtCatraca" id="txtCatraca" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $catraca; ?>"/></td>
                                <td align="right">Adaptador USB:</td>
                                <td><input name="txtUSB" id="ckb_USB" <?php
                                    echo $disabled;
                                    if ($usb == 1) {
                                        echo " CHECKED";
                                    }
                                    ?> value="1" type="checkbox" class="input-medium"/></td>
                            </tr>
                            <tr>
                                <td width="100" align="right">Tipo Franquia:</td>
                                <td width="234"><select name="txtTpFranquia" id="txtTpFranquia" class="select" style="width:100%" <?php echo $disabled; ?>>
                                        <option value="S" <?php
                                        if ($tpFranquia == "S") {
                                            echo "SELECTED";
                                        }
                                        ?>>Sivis</option>
                                    </select></td>
                                <td width="100" align="right"></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                    <div class="tab-pane" id="tab3">
                        <table width="680" border="0" cellpadding="3" cellspacing="0" class="textosComuns">
                            <tr>
                                <td colspan="8" style="background:#EEE; text-align:center"><b>LICENÇAS</b></td>
                            </tr>
                            <tr>
                                <td width="100" height="33"><div style="float:right">
                                        <input name="ckb_Acesso" id="ckb_Acesso" <?php
                                        echo $disabled;
                                        if ($mod_acesso == 1) {
                                            echo " CHECKED";
                                        }
                                        ?> value="A" type="checkbox" class="input-medium"/>
                                    </div></td><td>Acesso</td>
                                <td width="60" height="33"><div style="float:right">
                                        <input name="ckb_Turmas" id="ckb_Turmas" <?php
                                        echo $disabled;
                                        if ($mod_turmas == 1) {
                                            echo " CHECKED";
                                        }
                                        ?> value="T" type="checkbox" class="input-medium"/>
                                    </div></td><td>Turmas</td>
                                <td width="60" height="33"><div style="float:right">
                                        <input name="ckb_Financeiro" id="ckb_Financeiro" <?php
                                        echo $disabled;
                                        if ($mod_financeiro == 1) {
                                            echo " CHECKED";
                                        }
                                        ?> value="F" type="checkbox" class="input-medium"/>
                                    </div></td><td>Financeiro</td>
                                <td height="33"><div style="float:right">
                                        <input name="ckb_Vendas" id="ckb_Vendas" <?php
                                        echo $disabled;
                                        if ($mod_vendas == 1) {
                                            echo " CHECKED";
                                        }
                                        ?> value="V" type="checkbox" class="input-medium"/>
                                    </div></td><td>Vendas</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right">
                                        <input name="ckb_Marketing" id="ckb_Marketing" <?php
                                        echo $disabled;
                                        if ($mod_marketing == 1) {
                                            echo " CHECKED";
                                        }
                                        ?> value="M" type="checkbox" class="input-medium"/>
                                    </div></td><td>Marketing</td>
                                <td height="33"><div style="float:right">
                                        <input name="ckb_Clube" id="ckb_Clube" <?php
                                        echo $disabled;
                                        if ($mod_clube == 1) {
                                            echo " CHECKED";
                                        }
                                        ?> value="C" type="checkbox" class="input-medium"/>
                                    </div></td><td>Clube</td>
                                <td height="33"><div style="float:right">
                                        <input name="ckb_GymWeb" id="ckb_GymWeb" <?php
                                        echo $disabled;
                                        if ($mod_gymweb == 1) {
                                            echo " CHECKED";
                                        }
                                        ?> value="W" type="checkbox" class="input-medium"/>
                                    </div></td><td>Gym Web</td>
                                <td height="33"><div style="float:right">
                                        <input name="ckb_DCC" id="ckb_DCC" <?php
                                        echo $disabled;
                                        if ($mod_dcc == 1) {
                                            echo " CHECKED";
                                        }
                                        ?> value="D" type="checkbox" class="input-medium"/>
                                    </div></td><td>DCC</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right">
                                        <input name="ckb_biometrico" id="ckb_biometrico" <?php
                                        echo $disabled;
                                        if ($tksbiometrico == 1) {
                                            echo " CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/>
                                    </div></td><td>Leitor Biométrico</td>
                                <td height="33"><div style="float:right">
                                        <input name="ckb_sms" id="ckb_sms" <?php
                                        echo $disabled;
                                        if ($tkssms == 1) {
                                            echo " CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/>
                                    </div></td><td>SMS</td>
                                <td height="33"></td>
                                <td height="33"></td>
                            </tr>
                            <tr>
                                <td colspan="8" style="background:#EEE; text-align:center"><b>OUTROS</b></td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right">
                                        <input name="ckb_Token02" id="ckb_Token20" <?php
                                        echo $disabled;
                                        if ($tks02 == 1) {
                                            echo " CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/>
                                    </div></td><td>Token 2.0</td>
                                <td height="33"><div style="float:right">
                                        <input name="ckb_Token03" id="ckb_Token30" <?php
                                        echo $disabled;
                                        if ($tks03 == 1) {
                                            echo " CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/>
                                    </div></td><td>Token 3.0</td>
                                <td height="33"><div style="float:right">
                                        <input name="ckb_cartao" id="ckb_cartao" <?php
                                        echo $disabled;
                                        if ($tkscartao == 1) {
                                            echo " CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/>
                                    </div></td><td>Leitor de Cartão</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right">
                                        <input name="ckb_body" id="ckb_body" <?php
                                        echo $disabled;
                                        if ($tks_body == 1) {
                                            echo " CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/>
                                    </div></td><td>BODY</td>
                                <td height="33"><div style="float:right">
                                        <input name="ckb_gobody" id="ckb_gobody" <?php
                                        echo $disabled;
                                        if ($tks_gobody == 1) {
                                            echo " CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/>
                                    </div></td><td>GOBODY</td>
                                <td height="33"><div style="float:right">
                                        <input name="ckb_training" id="ckb_training" <?php
                                        echo $disabled;
                                        if ($tks_training == 1) {
                                            echo " CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/>
                                    </div></td><td>TRAINING</td>
                            </tr>
                        </table>
                    </div>
                    <div class="tab-pane" id="tab4">
                        <table width="100%" bordercolor="gray" border="1" cellpadding="10">
                            <thead>
                                <tr class="txtPq">
                                    <th width="200px" bgcolor="#e9e9e9" align="left" >Cod Acad:</th>
                                    <th width="200px" bgcolor="#e9e9e9" align="left" >Data:</th>
                                    <th bgcolor="#e9e9e9" align="left" >Plugue:</th>
                                </tr>
                            </thead>
                        </table>
                        <div style="overflow:scroll; height:205px;">
                            <table bordercolor="gray" border="1" cellpadding="10" width="100%">
                                <?php
                                if (is_numeric($id)) {
                                    $cur = odbc_exec($con, "select * from dbo.ca_cliplugue where COD_ACAD = " . $id);
                                    while ($RFP = odbc_fetch_array($cur)) {
                                        ?>
                                        <tr class="txtPq">
                                            <td width="200px"><?php echo str_pad($RFP['COD_ACAD'], 3, "0", STR_PAD_LEFT); ?></td>
                                            <td width="200px"><?php echo escreverDataHora($RFP['DT_CAD']); ?></td>
                                            <td><?php echo utf8_encode($RFP['COD_PLUGUE']); ?></td>
                                        </tr>
                                        <?php
                                    }
                                } ?>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab5">
                        <div style="float: left;width: 49%;">
                            <label>Operação:</label>
                            <select name="txtDccTpOperacao" id="txtDccTpOperacao" class="select" style="width:100%" <?php echo $disabled; ?>>
                                <option value="0" <?php
                                if ($dccTpOperacao == "0") {
                                    echo "SELECTED";
                                }
                                ?>>PRÉ PAGO</option>
                                <option value="2" <?php
                                if ($dccTpOperacao == "2") {
                                    echo "SELECTED";
                                }
                                ?>>PRÉ PAGO (SOMENTE ATIVOS - MUNDIPAGG)</option>
                                <option value="1" <?php
                                if ($dccTpOperacao == "1") {
                                    echo "SELECTED";
                                }
                                ?>>PÓS PAGO</option>
                            </select>
                        </div>
                        <div style="float: left;width: 49%;margin-left: 2%;">
                            <label>DCC Bloqueado:</label>
                            <select name="txtDccBloqueio" id="txtDccBloqueio" class="select" style="width:100%" <?php echo $disabled; ?>>
                                <option value="0" <?php
                                if ($dccBloqueio == "0") {
                                    echo "SELECTED";
                                }
                                ?>>Não</option>
                                <option value="1" <?php
                                if ($dccBloqueio == "1") {
                                    echo "SELECTED";
                                }
                                ?>>Sim</option>
                            </select>
                        </div>
                        <div style="float: left;width: 49%;margin-top: 10px;">
                            <div style="float: left;width: 30%">
                                <label>Loja:</label>
                                <input name="txtDccLoja" id="txtDccLoja" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $dccLoja; ?>"/>
                            </div>
                            <div style="float:left;margin-left: 1%;width: 68%">
                                <label>Chave de Acesso:
                                    <span style="float: right"><b>(Credencial 01)</b></span></label>
                                <input name="txtDccChave" id="txtDccChave" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $dccChave; ?>"/>
                            </div>
                            <div style="float: left;width: 30%">
                                <label>Tipo:</label>
                                <select name="txtDccTipo" id="txtDccTipo" class="select" style="width:100%" <?php echo $disabled; ?>>
                                    <option value="0" <?php
                                    if ($dccTipo == "0") {
                                        echo "SELECTED";
                                    }
                                    ?>>TEST</option>
                                    <option value="1" <?php
                                    if ($dccTipo == "1") {
                                        echo "SELECTED";
                                    }
                                    ?>>LIVE</option>
                                </select>
                            </div>
                            <div style="float:left;margin-left: 1%;width: 68%">
                                <label>Operadora:</label>
                                <select name="txtDccOperadora" id="txtDccOperadora" class="select" style="width:100%" <?php echo $disabled; ?>>
                                    <option value="1" <?php
                                    if ($dccOperadora == "1") {
                                        echo "SELECTED";
                                    }
                                    ?>>1 - SIMULADOR DE TESTES</option>
                                    <option value="2" <?php
                                    if ($dccOperadora == "2") {
                                        echo "SELECTED";
                                    }
                                    ?>>2 - Redecard (Komerci)</option>
                                    <option value="3" <?php
                                    if ($dccOperadora == "3") {
                                        echo "SELECTED";
                                    }
                                    ?>>3 - GetNet</option>
                                    <option value="4" <?php
                                    if ($dccOperadora == "4") {
                                        echo "SELECTED";
                                    }
                                    ?>>4 - Cielo</option>
                                    <option value="5" <?php
                                    if ($dccOperadora == "5") {
                                        echo "SELECTED";
                                    }
                                    ?>>5 - Redecard (E-Rede)</option>
                                    <option value="9" <?php
                                    if ($dccOperadora == "9") {
                                        echo "SELECTED";
                                    }
                                    ?>>9 - Stone</option>
                                </select>
                            </div>
                            <div style="float: left;width: 30%">
                                <label>Usuário Portal:</label>
                                <input name="txtDccUsuario" id="txtDccUsuario" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $dccUsuario; ?>"/>
                            </div>
                            <div style="float:left;margin-left: 1%;width: 68%">
                                <label>Senha Portal:</label>
                                <input name="txtDccSenha" id="txtDccSenha" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $dccSenha; ?>"/>
                            </div>
                            <div style="float: left;width: 99%;text-align: right;margin-top: 10px;">
                                <?php if ($id == "288") { ?>
                                    <button name="btnTeste" class="btn btn-primary" type="button" title="Abrir portal Test" onClick="window.open('https://testportal.maxipago.net/vpos/docs/maxipagologin.html', '_blank')"><span class="ico-file-4 icon-white"></span> Portal Test</button>
                                <?php } else { ?>
                                    <button name="btnLive" class="btn btn-success" type="button" title="Abrir portal Live" onClick="window.open('https://portal.maxipago.net/vpos/docs/maxipagologin.html', '_blank')"><span class="ico-file-4 icon-white"></span> Portal Live</button>
                                <?php } ?>
                            </div>
                        </div>
                        <div style="float: left;width: 49%;margin-left: 2%;margin-top: 10px;">
                            <div style="float: left;width: 30%">
                                <label>Loja:</label>
                                <input name="txtDccLoja2" id="txtDccLoja2" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $dccLoja2; ?>"/>
                            </div>
                            <div style="float:left;margin-left: 1%;width: 68%">
                                <label>Chave de Acesso:
                                    <span style="float: right"><b>(Credencial 02)</b></span></label>
                                <input name="txtDccChave2" id="txtDccChave2" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $dccChave2; ?>"/>
                            </div>
                            <div style="float: left;width: 30%">
                                <label>Tipo:</label>
                                <select name="txtDccTipo2" id="txtDccTipo2" class="select" style="width:100%" <?php echo $disabled; ?>>
                                    <option value="0" <?php
                                    if ($dccTipo2 == "0") {
                                        echo "SELECTED";
                                    }
                                    ?>>TEST</option>
                                    <option value="1" <?php
                                    if ($dccTipo2 == "1") {
                                        echo "SELECTED";
                                    }
                                    ?>>LIVE</option>
                                </select>
                            </div>
                            <div style="float:left;margin-left: 1%;width: 68%">
                                <label>Operadora:</label>
                                <select name="txtDccOperadora2" id="txtDccOperadora2" class="select" style="width:100%" <?php echo $disabled; ?>>
                                    <option value="1" <?php
                                    if ($dccOperadora2 == "1") {
                                        echo "SELECTED";
                                    }
                                    ?>>1 - SIMULADOR DE TESTES</option>
                                    <option value="2" <?php
                                    if ($dccOperadora2 == "2") {
                                        echo "SELECTED";
                                    }
                                    ?>>2 - Redecard (Komerci)</option>
                                    <option value="3" <?php
                                    if ($dccOperadora2 == "3") {
                                        echo "SELECTED";
                                    }
                                    ?>>3 - GetNet</option>
                                    <option value="4" <?php
                                    if ($dccOperadora2 == "4") {
                                        echo "SELECTED";
                                    }
                                    ?>>4 - Cielo</option>
                                    <option value="5" <?php
                                    if ($dccOperadora2 == "5") {
                                        echo "SELECTED";
                                    }
                                    ?>>5 - Redecard (E-Rede)</option>
                                    <option value="9" <?php
                                    if ($dccOperadora2 == "9") {
                                        echo "SELECTED";
                                    }
                                    ?>>9 - Stone</option>
                                </select>
                            </div>
                            <div style="float: left;width: 30%">
                                <label>Usuário Portal:</label>
                                <input name="txtDccUsuario2" id="txtDccUsuario2" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $dccUsuario2; ?>"/>
                            </div>
                            <div style="float:left;margin-left: 1%;width: 68%;">
                                <label>Senha Portal:</label>
                                <input name="txtDccSenha2" id="txtDccSenha2" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $dccSenha2; ?>"/>
                            </div>
                            <div style="float: left;width: 99%;text-align: right;margin-top: 10px;">
                                <?php if ($id == "288") { ?>
                                    <button name="btnTeste2" class="btn btn-primary" type="button" title="Abrir portal Test" onClick="window.open('https://testportal.maxipago.net/vpos/docs/maxipagologin.html', '_blank')"><span class="ico-file-4 icon-white"></span> Portal Test</button>
                                <?php } else { ?>
                                    <button name="btnLive2" class="btn btn-success" type="button" title="Abrir portal Live" onClick="window.open('https://portal.maxipago.net/vpos/docs/maxipagologin.html', '_blank')"><span class="ico-file-4 icon-white"></span> Portal Live</button>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab6">
                        <table width="680" border="0" cellpadding="3" cellspacing="0" class="textosComuns">
                            <tr>
                                <td width="100" align="right">Usuário Portal:</td>
                                <td width="234"><input name="txtSmsUsuario" id="txtSmsUsuario" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $smsUsuario; ?>"/></td>
                                <td width="100" align="right">Senha Portal:</td>
                                <td><input name="txtSmsSenha" id="txtSmsSenha" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $smsSenha; ?>"/></td>
                            </tr>
                            <tr>
                                <td width="100" align="right">Custo:</td>
                                <td width="234"><input name="txtSmsCusto" id="txtSmsCusto" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $smsCusto; ?>"/></td>
                                <td width="100" align="right"></td>
                                <td><button name="btnLive" class="btn btn-success" type="button" title="Abrir portal Live" onClick="window.open('https://cloud.infobip.com', '_blank')"><span class="ico-file-4 icon-white"></span> Portal Live</button></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" name="bntSave" id="bntOK"><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else { ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?');"><span class=" ico-remove"></span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
    <script type="text/javascript" src="../../js/util.js"></script>
    
    <script type="text/javascript">

        $(document).ready(function () {
            $("#txtSmsCusto").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"], centsLimit: 4});
        });

        $("#btnNewPlug").click(function () {
            var idFind = '';
            if ($("#txtCodigo").val() === "") {
                idFind = $("#txtId").val();
            } else {
                idFind = $("#txtCodigo").val();
            }
            $.getJSON("plug.ajax.php", {txtId: idFind, ajax: "true"}, function (j) {
                $('#txtPlugue').val(j[0].plugue).show();
            });
        });
        $(function () {
            $("#txtDesc").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "./../CRM/ajax.php",
                        dataType: "json",
                        data: {
                            q: request.term,
                            t: "C"
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    $('#txtFonecedor').val(ui.item.id).show();
                    $('#txtDesc').val(ui.item.label).show();
                    $('#carregando4').show();
                    $('#carregando5').show();
                    $.getJSON('./../Estoque/endereco.ajax.php?search=', {txtFonecedor: ui.item.id, ajax: 'true'}, function (j) {
                        var optionsDados = '';
                        var options = '';
                        var contato = '';
                        for (var i = 0; i < j.length; i++) {
                            if (j[i].endereco !== '') {
                                options += j[i].endereco;
                            }
                            if (j[i].numero !== '') {
                                options += ' N° ' + j[i].numero;
                            }
                            if (j[i].complemento !== '') {
                                options += ' ' + j[i].complemento;
                            }
                            if (j[i].bairro !== '') {
                                options += ',' + j[i].bairro;
                            }
                            if (j[i].cidade_nome !== '') {
                                options += '\n' + j[i].cidade_nome;
                            }
                            if (j[i].estado_sigla !== '') {
                                options += '-' + j[i].estado_sigla;
                            }
                            if (j[i].cep !== '') {
                                options += ', Cep: ' + j[i].cep;
                            }
                            if (j[i].cnpj !== '') {
                                optionsDados += 'CNPJ/CPF:' + j[i].cnpj;
                            }
                            if (j[i].telefone !== '') {
                                optionsDados += '\nTel:' + j[i].telefone;
                            }
                            if (j[i].email !== '') {
                                optionsDados += ' E-mail:' + j[i].email;
                            }
                            if (j[i].contato !== '') {
                                contato = j[i].contato;
                            }
                        }
                        $('#txtContato').val(contato).show();
                        $('#txtDados').html(optionsDados).show();
                        $('#txtEntrega').html(options).show();
                        $('#carregando4').hide();
                        $('#carregando5').hide();
                    });
                }
            });
        });
    </script>
<?php odbc_close($con); ?>
</body>
