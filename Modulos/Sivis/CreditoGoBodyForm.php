<?php include './../../Connections/configini.php'; ?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
<link href="../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<link href="../../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <div class="row-fluid">
        <form name="frmCreditoGoBodyForm" id="frmCreditoGoBodyForm">
            <div class="block title">
                <div class="head">
                    <div id="topBG">
                        <h2 class="head"><font color="#FFFFFF"> Cad Crédito GoBody </font>
                            <button style="border:none; float:right; width:25px; margin-right:25px; color:#FFFFFF; background-color: #333333" onClick="parent.FecharBox()" id="bntOK"><b>x</b></button>
                        </h2>
                    </div>
                </div>
            </div>
            <div style="clear:both"></div>
            <table width="680" style="margin-bottom:15px;" border="0" cellpadding="3" class="textosComuns">
                <tr>
                    <td width="122" align="right">Cliente:</td>
                    <td>
                        <input type="text" name="txtCliente" id="txtCliente" class="inputbox" value="" size="10" style="width:450px; color:#000 !important" placeholder="Selecione o Cliente"/>  </td>
                </tr>
                <tr>
                    <td width="122" align="right">Descrição:</td>
                    <td><input style="width:450px" name="txtDescr" id="txtDescr" maxlength="200" type="text" class="input-medium"/></td>
                </tr>
                <tr>
                    <td width="122" align="right">Quantidade:</td>
                    <td><input style="width:50px" name="txtQtd" id="txtQtd" maxlength="4" type="text" class="input-medium"/></td>
                </tr>
            </table>
            <div class="spanBNT" style="margin-top:0px; margin-bottom:0px; width:690px;">
                <div class="toolbar bottom tar">
                    <div class="data-fluid" style="overflow:hidden;">
                        <div class="btn-group">
                            <button class="btn btn-success" type="button" name="btnSave" title="Gravar" id="btnSave" style="float:left;"><span class="ico-checkmark"></span> Gravar</button>
                            <button class="btn btn-success" type="button" title="Cancelar" onClick="parent.FecharBox()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                        </div>
                    </div>
                </div>
            </div> 
        </form>
    </div>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script src="../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
    <script src="../../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtQtd").mask("999", {placeholder: ""});
            var idCliCredito = "";
            $(function () {
                $("#txtCliente").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "https://gobody.com.br/Api/Credito/CreditoErpAjax.php?BuscaCliente=S",
                            dataType: "jsonp",
                            data: {
                                q: request.term
                            },
                            success: function (data) {
                                response(data);
                            }
                        });
                    },
                    minLength: 3,
                    select: function (event, ui) {
                        idCliCredito = ui.item.id;
                    }
                });
            });

            $("#txtCliente").change(function () {
                if ($("#txtCliente").val() === '') {
                    idCliCredito = "";
                }
            });

            $("#txtCliente").focus(function () {
                $("#txtCliente").val('');
                idCliCredito = "";
            });

            $("#btnSave").click(function () {
                if (idCliCredito === "") {
                    alert('Cliente Inválido ou não encontrado!')
                } else {
                    $.ajax({
                        url: 'https://gobody.com.br/Api/Credito/CreditoErpAjax.php?IncluiCredito=' + idCliCredito + '&txtDescr=' + $("#txtDescr").val() + '&txtQtd=' + $("#txtQtd").val() + '&userErp=<?php echo $_SESSION["login_usuario"]; ?>',
                        dataType: 'jsonp',
                        success: function (result) {
                            if (result === 'YES') {
                                alert("Inclusao efetuada com sucesso!")
                                parent.$("#txtDesc").val($("#txtCliente").val());
                                parent.idCliente = idCliCredito;
                                parent.refreshTable();
                                parent.FecharBox();
                            }
                        }
                    });
                }
            });
        });

        $("#frmCreditoGoBodyForm").validate({
            errorPlacement: function (error, element) {
                return true;
            },
            rules: {
                txtDescr: {required: true},
                txtCliente: {required: true},
                txtQtd: {required: true}
            }
        });
    </script>
    <?php odbc_close($con); ?>
</body>