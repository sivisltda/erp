<?php include '../../Connections/configini.php'; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Sivis<small>Contas Pendentes - GoBody</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="float:left;width: 10%;margin-top: 15px;">
                                    <button id="btnEnviarEmail" class="button button-green btn-primary" type="button" title="Reenviar Email"><span class="ico-envelope-3 icon-white"></span></button>
                                </div>
                                <div style="float:right;width: 25%;">                                                                                                             
                                    <div style="float:left;margin-left: 1%;width: 85%;">
                                        <div width="100%">Busca:</div>
                                        <input id="txtDesc" maxlength="100" type="text" value="" title="" placeholder="Selecione o Cliente"/>
                                    </div>
                                    <div style="float:left;margin-left: 1%;width: 7%;margin-top: 15px;">
                                        <button class="button button-turquoise btn-primary" type="button" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Contas Pendentes - GoBody</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblLista">
                            <thead>
                                <tr id="formPQ">
                                    <th id="check_enviar" width="2%"><input id="checkAll" type="checkbox" class="checkall"></th>
                                    <th width="10%">Dt Cadastro</th>
                                    <th width="40%">Nome</th>
                                    <th width="15%">Email</th>
                                    <th width="8%">Telefone</th>
                                    <th width="5%">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div> 
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>         
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>          
        <script type="text/javascript">
            var idCliente = "";
            $(document).ready(function () {
                $(function () {
                    $("#txtDesc").autocomplete({
                        source: function (request, response) {
                            $.ajax({
                                url: "https://gobody.com.br/Api/Perfil/PerfilErpAjax.php?BuscaConta=S",
                                dataType: "jsonp",
                                data: {
                                    q: request.term
                                },
                                success: function (data) {
                                    response(data);
                                }
                            });
                        },
                        minLength: 3,
                        select: function (event, ui) {
                            idCliente = ui.item.id;
                        }
                    });
                });

                $("#txtDesc").change(function () {
                    if ($("#txtDesc").val() === '') {
                        idCliente = "";
                    }
                });

                var valSelect = new Array();
                var myTable = $('#tblLista').dataTable({
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "sPaginationType": "simple",
                    "order": [[1, "desc"]],
                    'aoColumns': [
                        {'bSortable': false},
                        {'bSortable': true},
                        {'bSortable': true},
                        {'bSortable': true},
                        {'bSortable': true},
                        {'bSortable': false}],
                    "sAjaxSource": "https://gobody.com.br/Api/Perfil/PerfilErpAjax.php?ListaContasPendentes=T",
                    "fnServerData": function (sUrl, aoData, fnCallback) {
                        aoData.push({"name": "params", "value": valSelect});
                        $.ajax({
                            "url": sUrl,
                            "data": aoData,
                            "success": fnCallback,
                            "dataType": "jsonp",
                            "cache": false
                        });
                    },
                    'oLanguage': {
                        'oPaginate': {
                            'sFirst': "Primeiro",
                            'sLast': "Último",
                            'sNext': "Próximo",
                            'sPrevious': "Anterior"},
                        'sEmptyTable': "Não foi encontrado nenhum resultado",
                        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                        'sLengthMenu': "Visualização de _MENU_ registros",
                        'sLoadingRecords': "Carregando...",
                        'sProcessing': "Processando...",
                        'sSearch': "Pesquisar:",
                        'sZeroRecords': "Não foi encontrado nenhum resultado"},
                    "sPaginationType": "full_numbers"
                });

                $("#btnfind").click(function () {
                    if ($("#txtDesc").val() === '') {
                        idCliente = "";
                    }
                    valSelect = [[idCliente]];
                    myTable.fnDraw(false);
                });

                $("#btnEnviarEmail").click(function () {
                    var total = 0;
                    var EmailSucess = 0;
                    var EmailFail = 0;
                    var EmailFailDesc = [];

                    var totalindice = $("#tblLista tbody input[type=checkbox]:checked").length;
                    $("#tblLista tbody input[type=checkbox]").each(function (index) {
                        if (this.checked) {
                            var id = $(this).val();
                            if ($("#txtEmail_" + id).text() !== "") {
                                $("#img_" + id).show();
                                $("#chk_" + id).hide();
                                total++;
                                $.ajax({
                                    url: 'https://gobody.com.br/Api/Perfil/PerfilErpAjax.php?ReenvioEmail=' + id + '&txtNome=' + $("#txtNome_" + id).text() + '&txtEmail=' + $("#txtEmail_" + id).text(),
                                    dataType: 'jsonp',
                                    success: function () {
                                        $("#img_" + id).hide();
                                        $("#chk_" + id).show();
                                        EmailSucess++;
                                        if (total === (EmailFail + EmailSucess)) {
                                            alertResult();
                                        }
                                    },
                                    error: function (jqXHR, exception) {
                                        EmailFail++;
                                        if (total === totalindice - 1) {
                                            alertResult();
                                        }
                                    }
                                });
                            } else {
                                EmailFailDesc.push($("#txtNome_" + id).text());
                                EmailFail++;
                                total++;
                                if (total === totalindice - 1) {
                                    alertResult();
                                }
                            }
                        }
                    });

                    function alertResult() {
                        var emails = "<table border='1' style='width: 100%;text-align: center;margin-bottom: 20px;'>";
                        for (var x = 0; x < EmailFailDesc.length; x++) {
                            if (x === EmailFailDesc.length) {
                                emails = emails + "<tr><td>" + EmailFailDesc[x] + "</td></tr></table>";
                            } else {
                                emails = emails + "<tr><td>" + EmailFailDesc[x] + "</td></tr>";
                            }

                        }
                        bootbox.alert(emails + "<p style='margin-bottom: 5px;'>E-mails Enviado com sucesso: " + EmailSucess + "</p><p>Sem e-mails cadastrados: " +
                                EmailFail + "</p><p style='    position: absolute;bottom:56px;'>Total de Email Enviados: " + total + "</p>");
                    }

                });

                var nEditing = null;
                var idEditing = "";
                var regEdit = "";
                $("body").on("click", "#tblLista #btnAlt", function () {
                    var nRow = $(this).parents('tr')[0];
                    if (nEditing !== null) {
                        restoreRow(nEditing, idEditing);
                    } else {
                        var id = $(this).attr("data-id");
                        if ($("#txtEmail_" + id).text() !== "") {
                            nEditing = nRow;
                            idEditing = id;
                            regEdit = $("#txtEmail_" + id).text();
                            var jqTds = $('>td', nRow);
                            jqTds[3].innerHTML = '<input id=\"txtEditEmail_' + id + '\" type="text" class="form-control input" value="' + $("#txtEmail_" + id).text() + '">';
                            jqTds[5].innerHTML = '<center>\n\
                            <a href="javascript:;" data-id="' + id + '" class="btn green-haze save" title="Salvar" style=\"padding: 0px 4px;background:#5bb75b;\"><span class="ico-checkmark"></span></a> \n\
                            <a href="javascript:;" data-id="' + id + '" class="btn red-haze cancel" title="Cancelar" style=\"padding: 0px 4px;background:red;\"><span class="ico-reply"></span></a> <center>';
                        } else {
                            nEditing = null;
                            idEditing = "";
                            regEdit = "";
                            alert('Usuario sem email cadastrado, o mesmo deverá ser alterado pelo proprio usuário no site');
                        }
                    }
                });

                myTable.on('click', '.cancel', function (e) {
                    var id = $(this).attr("data-id");
                    var nRow = $(this).parents('tr')[0];
                    restoreRow(nRow, id);
                });

                function restoreRow(nRow, id) {
                    var jqTds = $('>td', nRow);
                    jqTds[3].innerHTML = '<center id="txtEmail_' + id + '">' + regEdit + '</center>';
                    jqTds[5].innerHTML = '<center>\n\
                    <a href="javascript:;" data-id="' + id + '" id="btnAlt"><span class="ico-edit" title="Alterar" style="font-size:18px;"> </span></a> <center>';
                    nEditing = null;
                    regEdit = "";
                }

                myTable.on('click', '.save', function (e) {
                    var id = $(this).attr("data-id");
                    var r = confirm("Confirma o alteração do registro?");
                    if (r === true) {
                        $.ajax({
                            url: 'https://gobody.com.br/Api/Perfil/PerfilErpAjax.php?AlterarEmail=' + id + '&txtEmail=' + $("#txtEditEmail_" + id).val() + '&userErp=<?php echo $_SESSION["login_usuario"]; ?>',
                            dataType: 'jsonp',
                            success: function (result) {
                                if (result === 'YES') {
                                    var oTable = $('#tblLista').dataTable();
                                    oTable.fnDraw(false);
                                }
                            }
                        });
                    }
                });
            });
        </script>  
        <?php odbc_close($con); ?>
    </body>
</html>