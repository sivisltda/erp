<?php

if (!isset($_GET['pdf'])) {
    include ("./../../Connections/configini.php");
    include ("./../../util/util.php");
}

$aColumns = array('numero_contrato', 'razao_social', 'endereco', 'bairro', 'cidade', 'estado', 'telefone_contato', 
'numcref', 'nome_fantasia', 'cnpj', 'inscricao_estadual', 'id', 'servidor', 'inativa', 'id_fornecedores_despesas');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY numero_contrato asc ";
$sLimit = 20;
$_GET['sSearch'] = $_GET['Search'];
$imprimir = 0;

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (is_numeric($_GET['imp']) && $_GET['imp'] == "1") {
    $imprimir = 1;
}

if ($_GET['st'] != "") {
    $sWhereX = $sWhereX . " and inativa = " . $_GET['st'] . " ";
}

if ($_GET['fq'] != "") {
    $sWhereX = $sWhereX . " and mdl_empresa = " . $_GET['fq'] . " ";
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0 && intval($_GET['iSortCol_' . $i]) < 9) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY numero_contrato asc";
    }
}

if ($_GET['Search'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        if ($i !== 6) {
            $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['Search']) . "%' OR ";
        }
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,id,numero_contrato,servidor,inativa,razao_social, endereco, bairro, cidade_nome cidade, estado_sigla estado,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = id_fornecedores_despesas) email_contato,
            nome_fantasia, cnpj, inscricao_estadual,id_fornecedores_despesas
            FROM sf_fornecedores_despesas inner join ca_contratos on id_fornecedores_despesas = id_cliente left join tb_estados on estado = estado_codigo left join tb_cidades on cidade = cidade_codigo
            WHERE tipo in ('C','M') " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1=" . $imprimir . "" . $sOrder;
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);

$sQuery = "SELECT COUNT(*) total FROM sf_fornecedores_despesas inner join ca_contratos on id_fornecedores_despesas = id_cliente WHERE tipo in ('C','M') $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total FROM sf_fornecedores_despesas inner join ca_contratos on id_fornecedores_despesas = id_cliente WHERE tipo in ('C','M') $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $identificacao = utf8_encode($aRow[$aColumns[1]]);
    if (utf8_encode($aRow[$aColumns[8]]) != '') {
        $identificacao = utf8_encode($aRow[$aColumns[1]]) . ' (' . utf8_encode($aRow[$aColumns[8]]) . ')';
    }
    $row[] = "<center><a title=\"Atendimento\" href='./../Servicos/Servicos_Atendimentos.php?Cli=" . $aRow[$aColumns[14]] . "' ><center><img src=\"../../img/1367947965_schedule.png\" width=\"16\" height=\"16\"></center></a></center>";
    if (($aRow[$aColumns[13]] == 0 || $aRow[$aColumns[13]] == 2) && !isset($_GET['pdf'])) {
        $row[] = "<center><a href=\"https://teste.sivisweb.com.br/login_vai.php?ar=" . encrypt($aRow[$aColumns[0]] . "|" . $_SESSION["login_usuario"], "VipService123", true) . "\" target=\"_blank\"><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[0]]) . "'>" . utf8_encode($aRow[$aColumns[0]]) . "</div></a></center>";
    } else {
        $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[0]]) . "'>" . utf8_encode($aRow[$aColumns[0]]) . "</div></center>";
    }
    $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[$aColumns[11]]) . ")'><div id='formPQ' title='" . $identificacao . "'>" . $identificacao . "</div></a>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[4]]) . " / " . utf8_encode($aRow[$aColumns[5]]) . "'>" . utf8_encode($aRow[$aColumns[4]]) . " / " . utf8_encode($aRow[$aColumns[5]]) . "</div></center>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[6]]) . "'>" . utf8_encode($aRow[$aColumns[6]]) . "</div></center>";
    if ($aRow[$aColumns[12]] == 0) {
        $row[] = "<center><div id='formPQ' style='color:#005683' title=''>Locaweb (BR)</div></center>";
    } else {
        $row[] = "<center><div id='formPQ' style='color:#08c' title=''>Dragon (EUA)</div></center>";
    }
    if ($aRow[$aColumns[13]] == 0) {
        $row[] = "<center><div id='formPQ' title=''>Ativo</div></center>";
    } elseif ($aRow[$aColumns[13]] == 2) {
        $row[] = "<center><div id='formPQ' style='color:red;' title=''>Mensagem</div></center>";
    } else {
        $row[] = "<center><div id='formPQ' style='color:red;' title=''>Inativo</div></center>";
    }
    $row[] = "<center><a title='Excluir' href='CadERP.php?Del=" . $aRow[$aColumns[11]] . "'><input name='' type='image' onClick='return confirm('Deseja deletar esse registro?')' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
