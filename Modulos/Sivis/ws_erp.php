<?php

if (isset($_GET['Contrato']) && strlen($_GET['Contrato']) < 3) {
    $_GET['Contrato'] = str_pad($_GET['Contrato'], 3, "0", STR_PAD_LEFT);
}

if (isset($_GET['ValidaBanco'])) {
    require "../../configini.php";
    $cur = odbc_exec($con, "select * from ca_contratos
    left join sf_fornecedores_despesas on id_fornecedores_despesas = id_cliente 
    left join tb_estados on estado_codigo = estado
    left join tb_cidades on cidade_codigo = cidade 
    where numero_contrato = '" . $_GET['Contrato'] . "'");
    while ($RFP = odbc_fetch_array($cur)) {
        $rows['servidor'] = utf8_encode($RFP['servidor']);
        $rows['numero_contrato'] = utf8_encode($RFP['numero_contrato']);
        $rows['razao_social'] = utf8_encode($RFP['razao_social']);
        $rows['inativa'] = utf8_encode($RFP['inativa']);
    }
    print(json_encode($rows));
    odbc_close($con);
    exit();
} elseif (!isset($_GET['WinWhatsapp']) && !isset($_GET['GetGrupoEmpresas'])) {
    require "../../Connections/funcoesAux.php";
    $hostname = getDataBaseServer($_GET['Servidor']);
    $database = "ERP" . $_GET['Contrato'];
    $username = "erp" . $_GET['Contrato'];
    $password = "!Password123";
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());   
}

if (isset($_GET['ValidaUser'])) {
    include "../../util/util.php";    
    $cur = odbc_exec($con, "select id_usuario,nome,login_user from sf_usuarios A where inativo = 0 and login_user = " . 
    valoresTexto2($_GET['Usuario']) . " and senha = " . valoresTexto2(encrypt($_GET['Senha'], "VipService123", true)));
    while ($RFP = odbc_fetch_array($cur)) {
        $rows['id_usuario'] = utf8_encode($RFP['id_usuario']);
        $rows['nome'] = utf8_encode($RFP['nome']);
        $rows['login_user'] = utf8_encode($RFP['login_user']);
    }
    print(json_encode($rows));
    odbc_close($con);
    exit();
}

if (isset($_GET['GetSincBancoTotal'])) {
    $cur = odbc_exec($con, "select count(*) total from sf_fornecedores_despesas_acesso a
    inner join sf_fornecedores_despesas b on a.id_acesso = b.id_fornecedores_despesas and b.inativo = 0");
    while ($RFP = odbc_fetch_array($cur)) {
        print($RFP['total']);
    }    
    odbc_close($con);
    exit();    
}

if (isset($_GET['BuscaTbAcesso'])) {
    $sLimit = "";
    $mensagens_acesso = array();
    $cur = odbc_exec($con, "select id, conteudo from sf_mensagens_acesso");
    while ($RFP = odbc_fetch_array($cur)) {
        $mensagens_acesso[] = array($RFP['id'], utf8_encode($RFP['conteudo']));
    }
    if (isset($_GET['Page']) && isset($_GET['Page_total'])) {
        $sLimit = " order by id_acesso OFFSET " . ($_GET['Page_total'] * $_GET['Page']) . " ROWS FETCH NEXT " . $_GET['Page_total'] . " ROWS ONLY";
    }
    $cur = odbc_exec($con, "select id_acesso, convert(varbinary(max), digital) digital, qualidade, '' status_acesso,dt_acesso,
    isnull(data_limite_acesso,GETDATE()) data_limite_acesso, b.razao_social, b.tipo tipo_acesso, acesso_numero_provisorio, data_nascimento,
    (select max(dt_fim) from sf_vendas_planos where favorecido = b.id_fornecedores_despesas) limite, isnull(excecoes_digitais,0) excecoes_digitais, isnull(prof_resp,0) prof_resp,
    dbo.FU_ACESSO_CLI(id_fornecedores_despesas,1,0,1) limite_acesso
    from sf_fornecedores_despesas_acesso a inner join sf_fornecedores_despesas b on a.id_acesso = b.id_fornecedores_despesas and b.inativo = 0 " . $sLimit);
    while ($RFP = odbc_fetch_array($cur)) {
        $rows = array();        
        $limite_acesso = explode("|", $RFP['limite_acesso']);
        $rows['id_acesso'] = utf8_encode($RFP['id_acesso']);
        $rows['empresa'] = $_GET['Contrato'];
        $rows['digital'] = utf8_encode($RFP['digital']);
        $rows['qualidade'] = utf8_encode($RFP['qualidade']);        
        $rows['motivo'] = (count($limite_acesso) > 1 ? tratastatus($limite_acesso[0], $mensagens_acesso) : utf8_encode($RFP['status_acesso']));
        $rows['dt_acesso'] = utf8_encode($RFP['dt_acesso']);
        $rows['razao_social'] = utf8_encode($RFP['razao_social']);
        $rows['tipo_acesso'] = utf8_encode($RFP['tipo_acesso']);
        $rows['data_limite_acesso'] = (count($limite_acesso) > 1 ? str_replace("/", "-", $limite_acesso[1]) : utf8_encode($RFP['data_limite_acesso']));
        $rows['acesso_numero_provisorio'] = utf8_encode($RFP['acesso_numero_provisorio']);
        $rows['dt_nasc'] = escreverData($RFP['data_nascimento'], "dm");
        $rows['data_limite_real'] = (count($limite_acesso) > 2 ? escreverData(str_replace("/", "-", $limite_acesso[2])) : escreverData($RFP['limite']));
        $rows['excecoes_digitais'] = utf8_encode($RFP['excecoes_digitais']);
        $rows['prof_resp'] = utf8_encode($RFP['prof_resp']);
        $rows['modalidades'] = "";
        $records[] = $rows;
    }
    print(json_encode($records));
    odbc_close($con);
    exit();
}

if (isset($_GET['BuscaTbCatraca'])) {
    if (isset($_GET['IdAmbiente'])) {
        $cur = odbc_exec($con, "select * from sf_catracas A inner join sf_ambientes_itens B on A.id_catraca = B.catraca and ambiente = " . $_GET['IdAmbiente']);
    } else {
        $cur = odbc_exec($con, "select * from sf_catracas A inner join sf_ambientes_itens B on A.id_catraca = B.catraca");
    }
    while ($RFP = odbc_fetch_array($cur)) {
        $rows = array();
        $rows['id_catraca'] = utf8_encode($RFP['id_catraca']);
        $rows['nome_catraca'] = utf8_encode($RFP['nome_catraca']);
        $rows['tipo_catraca'] = utf8_encode($RFP['tipo_catraca']);
        $rows['num_inner_catraca'] = utf8_encode($RFP['num_inner_catraca']);
        $rows['porta_catraca'] = utf8_encode($RFP['porta_catraca']);
        $rows['qtd_digitos_catraca'] = utf8_encode($RFP['qtd_digitos_catraca']);
        $rows['tp_conexao_catraca'] = utf8_encode($RFP['tp_conexao_catraca']);
        $rows['tp_equipamento_catraca'] = utf8_encode($RFP['tp_equipamento_catraca']);
        $rows['sentido_catraca'] = utf8_encode($RFP['sentido_catraca']);
        $rows['intervalo_catraca'] = utf8_encode($RFP['intervalo_catraca']);
        $rows['variante_catraca'] = utf8_encode($RFP['variante_catraca']);
        $rows['ip_catraca'] = utf8_encode($RFP['ip_catraca']);
        $records[] = $rows;
    }
    print(json_encode($records));
    odbc_close($con);
    exit();
}

if (isset($_GET['EstacionamentoAcesso'])) {
    $cur = odbc_exec($con, "set dateformat dmy;
    select sum(case when motivo_liberacao = 'Entrada de Visitante' then 1 else 0 end) entradas,
    sum(case when motivo_liberacao = 'Outros' then 1 else 0 end) saidas
    from sf_acessos where data_acesso between '" . getData("T") . " 00:00:00' and '" . getData("T") . " 23:59:59'
    and id_fonecedor = 0 and ambiente = " . $_GET['IdAmbiente']);
    while ($RFP = odbc_fetch_array($cur)) {
        print(($RFP['entradas'] - $RFP['saidas']));
    }    
    odbc_close($con);
    exit();    
}

if (isset($_GET['BuscaTbAmbiente'])) {
    $rows = array();
    $rows['id_ambiente'] = "0";
    $rows['nome_ambiente'] = "AMBIENTE NÃO DEFINIDO";    
    $records[] = $rows;
    $cur = odbc_exec($con, "select * from sf_ambientes");
    while ($RFP = odbc_fetch_array($cur)) {
        $rows = array();
        $rows['id_ambiente'] = utf8_encode($RFP['id_ambiente']);
        $rows['nome_ambiente'] = utf8_encode($RFP['nome_ambiente']);
        $records[] = $rows;
    }
    print(json_encode($records));
    odbc_close($con);
    exit();
}

if (isset($_GET['BuscaLeitorFacial'])) {
    $records = [];
    $cur = odbc_exec($con, "select id_leitor, descricao, ip_leitor, mac_leitor, id_catraca from sf_biometria_facial");
    while ($RFP = odbc_fetch_array($cur)) {
        $rows = array();
        $rows['id_leitor'] = utf8_encode($RFP['id_leitor']);
        $rows['descricao'] = utf8_encode($RFP['descricao']);
        $rows['ip_leitor'] = utf8_encode($RFP['ip_leitor']);
        $rows['mac_leitor'] = utf8_encode($RFP['mac_leitor']);
        $rows['id_catraca'] = utf8_encode($RFP['id_catraca']);
        $records[] = $rows;
    }
    print(json_encode($records));
    odbc_close($con);
    exit();
}

if (isset($_GET['GetAlunos'])) {
    $records = array();
    $top = "";
    $order = "";
    $where = "";
    if (isset($_GET['FiltroAluno'])) {
        $where = "and (razao_social like '%" . $_GET['FiltroAluno'] . "%' or id_fornecedores_despesas like '%" . $_GET['FiltroAluno'] . "%')";
    } elseif (isset($_GET['Aluno']) && isset($_GET['Botao'])) {
        $top = "top 1";
        if ($_GET['Botao'] == 0) {
            $order = " asc";
        } elseif ($_GET['Botao'] == 1) {
            $where = " and id_fornecedores_despesas < " . $_GET['Aluno'];
            $order = " desc";
        } elseif ($_GET['Botao'] == 2) {
            $where = " and id_fornecedores_despesas > " . $_GET['Aluno'];
            $order = " asc";
        } elseif ($_GET['Botao'] == 3) {
            $order = " desc";
        }
    }
    $query = "select " . $top . " * from sf_fornecedores_despesas where id_fornecedores_despesas > 0 and 
    tipo in('C','E','P') and bloqueado = 0 and inativo = 0 " . $where . " order by id_fornecedores_despesas " . $order;
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $rows = array();
        $rows['id_fornecedores_despesas'] = utf8_encode($RFP['id_fornecedores_despesas']);
        $rows['empresa'] = $_GET['Contrato'];
        $rows['razao_social'] = utf8_encode($RFP['razao_social']);
        $rows['tipo'] = utf8_encode($RFP['tipo']);
        $rows['prof_resp'] = "";
        $rows['provisorio'] = utf8_encode($RFP['excecoes_digitais']);
        $records[] = $rows;
    }
    for ($i = 0; $i < count($records); $i++) {
        $itens = "";
        $query = "select id_fornecedores_despesas,razao_social from sf_fornecedores_despesas where prof_resp in (0," .
        $records[$i]["id_fornecedores_despesas"] . ") and tipo = 'R'";
        $cur = odbc_exec($con, $query);
        while ($RFP = odbc_fetch_array($cur)) {
            $itens .= utf8_encode($RFP['id_fornecedores_despesas']) . "," . utf8_encode($RFP['razao_social']) . ";";
        }
        $records[$i]["prof_resp"] = $itens;
    }
    print(json_encode($records));
    odbc_close($con);
    exit();
}

if (isset($_GET['GetImagem'])) {
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
    if ($_GET['IdImagem'] != "" and file_exists("./../../Pessoas/" . $_GET['Contrato'] . "/Clientes/" . $_GET['IdImagem'] . ".png")) {
        print(json_encode($url . "/Pessoas/" . $_GET['Contrato'] . "/Clientes/" . $_GET['IdImagem'] . ".png"));
    } else {
        print(json_encode(""));
    }
}

if (isset($_GET['SaveTbAcesso'])) {
    odbc_autocommit($con, false);
    $id_ambiente = null;
    $externo = '0';
    $catraca = '0';
    if (isset($_GET['idAmbiente'])) {
        $id_ambiente = $_GET['idAmbiente'];
    }
    if (isset($_GET['externo'])) {
        $externo = $_GET['externo'];
    }
    if (isset($_GET['idCatraca'])) {
        $catraca = $_GET['idCatraca'];
    }
    $cur = odbc_exec($con, "select id_fornecedores_despesas, dbo.FU_ACESSO_CLI(id_fornecedores_despesas," .
    $id_ambiente . "," . $externo . "," . $catraca . ") status_acesso from sf_fornecedores_despesas " . 
    "where id_fornecedores_despesas = " . $_GET['id_acesso']);
    while ($RFP = odbc_fetch_array($cur)) {
        $status = explode("|", $RFP['status_acesso']);
    }        
    $objExec = odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_acesso WHERE id_acesso = " . $_GET['id_acesso']) or die(odbc_errormsg());
    $query = "set dateformat dmy;
    INSERT INTO sf_fornecedores_despesas_acesso (id_acesso, digital, qualidade, dt_acesso,data_limite_acesso)
    VALUES(" . $_GET['id_acesso'] . ", " . $_GET['digital'] . ", " . $_GET['qualidade'] . "," . 
    valoresData2(str_replace("_", "/", $_GET['dt_acesso'])) . "," . valoresData2(escreverData($status[1])) . ")";    
    //echo $query; exit;
    $objExec = odbc_exec($con, $query) or die(odbc_errormsg());
    if (!$objExec) {
        odbc_rollback($con);
        print(json_encode("NO"));
        exit();
    }
    if ($objExec) {
        odbc_commit($con);
        print(json_encode(escreverData(str_replace("/", "-", $status[1]))));
        odbc_close($con);
        exit();
    }
}

function tratastatus($status, $mensagens_acesso) {
    foreach ($mensagens_acesso as $value) {
        if($value[0] == $status) {
            return $value[1];
        }
    }    
    return "";
}

if (isset($_GET['GetAcessoAluno'])) {
    odbc_autocommit($con, false);
    $idAcesso = "";
    $digital = "";
    $qualidade = "0";
    $externo = "0";
    $catraca = "0";    
    $usuario = "null";
    $modalidades = "";
    $id_reg = "";
    $mensagens_acesso = array();
    $cur = odbc_exec($con, "select id, conteudo from sf_mensagens_acesso");
    while ($RFP = odbc_fetch_array($cur)) {
        $mensagens_acesso[] = array($RFP['id'], utf8_encode($RFP['conteudo']));
    }
    $cur = odbc_exec($con, "select prof_resp from sf_fornecedores_despesas 
    where tipo = 'R' and id_fornecedores_despesas = " . $_GET['idAluno']);
    while ($RFP = odbc_fetch_array($cur)) {
        $_GET['idAluno'] = (is_numeric($RFP['prof_resp']) ? $RFP['prof_resp'] : $_GET['idAluno']);
    }
    $where = "where id_fornecedores_despesas = " . $_GET['idAluno'];
    if (isset($_GET['provisorio']) && $_GET['provisorio'] == "S" && is_numeric($_GET['idAluno'])) {
        $where = "where cast(acesso_numero_provisorio as bigint) = " . $_GET['idAluno'];
    }
    if (isset($_GET['externo']) && is_numeric($_GET['externo'])) {
        $externo = $_GET['externo'];
    }
    if (isset($_GET['idCatraca']) && is_numeric($_GET['idCatraca'])) {
        $cur = odbc_exec($con, "select id_catraca from sf_catracas where num_inner_catraca = '" . $_GET['idCatraca'] . "'");
        while ($RFP = odbc_fetch_array($cur)) {
            $catraca = $RFP['id_catraca'];
        }        
    }    
    if (isset($_GET['usuario']) && is_numeric($_GET['usuario'])) {
        $usuario = $_GET['usuario'];
    }        
    $cur = odbc_exec($con, "select top 1 id_fornecedores_despesas, razao_social,
    dbo.FU_ACESSO_CLI(id_fornecedores_despesas," . $_GET['idAmbiente'] . "," . $externo . "," . ($catraca == 0 || $externo == 1 ? "null" : $catraca) . ") status_acesso, 
    acesso_msg, acesso_numero_provisorio, data_nascimento, isnull(excecoes_digitais,0) excecoes_digitais, tipo, 
    isnull(prof_resp,0) prof_resp from sf_fornecedores_despesas " . $where);
    while ($RFP = odbc_fetch_array($cur)) {
        $status = explode("|", $RFP['status_acesso']);
        $idAcesso = utf8_encode($RFP['id_fornecedores_despesas']);
        $statusAcesso = $status[0];
        $motivoAcesso = tratastatus($status[0], $mensagens_acesso);
        $nmAcesso = utf8_encode($RFP['razao_social']);
        $dt_nasc = escreverData($RFP['data_nascimento'], "dm");
        $msg_acesso = utf8_encode($RFP['acesso_msg']);
        $numero_provisorio = utf8_encode($RFP['acesso_numero_provisorio']);
        $excecoes_digitais = utf8_encode($RFP['excecoes_digitais']);
        $tipo = utf8_encode($RFP['tipo']);
        $prof_resp = utf8_encode($RFP['prof_resp']);
    }
    if ($idAcesso == "") {
        echo "NO";
        exit;
    }
    $cur = odbc_exec($con, "SELECT descricao FROM sf_vendas_planos 
    inner join sf_produtos on id_prod_plano = conta_produto
    where dt_cancelamento is null and planos_status = 'Ativo' and favorecido = " . $idAcesso . "
    order by descricao");
    while ($RFP = odbc_fetch_array($cur)) {
        $modalidades .= utf8_encode(str_replace(",", "", $RFP['descricao'])) . ",";
    }    
    if (isset($_GET['motivo']) && $idAcesso == 0) {
        $statusAcesso = "LB";
        $motivoAcesso = $_GET['motivo'];
    }
    $cur = odbc_exec($con, "select id_acesso,digital,qualidade from sf_fornecedores_despesas_acesso where id_acesso = " . $idAcesso);
    while ($RFP = odbc_fetch_array($cur)) {
        $id_reg = $RFP['id_acesso'];
        $digital = utf8_encode($RFP['digital']);
        $qualidade = utf8_encode($RFP['qualidade']);
    }
    if ($id_reg !== "") {
        $query = "set dateformat dmy; update sf_fornecedores_despesas_acesso set dt_acesso = getdate(), motivo_acesso = '" . utf8_decode($motivoAcesso) . "'," . 
        " data_limite_acesso = " . valoresData2(escreverData($status[1])) . " where id_acesso = " . $idAcesso;
    } else {
        $query = "set dateformat dmy; INSERT INTO sf_fornecedores_despesas_acesso (id_acesso, dt_acesso,data_limite_acesso,motivo_acesso,qualidade)
        VALUES(" . $idAcesso . ",getdate()," . valoresData2(escreverData($status[1])) . ",'" . utf8_decode($motivoAcesso) . "',0)";
    }
    //echo $query; exit;
    $objExec = odbc_exec($con, $query);    
    $remover_provisorio = 0;
    $cur = odbc_exec($con, "select id, (select top 1 adm_catraca_urna from sf_configuracao) ativo,
    (select count(id_acesso) from sf_acessos where id_fonecedor = " . $idAcesso . 
    " and cast(data_acesso as date) = cast(getdate() as date) and status_acesso = 'AP') total from sf_cartao_urna 
    where cartao = " . valoresTexto2($numero_provisorio));
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['id'] > 0 && $RFP['ativo'] > 0 && $RFP['total'] % 2 == 0) {
            $remover_provisorio = 1;
        }
    }
    if ($remover_provisorio > 0) {    
        $numero_provisorio = "";        
        if (isset($_GET['origem']) && is_numeric($_GET['origem']) && $_GET['origem'] == "2") {
            odbc_exec($con, "update sf_fornecedores_despesas set acesso_tipo = 1, acesso_numero_provisorio = null where id_fornecedores_despesas = " . $idAcesso);            
        } else {
            $statusAcesso = "FA";
            $motivoAcesso = "Cartão fora da Urna";
            $status[1] = escreverDataSoma(getData("T"), " -1 days", "Y/m/d");
            $status[2] = "";
        }    
    }        
    $query = "set dateformat dmy; insert into sf_acessos (id_fonecedor, data_acesso, status_acesso, liberado_por, motivo_liberacao, ambiente, catraca) values 
    (" . $idAcesso . ",getdate(),'" . $statusAcesso . "', " . $usuario . ", '" . utf8_decode($motivoAcesso) . "', " . $_GET['idAmbiente'] . ", " . ($catraca == 0 || $externo == 1 ? "null" : $catraca) . ")";
    //echo $query; exit;
    $objExec = odbc_exec($con, $query);
    if ($idAcesso > 0 && $remover_provisorio == 0) {
        $motivoAcesso = tratastatus($status[0], $mensagens_acesso);
    }        
    $rows = array();
    $rows['id_acesso'] = $idAcesso;
    $rows['empresa'] = $_GET['Contrato'];
    $rows['digital'] = $digital;
    $rows['qualidade'] = $qualidade;
    $rows['motivo'] = utf8_encode($motivoAcesso);
    $rows['dt_acesso'] = date("Y-m-d H:i:s");
    $rows['razao_social'] = $nmAcesso;
    $rows['tipo_acesso'] = $tipo;
    $rows['data_limite_acesso'] = $status[1];
    $rows['acesso_msg'] = $msg_acesso;
    $rows['acesso_numero_provisorio'] = $numero_provisorio;
    $rows['dt_nasc'] = $dt_nasc;
    $rows['data_limite_real'] = escreverData($status[2]);
    $rows['excecoes_digitais'] = $excecoes_digitais;
    $rows['prof_resp'] = $prof_resp;
    $rows['modalidades'] = substr_replace($modalidades, "", -1);
    $records[] = $rows;
    if (!$objExec) {
        odbc_rollback($con);
        echo "ERROR";
        exit();
    }
    if ($objExec) {
        odbc_commit($con);
        print(json_encode($rows));
        odbc_close($con);
        exit();
    }
}

if (isset($_GET['ExcluirTbAcesso'])) {
    $query = "DELETE FROM sf_fornecedores_despesas_acesso WHERE id_acesso = " . $_GET['id_acesso'];
    odbc_exec($con, $query) or die(odbc_errormsg());
    $query = "YES";
    print(json_encode($query));
}

if (isset($_GET['ProvisorioTbAcesso'])) {
    $query = "UPDATE sf_fornecedores_despesas SET excecoes_digitais = " . valoresNumericos2($_GET['tipo']) . " WHERE id_fornecedores_despesas = " . $_GET['id_acesso'];
    odbc_exec($con, $query) or die(odbc_errormsg());
    $query = "YES";
    print(json_encode($query));
}

if (isset($_GET['GetGrupoEmpresas'])) {
    if ($_GET['Contrato'] != "") {
        require "../../configini.php";
        $query = "select case when c1.numero_contrato = '" . $_GET['Contrato'] . "' then c2.numero_contrato else c1.numero_contrato end numero_contrato,
        case when c1.numero_contrato = '" . $_GET['Contrato'] . "' then f2.razao_social else f1.razao_social end razao_social
        from ca_contratos_dependente cn inner join ca_contratos c1 on c1.id = cn.de_contrato
        inner join sf_fornecedores_despesas f1 on f1.id_fornecedores_despesas = c1.id_cliente
        inner join ca_contratos c2 on c2.id = cn.para_contrato
        inner join sf_fornecedores_despesas f2 on f2.id_fornecedores_despesas = c2.id_cliente
        where c1.numero_contrato = '" . $_GET['Contrato'] . "'";
        $cur = odbc_exec($con, $query);
        while ($RFP = odbc_fetch_array($cur)) {
            $rows = array();
            $rows['empresa'] = utf8_encode($RFP['numero_contrato']);
            $rows['razao_social'] = utf8_encode($RFP['razao_social']);
            $records[] = $rows;
        }
        print(json_encode($records));
        odbc_close($con);
        exit();
    }
}

if (isset($_GET['statusByEmail'])) {
    $cur = odbc_exec($con, "select dbo.FU_STATUS_CLI(id_fornecedores_despesas) status
    from sf_fornecedores_despesas inner join sf_fornecedores_despesas_contatos on fornecedores_despesas = id_fornecedores_despesas
    where tipo_contato = 2 and conteudo_contato = " . valoresTexto2($_GET['statusByEmail']));
    while ($RFP = odbc_fetch_array($cur)) {
        echo utf8_encode($RFP['status']);
    }
}

if (isset($_GET['infoByEmail'])) {
    $toReturn = [];
    $cur = odbc_exec($con, "select id_fornecedores_despesas, dbo.FU_STATUS_CLI(id_fornecedores_despesas) status,
    (select max(dt_fim) credencial from sf_fornecedores_despesas_credenciais where dt_cancelamento is null and id_fornecedores_despesas = fornecedores_despesas) credencial
    from sf_fornecedores_despesas inner join sf_fornecedores_despesas_contatos on fornecedores_despesas = id_fornecedores_despesas
    where tipo_contato = 2 and conteudo_contato = " . valoresTexto2($_GET['infoByEmail']));
    while ($RFP = odbc_fetch_array($cur)) {
        $toReturn['id'] = utf8_encode($RFP['id_fornecedores_despesas']);
        $toReturn['status'] = utf8_encode($RFP['status']);
        $toReturn['credencial'] = escreverData($RFP['credencial']);
    }
    if (is_numeric($toReturn['id'])) {
        $cur = odbc_exec($con, "select top 1 legenda_cartao, validade_cartao from sf_fornecedores_despesas_cartao
        where id_fornecedores_despesas = " . $toReturn['id']);
        while ($RFP = odbc_fetch_array($cur)) {
            $cartao = [];            
            $cartao['legenda_cartao'] = utf8_encode($RFP['legenda_cartao']);
            $cartao['validade_cartao'] = escreverData($RFP['validade_cartao']);
            $toReturn['cartao'][] = $cartao;                    
        }        
        $cur = odbc_exec($con, "select id_plano, descricao, [dbo].[FU_PLANO_FIM](id_plano) vencimento from sf_vendas_planos 
        inner join sf_produtos on conta_produto = id_prod_plano
        where dt_cancelamento is null and favorecido = " . $toReturn['id']);
        while ($RFP = odbc_fetch_array($cur)) {
            $plano = [];            
            $plano['id_plano'] = utf8_encode($RFP['id_plano']);
            $plano['descricao'] = utf8_encode($RFP['descricao']);
            $plano['vencimento'] = escreverData($RFP['vencimento']);
            $toReturn['plano'][] = $plano;            
        }        
    }
    print(json_encode($toReturn));
    odbc_close($con);
    exit();    
}

if (isset($_GET['GetWhatsapp'])) {       
    odbc_exec($con, "update sf_whatsapp set status = 3 where status = 0 and len(mensagem) = 0;") or die(odbc_errormsg());
    odbc_exec($con, "update sf_whatsapp set status = 3 where status = 0 and len(telefone) not in (10, 11);") or die(odbc_errormsg());
    odbc_exec($con, "update sf_whatsapp set status = 4 where status = 0 and id not in (select min(id) from sf_whatsapp where status = 0 group by telefone, mensagem);") or die(odbc_errormsg());
    if ($_GET['Contrato'] == "007") {
        odbc_exec($con, "update sf_whatsapp set status = 1 where id in (select w.id from sf_whatsapp w
        inner join sf_fornecedores_despesas c on w.fornecedor_despesas = c.id_fornecedores_despesas
        where sys_login not in ('SISTEMA') and c.grupo_pessoa = 18 and w.status = 0);") or die(odbc_errormsg());
        odbc_exec($con, "update sf_whatsapp set status = 1 where id in (select w.id from sf_whatsapp w
        inner join sf_configuracao_distrib c on w.fornecedor_despesas = c.id_conf where w.status = 0);") or die(odbc_errormsg());
    }
    $records = array();
    $query = "select top 100 * from sf_whatsapp where status = 0 and data_envio < getdate() order by data_envio, id;";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $rows = array();
        $rows['id'] = utf8_encode($RFP['id']);
        $rows['telefone'] = utf8_encode($RFP['telefone']);
        $rows['mensagem'] = utf8_encode($RFP['mensagem']);
        $rows['tipo'] = utf8_encode($RFP['tipo']);
        $records[] = $rows;
    }
    print(json_encode($records));
    odbc_close($con);
    exit();
}

if (isset($_GET['SaveWhatsapp'])) {
    $query = "update sf_whatsapp set status = " . valoresTexto2($_GET['save']) . " where id = " . valoresTexto2($_GET['id']);
    odbc_exec($con, $query) or die(odbc_errormsg());    
    if (isset($_GET['save']) && $_GET['save'] == "1") {
        $query = "insert into sf_telemarketing(de_pessoa,para_pessoa,data_tele,pendente,proximo_contato,obs,pessoa,data_tele_fechamento, obs_fechamento, de_pessoa_fechamento,especificacao,atendimento_servico,garantia,relato,origem,acompanhar,dt_inclusao)
        select isnull(id_usuario, 1), isnull(id_usuario, 1), data_envio, 0, getdate(), telefone, fornecedor_despesas, getdate(), 'WHATSAPP', isnull(id_usuario, 1), 'W', 0, 0, mensagem, 0, 0, getdate() 
        from sf_whatsapp left join sf_usuarios on sys_login = login_user where id = " . valoresTexto2($_GET['id']);
        odbc_exec($con, $query) or die(odbc_errormsg());
    }    
    $query = "YES";
    print(json_encode($query));
}

if (isset($_GET['RespWhatsapp'])) {
    $id_pessoa = 0;
    $nome_pessoa = "";    
    $id_tele = 0;
    $w_nivel = "";
    $saudacao = ((date('H') >= 6 && date('H') <= 12) ? 'bom dia' : (date('H') > 12 && date('H') <=18 ? 'boa tarde' : 'boa noite'));
    $cur = odbc_exec($con, "select id_fornecedores_despesas, upper(left(case when CHARINDEX(' ', razao_social) > 0 then SUBSTRING(razao_social, 0, CHARINDEX(' ', razao_social)) else razao_social end, 1)) +
    lower(stuff(case when CHARINDEX(' ', razao_social) > 0 then SUBSTRING(razao_social, 0, CHARINDEX(' ', razao_social)) else razao_social end, 1, 1, ''))
    razao_social from sf_fornecedores_despesas_contatos inner join sf_fornecedores_despesas on id_fornecedores_despesas = fornecedores_despesas
    where inativo = 0 and tipo_contato = 1 and replace(replace(replace(replace(conteudo_contato, '(', ''), ')', ''), '-', ''), ' ', '') = " . valoresTexto2($_GET['telefone']));
    while ($RFP = odbc_fetch_array($cur)) {
        $id_pessoa = utf8_encode($RFP['id_fornecedores_despesas']);
        $nome_pessoa = utf8_encode($RFP['razao_social']);
    }
    if ($id_pessoa > 0) {
        $cur = odbc_exec($con, "select top 1 id_tele, w_modelo from sf_telemarketing where especificacao = 'W' and cast(getdate() as date) = cast(data_tele as date) and pessoa = " . $id_pessoa . " order by 1 desc");
        while ($RFP = odbc_fetch_array($cur)) {
            $id_tele = utf8_encode($RFP['id_tele']);
            $w_nivel = utf8_encode($RFP['w_modelo']);
        }
        if ($id_tele == 0) {
            odbc_exec($con, "insert into sf_telemarketing (de_pessoa, para_pessoa, data_tele, pendente, proximo_contato, obs, pessoa, data_tele_fechamento, obs_fechamento, de_pessoa_fechamento, especificacao, atendimento_servico, garantia, relato, origem, acompanhar, dt_inclusao, tipo_proprietario) values (1,1, getdate(), 0, getdate(), " . valoresTexto2($_GET['telefone']) . ", " . $id_pessoa . ", getdate(), 'CHATBOT', 1, 'W', 0, 0, '', 0, 0, getdate(), 0)") or die(odbc_errormsg());
            $cur = odbc_exec($con, "select top 1 id_tele, w_modelo from sf_telemarketing where especificacao = 'W' and cast(getdate() as date) = cast(data_tele as date) and pessoa = " . $id_pessoa . " order by 1 desc");
            while ($RFP = odbc_fetch_array($cur)) {
                $id_tele = utf8_encode($RFP['id_tele']);
            }            
        }        
        if (buscarResposta($con, 1, $w_nivel, $_GET['ultima_msg'], $id_tele, array($nome_pessoa, $saudacao))) {
            buscarResposta($con, 0, "", $_GET['ultima_msg'], $id_tele, array($nome_pessoa, $saudacao));
        }
    } else {       
        $cur = odbc_exec($con, "select id_lead, razao_social from sf_lead where inativo = 0 and replace(replace(replace(replace(telefone_contato, '(', ''), ')', ''), '-', ''), ' ', '') = " . valoresTexto2($_GET['telefone']));
        while ($RFP = odbc_fetch_array($cur)) {
            $id_pessoa = utf8_encode($RFP['id_lead']);
            $nome_pessoa = utf8_encode($RFP['razao_social']);
        }
        if ($id_pessoa > 0) {
            if (strlen($nome_pessoa) == 0) {
                $nome_pessoa = $_GET['ultima_msg'];
                odbc_exec($con, "update sf_lead set razao_social = " . valoresTexto2($nome_pessoa) . ", nome_contato = " . valoresTexto2($nome_pessoa) . " where id_lead = " . $id_pessoa) or die(odbc_errormsg());
            }
            buscarResposta($con, 2, $w_nivel, $_GET['ultima_msg'], 0, array($nome_pessoa, $saudacao));
        } else {
            odbc_exec($con, "insert into sf_lead (nome_contato, razao_social, telefone_contato, assunto, procedencia, dt_cadastro) values ('', '', " . 
            valoresTexto2("(" . substr($_GET['telefone'], 0, 2) . ") " . substr($_GET['telefone'], 2, 5) . "-" . substr($_GET['telefone'], 7, 4)) . ", " . valoresTexto2($_GET['ultima_msg']) . ", 0, getdate());") or die(odbc_errormsg());
            buscarResposta($con, 3, $w_nivel, $_GET['ultima_msg'], 0, array($nome_pessoa, $saudacao));
        }
    }
}

function buscarResposta($con, $tipo, $nivel, $msg, $id_tele, $dados) {
    $cur = odbc_exec($con, "select top 1 id, refresh, saida, (select count(pp.id) from sf_whatsapp_modelo pp where pp.id_pai = sf_whatsapp_modelo.id) total 
    from sf_whatsapp_modelo where (entrada = '$' or entrada = " . valoresTexto2($msg) . ") 
    and tipo = " . $tipo . (is_numeric($nivel) ? " and id_pai = " . $nivel : " and id_pai is null") . " order by id");
    while ($RFP = odbc_fetch_array($cur)) {
        $msgReturn = str_replace(array("||A_nomp||", "||A_saudacao||"), $dados, utf8_encode($RFP['saida'])); 
        if ($id_tele > 0) {
            odbc_exec($con, "update sf_telemarketing set relato = relato + '" . date("H:i:S") . " - Cliente: ' + " . valoresTexto2($msg) . " + '\n' + '" . date("H:i:s") . " - Sistema: " . utf8_decode($msgReturn) . "\n'" . 
            ($RFP['refresh'] == "1" ? ", w_modelo = " . ($RFP['total'] > 0 ? $RFP['id'] : "null") : "")  . " where id_tele = " . $id_tele) or die(odbc_errormsg());
        }
        print(json_encode($msgReturn));        
        return false;               
    }
    return true;
}

if (isset($_GET['WinWhatsapp'])) {
    if ($_GET['Contrato'] != "") {
        require "../../Connections/configSivis.php";        
        require "../../Connections/funcoesAux.php";        
        $query = "update ca_contratos set wha_atv = " . valoresTexto2($_GET['save']) . " where numero_contrato = " . valoresTexto2($_GET['Contrato']);
        odbc_exec($con2, $query) or die(odbc_errormsg());
    }
    echo "YES"; exit;
}

odbc_close($con);