var codcli = $("#codcli").val();
var mod_emp = $("#mod_emp").val();
function FecharBox(opc) {
    $("#newbox").remove();
    if (opc === 0) {
    } else {
        $('#accordionContent div').remove();
        carregaUpdate();
    }
}

$(document).ready(function () {
    $(window).load(function () {
        $("#loader").show();
        carregaUpdate();
    });
});

function carregaUpdate() {
    $("#loader").show();
    var buscar;
    if (codcli !== "003") {
        buscar = mod_emp;
    } else {
        buscar = 'ALL';
    }
    var request = $.ajax({
        url: "ajax/update_server_processing.php",
        method: "POST",
        data: {
            op: 'r',
            emp: buscar
        },
        dataType: "html"
    });

    request.done(function (msg) {
        var json = JSON.parse(msg);
        if (codcli !== "003") {
            for (var x = 0; x < json.length; x++) {
                $("#accordionContent").append("<div><h3>" + json[x].data + " - " + json[x].titulo + "</h3></div><div>" + json[x].texto + "</div>");
            }
        } else {
            for (var x = 0; x < json.length; x++) {
                var ativo;
                if (json[x].ativo === 1) {
                    ativo = "<img style='margin-left: 12px;margin-top: -10px;' src='/img/1368741588_check.png'/>";
                } else {
                    ativo = "";
                }
                $("#accordionContent").append("<div><h3>" + json[x].data + " - " + json[x].titulo + ativo + "</h3><span class=\"ico-edit\" onclick=\"abrirTelaBox('FormCadUpdate.php?edit=y&cod=" + json[x].id + "', 540, 720)\"></span></div><div>" + json[x].texto + "</div>");
            }
        }
        $("#accordionContent").accordion({collapsible: true, heightStyle: "content"});
        $('#accordionContent').accordion("refresh");
        $("#loader").hide();
    });

    request.fail(function (jqXHR, textStatus) {
        console("Request failed: " + textStatus);
    });

    function ajustListUpdates(json) {
        var todos = [];
        var proquality = [];
        var sivisPGTO = [];
        var sivisERP = [];
        for (var x = 0; x < json.length; x++) {
            if (json[x].ativo === 1) {
                if (json[x].system === "") {
                    todos.push("<div><h3>" + json[x].data + " - " + json[x].titulo + "</h3></div><div>" + json[x].texto + "</div>");
                } else if (json[x].system.indexOf("0") !== -1) {
                    sivisERP.push("<div><h3>" + json[x].data + " - " + json[x].titulo + "</h3></div><div>" + json[x].texto + "</div>");
                } else if (json[x].system.indexOf("1") !== -1) {
                    sivisPGTO.push("<div><h3>" + json[x].data + " - " + json[x].titulo + "</h3></div><div>" + json[x].texto + "</div>");
                } else if (json[x].system.indexOf("2") !== -1) {
                    proquality.push("<div><h3>" + json[x].data + " - " + json[x].titulo + "</h3></div><div>" + json[x].texto + "</div>");
                }
            }
        }
    }
}
