$(document).ready(function () {
    $(window).load(function () {
        var ckeditor = CKEDITOR.replace('ckeditor', {removePlugins: 'elementspath'});
        if ($("#edit").val() !== '') {
            editar();
        } else {
            novo();
        }
    });
});

function editar() {
    $('button[title="Gravar"],button[title="Cancelar"]').hide();
    $('button[title="Novo"],button[title="Alterar"],button[title="Excluir"]').show();
    $("#txttitulo").prop('disabled', true);
    $("#ckbpublicado").prop('disabled', true);
    $('#itemsStat').select2('disable');
    setTimeout(function () {
        CKEDITOR.instances['ckeditor'].setReadOnly(true);
    }, 500);
}

function editar2() {
    $('button[title="Gravar"],button[title="Cancelar"]').show();
    $('button[title="Novo"],button[title="Alterar"],button[title="Excluir"]').hide();
    $("#txttitulo").prop('disabled', false);
    $("#ckbpublicado").prop('disabled', false);
    $('#itemsStat').select2();
    CKEDITOR.instances['ckeditor'].setReadOnly(false);
}

function novo() {
    $('button[title="Novo"],button[title="Alterar"],button[title="Excluir"]').hide();
    $('button[title="Gravar"],button[title="Cancelar"]').show();
    $("#txttitulo").prop('disabled', false);
    if ($("#edit").val() !== '' || $("#txttitulo").val() !== '') {
        CKEDITOR.instances['ckeditor'].setReadOnly(false);
        CKEDITOR.instances['ckeditor'].setData('');
    }
    $("#txttitulo").val('');
}

function cadUpdate(e) {
    var cod = $("#txtID").val();
    var title = $('#txttitulo').val();
    var texto = CKEDITOR.instances.ckeditor.getData();

    if (e === 'C') {
        parent.$("#loader").show();
        var Ativo;
        if ($('#ckbpublicado').is(":checked")) {
            Ativo = 1;
        } else {
            Ativo = 0;
        }
        var sistemas;
        if ($('#itemsStat').val() === null) {
            sistemas = null;
        } else {
            sistemas = JSON.stringify($('#itemsStat').val());
        }

        if (cod === undefined || cod === '') {
            var request = $.ajax({
                url: "ajax/update_server_processing.php",
                method: "POST",
                data: {
                    op: 'c',
                    titulo: title,
                    texto: texto,
                    ckbpublicado: Ativo,
                    system: sistemas
                }
            });

            request.done(function (data) {
                var dados = JSON.parse(data);
                parent.$("#loader").hide();
                editar();
                parent.$('#accordionContent div').remove();
                parent.carregaUpdate();
                $("#txtID").val(dados[0].total);
            });

            request.fail(function (jqXHR, textStatus) {
                console("Request failed: " + textStatus);
                parent.$("#loader").hide();
            });

        } else {
            var request = $.ajax({
                url: "ajax/update_server_processing.php",
                method: "POST",
                data: {
                    op: 'u',
                    cod: cod,
                    titulo: title,
                    texto: texto,
                    ckbpublicado: Ativo,
                    system: sistemas
                }
            });

            request.done(function (msg) {
                editar();
                parent.$('#accordionContent div').remove();
                parent.carregaUpdate();
                parent.$("#loader").hide();
            });

            request.fail(function (jqXHR, textStatus) {
                console("Request failed: " + textStatus);
                parent.$("#loader").hide();
                parent.carregaUpdate();
            });

        }
    } else if (e === 'D') {
        bootbox.confirm("Deseja realmente excluir?", function (r) {
            if (r === true) {
                parent.$("#loader").show();
                var request = $.ajax({
                    url: "ajax/update_server_processing.php",
                    method: "POST",
                    data: {
                        op: 'd',
                        cod: cod
                    }
                });

                request.done(function (msg) {
                    parent.$("#loader").hide();
                    parent.FecharBox();
                });

                request.fail(function (jqXHR, textStatus) {
                    console("Request failed: " + textStatus);
                    parent.$("#loader").hide();
                    parent.carregaUpdate();
                });
            }
        });
    }
}
