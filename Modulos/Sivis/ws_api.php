<?php

$request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
}
if (!$request_body) {
    $request_body = json_decode(file_get_contents('php://input'), 1);
}
  
if (isset($request_body["functionPage"]) && isset($request_body["loja"]) && is_numeric($request_body["loja"])) {
    require "../../Connections/funcoesAux.php";
    $hostname = getDataBaseServer(1);
    $database = "erp" . str_pad($request_body["loja"], 3, "0", STR_PAD_LEFT);
    $username = "erp" . str_pad($request_body["loja"], 3, "0", STR_PAD_LEFT);
    $password = "!Password123";
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
} else {
    echo json_encode("Loja inválida!"); exit;
}

if ($request_body["functionPage"] == "buscarUsuario") {
    $toReturn = [];
    $query = "select id_fornecedores_despesas, razao_social, cnpj, trabalha_empresa,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = id_fornecedores_despesas) telefone,
    cep, endereco, bairro, complemento, numero, estado, cidade
    from sf_fornecedores_despesas 
    inner join sf_fornecedores_despesas_contatos on id_fornecedores_despesas = fornecedores_despesas
    where tipo_contato = 2 and conteudo_contato = " . valoresTexto2($request_body["email"]);
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $rows = array();
        $rows['id_fornecedores_despesas'] = utf8_encode($RFP['id_fornecedores_despesas']);   
        $rows['razao_social'] = utf8_encode($RFP['razao_social']);   
        $rows['cnpj'] = utf8_encode($RFP['cnpj']);   
        $rows['trabalha_empresa'] = utf8_encode($RFP['trabalha_empresa']);   
        $rows['telefone'] = "";   
        $rows['cep'] = utf8_encode($RFP['cep']);   
        $rows['endereco'] = utf8_encode($RFP['endereco']);   
        $rows['bairro'] = utf8_encode($RFP['bairro']);   
        $rows['complemento'] = utf8_encode($RFP['complemento']);   
        $rows['numero'] = utf8_encode($RFP['numero']);   
        $rows['estado'] = utf8_encode($RFP['estado']);   
        $rows['cidade'] = utf8_encode($RFP['cidade']);   
        $toReturn["usuario"] = $rows;
    }
    echo json_encode($toReturn);
}

if ($request_body["functionPage"] === "buscarRegulamentos") {
    $toReturn = [];
    $contrato = str_pad($request_body["loja"], 3, "0", STR_PAD_LEFT);
    $pasta = __DIR__."/../../Pessoas/".$contrato."/Regulamentos";
    if (is_dir($pasta)) {
        $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' .
            ($_SERVER['HTTP_HOST'] === 'localhost' ? $_SERVER['HTTP_HOST'].'/erp'  : $_SERVER['HTTP_HOST']);
        $cdir = scandir($pasta);
        foreach ($cdir as $key => $value) {
            if (!(in_array($value, array(".", "..")) || is_dir($pasta.'/'.$value))) {
                $rows = [];
                $rows['nome'] = utf8_encode(substr($value,0, strrpos($value, '.')));
                $rows['url'] = $url."/Pessoas/".$contrato."/Regulamentos/".rawurlencode(utf8_encode($value));
                $toReturn[] = $rows;
            }
        }
    }
    echo json_encode($toReturn);
}

if ($request_body["functionPage"] === "buscarImages") {
    $toReturn = [];
    $contrato = str_pad($request_body["loja"], 3, "0", STR_PAD_LEFT);
    $tipo = $request_body['tipo'];
    $arquivo = isset($request_body['arquivo']) ? $request_body['arquivo'] : null;
    $pasta = __DIR__."/../../Pessoas/".$contrato."/".$tipo;
    if (is_dir($pasta)) {
        $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' .
            ($_SERVER['HTTP_HOST'] === 'localhost' ? $_SERVER['HTTP_HOST'].'/erp'  : $_SERVER['HTTP_HOST']);
        $cdir = scandir($pasta);
        foreach ($cdir as $key => $value) {
            if (!(in_array($value, array(".", "..")) || is_dir($pasta.'/'.$value))) {
                $nome = substr($value,0, strrpos($value, '.'));
                if (!$arquivo || $arquivo == $nome) {
                    $rows = [];
                    $rows['nome'] = utf8_encode($nome);
                    $rows['url'] = $url."/Pessoas/".$contrato."/".$tipo."/".utf8_encode($value);
                    $toReturn[] = $rows;
                }
            }
        }
    }
    echo json_encode($toReturn);
}

if ($request_body['functionPage'] === 'getInfoCarteira') {
    if (isset($request_body['id_plano'])) {
        $_GET['id_plano'] = $request_body['id_plano'];
    } else if (isset($request_body['id_usuario'])) {
        $_GET['id_pessoa'] = $request_body['id_usuario'];
    } 
    $dados = [];    
    $id = $request_body['id_carteira'];
    $sql = "select top 1 * from sf_carteirinhas where id = ".valoresSelect2($id);
    $cur = odbc_exec($con, $sql);
    while($row = odbc_fetch_array($cur)) {
        $dados = $row;
    }
    if (isset($request_body['id_plano']) && isset($request_body['id_usuario']) && $dados["list_dep"] == "1") {
        $_GET['id_dependente'] = $request_body['id_usuario'];
    }
    $tmpMsg = $dados['mensagem'];
    require __DIR__.'/../Comercial/modelos/variaveis.php';
    //echo json_encode($dicionario); exit;
    $arrayTemp = [];
    for ($i = 0; $i < count($dicionario); $i++) {
        if (is_array($dicionario[$i][1])) {
            $chave = substr($dicionario[$i][0], strrpos($dicionario[$i][0], '_') + 1);
            if (!array_key_exists($chave, $arrayTemp)) {
                $arrayTemp[$chave] = [];
            }
            foreach($dicionario[$i][1] as $key => $item) {
                $arrayTemp[$chave][$key][$dicionario[$i][0]] = $item;         
            }
        } else {
            $tmpMsg = str_replace("||" . $dicionario[$i][0] . (strpos($dicionario[$i][0], "|") ? "|" : "||"), $dicionario[$i][1], $tmpMsg);
        }
    }
    $tipos = ["dep"];
    if ($arrayTemp) {
        $templateHtml = htmlentities($tmpMsg);
        preg_match_all(htmlentities('#</?tr>#'),$templateHtml, $lines, PREG_OFFSET_CAPTURE);
        $linesFormatadas = array_map(function($item) {
            return $item[1];
        }, $lines[0]);
        foreach ($arrayTemp as $chave => $item) {
            if (in_array($chave, $tipos)) {
                $pos = strpos($templateHtml, $chave);
                if ($pos > 0) {
                    $linesFormatadas = array_map(function($item) {
                        return $item[1];
                    }, $lines[0]);
                    $step = [];
                    for ($i = 0; $i < count($linesFormatadas) -1; $i++) {
                        if ($pos > $linesFormatadas[$i] && $pos < $linesFormatadas[$i+1]) {
                            $step = [
                                'min' => $linesFormatadas[$i],
                                'max' => $linesFormatadas[$i+1]+14
                            ];
                        }
                    }
                    $itemHtml = substr($templateHtml, $step['min'], $step['max'] - $step['min']);
                    //echo $itemHtml; exit;
                    $subjectHtml = "";
                    foreach($item as $e) {
                        $subHtml = $itemHtml;
                        foreach ($e as $k => $v) {
                            $subHtml = str_replace("||" .$k. (strpos($k, "|") ? "|" : "||"), strtoupper($v), $subHtml);
                        }
                        $subjectHtml.= $subHtml;
                    }
                    $tmpMsg = html_entity_decode(str_replace($itemHtml, $subjectHtml, $templateHtml));
                }
            } 
        }
    }
    echo $tmpMsg;
}

if ($request_body['functionPage'] ==='getModelo') {
    $id = $request_body['id_modelo'];
    $sql = "select top 1 * from sf_emails where tipo = 2 and id_email = ".valoresSelect2($id);
    $cur = odbc_exec($con, $sql);
    $dados = [];
    while($row = odbc_fetch_array($cur)) {
        $dados = $row;
    }
    $tmpMsg = $dados['mensagem'];
    require __DIR__.'/../Comercial/modelos/variaveis.php';
    $arrayTemp = [];
    for ($i = 0; $i < count($dicionario); $i++) {
        $tmpMsg = str_replace("||" . $dicionario[$i][0] . (strpos($dicionario[$i][0], "|") ? "|" : "||"), $dicionario[$i][1], $tmpMsg);
    }
    echo $tmpMsg;
}

if ($request_body['functionPage'] === "buscarContratos") {
    $toReturn = [];
    $contrato = str_pad($request_body["loja"], 3, "0", STR_PAD_LEFT);
    $pasta = __DIR__."/../../Pessoas/".$contrato."/";
    if (is_dir($pasta)) {
        $toReturn = array_reduce($request_body['items'], 
            function($acc, $item) use($pasta) {
            $acc['contratos'][$item['id_plano']] = $item['contrato_id'] > 0 && file_exists($pasta."Assinaturas/".$item['id_cliente']."/".$item['id_plano']."_".$item['contrato_id'].".png") ? 1 : 0;
            
            $acc['comprovantes'][$item['id_mens']] = ['existe' => 0, 'arquivo'=> ''];            
            $files = is_file_save($pasta, $item, $item['id_mens']);
            if (count($files)) {
                $acc['comprovantes'][$item['id_mens']] = ['existe' => 1, 'arquivo'=> current($files)];
            }
            
            $acc['residencia'][$item['id_plano']] = ['existe' => 0, 'arquivo'=> ''];
            $files_res = is_file_save($pasta, $item, "res");
            if (count($files_res)) {
                $acc['residencia'][$item['id_plano']] = ['existe' => 1, 'arquivo'=> current($files_res)];
            }
            
            $acc['habilitacao'][$item['id_plano']] = ['existe' => 0, 'arquivo'=> ''];            
            $files_cnh = is_file_save($pasta, $item, "cnh");
            if (count($files_cnh)) {
                $acc['habilitacao'][$item['id_plano']] = ['existe' => 1, 'arquivo'=> current($files_cnh)];
            }            
            
            $acc['documento'][$item['id_plano']] = ['existe' => 0, 'arquivo'=> ''];            
            $files_cnh = is_file_save($pasta, $item, "doc");
            if (count($files_cnh)) {
                $acc['documento'][$item['id_plano']] = ['existe' => 1, 'arquivo'=> current($files_cnh)];
            }            
            
            return $acc;
        }, ['contratos' => [], 'comprovantes' => [], 'residencia' => [], 'habilitacao' => [], 'documento' => []]);
    }
    echo json_encode($toReturn);
}

if ($request_body['functionPage'] === "removerContrato") {
    $toReturn = [];
    $contrato = str_pad($request_body["loja"], 3, "0", STR_PAD_LEFT);
    $pasta = __DIR__."/../../Pessoas/".$contrato."/";
    if (file_exists($pasta."Assinaturas/".$request_body['id_usuario']."/".$request_body['id_plano']."_".$request_body['contrato_id'].".png")) {
        unlink($pasta."Assinaturas/".$request_body['id_usuario']."/".$request_body['id_plano']."_".$request_body['contrato_id'].".png");   
        $toReturn[] = "img";
    }
    if (file_exists($pasta."Documentos/".$request_body['id_usuario']."/".$request_body['id_plano']."_".$request_body['contrato_id'].".pdf")) {
        unlink($pasta."Documentos/".$request_body['id_usuario']."/".$request_body['id_plano']."_".$request_body['contrato_id'].".pdf");        
        $toReturn[] = "pdf";        
    }
    echo json_encode($toReturn);    
}

function is_file_save($pasta, $item, $doc) {
    return array_filter(scandir($pasta."Documentos/".$item['id_cliente']), 
    function($file) use($pasta, $item, $doc) {
        if (!(in_array($file, [".", ".."]) || is_dir($pasta.'Documentos/' . $item['id_cliente'] . '/' . $file))) {
            return strpos($file, $item['id_plano'] . "_" . $doc) > -1;
        }
        return false;
    });
}

if ($request_body['functionPage'] === "saveUploadComprovante") {
    if (!isset($_FILES['arquivo']) || $_FILES['arquivo']['erro'] > 0) {
        echo json_encode(['erro' => 'Arquivo não enviado']);
        exit;
    }
    $contrato = str_pad($request_body["loja"], 3, "0", STR_PAD_LEFT);
    $extension = explode('/', $_FILES['arquivo']['type'])[1];
    $pasta = __DIR__."/../../Pessoas/".$contrato."/Documentos/".$request_body['id_cliente'];
    if (!is_dir($pasta)) {
        mkdir($pasta, 0777);
    }
    $destination = $pasta."/".$request_body['id_plano'] . "_" . 
    ($request_body['tipo'] == "R" ? "res" : ($request_body['tipo'] == "H" ? "cnh" : ($request_body['tipo'] == "D" ? "doc" : $request_body['id_mens']))) . 
    "." . $extension;
    if (!move_uploaded_file($_FILES['arquivo']['tmp_name'], $destination)) {
        echo json_encode([
            'erro' => 'Ocorreu um erro ao salvar um arquivo'
        ]);
        exit;
    }
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' .
    ($_SERVER['HTTP_HOST'] === 'localhost' ? $_SERVER['HTTP_HOST'].'/erp'  : $_SERVER['HTTP_HOST']);
    echo json_encode([
        'arquivo' => $url."/Pessoas/".$contrato."/Documentos/".$request_body['id_cliente']."/"
        .$request_body['id_plano']."_".$request_body['id_mens'].".".$extension
    ]);
    exit;
}

odbc_close($con);
