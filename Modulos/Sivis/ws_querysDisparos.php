<?php

$arrayQuery = Array();

$id_item = "1"; //Sem presença
$query = "  select distinct B.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp,'' id_mens
            from sf_fornecedores_despesas B 
            where @lojaDisparoAuto inativo = 0 and B.fornecedores_status like 'Ativo%' and 
            (select datediff(day,MAX(data_acesso),getdate()) from sf_acessos where id_fonecedor = B.id_fornecedores_despesas) = @dias_tolerancia_item";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "2"; //Antes do vencimento do plano
$query = "  select distinct A.favorecido id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = A.favorecido) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = A.favorecido) 'email',
            B.id_user_resp,'' id_mens
            from sf_vendas_planos A inner join sf_fornecedores_despesas B on A.favorecido = B.id_fornecedores_despesas
            where A.dt_cancelamento is null AND B.tipo = 'C' AND @lojaDisparoAuto dt_fim = dateadd(day,@dias_tolerancia_item,cast(getdate() as date)) and A.planos_status = 'Ativo'";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "3"; //Após o vencimento do plano
$query = "  set dateformat dmy;
            select distinct A.favorecido id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = A.favorecido) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = A.favorecido) 'email',
            B.id_user_resp,'' id_mens
            from sf_vendas_planos A inner join sf_fornecedores_despesas B on A.favorecido = B.id_fornecedores_despesas
            where A.dt_cancelamento is null AND B.tipo = 'C' AND @lojaDisparoAuto dt_fim = dateadd(day,-@dias_tolerancia_item,cast(getdate() as date))";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "4"; //Após a matrícula
$query = "  set dateformat dmy;
            select distinct A.favorecido id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = A.favorecido) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = A.favorecido) 'email',
            B.id_user_resp,'' id_mens
            from sf_vendas_planos A inner join sf_fornecedores_despesas B on A.favorecido = B.id_fornecedores_despesas
            where A.dt_cancelamento is null AND @lojaDisparoAuto cast(B.dt_convert_cliente as date) = dateadd(day,-@dias_tolerancia_item,cast(getdate() as date))";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "5"; //Antes do vencimento do cartão Vantagens
$query = "  select distinct B.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp,'' id_mens from sf_fornecedores_despesas B 
            where @lojaDisparoAuto id_fornecedores_despesas > 0 and B.fornecedores_status = 'Ativo' and 
            validade_cartao is not null and validade_cartao = dateadd(day,@dias_tolerancia_item,cast(getdate() as date))";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "6"; //Após o vencimento do cartão Vantagens
$query = "  select distinct B.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp,'' id_mens from sf_fornecedores_despesas B 
            where @lojaDisparoAuto id_fornecedores_despesas > 0 and B.fornecedores_status = 'Ativo' and 
            validade_cartao is not null and validade_cartao = dateadd(day,-@dias_tolerancia_item,cast(getdate() as date))";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "7"; //Aniversário do cliente ativo
$query = "  set dateformat dmy;
            select distinct id_fornecedores_despesas, 
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp,'' id_mens from sf_fornecedores_despesas B 
            where @lojaDisparoAuto inativo = 0 and tipo = 'C' and B.fornecedores_status like 'Ativo%' 
            and MONTH(data_nascimento) = MONTH(GETDATE()) and DAY(data_nascimento) = DAY(GETDATE())";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "8"; //Aniversário do cliente inativo
$query = "  set dateformat dmy;
            select distinct B.id_fornecedores_despesas, 
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp,'' id_mens from sf_fornecedores_despesas B 
            where @lojaDisparoAuto inativo = 0 and tipo = 'C' and B.fornecedores_status like 'Inativo%' 
            and MONTH(data_nascimento) = MONTH(GETDATE()) and DAY(data_nascimento) = DAY(GETDATE())";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "9"; //Antes do vencimento do cartão DCC
$query = "  select distinct A.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = A.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = A.id_fornecedores_despesas) 'email',
            B.id_user_resp,'' id_mens from sf_fornecedores_despesas_cartao A inner join sf_fornecedores_despesas B on A.id_fornecedores_despesas = B.id_fornecedores_despesas 
            where @lojaDisparoAuto A.validade_cartao is not null and A.fornecedores_status like 'Ativo%'
            and A.validade_cartao = dateadd(day,@dias_tolerancia_item,cast(getdate() as date))";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "10"; //Após o vencimento do cartão DCC
$query = "  select distinct A.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = A.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = A.id_fornecedores_despesas) 'email',
            B.id_user_resp,'' id_mens from sf_fornecedores_despesas_cartao A inner join sf_fornecedores_despesas B on A.id_fornecedores_despesas = B.id_fornecedores_despesas 
            where @lojaDisparoAuto A.validade_cartao is not null and A.fornecedores_status like 'Ativo%'
            and A.validade_cartao = dateadd(day,-@dias_tolerancia_item,cast(getdate() as date))";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "11"; //Após cobrança recusada do DCC
$query = "  set dateformat dmy;
            select distinct A.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = A.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = A.id_fornecedores_despesas) 'email',
            B.id_user_resp, id_mens from dbo.sf_vendas_itens_dcc A 
            inner join sf_fornecedores_despesas B on A.id_fornecedores_despesas = B.id_fornecedores_despesas 
            left join sf_vendas_planos_mensalidade C on A.id_plano_item = C.id_mens
            where @lojaDisparoAuto status_transacao <> 'A' and status_transacao <> 'E' and C.id_item_venda_mens is null
            and dateadd(day,@dias_tolerancia_item,cast(transactionTimestamp as date)) = CAST(GETDATE() as date)";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "12"; //Antes da data de cancelamento agendado DCC
$query = "  set dateformat dmy;
            select distinct D.favorecido id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = D.favorecido) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = D.favorecido) 'email',
            B.id_user_resp,'' id_mens from dbo.sf_vendas_planos_dcc_agendamento A inner join sf_vendas_planos D on A.id_plano_agendamento = D.id_plano 
            inner join sf_fornecedores_despesas B on D.favorecido = B.id_fornecedores_despesas and D.fornecedores_status = 'Ativo' 
            where @lojaDisparoAuto dt_cancel_agendamento = dateadd(day,@dias_tolerancia_item,cast(getdate() as date))";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "13"; //Antes do vencimento da parcela (ERP)
$query = "  select distinct B.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp,'' id_mens
            from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento 
            inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento 
            inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas 
            left join sf_tipo_documento on sf_lancamento_movimento_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento 
            inner join sf_fornecedores_despesas B on sf_lancamento_movimento.fornecedor_despesa = B.id_fornecedores_despesas 
            where sf_lancamento_movimento.status = 'Aprovado' and @lojaDisparoAuto sf_contas_movimento.tipo = 'C' AND valor_pago = 0 AND data_vencimento < GETDATE() 
            AND dateadd(day,-@dias_tolerancia_item,data_vencimento) = cast(GETDATE() as date) AND sf_lancamento_movimento_parcelas.inativo = '0' 
            union select distinct B.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp,'' id_mens  
            from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao 
            inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento 
            inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas left join sf_tipo_documento on sf_solicitacao_autorizacao_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento 
            inner join sf_fornecedores_despesas B on sf_solicitacao_autorizacao.fornecedor_despesa = B.id_fornecedores_despesas 
            where @lojaDisparoAuto sf_contas_movimento.tipo = 'C' and status = 'Aprovado' AND valor_pago = 0 AND data_parcela < GETDATE() 
            AND dateadd(day,-@dias_tolerancia_item,data_parcela) = cast(GETDATE() as date) AND sf_solicitacao_autorizacao_parcelas.inativo = '0' 
            union select distinct B.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp,'' id_mens
            from sf_venda_parcelas inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda 
            inner join sf_contas_movimento on sf_vendas.conta_movimento = sf_contas_movimento.id_contas_movimento 
            inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas 
            left join sf_tipo_documento on sf_venda_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento 
            inner join sf_fornecedores_despesas B on sf_vendas.cliente_venda = B.id_fornecedores_despesas 
            where @lojaDisparoAuto sf_vendas.cov = 'V' and status = 'Aprovado' AND valor_pago = 0 AND data_parcela < GETDATE() 
            AND dateadd(day,-@dias_tolerancia_item,data_parcela) = cast(GETDATE() as date) AND sf_venda_parcelas.inativo = '0'";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "14"; //Após o vencimento da parcela (ERP)
$query = "  select distinct B.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp,'' id_mens
            from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento 
            inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento 
            inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas 
            left join sf_tipo_documento on sf_lancamento_movimento_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento 
            inner join sf_fornecedores_despesas B on sf_lancamento_movimento.fornecedor_despesa = B.id_fornecedores_despesas 
            where sf_lancamento_movimento.status = 'Aprovado' and @lojaDisparoAuto sf_contas_movimento.tipo = 'C' AND valor_pago = 0 AND data_vencimento < GETDATE() 
            AND dateadd(day,@dias_tolerancia_item,data_vencimento) = cast(GETDATE() as date) AND sf_lancamento_movimento_parcelas.inativo = '0'             
            union select distinct B.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp,'' id_mens
            from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao 
            inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento 
            inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas left join sf_tipo_documento on sf_solicitacao_autorizacao_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento 
            inner join sf_fornecedores_despesas B on sf_solicitacao_autorizacao.fornecedor_despesa = B.id_fornecedores_despesas 
            where @lojaDisparoAuto sf_contas_movimento.tipo = 'C' and status = 'Aprovado' AND valor_pago = 0 AND data_parcela < GETDATE() 
            AND dateadd(day,@dias_tolerancia_item,data_parcela) = cast(GETDATE() as date) AND sf_solicitacao_autorizacao_parcelas.inativo = '0'             
            union select distinct B.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp,'' id_mens  
            from sf_venda_parcelas inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda 
            inner join sf_contas_movimento on sf_vendas.conta_movimento = sf_contas_movimento.id_contas_movimento 
            inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas 
            left join sf_tipo_documento on sf_venda_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento 
            inner join sf_fornecedores_despesas B on sf_vendas.cliente_venda = B.id_fornecedores_despesas 
            where @lojaDisparoAuto sf_vendas.cov = 'V' and status = 'Aprovado' AND valor_pago = 0 AND data_parcela < GETDATE() 
            AND dateadd(day,@dias_tolerancia_item,data_parcela) = cast(GETDATE() as date) AND sf_venda_parcelas.inativo = '0'";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "15"; //Antes do vencimento da Mensalidade
$query = "  select B.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp, id_mens from sf_vendas_planos_mensalidade inner join sf_vendas_planos on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens
            inner join sf_fornecedores_despesas B on B.id_fornecedores_despesas = sf_vendas_planos.favorecido
            where sf_vendas_planos.dt_cancelamento is null AND B.tipo = 'C' AND @lojaDisparoAuto id_item_venda_mens is null and dateadd(day,-@dias_tolerancia_item,dt_inicio_mens) = cast(GETDATE() as date)";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "16"; //Após o vencimento da Mensalidade
$query = "  select B.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp, id_mens from sf_vendas_planos_mensalidade inner join sf_vendas_planos on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens
            inner join sf_fornecedores_despesas B on B.id_fornecedores_despesas = sf_vendas_planos.favorecido
            where sf_vendas_planos.dt_cancelamento is null AND B.tipo = 'C' AND @lojaDisparoAuto id_item_venda_mens is null and dateadd(day,@dias_tolerancia_item,dt_inicio_mens) = cast(GETDATE() as date)";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "17"; //Antes do vencimento da Documento
$query = "  select B.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp,'' id_mens from sf_fornecedores_despesas_documentos A
            inner join sf_fornecedores_despesas B on B.id_fornecedores_despesas = A.id_fornecedor_despesas
            where @lojaDisparoAuto dateadd(day,-@dias_tolerancia_item,data_documento) = cast(GETDATE() as date)";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "18"; //Após o vencimento da Documento
$query = "  select B.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp,'' id_mens from sf_fornecedores_despesas_documentos A
            inner join sf_fornecedores_despesas B on B.id_fornecedores_despesas = A.id_fornecedor_despesas
            where @lojaDisparoAuto dateadd(day,@dias_tolerancia_item,data_documento) = cast(GETDATE() as date)";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "19"; //Após do vencimento do Boleto
$query = "  select B.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp, id_mens from sf_vendas_planos_mensalidade inner join sf_vendas_planos on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens
            inner join sf_fornecedores_despesas B on B.id_fornecedores_despesas = sf_vendas_planos.favorecido
            inner join sf_boleto C on C.id_referencia = sf_vendas_planos_mensalidade.id_mens and tp_referencia = 'M'
            inner join sf_produtos_parcelas on id_parcela = id_parc_prod_mens
            where sf_produtos_parcelas.parcela not in (-2) and sf_vendas_planos.dt_cancelamento is null AND @lojaDisparoAuto id_item_venda_mens is null and dateadd(day,@dias_tolerancia_item,dt_inicio_mens) = cast(GETDATE() as date) 
            union select B.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp, C.id_mens from sf_vendas_planos_mensalidade inner join sf_vendas_planos on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens
            inner join sf_fornecedores_despesas B on B.id_fornecedores_despesas = sf_vendas_planos.favorecido
            inner join sf_boleto_online_itens C on C.id_mens = sf_vendas_planos_mensalidade.id_mens
            inner join sf_produtos_parcelas on id_parcela = id_parc_prod_mens
            where sf_produtos_parcelas.parcela not in (-2) and sf_vendas_planos.dt_cancelamento is null AND @lojaDisparoAuto id_item_venda_mens is null and dateadd(day,@dias_tolerancia_item,dt_inicio_mens) = cast(GETDATE() as date)";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "20"; //Antes o vencimento do Boleto
$query = "  select B.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp, id_mens from sf_vendas_planos_mensalidade inner join sf_vendas_planos on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens
            inner join sf_fornecedores_despesas B on B.id_fornecedores_despesas = sf_vendas_planos.favorecido
            inner join sf_boleto C on C.id_referencia = sf_vendas_planos_mensalidade.id_mens and tp_referencia = 'M'
            inner join sf_produtos_parcelas on id_parcela = id_parc_prod_mens
            where sf_produtos_parcelas.parcela not in (-2) and sf_vendas_planos.dt_cancelamento is null AND @lojaDisparoAuto id_item_venda_mens is null and dateadd(day,-@dias_tolerancia_item,dt_inicio_mens) = cast(GETDATE() as date) 
            union select B.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp, C.id_mens from sf_vendas_planos_mensalidade inner join sf_vendas_planos on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens
            inner join sf_fornecedores_despesas B on B.id_fornecedores_despesas = sf_vendas_planos.favorecido
            inner join sf_boleto_online_itens C on C.id_mens = sf_vendas_planos_mensalidade.id_mens
            inner join sf_produtos_parcelas on id_parcela = id_parc_prod_mens
            where sf_produtos_parcelas.parcela not in (-2) and sf_vendas_planos.dt_cancelamento is null AND @lojaDisparoAuto id_item_venda_mens is null and dateadd(day,-@dias_tolerancia_item,dt_inicio_mens) = cast(GETDATE() as date)";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "21"; //Após cadastro
$query = "  select distinct id_fornecedores_despesas, 
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp,'' id_mens from sf_fornecedores_despesas B 
            where @lojaDisparoAuto @id_pessoa inativo = 0 and tipo = 'C' union all ";
$query .= " select distinct FD.id_dependente, 
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = FD.id_dependente) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = FD.id_dependente) 'email',
            B.id_user_resp,'' id_mens from sf_fornecedores_despesas B 
            inner join sf_fornecedores_despesas_dependentes FD on FD.id_titular = B.id_fornecedores_despesas
            where @lojaDisparoAuto @id_pessoa inativo = 0 and tipo = 'C'";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "22"; //Após do vencimento do Pix
$query = "  select B.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp, id_mens from sf_vendas_planos_mensalidade inner join sf_vendas_planos on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens
            inner join sf_fornecedores_despesas B on B.id_fornecedores_despesas = sf_vendas_planos.favorecido
            inner join sf_boleto C on C.id_referencia = sf_vendas_planos_mensalidade.id_mens and tp_referencia = 'M'
            inner join sf_produtos_parcelas on id_parcela = id_parc_prod_mens
            where sf_produtos_parcelas.parcela in (-2) and sf_vendas_planos.dt_cancelamento is null AND @lojaDisparoAuto id_item_venda_mens is null and dateadd(day,@dias_tolerancia_item,dt_inicio_mens) = cast(GETDATE() as date)";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "23"; //Antes do vencimento do Pix
$query = "  select B.id_fornecedores_despesas,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp, id_mens from sf_vendas_planos_mensalidade inner join sf_vendas_planos on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens
            inner join sf_fornecedores_despesas B on B.id_fornecedores_despesas = sf_vendas_planos.favorecido
            inner join sf_boleto C on C.id_referencia = sf_vendas_planos_mensalidade.id_mens and tp_referencia = 'M'
            inner join sf_produtos_parcelas on id_parcela = id_parc_prod_mens
            where sf_produtos_parcelas.parcela in (-2) and sf_vendas_planos.dt_cancelamento is null AND @lojaDisparoAuto id_item_venda_mens is null and dateadd(day,-@dias_tolerancia_item,dt_inicio_mens) = cast(GETDATE() as date)";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "24"; //Após reprovação do veículo
$query = "  select distinct B.id_fornecedores_despesas, 
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp,'' id_mens from sf_fornecedores_despesas B 
            inner join sf_fornecedores_despesas_veiculo v on v.id_fornecedores_despesas = B.id_fornecedores_despesas
            where @lojaDisparoAuto @id_pessoa inativo = 0 and tipo = 'C' and status = 'Reprovado' and cast(data_aprov as date) = dateadd(day,-@dias_tolerancia_item,cast(getdate() as date))";
$arrayQuery[] = add_array($id_item, $query);

$id_item = "25"; //Após do vencimento da Vistoria de Cliente
$query = "  select distinct B.id_fornecedores_despesas, 
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = B.id_fornecedores_despesas) 'celular',
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = B.id_fornecedores_despesas) 'email',
            B.id_user_resp,'' id_mens 
            from sf_fornecedores_despesas B 
            inner join sf_fornecedores_despesas_veiculo v on v.id_fornecedores_despesas = B.id_fornecedores_despesas
            inner join sf_vistorias vi on vi.id_veiculo = v.id
            where @lojaDisparoAuto @id_pessoa b.inativo = 0 and b.tipo = 'C' and v.status = 'Aprovado' and vi.data_aprov is null and cast(data as date) = dateadd(day,-@dias_tolerancia_item,cast(getdate() as date))";
$arrayQuery[] = add_array($id_item, $query);

function add_array($id, $texto) {
    $row = Array();
    $row["id"] = $id;
    $row["query"] = $texto;
    return $row;
}
