<?php
set_time_limit(1800);

if (isset($_GET['GetBanco']) && in_array($_SERVER['REMOTE_ADDR'], ["148.72.177.101"])) {
    require "../../configini.php";
    $where = "";
    if (isset($_GET['server'])) {
        $where .= " and servidor = " . $_GET['server'];
    }
    if (isset($_GET['isAtivos'])) {
        $where .= " and A.inativa in (0,2) ";
    }
    if (isset($_GET['isWhatsapp'])) {
        $where .= " and A.inativa in (0,2) and A.mdl_wha = 1 ";
    }
    /*if (isset($_GET['isDCC']) && $_GET['isDCC'] == "S") {
        $query = "select distinct id_cliente, banco, local, login, senha, A.inativa, mdl_aca, wha_atv, mdl_cht from dbo.ca_contratos A
        inner join ca_clientes B on A.id_cliente = B.sf_cliente
        and B.dcc_bloqueio = 0 " . $where . " order by banco";
    } else {*/
        $query = "select id_cliente, banco, local, login, senha, inativa, mdl_aca, wha_atv, mdl_cht from dbo.ca_contratos A
        where id > 0 " . $where . " order by numero_contrato";
    //}
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $rows = array();
        $rows['id_cliente'] = utf8_encode($RFP['id_cliente']);
        $rows['label'] = str_replace("erp", "", $RFP['banco']);
        $rows['banco'] = utf8_encode($RFP['banco']);
        $rows['local'] = utf8_encode($RFP['local']);
        $rows['login'] = utf8_encode($RFP['login']);
        $rows['senha'] = utf8_encode($RFP['senha']);
        $rows['inativa'] = utf8_encode($RFP['inativa']);
        $rows['tipo'] = (isset($_GET['isWhatsapp']) ? utf8_encode($RFP['wha_atv']) : utf8_encode($RFP['mdl_aca']));
        $rows['chatbot'] = utf8_encode($RFP['mdl_cht']);        
        $records[] = $rows;
    }
    print(json_encode($records));
    odbc_close($con);
    exit();
}

if (isset($_GET['ExecutaScript']) && in_array($_SERVER['REMOTE_ADDR'], ["148.72.177.101"])) {
    $hostname = $_GET['Servidor'];
    $database = $_GET['Banco'];
    $username = $_GET['Login'];
    $password = $_GET['Senha'];
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
    require_once(__DIR__ . '/../../Connections/funcoesAux.php');
    $query = $_GET['QueryStr'];
    $objExec = odbc_exec($con, $query) or die(odbc_errormsg());
    if (!$objExec) {
        print(json_encode("NO"));
        odbc_close($con);
        exit();
    } else {
        print(json_encode("YES"));
        odbc_close($con);
        exit();
    }
}

if (isset($_GET['GeraDCC'])) {
   $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' .
        ($_SERVER['HTTP_HOST'] === 'localhost' ? $_SERVER['HTTP_HOST'].'/erp'  : $_SERVER['HTTP_HOST']);
    $totalDCC = 0;
    $totalSucesso = 0;
    $totalErro = 0;
    $paginacao = 10;
    $contrato = str_replace('ERP', '', strtoupper($_GET['Banco']));
    $hostname = $_GET['Servidor'];
    $database = $_GET['Banco'];
    $username = $_GET['Login'];
    $password = $_GET['Senha'];
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
    require_once(__DIR__ . '/../../Connections/funcoesAux.php');
    $dataAtualizacao = $_GET['DataAtualizacao'];
    $queryListaDCC = "set dateformat dmy;
    select * from sf_vendas_planos_mensalidade A
    inner join sf_produtos_parcelas B on (A.id_parc_prod_mens = B.id_parcela and parcela = 0)
    inner join sf_vendas_planos C on A.id_plano_mens = C.id_plano and C.dt_cancelamento is null
    inner join sf_fornecedores_despesas D on C.favorecido = D.id_fornecedores_despesas
    and dt_pagamento_mens is null and D.inativo = 0 
    and dt_inicio_mens between dateadd(day, (-1 * (select DCC_RECORRENCIA from sf_configuracao)), '" . $dataAtualizacao . "') and '" . $dataAtualizacao . "'
    and (select count(id_agendamento) from sf_vendas_planos_dcc_agendamento where id_plano_agendamento = C.id_plano
    and dt_cancel_agendamento < '" . $dataAtualizacao . "') = 0 
    and (select count(id_transacao) from sf_vendas_itens_dcc where cast(transactionTimestamp as DATE) = '" . $dataAtualizacao . "'
    and id_plano_item = A.id_mens) = 0 
    and (select count(id_cartao) from sf_fornecedores_despesas_cartao where D.id_fornecedores_despesas = id_fornecedores_despesas and validade_cartao > getdate()) > 0";
    $colSql = "select count(*) as total";
    $cur = odbc_exec($con, str_replace('select *', $colSql, $queryListaDCC)) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $totalDCC = $RFP['total'];
    }
    if ($totalDCC <= 0) {
        $rows['MensagemDCC'] = 'Nenhuma transação encontrada';
        $rows['TotalSucesso'] = 0;
        $rows['TotalErro'] = 0;
        $rows['TotalDcc'] = 0;
        print(json_encode($rows));
        exit();
    }
    $rows = array();
    $_POST['isList'] = 1;
    $_POST['btnSalvar'] = 1;
    include (__DIR__ . "/../../util/dcc/makeDCC.php");
    if ($msgReturn != "") {
        $rows['MensagemDCC'] = $msgReturn;
        $rows['TotalSucesso'] = 0;
        $rows['TotalErro'] = 0;
        $rows['TotalDcc'] = 0;
        print(json_encode($rows));
        exit();
    } else {
        /*if (($totalDCC > ($totalSivis - $totalMaxpago)) && substr($contrato, 0, 1) != "9" && $dcc_tp_operacao == 0) { //&& $tipo != "TEST"
            $rows['MensagemDCC'] = 'Saldo insuficiente';
            $rows['TotalSucesso'] = 0;
            $rows['TotalErro'] = 0;
            $rows['TotalDcc'] = 0;
            print(json_encode($rows));
            exit();
        }*/
        $jsonValorAde = 0;
        $colSql = "select top " . $paginacao . " id_mens, valor_mens, id_fornecedores_despesas, empresa, id_produto,id_plano,
        (select max(id_cartao) from sf_fornecedores_despesas_cartao where validade_cartao > getdate() and id_fornecedores_despesas = C.favorecido) id_cartao";
        $cur2 = odbc_exec($con, str_replace('select *', $colSql, $queryListaDCC)) or die(odbc_errormsg());
        while ($RFP2 = odbc_fetch_array($cur2)) {
            $dadosAcres = "?&verificaAcrescimo=" . $RFP2['id_fornecedores_despesas'] .
                    "&idProd=" . $RFP2['id_plano'] .
                    "&hostname=" . $hostname .
                    "&database=" . $database .
                    "&username=" . $username .
                    "&password=" . $password .
                    "&valorProduto=" . $RFP2['valor_mens'];
            $jsonurl = $url . "/modulos/academia/form/ClientesFormServer.php" . $dadosAcres;
            $jsonValorAcres = file_get_contents($jsonurl);
            $dadosConv = "?&verificaConvenio=" . $RFP2['id_fornecedores_despesas'] .
                    "&idProd=" . $RFP2['id_produto'] .
                    "&idPlano=" . $RFP2['id_mens'] .
                    "&idConvProd=" . $RFP2['id_plano'] .                    
                    "&hostname=" . $hostname .
                    "&database=" . $database .
                    "&username=" . $username .
                    "&password=" . $password .
                    "&valorProduto=" . ($RFP2['valor_mens'] + valoresNumericos2($jsonValorAcres));
            $jsonurl = $url . "/modulos/academia/form/ClientesFormServer.php" . $dadosConv;
            $jsonValorCon = file_get_contents($jsonurl);            
            $valorFinal = $RFP2['valor_mens'] + valoresNumericos2($jsonValorAcres) - valoresNumericos2($jsonValorCon);
            $dados = "?txtCartao=" . $RFP2['id_cartao'] .
                    "&txtValor=" . escreverNumero($valorFinal) .
                    "&txtPlanoItem=" . $RFP2['id_mens'] .
                    "&hostname=" . $hostname .
                    "&database=" . $database .
                    "&username=" . $username .
                    "&password=" . $password;
            $jsonurl = $url . "/util/dcc/makeItemDCC.php" . $dados;
            $json = file_get_contents($jsonurl);
            if (is_numeric($json)) {
                $totalSucesso = $totalSucesso + 1;
                $dadosPagamento = "?Trn=" . $json . "&txtValorAde=" . escreverNumero($jsonValorAde) .
                    "&hostname=" . $hostname . "&database=" . $database . "&username=" . $username . "&password=" . $password;
                $jsonUrlPg = $url . "/Modulos/Sivis/Dcc_Transacao_Pagamento.php" . $dadosPagamento;
                $jsonreult = file_get_contents($jsonUrlPg);
            } else {
                $totalErro = $totalErro + 1;
            }
        }
        $rows['MensagemDCC'] = '';
        $rows['TotalSucesso'] = $totalSucesso;
        $rows['TotalErro'] = $totalErro;
        $rows['TotalDcc'] = $totalDCC;
        print(json_encode($rows));
        odbc_close($con);
        exit();
    }
}

if (isset($_GET['GeraBoletoDCC'])) {
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' . ($_SERVER['HTTP_HOST'] === 'localhost' ? $_SERVER['HTTP_HOST'].'/erp'  : $_SERVER['HTTP_HOST']);
    $totalDCC = 0;
    $totalSucesso = 0;
    $totalErro = 0;
    $paginacao = 10;
    $boleto_url = "";
    $contrato = str_replace('ERP', '', strtoupper($_GET['Banco']));
    $hostname = $_GET['Servidor'];
    $database = $_GET['Banco'];
    $username = $_GET['Login'];
    $password = $_GET['Senha'];
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
    require_once(__DIR__ . '/../../Connections/funcoesAux.php');
    $dataAtualizacao = $_GET['DataAtualizacao'];    
    $queryListaBoleto = "set dateformat dmy; select * from sf_vendas_planos_mensalidade vpm
    inner join sf_vendas_planos planos on planos.id_plano  = vpm.id_plano_mens
    inner join sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = planos.favorecido
    inner join sf_produtos_parcelas pp on pp.id_parcela = vpm.id_parc_prod_mens and pp.parcela = -1
    where vpm.dt_pagamento_mens is null and fd.inativo = 0 and dt_cancelamento is null and fd.tipo = 'C'
    and vpm.id_mens not in (select id_referencia from sf_boleto)
    and (vpm.dt_inicio_mens <= dateadd(day, (select top 1 dcc_dias_boleto from sf_configuracao), '" . $dataAtualizacao . "'))
    and (select count(id_agendamento) from sf_vendas_planos_dcc_agendamento where id_plano_agendamento = planos.id_plano and dt_cancel_agendamento < '" . $dataAtualizacao . "') = 0 
    and (select count(bi.id_item) total from sf_boleto_online_itens bi inner join sf_boleto bo on bi.id_boleto = bo.id_boleto where bo.inativo = 0 and bi.id_mens = vpm.id_mens) = 0";
    $colSql = "select count(*) as total";
    $cur = odbc_exec($con, str_replace('select *', $colSql, $queryListaBoleto)) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $totalDCC = $RFP['total'];
        if ($totalDCC <= 0) {
            $rows['MensagemDCC'] = 'Nenhuma transação encontrada';
            $rows['TotalSucesso'] = 0;
            $rows['TotalErro'] = 0;
            $rows['TotalDcc'] = 0;
            print(json_encode($rows));
            exit();
        }
    }    
    $cur = odbc_exec($con, "select adm_atv_bol from sf_configuracao");
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['adm_atv_bol'] == 0) {
            $rows['MensagemDCC'] = 'Geracao automatica desativada';
            $rows['TotalSucesso'] = 0;
            $rows['TotalErro'] = 0;
            $rows['TotalDcc'] = 0;
            print(json_encode($rows));
            exit();
        }
    }    
    $rows = array();
    $_POST['isList'] = 1;
    $_POST['btnSalvar'] = 1;
    include (__DIR__ . "/../../util/dcc/makeBoleto.php");
    if ($msgReturn != "") {
        $rows['MensagemDCC'] = $msgReturn;
        $rows['TotalSucesso'] = 0;
        $rows['TotalErro'] = 0;
        $rows['TotalDcc'] = 0;
        print(json_encode($rows));
        exit();
    } else {
        /*if (($totalDCC > ($totalSivis - $totalMaxpago)) && substr($contrato, 0, 1) != "9" && $dcc_tp_operacao == 0) { //&& $tipo != "TEST"
            $rows['MensagemDCC'] = 'Saldo insuficiente';
            $rows['TotalSucesso'] = 0;
            $rows['TotalErro'] = 0;
            $rows['TotalDcc'] = 0;
            print(json_encode($rows));
            exit();
        }*/
        $jsonValorAde =  0;
        $colSql = "select top " . $paginacao . "id_mens, valor_mens, id_fornecedores_despesas, empresa, id_produto,id_plano";
        $cur2 = odbc_exec($con, str_replace('select *', $colSql, $queryListaBoleto)) or die(odbc_errormsg());
        while ($RFP2 = odbc_fetch_array($cur2)) {            
            $dadosAcres = "?&verificaAcrescimo=" . $RFP2['id_fornecedores_despesas'] .
                "&idProd=" . $RFP2['id_plano'] .
                "&hostname=" . $hostname .
                "&database=" . $database .
                "&username=" . $username .
                "&password=" . $password .
                "&valorProduto=" . $RFP2['valor_mens'];
            $jsonurl = $url . "/modulos/academia/form/ClientesFormServer.php" . $dadosAcres;
            $jsonValorAcres = file_get_contents($jsonurl);
            $dadosConv = "?&verificaConvenio=" . $RFP2['id_fornecedores_despesas'] .
                    "&idProd=" . $RFP2['id_produto'] .
                    "&idPlano=" .$RFP2['id_mens'] .
                    "&idConvProd=" . $RFP2['id_plano'] .                    
                    "&hostname=" . $hostname .
                    "&database=" . $database .
                    "&username=" . $username .
                    "&password=" . $password .
                    "&valorProduto=" . ($RFP2['valor_mens'] + valoresNumericos2($jsonValorAcres));
            $jsonurl = $url . "/modulos/academia/form/ClientesFormServer.php" . $dadosConv;
            $jsonValorCon = file_get_contents($jsonurl);
            $valorFinal = $RFP2['valor_mens'] + valoresNumericos2($jsonValorAcres) - valoresNumericos2($jsonValorCon);
            $valorReal = $RFP2['valor_mens'] + valoresNumericos2($jsonValorAcres);            
            $dados = "?txtValor=" . (is_numeric($valorFinal) ? escreverNumero($valorFinal) : $valorFinal) .
                    "&txtValorReal=" . (is_numeric($valorReal) ? escreverNumero($valorReal) : $valorReal) .
                    "&txtPlanoItem=" . $RFP2['id_mens'] .
                    "&hostname=" . $hostname .
                    "&database=" . $database .
                    "&username=" . $username .
                    "&password=" . $password;
            $jsonurl = $url . "/util/dcc/makeItemBoleto.php" . $dados;
            $json = json_decode(file_get_contents($jsonurl), 1);
            if (is_numeric($json)) {
                $totalSucesso = $totalSucesso + 1;
            } else {
                $totalErro = $totalErro + 1;
            }
        }
        $rows['MensagemDCC'] = '';
        $rows['TotalSucesso'] = $totalSucesso;
        $rows['TotalErro'] = $totalErro;
        $rows['TotalDcc'] = $totalDCC;
        print(json_encode($rows));
        odbc_close($con);
        exit();
    }
}

if (isset($_GET['GeraPIX'])) {
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' .
        ($_SERVER['HTTP_HOST'] === 'localhost' ? $_SERVER['HTTP_HOST'].'/erp'  : $_SERVER['HTTP_HOST']);
    $totalDCC = 0;
    $totalSucesso = 0;
    $totalErro = 0;
    $paginacao = 10;
    $boleto_url = "";
    $contrato = str_replace('ERP', '', strtoupper($_GET['Banco']));
    $hostname = $_GET['Servidor'];
    $database = $_GET['Banco'];
    $username = $_GET['Login'];
    $password = $_GET['Senha'];
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
    require_once(__DIR__ . '/../../Connections/funcoesAux.php');
    $dataAtualizacao = $_GET['DataAtualizacao'];
    
    $queryListaBoleto = "set dateformat dmy; select * from sf_vendas_planos_mensalidade vpm
    inner join sf_vendas_planos planos on planos.id_plano  = vpm.id_plano_mens
    inner join sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = planos.favorecido
    inner join sf_produtos_parcelas pp on pp.id_parcela = vpm.id_parc_prod_mens and pp.parcela = -2
    left join sf_boleto b on b.id_referencia = vpm.id_mens and b.tp_referencia = 'm' and b.inativo = 0
    where vpm.dt_pagamento_mens is null and (b.id_boleto is null) and fd.inativo = 0 and dt_cancelamento is null and fd.tipo = 'C'
    and (vpm.dt_inicio_mens <= dateadd(day, (select top 1 dcc_dias_boleto from sf_configuracao), '" . $dataAtualizacao . "'))
    and (select count(id_agendamento) from sf_vendas_planos_dcc_agendamento where id_plano_agendamento = planos.id_plano and dt_cancel_agendamento < '" . $dataAtualizacao . "') = 0 
    and (select count(bi.id_item) total from sf_boleto_online_itens bi inner join sf_boleto bo on bi.id_boleto = bo.id_boleto where bo.inativo = 0 and bi.id_mens = vpm.id_mens) = 0";        

    $colSql = "select count(*) as total";
    $cur = odbc_exec($con, str_replace('select *', $colSql, $queryListaBoleto)) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $totalDCC = $RFP['total'];
        if ($totalDCC <= 0) {
            $rows['MensagemDCC'] = 'Nenhuma transação encontrada';
            $rows['TotalSucesso'] = 0;
            $rows['TotalErro'] = 0;
            $rows['TotalDcc'] = 0;
            print(json_encode($rows));
            exit();
        }
    }
    $rows = array();
    $_POST['isList'] = 1;
    $_POST['btnSalvar'] = 1;
    include (__DIR__ . "/../../util/dcc/makePIX.php");
    if ($msgReturn != "") {
        $rows['MensagemDCC'] = $msgReturn;
        $rows['TotalSucesso'] = 0;
        $rows['TotalErro'] = 0;
        $rows['TotalDcc'] = 0;
        print(json_encode($rows));
        exit();
    } else {
        /*if (($totalDCC > ($totalSivis - $totalMaxpago)) && substr($contrato, 0, 1) != "9" && $dcc_tp_operacao == 0) { //&& $tipo != "TEST"
            $rows['MensagemDCC'] = 'Saldo insuficiente';
            $rows['TotalSucesso'] = 0;
            $rows['TotalErro'] = 0;
            $rows['TotalDcc'] = 0;
            print(json_encode($rows));
            exit();
        }*/
        $jsonValorAde =  0;
        $colSql = "select top " . $paginacao . "id_mens, valor_mens, id_fornecedores_despesas, empresa, id_produto,id_plano";
        $cur2 = odbc_exec($con, str_replace('select *', $colSql, $queryListaBoleto)) or die(odbc_errormsg());
        while ($RFP2 = odbc_fetch_array($cur2)) {            
            $dadosAcres = "?&verificaAcrescimo=" . $RFP2['id_fornecedores_despesas'] .
                "&idProd=" . $RFP2['id_plano'] .
                "&hostname=" . $hostname .
                "&database=" . $database .
                "&username=" . $username .
                "&password=" . $password .
                "&valorProduto=" . $RFP2['valor_mens'];
            $jsonurl = $url . "/modulos/academia/form/ClientesFormServer.php" . $dadosAcres;
            $jsonValorAcres = file_get_contents($jsonurl);
            $dadosConv = "?&verificaConvenio=" . $RFP2['id_fornecedores_despesas'] .
                    "&idProd=" . $RFP2['id_produto'] .
                    "&idPlano=" .$RFP2['id_mens'] .
                    "&idConvProd=" . $RFP2['id_plano'] .                    
                    "&hostname=" . $hostname .
                    "&database=" . $database .
                    "&username=" . $username .
                    "&password=" . $password .
                    "&valorProduto=" . ($RFP2['valor_mens'] + valoresNumericos2($jsonValorAcres));
            $jsonurl = $url . "/modulos/academia/form/ClientesFormServer.php" . $dadosConv;
            $jsonValorCon = file_get_contents($jsonurl);
            $valorFinal = $RFP2['valor_mens'] + valoresNumericos2($jsonValorAcres) - valoresNumericos2($jsonValorCon);
            $valorReal = $RFP2['valor_mens'] + valoresNumericos2($jsonValorAcres);            
            $dados = "?txtValor=" . (is_numeric($valorFinal) ? escreverNumero($valorFinal) : $valorFinal) .
                    "&txtValorReal=" . (is_numeric($valorReal) ? escreverNumero($valorReal) : $valorReal) .
                    "&txtPlanoItem=" . $RFP2['id_mens'] .
                    "&hostname=" . $hostname .
                    "&database=" . $database .
                    "&username=" . $username .
                    "&password=" . $password;
            $jsonurl = $url . "/util/dcc/makeItemPIX.php" . $dados;
            $json = json_decode(file_get_contents($jsonurl), 1);
            if (is_numeric($json)) {
                $totalSucesso = $totalSucesso + 1;
            } else {
                $totalErro = $totalErro + 1;
            }
        }
        $rows['MensagemDCC'] = '';
        $rows['TotalSucesso'] = $totalSucesso;
        $rows['TotalErro'] = $totalErro;
        $rows['TotalDcc'] = $totalDCC;
        print(json_encode($rows));
        odbc_close($con);
        exit();
    }
}

if (isset($_GET['getTotalAlunos'])) {
    $total = 0;
    $hostname = $_GET['Servidor'];
    $database = $_GET['Banco'];
    $username = $_GET['Login'];
    $password = $_GET['Senha'];
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
    require_once(__DIR__ . '/../../Connections/funcoesAux.php');
    $queryListaDCC = "select COUNT(id_fornecedores_despesas) total from sf_fornecedores_despesas WHERE id_fornecedores_despesas > 0 AND tipo = 'C';";
    $cur = odbc_exec($con, $queryListaDCC) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $total = $RFP['total'];
    }
    echo $total;
    odbc_close($con);
    exit();
}

if (isset($_GET['AtualizaStatus'])) {
    $hostname = $_GET['Servidor'];
    $database = $_GET['Banco'];
    $username = $_GET['Login'];
    $password = $_GET['Senha'];
    $sLimit = $_GET['De'];
    $sQtd = $_GET['Variacao'];
    $tipo = $_GET['tipo'];
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
    require_once(__DIR__ . '/../../Connections/funcoesAux.php');
    
    $queryListaDCC = "update sf_vendas_planos set dt_cancelamento = getdate(), usuario_canc = id_user_agendamento, obs_cancelamento = 'CANCELAMENTO AUTOMATICO'  
    from sf_vendas_planos inner join sf_vendas_planos_dcc_agendamento on id_plano_agendamento = id_plano
    where dt_cancelamento is null and dt_cancel_agendamento < getdate() and favorecido in (
    SELECT id_fornecedores_despesas FROM(SELECT ROW_NUMBER() OVER (order by id_fornecedores_despesas asc) as row, id_fornecedores_despesas 
    from sf_fornecedores_despesas WHERE id_fornecedores_despesas > 0 AND tipo = 'C' 
    ) as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . ")";
    odbc_exec($con, $queryListaDCC) or die(odbc_errormsg());
    
    $queryListaDCC = "update sf_vendas_planos set planos_status = dbo.FU_STATUS_PLANO_CLI(id_plano) where favorecido in (
    SELECT id_fornecedores_despesas FROM(SELECT ROW_NUMBER() OVER (order by id_fornecedores_despesas asc) as row,id_fornecedores_despesas 
    from sf_fornecedores_despesas WHERE id_fornecedores_despesas > 0 AND tipo = 'C' 
    ) as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . ")";
    odbc_exec($con, $queryListaDCC) or die(odbc_errormsg());
    
    $queryListaDCC = "update sf_fornecedores_despesas set fornecedores_status = dbo.FU_STATUS_CLI" . ($tipo == "0" ? "_ERP" : "") . "(id_fornecedores_despesas) where id_fornecedores_despesas in (
    SELECT id_fornecedores_despesas FROM(SELECT ROW_NUMBER() OVER (order by id_fornecedores_despesas asc) as row,id_fornecedores_despesas 
    from sf_fornecedores_despesas WHERE id_fornecedores_despesas > 0 AND tipo = 'C' 
    ) as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . ")";
    odbc_exec($con, $queryListaDCC) or die(odbc_errormsg());    
    
    echo "YES";
    odbc_close($con);
    exit();
}

if (isset($_GET['onLineDCC'])) {
    include (__DIR__ . "./../../util/util.php");
    if (isset($_GET["cc"]) && strlen($_GET["cc"]) > 0) {
        $resp = explode("|", encrypt($_GET["cc"], "VipService123", false));
        if (count($resp) > 0 && is_numeric($resp[0])) {
            $contrato = $resp[0];
        } else {
            $rows['MensagemDCC'] = 'Contrato Inválido!';
            $rows['TotalSucesso'] = 0;
            $rows['TotalErro'] = 0;
            $rows['TotalDcc'] = 0;
            if(isset($_GET['callback'])) echo $_GET['callback'] . "(" . json_encode($rows) . ")";
            else echo json_encode($rows);
            exit();
        }
    }
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' .
        ($_SERVER['HTTP_HOST'] === 'localhost' ? $_SERVER['HTTP_HOST'].'/erp'  : $_SERVER['HTTP_HOST']);
    $totalDCC = 0;
    $totalSucesso = 0;
    $totalErro = 0;
    $paginacao = 10;
    $hostname = '148.72.177.101,6000';
    $database = "erp" . $contrato;
    $username = "erp" . $contrato;
    $password = '!Password123';
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
    require_once(__DIR__ . "./../../Connections/funcoesAux.php");

    odbc_exec($con, "update sf_vendas_planos_mensalidade 
		set id_parc_prod_mens = (select top 1 id_parcela from sf_produtos_parcelas where id_produto = (
			select top 1 id_produto from sf_produtos_parcelas 
			inner join sf_produtos on sf_produtos.conta_produto = sf_produtos_parcelas.id_produto
			where id_parcela = sf_vendas_planos_mensalidade.id_parc_prod_mens
		) and parcela = 0) where id_mens = ". valoresNumericos2($_GET['IdMens']).";");

    $queryListaDCC = "set dateformat dmy;
    select * from sf_vendas_planos_mensalidade A
    inner join sf_produtos_parcelas B on (A.id_parc_prod_mens = B.id_parcela and parcela = 0)
    inner join sf_vendas_planos C on A.id_plano_mens = C.id_plano
    inner join sf_fornecedores_despesas D on C.favorecido = D.id_fornecedores_despesas
    inner join sf_produtos P on P.conta_produto = B.id_produto
    and dt_pagamento_mens is null and D.inativo = 0 and A.id_mens = " . valoresNumericos2($_GET['IdMens']);
    $colSql = "count(*) as total";
    $cur = odbc_exec($con, str_replace('*', $colSql, $queryListaDCC)) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $totalDCC = $RFP['total'];
    }
    if ($totalDCC <= 0) {
        $rows['MensagemDCC'] = 'Nenhuma transação encontrada';
        $rows['TotalSucesso'] = 0;
        $rows['TotalErro'] = 0;
        $rows['TotalDcc'] = 0;
        if(isset($_GET['callback'])) echo $_GET['callback'] . "(" . json_encode($rows) . ")";
        else echo json_encode($rows);
        exit();
    }
    $rows = array();
    $_POST['isList'] = 1;
    $_POST['btnSalvar'] = 1;
    include (__DIR__ . "./../../util/dcc/makeDCC.php");
    if ($msgReturn != "") {
        $rows['MensagemDCC'] = $msgReturn;
        $rows['TotalSucesso'] = 0;
        $rows['TotalErro'] = 0;
        $rows['TotalDcc'] = 0;
        if(isset($_GET['callback'])) echo $_GET['callback'] . "(" . json_encode($rows) . ")";
        else echo json_encode($rows);
        exit();
    } else {
        /*if (($totalDCC > ($totalSivis - $totalMaxpago)) && substr($contrato, 0, 1) != "9" && $dcc_tp_operacao == 0) { //&& $tipo != "TEST"
            $rows['MensagemDCC'] = 'Saldo insuficiente';
            $rows['TotalSucesso'] = 0;
            $rows['TotalErro'] = 0;
            $rows['TotalDcc'] = 0;
            if(isset($_GET['callback'])) echo $_GET['callback'] . "(" . json_encode($rows) . ")";
            else echo json_encode($rows);
            exit();
        }*/
        $colSql = " top " . $paginacao . " id_mens, valor_mens, id_fornecedores_despesas, empresa, id_produto, id_plano, adesao_soma,
        (SELECT TOP 1 id_mens FROM sf_vendas_planos_mensalidade where id_plano_mens = C.id_plano order by dt_inicio_mens asc) as id_prim_mens,
        isnull((SELECT TOP 1 s.preco_venda FROM sf_produtos s WHERE s.tipo = 'S' AND P.conta_produto_adesao = s.conta_produto), 0) as taxa_adesao,
        (select max(id_cartao) from sf_fornecedores_despesas_cartao where id_fornecedores_despesas = C.favorecido) id_cartao";
        $cur2 = odbc_exec($con, str_replace('*', $colSql, $queryListaDCC)) or die(odbc_errormsg());
        while ($RFP2 = odbc_fetch_array($cur2)) {
            $adesao_soma = $RFP2['adesao_soma'] == "0" && $RFP2['taxa_adesao'] > 0 && $RFP2['id_mens'] == $RFP2['id_prim_mens'];
            if ($adesao_soma) {
                $valorFinal = $RFP2['taxa_adesao'];
                $dadosAdesao = "?&verificaAdesao=" . $RFP2['id_fornecedores_despesas'] .
                    "&idProd=" . $RFP2['id_produto'] .
                    "&txtPlanoItem=" . $RFP2['id_mens'] .
                    "&hostname=" . $hostname . "&database=" . $database . "&username=" . $username . "&password=" . $password .
                    "&valorProduto=" . $RFP2['valor_mens'];
                $jsonurl = $url . "/modulos/academia/form/ClientesFormServer.php" . $dadosAdesao;
                $jsonValorAde = file_get_contents($jsonurl);
            } else {
                $dadosAcres = "?&verificaAcrescimo=" . $RFP2['id_fornecedores_despesas'] .
                    "&idProd=" . $RFP2['id_plano'] .
                    "&hostname=" . $hostname .
                    "&database=" . $database .
                    "&username=" . $username .
                    "&password=" . $password .
                    "&valorProduto=" . $RFP2['valor_mens'];
                $jsonurl = $url . "/modulos/academia/form/ClientesFormServer.php" . $dadosAcres;
                $jsonValorAcres = file_get_contents($jsonurl);
                $dadosConv = "?&verificaConvenio=" . $RFP2['id_fornecedores_despesas'] .
                        "&idProd=" . $RFP2['id_produto'] .
                        "&idPlano=" . $RFP2['id_mens'] .
                        "&idConvProd=" . $RFP2['id_plano'] .
                        "&hostname=" . $hostname . "&database=" . $database . "&username=" . $username . "&password=" . $password .
                        "&valorProduto=" . ($RFP2['valor_mens'] + valoresNumericos2($jsonValorAcres));
                $jsonurl = $url . "/modulos/academia/form/ClientesFormServer.php" . $dadosConv;
                $jsonValorCon = file_get_contents($jsonurl);

                $dadosAdesao = "?&verificaAdesao=" . $RFP2['id_fornecedores_despesas'] .
                    "&idProd=" . $RFP2['id_produto'] .
                    "&txtPlanoItem=" . $RFP2['id_mens'] .
                    "&hostname=" . $hostname . "&database=" . $database . "&username=" . $username . "&password=" . $password .
                    "&valorProduto=" . $RFP2['valor_mens'];
                $jsonurl = $url . "/modulos/academia/form/ClientesFormServer.php" . $dadosAdesao;
                $jsonValorAde = file_get_contents($jsonurl);
                $valorFinal = $RFP2['valor_mens'] + valoresNumericos2($jsonValorAde) + valoresNumericos2($jsonValorAcres) - valoresNumericos2($jsonValorCon);
            }            
            $dados = "?txtCartao=" . $RFP2['id_cartao'] .
                    "&txtValor=" . escreverNumero($valorFinal) .
                    "&txtPlanoItem=" . $RFP2['id_mens'] .
                    "&hostname=" . $hostname . "&database=" . $database . "&username=" . $username . "&password=" . $password;
            $jsonurl = $url . "/util/dcc/makeItemDCC.php" . $dados;
            $json = file_get_contents($jsonurl);            
            if (is_numeric($json)) {
                $totalSucesso = $totalSucesso + 1;
                $dadosPagamento = "?Trn=" . $json . "&txtValorAde=" . ($jsonValorAde) ."&adesaoSoma=". $adesao_soma .
                        "&hostname=" . $hostname . "&database=" . $database . "&username=" . $username . "&password=" . $password;
                $jsonUrlPg = $url . "/Modulos/Sivis/Dcc_Transacao_Pagamento.php" . $dadosPagamento;
                $jsonresult = file_get_contents($jsonUrlPg);
            } else {
                $totalErro = $totalErro + 1;
            }
        }
        $rows['MensagemDCC'] = '';
        $rows['TotalSucesso'] = $totalSucesso;
        $rows['TotalErro'] = $totalErro;
        $rows['TotalDcc'] = $totalDCC;
        //print(json_encode($rows));
        if(isset($_GET['callback'])) echo $_GET['callback'] . "(" . json_encode($rows) . ")";
        else echo json_encode($rows);
        odbc_close($con);
        exit();
    }
}

if (isset($_GET['onLineBoletoDCC'])) {
    include (__DIR__ . "./../../util/util.php");
    if (isset($_GET["cc"]) && strlen($_GET["cc"]) > 0) {
        $resp = explode("|", encrypt($_GET["cc"], "VipService123", false));
        if (count($resp) > 0 && is_numeric($resp[0])) {
            $contrato = $resp[0];
            $hostname = '148.72.177.101,6000';
            $database = "erp" . $contrato;
            $username = "erp" . $contrato;
            $password = '!Password123';    
            $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
            require_once(__DIR__ . "./../../Connections/funcoesAux.php");            
        } else {
            $rows['MensagemDCC'] = 'Contrato Inválido!';
            $rows['TotalSucesso'] = 0;
            $rows['TotalErro'] = 0;
            $rows['TotalDcc'] = 0;
            echo $_GET['callback'] . "(" . json_encode($rows) . ")";
            exit();
        }        
    } else {
        require_once(__DIR__ . "./../../Connections/configini.php");
    }
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' . ($_SERVER['HTTP_HOST'] === 'localhost' ? $_SERVER['HTTP_HOST'].'/erp'  : $_SERVER['HTTP_HOST']);
    $totalDCC = 0;
    $totalSucesso = 0;
    $totalErro = 0;
    $paginacao = 10;
    $boleto_url = "";
    $boleto_id = 0;

    $queryListaBoleto  = "set dateformat dmy;
    select * from sf_vendas_planos_mensalidade A
    inner join sf_produtos_parcelas B on (A.id_parc_prod_mens = B.id_parcela)
    inner join sf_vendas_planos C on A.id_plano_mens = C.id_plano
    inner join sf_fornecedores_despesas D on C.favorecido = D.id_fornecedores_despesas
    inner join sf_produtos P on P.conta_produto = B.id_produto
    and dt_pagamento_mens is null and D.inativo = 0 and A.id_mens = " . valoresNumericos2($_GET['IdMens']);
    $colSql = "count(*) as total";
    $cur = odbc_exec($con, str_replace('*', $colSql, $queryListaBoleto)) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $totalDCC = $RFP['total'];
    }
    if ($totalDCC <= 0) {
        $rows['MensagemDCC'] = 'Nenhuma transação encontrada';
        $rows['TotalSucesso'] = 0;
        $rows['TotalErro'] = 0;
        $rows['TotalDcc'] = 0;
        if(isset($_GET['callback'])) echo $_GET['callback'] . "(" . json_encode($rows) . ")";
        else echo json_encode($rows);
        exit();
    }
    $rows = array();
    $_POST['isList'] = 1;
    $_POST['btnSalvar'] = 1;
    include (__DIR__ . "./../../util/dcc/makeBoleto.php");
    if ($msgReturn != "") {
        $rows['MensagemDCC'] = $msgReturn;
        $rows['TotalSucesso'] = 0;
        $rows['TotalErro'] = 0;
        $rows['TotalDcc'] = 0;
        if(isset($_GET['callback'])) echo $_GET['callback'] . "(" . json_encode($rows) . ")";
        else echo json_encode($rows);
        exit();
    } else { 
        /*if (($totalDCC > ($totalSivis - $totalMaxpago)) && substr($contrato, 0, 1) != "9" && $dcc_tp_operacao == 0) { //&& $tipo != "TEST"
            $rows['MensagemDCC'] = 'Saldo insuficiente';
            $rows['TotalSucesso'] = 0;
            $rows['TotalErro'] = 0;
            $rows['TotalDcc'] = 0;
            echo $_GET['callback'] . "(" . json_encode($rows) . ")";
            exit();
        }*/
        $colSql = "select top " . $paginacao . "id_mens, valor_mens, id_fornecedores_despesas, empresa, id_produto,id_plano, adesao_soma,
        (SELECT TOP 1 id_mens FROM sf_vendas_planos_mensalidade where id_plano_mens = C.id_plano order by dt_inicio_mens asc) as id_prim_mens,
         isnull((SELECT TOP 1 s.preco_venda FROM sf_produtos s WHERE s.tipo = 'S' AND P.conta_produto_adesao = s.conta_produto), 0) as taxa_adesao";
        $cur2 = odbc_exec($con, str_replace('select *', $colSql, $queryListaBoleto)) or die(odbc_errormsg());
        while ($RFP2 = odbc_fetch_array($cur2)) {
            if ($RFP2['adesao_soma'] == "0" && $RFP2['taxa_adesao'] > 0 && $RFP2['id_mens'] == $RFP2['id_prim_mens']) {
                $dadosAdesao = "?&verificaAdesao=" . $RFP2['id_fornecedores_despesas'] .
                "&idProd=" . $RFP2['id_produto'] .
                "&txtPlanoItem=" . $RFP2['id_mens'] .
                "&hostname=" . $hostname . "&database=" . $database . "&username=" . $username . "&password=" . $password .
                "&valorProduto=" . $RFP2['valor_mens'];
                $jsonurl = $url . "/modulos/academia/form/ClientesFormServer.php" . $dadosAdesao;
                $valorFinal = file_get_contents($jsonurl);
                $valorReal = $RFP2['taxa_adesao'];                
            } else {
                $dadosAcres = "?&verificaAcrescimo=" . $RFP2['id_fornecedores_despesas'] .
                    "&idProd=" . $RFP2['id_plano'] .
                    "&hostname=" . $hostname .
                    "&database=" . $database .
                    "&username=" . $username .
                    "&password=" . $password .
                    "&valorProduto=" . $RFP2['valor_mens'];
                $jsonurl = $url . "/modulos/academia/form/ClientesFormServer.php" . $dadosAcres;
                $jsonValorAcres = file_get_contents($jsonurl);
                $dadosConv = "?&verificaConvenio=" . $RFP2['id_fornecedores_despesas'] .
                    "&idProd=" . $RFP2['id_produto'] .
                    "&idPlano=" .$RFP2['id_mens'] .
                    "&idConvProd=" . $RFP2['id_plano'] .                        
                    "&hostname=" . $hostname .
                    "&database=" . $database .
                    "&username=" . $username .
                    "&password=" . $password .
                    "&valorProduto=" . ($RFP2['valor_mens'] + valoresNumericos2($jsonValorAcres));
                $jsonurl = $url . "/modulos/academia/form/ClientesFormServer.php" . $dadosConv;
                $jsonValorCon = file_get_contents($jsonurl);
                $jsonValorAde = "0";
                if ($RFP2['id_mens'] === $RFP2['id_prim_mens']) {
                    $dadosAdesao = "?&verificaAdesao=" . $RFP2['id_fornecedores_despesas'] .
                        "&idProd=" . $RFP2['id_produto'] .
                        "&txtPlanoItem=" . $RFP2['id_mens'] .
                        "&hostname=" . $hostname . "&database=" . $database . "&username=" . $username . "&password=" . $password .
                        "&valorProduto=" . $RFP2['valor_mens'];
                    $jsonurl = $url . "/modulos/academia/form/ClientesFormServer.php" . $dadosAdesao;
                    $jsonValorAde = file_get_contents($jsonurl);
                }
                $valorFinal = $RFP2['valor_mens'] + valoresNumericos2($jsonValorAcres) + valoresNumericos2($jsonValorAde) - valoresNumericos2($jsonValorCon);
                $valorReal = $RFP2['valor_mens'] + valoresNumericos2($jsonValorAcres) + valoresNumericos2($jsonValorAde);                
            }
            $dados = "?txtValor=" .(is_numeric($valorFinal) ? escreverNumero($valorFinal) : $valorFinal).
                "&txtValorReal=" . (is_numeric($valorReal) ? escreverNumero($valorReal) : $valorReal) .
                "&txtPlanoItem=" . $RFP2['id_mens'] .
                "&hostname=" . $hostname .
                "&database=" . $database .
                "&username=" . $username .
                "&password=" . $password;            
            if (isset($_SESSION["login_usuario"])) {
                $dados .= "&login_usuario=" . $_SESSION["login_usuario"];
            }            
            $jsonurl = $url . "/util/dcc/makeItemBoleto.php" . $dados;
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );  
            $json = file_get_contents($jsonurl, false, stream_context_create($arrContextOptions));
            //echo $json; exit;
            if (is_numeric($json)) {
                $totalSucesso = $totalSucesso + 1;
                $boleto_id  = $json;
                $boleto_url = $url ."/Boletos/Boleto.php?id=M-".$RFP2['id_mens']."&crt=".$contrato;
            } else {
                $totalErro = $totalErro + 1;
            }
        }
        $rows['MensagemDCC'] = '';
        $rows['TotalSucesso'] = $totalSucesso;
        $rows['TotalErro'] = $totalErro;
        $rows['TotalDcc'] = $totalDCC;
        $rows['boleto'] = [
            'id'  => $boleto_id,
            'url' =>  $boleto_url
        ];
        //print(json_encode($rows));
        if(isset($_GET['callback'])) echo $_GET['callback'] . "(" . json_encode($rows) . ")";
        else echo json_encode($rows);
        odbc_close($con);
        exit();
    }
}

if (isset($_GET['onLinePIX'])) {
    include (__DIR__ . "./../../util/util.php");
    if (isset($_GET["cc"]) && strlen($_GET["cc"]) > 0) {
        $resp = explode("|", encrypt($_GET["cc"], "VipService123", false));
        if (count($resp) > 0 && is_numeric($resp[0])) {
            $contrato = $resp[0];
            $hostname = '148.72.177.101,6000';
            $database = "erp" . $contrato;
            $username = "erp" . $contrato;
            $password = '!Password123';    
            $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
            require_once(__DIR__ . "./../../Connections/funcoesAux.php");            
        } else {
            $rows['MensagemDCC'] = 'Contrato Inválido!';
            $rows['TotalSucesso'] = 0;
            $rows['TotalErro'] = 0;
            $rows['TotalDcc'] = 0;
            echo $_GET['callback'] . "(" . json_encode($rows) . ")";
            exit();
        }        
    } else {
        require_once(__DIR__ . "./../../Connections/configini.php");
    }
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' . ($_SERVER['HTTP_HOST'] === 'localhost' ? $_SERVER['HTTP_HOST'].'/erp'  : $_SERVER['HTTP_HOST']);
    $totalDCC = 0;
    $totalSucesso = 0;
    $totalErro = 0;
    $paginacao = 10;
    $boleto_url = "";
    $boleto_id = 0;

    $queryListaBoleto  = "set dateformat dmy;
    select * from sf_vendas_planos_mensalidade A
    inner join sf_produtos_parcelas B on (A.id_parc_prod_mens = B.id_parcela)
    inner join sf_vendas_planos C on A.id_plano_mens = C.id_plano
    inner join sf_fornecedores_despesas D on C.favorecido = D.id_fornecedores_despesas
    inner join sf_produtos P on P.conta_produto = B.id_produto
    and dt_pagamento_mens is null and D.inativo = 0 and A.id_mens = " . valoresNumericos2($_GET['IdMens']);
    $colSql = "count(*) as total";
    $cur = odbc_exec($con, str_replace('*', $colSql, $queryListaBoleto)) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $totalDCC = $RFP['total'];
    }
    if ($totalDCC <= 0) {
        $rows['MensagemDCC'] = 'Nenhuma transação encontrada';
        $rows['TotalSucesso'] = 0;
        $rows['TotalErro'] = 0;
        $rows['TotalDcc'] = 0;
        if(isset($_GET['callback'])) echo $_GET['callback'] . "(" . json_encode($rows) . ")";
        else echo json_encode($rows);
        exit();
    }
    $rows = array();
    $_POST['isList'] = 1;
    $_POST['btnSalvar'] = 1;
    include (__DIR__ . "./../../util/dcc/makePIX.php");
    if ($msgReturn != "") {
        $rows['MensagemDCC'] = $msgReturn;
        $rows['TotalSucesso'] = 0;
        $rows['TotalErro'] = 0;
        $rows['TotalDcc'] = 0;
        if(isset($_GET['callback'])) echo $_GET['callback'] . "(" . json_encode($rows) . ")";
        else echo json_encode($rows);
        exit();
    } else { 
        /*if (($totalDCC > ($totalSivis - $totalMaxpago)) && substr($contrato, 0, 1) != "9" && $dcc_tp_operacao == 0) { //&& $tipo != "TEST"
            $rows['MensagemDCC'] = 'Saldo insuficiente';
            $rows['TotalSucesso'] = 0;
            $rows['TotalErro'] = 0;
            $rows['TotalDcc'] = 0;
            echo $_GET['callback'] . "(" . json_encode($rows) . ")";
            exit();
        }*/
        $colSql = "select top " . $paginacao . "id_mens, valor_mens, id_fornecedores_despesas, empresa, id_produto,id_plano, adesao_soma,
        (SELECT TOP 1 id_mens FROM sf_vendas_planos_mensalidade where id_plano_mens = C.id_plano order by dt_inicio_mens asc) as id_prim_mens,
         isnull((SELECT TOP 1 s.preco_venda FROM sf_produtos s WHERE s.tipo = 'S' AND P.conta_produto_adesao = s.conta_produto), 0) as taxa_adesao";
        $cur2 = odbc_exec($con, str_replace('select *', $colSql, $queryListaBoleto)) or die(odbc_errormsg());
        while ($RFP2 = odbc_fetch_array($cur2)) {
            if ($RFP2['adesao_soma'] == "0" && $RFP2['taxa_adesao'] > 0 && $RFP2['id_mens'] == $RFP2['id_prim_mens']) {
                $dadosAdesao = "?&verificaAdesao=" . $RFP2['id_fornecedores_despesas'] .
                "&idProd=" . $RFP2['id_produto'] .
                "&txtPlanoItem=" . $RFP2['id_mens'] .
                "&hostname=" . $hostname . "&database=" . $database . "&username=" . $username . "&password=" . $password .
                "&valorProduto=" . $RFP2['valor_mens'];
                $jsonurl = $url . "/modulos/academia/form/ClientesFormServer.php" . $dadosAdesao;
                $valorFinal = file_get_contents($jsonurl);
                $valorReal = $RFP2['taxa_adesao'];                        
            } else {
                $dadosAcres = "?&verificaAcrescimo=" . $RFP2['id_fornecedores_despesas'] .
                    "&idProd=" . $RFP2['id_plano'] .
                    "&hostname=" . $hostname .
                    "&database=" . $database .
                    "&username=" . $username .
                    "&password=" . $password .
                    "&valorProduto=" . $RFP2['valor_mens'];
                $jsonurl = $url . "/modulos/academia/form/ClientesFormServer.php" . $dadosAcres;
                $jsonValorAcres = file_get_contents($jsonurl);
                $dadosConv = "?&verificaConvenio=" . $RFP2['id_fornecedores_despesas'] .
                    "&idProd=" . $RFP2['id_produto'] .
                    "&idPlano=" .$RFP2['id_mens'] .
                    "&idConvProd=" . $RFP2['id_plano'] .                        
                    "&hostname=" . $hostname .
                    "&database=" . $database .
                    "&username=" . $username .
                    "&password=" . $password .
                    "&valorProduto=" . ($RFP2['valor_mens'] + valoresNumericos2($jsonValorAcres));
                $jsonurl = $url . "/modulos/academia/form/ClientesFormServer.php" . $dadosConv;
                $jsonValorCon = file_get_contents($jsonurl);
                $jsonValorAde = "0";
                if ($RFP2['id_mens'] === $RFP2['id_prim_mens']) {
                    $dadosAdesao = "?&verificaAdesao=" . $RFP2['id_fornecedores_despesas'] .
                        "&idProd=" . $RFP2['id_produto'] .
                        "&txtPlanoItem=" . $RFP2['id_mens'] .
                        "&hostname=" . $hostname . "&database=" . $database . "&username=" . $username . "&password=" . $password .
                        "&valorProduto=" . $RFP2['valor_mens'];
                    $jsonurl = $url . "/modulos/academia/form/ClientesFormServer.php" . $dadosAdesao;
                    $jsonValorAde = file_get_contents($jsonurl);
                }
                $valorFinal = $RFP2['valor_mens'] + valoresNumericos2($jsonValorAcres) + valoresNumericos2($jsonValorAde) - valoresNumericos2($jsonValorCon);
                $valorReal = $RFP2['valor_mens'] + valoresNumericos2($jsonValorAcres) + valoresNumericos2($jsonValorAde);                
            }
            $dados = "?txtValor=" .(is_numeric($valorFinal) ? escreverNumero($valorFinal) : $valorFinal).
                "&txtValorReal=" . (is_numeric($valorReal) ? escreverNumero($valorReal) : $valorReal) .                    
                "&txtPlanoItem=" . $RFP2['id_mens'] .
                "&hostname=" . $hostname .
                "&database=" . $database .
                "&username=" . $username .
                "&password=" . $password;
            $jsonurl = $url . "/util/dcc/makeItemPIX.php" . $dados;
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );  
            $json = file_get_contents($jsonurl, false, stream_context_create($arrContextOptions));
            //echo $json; exit;
            if (is_numeric($json)) {
                $totalSucesso = $totalSucesso + 1;
                $boleto_id  = $json;
                $boleto_url = $url ."/Boletos/Boleto.php?id=M-".$RFP2['id_mens']."&crt=".$contrato;
            } else {
                $totalErro = $totalErro + 1;
            }
        }
        $rows['MensagemDCC'] = '';
        $rows['TotalSucesso'] = $totalSucesso;
        $rows['TotalErro'] = $totalErro;
        $rows['TotalDcc'] = $totalDCC;
        $rows['boleto'] = [
            'id'  => $boleto_id,
            'url' =>  $boleto_url
        ];
        //print(json_encode($rows));
        if(isset($_GET['callback'])) echo $_GET['callback'] . "(" . json_encode($rows) . ")";
        else echo json_encode($rows);
        odbc_close($con);
        exit();
    }
}

if (isset($_GET['FechamentoMes'])) {
    $hostname = $_GET['Servidor'];
    $database = $_GET['Banco'];
    $username = $_GET['Login'];
    $password = $_GET['Senha'];
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
    require_once(__DIR__ . '/../../Connections/funcoesAux.php');
    require_once(__DIR__ . '/../../Modulos/BI/ajax/Geral_query.php');

    $v1 = 0;
    $t1 = 0;
    $a1 = 0;

    $clientes_ativos = 0;
    $clientes_pagantes = 0;
    $indice_renovacao = 0;
    $indice_evasao = 0;
    $tiket_medio = 0.00;
    $custo_medio = 0.00;
    $contratos_cancelados = 0;
    $clientes_novos = 0;
    $clientes_retornos = 0;
    $clientes_renovados = 0;
    $valor_mensalidade = 0.00;
    $valor_servico = 0.00;
    $valor_dcc = 0;
    $valor_mensal = 0;
    $valor_bimestral = 0;
    $valor_trimestral = 0;
    $valor_quadrimestral = 0;
    $valor_semestral = 0;
    $valor_anual = 0;
    $valor_outros = 0;
    $veiculos_ativos = 0;

    $cur = odbc_exec($con, $sQueryAtivos);
    while ($RFP = odbc_fetch_array($cur)) {
        $clientes_ativos = $RFP['ativos'];
        $clientes_pagantes = $RFP['pagantes'];
    }

    $cur = odbc_exec($con, $sQueryRenovacoes);
    while ($RFP = odbc_fetch_array($cur)) {
        $indice_renovacao = $RFP['Renovacoes'];
    }

    $cur = odbc_exec($con, $sQueryEvasao);
    while ($RFP = odbc_fetch_array($cur)) {
        $indice_evasao = $RFP['nrenovar'];
    }

    $cur = odbc_exec($con, $sQueryTM);
    while ($RFP = odbc_fetch_array($cur)) {
        $v1 = $RFP['total_pago'];
        $a1 = $RFP['alunos'];
        $tiket_medio = escreverNumero($a1 > 0 ? ($v1 / $a1) : 0);
    }

    $cur = odbc_exec($con, $sQueryCM);
    while ($RFP = odbc_fetch_array($cur)) {
        $t1 = $RFP['valor_pago_manual'];
        $custo_medio = escreverNumero($a1 > 0 ? ($t1 / $a1) : 0);
    }

    $cur = odbc_exec($con, $sQueryCancelamento);
    while ($RFP = odbc_fetch_array($cur)) {
        $contratos_cancelados = $RFP['total'];
    }
   
    $cur = odbc_exec($con, $sQueryRenovacoes);
    while ($RFP = odbc_fetch_array($cur)) {
        $clientes_novos = $RFP['Novos'];
        $clientes_retornos = $RFP['Retornos'];
        $clientes_renovados = $RFP['Renovacoes'];
    }
    
    $cur = odbc_exec($con, $sQueryFaturamento);
    while ($RFP = odbc_fetch_array($cur)) {
        $valor_mensalidade = escreverNumero($RFP['valor_contrato']);
        $valor_servico = escreverNumero($RFP['valor_servico']);
    }

    $cur = odbc_exec($con, $sQueryParcelas);
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['parcela'] == 0) {
            $valor_dcc = $RFP['total'];
        } else if ($RFP['parcela'] == 1) {
            $valor_mensal = $RFP['total'];
        } else if ($RFP['parcela'] == 2) {
            $valor_bimestral = $RFP['total'];
        } else if ($RFP['parcela'] == 3) {
            $valor_trimestral = $RFP['total'];
        } else if ($RFP['parcela'] == 4) {
            $valor_quadrimestral = $RFP['total'];
        } else if ($RFP['parcela'] == 6) {
            $valor_semestral = $RFP['total'];
        } else if ($RFP['parcela'] == 12) {
            $valor_anual = $RFP['total'];
        } else {
            $valor_outros = $valor_outros + $RFP['total'];
        }
    }
    
    $cur = odbc_exec($con, $sQueryVeiculo);
    while ($RFP = odbc_fetch_array($cur)) {
        $veiculos_ativos = $RFP['total'];
    }    

    $query = "set dateformat dmy; insert into sf_fechamento_mes(data,
    clientes_ativos,clientes_pagantes,indice_renovacao,indice_evasao,
    tiket_medio,custo_medio,contratos_cancelados,
    clientes_novos,clientes_retornos,clientes_renovados,
    valor_mensalidade, valor_servico,
    valor_dcc, valor_mensal, valor_bimestral, valor_trimestral, valor_quadrimestral, 
    valor_semestral, valor_anual, valor_outros, veiculos_ativos) values (" .
            valoresData2(getData("T")) . "," .
            valoresNumericos2($clientes_ativos) . "," .
            valoresNumericos2($clientes_pagantes) . "," .
            valoresNumericos2($indice_renovacao) . "," .
            valoresNumericos2($indice_evasao) . "," .
            valoresNumericos2($tiket_medio) . "," .
            valoresNumericos2($custo_medio) . "," .
            valoresNumericos2($contratos_cancelados) . "," .
            valoresNumericos2($clientes_novos) . "," .
            valoresNumericos2($clientes_retornos) . "," .
            valoresNumericos2($clientes_renovados) . "," .
            valoresNumericos2($valor_mensalidade) . "," .
            valoresNumericos2($valor_servico) . "," .
            valoresNumericos2($valor_dcc) . "," .
            valoresNumericos2($valor_mensal) . "," .
            valoresNumericos2($valor_bimestral) . "," .
            valoresNumericos2($valor_trimestral) . "," .
            valoresNumericos2($valor_quadrimestral) . "," .
            valoresNumericos2($valor_semestral) . "," .
            valoresNumericos2($valor_anual) . "," .
            valoresNumericos2($valor_outros) . "," .
            valoresNumericos2($veiculos_ativos) . ")";
    odbc_exec($con, $query) or die(odbc_errormsg());
    $rows['qtdSucesso'] = 1;
    $rows['qtdErro'] = 0;
    $rows['msgErro'] = "";
    print(json_encode($rows));
    odbc_close($con);
    exit();
}

if (isset($_GET['ReemissaoWebhook'])) {
    include __DIR__.'/../../util/dcc/MundiPagg/webhooks_process.php';
}
