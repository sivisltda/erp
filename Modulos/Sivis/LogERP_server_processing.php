<?php

if (!isset($_GET['pdf'])) {
    include './../../Connections/configini.php';
}

$aColumns = array('data_acao', 'empresa_login', 'razao_social', 'sys_login', 'ip', 'acao', 'nome_fantasia');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$imprimir = 0;
$sLimit = 20;
$sOrder = " ORDER BY data_acao desc ";

if (is_numeric($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
    $sWhereX = $sWhereX . " and data_acao between " . valoresDataHora2($DateBegin, "00:00:00") . " AND " . valoresDataHora2($DateEnd, "23:59:59");
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0 && intval($_GET['iSortCol_' . $i]) < 9) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . "
                                    " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY data_acao desc";
    }
}

if ($_GET['Search'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['Search']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
        $sWhere .= $aColumns[$i] . " AND LIKE '%" . utf8_decode($_GET['sSearch_' . $i]) . "%' ";
    }
}

$sQuery = "SET DATEFORMAT DMY;SELECT COUNT(*) total from ca_acessos_logs l 
left join ca_contratos c on empresa_login = c.numero_contrato
left join sf_fornecedores_despesas f on f.id_fornecedores_despesas = c.id_cliente
WHERE id_log > 0 " . $sWhereX . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SET DATEFORMAT DMY;SELECT COUNT(*) total from ca_acessos_logs l 
left join ca_contratos c on empresa_login = c.numero_contrato
left join sf_fornecedores_despesas f on f.id_fornecedores_despesas = c.id_cliente
WHERE id_log > 0 " . $sWhereX;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SET DATEFORMAT DMY;SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,data_acao,empresa_login,razao_social,sys_login,ip,acao,nome_fantasia 
from ca_acessos_logs l left join ca_contratos c on empresa_login = c.numero_contrato
left join sf_fornecedores_despesas f on f.id_fornecedores_despesas = c.id_cliente
WHERE id_log > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder;
$cur = odbc_exec($con, $sQuery1);

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $identificacao = utf8_encode($aRow[$aColumns[2]]);
    if (utf8_encode($aRow[$aColumns[6]]) != '') {
        $identificacao = utf8_encode($aRow[$aColumns[2]]) . ' (' . utf8_encode($aRow[$aColumns[6]]) . ')';
    }
    $row[] = "<center><div id='formPQ' title='" . escreverDataHora($aRow[$aColumns[0]]) . "'>" . escreverDataHora($aRow[$aColumns[0]]) . "</div></center>";
    $row[] = "<center><div id='formPQ' title='" . str_pad(utf8_encode($aRow[$aColumns[1]]), 3, "0", STR_PAD_LEFT) . "'>" . str_pad(utf8_encode($aRow[$aColumns[1]]), 3, "0", STR_PAD_LEFT) . "</div></center>";
    $row[] = "<div id='formPQ' title='" . $identificacao . "'>" . $identificacao . "</div>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[3]]) . "'>" . utf8_encode($aRow[$aColumns[3]]) . "</div></center>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[4]]) . "'>" . utf8_encode($aRow[$aColumns[4]]) . "</div></center>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[5]]) . "'>" . utf8_encode($aRow[$aColumns[5]]) . "</div></center>";
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
