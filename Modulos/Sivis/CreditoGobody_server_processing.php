<?php

include './../../Connections/configini.php';
$aColumns = array('cod_acad', 'razao_social', 'endereco', 'bairro', 'cidade', 'estado', 'telefone', 'cod_plugue', 'nome_fantasia', 'cep', 'id_fornecedores_despesas');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY cod_acad asc ";

if ($_GET['es'] != "") {
    $sWhereX = $sWhereX . " and estado = " . $_GET['es'];
}

if ($_GET['ci'] != "") {
    $sWhereX = $sWhereX . " and cidade = " . $_GET['ci'];
}

if ($_GET['id'] != "") {
    $sWhereX = $sWhereX . " and id_fornecedores_despesas in (select fornecedor_despesa from dbo.sf_lancamento_movimento where sf_lancamento_movimento.status = 'Aprovado' and inativo = 0 and conta_movimento = " . $_GET['id'] . ")";
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0 && intval($_GET['iSortCol_' . $i]) < 9) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . "
                                    " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY cod_acad asc";
    }
}

if ($_GET['sSearch'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['sSearch']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
        $sWhere .= $aColumns[$i] . " AND LIKE '%" . utf8_decode($_GET['sSearch_' . $i]) . "%' ";
    }
}

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,cod_acad, razao_social, endereco + ' n:' + numero endereco, bairro, cidade_nome cidade, estado_sigla estado,cep, telefone, email, nome_fantasia, descricao_grupo
            ,isnull((select COUNT(l.id_lancamento_movimento) from sf_lancamento_movimento l inner join sf_lancamento_movimento_parcelas p on l.id_lancamento_movimento = p.id_lancamento_movimento inner join sf_contas_movimento c on c.id_contas_movimento = l.conta_movimento where l.status = 'Aprovado' and l.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas and p.data_pagamento is null and dateadd(day,dias_tolerancia,data_vencimento) < GETDATE() and p.inativo = 0),0) as num_parc
            ,isnull((select COUNT(l.id_lancamento_movimento) from sf_lancamento_movimento l inner join sf_lancamento_movimento_parcelas p on l.id_lancamento_movimento = p.id_lancamento_movimento inner join sf_contas_movimento c on c.id_contas_movimento = l.conta_movimento where l.status = 'Aprovado' and l.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas and data_vencimento > GETDATE() and p.inativo = 0),0) as qtd_parc
            ,id_fornecedores_despesas,opc_cli,cod_plugue,bloqueado FROM ca_clientes inner join sf_fornecedores_despesas on id_fornecedores_despesas = sf_cliente left join sf_tipo_documento on forma_pagamento = id_tipo_documento left join tb_estados on estado = estado_codigo 
            left join tb_cidades on cidade = cidade_codigo left join sf_grupo_cliente on grupo_pessoa = id_grupo
            WHERE id_fornecedores_despesas > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . "" . $sOrder;

$cur = odbc_exec($con, $sQuery1);

$sQuery = "SELECT COUNT(*) total FROM ca_clientes inner join sf_fornecedores_despesas on id_fornecedores_despesas = sf_cliente WHERE id_fornecedores_despesas > 0 $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total FROM ca_clientes inner join sf_fornecedores_despesas on id_fornecedores_despesas = sf_cliente WHERE id_fornecedores_despesas > 0 $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $identificacao = utf8_encode($aRow[$aColumns[1]]);
    if (utf8_encode($aRow[$aColumns[8]]) != '') {
        $identificacao = utf8_encode($aRow[$aColumns[1]]) . ' (' . utf8_encode($aRow[$aColumns[8]]) . ')';
    }
    if ($aRow['bloqueado'] == 1) {
        $status = "Bloqueado";
        $bgstatus = "style='color:red;' ";
    } else {
        if ($aRow['num_parc'] > 0) {
            $status = "Pendência Financeira";
            $bgstatus = "style='color:red;' ";
        } else {
            if ($aRow['qtd_parc'] > 0) {
                $status = " Ativo";
                $bgstatus = "";
            } else {
                $status = " Sem contrato";
                $bgstatus = "";
            }
        }
    }
    $row[] = "<center><div id='formPQ' title='" . str_pad(utf8_encode($aRow[$aColumns[0]]), 3, "0", STR_PAD_LEFT) . "'>" . str_pad(utf8_encode($aRow[$aColumns[0]]), 3, "0", STR_PAD_LEFT) . "</div></center>";
    $row[] = "<a href='#' onClick='AbrirBox(" . $aRow[$aColumns[0]] . ")'><div id='formPQ' title='" . $identificacao . "'>" . $identificacao . "</div></a>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[6]]) . "'>" . utf8_encode($aRow[$aColumns[6]]) . "</div></center>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[6]]) . "'>" . utf8_encode($aRow[$aColumns[6]]) . "</div></center>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[7]]) . "'>" . utf8_encode($aRow[$aColumns[7]]) . "</div></center>";
    $row[] = "<center><div id='formPQ' title='" . str_replace("-", "", utf8_encode($aRow['opc_cli'])) . "'>" . str_replace("-", "", utf8_encode($aRow['opc_cli'])) . "</div></center>";
    $row[] = "<center><div id='formPQ' " . $bgstatus . " title='" . $status . "'>" . $status . "</div></center>";
    $output['aaData'][] = $row;
}
echo json_encode($output);
odbc_close($con);
