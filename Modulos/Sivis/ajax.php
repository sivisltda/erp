<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include './../../Connections/configini.php';

$texto = $_REQUEST['q'];
$tipo = str_replace("\\", "", $_REQUEST['t']);
$sql = "select cod_acad,razao_social,nome_fantasia,versao,cod_plugue from sf_fornecedores_despesas f
        inner join ca_clientes c on f.id_fornecedores_despesas = c.sf_cliente
        where id_fornecedores_despesas > 0 and tipo in ('" . $tipo . "') and (razao_social like '%" . str_replace("'", "", $texto) . "%' or nome_fantasia like '%" . str_replace("'", "", $texto) . "%') ORDER BY razao_social";
$cur = odbc_exec($con, $sql) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $descricao = utf8_encode($RFP['razao_social']);
    if ($RFP['nome_fantasia'] != "") {
        $descricao = $descricao . " (" . utf8_encode($RFP['nome_fantasia']) . ")";
    }
    $row['value'] = $descricao;
    $row['id'] = str_pad(utf8_encode($RFP['cod_acad']), 3, "0", STR_PAD_LEFT);
    $item = explode(".", $RFP['versao']);
    if (count($item) == 3) {
        $row['versao1'] = $item[0];
        $row['versao2'] = $item[1];
        $row['versao3'] = $item[2];
    }
    $row['cod_plugue'] = utf8_encode($RFP['cod_plugue']);
    $row_set[] = $row;
}

echo json_encode($row_set);
odbc_close($con);
