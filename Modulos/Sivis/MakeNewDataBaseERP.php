<?php

$file = "./../../";

function recurse_copy($src, $dst) {
    $dir = opendir($src);
    @mkdir($dst);
    while (false !== ( $file = readdir($dir))) {
        if (($file != '.' ) && ( $file != '..')) {
            if (is_dir($src . '/' . $file)) {
                recurse_copy($src . '/' . $file, $dst . '/' . $file);
            } else {
                copy($src . '/' . $file, $dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}

if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    $contrato = str_pad($_GET['id'], 3, "0", STR_PAD_LEFT);
    if (is_dir($file . "Pessoas/000") && !is_dir($file . "Pessoas/" . $contrato)) {
        recurse_copy($file . "Pessoas/000", $file . "Pessoas/" . $contrato);
        recurse_copy($file . "Produtos/000", $file . "Produtos/" . $contrato);
        echo $_GET['callback'] . "('YES')";
    }
}
