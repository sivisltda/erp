<?php

if (!isset($_GET['Trn'])) {
    echo "Erro Parâmetros!";
    exit;
} else {
    $transacao = $_GET["Trn"];
}

$loginUsuario = 'SISTEMA';
$hostname = $_GET['hostname'];
$database = $_GET['database'];
$username = $_GET['username'];
$password = $_GET['password'];
$contrato = $_GET['contrato'];
$valorPago = $_GET['valorpago'];
$dataPagamento = trim($_GET['datapagto']);
$status = $_GET['status'];

$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' .
        ($_SERVER['HTTP_HOST'] === 'localhost' ? $_SERVER['HTTP_HOST'] . '/erp' : $_SERVER['HTTP_HOST']);

$con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
require_once(__DIR__ . '/../../Connections/funcoesAux.php');
require_once(__DIR__ . '/../../util/util.php');

if (isset($_SESSION["login_usuario"]) && strlen($_SESSION["login_usuario"]) > 0) {
    $loginUsuario = strtoupper($_SESSION["login_usuario"]);
} elseif (isset($_GET["login_usuario"]) && strlen($_GET["login_usuario"]) > 0) {
    $loginUsuario = strtoupper($_GET["login_usuario"]);
}

$sql = "SELECT top 1 vpm.id_mens, vpm.dt_inicio_mens, vpm.dt_fim_mens, vp.id_prim_mens, vp.id_plano, b.id_boleto, b.id_venda as id_venda_boleto,
ACA_DIA_VENCIMENTO, ACA_TIPO_VENCIMENTO, ACA_EX_VEN_PR, fd.id_fornecedores_despesas, dbo.VALOR_REAL_MENSALIDADE(vpm.id_mens) as valor_mens,
(isnull(dependentes.adesao_dependente,0) + (case when vpm.valor_mens > 0 and id_prim_mens = vpm.id_mens then vp.taxa_adesao else 0 end)) as valor_adesao,
taxa_adesao, dependentes.*, vp.data_prazo, vp.prorata, vi.id_venda, fd.tipo as tipo_pessoa, adesao_soma, planos_cancelados,
case when ACA_TIPO_VENCIMENTO = 2 and id_prim_mens = vpm.id_mens then isnull((select top 1 dia from sf_configuracao_vencimentos where dia >= (select top 1 case when dt_fim_mens = EOMONTH(dt_fim_mens) then 1 
else datepart(DAY, dt_fim_mens) end as dia)), datepart(DAY, dateadd(day, 1, dt_fim_mens)))
when ACA_TIPO_VENCIMENTO = 1 then ACA_DIA_VENCIMENTO 
else datepart(DAY, vpm.dt_inicio_mens) end as dia_vencimento
from sf_vendas_planos_mensalidade vpm 
cross apply (select top 1 aca_dias_tolerancia_status, ACA_DIA_VENCIMENTO, ACA_TIPO_VENCIMENTO, ACA_EX_VEN_PR, aca_adesao_dep, dcc_dias_boleto from sf_configuracao) as configuracao
cross apply (select *, DATEDIFF(day,(select case when x.parcela = 0 or x.id_prim_mens = x.id_mens then x.dt_inicio_mens 
else DATEADD(day,-configuracao.dcc_dias_boleto, x.dt_inicio_mens) end as data_prazo), x.prev_mens) as prorata,
(SELECT count(*) FROM sf_vendas_planos WHERE  planos_status = 'Cancelado' AND favorecido = x.favorecido AND id_plano <> id_plano_mens) as planos_cancelados,
case when x.parcela = 0 or x.id_prim_mens = x.id_mens then x.dt_inicio_mens else DATEADD(day,-configuracao.dcc_dias_boleto, x.dt_inicio_mens) end as data_prazo
from (select top 1 case when (pp.parcela = 0) then 'DCC' when (pp.parcela = -1) then 'BOLETO' else 'MENSAL' end as tipo_pagamento,
(select top 1 id_mens from sf_vendas_planos_mensalidade where id_plano_mens = A.id_plano order by dt_inicio_mens asc) as id_prim_mens,
(SELECT TOP 1 s.preco_venda FROM sf_produtos s WHERE s.tipo = 'S' AND s.conta_produto = B.conta_produto_adesao) as taxa_adesao,
isnull((select max(dt_inicio_mens) from sf_vendas_planos_mensalidade where id_plano_mens = A.id_plano and id_mens < vpm.id_mens), dateadd(month, -(select case when pp.parcela < 1 then 1 else pp.parcela end), dt_inicio_mens)) as prev_mens,
id_mens, A.favorecido, A.id_plano, A.id_prod_plano, A.planos_status, B.conta_produto_adesao, pp.id_parcela, pp.parcela, vpm.dt_inicio_mens
from sf_vendas_planos A inner join sf_produtos B on B.conta_produto = A.id_prod_plano
inner join sf_produtos_parcelas pp ON pp.id_parcela = vpm.id_parc_prod_mens
where  A.id_plano = id_plano_mens) as x) as vp  
outer apply (select * from (select top 1 cupom_meses as convenios_meses_site, id_convenio, valor, tp_valor, co.cupom, co.descricao as convenio_descricao,
isnull((select top 1 id_prod_convenio from sf_convenios_planos where id_convenio_planos = p.conta_produto and id_prod_convenio = p.conta_produto_adesao), 0) as desconto_adesao 
from sf_fornecedores_despesas_convenios fdc inner join sf_convenios co on co.id_convenio = fdc.convenio
inner join sf_convenios_regras cr on cr.convenio = co.id_convenio left join sf_produtos p on co.id_convenio = p.convenios_site
where ((co.convenio_especifico = 1 and (conta_produto = p.conta_produto or co.id_convenio in (select id_convenio_planos from sf_convenios_planos
where id_prod_convenio = p.conta_produto))) or co.convenio_especifico = 0) and co.ativo = 0) as x) as convenio 
outer apply (select sum(valor * dependentes) as acrescimo,  novo, dependentes, valor as valor_dependente, 
sum((case when id is null or valor = 0  then 0
when config_adesao = 1 and (convenio_desconto = 0 or convenio_desconto is null) then taxa_adesao 
when config_adesao = 1 and convenio_desconto = 1 and tipo_convenio = 'D' then taxa_adesao - convenio_valor  
when config_adesao = 1 and convenio_desconto = 1 and tipo_convenio = 'P' 
then taxa_adesao - (valor * convenio_valor) / 100 else 0 end) * novo) as adesao_dependente
from (select id, valor, taxa_adesao, count(case when cast(data_inclusao as date) <= x.dt_prazo then id_dependente else null end) as dependentes, config_adesao, convenio_desconto, tipo_convenio, convenio_valor,
count(case when (cast(data_inclusao as date) <= x.dt_prazo and cast(data_inclusao as date) > DATEADD(day, x.prorata, x.dt_prazo)) then id_dependente else null end) as novo 
from (select id, id_dependente, valor, data_inclusao, vp.data_prazo as dt_prazo, vp.prorata as prorata, vp.taxa_adesao as taxa_adesao, 
convenio.tp_valor as tipo_convenio, convenio.valor as convenio_valor, convenio.desconto_adesao as convenio_desconto, configuracao.aca_adesao_dep as config_adesao
from sf_vendas_planos inner join sf_fornecedores_despesas_dependentes fdd on fdd.id_titular = favorecido
inner join sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = fdd.id_dependente and fd.inativo = 0
left join sf_produtos_acrescimo on id_produto = id_prod_plano and ((datediff(year,data_nascimento,getdate()) between idade_min and idade_max_m and sexo = 'M') or
(datediff(year,data_nascimento,getdate()) between idade_min and idade_max_f and sexo = 'F')) and (id_parentesco is null or id_parentesco = parentesco)
where id_plano = vp.id_plano) as x group by id, valor, taxa_adesao, config_adesao, convenio_desconto, tipo_convenio, convenio_valor) as y group by id, valor, dependentes, novo) as dependentes
INNER JOIN sf_produtos p ON p.conta_produto = vp.id_prod_plano
inner join sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = vp.favorecido
LEFT JOIN sf_vendas_itens vi ON vi.id_item_venda = vpm.id_item_venda_mens
inner JOIN sf_boleto b ON b.id_referencia = vpm.id_mens AND b.tp_referencia = 'M' and b.inativo = 0
where mundi_id = " . valoresTexto2($transacao);
//echo $sql; exit;
$res = odbc_exec($con, $sql);
if(odbc_num_rows($res) < 1) {
    salvarLogTransacao("Status: Erro não foi possivel encontrar a mensalidade da transacao: ".$transacao);
    echo "Código de Transação Inválido";
    exit;
} 

while ($row = odbc_fetch_array($res)) {
    $row['dt_inicio_mens'] = escreverData($row['dt_inicio_mens']);
    $row['dt_fim_mens'] = escreverData($row['dt_fim_mens']);
    $row['data_prazo'] = escreverData($row['data_prazo']);
    salvarLogTransacao("Status: mensalidade encontrada do codigo: ".$row['id_mens']);
    $idBoleto = $row['id_boleto'];
    $idFornecedores = $row['id_fornecedores_despesas'];
    $idPlano = $row['id_plano'];
    $idMens = $row['id_mens'];
    $tipoPessoa = $row['tipo_pessoa'];
    $idVenda = $row['id_venda'];
    $valorAdesao = escreverNumero($row['valor_adesao']);
    $adesaoSoma = $row['adesao_soma'] == "0" && $row['valor_adesao'] > 0 && $row['id_mens'] == $row['id_prim_mens'] ? 1 : 0;
    atualizaExpBoleto($con, $row);    
    if (!$idVenda && in_array($status, ['charge.paid', 'charge.overpaid'])) {
        atualizaParaTipoCliente($con, $idFornecedores);
        if ($row['id_mens'] == $row['id_prim_mens']) {
            $diaVencimento = diaVencimento($row, $dataPagamento);
            atualizaDataInicioFimMensalidade($con, $row, $diaVencimento);
        }
        $dadosBoleto = "?Trn=" . $idBoleto . "&hostname=" . $hostname . "&database=" . $database . "&adesaoSoma=" . $adesaoSoma
                ."&username=" . $username . "&password=" . $password . "&txtValorAde=" . $valorAdesao . "&Emp=" . $contrato
                ."&dataPagto=" .$dataPagamento;
        $jsonurl = $url . "/Modulos/Sivis/Boleto_Transacao_Pagamento.php" . $dadosBoleto;
        $jsonVendaId = file_get_contents($jsonurl);
        if (is_numeric($jsonVendaId)) {
            salvarLogTransacao("Status: A venda foi concluida com sucesso: ".$jsonVendaId);
            if ($row['id_mens'] == $row['id_prim_mens'] && $row['ACA_EX_VEN_PR'] && $diaVencimento && in_array(intval($row['ACA_TIPO_VENCIMENTO']), [1, 2])) {
                criarCredencial($con, $idFornecedores, $diaVencimento);
            }
            baixaBoleto($con, $jsonVendaId, $valorPago);
            atualizaInicioDoPlano($con, $row);
            atualizaDependentes($con, $row);
            atualizaStatusPlano($con, $idPlano);
        } else {
            salvarLogTransacao("Status: Ocorreu um erro ao gerar a venda");
            if ($tipoPessoa === 'P') {
                atualizaUsuarioParaProspect($con, $idFornecedores);
            }
            salvarLogErro("Status: Ocorreu um erro ao gerar da venda da mensalidade: ".$idMens." da transação: ".$transacao.", Data: ".now());
            http_response_code(500);
            echo "Ocorreu um erro ao gerar a venda";
            exit;
        }
        echo $jsonVendaId;
        exit;
    } else if (!$idVenda && $status === 'charge.underpaid') {
        salvarLogTransacao("Status: Foi identificado pagamento menor do boleto: ".$idBoleto);
        criaUmChamado($con, $idFornecedores, $valorPago, $dataPagamento, $idMens, 
        'Foi identificado pagamento menor do boleto gerado', 
        'PAGAMENTO MENOR', 'menor');
        echo "Valor Pago menor";
        exit;
    } else if (!$idVenda && in_array($status, ['charge.payment_failed'])) {
        salvarLogTransacao("Status: Houve uma falha no pagamento do boleto: ".$idBoleto);
        inativaBoleto($con, $idBoleto, $loginUsuario);
        echo "Inativado o boleto";
        exit;
    } else {
        salvarLogTransacao("Status: A Transacao já encontra paga: ".$transacao);
        if (!$row['id_venda_boleto']) {
            criaUmChamado($con, $idFornecedores, $valorPago, $dataPagamento, $idMens, 'Foi identificado pagamento duplicado da mensalidade do boleto gerado', 'PAGAMENTO DUPLICADO', 'duplicado');
        }
        echo "Transação ja paga";
        exit;
    }
}

function diaVencimento($mensalidade, $diaPagamento) {
    try {
        list($dia, $mes, $ano ) = explode('/', date('d/m/Y', strtotime('now')));
        $diaVencimento = $mensalidade['dia_vencimento'].'/'.$mes.'/'.$ano; 
        $dataVencimento = DateTime::createFromFormat('d/m/Y', trim($diaVencimento));
        $agora = new DateTime();
        $diff = intval($agora->diff($dataVencimento)->format('%R%a'));
        $proximoMes = (new DateTime())->modify('+1 month -1 days');
        $diffProxMes = intval($agora->diff($proximoMes)->format('%R%a'));        
        if ($mensalidade['ACA_TIPO_VENCIMENTO'] == 0) {
            return $diaPagamento;
        } else if ($mensalidade['ACA_TIPO_VENCIMENTO'] == 1) {
            return $diaVencimento;
        } else if ($diff + 10 < 0) {
            $dataVencimento->modify('+1 month');
            $diffDataVencimento = intval($agora->diff($dataVencimento)->format('%R%a'));
            if ($diffDataVencimento > $diffProxMes + 10) {
                $dataVencimento->modify('-1 month');
            }
        }
        return  $dataVencimento->format('d/m/Y');        
    } catch (Exception $e) {
        throw new Exception($e);
    }
}

function atualizaParaTipoCliente($con, $idFornecedores) {
    $sqlAtualizaUsuario = "update sf_fornecedores_despesas set tipo = 'C', dt_convert_cliente = getdate() 
    WHERE id_fornecedores_despesas = " . valoresSelect2($idFornecedores) . " and tipo = 'P';";
    odbc_exec($con, $sqlAtualizaUsuario);
    salvarLogTransacao("Status: Atualiza o prospect para cliente, código: ".$idFornecedores);
}

function atualizaDataInicioFimMensalidade($con, $row, $diaVencimento) {
    $sqlAtualizaProdutoMens = "set dateformat dmy; update sf_vendas_planos_mensalidade set dt_inicio_mens = " . valoresData2($diaVencimento) . ", 
        dt_fim_mens = dateadd(day, -1, dateadd(month, case when pp.parcela < 1 then 1 else pp.parcela end, " . valoresData2($diaVencimento) . "))"
        ." from sf_vendas_planos_mensalidade pm inner join sf_vendas_planos vp ON vp.id_plano = pm.id_plano_mens 
        inner join sf_produtos_parcelas pp on pp.id_parcela = pm.id_parc_prod_mens
        where id_mens = " . valoresSelect2($row['id_mens']) . ";";
    odbc_exec($con, $sqlAtualizaProdutoMens);
    salvarLogTransacao("Status: Atualiza a data do inicio da Mensalidade, código: ".$row['id_mens']);
    
}

function baixaBoleto($con, $idVenda, $valorPago) {
    $sqlBaixaBoleto = "update sf_venda_parcelas set valor_pago = " . valoresNumericos2(escreverNumero($valorPago / 100)) . " where venda = " . valoresSelect2($idVenda) . ";";
    odbc_exec($con, $sqlBaixaBoleto);      
    $query = "update sf_venda_parcelas set valor_parcela_liquido = [dbo].[FU_TAXA_FORMA_PAGAMENTO](sf_venda_parcelas.tipo_documento,sf_venda_parcelas.venda,0, valor_pago) where venda = " . valoresSelect2($idVenda) . ";"; 
    odbc_exec($con, $query);    
    $query2 = "update sf_venda_parcelas set valor_pago = valor_parcela_liquido where venda = " . valoresSelect2($idVenda) . ";"; 
    odbc_exec($con, $query2);    
    salvarLogTransacao("Status: Efetua a baixa do boleto da venda, código: ".$idVenda);
}

function atualizaInicioDoPlano($con, $row) {
    if ($row['id_mens'] === $row['id_prim_mens']) {
        $sqlAtualizaPeriodoPlano = "set dateformat dmy; update sf_vendas_planos set dt_inicio = dbo.FU_PLANO_INI(id_plano), 
        dt_fim = dbo.FU_PLANO_FIM(id_plano) WHERE id_plano = " . valoresSelect2($row['id_plano']);
        odbc_exec($con, $sqlAtualizaPeriodoPlano);
        salvarLogTransacao("Status: Atualiza a data do inicio do plano: ".$row['id_plano']);
    }
}

function atualizaExpBoleto($con, $row) {
    if(!$row['id_venda']) {
        $sql = "update sf_boleto set exp_remessa = 1 WHERE id_boleto = ".valoresSelect2($row['id_boleto']);
        odbc_exec($con, $sql);
        salvarLogTransacao("Status: Atualiza o exp do boleto do codigo: ".$row['id_boleto']);
    }
}

function atualizaDependentes($con, $row) {
        $sqlAtualizaDependente = "set dateformat dmy; update sf_fornecedores_despesas set tipo = 'C', dt_convert_cliente = getdate(),
            fornecedores_status = (select case when id_fornecedores_despesas = " . valoresSelect2($row['id_fornecedores_despesas']) . "
            then dbo.FU_STATUS_CLI(id_fornecedores_despesas) else 'Dependente' end)
            from sf_fornecedores_despesas fd
            inner join (select *, isnull(DATEDIFF(day, x.data_prazo , prev_mens), -30) as prorata from (
                select case when (SELECT TOP 1 id_mens FROM sf_vendas_planos_mensalidade where id_plano_mens = vp.id_plano order by dt_inicio_mens asc) != vpm.id_mens
                then DATEADD(day,(select top 1 -dcc_dias_boleto from sf_configuracao), vpm.dt_inicio_mens) else vpm.dt_inicio_mens end as data_prazo, id_mens, vp.favorecido,
                vp.id_plano, vp.id_prod_plano, vp.planos_status, pc.id as produto_acrescimo_id, parcela,
                (SELECT TOP 1 s.preco_venda FROM sf_produtos s WHERE s.tipo = 'S' AND p.conta_produto_adesao = s.conta_produto) as taxa_adesao,
                (SELECT TOP 1 id_mens FROM sf_vendas_planos_mensalidade where id_plano_mens = vp.id_plano order by dt_inicio_mens asc) as id_prim_mens,
                (select max(dt_inicio_mens) from sf_vendas_planos_mensalidade where id_plano_mens = vpm.id_plano_mens and dt_inicio_mens < vpm.dt_inicio_mens) as prev_mens
                from sf_vendas_planos vp 
                inner join sf_vendas_planos_mensalidade vpm ON vpm.id_plano_mens = vp.id_plano
                inner join sf_boleto b on b.id_referencia = vpm.id_mens and tp_referencia = 'M' and inativo = 0
                inner join sf_produtos_parcelas pp ON pp.id_parcela = vpm.id_parc_prod_mens
                left join sf_produtos_acrescimo pc on pc.id_produto = id_prod_plano
                INNER JOIN sf_produtos p ON p.conta_produto = pp.id_produto where planos_status not in ('Cancelado')
            ) as x ) as planos on (planos.favorecido = fd.id_fornecedores_despesas or planos.favorecido = (select top 1  id_titular from sf_fornecedores_despesas_dependentes where id_dependente = fd.id_fornecedores_despesas)) 
            where inativo = 0 and tipo = 'P' and id_mens = " . valoresSelect2($row['id_mens']) . " and ((id_fornecedores_despesas = " . valoresSelect2($row['id_fornecedores_despesas']) . "
            and (cast(fd.dt_cadastro as date) <= planos.data_prazo and cast(fd.dt_cadastro as date) > DATEADD(day, planos.prorata, planos.data_prazo))) 
            OR id_fornecedores_despesas in (
            SELECT id_dependente from sf_fornecedores_despesas_dependentes 
            where (cast(data_inclusao as date) <= planos.data_prazo and cast(data_inclusao as date) > DATEADD(day, planos.prorata, planos.data_prazo)) and id_titular = " . valoresSelect2($row['id_fornecedores_despesas']) . "));";
    odbc_exec($con, $sqlAtualizaDependente);
    salvarLogTransacao("Status: Atualizando os dependentes");
}

function inativaBoleto($con, $idBoleto, $loginUsuario) {
    $sqlInativaBoleto = "update sf_boleto set inativo = 1 where id_boleto = " . valoresSelect2($idBoleto) . ";";
    $sqlInativaBoleto .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) 
    select 'sf_boleto', id_mens, '" . $loginUsuario . "', 'C', 'CANCELAMENTO BOLETO: ' + cast(bol_nosso_numero as varchar), GETDATE(), favorecido 
    from sf_boleto inner join sf_vendas_planos_mensalidade on id_referencia = id_mens
    inner join sf_vendas_planos on id_plano_mens = id_plano where id_boleto = " . valoresSelect2($idBoleto);        
    odbc_exec($con, $sqlInativaBoleto);
    salvarLogTransacao("Status: Inativando o boleto de id: ".$idBoleto);
}

function atualizaStatusPlano($con, $idPlano) {
    $sqlAtualizaPlano = "update sf_vendas_planos set planos_status = dbo.FU_STATUS_PLANO_CLI(id_plano)
      where id_plano = " . valoresSelect2($idPlano);
    odbc_exec($con, $sqlAtualizaPlano);
    salvarLogTransacao("Status: Atualizando o status do plano de id: ".$idPlano);
}

function atualizaUsuarioParaProspect($con, $idFornecedores) {
    $sqlAtualizaUsuario = "update sf_fornecedores_despesas set tipo = 'P', dt_convert_cliente = null 
    WHERE id_fornecedores_despesas = " . valoresSelect2($idFornecedores) . " and tipo = 'C';";
    odbc_exec($con, $sqlAtualizaUsuario);
    salvarLogTransacao("Status: Atualizando o Usuario para Prospect apos o erro na geração da Venda");
}

function criaUmChamado($con, $id_usuario, $valorPago, $dataPagamento, $idMens, $msg, $obs, $tipo) {
    $dat_mens = "";    
    $sqlCriaUmChamado = "set dateformat dmy; insert into sf_telemarketing(de_pessoa, para_pessoa, data_tele, data_tele_fechamento, pendente, proximo_contato, pessoa, 
    especificacao, relato, dt_inclusao, obs, obs_fechamento, acompanhar, origem, atendimento_servico) values(1, null, getdate(), getdate(), 1, getdate(),
    " . valoresSelect2($id_usuario) . ", 'T', " . valoresTexto2($msg) . ", getdate(), " . valoresTexto2($obs) . ", " . valoresTexto2($obs) . ", 0, 0, 0);
    SELECT SCOPE_IDENTITY() id;";
    $res = odbc_exec($con, $sqlCriaUmChamado);
    odbc_next_result($res);
    $id_tele = odbc_result($res, 1);        
    if ($id_tele && is_numeric($id_tele)) {
        $cur = odbc_exec($con, "select dt_inicio_mens, valor_mens from sf_vendas_planos_mensalidade where id_mens = " . $idMens);
        while ($RFP = odbc_fetch_array($cur)) {
            $dat_mens = escreverData($RFP['dt_inicio_mens']);
        }        
        $detalheChamado = "SET DATEFORMAT DMY;" .
        "insert into sf_mensagens_telemarketing (id_usuario_de, id_tele, id_usuario_para,data,mensagem,dt_inclusao) values(" .
        "1," . $id_tele . ",null," .  valoresDataHora2($dataPagamento, " 00:00:00") . ",
        ".valoresTexto2('foi identificado um pagamento '.$tipo.' de boleto, no valor de '
        .escreverNumero(($valorPago / 100), 1).' referente a mensalidade de vencimento no dia '
        .$dat_mens).", getdate())";
        odbc_exec($con, $detalheChamado);
        
        salvarLogTransacao("Status: Foi criado um chamado para Mensalidade do código: ".$idMens." por que o valor pago foi menor do que o esperado");   
    }
}

function criarCredencial($con, $idFornecedores, $diaVencimento) {
    $agora = new DateTime();
    $dataVencimento = DateTime::createFromFormat('d/m/Y', $diaVencimento);
    $dataDiff = intval($agora->diff($dataVencimento)->format('%R%a'));
    if ($dataDiff > 0) {
        $sql = "set dateformat dmy; insert into sf_fornecedores_despesas_credenciais
        (id_fornecedores_despesas, id_credencial, dt_inicio, dt_fim, motivo, dt_cadastro, usuario_resp) 
        values (".valoresSelect2($idFornecedores).", 1, getdate(), dateadd(day,-1,".valoresData2($diaVencimento)."), ".valoresTexto2('período grátis').", getdate(), 1);";
        odbc_exec($con, $sql);
        salvarLogTransacao("Status: cria a credencial do período gratis");
    }
}

function salvarLogTransacao($content) {
    $transacao = $_GET["Trn"];
    salvaLog('Webhooks', 'BOL', $transacao.".txt", $content.", Data: ".now()."\r\n", true);
}

function salvarLogErro($content) {
    salvaLog("Log", 'Erro', 'erro',  $content, true);
}

