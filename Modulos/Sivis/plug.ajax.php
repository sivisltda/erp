<?php

header('Cache-Control: no-cache');
include './../../Connections/configini.php';

$local = array();
$txtId = $_REQUEST['txtId'];
if (is_numeric($txtId)) {
    $sql = "SELECT MAX(COD_PLUGUE) + 1 total FROM ca_cliplugue WHERE COD_ACAD = " . $txtId;
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $plugue = str_pad($RFP['total'], 5, "0", STR_PAD_LEFT);
    }
    if ($plugue == "00000") {
        $plugue = $txtId . "01";
    }
}
$local[] = array('plugue' => $plugue);
echo(json_encode($local));
odbc_close($con);
