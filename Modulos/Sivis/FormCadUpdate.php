<?php
include './../../Connections/configSivis.php';
include '../../util/util.php';
$local = array();

if (isset($_GET["cod"])) {
    $cod = $_GET["cod"];
    $sQuery1 = "select * from ca_feedatualizacao where id=$cod";
    $cur = odbc_exec($con2, $sQuery1) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('id' => $RFP['id'], 'data' => $RFP['data'], 'titulo' => utf8_encode($RFP['titulo']), 'texto' => $RFP['texto'], 'ativo' => $RFP['ativo'], 'system' => $RFP['sistemas']);
    }
    $disabled = "disabled";
}

?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
<link href="../../css/main.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../../css/styleLeo.css"/>
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <div class="row-fluid">
        <div id="inline1">
            <div class="block title">
                <div class="head">
                    <div id="topBG" style="margin-bottom:10px">
                        <h2><font color="#FFFFFF">Cadastrar Atualização</font>
                            <button style="border:none; float:right; width:25px; margin-right:20px; color:#FFFFFF; background-color:#333333;" onClick="parent.FecharBox(0)" id="bntOK"><b>x</b></button>
                        </h2>
                    </div>
                </div>
            </div>
            <div>
                <div class="block">
                    <input type="hidden" id="edit" name="" value="<?php echo $_GET["edit"]; ?>">
                    <input type="hidden" id="txtID" value="<?php echo $_GET["cod"]; ?>">
                    <div class="formContent">
                        <p>Titulo: </p>
                        <input type="text" id="txttitulo" <?php echo $disabled; ?> value="<?php echo $local[0]["titulo"]; ?>">
                    </div>
                    <div class="formContent">
                        <p style="margin-top: 10px;">Texto:</p>
                        <div style="margin-bottom: 15px;">
                            <textarea id="ckeditor" name="ckeditor"  style="height: 334px;"><?php echo $local[0]["texto"]; ?></textarea>
                        </div>

                        <div class="ajustPublic">
                            <p>Sistemas:</p>
                            <select name="itemsStat[]" id="itemsStat" multiple="multiple" style="width:50%;" class="select" class="input-medium">
                                <option value="0" <?php echo (strpos($local[0]["system"], "0") ? "selected" : ""); ?>>Sivis-ERP</option>
                                <option value="1" <?php echo (strpos($local[0]["system"], "1") ? "selected" : ""); ?>>Sivis-PGTO</option>
                                <option value="2" <?php echo (strpos($local[0]["system"], "2") ? "selected" : ""); ?>>Pro-Quality</option>
                            </select>
                            <input type="checkbox" name="ckbpublicado" value="" <?php echo ($local[0]["ativo"] == 1 ? "checked" : ""); ?> id="ckbpublicado"/> <p>Publicado</p>
                        </div>
                    </div>
                    <div class="spanBNT" style="left:0px; margin-top:0px; margin-bottom:0px">
                        <div class="toolbar bottom tar">
                            <div class="data-fluid" style="overflow:hidden;">
                                <div class="btn-group">
                                    <button class="btn btn-success" type="submit" name="bntSave"    title="Gravar"   id="bntOK" onClick="cadUpdate('C')"><span class="ico-checkmark"></span> Gravar</button>
                                    <button class="btn btn-success" onClick="parent.FecharBox(0)"   title="Cancelar" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                                    <button class="btn  btn-success"type="submit" name="bntNew"   onClick="novo()"  title="Novo"     id="bntOK" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                                    <button class="btn btn-success" type="submit" name="bntEdit"    title="Alterar"   id="bntOK"      onClick="editar2();"><span class="ico-edit"></span> Alterar</button>
                                    <button class="btn red" type="submit" title="Excluir" name="bntDelete" id="bntOK" value="Excluir" onClick="cadUpdate('D')" ><span class=" ico-remove"> </span> Excluir</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
    <script type="text/javascript" src="../../js/util.js"></script>
    <script type='text/javascript' src='../../js/plugins/ckeditor/ckeditor.js'></script>      
    <script type="text/javascript" src="js/updateForm.js"></script>
</body>
