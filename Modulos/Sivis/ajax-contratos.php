<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include './../../Connections/configini.php';

$texto = $_REQUEST['q'];
$sql = "select id,numero_contrato,razao_social,nome_fantasia FROM sf_fornecedores_despesas
inner join ca_contratos on id_fornecedores_despesas = id_cliente
WHERE tipo = 'C' and (numero_contrato like '%" . str_replace("'", "", $texto) . "%'
or razao_social like '%" . str_replace("'", "", $texto) . "%'
or nome_fantasia like '%" . str_replace("'", "", $texto) . "%') order by numero_contrato";

$cur = odbc_exec($con, $sql) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $descricao = utf8_encode($RFP['numero_contrato']) . " - " . utf8_encode($RFP['razao_social']);
    if ($RFP['nome_fantasia'] != "") {
        $descricao = $descricao . " (" . utf8_encode($RFP['nome_fantasia']) . ")";
    }
    $row['value'] = $descricao;
    $row['id'] = $RFP['id'];
    $row_set[] = $row;
}
echo json_encode($row_set);
odbc_close($con);
