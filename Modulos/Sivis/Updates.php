<?php
include './../../Connections/configini.php';
$imprimir = 0;

if (is_numeric($_GET["imp"])) {
    $imprimir = 1;
} ?>
<!DOCTYPE html>
<html lang="en">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/styleLeo.css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body>
        <?php if ($imprimir == 0) { ?>
            <div id="loader"><img src="../../img/loader.gif"/></div><?php } ?>
        <div class="wrapper">
            <?php
            if ($imprimir == 0) {
                include("../../menuLateral.php");
            }
            ?>
            <div class="body">
                <?php
                if ($imprimir == 0) {
                    include("../../top.php");
                }
                ?>
                <div class="content">
                    <?php if ($imprimir == 0) { ?>
                        <div class="page-header">
                            <div class="icon"> <span class="ico-arrow-right"></span></div>
                            <h1>Sivis<small>Relatório de Atualizações</small></h1>
                        </div>
                        <div class="row-fluid" style="border-bottom: 1px solid #e0e0e0;padding-bottom: 8px;">
                            <div class="span12">
                                <div class="block" style="font-size:13px">
                                    <?php if ($_GET["codcli"] == 003) { ?>
                                        <button id="btnNovo" class="button button-green btn-primary" type="button" onclick="abrirTelaBox('FormCadUpdate.php', 540, 720)"><span class="ico-file-4 icon-white"></span></button>
                                    <?php } ?>
                                    <input type="hidden" id="codcli" value="<?php echo $_GET["codcli"]; ?>">
                                    <input type="hidden" id="mod_emp" value="<?php echo $_SESSION["mod_emp"]; ?>">
                                    <span style="float:right">
                                        Pesquisar: <input name="txtBuscar" id="txtBuscar" type="text" value="" style="width:200px; height:31px"/>
                                        <button class="button button-turquoise btn-primary" type="button" onclick="refresh()" id="btnPesquisar"><span class="ico-search icon-white"></span></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="block">
                        <div class="boxhead">
                            <div class="boxtext">Atualizações</div>
                        </div>
                        <div id="accordionContent">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type='text/javascript' src='../../js/plugins/highlight/jquery.highlight-4.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>
        <script type="text/javascript" src="js/update.js" charset="UTF-8"></script>
        <?php odbc_close($con); ?>
    </body>
</html>
