<?php
include './../../Connections/configini.php';
$disabled = 'disabled';

if (isset($_POST['bntSave'])) {
    if ((valoresTexto('txtSenha') == valoresTexto('txtConSenha')) && strlen(valoresTexto('txtSenha')) > 2) {
        if (is_numeric($_POST['txtId'])) {
            odbc_exec($con, "update ca_user_portal set " .
            "usuario = " . valoresTexto('txtUsuario') . ", " .
            "senha = " . valoresTexto('txtSenha') . ", " .
            "email = " . valoresTexto('txtEmail') . ", " .
            "acessoGym = " . valoresCheck('ckbSivisGym') . ", " .
            "acessoBody = " . valoresCheck('ckbSivisBody') . ", " .
            "acessoTraining = " . valoresCheck('ckbSivisTraining') . ", " .
            "acessoMfCom = " . valoresCheck('ckbMfCom') . ", " .
            "acessoSecullum = " . valoresCheck('ckbSecullum') . ", " .
            "informativo = " . valoresCheck('ckbinformativo') . ", " .
            "persona = " . valoresCheck('ckbPersona') . ", " .
            "tutorial = " . valoresCheck('ckbTutorial') . ", " .
            "observacoes = " . valoresTexto('txtMensagem') . ", " .
            "status = " . valoresTexto('txtStatus') . " where id_usuario = " . $_POST['txtId']) or die(odbc_errormsg());
        } else {
            odbc_exec($con, "insert into ca_user_portal(usuario,senha,email,acessoGym,acessoBody,informativo,acessoTraining,acessoMfCom,acessoSecullum,persona,tutorial,observacoes,status)values(" .
            valoresTexto('txtUsuario') . ", " .
            valoresTexto('txtSenha') . ", " .
            valoresTexto('txtEmail') . ", " .
            valoresCheck('ckbSivisGym') . ", " .
            valoresCheck('ckbSivisBody') . ", " .
            valoresCheck('ckbinformativo') . ", " .
            valoresCheck('ckbSivisTraining') . ", " .
            valoresCheck('ckbMfCom') . ", " .
            valoresCheck('ckbSecullum') . ", " .
            valoresCheck('ckbPersona') . ", " .
            valoresCheck('ckbTutorial') . ", " .
            valoresTexto('txtMensagem') . ", " .
            valoresTexto('txtStatus') . ")") or die(odbc_errormsg());
            $res = odbc_exec($con, "select top 1 id_usuario from ca_user_portal order by id_usuario desc") or die(odbc_errormsg());
            $_POST['txtId'] = odbc_result($res, 1);
        }
    } else {
        echo "<script>alert('Verifique os valores preenchidos para o cadastro da senha!');</script>";
    }
}

if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        odbc_exec($con, "DELETE FROM ca_user_portal where id_usuario = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from ca_user_portal where id_usuario = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $txtId = utf8_encode($RFP['id_usuario']);
        $txtUsuario = utf8_encode($RFP['usuario']);
        $txtSenha = utf8_encode($RFP['senha']);
        $txtStatus = utf8_encode($RFP['status']);
        $txtEmail = utf8_encode($RFP['email']);
        $ckbMfCom = utf8_encode($RFP['acessoMfCom']);
        $ckbSecullum = utf8_encode($RFP['acessoSecullum']);
        $ckbSivisGym = utf8_encode($RFP['acessoGym']);
        $ckbTutorial = utf8_encode($RFP['tutorial']);
        $ckbPersona = utf8_encode($RFP['persona']);
        $ckbSivisBody = utf8_encode($RFP['acessoBody']);
        $ckbSivisTraining = utf8_encode($RFP['acessoTraining']);
        $ckbSivisInfo = utf8_encode($RFP['informativo']);
        $txtMensagem = utf8_encode($RFP['observacoes']);
    }
} else {
    $disabled = '';
    $txtId = '';
    $txtUsuario = '';
    $txtSenha = '';
    $txtStatus = '';
    $txtEmail = '';
    $ckbMfCom = '';
    $ckbSecullum = '';
    $ckbSivisGym = '';
    $ckbTutorial = '';
    $ckbPersona = '';
    $ckbSivisBody = '';
    $ckbSivisTraining = '';
    $ckbSivisInfo = '';
    $txtMensagem = '';
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
<link href="../../css/main.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../../css/styleLeo.css"/>
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <form action="FormUsuarioPortal.php" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Usuário do Portal</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont">
            <table width="730" style="margin-bottom:25px" border="0" cellpadding="3" class="textosComuns">
                <tr>
                <input name="txtId" id="txtId" value="<?php echo $txtId; ?>" type="hidden"/>
                <td width="100" align="right">Usuário:</td>
                <td colspan="2"><input name="txtUsuario" id="txtUsuario" type="text" class="input-medium" value="<?php echo $txtUsuario; ?>" <?php echo $disabled; ?>/></td>
                <td width="237">
                    <div style="width:100px; float:left; line-height:27px" align="right">Status:</div>
                    <div style="width:144px; float:left; margin-left:5px">
                        <select class="select" name="txtStatus" id="txtStatus" style="width:100%" <?php echo $disabled; ?>>
                            <option value="0" <?php
                            if ($txtStatus == "0") {
                                echo "SELECTED";
                            }
                            ?>>Ativo</option>
                            <option value="1" <?php
                            if ($txtStatus == "1") {
                                echo "SELECTED";
                            }
                            ?>>Inativo</option>
                        </select>
                    </div>
                </td>
                </tr>
                <tr>
                    <td width="100" align="right">Senha:</td>
                    <td width="237"><input name="txtSenha" id="txtSenha" type="password" value="<?php echo $txtSenha; ?>" class="input-medium" <?php echo $disabled; ?>/></td>
                    <td width="100" align="right">Confirmar Senha:</td>
                    <td width="237"><input name="txtConSenha" id="txtConSenha" type="password" value="<?php echo $txtSenha; ?>" class="input-medium" <?php echo $disabled; ?>/></td>
                </tr>
                <tr>
                    <td width="100" align="right">E-mail:</td>
                    <td colspan="3"><input name="txtEmail" id="txtEmail" type="text" class="input-medium" style="width:100%" value="<?php echo $txtEmail; ?>" <?php echo $disabled; ?>/></td>
                </tr>
                <tr>
                    <td width="100" align="right">Permitir Acesso:</td>
                    <td colspan="3">
                        <table width="100%" border="0" cellpadding="3" class="textosComuns">
                            <tr>
                                <td width="13" height="33"><div><input name="ckbMfCom" id="ckbMfCom" <?php
                                        echo $disabled;
                                        if ($ckbMfCom == "1") {
                                            echo " CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="130">MfCom</td>
                                <td width="13"><div><input name="ckbSecullum" id="ckbSecullum" <?php
                                        echo $disabled;
                                        if ($ckbSecullum == "1") {
                                            echo " CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="130">Secullum</td>
                                <td width="13"><div><input name="ckbSivisGym" id="ckbSivisGym" <?php
                                        echo $disabled;
                                        if ($ckbSivisGym == "1") {
                                            echo " CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="130">Sivis Gym</td>
                                <td width="13"><div><input name="ckbTutorial" id="ckbTutorial" <?php
                                        echo $disabled;
                                        if ($ckbTutorial == "1") {
                                            echo " CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="130">Tutorial</td>
                            </tr>
                            <tr>
                                <td width="13" height="33"><div><input name="ckbPersona" id="ckbPersona" <?php
                                        echo $disabled;
                                        if ($ckbPersona == "1") {
                                            echo " CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Persona</td>
                                <td width="13"><div><input name="ckbSivisBody" id="ckbSivisBody" <?php
                                        echo $disabled;
                                        if ($ckbSivisBody == "1") {
                                            echo " CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Sivis Body</td>
                                <td width="13"><div><input name="ckbSivisTraining" id="ckbSivisTraining" <?php
                                        echo $disabled;
                                        if ($ckbSivisTraining == "1") {
                                            echo " CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Sivis Training</td>
                                <td width="13"><div><input name="ckbinformativo" id="ckbinformativo" <?php
                                        echo $disabled;
                                        if ($ckbSivisInfo == "1") {
                                            echo " CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Informativo - Splash</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="100" align="right">Mensagem:</td>
                    <td colspan="3">
                        <div style="border:1px solid #CCC">
                            <textarea name="txtMensagem" id="txtMensagem" style="width:100%; height:100px" <?php echo $disabled; ?>><?php echo $txtMensagem; ?></textarea>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" name="bntSave" id="bntOK"><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else { ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?');"><span class=" ico-remove"></span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
    <script type="text/javascript" src="../../js/util.js"></script>
<?php odbc_close($con); ?>
</body>
