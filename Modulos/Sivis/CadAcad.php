<?php
include '../../Connections/configini.php';
if (is_numeric($_GET['Del'])) {
    odbc_exec($con, "DELETE FROM sf_mensagens_fornecedores WHERE id_fornecedores_despesas = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_endereco_entrega WHERE fornecedores_despesas = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_fornecedores_despesas WHERE id_fornecedores_despesas = " . $_GET['Del']);
    echo "<script>window.top.location.href = 'CadAcad.php'; </script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Sivis<small>Cadastro de Clientes</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="float:left;width: 10%;margin-top: 15px;">
                                    <button class="button button-green btn-primary" type="button" onClick="AbrirBox(0)"><span class="ico-file-4 icon-white"></span></button>
                                    <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir()" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                </div>
                                <div style="float:right;width: 90%;">
                                    <div style="float:left;margin-left: 1%;width: 12%;">
                                        <div width="100%">UF:</div>
                                        <select id="txtEstado" class="select">
                                            <option value="null">Selecione</option>
                                            <?php
                                            $cur = odbc_exec($con, "select estado_codigo,estado_sigla from tb_estados where estado_codigo > 0 order by estado_nome") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['estado_codigo'] ?>"><?php echo utf8_encode($RFP['estado_sigla']) ?></option>
                                            <?php } ?>
                                        </select> 
                                    </div>
                                    <div style="float:left;margin-left: 1%;width: 15%;">                                    
                                        <div width="100%">Cidade:</div>
                                        <select id="txtCidade" class="select">
                                            <option value="null">Selecione</option>                                        
                                        </select>                                                                        
                                    </div>
                                    <div style="float:left;margin-left: 1%;width: 12%;">                                    
                                        <div width="100%">Franquia:</div>
                                        <select id="txtFranquia" class="select">
                                            <option value="null">Selecione</option>                                        
                                            <option value="S">Sivis</option>                                                                             
                                        </select>                                           
                                    </div>
                                    <div style="float:left;margin-left: 1%;width: 26%;">     
                                        <div width="100%">Contrato:</div>
                                        <select id="txtContrato" class="select">
                                            <option value="null" >Selecione</option>
                                            <?php
                                            $cur = odbc_exec($con, "select id_contas_movimento,sf_grupo_contas.descricao d1, sf_contas_movimento.descricao d2 from sf_contas_movimento left join sf_grupo_contas on id_grupo_contas = grupo_conta where tipo = 'C' order by sf_grupo_contas.descricao,sf_contas_movimento.descricao") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id_contas_movimento']; ?>"><?php echo utf8_encode($RFP['d1']) . " - " . utf8_encode($RFP['d2']); ?></option>
                                            <?php } ?>                                                    
                                        </select>                                     
                                    </div>                                        
                                    <div style="float:left;margin-left: 1%;width: 25%;">
                                        <div width="100%">Busca:</div>
                                        <input id="txtBusca" maxlength="100" type="text" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title=""/>
                                    </div>
                                    <div style="float:left;margin-left: 1%;width: 4%;margin-top: 15px;">
                                        <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Cadastro de Clientes</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblLista">
                            <thead>
                                <tr>
                                    <th width="3%"></th>
                                    <th width="5%">Cód.</th>
                                    <th width="30%">Nome Fantasia/Razão Social/Nome:</th>                                               
                                    <th width="10%">Endereço:</th>                                                
                                    <th width="15%">Tel.:</th>
                                    <th width="8%">Plugue:</th>                                                                                                                                            
                                    <th width="8%"><center>Permissão:</center></th> 
                                    <th width="11%">Status:</th>
                                    <th width="8%">Cód.ERP</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div> 
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>         
        <script type="text/javascript" src="../../js/util.js"></script>                        
        <script type="text/javascript">
            $(document).ready(function () {
                $("#btnfind").click(function () {
                    tbLista.fnReloadAjax(finalFind(0));
                });

                $('#txtEstado').change(function () {
                    if ($(this).val()) {
                        $('#txtCidade').hide();
                        $('#carregando').show();
                        $.getJSON('../../Modulos/Comercial/locais.ajax.php?search=', {txtEstado: $(this).val(), ajax: 'true'}, function (j) {
                            var options = '<option value="null">Selecione</option>';
                            for (var i = 0; i < j.length; i++) {
                                options += '<option value="' + j[i].cidade_codigo + '">' + j[i].cidade_nome + '</option>';
                            }
                            $('#txtCidade').html(options).show();
                            $('#carregando').hide();
                        });
                    } else {
                        $('#txtCidade').html('<option value="">Selecione</option>');
                    }
                });
            });

            function finalFind(imp) {
                var retPrint = (imp === 1 ? '&imp=1' : '?imp=0');
                if ($("#txtEstado").val() !== "null") {
                    retPrint = retPrint + '&es=' + $("#txtEstado").val();
                }
                if ($("#txtCidade").val() !== "null") {
                    retPrint = retPrint + '&ci=' + $("#txtCidade").val();
                }
                if ($("#txtContrato").val() !== "null") {
                    retPrint = retPrint + '&id=' + $("#txtContrato").val();
                }
                if ($("#txtFranquia").val() !== "null") {
                    retPrint = retPrint + '&fq=' + $("#txtFranquia").val();
                }
                if ($("#txtBusca").val() !== "") {
                    retPrint = retPrint + '&Search=' + $("#txtBusca").val();
                }
                return "CadAcad_server_processing.php" + retPrint.replace(/\//g, "_");
            }

            function AbrirBox(id) {
                abrirTelaBox("FormCadAcad.php" + (id > 0 ? "?id=" + id : ""), 425, 740);
            }

            function FecharBox() {
                $("#newbox").remove();
                tbLista.fnReloadAjax(finalFind(0));
            }

            function imprimir() {
                var pRel = "&NomeArq=" + "Cadastro de Clientes" +
                        "&lbl=Cod|Nome|Endereço|Tel.|Plugue|Permissão|Status|Cod.ERP" +
                        "&siz=40|220|100|80|60|60|80|60" +
                        "&pdf=0" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/Sivis/" + finalFind(1); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }

            var tbLista = $('#tblLista').dataTable({
                "iDisplayLength": 20,
                "aLengthMenu": [20, 30, 40, 50, 100],
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "aaSorting": [[1, 'asc']],
                "aoColumns": [
                    {"bSortable": false},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true}],
                "sAjaxSource": finalFind(0),
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"},
                "sPaginationType": "full_numbers"
            });

            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }
        </script>        
        <?php odbc_close($con); ?>
    </body>
</html>