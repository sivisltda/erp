<?php

if (!isset($_GET['pdf'])) {
    include './../../Connections/configini.php';
}
$aColumns = array('pk', 'razao_social', 'email', 'bol_nosso_numero', 'Vecto', 'historico_baixa', 'pa', 'valor_parcela', 'valor_pago', 'inativo');
$iTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY Vecto asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (is_numeric($_GET['imp']) && $_GET['imp'] == "1") {
    $imprimir = 1;
}

if ($_GET['dti'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
}

if ($_GET['dtf'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0 && intval($_GET['iSortCol_' . $i]) < 9) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY Vecto asc";
    }
}

$comando = " AND valor_pago = 0 ";
$comandoX1 = "";
$comandoX2 = "";
$comandoX3 = "";

if ($_GET['Search'] != "") {
    $comando .= " AND " . $aColumns[1] . " LIKE '%" . utf8_decode($_GET['Search']) . "%' ";
}

if ($DateBegin != "") {
    $comandoX1 .= " AND data_vencimento >= " . valoresData2($DateBegin) . " ";
    $comandoX2 .= " AND data_parcela >= " . valoresData2($DateBegin) . " ";
    $comandoX3 .= " AND data_parcela >= " . valoresData2($DateBegin) . " ";
}
if ($DateEnd != "") {
    $comandoX1 .= " AND data_vencimento <= " . valoresData2($DateEnd) . " ";
    $comandoX2 .= " AND data_parcela <= " . valoresData2($DateEnd) . " ";
    $comandoX3 .= " AND data_parcela <= " . valoresData2($DateEnd) . " ";
}
$comandoX1 .= " AND sf_lancamento_movimento_parcelas.inativo = '0' ";
$comandoX2 .= " AND sf_solicitacao_autorizacao_parcelas.inativo = '0' ";
$comandoX3 .= " AND sf_venda_parcelas.inativo = '0' ";

$sQuery1 = "select 'D-' +cast(sf_lancamento_movimento_parcelas.id_parcela as VARCHAR) as pk ,data_vencimento 'Vecto',pa ,sf_lancamento_movimento.empresa empresa,razao_social,
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = sf_fornecedores_despesas.id_fornecedores_despesas) email, historico_baixa, 'D' Origem , sf_contas_movimento.descricao 'cm',
valor_parcela,valor_pago,sf_grupo_contas.descricao gc,sf_lancamento_movimento_parcelas.inativo inativo, sf_lancamento_movimento_parcelas.sa_descricao documento,obs_baixa,isnull(bol_id_banco,0) bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,bol_nosso_numero,data_pagamento,data_cadastro from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento 
on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
left join sf_tipo_documento on sf_lancamento_movimento_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento                                                                        
inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_lancamento_movimento.status = 'Aprovado' and sf_contas_movimento.tipo = 'C' " . $comando . $comandoX1 . "
and (sf_lancamento_movimento_parcelas.bol_nosso_numero is null or sf_lancamento_movimento_parcelas.bol_nosso_numero not in (select bol_nosso_numero from sf_lancamento_movimento_parcelas group by bol_nosso_numero having bol_nosso_numero is not null and count(id_parcela) > 1))                                                                            
union
select 'Y-' +cast(max(sf_lancamento_movimento_parcelas.id_parcela) as VARCHAR) as pk,MAX(data_vencimento) 'Vecto',MAX(pa) pa,MAX(sf_lancamento_movimento.empresa) empresa
,MAX(razao_social) razao_social,(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = MAX(sf_fornecedores_despesas.id_fornecedores_despesas)) email, MAX(historico_baixa) historico_baixa, 'D' Origem , MAX(sf_contas_movimento.descricao) 'cm',
sum(valor_parcela) valor_parcela,sum(valor_pago) valor_pago,MAX(sf_grupo_contas.descricao) gc,MAX(sf_lancamento_movimento_parcelas.inativo) inativo, 
MAX(sf_lancamento_movimento_parcelas.sa_descricao) documento,
MAX(obs_baixa) obs_baixa,MAX(isnull(bol_id_banco,0)) bol_id_banco,MAX(sf_tipo_documento.abreviacao)abreviacao,
MAX(sf_tipo_documento.descricao) tdn,MAX(id_fornecedores_despesas) id_fornecedores_despesas,MAX(bol_nosso_numero) bol_nosso_numero, MAX(data_pagamento) data_pagamento, Max(data_cadastro) data_cadastro
from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento 
on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
left join sf_tipo_documento on sf_lancamento_movimento_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento                                                                        
inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_lancamento_movimento.status = 'Aprovado' and sf_contas_movimento.tipo = 'C' " . $comando . $comandoX1 . "
group by bol_nosso_numero having bol_nosso_numero is not null and count(id_parcela) > 1
union
select 'A-' +cast(sf_solicitacao_autorizacao_parcelas.id_parcela as VARCHAR) as pk ,data_parcela 'Vecto',pa ,sf_solicitacao_autorizacao.empresa empresa,razao_social,
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = sf_fornecedores_despesas.id_fornecedores_despesas) email, historico_baixa,'A' Origem, sf_contas_movimento.descricao 'cm',
valor_parcela,valor_pago,sf_grupo_contas.descricao gc,sf_solicitacao_autorizacao_parcelas.inativo inativo,sf_solicitacao_autorizacao_parcelas.sa_descricao documento,obs_baixa,isnull(bol_id_banco,0) bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,bol_nosso_numero,data_pagamento,data_cadastro from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao                                                                        
inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas
left join sf_tipo_documento on sf_solicitacao_autorizacao_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento                                                                                                                                                
inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'C' and status = 'Aprovado' " . $comando . $comandoX2 . "
union    
select 'V-' +cast(sf_venda_parcelas.id_parcela as VARCHAR) as pk ,data_parcela 'Vecto',pa ,sf_vendas.empresa empresa,razao_social,
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = sf_fornecedores_despesas.id_fornecedores_despesas) email, sf_vendas.historico_venda historico_baixa,'V' Origem, 
'VENDAS' cm,valor_parcela,valor_pago,'VENDAS' gc,sf_venda_parcelas.inativo inativo,sf_venda_parcelas.sa_descricao documento,obs_baixa,bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,bol_nosso_numero,data_pagamento,data_cadastro   
from sf_venda_parcelas inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda left join sf_tipo_documento on sf_venda_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento                                                                                                                                                                                                    
inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas where sf_vendas.cov = 'V' and status = 'Aprovado' " . $comando . $comandoX3;

$query = "set dateformat dmy;SELECT COUNT(*) total from (" . $sQuery1 . ") as x ";
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $iTotal = $RFP["total"];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iTotal,
    "aaData" => array()
);

$query = "set dateformat dmy;SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row, * from (" . $sQuery1 .
") as x) as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder;
//echo $query;exit;
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $row = array();
    $backColor = "";
    $data = "";
    if ($RFP[$aColumns[8]] == 0) {
        if ($RFP[$aColumns[4]] != "") {
            if (escreverData($RFP[$aColumns[4]], "Y-m-d") < date("Y-m-d")) {
                $backColor = " style='color:red' ";
            }
        }
    }
    if ($RFP[$aColumns[9]] == '1') {
        $Estado = "inativo";
    } else {
        if ($RFP[$aColumns[8]] > 0) {
            $Estado = "pago";
        } else {
            $Estado = "apagar";
        }
    }
    if ($RFP[$aColumns[4]] != "") {
        $data = escreverData($RFP[$aColumns[4]]);
    }
    if ($Estado == "apagar" && $RFP[$aColumns[2]] != "") {
        $pk = $RFP[$aColumns[0]];
        $row[] = "<center><input id='check' type='checkbox' class='caixa' name='items[]' value='" . $pk . "'/>" .
                "<input name=\"txtCl_" . $pk . "\" value=\"" . utf8_encode($RFP[$aColumns[1]]) . "\" type=\"hidden\"/>" .
                "<input name=\"txtEm_" . $pk . "\" value=\"" . utf8_encode($RFP[$aColumns[2]]) . "\" type=\"hidden\"/>" .
                "<input name=\"txtNn_" . $pk . "\" value=\"" . utf8_encode($RFP[$aColumns[3]]) . "\" type=\"hidden\"/>" .
                "<input name=\"txtVc_" . $pk . "\" value=\"" . $data . "\" type=\"hidden\"/>" .
                "<input name=\"txtHs_" . $pk . "\" value=\"" . utf8_encode($RFP[$aColumns[5]]) . "\" type=\"hidden\"/>" .
                "<input name=\"txtPa_" . $pk . "\" value=\"" . utf8_encode($RFP[$aColumns[6]]) . "\" type=\"hidden\"/>" .
                "<input name=\"txtVl_" . $pk . "\" value=\"" . escreverNumero($RFP[$aColumns[7]], 1) . "\" type=\"hidden\"/></center>";
    } else {
        $row[] = "<center><input id='check' type='checkbox' class='caixa' name='items[]' value='0' disabled></center>";
    }
    $row[] = "<div id='formPQ' " . $backColor . " title='" . utf8_encode($RFP[$aColumns[1]]) . "'>" . utf8_encode($RFP[$aColumns[1]]) . "</div>";
    $row[] = "<div id='formPQ' " . $backColor . " title='" . utf8_encode($RFP[$aColumns[2]]) . "'>" . utf8_encode($RFP[$aColumns[2]]) . "</div>";
    $row[] = "<div id='formPQ' " . $backColor . " title='" . utf8_encode($RFP[$aColumns[3]]) . "'>" . utf8_encode($RFP[$aColumns[3]]) . "</div>";
    $row[] = "<div id='formPQ' " . $backColor . " title='" . $data . "'>" . $data . "</div>";
    $row[] = "<div id='formPQ' " . $backColor . " title='" . utf8_encode($RFP[$aColumns[5]]) . "'>" . utf8_encode($RFP[$aColumns[5]]) . "</div>";
    $row[] = "<div id='formPQ' " . $backColor . " title='" . utf8_encode($RFP[$aColumns[6]]) . "'>" . utf8_encode($RFP[$aColumns[6]]) . "</div>";
    $row[] = "<div id='formPQ' " . $backColor . " title='" . escreverNumero($RFP[$aColumns[7]], 1) . "'>" . escreverNumero($RFP[$aColumns[7]], 1) . "</div>";
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
