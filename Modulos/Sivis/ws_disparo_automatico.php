<?php
set_time_limit(1800);

if (!in_array($_SERVER['REMOTE_ADDR'], ["148.72.177.101"])) {
    exit;
}

$hostname = $_GET['Servidor'];
$database = $_GET['Banco'];
$username = $_GET['Login'];
$password = $_GET['Senha'];
$con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
require_once(__DIR__ . '/../../Connections/funcoesAux.php');
$contrato = str_replace("ERP", "", strtoupper($_GET['Banco']));

if (isset($_GET['ListaItensAtivos'])) {
    $query = "select A.id_disparo_item, B.id_disparo_auto, A.dias_tolerancia_item, isnull(A.id_email_item,0) id_email_item, isnull(A.id_user_resp_item,0) id_user_resp_item, loja_disparo_auto 
    from dbo.sf_disparo_auto_item A inner join sf_disparo_auto B on A.id_disparo_auto_item = B.id_disparo_auto
    where tp_disparo_item = '" . $_GET['ListaItensAtivos'] . "' and bloqueio_disparo_item = 0 order by B.id_disparo_auto";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $rows = array();
        $rows['id_disparo_item'] = utf8_encode($RFP['id_disparo_item']);
        $rows['id_disparo_auto'] = utf8_encode($RFP['id_disparo_auto']);
        $rows['dias_tolerancia_item'] = utf8_encode($RFP['dias_tolerancia_item']);
        $rows['id_email_item'] = utf8_encode($RFP['id_email_item']);
        $rows['id_user_resp_item'] = utf8_encode($RFP['id_user_resp_item']);
        $rows['loja_disparo_auto'] = utf8_encode($RFP['loja_disparo_auto']);
        $records[] = $rows;
    }
    print(json_encode($records));
    odbc_close($con);
    exit();
}

if (isset($_GET['EnviaEmails'])) {
    include "ws_querysDisparos.php";
    $posItem = array_search($_GET['EnviaEmails'], array_column($arrayQuery, 'id'));
    $query = str_replace("@dias_tolerancia_item", $_GET['DiasTolerancia'], $arrayQuery[$posItem]["query"]);
    $query = str_replace("@lojaDisparoAuto", (isset($_GET['lojaDisparoAuto']) && $_GET['lojaDisparoAuto'] > 0 ? "B.empresa = " . $_GET['lojaDisparoAuto'] . " and " : ""), $query);
    $query = str_replace("@id_pessoa", (isset($_GET['id_pessoa']) && $_GET['id_pessoa'] > 0 ? "B.id_fornecedores_despesas = " . $_GET['id_pessoa'] . " and " : ""), $query);    
    $idEmail = $_GET['IdModeloEmail'];
    $idUser = $_GET['IdUserRespEmail'];
    $cur = odbc_exec($con, $query);
    $emailRepeticao = "";
    $itemAtual = 1;
    $qtdSucesso = 0;
    $totalItens = 0;
    $ListSend = array();
    while ($RFP = odbc_fetch_array($cur)) {
        if (trim($RFP['email']) !== "") {
            $ListSend[] = array($RFP['email'], $RFP['id_fornecedores_despesas'], $RFP['id_mens']);
        }
    }     
    for($i = 0; $i < count($ListSend); $i++) {
        $totalItens = $totalItens + 1;
        if ($itemAtual < 2) {
            $emailrepeticao = $emailrepeticao . (trim($emailrepeticao) !== "" ? ("," . $ListSend[$i][0] . "|" . $ListSend[$i][1] . "||" . $ListSend[$i][2]) : ($ListSend[$i][0] . "|" . $ListSend[$i][1] . "||" . $ListSend[$i][2]));
            $itemAtual = $itemAtual + 1;                
        } else {
            $emailrepeticao = $emailrepeticao . (trim($emailrepeticao) !== "" ? ("," . $ListSend[$i][0] . "|" . $ListSend[$i][1] . "||" . $ListSend[$i][2]) : ($ListSend[$i][0] . "|" . $ListSend[$i][1] . "||" . $ListSend[$i][2]));
            $qtdSucesso = $qtdSucesso + enviaLoteEmail($hostname, $database, $username, $password, $contrato, $idEmail, $idUser, $emailrepeticao);
            $itemAtual = 1;
            $emailrepeticao = "";
        }            
    }
    if(strlen($emailrepeticao) > 0) {
        $qtdSucesso = $qtdSucesso + enviaLoteEmail($hostname, $database, $username, $password, $contrato, $idEmail, $idUser, $emailrepeticao); //envia ultimo lote
    }
    $queryupdt = "set dateformat dmy; update sf_disparo_auto_item set dt_ultimo_disparo_item = GETDATE() where id_disparo_item = " . $_GET['IdItem'];
    odbc_exec($con, $queryupdt) or die(odbc_errormsg());
    trataResultado($qtdSucesso, $totalItens, "");
}

function enviaLoteEmail($hostname, $database, $username, $password, $contrato, $idEmail, $idUser, $emailrepeticao) {
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
    $dadosEmails = "?btnEnviar=S" .
            "&hostname=" . $hostname .
            "&database=" . $database .
            "&username=" . $username .
            "&password=" . $password .
            "&txtContrato=" . $contrato .
            "&txtId=" . $idEmail .
            "&txtUser=" . $idUser .
            "&txtEmails=" . $emailrepeticao;
    $jsonurl = $url . "/modulos/CRM/form/EmailsEnvioFormServer.php" . $dadosEmails;    
    $json = file_get_contents($jsonurl);
    $obj = json_decode($json, true);
    return $obj[0]["qtd_sucesso"];
}

if (isset($_GET['EnviaSMS'])) {
    include "ws_querysDisparos.php";
    $posItem = array_search($_GET['EnviaSMS'], array_column($arrayQuery, 'id'));
    $query = str_replace("@dias_tolerancia_item", $_GET['DiasTolerancia'], $arrayQuery[$posItem]["query"]);
    $query = str_replace("@lojaDisparoAuto", (isset($_GET['lojaDisparoAuto']) && $_GET['lojaDisparoAuto'] > 0 ? "B.empresa = " . $_GET['lojaDisparoAuto'] . " and " : ""), $query);
    $query = str_replace("@id_pessoa", (isset($_GET['id_pessoa']) && $_GET['id_pessoa'] > 0 ? "B.id_fornecedores_despesas = " . $_GET['id_pessoa'] . " and " : ""), $query);    
    $idEmail = $_GET['IdModelo'];  
    $idUser = $_GET['IdUserResp'];       
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
    $dadosSaldoSms = "?hostname=" . $hostname .
            "&database=" . $database .
            "&username=" . $username .
            "&password=" . $password .
            "&txtContrato=" . $contrato;
    $jsonurl = $url . "/util/sms/SmsSaldoForm.php" . $dadosSaldoSms;
    $jsonSaldo = file_get_contents($jsonurl);
    if (!is_numeric($jsonSaldo)) {
        trataResultado(0, 0, "Erro ao consultar Saldo");
    }
    $cur = odbc_exec($con, $query);
    $totalEnviar = odbc_num_rows($cur);
    if ($totalEnviar > $jsonSaldo) {
        trataResultado(0, 0, "Saldo insuficiente!");
    }
    $totalItens = 0;
    $qtdSucesso = 0;
    while ($RFP = odbc_fetch_array($cur)) {
        if (trim($RFP['celular']) !== "") {
            $totalItens = $totalItens + 1;
            if ($itemAtual < 2) {
                $smsrepeticao = $smsrepeticao . (trim($smsrepeticao) !== "" ? ("," . $RFP['celular'] . "|" . $RFP['id_fornecedores_despesas'] . "||" . $RFP['id_mens']) : ($RFP['celular'] . "|" . $RFP['id_fornecedores_despesas'] . "||" . $RFP['id_mens']));
                $itemAtual = $itemAtual + 1;
            } else {
                $smsrepeticao = $smsrepeticao . (trim($smsrepeticao) !== "" ? ("," . $RFP['celular'] . "|" . $RFP['id_fornecedores_despesas'] . "||" . $RFP['id_mens']) : ($RFP['celular'] . "|" . $RFP['id_fornecedores_despesas'] . "||" . $RFP['id_mens']));
                $qtdSucesso = $qtdSucesso + enviaLoteSMS($hostname, $database, $username, $password, $smsrepeticao, $idEmail, $idUser);
                $itemAtual = 1;
                $smsrepeticao = "";
            }
        }
    }
    if(strlen($smsrepeticao) > 0) {
        $qtdSucesso = $qtdSucesso + enviaLoteSMS($hostname, $database, $username, $password, $smsrepeticao, $idEmail, $idUser); //envia ultimo lote
    }
    $queryupdt = "set dateformat dmy; update sf_disparo_auto_item set dt_ultimo_disparo_item = GETDATE() where id_disparo_item = " . $_GET['IdItem'];
    odbc_exec($con, $queryupdt) or die(odbc_errormsg());
    trataResultado($qtdSucesso, $totalItens, "");
}

function enviaLoteSMS($hostname, $database, $username, $password, $smsrepeticao, $idEmail, $idUser) {
    $dadosSMS = "?btnEnviar=S" .
            "&hostname=" . $hostname .
            "&database=" . $database .
            "&username=" . $username .
            "&password=" . $password .
            "&txtId=" . $idEmail .            
            "&txtUser=" . $idUser .
            "&txtSendSms=" . str_replace(array(" ", "(", ")", "-"), "", $smsrepeticao);
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];    
    $jsonurl = $url . "/util/sms/send_sms.php" . $dadosSMS;
    $jsonSendSms = file_get_contents($jsonurl);
    $obj = json_decode($jsonSendSms, true);
    return $obj[0]["qtd_sucesso"];
}

if (isset($_GET['EnviaWhatsapp'])) {
    include "ws_querysDisparos.php";
    $posItem = array_search($_GET['EnviaWhatsapp'], array_column($arrayQuery, 'id'));
    $query = str_replace("@dias_tolerancia_item", $_GET['DiasTolerancia'], $arrayQuery[$posItem]["query"]);
    $query = str_replace("@lojaDisparoAuto", (isset($_GET['lojaDisparoAuto']) && $_GET['lojaDisparoAuto'] > 0 ? "B.empresa = " . $_GET['lojaDisparoAuto'] . " and " : ""), $query);
    $query = str_replace("@id_pessoa", (isset($_GET['id_pessoa']) && $_GET['id_pessoa'] > 0 ? "B.id_fornecedores_despesas = " . $_GET['id_pessoa'] . " and " : ""), $query);
    $idEmail = $_GET['IdModelo'];
    $idUser = $_GET['IdUserResp'];    
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
    $dadosSaldoWhatsapp = "?hostname=" . $hostname .
            "&database=" . $database .
            "&username=" . $username .
            "&password=" . $password .
            "&tipo=1" .            
            "&txtContrato=" . $contrato;
    $jsonurl = $url . "/util/sms/SmsSaldoForm.php" . $dadosSaldoWhatsapp;
    $jsonSaldo = file_get_contents($jsonurl);
    if (!is_numeric($jsonSaldo)) {
        trataResultado(0, 0, "Erro ao consultar Saldo");
    } 
    //echo $query; exit;        
    $cur = odbc_exec($con, $query);
    $totalEnviar = odbc_num_rows($cur);
    if ($totalEnviar > $jsonSaldo) {
        trataResultado(0, 0, "Saldo insuficiente!");
    }    
    $totalItens = 0;
    $qtdSucesso = 0;
    while ($RFP = odbc_fetch_array($cur)) {
        if (trim($RFP['celular']) !== "") {
            $totalItens = $totalItens + 1;
            if ($itemAtual < 2) {
                $smsrepeticao = $smsrepeticao . (trim($smsrepeticao) !== "" ? ("," . $RFP['celular'] . "|" . $RFP['id_fornecedores_despesas'] . "||" . $RFP['id_mens']) : ($RFP['celular'] . "|" . $RFP['id_fornecedores_despesas'] . "||" . $RFP['id_mens']));
                $itemAtual = $itemAtual + 1;
            } else {
                $smsrepeticao = $smsrepeticao . (trim($smsrepeticao) !== "" ? ("," . $RFP['celular'] . "|" . $RFP['id_fornecedores_despesas'] . "||" . $RFP['id_mens']) : ($RFP['celular'] . "|" . $RFP['id_fornecedores_despesas'] . "||" . $RFP['id_mens']));
                $qtdSucesso = $qtdSucesso + enviaLoteWhatsapp($hostname, $database, $username, $password, $smsrepeticao, $idEmail, $idUser);
                $itemAtual = 1;
                $smsrepeticao = "";
            }
        }
    }
    if(strlen($smsrepeticao) > 0) {    
        $qtdSucesso = $qtdSucesso + enviaLoteWhatsapp($hostname, $database, $username, $password, $smsrepeticao, $idEmail, $idUser); //envia ultimo lote
    }
    $queryupdt = "set dateformat dmy; update sf_disparo_auto_item set dt_ultimo_disparo_item = GETDATE() where id_disparo_item = " . $_GET['IdItem'];
    odbc_exec($con, $queryupdt) or die(odbc_errormsg());
    trataResultado($qtdSucesso, $totalItens, "");
}

function enviaLoteWhatsapp($hostname, $database, $username, $password, $smsrepeticao, $idEmail, $idUser) {
    $dadosWhatsapp = "?btnEnviar=S" . 
            "&txtTipo=1" .
            "&hostname=" . $hostname .
            "&database=" . $database .
            "&username=" . $username .
            "&password=" . $password .
            "&txtId=" . $idEmail .            
            "&txtUser=" . $idUser .
            "&txtSendSms=" . str_replace(array(" ", "(", ")", "-"), "", $smsrepeticao);
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];    
    $jsonurl = $url . "/util/sms/send_sms.php" . $dadosWhatsapp;
    $jsonSendWhatsapp = file_get_contents($jsonurl);
    $obj = json_decode($jsonSendWhatsapp, true);
    return $obj[0]["qtd_sucesso"];
}

function trataResultado($qtdSucesso, $totalItens, $msg) {
    $rows['qtdSucesso'] = $qtdSucesso;
    $rows['qtdErro'] = $totalItens - $qtdSucesso;
    $rows['msgErro'] = $msg;
    print(json_encode($rows));
    exit();
}

if (isset($_GET['EnviaLigacao'])) {
    include "ws_querysDisparos.php";
    $posItem = array_search($_GET['EnviaLigacao'], array_column($arrayQuery, 'id'));
    $query = str_replace("@dias_tolerancia_item", $_GET['DiasTolerancia'], $arrayQuery[$posItem]["query"]);
    $query = str_replace("@lojaDisparoAuto", (isset($_GET['lojaDisparoAuto']) && $_GET['lojaDisparoAuto'] > 0 ? "B.empresa = " . $_GET['lojaDisparoAuto'] . " and " : ""), $query);
    $query = str_replace("@id_pessoa", (isset($_GET['id_pessoa']) && $_GET['id_pessoa'] > 0 ? "B.id_fornecedores_despesas = " . $_GET['id_pessoa'] . " and " : ""), $query);    
    $queryTexto = " select texto_item from sf_disparo_auto_item where id_disparo_item = " . $_GET['IdItem'];
    $cur = odbc_exec($con, $queryTexto);
    while ($RFP = odbc_fetch_array($cur)) {
        $textoItem = $RFP['texto_item'];
    }
    if ($textoItem == "") {
        trataResultado(0, 0, "Erro ao ler Item");
    }
    $qtdSucesso = 0;
    $totalItens = 0;
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $id_user_resp = $RFP['id_user_resp'] == "" ? $_GET['IdUserResp'] : $RFP['id_user_resp'];
        $arrayInsert[] = "insert into sf_telemarketing(de_pessoa,para_pessoa,data_tele,pendente,proximo_contato,obs,pessoa,atendimento_servico,relato,departamento,dt_inclusao)
        values(1," . $id_user_resp . ",GETDATE(),1,GETDATE(),'DISPARO AUTOMATICO'," . $RFP['id_fornecedores_despesas'] . ",0,'" . $textoItem . "',null,getdate());";
    }
    for ($i = 0; $i < count($arrayInsert); $i++) {
        $totalItens = $totalItens + 1;
        $objExec = odbc_exec($con, $arrayInsert[$i]) or die(odbc_errormsg());
        if ($objExec) {
            $qtdSucesso = $qtdSucesso + 1;
        }
    }
    $queryupdt = "set dateformat dmy; update sf_disparo_auto_item set dt_ultimo_disparo_item = GETDATE() where id_disparo_item = " . $_GET['IdItem'];
    odbc_exec($con, $queryupdt) or die(odbc_errormsg());
    trataResultado($qtdSucesso, $totalItens, "");
}

if (isset($_GET['DesmatricularTurma'])) {
    $qtdSucesso = 0;
    $totalItens = 0;
    $turmas = "";
    $arrayMark = "";
    $query = "set dateformat dmy;
    select A.id_turma, id_fornecedores_despesas, A.id_plano, B.descricao from dbo.sf_fornecedores_despesas_turmas A 
    inner join sf_turmas B on A.id_turma = B.id_turma
    inner join sf_vendas_planos C on A.id_plano = C.id_plano
    and desmatricular = 1 and DATEADD(day,dias_desmatricula,dbo.FU_PLANO_FIM(C.id_plano)) <= cast(GETDATE() as date)";
    $cur = odbc_exec($con, $query);
    $arrayInsert = "set dateformat dmy;";
    while ($RFP = odbc_fetch_array($cur)) {
        $totalItens += 1;
        $qtdSucesso += 1;
        $turmas .= $RFP['id_turma'] . ",";
        $arrayInsert .= "delete from sf_fornecedores_despesas_turmas where id_fornecedores_despesas = " . $RFP['id_fornecedores_despesas'] . " and id_turma = " . $RFP['id_turma'] . " and id_plano = " . $RFP['id_plano'] . ";
        insert into sf_turmas_desmatriculas (data, id_fornecedores_despesas, id_turma, descricao_turma, motivo, login_user) values (getdate(), " . $RFP['id_fornecedores_despesas'] . ", " . $RFP['id_turma'] . ", '" . utf8_decode($RFP['descricao']) . "', 'DESMATRICULA TURMA AUTOMATICO - Plano: " . $RFP['id_plano'] . "', 'ADMIN');";
    }
    if ($arrayInsert !== "") {
        odbc_exec($con, $arrayInsert);
    }
    if ($turmas !== "") {
        $turmas = substr_replace($turmas, "", -2);
        $query = "select top " . $qtdSucesso . " id_user_resp, A.id_fornecedores_despesas, C.descricao from dbo.sf_turmas_espera A 
        inner join sf_fornecedores_despesas B on A.id_fornecedores_despesas = B.id_fornecedores_despesas
        inner join sf_turmas C on A.id_turma = C.id_turma
        where A.id_turma in(" . $turmas . ") and id_user_resp is not null order by data_pedido";
        $cur = odbc_exec($con, $query);
        while ($RFP = odbc_fetch_array($cur)) {
            $arrayMark .= "insert into sf_telemarketing(de_pessoa,para_pessoa,data_tele,pendente,proximo_contato,obs,pessoa,atendimento_servico,relato,departamento,dt_inclusao)
            values(1," . valoresSelect2($RFP['id_user_resp']) . ",GETDATE(),1,GETDATE(),'DISPARO AUTOMATICO'," . $RFP['id_fornecedores_despesas'] . ",0,'VAGA TURMA: " . utf8_decode($RFP['descricao']) . "',null,getdate());";
        }
        if ($arrayMark !== "") {
            odbc_exec($con, $arrayMark);
        }
    }
    trataResultado($qtdSucesso, $totalItens, "");
}

if (isset($_GET['DesmatricularParentesco'])) {
    $qtdSucesso = 0;
    $totalItens = 0;
    $arrayInsert = "";    
    $query = "set dateformat dmy;
    select id_dependente, id_titular, id_user_resp, nome_parentesco,
    (select razao_social from sf_fornecedores_despesas where id_fornecedores_despesas = id_titular) titular, 0 id_credencial
    from sf_fornecedores_despesas_dependentes inner join sf_fornecedores_despesas on id_fornecedores_despesas = id_dependente
    inner join sf_parentesco on parentesco = id_parentesco where isnull(idade_parentesco,0) > 0 
    and FLOOR(DATEDIFF(MONTH, DATEADD(day, (select ACA_DIAS_TOLERANCIA_PARENTESCO from sf_configuracao), data_nascimento), GETDATE()) / 12) >= idade_parentesco
    union
    select id_dependente, id_titular, id_user_resp, nome_parentesco,
    (select razao_social from sf_fornecedores_despesas where id_fornecedores_despesas = id_titular) titular, cr.id_fornecedores_despesas_credencial
    from sf_fornecedores_despesas_dependentes 
    inner join sf_fornecedores_despesas c on id_fornecedores_despesas = id_dependente
    inner join sf_parentesco on parentesco = id_parentesco 
    inner join sf_fornecedores_despesas_credenciais cr on cr.id_fornecedores_despesas = c.id_fornecedores_despesas
    inner join sf_credenciais cd on cd.id_credencial = cr.id_credencial
    where cr.dt_cancelamento is null and cd.del_dep_venc = 1 and dt_fim < cast(getdate() as date)";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $totalItens += 1;
        $qtdSucesso += 1;   
        $arrayInsert .= "update sf_fornecedores_despesas_credenciais set dt_cancelamento = getdate(), usuario_canc = 1 where id_fornecedores_despesas_credencial = " . $RFP['id_credencial'] . ";";
        $arrayInsert .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values ('sf_fornecedores_despesas_dependentes', " . $RFP['id_dependente'] . ", 'SISTEMA',
        'E', 'EXCLUSAO DO DEPENDENTE " . $RFP['id_dependente'] . " - ' + (select max(razao_social) from sf_fornecedores_despesas where id_fornecedores_despesas = " . $RFP['id_dependente'] . "), GETDATE()," . $RFP['id_titular'] . ");";
        $arrayInsert .= "delete from sf_fornecedores_despesas_dependentes where id_dependente = " . $RFP['id_dependente'] . " and id_titular = " . $RFP['id_titular'] . ";";
        $arrayInsert .= "insert into sf_telemarketing(de_pessoa,para_pessoa,data_tele,pendente,proximo_contato,obs,pessoa,atendimento_servico,relato,departamento,dt_inclusao)
        values(1," . valoresSelect2($RFP['id_user_resp']) . ",GETDATE(),1,GETDATE(),'DISPARO AUTOMATICO'," . $RFP['id_dependente'] . ",0,'CANCELAMENTO DE PARENTESCO: " . 
        utf8_decode($RFP['nome_parentesco']) . " de " . $RFP['id_titular'] . " - " . utf8_decode($RFP['titular']) . "',null,getdate());";
    }    
    if ($arrayInsert !== "") {
        odbc_exec($con, "set dateformat dmy;" . $arrayInsert);
    }
    trataResultado($qtdSucesso, $totalItens, "");
}

if (isset($_GET['CancelamentoPlanos'])) {
    $qtdSucesso = 0;
    $totalItens = 0;
    $arrayInsert = "";    
    $query = "select id_plano from sf_vendas_planos where (select aca_dias_cancelamento from sf_configuracao) > 0 and dt_cancelamento is null 
    and planos_status not in ('Novo') and dateadd(day, (select aca_dias_cancelamento from sf_configuracao), dbo.FU_PLANO_FIM(id_plano)) < getdate()";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $totalItens += 1;
        $qtdSucesso += 1;
        $arrayInsert .= "update sf_vendas_planos set dt_cancelamento = GETDATE(), usuario_canc = 1, obs_cancelamento = 'CANCELAMENTO PROGRAMADO', 
        planos_status = 'Cancelado' where id_plano = " . $RFP['id_plano'] . ";";
        $arrayInsert .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        select 'sf_vendas_planos', id_plano, 'SISTEMA', 'C', 'CANCELAMENTO DE PLANO (' + (select descricao from sf_produtos t where t.conta_produto = ft.id_prod_plano) + ')', GETDATE(), favorecido 
        from sf_vendas_planos ft where id_plano = " . $RFP['id_plano'] . ";";        
    }    
    if ($arrayInsert !== "") {
        odbc_exec($con, $arrayInsert);
    }
    trataResultado($qtdSucesso, $totalItens, "");
}

if (isset($_GET['EnviaAssistencia24h'])) {
    $qtdSucesso = 0;
    $totalItens = 0;    
    $idRefreshWa = array();
    $cur = odbc_exec($con, "select id, id_externo from sf_fornecedores_despesas_veiculo 
    inner join sf_vendas_planos on id_veiculo = id
    inner join sf_fornecedores_despesas on favorecido = sf_fornecedores_despesas.id_fornecedores_despesas
    where status = 'Aprovado' and ((id_externo is null and planos_status in ('Ativo') and fornecedores_status not in ('AtivoEmAberto')) or (id_externo is not null and fornecedores_status in ('AtivoEmAberto')) or (id_externo is not null and planos_status not in ('Ativo')
    and id_veiculo not in (select x.id_veiculo from sf_vendas_planos x where x.id_veiculo is not null and x.planos_status in ('Ativo'))
    and datediff(day,dbo.FU_PLANO_FIM(id_plano), getdate()) > (select ACA_TOL_ASSISTENCIA from sf_configuracao)))
    and id in (select id_veiculo from sf_vistorias where data_aprov is not null)
    union select id, id_externo from sf_fornecedores_despesas_veiculo where status = 'Reprovado' and id_externo is not null") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $idRefreshWa[] = array($RFP["id"], $RFP["id_externo"]);        
    }
    foreach ($idRefreshWa as $item) {
        $totalItens += 1;
        $qtdSucesso += enviaAssistencia24h($hostname, $database, $username, $password, $contrato, $item[0], $item[1]);
    }        
    trataResultado($qtdSucesso, $totalItens, "");
}

function enviaAssistencia24h($hostname, $database, $username, $password, $contrato, $id, $id_externo) {
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
    $dadosEmails = "?functionPage=" . (is_numeric($id_externo) ? "alterarVeiculo&excluir=S" : "criarNovoVeiculo") .
            "&hostname=" . $hostname .
            "&database=" . $database .
            "&username=" . $username .
            "&password=" . $password .
            "&txtContrato=" . $contrato .            
            "&id_veiculo=" . $id;
    $jsonurl = $url . "/modulos/Seguro/ajax/assistencia.php" . $dadosEmails;    
    $json = file_get_contents($jsonurl);
    return ($json == "OK" ? 1 : 0);
}

if (isset($_GET['EnviaRastreamento'])) {
    $qtdSucesso = 0;
    $totalItens = 0;    
    $idRefreshWa = array();
    $cur = odbc_exec($con, "select id, id_externo_rast, status_ext_veic from sf_fornecedores_despesas_veiculo 
    inner join sf_vendas_planos on id_veiculo = id
    inner join sf_fornecedores_despesas on favorecido = sf_fornecedores_despesas.id_fornecedores_despesas
    where status = 'Aprovado' and (
    (id_externo_rast is null and planos_status in ('Ativo') and fornecedores_status not in ('AtivoEmAberto')) or 
    (id_externo_rast is not null and status_ext_veic = 'N' and planos_status in ('Ativo') and fornecedores_status not in ('AtivoEmAberto')) or 
    (id_externo_rast is not null and status_ext_veic = 'S' and fornecedores_status in ('AtivoEmAberto')) or 
    (id_externo_rast is not null and status_ext_veic = 'S' and planos_status not in ('Ativo') and id_veiculo not in (select x.id_veiculo from sf_vendas_planos x where x.id_veiculo is not null and x.planos_status in ('Ativo')) 
    and datediff(day,dbo.FU_PLANO_FIM(id_plano), getdate()) > (select ACA_TOL_ASSISTENCIA from sf_configuracao)))
    and id in (select id_veiculo from sf_vistorias where data_aprov is not null)
    and id_rastreador is not null and id_operadora is not null and len(imei) > 0 and len(chip) > 0
    union select id, id_externo_rast, status_ext_veic from sf_fornecedores_despesas_veiculo where status = 'Reprovado' and id_externo_rast is not null and status_ext_veic = 'S'") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $idRefreshWa[] = array($RFP["id"], $RFP["id_externo_rast"], $RFP["status_ext_veic"]);        
    }
    foreach ($idRefreshWa as $item) {
        $totalItens += 1;
        $qtdSucesso += enviaRastreamento($hostname, $database, $username, $password, $contrato, $item[0], $item[1], $item[2]);
    }        
    trataResultado($qtdSucesso, $totalItens, "");
}

function enviaRastreamento($hostname, $database, $username, $password, $contrato, $id, $id_externo, $status_ext) {
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
    $dadosEmails = "?functionPage=" . (is_numeric($id_externo) && $status_ext == "S" ? "alterarVeiculo&excluir=S" : "criarNovoVeiculo") .
            "&hostname=" . $hostname .
            "&database=" . $database .
            "&username=" . $username .
            "&password=" . $password .
            "&txtContrato=" . $contrato .            
            "&id_veiculo=" . $id;
    $jsonurl = $url . "/modulos/Seguro/ajax/rastreamento.php" . $dadosEmails;    
    $json = file_get_contents($jsonurl);
    return ($json == "OK" ? 1 : 0);
}

odbc_close($con);