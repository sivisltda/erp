<?php
include '../../Connections/configini.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Sivis<small>Gerar Senha</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="float:left;width: 10%;margin-top: 15px;">
                                    <button class="button button-green btn-primary" type="button" onClick="AbrirBox(0)"><span class="ico-file-4 icon-white"></span></button>
                                    <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir()" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                </div>
                                <div style="float:right;width: 57%;">   
                                    <div style="float:left;margin-left: 1%;width: 14%;">
                                        <div width="100%">De:</div>
                                        <input type="text" style="width: 79px;vertical-align: top;" id="txt_dt_begin" class="datepicker" value="<?php echo getData("T"); ?>" placeholder="Data inicial">
                                    </div>
                                    <div style="float:left;margin-left: 1%;width: 14%;">
                                        <div width="100%">Até:</div>
                                        <input type="text" style="width: 79px;vertical-align: top;" id="txt_dt_end" class="datepicker" value="<?php echo getData("T"); ?>" placeholder="Data Final">
                                    </div>
                                    <div style="float:left;margin-left: 1%;width: 22%;">                                    
                                        <div width="100%">Sistema:</div>
                                        <select id="txtSistema">
                                            <option value="null">Selecione</option>                                        
                                            <option value="G">GYM</option>                                        
                                            <option value="B">BODY</option>                                        
                                            <option value="T">TRAINING</option>                                        
                                        </select>                                           
                                    </div>                                                                           
                                    <div style="float:left;margin-left: 1%;width: 35%;">
                                        <div width="100%">Busca:</div>
                                        <input id="txtBusca" maxlength="100" type="text" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title=""/>
                                    </div>
                                    <div style="float:left;margin-left: 1%;width: 7%;margin-top: 15px;">
                                        <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Gerar Senha</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblLista">
                            <thead>
                                <tr>
                                    <th width="12%">Data</th>
                                    <th width="5%">Cód.</th>
                                    <th width="43%">Nome Fantasia/Razão Social/Nome:</th>                                               
                                    <th width="10%">Sistema:</th>                                                                                                
                                    <th width="10%">Plugue:</th>                                                                                                                                            
                                    <th width="10%"><center>Versão:</center></th> 
                                    <th width="10%"><center>Operador:</center></th> 
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>         
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>   
        <script type='text/javascript' src='../../js/plugins.js'></script>        
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type='text/javascript' src='../../js/plugins/highlight/jquery.highlight-4.js'></script>        
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>            
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>   
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>    
        <script type="text/javascript">
            
            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
            
            $(document).ready(function () {
                $("#btnfind").click(function () {
                    tbLista.fnReloadAjax(finalFind(0));
                });                                
            });

            function finalFind(imp) {
                var retPrint = (imp === 1 ? '&imp=1' : '?imp=0');   
                if ($('#txt_dt_begin').val() !== ""){
                    retPrint = retPrint + "&dti=" + $('#txt_dt_begin').val().replace(/\//g, "_"); 
                }
                if ($('#txt_dt_end').val() !== ""){
                    retPrint = retPrint + "&dtf=" + $('#txt_dt_end').val().replace(/\//g, "_"); 
                }
                if ($("#txtSistema").val() !== "null") {
                    retPrint = retPrint + '&fq=' + $("#txtSistema").val();
                }
                if ($("#txtBusca").val() !== "") {
                    retPrint = retPrint + '&Search=' + $("#txtBusca").val();
                }
                return "GerarSenha_server_processing.php" + retPrint.replace(/\//g, "_");
            }

            function AbrirBox(id) {
                abrirTelaBox("FormGerarSenha.php" + (id > 0 ? "?id=" + id : ""), 350, 440);
            }

            function FecharBox() {
                $("#newbox").remove();
                tbLista.fnReloadAjax(finalFind(0));
            }

            function imprimir() {
                var pRel = "&NomeArq=" + "Gerar Senha" +
                "&lbl=Data|Cod|Nome|Sistema|Plugue|Versão|Operador" +
                "&siz=80|40|260|80|80|80|80" +
                "&pdf=7" + // Colunas do server processing que não irão aparecer no pdf
                "&filter=" + //Label que irá aparecer os parametros de filtro
                "&PathArqInclude=" + "../Modulos/Sivis/" + finalFind(1); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }

            var tbLista = $('#tblLista').dataTable({
                "iDisplayLength": 20,
                "aLengthMenu": [20, 30, 40, 50, 100],
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "aaSorting": [[0,'desc']],
                "aoColumns": [
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true}],
                "sAjaxSource": finalFind(0),
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"},
                "sPaginationType": "full_numbers"
            });
                        
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }
        </script>        
        <?php odbc_close($con); ?>
    </body>
</html>