<?php
include __DIR__ . "./../../Connections/funcoesAux.php";
include __DIR__ . "./../../util/util.php";  

$msg = "Link Inválido!";

if (isset($_GET["link"])) {
    $link_go = encrypt($_GET["link"], "VipService123", false);
    $itens = explode("|", $link_go);
    if (count($itens) == 5 && is_numeric($itens[1])) {
        $hostname2 = "148.72.177.101,6000";
        $database2 = "erp" . $itens[1];
        $username2 = "erp" . $itens[1];
        $password2 = "!Password123";
        $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname2 . "; DATABASE=" . $database2 . ";", $username2, $password2)or die(odbc_errormsg());
        $cur = odbc_exec($con, "select 
        (select count(*) from sf_login_white where ip = " . valoresTexto2($itens[0]) . ") tot_white,
        (select count(*) from sf_login_black where ip = " . valoresTexto2($itens[0]) . ") tot_black");
        while ($RFP = odbc_fetch_array($cur)) {
            if ($RFP['tot_black'] > 0) {
                $msg = "Ip Inválido!";
            } else if ($RFP['tot_white'] > 0) {
                $msg = "Ip Liberado!";
            } else {
                $msg = "";
            }
        }
    }
}

if ($msg == "" && $itens[2] == "B") {
    odbc_exec($con, "insert into sf_login_black (descricao, data_cad, ip) values ('Link WhatsApp', getdate()," .
                    valoresTexto2($itens[0]) . ");") or die(odbc_errormsg());
    $msg = "Ip Bloqueado com Sucesso!";
}

if ($msg == "" && $itens[2] == "L" && isset($_POST["btnSave"])) {
    if (strlen($_POST["image-tag"]) > 0 && strlen($_POST["latitude"]) > 0 && strlen($_POST["longitude"]) > 0) {
        $pasta = "../../Pessoas/" . $itens[1] . "/Ips/";
        if (!is_dir($pasta)) {
            mkdir($pasta);
        }
        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $_POST["image-tag"]));
        file_put_contents($pasta . str_replace(".", "_", $itens[0]) . ".jpg", $data);
        odbc_exec($con, "insert into sf_login_white (descricao, data_cad, ip, id_usuario, latitude, longitude) values ('Link WhatsApp', getdate()," .
                        valoresTexto2($itens[0]) . "," . valoresSelect2($itens[3]) . "," .
                        valoresTexto("latitude") . "," . valoresTexto("longitude") . ");") or die(odbc_errormsg());
        $msg = "Ip Liberado com Sucesso!";
    } else {
        $msg = "Dados Inválidos!";
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Confirmação de Rede</title>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
        <script type="text/javascript" src="../../js/leaflet/leaflet.js"></script>        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
        <link href="../../js/leaflet/leaflet.css" rel="stylesheet" type="text/css"/>        
        <style type="text/css">
            .quadro { 
                min-height: 390px;
                width: 330px;
                border:1px solid; 
                background:#ccc; 
            }
            .botao {
                width: 100%;
                height: 100px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <?php if ($msg == "") { ?>
                <h1 class="text-center">IP <?php echo $itens[0]; ?></h1>
                <form method="POST" action="ws_security.php?link=<?php echo $_GET["link"]; ?>">
                    <div class="row">
                        <input id="image-tag" name="image-tag" type="hidden">
                        <input id="latitude" name="latitude" type="hidden">
                        <input id="longitude" name="longitude" type="hidden">
                        <div class="col-md-6 d-flex justify-content-center" style="text-align: center;">                       
                            <div id="my_camera" class="quadro"></div><br/>
                        </div>
                        <div class="col-md-6 d-flex justify-content-center">
                            <div id="results" class="quadro d-flex align-items-center justify-content-center">Clique no botão Capturar</div>
                        </div>               
                    </div>                    
                    <div class="row mt-4">
                        <div class="col-md-12" style="text-align: center;">
                            <button id="btnCapturar" type="button" class="btn botao btn-success" onclick="take_snapshot();"><h1>Capturar</h1></button>
                        </div>                    
                    </div>                    
                    <div class="row mt-4 justify-content-center">
                        <div id="map" style="width: 600px; height: 400px;"></div>
                    </div>                    
                    <div class="row mt-4">                    
                        <div class="col-md-12" style="text-align: center;">
                            <button id="btnSave" name="btnSave" type="submit" class="btn botao" disabled><h1>Salvar</h1></button>
                        </div>
                    </div>
                </form>
            <?php } else { ?>
                <h1 class="text-center"><?php echo $msg; ?></h1>
            <?php } ?>
        </div>
        <?php if ($msg == "") { ?>
            <script language="JavaScript">
                Webcam.set({
                    height: 390,
                    crop_width: 390,
                    image_format: 'jpeg',
                    jpeg_quality: 90
                });
                Webcam.attach('#my_camera');

                function take_snapshot() {
                    Webcam.snap(function (data_uri) {
                        $("#image-tag").val(data_uri);
                        $("#results").html('<img " src="' + data_uri + '"/>');
                        $("#btnCapturar").removeClass("btn-success");
                        $("#btnSave").addClass("btn-success");
                        $("#btnSave").prop("disabled", false);
                        window.scrollTo(0, document.body.scrollHeight);
                    });
                }

                const map = L.map('map').fitWorld();
                L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {maxZoom: 19, attribution: ''}).addTo(map);
                map.locate({setView: true, maxZoom: 16});

                function onLocationFound(e) {
                    var radius = e.accuracy;
                    L.marker(e.latlng).addTo(map).bindPopup("Seu Local").openPopup();
                    L.circle(e.latlng, radius).addTo(map);
                    $("#latitude").val(e.latitude);
                    $("#longitude").val(e.longitude);
                }
                map.on('locationfound', onLocationFound);

                function onLocationError(e) {
                    alert(e.message);
                }
                map.on('locationerror', onLocationError);

                $("form").on("submit", function (event) {
                    if ($("#image-tag").val() === "" || $("#latitude").val() === "" || $("longitude").val() === "") {
                        alert('Dados inválidos!');
                        event.preventDefault();
                    } else {
                        return;
                    }
                });

            </script>
        <?php } ?>
    </body>
</html>