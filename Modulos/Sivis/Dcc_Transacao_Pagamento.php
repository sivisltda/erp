<?php

if (!is_numeric($_GET["Trn"])) {
    echo "Erro Parâmetros!";
    exit;
} else {
    $empresa = isset($_GET["Emp"]) ? str_pad($_GET["Emp"], 3, '0', STR_PAD_LEFT) : "";
    $transacao = $_GET["Trn"];
}

$login_usuario = 'SISTEMA';
$id_mens = 0;
$tipo_documento = 0;
$somar_dias = 0;
$valorAdesao = 0;
$valorAcrecimo = 0;
$valorAuto = 0;
$idServicoAcrecimo = 0; 
$servico_adesao = 0;
$id_fornecedores_despesas = 0;
$bandeira = 0;

$hostname = $_GET['hostname'];
$database = $_GET['database'];
$username = $_GET['username'];
$password = $_GET['password'];
$contrato = str_replace("ERP", "", strtoupper($_GET['database']));
$adesao_soma = isset($_GET['adesaoSoma']) ? $_GET['adesaoSoma'] : false;

$con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
require_once(__DIR__ . '/../../Connections/funcoesAux.php');

$cur = odbc_exec($con, "select ACA_SERVICO_ADESAO from sf_configuracao");
while ($RFP = odbc_fetch_array($cur)) {
    $servico_adesao = $RFP['ACA_SERVICO_ADESAO'];
}

if (isset($_SESSION["login_usuario"]) && strlen($_SESSION["login_usuario"]) > 0) {
    $login_usuario = strtoupper($_SESSION["login_usuario"]);
} elseif (isset($_GET["login_usuario"]) && strlen($_GET["login_usuario"]) > 0) {
    $login_usuario = strtoupper($_GET["login_usuario"]);
}

$cur = odbc_exec($con, "select top 1 id_plano_item,id_tipo_documento,id_fornecedores_despesas,mkr_login,id_bandeira 
from sf_vendas_itens_dcc 
where id_venda_transacao is null and status_transacao = 'A' and id_transacao = '" . valoresSelect2($transacao) . "'");
while ($RFP = odbc_fetch_array($cur)) {
    $id_mens = $RFP['id_plano_item'];
    $tipo_documento = $RFP['id_tipo_documento'];
    $id_fornecedores_despesas = $RFP['id_fornecedores_despesas'];
    $login_usuario = strtoupper($RFP['mkr_login']);
    $bandeira = $RFP['id_bandeira'];
}

if (is_numeric($tipo_documento)) {
    $cur = odbc_exec($con, "select dias from sf_tipo_documento where id_tipo_documento = " . valoresSelect2($tipo_documento));
    while ($RFP = odbc_fetch_array($cur)) {
        $somar_dias = $RFP['dias'];
    }
}

if (is_numeric($id_mens) && intval($servico_adesao) > 0) {
    $cur = odbc_exec($con, "select conta_produto,preco_venda from sf_produtos where tipo = 'S' and inativa = 0 and conta_produto in (
    select top 1 id_servico from sf_vendas_planos vp
    inner join sf_produtos_acrescimo_servicos pa on pa.id_produto = vp.id_prod_plano
    and (select count(id_dependente) from sf_fornecedores_despesas_dependentes where id_titular = favorecido) between qtd_min and qtd_max
    inner join sf_produtos on conta_produto = vp.id_prod_plano
    inner join sf_vendas_planos_mensalidade on id_plano = id_plano_mens
    where id_mens = " . valoresSelect2($id_mens) . " and adesao_tipo = 1 and adesao_intervalo = month(getdate()) order by qtd_max desc)");
    while ($RFP = odbc_fetch_array($cur)) {
        $idServicoAcrecimo = $RFP['conta_produto'];
        $valorAcrecimo = $RFP['preco_venda'];
    }
}

if (isset($_GET['txtValorAde']) && valoresNumericos2($_GET['txtValorAde']) > 0) {
    $valorAdesao = valoresNumericos2($_GET['txtValorAde']);
}

if (is_numeric($id_mens) && is_numeric($transacao)) {
    
    odbc_exec($con, "EXEC dbo.SP_MK_COMISSAO_REGRAS @id_cliente = " . $id_fornecedores_despesas . ", @id_regra = 0;");    
    
    $historico = $adesao_soma ?  valoresTexto2('ABONO / PLANO') : valoresTexto2('PAG PLANO / SERVIÇO');
    $query = "set dateformat dmy;
    insert into sf_vendas (vendedor,cliente_venda,historico_venda,data_venda,sys_login,empresa,destinatario,status,tipo_documento,
    cov,descontop,descontos,descontotp,descontots,n_servicos,data_aprov,grupo_conta,conta_movimento,favorito,reservar_estoque, n_produtos)
    select " . valoresTexto2($login_usuario) . ",id_fornecedores_despesas," . $historico . ",transactionTimestamp, " 
    . valoresTexto2($login_usuario) . ",1," . valoresTexto2($login_usuario) . ",'Aprovado',id_tipo_documento,
    'V',0.00,0.00,0,0,0,transactionTimestamp,14,1,0,0,0 from sf_vendas_itens_dcc where id_venda_transacao is null and status_transacao = 'A' 
    and id_transacao = " . valoresSelect2($transacao) . "
    SELECT SCOPE_IDENTITY() ID;";   
    $result = odbc_exec($con, $query) or die(odbc_errormsg());
    odbc_next_result($result);
    $idVenda = odbc_result($result, 1);

    $valorTransacao = $adesao_soma ? '0.00' : "(valor_transacao - " . $valorAdesao . " - " . $valorAcrecimo . ")";
    $query = "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total,valor_bruto)
    select " . valoresSelect2($idVenda) . ",conta_movimento,id_produto,1, $valorTransacao valor_transacao,
    $valorTransacao valor_transacao from sf_vendas_itens_dcc 
    left join sf_produtos on sf_produtos.conta_produto = id_produto
    where id_venda_transacao is null and status_transacao = 'A' and id_transacao = " . valoresSelect2($transacao) . ";";

    if ($valorAdesao > 0) {
        $query .= "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total,valor_bruto)
        select " . $idVenda . ",conta_movimento,conta_produto,1," . ($valorAdesao - $valorAuto) . "," . ($valorAdesao - $valorAuto) . " from sf_produtos where inativa = 0 
        and conta_produto in (select conta_produto_adesao from sf_vendas_itens_dcc left join sf_produtos on sf_produtos.conta_produto = id_produto
        where id_venda_transacao is null and status_transacao = 'A' and id_transacao = " . valoresSelect2($transacao) . ");";
    }
    
    if ($valorAcrecimo > 0 && !$adesao_soma) {
        $query .= "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total,valor_bruto)
        select " . valoresSelect2($idVenda) . ",conta_movimento,conta_produto,1,preco_venda,preco_venda from sf_produtos where inativa = 0 
        and conta_produto in (" . $idServicoAcrecimo . ") and conta_produto > 0 and preco_venda > 0;";
    }    

    if ($valorAdesao > 0 || !$adesao_soma) {
        $parcelas = (isset($_GET['txtParcelas']) && is_numeric($_GET['txtParcelas']) ? $_GET['txtParcelas'] : 1);
        for ($i = 0; $i < $parcelas; $i++) {
            $query .= "insert into sf_venda_parcelas(venda,numero_parcela,data_parcela,valor_parcela,valor_pago,data_pagamento,
            id_banco,pa,inativo,historico_baixa,obs_baixa,valor_multa,valor_juros,syslogin,tipo_documento,valor_parcela_liquido, data_cadastro, exp_remessa)
            select " . valoresSelect2($idVenda) . "," . $parcelas . ",dateadd(month," . $i. ",dateadd(day," . $somar_dias . ",cast(transactionTimestamp as date))), (valor_transacao/" . $parcelas ."),0,null,                
            1,'" . str_pad(($i + 1), 2, "0", STR_PAD_LEFT) . "/" . str_pad($parcelas, 2, "0", STR_PAD_LEFT) . "',0," . valoresTexto2('PAG PLANO / SERVIÇO') . ", null,0.00,0.00,'SISTEMA',id_tipo_documento,0.00,transactionTimestamp,0
            from sf_vendas_itens_dcc where id_venda_transacao is null and status_transacao = 'A' and id_transacao = " . valoresSelect2($transacao) . ";";
        }
        $query .= "update sf_venda_parcelas set valor_parcela_liquido = 
        [dbo].[FU_TAXA_FORMA_PAGAMENTO](sf_venda_parcelas.tipo_documento,sf_venda_parcelas.venda, " . $bandeira . ", valor_parcela) 
        where venda = " . valoresSelect2($idVenda) . ";";        
        $query .= "update sf_vendas set conta_movimento = [dbo].[FU_SUB_GRUPO_VENDA](id_venda) where cov = 'V' and id_venda = " . valoresSelect2($idVenda) . ";";                                                
    }

    $query .= "update sf_vendas_planos_mensalidade set
    id_item_venda_mens = id_item_venda,dt_pagamento_mens = cast(transactionTimestamp as date)
    from sf_vendas_itens_dcc inner join sf_vendas_planos_mensalidade on id_mens = id_plano_item
    inner join sf_vendas_itens on produto = id_produto
    where id_venda_transacao is null and status_transacao = 'A'
    and id_transacao = " . valoresSelect2($transacao) . " and id_venda = " . valoresSelect2($idVenda) . ";";

    $query .= "insert into sf_vendas_planos_mensalidade (id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens)
    select id_plano_mens, dateadd(month, (select case when parcela < 1 then 1 else parcela end), dt_inicio_mens),
    dateadd(day,-1, dateadd(month, 2 * (select case when parcela < 1 then 1 else parcela end), dt_inicio_mens)), 
    id_parc_prod_mens, GETDATE(),valor_unico valor_mens 
    from sf_vendas_planos_mensalidade m1 inner join sf_produtos_parcelas on id_parc_prod_mens = id_parcela 
    inner join sf_produtos on sf_produtos.conta_produto = sf_produtos_parcelas.id_produto
    inner join sf_vendas_planos on id_plano = id_plano_mens     
    where inativar_renovacao = 0 and id_mens = " . valoresSelect2($id_mens) . " and dt_cancelamento is null 
    and (select COUNT(*) from sf_vendas_planos_mensalidade m2
    where m2.id_plano_mens = m1.id_plano_mens and m2.id_parc_prod_mens = m1.id_parc_prod_mens
    and month(m2.dt_inicio_mens) = month(dateadd(month ,(select case when parcela < 1 then 1 else parcela end),m1.dt_inicio_mens))
    and year(m2.dt_inicio_mens) = year(dateadd(month ,(select case when parcela < 1 then 1 else parcela end) ,m1.dt_inicio_mens))) = 0;";

    $query .= "update sf_vendas_itens_dcc set id_venda_transacao = " . valoresSelect2($idVenda) . " where id_transacao = " . valoresSelect2($transacao) . ";";
    
    $query .= "update sf_fornecedores_despesas set tipo = 'C', dt_convert_cliente = getdate(), fornecedores_status = dbo.FU_STATUS_CLI(id_fornecedores_despesas)
    WHERE tipo = 'P' and id_fornecedores_despesas in (select id_fornecedores_despesas from sf_vendas_itens_dcc 
    where status_transacao = 'A' and id_transacao = " . valoresSelect2($transacao) . ");";

    $query.= "update sf_fornecedores_despesas set tipo = 'C', dt_convert_cliente = getdate(), fornecedores_status = 'Dependente'
    where inativo = 0 and tipo = 'P' and id_fornecedores_despesas in (SELECT id_dependente from sf_fornecedores_despesas_dependentes where id_titular = 
    (select id_fornecedores_despesas from sf_vendas_itens_dcc  where status_transacao = 'A' and id_transacao = " . valoresSelect2($transacao) . "));";

    $query .= "update sf_vendas_planos set dt_fim = dbo.FU_PLANO_FIM(id_plano) where id_plano in(
    select id_plano_mens from sf_vendas_planos_mensalidade where id_mens = " . valoresSelect2($id_mens) . ");";
    
    $query .= "EXEC dbo.SP_MK_VISTORIA @id_venda = " . $idVenda . ", @loja = '" . $contrato . "';";
    
    $objExec = odbc_exec($con, $query);
    if (!$objExec) {
        odbc_rollback($con);
        echo "ERROR" . $query;
        exit();
    }
    if ($objExec) {
        odbc_commit($con);
        echo $idVenda;
    }
} else {
    echo "Erro, dados não encontrados!";
}