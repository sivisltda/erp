<?php

$rows = [];
$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];

$query = "select tp_disparo_item, dias_tolerancia_item, id_email_item, id_user_resp_item, id_disparo_item, loja_disparo_auto 
from sf_disparo_auto_item where dias_tolerancia_item = 0 and id_disparo_auto_item = " . $id_padrao . " 
and bloqueio_disparo_item = 0 and loja_disparo_auto in (0)";
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $rows[] = array($RFP['tp_disparo_item'], $RFP['dias_tolerancia_item'], $RFP['id_email_item'], $RFP['id_user_resp_item'], $RFP['id_disparo_item'], $RFP['loja_disparo_auto']);
}

foreach ($rows as $value) {
    $dadosAcres = "?DiasTolerancia=" . $value[1] .
            "&IdItem=" . $value[4] .
            "&lojaDisparoAuto=" . $value[5] .
            "&Servidor=" . $hostname .
            "&Banco=" . $database .
            "&Login=" . $username .
            "&Senha=" . $password;
    if ($value[0] == "E") {
        $dadosAcres .= "&IdModeloEmail=" . $value[2] .
        "&IdUserRespEmail=" . $value[3] .        
        "&EnviaEmails=" . $id_padrao;
    } else if ($value[0] == "S") {
        $dadosAcres .= "&IdModelo=" . $value[2] .
        "&IdUserResp=" . $value[3] .                 
        "&EnviaSMS=" . $id_padrao;
    } else if ($value[0] == "W") {
        $dadosAcres .= "&IdModelo=" . $value[2] .
        "&IdUserResp=" . $value[3] .                 
        "&EnviaWhatsapp=" . $id_padrao;
    } else if ($value[0] == "L") {
        $dadosAcres .= "&EnviaLigacao=" . $id_padrao;
    }
    $dadosAcres .= "&id_pessoa=" . $_POST['txtId'];    
    $jsonurl = $url . "/modulos/Sivis/ws_disparo_automatico.php" . $dadosAcres;
    //echo $jsonurl; exit;
    $response = file_get_contents($jsonurl, false, stream_context_create(array("ssl" => array("verify_peer" => false, "verify_peer_name" => false))));
    //echo $response; exit;
    $query = "update sf_disparo_auto_item set dt_ultimo_disparo_item = getdate() where id_disparo_item = " . $value[4];
    odbc_exec($con, $query) or die(odbc_errormsg());    
}