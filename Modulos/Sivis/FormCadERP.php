<?php
include './../../Connections/configini.php';
$disabled = 'disabled';

if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update ca_contratos set " .
        "id_cliente = " . utf8_decode($_POST['txtFonecedor']) . "," .
        "numero_contrato = " . valoresTexto('txtCodigo') . "," .
        "servidor = " . valoresNumericos('txtServidor') . "," .
        "local = " . valoresTexto('txtIp') . "," .
        "login = " . valoresTexto('txtLogin') . "," .
        "senha = " . valoresTexto('txtSenha') . "," .
        "banco = " . valoresTexto('txtBanco') . "," .
        "mdl_adm = " . valoresCheck('mdl_adm') . "," .
        "mdl_fin = " . valoresCheck('mdl_fin') . "," .
        "mdl_est = " . valoresCheck('mdl_est') . "," .
        "mdl_nfe = " . valoresCheck('mdl_nfe') . "," .
        "mdl_crm = " . valoresCheck('mdl_crm') . "," .
        "mdl_ser = " . valoresCheck('mdl_ser') . "," .
        "mdl_aca = " . valoresCheck('mdl_aca') . "," .
        "mdl_tur = " . valoresCheck('mdl_tur') . "," .
        "mdl_gbo = " . valoresCheck('mdl_gbo') . "," .
        "mdl_srs = " . valoresCheck('mdl_srs') . "," .
        "mdl_esd = " . valoresCheck('mdl_esd') . "," .
        "mdl_clb = " . valoresCheck('mdl_clb') . "," .
        "mdl_mil = " . valoresCheck('mdl_mil') . "," .
        "mdl_cnt = " . valoresCheck('mdl_cnt') . "," .
        "mdl_seg = " . valoresCheck('mdl_seg') . "," .
        "mdl_sau = " . valoresCheck('mdl_sau') . "," .
        "mdl_wha = " . valoresCheck('mdl_wha') . "," .
        "mdl_cht = " . valoresCheck('mdl_cht') . "," .
        "mdl_bodweb = " . valoresCheck('mdl_bodweb') . "," .
        "mdl_empresa = " . valoresNumericos('mdl_empresa') . "," .
        "inativa = " . valoresNumericos('txtInativa') . ", " .
        "gerenciar = " . valoresNumericos('txtGerenciar') . " " .
        "where id = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        $query = "INSERT INTO ca_contratos(id_cliente,numero_contrato,servidor,contrato_usr,local,login,senha,banco,
        mdl_adm,mdl_fin,mdl_est,mdl_nfe,mdl_crm,mdl_ser,mdl_aca,mdl_tur,mdl_gbo,mdl_srs,mdl_esd,mdl_clb,mdl_mil,
        gerenciar,inativa,mdl_bodweb,mdl_cnt,mdl_seg,mdl_sau,mdl_wha,mdl_cht,instalado)values(" .
        utf8_decode($_POST['txtFonecedor']) . "," .
        valoresTexto('txtCodigo') . "," .
        valoresNumericos('txtServidor') . ",2," .
        valoresTexto('txtIp') . "," .
        valoresTexto('txtLogin') . "," .
        valoresTexto('txtSenha') . "," .
        valoresTexto('txtBanco') . "," .
        valoresCheck('mdl_adm') . "," .
        valoresCheck('mdl_fin') . "," .
        valoresCheck('mdl_est') . "," .
        valoresCheck('mdl_nfe') . "," .
        valoresCheck('mdl_crm') . "," .
        valoresCheck('mdl_ser') . "," .
        valoresCheck('mdl_aca') . "," .
        valoresCheck('mdl_tur') . "," .
        valoresCheck('mdl_gbo') . "," .
        valoresCheck('mdl_srs') . "," .
        valoresCheck('mdl_esd') . "," .
        valoresCheck('mdl_clb') . "," .
        valoresCheck('mdl_mil') . "," .
        valoresNumericos('txtGerenciar') . "," .
        valoresNumericos('txtInativa') . "," .
        valoresNumericos('mdl_bodweb') . "," .
        valoresCheck('mdl_cnt') . "," .
        valoresCheck('mdl_seg') . "," .
        valoresCheck('mdl_sau') . "," .
        valoresCheck('mdl_wha') . "," .
        valoresCheck('mdl_cht') . ",0);";
        //echo $query;exit();
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id from ca_contratos order by id desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM ca_contratos where id = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso');parent.FecharBox(0);</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "SELECT *,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = fd.id_fornecedores_despesas) telefone_contato,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = fd.id_fornecedores_despesas) email_contato
    FROM ca_contratos inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = ca_contratos.id_cliente
    LEFT JOIN tb_estados e on f.estado = e.estado_codigo left join tb_cidades c on f.cidade = c.cidade_codigo
    LEFT JOIN sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = id_cliente where id = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id'];
        $codigo = $RFP['numero_contrato'];
        $cliente_venda = utf8_encode($RFP['id_fornecedores_despesas']);
        $nomecliente_venda = utf8_encode($RFP['razao_social']);
        if ($RFP['endereco'] != '') {
            $dados1 = utf8_encode($RFP['endereco']);
        }
        if ($RFP['numero'] != '') {
            $dados1 = $dados1 . ' N° ' . utf8_encode($RFP['numero']);
        }
        if ($RFP['complemento'] != '') {
            $dados1 = $dados1 . ' ' . utf8_encode($RFP['complemento']);
        }
        if ($RFP['bairro'] != '') {
            $dados1 = $dados1 . ',' . utf8_encode($RFP['bairro']);
        }
        if ($RFP['cidade_nome'] != '') {
            $dados1 = $dados1 . "\n" . utf8_encode($RFP['cidade_nome']);
        }
        if ($RFP['estado_sigla'] != '') {
            $dados1 = $dados1 . '-' . utf8_encode($RFP['estado_sigla']);
        }
        if ($RFP['cep'] != '') {
            $dados1 = $dados1 . ', Cep: ' . utf8_encode($RFP['cep']);
        }
        if ($RFP['cnpj'] != '') {
            $dados2 = $dados2 . 'CNPJ/CPF:' . utf8_encode($RFP['cnpj']);
        }
        if ($RFP['telefone_contato'] != '') {
            $dados2 = $dados2 . "\n" . 'Tel:' . utf8_encode($RFP['telefone_contato']);
        }
        if ($RFP['email_contato'] != '') {
            $dados2 = $dados2 . ' E-mail:' . utf8_encode($RFP['email_contato']);
        }
        $banco = utf8_encode($RFP['banco']);
        $ip = utf8_encode($RFP['local']);
        $servidor = utf8_encode($RFP['servidor']);
        $inativa = utf8_encode($RFP['inativa']);
        $senha = utf8_encode($RFP['senha']);
        $login = utf8_encode($RFP['login']);
        $mdl_adm = utf8_encode($RFP['mdl_adm']);
        $mdl_fin = utf8_encode($RFP['mdl_fin']);
        $mdl_est = utf8_encode($RFP['mdl_est']);
        $mdl_nfe = utf8_encode($RFP['mdl_nfe']);
        $mdl_crm = utf8_encode($RFP['mdl_crm']);
        $mdl_ser = utf8_encode($RFP['mdl_ser']);
        $mdl_aca = utf8_encode($RFP['mdl_aca']);
        $mdl_tur = utf8_encode($RFP['mdl_tur']);
        $mdl_gbo = utf8_encode($RFP['mdl_gbo']);
        $mdl_srs = utf8_encode($RFP['mdl_srs']);
        $mdl_esd = utf8_encode($RFP['mdl_esd']);
        $mdl_clb = utf8_encode($RFP['mdl_clb']);
        $mdl_mil = utf8_encode($RFP['mdl_mil']);
        $mdl_cnt = utf8_encode($RFP['mdl_cnt']);
        $mdl_seg = utf8_encode($RFP['mdl_seg']);
        $mdl_sau = utf8_encode($RFP['mdl_sau']);
        $mdl_wha = utf8_encode($RFP['mdl_wha']);
        $mdl_cht = utf8_encode($RFP['mdl_cht']);
        $mdl_bodweb = utf8_encode($RFP['mdl_bodweb']);
        $empresa = utf8_encode($RFP['mdl_empresa']);
        $instalado = utf8_encode($RFP['instalado']);
        $gerenciar = utf8_encode($RFP['gerenciar']);
    }
} else {
    $disabled = '';
    $id = '';
    $codigo = '';
    $res = odbc_exec($con, "select top 1 numero_contrato from ca_contratos where numero_contrato < 700 order by numero_contrato desc") or die(odbc_errormsg());
    $idSugere = odbc_result($res, 1) + 1;
    $cliente_venda = '';
    $nomecliente_venda = '';
    $dados1 = '';
    $dados2 = '';
    $banco = 'erp' . str_pad($idSugere, 3, "0", STR_PAD_LEFT);
    $servidor = '0';
    $ip = getDataBaseServer($servidor);
    $inativa = '0';
    $senha = '!Password123';
    $login = 'erp' . str_pad($idSugere, 3, "0", STR_PAD_LEFT);
    $mdl_adm = '1';
    $mdl_fin = '1';
    $mdl_est = '1';
    $mdl_nfe = '0';
    $mdl_crm = '1';
    $mdl_ser = '0';
    $mdl_aca = '1';
    $mdl_tur = '0';
    $mdl_gbo = '0';
    $mdl_srs = '0';
    $mdl_esd = '0';
    $mdl_clb = '0';
    $mdl_mil = '0';
    $mdl_cnt = '0';
    $mdl_seg = '0';
    $mdl_sau = '0';
    $mdl_wha = '0';
    $mdl_cht = '0';
    $instalado = '1';
    $gerenciar = '0';
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
<link href="../../css/main.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../../css/styleLeo.css"/>
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <form action="FormCadERP.php" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Cad ERP</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont">
            <div class="data-fluid tabbable" style="overflow:hidden; height:240px">
                <ul class="nav nav-tabs">
                    <li class="active" style="margin-left:10px"><a href="#tab1" data-toggle="tab">Dados do Cliente</a></li>
                    <li><a href="#tab2" data-toggle="tab">Dados do Servidor</a></li>
                    <li><a href="#tab3" data-toggle="tab">Módulos do sistema</a></li>
                    <li><a href="#tab4" data-toggle="tab">Grupo</a></li>
                </ul>
                <div class="tab-content" style="height:260px">
                    <div class="tab-pane active" id="tab1">
                        <table width="550" border="0" cellpadding="3" cellspacing="0" class="textosComuns">
                            <tr>
                                <td width="90" align="right">Código:</td>
                            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                            <td width="120"><input name="txtCodigo" id="txtCodigo" <?php echo $disabled; ?> type="text" maxlength="3" class="input-medium" value="<?php
                                if (is_numeric($codigo)) {
                                    echo str_pad($codigo, 3, "0", STR_PAD_LEFT);
                                } else {
                                    echo str_pad($idSugere, 3, "0", STR_PAD_LEFT);
                                }
                                ?>"/></td>
                            <td width="210" align="right">Gerenciar:</td>
                            <td width="130">
                                <select name="txtGerenciar" id="txtGerenciar" class="select" style="width:100%" <?php echo $disabled; ?>>
                                    <option value="0" <?php
                                    if ($gerenciar == "0") {
                                        echo "SELECTED";
                                    }
                                    ?>>Não</option>
                                    <option value="1" <?php
                                    if ($gerenciar == "1") {
                                        echo "SELECTED";
                                    }
                                    ?>>Sim</option>
                                </select>
                            </td>
                            </tr>
                            <tr>
                                <td align="right">Cliente:</td>
                                <td colspan="3">
                                    <input name="txtFonecedor" id="txtFonecedor" value="<?php echo $cliente_venda; ?>" type="hidden"/>
                                    <input type="text" style="width:100%" <?php echo $disabled; ?> name="txtDesc" id="txtDesc" class="inputbox" value="<?php echo $nomecliente_venda; ?>" size="10" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">Dados do Cliente:</td>
                                <td colspan="3"><textarea disabled="true" name="txtDados" id="txtDados" style="width:100%" rows="10"><?php echo $dados2; ?></textarea></td>
                            </tr>
                            <tr>
                                <td align="right">Endereço:</td>
                                <td colspan="3"><textarea disabled="true" name="txtEntrega" id="txtEntrega" style="width:100%" rows="10"><?php echo $dados1; ?></textarea></td>
                            </tr>
                        </table>
                    </div>
                    <div class="tab-pane" id="tab2">
                        <table width="550" border="0" cellpadding="3" cellspacing="0" class="textosComuns">
                            <tr>
                                <td width="90" align="right">Servidor:</td>
                                <td width="185">
                                    <select name="txtServidor" id="txtServidor" class="select" style="width:100%" <?php echo $disabled; ?>>
                                        <option value="1" <?php if ($servidor == "1"){ echo "SELECTED"; }?>>DRAGON</option>
                                        <option value="2" <?php if ($servidor == "2"){ echo "SELECTED"; }?>>CONDOR</option>
                                    </select>
                                </td>
                                <td width="90" align="right">Endereço IP:</td>
                                <td width="185">
                                    <input name="txtIp" id="txtIp" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $ip; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">Banco:</td>
                                <td>
                                    <input name="txtBanco" id="txtBanco" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $banco; ?>"/>
                                </td>
                                <td align="right">Login:</td>
                                <td>
                                    <input name="txtLogin" id="txtLogin" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $login; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">Senha:</td>
                                <td><input name="txtSenha" id="txtSenha" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $senha; ?>"/></td>
                                <td align="right">Status:</td>
                                <td>
                                    <select name="txtInativa" id="txtInativa" class="select" style="width:100%" <?php echo $disabled; ?>>
                                        <option value="0" <?php
                                        if ($inativa == "0") {
                                            echo "SELECTED";
                                        }
                                        ?>>Ativo</option>
                                        <option value="1" <?php
                                        if ($inativa == "1") {
                                            echo "SELECTED";
                                        }
                                        ?>>Inativo</option>
                                        <option value="2" <?php
                                        if ($inativa == "2") {
                                            echo "SELECTED";
                                        }
                                        ?>>Mensagem</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="tab-pane" id="tab3">
                        <table width="550" border="0" cellpadding="3" cellspacing="0" class="textosComuns">
                            <tr>
                                <td width="90" align="right">Administrativo:</td>
                                <td width="20">
                                    <input type="checkbox" name="mdl_adm" value="1" <?php
                                    if ($mdl_adm == "1") {
                                        echo "CHECKED ";
                                    } echo $disabled;
                                    ?>/>
                                </td>
                                <td width="120" align="right">Financeiro:</td>
                                <td width="20">
                                    <input type="checkbox" name="mdl_fin" value="1" <?php
                                    if ($mdl_fin == "1") {
                                        echo "CHECKED ";
                                    } echo $disabled;
                                    ?>/>
                                </td>
                                <td width="80" align="right">GoBody:</td>
                                <td width="20">
                                    <input type="checkbox" name="mdl_gbo" value="1" <?php
                                    if ($mdl_gbo == "1") {
                                        echo "CHECKED ";
                                    } echo $disabled;
                                    ?>/>
                                </td>
                                <td width="50" align="right">Empresa:</td>
                                <td width="150">
                                    <select class="" name="mdl_empresa" <?php echo $disabled; ?>>
                                        <option value="0" <?php if ($empresa == 0) { echo "selected";} ?>>SIVIS</option>
                                        <option value="1" <?php if ($empresa == 1) { echo "selected";} ?>>PAGTO</option>
                                        <option value="2" <?php if ($empresa == 2) { echo "selected";} ?>>M.UNIVERSITY</option>                                        
                                        <option value="3" <?php if ($empresa == 3) { echo "selected";} ?>>SIVIS CLUBE</option>                                        
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">Estoque:</td>
                                <td>
                                    <input type="checkbox" name="mdl_est" value="1" <?php
                                    if ($mdl_est == "1") {
                                        echo "CHECKED ";
                                    } echo $disabled;
                                    ?>/>
                                </td>
                                <td align="right">Nota Fiscal Eletrônica:</td>
                                <td>
                                    <input type="checkbox" name="mdl_nfe" value="1" <?php
                                    if ($mdl_nfe == "1") {
                                        echo "CHECKED ";
                                    } echo $disabled;
                                    ?>/>
                                </td>
                                <td align="right">BodyWeb</td>
                                <td>
                                    <input type="checkbox" name="mdl_bodweb" value="1" <?php
                                    if ($mdl_bodweb == "1") {
                                        echo "CHECKED ";
                                    } echo $disabled;
                                    ?>/>
                                </td>
                                <td align="right">Chatbot</td>
                                <td>
                                    <input type="checkbox" name="mdl_cht" value="1" <?php
                                    if ($mdl_cht == "1") {
                                        echo "CHECKED ";
                                    } echo $disabled;
                                    ?>/>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">CRM:</td>
                                <td>
                                    <input type="checkbox" name="mdl_crm" value="1" <?php
                                    if ($mdl_crm == "1") {
                                        echo "CHECKED ";
                                    } echo $disabled;
                                    ?>/>
                                </td>
                                <td align="right">Serviços:</td>
                                <td>
                                    <input type="checkbox" name="mdl_ser" value="1" <?php
                                    if ($mdl_ser == "1") {
                                        echo "CHECKED ";
                                    } echo $disabled;
                                    ?>/>
                                </td>
                                <td align="right">Estudio:</td>
                                <td>
                                    <input type="checkbox" name="mdl_esd" value="1" <?php
                                    if ($mdl_esd == "1") {
                                        echo "CHECKED ";
                                    } echo $disabled;
                                    ?>/>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">Academia:</td>
                                <td>
                                    <input type="checkbox" name="mdl_aca" value="1" <?php
                                    if ($mdl_aca == "1") {
                                        echo "CHECKED ";
                                    } echo $disabled;
                                    ?>/>
                                </td>
                                <td align="right">SERASA:</td>
                                <td>
                                    <input type="checkbox" name="mdl_srs" value="1" <?php
                                    if ($mdl_srs == "1") {
                                        echo "CHECKED ";
                                    } echo $disabled;
                                    ?>/>
                                </td>
                                <td align="right">Turmas:</td>
                                <td>
                                    <input type="checkbox" name="mdl_tur" value="1" <?php
                                    if ($mdl_tur == "1") {
                                        echo "CHECKED ";
                                    } echo $disabled;
                                    ?>/>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">Clube:</td>
                                <td>
                                    <input type="checkbox" name="mdl_clb" value="1" <?php
                                    if ($mdl_clb == "1") {
                                        echo "CHECKED ";
                                    } echo $disabled;
                                    ?>/>
                                </td>
                                <td align="right">Militar:</td>
                                <td>
                                    <input type="checkbox" name="mdl_mil" value="1" <?php
                                    if ($mdl_mil == "1") {
                                        echo "CHECKED ";
                                    } echo $disabled;
                                    ?>/>
                                </td>
                                <td align="right">
                                  Contrato
                                </td>
                                <td>
                                  <input type="checkbox" name="mdl_cnt" value="1" <?php
                                  if ($mdl_cnt == "1") {
                                      echo "CHECKED ";
                                  } echo $disabled;
                                  ?>/>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                  Seguro
                                </td>
                                <td>
                                  <input type="checkbox" name="mdl_seg" value="1" <?php
                                  if ($mdl_seg == "1") {
                                      echo "CHECKED ";
                                  } echo $disabled;
                                  ?>/>
                                </td>
                                <td align="right">
                                    Saúde
                                </td>
                                <td>
                                    <input type="checkbox" name="mdl_sau" value="1" <?php
                                    if ($mdl_sau == "1") {
                                        echo "CHECKED ";
                                    } echo $disabled;
                                    ?>/>
                                </td>
                                <td align="right">
                                    Whatsapp
                                </td>
                                <td>
                                    <input type="checkbox" name="mdl_wha" value="1" <?php
                                    if ($mdl_wha == "1") {
                                        echo "CHECKED ";
                                    } echo $disabled;
                                    ?>/>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="tab-pane" id="tab4">
                        <div class="buscacntt">
                            <p>Empresa:</p>
                            <input type="text" id="txtempresa" name="empresa">
                            <button type="button" class="btn dblue" onclick="addContrato()"><span class="icon-plus icon-white"></span></button>
                        </div>
                        <div class="cnttable" style="border-top: 1px solid #ddd;">
                            <div class="headerContt">
                                <div class="">
                                    Cod Acad:
                                </div>
                                <div class="">
                                    Nome:
                                </div>
                                <div class="">
                                    Ação:
                                </div>
                            </div>
                            <div class="forConteudo">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" name="bntSave" id="bntOK"><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else {
                    if ($instalado == 0) { ?>
                        <button class="btn btn-success" type="button" title="Instalar" name="bntInstalar" id="bntInstalar" onclick="CreateDB();" style="float: left;margin-top: 15px;" value="Instalar"><span class="ico-plus-sign"></span> Instalar</button>
                    <?php } ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?');"><span class=" ico-remove"></span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
    <script type="text/javascript" src="../../js/util.js"></script>
    <script type="text/javascript">

        var selecionado;
        var acadVinculadas;
        $(function () {
            $("#txtempresa").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "ajax-contratos.php",
                        dataType: "json",
                        data: {
                            q: request.term
                        },
                        success: function (data) {
                            if (acadVinculadas !== null && data !== null) {
                                response(clearRepetidos(data, acadVinculadas));
                            } else {
                                response(data);
                            }
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    selecionado = ui;
                }
            });
        });

        function clearRepetidos(dados, lista) {
            var acadFiltradas = [];
            for (var x = 0; x < dados.length; x++) {
                if (!inArray(dados[x].id, lista) && dados[x].id !== $("#txtId").val()) {
                    acadFiltradas.push(dados[x]);
                }
            }
            return acadFiltradas;
        }

        function inArray(val, lista) {
            for (var x = 0; x < lista.length; x++) {
                if (lista[x].de_contrato === val || lista[x].para_contrato === val) {
                    return true;
                }
            }
            return false;
        }

        function refreshList() {
            $.ajax({
                url: "ajax-contratos-insert.php",
                dataType: "json",
                data: {
                    idcliente: $("#txtId").val(),
                    e: 'r'
                },
                success: function (data) {
                    acadVinculadas = data;
                    var options = '';
                    if (data !== null) {
                        for (var i = 0; i < data.length; i++) {
                            options += "<div class='conteudo'><div>" + data[i].numero_contrato + "</div><div>" + data[i].razao_social + "</div><div><span onclick=\"rmContrato(" + data[i].de_contrato + "," + data[i].para_contrato + ")\" title=\"Excluir\"><img name=\"\" src=\"../../img/1365123843_onebit_33 copy.PNG\" width=\"18\" height=\"18\"></span></div></div>";
                        }
                        $('.forConteudo').html(options).show();
                    } else {
                        $('.forConteudo').html('').show();
                    }
                }
            });
        }

        function addContrato() {
            if (selecionado !== undefined && $("#txtempresa").val() !== "" && selecionado.item.id !== undefined) {
                $.ajax({
                    url: "ajax-contratos-insert.php",
                    dataType: "json",
                    data: {
                        idcliente: $("#txtId").val(),
                        codAcademia: selecionado.item.id,
                        e: 'c'
                    },
                    success: function (data) {
                        refreshList();
                        $("#txtempresa").val("");
                    }
                });
            } else {
                bootbox.alert({
                    message: "Seleciona uma academia primeiro!",
                    size: 'small'
                });
            }
        }

        function rmContrato(idcliente, idacad) {
            bootbox.confirm("Deseja realmente excluir o vinculo?", function (result) {
                if (result === true) {
                    $.ajax({
                        url: "ajax-contratos-insert.php",
                        dataType: "json",
                        data: {
                            idcliente: idcliente,
                            codAcademia: idacad,
                            e: 'd'
                        },
                        success: function (data) {
                            refreshList();
                        }
                    });
                }
            });
        }

        $(document).ready(function () {
            refreshList();
            $("#txtCodigo").change(function () {
                $("#txtBanco").val("erp" + $("#txtCodigo").val());
                $("#txtLogin").val("erp" + $("#txtCodigo").val());
            });
            $("#txtServidor").change(function () {
                $("#txtIp").val(getIpServer());
            });
        });

        function getIpServer() {
            if ($("#txtServidor").val() === "1") {
                return "<?php echo getDataBaseServer(1); ?>";
            } else if ($("#txtServidor").val() === "2") {                
                return "<?php echo getDataBaseServer(2); ?>";
            }
            return "";
        }

        function getUrlServer() {
            if ($("#txtServidor").val() === "1") {
                return "https://sivisweb.com.br";
            } else if ($("#txtServidor").val() === "2") {
                return "";
            }
            return "";
        }

        $(function () {
            $("#txtDesc").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "./../CRM/ajax.php",
                        dataType: "json",
                        data: {
                            q: request.term,
                            t: "C"
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    $('#txtFonecedor').val(ui.item.id).show();
                    $('#txtDesc').val(ui.item.label).show();
                    $('#carregando4').show();
                    $('#carregando5').show();
                    $.getJSON('./../Estoque/endereco.ajax.php?search=', {txtFonecedor: ui.item.id, ajax: 'true'}, function (j) {
                        var optionsDados = '';
                        var options = '';
                        var contato = '';
                        for (var i = 0; i < j.length; i++) {
                            if (j[i].endereco !== '') {
                                options += j[i].endereco;
                            }
                            if (j[i].numero !== '') {
                                options += ' N° ' + j[i].numero;
                            }
                            if (j[i].complemento !== '') {
                                options += ' ' + j[i].complemento;
                            }
                            if (j[i].bairro !== '') {
                                options += ',' + j[i].bairro;
                            }
                            if (j[i].cidade_nome !== '') {
                                options += '\n' + j[i].cidade_nome;
                            }
                            if (j[i].estado_sigla !== '') {
                                options += '-' + j[i].estado_sigla;
                            }
                            if (j[i].cep !== '') {
                                options += ', Cep: ' + j[i].cep;
                            }
                            if (j[i].cnpj !== '') {
                                optionsDados += 'CNPJ/CPF:' + j[i].cnpj;
                            }
                            if (j[i].telefone !== '') {
                                optionsDados += '\nTel:' + j[i].telefone;
                            }
                            if (j[i].email !== '') {
                                optionsDados += ' E-mail:' + j[i].email;
                            }
                            if (j[i].contato !== '') {
                                contato = j[i].contato;
                            }
                        }
                        $('#txtContato').val(contato).show();
                        $('#txtDados').html(optionsDados).show();
                        $('#txtEntrega').html(options).show();
                        $('#carregando4').hide();
                        $('#carregando5').hide();
                    });
                }
            });
        });

        function CreateDB() {
            $('#bntInstalar').attr('disabled', true);
            var urlFind = getUrlServer();
            $.ajax({url: urlFind + "/Modulos/Sivis/MakeNewDataBaseERP.php?id=" + $("#txtCodigo").val(), jsonp: "callback", dataType: "jsonp", data: {format: "json"}, success: function (response) {
                $.ajax({url: urlFind + "/Modulos/Sivis/MakeNewBackup.php?tp=0&id=" + $("#txtCodigo").val(), jsonp: "callback", dataType: "jsonp", data: {format: "json"}, success: function (response) {
                    $.ajax({url: urlFind + "/Modulos/Sivis/MakeNewBackup.php?tp=1&id=" + $("#txtCodigo").val(), jsonp: "callback", dataType: "jsonp", data: {format: "json"}, success: function (response) {
                        $.ajax({url: urlFind + "/Modulos/Sivis/MakeNewScript.php?id=" + $("#txtCodigo").val(), jsonp: "callback", dataType: "jsonp", data: {format: "json"}, success: function (response) {
                          $.ajax({url: urlFind + "/Modulos/Sivis/MakeNewFoldersBodyWeb.php?id=" + $("#txtCodigo").val(), jsonp: "callback", dataType: "jsonp", data: {format: "json"}, success: function (response) {
                            $('#bntInstalar').hide();
                          }});
                        }});
                    }});
                }});
            }});
        }
    </script>
    <?php odbc_close($con); ?>
</body>
