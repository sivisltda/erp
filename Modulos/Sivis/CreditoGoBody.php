<?php include './../../Connections/configini.php'; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../SpryAssets/SpryValidationTextField.css"/>
        <style>
            .fancybox-wrap, .fancybox-skin, .fancybox-outer, .fancybox-inner, .fancybox-image,  .fancybox-wrap object, .fancybox-nav, .fancybox-nav span, .fancybox-tmp {
                padding: 0;
                margin: 0;
                border: 0;
                outline: none;
                vertical-align: top;
                max-height:242px;
                background:url(../../img/fundosForms/formBancos.PNG);
                height: 243px;
            }                     
            #formulario {
                float: left;
                width: 100%;
            }
            #formulario form .linha {
                float: left;
                height: 26px;
                width: 100%;
                display: block;
                border-bottom-width: 1px;
                border-bottom-style: solid;
                border-bottom-color: #f7f7f7;
                background-color: #f7f7f7;
                border-top-width: 1px;
                border-top-style: solid;
                border-top-color: #FFF;
                padding-top: 10px;
                padding-bottom: 10px;
            }
        </style>
    </head>
    <body>
        <?php if ($imprimir == 0) { ?>
            <div id="loader"><img src="../../img/loader.gif"/></div><?php } ?>
        <div class="wrapper">
            <?php
            if ($imprimir == 0) {
                include('../../menuLateral.php');
            }
            ?>
            <div <?php if ($imprimir == 0) { ?>class="body"<?php } ?>>
                <?php
                if ($imprimir == 0) {
                    include("../../top.php");
                }
                ?>
                <div class="content">
                    <form method="POST" action="Orcamentos.php<?php echo $sendURL; ?>">
                        <div class="page-header">
                            <?php if ($imprimir == 0) { ?>
                                <div class="icon"> <span class="ico-arrow-right"></span> </div>
                                <h1>Sivis<small>Crédito GoBody</small></h1>
                            <?php } ?>
                        </div>
                        <?php if ($imprimir == 0) { ?>                            
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="block">
                                        <div id="formulario">       
                                            <span class="linha"> 
                                                <span style="display:block; float:left; text-align:left;">
                                                    <button name="btnNovo" class="btn btn-success" type="button" title="Novo" onClick="AbrirBox(0)"><span class="ico-file-4 icon-white"></span> Cadastrar Novo</button>
                                                </span>                                                    
                                                <span style="width:30%; display:block; float:left; margin-left:1%; padding-left:1%; border-left:dashed 1px #CCC"> 
                                                    <span style="width:100%; display:block; float:left;">
                                                        <span id="spryselect1">
                                                            <input type="text" name="txtDesc" id="txtDesc" class="inputbox" value="" size="10" style="width:300px; color:#000 !important" placeholder="Selecione o Cliente"/>  
                                                        </span>
                                                    </span>
                                                </span>                                                
                                                <span style="width:17%; display:block; float:left; text-align:left; margin-left:1%; padding-left:1%; border-left:dashed 1px #CCC"> 
                                                    <span style="width:40%; display:block; margin-top:5px;  margin-left:0px;  float:left;"> Data Inicial: </span>  
                                                    <span style="width:60%; display:block; margin-top:0px; float:left;">              
                                                        <input type="text" style="width:100%" id="txt_dt_begin" class="datepicker" value="<?php echo $dt_begin; ?>" placeholder="Data inicial">
                                                    </span>
                                                </span>
                                                <span style="width:17%; display:block; float:left; text-align:left; margin-left:1%; padding-left:1%; border-left:dashed 1px #CCC"> 
                                                    <span style="width:40%; display:block; margin-top:5px; margin-left:0px; float:left;"> Data Final: </span>  
                                                    <span style="width:60%; display:block; margin-top:0px; float:left;">           
                                                        <input type="text" style="width:90%" class="datepicker" id="txt_dt_end" value="<?php echo $dt_end; ?>" placeholder="Data Final">
                                                    </span>
                                                </span>
                                                <span style="display:block; float:left; text-align:left"> 
                                                    <span style="display:block; margin-top:0px; float:left">
                                                        <button id="btnfind" class="btn btn-primary" type="button" title="Buscar">Buscar</button>
                                                    </span>
                                                </span>
                                            </span>                                      
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="block">
                                        <?php
                                        if ($imprimir == 1) {
                                            $titulo_pagina = "RELATÓRIO DE CONSULTA<br/>Cadastro de Crédito GoBody";
                                            include "../Financeiro/Cabecalho-Impressao.php";
                                        } else {
                                            ?>
                                            <div class="head dblue">
                                                <div class="icon"><span class="ico-money"></span></div>
                                                <h2>Cadastro de Crédito - GoBody</h2>
                                                <ul class="buttons"><li><a href="#" onClick="source('table_sort_pagination'); return false;"><div class="icon"><span class="ico-info"></span></div></a></li></ul>
                                            </div>
                                        <?php } ?>
                                        <style>#example td { vertical-align: middle }</style>
                                        <div class="data-fluid">
                                            <table <?php
                                            if ($imprimir == 0) {
                                                echo "class=\"table\"";
                                            } else {
                                                echo "border=\"1\"";
                                            }
                                            ?> cellpadding="0" cellspacing="0" width="100%" id="example">
                                                <thead>
                                                    <tr id="formPQ">
                                                        <th width="10%">Dt Movimento</th>
                                                        <th width="40%">Descrição</th>
                                                        <th width="15%">Preço</th>
                                                        <th width="8%">Credito</th>                                                                                                                                            
                                                        <th width="8%"><center>Débito</center></th> 
                                                <th width="10%">Saldo</th> 
                                                <th width="5%">Est.</th> 
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="5" class="dataTables_empty">Carregando dados do Cliente</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="widgets"></div>
                                </div>
                            </div>
                        </div>                                                            
                    </form>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />  
        <script type="text/javascript" src="../../SpryAssets/SpryValidationTextField.js"></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>                 
        <script language="JavaScript">
            function AbrirBox(opc){
            $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:670px; height:230px; position:absolute; top:50%; left:50%; margin-left:-358px; margin-top:-195px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:240px' src='CreditoGoBodyForm.php'></iframe></div></div></div></div></div>");
            }
            function FecharBox(opc){
            $("#newbox").remove();
            }

            var idCliente = "";
            $(document).ready(function() {
            $(function() {
            $("#txtDesc").autocomplete({
            source: function(request, response) {
            $.ajax({
            url: "https://gobody.com.br/Api/Credito/CreditoErpAjax.php?BuscaCliente=S",
                    dataType: "jsonp",
                    data: {
                    q: request.term
                    },
                    success: function(data) {
                    response(data);
                    }
            });
            },
                    minLength: 3,
                    select: function(event, ui){
                    idCliente = ui.item.id;
                    }
            });
            });
            $("#txtDesc").change(function() {
            if ($("#txtDesc").val() === '') {
            idCliente = "";
            }
            });
            $("#txt_dt_begin").val('<?php echo getData("B"); ?>');
            $("#txt_dt_end").val('<?php echo getData("T"); ?>');
            var valSelect = [];
            var myTable = $('#example').dataTable({
            "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "bSort": false,
                    "sDom": '<"H">r<"F">',
                    "sPaginationType": "simple",
                    "sAjaxSource": "https://gobody.com.br/Api/Credito/CreditoErpAjax.php?ListaCreditos=T",
                    "fnServerData": function(sUrl, aoData, fnCallback) {
                    aoData.push({"name": "params", "value": valSelect});
                    $.ajax({
                    "url": sUrl,
                            "data": aoData,
                            "success": fnCallback,
                            "dataType": "jsonp",
                            "cache": false
                    });
                    },
                    'oLanguage': {
                    'oPaginate': {
                    'sFirst':       "Primeiro",
                            'sLast':        "Último",
                            'sNext':        "Próximo",
                            'sPrevious':    "Anterior"},
                            'sEmptyTable':    "Não foi encontrado nenhum resultado",
                            'sInfo':            "Visualização do registro _START_ ao _END_ de _TOTAL_",
                            'sInfoEmpty':       "Visualização do registro 0 ao 0 de 0",
                            'sInfoFiltered':    "(filtrado de _MAX_ registros totais)",
                            'sLengthMenu':      "Visualização de _MENU_ registros",
                            'sLoadingRecords':  "Carregando...",
                            'sProcessing':      "Processando...",
                            'sSearch':          "Pesquisar:",
                            'sZeroRecords':     "Não foi encontrado nenhum resultado"},
                    "sPaginationType":  "full_numbers"
            });
            $("#example").removeClass();
            $("#example").addClass('table table-hover table-light no-footer');
            $("#btnfind").click(function() {
            if ($("#txtDesc").val() === '' || $("#txt_dt_begin").val() === '' || $("#txt_dt_end").val() === '') {
            alert('Todos os filtros devem ser preenchidos!');
            } else{
            valSelect = [[$("#txt_dt_begin").val().replace(/\//g, "_")],
            [$("#txt_dt_end").val().replace(/\//g, "_")],
            [idCliente]];
            myTable.fnDraw(false);
            }
            });
            $("body").on("click", "#example #btnEstorno", function (){
            var id = $(this).attr("data-id");
            var r = confirm("Confirma o estornodo registro?");
            if (r === true) {
            $.ajax({
            url: 'https://gobody.com.br/Api/Credito/CreditoErpAjax.php?EstornarCredito=' + id + '&userErp=<?php echo $_SESSION["login_usuario"]; ?>',
                    dataType: 'jsonp',
                    success: function(result){
                    if (result === 'YES') {
                        myTable.fnDraw(false);
                    }
                    }
            });
            }
            });
            });
            function refreshTable(){
            $('#btnfind').click();
            }
        </script>
        <style type="text/css">
            .fancybox-custom .fancybox-skin {
                box-shadow: 0 0 50px #069;
            }
        </style>
        <?php if ($imprimir == 1) { ?>
            <script type="text/javascript">
            $(window).load(function () {
            $(".body").css("margin-left", 0);
            $("#example_length").remove();
            $("#example_filter").remove();
            $("#example_paginate").remove();
            $("#formPQ > th").css("background-image", "none");
        <?php if ($F1 == 5) { ?>
                $("#example_info").remove();
        <?php } ?>
            });
            </script>
            <?php
        }
        odbc_close($con);?>
    </body>
</html>