<?php
include './../../Connections/configini.php';

function MakeDecimal($Decimal) {
    if (strlen($Decimal) == 16) {
        if (substr($Decimal, -1, 1) == 0) {
            return substr($Decimal, 0, 15);
        } else {
            return $Decimal;
        }
    } else {
        return $Decimal;
    }
}

function TiraString($valor) {
    $Return = '';
    for ($i = 1; $i <= strlen($valor); $i++) {
        if (ctype_xdigit(substr($valor, $i - 1, 1))) {
            $Return = $Return . substr($valor, $i - 1, 1);
        }
    }
    if ($Return == '') {
        return '0';
    } else {
        return $Return;
    }
}

function DecimalToHexa($StrNumber) {
    $q = '';
    $GetValue = '';
    $n = $StrNumber;
    $d = 16;
    $i = 0;
    do {
        $i = $i + 1;
        $q = floor(($n / $d));
        $R = $n - ($q * $d);
        $n = $q;
        If ($R <= 9) {
            $GetValue = $GetValue . $R;
        }
        If ($R > 9) {
            if ($R == 10) {
                $GetValue = $GetValue . 'A';
            }
            if ($R == 11) {
                $GetValue = $GetValue . 'B';
            }
            if ($R == 12) {
                $GetValue = $GetValue . 'C';
            }
            if ($R == 13) {
                $GetValue = $GetValue . 'D';
            }
            if ($R == 14) {
                $GetValue = $GetValue . 'E';
            }
            if ($R == 15) {
                $GetValue = $GetValue . 'F';
            }
        }
    } while ($n >= $d);

    If ($q <= 9) {
        $GetValue = $GetValue . $q;
    }

    If ($q > 9) {
        if ($q == 10) {
            $GetValue = $GetValue . 'A';
        }
        if ($q == 11) {
            $GetValue = $GetValue . 'B';
        }
        if ($q == 12) {
            $GetValue = $GetValue . 'C';
        }
        if ($q == 13) {
            $GetValue = $GetValue . 'D';
        }
        if ($q == 14) {
            $GetValue = $GetValue . 'E';
        }
        if ($q == 15) {
            $GetValue = $GetValue . 'F';
        }
    }

    If ($StrNumber <= 9) {
        $GetValue = $StrNumber;
    }

    If ($q > 9) {
        if ($StrNumber == 10) {
            $GetValue = 'A';
        }
        if ($StrNumber == 11) {
            $GetValue = 'B';
        }
        if ($StrNumber == 12) {
            $GetValue = 'C';
        }
        if ($StrNumber == 13) {
            $GetValue = 'D';
        }
        if ($StrNumber == 14) {
            $GetValue = 'E';
        }
        if ($StrNumber == 15) {
            $GetValue = 'F';
        }
    }
    return strrev($GetValue);
}

if (isset($_POST['bntSave'])) {
    $Chave = $_POST['txtChave'];
    $Ver1 = $_POST['txtVer1'];
    $Ver2 = $_POST['txtVer2'];
    $Ver3 = $_POST['txtVer3'];
    $Acad = (int) $_POST['txtFonecedor'];
    $System = $_POST['optSYS'];
    if ($Chave <> '' and $Ver1 <> '' and $Ver2 <> '' and $Ver3 <> '') {
        if ($System == 0) {
            $Plugue = $_POST['txtPlugue'];
            $key = floor(TiraString($Chave) / 4);
            $key = $key . date("dmy") . $Ver1 . $Ver2 . $Plugue . $Acad;
            $Serial = TiraString(DecimalToHexa(escreverNumero(TiraString(escreverNumero(($key * $key) / ($key * strrev($key)), 0, 14, ',', ' ')), 0, 0, ',', '')));
            $query = "insert into ca_clisenha values (" . $Acad . "," .
                    "'" . $Plugue . "',getdate(),'" . $_SESSION["login_usuario"] . "'," .
                    "'" . $Ver1 . '.' . $Ver2 . '.' . $Ver3 . "','" . $Chave . "','G','" . $Serial . "')";
            odbc_exec($con, $query) or die(odbc_errormsg());
        } elseif ($System == 1) {
            $keyB = floor(TiraString($Chave) / 4);
            $keyB = $keyB . date("dmy") . '135';
            $Serial = TiraString(DecimalToHexa(escreverNumero(MakeDecimal(TiraString(escreverNumero(($keyB * $keyB) / ($keyB * strrev($keyB)), 0, 15, ',', ' '))), 0, 0, ',', '')));
            $query = "insert into ca_clisenha values (" . $Acad . "," .
                    "'00000',getdate(),'" . $_SESSION["login_usuario"] . "'," .
                    "'" . $Ver1 . '.' . $Ver2 . '.' . $Ver3 . "','" . $Chave . "','B','" . $Serial . "')";
            odbc_exec($con, $query) or die(odbc_errormsg());
        } elseif ($System == 2) {
            $keyT = floor(TiraString($Chave) / 4);
            $keyT = $keyT . date("dmy") . '505';
            $Serial = TiraString(DecimalToHexa(escreverNumero(MakeDecimal(TiraString(escreverNumero(($keyT * $keyT) / ($keyT * strrev($keyT)), 0, 15, ',', ' '))), 0, 0, ',', '')));
            $query = "insert into ca_clisenha values (" . $Acad . "," .
                    "'00000',getdate(),'" . $_SESSION["login_usuario"] . "'," .
                    "'" . $Ver1 . '.' . $Ver2 . '.' . $Ver3 . "','" . $Chave . "','T','" . $Serial . "')";
            odbc_exec($con, $query) or die(odbc_errormsg());
        } else {
            $Serial = '';
        }
    } else {
        $Serial = '';
    }
} else {
    $System = 0;
    $Serial = '';
}

if (isset($_POST['bntNew'])) {
    $System = 0;
    $cliente_venda = 0;
    $nomecliente_venda = "";
    $chave = "";
    $ver1 = "";
    $ver2 = "";
    $ver3 = "";
    $plugue = "";
    $Serial = "";
} else {
    $System = $_POST['optSYS'];
    $cliente_venda = $_POST['txtFonecedor'];
    $nomecliente_venda = $_POST['txtDesc'];
    $chave = $_POST['txtChave'];
    $ver1 = $_POST['txtVer1'];
    $ver2 = $_POST['txtVer2'];
    $ver3 = $_POST['txtVer3'];
    $plugue = $_POST['txtPlugue'];
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
<link href="../../css/main.css" rel="stylesheet">
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <form action="FormGerarSenha.php" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Cad Acad</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont">                                        
            <div style="height: 190px;width: 380px;" class="tab-content">
                <table width="380" border="0" cellpadding="3" cellspacing="0" class="textosComuns">                                       
                    <tr>
                        <td width="70" align="right">Sistema:</td>
                        <td colspan="3">
                            <select id="optSel"  class="input_txt2" name="optSYS" onchange="Carrega()">
                                <option value="0" <?php
                                if ($System == 0) {
                                    echo 'SELECTED';
                                }
                                ?>>Sivis GYM</option>
                                <option value="1" <?php
                                if ($System == 1) {
                                    echo 'SELECTED';
                                }
                                ?>>Sivis BODY</option>
                                <option value="2" <?php
                                if ($System == 2) {
                                    echo 'SELECTED';
                                }
                                ?>>Sivis TRAINING</option>                                                                                                     
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="70" align="right">Cliente:</td>
                        <td colspan="3">
                            <input name="txtFonecedor" id="txtFonecedor" value="<?php echo $cliente_venda; ?>" type="hidden"/>
                            <input type="text" style="width:100%" name="txtDesc" id="txtDesc" class="inputbox" value="<?php echo $nomecliente_venda; ?>" size="10" />
                        </td>
                    </tr>
                    <tr>
                        <td width="70" align="right">N° de Série:</td>
                        <td colspan="3">
                            <input name="txtChave" type="text" class="input_txt" id="txtChave" value="<?php echo $chave; ?>" size="19" maxlength="20" />
                        </td>
                    </tr>
                    <tr>
                        <td width="70" align="right">Versão:</td>
                        <td>
                            <input name="txtVer1" type="text" class="input_txt_pq" id="txtVer1" value="<?php echo $ver1; ?>" size="1" maxlength="1" />
                        </td><td>
                            <input name="txtVer2" type="text" class="input_txt_pq" id="txtVer2" value="<?php echo $ver2; ?>" size="1" maxlength="1" />
                        </td><td>
                            <input name="txtVer3" type="text" class="input_txt_pq4" id="txtVer3" value="<?php echo $ver3; ?>" size="2" maxlength="2" />
                        </td>
                    </tr>
                    <tr id="trPlug" <?php
                    if ($System == 1 || $System == 2) {
                        echo 'style="display: none"';
                    }
                    ?>>
                        <td width="70" align="right">Plugue:</td>
                        <td colspan="3">
                            <div id="txtPlugueX"><strong><?php echo $plugue; ?></strong></div>
                            <input type="hidden" size="6" value="<?php echo $plugue; ?>" name="txtPlugue"  id="txtPlugue" class="input_txt_pq2" />
                        </td>
                    </tr>
                    <tr>
                        <td width="70" align="right">Senha:</td>
                        <td colspan="3" style="height: 30px;font-size:16px; color:#F60;">
                            <?php echo $Serial; ?>
                        </td>
                    </tr>
                </table>
            </div>                                                                       
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($Serial == '') { ?>
                    <button class="btn btn-success" type="submit" name="bntSave" title="Gravar" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                <?php } else { ?>
                    <button class="btn  btn-success" type="submit" title="Novo" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                <?php } ?>
                <button class="btn btn-success" title="Cancelar" onClick="parent.FecharBox(0)" id="bntOK"><span class="ico-reply"></span> Cancelar</button>                
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>                
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>  
    <script type="text/javascript" src="../../js/util.js"></script>        
    <script type="text/javascript">
        function Carrega() {
            var choice = document.getElementById("optSel").selectedIndex;
            if (choice === 0) {
                $("#trPlug").show();
            } else {
                $("#trPlug").hide();
            }
        }
        $(function () {
            $("#txtDesc").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "ajax.php",
                        dataType: "json",
                        data: {
                            q: request.term,
                            t: "C"
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    $('#txtFonecedor').val(ui.item.id).show();
                    $('#txtDesc').val(ui.item.label).show();
                    $('#txtVer1').val(ui.item.versao1).show();
                    $('#txtVer2').val(ui.item.versao2).show();
                    $('#txtVer3').val(ui.item.versao3).show();
                    $('#txtPlugue').val(ui.item.cod_plugue).show();
                    $('#txtPlugueX').html("<strong>" + ui.item.cod_plugue + "</strong>").show('slow');
                    $('#txtChave').focus();
                }
            });
        });
    </script>         
    <?php odbc_close($con); ?>
</body>