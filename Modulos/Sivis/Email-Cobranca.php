<?php
include "../../Connections/configini.php";

if (isset($_POST['bntSave'])) {
    if ($_POST['txt_assunto'] != "") {
        $nome = $_FILES['arquivo']['name'];
        $tmp = $_FILES['arquivo']['tmp_name'];
        if ($tmp) {
            require("../../Modulos/Contas-a-pagar/phpmailer/class.phpmailer.php");
            $mail = new PHPMailer();
            $cur = odbc_exec($con, "select isnull(email_host,'') email_host,isnull(email_ckb,'') email_ckb, isnull(email_ssl,0) email_ssl,
            isnull(email_login,'') email_login,isnull(email_senha,'') email_senha,isnull(email_smtp,0) email_smtp,
            isnull(email_porta,'') email_porta,isnull(email_login, '')email_login,
            isnull(email_desc,'') email_desc,isnull(copyMe,'') copyMe,nome, isnull(email,'') email from sf_usuarios 
            where id_usuario = " . $_SESSION["id_usuario"] . " and email_login <> '' and email_porta <> '' and email_host <> '' and email_senha <> ''");
            if (odbc_num_rows($cur) > 0) {            
                while ($RFP = odbc_fetch_array($cur)) {
                    if ($RFP['email_smtp'] == 0) {
                        $mail->IsSMTP();
                    }
                    $mail->Host = utf8_encode($RFP['email_host']);
                    $mail->SMTPAuth = utf8_encode($RFP['email_ckb']);
                    $mail->Username = utf8_encode($RFP['email_login']); // Seu e-mail de saida
                    $mail->Password = utf8_encode($RFP['email_senha']);
                    $mail->Port = utf8_encode($RFP['email_porta']);
                    $mail->From = utf8_encode($RFP['email']);       // Seu e-mail
                    $mail->FromName = utf8_decode(utf8_encode($RFP['email_desc'])); //nome do remetente
                    $mail->SMTPDebug = 1;
                    if ($RFP['email_ssl'] == 1) {
                        $mail->SMTPSecure = 'ssl';
                    } elseif ($RFP['email_ssl'] == 2) {
                        $mail->SMTPSecure = 'tls';
                    }
                    if ($RFP['copyMe'] == 1) {
                        $mail->AddBCC(utf8_encode($RFP['email_login']), utf8_encode($RFP['nome']));
                    }
                }
            } else {
                echo "Verifique as configurações de E-mail!"; exit;
            }                
            $mail->IsHTML(true);
            $conteudo = "";
            $arquivo = fopen($tmp, 'r');
            if ($arquivo == false){
                echo 'Nao foi possível abrir o arquivo.'; exit;
            }
            while (true) {
                $linha = fgets($arquivo);
                if ($linha == null){
                    break;
                }
                $conteudo = $conteudo . $linha;
            }
            fclose($arquivo);
            $items = $_POST['items'];
            $qtd_sucesso = "";
            $invalidos_ = "";
            for ($i = 0; $i < sizeof($items); $i++) {
                $r1 = array('@@@1@@@', '@@@2@@@', '@@@3@@@', '@@@4@@@', '@@@5@@@', '@@@6@@@', '@@@7@@@');
                $r2 = array(utf8_decode($_POST['txtCl_' . $items[$i]]), utf8_decode($_POST['txtEm_' . $items[$i]]), utf8_decode($_POST['txtNn_' . $items[$i]]), utf8_decode($_POST['txtVc_' . $items[$i]]), utf8_decode($_POST['txtHs_' . $items[$i]]), utf8_decode($_POST['txtPa_' . $items[$i]]), utf8_decode($_POST['txtVl_' . $items[$i]]));
                $conteudoPC = str_replace($r1, $r2, $conteudo);
                $mail->AddAddress(utf8_decode($_POST['txtEm_' . $items[$i]]), utf8_decode($_POST['txtCl_' . $items[$i]]));
                $mail->Subject = $_POST['txt_assunto'];
                $mail->Body = $conteudoPC;
                $enviado = $mail->Send();
                $mail->ClearAllRecipients();
                $mail->ClearAttachments();
                if ($enviado) {
                    $qtd_sucesso = $qtd_sucesso + 1;
                } else {
                    $invalidos_ = $invalidos_ . utf8_decode($_POST['txtEm_' . $items[$i]]) . "-";
                }
            }
            $msg = "E-mails(s) " . $qtd_sucesso . ", enviados com sucesso!";
            $msg2 = "E-mails(s) inválidos, " . $invalidos_ . "";
        } else {
            $msg = "Não foi possível enviar o arquivo de conteúdo anexado!";
            $msg2 = "";
        }
    }
} ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Sivis<small>Email de Cobrança</small></h1>
                    </div>
                    <form action="Email-Cobranca.php" method="post" enctype="multipart/form-data" name="enviar" id="enviar">                        
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="boxfilter block">
                                    <div style="float:left;width: 58%;margin-top: 15px;">
                                        <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir()" title="Imprimir"><span class="ico-print icon-white"></span></button>                                        
                                        <input type="file" id="arquivo" name="arquivo" style="background:linear-gradient(#D8D8D8, #E8E8E8);font-size: 12px;" />
                                        <input type="text" id="txt_assunto" name="txt_assunto" value="" placeholder="Assunto do E-mail" style="width: 37%">
                                        <button class="button button-green btn-success" type="submit" onclick="return validar();" name="bntSave" id="bntSave" title="Enviar"><span class="ico-ok"></span></button>                                                                                
                                    </div>                                     
                                    <div style="float:right;width: 42%;">       
                                        <div style="float:left;margin-left: 1%;width: 20%;">       
                                            <div width="100%">De:</div>
                                            <input type="text" style="width: 79px;" class="datepicker" id="txt_dt_bgn" name="txt_dt_bgn" value="" placeholder="Data Inicio">                                        
                                        </div>
                                        <div style="float:left;margin-left: 1%;width: 20%;">       
                                            <div width="100%">Até:</div>
                                            <input type="text" style="width: 79px;" class="datepicker" id="txt_dt_end" name="txt_dt_end" value="<?php echo getData("T");?>" placeholder="Data Final">                                        
                                        </div>                                        
                                        <div style="float:left;margin-left: 1%;width: 47%;">
                                            <div width="100%">Busca:</div>
                                            <input id="txtBusca" maxlength="100" type="text" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title=""/>
                                        </div>
                                        <div style="float:left;margin-left: 1%;width: 7%;margin-top: 15px;">
                                            <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="clear:both"></div>
                        <div class="boxhead">
                            <div class="boxtext">Envio de E-mail personalizado para Clientes</div>
                        </div>
                        <div class="boxtable">
                            <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblLista">
                                <thead>
                                    <tr>
                                        <th width="3%" style="top:0"><input id="checkAll" type="checkbox" class="checkall"/></th>
                                        <th width="20%"><div id="formPQ">Cliente</div></th>
                                        <th width="30%"><div id="formPQ">Email</div></th>
                                        <th width="6%"><div id="formPQ">Ns.Num</div></th> 
                                        <th width="6%"><div id="formPQ">Vecto</div></th>
                                        <th width="18%"><div id="formPQ">Histórico:</div></th>
                                        <th width="4%"><div id="formPQ">Parc</div></th>
                                        <th width="7%"><div id="formPQ">Valor</div></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <div style="clear:both"></div>
                        </div>
                        <strong><?php echo $msg . "<br>" . ($msg2 != "" ? $msg2 . "<br>" : ""); ?></strong>                         
                    </form>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div> 
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>  
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>        
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>                        
        <script type="text/javascript">
            
            $("#txt_dt_bgn, #txt_dt_end").mask(lang["dateMask"]);
            
            $(document).ready(function () {
                $("#btnfind").click(function () {
                    tbLista.fnReloadAjax(finalFind(0));
                });                                
            });

            function validar() {
                if ($('#arquivo').val() === ""){
                    bootbox.alert("Selecione um arquivo de modelo para o Envio!");
                    return false;
                }
                if ($('#txt_assunto').val() === ""){
                    bootbox.alert("Preencha o assunto do E-mail!");
                    return false;
                }
                if($.find('input[name="items[]"]:checked').length === 0){
                    bootbox.alert("Selecione uma ou mais linhas para envio!");
                    return false;                    
                }
                return true;
            }
            
            function finalFind(imp) {
                var retPrint = (imp === 1 ? '&imp=1' : '?imp=0');                
                if ($('#txt_dt_bgn').val() !== ""){
                    retPrint = retPrint + "&dti=" + $('#txt_dt_bgn').val().replace(/\//g, "_"); 
                }
                if ($('#txt_dt_end').val() !== ""){
                    retPrint = retPrint + "&dtf=" + $('#txt_dt_end').val().replace(/\//g, "_"); 
                }
                if ($("#txtBusca").val() !== "") {
                    retPrint = retPrint + '&Search=' + $("#txtBusca").val();
                }
                return "Email-Cobranca_server_processing.php" + retPrint.replace(/\//g, "_");
            }

            function imprimir() {
                var pRel = "&NomeArq=" + "Email de Cobrança" +
                "&lbl=Cliente|Email|Ns.Num|Vecto|Histórico|Parc|Valor" +
                "&siz=160|150|70|50|150|50|70" +
                "&pdf=0" + // Colunas do server processing que não irão aparecer no pdf
                "&filter=" + //Label que irá aparecer os parametros de filtro
                "&PathArqInclude=" + "../Modulos/Sivis/" + finalFind(1); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }

            var tbLista = $('#tblLista').dataTable({
                "iDisplayLength": 20,
                "aLengthMenu": [20, 30, 40, 50, 100],
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "aoColumns": [
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false}],
                "sAjaxSource": finalFind(0),
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"},
                "sPaginationType": "full_numbers"
            });
                        
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }
        </script>        
        <?php odbc_close($con); ?>
    </body>
</html>