<?php
include "./../../Connections/configini.php";
$idTop = 0;
$alerta = "1";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Ponto de Venda</title>
        <link href="./../../css/icons.css" rel="stylesheet" type="text/css"/>
        <link href="./../../css/Tela-de-Venda.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="./../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body style="overflow:hidden" onClick="toggleFullScreen()">
        <input id="txtMnContrato" type="hidden" value="<?php echo $contrato; ?>" />        
        <input id="txtMnFilial" type="hidden" value="<?php echo $filial; ?>" />
        <input id="txtMnLanguage" type="hidden" value="<?php echo $languages; ?>" />        
        <table id="division" width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="2" class="headbar"></td>
            </tr>
            <tr>
                <td class="products">
                    <div class="contents">
                        <div class="button" style="width:100%">
                            <div class="title">SENHA</div>
                            <div class="newpwd"></div>
                        </div>
                    </div>
                </td>
                <td class="receipts">
                    <div class="contents" style="overflow-y:hidden">
                        <div class="title">ATENDIDOS</div>
                        <div class="updown">
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="footbar"><marquee>Sua senha esta localizada em seu cupom! &nbsp; &nbsp; &nbsp; Lembramos que as senhas distribuidas pelo nosso sistema sao aleatorias.</marquee></td>
                <td class="footbar"><img src="./../../img/logo2.png" width="176" height="55"/></td>
            </tr>
        </table>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            setInterval("newSenha()", 5000);
            setInterval("oldSenha()", 5000);
            function resizeAll() {
                $("marquee").css("width", $(window).width() * 0.73);
                $("#division").css("height", $(window).height());
                $(".products > .contents, .receipts > .contents").css("height", "100%");
                $(".products > .contents > .button").css({"height": $(window).height() - 123, "max-height": $(window).height() - 123});
                $(".receipts > .contents > .updown").css({"height": $(window).height() - 168, "max-height": $(window).height() - 168});
            }
            $(document).ready(function () {
                resizeAll();
            });
            $(window).on("resize", function () {
                resizeAll();
            });
            $(window).bind("resize", function () {
                resizeAll();
            });
            $(document).ready(function () {
                var count;
                var interval;
                $("#rec_up").on("mouseover", function () {
                    interval = setInterval(function () {
                        count = count || 1;
                        var pos = $(".receipts > .contents").scrollTop();
                        $(".receipts > .contents").scrollTop(pos - count);
                    }, 1);
                }).click(function () {
                    if (count < 6) {
                        count = count + 1;
                    }
                }).on("mouseout", function () {
                    clearInterval(interval);
                });
                $("#rec_down").on("mouseover", function () {
                    interval = setInterval(function () {
                        count = count || 1;
                        var pos = $(".receipts > .contents").scrollTop();
                        $(".receipts > .contents").scrollTop(pos + count);
                    }, 1);
                }).click(function () {
                    if (count < 6) {
                        count = count + 1;
                    }
                }).on("mouseout", function () {
                    clearInterval(interval);
                });
            });
            function toggleFullScreen() {
                if ((document.fullScreenElement && document.fullScreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
                    if (document.documentElement.requestFullScreen) {
                        document.documentElement.requestFullScreen();
                    } else if (document.documentElement.mozRequestFullScreen) {
                        document.documentElement.mozRequestFullScreen();
                    } else if (document.documentElement.webkitRequestFullScreen) {
                        document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
                    }
                } else {
                    if (document.cancelFullScreen) {
                        document.cancelFullScreen();
                    } else if (document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                    } else if (document.webkitCancelFullScreen) {
                        document.webkitCancelFullScreen();
                    }
                }
            }
            function novaSenhaTela() {
                function pulsate(element) {
                    for (var i = 0; i < 2; i++) {
                        $(element || this).fadeOut(1000).fadeIn(100, mysound);
                    }
                }
                function mysound() {
                    var audioElement = document.createElement("audio");
                    audioElement.setAttribute("src", "imagem/audio.mp3");
                    audioElement.setAttribute("autoplay", "autoplay");
                }
                pulsate($(".newpwd"));
            }
            function oldSenha() {
                $.getJSON("senha.ajax.php?senha=0", {ajax: "true"}, function (j) {
                    var senha = "";
                    for (var i = 0; i < j.length; i++) {
                        senha = senha + "<div class=\"oldpwd\">" + j[i].cod_pedido + "</div>";
                    }
                    $(".receipts > .contents > .updown").html("");
                    $(".receipts > .contents > .updown").html(senha);
                });
            }
            function newSenha() {
                $.getJSON("senha.ajax.php?senha=1", {ajax: "true"}, function (j) {
                    if(j.length > 0) {
                        var senha = j[0].cod_pedido;
                        var liberado = j[0].lib_pedido;
                        $(".products > .contents > .button > .newpwd").html("");
                        $(".products > .contents > .button > .newpwd").html(j[0].cod_pedido);
                        if (liberado === 0) {
                            novaSenhaTela();
                        }                        
                    }
                });
            }
        </script>
    </body>
    <?php odbc_close($con);?>
</html>