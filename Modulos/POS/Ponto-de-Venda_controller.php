<?php

include "./../../Connections/configini.php";

$local = array();

if (isset($_POST['idCancelarNota']) && is_numeric($_POST['idCancelarNota'])) {
    odbc_exec($con, "update sf_vendas set status = 'Aguarda', data_aprov = getdate() where id_venda = '" . $_POST['idCancelarNota'] . "'") or die(odbc_errormsg());
}

if (isset($_POST['salvaComanda'])) {
    if (isset($_POST['id']) && is_numeric($_POST['id'])) {
        odbc_exec($con, "update sf_vendas set vendedor = '" . strtoupper($_POST['vendedor']) . "',indicador = " . valoresSelect("indicador") . ", cliente_venda = " . $_POST['cliente'] . ", comentarios_venda = " . valoresTexto('comentario') . ", cod_pedido = '" . $_POST['comanda'] . "' where id_venda = " . $_POST['id']) or die(odbc_errormsg());
    } else {
        $query = "set dateformat dmy;
                  insert into sf_vendas (data_venda, status, historico_venda, vendedor, indicador, cliente_venda, comentarios_venda, cod_pedido, sys_login, empresa, cov, grupo_conta, conta_movimento, destinatario) 
                  values (" . valoresDataHora2($_POST['data_venda'], date("H:i:s")) . ", 'Aguarda', 'VENDA AVULSA', '" . strtoupper($_POST['vendedor']) . "', " . valoresSelect("indicador") . ", " . valoresNumericos('cliente') . ", " . valoresTexto('comentario') . ", '" . $_POST['comanda'] . "', '" . $_SESSION["login_usuario"] . "'," . $_POST['empresa'] . ", 'V', 14, 1,'" . $_SESSION["login_usuario"] . "' );
                  SELECT SCOPE_IDENTITY() ID;";
        $result = odbc_exec($con, $query) or die(odbc_errormsg());
        odbc_next_result($result);
        $_POST['id'] = odbc_result($result, 1);
    }
    echo $_POST['id'];
}

if (isset($_POST['insereItem'])) {
    $id_vd = $_POST['id_vd'];
    $prod_cod = $_POST['prod_cod'];
    $prod_grp = $_POST['prod_grp'];
    $prod_com = $_POST['prod_com'];
    $prod_pct = $_POST['prod_pct'];
    $codCliente = $_POST['cod_cli'];
    $valorSubTotal = (valoresNumericos('prod_prc') * valoresNumericos('prod_qnt'));
    $sQuery = " BEGIN TRANSACTION
                    insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total,vendedor_comissao) values(" . $id_vd . "," . $prod_grp . "," . $prod_cod . "," . valoresNumericos('prod_qnt') . "," . $valorSubTotal . "," . $prod_com . ");
                    DECLARE @newID INT;
                    SELECT @newID = SCOPE_IDENTITY();
                    insert into sf_vendas_itens_pacotes(produto_usar,venda_item,venda_aplicada)
                        select id_prodmp, @newID venda,null venda_aplicada from sf_produtos_materiaprima where id_produtomp in (" . $prod_cod . ");
                    update sf_vendas_itens_pacotes set venda_aplicada = @newID where id_item_pacote in(" . $prod_pct . ");
                IF @@ERROR = 0
                COMMIT
                ELSE
                ROLLBACK;";

    odbc_exec($con, $sQuery) or die(odbc_errormsg());
    echo 'YES';
    //echo $sQuery;
}

if (isset($_POST['removeItem'])) {
    $sQuery = " BEGIN TRANSACTION
                    DELETE from sf_vendas_itens where id_item_venda in ( select venda_aplicada from sf_vendas_itens_pacotes where venda_item = " . $_POST['id_item'] . " and venda_aplicada is not null)                    
                    DELETE from sf_vendas_itens_pacotes where venda_item = " . $_POST['id_item'] . ";                    
                    DELETE FROM sf_vendas_itens where id_item_venda = " . $_POST['id_item'] . ";
                    UPDATE sf_vendas_itens_pacotes SET VENDA_APLICADA = NULL where venda_aplicada = " . $_POST['id_item'] . ";
                    IF @@ERROR = 0
                COMMIT
                ELSE
                ROLLBACK;";

    odbc_exec($con, $sQuery) or die(odbc_errormsg());
    echo 'YES';
}

if (isset($_POST['insereItemFrete'])) {
    $valorParcela = valoresNumericos("txtValorFrete");
    $cur = odbc_exec($con, "select id_contas_movimento,id_grupo_contas from sf_contas_movimento cm inner join sf_grupo_contas gc on cm.grupo_conta = gc.id_grupo_contas where cm.descricao = '" . utf8_decode("FRETE") . "' and gc.descricao = '" . utf8_decode("DESPESAS EVENTUAIS") . "'");
    while ($RFP = odbc_fetch_array($cur)) {
        $cm = $RFP['id_contas_movimento'];
        $gc = $RFP['id_grupo_contas'];
    }
    $cur = odbc_exec($con, "select id_tipo_documento from dbo.sf_tipo_documento where descricao = 'DINHEIRO'");
    while ($RFP = odbc_fetch_array($cur)) {
        $doc = $RFP['id_tipo_documento'];
    }
    $sQuery = " set dateformat dmy;
                BEGIN TRANSACTION
                INSERT into sf_solicitacao_autorizacao(empresa,grupo_conta,conta_movimento,fornecedor_despesa,tipo_documento,historico,destinatario,comentarios,sys_login,data_lanc,status)
                VALUES(" . valoresSelect('txtEmpresa') . "," . $gc . "," . $cm . "," . $_POST['txtTransportadora'] . "," .
            $doc . ",'VENDA AVULSA','" . $_SESSION["login_usuario"] . "','','SYSADM',GetDate(),'Aguarda');
                    DECLARE @newID INT;
                    SELECT @newID = SCOPE_IDENTITY();
                    INSERT into sf_solicitacao_autorizacao_parcelas(solicitacao_autorizacao,numero_parcela,data_parcela,sa_descricao,valor_parcela,historico_baixa,pa,bol_id_banco,bol_data_criacao,bol_valor,bol_juros,bol_multa,bol_nosso_numero,data_cadastro,tipo_documento) 
                    VALUES (@newID,1,cast(getdate() as date),''," . $valorParcela . ",'VENDA AVULSA','01/01',null,null,null,null,null,null,getdate()," . $doc . ");
                    INSERT INTO sf_frete_historico(frete_venda,frete_transportadora,frete_solicitacao_autorizacao)
                    VALUES (" . $_POST['idVenda'] . "," . $_POST['txtTransportadora'] . ",@newID);
                IF @@ERROR = 0
                    COMMIT
                ELSE
                    ROLLBACK;";

    odbc_exec($con, $sQuery) or die(odbc_errormsg());
    echo 'YES';
    //echo $sQuery;
}

if (isset($_POST['removeItemFrete'])) {
    $sQuery = " BEGIN TRANSACTION
                    DELETE from sf_solicitacao_autorizacao_parcelas where solicitacao_autorizacao in(select frete_solicitacao_autorizacao from sf_frete_historico where frete_venda = " . $_POST['removeItemFrete'] . ");
                    DELETE from sf_solicitacao_autorizacao where id_solicitacao_autorizacao in(select frete_solicitacao_autorizacao from sf_frete_historico where frete_venda = " . $_POST['removeItemFrete'] . ");
                    DELETE from sf_frete_historico where frete_venda = " . $_POST['removeItemFrete'] . ";
                    IF @@ERROR = 0
                COMMIT
                ELSE
                ROLLBACK;";

    odbc_exec($con, $sQuery) or die(odbc_errormsg());
    echo 'YES';
    //echo $sQuery;
}

if (isset($_GET['listaArvorePct'])) {
    $sQuery = "select venda_aplicada from sf_vendas_itens_pacotes where venda_item = " . $_GET['listaArvorePct'] . " and venda_aplicada is not null";
    $cur = odbc_exec($con, $sQuery);
    while ($aRow = odbc_fetch_array($cur)) {
        $local[] = array('id_item' => utf8_encode($aRow['venda_aplicada']));
    }
    echo(json_encode($local));
}

if (isset($_GET['listaItensVenda'])) {
    $sQuery = "select id_item_venda,vendedor_comissao,C.razao_social,grupo,produto,quantidade,valor_total,descricao 
    from sf_vendas_itens inner join sf_produtos on produto = conta_produto
    left join sf_fornecedores_despesas C on sf_vendas_itens.vendedor_comissao = C.id_fornecedores_despesas
    where sf_produtos.tipo = '" . $_GET['listaItensTipo'] . "' and id_venda = " . $_GET['listaItensVenda'];
    $cur = odbc_exec($con, $sQuery);
    $i = 1;
    while ($aRow = odbc_fetch_array($cur)) {

        $prc_unitario = escreverNumero(($aRow['valor_total'] / $aRow['quantidade']));
        $item_count = str_pad($i, 3, "0", STR_PAD_LEFT);
        $local[] = array('id_item_venda' => utf8_encode($aRow['id_item_venda']), 'vendedor_comissao' => utf8_encode($aRow['vendedor_comissao']),
            'razao_social' => utf8_encode($aRow['razao_social']), 'grupo' => utf8_encode($aRow['grupo']), 'produto' => utf8_encode($aRow['produto']),
            'quantidade' => escreverNumero($aRow['quantidade'], 0, 0), 'valor_total' => escreverNumero($aRow['valor_total']), 'descricao' => utf8_encode($aRow['descricao']),
            'prc_unitario' => $prc_unitario, 'item_count' => $item_count);
        $i++;
    }
    echo(json_encode($local));
}

if (isset($_POST['cancelaVenda']) && is_numeric($_POST['cancelaVenda'])) {
    $sQuery = " BEGIN TRANSACTION
                    update sf_vendas set status = 'Reprovado' where id_venda = " . $_POST['cancelaVenda'] . ";
                    update sf_solicitacao_autorizacao set status = 'Reprovado' where id_solicitacao_autorizacao in( 
                                select id_solicitacao_autorizacao from sf_frete_historico A inner join sf_solicitacao_autorizacao B on A.frete_solicitacao_autorizacao = B.id_solicitacao_autorizacao
                                where frete_venda = " . $_POST['cancelaVenda'] . ");
                IF @@ERROR = 0
                COMMIT
                ELSE
                ROLLBACK;";

    odbc_exec($con, $sQuery) or die(odbc_errormsg());
    echo 'YES';
    //echo $sQuery;
}

if (isset($_GET['listaVendComissaoGrupo'])) {
    $where = "";
    if ($_GET['empresa'] !== '0') {
        $where = " and A.empresa = " . $_GET['empresa'];
    }
    if ($_GET['tipo'] == 'P') {
        $sQuery = "select id_fornecedores_despesas, razao_social from dbo.sf_fornecedores_despesas A 
                      where A.inativo = 0 and A.tipo = 'E' and A.recebe_comissao = 1 and dt_demissao is null " . $where . "
               order by razao_social";
    } else {
        $sQuery = "select id_fornecedores_despesas, razao_social from dbo.sf_fornecedores_despesas A 
                      inner join sf_fornecedores_despesas_grupo_servicos B on (A.id_fornecedores_despesas = B.id_funcionario) and B.id_grupo_servico = " . $_GET['listaVendComissaoGrupo'] . " 
                      and A.inativo = 0 and A.tipo = 'E' and A.recebe_comissao = 1 and dt_demissao is null " . $where . "
               order by razao_social";
    }

    $cur = odbc_exec($con, $sQuery);

    while ($aRow = odbc_fetch_array($cur)) {
        $local[] = array('id_fornecedores_despesas' => utf8_encode($aRow['id_fornecedores_despesas']), 'razao_social' => formatNameCompact(utf8_encode($aRow['razao_social'])));
    }
    echo(json_encode($local));
}

if (isset($_GET['listaUltimasVendas'])) {
    $sQuery = "select id_venda,data_venda,(select SUM(valor_parcela) from sf_venda_parcelas where venda = id_venda) valor, cod_pedido, isnull((select id_venda_nfe from sf_nfe_notas where sf_nfe_notas.id_venda_nfe = sf_vendas.id_venda and sf_nfe_notas.status_nfe = 'Autorizada'),0) id_venda_nfe, sf_nfe_notas.code
    from sf_vendas left join sf_nfe_notas on sf_vendas.id_venda = sf_nfe_notas.id_venda_nfe where cov = 'V' and sf_vendas.status = 'Aprovado' and empresa = " . $_GET['filial'] . " and CAST(data_venda AS DATE)= CAST(GETDATE() AS DATE) order by id_venda desc";
    $cur = odbc_exec($con, $sQuery);
    while ($aRow = odbc_fetch_array($cur)) {
        $local[] = array('id_venda' => utf8_encode($aRow['id_venda']),
            'data_venda' => escreverData($aRow['data_venda']),
            'valor' => escreverNumero($aRow['valor']),
            'cod_pedido' => utf8_encode($aRow['cod_pedido']),
            'id_venda_nfe' => utf8_encode($aRow['id_venda_nfe']),
            'code' => utf8_encode($aRow['code']));
    }
    echo(json_encode($local));
}
odbc_close($con);
