<?php
include "./../../Connections/configini.php";
include './../../util/nfe/vendor/autoload.php';
$id = "";
$total = "0";
$id_frete = "0";
$id_freteGrupo = "0";
$txtTpTela = "0";
$tipo_tela = "V";

use NFePHP\NFe\ToolsNFe;

if (isset($_GET["tp"]) && $_GET["tp"] == "1") {
    $txtTpTela = "1";
    $tipo_tela = "O";
}

if (is_numeric($_GET['id'])) {
    if ($txtTpTela === "0") {
        echo '<script>document.addEventListener("DOMContentLoaded", function(event) {telaImpressao();}); </script>';
    }
}

$cur = odbc_exec($con, "select conta_produto from sf_produtos where tipo = 'S' and descricao = 'FRETE'");
while ($RFP = odbc_fetch_array($cur)) {
    $id_frete = $RFP['conta_produto'];
    $id_freteGrupo = "null";
}

$cur = odbc_exec($con, "select * from dbo.sf_configuracao");
while ($RFP = odbc_fetch_array($cur)) {
    $conf_comandaObr = $RFP['FIN_VEN_COMANDA_OBRIGATORIA'];
    $conf_clienteObr = $RFP['FIN_VEN_CLIENTE_OBRIGATORIO'];
    $conf_comissObr = $RFP['FIN_VEN_COMISS_OBRIG'];
    $conf_comandaUni = $RFP['FIN_VEN_COMANDA_UNICA'];
    $conf_comandaUni_Gerar = $RFP['FIN_VEN_COM_UN_GER'];
    $conf_qtdDecimal = $RFP['FIN_VEN_QTD_DEC'];
    $conf_indicador = $RFP['FIN_VEN_INDICADOR'];
    $conf_filtroComissFilial = $RFP['FIN_VEN_FILTRAR_COMISS_FILIAL'];
    $conf_comandaPers = $RFP['FIN_VEN_COMANDA_PERSONALIZADA'];
    $conf_comandaMask = $RFP['FIN_VEN_COMANDA_MASCARA'];
}

$query = "select ISNULL(filial, (SELECT id_filial FROM dbo.sf_filiais where id_filial = 1)) filial from dbo.sf_usuarios where login_user = '" . $_SESSION["login_usuario"] . "'";
$cur = odbc_exec($con, $query) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $idFilial = $RFP['filial'];
}

$tpAmb = "2";
$isNFCe = "N";
$filename = './../../Pessoas/' . $contrato . '/Emitentes/*.pfx';
if (is_array(glob($filename))) {
    if (file_exists('./../../Pessoas/' . $contrato . '/Emitentes/config.json')) {
        $nfeTools = new ToolsNFe('./../../Pessoas/' . $contrato . '/Emitentes/config.json');
        $tpAmb = $nfeTools->aConfig['tpAmb'];
        if (($nfeTools->aConfig['tokenNFCe'] !== "") && ($nfeTools->aConfig['tokenNFCeId'] !== "") &&
                ($nfeTools->aConfig['certPfxName'] !== "") && ($nfeTools->aConfig['certPassword'] !== "") &&
                ($nfeTools->aConfig['cnpj'] !== "")) {
            $isNFCe = "S";
        }
    }
}

if (isset($_POST['bntConvert'])) {
    $nn = $_POST['txtId'];
    odbc_exec($con, "update sf_fornecedores_despesas set tipo = 'C' where id_fornecedores_despesas in (select cliente_venda from sf_vendas where id_venda = '" . $_POST['txtId'] . "') and tipo = 'P'") or die(odbc_errormsg());
    odbc_exec($con, "update sf_vendas set cov = 'V' where id_venda = '" . $nn . "'") or die(odbc_errormsg());
    include "form/FormEmitir-Nota.php"; //Insere em NFE        
    header("Location: Ponto-de-Venda.php?tp=0&id=" . $nn . "&nfe=" . $cod_nfe);
}

if (isset($_POST['bntSave'])) {
    $txtTpTela = $_POST['txtTpTela'];
    $codCliente = 0;
    $nn = "";
    $valorParcela = 0;
    if (is_numeric($_POST['user_cart'])) {
        $codCliente = $_POST['user_cart'];
    }
    if ($_POST['txtComanda'] !== "") {
        $senhaPedido = $_POST['txtComanda'];
    } else {
        $senhaPedido = rand(1, 999);
        $cur = odbc_exec($con, "set dateformat dmy;select (select MAX(cod_pedido) from sf_vendas where data_venda between " . valoresDataHora2(getData("T"), "00:00:00") . " and " . valoresDataHora2(getData("T"), "23:59:59") . ") maior, cod_pedido from sf_vendas where data_venda between " . valoresDataHora2(getData("T"), "00:00:00") . " and " . valoresDataHora2(getData("T"), "23:59:59") . " and cod_pedido = '" . $senhaPedido . "' group by cod_pedido");
        while ($RFP = odbc_fetch_array($cur)) {
            $senhaPedido = $RFP['maior'] + 1;
        }
    }
    if ($_POST['txtVendedor'] !== "") { //vendedor pegar txtVendedor senao login_usuario
        $vendedor = strtoupper($_POST['txtVendedor']);
    } else {
        $vendedor = strtoupper(utf8_decode($_SESSION["login_usuario"]));
    }
    if (!is_numeric($_POST['txtId'])) {
        $query = "set dateformat dmy;
        insert into sf_vendas(data_venda, status, vendedor, cliente_venda,indicador, historico_venda, destinatario,tipo_documento,
        descontop,descontos,entrega,validade,descontotp,descontots,comentarios_venda,sys_login,cov,
        garantia_venda,garantia_servico,cod_pedido,grupo_conta, conta_movimento,empresa, data_aprov)
        values(" . valoresDataHora2($_POST['data_venda'], date("H:i:s")) . ", 'Aprovado', '" . $vendedor . "'," . $codCliente . "," . valoresSelect("txtIndicador") . ",'VENDA AVULSA','" . utf8_decode($_SESSION["login_usuario"]) . "'," . $_POST['form_pagto'] . "," .
        valoresNumericos2($_POST['desc_valor']) . ",0.00,null,null," . $_POST['txtProdSinal'] . ",0,'" . utf8_decode($_POST['txtComentarios']) . "','" . $_SESSION["login_usuario"] . "','" . $tipo_tela . "',
        null,null," . $senhaPedido . ",14,1," . $idFilial . ", GETDATE())";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_venda from sf_vendas order by id_venda desc") or die(odbc_errormsg());
        $nn = odbc_result($res, 1);
        if ($tipo_tela == "V") {
            $query = "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
            values ('sf_vendas', " . $nn . ", '" . $_SESSION["login_usuario"] . "', 'V', 'VENDA " . ($_POST['data_venda'] != getData("T") ? "RETROATIVA" : "") . " DTPAG: " . $_POST['data_venda'] . "', GETDATE(), " . $codCliente . "); ";
            odbc_exec($con, $query) or die(odbc_errormsg());
        }
    } else {
        $nn = $_POST['txtId'];
        $query = "set dateformat dmy;update sf_vendas set data_aprov = GETDATE(), status = 'Aprovado', tipo_documento = " . $_POST['form_pagto'] . ",
        descontotp = " . $_POST['txtProdSinal'] . ", descontop = " . valoresNumericos2($_POST['desc_valor']) . " where  id_venda = " . $nn;
        odbc_exec($con, $query) or die(odbc_errormsg());
        if ($tipo_tela == "V") {
            $query = "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
            values ('sf_vendas', " . $nn . ", '" . $_SESSION["login_usuario"] . "', 'V', 'VENDA " . ($_POST['data_venda'] != getData("T") ? "RETROATIVA" : "") . " ALTERADA DTPAG: " . $_POST['data_venda'] . "', GETDATE(), " . $codCliente . "); ";
            odbc_exec($con, $query) or die(odbc_errormsg());
        }
    }
    if (is_numeric($nn) && $conf_comandaUni_Gerar == 1) { //Auto Numeração de Comanda
        $query = "update sf_vendas set cod_pedido = " . $nn . " where id_venda = " . $nn;
        odbc_exec($con, $query) or die(odbc_errormsg());
    }
    if (is_numeric($nn)) {
        //print_r($_POST['prod_grp']);exit();
        $prod_cod = $_POST['prod_cod'];
        $prod_grp = $_POST['prod_grp']; //(sizeof($_POST['prod_grp']) > 1 ? $_POST['prod_grp'] : null);
        $prod_qnt = $_POST['prod_qnt'];
        $prod_prc = $_POST['prod_prc'];
        $prod_com = $_POST['prod_com'];
        $prod_pct = $_POST['prod_pct'];
        $prod_item = $_POST['prod_item'];
        $cod_produtos = "0";
        $id_pct_pai = 0;
        $id_pct_filho = 0;
        $prod_pct_pai_id = "";
        $prod_pct_pai_item = "";
        $prod_pct_filho = "";
        for ($i = 0; $i < sizeof($prod_cod); $i++) {
            $cod_produtos = $prod_cod[$i];
            $valorSubTotal = (valoresNumericos2($prod_prc[$i]) * valoresNumericos2($prod_qnt[$i]));
            $valorParcela = $valorParcela + $valorSubTotal;
            if (!is_numeric($_POST['txtId'])) {
                $proGrp = '';
                if ($prod_grp[$i] > 0) {
                    $proGrp = $prod_grp[$i];
                } else {
                    $proGrp = 'NULL';
                }
                $query = "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total,vendedor_comissao,valor_bruto) values(" . $nn . "," . $proGrp . "," . $prod_cod[$i] . "," . valoresNumericos2($prod_qnt[$i]) . "," . $valorSubTotal . "," . $prod_com[$i] . "," . $valorSubTotal . ");
                SELECT SCOPE_IDENTITY() ID;";
                //echo $query;exit();
                $result = odbc_exec($con, $query) or die(odbc_errormsg());
                odbc_next_result($result);
                $id_item = odbc_result($result, 1);
                $query = "insert into sf_vendas_itens_pacotes(produto_usar,venda_item,venda_aplicada)
                select id_prodmp, " . $id_item . " venda,null venda_aplicada from sf_produtos_materiaprima where id_produtomp = " . $cod_produtos . "
                SELECT SCOPE_IDENTITY() ID;";
                $result = odbc_exec($con, $query) or die(odbc_errormsg());
                odbc_next_result($result);
                $id_item_pct = odbc_result($result, 1);
                if ($prod_pct[$i] == "NULL" && $prod_item[$i] !== "") {
                    $prod_pct_pai_id[$id_pct_pai] = $prod_item[$i];
                    $prod_pct_pai_item[$id_pct_pai] = $id_item;
                    $id_pct_pai = $id_pct_pai + 1;
                }
                if ($prod_pct[$i] !== "NULL") {
                    if (strstr($prod_item[$i], '*')) {
                        $key = array_search($prod_item[$i], $prod_pct_pai_id);
                        $prod_pct_filho[$id_pct_filho]["P"] = $prod_pct_pai_item[$key];
                        $prod_pct_filho[$id_pct_filho]["F"] = $id_item_pct;
                        $prod_pct_filho[$id_pct_filho]["Prod"] = $prod_pct[$i];
                        $id_pct_filho = $id_pct_filho + 1;
                    } else {
                        odbc_exec($con, "update sf_vendas_itens_pacotes set venda_aplicada = " . $id_item_pct . " where id_item_pacote = " . $prod_item[$i]) or die(odbc_errormsg());
                    }
                }
            }
        }
        //atualiza os pacotes
        if (!is_numeric($_POST['txtId'])) {
            if ($prod_pct_filho != "") {
                for ($i = 0; $i < sizeof($prod_pct_filho); $i++) {
                    odbc_exec($con, "update sf_vendas_itens_pacotes set venda_aplicada = " . $prod_pct_filho[$i]["F"] . " where venda_item = " . $prod_pct_filho[$i]["P"] . " and produto_usar = " . $prod_pct_filho[$i]["Prod"]) or die(odbc_errormsg());
                }
            }
        }
        $qtde = $_POST['qnt_vezes'];
        if (is_numeric($qtde) && $valorParcela > 0) {
            if ($qtde > 1) {
                for ($i = 1; $i <= $qtde; $i++) {
                    $valorParcela = valoresNumericos2($_POST['parc_valor_' . $i]);
                    //$valorLiquido = $valorParcela;
                    if (is_numeric($valorParcela) && is_numeric($_POST['parcPagto_' . $i]) && valoresData('parc_prazo_' . $i) != "null") {
                        if ($valorParcela > 0) {
                            /*$cur = odbc_exec($con, "select taxa from sf_tipo_documento where id_tipo_documento = " . $_POST['parcPagto_' . $i]) or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) {
                                $valorLiquido = $valorLiquido - ($valorParcela * $RFP['taxa'] / 100);
                            }*/                                                        
                            $valor_pago = 0;
                            $valorLiquido = 0;
                            $data_pagamento = "null";
                            $id_banco = 1;
                            if ($_POST['parcPagto_' . $i] == 6) {
                                $valor_pago = $valorParcela;
                                $data_pagamento = valoresDataHora2($_POST['data_venda'], date("H:i:s"));
                            }                               
                            $queryP = "set dateformat dmy; insert into sf_venda_parcelas(venda,numero_parcela,data_parcela,sa_descricao,valor_parcela,valor_parcela_liquido,tipo_documento,historico_baixa,pa,bol_id_banco,bol_data_criacao,bol_valor,bol_juros,bol_multa,bol_nosso_numero,data_cadastro,valor_pago,data_pagamento,id_banco,syslogin) values (" .
                                    $nn . "," . $i . "," . valoresData('parc_prazo_' . $i) . ",''," . $valorParcela . "," . $valorLiquido . "," . $_POST['parcPagto_' . $i] . ",'VENDA AVULSA','" . str_pad($i, 2, "0", STR_PAD_LEFT) . "/" . str_pad($qtde, 2, "0", STR_PAD_LEFT) . "'," .
                                    "null,null,null,null,null,null,getdate()," . $valor_pago . "," . $data_pagamento . "," . $id_banco . "," . valoresTexto2($_SESSION["login_usuario"]) . ")";
                            odbc_exec($con, $queryP) or die(odbc_errormsg());
                        }
                    }
                }                
            } else {
                $i = 1;
                if ($_POST['txtProdSinal'] == 0) {
                    $valorParcela = $valorParcela * ((100 - valoresNumericos2($_POST['desc_valor'])) / 100);
                } else {
                    $valorParcela = $valorParcela - valoresNumericos2($_POST['desc_valor']);
                }
                //$valorLiquido = $valorParcela;
                if (is_numeric($valorParcela) && is_numeric($_POST['form_pagto'])) {
                    /*$cur = odbc_exec($con, "select taxa from sf_tipo_documento where id_tipo_documento = " . $_POST['form_pagto']) or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) {
                        $valorLiquido = $valorLiquido - ($valorParcela * $RFP['taxa'] / 100);
                    }*/
                    $valor_pago = 0;
                    $valorLiquido = 0;
                    $data_pagamento = "null";
                    $id_banco = 1;
                    if ($_POST['form_pagto'] == 6) {
                        $valor_pago = $valorParcela;
                        $data_pagamento = valoresDataHora2($_POST['data_venda'], date("H:i:s"));
                    }                  
                    $queryP = "set dateformat dmy; insert into sf_venda_parcelas(venda,numero_parcela,data_parcela,sa_descricao,valor_parcela,valor_parcela_liquido,tipo_documento,historico_baixa,pa,bol_id_banco,bol_data_criacao,bol_valor,bol_juros,bol_multa,bol_nosso_numero,data_cadastro,valor_pago,data_pagamento,id_banco,syslogin) values (" .
                    $nn . "," . $i . ",cast(getdate() as date),''," . $valorParcela . "," . $valorLiquido . "," . $_POST['form_pagto'] . ",'VENDA AVULSA','" . str_pad($i, 2, "0", STR_PAD_LEFT) . "/" . str_pad(1, 2, "0", STR_PAD_LEFT) . "'," .
                    "null,null," . valoresNumericos2($_POST['pagto_valor']) . ",null,null,null,getdate()," . $valor_pago . "," . $data_pagamento . "," . $id_banco . "," . valoresTexto2($_SESSION["login_usuario"]) . ")";
                    odbc_exec($con, $queryP) or die(odbc_errormsg());
                }
            }            
            $queryTx = "update sf_venda_parcelas set valor_parcela_liquido = 
            [dbo].[FU_TAXA_FORMA_PAGAMENTO](sf_venda_parcelas.tipo_documento,sf_venda_parcelas.venda, 0, valor_parcela)
            where venda = " . $nn . ";";            
            $queryTx .= "update sf_vendas set conta_movimento = [dbo].[FU_SUB_GRUPO_VENDA](id_venda) where cov = 'V' and id_venda = " . $nn . ";";                                                            
            odbc_exec($con, $queryTx) or die(odbc_errormsg());
        }
    }
    if (!is_numeric($_POST['txtId'])) {
        if (isset($_POST['txtTransportadora']) && is_numeric($_POST['txtTransportadora'])) {
            $valorParcela = valoresNumericos("txtValorFrete");
            if ($_POST['txtTransportadora'] != "null" && is_numeric($valorParcela)) {
                $gc = 0;
                $cm = 0;
                $doc = 0;
                $cur = odbc_exec($con, "select id_contas_movimento,id_grupo_contas from sf_contas_movimento cm inner join sf_grupo_contas gc on cm.grupo_conta = gc.id_grupo_contas where cm.descricao = '" . utf8_decode("FRETE") . "' and gc.descricao = '" . utf8_decode("DESPESAS EVENTUAIS") . "'");
                while ($RFP = odbc_fetch_array($cur)) {
                    $cm = $RFP['id_contas_movimento'];
                    $gc = $RFP['id_grupo_contas'];
                }
                $cur = odbc_exec($con, "select id_tipo_documento from dbo.sf_tipo_documento where descricao = 'DINHEIRO'");
                while ($RFP = odbc_fetch_array($cur)) {
                    $doc = $RFP['id_tipo_documento'];
                }
                if ($valorParcela > 0 && $gc > 0 && $cm > 0 && $doc > 0) {
                    $sa = '';
                    $query = "set dateformat dmy;
                    insert into sf_solicitacao_autorizacao(empresa,grupo_conta,conta_movimento,fornecedor_despesa,tipo_documento,historico,destinatario,comentarios,sys_login,data_lanc,status)
                    values(" . $idFilial . "," . $gc . "," . $cm . "," . $_POST['txtTransportadora'] . "," . $doc . ",'VENDA AVULSA','" . $_SESSION["login_usuario"] . "','" . utf8_decode($_POST['txtComentarios']) . "','SYSADM',GetDate(),'Aguarda')";
                    odbc_exec($con, $query) or die(odbc_errormsg());
                    $res = odbc_exec($con, "select top 1 id_solicitacao_autorizacao from sf_solicitacao_autorizacao order by id_solicitacao_autorizacao desc") or die(odbc_errormsg());
                    $sa = odbc_result($res, 1);
                    //----------------------------------------------Parcelas-----------------------------------------
                    odbc_exec($con, "set dateformat dmy; insert into sf_solicitacao_autorizacao_parcelas(solicitacao_autorizacao,numero_parcela,data_parcela,sa_descricao,valor_parcela,historico_baixa,pa,bol_id_banco,bol_data_criacao,bol_valor,bol_juros,bol_multa,bol_nosso_numero,data_cadastro,tipo_documento) values (" .
                                    $sa . ",1,cast(getdate() as date),''," . $valorParcela . ",'VENDA AVULSA','01/01',null,null,null,null,null,null,getdate()," . $doc . ")") or die(odbc_errormsg());
                    odbc_exec($con, "INSERT INTO sf_frete_historico(frete_venda,frete_transportadora,frete_solicitacao_autorizacao) VALUES (" . $nn . "," . $_POST['txtTransportadora'] . "," . $sa . ")") or die(odbc_errormsg());
                } else {
                    echo "<script>bootbox.alert('Verifique os campos necessários para geração de conta do tipo FRETE!');</script>";
                }
            }            
        }
    }
    if ($txtTpTela == "1") {
        $FinalUrl = "?tp=" . $txtTpTela . "&vd=" . $nn;
    } else {
        include "form/FormEmitir-Nota.php";
        $FinalUrl = "?tp=" . $txtTpTela . "&id=" . $nn . "&nfe=" . $cod_nfe;
    }
    header("Location: Ponto-de-Venda.php" . $FinalUrl);
}

if (is_numeric($_GET['vd'])) {
    $cur = odbc_exec($con, "select A.*, B.razao_social from sf_vendas A
    inner join sf_fornecedores_despesas B on B.id_fornecedores_despesas = A.cliente_venda
    where A.id_venda = " . $_GET['vd']);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_venda'];
        $cliente_venda = $RFP['cliente_venda'];
        $vendedor = strtoupper(utf8_encode($RFP['vendedor']));
        $indicador = $RFP['indicador'];
        $comanda = $RFP['cod_pedido'];
        $nmCliente = utf8_encode($RFP['razao_social']);
        $comentario = utf8_encode($RFP['comentarios_venda']);
        $status = utf8_encode($RFP['status']);
        $data_venda = escreverData($RFP['data_venda']);
    }
    if ($status !== 'Aguarda' && $txtTpTela === "0") {
        echo "<script>window.top.location.href = 'Ponto-de-Venda.php?tp=0';</script>";
    }
    $cur = odbc_exec($con, "select top 1 A.frete_transportadora, B.valor_parcela from sf_frete_historico A
    inner join sf_solicitacao_autorizacao_parcelas B
    on A.frete_solicitacao_autorizacao = B.solicitacao_autorizacao
    where frete_venda = " . $_GET['vd'] . "order by data_parcela");
    while ($RFP = odbc_fetch_array($cur)) {
        $frete_transportadora = $RFP['frete_transportadora'];
        $frete_valor = escreverNumero($RFP['valor_parcela']);
    }
} else {
    $id = "";
    $cliente_venda = "";
    $vendedor = "";
    $indicador = "null";
    $comanda = "";
    $nmCliente = "";
    $comentario = "";
    $frete_transportadora = "";
    $frete_valor = "";
    $data_venda = getData("T");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Ponto de Venda</title>
        <link href="./../../css/icons.css" rel="stylesheet" type="text/css"/>
        <link href="./../../css/Ponto-de-Venda.css" rel="stylesheet" type="text/css"/>
        <link href="pickadate/themes/classic.css" rel="stylesheet" type="text/css"/>
        <link href="pickadate/themes/classic.date.css" rel="stylesheet" type="text/css"/>
        <link href="keypad/jquery.keypad.css" rel="stylesheet" type="text/css"/>
        <link href="./../../css/bootbox_Ponto_Venda.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="./../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body onload="carrega(-1)" style="overflow:hidden;<?php
    if ($tipo_tela == "O") {
        echo " background:url('./../../css/background_2.jpg') no-repeat center center fixed";
    }
    ?>">
        <input id="txtMnContrato" type="hidden" value="<?php echo $contrato; ?>" />        
        <input id="txtMnFilial" type="hidden" value="<?php echo $filial; ?>" />
        <input id="txtMnLanguage" type="hidden" value="<?php echo $languages; ?>" />
        <form action="Ponto-de-Venda.php<?php echo "?tp=" . $_GET["tp"]; ?>" method="POST" name="frmEnviaDados" id="frmEnviaDados">
            <div id="loader" class="loader" hidden><img src="../../img/loader.gif"/></div>
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <input name="txtFilial" id="txtFilial" value="<?php echo $idFilial; ?>" type="hidden"/>
            <input name="txtFrete" id="txtFrete" value="<?php echo $id_frete; ?>" type="hidden"/>
            <input name="txtFreteGrupo" id="txtFreteGrupo" value="<?php echo $id_freteGrupo; ?>" type="hidden"/>
            <input name="txtTransportadora" id="txtTransportadora" value="<?php echo $frete_transportadora; ?>" type="hidden"/>
            <input name="txtValorFrete" id="txtValorFrete" value="<?php echo $frete_valor; ?>" type="hidden"/>
            <input name="txtComentarios" id="txtComentarios" value="<?php echo $comentario; ?>" type="hidden"/>
            <input name="txtVendedor" id="txtVendedor" value="<?php echo $vendedor; ?>" type="hidden"/>
            <input name="txtIndicador" id="txtIndicador" value="<?php echo $indicador; ?>" type="hidden"/>
            <input name="txtComanda" id="txtComanda" value="<?php echo $comanda; ?>" type="hidden"/>
            <input name="txtTpTela" id="txtTpTela" value="<?php echo $txtTpTela; ?>" type="hidden"/>
            <input name="txtChave" id="txtChave" value="" type="hidden"/>
            <input name="txtModelo" id="txtModelo" value="65" type="hidden"/>
            <input name="txtDtEmissao" id="txtDtEmissao" value="<?php echo getData("T"); ?>" type="hidden"/>
            <table id="division" width="100%" cellspacing="0" cellpadding="0" style="table-layout:fixed">
                <tr><td width="55%"></td><td></td><td width="27%"></td></tr>
                <tr>
                    <td class="navigation">
                        <div class="button" style="background-color:#009AD7; margin-right:10px" onClick="trocarMenu(1)" title="Localizar">
                            <div class="icon"><span class="ico-search"></span></div>
                        </div>
                        <a href="javascript:void(0)"><div id="prev" class="button" style="background-color:#009AD7">
                                <div class="icon"><span class="ico-arrow-left"></span></div>
                            </div></a>
                        <div class="slides" style="overflow-x:hidden">
                            <div id="menuProd" class="goback">
                                <?php
                                $cur = odbc_exec($con, "select id_contas_movimento,descricao from sf_contas_movimento where tipo = 'P' and terminal=1 order by descricao") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) {
                                    ?><div class="option"><div class="name" onClick="carrega(<?php echo $RFP["id_contas_movimento"] ?>)"><?php echo utf8_encode($RFP["descricao"]) ?></div></div><?php } ?>
                            </div>
                            <div id="menuServ" class="goback" style="display:none">
                                <?php
                                $cur = odbc_exec($con, "select id_contas_movimento,descricao from sf_contas_movimento where tipo = 'S' and terminal=1 order by descricao") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) {
                                    ?><div class="option"><div class="name" onClick="carrega(<?php echo $RFP["id_contas_movimento"] ?>)"><?php echo utf8_encode($RFP["descricao"]) ?></div></div><?php } ?>
                            </div>
                        </div>
                        <div id="next" class="button" style="background-color:#009AD7">
                            <div class="icon"><span class="ico-arrow-right"></span></div>
                        </div>
                    </td>
                    <td class="searchcode" style="display: none">
                        <div class="button" style="background-color:#009AD7; margin-right:10px" onClick="trocarMenu(0)" title="Exibir Grupos">
                            <div class="icon" style="margin-top:8px"><span class="ico-list-3"></span></div>
                        </div>
                        <div class="slides" style="overflow:hidden"><input id="cod_busca" type="text" class="value" onkeypress="return runScript(event, 'cod_busca')"/></div>
                        <div id="code" class="button" style="background-color:#009AD7" onclick="novaBusca()">
                            <div class="icon"><span class="ico-search"></span></div>
                        </div>
                    </td>
                    <td class="clocktime" colspan="2">
                        <div id="method" class="button" style="background-color:#009AD7" onClick="trocarTipo(0)" title="Alterar para Serviço">
                            <div class="icon"><span class="ico-stack-3"></span></div>
                        </div>
                        <a href="#">
                            <div class="login" onClick="novoUserLogin()">
                                <div class="photo"><img src="<?php
                                    if ($_SESSION["imagem"] != '') {
                                        echo "./../../Pessoas/" . $contrato . "/" . $_SESSION["imagem"];
                                    } else {
                                        echo "./../../img/dmitry_m.gif";
                                    }
                                    ?>" width="38px" align="left" style="border-left:solid 3px greenyellow; float:left;"/></div>
                                    <?php
                                    $query = "select ISNULL(numero_filial +' - '+ Descricao, (SELECT numero_filial +' - '+ Descricao FROM dbo.sf_filiais where id_filial = 1)) as filial from dbo.sf_usuarios A left join sf_filiais B on A.filial = B.id_filial where login_user = '" . $_SESSION["login_usuario"] . "'";
                                    $cur = odbc_exec($con, $query) or die(odbc_errormsg());
                                    while ($RFP = odbc_fetch_array($cur)) {
                                        $descFilial = $RFP['filial'];
                                    }
                                    ?>
                                <div class="person"><span style="color:#CCC;font-size: 11px;"><b>Bem vindo</b></span><br/><?php echo $_SESSION["login_usuario"]; ?><br/><b style="font-size: 10px;"><?php echo $descFilial; ?></b></div>
                            </div>
                        </a>
                        <div class="display">
                            <div id="time" class="time"></div>
                            <div id="data_click" class="date"><?php echo $data_venda; ?></div>
                            <input id="data_venda" hidden name="data_venda" class="value" value="<?php echo $data_venda; ?>"/>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="products">
                        <div class="statics" style="height:10px"></div>
                        <div class="contents" style="overflow-y:auto">
                            <div class="updown"></div>
                        </div>
                        <div class="payments" style="overflow-y:hidden; display:none">
                            <div class="updown">
                                <div class="mylist"></div>
                                <div class="option">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="100%">
                                                <div class="label">Forma de Pagamento</div>
                                                <div class="input">
                                                    <select id="form_pagto" name="form_pagto" class="value select">
                                                        <?php
                                                        $cur = odbc_exec($con, "select id_tipo_documento,descricao from sf_tipo_documento where s_tipo in ('A','C') order by descricao") or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) {
                                                            ?>
                                                            <option value="<?php echo $RFP["id_tipo_documento"] ?>"<?php
                                                            if ($RFP["id_tipo_documento"] == 6) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                                                <?php } ?>
                                                    </select>
                                                </div>
                                            </td>
                                            <td width="60px">
                                                <div class="button" style="width:50px; background-color:#009AD7" onClick="if ($('#btnPagamento').is(':visible')) { abreSelect(form_pagto); }">
                                                    <div class="icon"><span class="ico-chevron-down"></span></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="option">
                                    <table id="resize" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="60px">
                                                <div class="button" style="width:50px; background-color:#009AD7" onClick="if ($('#btnPagamento').is(':visible')) { updateSoma('vezes', 0); }">
                                                    <div class="icon"><span class="ico-minus-2"></span></div>
                                                </div>
                                            </td>
                                            <td width="100%">
                                                <div class="label">Parcelas</div>
                                                <div class="input"><input id="qnt_vezes" name="qnt_vezes" type="text" class="value" value="1" style="width:60px"/></div>
                                            </td>
                                            <td width="60px">
                                                <div class="button" style="width:50px; background-color:#009AD7" onClick="if ($('#btnPagamento').is(':visible')) { updateSoma('vezes', 1);}">
                                                    <div class="icon"><span class="ico-plus"></span></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="option">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="100%">
                                                <div class="label">1º Vencimento</div>
                                                <div class="input"><input id="prim_prazo" type="text" class="value" value="<?php echo getData("T"); ?>"/></div>
                                            </td>
                                            <td width="60px">
                                                <div id="prim_click" class="button" style="width:50px; background-color:#009AD7">
                                                    <div class="icon" style="margin-top:7px"><span class="ico-calendar"></span></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="option">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="100%">
                                                <div class="label">Desconto</div>
                                                <div class="input">
                                                    <input id="desc_valor" name="desc_valor" type="text" class="value" value="<?php echo escreverNumero(0); ?>"/>
                                                </div>
                                            </td>
                                            <td width="60px">
                                                <div id="desc_click" class="button" style="width:50px; background-color:#009AD7">
                                                    <div id="desc_sinal" class="icon" style="margin-top:1px"><b>%</b></div>
                                                    <input name="txtProdSinal" id="txtProdSinal" value="0" type="hidden"/>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="open_cart" class="option" style="//display:none">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="100%">
                                                <div class="label">Cliente</div>
                                                <div class="input">
                                                    <select id="user_cart" name="user_cart" class="value select">
                                                        <?php if ($cliente_venda !== "") { ?>
                                                            <option value="<?php echo $cliente_venda; ?>" selected><?php echo $nmCliente; ?></option>
                                                        <?php } else { ?>
                                                            <option value="">Selecione</option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </td>
                                            <td width="60px">
                                                <div class="button" style="width:50px; background-color:#009AD7" onClick="if ($('#btnPagamento').is(':visible')) { abreSelect(user_cart);}">
                                                    <div class="icon"><span class="ico-chevron-down"></span></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="option">
                                    <div id="btnPagamento" class="button" style="background-color:#68AF27" onClick="novoDesconto();novaCarteira()">
                                        <div class="icon"><span class="ico-arrow-right"></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="nmCliente" name="nmCliente" style="font-size: 30px; padding:0; background:#DDD">&nbsp;<b><?php
                                if ($nmCliente == "") {
                                    echo "Venda Avulsa";
                                } else {
                                    echo $nmCliente;
                                }
                                ?></b></div>
                        <div class="statics" style="height:50px; padding:5px;text-align: center;">
                            <div id="pro_down" class="button" style="background-color:#009AD7">
                                <div class="icon"><span class="ico-arrow-down"></span></div>
                            </div>
                            <div id="pro_up" class="button" style="background-color:#009AD7; margin-right:5px">
                                <div class="icon"><span class="ico-arrow-up"></span></div>
                            </div>
                            <?php if ($tipo_tela == "O") { ?>
                                <div class="button" style="background-color:#D53F26; margin-right:5px; float:left" onClick="window.location.href = 'Ponto-de-Venda.php?tp=0'" title="Alterar para Venda">
                                    <div class="icon"><span class="ico-refresh"></span></div>
                                    <div class="text" style="margin-top: -3px;font-size: 11px;">ORÇA.</div>                                    
                                </div>
                            <?php } else { ?>
                                <div class="button" style="background-color:#68AF27; margin-right:5px; float:left" onClick="window.location.href = 'Ponto-de-Venda.php?tp=1'" title="Alterar para Orçamento">                                    
                                    <div class="icon"><span class="ico-refresh"></span></div>
                                    <div class="text" style="margin-top: -3px;font-size: 11px;">VENDA</div>
                                </div>
                            <?php } ?>                            
                            <div id="btn_entrega" class="button" style="background-color:#009AD7; margin-right:5px; float:left" onClick="entregaVenda()" title="Frete">
                                <div class="icon"><span class="ico-plane"></span></div>
                                <div class="text" style="margin-top: -3px;font-size: 11px;">Frete</div>
                            </div>
                            <div id="btn_comenta" class="button" style="background-color:#009AD7; margin-right:5px; float:left" onClick="comentaVenda()" title="Comentários">
                                <div class="icon"><span class="ico-chat-3"></span></div>
                                <div class="text" style="margin-top: -3px;font-size: 10px;">Comentário</div>
                            </div>
                            <div id="btn_usuario" class="button" style="background-color:#009AD7; margin-right:5px; float:left" onClick="usuarioVenda()" title="Clientes">
                                <div class="icon"><span class="ico-user"></span></div>
                                <div class="text" style="margin-top: -3px;font-size: 11px;">Cliente</div>
                            </div>
                            <?php if ($conf_comandaUni_Gerar == 0) { ?>
                                <div id="btn_ncomanda" class="button" style="background-color:#68AF27; margin-right:5px; float:left" onClick="novaComanda()" title="Nova Comanda">
                                    <div class="icon"><span class="ico-clipboard-2"></span></div>
                                    <div class="text" style="margin-top: -3px;font-size: 11px;">Comanda</div>
                                </div>
                            <?php } ?>
                            <div id="btn_lcomanda" class="button" style="background-color:#FFAA31; float:left" onClick="listaComanda()" title="<?php echo ($tipo_tela == "O" ? "Orçamentos" : "Comandas em Aberto"); ?>">
                                <div class="icon"><span class="ico-clipboard"></span></div>
                                <div class="text" style="margin-top: -3px;font-size: 11px;"><?php echo ($tipo_tela == "O" ? "Buscar" : "Em aberto"); ?></div>
                            </div>
                        </div>
                    </td>
                    <td class="quantity">
                        <div class="contents" style="overflow-y:hidden">
                            <input name="qnt_titulo" id="qnt_titulo" type="hidden"/>
                            <input name="qnt_codigo" id="qnt_codigo" type="hidden"/>
                            <input name="qnt_grupo" id="qnt_grupo" type="hidden"/>
                            <input name="qnt_pacote" id="qnt_pacote" type="hidden"/>                                                        
                            <div class="input" style="margin-top: -10px; float: left; width: 98%;">
                                <input id="qnt_valor" class="input" value="<?php echo ($conf_qtdDecimal == 1 ? escreverNumero(0) : "0"); ?>" style="width: 100%;" onBlur="updateQnt()" onkeypress="return runScript(event, 'qnt_valor')"></input>
                            </div>                            
                            <div class="button" style="margin-top: 10px; float: left; background-color:#009AD7; width: 40%;" onClick="updateSoma('valor', 1)">
                                <div class="icon"><span class="ico-plus"></span></div>
                            </div>
                            <div class="button" style="margin-top: 10px; float: right; background-color:#009AD7; width: 40%;" onClick="updateSoma('valor', 0)">
                                <div class="icon"><span class="ico-minus-2"></span></div>
                            </div>
                            <div style="clear: both;"></div>
                            <div class="label" style="margin-top:10px">Unitário</div>
                            <input id="prc_valor" class="input" value="<?php echo escreverNumero(0); ?>" style="width: 97.5%;" onBlur="updateQnt()" onkeypress="return runScript(event, 'prc_valor')"></input>
                            <div class="label" style="margin-top:10px">Soma Total</div>
                            <div id="prc_total" class="input"><?php echo escreverNumero(0); ?></div>
                            <div class="label" style="margin-top:10px">Profissional</div>
                            <div class="input">
                                <select id="vnd_comis" name="vnd_comis" class="value select" style="width:98%; -webkit-appearance:none">
                                    <option value="NULL">Selecione</option>                                    
                                </select>
                            </div>
                            <div class="button" style="height:40px; background-color:#68AF27; margin-top:10px" onClick="novaLinha()" title="Adicionar item">
                                <div class="icon" style="margin-top:6px"><span class="ico-arrow-right"></span></div>
                            </div>
                            <div class="label" style="margin-top:10px">Comanda</div>
                            <div id="num_comanda" class="input"><?php echo $comanda; ?></div>
                        </div>
                    </td>
                    <td class="receipts">
                        <div class="statics" style="height:10px"></div>
                        <div class="contents" style="overflow-y:hidden">
                            <div class="updown">
                                <table width="100%" style="margin:0; padding:0"><tr><td style="padding:0; background:#DDD"><center><b>Produtos</b></center></td></tr></table>
                                <table id="listProd" width="100%" style="margin:0; padding-top:0">                            
                                </table>
                                <table width="100%" style="margin:0; padding:0"><tr><td style="padding:0; background:#DDD"><center><b>Serviços</b></center></td></tr></table>
                                <table id="listServ" width="100%" style="margin:0; padding-top:0">                            
                                </table>
                            </div>
                        </div>
                        <div class="statics" style="height:50px; padding:5px">
                            <div id="rec_down" class="button" style="background-color:#009AD7">
                                <div class="icon"><span class="ico-arrow-down"></span></div>
                            </div>
                            <div id="rec_up" class="button" style="background-color:#009AD7; margin-right:5px">
                                <div class="icon"><span class="ico-arrow-up"></span></div>
                            </div>
                            <div id="rec_del" class="button" style="background-color:#D53F26; margin-right:5px; float:left" title="Remover item">
                                <div class="icon"><span class="ico-trashcan"></span></div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="checkout" colspan="2">
                        <span id="buy_check">
                            <div class="button" style="background-color:#68AF27; margin-right:1%" onClick="trocaTela(1)">
                                <div class="icon"><span class="ico-ok"></span></div>
                                <div class="text">Pagamento</div>
                            </div>
                            <div class="button" style="background-color:#FFAA31; margin-right:1%" onClick="carrega(-1)">
                                <div class="icon"><span class="ico-star"></span></div>
                                <div class="text">Favoritos</div>
                            </div>
                            <div class="button" style="background-color:#D53F26;margin-right: 1%" onClick="novaPermissao(2);">
                                <div class="icon"><span class="ico-cancel"></span></div>
                                <div class="text">Cancelar</div>
                            </div>
                            <?php if (is_numeric($_GET['vd'])) { ?>
                                <div class="button" style="background-color:#009AD7; margin-right:1%" onClick="window.open('../../Modulos/POS/cupom.php?id=<?php echo $_GET['vd']; ?>&crt=<?php echo $_SESSION['contrato']; ?>');" value="blank">
                                    <div class="icon"><span class="ico-print"></span></div>
                                    <div class="text">Imprimir</div>
                                </div>
                            <?php } if ($txtTpTela === '1' && is_numeric($_GET['vd'])) { ?>
                                <div class="button" style="background-color:#005683; margin-right:1%" onClick="bootbox.confirm('Confirma conversão para venda?', function (result) {
                                            if (result) {
                                                if (confirmaVenda()) {
                                                    $('#bntConvert').click();
                                                }
                                            }
                                        });" value="blank" >
                                    <div class="icon"><span class="ico-share-alt"></span></div>
                                    <div class="text">G. Venda</div>
                                </div>
                                <input id="bntConvert" name="bntConvert" type="submit" style="display:none"/>                    
                            <?php } ?>
                            <div class="button" style="background-color:#009AD7;" onClick="bootbox.confirm('Os dados preenchidos serão perdidos, Confirma?', function (result) {
                                        if (result === true) {
                                            limpaTelaComanda('L');
                                        } else {
                                            return;
                                        }
                                    });">
                                <div class="icon"><span class="ico-file"></span></div>
                                <div class="text">Novo</div>
                            </div>
                        </span>
                        <span id="pay_check" style="display:none">
                            <div class="button" style="background-color:#68AF27; margin-right:1%" onClick="bootbox.confirm('Confirmar <?php
                            if ($tipo_tela == 'V') {
                                echo "venda";
                            } else {
                                echo "orçamento";
                            }
                            ?> ?', function (result) {
                                        if (result) {
                                            if (confirmaVenda()) {
                                                $('#bntSave').click();
                                            }
                                        }
                                    });">
                                <div class="icon"><span class="ico-shopping-cart"></span></div>
                                <div class="text">Concluir</div>
                            </div>
                            <input id="bntSave" name="bntSave" type="submit" style="display:none"/>
                            <div class="button" style="background-color:#D53F26" onClick="trocaTela(0)">
                                <div class="icon"><span class="ico-undo"></span></div>
                                <div class="text">Retornar</div>
                            </div>
                        </span>
                        <div style="float:right; height:70px; margin:0 10px"><img onClick="toggleFullScreen()" src="./../../img/logo.png" width="176" height="102"></div>
                        <?php
                        if ($_GET["tp"] != "1") {
                            if ($ckb_out_terc_ > 0) { ?>
                                <span style="float:right; margin-right:1%">
                                    <div class="button" style="background-color:#009AD7" onClick="contaCaixa()">
                                        <div class="icon"><span class="ico-coins"></span></div>
                                        <div class="text">Caixa</div>
                                    </div>
                                </span>
                            <?php } ?>
                            <span style="float:right; margin-right:1%">
                                <div class="button" style="background-color:#009AD7" onClick="listaVendas()">
                                    <div class="icon"><span class="ico-barcode"></span></div>
                                    <div class="text">Últ. Notas</div>
                                </div>
                            </span>
                        <?php } ?>
                    </td>
                    <td class="logmein">
                        <div class="button" style="color:#FF0000; font-size:35px; background-color:#FFF">
                            <input id="money_total" type="hidden" value="<?php echo escreverNumero($total); ?>"/>
                            <div id="money" class="money" style="font-size:50px"><span style="font-size:35px"></span> <?php echo escreverNumero($total, 1); ?></div>
                        </div>
                    </td>
                </tr>
            </table>
            <script type="text/javascript" src="maskmoney/jquery.maskMoney.min.js"></script>
            <script type="text/javascript" src="maskmoney/jquery.mask.js"></script>
            <script type="text/javascript" src="pickadate/picker.js"></script>
            <script type="text/javascript" src="pickadate/picker.date.js"></script>
            <script type="text/javascript" src="keypad/jquery.plugin.js"></script>
            <script type="text/javascript" src="keypad/jquery.keypad.js"></script>
            <script type="text/javascript" src="./../../js/plugins/bootstrap/bootstrap.min.js"></script>
            <script type="text/javascript" src="./../../js/plugins/bootbox/bootbox.js"></script>
            <script type="text/javascript" src="./../../js/moment.min.js"></script>
            <script type="text/javascript" src="./../../js/util.js"></script>          
            <script type="text/javascript" src="./../NFE/js/ProcessarNota.js"></script>                                
            <script type="text/javascript">
    
                function toggleFullScreen() {
                    if ((document.fullScreenElement && document.fullScreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
                        if (document.documentElement.requestFullScreen) {
                            document.documentElement.requestFullScreen();
                        } else if (document.documentElement.mozRequestFullScreen) {
                            document.documentElement.mozRequestFullScreen();
                        } else if (document.documentElement.webkitRequestFullScreen) {
                            document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
                        }
                    } else {
                        if (document.cancelFullScreen) {
                            document.cancelFullScreen();
                        } else if (document.mozCancelFullScreen) {
                            document.mozCancelFullScreen();
                        } else if (document.webkitCancelFullScreen) {
                            document.webkitCancelFullScreen();
                        }
                    }
                }                                
                
                $("#form_pagto").change(function () {
                    if ($("#form_pagto option:selected").val() === 7) {
                        $("#prim_prazo").val(moment().add(30, 'days').format(lang["dmy"]));
                    } else if ($("#form_pagto option:selected").val() === 9) {
                        $("#prim_prazo").val(moment().add(2, 'days').format(lang["dmy"]));
                    } else {
                        $("#prim_prazo").val(moment().format(lang["dmy"]));
                    }
                    if ($("#form_pagto option:selected").val() === 6) {
                        $("#qnt_vezes").val("1");
                    }
                });

                function carrega(id) {
                    if ($(".products > .contents").is(":visible")) {
                        $(".products > .contents > .updown").html("");
                        var met = "S";
                        if ($(".clocktime .button .icon").html() === "<span class=\"ico-stack-3\"></span>") {
                            met = "P";
                        }
                        if (id === 0) {
                            $(".receipts > .contents > .updown > table").html("");
                            novoTotal();
                        }
                        $.getJSON("./../Estoque/conta_p.ajax.php?search=", {txtGrupo: id, txtMetodo: met, txtTabelaPreco: "S", ajax: "true"}, function (j) {
                            var produto = "";
                            var contrato = $("#txtMnContrato").val();
                            for (var i = 0; i < j.length; i++) {
                                var classBtn = 'button';
                                var onclickBtn = 'onClick=\"novaQuant(' + j[i].id_contas_movimento + ',' + j[i].grupo + ',' + j[i].pacote + ')\"';
                                if (j[i].naoVenderSemEstoque === '1' && j[i].qtdDisponivel === 0) {
                                    classBtn = 'button naovender';
                                    onclickBtn = "onClick=\"bootbox.alert('Sem Estoque');\"";
                                }
                                produto += "<div id=\"but_" + j[i].id_contas_movimento + "\" class=\"" + classBtn + "\" title=\"Estoque Atual: &#013;" + j[i].filiais + "\" style=\"background-image:url('" + j[i].img + "'); background-position:center center; background-repeat:no-repeat; background-size:150px 150px\" " + onclickBtn + ">";
                                produto += "<div id=\"tit_" + j[i].id_contas_movimento + "\" class=\"title\">" + j[i].descricao + "</div>";
                                produto += "<div id=\"prc_" + j[i].id_contas_movimento + "\" class=\"price\">" + numberFormat(j[i].preco_venda) + "</div></div>";

                            }
                            $(".products > .contents > .updown").html(produto);
                            limpaQuant();
                        });
                    }
                }

                function confirmaVenda() {
                    if ($("#parc_valor_1").length) {
                        var parcTotal = textToNumber($("#money").text());
                        var parAtual = 0;
                        for (var i = 1; i <= eval($("#qnt_vezes").val()); i++) {
                            parAtual = parAtual + textToNumber($("#parc_valor_" + i).val());
                        }
                        if (parseFloat(parcTotal.toFixed(2)) !== parseFloat(parAtual.toFixed(2))) {
                            bootbox.alert("Valores de parcela não batem com o total!");
                            return false;
                        }
                    }
                    if (($('#tdp_1').length || $('#tdp_2').length) || !$('#btnPagamento').is(':visible')) {
                        if ($('#tdp_1').length) {
                            if (textToNumber($("#pagto_valor").val()) > 0.00) {
                                return true;
                            } else {
                                bootbox.alert('O valor pago não é um valor válido!');
                                return false;
                            }
                        } else {
                            return true;
                        }
                    } else {
                        bootbox.alert('Selecione uma forma de pagamento!');
                        return false;
                    }
                }
                
                function startTime() {
                    //SCRIPT PARA MANTER O HORÁRIO ATUALIZADO NO CANTO SUPERIOR
                    var myDay = new Date();
                    if (myDay.getHours() < 10) {
                        var h = "0" + myDay.getHours();
                    } else {
                        var h = myDay.getHours();
                    }
                    if (myDay.getMinutes() < 10) {
                        var m = "0" + myDay.getMinutes();
                    } else {
                        var m = myDay.getMinutes();
                    }
                    if (myDay.getSeconds() < 10) {
                        var s = "0" + myDay.getSeconds();
                    } else {
                        var s = myDay.getSeconds();
                    }
                    document.getElementById("time").innerHTML = h + ":" + m + ":" + s;
                    t = setTimeout(function () {
                        startTime();
                    }, 1000);
                }
                
                function resizeAll() {
                    //SCRIPT PARA REDIMENSIONAR TODAS AS JANELAS PROPORCIONALMENTE
                    $("#division").css("height", $(window).height());
                    $(".slides").css({"width": $(".products").width() - 209, "max-width": $(".products").width() - 190});
                    $(".quantity > .contents").css({"height": $(window).height() - 183, "max-height": $(window).height() - 183});
                    if ($(".products > .contents").is(":visible")) {
                        $(".products > .contents").css({"height": $(window).height() - 278, "max-height": $(window).height() - 278});
                        $(".receipts > .contents").css({"height": $(window).height() - 238, "max-height": $(window).height() - 238});
                    } else {
                        $(".products > .payments").css({"height": $(window).height() - 278, "max-height": $(window).height() - 278});                        
                        $(".receipts > .contents").css({"height": $(window).height() - 238, "max-height": $(window).height() - 238});
                        $("#form_pagto").css({"width": $("#resize").width() - 64, "max-width": $("#resize").width() - 64});
                    }
                    if ($("#user_pop").length === 1) {
                        var sizew = $(window).width() - (140 + ($(".clocktime").innerWidth() - $(".clocktime").width()));
                        var sizeh = 50 + (($(".clocktime").innerHeight() - $(".clocktime").height()) / 2);
                        $(".keypad-login").css({"top": sizeh + "px", "left": sizew + "px"});
                    }
                }
                
                $(window).on("resize", function () {
                    resizeAll();
                });
                $(window).bind("resize", function () {
                    resizeAll();
                });
                $('#frmEnviaDados').submit(function () {
                    $(".products > .payments, #pay_check").find("div >").removeAttr('disabled');
                });
                $(document).ready(function () {
                    <?php if (isset($_GET['newCom'])) { ?>
                        novaComanda();
                    <?php } ?>
                    startTime();
                    resizeAll();
                    id_pct_pai = 0;
                    
                    var interval;
                    //SCRIPT PARA O FUNCIONAMENTO DOS SCROLLS DO MENU E JANELAS
                    $("#prev").on("mousedown", function () {
                        interval = setInterval(function () {
                            var pos = $(".slides").scrollLeft();
                            $(".slides").scrollLeft(pos - 3);
                        }, 1);
                    }).click().on("mouseup", function () {
                        clearInterval(interval);
                    });
                    $("#next").on("mousedown", function () {
                        interval = setInterval(function () {
                            var pos = $(".slides").scrollLeft();
                            $(".slides").scrollLeft(pos + 3);
                        }, 1);
                    }).click().on("mouseup", function () {
                        clearInterval(interval);
                    });
                    $("#pro_up").on("mousedown", function () {
                        interval = setInterval(function () {
                            if ($(".products > .contents").is(":visible")) {
                                var pos = $(".products > .contents").scrollTop();
                                $(".products > .contents").scrollTop(pos - 3);
                            } else {
                                var pos = $(".products > .payments").scrollTop();
                                $(".products > .payments").scrollTop(pos - 3);
                            }
                        }, 1);
                    }).click().on("mouseup", function () {
                        clearInterval(interval);
                    });
                    $("#pro_down").on("mousedown", function () {
                        interval = setInterval(function () {
                            if ($(".products > .contents").is(":visible")) {
                                var pos = $(".products > .contents").scrollTop();
                                $(".products > .contents").scrollTop(pos + 3);
                            } else {
                                var pos = $(".products > .payments").scrollTop();
                                $(".products > .payments").scrollTop(pos + 3);
                            }
                        }, 1);
                    }).click().on("mouseup", function () {
                        clearInterval(interval);
                    });
                    $("#rec_up").on("mousedown", function () {
                        interval = setInterval(function () {
                            var pos = $(".receipts > .contents").scrollTop();
                            $(".receipts > .contents").scrollTop(pos - 3);
                        }, 1);
                    }).click().on("mouseup", function () {
                        clearInterval(interval);
                    });
                    $("#rec_down").on("mousedown", function () {
                        interval = setInterval(function () {
                            var pos = $(".receipts > .contents").scrollTop();
                            $(".receipts > .contents").scrollTop(pos + 3);
                        }, 1);
                    }).click().on("mouseup", function () {
                        clearInterval(interval);
                    });

                    carregaItensVenda('S');
                    carregaItensVenda('P');
                });

                function carregaItensVenda(tipo) {
                    var itens = "";
                    $.getJSON("Ponto-de-Venda_controller.php", {listaItensVenda: $("#txtId").val(), listaItensTipo: tipo}, function (j) {
                        if (j !== null) {
                            var qtd = 1;
                            for (var i = 0; i < j.length; i++) {
                                var razaoSocial = "";
                                var idLinha = "";
                                if (j[i].vendedor_comissao !== "") {
                                    razaoSocial = ' (' + j[i].razao_social + ')';
                                }
                                if (tipo === "P") {
                                    idLinha = 1000 + qtd;
                                } else {
                                    idLinha = 5000 + qtd;
                                }
                                itens = itens + '<tr id="list_' + idLinha + '" class="updown_normal" onclick="selectLinha(' + idLinha + ',' + j[i].id_item_venda + ')">\n\
                                <input type="hidden" name="prod_item[]" value="' + j[i].id_item_venda + '"/> \n\
                                <input type="hidden" name="prod_pct[]" value=""/> \n\
                                <input type="hidden" name="prod_com[]" value="' + j[i].vendedor_comissao + '"/>\n\
                                <input type="hidden" name="prod_grp[]" value="' + j[i].grupo + '"/>\n\
                                <input type="hidden" name="prod_cod[]" value="' + j[i].produto + '"/>\n\
                                <input type="hidden" name="prod_qnt[]" value="' + j[i].quantidade + '"/>\n\
                                <input type="hidden" name="prod_prc[]" value="' + j[i].prc_unitario + '"/>\n\
                                <td width="23px" class="item_count">' + j[i].item_count + '</td>\n\
                                <td>' + j[i].descricao + '<span style="color: red;">' + razaoSocial + '</span></td>\n\
                                <td width="23px">' + j[i].quantidade + '</td><td width="52px">' + j[i].prc_unitario + '</td>\n\
                                <td id="total_1" width="59px" class="list_total">' + j[i].valor_total + '</td>\n\
                                </tr>';
                                qtd = qtd + 1;
                            }
                        }
                    }).done(function () {
                        if (tipo === 'P') {
                            $('#listProd tr').remove();
                            $("#listProd").append(itens);
                        } else {
                            $('#listServ tr').remove();
                            $("#listServ").append(itens);
                        }
                        novoTotal();
                    });
                }

                function updateSoma(id, opt) {
                    //SCRIPT PARA O FUNCIONAMENTO DOS BOTÕES MAIS/MENOS EM "QUANTIDADE" E "PAGAMENTOS"
                    if ($("#form_pagto option:selected").val() !== 6 || id === "valor") {
                        var quant = textToNumber($("#qnt_" + id).val());
                        if (opt === 1 && quant > 0) {
                            if (id === "valor" || (id === "vezes" && quant < 60)) {
                                $("#qnt_" + id).val(quant + 1);
                            }
                        }
                        if (opt === 0 && quant > 1) {
                            if (id === "valor" || (id === "vezes" && quant < 61)) {
                                $("#qnt_" + id).val(quant - 1);
                            }
                        }
                        if (id === "valor") {
                        <?php if ($conf_qtdDecimal == 1) { ?>
                                $("#qnt_" + id).val(numberFormat($("#qnt_" + id).val()));
                        <?php } ?>
                            updateQnt();
                        }
                    }
                }

                function updateQnt() {
                    //SCRIPT PARA ATUALIZAR O VALOR TOTAL EM "QUANTIDADE"
                    var money = textToNumber($("#prc_valor").val()) * textToNumber($("#qnt_valor").val());
                    $("#prc_total").html(numberFormat(money));
                }

                function novaQuant(id, grp, pct) {
                    //SCRIPT PARA ADICIONAR UM PRODUTO EM "QUANTIDADE"
                    if ($(".products > .contents").is(":visible")) {
                        $(".active").removeClass("button active").addClass("button");
                        $("#but_" + id).removeClass("button").addClass("button active");
                        $("#prc_valor").val($("#prc_" + id).html());
                        $("#prc_total").html($("#prc_" + id).html());
                        $("#qnt_titulo").val($("#tit_" + id).html());
                        $("#qnt_codigo").val(id);
                        $("#qnt_grupo").val(grp);
                        $("#qnt_pacote").val(pct);
                        $("#qnt_valor").val("<?php echo ($conf_qtdDecimal == 1 ? escreverNumero(1) : "1"); ?>");
                        resizeAll();
                    }
                    var met = "S";
                    if ($(".clocktime .button .icon").html() === "<span class=\"ico-stack-3\"></span>") {
                        met = "P";
                    }
                    if (grp !== -1) {
                        $.getJSON("Ponto-de-Venda_controller.php", {listaVendComissaoGrupo: grp, tipo: met, empresa: <?php
                            if ($conf_filtroComissFilial == "1") {
                                echo $idFilial;
                            } else {
                                echo "0";
                            }
                            ?>}, function (j) {
                            var options = "<option value=\"NULL\">Selecione</option>";
                            if (j !== null) {
                                if (j.length > 0) {
                                    for (var i = 0; i < j.length; i++) {
                                        options = options + "<option value=\"" + j[i].id_fornecedores_despesas + "\">" + j[i].razao_social + "</option>";
                                    }
                                }
                            }
                            $("#vnd_comis").html(options);
                        });
                    }
                }

                function novaLinha() {
                    <?php if ($conf_comandaObr == "1" && $txtTpTela == "0") { ?>
                        if ($("#txtComanda").val() === "") {
                            bootbox.alert("Para inserir produtos e necessário incluir uma Comanda");
                            novaComanda();
                            return;
                        }
                    <?php } if ($conf_clienteObr == "1") { ?>
                        if ($("#user_cart").val() === "" || $("#user_cart").val() === "0") {
                            bootbox.alert("Para inserir produtos e necessário incluir um Cliente");
                            usuarioVenda();
                            return;
                        }
                    <?php } if ($conf_comissObr == "1" && $txtTpTela == "0") { ?>
                        if ($("#vnd_comis").val() === "NULL") {
                            bootbox.alert("É obrigatório informar um comissionado!");
                            return;
                        }
                    <?php } ?>
                    if ($("#menuServ").is(":visible") && $("#qnt_pacote").val() > 0 && $("#user_cart").val() === "") {
                        bootbox.alert("O uso de pacote somente é permitido após selecionar um cliente!");
                        return;
                    }
                    if ($("#txtId").val() !== "") {
                        adicionaItem($("#qnt_codigo").val(), $("#qnt_grupo").val(), $("#qnt_valor").val(), $("#prc_valor").val(), $("#vnd_comis").val(), 0, $("#user_cart").val(), 0, '');
                    } else {
                        adicionaCupom();
                    }
                }

                function adicionaItem(prod_cod, prod_grp, prod_qnt, prod_prc, prod_com, prod_pct, cod_cli, isTipoCupom, desc_servico) {
                    $.ajax({
                        type: "POST",
                        url: "Ponto-de-Venda_controller.php",
                        data: {insereItem: 'S',
                            id_vd: $("#txtId").val(),
                            prod_cod: prod_cod,
                            prod_grp: prod_grp,
                            prod_qnt: prod_qnt,
                            prod_prc: prod_prc,
                            prod_com: prod_com,
                            prod_pct: prod_pct,
                            cod_cli: cod_cli !== "" ? cod_cli : 0},
                        success: function (data) {
                            if (data.substr(0, 3) === "YES") {
                                carregaItensVenda('S');
                                carregaItensVenda('P');
                                if (isTipoCupom === 0 || isTipoCupom === 1) { //0 - Item Normal, 1 - Pacote, 2 - Frete
                                    if ($("#qnt_pacote").val() > 0) {
                                        if ($("#lcom_win").length === 0) {
                                            $.get("Ponto-de-Venda_itens_pacote.php", {cliente: cod_cli, venda: $("#txtId").val(), servico: prod_cod}, function (data) {
                                                $("body").append(data);
                                            });
                                        } else {
                                            $("#lcom_win").remove();
                                        }
                                    }
                                } else {
                                    novoTotal();
                                }
                            }
                        }
                    });
                }
                
                var id_pct_pai = 0;
                function adicionaCupom() {
                    //SCRIPT PARA ADICIONAR UM PRODUTO EM "CUPOM FISCAL"
                    if ($(".products > .contents").is(":visible")) {
                        var nmComissao = $("#vnd_comis").val() !== "NULL" ? "(" + $("#vnd_comis>option:selected").text() + ")" : '';
                        if ($(".clocktime .button .icon").html() === "<span class=\"ico-stack-3\"></span>") {
                            var conta = 1;
                            $(".receipts > .contents > .updown > #listProd").find("tr > .item_count").each(function () {
                                conta = conta + 1;
                            });
                            if (($("#prc_valor").val() !== numberFormat(0)) && ($("#qnt_valor").val() !== numberFormat(0)) && ($("#prc_total").html() !== numberFormat(0))) {
                                if (conta > 9 && conta < 100) {
                                    var codigo = "0" + conta;
                                } else if (conta < 10) {
                                    var codigo = "00" + conta;
                                } else {
                                    var codigo = conta;
                                }
                                $(".receipts > .contents > .updown > #listProd").append("<tr id=\"list_" + (conta + 1000) + "\" class=\"updown_normal\" onClick=\"selectLinha(" + (conta + 1000) + ",null)\"><input type=\"hidden\" name=\"prod_item[]\" value=\"NULL\"><input type=\"hidden\" name=\"prod_pct[]\" value=\"NULL\"><input type=\"hidden\" name=\"prod_com[]\" value=\"" + $("#vnd_comis").val() + "\"><input type=\"hidden\" name=\"prod_grp[]\" value=\"" + $("#qnt_grupo").val() + "\"><input type=\"hidden\" name=\"prod_cod[]\" value=\"" + $("#qnt_codigo").val() + "\"><input type=\"hidden\" name=\"prod_qnt[]\" value=\"" + $("#qnt_valor").val() + "\"><input type=\"hidden\" name=\"prod_prc[]\" value=\"" + $("#prc_valor").val() + "\"><td width=\"23px\" class=\"item_count\">" + codigo + "</td><td>" + $("#qnt_titulo").val() + " <span style=\"color: red;\">" + nmComissao + "</span></td><td width=\"23px\">" + $("#qnt_valor").val() + "</td><td width=\"52px\">" + $("#prc_valor").val() + "</td><td id=\"total_" + conta + "\" width=\"59px\" class=\"list_total\">" + $("#prc_total").html() + "</td></tr>");
                                limpaQuant();
                            }
                        } else {
                            id_pct_pai = id_pct_pai + 1;
                            var conta = 1;
                            $(".receipts > .contents > .updown > #listServ").find("tr > .item_count").each(function () {
                                conta = conta + 1;
                            });
                            if (($("#prc_valor").val() !== numberFormat(0)) && ($("#qnt_valor").val() !== "0") && ($("#prc_total").html() !== numberFormat(0))) {
                                if (conta > 9 && conta < 100) {
                                    var codigo = "0" + conta;
                                } else if (conta < 10) {
                                    var codigo = "00" + conta;
                                } else {
                                    var codigo = conta;
                                }
                                $(".receipts > .contents > .updown > #listServ").append("<tr id=\"list_" + (conta + 5000) + "\" class=\"updown_normal\" onClick=\"selectLinha(" + (conta + 5000) + ",null)\"><input type=\"hidden\" name=\"prod_item[]\" value=\"*" + id_pct_pai + "\"><input type=\"hidden\" name=\"prod_pct[]\" value=\"NULL\"><input type=\"hidden\" name=\"prod_com[]\" value=\"" + $("#vnd_comis").val() + "\"><input type=\"hidden\" name=\"prod_grp[]\" value=\"" + $("#qnt_grupo").val() + "\"><input type=\"hidden\" name=\"prod_cod[]\" value=\"" + $("#qnt_codigo").val() + "\"><input type=\"hidden\" name=\"prod_qnt[]\" value=\"" + $("#qnt_valor").val() + "\"><input type=\"hidden\" name=\"prod_prc[]\" value=\"" + $("#prc_valor").val() + "\"><td width=\"23px\" class=\"item_count\">" + codigo + "</td><td>" + $("#qnt_titulo").val() + " <span style=\"color: red;\">" + nmComissao + "</span></td><td width=\"23px\">" + $("#qnt_valor").val() + "</td><td width=\"52px\">" + $("#prc_valor").val() + "</td><td id=\"total_" + conta + "\" width=\"59px\" class=\"list_total\">" + $("#prc_total").html() + "</td></tr>");
                                if ($("#qnt_pacote").val() > 0) {
                                    listaItensPacote($("#qnt_codigo").val());
                                }
                                limpaQuant();
                            }
                        }
                        novoTotal();
                        resizeAll();
                    }
                }

                function novoTotal() {
                    //SCRIPT PARA ATUALIZAR O VALOR TOTAL EM "CUPOM FISCAL"
                    var money = 0.00;
                    $(".receipts > .contents > .updown > #listProd").find("tr > .list_total").each(function () {
                        money = money + textToNumber($(this).html());
                    });
                    $(".receipts > .contents > .updown > #listServ").find("tr > .list_total").each(function () {
                        money = money + textToNumber($(this).html());
                    });
                    $("#money_total").val(numberFormat(money));
                    $("#money").html(numberFormat(money, 1));
                }
                function selectLinha(id, item_venda) {
                    //SCRIPT PARA SELECIONAR UM PRODUTO EM "CUPOM FISCAL"
                    if ($(".products > .contents").is(":visible")) {
                        $(".updown_enable").removeClass("updown_enable").addClass("updown_normal");
                        $("#list_" + id).removeClass("updown_normal").addClass("updown_enable");
                        $("#rec_del").attr("onclick", "removeLinha(" + id + ", " + item_venda + ")");
                    }
                }
                function removeLinhaCupom(id) {
                    //SCRIPT PARA REMOVER UM PRODUTO DE "CUPOM FISCAL"
                    if ($(".products > .contents").is(":visible")) {
                        if (id < 5000) {
                            var conta = 1;
                            $("#list_" + id).remove();
                            $(".receipts > .contents > .updown > #listProd").find("tr > .item_count").each(function () {
                                if (conta > 9 && conta < 100) {
                                    $(this).html("0" + conta);
                                }
                                if (conta < 10) {
                                    $(this).html("00" + conta);
                                }
                                if (conta > 99) {
                                    $(this).html(conta);
                                }
                                conta = conta + 1;
                            });
                        } else {
                            var conta = 1;
                            $("#list_" + id).remove();
                            $(".receipts > .contents > .updown > #listServ").find("tr > .item_count").each(function () {
                                if (conta > 9 && conta < 100) {
                                    $(this).html("0" + conta);
                                }
                                if (conta < 10) {
                                    $(this).html("00" + conta);
                                }
                                if (conta > 99) {
                                    $(this).html(conta);
                                }
                                conta = conta + 1;
                            });
                        }
                        novoTotal();
                    }
                }
                
                function removeLinha(id, item_venda) {
                    if ($("#txtId").val() !== "") {
                        $.getJSON("Ponto-de-Venda_controller.php", {listaArvorePct: item_venda}, function (j) {
                            if (j !== null) {
                                for (var i = 0; i < j.length; i++) {
                                    $("input[value='" + j[i].id_item + "']").parent().remove();
                                }
                            }
                        }).done(function () {
                            $.ajax({
                                type: "POST",
                                url: "Ponto-de-Venda_controller.php",
                                data: {removeItem: 'S',
                                    id_item: item_venda},
                                success: function (data) {
                                    if (data.substr(0, 3) === "YES") {
                                        removeLinhaCupom(id);
                                    }
                                }
                            });
                        });
                    } else {
                        removeLinhaCupom(id);
                    }
                }
                
                function limpaQuant() {
                    //SCRIPT PARA LIMPAR AS INFORMAÇÕES CONTIDAS EM "QUANTIDADE"
                    $(".active").removeClass("button active").addClass("button");
                    $("#qnt_valor").val("<?php echo ($conf_qtdDecimal == 1 ? escreverNumero(0) : "0"); ?>");
                    $("#prc_valor").val(numberFormat(0));
                    $("#prc_total").html(numberFormat(0));
                    $("#qnt_titulo").val("");
                    $("#qnt_codigo").val("");
                    $("#qnt_grupo").val("");
                    $("#qnt_pacote").val("");
                }
                
                function limpaPagto() {
                    //SCRIPT PARA LIMPAR AS INFORMAÇÕES CONTIDAS EM "PAGAMENTOS"
                    $("#qnt_vezes").val("1");
                    $("#desc_valor").val(numberFormat(0));
                    $(".payments > .updown > .mylist").html("");
                    $("#money").html($("#money_total").val());
                    $("#prim_prazo, #data_venda").val(moment().format(lang["dmy"]));
                }
                
                function trocaTela(id) {
                    //SCRIPT PARA ALTERNAR A TELA DE PRODUTOS PARA PAGAMENTOS E VICE-VERSA
                    if ($(".receipts > .contents > .updown > #listServ").find("tr > .item_count").length > 0 ||
                            $(".receipts > .contents > .updown > #listProd").find("tr > .item_count").length) {
                        if (id === 1) {
                            $(".products > .contents, #buy_check").hide();
                            $(".products > .payments, #pay_check").show();
                        } else {
                            $(".products > .payments, #pay_check").hide();
                            $(".products > .contents, #buy_check").show();
                        }
                        if (textToNumber($("#money_total").val()) === 0.00) {
                            $(".products > .payments, #pay_check").find("#btnPagamento").hide();
                            $(".products > .payments, #pay_check").find("div >").attr('disabled', 'true');
                            $(".products > .payments, #pay_check").find("div >").find("select").css("background", "rgb(235, 235, 228)");
                        } else {
                            $(".products > .payments, #pay_check").find("#btnPagamento").show();
                            $(".products > .payments, #pay_check").find("div >").removeAttr('disabled');
                            $(".products > .payments, #pay_check").find("div >").find("select").css("background", "white");
                        }
                        $(".updown_enable").removeClass("updown_enable").addClass("updown_normal");
                        $(".products > .contents > .updown").html("");
                        limpaQuant();
                        limpaPagto();
                        resizeAll();
                    }
                }
                function abreSelect(elem) {
                    //SCRIPT PARA ABRIR UM SELECT AO CLICAR EM UM ÍCONE OU LINK
                    if (document.createEvent) {
                        var e = document.createEvent("MouseEvents");
                        e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                        elem[0].dispatchEvent(e);
                    } else if (element.fireEvent) {
                        elem[0].fireEvent("onmousedown");
                    }
                }
                //SCRIPT PARA ABRIR O CALENDÁRIO NO "VENCIMENTO"
                $.extend($.fn.pickadate.defaults, {
                    monthsFull: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
                    weekdaysShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
                    format: lang["dmy"].toLowerCase(),
                    clear: "Limpar",
                    today: "Hoje"
                });
                var pickerPrazo = $("#prim_prazo").pickadate({clear: ''}).pickadate("picker");
                $("#prim_click").click(function () {
                    if ($('#btnPagamento').is(':visible')) {
                        pickerPrazo.open();
                        event.stopPropagation();
                    }
                });
                var pickerDataV = $("#data_venda").pickadate(
                {onSet: function () { 
                $("#data_click").html(pickerDataV.get("select", lang["dmy"].toLowerCase()));}, clear: ''}).pickadate("picker");
                $("#data_click").click(function () {
                    pickerDataV.open();
                    event.stopPropagation();
                    $("#data_venda_root").find(".picker__holder").css("transform", 'translateX(-350px)');
                    pickerDataV.set('max', true);
                });
                //SCRIPT PARA TROCA DE SINAL EM "DESCONTO"
                $("#desc_click").click(function () {
                    if ($('#btnPagamento').is(':visible')) {
                        if ($("#desc_sinal").html() === "<b>%</b>") {
                            $("#desc_sinal").html("<b>$</b>");
                            $("#txtProdSinal").val("1");
                        } else {
                            $("#desc_sinal").html("<b>%</b>");
                            $("#txtProdSinal").val("0");
                        }
                        novoDesconto();
                    }
                });
                
                function novoDesconto() {
                    //SCRIPT PARA APLICAR NOVO "DESCONTO" QUANDO ALTERADO
                    var calcTotal = textToNumber($("#money_total").val());
                    var calcDesct = textToNumber($("#desc_valor").val());
                    if ($("#desc_sinal").html() === "<b>%</b>") {
                        calcDesct = calcTotal * (calcDesct / 100);
                    }
                    if (calcDesct >= calcTotal || calcDesct < 0.00) {
                        calcDesct = 0.00;
                    }
                    $("#money").html(numberFormat(calcTotal - calcDesct, 1));
                    if ($(".payments > .updown > .mylist").html() !== "") {
                        novaParcela();
                    }
                }
                
                function novaCarteira() {
                    //SCRIPT PARA FAZER AS VERIFICAÇÕES NECESSÁRIAS NA OPÇÃO CARTEIRA
                    var cartForma = parseInt($("#form_pagto option:selected").val());
                    $(".payments > .updown > .mylist").html("");
                    var permissaoAtual = <?php
                    if (is_numeric($ckb_fin_crt_)) {
                        echo $ckb_fin_crt_;
                    } else {
                        echo "0";
                    }
                    ?>;
                    if (cartForma === 12) {
                        if ($("#user_cart option:selected").val() !== "" && permissaoAtual === 1) {
                            $("#user_cart").css("background-color", "#FFFFFF");
                            novaParcela();
                        } else if ($("#user_cart option:selected").val() !== "") {
                            $("#user_cart").css("background-color", "#FFFFFF");
                            novaPermissao(0);
                        } else {
                            $("#user_cart").css("background-color", "#FAC8C8");
                        }
                    } else {
                        novaParcela();
                    }
                }
                function recalcula(id) {
                    var parcTotal = textToNumber($("#money").text());
                    var parAtual = 0;
                    for (var i = 1; i <= id; i++) {
                        parAtual = parAtual + textToNumber($("#parc_valor_" + i).val());
                    }
                    var parcRest = (parcTotal - parAtual) / (eval($("#qnt_vezes").val()) - id);
                    for (var i = id + 1; i <= eval($("#qnt_vezes").val()); i++) {
                        $("#parc_valor_" + i).val(numberFormat(parcRest));
                    }
                }
                function novaParcela() {
                    //SCRIPT PARA GERAR AS "PARCELAS" QUANDO CLICADO
                    var parcTotal = textToNumber($("#money_total").val());
                    var parcDesct = textToNumber($("#desc_valor").val());
                    var parcForma = parseInt($("#form_pagto option:selected").val());
                    var parcVezes = parseInt($("#qnt_vezes").val());
                    var parcPagto = $("#form_pagto").html();
                    if ($("#desc_sinal").html() === "<b>%</b>") {
                        parcDesct = parcTotal * (parcDesct / 100);
                    }
                    if (parcDesct >= parcTotal || parcDesct < 0.00) {
                        parcDesct = 0.00;
                    }
                    var parcValor = numberFormat((parcTotal - parcDesct) / parcVezes);
                    if (parcVezes === 1 && parcForma === 6) {
                        var parcLista = "<table cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%; margin-top:10px\"><tr><td width=\"60%\"><div class=\"ready\">" + parcValor + "</div></td><td width=\"40%\"><div class=\"button\" style=\"background-color:#009AD7\"><div class=\"icon\" style=\"margin-top:1px\"><b>TOTAL</b></div></div></td></tr></table>";
                        var parcLista = parcLista + "<table cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%; margin-top:10px\"><tr><td width=\"60%\"><div class=\"ready\"><input id=\"pagto_valor\" name=\"pagto_valor\" type=\"text\" class=\"value\" value=\"" + parcValor + "\" onBlur=\"novoTroco()\"></div></td><td width=\"40%\"><div class=\"button\" style=\"background-color:#68AF27\"><div class=\"icon\" style=\"margin-top:1px\"><b>PAGTO</b></div></div></td></tr></table>";
                        var parcLista = parcLista + "<table cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%; margin-top:10px\"><tr><td width=\"60%\"><div id=\"pagto_troco\" class=\"ready\">" + numberFormat(0) + "</div></td><td width=\"40%\"><div class=\"button\" style=\"background-color:#D53F26\"><div class=\"icon\" style=\"margin-top:1px\"><b>TROCO</b></div></div></td></tr></table>";
                        $(".payments > .updown > .mylist").html("<div id=\"tdp_1\" class=\"label\">Detalhamento do Pagamento</div>" + parcLista);
                        $("#pagto_valor").keypad();
                        $("#pagto_valor").maskMoney({thousands: lang["thousandsSeparator"], decimal: lang["centsSeparator"]});
                    } else {
                        var parcLista = "";
                        for (var i = 1; i <= parcVezes; i++) {
                            var aDate = moment($("#prim_prazo").val(), lang["dmy"], true);                
                            aDate = aDate.add((i - 1), 'month').format(lang["dmy"]);                                   
                            parcLista = parcLista + "<tr><td>" + (i < 10 ? "0" + i : i) + "</td><td><input id=\"parc_prazo_" + i + "\" name=\"parc_prazo_" + i + "\" type=\"text\" class=\"value\" value=\"" + aDate + "\"/></td><td><input id=\"parc_valor_" + i + "\" name=\"parc_valor_" + i + "\" type=\"text\" class=\"value\" value=\"" + parcValor + "\" onchange=\"recalcula(" + i + ");\" /></td><td><select id=\"parcPagto_" + i + "\" name=\"parcPagto_" + i + "\" class=\"select\">" + parcPagto + "</select></td></tr>";
                        }
                        $(".payments > .updown > .mylist").html("<div id=\"tdp_2\" class=\"label\">Detalhamento das Parcelas</div><div class=\"input\"><table><tr bgcolor=\"#DDD\"><td width=\"7%\">Nº</td><td width=\"28%\">Vencimento</td><td width=\"20%\">Valor</td><td width=\"45%\">Forma de Pagto</td></tr>" + parcLista + "</table></div>");
                        var TotalVenda = textToNumber($("#money").text());
                        var parTotal = 0;
                        for (var i = 1; i <= eval($("#qnt_vezes").val()); i++) {
                            parTotal = parTotal + textToNumber($("#parc_valor_" + i).val());
                        }
                        var difTotal = textToNumber($("#parc_valor_" + eval($("#qnt_vezes").val())).val()) + (TotalVenda - parTotal);
                        $("#parc_valor_" + eval($("#qnt_vezes").val())).val(numberFormat(difTotal));
                        novoCalendario(parcVezes, parcForma);
                        //CORREÇÃO DAS PARCELAR COM NÚMEROS NÃO-REDONDO
                        var parcValue = parseFloat((parcTotal - parcDesct) / parcVezes);
                        var parcFixed = parseFloat((parcTotal - parcDesct) - (parcValue * parcVezes));
                        $("#parc_valor_1").val(numberFormat(parcValue + parcFixed));
                        //CORREÇÃO PARA A OPÇÃO CARTEIRA NÃO SER EXIBIDA QUANDO NÃO SELECIONADA
                        if (parcForma !== 12) {
                            for (var i = 1; i <= parcVezes; i++) {
                                $("#parcPagto_" + i + " option[value=\"12\"]").remove();
                            }
                        }
                    }
                }

                function novoUserLogin() {
                    //POPUP DE USUÁRIO PARA TROCA DE USUÁRIO E LOGOUT
                    if ($("#user_pop").length === 0) {
                        var sizew = $(window).width() - (140 + ($(".clocktime").innerWidth() - $(".clocktime").width()));
                        var sizeh = 50 + (($(".clocktime").innerHeight() - $(".clocktime").height()) / 2);
                        var user_pop = "<div id=\"user_pop\">";
                        user_pop = user_pop + "	<div class=\"keypad-login\" style=\"width:130px; height:50px; position:absolute; top:" + sizeh + "px; left:" + sizew + "px; display:block\">";
                        user_pop = user_pop + "		<button type=\"button\" class=\"keypad-special keypad-log-btn\" style=\"width:60px; height:50px; background-color:#D53F26\" onClick=\"window.top.location.href='https://sivisweb.com.br/login.php?ex=1'\"><div class=\"icon\"><span class=\"ico-signout\"></span></div></button>";
                        user_pop = user_pop + "		<button type=\"button\" class=\"keypad-special keypad-log-btn\" style=\"width:60px; height:50px; background-color:#FFAA31\" onClick=\"novaPermissao(1); novoUserLogin()\"><div class=\"icon\"><span class=\"ico-locked\"></span></div></button>";
                        user_pop = user_pop + "	</div>";
                        user_pop = user_pop + "</div>";
                        $("body").append(user_pop);
                    } else {
                        $("#user_pop").remove();
                    }
                }
                
                function novaPermissao(login) {
                    //TELA DE LOGIN PARA SOLICITAR PERMISSÃO PARA VENDA EM CARTEIRA
                    if ($("#perm_win").length === 0) {
                        if (login === 1) {
                            var label = "TROCA DE SESSÃO";
                        } else {
                            var label = "PERMISSÃO";
                        }
                        var perm_pop = "<div id=\"perm_win\">";
                        perm_pop = perm_pop + "	<div style=\"width:100%; height:100%; position:absolute; top:0; left:0; z-index:9\"><img src=\"./../../img/bg_preto.png\" style=\"width:100%; height:100%\"></div>";
                        perm_pop = perm_pop + "	<div class=\"keypad-popup\" style=\"width:268px; height:208px; position:absolute; top:50%; left:50%; margin-top:-104px; margin-left:-134px; display:block\">";
                        perm_pop = perm_pop + "		<div class=\"keypad-row\" style=\"width:248px; height:40px; font-size:18px; color:#333; text-align:center\"><b>" + label + "</b></div>";
                        perm_pop = perm_pop + "		<div class=\"keypad-row\" style=\"height:50px\">Login:<br><input id=\"perm_user\" name=\"perm_user\" type=\"text\" class=\"value\" style=\"width:244px\"></div>";
                        perm_pop = perm_pop + "		<div class=\"keypad-row\" style=\"height:50px\">Senha:<br><input id=\"perm_pass\" name=\"perm_pass\" type=\"password\" class=\"value\" style=\"width:244px\"></div>";
                        perm_pop = perm_pop + "		<div class=\"keypad-row\" style=\"margin-top:10px\">";
                        perm_pop = perm_pop + "			<button type=\"button\" class=\"keypad-special keypad-clear\" style=\"margin-right:42px\" onClick=\"novaChecagem(" + login + ")\">Concluir</button>";
                        perm_pop = perm_pop + "			<button type=\"button\" class=\"keypad-special keypad-clear\" style=\"margin-left:42px\" onClick=\"novaPermissao(" + login + ")\">Cancelar</button>";
                        perm_pop = perm_pop + "		</div>";
                        perm_pop = perm_pop + "	</div>";
                        perm_pop = perm_pop + "</div>";
                        $("body").append(perm_pop);
                    } else {
                        $("#perm_win").remove();
                    }
                }
                
                function novaChecagem(login) {
                    //VERIFICAÇÃO DE LOGIN DA PERMISSÃO ATRAVÉS DO JSON
                    if (login === 1) {
                        $("#log_user").val($("#perm_user").val());
                        $("#log_pass").val($("#perm_pass").val());
                        setTimeout(function () {
                            $("#log_form").submit();
                        }, 500);
                    } else {
                        $.getJSON("logar.ajax.php", {login: $("#perm_user").val(), senha: $("#perm_pass").val(), tipo: login}, function (data) {
                            if (data[0].valor && login === 0) {
                                novaPermissao(0);
                                novaParcela();
                            } else if (data[0].valor && login === 2) {
                                novaPermissao(0);
                                cancelaVenda();
                            } else {
                                bootbox.alert("Ocorreu um dos seguintes erros:\n- Login ou senha incorreta.\n- Conta com permissão incompatível.");
                            }
                        });
                    }
                }
                
                function cancelaNota(idVenda) {
                    bootbox.confirm('Confirmar cancelamento da venda?',
                    function (result) {
                        if (result === true) {
                            $.post("Ponto-de-Venda_controller.php", {
                            idCancelarNota: idVenda})
                                .done(function (data) {
                                    $("#last_win").remove();
                                    listaVendas();
                                });
                        } else {
                            return;
                        }
                    });
                }
                
                function listaVendas() {
                    //JANELA PARA LISTAR AS ÚLTIMAS VENDAS REALIZADAS
                    if ($("#last_win").length === 0) {
                        $.get("Ponto-de-Venda_vendas.php", function (data) {
                            $("body").append(data);
                        });
                    } else {
                        $("#last_win").remove();
                    }
                }
                
                function contaCaixa() {
                    if ($("#cont_win").length === 0) {
                        var cont_pop = "<div id=\"cont_win\">";
                        cont_pop = cont_pop + "	<div style=\"width:100%; height:100%; position:absolute; top:0; left:0; z-index:9\"><img src=\"./../../img/bg_preto.png\" style=\"width:100%; height:100%\"></div>";
                        cont_pop = cont_pop + "	<div class=\"keypad-popup\" style=\"width:318px; height:350px; position:absolute; top:50%; left:50%; margin-top:-204px; margin-left:-159px; display:block\">";
                        cont_pop = cont_pop + "		<div class=\"keypad-row\" style=\"width:318px; margin:0px;text-align:center\">";
                        cont_pop = cont_pop + "             <table class=\"resumo_caixa\" cellspacing=\"0\" cellpadding=\"0\">";
                        cont_pop = cont_pop + "                 <tr>";
                        cont_pop = cont_pop + "                     <td width=\"30px\">";
                        cont_pop = cont_pop + " 			<input style=\"width:70px;height:46px;margin-right:5px;text-align: center;\" id=\"txtData\" type=\"text\" class=\"input-medium\" onchange=\"refresh_resumo_caixa();\" maxlength=\"10\" value=\"<?php echo getData("T"); ?>\"/>";
                        cont_pop = cont_pop + "                     </td>";
                        cont_pop = cont_pop + "                     <td width=\"100%\">";
                        cont_pop = cont_pop + "                         <div class=\"label\">Vendedor</div>";
                        cont_pop = cont_pop + "                         <div class=\"input\">";
                        cont_pop = cont_pop + "                             <select id=\"form_vend\" name=\"form_vend\" onchange=\"refresh_resumo_caixa();\" class=\"value select\">";
                        cont_pop = cont_pop + "                                 <option value=''>TODOS</option>";
<?php
$cur = odbc_exec($con, "select login_user,nome from sf_usuarios where inativo = 0 order by nome") or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) { ?>
                            cont_pop = cont_pop + "                                 <?php
    echo "<option value='" . utf8_encode($RFP["login_user"]) . "' ";
    if (!(strcmp($RFP['login_user'], $_SESSION["login_usuario"]))) {
        echo "SELECTED";
    }
    echo " >" . utf8_encode(strtoupper($RFP["login_user"])) . "</option>";
    ?> ";
<?php } ?>
                        cont_pop = cont_pop + "                             </select>";
                        cont_pop = cont_pop + "                         </div>";
                        cont_pop = cont_pop + "                     </td>";
                        cont_pop = cont_pop + "                     <td width=\"30px\">";
                        cont_pop = cont_pop + "                         <div class=\"button\" style=\"width:50px; background-color:#009AD7\" onClick=\"abreSelect(form_vend)\">";
                        cont_pop = cont_pop + "                             <div class=\"icon\"><span class=\"ico-chevron-down\"></span></div>";
                        cont_pop = cont_pop + "                         </div>";
                        cont_pop = cont_pop + "                     </td>";
                        cont_pop = cont_pop + "                 </tr>";
                        cont_pop = cont_pop + "             </table>";
                        cont_pop = cont_pop + "		</div>";
                        cont_pop = cont_pop + "		<div class=\"keypad-row\" style=\"border: 1px solid #CCC;width:318px; height:235px; margin:0px;margin-top:5px; text-align:center\">";
                        cont_pop = cont_pop + "             <table width=\"100%\">";
                        cont_pop = cont_pop + "                 <tr style=\"background-color:#333; color:#EEE\"><td colspan=\"3\"><b>RESUMO DO CAIXA</b></td></tr>";
                        cont_pop = cont_pop + "             </table>";
                        cont_pop = cont_pop + "             <table id=\"tbl_resumo_caixa\" width=\"100%\"></table>";
                        cont_pop = cont_pop + "		</div>";
                        cont_pop = cont_pop + "		<div class=\"keypad-row\" style=\"width:318px; margin:0px; margin-top:5px; text-align:center\">";
                        cont_pop = cont_pop + "			<button type=\"button\" class=\"keypad-special keypad-clear\" style=\"margin-right:20px\" onClick=\"abreBox()\" >Imprimir</button>";
                        cont_pop = cont_pop + "			<button type=\"button\" class=\"keypad-special keypad-clear\" style=\"margin-left:20px\" onClick=\"contaCaixa()\">Cancelar</button>";
                        cont_pop = cont_pop + "			<script>$(\"#txtData\").pickadate().pickadate(\"picker\");<\/script>";
                        cont_pop = cont_pop + "		</div>";
                        cont_pop = cont_pop + "	</div>";
                        cont_pop = cont_pop + "</div>";
                        $("body").append(cont_pop);
                        refresh_resumo_caixa();
                    } else {
                        $("#cont_win").remove();
                    }
                }
                
                function cancelaVenda() {
                    if ($("#txtId").val() !== "") {
                        $.ajax({
                            type: "POST",
                            url: "Ponto-de-Venda_controller.php",
                            data: {cancelaVenda: $("#txtId").val()},
                            success: function (data) {
                                if (data.substr(0, 3) === "YES") {
                                    window.top.location.href = 'Ponto-de-Venda.php?tp=<?php echo $txtTpTela; ?>';
                                }
                            }
                        });
                    } else {
                        carrega(0);
                    }
                }

                function entregaVenda() {
                    //JANELA PARA CONFIGURAR A ENTREGA
                    if ($("#trans_win").length === 0) {
                        var disable = "";
                        if ($("#txtTransportadora").val() !== "" && $("#txtId").val() !== "") {
                            disable = "disabled";
                        }
                        var transp = $("#txtTransportadora").val();
                        var trans_pop = "<div id=\"trans_win\">";
                        trans_pop = trans_pop + "	<div style=\"width:100%; height:100%; position:absolute; top:0; left:0; z-index:9\"><img src=\"./../../img/bg_preto.png\" style=\"width:100%; height:100%\"></div>";
                        trans_pop = trans_pop + "	<div class=\"keypad-popup\" style=\"width:338px; height:208px; position:absolute; top:50%; left:50%; margin-top:-104px; margin-left:-169px; display:block; z-index:9\">";
                        trans_pop = trans_pop + "		<div class=\"keypad-row\" style=\"width:338px; height:150px; margin:0px; text-align:center\">";
                        trans_pop = trans_pop + "			<div class=\"trans_venda\" style=\"width:100%\">";
                        trans_pop = trans_pop + "				<div style=\"float:left; width:calc(100% - 60px)\">";
                        trans_pop = trans_pop + "					<div class=\"label\">Transportadora</div>";
                        trans_pop = trans_pop + "					<div class=\"input\">";
                        trans_pop = trans_pop + "						<select id=\"trans_nome\" name=\"trans_nome\" class=\"value select\" " + disable + ">";
<?php
$cur = odbc_exec($con, "select id_fornecedores_despesas,razao_social from sf_fornecedores_despesas where tipo = 'T' order by razao_social") or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) { ?>
                            trans_pop = trans_pop + "                                 <?php echo "<option value='" . utf8_encode($RFP["id_fornecedores_despesas"]) . "'>" . utf8_encode(strtoupper($RFP["razao_social"])) . "</option>"; ?> ";
<?php } ?>
                        trans_pop = trans_pop + "						</select>";
                        trans_pop = trans_pop + "					</div>";
                        trans_pop = trans_pop + "				</div>";
                        trans_pop = trans_pop + "				<div class=\"button\" style=\"float:left; width:50px; background-color:#009AD7\" onClick=\"abreSelect(trans_nome)\">";
                        trans_pop = trans_pop + "					<div class=\"icon\"><span class=\"ico-chevron-down\"></span></div>";
                        trans_pop = trans_pop + "				</div>";
                        trans_pop = trans_pop + "				<div style=\"clear:both\"></div>";
                        trans_pop = trans_pop + "				<div class=\"label\" style=\"margin-top:10px\">Valor de Custo do Transporte:</div>";
                        trans_pop = trans_pop + "				<div class=\"ready\" style=\"display:table-cell\">";
                        trans_pop = trans_pop + "					<input " + disable + " id=\"trans_valor\" name=\"trans_valor\" type=\"text\" class=\"value\" readonly=\"readonly\" style=\"background-color: white;\">";
                        trans_pop = trans_pop + "				</div>";
                        trans_pop = trans_pop + "				<div class=\"button\" style=\"display:table-cell; width:35%; background-color:#009AD7\">";
                        trans_pop = trans_pop + "					<div class=\"icon\" style=\"margin-top:1px\"><b>TOTAL</b></div>";
                        trans_pop = trans_pop + "				</div>";
                        trans_pop = trans_pop + "			</div>";
                        trans_pop = trans_pop + "		</div>";
                        trans_pop = trans_pop + "		<div class=\"keypad-row\" style=\"width:338px; margin:0px; text-align:center\">";
                        if ($("#txtTransportadora").val() !== "") {
                            trans_pop = trans_pop + "			<button type=\"button\" class=\"keypad-special keypad-clear\" style=\"margin-right:20px;\" onClick=\"removeFrete()\" >Remover</button>";
                        } else {
                            trans_pop = trans_pop + "			<button type=\"button\" class=\"keypad-special keypad-clear\" style=\"margin-right:20px\" onClick=\"entregaVenda()\" >Aceitar</button>";
                        }
                        trans_pop = trans_pop + "			<button type=\"button\" class=\"keypad-special keypad-clear\" style=\"margin-left:20px\" onClick=\"fechaFrete()\">Cancelar</button>";
                        trans_pop = trans_pop + "		</div>";
                        trans_pop = trans_pop + "	</div>";
                        trans_pop = trans_pop + "</div>";
                        $("body").append(trans_pop);
                        $("#trans_valor").keypad();
                        $("#trans_valor").maskMoney({thousands: lang["thousandsSeparator"], decimal: lang["centsSeparator"]});
                        $("#trans_nome").val($("#txtTransportadora").val());
                        $("#trans_valor").val($("#txtValorFrete").val());
                    } else {
                        $("#txtTransportadora").val($("#trans_nome").val());
                        $("#txtValorFrete").val($("#trans_valor").val());
                        if ($("#txtId").val() !== "") {
                            adicionaItem($("#txtFrete").val(), $("#txtFreteGrupo").val(), 1, $("#trans_valor").val(), 'NULL', 0, $("#user_cart").val(), 2, '');
                            $.ajax({
                                type: "POST",
                                url: "Ponto-de-Venda_controller.php",
                                data: {insereItemFrete: 'S',
                                    idVenda: $("#txtId").val(),
                                    txtTransportadora: $("#txtTransportadora").val(),
                                    txtValorFrete: $("#txtValorFrete").val(),
                                    txtEmpresa: $("#txtFilial").val()},
                                success: function (data) {
                                    if (data.substr(0, 3) === "YES") {
                                        carregaItensVenda('S');
                                    }
                                }
                            });
                        } else {
                            adicionaCupomFrete();
                        }
                        $("#trans_win").remove();
                    }
                }
                
                function fechaFrete() {
                    $("#trans_win").remove();
                }
                
                function adicionaCupomFrete() {
                    var conta = 1;
                    var codigo = "";
                    $(".receipts > .contents > .updown > #listServ").find("tr > .item_count").each(function () {
                        conta = conta + 1;
                    });
                    if ($("#trans_valor").val() !== numberFormat(0) && $("#trans_valor").val() !== "" && $("#txtFrete").val() !== "") {
                        if (conta > 9 && conta < 100) {
                            codigo = "0" + conta;
                        } else if (conta < 10) {
                            codigo = "00" + conta;
                        } else {
                            codigo = conta;
                        }
                        $(".receipts > .contents > .updown > #listServ").append("<tr id=\"list_" + (conta + 5000) + "\" class=\"updown_normal\" onClick=\"selectLinha(" + (conta + 5000) + ",null)\"><input type=\"hidden\" name=\"prod_item[]\" value=\"NULL\"><input type=\"hidden\" name=\"prod_pct[]\" value=\"NULL\"><input type=\"hidden\" name=\"prod_com[]\" value=\"NULL\"><input type=\"hidden\" name=\"prod_grp[]\" value=\"" +
                        $("#txtFreteGrupo").val() + "\"><input type=\"hidden\" name=\"prod_cod[]\" value=\"" +
                        $("#txtFrete").val() + "\"><input type=\"hidden\" name=\"prod_qnt[]\" value=\"1\"><input type=\"hidden\" name=\"prod_prc[]\" value=\"" +
                        $("#trans_valor").val() + "\"><td width=\"23px\" class=\"item_count\">" + codigo + "</td><td>FRETE</td><td width=\"23px\">1</td><td width=\"52px\">" +
                        $("#trans_valor").val() + "</td><td id=\"total_" + conta + "\" width=\"59px\" class=\"list_total\">" +
                        $("#trans_valor").val() + "</td></tr>");
                    }
                    novoTotal();
                }

                function removeFrete() {
                    $.ajax({
                        type: "POST",
                        url: "Ponto-de-Venda_controller.php",
                        data: {removeItemFrete: $("#txtId").val()},
                        success: function (data) {
                            if (data.substr(0, 3) === "YES") {
                                $("#txtTransportadora").val("");
                                $("#txtValorFrete").val("");
                                fechaFrete();
                            }
                        }
                    });
                }

                function comentaVenda(acao) {
                    //JANELA PARA CONFIGURAR A ENTREGA
                    if ($("#coment_win").length === 0) {
                        var coment_pop = "<div id=\"coment_win\">";
                        coment_pop = coment_pop + "	<div style=\"width:100%; height:100%; position:absolute; top:0; left:0; z-index:9\"><img src=\"./../../img/bg_preto.png\" style=\"width:100%; height:100%\"></div>";
                        coment_pop = coment_pop + "	<div class=\"keypad-popup\" style=\"width:338px; height:208px; position:absolute; top:50%; left:50%; margin-top:-104px; margin-left:-169px; display:block; z-index:9\">";
                        coment_pop = coment_pop + "		<div class=\"keypad-row\" style=\"width:338px; height:150px; margin:0px; text-align:center\">";
                        coment_pop = coment_pop + "			<div class=\"trans_venda\" style=\"width:100%\">";
                        coment_pop = coment_pop + "				<div class=\"label\">Comentários:</div>";
                        coment_pop = coment_pop + "				<div class=\"ready\" style=\"height:110px; width:calc(100% - 2px); border-right:1px solid #CCC\">";
                        coment_pop = coment_pop + "					<textarea id=\"coment_text\" name=\"coment_text\" style=\"width:calc(100% - 5px); height:calc(100% - 5px); border:0; resize:none\"></textarea>";
                        coment_pop = coment_pop + "				</div>";
                        coment_pop = coment_pop + "			</div>";
                        coment_pop = coment_pop + "		</div>";
                        coment_pop = coment_pop + "		<div class=\"keypad-row\" style=\"width:338px; margin:0px; text-align:center\">";
                        coment_pop = coment_pop + "			<button type=\"button\" class=\"keypad-special keypad-clear\" style=\"margin-right:20px\" onClick=\"comentaVenda(1)\" >Aceitar</button>";
                        coment_pop = coment_pop + "			<button type=\"button\" class=\"keypad-special keypad-clear\" style=\"margin-left:20px\" onClick=\"comentaVenda(0)\">Cancelar</button>";
                        coment_pop = coment_pop + "		</div>";
                        coment_pop = coment_pop + "	</div>";
                        coment_pop = coment_pop + "</div>";
                        $("body").append(coment_pop);
                        $("#coment_text").val($("#txtComentarios").val());
                    } else {
                        if (acao === 1) {
                            $("#txtComentarios").val($("#coment_text").val());
                            if ($("#txtId").val() !== "" && acao === 1) {
                                salvaComanda();
                            }
                        }
                        $("#coment_win").remove();
                    }
                }

                function usuarioVenda() {
                    //JANELA PARA CONFIGURAR O USUÁRIO
                    if ($("#user_win").length === 0) {
                        $.get("Ponto-de-Venda_usuarios.php", function (data) {
                            $("body").append(data);
                            $("#user_text").focus();
                        });
                    } else {
                        $("#user_win").remove();
                    }
                }

                function limpaTelaComanda(tipo) {
                    if (tipo === "L") {
                        window.top.location.href = 'Ponto-de-Venda.php?tp=' + $("#txtTpTela").val();
                    } else {
                        window.top.location.href = 'Ponto-de-Venda.php?newCom=1&tp=' + $("#txtTpTela").val();
                    }
                }

                function geraCupom() {
                    <?php if (is_numeric($_GET['id'])) { ?>
                        window.open('cupom.php?id=' +<?php echo $_GET['id']; ?> + '&crt=' + <?php echo $_SESSION["contrato"]; ?>, '_blank');
                        location.replace('Ponto-de-Venda.php?tp=0');
                    <?php } else { ?>
                        alert("Erro nos parametros da página!");
                    <?php } ?>
                }

                function geraNFCe() {
                    <?php if (is_numeric($_GET['nfe'])) { ?>
                        $("#txtId").val('<?php echo $_GET['nfe']; ?>');
                        $("#txtModelo").val('65');
                        assinar_nota();
                    <?php } else { ?>
                        alert("Erro nos parametros da página!");
                    <?php } ?>
                }

                function telaImpressao() {
                    if ($("#gera_impressao").length === 0) {
                        <?php if ($isNFCe === "S") { ?>
                            var disabled = "";
                        <?php } else { ?>
                            var disabled = "disabled";
                        <?php } ?>
                        var impr_pop = "<div id=\"gera_impressao\">";
                        impr_pop = impr_pop + "	<div style=\"width:100%; height:100%; position:absolute; top:0; left:0; z-index:9\"><img src=\"./../../img/bg_preto.png\" style=\"width:100%; height:100%\"></div>";
                        impr_pop = impr_pop + "	<div class=\"keypad-popup\" style=\"width:338px; height:117px; position:absolute; top:50%; left:50%; margin-top:-104px; margin-left:-169px; display:block; z-index:9\">";
                        impr_pop = impr_pop + "		<div class=\"keypad-row\" style=\"width:338px; margin:0px; text-align:center\">";
                        impr_pop = impr_pop + "			<div class=\"trans_venda\" style=\"width:100%\">";
                        impr_pop = impr_pop + "				<div style=\"clear:both\"></div>";
                        impr_pop = impr_pop + "				<div class=\"label\" style=\"font-size:28px;height: 27px;padding: 10px 9px 3px 5px;\">Impressão</div>";
                        impr_pop = impr_pop + "			</div>";
                        impr_pop = impr_pop + "		</div>";
                        impr_pop = impr_pop + "		<div class=\"keypad-row\" style=\"width:338px; margin:0px; text-align:center;padding-top:10px;\">";
                        impr_pop = impr_pop + "			<button " + disabled + " type=\"button\" class=\"keypad-special keypad-clear\" style=\"font-size: 35px;width: 43%;\" onClick=\"geraNFCe()\" >NFC-e</button>";
                        impr_pop = impr_pop + "			<button type=\"button\" class=\"keypad-special keypad-clear\" style=\"margin-left:20px;font-size: 35px;width: 43%;\" onClick=\"geraCupom()\">CUPOM</button>";
                        impr_pop = impr_pop + "		</div>";
                        impr_pop = impr_pop + "	</div>";
                        impr_pop = impr_pop + "</div>";
                        $("body").append(impr_pop);
                    } else {
                        $("#gera_impressao").remove();
                    }
                }

                function novaComanda() {
                    //JANELA PARA NOVA COMANDA
                    if ($("#ncom_win").length === 0) {
                        var isTeclado = "onkeypress=\"return runScript(event, 'ncom_valor');\"";
                        var maskInput = "";
                        <?php if ($conf_comandaPers == 1) { ?>
                            isTeclado = '';
                            var maskInput = "placeholder=\"<?php echo $conf_comandaMask; ?>\"";
                        <?php } ?>
                        var ncom_pop = "<div id=\"ncom_win\">";
                        ncom_pop = ncom_pop + "	<div style=\"width:100%; height:100%; position:absolute; top:0; left:0; z-index:9\"><img src=\"./../../img/bg_preto.png\" style=\"width:100%; height:100%\"></div>";
                        ncom_pop = ncom_pop + "	<div class=\"keypad-popup\" style=\"width:338px;<?php
                        if ($conf_indicador == "1") {
                            echo "height:319px;";
                        } else {
                            echo "height:259px;";
                        }
                        ?>position:absolute; top:50%; left:50%;<?php
                        if ($conf_indicador == "1") {
                            echo "margin-top:-200px;";
                        } else {
                            echo "margin-top:-180px;";
                        }
                        ?> margin-left:-169px; display:block; z-index:9\">";
                        ncom_pop = ncom_pop + "		<div class=\"keypad-row\" style=\"width:338px; height:150px; margin:0px; text-align:center\">";
                        ncom_pop = ncom_pop + "			<div class=\"trans_venda\" style=\"width:100%\">";
                        ncom_pop = ncom_pop + "				<div class=\"label\">Número da Comanda:</div>";
                        ncom_pop = ncom_pop + "				<div class=\"ready\" style=\"display:table-cell; border-right:1px solid #CCC\">";
                        ncom_pop = ncom_pop + "					<input id=\"ncom_valor\" name=\"ncom_valor\" type=\"text\" class=\"value\" readonly=\"false\" " + isTeclado + " " + maskInput + " >";
                        ncom_pop = ncom_pop + "				</div>";
                        ncom_pop = ncom_pop + "				<div style=\"clear:both\"></div>";
                        ncom_pop = ncom_pop + "				<div style=\"float:left; width:calc(100% - 60px)\">";
                        ncom_pop = ncom_pop + "					<div class=\"label\" style=\"margin-top:10px\">Cliente:</div>";
                        ncom_pop = ncom_pop + "					<div id=\"nmClienteComanda\" maxlength=\"20\" style=\"text-align: left;\" class=\"input\"> </div>";
                        ncom_pop = ncom_pop + "				</div>";
                        ncom_pop = ncom_pop + "				<div class=\"button\" style=\"float:left; width:50px; margin-top:10px; background-color:#009AD7\" onClick=\"usuarioVenda()\">";
                        ncom_pop = ncom_pop + "					<div class=\"icon\"><span class=\"ico-search\"></span></div>";
                        ncom_pop = ncom_pop + "				</div>";
                        ncom_pop = ncom_pop + "				<div style=\"float:left; width:calc(100% - 60px)\">";
                        ncom_pop = ncom_pop + "					<div class=\"label\" style=\"margin-top:10px\">Vendedor(a):</div>";
                        ncom_pop = ncom_pop + "					<div class=\"input\">";
                        ncom_pop = ncom_pop + "						<select id=\"ncom_nome\" name=\"ncom_nome\" class=\"value select\">";
                        <?php
                        $cur = odbc_exec($con, "select funcionario, login_user from sf_usuarios where inativo = 0 order by nome") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                        ncom_pop = ncom_pop + "                                 <?php echo "<option value='" . utf8_encode($RFP["funcionario"]) . "'>" . utf8_encode(strtoupper($RFP["login_user"])) . "</option>"; ?> ";
                        <?php } ?>
                        ncom_pop = ncom_pop + "						</select>";
                        ncom_pop = ncom_pop + "					</div>";
                        ncom_pop = ncom_pop + "				</div>";
                        ncom_pop = ncom_pop + "				<div class=\"button\" style=\"float:left; width:50px; margin-top:10px; background-color:#009AD7\" onClick=\"abreSelect(ncom_nome)\">";
                        ncom_pop = ncom_pop + "					<div class=\"icon\"><span class=\"ico-chevron-down\"></span></div>";
                        ncom_pop = ncom_pop + "				</div>";
                    <?php if ($conf_indicador == "1") { ?>
                            ncom_pop = ncom_pop + "				<div style=\"float:left; width:calc(100% - 60px)\">";
                            ncom_pop = ncom_pop + "                                 <div class=\"label\" style=\"margin-top:10px\">Indicador(a):</div>";
                            ncom_pop = ncom_pop + "                                 <div class=\"input\">";
                            ncom_pop = ncom_pop + "                                     <select id=\"icom_nome\" name=\"icom_nome\" class=\"value select\">";
                            ncom_pop = ncom_pop + "                                         <option value=\"null\">Selecione</option>";
    <?php
    $cur = odbc_exec($con, "select id_fornecedores_despesas,razao_social from sf_fornecedores_despesas where tipo = 'I' and inativo = 0 order by razao_social") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) { ?>
                                ncom_pop = ncom_pop + "                                 <?php echo "<option value='" . utf8_encode($RFP["id_fornecedores_despesas"]) . "'>" . utf8_encode(strtoupper($RFP["razao_social"])) . "</option>"; ?> ";
    <?php } ?>
                            ncom_pop = ncom_pop + "                                     </select>";
                            ncom_pop = ncom_pop + "                                 </div>";
                            ncom_pop = ncom_pop + "				</div>";
                            ncom_pop = ncom_pop + "				<div class=\"button\" style=\"float:left; width:50px;margin-top:10px;background-color:#009AD7\" onClick=\"abreSelect(icom_nome)\">";
                            ncom_pop = ncom_pop + "                                 <div class=\"icon\"><span class=\"ico-chevron-down\"></span></div>";
                            ncom_pop = ncom_pop + "				</div>";
<?php } ?>
                        ncom_pop = ncom_pop + "			</div>";
                        ncom_pop = ncom_pop + "		</div>";
                        ncom_pop = ncom_pop + "		<div class=\"keypad-row\" style=\"width:338px; margin:0px;<?php
if ($conf_indicador == "1") {
    echo "margin-top:78px !important;";
} else {
    echo "margin-top:18px !important;";
}
?>text-align:center\">";
                        ncom_pop = ncom_pop + "			<button type=\"button\" class=\"keypad-special keypad-clear\" style=\"margin: 7px;\" onClick=\"novaComanda()\">Salvar</button>";
                        ncom_pop = ncom_pop + "			<button type=\"button\" class=\"keypad-special keypad-clear\" style=\"margin: 7px;\" onClick=\"cancelaComanda()\">Cancelar</button>";
<?php if ($id !== "") { ?>
                            ncom_pop = ncom_pop + "			<button type=\"button\" class=\"keypad-special keypad-clear\" style=\"margin: 7px;\" onClick=\"limpaTelaComanda('N')\">Novo</button>";
<?php } ?>
                        ncom_pop = ncom_pop + "		</div>";
                        ncom_pop = ncom_pop + "	</div>";
                        ncom_pop = ncom_pop + "</div>";
                        $("body").append(ncom_pop);
                        var id1 = $('#ncom_nome').find("option:contains('<?php echo strtoupper($_SESSION["login_usuario"]); ?>')").val();
                        if ($("#txtVendedor").val() !== "") {
                            id1 = $('#ncom_nome').find('option:contains("' + $("#txtVendedor").val() + '")').val();
                        }
                        $("#ncom_nome").val(id1);

<?php if ($conf_comandaPers == 0) { ?>
                            $("#ncom_valor").keypad();
                            $("#ncom_valor").mask('0000000000');
<?php } else { ?>
                            $("#ncom_valor").mask('<?php echo $conf_comandaMask; ?>');
<?php } ?>

                        $("#ncom_valor").prop('readonly', false);
                        $("#ncom_valor").val($("#txtComanda").val());
                        $("#nmClienteComanda").html($("#nmCliente").text().substring(0, 20));
<?php if ($conf_indicador == "1") { ?>
                            if ($("#txtIndicador").val() !== "null") {
                                $('#icom_nome').val($("#txtIndicador").val());
                            }
<?php } ?>
                    } else {
                        if (<?php echo $conf_clienteObr; ?> === 1 && $("#nmClienteComanda").text().trim() === "") {
                            bootbox.alert("Cliente obrigatorio!");
                            return;
                        }
                        if ($("#ncom_valor").val() !== "") {
                            $.ajax({
                                type: "GET",
                                url: "../Estoque/Vendas_controller.php",
                                data: {existeComanda: $("#ncom_valor").val(),
                                    comandaUnica: <?php echo $conf_comandaUni; ?>,
                                    idVenda: $("#txtId").val()},
                                success: function (data) {
                                    if (data.substr(0, 3) === "YES") {
                                        bootbox.alert("Existe uma comanda com esta numeração!");
                                        $("#ncom_valor").val("");
                                        $("#ncom_win").find("#ncom_valor").focus();
                                    } else {
                                        fechaComanda();
                                        salvaComanda();
                                    }
                                }
                            });
                        } else {
                    <?php if ($conf_comandaObr == 1) { ?>
                        bootbox.alert("Comanda Obrigátoria");
                    <?php } else { ?>
                        fechaComanda();
                    <?php } ?>
                        }
                    }
                }
                function salvaComanda() {
                    $.ajax({
                        type: "POST",
                        url: "Ponto-de-Venda_controller.php",
                        data: {salvaComanda: 'S',
                            id: $("#txtId").val(),
                            vendedor: $("#txtVendedor").val(),
                            indicador: $("#txtIndicador").val(),
                            cliente: $("#user_cart").val() !== "" ? $("#user_cart").val() : 0,
                            comentario: $("#txtComentarios").val(),
                            comanda: $("#txtComanda").val().toUpperCase(),
                            empresa: $("#txtFilial").val(),
                            data_venda: $("#data_venda").val()},
                        success: function (data) {
                            if ($("#txtId").val() === "") {
                                window.top.location.href = 'Ponto-de-Venda.php?tp=' + $("#txtTpTela").val() + '&vd=' + data.toString();
                            }
                        }
                    });
                }
                
                function fechaComanda() {
                    $("#txtVendedor").val($("#ncom_nome>option:selected").text());
                    $("#txtIndicador").val($('#icom_nome').val());
                    $("#num_comanda").text($("#ncom_valor").val());
                    $("#txtComanda").val($("#ncom_valor").val());
                    $("#vnd_comis").val($("#ncom_nome").val());
                    $("#ncom_win").remove();
                }
                
                function cancelaComanda() {
                    $("#ncom_valor").val("");
                    $("#ncom_win").remove();
                }
                
                function listaComanda() {
                    //JANELA PARA LISTAR A COMANDA
                    if ($("#lcom_win").length === 0) {
                        $.get("Ponto-de-Venda_comandas.php?tp=" + $("#txtTpTela").val(), function (data) {
                            $("body").append(data);
                        });
                    } else {
                        $("#lcom_win").remove();
                    }
                }

                function runScript(e, campo) {
                    if (e.keyCode === 13) {
                        if (campo === 'cod_busca') {
                            novaBusca();
                        }
                        if (campo === 'ncom_valor' || campo === 'prc_valor') {
                            $(".keypad-close").trigger('click');
                        }
                        if (campo === 'qnt_valor') {
                            if ($("#qnt_valor").val() > 0) {
                                novaLinha();
                            }
                        }
                    }
                }

                function listaItensPacote(servico) {
                    //Ao selecionar um serviço pacote, mostrar os itens referentes ao mesmo
                    if ($("#lcom_win").length === 0) {
                        $.get("Ponto-de-Venda_itens_pacote.php", {servico: servico}, function (data) {
                            $("body").append(data);
                        });
                    } else {
                        $("#lcom_win").remove();
                    }
                }
                
                function adicionaItemPacote(pac_serv, servico, grupo_servico, desc_servico, id_item_pct) {
                    if ($("#txtId").val() !== "") {
                        adicionaItem(servico, grupo_servico, 1, numberFormat(0), 'NULL', id_item_pct, $("#user_cart").val(), 1, desc_servico);
                    } else {
                        adicionaCupomItemPacote(pac_serv, servico, grupo_servico, desc_servico, id_item_pct);
                    }
                    $("#qnt_pacote").val("");
                    $("#lcom_win").remove();
                }
                
                function adicionaCupomItemPacote(pac_serv, servico, grupo_servico, desc_servico, id_item_pct) {
                    var conta = 1;
                    var codigo = "";
                    $(".receipts > .contents > .updown > #listServ").find("tr > .item_count").each(function () {
                        conta = conta + 1;
                    });
                    if (conta > 9 && conta < 100) {
                        codigo = "0" + conta;
                    } else if (conta < 10) {
                        codigo = "00" + conta;
                    } else {
                        codigo = conta;
                    }
                    var prod_item = id_item_pct === "" ? "*" + id_pct_pai : id_item_pct;
                    $(".receipts > .contents > .updown > #listServ").append("<tr id=\"list_" + (conta + 5000) + "\" class=\"updown_normal\" onClick=\"selectLinha(" + (conta + 5000) + ",null)\"><input type=\"hidden\" name=\"prod_item[]\" value=\"" + prod_item + "\"><input type=\"hidden\" name=\"prod_pct[]\" value=\"" + pac_serv +
                    "\"><input type=\"hidden\" name=\"prod_com[]\" value=\"NULL\"><input type=\"hidden\" name=\"prod_grp[]\" value=\"" +
                    grupo_servico + "\"><input type=\"hidden\" name=\"prod_cod[]\" value=\"" +
                    servico + "\"><input type=\"hidden\" name=\"prod_qnt[]\" value=\"1\"><input type=\"hidden\" name=\"prod_prc[]\" value=\"" +
                    numberFormat(0) + "\"><td width=\"23px\" class=\"item_count\">" + codigo + "</td><td>" + desc_servico + "</td><td width=\"23px\">1</td><td width=\"52px\">" +
                    numberFormat(0) + "</td><td id=\"total_" + conta + "\" width=\"59px\" class=\"list_total\">" +
                    numberFormat(0) + "</td></tr>");
                }

                function abreBox() {
                    var data = "&dt=" + $("#txtData").val().replace(new RegExp("/", 'g'), "_");
                    window.open("consulta.php?user=" + $("#form_vend").val() + "<?php echo "&crt=" . $contrato; ?>" + data, "_blank");
                }

                function refresh_resumo_caixa() {
                    $('#tbl_resumo_caixa').html('');
                    $.getJSON('Ponto-de-Venda_server_processing.php?search=', {txtVendedor: $("#form_vend").val(), txtData: $("#txtData").val().replace(new RegExp("/", 'g'), "_"), filial: $("#txtFilial").val(), ajax: 'true'}, function (j) {
                        var total = 0.00;
                        for (var i = 0; i < j.length; i++) {
                            total = total + parseFloat(j[i].valor);
                            $('#tbl_resumo_caixa').append('<tr><td style="text-align: left;">' + j[i].descricao + '</td><td>' + numberFormat(j[i].valor) + '</td></tr>');
                        }
                        $('#tbl_resumo_caixa').append('<tr><td style="text-align: left;"><b>TOTAL:</b></td><td><b>' + numberFormat(total) + '</b></td></tr>');
                    });
                }

                function novoCalendario(loop, select) {
                    //SCRIPT PARA GERAR CALENDÁRIOS PARA TODAS AS PARCELAS
                    var parcPicker = [];
                    for (var i = 1; i <= loop; i++) {
                        $("#parcPagto_" + i + " option[value='" + select + "']").prop("selected", true);
                        parcPicker[i] = $("#parc_prazo_" + i).pickadate().pickadate("parcPicker[" + i + "]");
                        $("#parc_valor_" + i).keypad();
                        $("#parc_valor_" + i).maskMoney({thousands: lang["thousandsSeparator"], decimal: lang["centsSeparator"]});
                    }
                }
                
                function novoTroco() {
                    //SCRIPT PARA CALCULAR O "TROCO"
                    var parcValor = textToNumber($("#pagto_valor").val());
                    var parcTotal = textToNumber($("#money_total").val());
                    var parcDesct = textToNumber($("#desc_valor").val());
                    if ($("#desc_sinal").html() === "<b>%</b>") {
                        parcDesct = parcTotal * (parcDesct / 100);
                    }
                    if (parcDesct >= parcTotal || parcDesct < 0.00) {
                        parcDesct = 0.00;
                    }
                    if ((parcTotal - parcDesct) < parcValor) {
                        var parcTroco = numberFormat(parcValor - (parcTotal - parcDesct));
                    } else {
                        var parcTroco = "<font color=\"FF0000\">" + numberFormat(parcValor - (parcTotal - parcDesct)) + "</font>";
                    }
                    $("#pagto_troco").html(parcTroco);
                }
                
                function trocarMenu(acao) {
                    //SCRIPT PARA TROCAR O "MENU" E A "BUSCA"
                    if (acao === 1) {
                        $(".navigation").hide();
                        $(".searchcode").show();
                    }
                    if (acao === 0) {
                        $(".searchcode").hide();
                        $(".navigation").show();
                    }
                }
                
                function trocarTipo(acao) {
                    //SCRIPT PARA TROCAR O "MÉTODO" ENTRE "PRODUTOS" E "SERVIÇOS"
                    if (acao === 1) {
                        $("#menuProd").show();
                        $("#menuServ").hide();
                        $(".clocktime .button").attr("onclick", "trocarTipo(0)");
                        $(".clocktime .button").attr("title", "Alterar para Serviço");
                        $(".clocktime .button .icon").html("<span class=\"ico-stack-3\"></span>");
                        carrega(-1);
                    }
                    if (acao === 0) {
                        $("#menuProd").hide();
                        $("#menuServ").show();
                        $(".clocktime .button").attr("onclick", "trocarTipo(1)");
                        $(".clocktime .button").attr("title", "Alterar para Produto");
                        $(".clocktime .button .icon").html("<span class=\"ico-tools\"></span>");
                        carrega(-1);
                    }
                }
                
                function novaBusca() {
                    //SCRIPT PARA BUSCAR O PRODUTO SELECIONADO POR CÓDIGO DE BARRAS
                    carrega($("#cod_busca").val());
                }
                
                $(function () {
                    //SCRIPT PARA TRAVAR O ENTER, SENDO USADO APENAS NA BUSCA E QUANTIDADE
                    $(document).keypress(function (evento) {
                        if (evento.which === 13) {
                            evento.preventDefault();
                            return false;
                        }
                    });
                });
                
                $("#desc_valor").change(function () {
                    if ($("#desc_valor").val() === "") {
                        $("#desc_valor").val(numberFormat(0));
                    }
                });
                $("#qnt_valor, #qnt_vezes, #desc_valor, #prc_valor").keypad();
                $("#desc_valor").maskMoney({thousands: lang["thousandsSeparator"], decimal: lang["centsSeparator"]});

                <?php if ($conf_qtdDecimal == 1) { ?>
                $("#qnt_valor").change(function () {
                    if ($("#qnt_valor").val() === "") {
                        $("#qnt_valor").val(numberFormat(0));
                    }
                });
                $("#qnt_valor").maskMoney({thousands: lang["thousandsSeparator"], decimal: lang["centsSeparator"], precision: 2});
                <?php } ?>

                $("#prc_valor").change(function () {
                    if ($("#prc_valor").val() === "") {
                        $("#prc_valor").val(numberFormat(0));
                    }
                });
                $("#prc_valor").maskMoney({thousands: lang["thousandsSeparator"], decimal: lang["centsSeparator"]});

                function trocaTab(hide, show) {
                    $("#" + hide).hide();
                    $("#" + show).show();
                }
                $(window).keydown(function (event) {
                    if (event.ctrlKey && event.keyCode === 74) {
                        event.preventDefault();
                    }
                });
                
                //Inatividade de 1 min abre tela de seleção de comandas                
                var timeoutInMiliseconds = 60000;
                var timeoutId; 

                function startTimer() { 
                    timeoutId = window.setTimeout(doInactive, timeoutInMiliseconds)
                }

                function doInactive() {
                    listaComanda();
                }

                function setupTimers () {
                    document.addEventListener("mousemove", resetTimer, false);
                    document.addEventListener("mousedown", resetTimer, false);
                    document.addEventListener("keypress", resetTimer, false);
                    document.addEventListener("touchmove", resetTimer, false);     
                    startTimer();
                }

                function resetTimer() { 
                    window.clearTimeout(timeoutId)
                    startTimer();
                }
                
                setupTimers();                                
                toggleFullScreen();
            </script>
        </form>
        <form method="post" id="log_form" action="./../../login_vai.php">
            <input type="hidden" name="empresa" value="<?php echo $contrato; ?>"/>
            <input type="hidden" id="log_pass" name="password"/>
            <input type="hidden" id="log_user" name="login"/>
            <input type="hidden" name="comeback" value="1"/>
        </form>
    </body>
    <?php odbc_close($con); ?>
</html>