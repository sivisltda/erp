<?php
include "./../../Connections/configini.php";

if (isset($_GET["tp"])) {
    if ($_GET["tp"] == "1") {
        $tipo_tela = "O";
    } else {
        $tipo_tela = "V";
    }
} else {
    $tipo_tela = "V";
}

if (is_numeric($_GET['id'])) {
    echo "<script>window.open('cupom.php?id=" . $_GET['id'] . "&crt=" . $_SESSION["contrato"] . "','_blank');</script>";
    echo "<script>window.top.location.href = 'Ponto-de-Venda.php';</script>";
}

$id = "";
if (isset($_POST['bntSave'])) {
    $codCliente = 0;
    $nn = "";
    $valorParcela = 0;
    if (is_numeric($_POST['user_cart']) && $_POST['form_pagto'] == 12) {
        $codCliente = $_POST['user_cart'];
    }
    $senhaPedido = rand(1, 999);
    $cur = odbc_exec($con, "set dateformat dmy;select (select MAX(cod_pedido) from sf_vendas where data_venda between '" . date("d/m/Y") . " 00:00:00' and '" . date("d/m/Y") . " 23:59:59') maior, cod_pedido from sf_vendas where data_venda between '" . date("d/m/Y") . " 00:00:00' and '" . date("d/m/Y") . " 23:59:59' and cod_pedido = '" . $senhaPedido . "' group by cod_pedido");
    while ($RFP = odbc_fetch_array($cur)) {
        $senhaPedido = $RFP['maior'] + 1;
    }
    $query = "set dateformat dmy;insert into sf_vendas(vendedor,cliente_venda,historico_venda,data_venda,destinatario,status,tipo_documento,descontop,descontos,entrega,validade,descontotp,descontots,comentarios_venda,sys_login,cov,garantia_venda,garantia_servico,cod_pedido,grupo_conta,conta_movimento)values('" .
            utf8_decode($_SESSION["login_usuario"]) . "'," . $codCliente . ",'VENDA AVULSA','" . date("d/m/Y") . " " . date("H:i:s") . "','" .
            utf8_decode($_SESSION["login_usuario"]) . "','Aprovado'," .
            $_POST['form_pagto'] . "," . valoresNumericos($_POST['desc_valor']) . ",0.00,null,null," . $_POST['txtProdSinal'] . ",0,'" .
            utf8_decode($_POST['txtComentarios']) . "','" . $_SESSION["login_usuario"] . "','" . $tipo_tela . "',null,null," . $senhaPedido . ",14,1)";
    odbc_exec($con, $query) or die(odbc_errormsg());
    $res = odbc_exec($con, "select top 1 id_venda from sf_vendas order by id_venda desc") or die(odbc_errormsg());
    $nn = odbc_result($res, 1);
    //--------------------------------------------------------------------------------------------
    if (is_numeric($nn)) {
        $prod_cod = $_POST['prod_cod'];
        $prod_grp = $_POST['prod_grp'];
        $prod_qnt = $_POST['prod_qnt'];
        $prod_prc = $_POST['prod_prc'];
        for ($i = 0; $i < sizeof($prod_cod); $i++) {
            $valorSubTotal = (valoresNumericos($prod_prc[$i]) * valoresNumericos($prod_qnt[$i]));
            $valorParcela = $valorParcela + $valorSubTotal;
            odbc_exec($con, "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total) values(" . $nn . "," . $prod_grp[$i] . "," . $prod_cod[$i] . "," . valoresNumericos($prod_qnt[$i]) . "," . $valorSubTotal . ")") or die(odbc_errormsg());
        }
        //-------------------------------------------------------------------------------------------------------------              
        $qtde = $_POST['qnt_vezes'];
        if (is_numeric($qtde)) {
            if ($qtde > 1) {
                for ($i = 1; $i <= $qtde; $i++) {
                    $explode = explode("/", $_POST['parc_prazo_' . $i]);
                    $valorParcela = valoresNumericos($_POST['parc_valor_' . $i]);
                    $valorLiquido = $valorParcela;
                    if (count($explode) == 3) {
                        if (is_numeric($valorParcela) && is_numeric($_POST['parcPagto_' . $i]) && checkdate($explode[1], $explode[0], $explode[2])) {
                            if ($valorParcela > 0) {
                                $cur = odbc_exec($con, "select taxa from sf_tipo_documento where id_tipo_documento = " . $_POST['parcPagto_' . $i]) or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) {
                                    $valorLiquido = $valorLiquido - ($valorParcela * $RFP['taxa'] / 100);
                                }
                                if ($_POST['parcPagto_' . $i] == 12) {
                                    $valor_pago = "0.00";
                                    $data_pagamento = "null";
                                    $id_banco = "null";
                                    $syslogin = "null";
                                } else {
                                    $valor_pago = $valorParcela;
                                    $data_pagamento = "'" . date("d/m/Y") . "'";
                                    $id_banco = 1;
                                    $syslogin = "'" . $_SESSION["login_usuario"] . "'";
                                }
                                $queryP = "set dateformat dmy; insert into sf_venda_parcelas(venda,numero_parcela,data_parcela,sa_descricao,valor_parcela,valor_parcela_liquido,tipo_documento,historico_baixa,pa,bol_id_banco,bol_data_criacao,bol_valor,bol_juros,bol_multa,bol_nosso_numero,data_cadastro,valor_pago,data_pagamento,id_banco,syslogin) values (" .
                                        $nn . "," . $i . ",'" . $_POST['parc_prazo_' . $i] . "',''," . $valorParcela . "," . $valorLiquido . "," . $_POST['parcPagto_' . $i] . ",'VENDA AVULSA','" . str_pad($i, 2, "0", STR_PAD_LEFT) . "/" . str_pad($qtde, 2, "0", STR_PAD_LEFT) . "'," .
                                        "null,null,null,null,null,null,getdate()," . $valor_pago . "," . $data_pagamento . "," . $id_banco . "," . $syslogin . ")";
                                odbc_exec($con, $queryP) or die(odbc_errormsg());
                            }
                        }
                    }
                }
            } else {
                $i = 1;
                $explode = explode("/", date("d/m/Y"));
                if ($_POST['txtProdSinal'] == 0) {
                    $valorParcela = $valorParcela * ((100 - valoresNumericos($_POST['desc_valor'])) / 100);
                } else {
                    $valorParcela = $valorParcela - valoresNumericos($_POST['desc_valor']);
                }
                $valorLiquido = $valorParcela;
                if (count($explode) == 3) {
                    if (is_numeric($valorParcela) && is_numeric($_POST['form_pagto']) && checkdate($explode[1], $explode[0], $explode[2])) {
                        $cur = odbc_exec($con, "select taxa from sf_tipo_documento where id_tipo_documento = " . $_POST['form_pagto']) or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) {
                            $valorLiquido = $valorLiquido - ($valorParcela * $RFP['taxa'] / 100);
                        }
                        if ($_POST['form_pagto'] == 12) {
                            $valor_pago = "0.00";
                            $data_pagamento = "null";
                            $id_banco = "null";
                            $syslogin = "null";
                        } else {
                            $valor_pago = $valorParcela;
                            $data_pagamento = "'" . date("d/m/Y") . "'";
                            $id_banco = 1;
                            $syslogin = "'" . $_SESSION["login_usuario"] . "'";
                        }
                        $queryP = "set dateformat dmy; insert into sf_venda_parcelas(venda,numero_parcela,data_parcela,sa_descricao,valor_parcela,valor_parcela_liquido,tipo_documento,historico_baixa,pa,bol_id_banco,bol_data_criacao,bol_valor,bol_juros,bol_multa,bol_nosso_numero,data_cadastro,valor_pago,data_pagamento,id_banco,syslogin) values (" .
                                $nn . "," . $i . ",'" . date("d/m/Y") . "',''," . $valorParcela . "," . $valorLiquido . "," . $_POST['form_pagto'] . ",'VENDA AVULSA','" . str_pad($i, 2, "0", STR_PAD_LEFT) . "/" . str_pad(1, 2, "0", STR_PAD_LEFT) . "'," .
                                "null,null," . valoresNumericos($_POST['pagto_valor']) . ",null,null,null,getdate()," . $valor_pago . "," . $data_pagamento . "," . $id_banco . "," . $syslogin . ")";
                        odbc_exec($con, $queryP) or die(odbc_errormsg());
                    }
                }
            }
        }
    }
    $_POST["txtId"] = $nn;
    $FinalUrl = "?tp=" . $tipo_tela . "&id=" . $nn;
    echo "<script>window.top.location.href = 'Ponto-de-Venda.php" . $FinalUrl . "';</script>";
}
?>        
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Ponto de Venda</title>
        <link href="./../../css/icons.css" rel="stylesheet" type="text/css"/>
        <link href="./../../css/Ponto-de-Venda.css" rel="stylesheet" type="text/css"/>        
        <link href="pickadate/themes/classic.css" rel="stylesheet" type="text/css"/>
        <link href="pickadate/themes/classic.date.css" rel="stylesheet" type="text/css"/>
        <link href="keypad/jquery.keypad.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="./../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body onload="carrega(-1)" style="overflow:hidden;<?php
    if ($tipo_tela == "O") {
        echo " background:url('./../../css/background_2.jpg') no-repeat center center fixed";
    }
    ?>">
        <input id="txtMnContrato" type="hidden" value="<?php echo $contrato; ?>" />        
        <input id="txtMnFilial" type="hidden" value="<?php echo $filial; ?>" />
        <input id="txtMnLanguage" type="hidden" value="<?php echo $languages; ?>" />        
        <form action="Ponto-de-Venda.php<?php echo "?tp=" . $_GET["tp"]; ?>" method="POST" name="frmEnviaDados" id="frmEnviaDados">                         
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <table id="division" width="100%" cellspacing="0" cellpadding="0" style="table-layout:fixed">
                <tr><td width="60%"></td><td></td><td width="27%"></td></tr>
                <tr>
                    <td class="searchcode" colspan="2">
                        <div class="slides" style="overflow:hidden"><input id="cod_busca" type="text" class="value"/></div>
                        <div id="code" class="button" style="background-color:#009AD7">
                            <div class="icon"><span class="ico-search"></span></div>
                        </div>
                    </td>
                    <td class="clocktime">
                        <a href="#">
                            <div class="login" onClick="novoUserLogin()">
                                <div class="photo"><img src="<?php
                                    if ($_SESSION["imagem"] != '') {
                                        echo "./../../Pessoas/" . $contrato . "/" . $_SESSION["imagem"];
                                    } else {
                                        echo "./../../img/dmitry_m.gif";
                                    }
                                    ?>" width="38px" align="left" style="border-left:solid 3px greenyellow; float:left;"/></div>
                                <div class="person"><span style="color:#CCC;"><b>Bem vindo</b></span><br/><?php echo $_SESSION["login_usuario"]; ?></div>
                            </div>
                        </a>
                        <div class="display">
                            <div id="time" class="time"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="products" colspan="2">
                        <div class="academia">
                            <div class="myblock">
                                <div class="statics" style="height:10px"></div>
                                <div class="clients"></div>
                                <div class="selects">
                                    <div class="plan_select">
                                        <div class="myselect">
                                            <div class="label">Seleção de Planos</div>
                                            <div class="input">
                                                <select id="sel_plano" name="sel_plano" class="value select">
                                                    <?php
                                                    $cur = odbc_exec($con, "select id_tipo_documento,descricao from sf_tipo_documento where s_tipo in ('A','C') order by descricao") or die(odbc_errormsg());
                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                        ?>
                                                        <option value="<?php echo $RFP["id_tipo_documento"] ?>"<?php
                                                        if ($RFP["id_tipo_documento"] == 6) {
                                                            echo "SELECTED";
                                                        }
                                                        ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                                            <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="button" style="width:50px; background-color:#009AD7" onClick="abreSelect(sel_plano)">
                                            <div class="icon"><span class="ico-chevron-down"></span></div>
                                        </div>
                                        <div class="button" style="width:50px; margin-left:5px; background-color:#68AF27">
                                            <div class="icon"><span class="ico-plus"></span></div>
                                        </div>
                                    </div>
                                    <div class="plan_list" style="overflow-y:hidden">
                                        <div class="updown">
                                            <div id="plan_1" class="linha" onclick="checkPlano(1)">
                                                <div class="cod">1</div>
                                                <div class="desc">Natação</div>
                                                <div class="data">99/99/9999</div>
                                                <div class="data">99/99/9999</div>
                                            </div>
                                            <div id="plan_2" class="linha" onclick="checkPlano(2)">
                                                <div class="cod">2</div>
                                                <div class="desc">Natação</div>
                                                <div class="data">99/99/9999</div>
                                                <div class="data">99/99/9999</div>
                                            </div>
                                            <div id="plan_3" class="linha" onclick="checkPlano(3)">
                                                <div class="cod">3</div>
                                                <div class="desc">Natação</div>
                                                <div class="data">99/99/9999</div>
                                                <div class="data">99/99/9999</div>
                                            </div>
                                            <div id="plan_4" class="linha" onclick="checkPlano(4)">
                                                <div class="cod">4</div>
                                                <div class="desc">Natação</div>
                                                <div class="data">99/99/9999</div>
                                                <div class="data">99/99/9999</div>
                                            </div>
                                            <div id="plan_5" class="linha" onclick="checkPlano(5)">
                                                <div class="cod">5</div>
                                                <div class="desc">Natação</div>
                                                <div class="data">99/99/9999</div>
                                                <div class="data">99/99/9999</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="plan_stat">
                                        <div id="plan_up" class="button">
                                            <div class="icon"><span class="ico-arrow-up"></span></div>
                                        </div>
                                        <div id="plan_down" class="button" style="margin-top:5px">
                                            <div class="icon"><span class="ico-arrow-down"></span></div>
                                        </div>
                                    </div>
                                    <div style="clear:both"></div>
                                    <div class="statics" style="height:5px"></div>
                                </div>
                            </div>
                            <div class="parcels" style="margin-top:0">
                                <div class="statics" style="height:10px"></div>
                                <div class="mylist">
                                    <div class="updown">
                                        <div id="parc_btn_1" class="button" onclick="checkParcela(1)">
                                            <div class="icon"><span class="ico-check-empty"></span></div>
                                            <input type="radio" name="parc_rad" class="parc_rad" value="1" style="display:none"/>
                                        </div><div id="parc_txt_1" class="texto"><b>01 x 1200,00</b><br/><span>Desconto: 50%</span></div>
                                        <div id="parc_btn_2" class="button" onclick="checkParcela(2)">
                                            <div class="icon"><span class="ico-check-empty"></span></div>
                                            <input type="radio" name="parc_rad" class="parc_rad" value="2" style="display:none"/>
                                        </div><div id="parc_txt_2" class="texto"><b>02 x 600,00</b><br/><span>Desconto: 50%</span></div>
                                        <div id="parc_btn_3" class="button" onclick="checkParcela(3)">
                                            <div class="icon"><span class="ico-check-empty"></span></div>
                                            <input type="radio" name="parc_rad" class="parc_rad" value="3" style="display:none"/>
                                        </div><div id="parc_txt_3" class="texto"><b>03 x 400,00</b><br/><span>Desconto: 50%</span></div>
                                        <div id="parc_btn_4" class="button" onclick="checkParcela(4)">
                                            <div class="icon"><span class="ico-check-empty"></span></div>
                                            <input type="radio" name="parc_rad" class="parc_rad" value="4" style="display:none"/>
                                        </div><div id="parc_txt_4" class="texto"><b>04 x 300,00</b><br/><span>Desconto: 50%</span></div>
                                        <div id="parc_btn_5" class="button" onclick="checkParcela(5)">
                                            <div class="icon"><span class="ico-check-empty"></span></div>
                                            <input type="radio" name="parc_rad" class="parc_rad" value="5" style="display:none"/>
                                        </div><div id="parc_txt_5" class="texto"><b>05 x 240,00</b><br/><span>Desconto: 50%</span></div>
                                        <div id="parc_btn_6" class="button" onclick="checkParcela(6)">
                                            <div class="icon"><span class="ico-check-empty"></span></div>
                                            <input type="radio" name="parc_rad" class="parc_rad" value="6" style="display:none"/>
                                        </div><div id="parc_txt_6" class="texto"><b>06 x 200,00</b><br/><span>Desconto: 50%</span></div>
                                        <div id="parc_btn_7" class="button" onclick="checkParcela(7)">
                                            <div class="icon"><span class="ico-check-empty"></span></div>
                                            <input type="radio" name="parc_rad" class="parc_rad" value="7" style="display:none"/>
                                        </div><div id="parc_txt_7" class="texto"><b>07 x 171,42</b><br/><span>Desconto: 50%</span></div>
                                        <div id="parc_btn_8" class="button" onclick="checkParcela(8)">
                                            <div class="icon"><span class="ico-check-empty"></span></div>
                                            <input type="radio" name="parc_rad" class="parc_rad" value="8" style="display:none"/>
                                        </div><div id="parc_txt_8" class="texto"><b>08 x 150,00</b><br/><span>Desconto: 50%</span></div>
                                        <div id="parc_btn_9" class="button" onclick="checkParcela(9)">
                                            <div class="icon"><span class="ico-check-empty"></span></div>
                                            <input type="radio" name="parc_rad" class="parc_rad" value="9" style="display:none"/>
                                        </div><div id="parc_txt_9" class="texto"><b>09 x 133,33</b><br/><span>Desconto: 50%</span></div>
                                        <div id="parc_btn_10" class="button" onclick="checkParcela(10)">
                                            <div class="icon"><span class="ico-check-empty"></span></div>
                                            <input type="radio" name="parc_rad" class="parc_rad" value="10" style="display:none"/>
                                        </div><div id="parc_txt_10" class="texto"><b>10 x 120,00</b><br/><span>Desconto: 50%</span></div>
                                        <div id="parc_btn_11" class="button" onclick="checkParcela(11)">
                                            <div class="icon"><span class="ico-check-empty"></span></div>
                                            <input type="radio" name="parc_rad" class="parc_rad" value="11" style="display:none"/>
                                        </div><div id="parc_txt_11" class="texto"><b>11 x 109,09</b><br/><span>Desconto: 50%</span></div>
                                        <div id="parc_btn_12" class="button" onclick="checkParcela(12)">
                                            <div class="icon"><span class="ico-check-empty"></span></div>
                                            <input type="radio" name="parc_rad" class="parc_rad" value="12" style="display:none"/>
                                        </div><div id="parc_txt_12" class="texto"><b>12 x 100,00</b><br/><span>Desconto: 50%</span></div>
                                    </div>
                                </div>
                                <div class="parc_stat">
                                    <div id="parc_up" class="button">
                                        <div class="icon"><span class="ico-arrow-up"></span></div>
                                    </div>
                                    <div id="parc_down" class="button" style="margin-top:135px">
                                        <div class="icon"><span class="ico-arrow-down"></span></div>
                                    </div>
                                </div>
                                <div style="clear:both"></div>
                                <div class="plan_select">
                                    <div class="myselect" style="width:calc(100% - 225px)">
                                        <div class="label">Início do Plano</div>
                                        <div class="input"><input id="dat_plano" type="text" class="value" value="<?php echo date("d/m/Y"); ?>"/></div>
                                    </div>
                                    <div id="dat_click" class="button" style="width:50px; background-color:#009AD7">
                                        <div class="icon"><span class="ico-calendar"></span></div>
                                    </div>
                                    <div class="button" style="width:150px; margin-left:5px; background-color:#68AF27">
                                        <div class="text">COMPRAR</div>
                                    </div>
                                </div>
                            </div>
                            <div style="clear:both; margin-bottom:5px"></div>
                            <div class="statics" style="height:5px"></div>
                            <div class="contents" style="overflow-y:hidden">
                                <div class="updown">
                                    <div id="contract_1" class="linha">
                                        <div class="mes">Setembro</div>
                                        <div class="parc">1</div>
                                        <div class="prec">1200,00</div>
                                        <div class="prec">1200,00</div>
                                        <div class="venc">99/99/9999</div>
                                        <div class="pago">Não</div>
                                        <div class="icon">RENOVAR</div>
                                    </div>
                                    <div id="contract_2" class="linha">
                                        <div class="mes">Outubro</div>
                                        <div class="parc">2</div>
                                        <div class="prec">600,00</div>
                                        <div class="prec">1200,00</div>
                                        <div class="venc">99/99/9999</div>
                                        <div class="pago">Não</div>
                                        <div class="icon">RENOVAR</div>
                                    </div>
                                    <div id="contract_3" class="linha">
                                        <div class="mes">Novembro</div>
                                        <div class="parc">3</div>
                                        <div class="prec">400,00</div>
                                        <div class="prec">1200,00</div>
                                        <div class="venc">99/99/9999</div>
                                        <div class="pago">Não</div>
                                        <div class="icon">RENOVAR</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="payments" style="overflow-y:hidden; display:none">
                            <div class="updown">
                                <div class="mylist"></div>
                                <div class="option">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="100%">
                                                <div class="label">Forma de Pagamento</div>
                                                <div class="input">
                                                    <select id="form_pagto" name="form_pagto" class="value select">
                                                        <?php
                                                        $cur = odbc_exec($con, "select id_tipo_documento,descricao from sf_tipo_documento where s_tipo in ('A','C') order by descricao") or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) {
                                                            ?>
                                                            <option value="<?php echo $RFP["id_tipo_documento"] ?>"<?php
                                                            if ($RFP["id_tipo_documento"] == 6) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                                                <?php } ?>
                                                    </select>
                                                </div>
                                            </td>
                                            <td width="60px">
                                                <div class="button" style="width:50px; background-color:#009AD7" onClick="abreSelect(form_pagto)">
                                                    <div class="icon"><span class="ico-chevron-down"></span></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>                                
                                <div class="option">
                                    <table id="resize" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="60px">
                                                <div class="button" style="width:50px; background-color:#009AD7" onClick="updateSoma('vezes', 0)">
                                                    <div class="icon"><span class="ico-minus-2"></span></div>
                                                </div>
                                            </td>
                                            <td width="100%">
                                                <div class="label">Parcelas</div>
                                                <div class="input"><input id="qnt_vezes" name="qnt_vezes" type="text" class="value" value="1" style="width:60px"/></div>
                                            </td>
                                            <td width="60px">
                                                <div class="button" style="width:50px; background-color:#009AD7" onClick="updateSoma('vezes', 1)">
                                                    <div class="icon"><span class="ico-plus"></span></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="option">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="100%">
                                                <div class="label">1º Vencimento</div>
                                                <div class="input"><input id="prim_prazo" type="text" class="value" value="<?php echo date("d/m/Y"); ?>"/></div>
                                            </td>
                                            <td width="60px">
                                                <div id="prim_click" class="button" style="width:50px; background-color:#009AD7">
                                                    <div class="icon" style="margin-top:7px"><span class="ico-calendar"></span></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="option">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="100%">
                                                <div class="label">Desconto</div>
                                                <div class="input">
                                                    <input id="desc_valor" name="desc_valor" type="text" class="value" value="0,00"/><!-- onBlur="novoDesconto()" -->
                                                </div>
                                            </td>
                                            <td width="60px">
                                                <div id="desc_click" class="button" style="width:50px; background-color:#009AD7">
                                                    <div id="desc_sinal" class="icon" style="margin-top:1px"><b>%</b></div>
                                                    <input name="txtProdSinal" id="txtProdSinal" value="0" type="hidden"/>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="open_cart" class="option" style="display:none">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="100%">
                                                <div class="label">Cliente</div>
                                                <div class="input">
                                                    <select id="user_cart" name="user_cart" class="value select"></select>
                                                </div>
                                            </td>
                                            <td width="60px">
                                                <div class="button" style="width:50px; background-color:#009AD7" onClick="abreSelect(user_cart)">
                                                    <div class="icon"><span class="ico-chevron-down"></span></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="option">
                                    <div class="button" style="background-color:#68AF27" onClick="novoDesconto();
                                            novaCarteira()">
                                        <div class="icon"><span class="ico-arrow-right"></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="statics" style="height:50px; padding:5px">
                            <div id="pro_down" class="button" style="background-color:#009AD7">
                                <div class="icon"><span class="ico-arrow-down"></span></div>
                            </div>
                            <div id="pro_up" class="button" style="background-color:#009AD7; margin-right:5px">
                                <div class="icon"><span class="ico-arrow-up"></span></div>
                            </div>
                        </div>
                    </td>
                    <td class="receipts">
                        <div class="statics" style="height:10px"></div>
                        <div class="contents" style="overflow-y:hidden">
                            <div class="updown">
                                <table width="100%" style="margin:0; padding:0"><tr><td style="padding:0; background:#DDD"><center><b>Produtos</b></center></td></tr></table>
                                <table id="listProd" width="100%" style="margin:0; padding-top:0"></table>
                                <table width="100%" style="margin:0; padding:0"><tr><td style="padding:0; background:#DDD"><center><b>Serviços</b></center></td></tr></table>
                                <table id="listServ" width="100%" style="margin:0; padding-top:0"></table>
                            </div>
                        </div>
                        <div class="statics" style="height:50px; padding:5px">
                            <div id="rec_down" class="button" style="background-color:#009AD7">
                                <div class="icon"><span class="ico-arrow-down"></span></div>
                            </div>
                            <div id="rec_up" class="button" style="background-color:#009AD7; margin-right:5px">
                                <div class="icon"><span class="ico-arrow-up"></span></div>
                            </div>
                            <div id="rec_del" class="button" style="background-color:#D53F26; margin-right:5px; float:left">
                                <div class="icon"><span class="ico-trashcan"></span></div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="checkout" colspan="2">
                        <span id="buy_check">
                            <div class="button" style="background-color:#68AF27; margin-right:1%" onClick="trocaTela(1)">
                                <div class="icon"><span class="ico-ok"></span></div>
                                <div class="text">Pagamento</div>
                            </div>
                            <div class="button" style="background-color:#FFAA31; margin-right:1%" onClick="carrega(-1)">
                                <div class="icon"><span class="ico-star"></span></div>
                                <div class="text">Favoritos</div>
                            </div>
                            <div class="button" style="background-color:#D53F26" onClick="if (confirm('Continuar com o cancelamento da venda?')) {
                                        carrega(0);
                                    }">
                                <div class="icon"><span class="ico-cancel"></span></div>
                                <div class="text">Cancelamento</div>
                            </div>
                        </span>
                        <span id="pay_check" style="display:none">
                            <div class="button" style="background-color:#68AF27; margin-right:1%" onClick="$('#bntSave').click()">
                                <div class="icon"><span class="ico-shopping-cart"></span></div>
                                <div class="text">Concluir</div>
                            </div>
                            <input id="bntSave" name="bntSave" type="submit" style="display:none" onClick="if (confirmaVenda() == false) {
                                        return false;
                                    }"/>
                            <div class="button" style="background-color:#D53F26" onClick="trocaTela(0)">
                                <div class="icon"><span class="ico-undo"></span></div>
                                <div class="text">Retornar</div>
                            </div>
                        </span>
                        <div style="float:right; height:70px; margin:0 10px"><img src="./../../img/logo.png" width="176" height="102"></div>
                        <?php
                        if ($_GET["tp"] != "1") {
                            if ($ckb_out_terc_ > 0) {
                                ?>
                                <span style="float:right; margin-right:1%">
                                    <div class="button" style="background-color:#009AD7" onClick="contaCaixa()">
                                        <div class="icon"><span class="ico-coins"></span></div>
                                        <div class="text">Caixa</div>
                                    </div>
                                </span>
                            <?php } ?>
                            <span style="float:right; margin-right:1%">
                                <div class="button" style="background-color:#009AD7" onClick="listaVendas()">
                                    <div class="icon"><span class="ico-barcode"></span></div>
                                    <div class="text">Últimas Notas</div>
                                </div>
                            </span>
                        <?php } ?>
                    </td>
                    <td class="logmein">
                        <div class="button" style="color:#FF0000; font-size:35px; background-color:#FFF">
                            <input id="money_total" type="hidden" value="0,00"/>
                            <div id="money" class="money" style="font-size:50px"><span style="font-size:35px">R$</span> 0,00</div>
                        </div>
                    </td>
                </tr>
            </table>   
            <script type="text/javascript" src="maskmoney/jquery.maskMoney.min.js"></script>
            <script type="text/javascript" src="pickadate/picker.js"></script>
            <script type="text/javascript" src="pickadate/picker.date.js"></script>
            <script type="text/javascript" src="keypad/jquery.plugin.js"></script>
            <script type="text/javascript" src="keypad/jquery.keypad.js"></script> 
            <script type="text/javascript" src="./../../js/util.js"></script>                                        
            <script type="text/javascript">
                $("#form_pagto").change(function () {
                    if ($("#form_pagto option:selected").val() == 12) {
                        $.getJSON("./../Contas-a-pagar/fornecedor.ajax.php?search=", {txtTipo: "C", txtBloq: "S", ajax: "true"}, function (j) {
                            var options = "<option value=\"\">--Selecione--</option>";
                            for (var i = 0; i < j.length; i++) {
                                options += "<option value=\"" + j[i].id_grupo_contas + "\">" + j[i].descricao + "</option>";
                            }
                            $("#open_cart").show();
                            $("#user_cart").html(options);
                        });
                    } else {
                        $("#open_cart").hide();
                        $("#user_cart").html("");
                    }
                    if ($("#form_pagto option:selected").val() == 7) {
                        $("#prim_prazo").val("<?php echo date("d/m/Y", strtotime("+30 days")); ?>");
                    } else if ($("#form_pagto option:selected").val() == 9) {
                        $("#prim_prazo").val("<?php echo date("d/m/Y", strtotime("+2 days")); ?>");
                    } else {
                        $("#prim_prazo").val("<?php echo date("d/m/Y"); ?>");
                    }
                    if ($("#form_pagto option:selected").val() == 6) {
                        $("#qnt_vezes").val("1");
                    }
                });
                                
                function carrega(id) {
                    if ($(".products .academia .clients").is(":visible")) {
                        $(".products .academia .clients").html("");
                        if ($(".clocktime .button .icon").html() == "<span class=\"ico-stack-3\"></span>") {
                            var met = "P";
                        } else {
                            var met = "S";
                        }
                        if (id == 0) {
                            $(".receipts .contents .updown table").html("");
                            novoTotal();
                        }
                        $.getJSON("./../Estoque/conta_p.ajax.php?search=", {txtGrupo: id, txtMetodo: met, ajax: "true"}, function (j) {
                            var produto = "";
                            var contrato = "<?php echo $contrato; ?>";
                            for (var i = 0; i < j.length; i++) {
                                produto += "<div id=\"but_" + j[i].id_contas_movimento + "\" class=\"button\" title=\"Estoque Atual: " + j[i].qtdDisponivel + "\" style=\"background-image:url('" + j[i].img + "')\" onClick=\"novaQuant(" + j[i].id_contas_movimento + "," + j[i].grupo + ")\">";
                                produto += "<div id=\"prc_" + j[i].id_contas_movimento + "\" class=\"title\">" + numberFormat(j[i].preco_venda) + "</div></div>";
                            }
                            $(".products .academia .clients").html(produto);
                            limpaQuant();
                        });
                    }
                }
                function checkPlano(id) {
                    $(".products .plan_list .ativo").each(function () {
                        $(this).removeClass("ativo");
                    });
                    $("#plan_" + id).addClass("ativo");
                }
                function checkParcela(id) {
                    $(".products .parcels .ativo").each(function () {
                        $(this).removeClass("ativo");
                    });
                    $("#parc_btn_" + id).addClass("ativo");
                    $(".products .parcels .light").each(function () {
                        $(this).removeClass("light");
                    });
                    $("#parc_txt_" + id).addClass("light");
                    $(".parcels .button .ico-check").each(function () {
                        $(this).removeClass("ico-check").addClass("ico-check-empty");
                    });
                    $("#parc_btn_" + id + " .ico-check-empty").removeClass("ico-check-empty").addClass("ico-check");
                    $(".parcels .button .parc_rad").each(function () {
                        $(this).attr("checked", false);
                    });
                    $("input[name=parc_rad][value=" + id + "]").attr("checked", true);
                }
                function confirmaVenda() {
                    if (confirm('Confirmar venda?')) {
                        if ($('#tdp_1').length || $('#tdp_2').length) {
                            if ($('#tdp_1').length) {
                                if ($("#pagto_valor").val().replace(".", "").replace(",", ".") > 0.00) {
                                    return true;
                                } else {
                                    alert('O valor pago não é um valor válido!');
                                    return false;
                                }
                            } else {
                                return true;
                            }
                        } else {
                            alert('Selecione uma forma de pagamento!');
                            return false;
                        }
                    } else {
                        return false;
                    }
                }
                function startTime() {
                    //SCRIPT PARA MANTER O HORÁRIO ATUALIZADO NO CANTO SUPERIOR
                    var myDay = new Date();
                    if (myDay.getHours() < 10) {
                        var h = "0" + myDay.getHours();
                    } else {
                        var h = myDay.getHours();
                    }
                    if (myDay.getMinutes() < 10) {
                        var m = "0" + myDay.getMinutes();
                    } else {
                        var m = myDay.getMinutes();
                    }
                    if (myDay.getSeconds() < 10) {
                        var s = "0" + myDay.getSeconds();
                    } else {
                        var s = myDay.getSeconds();
                    }
                    document.getElementById("time").innerHTML = h + ":" + m + ":" + s;
                    t = setTimeout(function () {
                        startTime()
                    }, 1000);
                }                
                function resizeAll() {
                    //SCRIPT PARA REDIMENSIONAR TODAS AS JANELAS PROPORCIONALMENTE
                    $("#division").css("height", $(window).height());
                    $(".slides").css({"width": $(".products").width() - 120, "max-width": $(".products").width() - 120});
                    $(".quantity > .contents").css({"height": $(window).height() - 183, "max-height": $(window).height() - 183});
                    if ($(".products > .academia > .contents").is(":visible")) {
                        $(".receipts > .contents").css({"height": $(window).height() - 238, "max-height": $(window).height() - 238});
                        $(".products > .academia > .contents").css({"height": $(window).height() - 543, "max-height": $(window).height() - 543});
                    } else {
                        $(".receipts > .contents, .products > .payments").css({"height": $(window).height() - 238, "max-height": $(window).height() - 238});
                        $("#form_pagto").css({"width": $("#resize").width() - 64, "max-width": $("#resize").width() - 64});
                    }
                    if ($("#user_pop").length == 1) {
                        var sizew = $(window).width() - (140 + ($(".clocktime").innerWidth() - $(".clocktime").width()));
                        var sizeh = 50 + (($(".clocktime").innerHeight() - $(".clocktime").height()) / 2);
                        $(".keypad-login").css({"top": sizeh + "px", "left": sizew + "px"});
                    }
                }
                $(window).on("resize", function () {
                    resizeAll();
                });
                $(window).bind("resize", function () {
                    resizeAll();
                });
                $(document).ready(function () {
                    startTime();
                    resizeAll();
                });
                $(document).ready(function () {
                    var interval;
                    //SCRIPT PARA O FUNCIONAMENTO DOS SCROLLS DO MENU E JANELAS
                    $("#plan_up").on("mousedown", function () {
                        interval = setInterval(function () {
                            var pos = $(".selects > .plan_list").scrollTop();
                            $(".selects > .plan_list").scrollTop(pos - 3);
                        }, 1);
                    }).click().on("mouseup", function () {
                        clearInterval(interval);
                    });
                    $("#plan_down").on("mousedown", function () {
                        interval = setInterval(function () {
                            var pos = $(".selects > .plan_list").scrollTop();
                            $(".selects > .plan_list").scrollTop(pos + 3);
                        }, 1);
                    }).click().on("mouseup", function () {
                        clearInterval(interval);
                    });
                    $("#parc_up").on("mousedown", function () {
                        interval = setInterval(function () {
                            var pos = $(".parcels > .mylist").scrollTop();
                            $(".parcels > .mylist").scrollTop(pos - 3);
                        }, 1);
                    }).click().on("mouseup", function () {
                        clearInterval(interval);
                    });
                    $("#parc_down").on("mousedown", function () {
                        interval = setInterval(function () {
                            var pos = $(".parcels > .mylist").scrollTop();
                            $(".parcels > .mylist").scrollTop(pos + 3);
                        }, 1);
                    }).click().on("mouseup", function () {
                        clearInterval(interval);
                    });
                    $("#pro_up").on("mousedown", function () {
                        interval = setInterval(function () {
                            if ($(".products > .academia > .contents").is(":visible")) {
                                var pos = $(".products > .academia > .contents").scrollTop();
                                $(".products > .academia > .contents").scrollTop(pos - 3);
                            } else {
                                var pos = $(".products > .payments").scrollTop();
                                $(".products > .payments").scrollTop(pos - 3);
                            }
                        }, 1);
                    }).click().on("mouseup", function () {
                        clearInterval(interval);
                    });
                    $("#pro_down").on("mousedown", function () {
                        interval = setInterval(function () {
                            if ($(".products > .academia > .contents").is(":visible")) {
                                var pos = $(".products > .academia > .contents").scrollTop();
                                $(".products > .academia > .contents").scrollTop(pos + 3);
                            } else {
                                var pos = $(".products > .payments").scrollTop();
                                $(".products > .payments").scrollTop(pos + 3);
                            }
                        }, 1);
                    }).click().on("mouseup", function () {
                        clearInterval(interval);
                    });
                    $("#rec_up").on("mousedown", function () {
                        interval = setInterval(function () {
                            var pos = $(".receipts > .contents").scrollTop();
                            $(".receipts > .contents").scrollTop(pos - 3);
                        }, 1);
                    }).click().on("mouseup", function () {
                        clearInterval(interval);
                    });
                    $("#rec_down").on("mousedown", function () {
                        interval = setInterval(function () {
                            var pos = $(".receipts > .contents").scrollTop();
                            $(".receipts > .contents").scrollTop(pos + 3);
                        }, 1);
                    }).click().on("mouseup", function () {
                        clearInterval(interval);
                    });
                });
                function updateSoma(id, opt) {
                    //SCRIPT PARA O FUNCIONAMENTO DOS BOTÕES MAIS/MENOS EM "QUANTIDADE" E "PAGAMENTOS"
                    if ($("#form_pagto option:selected").val() != 6 || id == "valor") {
                        var quant = parseInt($("#qnt_" + id).val());
                        if (opt == 1 && quant > 0) {
                            if (id == "valor" || (id == "vezes" && quant < 60)) {
                                $("#qnt_" + id).val(quant + 1);
                            }
                        }
                        if (opt == 0 && quant > 1) {
                            if (id == "valor" || (id == "vezes" && quant < 61)) {
                                $("#qnt_" + id).val(quant - 1);
                            }
                        }
                        if (id == "valor") {
                            updateQnt();
                        }
                    }
                }
                function updateQnt() {
                    //SCRIPT PARA ATUALIZAR O VALOR TOTAL EM "QUANTIDADE"
                    var money = $("#prc_valor").html().replace(".", "").replace(",", ".") * parseInt($("#qnt_valor").val())
                    $("#prc_total").html(numberFormat(money));
                }
                function novaQuant(id, grp) {
                    //SCRIPT PARA ADICIONAR UM PRODUTO EM "QUANTIDADE"
                    if ($(".products > .academia > .contents").is(":visible")) {
                        $(".active").removeClass("button active").addClass("button");
                        $("#but_" + id).removeClass("button").addClass("button active");
                        $("#prc_valor, #prc_total").html($("#prc_" + id).html());
                        $("#qnt_titulo").val($("#tit_" + id).html());
                        $("#qnt_codigo").val(id);
                        $("#qnt_grupo").val(grp);
                        $("#qnt_valor").val("1");
                        resizeAll();
                    }
                }
                function novaLinha() {
                    //SCRIPT PARA ADICIONAR UM PRODUTO EM "CUPOM FISCAL"
                    if ($(".products > .academia > .contents").is(":visible")) {
                        if ($(".clocktime .button .icon").html() == "<span class=\"ico-stack-3\"></span>") {
                            var conta = 1;
                            $(".receipts > .contents > .updown > #listProd").find("tr > .item_count").each(function () {
                                conta = conta + 1
                            });
                            if (($("#prc_valor").html() != "0,00") && ($("#qnt_valor").val() != "0") && ($("#prc_total").html() != "0,00")) {
                                if (conta > 9 && conta < 100) {
                                    var codigo = "0" + conta;
                                } else if (conta < 10) {
                                    var codigo = "00" + conta;
                                } else {
                                    var codigo = conta;
                                }
                                $(".receipts > .contents > .updown > #listProd").append("<tr id=\"list_" + (conta + 1000) + "\" class=\"updown_normal\" onClick=\"selectLinha(" + (conta + 1000) + ")\"><input type=\"hidden\" name=\"prod_grp[]\" value=\"" + $("#qnt_grupo").val() + "\"><input type=\"hidden\" name=\"prod_cod[]\" value=\"" + $("#qnt_codigo").val() + "\"><input type=\"hidden\" name=\"prod_qnt[]\" value=\"" + $("#qnt_valor").val() + "\"><input type=\"hidden\" name=\"prod_prc[]\" value=\"" + $("#prc_valor").html() + "\"><td width=\"23px\" class=\"item_count\">" + codigo + "</td><td>" + $("#qnt_titulo").val() + "</td><td width=\"23px\">" + $("#qnt_valor").val() + "</td><td width=\"52px\">" + $("#prc_valor").html() + "</td><td id=\"total_" + conta + "\" width=\"59px\" class=\"list_total\">" + $("#prc_total").html() + "</td></tr>");
                                limpaQuant();
                            }
                        } else {
                            var conta = 1;
                            $(".receipts > .contents > .updown > #listServ").find("tr > .item_count").each(function () {
                                conta = conta + 1
                            });
                            if (($("#prc_valor").html() != "0,00") && ($("#qnt_valor").val() != "0") && ($("#prc_total").html() != "0,00")) {
                                if (conta > 9 && conta < 100) {
                                    var codigo = "0" + conta;
                                } else if (conta < 10) {
                                    var codigo = "00" + conta;
                                } else {
                                    var codigo = conta;
                                }
                                $(".receipts > .contents > .updown > #listServ").append("<tr id=\"list_" + (conta + 5000) + "\" class=\"updown_normal\" onClick=\"selectLinha(" + (conta + 5000) + ")\"><input type=\"hidden\" name=\"prod_grp[]\" value=\"" + $("#qnt_grupo").val() + "\"><input type=\"hidden\" name=\"prod_cod[]\" value=\"" + $("#qnt_codigo").val() + "\"><input type=\"hidden\" name=\"prod_qnt[]\" value=\"" + $("#qnt_valor").val() + "\"><input type=\"hidden\" name=\"prod_prc[]\" value=\"" + $("#prc_valor").html() + "\"><td width=\"23px\" class=\"item_count\">" + codigo + "</td><td>" + $("#qnt_titulo").val() + "</td><td width=\"23px\">" + $("#qnt_valor").val() + "</td><td width=\"52px\">" + $("#prc_valor").html() + "</td><td id=\"total_" + conta + "\" width=\"59px\" class=\"list_total\">" + $("#prc_total").html() + "</td></tr>");
                                limpaQuant();
                            }
                        }
                        novoTotal();
                        resizeAll();
                    }
                }
                function novoTotal() {
                    //SCRIPT PARA ATUALIZAR O VALOR TOTAL EM "CUPOM FISCAL"
                    var money = 0.00;
                    $(".receipts > .contents > .updown > #listProd").find("tr > .list_total").each(function () {
                        money = money + parseFloat($(this).html().replace(".", "").replace(",", "."));
                    });
                    $(".receipts > .contents > .updown > #listServ").find("tr > .list_total").each(function () {
                        money = money + parseFloat($(this).html().replace(".", "").replace(",", "."));
                    });
                    $("#money_total").val(numberFormat(money));
                    $("#money").html("<span style=\"font-size:35px\">R$</span> " + numberFormat(money));
                }
                function selectLinha(id) {
                    //SCRIPT PARA SELECIONAR UM PRODUTO EM "CUPOM FISCAL"
                    if ($(".products > .academia > .contents").is(":visible")) {
                        $(".updown_enable").removeClass("updown_enable").addClass("updown_normal");
                        $("#list_" + id).removeClass("updown_normal").addClass("updown_enable");
                        $("#rec_del").attr("onclick", "removeLinha(" + id + ")");
                    }
                }
                function removeLinha(id) {
                    //SCRIPT PARA REMOVER UM PRODUTO DE "CUPOM FISCAL"
                    if ($(".products > .academia > .contents").is(":visible")) {
                        if (id < 5000) {
                            var conta = 1;
                            $("#list_" + id).remove();
                            $(".receipts > .contents > .updown > #listProd").find("tr > .item_count").each(function () {
                                if (conta > 9 && conta < 100) {
                                    $(this).html("0" + conta);
                                }
                                if (conta < 10) {
                                    $(this).html("00" + conta);
                                }
                                if (conta > 99) {
                                    $(this).html(conta);
                                }
                                conta = conta + 1
                            });
                        } else {
                            var conta = 1;
                            $("#list_" + id).remove();
                            $(".receipts > .contents > .updown > #listServ").find("tr > .item_count").each(function () {
                                if (conta > 9 && conta < 100) {
                                    $(this).html("0" + conta);
                                }
                                if (conta < 10) {
                                    $(this).html("00" + conta);
                                }
                                if (conta > 99) {
                                    $(this).html(conta);
                                }
                                conta = conta + 1
                            });
                        }
                        novoTotal();
                    }
                }
                function limpaQuant() {
                    //SCRIPT PARA LIMPAR AS INFORMAÇÕES CONTIDAS EM "QUANTIDADE"
                    $(".active").removeClass("button active").addClass("button");
                    $("#prc_valor, #prc_total").html("0,00");
                    $("#qnt_titulo").val("");
                    $("#qnt_codigo").val("");
                    $("#qnt_grupo").val("");
                    $("#qnt_valor").val("0");
                }
                function limpaPagto() {
                    //SCRIPT PARA LIMPAR AS INFORMAÇÕES CONTIDAS EM "PAGAMENTOS"
                    $("#qnt_vezes").val("1");
                    $("#desc_valor").val("0,00");
                    $(".payments > .updown > .mylist").html("");
                    $("#money").html("<span style=\"font-size:35px\">R$</span> " + $("#money_total").val());
                    $("#prim_prazo").val("<?php echo date("d/m/Y"); ?>");
                }
                function trocaTela(id) {
                    //SCRIPT PARA ALTERNAR A TELA DE PRODUTOS PARA PAGAMENTOS E VICE-VERSA
                    if ($("#money_total").val().replace(".", "").replace(",", ".") > 0.00) {
                        if (id == 1) {
                            $(".products > .academia, #buy_check").hide();
                            $(".products > .payments, #pay_check").show();
                        } else {
                            $(".products > .payments, #pay_check").hide();
                            $(".products > .academia, #buy_check").show();
                        }
                        $(".updown_enable").removeClass("updown_enable").addClass("updown_normal");
                        $(".products > .academia > .updown").html("");
                        limpaQuant();
                        limpaPagto();
                        resizeAll();
                    }
                }
                function abreSelect(elem) {
                    //SCRIPT PARA ABRIR UM SELECT AO CLICAR EM UM ÍCONE OU LINK    
                    if (document.createEvent) {
                        var e = document.createEvent("MouseEvents");
                        e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                        elem[0].dispatchEvent(e);
                    } else if (element.fireEvent) {
                        elem[0].fireEvent("onmousedown");
                    }
                }
                //SCRIPT PARA ABRIR O CALENDÁRIO NO "VENCIMENTO"
                $.extend($.fn.pickadate.defaults, {
                    monthsFull: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
                    weekdaysShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
                    //formatSubmit: "dd/mm/yyyy",
                    format: "dd/mm/yyyy",
                    clear: "Limpar",
                    today: "Hoje"
                });
                var picker = $("#prim_prazo").pickadate().pickadate("picker");
                $("#prim_click").click(function () {
                    picker.open();
                    event.stopPropagation();
                });
                var picker = $("#dat_plano").pickadate().pickadate("picker");
                $("#dat_click").click(function () {
                    picker.open();
                    event.stopPropagation();
                });
                //SCRIPT PARA TROCA DE SINAL EM "DESCONTO"
                $("#desc_click").click(function () {
                    if ($("#desc_sinal").html() == "<b>%</b>") {
                        $("#desc_sinal").html("<b>$</b>");
                        $("#txtProdSinal").val("1");
                    } else {
                        $("#desc_sinal").html("<b>%</b>");
                        $("#txtProdSinal").val("0");
                    }
                    novoDesconto();
                });
                function novoDesconto() {
                    //SCRIPT PARA APLICAR NOVO "DESCONTO" QUANDO ALTERADO
                    var calcTotal = parseFloat($("#money_total").val().replace(".", "").replace(",", "."));
                    var calcDesct = parseFloat($("#desc_valor").val().replace(".", "").replace(",", "."));
                    if ($("#desc_sinal").html() == "<b>%</b>") {
                        calcDesct = calcTotal * (calcDesct / 100);
                    }
                    if (calcDesct >= calcTotal || calcDesct < 0.00) {
                        calcDesct = 0.00;
                    }
                    $("#money").html("<span style=\"font-size:35px\">R$</span> " + numberFormat(calcTotal - calcDesct));
                    if ($(".payments > .updown > .mylist").html() != "") {
                        novaParcela();
                    }
                }
                function novaCarteira() {
                    //SCRIPT PARA FAZER AS VERIFICAÇÕES NECESSÁRIAS NA OPÇÃO CARTEIRA
                    var cartForma = parseInt($("#form_pagto option:selected").val());
                    $(".payments > .updown > .mylist").html("");
                    var permissaoAtual = <?php
                        if (is_numeric($ckb_fin_crt_)) {
                            echo $ckb_fin_crt_;
                        } else {
                            echo "0";
                        }
                        ?>;
                    if (cartForma == 12) {
                        if ($("#user_cart option:selected").val() != "" && permissaoAtual == 1) {
                            $("#user_cart").css("background-color", "#FFFFFF");
                            novaParcela();
                        } else if ($("#user_cart option:selected").val() != "") {
                            $("#user_cart").css("background-color", "#FFFFFF");
                            novaPermissao(0);
                        } else {
                            $("#user_cart").css("background-color", "#FAC8C8");
                        }
                    } else {
                        novaParcela();
                    }
                }
                function novaParcela() {
                    //SCRIPT PARA GERAR AS "PARCELAS" QUANDO CLICADO
                    var parcTotal = parseFloat($("#money_total").val().replace(".", "").replace(",", "."));
                    var parcDesct = parseFloat($("#desc_valor").val().replace(".", "").replace(",", "."));
                    var parcForma = parseInt($("#form_pagto option:selected").val());
                    var parcPVenc = $("#prim_prazo").val().split("/");
                    var parcVezes = parseInt($("#qnt_vezes").val());
                    var parcPagto = $("#form_pagto").html();
                    var parcDia = parseInt(parcPVenc[0]);
                    var parcMes = parseInt(parcPVenc[1]) - 1;
                    var parcAno = parseInt(parcPVenc[2]);
                    if ($("#desc_sinal").html() == "<b>%</b>") {
                        parcDesct = parcTotal * (parcDesct / 100);
                    }
                    if (parcDesct >= parcTotal || parcDesct < 0.00) {
                        parcDesct = 0.00;
                    }
                    var parcValor = numberFormat((parcTotal - parcDesct) / parcVezes);
                    if (parcVezes == 1 && parcForma == 6) {
                        var parcLista = "<table cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%; margin-top:10px\"><tr><td width=\"60%\"><div class=\"ready\">" + parcValor + "</div></td><td width=\"40%\"><div class=\"button\" style=\"background-color:#009AD7\"><div class=\"icon\" style=\"margin-top:1px\"><b>TOTAL</b></div></div></td></tr></table>";
                        var parcLista = parcLista + "<table cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%; margin-top:10px\"><tr><td width=\"60%\"><div class=\"ready\"><input id=\"pagto_valor\" name=\"pagto_valor\" type=\"text\" class=\"value\" value=\"" + parcValor + "\" onBlur=\"novoTroco()\"></div></td><td width=\"40%\"><div class=\"button\" style=\"background-color:#68AF27\"><div class=\"icon\" style=\"margin-top:1px\"><b>PAGTO</b></div></div></td></tr></table>";
                        var parcLista = parcLista + "<table cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%; margin-top:10px\"><tr><td width=\"60%\"><div id=\"pagto_troco\" class=\"ready\">0,00</div></td><td width=\"40%\"><div class=\"button\" style=\"background-color:#D53F26\"><div class=\"icon\" style=\"margin-top:1px\"><b>TROCO</b></div></div></td></tr></table>";
                        $(".payments > .updown > .mylist").html("<div id=\"tdp_1\" class=\"label\">Detalhamento do Pagamento</div>" + parcLista);
                        $("#pagto_valor").keypad();
                        $("#pagto_valor").maskMoney({thousands: ".", decimal: ","});
                    } else {
                        var parcLista = "";
                        for (var i = 1; i <= parcVezes; i++) {
                            if (parcMes == 12) {
                                parcMes = 0;
                                parcAno = parcAno + 1;
                                var parcNData = new Date(parcAno, parcMes, parcDia);
                            } else if ((parcMes + 1) == 2 && parcDia > 28) {
                                var parcNData = new Date(parcAno, parcMes, 28);
                            } else if (((parcMes + 1) == 4 || (parcMes + 1) == 6 || (parcMes + 1) == 9 || (parcMes + 1) == 11) && parcDia > 30) {
                                var parcNData = new Date(parcAno, parcMes, 30);
                            } else {
                                var parcNData = new Date(parcAno, parcMes, parcDia);
                            }
                            parcMes = parcMes + 1;
                            parcLista = parcLista + "<tr><td>" + (i < 10 ? "0" + i : i) + "</td><td><input id=\"parc_prazo_" + i + "\" name=\"parc_prazo_" + i + "\" type=\"text\" class=\"value\" value=\"" + (parcNData.getDate() < 10 ? "0" + parcNData.getDate() : parcNData.getDate()) + "/" + ((parcNData.getMonth() + 1) < 10 ? "0" + (parcNData.getMonth() + 1) : (parcNData.getMonth() + 1)) + "/" + parcNData.getFullYear() + "\"/></td><td><input id=\"parc_valor_" + i + "\" name=\"parc_valor_" + i + "\" type=\"text\" class=\"value\" value=\"" + parcValor + "\"/></td><td><select id=\"parcPagto_" + i + "\" name=\"parcPagto_" + i + "\" class=\"select\">" + parcPagto + "</select></td></tr>";
                        }
                        $(".payments > .updown > .mylist").html("<div id=\"tdp_2\" class=\"label\">Detalhamento das Parcelas</div><div class=\"input\"><table><tr bgcolor=\"#DDD\"><td width=\"7%\">Nº</td><td width=\"28%\">Vencimento</td><td width=\"20%\">Valor</td><td width=\"45%\">Forma de Pagto</td></tr>" + parcLista + "</table></div>");
                        novoCalendario(parcVezes, parcForma);
                        //CORREÇÃO DAS PARCELAR COM NÚMEROS NÃO-REDONDO
                        var parcValue = parseFloat((parcTotal - parcDesct) / parcVezes);
                        var parcFixed = parseFloat((parcTotal - parcDesct) - (parcValue * parcVezes));
                        $("#parc_valor_1").val(numberFormat(parcValue + parcFixed));
                        //CORREÇÃO PARA A OPÇÃO CARTEIRA NÃO SER EXIBIDA QUANDO NÃO SELECIONADA
                        if (parcForma != 12) {
                            for (var i = 1; i <= parcVezes; i++) {
                                $("#parcPagto_" + i + " option[value='12']").remove();
                            }
                        }
                    }
                }
                function novoUserLogin() {
                    //POPUP DE USUÁRIO PARA TROCA DE USUÁRIO E LOGOUT
                    if ($("#user_pop").length == 0) {
                        var sizew = $(window).width() - (140 + ($(".clocktime").innerWidth() - $(".clocktime").width()));
                        var sizeh = 50 + (($(".clocktime").innerHeight() - $(".clocktime").height()) / 2);
                        var user_pop = "<div id=\"user_pop\">";
                        user_pop = user_pop + "	<div class=\"keypad-login\" style=\"width:130px; height:50px; position:absolute; top:" + sizeh + "px; left:" + sizew + "px; display:block\">";
                        user_pop = user_pop + "		<button type=\"button\" class=\"keypad-special keypad-log-btn\" style=\"width:60px; height:50px; background-color:#D53F26\" onClick=\"window.top.location.href='http://finance.dragon296.startdedicated.com/login.php?ex=1'\"><div class=\"icon\"><span class=\"ico-signout\"></span></div></button>";
                        user_pop = user_pop + "		<button type=\"button\" class=\"keypad-special keypad-log-btn\" style=\"width:60px; height:50px; background-color:#FFAA31\" onClick=\"novaPermissao(1); novoUserLogin()\"><div class=\"icon\"><span class=\"ico-locked\"></span></div></button>";
                        user_pop = user_pop + "	</div>";
                        user_pop = user_pop + "</div>";
                        $("body").append(user_pop);
                    } else {
                        $("#user_pop").remove();
                    }
                }
                function novaPermissao(login) {
                    //TELA DE LOGIN PARA SOLICITAR PERMISSÃO PARA VENDA EM CARTEIRA
                    if ($("#perm_win").length == 0) {
                        if (login == 1) {
                            var label = "TROCA DE SESSÃO";
                        } else {
                            var label = "PERMISSÃO";
                        }
                        var perm_pop = "<div id=\"perm_win\">";
                        perm_pop = perm_pop + "	<div style=\"width:100%; height:100%; position:absolute; top:0; left:0; z-index:9\"><img src=\"./../../img/bg_preto.png\" style=\"width:100%; height:100%\"></div>";
                        perm_pop = perm_pop + "	<div class=\"keypad-popup\" style=\"width:268px; height:208px; position:absolute; top:50%; left:50%; margin-top:-104px; margin-left:-134px; display:block\">";
                        perm_pop = perm_pop + "		<div class=\"keypad-row\" style=\"width:248px; height:40px; font-size:18px; color:#333; text-align:center\"><b>" + label + "</b></div>";
                        perm_pop = perm_pop + "		<div class=\"keypad-row\" style=\"height:50px\">Login:<br><input id=\"perm_user\" name=\"perm_user\" type=\"text\" class=\"value\" style=\"width:244px\"></div>";
                        perm_pop = perm_pop + "		<div class=\"keypad-row\" style=\"height:50px\">Senha:<br><input id=\"perm_pass\" name=\"perm_pass\" type=\"password\" class=\"value\" style=\"width:244px\"></div>";
                        perm_pop = perm_pop + "		<div class=\"keypad-row\" style=\"margin-top:10px\">";
                        perm_pop = perm_pop + "			<button type=\"button\" class=\"keypad-special keypad-clear\" style=\"margin-right:42px\" onClick=\"novaChecagem(" + login + ")\">Concluir</button>";
                        perm_pop = perm_pop + "			<button type=\"button\" class=\"keypad-special keypad-clear\" style=\"margin-left:42px\" onClick=\"novaPermissao(" + login + ")\">Cancelar</button>";
                        perm_pop = perm_pop + "		</div>";
                        perm_pop = perm_pop + "	</div>";
                        perm_pop = perm_pop + "</div>";
                        $("body").append(perm_pop);
                    } else {
                        $("#perm_win").remove();
                    }
                }
                function novaChecagem(login) {
                    //VERIFICAÇÃO DE LOGIN DA PERMISSÃO ATRAVÉS DO JSON
                    if (login == 1) {
                        $("#log_user").val($("#perm_user").val());
                        $("#log_pass").val($("#perm_pass").val());
                        setTimeout(function () {
                            $("#log_form").submit();
                        }, 500);
                    } else {
                        $.getJSON("logar.ajax.php", {login: $("#perm_user").val(), senha: $("#perm_pass").val()}, function (data) {
                            if (data[0].valor) {
                                novaPermissao(0);
                                novaParcela();
                            } else {
                                alert("Ocorreu um dos seguintes erros:\n- Login ou senha incorreta.\n- Conta com permissão incompatível.");
                            }
                        });
                    }
                }
                function listaVendas() {
                    //JANELA PARA LISTAR AS ÚLTIMAS VENDAS REALIZADAS
                    if ($("#last_win").length == 0) {
                        var last_pop = "<div id=\"last_win\">";
                        last_pop = last_pop + "	<div style=\"width:100%; height:100%; position:absolute; top:0; left:0; z-index:9\"><img src=\"./../../img/bg_preto.png\" style=\"width:100%; height:100%\"></div>";
                        last_pop = last_pop + "	<div class=\"keypad-popup\" style=\"width:338px; height:308px; position:absolute; top:50%; left:50%; margin-top:-154px; margin-left:-169px; display:block\">";
                        last_pop = last_pop + "		<div class=\"keypad-row\" style=\"width:338px; height:250px; margin:0px; text-align:center\">";
                        last_pop = last_pop + "			<table width=\"100%\">";
                        last_pop = last_pop + "				<tr style=\"background-color:#333; color:#EEE\"><td><b>DATA</b></td><td><b>VALOR</b></td><td><b>SENHA</b></td><td><b>AÇÃO</b></td></tr>";
<?php
$cur = odbc_exec($con, "select top 5 id_venda,data_venda,(select SUM(valor_parcela) from sf_venda_parcelas where venda = id_venda) valor, cod_pedido from sf_vendas where cov = 'V' order by id_venda desc") or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    ?>
                            last_pop = last_pop + "				<tr style=\"background-color:#EEE; color:#333; font-size:18px; height:42px\">";
                            last_pop = last_pop + "					<td><?php echo "<a style='color:#333' href='cupom.php?id=" . $RFP['id_venda'] . "&crt=" . $contrato . "' target='_blank'>" . date_format(date_create(utf8_encode($RFP['data_venda'])), 'd/m/Y') . "</a>" ?></td>";
                            last_pop = last_pop + "					<td><?php echo "<a style='color:#333' href='cupom.php?id=" . $RFP['id_venda'] . "&crt=" . $contrato . "' target='_blank'>" . number_format($RFP['valor'], 2, ',', '.') . "</a>"; ?></td>";
                            last_pop = last_pop + "					<td><?php echo "<a style='color:#333' href='cupom.php?id=" . $RFP['id_venda'] . "&crt=" . $contrato . "' target='_blank'>" . $RFP['cod_pedido'] . "</a>"; ?></td>";
                            last_pop = last_pop + "					<td><button type=\"button\" class=\"keypad-special\" style=\"width:38px; height:38px; color:#FFF; background-color:#D53F26\"><div class=\"icon\"><span class=\"ico-cancel\"></span></div></button></td>";
                            last_pop = last_pop + "				</tr>";
<?php } ?>
                        last_pop = last_pop + "			</table>";
                        last_pop = last_pop + "		</div>";
                        last_pop = last_pop + "		<div class=\"keypad-row\" style=\"width:338px; margin:0px; text-align:center\">";
                        last_pop = last_pop + "			<button type=\"button\" class=\"keypad-special keypad-clear\" onClick=\"listaVendas()\">Fechar</button>";
                        last_pop = last_pop + "		</div>";
                        last_pop = last_pop + "	</div>";
                        last_pop = last_pop + "</div>";
                        $("body").append(last_pop);
                    } else {
                        $("#last_win").remove();
                    }
                }
                function contaCaixa() {
                    if ($("#cont_win").length == 0) {
                        var cont_pop = "<div id=\"cont_win\">";
                        cont_pop = cont_pop + "	<div style=\"width:100%; height:100%; position:absolute; top:0; left:0; z-index:9\"><img src=\"./../../img/bg_preto.png\" style=\"width:100%; height:100%\"></div>";
                        cont_pop = cont_pop + "	<div class=\"keypad-popup\" style=\"width:318px; height:308px; position:absolute; top:50%; left:50%; margin-top:-154px; margin-left:-159px; display:block\">";
                        cont_pop = cont_pop + "		<div class=\"keypad-row\" style=\"width:318px; height:250px; margin:0px; text-align:center\">";
                        cont_pop = cont_pop + "			<table width=\"100%\">";
                        cont_pop = cont_pop + "				<tr style=\"background-color:#333; color:#EEE\"><td colspan=\"3\"><b>RESUMO DO CAIXA</b></td></tr>";
<?php
$cur = odbc_exec($con, "set dateformat dmy;select td.descricao descricao,sum(vp.valor_pago) valor from sf_venda_parcelas vp 
                                                                                                                inner join sf_vendas v on vp.venda = v.id_venda
                                                                                                                inner join sf_tipo_documento td on td.id_tipo_documento = vp.tipo_documento
                                                                                                                where vendedor = '" . $_SESSION['login_usuario'] . "' and data_venda between '" . date("d/m/Y") . " 00:00:00' and '" . date("d/m/Y") . " 23:59:59' and valor_pago > 0 group by td.descricao order by 1") or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    ?>
                            cont_pop = cont_pop + "				<tr style=\"background-color:#EEE; color:#333; font-size:18px; height:42px\">";
                            cont_pop = cont_pop + "					<td><b><?php echo utf8_encode($RFP['descricao']); ?>:</b></td>";
                            cont_pop = cont_pop + "					<td><?php
    echo number_format($RFP['valor'], 2, ',', '.');
    $total = $RFP['valor'] + $total;
    ?></td>";
                            cont_pop = cont_pop + "				</tr>";
<?php } ?>
                        cont_pop = cont_pop + "				<tr style=\"background-color:#EEE; color:#333; font-size:18px; height:42px\">";
                        cont_pop = cont_pop + "					<td><b>Total:</b></td>";
                        cont_pop = cont_pop + "					<td><?php echo number_format($total, 2, ',', '.'); ?></td>";
                        cont_pop = cont_pop + "				</tr>";
                        cont_pop = cont_pop + "			</table>";
                        cont_pop = cont_pop + "		</div>";
                        cont_pop = cont_pop + "		<div class=\"keypad-row\" style=\"width:318px; margin:0px; margin-top:5px; text-align:center\">";
                        cont_pop = cont_pop + "			<input style=\"width:70px;height:45px;margin-right:20px;text-align: center;\" id=\"txtData\" type=\"text\" class=\"input-medium\" maxlength=\"10\" value=\"<?php echo date("d/m/Y"); ?>\"/>";
                        cont_pop = cont_pop + "			<button type=\"button\" class=\"keypad-special keypad-clear\" style=\"margin-right:20px\" onClick=\"abreBox()\" >Imprimir</button>";
                        cont_pop = cont_pop + "			<button type=\"button\" class=\"keypad-special keypad-clear\" style=\"margin-left:20px\" onClick=\"contaCaixa()\">Cancelar</button>";
                        cont_pop = cont_pop + "			<script>$(\"#txtData\").pickadate().pickadate(\"picker\");<\/script>";
                        cont_pop = cont_pop + "		</div>";
                        cont_pop = cont_pop + "	</div>";
                        cont_pop = cont_pop + "</div>";
                        $("body").append(cont_pop);
                    } else {
                        $("#cont_win").remove();
                    }
                }
                function abreBox() {
                    var data = "&dt=" + $("#txtData").val().replace(new RegExp("/", 'g'), "_");
                    window.open("consulta.php<?php echo "?user=" . $_SESSION['login_usuario'] . "&crt=" . $contrato; ?>" + data, "_blank");
                }
                function novoCalendario(loop, select) {
                    //SCRIPT PARA GERAR CALENDÁRIOS PARA TODAS AS PARCELAS
                    var parcPicker = [];
                    for (var i = 1; i <= loop; i++) {
                        $("#parcPagto_" + i + " option[value='" + select + "']").prop("selected", true);
                        parcPicker[i] = $("#parc_prazo_" + i).pickadate().pickadate("parcPicker[" + i + "]");
                        $("#parc_valor_" + i).keypad();
                        $("#parc_valor_" + i).maskMoney({thousands: ".", decimal: ","});
                    }
                }
                function novoTroco() {
                    //SCRIPT PARA CALCULAR O "TROCO"
                    var parcValor = parseFloat($("#pagto_valor").val().replace(".", "").replace(",", "."));
                    var parcTotal = parseFloat($("#money_total").val().replace(".", "").replace(",", "."));
                    var parcDesct = parseFloat($("#desc_valor").val().replace(".", "").replace(",", "."));
                    if ($("#desc_sinal").html() == "<b>%</b>") {
                        parcDesct = parcTotal * (parcDesct / 100);
                    }
                    if (parcDesct >= parcTotal || parcDesct < 0.00) {
                        parcDesct = 0.00;
                    }
                    if ((parcTotal - parcDesct) < parcValor) {
                        var parcTroco = numberFormat(parcValor - (parcTotal - parcDesct));
                    } else {
                        var parcTroco = "<font color=\"FF0000\">" + numberFormat(parcValor - (parcTotal - parcDesct)) + "</font>";
                    }
                    $("#pagto_troco").html(parcTroco);
                }
                function trocarMenu(acao) {
                    //SCRIPT PARA TROCAR O "MENU" E A "BUSCA"
                    if (acao == 1) {
                        $(".navigation").hide();
                        $(".searchcode").show();
                    }
                    if (acao == 0) {
                        $(".searchcode").hide();
                        $(".navigation").show();
                    }
                }
                function trocarTipo(acao) {
                    //SCRIPT PARA TROCAR O "MÉTODO" ENTRE "PRODUTOS" E "SERVIÇOS"
                    if (acao == 1) {
                        $("#menuProd").show();
                        $("#menuServ").hide();
                        $(".clocktime .button").attr("onclick", "trocarTipo(0)");
                        $(".clocktime .button .icon").html("<span class=\"ico-stack-3\"></span>");
                        carrega(-1);
                    }
                    if (acao == 0) {
                        $("#menuProd").hide();
                        $("#menuServ").show();
                        $(".clocktime .button").attr("onclick", "trocarTipo(1)");
                        $(".clocktime .button .icon").html("<span class=\"ico-tools\"></span>");
                        carrega(-1);
                    }
                }
                function novaBusca() {
                    //SCRIPT PARA BUSCAR O PRODUTO SELECIONADO POR CÓDIGO DE BARRAS
                    carrega($("#cod_busca").val());
                }
                $(function () {
                    //SCRIPT PARA TRAVAR O ENTER, SENDO USADO APENAS NA BUSCA E QUANTIDADE
                    $(document).keypress(function (evento) {
                        if (evento.which == 13) {
                            if ($("#qnt_valor").val() > 0) {
                                novaLinha();
                            }
                            if ($("#cod_busca").val() != "") {
                                novaBusca();
                            }
                            evento.preventDefault();
                            return false;
                        }
                    });
                });
                $("#desc_valor").change(function () {
                    //SCRIPT DE CORREÇÃO DO VALOR DE DESCONTO, ZERANDO-O QUANDO ESTIVER VAZIO
                    if ($("#desc_valor").val() == "") {
                        $("#desc_valor").val("0,00");
                    }
                });
                $("#qnt_valor, #qnt_vezes, #desc_valor").keypad();
                $("#desc_valor").maskMoney({thousands: ".", decimal: ","});
            </script>
        </form>
        <form method="post" id="log_form" action="./../../login_vai.php">
            <input type="hidden" name="empresa" value="<?php echo $contrato; ?>"></input>
            <input type="hidden" id="log_pass" name="password"></input>
            <input type="hidden" id="log_user" name="login"></input>
            <input type="hidden" name="comeback" value="1"></input>
        </form>
    </body>
<?php odbc_close($con); ?>
</html>