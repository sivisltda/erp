<?php

$cnpj = $_SESSION["cpf_cnpj"];
$query = "select id_fornecedores_despesas,cnpj,cidade_codigoEstado,cidade_codigo,estado_gov from sf_fornecedores_despesas f 
inner join tb_cidades c on f.cidade = c.cidade_codigo 
inner join tb_estados e on c.cidade_codigoEstado = e.estado_codigo where cnpj = '" . $cnpj . "'";
$cur = odbc_exec($con, $query) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $id_emitente = $RFP['id_fornecedores_despesas'];
    $estado = $RFP['cidade_codigoEstado'];
    $cidade = $RFP['cidade_codigo'];    
    $estadoNFEX = $RFP['estado_gov'];
}
$res = odbc_exec($con, "select isnull(max(nnf),0) + 1 nnf from sf_nfe_notas where mod > 0") or die(odbc_errormsg());
$numNFE = odbc_result($res, 1);
$serie = "001";
$cod_numerico = rand(99999999, 11111111);
$for_pgto = '0';
$cfop = '5102';
$dtEmissao = getData("T");
$chave = "";
$dv = "0";
$cod_nfe = "";
if (is_numeric($id_emitente)) { //insere NFCe
    $query = "set dateformat dmy;
        INSERT INTO sf_nfe_notas(status_nfe,code,versao_xml,mod,serie,nnf,demis,cnf,cdv,
        tpnf,dSaiEnt,hSaiEnt,tpemis,indPag,finNFe,tpImp,natop,uf,cmunfg,contigencia_hora,contingencia_motivo,tpAmb,procEmi,verProc,
        codigo_emitente,codigo_destinatario,impresso,exportado,mod_transp,cod_transp,placa_veiculo,estado_veiculo,reboque_veiculo,reboque_veiculo_placa,reboque_veiculo_estado,
        tipo_volume,volume_transp,peso_liq_transp,peso_bruto_transp,endereco_entrega,info_add_fisco,info_add_complementar,xml,dhRecbto,nProt,
        destinatario,status,cfop_padrao,msg_comp,msg_comp_fisco,descontop,descontotp,hSaiEmi,conFinal,destOper,tipoAten,id_venda_nfe)
        VALUES('" . utf8_decode('Em Digitação') . "','" . $chave . "','3.10',65," . $serie . "," . $numNFE . ",'" . $dtEmissao . "'," . utf8_decode($cod_numerico) . "," . $dv .
            ",'1',cast(getdate() as date),'" . date("H:i:s") . "', 1, 0, 1, 4, 'VENDA'," . utf8_decode($estado) . "," . utf8_decode($cidade) . ", NULL,''," . $tpAmb . ",3,'3.10.86'," .
            utf8_decode($id_emitente) . "," . utf8_decode($codCliente) . ", 0, 0, 9,NULL,'',NULL,'','',NULL," .
            "'',0,0,0,NULL,'','',null,null,null,'" .
            utf8_decode($_SESSION["login_usuario"]) . "','Aprovado','" . utf8_decode($cfop) . "', NULL, NULL,0,0,'" . date("H:i:s") . "', 1,1,0," . $nn . ")";
    odbc_exec($con, $query) or die(odbc_errormsg());
    $res = odbc_exec($con, "select top 1 cod_nfe from sf_nfe_notas where id_venda_nfe = " . $nn . " order by cod_nfe desc") or die(odbc_errormsg());
    $cod_nfe = odbc_result($res, 1);
    if (is_numeric($cod_nfe)) { //insere item NFCe
        $query = "select isnull((select SUM(valor_total) from sf_vendas_itens A inner join sf_produtos B on A.produto = B.conta_produto where id_venda = i.id_venda
            and B.tipo = 'S' and B.compoe_frete_nfe = 1) / 
            (select COUNT(*) from sf_vendas_itens A inner join sf_produtos B on A.produto = B.conta_produto where id_venda = i.id_venda
            and B.tipo = 'P'),0) tot_frete,
            isnull((select descontop from sf_vendas where id_venda = i.id_venda) / 
            isnull((select COUNT(*) from sf_vendas_itens A inner join sf_produtos B on A.produto = B.conta_produto where id_venda = i.id_venda
            and B.tipo = 'P'),0),0) total_desconto
            from sf_vendas_itens i where id_venda = " . $nn . "
            group by id_venda";
        $cur = odbc_exec($con, $query);
        $tot_frete = 0;
        $tot_desconto = 0;
        while ($RFP = odbc_fetch_array($cur)) {
            $tot_frete = $RFP['tot_frete'];
            $tot_desconto = $RFP['total_desconto'];
        }        
        $query = "INSERT INTO sf_nfe_notas_itens (
            cod_nota,cod_prod,qtd_comercial,tot_val_bruto,
            cfop,qtd_trib,tot_seguro,tot_desconto,tot_frete,tot_outras_despesas,
            icms_sit_trib,icms_origem,pis_sit_trib,pis_quantidade,cofins_sit_trib,cofins_quantidade,info_add, 
            icms_alig_calc_cred, icms_val_deson, icms_red_bc,icms_aliq,icms_mod_det_st,icms_marg_va_st,icms_red_bc_st,icms_aliq_st,
            pis_aliq_perc,pis_aliq_real,pis_tipo_calc,cofins_aliq_perc,cofins_aliq_real,cofins_tipo_calc,icms_mot_deson)
            select " . $cod_nfe . ",produto,quantidade,valor_total,
            cfop,quantidade,0," . $tot_desconto . "," . $tot_frete . ",0,
            ICMSSitTrib, ICMSOrigem, PISSitTrib, PISQtdVend, COFINSSitTrib, COFINSQtdVend, '',
            AliqCalcCred, ICMSValDes, ICMSRedBc, ICMSAliq, ICMSModDetSt, ICMSMargValAdSt, ICMSRedBcSt, ICMSAliqSt,
            PISAliqPerc, PISAliqReal, PISTipoCalc, COFINSAliqPerc, COFINSAliqReal, PISTipoCalc, ICMSValMot
            from sf_vendas_itens C inner join sf_produtos A on C.produto = A.conta_produto 
            inner join sf_nfe_cenarios B on A.cenario = B.id_cenario
            where id_venda = " . $nn . " and A.tipo = 'P'";
        odbc_exec($con, $query) or die(odbc_errormsg());
    }
}
