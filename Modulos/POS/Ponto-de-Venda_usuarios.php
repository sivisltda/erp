<?php include "./../../Connections/configini.php"; ?>
<div id="user_win">
    <div style="width:100%; height:100%; position:absolute; top:0; left:0; z-index:9"><img src="./../../img/bg_preto.png" style="width:100%; height:100%"></div>
    <div class="keypad-popup" style="width:700px; height:500px; position:absolute; top:50%; left:50%; margin-top:-250px; margin-left:-350px; display:block">
        <div class="keypad-row" style="width:700px; height:500px; margin:0px; text-align:center">
            <div id="listar" class="trans_venda" style="width:100%">
                <div style="float:left; width:calc(100% - 130px)">
                    <div class="label">Pesquisar por Nome ou Telefone:</div>
                    <div class="input">
                        <input id="user_text" name="user_text" type="text" class="value" onkeypress="return runScript(event, 'user_text')">
                    </div>
                </div>
                <div class="button" style="float:left; width:50px; background-color:#009AD7" onClick="buscarUsuario()">
                    <div class="icon"><span class="ico-search"></span></div>
                </div>
                <div class="button" style="float:left; width:50px; margin-left:10px; background-color:#D53F26" onClick="usuarioVenda()">
                    <div class="icon"><span class="ico-cancel"></span></div>
                </div>
                <div style="clear:both"></div>
                <div class="label" style="margin-top:10px">Lista de Pessoas:</div>
                <div id="lista_user" style="overflow-y:hidden; width:calc(100% - 2px); height:378px; border-left:1px solid #CCC; border-right:1px solid #CCC"></div>
                <div style="clear:both"></div>
                <div class="label" style="line-height:40px">&nbsp;
                    <button type="button" class="keypad-special" style="float:left; width:42px; height:38px; color:#FFF; background-color:#68AF27" onClick="trocaTab('listar', 'criar')">
                        <div class="icon" style="margin-top:5px"><span class="ico-plus" style="font-size:22px"></span></div>
                    </button>
                    <button id="user_down" type="button" class="keypad-special" style="float:right; width:42px; height:38px; color:#FFF; background-color:#009AD7">
                        <div class="icon" style="margin-top:5px"><span class="ico-arrow-down" style="font-size:22px"></span></div>
                    </button>
                    <button id="user_up" type="button" class="keypad-special" style="float:right; width:42px; height:38px; color:#FFF; margin-right:0; background-color:#009AD7">
                        <div class="icon" style="margin-top:5px"><span class="ico-arrow-up" style="font-size:22px"></span></div>
                    </button>
                </div>
            </div>
            <div id="criar" class="trans_venda" style="width:100%; display:none">
                <div style="float:left; width:calc(50% - 40px); margin-bottom:10px">
                    <div class="label">Nome:</div>
                    <div class="input" style="border-right:1px solid #CCC">
                        <input id="user_nome" name="user_nome" type="text" class="value" style="text-align:left"/>
                    </div>
                </div>
                <div style="float:left; width:calc(50% - 40px); margin-left:10px">
                    <div class="label">Sobrenome:</div>
                    <div class="input" style="border-right:1px solid #CCC">
                        <input id="user_sobre" name="user_sobre" type="text" class="value" style="text-align:left"/>
                    </div>
                </div>
                <div class="button" style="float:left; width:50px; margin-left:10px; background-color:#D53F26" onClick="usuarioVenda()">
                    <div class="icon"><span class="ico-cancel"></span></div>
                </div>
                <div style="clear:both"></div>
                <div style="float:left; width:20%; margin-right:10px">
                    <div class="label">CEP:</div>
                    <div class="input" style="border-right:1px solid #CCC">
                        <input id="user_cep" name="user_cep" type="text" class="value"/>
                    </div>
                </div>
                <div style="float:left; width:calc(80% - 10px); margin-bottom:10px">
                    <div class="label">Endereço:</div>
                    <div class="input" style="border-right:1px solid #CCC">
                        <input id="user_end" name="user_end" type="text" class="value" style="text-align:left"/>
                    </div>
                </div>
                <div style="clear:both"></div>
                <div style="float:left; width:15%; margin-right:10px">
                    <div class="label">Número:</div>
                    <div class="input" style="border-right:1px solid #CCC">
                        <input id="user_num" name="user_num" type="text" class="value" style="text-align:left"/>
                    </div>
                </div>
                <div style="float:left; width:calc(40% - 20px); margin-right:10px">
                    <div class="label">Bairro:</div>
                    <div class="input" style="border-right:1px solid #CCC">
                        <input id="user_bai" name="user_bai" type="text" class="value" style="text-align:left"/>
                    </div>
                </div>
                <div style="float:left; width:45%; margin-bottom:10px">
                    <div class="label">Complemento:</div>
                    <div class="input" style="border-right:1px solid #CCC">
                        <input id="user_com" name="user_com" type="text" class="value" style="text-align:left"/>
                    </div>
                </div>
                <div style="clear:both"></div>
                <div style="float:left; width:32.5%; margin-bottom:10px">
                    <div class="label">Estado:</div>
                    <div class="input">
                        <select id="user_est" name="user_est" class="value select">
                            <option uf="" value="">Selecione</option>
                            <?php
                            $cur = odbc_exec($con, "select estado_codigo,estado_nome,estado_sigla from tb_estados where estado_codigo > 0 order by estado_nome") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option uf="<?php echo strtoupper(utf8_encode($RFP['estado_sigla'])); ?>" value="<?php echo $RFP['estado_codigo'] ?>"><?php echo utf8_encode($RFP['estado_nome']) ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="button" style="float:left; width:50px; margin-right:10px; background-color:#009AD7" onclick="abreSelect(user_est)">
                    <div class="icon"><span class="ico-chevron-down"></span></div>
                </div>
                <div style="float:left; width:calc(67.5% - 130px); margin-bottom:10px">
                    <div class="label">Cidade:</div>
                    <div class="input">
                        <select id="user_cid" name="user_cid" class="value select">
                            <option value="">Selecione a Cidade</option>
                        </select>
                    </div>
                </div>
                <div class="button" style="float:left; width:50px; background-color:#009AD7" onclick="abreSelect(user_cid)">
                    <div class="icon"><span class="ico-chevron-down"></span></div>
                </div>
                <div style="clear:both"></div>
                <div style="float:left; width:20%; margin-right:10px">
                    <div class="label">Nascimento:</div>
                    <div class="input" style="border-right:1px solid #CCC">
                        <input id="user_nasc" name="user_nasc" type="text" class="value"/>
                    </div>
                </div>
                <div style="float:left; width:20%; margin-bottom:10px">
                    <div class="label">Contato:</div>
                    <div class="input">
                        <select id="user_tipo" name="user_tipo" class="value select" onChange="alterarContato()">
                            <option value="0">Telefone</option>
                            <option value="1">Celular</option>
                            <option value="2">E-mail</option>
                            <option value="3">Site</option>
                            <option value="4">Outros</option>
                        </select>
                    </div>
                </div>
                <div class="button" style="float:left; width:50px; margin-right:10px; background-color:#009AD7" onclick="abreSelect(user_tipo)">
                    <div class="icon"><span class="ico-chevron-down"></span></div>
                </div>
                <div style="float:left; width:calc(60% - 140px); margin-bottom:10px">
                    <div id="user_label" class="label">Endereço de E-mail:</div>
                    <div class="input" style="border-right:1px solid #CCC">
                        <input id="user_cont" name="user_cont" type="text" class="value" style="text-align:left"/>
                    </div>
                </div>
                <div class="button" style="float:left; width:50px; background-color:#68AF27" onclick="incluirContato()">
                    <div class="icon"><span class="ico-plus"></span></div>
                </div>
                <div style="clear:both"></div>
                <div class="label">Lista de Contatos:</div>
                <div id="lista_cont" style="overflow-y:hidden; width:calc(100% - 2px); height:130px; border-left:1px solid #CCC; border-right:1px solid #CCC"></div>
                <div style="clear:both"></div>
                <div class="label" style="line-height:40px">&nbsp;
                    <button type="button" class="keypad-special" style="float:left; width:42px; height:38px; color:#FFF; margin-right:0; background-color:#68AF27">
                        <div class="icon" style="margin-top:5px"><span onclick="salvarCliente()" class="ico-ok" style="font-size:22px"></span></div>
                    </button>
                    <button type="button" class="keypad-special" style="float:left; width:42px; height:38px; color:#FFF; background-color:#D53F26" onClick="trocaTab('criar', 'listar')">
                        <div class="icon" style="margin-top:5px"><span class="ico-reply" style="font-size:22px"></span></div>
                    </button>
                    <button id="cont_down" type="button" class="keypad-special" style="float:right; width:42px; height:38px; color:#FFF; background-color:#009AD7">
                        <div class="icon" style="margin-top:5px"><span class="ico-arrow-down" style="font-size:22px"></span></div>
                    </button>
                    <button id="cont_up" type="button" class="keypad-special" style="float:right; width:42px; height:38px; color:#FFF; margin-right:0; background-color:#009AD7">
                        <div class="icon" style="margin-top:5px"><span class="ico-arrow-up" style="font-size:22px"></span></div>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="./../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript">
        $("#user_up").on("mousedown", function () {
            interval = setInterval(function () {
                var pos = $("#lista_user").scrollTop();
                $("#lista_user").scrollTop(pos - 2);
            }, 1);
        }).click().on("mouseup", function () {
            clearInterval(interval);
        });
        $("#user_down").on("mousedown", function () {
            interval = setInterval(function () {
                var pos = $("#lista_user").scrollTop();
                $("#lista_user").scrollTop(pos + 2);
            }, 1);
        }).click().on("mouseup", function () {
            clearInterval(interval);
        });
        $("#cont_up").on("mousedown", function () {
            interval = setInterval(function () {
                var pos = $("#lista_cont").scrollTop();
                $("#lista_cont").scrollTop(pos - 2);
            }, 1);
        }).click().on("mouseup", function () {
            clearInterval(interval);
        });
        $("#cont_down").on("mousedown", function () {
            interval = setInterval(function () {
                var pos = $("#lista_cont").scrollTop();
                $("#lista_cont").scrollTop(pos + 2);
            }, 1);
        }).click().on("mouseup", function () {
            clearInterval(interval);
        });
        var loop = 0;
        $("#user_cep").mask("99.999-999");
        $("#user_nasc").mask(lang["dateMask"]);
        $("#user_cont").mask("(99) 9999-9999");
        $("#user_label").html("Número de Telefone:");
        function alterarContato() {
            $("#user_cont").val("");
            var tipo = $("#user_tipo option:selected").val();
            if (tipo === "0") {
                $("#user_cont").mask("(99) 9999-9999");
                $("#user_label").html("Número de Telefone:");
            } else if (tipo === "1") {
                $("#user_cont").mask("(99) 99999-9999");
                $("#user_label").html("Número de Celular:");
            } else if (tipo === "2") {
                $("#user_cont").unmask("(99) 9999-9999");
                $("#user_cont").unmask("(99) 99999-9999");
                $("#user_label").html("Endereço de E-mail:");
            } else if (tipo === "3") {
                $("#user_cont").unmask("(99) 9999-9999");
                $("#user_cont").unmask("(99) 99999-9999");
                $("#user_label").html("Endereço de Site:");
            } else if (tipo === "4") {
                $("#user_cont").unmask("(99) 9999-9999");
                $("#user_cont").unmask("(99) 99999-9999");
                $("#user_label").html("Outros:");
            }
        }
        function buscarUsuario() {
            var novaBusca = "";
            if ($("#user_text").val().length >= 3) {
                $("#loader").show();
                $("#lista_user").html("");
                $.getJSON("clientes.ajax.php?search=", {txtBusca: $("#user_text").val(), ajax: "true"}, function (j) {
                    if (j.length > 0) {
                        for (var i = 0; i < j.length; i++) {
                            novaBusca = "<div style=\"background:#EEE; color:#999; font-size:12px; text-align:left; height:40px; border-bottom:1px solid #FFF\">";
                            novaBusca = novaBusca + "	<div style=\"float:right; width:44px; line-height:40px\">";
                            novaBusca = novaBusca + "		<button type=\"button\" class=\"keypad-special\" style=\"width:42px; height:38px; color:#FFF; background-color:#68AF27\">";
                            novaBusca = novaBusca + "			<div class=\"icon\" style=\"margin-top:1px\"><span onclick=\"selecionaCliente(" + j[i].cliente_id + ",'" + j[i].cliente_nome + "'," + j[i].pacotes + ")\" class=\"ico-arrow-right\" style=\"font-size:22px\"></span></div>";
                            novaBusca = novaBusca + "		</button>";
                            novaBusca = novaBusca + "	</div>";
                            novaBusca = novaBusca + "	<div style=\"float:left; color:#333; font-size:14px; width:calc(100% - 50px); padding:5px 0 0 5px\"><b>" + j[i].cliente_nome + "</b></div>";
                            novaBusca = novaBusca + "	<div style=\"float:left; padding:0 10px 0 5px\">" + j[i].cliente_telefone + "</div><div style=\"float:left\">" + j[i].cliente_endereco + "</div>";
                            novaBusca = novaBusca + "</div>";
                            $("#lista_user").append(novaBusca);
                        }
                    } else {
                        $("#lista_user").append("<div> Cliente não encontrado para os dados informados</div>")
                    }
                }).always(function () {
                    $("#loader").hide();
                });
                ;
            }
        }
        function incluirContato() {
            var cont = $("#user_cont").val();
            var tipo = legendaTipo($("#user_tipo option:selected").val());
            if (cont.length > 8) {
                var novaLinha = "<div id=\"cont_" + loop + "\" style=\"background:#EEE; color:#999; font-size:12px; text-align:left; height:40px; border-bottom:1px solid #FFF\">";
                novaLinha = novaLinha + "	<div style=\"float:right; width:44px; line-height:40px\">";
                novaLinha = novaLinha + "		<button type=\"button\" class=\"keypad-special\" style=\"width:42px; height:38px; color:#FFF; background-color:#D53F26\" onClick=\"excluirContato(" + loop + ")\">";
                novaLinha = novaLinha + "			<div class=\"icon\" style=\"margin-top:5px\"><span class=\"ico-cancel\" style=\"font-size:22px\"></span></div>";
                novaLinha = novaLinha + "		</button>";
                novaLinha = novaLinha + "	</div>";
                novaLinha = novaLinha + "	<input type=\"hidden\" name=\"tipo_cont[]\" value=\"" + $("#user_tipo option:selected").val() + "\">";
                novaLinha = novaLinha + "	<input type=\"hidden\" name=\"val_cont[]\" value=\"" + $("#user_cont").val() + "\">";
                novaLinha = novaLinha + "	<div style=\"float:left; color:#333; font-size:14px; line-height:40px; width:150px; padding-left:10px\"><b>" + tipo + "</b></div>";
                novaLinha = novaLinha + "	<div style=\"float:left; color:#333; font-size:14px; line-height:40px\">" + cont + "</div>";
                novaLinha = novaLinha + "</div>";
                $("#lista_cont").append(novaLinha);
                $("#user_cont").val("");
                loop = loop + 1;
            }
        }
        function legendaTipo(val) {
            if (val === "0") {
                return "Telefone";
            } else if (val === "1") {
                return "Celular";
            } else if (val === "2") {
                return "E-mail";
            } else if (val === "3") {
                return "Site";
            } else if (val === "4") {
                return "Outros";
            }
        }
        function excluirContato(loop) {
            $("#cont_" + loop).remove();
        }
        $("#user_cep").change(function (event) {
            if ($("#user_cep").val() !== "") {
                $.get("https://viacep.com.br/ws/" + $(this).val() + "/json/",
                        function (data) {
                            var str = jQuery.parseJSON(data);
                            $("#user_end, #user_bai, #user_cid, #user_est").attr("readonly", true);
                            if (str !== false) {
                                var estado = $("#user_est option[uf='" + str.uf.toUpperCase() + "']").val();
                                $("#user_end").val(decodeURIComponent(escape(str.logradouro)));
                                $("#user_bai").val(decodeURIComponent(escape(str.bairro)));
                                $("#user_cid").val(decodeURIComponent(escape(str.localidade)));
                                $("#user_est").val(estado);
                                $("#user_num").focus();
                                carregaCidade(estado, str.localidade);
                            } else if (str === false) {
                                $("#user_end").focus();
                                $("#user_end, #user_bai, #user_cid, #user_est").val("");
                                $("#user_end, #user_bai, #user_cid, #user_est").attr("readonly", false);
                            }
                        });
            } else {
                $("#user_end, #user_bai, #user_cid, #user_est").val("");
                $("#user_cep").focus();
            }
        });
        $("#user_est").change(function (event) {
            carregaCidade($(this).val(), "");
        });
        $("#user_sobre").change(function (event) {
            if ($('#user_nome').val() !== "") {
                buscaNome($('#user_nome').val() + ' ' + $('#user_sobre').val());
            }
        });
        $("#user_nome").change(function (event) {
            if ($('#user_sobre').val() !== "") {
                buscaNome($('#user_nome').val() + ' ' + $('#user_sobre').val());
            }
        });

        function buscaNome(nome) {
            var novaBusca = "";
            $("#lista_user").html("");
            $.getJSON("clientes.ajax.php?search=", {txtBusca: nome, ajax: "true"}, function (j) {
                if (j.length > 0) {
                    bootbox.confirm("Foi encontrado um cliente com este nome. Deseja continuar com um novo cadastro?", function (result) {
                        if (result === true) {
                            return;
                        } else {
                            for (var i = 0; i < j.length; i++) {
                                novaBusca = "<div style=\"background:#EEE; color:#999; font-size:12px; text-align:left; height:40px; border-bottom:1px solid #FFF\">";
                                novaBusca = novaBusca + "	<div style=\"float:right; width:44px; line-height:40px\">";
                                novaBusca = novaBusca + "		<button type=\"button\" class=\"keypad-special\" style=\"width:42px; height:38px; color:#FFF; background-color:#68AF27\">";
                                novaBusca = novaBusca + "			<div class=\"icon\" style=\"margin-top:1px\"><span onclick=\"selecionaCliente(" + j[i].cliente_id + ",'" + j[i].cliente_nome + "'," + j[i].pacotes + ")\" class=\"ico-arrow-right\" style=\"font-size:22px\"></span></div>";
                                novaBusca = novaBusca + "		</button>";
                                novaBusca = novaBusca + "	</div>";
                                novaBusca = novaBusca + "	<div style=\"float:left; color:#333; font-size:14px; width:calc(100% - 50px); padding:5px 0 0 5px\"><b>" + j[i].cliente_nome + "</b></div>";
                                novaBusca = novaBusca + "	<div style=\"float:left; padding:0 10px 0 5px\">" + j[i].cliente_telefone + "</div><div style=\"float:left\">" + j[i].cliente_endereco + "</div>";
                                novaBusca = novaBusca + "</div>";
                                $("#lista_user").append(novaBusca);
                                $("#user_text").val(nome);
                                trocaTab('criar', 'listar');
                            }
                        }
                    });
                }

            });
        }
        function carregaCidade(estado, cidade) {
            $("#loader").show();
            $.getJSON("../Comercial/locais.ajax.php?search=", {txtEstado: estado, ajax: "true"}, function (j) {
                var options = "<option value=\"\">Selecione a Cidade</option>";
                var selecao = "";
                if (j.length > 0) {
                    for (var i = 0; i < j.length; i++) {
                        if (cidade === j[i].cidade_nome) {
                            selecao = "selected";
                        } else {
                            selecao = "";
                        }
                        options = options + "<option value=\"" + j[i].cidade_codigo + "\"" + selecao + ">" + j[i].cidade_nome + "</option>";
                    }
                    $("#user_cid").html(options);
                } else {
                    bootbox.alert("CEP não encontrado!");
                    $(".bootbox").find("div.modal-content").css("width", "250px");
                    $(".bootbox").find("div.modal-content").css("left", "200px");
                    $("#user_end").focus();
                }
            }).always(function () {
                $("#loader").hide();
            });
        }
        function selecionaCliente(id, nome, pacotes) {
            var options = "<option value=\"" + id + "\" selected>" + nome + "</option>";
            $("#user_cart").html(options);
            $("#nmCliente").html("&nbsp;<b>" + nome + "</b>");
            $("#nmClienteComanda").html(nome.substring(0, 20));
            usuarioVenda();
            if ($("#txtId").val() !== "") {
                salvaComanda();
            }
            if (pacotes > 0) {
                listaItensPacoteClientes(id);
            }
        }
        function listaItensPacoteClientes(cliente) {
            //Ao selecionar um serviço pacote, mostrar os itens referentes ao mesmo
            if ($("#lcom_win").length === 0) {
                $.get("Ponto-de-Venda_itens_pacote.php", {cliente: cliente}, function (data) {
                    $("body").append(data);
                });
            } else {
                $("#lcom_win").remove();
            }
        }
        function salvarCliente() {
            if ($("#user_nome").val().length > 1 && $("#user_sobre").val().length > 1) {
                $("#loader").show();
                $.post("clientes.controler.ajax.php", {
                    cli_nome: $("#user_nome").val() + " " + $("#user_sobre").val(),
                    cli_cep: $("#user_cep").val(),
                    cli_end: $("#user_end").val(),
                    cli_num: $("#user_num").val(),
                    cli_bai: $("#user_bai").val(),
                    cli_com: $("#user_com").val(),
                    cli_est: $("#user_est").val(),
                    cli_cid: $("#user_cid").val(),
                    cli_nas: $("#user_nasc").val(),
                    tipo_cont: $('input[name="tipo_cont[]"]').serializeArray(),
                    val_cont: $('input[name="val_cont[]"]').serializeArray(),
                    acao: 0, id: 0}).done(function (data) {
                    if ($.isNumeric(data)) {
                        $("#user_text").val($("#user_nome").val() + " " + $("#user_sobre").val());
                        buscarUsuario();
                        trocaTab('criar', 'listar');
                    }
                }).always(function () {
                    $("#loader").hide();
                });
            } else {
                bootbox.alert("Nome e sobrenome obrigatórios!");
                $(".bootbox").find("div.modal-content").css("width", "300px");
                $(".bootbox").find("div.modal-content").css("left", "200px");
            }
        }

        function runScript(e, campo) {
            if (e.keyCode === 13 && campo === 'user_text') {
                buscarUsuario();
            }
        }
    </script>
    <?php odbc_close($con);?>
</div>
