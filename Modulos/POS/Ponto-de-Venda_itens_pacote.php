<?php
include "./../../Connections/configini.php";
$id = $_GET['servico'];
$cliente = $_GET['cliente'];
$venda = $_GET['venda'];
?>
<div id="lcom_win">
    <div style="width:100%; height:100%; position:absolute; top:0; left:0; z-index:9"><img src="./../../img/bg_preto.png" style="width:100%; height:100%"></div>
    <div class="keypad-popup" style="width:700px; height:500px; position:absolute; top:50%; left:50%; margin-top:-250px; margin-left:-350px; display:block">
        <div class="keypad-row" style="width:700px; height:500px; margin:0px; text-align:center">
            <div class="trans_venda" style="width:100%">
                <div class="label">Serviços do Pacote:</div>
                <div id="lista_comanda" style="overflow-y:hidden; width:calc(100% - 2px); height:440px; border-left:1px solid #CCC; border-right:1px solid #CCC">
                    <?php
                    if (is_numeric($id) || is_numeric($cliente) || is_numeric($venda)) {
                        if (is_numeric($venda)) {
                            $where = "";
                            if (is_numeric($id)) {
                                $where = " and id_produtomp = " . $id;
                            }
                            $cur = odbc_exec($con, "select id_prodmp,id_materiaprima,p1.descricao d1,p2.descricao d2,p2.conta_movimento, (select sf_vendas.data_venda from sf_vendas where id_venda = a.id_venda) data_venda, id_item_pacote  
                            from sf_vendas_itens_pacotes p 
                            inner join sf_produtos_materiaprima mp on mp.id_prodmp = p.produto_usar
                            inner join sf_produtos p1 on mp.id_produtomp = p1.conta_produto 
                            inner join sf_produtos p2 on mp.id_materiaprima = p2.conta_produto
                            inner join sf_vendas_itens vi on p.venda_item = vi.id_item_venda
                                 and isnull((select COUNT(*) from sf_vendas_itens_pacotes where venda_item = vi.id_item_venda and venda_aplicada is null),0) > 0
                            inner join sf_vendas v on vi.id_venda = v.id_venda
                            left join sf_vendas_itens a on p.venda_aplicada = a.id_item_venda
                            where v.cliente_venda = " . $cliente . " and v.id_venda = " . $venda . $where . " and v.status <> 'Reprovado' order by d1,d2");
                        } elseif (is_numeric($id)) {
                            $cur = odbc_exec($con, "select id_prodmp,id_materiaprima,p1.descricao d1,p2.descricao d2,p2.conta_movimento
                            from sf_produtos_materiaprima 
                            inner join sf_produtos p1 on id_produtomp = p1.conta_produto 
                            inner join sf_produtos p2 on id_materiaprima = p2.conta_produto 
                            where id_produtomp = " . $id . " order by d1,d2");
                        } elseif (is_numeric($cliente)) {
                            $cur = odbc_exec($con, "select id_prodmp,id_materiaprima,p1.descricao d1,p2.descricao d2,p2.conta_movimento, (select sf_vendas.data_venda from sf_vendas where id_venda = a.id_venda) data_venda, id_item_pacote  
                            from sf_vendas_itens_pacotes p 
                            inner join sf_produtos_materiaprima mp on mp.id_prodmp = p.produto_usar
                            inner join sf_produtos p1 on mp.id_produtomp = p1.conta_produto 
                            inner join sf_produtos p2 on mp.id_materiaprima = p2.conta_produto
                            inner join sf_vendas_itens vi on p.venda_item = vi.id_item_venda
                                 and isnull((select COUNT(*) from sf_vendas_itens_pacotes where venda_item = vi.id_item_venda and venda_aplicada is null),0) > 0
                            inner join sf_vendas v on vi.id_venda = v.id_venda
                            left join sf_vendas_itens a on p.venda_aplicada = a.id_item_venda
                            where v.cliente_venda = " . $cliente . " and v.status <> 'Reprovado' order by d1,d2");
                        }
                        while ($RFP = odbc_fetch_array($cur)) {
                            if (is_numeric($venda) || is_numeric($cliente)) {
                                $idItem = $RFP["id_item_pacote"];
                            } else {
                                $idItem = $RFP["id_prodmp"];
                            }
                            ?>                            
                            <div style="background:#EEE; font-size:12px; text-align:left; height:40px; line-height:40px; border-bottom:1px solid #FFF">
                                <div style="float:right; width:44px; line-height:40px">                                    
                                    <button id="btn<?php echo utf8_encode($idItem); ?>" type="button" class="keypad-special" style="float:left; width:42px; height:38px; margin-right:0;<?php
                                    if ($RFP['data_venda'] != "") {
                                        echo "background-color:#385b15;color: #FFF";
                                    } else {
                                        echo "background-color:#68AF27;color: #68AF27";
                                    }
                                    ?>" <?php if ($RFP['data_venda'] == "") { ?>onclick="seleciona(<?php echo utf8_encode($idItem); ?>)"<?php } ?> >
                                        <div style="margin-top:5px"><span class="ico-ok" style="font-size:22px;"></span></div>
                                        <?php if ($RFP['data_venda'] == "") { ?>
                                            <input id="<?php echo utf8_encode($idItem); ?>" style="margin-top: -20px;display: none;" type="checkbox" name="ckb_item" value="<?php echo utf8_encode($RFP["id_prodmp"]) . "|" . utf8_encode($RFP["id_materiaprima"]) . "|" . utf8_encode($RFP["conta_movimento"]) . "|" . utf8_encode($RFP["d2"]) . "|" . utf8_encode($RFP["id_item_pacote"]) . ""; ?>"/>
                                        <?php } ?>
                                    </button>                                    
                                </div>
                                <div style="float:left; color:#333; font-size:14px; width:calc(30% - 20px); padding-left:10px"><?php echo utf8_encode($RFP["d1"]); ?></b></div>
                                <div style="float:left; color:#333; font-size:14px; width:calc(30% - 20px)"><b>&nbsp;<?php echo utf8_encode($RFP["d2"]); ?></b></div>
                                <div style="float:left; color:#333; font-size:14px; width:calc(40% - 15px)"><b>&nbsp;<?php echo escreverData($RFP['data_venda']); ?></b></div>
                            </div>
                    <?php
                    }
                }?>    
                </div>
                <div style="clear:both"></div>
                <div class="label" style="line-height:40px">&nbsp;
                    <button type="button" class="keypad-special" style="float:left; width:42px; height:38px; color:#FFF; margin-right:0; background-color:#68AF27" onclick="salvarItens()">
                        <div class="icon" style="margin-top:5px"><span class="ico-ok" style="font-size:22px"></span></div>
                    </button>
                    <button type="button" class="keypad-special" style="float:left; width:42px; height:38px; color:#FFF; background-color:#D53F26" onClick="listaItensPacote(0)">
                        <div class="icon" style="margin-top:5px"><span class="ico-cancel" style="font-size:22px"></span></div>
                    </button>
                    <button id="user_down" type="button" class="keypad-special" style="float:right; width:42px; height:38px; color:#FFF; background-color:#009AD7">
                        <div class="icon" style="margin-top:5px"><span class="ico-arrow-down" style="font-size:22px"></span></div>
                    </button>
                    <button id="user_up" type="button" class="keypad-special" style="float:right; width:42px; height:38px; color:#FFF; margin-right:0; background-color:#009AD7">
                        <div class="icon" style="margin-top:5px"><span class="ico-arrow-up" style="font-size:22px"></span></div>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="./../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript">
        function seleciona(id) {
            if ($("#" + id).is(":checked")) {
                $("#btn" + id).css("color", "#68AF27");
                $("#" + id).prop('checked', false);
            } else {
                $("#btn" + id).css("color", "#FFF");
                $("#" + id).prop('checked', true);
            }
        }
        function salvarItens() {
            $.each($("input[name='ckb_item']:checked"), function () {
                var arr = $(this).val().split('|');
                adicionaItemPacote(arr[0], arr[1], arr[2], arr[3], arr[4]);
            });
        }
        $("#user_up").on("mousedown", function () {
            interval = setInterval(function () {
                var pos = $("#lista_user").scrollTop();
                $("#lista_user").scrollTop(pos - 2);
            }, 1);
        }).click().on("mouseup", function () {
            clearInterval(interval);
        });
        $("#user_down").on("mousedown", function () {
            interval = setInterval(function () {
                var pos = $("#lista_user").scrollTop();
                $("#lista_user").scrollTop(pos + 2);
            }, 1);
        }).click().on("mouseup", function () {
            clearInterval(interval);
        });
        $("#cont_up").on("mousedown", function () {
            interval = setInterval(function () {
                var pos = $("#lista_cont").scrollTop();
                $("#lista_cont").scrollTop(pos - 2);
            }, 1);
        }).click().on("mouseup", function () {
            clearInterval(interval);
        });
        $("#cont_down").on("mousedown", function () {
            interval = setInterval(function () {
                var pos = $("#lista_cont").scrollTop();
                $("#lista_cont").scrollTop(pos + 2);
            }, 1);
        }).click().on("mouseup", function () {
            clearInterval(interval);
        });
    </script>
    <?php odbc_close($con);?>
</div>