<?php include "./../../Connections/configini.php"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Ponto de Venda</title>
        <link href="./../../css/icons.css" rel="stylesheet" type="text/css"/>
        <link href="./../../css/Tela-de-Venda.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="./../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body style="overflow:hidden" onClick="toggleFullScreen()">
        <input id="txtMnContrato" type="hidden" value="<?php echo $contrato; ?>" />        
        <input id="txtMnFilial" type="hidden" value="<?php echo $filial; ?>" />
        <input id="txtMnLanguage" type="hidden" value="<?php echo $languages; ?>" />        
        <table id="division" width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <td class="headbar"></td>
            </tr>
            <tr>
                <td class="working">
                    <div class="contents" style="overflow-y:hidden">
                        <div class="title">ATENDIMENTOS</div>
                        <div class="updown">
                            <table width="100%" cellspacing="5" cellpadding="0">
                            </table>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="workbar"></td>
            </tr>
        </table>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script>
            
            setInterval("newWork()", 10000);
            
            function setPropert(id, btn) {
                if (btn === 1) {
                    if (confirm('Confirma o cancelamento do pedido?')) {
                        $.getJSON("producao.ajax.php", {txtId: id, txtBtn: btn, ajax: "true"}, function (j) {
                            $('#' + id).remove();
                        });
                    }
                } else {
                    $.getJSON("producao.ajax.php", {txtId: id, txtBtn: btn, ajax: "true"}, function (j) {
                        $('#' + id).remove();
                    });
                }
            }
            
            function resizeAll() {
                $(".working > .contents").css("height", "100%");
                $(".working > .contents > .updown").css("height", $(window).height() - 113);
                $(".working > .contents > .updown").css("max-height", $(window).height() - 113);
            }
            
            $(window).on("resize", function () {
                resizeAll();
            });
            
            $(window).bind("resize", function () {
                resizeAll();
            });
            
            $(document).ready(function () {                
                resizeAll();
                var count;
                var interval;
                $("#rec_up").on("mouseover", function () {
                    interval = setInterval(function () {
                        count = count || 1;
                        var pos = $(".working > .contents").scrollTop();
                        $(".working > .contents").scrollTop(pos - count);
                    }, 1);
                }).click(function () {
                    if (count < 6) {
                        count = count + 1;
                    }
                }).on("mouseout", function () {
                    clearInterval(interval);
                });
                $("#rec_down").on("mouseover", function () {
                    interval = setInterval(function () {
                        count = count || 1;
                        var pos = $(".working > .contents").scrollTop();
                        $(".working > .contents").scrollTop(pos + count);
                    }, 1);
                }).click(function () {
                    if (count < 6) {
                        count = count + 1;
                    }
                }).on("mouseout", function () {
                    clearInterval(interval);
                });
            });
            
            function toggleFullScreen() {
                if ((document.fullScreenElement && document.fullScreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
                    if (document.documentElement.requestFullScreen) {
                        document.documentElement.requestFullScreen();
                    } else if (document.documentElement.mozRequestFullScreen) {
                        document.documentElement.mozRequestFullScreen();
                    } else if (document.documentElement.webkitRequestFullScreen) {
                        document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
                    }
                } else {
                    if (document.cancelFullScreen) {
                        document.cancelFullScreen();
                    } else if (document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                    } else if (document.webkitCancelFullScreen) {
                        document.webkitCancelFullScreen();
                    }
                }
            }
            
            function newWork() {
                $.getJSON("producao_load.ajax.php?prod=0", {ajax: "true"}, function (j) {
                    var prod = "";
                    for (var i = 0; i < j.length; i++) {
                        prod = prod + "<tr id=" + j[i].id_venda + ">\n";
                        prod = prod + "	<td class=\"workpwd\">" + j[i].cod_pedido + "</td>\n";
                        prod = prod + "	<td class=\"worktxt\">" + j[i].des_pedido + "</td>\n";
                        prod = prod + "	<td style=\"width:131px\">\n";
                        prod = prod + "		<div class=\"button\" style=\"background-color:#68AF27\" onclick=\"setPropert(" + j[i].id_venda + ",0)\">\n";
                        prod = prod + "			<div class=\"icon\"><span class=\"ico-ok\"></span></div>\n";
                        prod = prod + "		</div>\n";
                        prod = prod + "		<div class=\"button\" style=\"background-color:#D53F26\" onclick=\"setPropert(" + j[i].id_venda + ",1)\">\n";
                        prod = prod + "			<div class=\"icon\"><span class=\"ico-cancel\"></span></div>\n";
                        prod = prod + "		</div>\n";
                        prod = prod + "	</td>\n";
                        prod = prod + "</tr>\n";
                    }
                    $(".working > .contents > .updown > table").html("");
                    $(".working > .contents > .updown > table").html(prod);
                });
            }
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>