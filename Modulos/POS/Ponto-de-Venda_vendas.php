<?php include "./../../Connections/configini.php"; ?>
<div id="last_win">
    <div style="width:100%; height:100%; position:absolute; top:0; left:0; z-index:9"><img src="./../../img/bg_preto.png" style="width:100%; height:100%"></div>
    <div class="keypad-popup" style="width:338px; height:308px; position:absolute; top:50%; left:50%; margin-top:-154px; margin-left:-169px; display:block">
        <div class="keypad-row" style="width:338px; height:250px; margin:0px; text-align:center">
            <div class="trans_venda" style="width:100%">
                <div class="label">
                    <div style="float:left;background: #333;width:25%;"><b>DATA</b></div>
                    <div style="float:left;background: #333;width:23%; "><b>VALOR</b></div>
                    <div style="float:left;background: #333;width:28%; "><b>SENHA</b></div>
                    <div style="float:left;background: #333;width:24%;"><b>AÇÃO</b></div>
                </div>
                <div id="lista_comanda" style="overflow-y:hidden; width:calc(100% - 2px); height:246px; border-left:1px solid #CCC; border-right:1px solid #CCC"></div>
                <div style="clear:both;height: 7px;"></div>
                <div class="label" style="line-height:40px">&nbsp;
                    <button type="button" class="keypad-special" style="float:left; width:42px; height:38px; color:#FFF; background-color:#D53F26" onClick="listaVendas()">
                        <div class="icon" style="margin-top:5px"><span class="ico-cancel" style="font-size:22px"></span></div>
                    </button>
                    <button id="user_down" type="button" class="keypad-special" style="float:right; width:42px; height:38px; color:#FFF; background-color:#009AD7">
                        <div class="icon" style="margin-top:5px"><span class="ico-arrow-down" style="font-size:22px"></span></div>
                    </button>
                    <button id="user_up" type="button" class="keypad-special" style="float:right; width:42px; height:38px; color:#FFF; margin-right:0; background-color:#009AD7">
                        <div class="icon" style="margin-top:5px"><span class="ico-arrow-up" style="font-size:22px"></span></div>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php odbc_close($con);?>
<script type="text/javascript">
    $("#user_up").on("mousedown", function () {
        interval = setInterval(function () {
            var pos = $("#lista_comanda").scrollTop();
            $("#lista_comanda").scrollTop(pos - 2);
        }, 1);
    }).click().on("mouseup", function () {
        clearInterval(interval);
    });
    $("#user_down").on("mousedown", function () {
        interval = setInterval(function () {
            var pos = $("#lista_comanda").scrollTop();
            $("#lista_comanda").scrollTop(pos + 2);
        }, 1);
    }).click().on("mouseup", function () {
        clearInterval(interval);
    });

    function btnImprimir(code) {
        $("#txtChave").val(code);
        $("#txtModelo").val("65");
        imprimir_nota('');
    }

    jQuery(document).ready(function () {
        var novaBusca = "";
        $.getJSON("Ponto-de-Venda_controller.php", {listaUltimasVendas: 'S', filial: $("#txtFilial").val()}, function (j) {
            for (var i = 0; i < j.length; i++) {
                var imprimir = "href='cupom.php?id=" + j[i].id_venda + "' target='_blank'";
                if (j[i].id_venda_nfe !== "0" && j[i].code !== "") {
                    imprimir = "onclick='btnImprimir(\"" + j[i].code + "\");'";
                }
                novaBusca = "<div style=\"background:#EEE; color:#999; font-size:12px; text-align:left; height:40px; line-height:40px; border-bottom:1px solid #FFF\">" +
                "<div style=\"float:left; color:#333; font-size:14px; width:25%; padding-left:10px;cursor: pointer;\"><a style='color:#333' " + imprimir + " >" + j[i].data_venda + "</a></div>" +
                "<div style=\"float:left; color:#333; font-size:14px; width:23%; padding-left:10px\">" + j[i].valor + "</div>" +
                "<div style=\"float:left; color:#333; font-size:14px; width:28%; padding-left:10px\">&nbsp;" + j[i].cod_pedido + "</div>";
                if (j[i].id_venda_nfe === "0") {
                    novaBusca = novaBusca + " <div style=\"float:left; color:#333; font-size:14px; padding-left:10px\"><button onclick=\"cancelaNota('" + j[i].id_venda + "')\" type=\"button\" class=\"keypad-special\" style=\"width:38px; height:38px; color:#FFF; background-color:#D53F26\"><div class=\"icon\"><span class=\"ico-cancel\"></span></div></button></div>";
                }
                novaBusca = novaBusca + "</div>";
                $("#lista_comanda").append(novaBusca);
            }
        });
    });
</script>

