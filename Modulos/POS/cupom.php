<?php

include "./../../Connections/configini.php";

$PegaURL = "";
$frete_total = 0;
$comentario = "";
$clinte = 0;

if (is_numeric($_GET["id"])) {
    $PegaURL = $_GET["id"];
}

if (is_numeric($PegaURL)) {    
    $cur = odbc_exec($con, "select vendedor, id_venda,data_venda,descontop,descontotp,
    isnull(cod_pedido,0) cod_pedido,empresa,cliente_venda,comentarios_venda
    from sf_vendas where id_venda = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP["id_venda"];
        $data_venda = escreverData($RFP["data_venda"]) . " " . date("H:i");
        $descontop = $RFP["descontop"];
        $descontotp = $RFP["descontotp"];
        $cod_pedido = $RFP["cod_pedido"];
        $operador = utf8_encode($RFP["vendedor"]);
        $empresa = utf8_encode($RFP["empresa"]);
        $clinte = utf8_encode($RFP['cliente_venda']);
        $comentario = utf8_encode($RFP['comentarios_venda']);        
    }
    if (is_numeric($empresa)) {        
        $cur = odbc_exec($con, "select * from sf_filiais where numero_filial = " . $empresa);
        while ($RFP = odbc_fetch_array($cur)) {            
            $cedente = utf8_encode($RFP["razao_social_contrato"]);
            $nome_fantasia = utf8_encode($RFP["nome_fantasia_contrato"]);            
            $cpf_cnpj = utf8_encode($RFP["cnpj"]);                
            $inscricao = utf8_encode($RFP["inscricao_estadual"]);
            $endereco = utf8_encode($RFP['endereco'])." n°".utf8_encode($RFP['numero']).",".utf8_encode($RFP['bairro']);
            $cidade_uf = utf8_encode($RFP['cidade_nome'])." ".utf8_encode($RFP['estado']);            
            $cep = utf8_encode($RFP['cep']);      
            $telefone = utf8_encode($RFP['telefone']);
            $site = utf8_encode($RFP['site']);
        }
    } 
}

?>      
<html> 
    <head>
        <title>SIVIS - Cupom Fiscal</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="./../../css/cupom.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="./../../js/plugins/jquery/jquery-1.9.1.min.js"></script>        
    </head>
    <body>
        <input id="txtMnLanguage" type="hidden" value="<?php echo $languages; ?>" />        
        <table cellspacing="0" cellpadding="0">            
            <tr>
                <td colspan="6" class="tit"><?php echo $nome_fantasia; ?></td>
            </tr>       
            <tr>
                <td colspan="6" class="end"><?php echo $cedente; ?><br/><?php echo $endereco; ?><br/><?php echo $cidade_uf . " - CEP: " . $cep; ?></td>
            </tr>
            <tr>
                <td colspan="6" class="doc"><b>CNPJ: <?php echo $cpf_cnpj; ?></b><br/>I.E.: <?php echo $inscricao; ?></td>
            </tr>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <tr>
                <td colspan="3" class="doc"><b><?php echo $data_venda; ?></b></td>
                <td colspan="3" class="doc" style="text-align:right"><b>COD: <?php echo str_pad($id, 6, "0", STR_PAD_LEFT); ?></b></td>
            </tr>
            <tr>
                <td colspan="6" class="tit">CUPOM NÃO FISCAL</td>
            </tr>
            <tr>
                <td>ITEM</td>
                <td style="text-align:center">CÓDIGO</td>
                <td>DESCRIÇÃO</td>
                <td style="text-align:center">QTD</td>
                <td style="text-align:right">VALOR</td>
                <td style="text-align:right">TOTAL</td>
            </tr>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <?php
            if ($PegaURL != "") {
                $loop = 1;
                $sub_total = 0;
                $cur = odbc_exec($con, "select id_item_venda,cm.conta_movimento grupo,gc.descricao grupodesc,conta_produto produto,cm.descricao produtodesc,
                quantidade,valor_total,descontop,descontos,descontotp,descontots,comentarios_venda,cliente_venda from sf_vendas_itens vi left join sf_contas_movimento gc on vi.grupo = gc.id_contas_movimento inner join sf_produtos cm 
                on cm.conta_produto = vi.produto inner join sf_vendas v on v.id_venda = vi.id_venda where cm.tipo = 'P' and vi.id_venda = " . $PegaURL);
                $titulo = "";
                while ($RFP = odbc_fetch_array($cur)) {
                    if ($titulo !== "S") {
                        ?>
                        <tr>
                            <td colspan="6" class="grupo"> PRODUTO </td>
                        </tr>
                        <?php
                    }
                    $titulo = "S";
                    $valor_unitario = $RFP["valor_total"] / $RFP["quantidade"];
                    $sub_total = $sub_total + $RFP["valor_total"];
                    $comentario = utf8_encode($RFP['comentarios_venda']);
                    $clinte = utf8_encode($RFP['cliente_venda']);
                    if ($RFP["descontotp"] == 0) {
                        $desc_valor = $sub_total * ($RFP["descontop"] / 100);
                        $desconto = "%";
                    } else {
                        $desc_valor = $RFP["descontop"];
                        $desconto = "$";
                    }
                    ?>
                    <tr>
                        <td style="vertical-align: top;vertical-align: top;"><?php echo str_pad($loop, 3, "0", STR_PAD_LEFT); ?></td>
                        <td style="text-align:center;vertical-align: top;"><?php
                            echo str_pad($RFP["produto"], 6, "0", STR_PAD_LEFT);
                            ?></td>
                        <td style="vertical-align: top;"><?php echo utf8_encode($RFP["grupodesc"]) . "-" . utf8_encode($RFP["produtodesc"]); ?></td>
                        <td style="text-align:center;vertical-align: top;"><?php echo $RFP["quantidade"]; ?> X</td>
                        <td style="text-align:right;vertical-align: top;"><?php echo escreverNumero($valor_unitario); ?></td>
                        <td style="text-align:right;vertical-align: top;"><?php echo escreverNumero($RFP["valor_total"]); ?></td>
                    </tr>
                    <?php
                    $loop++;
                }
                $cur = odbc_exec($con, "select id_item_venda,cm.conta_movimento grupo,gc.descricao grupodesc,conta_produto produto,cm.descricao produtodesc,
                quantidade,valor_total,descontop,descontos,descontotp,descontots,comentarios_venda,cliente_venda from sf_vendas_itens vi left join sf_contas_movimento gc on vi.grupo = gc.id_contas_movimento inner join sf_produtos cm 
                on cm.conta_produto = vi.produto inner join sf_vendas v on v.id_venda = vi.id_venda where cm.tipo = 'S' and vi.id_venda = " . $PegaURL);
                $titulo = "";
                while ($RFP = odbc_fetch_array($cur)) {
                    if ($titulo !== "S") {
                        ?>
                        <tr>
                            <td colspan="6" class="grupo"> SERVIÇO </td>
                        </tr>
                        <?php
                    }
                    $titulo = "S";

                    $valor_unitario = $RFP["valor_total"] / $RFP["quantidade"];
                    $sub_total = $sub_total + $RFP["valor_total"];
                    $comentario = utf8_encode($RFP['comentarios_venda']);
                    $clinte = utf8_encode($RFP['cliente_venda']);
                    if ($RFP["descontotp"] == 0) {
                        $desc_valor = $sub_total * ($RFP["descontop"] / 100);
                        $desconto = "%";
                    } else {
                        $desc_valor = $RFP["descontop"];
                        $desconto = "$";
                    }
                    ?>
                    <tr>
                        <td style="vertical-align: top;vertical-align: top;"><?php echo str_pad($loop, 3, "0", STR_PAD_LEFT); ?></td>
                        <td style="text-align:center;vertical-align: top;"><?php
                            echo str_pad($RFP["produto"], 6, "0", STR_PAD_LEFT);
                            ?></td>
                        <td style="vertical-align: top;"><?php echo utf8_encode($RFP["grupodesc"]) . "-" . utf8_encode($RFP["produtodesc"]); ?></td>
                        <td style="text-align:center;vertical-align: top;"><?php echo $RFP["quantidade"]; ?> X</td>
                        <td style="text-align:right;vertical-align: top;"><?php echo escreverNumero($valor_unitario); ?></td>
                        <td style="text-align:right;vertical-align: top;"><?php echo escreverNumero($RFP["valor_total"]); ?></td>
                    </tr>
                    <?php
                    $loop++;
                }
            }
            ?>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <tr>
                <td colspan="4" class="doc"><b>Parcelamento:</b></td>
                <td colspan="2" class="doc" style="text-align:right"><b></b></td>
            </tr>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <tr>
                <td colspan="2" class="doc"><b>Data:</b></td>
                <td colspan="2" class="doc"><b>Tipo:</b></td>
                <td colspan="2" class="doc"><b>Valor:</b></td>                
            </tr>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <?php
            $sql = "select * from sf_venda_parcelas v left join sf_tipo_documento t on v.tipo_documento = t.id_tipo_documento where v.inativo = 0 and venda = " . $PegaURL;
            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                $valorPagoDinheiro = 0;
                if (is_numeric($RFP["bol_valor"])) {
                    $valorPagoDinheiro = $RFP["bol_valor"];
                }
                ?>
                <tr>
                    <td colspan="2" class="doc"><?php echo escreverData($RFP["data_parcela"]); ?></td>
                    <td colspan="2" class="doc"><?php echo utf8_encode($RFP["descricao"]); ?></td>
                    <td colspan="2" class="doc"><?php echo escreverNumero($RFP["valor_parcela"], 1); ?></td>                
                </tr>            
            <?php } ?>            
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <?php
            $cur = odbc_exec($con, "select razao_social,(select sum(valor_parcela) 
            from sf_solicitacao_autorizacao_parcelas where solicitacao_autorizacao = frete_solicitacao_autorizacao) parcelas 
            from sf_frete_historico inner join sf_fornecedores_despesas on id_fornecedores_despesas = frete_transportadora
            where tipo = 'T' and frete_venda = " . $PegaURL . "");
            while ($RFP = odbc_fetch_array($cur)) { ?>
                <tr>
                    <td colspan="4" class="doc"><b>Tx. Entrega:</b></td>
                    <td colspan="2" class="doc" style="text-align:right"><b></b></td>
                </tr>  
                <tr>
                    <td colspan="6" class="lin"></td>
                </tr>                
                <tr>
                    <td colspan="4"><?php echo utf8_encode($RFP['razao_social']); ?></td>
                    <td colspan="2"><?php
                        $frete_total = $RFP['parcelas'];
                        echo escreverNumero($RFP['parcelas'], 1);
                        ?></td>                    
                </tr>                    
            <?php } ?>            
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <tr>
                <td colspan="4" class="doc"><b>SUB TOTAL:</b></td>
                <td colspan="2" class="doc" style="text-align:right"><b><?php echo escreverNumero($sub_total); ?></b></td>
            </tr>
            <tr>
                <td colspan="4" class="doc"><b>DESCONTO:<?php
                        if ($desconto != "") {
                            echo " (" . $desconto . ")";
                        }
                        ?></b></td>
                <td colspan="2" class="doc" style="text-align:right"><b><?php echo escreverNumero($desc_valor) ?></b></td>
            </tr>
            <tr>
                <td colspan="4" class="doc"><b>TX. ENTREGA:</b></td>
                <td colspan="2" class="doc" style="text-align:right"><b><?php echo escreverNumero($frete_total) ?></b></td>
            </tr>
            <tr>
                <td colspan="4" class="doc"><b>TOTAL:</b></td>
                <td colspan="2" class="doc" style="text-align:right"><b><?php echo escreverNumero(($sub_total - $desc_valor) + $frete_total); ?></b></td>
            </tr>  
            <?php if ($valorPagoDinheiro > 0) { ?>
                <tr>
                    <td colspan="4" class="doc"><b>PAGO:</b></td>
                    <td colspan="2" class="doc" style="text-align:right"><b><?php echo escreverNumero($valorPagoDinheiro); ?></b></td>
                </tr>  
                <tr>
                    <td colspan="4" class="doc"><b>TROCO:</b></td>
                    <td colspan="2" class="doc" style="text-align:right"><b><?php echo escreverNumero($valorPagoDinheiro - (($sub_total - $desc_valor) + $frete_total)); ?></b></td>
                </tr>  
            <?php } ?>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>     
            <?php if ($comentario != "") { ?>
                <tr>
                    <td colspan="4" class="doc"><b>Comentários:</b></td>
                    <td colspan="2" class="doc" style="text-align:right"><b></b></td>
                </tr>  
                <tr>
                    <td colspan="6" class="lin"></td>
                </tr>                
                <tr>
                    <td colspan="6"><?php echo $comentario; ?></td>
                </tr>
            <?php } if (is_numeric($clinte) && $clinte > 0) { ?>
                <tr>
                    <td colspan="6" class="lin"></td>
                </tr> 
                <tr>
                    <td colspan="4" class="doc"><b>Cliente:</b></td>
                    <td colspan="2" class="doc" style="text-align:right"><b></b></td>
                </tr>  
                <tr>
                    <td colspan="6">
                        <?php
                        $cur = odbc_exec($con, "select razao_social,nome_fantasia,endereco,numero,complemento,bairro,tb_cidades.cidade_nome,tb_estados.estado_sigla,cep 
                        from sf_fornecedores_despesas left join tb_cidades on tb_cidades.cidade_codigo = cidade left join tb_estados on tb_estados.estado_codigo = estado
                        where id_fornecedores_despesas = " . $clinte . "");
                        while ($RFP = odbc_fetch_array($cur)) {
                            echo utf8_encode($RFP['razao_social']);
                            if ($RFP['nome_fantasia'] != "") {
                                echo "(" . utf8_encode($RFP['nome_fantasia']) . ")";
                            }
                            echo "<br>" . utf8_encode($RFP['endereco']) . ",N° " . utf8_encode($RFP['numero']) . " " . utf8_encode($RFP['complemento']) . "<br>";
                            echo utf8_encode($RFP['bairro']) . " " . utf8_encode($RFP['cidade_nome']) . "-" . utf8_encode($RFP['estado_sigla']) . " " . utf8_encode($RFP['cep']);
                        }
                        ?>                                                                     
                    </td>
                </tr>
            <?php } ?>
            <tr>
                <td colspan="6" class="lin"></td>
            </tr>
            <?php if ($cod_pedido !== "") { ?>
                <tr>
                    <td colspan="6" class="tit">SENHA: <?php echo $cod_pedido; ?></td>
                </tr>
            <?php } ?>
        </table>
    </body>
    <script type="text/javascript" src="./../../js/util.js"></script>    
    <script type="text/javascript">
        $(window).load(function () {
            window.print();
            <?php if ($cod_pedido !== "") { ?>
                setTimeout(function () {
                    window.print();
                }, 100);
            <?php } ?>
            setTimeout(function () {
                window.close();
            }, 500);
        });
    </script>    
    <?php odbc_close($con);?>
</html>