<?php

include "./../../Connections/configini.php";

$aColumns = array('descricao', 'valor');
if (strlen($_REQUEST['txtVendedor']) > 0) {
    $funcionario = " UPPER(vendedor) = UPPER('" . str_replace("'", "", $_REQUEST['txtVendedor']) . "') and ";
}
$where = "";
if (is_numeric($_REQUEST['filial'])) {
    $where = ' and empresa = ' . $_REQUEST['filial'];
}
$local = array();

function filtrosData() {
    $explode = valoresData2($_REQUEST['txtData']);
    if ($explode == "null") {
        return getData("T");
    } else {
        return str_replace("_", "/", $_REQUEST['txtData']);
    }
}

$sQuery1 = "set dateformat dmy;select td.descricao descricao,sum(vp.valor_parcela) valor from sf_venda_parcelas vp 
            inner join sf_vendas v on vp.venda = v.id_venda
            inner join sf_tipo_documento td on td.id_tipo_documento = vp.tipo_documento
            where " . $funcionario . " data_venda between '" . filtrosData() . " 00:00:00' and '" . filtrosData() . " 23:59:59' 
            and status = 'Aprovado' " . $where . " group by td.descricao order by 1";
$cur = odbc_exec($con, $sQuery1);

while ($aRow = odbc_fetch_array($cur)) {
    $local[] = array('descricao' => utf8_encode($aRow[$aColumns[0]]), 'valor' => utf8_encode($aRow[$aColumns[1]]));
}
echo(json_encode($local));
odbc_close($con);
