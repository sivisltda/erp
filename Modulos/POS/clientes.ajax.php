<?php

include "./../../Connections/configini.php";

$busca = str_replace("'", "", utf8_decode($_REQUEST['txtBusca']));
$local = array();
$sql = "select id_fornecedores_despesas,razao_social,nome_fantasia,endereco,numero,complemento,bairro,cidade_nome,estado_sigla,tipo,
(select max(conteudo_contato) from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas = id_fornecedores_despesas) telefone,
(select COUNT(*) from sf_vendas_itens_pacotes p inner join sf_vendas_itens v on p.venda_item = v.id_item_venda inner join sf_vendas on sf_vendas.id_venda = v.id_venda where p.venda_aplicada is null and sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas and sf_vendas.status <> 'Reprovado') pacotes
from sf_fornecedores_despesas left join tb_estados on estado_codigo = estado left join tb_cidades on cidade_codigo = cidade
where tipo in ('C','E','P') and (razao_social like '%" . $busca . "%' or nome_fantasia like '%" . $busca . "%' or id_fornecedores_despesas in (select fornecedores_despesas
from sf_fornecedores_despesas_contatos where tipo_contato = 0 and conteudo_contato like '%" . $busca . "%')) order by razao_social ";

$cur = odbc_exec($con, $sql) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $nome_cliente = utf8_encode($RFP['razao_social']);
    if ($RFP['nome_fantasia'] != "") {
        $nome_cliente .= " (" . utf8_encode($RFP['nome_fantasia']) . ")";
    }
    if ($RFP['tipo'] != "C") {
        $nome_cliente .= " (" . ($RFP['tipo'] == "P" ? "Prospect" : "Funcionário") . ")";
    }
    $endereco_cliente = utf8_encode($RFP['endereco']) . " " . utf8_encode($RFP['numero']) . " " . utf8_encode($RFP['complemento']) . " " . utf8_encode($RFP['bairro']) . " " . utf8_encode($RFP['cidade_nome']) . " " . utf8_encode($RFP['estado_sigla']);
    $local[] = array('cliente_id' => $RFP['id_fornecedores_despesas'], 'cliente_nome' => $nome_cliente, 'cliente_telefone' => utf8_encode($RFP['telefone']), 'cliente_endereco' => $endereco_cliente, 'pacotes' => utf8_encode($RFP['pacotes']));
}
echo(json_encode($local));
odbc_close($con);
