<?php
include "./../../Connections/configini.php";

$query = "select numero_filial from dbo.sf_usuarios A left join sf_filiais B on A.filial = B.id_filial where login_user = '" . $_SESSION["login_usuario"] . "'";
$cur = odbc_exec($con, $query) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $empresa = $RFP['numero_filial'];
}

if (is_numeric($empresa)) {
    $cur = odbc_exec($con, "select * from sf_filiais where numero_filial = " . $empresa);
    while ($RFP = odbc_fetch_array($cur)) {
        $cedente = utf8_encode($RFP["razao_social_contrato"]);
        $nome_fantasia = utf8_encode($RFP["nome_fantasia_contrato"]);
        $cpf_cnpj = utf8_encode($RFP["cnpj"]);
        $inscricao = utf8_encode($RFP["inscricao_estadual"]);
        $endereco = utf8_encode($RFP['endereco']) . " n°" . utf8_encode($RFP['numero']) . "," . utf8_encode($RFP['bairro']);
        $cidade_uf = utf8_encode($RFP['cidade_nome']) . " " . utf8_encode($RFP['estado']);
        $cep = utf8_encode($RFP['cep']);
        $telefone = utf8_encode($RFP['telefone']);
        $site = utf8_encode($RFP['site']);
    }
}

function filtrosData() {
    $explode = valoresData2(str_replace("_", "/", $_GET['dt']));
    if ($explode == "null") {
        return str_replace("_", "/", $_GET['dt']);
    } else {
        return getData("T");
    }
}
?>      
<html>
    <head>
        <title>SIVIS - Resumo do Caixa</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="./../../css/cupom.css" rel="stylesheet" type="text/css"/>   
        <script type="text/javascript" src="./../../js/plugins/jquery/jquery-1.9.1.min.js"></script>        
    </head>
    <body>
        <input id="txtMnLanguage" type="hidden" value="<?php echo $languages; ?>" />        
        <table cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="2" class="tit"><?php echo $cedente; ?></td>
            </tr>
            <tr>
                <td colspan="2" class="end"><?php echo $endereco; ?><br/><?php echo $cidade_uf . " - CEP: " . $cep; ?></td>
            </tr>
            <tr>
                <td colspan="2" class="doc"><b>CNPJ: <?php echo $cpf_cnpj; ?></b><br/>I.E.: <?php echo $inscricao; ?></td>
            </tr>
            <tr>
                <td colspan="2" class="lin"></td>
            </tr>
            <tr>
                <td colspan="1" class="doc"><b><?php echo filtrosData() . date(" H:i"); ?></b></td>
                <td colspan="1" class="doc" style="text-align:right"><b>Funcionário: <?php
                        if (strlen($_GET['user']) > 0) {
                            echo $_GET['user'];
                        } else {
                            echo "TODOS";
                        }
                        ?></b></td>
            </tr>
            <tr>
                <td colspan="2" class="tit">RESUMO DO CAIXA</td>
            </tr>
            <tr>
                <td>DESCRIÇÃO</td>
                <td style="text-align:right">VALOR</td>
            </tr>
            <tr>
                <td colspan="2" class="lin"></td>
            </tr>
            <tr>
                <td colspan="2" class="spc"></td>
            </tr>
            <?php
            $funcionario = "";
            if (strlen($_GET['user']) > 0) {
                $funcionario = " UPPER(vendedor) = UPPER('" . $_GET['user'] . "') and ";
            }
            $query = "set dateformat dmy;select td.descricao descricao,sum(vp.valor_pago) valor from sf_venda_parcelas vp 
                inner join sf_vendas v on vp.venda = v.id_venda
                inner join sf_tipo_documento td on td.id_tipo_documento = vp.tipo_documento
                where " . $funcionario . " data_venda between '" . filtrosData() . " 00:00:00' and '" . filtrosData() . " 23:59:59' and valor_pago > 0 and status = 'Aprovado' group by td.descricao order by 1";
            $cur = odbc_exec($con, $query) or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                ?>
                <tr>
                    <td><?php echo utf8_encode($RFP['descricao']); ?></td>
                    <td style="text-align:right"><?php
                        echo escreverNumero($RFP['valor']);
                        $total = $RFP['valor'] + $total;
                        ?></td>
                </tr>
            <?php } ?>
            <tr>
                <td colspan="2" class="lin"></td>
            </tr>
            <tr>
                <td colspan="2" class="lin"></td>
            </tr>
            <tr>
                <td colspan="1" class="doc"><b>TOTAL:</b></td>
                <td colspan="1" class="doc" style="text-align:right"><b><?php echo escreverNumero($total); ?></b></td>
            </tr>
        </table>
    </body>
    <script type="text/javascript" src="./../../js/util.js"></script>    
    <script type="text/javascript">
        $(window).load(function () {
            window.print();
            setTimeout(function () {
                window.close();
            }, 500);
        });
    </script> 
    <?php odbc_close($con);?>
</html>