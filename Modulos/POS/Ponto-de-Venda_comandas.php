<?php
include "./../../Connections/configini.php";
$txtTpTela = "0";
if (isset($_GET["tp"]) && $_GET["tp"] == "1") {
    $txtTpTela = "1";
}
?>
<link rel="stylesheet" type="text/css" href="../../css/jquery/ui.css"/>
<div id="lcom_win">    
    <div style="width:100%; height:100%; position:absolute; top:0; left:0; z-index:9"><img src="./../../img/bg_preto.png" style="width:100%; height:100%"></div>
    <div class="keypad-popup" style="width:700px; height:550px; position:absolute; top:50%; left:50%; margin-top:-280px; margin-left:-350px; display:block">
        <div class="keypad-row" style="width:700px; height:550px; margin:0px; text-align:center">
            <div class="trans_venda" style="float:left;">
                <div style="width: 10%;float: left;border:1px solid #CCC;">
                    <div class="label">Comanda</div>
                    <input id="txtCom" type="text" style="width: 100%;border: none;" value=""/>                    
                </div>               
                <div style="width: 10%;float: left;border:1px solid #CCC;">
                    <div class="label">De</div>
                    <input id="txtDtI" type="text" style="width: 100%;border: none;" value="<?php echo escreverDataSoma(getData("T"), " -7 days"); ?>"/>                    
                </div>
                <div style="width: 10%;float: left;border:1px solid #CCC;">
                    <div class="label">Até</div>
                    <input id="txtDtF" type="text" style="width: 100%;border: none;" value="<?php echo getData("T"); ?>"/>
                </div>
                <div style="width: 39%;float: left;border:1px solid #CCC;">
                    <div class="label">Cliente</div>
                    <input name="txtIdNota" id="txtIdNota" value="" type="hidden"/>
                    <input type="text" style="width:100%;border: none;" name="txtDesc" id="txtDesc" class="inputbox" value="" size="10" />                    
                </div>
                <div style="width: 22%;float: left;border:1px solid #CCC;">
                    <div class="label">Vendedor</div>
                    <select id="txtUsu" style="width: 100%;border: none;">
                        <option value="null" selected>TODOS</option>
                        <?php $cur = odbc_exec($con, "select login_user,nome from sf_usuarios where inativo = 0 order by nome") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option value="<?php echo utf8_encode($RFP["login_user"]); ?>"><?php echo utf8_encode(strtoupper($RFP["login_user"])); ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div style="width: 7%;float: left">
                    <button type="button" class="keypad-special" onclick="buscarVendas();" style="float:right; width:42px; height:38px; color:#FFF; background-color:#009AD7">
                        <div class="icon" style="margin-top:0px"><span class="ico-search" style="font-size:22px"></span></div>
                    </button>                    
                </div>                    
            </div>
            <div style="clear:both"></div>
            <div class="trans_venda" style="width:100%;margin-top: 8px;">
                <div class="label"><?php echo ($txtTpTela == 1 ? "Orçamentos" : "Vendas em Aberto"); ?>:</div>
                <div id="lista_comanda" style="overflow-y:hidden; width:calc(100% - 2px); height:440px; border-left:1px solid #CCC; border-right:1px solid #CCC"></div>
                <div style="clear:both"></div>
                <div class="label" style="line-height:40px">&nbsp;
                    <button type="button" class="keypad-special" style="float:left; width:42px; height:38px; color:#FFF; background-color:#D53F26" onClick="listaComanda()">
                        <div class="icon" style="margin-top:5px"><span class="ico-cancel" style="font-size:22px"></span></div>
                    </button>
                    <button id="user_down" type="button" class="keypad-special" style="float:right; width:42px; height:38px; color:#FFF; background-color:#009AD7">
                        <div class="icon" style="margin-top:5px"><span class="ico-arrow-down" style="font-size:22px"></span></div>
                    </button>
                    <button id="user_up" type="button" class="keypad-special" style="float:right; width:42px; height:38px; color:#FFF; margin-right:0; background-color:#009AD7">
                        <div class="icon" style="margin-top:5px"><span class="ico-arrow-up" style="font-size:22px"></span></div>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="./../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='./../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>   
    <script type="text/javascript">
        jQuery(document).ready(function () {            
            $("#txtDtI, #txtDtF").pickadate().pickadate("picker");   
            buscarVendas();
        });        
        
        $("#txtDesc").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "../CRM/ajax.php",
                    dataType: "json",
                    data: {q: request.term, t: "C"},
                    success: function (data) {
                        response(data);
                    }
                });
            },
            minLength: 3,
            select: function (event, ui) {
                $('#txtIdNota').val(ui.item.id).show();
                $('#txtDesc').val(ui.item.label).show();                
            }
        });        
        
        function buscarVendas() {
            $("#lista_comanda").html("");
            var novaBusca = "";                       
            $.getJSON("../Estoque/<?php echo ($txtTpTela == 1 ? "Orcamentos_server_processing.php" : "Vendas_server_processing.php"); ?>", {
                id: '<?php echo ($txtTpTela == 1 ? "2" : "1"); ?>', imp: '1', comanda: 'S', 
                filial: $("#txtFilial").val(),com: $("#txtCom").val(),
                dti: $("#txtDtI").val(),dtf: $("#txtDtF").val(),
                tp: <?php echo ($txtTpTela == 1 ? "''" : "0"); ?>, <?php echo ($txtTpTela == 1 ? "Cli" : "td"); ?>: ($("#txtDesc").val() !== "" ? $("#txtIdNota").val() : ""),
                usr: $("#txtUsu").val()
            }, function (j) {
                for (var i = 0; i < j.aaData.length; i++) {
                    novaBusca = '<div style="background:#EEE; color:#999; font-size:12px; text-align:left; height:40px; line-height:40px; border-bottom:1px solid #FFF">\n\
                    <div style="float:right; width:44px; line-height:40px">\n\
                    <a href="/Modulos/POS/Ponto-de-Venda.php?tp=<?php echo $txtTpTela; ?>&vd=' + j.aaData[i][0] + '">\n\
                    <button type="button" class="keypad-special" style="width:42px; height:38px; color:#FFF; background-color:#68AF27">\n\
                    <div class="icon" style="margin-top:1px"><span class="ico-arrow-right" style="font-size:22px"></span></div>\n\
                    </button>\n\
                    </a>\n\
                    </div>\n\
                    <div style="float:left; color:#333; font-size:12px; width:10%;"><b>&nbsp;' + j.aaData[i][1] + '</b></div>\n\
                    <div style="float:left; color:#333; font-size:12px; width:10%;"><b>' + j.aaData[i][2] + '</b></div>\n\
                    <div style="float:left; color:#333; font-size:12px; width:40%;"><b>' + j.aaData[i][3] + '</b></div>\n\
                    <div style="float:left; color:#333; font-size:12px; width:20%;"><b>' + j.aaData[i][4] + '</b></div>\n\
                    <div style="float:left; color:#333; font-size:12px; width:10%;"><b>' + j.aaData[i][5] + '</b></div>\n\
                    </div>';
                    $("#lista_comanda").append(novaBusca);
                }
            });
        }
        
        $("#user_up").on("mousedown", function () {
            interval = setInterval(function () {
                var pos = $("#lista_comanda").scrollTop();
                $("#lista_comanda").scrollTop(pos - 2);
            }, 1);
        }).click().on("mouseup", function () {
            clearInterval(interval);
        });
        
        $("#user_down").on("mousedown", function () {
            interval = setInterval(function () {
                var pos = $("#lista_comanda").scrollTop();
                $("#lista_comanda").scrollTop(pos + 2);
            }, 1);
        }).click().on("mouseup", function () {
            clearInterval(interval);
        });
    </script>
    <?php odbc_close($con); ?>
</div>