<?php

include "../../Connections/configini.php";
$tipo = $_REQUEST['txtTipo'];
$id = $_REQUEST['txtId'];
$idItem = $_REQUEST['txtIdItem'];
$conteudo = $_REQUEST['txtConteudo'];
$contrato = 0;
if (isset($_GET["contratoSivis"])) {
    $_POST["Modelo"] = 0;
}

if ($tipo == "L" && is_numeric($id)) {
    $local = array();
    $sql = "select id_contatos,conteudo_contato from sf_fornecedores_despesas_contatos
    where tipo_contato = 2 and fornecedores_despesas = " . $id;
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('id_contatos' => $RFP['id_contatos'], 'conteudo_contato' => utf8_encode($RFP['conteudo_contato']));
    }
    echo (json_encode($local));
} elseif ($tipo == "D" && is_numeric($idItem)) {
    $sql = "DELETE FROM sf_fornecedores_despesas_contatos WHERE id_contatos = " . $idItem;
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    echo "YES";
} elseif ($tipo == "E" && is_numeric($idItem) && strlen($conteudo) > 0) {
    $sql = "UPDATE sf_fornecedores_despesas_contatos SET conteudo_contato = " . valoresTexto2($conteudo) .
            " WHERE id_contatos = " . $idItem;
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    echo "YES";
} elseif ($tipo == "S" && is_numeric($id) && strlen($conteudo) > 0) {
    $sql = "INSERT INTO sf_fornecedores_despesas_contatos (fornecedores_despesas,tipo_contato,conteudo_contato) VALUES (" .
            $id . ",2," . valoresTexto2($conteudo) . ")";
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    echo "YES";
} elseif ($tipo == "F" && is_numeric($id)) {
    $_GET["id"] = $id;
    $_GET["tpImp"] = "F";
    $_GET["NomeArq"] = "Contrato";
    if ($_POST["Modelo"] == "0") {
        $_GET["Modelo"] = "";
        $_GET["PathArq"] = "ContratoAcademia";
        if (isset($_POST["idContrato"])) {
            $contrato = $_POST["idContrato"];
        }
    } elseif ($_POST["Modelo"] == "1") {
        $_GET["Modelo"] = "C";
        $_GET["PathArq"] = "ContratoAcademia";
        if (isset($_POST["idContrato"])) {
            $contrato = $_POST["idContrato"];
        }        
    } elseif ($_POST["Modelo"] == "2") {
        $_GET['pAlt'] = "90";
        $_GET['pLar'] = "200";
        $_GET["PathArq"] = "../../Modulos/Academia/cupom_plano.php";
    } elseif ($_POST["Modelo"] == "3") {
        $_GET['pAlt'] = "90";
        $_GET['pLar'] = "200";
        $_GET["PathArq"] = "../../Modulos/Academia/cupom_agendamento.php";
    }
    include "../../util/ImpressaoPdf.php";
    echo "YES";
} elseif ($tipo == "M" && is_numeric($id)) {
    $pathArq = '../../Pessoas/' . $_SESSION['contrato'] . '/Contrato/' . $id . '.pdf';
    if (file_exists($pathArq)) {
        echo "YES";
    } else {
        echo "Não foi possível gerar o documento!";
    }
}
odbc_close($con);
