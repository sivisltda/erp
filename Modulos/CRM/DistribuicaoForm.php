<?php include "../../Connections/configini.php"; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <link href="../../js/plugins/jquery-ui-1.11.4/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    </head>
    <body>
        <form name="frmEnviaDados" id="frmEnviaDados" method="POST">                        
            <div class="frmhead">
                <div class="frmtext">Distribuir Prospects</div>
                <div class="frmicon" style="top:12px" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <div style="height:42px">
                    <label for="txtUserResp">Usuário de Origem:</label>
                    <select class="select" id="txtUserResp" name="txtUserResp" style="width:100%;float: left;">
                        <option value="null">Selecione</option>
                        <?php $cur = odbc_exec($con, "select id_usuario, nome from sf_usuarios where inativo = 0 and id_usuario > 1") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option value="<?php echo $RFP['id_usuario'] ?>"><?php echo formatNameCompact(strtoupper(utf8_encode($RFP['nome']))); ?></option>
                        <?php } ?>
                    </select>
                </div>
                <label for="txtUserResp">Usuários para Transferência:</label>
                <div class="data-fluid" style="overflow-x: inherit;">
                    <div style="background:#F1F1F1; border:1px solid #DDD; border-bottom:0; padding:10px 10px 0">
                        <div style="float:left; width:89%;">
                            <select id="txtGrupoSel" class="select" <?php echo $disabled; ?> style="width:100%">
                                <option value="null">Selecione</option>
                                <?php $cur = odbc_exec($con, "select id_usuario, nome from sf_usuarios where inativo = 0 and id_usuario > 1") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP['id_usuario'] ?>"><?php echo formatNameCompact(strtoupper(utf8_encode($RFP['nome']))); ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div style="float:left; width:10%; margin-left:1%;">
                            <button type="button" onclick="addCampos();" class="btn dblue" <?php echo $disabled; ?> style="height: 27px;background-color: #308698 !important;">
                                <span class="ico-plus icon-white"></span>
                            </button>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div style="background:#F1F1F1; border:1px solid #DDD; border-top:0; padding:5px 10px 10px">
                        <div id="divContatos" style="height:336px; background:#FFF; border:1px solid #DDD; overflow-y:scroll;">
                            <?php $i = 0;
                            $cur = odbc_exec($con, "select c.id_usuario, u.nome from sf_configuracao_usuarios c
                            inner join sf_usuarios u on u.id_usuario = c.id_usuario order by c.id") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { $i++; ?>                            
                                <div id="tabela_linha_<?php echo $i; ?>" style="padding:2px; border-bottom:1px solid #DDD; overflow: hidden;">
                                <input id="txtIdItem_<?php echo $i; ?>" name="txtIdItem[]" value="<?php echo $RFP['id_usuario']; ?>" type="hidden">     
                                <div id="tabela_desc_<?php echo $i; ?>" style="line-height:27px; float:left"><?php echo formatNameCompact(strtoupper(utf8_encode($RFP['nome']))); ?></div>
                                <div style="width: 5%; float: right; cursor: pointer;" onclick="return removeLinha('<?php echo $i; ?>');"><span class="ico-remove" style="font-size:20px"></span></div></div>                              
                            <?php } ?>
                            <input id="txtContatosTotal" name="txtContatosTotal" type="hidden" value="<?php echo $i; ?>">
                        </div>
                    </div>
                </div>                
            </div>
            <div class="frmfoot">
                <div style="width: 50%;float: left;text-align: left;"></div>
                <div class="frmbtn">
                    <button class="btn btn-success" type="button" onclick="btnSave();" name="btnEnviar" title="Enviar" id="btnEnviar" value="Enviar"><span class="ico-checkmark"></span> Enviar </button>
                    <button class="btn yellow" title="Cancelar" onClick="parent.FecharBox(1)" id="btnCancelar"><span class="ico-reply"></span> Cancelar</button>
                </div>
            </div>
        </form>
        <script type='text/javascript' src='../../js/plugins/jquery-ui-1.11.4/jquery-ui.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/plugins/ckeditor/ckeditor.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>
        <script type="text/javascript">
            
            function btnSave() {
                if (validarSave()) {
                    bootbox.confirm('Confirma a Troca de Usuários? Essa ação não poderá ser revertida.', function (result) {
                        if (result === true) {
                            $.post("form/CRMFormServer.php", "btnTrocaUsuarios=S&isAjax=S&" +
                            $("#frmEnviaDados").serialize()).done(function (data) {
                                if (data === "YES") {
                                    bootbox.alert({message: "Usuários transferidos com sucesso!", 
                                        callback: function() {
                                            parent.FecharBox();
                                        } 
                                    });
                                } else {
                                    bootbox.alert(data);
                                }
                            });
                        }
                    });
                }
            }
                                    
            function validarSave() {
                if ($('#txtUserResp').val() === "null") {
                    bootbox.alert("Selecione um Usuário de Origem!");
                    return false;
                }
                if ($('input[name="txtIdItem[]"]').length === 0) {
                    bootbox.alert("Adicione Usuário(s) para Transferência!");
                    return false;
                }
                return true;
            }
            
            function addCampos() {
                if (validar()) {
                    let id = parseInt($("#txtContatosTotal").val()) + 1;
                    let linha = "<div id=\"tabela_linha_" + id + "\" style=\"padding:2px; border-bottom:1px solid #DDD; overflow: hidden;\">" +                         
                    "<input id=\"txtIdItem_" + id + "\" name=\"txtIdItem[]\" value=\"" + $("#txtGrupoSel").val() + "\" type=\"hidden\">" +                
                    "<div id=\"tabela_desc_" + id + "\" style=\"line-height:27px; float:left\">" + $("#txtGrupoSel option:selected").text() + "</div>" +
                    "<div style=\"width: 5%; float: right; cursor: pointer;\" onclick=\"return removeLinha('" + id + "');\"><span class=\"ico-remove\" style=\"font-size:20px\"></span></div></div>";
                    $("#divContatos").append(linha);  
                    $("#txtContatosTotal").val(id);
                    $("#txtGrupoSel").select2("val", "null");
                }
            }

            function validar() {
                if ($('#txtGrupoSel').val() === "null") {
                    bootbox.alert("Selecione um Usuário!");
                    return false;
                }
                return true;
            }

            function removeLinha(id) {
                $("#tabela_linha_" + id).remove();
            }
        </script>
    </body>
</html>