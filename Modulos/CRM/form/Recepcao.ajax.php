<?php

include "../../../Connections/configini.php";

if (is_numeric($_REQUEST['cod'])) {

    $id = '';
    $operador = $_SESSION["login_usuario"];
    $data = getData("T");
    $hora = date("H:i");
    $relato = '';
    $departamento = '';
    $para = '';
    $contato = '';
    $tipo = 'C';
    $nomecon = '';
    $nomecontato = '';
    $obs = '';
    $id_servico = 0;
    $pendente = 1;
    $pendenteX = 0;

    $cur = odbc_exec($con, "select id_tele,u.login_user,para_pessoa,data_tele,pendente,proximo_contato,t.obs,id_fornecedores_despesas,bloqueado,tipo,us.login_user login_user_fechamento,data_tele_fechamento,obs_fechamento,especificacao,
                    garantia,atendimento_servico,nota_fiscal,data_nf,valor_orcamento,relato,dias_garantia,t.departamento,nomecon,razao_social,nome_fantasia
                    from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa
                    LEFT JOIN sf_usuarios us on us.id_usuario = t.de_pessoa_fechamento
                    LEFT JOIN sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = t.pessoa where id_tele = " . $_REQUEST['cod']);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_tele']);
        $operador = utf8_encode($RFP['login_user']);
        $data = escreverData($RFP['data_tele']);
        $hora = escreverData($RFP['data_tele'], 'H:i');
        $relato = utf8_encode($RFP['relato']);
        $departamento = utf8_encode($RFP['departamento']);
        $para = utf8_encode($RFP['para_pessoa']);
        $contato = utf8_encode($RFP['id_fornecedores_despesas']);
        $tipo = utf8_encode($RFP['tipo']);
        $nomecon = utf8_encode($RFP['nomecon']);
        $nomecontato = utf8_encode($RFP['razao_social']);
        if ($RFP['nome_fantasia'] != "") {
            $nomecontato = $nomecontato . " (" . utf8_encode($RFP['nome_fantasia']) . ")";
        }
        $obs = utf8_encode($RFP['obs']);
        $id_servico = utf8_encode($RFP['atendimento_servico']);
        $pendente = utf8_encode($RFP['pendente']);
    }
    if (is_numeric($contato)) {
        $cur = odbc_exec($con, "select SUM(total) total from (
        select COUNT(lp.id_lancamento_movimento) total from sf_lancamento_movimento_parcelas lp 
        inner join sf_lancamento_movimento l on lp.id_lancamento_movimento = l.id_lancamento_movimento
        inner join sf_contas_movimento c on c.id_contas_movimento = l.conta_movimento                                        
        where data_pagamento is null and status = 'Aprovado' and lp.inativo = 0 and dateadd(day,dias_tolerancia,data_vencimento) < GETDATE() and fornecedor_despesa = " . $contato . "
        union select COUNT(sp.solicitacao_autorizacao) total from sf_solicitacao_autorizacao sa 
        inner join sf_solicitacao_autorizacao_parcelas sp on sa.id_solicitacao_autorizacao = sp.solicitacao_autorizacao
        inner join sf_contas_movimento c on c.id_contas_movimento = sa.conta_movimento
        where data_pagamento is null and status = 'Aprovado' and sp.inativo = 0 and dateadd(day,dias_tolerancia,data_parcela) < GETDATE() and fornecedor_despesa = " . $contato . "
        union select count(v.id_venda) total from sf_vendas v inner join sf_venda_parcelas vp on v.id_venda = vp.venda
        where data_pagamento is null and status = 'Aprovado' and vp.inativo = 0 and data_parcela < GETDATE() and cliente_venda = " . $contato . ") as x");
        while ($RFP = odbc_fetch_array($cur)) {
            if (utf8_encode($RFP['total']) > 0) {
                $pendenteX = 1;
            }
        }
    }
    $local[] = array('id' => $id, 'operador' => $operador, 'data' => $data, 'hora' => $hora,
        'relato' => $relato, 'departamento' => $departamento, 'para' => $para, 'contato' => $contato,
        'tipo' => $tipo, 'nomecontato' => $nomecontato, 'nomecon' => $nomecon, 'obs' => $obs, 'id_servico' => $id_servico,
        'pendente' => $pendente, 'pendenteX' => $pendenteX);
    echo(json_encode($local));
}

if (is_numeric($_GET['Del'])) {
    odbc_exec($con, "DELETE FROM sf_mensagens_telemarketing WHERE id_tele = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_telemarketing_item WHERE id_telemarketing = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_telemarketing WHERE id_tele = " . $_GET['Del']);
    echo "YES";
}

if (isset($_POST['bntSave'])) {
    $pessoaP = $_POST['txtIdNota'];
    $datah = valoresDataHora("txtData", "txtHora");
    $datap = valoresDataHora("txtDataProximo", "txtHoraProximo");
    if (is_numeric($pessoaP)) {
        $notIn = '0';
        if (isset($_POST['txtQtdContatos'])) {
            $max = $_POST['txtQtdContatos'];
            for ($i = 0; $i <= $max; $i++) {
                if (isset($_POST['txtIdContato_' . $i]) && is_numeric($_POST['txtIdContato_' . $i])) {
                    if ($_POST['txtIdContato_' . $i] != 0) {
                        $notIn = $notIn . "," . $_POST['txtIdContato_' . $i];
                    }
                }
            }
            if ($notIn != "") {
                odbc_exec($con, "delete from sf_fornecedores_despesas_contatos where fornecedores_despesas = " . $pessoaP . " and id_contatos not in (" . $notIn . ")");
            }
        }
    } elseif ($_POST['txtPenCon'] == "P") {
        $query = "insert into sf_fornecedores_despesas(razao_social,tipo,empresa,dt_cadastro)values(" .
                valoresTexto('txtDesc') . "," .
                "'P','001',getDate())";
        echo $query;
        exit();
        odbc_exec($con, $query) or die(odbc_errormsg());
        $sql = "select top 1 id_fornecedores_despesas from sf_fornecedores_despesas order by id_fornecedores_despesas desc";
        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $pessoaP = $RFP['id_fornecedores_despesas'];
        }
    }
    if (is_numeric($pessoaP)) {
        if (isset($_POST['txtQtdContatos'])) {
            $max = $_POST['txtQtdContatos'];
            for ($i = 1; $i <= $max; $i++) {
                if (isset($_POST['txtIdContato_' . $i])) {
                    if ($_POST['txtIdContato_' . $i] !== "0") {
                        odbc_exec($con, "update sf_fornecedores_despesas_contatos set tipo_contato = " . $_POST['txtTpContato_' . $i] . ",conteudo_contato = " . valoresTexto('txtTxContato_' . $i) . ",resp_contato = " . valoresTexto('txtTxResponsavel_' . $i) . " where id_contatos = " . $_POST['txtIdContato_' . $i]) or die(odbc_errormsg());
                    } else {
                        odbc_exec($con, "insert into sf_fornecedores_despesas_contatos(fornecedores_despesas,tipo_contato,conteudo_contato,resp_contato) values(" . $pessoaP . "," . $_POST['txtTpContato_' . $i] . "," . valoresTexto('txtTxContato_' . $i) . "," . valoresTexto('txtTxResponsavel_' . $i) . ")") or die(odbc_errormsg());
                    }
                }
            }
        }
    }
    if ($_POST['txtId'] != '') {
        $res = odbc_exec($con, "select count(*) from sf_telemarketing where (de_pessoa = " . $_SESSION["id_usuario"] . " or para_pessoa = " . $_SESSION["id_usuario"] . " or para_pessoa is null) and id_tele = " . $_POST['txtId']) or die(odbc_errormsg());
        if (odbc_result($res, 1) > 0) {
            $queryZ = '';
            if (utf8_decode($_POST['txtPendente']) == "0" && utf8_decode($_POST['txtEraPendente']) == "1") {
                $queryZ = " data_tele_fechamento = GetDate(), de_pessoa_fechamento = " . $_SESSION["id_usuario"] . ", ";
            }
            $query = "set dateformat dmy; update sf_telemarketing set " .
                    "para_pessoa = " . utf8_decode($_POST['txtPara']) . "," .
                    "pendente = " . utf8_decode($_POST['txtPendente']) . "," .
                    "proximo_contato = " . $datap . "," . $queryZ .
                    "pessoa = " . $pessoaP . "," .
                    "especificacao = null," .
                    "garantia = " . valoresCheck('ckbGarantia') . "," .
                    "atendimento_servico = " . $_POST['txtservico'] . "," .
                    "nota_fiscal = " . valoresTexto('txtNotaFiscal') . "," .
                    "data_nf = " . valoresData('txtDataNota') . "," .
                    "data_tele = " . $datah . "," .
                    "valor_orcamento = " . valoresNumericos('txtTaxaOrc') . "," .
                    "relato = " . valoresTexto('txtRelato') . "," .
                    "nomecon = " . valoresTexto('txtNomeCon') . "," .
                    "departamento = " . utf8_decode($_POST['txtDepartamento']) . "," .
                    "dias_garantia = '" . utf8_decode($_POST['txtPrevOrc']) . "'," .
                    "obs_fechamento=" . valoresTexto('txtObsFechamento') . ",obs =" . valoresTexto('txtObs') . " where id_tele = " . $_POST['txtId'];
            odbc_exec($con, $query) or die(odbc_errormsg());
            echo $_POST['txtId'];
        } else {
            echo "Não é possível alterar este chamado, o proprietário e o destinatário do chamado estão relacionados a outro usuário!";
        }
    } else {
        $dataF = 'null';
        $pesF = 'null';
        $ObsF = '';
        if (utf8_decode($_POST['txtPendente']) == "0") {
            $dataF = 'GetDate()';
            $pesF = $_SESSION["id_usuario"];
            $ObsF = utf8_decode($_POST['txtObsFechamento']);
        }
        $query = "set dateformat dmy; insert into sf_telemarketing(de_pessoa,para_pessoa,pendente,proximo_contato,obs,pessoa,data_tele,data_tele_fechamento,obs_fechamento,de_pessoa_fechamento,garantia,atendimento_servico,nota_fiscal,data_nf,valor_orcamento,relato,nomecon,dias_garantia,especificacao,departamento,dt_inclusao)values(" .
                $_SESSION["id_usuario"] . "," .
                utf8_decode($_POST['txtPara']) . "," .
                utf8_decode($_POST['txtPendente']) . "," .
                $datap . ",'" .
                utf8_decode($_POST['txtObs']) . "'," . $pessoaP . "," . $datah . "," . $dataF . ",'" . $ObsF . "'," . $pesF .
                "," . valoresCheck('ckbGarantia') .
                "," . $_POST['txtservico'] .
                "," . valoresTexto('txtNotaFiscal') .
                "," . valoresData('txtDataNota') .
                "," . valoresNumericos('txtTaxaOrc') .
                "," . valoresTexto('txtRelato') .
                "," . valoresTexto('txtNomeCon') .
                "," . valoresTexto('txtPrevOrc') .
                ",null," . utf8_decode($_POST['txtDepartamento']) . ",getdate())";

        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_tele from sf_telemarketing order by id_tele desc") or die(odbc_errormsg());
        echo odbc_result($res, 1);
    }
}
odbc_close($con);
