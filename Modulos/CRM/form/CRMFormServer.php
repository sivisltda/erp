<?php

if (isset($_POST['isAjax'])) {
    include "../../../Connections/configini.php";
}

$disabled = '';
$id = '';
$tipo = 'F';
$nome = '';
$dtCadastro = '';
$dtConvCliente = '';
$dtAprProspect = '';
$statusAluno = 'Aguardando';
$cpf = '';
$rg = '';
$dtNascimento = '';
$sexo = '';
$orgExp = '';
$tpsang = '';
$estadoCivil = '';
$profissao = '';
$nmEmpresa = '';
$planoSaude = '';
$nmEmergencia = '';
$telEmergencia = '';
$procedencia = '';
$cep = '';
$endereco = '';
$numero = '';
$bairro = '';
$estado = '';
$cidade = '';
$complemento = '';
$indicador = '';
$profResp = '';
$profRespNome = '';
$ckempresa = '';
$user_resp = '';
$Obs = '';
$acessoTipo = '';
$acessoNumProv = '';
$acessomsg = '';
$grupoPessoa = '';
$juridico_tipo = '';
$emailMarketing = '1';
$celularMarketing = '1';
$inativo = '0';
$nmFantasia = '';
$abas = "style=\"display: none;\"";
$usuario_cadastro = "";
$regimetributario = "";
$gymPassId = '';
$termometro = '2';
$insMunicipal = '';
$numCnh = '';
$catCnh = '';
$vencCnh = '';
$priCnh = '';
$nfe_iss = '';
$id_pessoa = '';

if (isset($_POST['btnSave'])) {   
    odbc_exec($con, "set dateformat dmy;") or die(odbc_errormsg());
    $cur = odbc_exec($con, "select id_fornecedores_despesas from sf_fornecedores_despesas 
    where len(cnpj) > 0 and cnpj = '" . $_REQUEST['txtCpf'] . "' and id_fornecedores_despesas not in (" . valoresNumericos2($_REQUEST['txtId']) . ")
    and tipo not in ('R','E','F') and inativo = 0");
    while ($RFP = odbc_fetch_array($cur)) {
        if (is_numeric($RFP['id_fornecedores_despesas'])) {
            echo "CPF inválido para cadastro!"; exit;            
        }
    }      
    if (is_numeric($_POST['txtId'])) {        
        $query = "select bloqueado, isnull((select login_user from sf_usuarios where id_usuario = id_user_resp), 'Nenhum') usuario_atual, 
        isnull((select login_user from sf_usuarios where id_usuario = " . valoresSelect('txtUserResp') . "), 'Nenhum') usuario_novo, 
        isnull((select razao_social from sf_fornecedores_despesas as y where y.id_fornecedores_despesas > 0 and y.id_fornecedores_despesas = x.prof_resp), 'Nenhum') prof_atual,
        isnull((select razao_social from sf_fornecedores_despesas as z where z.id_fornecedores_despesas > 0 and z.id_fornecedores_despesas = " . valoresTexto('txtProfResp') . "), 'Nenhum') prof_novo,
        isnull((select descricao_grupo from sf_grupo_cliente where id_grupo = grupo_pessoa), 'Nenhum') grupo_atual,
        isnull((select descricao_grupo from sf_grupo_cliente where id_grupo = " . valoresSelect('txtGrupo') . "), 'Nenhum') grupo_novo
        from sf_fornecedores_despesas as x where id_fornecedores_despesas = " . $_POST['txtId'];
        $cur = odbc_exec($con, $query);
        while ($RFP = odbc_fetch_array($cur)) {
            if (utf8_encode($RFP['usuario_atual']) != utf8_encode($RFP['usuario_novo'])) {
                odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
                values ('sf_fornecedores_despesas', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'P', '" . utf8_decode("ALTERAÇÃO DE USUÁRIO RESPONSÁVEL DE ") . $RFP['usuario_atual'] . " para " . $RFP['usuario_novo'] . "', GETDATE(), " . $_POST['txtId'] . ")");
            } 
            if (utf8_encode($RFP['prof_atual']) != utf8_encode($RFP['prof_novo'])) {
                odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
                values ('sf_fornecedores_despesas', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'P', '" . utf8_decode("ALTERAÇÃO DE INDICADOR RESPONSÁVEL DE ") . $RFP['prof_atual'] . " para " . $RFP['prof_novo'] . "', GETDATE(), " . $_POST['txtId'] . ")");
            }
            if ($RFP['bloqueado'] == 0 && valoresCheck('ckbInativo') == 1) {
                odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
                values ('sf_fornecedores_despesas', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'B', '" . utf8_decode("INCLUSÃO DE BLOQUEIO") . "', GETDATE(), " . $_POST['txtId'] . ")");
            }
            if ($RFP['bloqueado'] == 1 && valoresCheck('ckbInativo') == 0) {
                odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
                values ('sf_fornecedores_despesas', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'B', '" . utf8_decode("EXCLUSÃO DE BLOQUEIO") . "', GETDATE(), " . $_POST['txtId'] . ")");
            }
            if (utf8_encode($RFP['grupo_atual']) != utf8_encode($RFP['grupo_novo'])) {
                odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
                values ('sf_fornecedores_despesas', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'G', '" . utf8_decode("ALTERAÇÃO DE GRUPO DE ") . $RFP['grupo_atual'] . " para " . $RFP['grupo_novo'] . "', GETDATE(), " . $_POST['txtId'] . ")");
            }            
        }
    }    
    if (is_numeric($_POST['txtId'])) {
        $query = "update sf_fornecedores_despesas set " .
                "juridico_tipo = " . valoresTexto('txtJuridicoTipo') . "," .
                "razao_social = " . strtoupper(str_replace("*", "&", valoresTexto('txtNome'))) . "," .
                "cnpj = " . valoresTexto('txtCpf') . "," .
                "inscricao_estadual = " . valoresTexto('txtRG') . "," .
                "endereco = " . valoresTexto('txtEndereco') . "," .
                "numero = " . valoresTexto('txtNumero') . "," .
                "complemento = " . valoresTexto('txtComplemento') . "," .
                "bairro = " . valoresTexto('txtBairro') . "," .
                "org_exped_rg = " . valoresTexto('txtOrg') . "," . 
                "tipo_sang = " . valoresTexto('txtTpSang') . "," .                  
                "estado = " . valoresSelect('txtEstado') . "," .
                "cidade = " . valoresSelect('txtCidade') . "," .
                "cep = " . valoresTexto('txtCep') . "," .
                "data_nascimento = " . valoresData('txtDtNasc') . "," .
                "profissao = " . valoresTexto('txtProfissao') . "," .
                "inscricao_municipal = " . valoresTexto('txtInsMunicipal') . "," .                
                "trabalha_empresa = " . valoresTexto('txtNmEmpresa') . "," .
                "estado_civil = " . valoresTexto('txtEstadoCivil') . "," .
                "Observacao = " . valoresTexto('txtObs') . "," .                
                "plano_saude = " . valoresTexto('txtPlanoSaude') . "," .                
                "telefone_ps1 = " . valoresTexto('txtNmEmergencia') . "," .
                "telefone_ps2 = " . valoresTexto('txtTelEmerg') . "," .
                "procedencia = " . valoresSelect('txtProcedencia') . "," .
                "sexo = " . valoresTexto('txtSexo') . "," .
                "indicador = " . valoresSelect('txtIndicador') . "," .
                "id_user_resp = " . valoresSelect('txtUserResp') . "," .
                "prof_resp = " . valoresSelect('txtProfResp') . "," .                                
                "grupo_pessoa = " . valoresSelect('txtGrupo') . "," .
                "email_marketing = " . (valoresCheck('ckbInativo') == 0 ? valoresCheck('ckbEmailMarketing') : 0) . "," .
                "celular_marketing = " . (valoresCheck('ckbInativo') == 0 ? valoresCheck('ckbCelularMarketing') : 0) . "," .              
                "empresa = " . valoresTexto('txtFilial') . "," .                
                "bloqueado = " . valoresCheck('ckbInativo') . "," .
                "nome_fantasia = " . valoresTexto('txtNmFantasia') . "," .
                "gyp_idaluno = " . valoresTexto('txtGymPassId') . "," .                
                "termometro = " . valoresSelect('txtTermometro') . "," .                
                "regime_tributario = " . valoresSelect('txtRegTributario') .  "," .   
                "num_cnh = " . valoresTexto('txtNumCnh') . "," .
                "cat_cnh = " . valoresTexto('txtCatCnh') . "," .
                "ven_cnh = " . valoresData('txtVencCnh') . "," .
                "nfe_iss = " . valoresCheck('ckbISS') . "," .
                "pri_cnh = " . valoresData('txtPriCnh') .  
                " where id_fornecedores_despesas = " . $_POST['txtId'];
        //echo $query;exit();
        odbc_exec($con, $query) or die(odbc_errormsg());
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        values ('sf_clientes', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'E', 'EDITAR - PROSPECT', GETDATE(), " . $_POST['txtId'] . ")");        
    } else {
        $convertCli = "";
        if ($_SESSION["mod_emp"] == 1) {
            $convertCli = "GETDATE()";
        } else {
            $convertCli = "null";
        }
        $query = "insert into sf_fornecedores_despesas (juridico_tipo,razao_social,cnpj,inscricao_estadual,endereco,numero,complemento,bairro,estado,cidade,cep,data_nascimento,
                profissao,procedencia,trabalha_empresa,estado_civil,Observacao,telefone_ps1,telefone_ps2,sexo,indicador,id_user_resp,plano_saude,grupo_pessoa,bloqueado,tipo,empresa,dt_cadastro, 
                email_marketing, celular_marketing,nome_fantasia, usuario_cadastro, gyp_idaluno, termometro, org_exped_rg, tipo_sang, prof_resp, regime_tributario, dt_aprov_prospect, dt_convert_cliente, inscricao_municipal,
                num_cnh, cat_cnh, ven_cnh, pri_cnh, nfe_iss) values (" .
                valoresTexto('txtJuridicoTipo') . "," .
                strtoupper(valoresTexto('txtNome')) . "," .
                valoresTexto('txtCpf') . "," .
                valoresTexto('txtRG') . "," .
                valoresTexto('txtEndereco') . "," .
                valoresTexto('txtNumero') . "," .
                valoresTexto('txtComplemento') . "," .
                valoresTexto('txtBairro') . "," .
                valoresSelect('txtEstado') . "," .
                valoresSelect('txtCidade') . "," .
                valoresTexto('txtCep') . "," .
                valoresData('txtDtNasc') . "," .
                valoresTexto('txtProfissao') . "," .
                valoresSelect('txtProcedencia') . "," .
                valoresTexto('txtNmEmpresa') . "," .
                valoresTexto('txtEstadoCivil') . "," .
                valoresTexto('txtObs') . "," .
                valoresTexto('txtNmEmergencia') . "," .
                valoresTexto('txtTelEmerg') . "," .
                valoresTexto('txtSexo') . "," .
                valoresSelect('txtIndicador') . "," .
                valoresSelect('txtUserResp') . "," .                
                valoresTexto('txtPlanoSaude') . "," .
                valoresSelect('txtGrupo') . "," .
                valoresCheck('ckbInativo') . "," .
                valoresTexto('txtTpCli') . "," . 
                valoresTexto('txtFilial') . "," . 
                "getDate()," . 
                (valoresCheck('ckbInativo') == 0 ? valoresCheck('ckbEmailMarketing') : 0) . "," .
                (valoresCheck('ckbInativo') == 0 ? valoresCheck('ckbCelularMarketing') : 0) . "," .
                valoresTexto('txtNmFantasia') . "," .
                $_SESSION["id_usuario"] . "," .
                valoresTexto('txtGymPassId') . "," .
                valoresSelect('txtTermometro') . "," .
                valoresTexto('txtOrg') . "," .
                valoresTexto('txtTpSang') . "," .
                valoresSelect('txtProfResp') . "," .                
                valoresSelect('txtRegTributario') . "," . $convertCli . "," . $convertCli . "," .
                valoresTexto('txtInsMunicipal') . "," .
                valoresTexto('txtNumCnh') . "," .
                valoresTexto('txtCatCnh') . "," .
                valoresData('txtVencCnh') . "," .
                valoresData('txtPriCnh') . "," .
                valoresCheck('ckbISS') . ")";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_fornecedores_despesas from sf_fornecedores_despesas order by id_fornecedores_despesas desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        values ('sf_clientes', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'I', 'INCLUIR - PROSPECT', GETDATE(), " . $_POST['txtId'] . ")");        
        if (valoresSelect('txtUserResp') == "null") {
            odbc_exec($con, "update sf_configuracao set id_ultimo_usuario = isnull((select top 1 id from sf_configuracao_usuarios where id > 
            (select isnull(id_ultimo_usuario, 0) from sf_configuracao) order by id), (select min(id) from sf_configuracao_usuarios));") or die(odbc_errormsg());
            odbc_exec($con, "update sf_fornecedores_despesas set id_user_resp = (select u.id_usuario from sf_configuracao c inner join sf_configuracao_usuarios u on c.id_ultimo_usuario = u.id) where id_fornecedores_despesas = " . $_POST['txtId']);
        }
    }
    if (isset($_POST['txtQtdContatos'])) {
        $max = $_POST['txtQtdContatos'];
        $notIn = '0';
        for ($i = 0; $i <= $max; $i++) {
            if (isset($_POST['txtIdContato_' . $i]) && is_numeric($_POST['txtIdContato_' . $i])) {
                if ($_POST['txtIdContato_' . $i] != 0) {
                    $notIn = $notIn . "," . $_POST['txtIdContato_' . $i];
                }
            }
        }
        odbc_exec($con, "delete from sf_fornecedores_despesas_contatos where fornecedores_despesas = " . $_POST['txtId'] . " and id_contatos not in (" . $notIn . ")");
        for ($i = 0; $i <= $max; $i++) {
            if (isset($_POST['txtIdContato_' . $i])) {
                if ($_POST['txtIdContato_' . $i] !== "0") {
                    
                } else {
                    odbc_exec($con, "insert into sf_fornecedores_despesas_contatos(fornecedores_despesas,tipo_contato,conteudo_contato,resp_contato) values(" . $_POST['txtId'] . "," . $_POST['txtTpContato_' . $i] . "," . valoresTexto('txtTxContato_' . $i) . "," . valoresTexto('txtTxResponsavel_' . $i) . ")") or die(odbc_errormsg());
                }
            }
        }
        echo $_POST['txtId'];
    }
}

if (isset($_POST['btnTrocaUsuarios'])) {
    $usuarios = [];
    foreach($_POST['txtIdItem'] as $usuario) {
        $usuarios[] = $usuario;
    }
    if (count($usuarios) > 0) {
        $i = 0;
        $query = "";
        $cur = odbc_exec($con, "select id_fornecedores_despesas from sf_fornecedores_despesas where tipo = 'P' and id_user_resp = " . valoresSelect("txtUserResp"));
        while ($RFP = odbc_fetch_array($cur)) {
            $query .= "update sf_fornecedores_despesas set id_user_resp = " . $usuarios[$i] . " where id_fornecedores_despesas = " . $RFP['id_fornecedores_despesas'] . ";" .
            "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values ('sf_fornecedores_despesas', " . valoresSelect("txtUserResp") . ", '" . $_SESSION["login_usuario"] . "', 'A', 'TRANSFERENCIA RESPONSAVEL De: " . valoresSelect("txtUserResp") . " Para: " . $usuarios[$i] . "', GETDATE(), " . $RFP['id_fornecedores_despesas'] . ");";            
            $i = (($i + 1) < count($usuarios) ? ($i + 1) : 0);
        }
        odbc_exec($con, $query);
    }    
    echo "YES"; exit;
}

if (isset($_POST['btnAprovProspect'])) {
    if (is_numeric($_POST['txtId'])) {
        odbc_exec($con, "update sf_fornecedores_despesas set dt_aprov_prospect = GETDATE() where dt_aprov_prospect is null and id_fornecedores_despesas = " . $_POST['txtId']);
        echo $_POST['txtId'];
    }
}

if (isset($_POST['btnConvCliente'])) {
    if (is_numeric($_POST['txtId'])) {
        odbc_exec($con, "EXEC dbo.SP_MK_COMISSAO_REGRAS @id_cliente = " . $_POST['txtId'] . ", @id_regra = 0;");        
        odbc_exec($con, "update sf_fornecedores_despesas set 
        dt_convert_cliente = GETDATE(), dt_aprov_prospect = GETDATE(), tipo = 'C' 
        where dt_convert_cliente is null and id_fornecedores_despesas = " . $_POST['txtId']);
        odbc_exec($con, "update sf_fornecedores_despesas set 
        dt_convert_cliente = GETDATE(), dt_aprov_prospect = GETDATE(), tipo = 'C' 
        where dt_convert_cliente is null and id_fornecedores_despesas in 
        (select id_dependente from sf_fornecedores_despesas_dependentes where id_titular = " . $_POST['txtId'] . ")");               
        odbc_exec($con, "UPDATE sf_fornecedores_despesas set fornecedores_status = dbo.FU_STATUS_CLI(id_fornecedores_despesas) where id_fornecedores_despesas in 
        (select id_dependente from sf_fornecedores_despesas_dependentes where id_titular = " . $_POST['txtId'] . ")");          
        //------------------------------------------------------------------------------------------------------------------------------------------------        
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        values ('sf_fornecedores_despesas', null, '" . $_SESSION["login_usuario"] . "', 'C', 'CONVERSAO - CLIENTE', GETDATE(), " . $_POST['txtId'] . ")");        
        $count = 0;
        $cur = odbc_exec($con, "select id, id_lead, indicador_lead, conv_pro_cli from sf_lead 
        inner join sf_lead_campanha on id_procedencia = procedencia
        where getdate() between data_ini and data_fim and sf_lead_campanha.inativo = 0 and sf_lead.inativo = 0 
        and indicador_lead is not null and prospect = " . $_POST['txtId']);
        while ($RFP = odbc_fetch_array($cur)) {
            $id = utf8_encode($RFP['id']);
            $id_lead = utf8_encode($RFP['id_lead']);            
            $id_indicador = utf8_encode($RFP['indicador_lead']);
            $count = utf8_encode($RFP['conv_pro_cli']);
        }
        for ($i = 0; $i < $count; $i++) {
            $cur = odbc_exec($con, "EXEC dbo.SP_LEAD_CUPOM @campanha = " . $id . ", @lead = " . $id_indicador . ", @lead_origem = " . $id_lead . ";");
            $idcupom = odbc_result($cur, 1);
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------                
        $id_padrao = "21";        
        include "../../Sivis/ws_disparo_imediato.php";   
        //------------------------------------------------------------------------------------------------------------------------------------------------        
        echo $_POST['txtId'];
    }
}

if (isset($_POST['btnExcluir'])) {    
    if (is_numeric($_POST['txtId'])) {
        $delTitularDependente = "";
        $cur = odbc_exec($con, "select * from sf_configuracao");
        while ($RFP = odbc_fetch_array($cur)) {
            if ($RFP['ACA_DEL_TITU_DEP'] > 0) {
                $delTitularDependente = " or id_fornecedores_despesas in (select id_dependente from sf_fornecedores_despesas_dependentes where id_titular = " . $_POST['txtId'] . ")";
            }
        }
        odbc_exec($con, "UPDATE sf_fornecedores_despesas set inativo = 1 where id_fornecedores_despesas = " . $_POST['txtId'] . $delTitularDependente);
        odbc_exec($con, "UPDATE sf_fornecedores_despesas set fornecedores_status = dbo.FU_STATUS_CLI(" . $_POST['txtId'] . ") where id_fornecedores_despesas = " . $_POST['txtId'] . $delTitularDependente);
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        values ('sf_clientes', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'E', 'EXCLUIR - PROSPECT', GETDATE(), " . $_POST['txtId'] . ")");
        echo $_POST['txtId'];
    }
}

if (is_numeric($_GET['id'])) {
    $disabled = 'disabled';
    $cur = odbc_exec($con, "select A.id_fornecedores_despesas id,*, 
    case when A.tipo = 'C' then dbo.FU_STATUS_CLI_ERP(A.id_fornecedores_despesas) else dbo.FU_STATUS_PRO(A.id_fornecedores_despesas) end statusAluno,
    (select razao_social from sf_fornecedores_despesas as y where y.id_fornecedores_despesas = A.prof_resp) prof_atual
    from sf_fornecedores_despesas A left join sf_usuarios on A.usuario_cadastro = sf_usuarios.id_usuario
    where A.id_fornecedores_despesas = " . $_GET['id']);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id']);
        $idTitular = utf8_encode($RFP['id']);
        $tipoCli = utf8_encode($RFP['tipo']);
        $nome = utf8_encode($RFP['razao_social']);
        $cpf = utf8_encode($RFP['cnpj']);
        $rg = utf8_encode($RFP['inscricao_estadual']);
        $dtCadastro = escreverData($RFP['dt_cadastro']);
        $statusAluno = utf8_encode($RFP['statusAluno']);        
        $dtAprProspect = escreverData($RFP['dt_aprov_prospect']);
        $dtConvCliente = escreverData($RFP['dt_convert_cliente']);
        $dtNascimento = escreverData($RFP['data_nascimento']);
        $sexo = utf8_encode($RFP['sexo']);
        $orgExp = utf8_encode($RFP['org_exped_rg']);   
        $tpsang = utf8_encode($RFP['tipo_sang']);            
        $planoSaude = utf8_encode($RFP['plano_saude']);
        $nmEmergencia = utf8_encode($RFP['telefone_ps1']);
        $telEmergencia = utf8_encode($RFP['telefone_ps2']);
        $procedencia = utf8_encode($RFP['procedencia']);        
        $estadoCivil = utf8_encode($RFP['estado_civil']);
        $profissao = utf8_encode($RFP['profissao']);
        $nmEmpresa = utf8_encode($RFP['trabalha_empresa']);
        $ckempresa = utf8_encode($RFP['empresa']);        
        $cep = utf8_encode($RFP['cep']);
        $endereco = utf8_encode($RFP['endereco']);
        $numero = utf8_encode($RFP['numero']);
        $complemento = utf8_encode($RFP['complemento']);
        $bairro = utf8_encode($RFP['bairro']);
        $estado = utf8_encode($RFP['estado']);
        $cidade = utf8_encode($RFP['cidade']);
        $user_resp = utf8_encode($RFP['id_user_resp']);        
        $indicador = utf8_encode($RFP['indicador']);
        $profResp = utf8_encode($RFP['prof_resp']);
        $profRespNome = utf8_encode($RFP['prof_atual']);
        $Obs = utf8_encode($RFP['Observacao']);        
        $acessoTipo = utf8_encode($RFP['acesso_tipo']);
        $acessoNumProv = utf8_encode($RFP['acesso_numero_provisorio']);
        $acessomsg = utf8_encode($RFP['acesso_msg']);        
        $grupoPessoa = utf8_encode($RFP['grupo_pessoa']);
        $juridico_tipo = utf8_encode($RFP['juridico_tipo']);        
        $inativo = utf8_encode($RFP['bloqueado']);
        $emailMarketing = utf8_encode($RFP['email_marketing']);
        $celularMarketing = utf8_encode($RFP['celular_marketing']);
        $nmFantasia = utf8_encode($RFP['nome_fantasia']);
        $abas = "";
        $usuario_cadastro = utf8_encode($RFP['login_user']);
        $regimetributario = utf8_encode($RFP['regime_tributario']);
        $gymPassId = utf8_encode($RFP['gyp_idaluno']);      
        $termometro = utf8_encode($RFP['termometro']);
        $insMunicipal = utf8_encode($RFP['inscricao_municipal']);
        $numCnh = utf8_encode($RFP['num_cnh']);
        $catCnh = utf8_encode($RFP['cat_cnh']);
        $vencCnh = escreverData($RFP['ven_cnh']);
        $priCnh = escreverData($RFP['pri_cnh']);  
        $nfe_iss = $RFP['nfe_iss'];
        $id_pessoa = utf8_encode($RFP['id_fornecedores_despesas']);
    }
    if (strlen($statusAluno) > 0) {
        odbc_exec($con, "update sf_fornecedores_despesas set fornecedores_status = " . valoresTexto2($statusAluno) . " where id_fornecedores_despesas = " . $_GET['id']);
    }
    if ($typePage == "Prospect") {
        $txtPage = "ProspectsForm.php";
        if ($tipoCli == 'C' && substr($_SESSION["modulos"], 6, 1) == 1) {
            header('Location: ../../Modulos/Academia/ClientesForm.php?id=' . $id);
        }
    } else {
        $txtPage = "CRMForm.php";
    }
}

$cur = odbc_exec($con, "select * from sf_configuracao where id = 1");
while ($RFP = odbc_fetch_array($cur)) {
    $gymPass_id = $RFP['gyp_recid'];
    $prospectIr = $RFP['ACA_PROSP_CLI'];
    $cat_topdata_lista = $RFP['cat_topdata_lista'];
    $aca_prosp_pag = $RFP['aca_prosp_pag'];
}