<?php

include "../../../Connections/configini.php";

if (isset($_POST['bntSave'])) {

    $id = "";
    if (is_numeric($_POST['txtIdx'])) {
        $id = $_POST['txtIdx'];
        $query = "SET DATEFORMAT DMY;" .
                "UPDATE [sf_telemarketing] SET " .
                "[atendimento_servico] = " . valoresNumericos("txtAS") . "," .
                "[pessoa] = " . valoresNumericos("txtId") . "," .
                "[data_tele] = " . valoresDataHora("txtData", "txtHora") . "," .
                "[especificacao] = " . valoresTexto("txtTipoChamado") . "," .
                "[de_pessoa] = " . valoresTexto("txtDeHistorico") . "," .
                "[para_pessoa] = " . valoresSelect("txtPara") . "," .
                "[proximo_contato] = " . valoresDataHora("txtDataProximo", "txtHoraProximo") . "," .
                "[obs] = " . valoresTexto("txtObs") . "," .
                "[relato] = " . valoresTexto("txtRelato") . "," .
                "[acompanhar] = " . valoresCheck("ckbAcompanhar") . "," .
                "[garantia] = " . valoresCheck("ckbGarantia") . "," .
                "[nomecon] = " . valoresTexto("txtNomeCon") . "," .
                "[departamento] = " . valoresSelect("txtDepartamento") . "," .
                "[nota_fiscal] = " . valoresTexto("txtNotaFiscal") . "," .
                "[data_nf] = " . valoresData("txtDataNota") . "," .
                "[pendente] = 1 " .
                "WHERE id_tele = " . $_POST['txtIdx'];

        odbc_exec($con, $query) or die(odbc_errormsg());
        $query = "0";
        if (isset($_POST['txtEspecificacao'])) {
            for ($i = 0; $i < count($_POST['txtEspecificacao']); $i++) {
                if ($_POST['txtEspecificacao'][$i] != '') {
                    $query = $query . "," . $_POST['txtEspecificacao'][$i];
                }
            }
            odbc_exec($con, "delete from sf_telemarketing_item where id_telemarketing = " . $_POST['txtIdx'] . " and id_especificacao not in (" . $query . ")") or die(odbc_errormsg());
            $ToAdd[0] = "";
            $w = 0;
            $cur = odbc_exec($con, "select te.id_especificacao x ,ti.id_especificacao y from sf_telemarketing_especificacao te left join
            sf_telemarketing_item ti on te.id_especificacao = ti.id_especificacao and ti.id_telemarketing = " . $_POST['txtIdx'] . "
            where te.id_especificacao in (" . $query . ")");
            while ($RFP = odbc_fetch_array($cur)) {
                if ($RFP['y'] == null) {
                    $ToAdd[$w] = $RFP['x'];
                    $w++;
                }
            }
            for ($j = 0; $j < $w; $j++) {
                $query = "insert into sf_telemarketing_item(id_telemarketing,id_especificacao) values(" . $_POST['txtIdx'] . "," . $ToAdd[$j] . ")";
                odbc_exec($con, $query);
            }
        }
    } else {
        $query = "SET DATEFORMAT DMY;
       INSERT INTO [sf_telemarketing] ([atendimento_servico],
       [pessoa],[data_tele],[especificacao],[de_pessoa],[para_pessoa],
       [proximo_contato],[obs],[relato],[acompanhar],[garantia],
       [nomecon],[departamento],[nota_fiscal],[data_nf],[pendente],[dt_inclusao])VALUES(" .
                valoresNumericos("txtAS") . "," .
                valoresNumericos("txtId") . "," .
                valoresDataHora("txtData", "txtHora") . "," .
                valoresTexto("txtTipoChamado") . "," .
                valoresTexto("txtDeHistorico") . "," .
                valoresSelect("txtPara") . "," .
                valoresDataHora("txtDataProximo", "txtHoraProximo") . "," .
                valoresTexto("txtObs") . "," .
                valoresTexto("txtRelato") . "," .
                valoresCheck("ckbAcompanhar") . "," .
                valoresCheck("ckbGarantia") . "," .
                valoresTexto("txtNomeCon") . "," .
                valoresSelect("txtDepartamento") . "," .
                valoresTexto("txtNotaFiscal") . "," .
                valoresData("txtDataNota") . "," .
                "1, getdate())";

        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_tele from sf_telemarketing order by id_tele desc") or die(odbc_errormsg());
        $id = odbc_result($res, 1);
        if (isset($_POST['txtEspecificacao'])) {
            for ($i = 0; $i < count($_POST['txtEspecificacao']); $i++) {
                if ($_POST['txtEspecificacao'][$i] != '') {
                    $query = "insert into sf_telemarketing_item(id_telemarketing,id_especificacao) values(" . $id . "," . $_POST['txtEspecificacao'][$i] . ")";
                    odbc_exec($con, $query);
                }
            }
        }
    }
    if (is_numeric($id)) {
        $id_venda = $_POST['txtIdVenda'];
        $prod_grp = $_POST['txtPD'];
        $prod_grpS = $_POST['txtPDS'];
        if (!is_numeric($id_venda) && (sizeof($prod_grp) > 0 || sizeof($prod_grpS) > 0)) {
            $query = "insert into sf_vendas(vendedor,cliente_venda,historico_venda,data_venda,sys_login,empresa, destinatario,[status],cov,descontop,descontos, descontotp,descontots,n_servicos,grupo_conta,conta_movimento,favorito, validade, reservar_estoque)
            values('" . $_SESSION["login_usuario"] . "'," . valoresNumericos("txtId") . "," . valoresTexto2("ORÇAMENTO") . ",GETDATE(),'" . $_SESSION["login_usuario"] . "',1,'" . $_SESSION["login_usuario"] . "','Aguarda','O',0,0,0,0,0,14,1,0," . valoresData('txtDtValidade') . ", " . valoresCheck('txtReservarEstoque') . ")";
            odbc_exec($con, $query);
            $res = odbc_exec($con, "select top 1 id_venda from sf_vendas order by id_venda desc") or die(odbc_errormsg());
            $id_venda = odbc_result($res, 1);
            odbc_exec($con, "update sf_telemarketing set id_venda = " . $id_venda . " where id_tele = " . $id);
        }

        if (is_numeric($id_venda)) {
            $query = "update sf_vendas set validade = " . valoresData('txtDtValidade') . ", reservar_estoque = " . valoresCheck('txtReservarEstoque') . " where id_venda = " . $id_venda;
            odbc_exec($con, $query) or die(odbc_errormsg());

            $notIn = '0';
            for ($i = 0; $i < sizeof($prod_grp); $i++) {
                if (is_numeric($prod_grp[$i])) {
                    if (is_numeric($_POST['txtIP'][$i])) {
                        $notIn .= "," . $_POST['txtIP'][$i];
                    }
                }
            }
            for ($i = 0; $i < sizeof($prod_grpS); $i++) {
                if (is_numeric($prod_grpS[$i])) {
                    if (is_numeric($_POST['txtIPS'][$i])) {
                        $notIn .= "," . $_POST['txtIPS'][$i];
                    }
                }
            }
            odbc_exec($con, "delete from sf_vendas_itens where id_venda = " . $id_venda . " and id_item_venda not in (" . $notIn . ")");
            for ($i = 0; $i < sizeof($prod_grp); $i++) {
                if (is_numeric($prod_grp[$i])) {
                    if (is_numeric($_POST['txtIP'][$i])) {
                        odbc_exec($con, "update sf_vendas_itens set grupo = " . valoresSelect2($_POST['txtGR'][$i]) . ",produto = " . $prod_grp[$i] . ",quantidade = " . valoresNumericos2($_POST['txtVQ'][$i]) . " ,valor_total = " . valoresNumericos2($_POST['txtVP'][$i]) . " where id_item_venda = " . $_POST['txtIP'][$i]) or die(odbc_errormsg());
                    } else {
                        odbc_exec($con, "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total) values(" . $id_venda . "," . valoresSelect2($_POST['txtGR'][$i]) . "," . $prod_grp[$i] . "," . valoresNumericos2($_POST['txtVQ'][$i]) . "," . valoresNumericos2($_POST['txtVP'][$i]) . ")");
                    }
                }
            }
            for ($i = 0; $i < sizeof($prod_grpS); $i++) {
                if (is_numeric($prod_grpS[$i])) {
                    if (is_numeric($_POST['txtIPS'][$i])) {
                        odbc_exec($con, "update sf_vendas_itens set grupo = " . valoresSelect2($_POST['txtGRS'][$i]) . ",produto = " . $prod_grpS[$i] . ",quantidade = " . valoresNumericos2($_POST['txtVQS'][$i]) . " ,valor_total = " . valoresNumericos2($_POST['txtVPS'][$i]) . " where id_item_venda = " . $_POST['txtIPS'][$i]) or die(odbc_errormsg());
                    } else {
                        odbc_exec($con, "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total) values(" . $id_venda . "," . valoresSelect2($_POST['txtGRS'][$i]) . "," . $prod_grpS[$i] . "," . valoresNumericos2($_POST['txtVQS'][$i]) . "," . valoresNumericos2($_POST['txtVPS'][$i]) . ")") or die(odbc_errormsg());
                    }
                }
            }
        }
    }
    echo "YES";
}

if (isset($_POST['bntAddMensagem'])) {
    if (is_numeric($_POST['txtIdx']) && is_numeric($_SESSION["id_usuario"])) {
        if (is_numeric($_POST['txtId'])) {
            $query = "SET DATEFORMAT DMY;" .
                    "UPDATE sf_mensagens_telemarketing SET " .
                    "id_usuario_de = " . $_SESSION["id_usuario"] . "," .
                    "id_usuario_para = " . valoresSelect("txtMotivo") . "," .
                    "data = " . valoresDataHora("txtDataFechar", "txtHoraFechar") . "," .
                    "mensagem = " . valoresTexto("txtObsFechamento") . " WHERE id = " . $_POST['txtId'];
            //echo $query;exit();
            odbc_exec($con, $query) or die(odbc_errormsg());
        } else {
            $query = "SET DATEFORMAT DMY;" .
                    "insert into sf_mensagens_telemarketing (id_usuario_de, id_tele, id_usuario_para,data,mensagem,dt_inclusao) values(" .
                    $_SESSION["id_usuario"] . "," .
                    $_POST['txtIdx'] . "," .
                    valoresSelect("txtMotivo") . "," .
                    valoresDataHora("txtDataFechar", "txtHoraFechar") . "," .
                    valoresTexto("txtObsFechamento") . ", getdate())";
            //echo $query;exit();
            odbc_exec($con, $query) or die(odbc_errormsg());
        }
        echo "YES";
    }
}

if (isset($_POST['bntDelMensagem'])) {
    if (is_numeric($_POST['txtIdx'])) {
        $query = "DELETE FROM sf_mensagens_telemarketing WHERE id = " . $_POST['txtIdx'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        echo "YES";
    }
}

if (isset($_POST['bntFecharChamado'])) {
    if (is_numeric($_POST['txtIdx'])) {
        $query = "SET DATEFORMAT DMY;" .
                "UPDATE [sf_telemarketing] SET " .
                "[pendente] = 0," .
                "[de_pessoa_fechamento] = " . $_SESSION["id_usuario"] . "," .
                "[data_tele_fechamento] = " . valoresDataHora("txtDataFechar", "txtHoraFechar") . "," .
                "[obs_fechamento] = " . valoresTexto("txtObsFechamento") . "," .
                "[procedencia] = " . valoresSelect("txtMotivo") . " " .
                "WHERE id_tele = " . $_POST['txtIdx'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        echo "YES";
    }
}

if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtIdx'])) {
        $sql = "select ISNULL(COUNT(*),0) total from sf_vendas where id_venda in(select id_venda from sf_telemarketing where id_tele = " . $_POST['txtIdx'] . ")";
        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            if ($RFP['total'] > 0) {
                echo "VENDA";
            } else {
                odbc_exec($con, "DELETE FROM sf_telemarketing_item WHERE id_telemarketing = " . $_POST['txtIdx']);
                odbc_exec($con, "DELETE FROM sf_mensagens_telemarketing WHERE id_tele = " . $_POST['txtIdx']);
                odbc_exec($con, "DELETE FROM sf_telemarketing WHERE id_tele = " . $_POST['txtIdx']);
                echo "YES";
            }
        }
    }
}

if ($_POST['txtQualificacao'] == "") {
    if (is_numeric($_POST['txtId'])) {
        odbc_exec($con, "UPDATE sf_fornecedores_despesas SET dt_aprov_prospect = GETDATE() WHERE dt_aprov_prospect is null and id_fornecedores_despesas = " . $_POST['txtId']);
    }
}

if (isset($_REQUEST['bntFind']) && is_numeric($_REQUEST['txtIdx'])) {
    $dados = array();
    $sql = "select * from sf_telemarketing where id_tele = " . $_REQUEST['txtIdx'];
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $dados[] = array('id_tele' => utf8_encode($RFP['id_tele'])
            , 'de_pessoa' => utf8_encode($RFP['de_pessoa'])
            , 'para_pessoa' => utf8_encode($RFP['para_pessoa'])
            , 'data_tele' => escreverData($RFP['data_tele'])
            , 'data_teleh' => escreverData($RFP['data_tele'], 'H:i')
            , 'pendente' => utf8_encode($RFP['pendente'])
            , 'proximo_contato' => escreverData($RFP['proximo_contato'])
            , 'proximo_contatoh' => escreverData($RFP['proximo_contato'], 'H:i')
            , 'obs' => utf8_encode($RFP['obs'])
            , 'pessoa' => utf8_encode($RFP['pessoa'])
            , 'data_tele_fechamento' => escreverData($RFP['data_tele_fechamento'])
            , 'data_tele_fechamentoh' => escreverData($RFP['data_tele_fechamento'], 'H:i')
            , 'obs_fechamento' => utf8_encode($RFP['obs_fechamento'])
            , 'de_pessoa_fechamento' => utf8_encode($RFP['de_pessoa_fechamento'])
            , 'especificacao' => utf8_encode($RFP['especificacao'])
            , 'atendimento_servico' => utf8_encode($RFP['atendimento_servico'])
            , 'garantia' => utf8_encode($RFP['garantia'])
            , 'nota_fiscal' => utf8_encode($RFP['nota_fiscal'])
            , 'data_nf' => utf8_encode($RFP['data_nf'])
            , 'valor_orcamento' => utf8_encode($RFP['valor_orcamento'])
            , 'relato' => utf8_encode($RFP['relato'])
            , 'dias_garantia' => utf8_encode($RFP['dias_garantia'])
            , 'nomecon' => utf8_encode($RFP['nomecon'])
            , 'departamento' => utf8_encode($RFP['departamento'])
            , 'procedencia' => utf8_encode($RFP['procedencia'])
            , 'origem' => utf8_encode($RFP['origem'])
            , 'numficha' => utf8_encode($RFP['numficha'])
            , 'acompanhar' => utf8_encode($RFP['acompanhar'])
        );
    }
    echo(json_encode($dados));
}

$id = '';
$de = $_SESSION["id_usuario"];
$operador = $_SESSION["login_usuario"];
$nomecontato = '';
$data = getData("T");
$hora = date("H:i");
$relato = '';
$obs = '';
$departamento = '';
$para = $_SESSION["id_usuario"];
$dataProximo = getData("T");
$horaProximo = date("H:i");
$nota_fiscal = '';
$data_nota = '';
$ckb_garantia = '';
$ckbAcompanhar = 0;
$tipoChamado = "T";
$tp_venda = "";
$id_venda = "";
$dtValidade = "";
$reservar_estoque = "";

if (is_numeric($_GET['id'])) {
    $cur = odbc_exec($con, "select id_tele,de_pessoa,u.login_user,para_pessoa,data_tele,pendente,proximo_contato,t.obs,id_fornecedores_despesas,tipo,us.login_user login_user_fechamento,data_tele_fechamento,obs_fechamento,especificacao,razao_social,nome_fantasia,
    garantia,atendimento_servico,nota_fiscal,data_nf,valor_orcamento,relato,dias_garantia,t.departamento,nomecon,t.procedencia,t.origem,t.numficha,bloqueado,acompanhar, id_venda
    from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa
    LEFT JOIN sf_usuarios us on us.id_usuario = t.de_pessoa_fechamento
    LEFT JOIN sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = t.pessoa where id_tele = " . $_GET['id']);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_tele']);
        $de = utf8_encode($RFP['de_pessoa']);
        $operador = utf8_encode($RFP['login_user']);
        $nomecontato = utf8_encode($RFP['nomecon']);
        $data = escreverData($RFP['data_tele']);
        $hora = escreverData($RFP['data_tele'], 'H:i');
        $relato = utf8_encode($RFP['relato']);
        $obs = utf8_encode($RFP['obs']);
        $departamento = utf8_encode($RFP['departamento']);
        $para = utf8_encode($RFP['para_pessoa']);
        $dataProximo = escreverData($RFP['proximo_contato']);
        $horaProximo = escreverData($RFP['proximo_contato'], 'H:i');
        $nota_fiscal = utf8_encode($RFP['nota_fiscal']);
        $data_nota = escreverData($RFP['data_nf']);
        $ckb_garantia = utf8_encode($RFP['garantia']);
        $ckbAcompanhar = utf8_encode($RFP['acompanhar']);
        $tipoChamado = utf8_encode($RFP['especificacao']);
        $id_venda = utf8_encode($RFP['id_venda']);
    }
    if (is_numeric($id_venda)) {
        $cur = odbc_exec($con, "select case when cov = 'C' then 'COMPRA:' when cov = 'O' then 'ORÇAMENTO:' when cov = 'P' then 'PEDIDO:' when cov = 'V' then 'VENDA:' END tipo, validade, reservar_estoque from sf_vendas where id_venda = " . $id_venda);
        while ($RFP = odbc_fetch_array($cur)) {
            $tp_venda = $RFP['tipo'];
            $dtValidade = $RFP['validade'];
            $reservar_estoque = $RFP['reservar_estoque'];
        }
    }
}

if (isset($_POST['bntSave'])) {
    odbc_exec($con, "insert into sf_serasa (id_fornecedor,usuario,data_lanc,tipo,observacao)
    values (" . valoresNumericos("txtId") . ", '" . $_SESSION["login_usuario"] . "', GETDATE(),'M'," . valoresTexto("txtRelato") . ")");
}

if (isset($_POST['getStatus']) && is_numeric($_POST['getStatus'])) {
    odbc_exec($con, "update sf_fornecedores_despesas set fornecedores_status = dbo.FU_STATUS_PRO(id_fornecedores_despesas) where tipo = 'P' and id_fornecedores_despesas = " . $_POST['getStatus']);
    $cur = odbc_exec($con, "select fornecedores_status from sf_fornecedores_despesas where tipo = 'P' and id_fornecedores_despesas = " . $_POST['getStatus']) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        echo $RFP['fornecedores_status'];
    }
}