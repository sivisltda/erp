<?php

include "../../../Connections/configini.php";

if (isset($_POST['bntSave'])) {
    $query = "insert into sf_configuracao_vencimentos(dia, de, ate, minimo, pro_rata) values (" . 
    valoresNumericos('txtDiaVencimento') . "," .
    valoresNumericos('txtDiaDeVencimento') . "," .
    valoresNumericos('txtDiaAteVencimento') . "," .
    valoresNumericos('txtTpVeMin') . "," .
    valoresNumericos('txtTpVePr') . ")";
    odbc_exec($con, $query);
    echo "YES";
}

if (isset($_POST['btnDel'])) {
    if (is_numeric($_POST['txtId'])) {
        odbc_exec($con, "DELETE FROM sf_configuracao_vencimentos WHERE id = " . $_POST['txtId']);
        echo "YES";
    }
}

odbc_close($con);
