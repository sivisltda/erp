<?php

$disabled = 'disabled';
$id = '';
$descricao = '';
$assunto = '';
$tipo = 0;
$mensagem = '';
$image = "0";
$anexo = "";
$isAnexo = "0";
$pasta = "./../../Pessoas/" . $contrato . "/Email/";

if (isset($_POST['btnDelAnexo'])) {
    $arquivo = $pasta . "/" . $_POST['txtId'] . "/" . $_POST['txtNmArquivo'];
    if (file_exists(iconv('utf-8', 'cp1252', $arquivo))) {
        unlink(iconv('utf-8', 'cp1252', $arquivo));
    }
    odbc_exec($con, "update sf_emails set anexo = '' where id_email =" . $_POST['txtId']);
}

if (isset($_POST['btnNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if (isset($_POST['btnSave'])) {
    $anexo = "";
    if ($_FILES['anexo']['name']) {
        $anexo = ", anexo='" . $_FILES['anexo']['name'] . "'";
    }
    
    if ($_POST['txtId'] != '') {
        $Query = "UPDATE sf_emails SET
        descricao = UPPER(" . valoresTexto('txtDescricao') . "),
        assunto = " . valoresTexto('txtAssunto') . ",
        tipo = " . valoresSelect('txtTipo') . ",
        mensagem = " . valoresTexto('ckeditor' . (valoresSelect('txtTipo') == "0" ? "" : "2")) . 
        $anexo . " WHERE id_email = " . $_POST['txtId'];
        odbc_exec($con, $Query) or die(odbc_errormsg());
    } else {
        $Query = "INSERT INTO sf_emails(descricao, assunto, tipo, mensagem, anexo)VALUES
        (UPPER(" . valoresTexto('txtDescricao') . ")," . valoresTexto('txtAssunto') . ", " . valoresSelect('txtTipo') . ", " . 
        valoresTexto('ckeditor' . (valoresSelect('txtTipo') == "0" ? "" : "2")) . ",'" . utf8_decode($_FILES['anexo']['name']) . "')";
        odbc_exec($con, $Query) or die(odbc_errormsg());
        $res = odbc_exec($con, "SELECT TOP 1 id_email FROM sf_emails ORDER BY id_email DESC") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }

    if (!is_dir($pasta . "/" . $_POST['txtId'] . "/")) {
        mkdir($pasta . "/" . $_POST['txtId'] . "/");
    }

    $nome = $_FILES['arquivo']['name'];
    $type = $_FILES['arquivo']['type'];
    $size = $_FILES['arquivo']['size'];
    $tmp = $_FILES['arquivo']['tmp_name'];
    if ($tmp) {
        move_uploaded_file($tmp, $pasta . "/" . $_POST['txtId'] . "/" . $_POST['txtId'] . ".jpg");
    }
    $nome = utf8_decode($_FILES['anexo']['name']);
    $type = $_FILES['anexo']['type'];
    $size = $_FILES['anexo']['size'];
    $tmp = $_FILES['anexo']['tmp_name'];
    if ($tmp) {
        move_uploaded_file($tmp, $pasta . "/" . $_POST['txtId'] . "/" . $nome);
    }
}

if (isset($_POST['btnDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        if (isset($_POST['List'])) {
            include "../../../Connections/configini.php";
        }
        odbc_exec($con, "DELETE FROM sf_emails WHERE id_email = " . $_POST['txtId']);
        deleteDirectory($pasta . "/" . $_POST['txtId']);
        if (isset($_POST['List'])) {
            echo "YES";
        } else {
            echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox(1);</script>";
        }
        odbc_close($con);
        exit();
    }
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select *, substring(mensagem,1,4000) mensagem1" .
                ", substring(mensagem,4001,4000) mensagem2" .
                ", substring(mensagem,8001,4000) mensagem3" .
                ", substring(mensagem,12001,4000) mensagem4" .
                ", substring(mensagem,16001,4000) mensagem5 " .
                ", substring(mensagem,20001,4000) mensagem6 " .
                ", substring(mensagem,24001,4000) mensagem7 " .
                ", substring(mensagem,28001,4000) mensagem8 " .
                ", substring(mensagem,32001,4000) mensagem9 " .
                ", substring(mensagem,36001,4000) mensagem10 " .
                ", substring(mensagem,40001,4000) mensagem11 " .
                ", substring(mensagem,44001,4000) mensagem12 " .
                ", substring(mensagem,48001,4000) mensagem13 " .
                ", substring(mensagem,52001,4000) mensagem14 " .
                ", substring(mensagem,56001,4000) mensagem15 " .
                ", substring(mensagem,60001,4000) mensagem16 " .
                ", substring(mensagem,64001,4000) mensagem17 " .
                ", substring(mensagem,68001,4000) mensagem18 " .
                ", substring(mensagem,72001,4000) mensagem19 " .
                ", substring(mensagem,76001,4000) mensagem20 " .
                ", substring(mensagem,80001,4000) mensagem21 " .
                ", substring(mensagem,84001,4000) mensagem22 " .
                ", substring(mensagem,88001,4000) mensagem23 " .
                ", substring(mensagem,92001,4000) mensagem24 " .
                ", substring(mensagem,96001,4000) mensagem25 " .
                ", substring(mensagem,100001,4000) mensagem26 " .
                ", substring(mensagem,104001,4000) mensagem27 " .
                ", substring(mensagem,108001,4000) mensagem28 " .
                ", substring(mensagem,112001,4000) mensagem29 " .
                ", substring(mensagem,116001,4000) mensagem30 " .
                ", substring(mensagem,120001,4000) mensagem31 " .
                ", substring(mensagem,124001,4000) mensagem32 " .
                ", substring(mensagem,128001,4000) mensagem33 " .
                ", substring(mensagem,132001,4000) mensagem34 " .
                ", substring(mensagem,136001,4000) mensagem35 " .
                ", substring(mensagem,140001,4000) mensagem36 " .
                ", substring(mensagem,144001,4000) mensagem37 " .
                ", substring(mensagem,150001,4000) mensagem38 " .
                ", substring(mensagem,154001,4000) mensagem39 " .
                ", substring(mensagem,158001,4000) mensagem40 from sf_emails where id_email =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_email'];
        $descricao = utf8_encode($RFP['descricao']);
        $assunto = utf8_encode($RFP['assunto']);
        $tipo = utf8_encode($RFP['tipo']);
        $mensagem = utf8_encode($RFP['mensagem1']) . utf8_encode($RFP['mensagem2']) . utf8_encode($RFP['mensagem3']) . utf8_encode($RFP['mensagem4']) . utf8_encode($RFP['mensagem5']) . utf8_encode($RFP['mensagem6']) . utf8_encode($RFP['mensagem7']) . utf8_encode($RFP['mensagem8']) . utf8_encode($RFP['mensagem9']) . utf8_encode($RFP['mensagem10']) .
        utf8_encode($RFP['mensagem11']) . utf8_encode($RFP['mensagem12']) . utf8_encode($RFP['mensagem13']) . utf8_encode($RFP['mensagem14']) . utf8_encode($RFP['mensagem15']) . utf8_encode($RFP['mensagem16']) . utf8_encode($RFP['mensagem17']) . utf8_encode($RFP['mensagem18']) . utf8_encode($RFP['mensagem19']) . utf8_encode($RFP['mensagem20']) .
        utf8_encode($RFP['mensagem21']) . utf8_encode($RFP['mensagem22']) . utf8_encode($RFP['mensagem23']) . utf8_encode($RFP['mensagem24']) . utf8_encode($RFP['mensagem25']) . utf8_encode($RFP['mensagem26']) . utf8_encode($RFP['mensagem27']) . utf8_encode($RFP['mensagem28']) . utf8_encode($RFP['mensagem29']) . utf8_encode($RFP['mensagem30']) .
        utf8_encode($RFP['mensagem31']) . utf8_encode($RFP['mensagem32']) . utf8_encode($RFP['mensagem33']) . utf8_encode($RFP['mensagem34']) . utf8_encode($RFP['mensagem35']) . utf8_encode($RFP['mensagem36']) . utf8_encode($RFP['mensagem37']) . utf8_encode($RFP['mensagem38']) . utf8_encode($RFP['mensagem39']) . utf8_encode($RFP['mensagem40']);
        $anexo = utf8_encode($RFP['anexo']);
    }

    if (file_exists($pasta . "/" . $PegaURL . "/" . $PegaURL . ".jpg")) {
        $image = "1";
    }
    if ($anexo !== "" && file_exists($pasta . "/" . $PegaURL . "/" . $anexo)) {
        $isAnexo = "1";
    }
} else {
    $disabled = '';
}

if (isset($_POST['btnEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}

if (isset($_POST['DelImage'])) {
    include "../../../Connections/configini.php";
    $pasta = "./../../../Pessoas/" . $contrato . "/Email/";
    $arquivo = $pasta . "/" . $_POST['DelImage'] . "/" . $_POST['DelImage'] . ".jpg";
    if (file_exists($arquivo)) {
        unlink($arquivo);
    }
    echo "YES";
    odbc_close($con);
}

if (isset($_GET['abrirArquivo'])) {
    include "../../../Connections/configini.php";
    $pasta = "./../../../Pessoas/" . $contrato . "/Email/";
    $arquivo = $pasta . "/" . $_GET['abrirArquivo'] . "/" . $_GET['nmArquivo'];

    header('Content-Type: application/octet-stream');
    header("Content-Transfer-Encoding: Binary");
    header("Content-disposition: attachment; filename=\"" . basename($arquivo) . "\"");
    readfile($arquivo);
    odbc_close($con);
}


if (isset($_GET['listModelosEmails'])) {
    include "../../../Connections/configini.php";
    include "../../../util/util.php";
    $local = array();
    if (is_numeric($_GET['tipo'])) {
        $cur = odbc_exec($con, "select id_email, descricao from sf_emails where tipo = " . $_GET['tipo']);
        while ($RFP = odbc_fetch_array($cur)) {
            $local[] = array('id_email' => tableFormato($RFP['id_email'], 'T', '', ''), 'descricao' => tableFormato($RFP['descricao'], 'T', '', ''));
        }
    }
    echo json_encode($local);
    odbc_close($con);
}

