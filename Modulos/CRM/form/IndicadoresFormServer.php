<?php

require_once(__DIR__ . '/../../../Connections/configini.php');

$disabled = '';
$id = '';
$nome = '';
$juridico_tipo = '';
$dtCadastro = getData("T");
$pis = '';
$cpf = '';
$rg = '';
$dtNascimento = '';
$sexo = '';
$estadoCivil = '';
$contato = '';
$ckempresa = '';
$data_admissao = '';
$data_demissao = '';
$grupoPessoa = '';
$cref = '';
$cep = '';
$endereco = '';
$numero = '';
$bairro = '';
$estado = '';
$cidade = '';
$complemento = '';
$Obs = '';
$acessoTipo = '';
$acessoNumProv = '';
$acessomsg = '';
$tipoSelect = 'F';
$departamento = '';
$profissao = '';
$nmEmpresa = '';
$user_resp = "";
$profResp = ""; 
$profRespNome = '';
$planoSaude = '';
$nmEmergencia = '';
$telEmergencia = '';
$procedencia = '';
$emailMarketing = '1';
$celularMarketing = '1';
$inativo = '0';
$regimetributario = '';
$referencia_ps1 = '';
$comiss_planoPri = '';
$comiss_gerePri = '';
$tbcomiss_prod_tipo = '';
$tbcomiss_ser_tipo = '';
$typePage= "Funcionario";
$insMunicipal = '';
$numCnh = '';
$catCnh = '';
$vencCnh = '';
$priCnh = '';
$nfe_iss = '';

if (isset($_POST['btnSave'])) {
    $mdl_aca_ = returnPart($_SESSION["modulos"], 6);
    $comissao_plano = "";
    $comissao_plano_cam = "";
    $comissao_plano_val = "";
    $query = "set dateformat dmy;";

    if (is_numeric($_POST['txtId'])) {
        if ($mdl_aca_ == 1) {
            $comissao_plano = "tbcomiss_plano = " . valoresSelect('selComPlano') . 
            ",comiss_plano = " . valoresNumericos('txtComPlano') . 
            ",comiss_planoPri = " . valoresNumericos('txtComPlanoPri') . ",";
        }  
        $query .= "update sf_fornecedores_despesas set " .
                "juridico_tipo = " . valoresTexto('txtJuridicoTipo') . "," .
                "razao_social = " . strtoupper(valoresTexto('txtNome')) . "," .
                "nome_fantasia = " . strtoupper(valoresTexto('txtNmFantasia')) . "," .
                "cnpj = " . valoresTexto('txtCpf') . "," .
                "inscricao_estadual = " . valoresTexto('txtRG') . "," .
                "data_nascimento = " . valoresData('txtDtNasc') . "," .
                "sexo = " . valoresTexto('txtSexo') . "," .
                "estado_civil = " . valoresTexto('txtEstadoCivil') . "," .
                "contato = " . valoresTexto('txtContato') . "," .
                "empresa = " . valoresTexto('txtFilial') . "," .
                "org_exped_rg = " . valoresTexto('txtOrg') . "," .
                "dt_admissao = " . valoresData('txtDtAdmissao') . "," .
                "dt_demissao = " . valoresData('txtDtDemissao') . "," .
                "grupo_pessoa = " . valoresSelect('txtGrupo') . ", " .
                "fornecedores_hr = " . valoresSelect('txtfornecedorHR') . ", " .
                "numcref = " . valoresTexto('txtCref') . "," .
                "cep = " . valoresTexto('txtCep') . "," .
                "endereco = " . valoresTexto('txtEndereco') . "," .
                "numero = " . valoresTexto('txtNumero') . "," .
                "bairro = " . valoresTexto('txtBairro') . "," .
                "estado = " . valoresSelect('txtEstado') . "," .
                "cidade = " . valoresSelect('txtCidade') . "," .
                "complemento = " . valoresTexto('txtComplemento') . "," .
                "Observacao = " . valoresTexto('txtObs') . "," .
                "tipo_sang = " . valoresTexto('txtTpSang') . "," .
                "departamento = " . valoresSelect('txtDepartamento') . "," .                
                "recebe_comissao = " . valoresCheck('txtRecebeComissao') . "," .
                "tbcomiss_prod = " . valoresSelect('selComProd') . "," .
                "comiss_prod = " . valoresNumericos('txtComProd') . "," .
                "tbcomiss_serv = " . valoresSelect('selComServ') . "," .
                "comiss_serv = " . valoresNumericos('txtComServ') . "," .
                "profissao = " . valoresTexto('txtProfissao') . "," .
                "inscricao_municipal = " . valoresTexto('txtInsMunicipal') . "," .
                "plano_saude = " . valoresTexto('txtPlanoSaude') . "," .                
                "telefone_ps1 = " . valoresTexto('txtNmEmergencia') . "," .
                "telefone_ps2 = " . valoresTexto('txtTelEmerg') . "," .
                "procedencia = " . valoresSelect('txtProcedencia') . "," .                 
                "id_user_resp = " . valoresSelect('txtUserResp') . "," .
                "prof_resp = " . valoresSelect('txtProfResp') . "," .                                
                "email_marketing = " . valoresCheck('ckbEmailMarketing') . "," .
                "celular_marketing = " . valoresCheck('ckbCelularMarketing') . "," .   
                "regime_tributario = " . valoresSelect('txtRegTributario') . "," .
                "trabalha_empresa = " . valoresTexto('txtNmEmpresa') . "," .                
                "referencia_ps1 = " . valoresTexto('txtMatriculaRegistro') . "," .                
                $comissao_plano .
                "tbcomiss_gere = " . valoresSelect('selComGere') . "," .
                "inativo = " . ($_POST['txtDtDemissao'] !== "" ? "1" : "0") . "," .
                "bloqueado = " . ($_POST['txtDtDemissao'] !== "" ? "1" : "0") . "," .
                "comiss_gere = " . valoresNumericos('txtComGere') . "," .
                "comiss_gerePri = " . valoresNumericos('txtComGerePri') . "," .
                "tbcomiss_prod_tipo = " . valoresSelect('selComProdTipo') . "," .
                "tbcomiss_ser_tipo = " . valoresSelect('selComSerTipo') . "," .
                "num_cnh = " . valoresTexto('txtNumCnh') . "," .
                "cat_cnh = " . valoresTexto('txtCatCnh') . "," .
                "ven_cnh = " . valoresData('txtVencCnh') . "," .
                "nfe_iss = " . valoresCheck('ckbISS') . "," .                
                "pri_cnh = " . valoresData('txtPriCnh') . 
                " where id_fornecedores_despesas = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
    } else {
        $perfil = "";    
        $cur = odbc_exec($con, "select adm_func_usuario from sf_configuracao where id = 1");
        while ($RFP = odbc_fetch_array($cur)) {
            $perfil = $RFP['adm_func_usuario'];
        }        
        if ($mdl_aca_ == 1) {
            $comissao_plano_cam = "tbcomiss_plano, comiss_plano, comiss_planoPri,";
            $comissao_plano_val = valoresNumericos('selComPlano') . "," . 
            valoresNumericos('txtComPlano') . "," . valoresNumericos('txtComPlanoPri') . ",";
        }        
        $query .= "INSERT INTO sf_fornecedores_despesas(juridico_tipo, razao_social, nome_fantasia,cnpj,inscricao_estadual,org_exped_rg,data_nascimento,sexo,
        tipo_sang,departamento,estado_civil,contato,empresa,dt_admissao,dt_demissao,grupo_pessoa,fornecedores_hr,numcref,cep,endereco,numero,
        bairro,estado,cidade,complemento,Observacao,recebe_comissao,tbcomiss_prod,comiss_prod,tbcomiss_serv,comiss_serv," . $comissao_plano_cam . 
        "tbcomiss_gere,comiss_gere,tipo,dt_cadastro,comiss_gerePri,profissao,trabalha_empresa,tbcomiss_prod_tipo,tbcomiss_ser_tipo,
        usuario_cadastro, plano_saude, telefone_ps1, telefone_ps2, procedencia, id_user_resp, prof_resp, email_marketing, celular_marketing, inativo, bloqueado, regime_tributario, inscricao_municipal, referencia_ps1,
        num_cnh, cat_cnh, ven_cnh, pri_cnh, nfe_iss)VALUES(" .
                valoresTexto('txtJuridicoTipo') . "," .
                strtoupper(valoresTexto('txtNome')) . "," .
                valoresTexto('txtNmFantasia') . "," .
                valoresTexto('txtCpf') . "," .
                valoresTexto('txtRG') . "," .
                valoresTexto('txtOrg') . "," .
                valoresData('txtDtNasc') . "," .
                valoresTexto('txtSexo') . "," .
                valoresTexto('txtTpSang') . "," .
                valoresSelect('txtDepartamento') . "," . 
                valoresTexto('txtEstadoCivil') . "," .
                valoresTexto('txtContato') . "," .
                valoresTexto('txtFilial') . "," .
                valoresData('txtDtAdmissao') . "," .
                valoresData('txtDtDemissao') . "," .
                valoresSelect('txtGrupo') . "," .
                valoresSelect('txtfornecedorHR') . "," .
                valoresTexto('txtCref') . "," .
                valoresTexto('txtCep') . "," .
                valoresTexto('txtEndereco') . "," .
                valoresTexto('txtNumero') . "," .
                valoresTexto('txtBairro') . "," .
                valoresSelect('txtEstado') . "," .
                valoresSelect('txtCidade') . "," .
                valoresTexto('txtComplemento') . "," .
                valoresTexto('txtObs') . "," .
                valoresCheck('txtRecebeComissao') . "," .
                valoresSelect('selComProd') . "," .
                valoresNumericos('txtComProd') . "," .
                valoresSelect('selComServ') . "," .
                valoresNumericos('txtComServ') . "," .
                $comissao_plano_val .
                valoresSelect('selComGere') . "," .
                valoresNumericos('txtComGere') . ", 'I',getDate()," . 
                valoresNumericos('txtComGerePri') . "," .                
                valoresTexto('txtProfissao') . "," .
                valoresTexto('txtNmEmpresa') . "," . 
                valoresSelect('selComProdTipo') . "," .
                valoresSelect('selComSerTipo') . "," . 
                $_SESSION["id_usuario"] . "," .
                valoresTexto('txtPlanoSaude') . "," .                
                valoresTexto('txtNmEmergencia') . "," .
                valoresTexto('txtTelEmerg') . "," .
                valoresSelect('txtProcedencia') . "," .                   
                valoresSelect('txtUserResp') . "," .
                valoresSelect('txtProfResp') . "," .
                valoresCheck('ckbEmailMarketing') . "," .
                valoresCheck('ckbCelularMarketing') . "," .
                ($_POST['txtDtDemissao'] !== "" ? "1" : "0") . "," .
                ($_POST['txtDtDemissao'] !== "" ? "1" : "0") . "," .                
                valoresSelect('txtRegTributario') . "," .
                valoresTexto('txtInsMunicipal') . "," .
                valoresTexto('txtMatriculaRegistro') . "," .
                valoresTexto('txtNumCnh') . "," .
                valoresTexto('txtCatCnh') . "," .
                valoresData('txtVencCnh') . "," .
                valoresData('txtPriCnh') . "," .
                valoresCheck('ckbISS') . ")";           
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_fornecedores_despesas from sf_fornecedores_despesas order by id_fornecedores_despesas desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        values ('sf_funcionarios', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'I', 'INCLUSAO INDICADORES', GETDATE(), " . $_POST['txtId'] . ")");                
        if (is_numeric($perfil) && $perfil > 0) {
            $fQuery = "insert into sf_usuarios (login_user, senha, nome, master, email, imagem, funcionario, email_desc,
            email_host, email_porta, email_login, email_senha, email_ckb, 
            copyMe, filial, assinatura, email_ssl, data_cadastro, email_smtp, adm_mu) 
            values ('" . str_replace(" ", ".", strtolower(CharReplace(formatNameCompact($_POST['txtNome'])))) . "', 'Z1tDZ1BE', " . strtoupper(valoresTexto('txtNome')) . ", " . $perfil . ", '', '', " . $_POST['txtId'] . ", '','', '', '', '', 'false', 0, 1, '', 0, getdate(), 0, 0);";
            odbc_exec($con, $fQuery) or die(odbc_errormsg());            
        }
    }
    if (isset($_POST['txtQtdContatos'])) {
        $max = $_POST['txtQtdContatos'];
        $notIn = '0';
        for ($i = 0; $i <= $max; $i++) {
            if (isset($_POST['txtIdContato_' . $i]) && is_numeric($_POST['txtIdContato_' . $i])) {
                if ($_POST['txtIdContato_' . $i] != 0) {
                    $notIn = $notIn . "," . $_POST['txtIdContato_' . $i];
                }
            }
        }
        odbc_exec($con, "delete from sf_fornecedores_despesas_contatos where fornecedores_despesas = " . $_POST['txtId'] . " and id_contatos not in (" . $notIn . ")");
        for ($i = 0; $i <= $max; $i++) {
            if (isset($_POST['txtIdContato_' . $i])) {
                if ($_POST['txtIdContato_' . $i] === "0") {
                    odbc_exec($con, "insert into sf_fornecedores_despesas_contatos(fornecedores_despesas,tipo_contato,conteudo_contato,resp_contato) values(" . $_POST['txtId'] . "," . $_POST['txtTpContato_' . $i] . "," . valoresTexto('txtTxContato_' . $i) . "," . valoresTexto('txtTxResponsavel_' . $i) . ")") or die(odbc_errormsg());
                }
            }
        }
    }
    $query = "delete from sf_usuarios_dependentes where id_fornecedor_despesas = " . $_POST['txtId'];
    odbc_exec($con, $query);
    if (isset($_POST['items'])) {
        for ($i = 0; $i < count($_POST['items']); $i++) {
            $query = "insert into sf_usuarios_dependentes(id_usuario,id_fornecedor_despesas) values(" . $_POST['items'][$i] . "," . $_POST['txtId'] . ")";
            odbc_exec($con, $query);
        }
    }
    $query = "delete from sf_fornecedores_despesas_grupo_servicos where id_funcionario = " . $_POST['txtId'];
    odbc_exec($con, $query);
    if (isset($_POST['itemsGrupo'])) {
        for ($i = 0; $i < count($_POST['itemsGrupo']); $i++) {
            $query = "insert into sf_fornecedores_despesas_grupo_servicos(id_funcionario,id_grupo_servico) values(" . $_POST['txtId'] . "," . $_POST['itemsGrupo'][$i] . ")";
            odbc_exec($con, $query);
        }
    }
    echo $_POST['txtId'];
}

function CharReplace($nome) {
    $caracteresBR = array('À', 'Á', 'Ã', 'Â', 'à', 'á', 'ã', 'â', 'Ê', 'É', 'Í', 'í', 'Ó', 'Õ', 'Ô', 'ó', 'õ', 'ô', 'Ú', 'Ü', 'Ç', 'ç', 'é', 'ê', 'ú', 'ü');
    $caracteresUS = array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'E', 'E', 'I', 'i', 'O', 'O', 'O', 'o', 'o', 'o', 'U', 'U', 'C', 'c', 'e', 'e', 'u', 'u');
    return str_replace($caracteresBR, $caracteresUS, $nome);
}

if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    $disabled = 'disabled';                
    $cur = odbc_exec($con, "select *, 
    (select razao_social from sf_fornecedores_despesas as y where y.id_fornecedores_despesas = x.prof_resp) prof_atual        
    from sf_fornecedores_despesas x where id_fornecedores_despesas = " . $_GET['id']);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_fornecedores_despesas']);
        $nome = utf8_encode($RFP['razao_social']);
        $juridico_tipo = utf8_encode($RFP['juridico_tipo']);          
        $dtCadastro = escreverData($RFP['dt_cadastro']);
        $nmFantasia = utf8_encode($RFP['nome_fantasia']);
        $cpf = utf8_encode($RFP['cnpj']);
        $rg = utf8_encode($RFP['inscricao_estadual']);
        $dtNascimento = escreverData($RFP['data_nascimento']);
        $sexo = utf8_encode($RFP['sexo']);
        $estadoCivil = utf8_encode($RFP['estado_civil']);
        $contato = utf8_encode($RFP['contato']);
        $ckempresa = utf8_encode($RFP['empresa']);
        $orgExp = utf8_encode($RFP['org_exped_rg']);
        $data_admissao = escreverData($RFP['dt_admissao']);
        $data_demissao = escreverData($RFP['dt_demissao']);
        $grupoPessoa = utf8_encode($RFP['grupo_pessoa']);
        $cref = utf8_encode($RFP['numcref']);
        $cep = utf8_encode($RFP['cep']);
        $endereco = utf8_encode($RFP['endereco']);
        $numero = utf8_encode($RFP['numero']);
        $bairro = utf8_encode($RFP['bairro']);
        $estado = utf8_encode($RFP['estado']);
        $cidade = utf8_encode($RFP['cidade']);
        $complemento = utf8_encode($RFP['complemento']);
        $Obs = utf8_encode($RFP['Observacao']);     
        $tpsang = utf8_encode($RFP['tipo_sang']); 
        $departamento = utf8_encode($RFP['departamento']); 
        $fornecedores_hr = utf8_encode($RFP['fornecedores_hr']);
        $recebe_comiss = $RFP['recebe_comissao'];
        $tbcomiss_prod = $RFP['tbcomiss_prod'];
        $comiss_prod = escreverNumero($RFP['comiss_prod']);
        $tbcomiss_serv = $RFP['tbcomiss_serv'];
        $comiss_serv = escreverNumero($RFP['comiss_serv']);
        $tbcomiss_plano = $RFP['tbcomiss_plano'];
        $comiss_plano = escreverNumero($RFP['comiss_plano']);
        $tbcomiss_gere = $RFP['tbcomiss_gere'];
        $comiss_gere = escreverNumero($RFP['comiss_gere']);
        $acessoTipo = utf8_encode($RFP['acesso_tipo']);
        $acessoNumProv = utf8_encode($RFP['acesso_numero_provisorio']);
        $acessomsg = utf8_encode($RFP['acesso_msg']);
        $profissao = utf8_encode($RFP['profissao']);
        $nmEmpresa = utf8_encode($RFP['trabalha_empresa']);                
        $user_resp = utf8_encode($RFP['id_user_resp']);
        $profResp = utf8_encode($RFP['prof_resp']);        
        $profRespNome = utf8_encode($RFP['prof_atual']);
        $emailMarketing = utf8_encode($RFP['email_marketing']);
        $celularMarketing = utf8_encode($RFP['celular_marketing']);   
        $planoSaude = utf8_encode($RFP['plano_saude']);
        $nmEmergencia = utf8_encode($RFP['telefone_ps1']);
        $telEmergencia = utf8_encode($RFP['telefone_ps2']);
        $procedencia = utf8_encode($RFP['procedencia']);
        $inativo = utf8_encode($RFP['bloqueado']);
        $regimetributario = utf8_encode($RFP['regime_tributario']);
        $referencia_ps1 = utf8_encode($RFP['referencia_ps1']);
        $comiss_planoPri = escreverNumero($RFP['comiss_planoPri']);
        $comiss_gerePri = escreverNumero($RFP['comiss_gerePri']);        
        $tbcomiss_prod_tipo = $RFP['tbcomiss_prod_tipo'];
        $tbcomiss_ser_tipo = $RFP['tbcomiss_ser_tipo'];
        $insMunicipal = utf8_encode($RFP['inscricao_municipal']);
        $numCnh = utf8_encode($RFP['num_cnh']);
        $catCnh = utf8_encode($RFP['cat_cnh']);
        $vencCnh = escreverData($RFP['ven_cnh']);
        $priCnh = escreverData($RFP['pri_cnh']);   
        $nfe_iss = $RFP['nfe_iss'];
    }        
    $cur = odbc_exec($con, "select gyp_recid, gyp_ptype, gyp_token, cat_topdata_lista, ACA_PROSP_CLI from sf_configuracao where id = 1");
    while ($RFP = odbc_fetch_array($cur)) {
        $gymPass_id = $RFP['gyp_recid'];
        $prospectIr = $RFP['ACA_PROSP_CLI'];
        $cat_topdata_lista = $RFP['cat_topdata_lista'];
    }
}

if (isset($_POST['excluirFunc'])) {
    $query = "update sf_fornecedores_despesas set dt_demissao = getdate(), inativo = 1  where id_fornecedores_despesas = " . $_POST['txtId'];
    odbc_exec($con, $query);
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    values ('sf_funcionarios', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'A', 'DEMISSAO INDICADORES', GETDATE(), " . $_POST['txtId'] . ")");
    echo "YES";
}

if (isset($_POST['verificaPendencia'])) {
    $query = "select count(id_tele) totalPend from sf_usuarios A
              inner join sf_fornecedores_despesas B on A.funcionario = B.id_fornecedores_despesas
              inner join sf_telemarketing C on (C.de_pessoa = A.id_usuario or C.para_pessoa = A.id_usuario)
              where B.id_fornecedores_despesas = " . $_POST['txtId'] . " and pendente = 1";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        echo $RFP['totalPend'];
    }
}

if (isset($_GET['existeCPF'])) {
    $inat = 0;
    $isCPF = '';
    $cur = odbc_exec($con, "select cnpj, dt_demissao from sf_fornecedores_despesas where tipo = 'I' and cnpj = '" . $_GET['txtCPF'] . "'");
    while ($RFP = odbc_fetch_array($cur)) {
        $isCPF = $RFP['cnpj'];
        $demissao = $RFP['dt_demissao'];
    }
    if ($isCPF !== "") {
        if ($demissao !== '') {
            echo 'INATIVO';
        } else {
            echo 'ATIVO';
        }
    } else {
        echo 'NO';
    }
}

if (isset($_POST['reativarCadastro'])) {
    if (isset($_POST['txtCPF'])) {
        $cur = odbc_exec($con, "select id_fornecedores_despesas from sf_fornecedores_despesas where cnpj = '" . $_POST['txtCPF'] . "' and dt_demissao is not null");
        while ($RFP = odbc_fetch_array($cur)) {
            $idCliente = $RFP['id_fornecedores_despesas'];
        }
    } else {
        $idCliente = $_POST['idCli'];
    }
    odbc_exec($con, "UPDATE sf_fornecedores_despesas set dt_demissao = null where id_fornecedores_despesas = " . $idCliente);
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    values ('sf_funcionarios', " . $idCliente . ", '" . $_SESSION["login_usuario"] . "', 'I', 'EDITAR - REATIVAR FUNCIONARIO', GETDATE(), " . $idCliente . ")");
    echo $idCliente;
}

if (isset($_GET['listAtendimento'])) {
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );

    $query = "select id_tele,case when C.de_pessoa = A.id_usuario then A.login_user else (select login_user from sf_usuarios where id_usuario = C.de_pessoa) end de_pessoa,
    case when C.para_pessoa = A.id_usuario then A.login_user else (select login_user from sf_usuarios where id_usuario = C.para_pessoa) end para_pessoa,
    data_tele, pendente,proximo_contato,obs from sf_usuarios A
    inner join sf_fornecedores_despesas B on A.funcionario = B.id_fornecedores_despesas
    inner join sf_telemarketing C on (C.de_pessoa = A.id_usuario or C.para_pessoa = A.id_usuario)
    where B.id_fornecedores_despesas = " . $_GET['txtId'] . " order by 1 desc";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $cont = $cont + 1;
        $row = array();

        $backColor = "";
        if ($RFP['pendente'] === "1") {
            $backColor = " color:red;";
        } else {
            $backColor = "";
        }
        $status = "NÃO";
        if ($RFP['pendente'] === "1") {
            $status = "SIM";
        }
        $row[] = "<div style='" . $backColor . "'>" . $status . "</div>";
        $row[] = "<div style='" . $backColor . "'>" . escreverData($RFP['data_tele']) . "</div>";
        $row[] = "<div style='" . $backColor . "'>" . utf8_encode($RFP['de_pessoa']) . "</div>";
        $row[] = "<div style='" . $backColor . "'>" . utf8_encode($RFP['para_pessoa']) . "</div>";
        $row[] = "<div style='" . $backColor . "'>" . escreverData($RFP['proximo_contato']) . "</div>";
        $row[] = "<div style='" . $backColor . "'>" . utf8_encode($RFP['obs']) . "</div>";
        $records['aaData'][] = $row;
    }

    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}

if (isset($_POST['transfPendencia'])) {
    $query = "select top 1 id_usuario from sf_usuarios where funcionario = " . $_POST['txtIdDe'];
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $idDe = $RFP['id_usuario'];
    }
    if ($idDe !== "") {
        $query = "update sf_telemarketing set para_pessoa = " . $_POST['txtIdPara'] . ", de_pessoa = " . $_POST['txtIdPara'] . " where pendente = 1 and (para_pessoa = " . $idDe . " or de_pessoa = " . $idDe . ")";
        odbc_exec($con, $query);
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        values ('sf_telemarketing', " . $_POST['txtIdDe'] . ", '" . $_SESSION["login_usuario"] . "', 'A', 'TRANSFERENCIA PENDENCIA De:" . $idDe . "Para:" . $_POST['txtIdPara'] . "', GETDATE(), " . $_POST['txtIdDe'] . ")");
        echo "YES";
    } else {
        echo "ERRO";
    }
}

if (isset($_POST['transfResponsavel'])) {
    $query = "select top 1 id_usuario from sf_usuarios where funcionario = " . $_POST['txtIdDe'];
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $idDe = $RFP['id_usuario'];
    }
    if ($idDe !== "") {
        $query = "update sf_fornecedores_despesas set id_user_resp = " . $_POST['txtIdPara'] . " where id_user_resp = " . $idDe;
        odbc_exec($con, $query);
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        values ('sf_fornecedores_despesas', " . $_POST['txtIdDe'] . ", '" . $_SESSION["login_usuario"] . "', 'A', 'TRANSFERENCIA RESPONSAVEL De:" . $idDe . "Para:" . $_POST['txtIdPara'] . "', GETDATE(), " . $_POST['txtIdDe'] . ")");
        echo "YES";
    } else {
        echo "ERRO";
    }
}