<?php

$disabled = 'disabled';
$id = ''; 
$id_pai = (isset($_GET["id_pai"]) ? $_GET["id_pai"] : ""); 
$entrada = ''; 
$saida = ''; 
$tipo = (isset($_GET["tipo"]) ? $_GET["tipo"] : "");
$refresh = '1';

if (isset($_POST['btnNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if (isset($_POST['btnSave'])) {
    if ($_POST['txtId'] != '') {
        $Query = "UPDATE sf_whatsapp_modelo SET
            id_pai = " . valoresSelect('txtPai') . ",
            entrada = " . valoresTexto('txtEntrada') . ",
            saida = " . valoresTexto('txtSaida') . ",
            tipo = " . valoresNumericos('txtTipo') . ",
            refresh = " . valoresNumericos('txtRefresh') . " 
            WHERE id = " . $_POST['txtId'];
        odbc_exec($con, $Query) or die(odbc_errormsg());
    } else {
        $Query = "INSERT INTO sf_whatsapp_modelo(id_pai, entrada, saida, tipo, refresh) VALUES (" . 
            valoresSelect('txtPai') . "," . 
            valoresTexto('txtEntrada') . "," .
            valoresTexto('txtSaida') . "," .
            valoresNumericos('txtTipo') . "," .
            valoresNumericos('txtRefresh') . ")";
        odbc_exec($con, $Query) or die(odbc_errormsg());
        $res = odbc_exec($con, "SELECT TOP 1 id FROM sf_whatsapp_modelo ORDER BY id DESC") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}

if (isset($_POST['btnDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        if (isset($_POST['List'])) {
            include "../../../Connections/configini.php";
        }
        odbc_exec($con, "DELETE FROM sf_whatsapp_modelo WHERE id = " . $_POST['txtId']);
        if (isset($_POST['List'])) {
            echo "YES";
        } else {
            echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
        }
        odbc_close($con);
        exit();
    }
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_whatsapp_modelo where id =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id'];
        $id_pai = utf8_encode($RFP['id_pai']);
        $entrada = utf8_encode($RFP['entrada']);
        $saida = utf8_encode($RFP['saida']);
        $tipo = utf8_encode($RFP['tipo']);
        $refresh = utf8_encode($RFP['refresh']);
    }
} else {
    $disabled = '';
}

if (isset($_POST['btnEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}

if (isset($_POST['listChatbot'])) {
    include "../../../Connections/configini.php";
    getNode($con, "");
    odbc_close($con);
}

function getNode($con, $id_pai) {
    $sQuery = "select id, id_pai, entrada, saida, tipo, refresh from sf_whatsapp_modelo 
    where id_pai " . (is_numeric($id_pai) ? " = " . $id_pai : " is null") . " order by tipo desc, id";
    $cur = odbc_exec($con, $sQuery);
    while ($RFP = odbc_fetch_array($cur)) { ?>
    <ul>
        <li data-jstree='{"icon":"<?php echo ($RFP['tipo'] == "0" ? "ico-question-sign li-orange" : ($RFP['tipo'] == "1" ? "ico-comments li-blue" : "ico-comments li-green"));?>"}'>
            <span>
                <?php echo "[" . utf8_encode($RFP['entrada']) . "] => " . utf8_encode($RFP['saida']); ?>                                                
                <input class="btn" type="button" value="+" style="margin:0px; padding:2px 4px 3px 4px; line-height:10px;" onclick="AddBox(<?php echo $RFP['id'];?>,<?php echo $RFP['tipo'];?>);">
                <input class="btn green" type="button" value="e" style="margin:0px; padding:2px 4px 3px 4px; line-height:10px;" onclick="AbrirBox(<?php echo $RFP['id'];?>);">
                <input class="btn red" type="button" value="x" style="margin:0px; padding:2px 4px 3px 4px; line-height:10px;" onclick="excluir(<?php echo $RFP['id'];?>);">
            </span>
            <?php getNode($con, $RFP['id']); ?>
        </li>
    </ul>
    <?php }
}