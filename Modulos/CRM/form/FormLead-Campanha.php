<?php

if (isset($_POST['List'])) {
    include "../../../Connections/configini.php";
}

$disabled = 'disabled';
$pasta = "./../../Pessoas/" . $contrato . "/Campanha/";

if (isset($_POST['btnSave'])) {
    $anexo = "";
    if ($_FILES['arquivo']['name']) {
        $anexo .= ", id_foto = " . valoresTexto2($_FILES['arquivo']['name']);
    }    
    if (is_numeric($_POST['txtId'])) {     
        odbc_exec($con, "set dateformat dmy;update sf_lead_campanha set " .
        "titulo = " . valoresTexto("txtTitulo") . "," .
        "descricao = " . valoresTexto("ckeditor") . "," .
        "data_ini = " . valoresData("txtDataInicio") . "," .
        "data_fim = " . valoresData("txtDataFim") . "," .
        "id_procedencia = " . valoresSelect("txtProcedencia") . "," .
        "min_range = " . valoresNumericos("txtMinRange") . "," .
        "max_range = " . valoresNumericos("txtMaxRange") . "," .
        "conv_led_pro = " . valoresNumericos("txtConvProspect") . "," .
        "conv_pro_cli = " . valoresNumericos("txtConvCliente") . "," .
        "inativo = " . valoresCheck("txtInativo") . " " . $anexo .              
        "where id = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "set dateformat dmy; insert into sf_lead_campanha(titulo, descricao, data_ini, data_fim,
        id_procedencia, min_range, max_range, conv_led_pro, conv_pro_cli, id_foto, inativo) values (" .
        valoresTexto("txtTitulo") . "," .
        valoresTexto("ckeditor") . "," .
        valoresData("txtDataInicio") . "," .
        valoresData("txtDataFim") . "," .
        valoresSelect("txtProcedencia") . "," .
        valoresNumericos("txtMinRange") . "," .
        valoresNumericos("txtMaxRange") . "," .
        valoresNumericos("txtConvProspect") . "," .
        valoresNumericos("txtConvCliente") . "," .
        valoresTexto2($_FILES['arquivo']['name']) . "," .
        valoresCheck("txtInativo") . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id from sf_lead_campanha order by id desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
    if (!is_dir($pasta . "/" . $_POST['txtId'] . "/")) {
        mkdir($pasta . "/" . $_POST['txtId'] . "/", 0777, true);
    }
    if ($_FILES['arquivo']['name']) {
        $nome = utf8_decode($_FILES['arquivo']['name']);
        $tmp = $_FILES['arquivo']['tmp_name'];
        if ($tmp) {
            move_uploaded_file($tmp, $pasta . "/" . $_POST['txtId'] . "/" . $nome);
        }
    }    
}

if (isset($_POST['btnDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_lead_campanha WHERE id = " . $_POST['txtId']);
        deleteDirectory($pasta . "/" . $_POST['txtId']);        
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}

if (isset($_POST['DelImage'])) {
    if (is_numeric($_POST['txtId'])) {
        odbc_exec($con, "update sf_lead_campanha set id_foto = '' WHERE id = " . $_POST['txtId']) or die(odbc_errormsg());
        $arquivo = "." . $pasta . "/" . $_POST['txtId'] . "/" . $_POST['DelImage'];
        if (file_exists($arquivo)) {
            unlink($arquivo);
        }
    }
}

if (isset($_POST['btnNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_lead_campanha where id = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id']);
        $titulo = utf8_encode($RFP['titulo']);
        $dt_inicio = escreverData($RFP['data_ini']);
        $dt_fim = escreverData($RFP['data_fim']);
        $procedencia = utf8_encode($RFP['id_procedencia']);
        $min_range = utf8_encode($RFP['min_range']);
        $max_range = utf8_encode($RFP['max_range']);
        $conv_prospect = utf8_encode($RFP['conv_led_pro']);
        $conv_cliente = utf8_encode($RFP['conv_pro_cli']);
        $inativo = utf8_encode($RFP['inativo']);
        $mensagem = utf8_encode($RFP['descricao']);
        $image = utf8_encode($RFP['id_foto']);
    }
} else {
    $disabled = '';
    $id = '';
    $titulo = '';
    $dt_inicio = getData("T");
    $dt_fim = getData("E");
    $procedencia = '';
    $min_range = '1';
    $max_range = '99999';
    $conv_prospect = '1';
    $conv_cliente = '2';
    $inativo = '';
    $mensagem = '';
    $image = '';
}

if (isset($_POST['btnEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}