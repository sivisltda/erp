<?php
include '../../../Connections/configini.php';

$disabled = 'disabled';
$mdl_seg_ = returnPart($_SESSION["modulos"], 14);

if (isset($_POST['bntSave'])) {
    if (is_numeric($_POST['txtId'])) {
        $id = $_POST['txtId'];
        $query = "SET DATEFORMAT DMY;" .
                "UPDATE [sf_telemarketing] SET " .
                "[atendimento_servico] = 1," .
                "[pessoa] = " . valoresNumericos("txtIdPessoa") . "," .
                "[id_veiculo] = " . valoresNumericos("txtIdVeiculo") . "," .                
                "[data_tele] = " . valoresDataHora("txtData", "txtHora") . "," .
                "[especificacao] = " . valoresTexto("txtTipoChamado") . "," .
                "[de_pessoa] = " . valoresTexto("txtDe") . "," .
                "[para_pessoa] = " . valoresTexto("txtPara") . "," .
                "[proximo_contato] = " . valoresDataHora("txtDataProximo", "txtHoraProximo") . "," .
                "[obs] = " . valoresTexto("txtObs") . "," .
                "[obs_fechamento] = " . valoresTexto("txtObsFechamento") . "," .
                "[relato] = " . valoresTexto("txtRelato") . "," .
                "[acompanhar] = " . valoresCheck("ckbAcompanhar") . "," .
                "[garantia] = " . valoresCheck("ckbGarantia") . "," .
                "[nomecon] = " . valoresTexto("txtNomeCon") . "," .
                "[departamento] = " . valoresSelect("txtDepartamento") . "," .
                "[nota_fiscal] = " . valoresTexto("txtNotaFiscal") . "," .
                "[data_nf] = " . valoresData("txtDataNota") . "," .
                "[pendente] = 1 " .
                "WHERE id_tele = " . $_POST['txtId'];
        //echo $query; exit;
        odbc_exec($con, $query) or die(odbc_errormsg());
        $query = "0";
        if (isset($_POST['txtEspecificacao'])) {
            for ($i = 0; $i < count($_POST['txtEspecificacao']); $i++) {
                if ($_POST['txtEspecificacao'][$i] != '') {
                    $query = $query . "," . $_POST['txtEspecificacao'][$i];
                }
            }
            odbc_exec($con, "delete from sf_telemarketing_item where id_telemarketing = " . $_POST['txtId'] . " and id_especificacao not in (" . $query . ")") or die(odbc_errormsg());
            $ToAdd[0] = "";
            $w = 0;
            $cur = odbc_exec($con, "select te.id_especificacao x ,ti.id_especificacao y from sf_telemarketing_especificacao te left join
            sf_telemarketing_item ti on te.id_especificacao = ti.id_especificacao and ti.id_telemarketing = " . $_POST['txtId'] . "
            where te.id_especificacao in (" . $query . ")");
            while ($RFP = odbc_fetch_array($cur)) {
                if ($RFP['y'] == null) {
                    $ToAdd[$w] = $RFP['x'];
                    $w++;
                }
            }
            for ($j = 0; $j < $w; $j++) {
                $query = "insert into sf_telemarketing_item(id_telemarketing,id_especificacao) values(" . $_POST['txtId'] . "," . $ToAdd[$j] . ")";
                odbc_exec($con, $query);
            }
        }
    } else {
        $query = "SET DATEFORMAT DMY;
       INSERT INTO [sf_telemarketing] ([atendimento_servico],
       [pessoa],[id_veiculo],[data_tele],[especificacao],[de_pessoa],[para_pessoa],
       [proximo_contato],[obs],[obs_fechamento],[relato],[acompanhar],[garantia],
       [nomecon],[departamento],[nota_fiscal],[data_nf],[pendente],[dt_inclusao])VALUES(1," .
                valoresNumericos("txtIdPessoa") . "," .
                valoresNumericos("txtIdVeiculo") . "," .                
                valoresDataHora("txtData", "txtHora") . "," .
                valoresTexto("txtTipoChamado") . "," .
                valoresTexto("txtDe") . "," .
                valoresTexto("txtPara") . "," .
                valoresDataHora("txtDataProximo", "txtHoraProximo") . "," .
                valoresTexto("txtObs") . "," .
                valoresTexto("txtObsFechamento") . "," .                
                valoresTexto("txtRelato") . "," .
                valoresCheck("ckbAcompanhar") . "," .
                valoresCheck("ckbGarantia") . "," .
                valoresTexto("txtNomeCon") . "," .
                valoresSelect("txtDepartamento") . "," .
                valoresTexto("txtNotaFiscal") . "," .
                valoresData("txtDataNota") . "," .
                "1, getdate())";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_tele from sf_telemarketing order by id_tele desc") or die(odbc_errormsg());
        $id = odbc_result($res, 1);
        if (isset($_POST['txtEspecificacao'])) {
            for ($i = 0; $i < count($_POST['txtEspecificacao']); $i++) {
                if ($_POST['txtEspecificacao'][$i] != '') {
                    $query = "insert into sf_telemarketing_item(id_telemarketing,id_especificacao) values(" . $id . "," . $_POST['txtEspecificacao'][$i] . ")";
                    odbc_exec($con, $query);
                }
            }
        }
    }
    if (valoresNumericos("txtIdVeiculo") > 0) {
        $res = odbc_exec($con, "select count(id) total from sf_fornecedores_despesas_veiculo_kms where id_veiculo = " . valoresNumericos("txtIdVeiculo") . " and data = " . valoresData("txtData")) or die(odbc_errormsg());
        $total = odbc_result($res, 1);
        if ($total == 0) {
            odbc_exec($con, "SET DATEFORMAT DMY; insert into sf_fornecedores_despesas_veiculo_kms (id_veiculo, data, kms, sys_login, data_cad) values (" . 
            valoresNumericos("txtIdVeiculo") . ", " . valoresData("txtData") . ", " . valoresNumericos("txtKms") . ", '" . $_SESSION["login_usuario"] . "', getdate());");
        }    
    }
}

if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        $sql = "select ISNULL(COUNT(*),0) total from sf_vendas where id_venda in(select id_venda from sf_telemarketing where id_tele = " . $_POST['txtId'] . ")";
        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            if ($RFP['total'] > 0) {
                echo "<script>alert('Registro com venda!'); parent.FecharBox(2);</script>";
            } else {
                odbc_exec($con, "DELETE FROM sf_telemarketing_item WHERE id_telemarketing = " . $_POST['txtId']);
                odbc_exec($con, "DELETE FROM sf_mensagens_telemarketing WHERE id_tele = " . $_POST['txtId']);
                odbc_exec($con, "DELETE FROM sf_telemarketing WHERE id_tele = " . $_POST['txtId']);
                echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox(2);</script>";
            }
        }
    }    
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select id_tele,de_pessoa,u.login_user,para_pessoa,data_tele,pendente,proximo_contato,t.obs,t.pessoa,
    tipo,us.login_user login_user_fechamento,data_tele_fechamento,obs_fechamento,especificacao,razao_social,nome_fantasia,
    garantia,atendimento_servico,nota_fiscal,data_nf,valor_orcamento,relato,dias_garantia,t.departamento,nomecon,t.procedencia,
    t.origem,t.numficha,bloqueado,acompanhar, id_venda, id_veiculo,
    (select top 1 k.kms from sf_fornecedores_despesas_veiculo_kms k where k.id_veiculo = t.id_veiculo and k.data = cast(t.data_tele as date) order by k.id desc) kms
    from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa
    LEFT JOIN sf_usuarios us on us.id_usuario = t.de_pessoa_fechamento
    LEFT JOIN sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = t.pessoa where id_tele = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_tele']);
        $de = utf8_encode($RFP['de_pessoa']);        
        $idVeiculo = utf8_encode($RFP['id_veiculo']);
        $operador = utf8_encode($RFP['login_user']);
        $nomecontato = utf8_encode($RFP['nomecon']);
        $data = escreverData($RFP['data_tele']);
        $hora = escreverData($RFP['data_tele'], 'H:i');
        $relato = utf8_encode($RFP['relato']);
        $obs = utf8_encode($RFP['obs']);
        $obs_fechamento = utf8_encode($RFP['obs_fechamento']);
        $departamento = utf8_encode($RFP['departamento']);
        $para = utf8_encode($RFP['para_pessoa']);
        $dataProximo = escreverData($RFP['proximo_contato']);
        $horaProximo = escreverData($RFP['proximo_contato'], 'H:i');
        $nota_fiscal = utf8_encode($RFP['nota_fiscal']);
        $data_nota = escreverData($RFP['data_nf']);
        $ckb_garantia = utf8_encode($RFP['garantia']);
        $ckbAcompanhar = utf8_encode($RFP['acompanhar']);
        $tipoChamado = utf8_encode($RFP['especificacao']);
        $id_comanda = utf8_encode($RFP['id_tele']);        
        $id_pessoa = utf8_encode($RFP['pessoa']);        
        $kms = utf8_encode($RFP['kms']);        
    }
} else {
    $disabled = '';
    $id = '';
    $de = $_SESSION["id_usuario"];
    $idVeiculo = '0';
    $operador = $_SESSION["login_usuario"];
    $nomecontato = '';
    $data = getData("T");
    $hora = date("H:i");
    $relato = '';
    $obs = '';
    $obs_fechamento = '';
    $departamento = '';
    $para = $_SESSION["id_usuario"];
    $dataProximo = getData("T");
    $horaProximo = date("H:i");
    $nota_fiscal = '';
    $data_nota = '';
    $ckb_garantia = '';
    $ckbAcompanhar = 0;
    $tipoChamado = "T";
    $tp_venda = "";
    $id_comanda = "";
    $id_pessoa = $_GET['cli'];
    $kms = "";
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<!DOCTYPE html>
<head>
    <link rel="icon" type="image/ico" href="../../../favicon.ico"/>
    <link href="../../../css/stylesheets.css" rel="stylesheet" type="text/css" />
    <link href="../../../css/stylesheet.css" rel="stylesheet" type="text/css" />
    <link href="../../../css/main.css" rel="stylesheet">
    <link href="../../../css/bootbox.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    <style>
        .table th {
            line-height: 30px;
            background: #E9E9E9;
            padding: 0px 10px;
            margin: 0px;
            font-size: 11px;
            color: #666;
        }
        .table th, .table td {
            padding: 4px;
            line-height: 20px;            
        }               
        table td #formPQ {
            height: 15px;
            overflow: hidden;
        }  
        .box {
            float: left;
            margin-right: 2%;
            margin-bottom: 2%;         
            width: 23%;            
        }
        .img-car {
            border: 1px solid #ddd;
            height: 50px;            
            width: 100%;                
        }        
        .img-car-master {
            margin-top: 3.5%;
            margin-bottom: 2%;            
            height: 200px;            
            width: 98%;
        }
        .left-imgs {
            float: left; 
            width: 34%; 
            overflow: hidden; 
        }
        .item-ckb {
            float:left; 
            width:24%;
            margin-left: 1%;
            display: flex;
        }
        .deleteImg {
            position: absolute; 
            margin: 0px; 
            padding: 2px 4px 3px; 
            line-height: 10px; 
            cursor: pointer;
        }        
    </style>
</head>
<body>
    <div class="row-fluid">
        <form id="frmTelemarketing" name="frmTelemarketing" action="FormTelemarketing.php?cli=<?php echo $_GET['cli']; ?>" method="POST">
            <div class="frmhead">
                <div class="frmtext">Ordem de Serviço</div>
                <div class="frmicon" style="top:13px" onClick="parent.FecharBox(2)">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <input name="txtDe" id="txtDe" value="<?php echo $de; ?>" type="hidden"/>
            <input name="txtIdPessoa" id="txtIdPessoa" value="<?php echo $id_pessoa; ?>" type="hidden"/>
            <div class="tabbable frmtabs">
                <ul class="nav nav-tabs" style="margin:0">
                    <li class="active"><a href="#tab1" data-toggle="tab">Dados do Chamado</a></li>
                    <li><a href="#tab2" data-toggle="tab">Observações</a></li>
                    <?php if (is_numeric($id)) { ?>
                    <li><a href="#tab3" data-toggle="tab">Orçamentos</a></li>
                    <li><a href="#tab4" data-toggle="tab">Vendas</a></li>
                    <?php } ?>
                </ul>
                <div class="tab-content frmcont" style="height:363px;font-size:11px !important;">
                    <div class="tab-pane active" id="tab1">
                        <div style="line-height:25px; font-size:13px; color:#666;">
                            <strong>Número OS: <?php echo $id; ?></strong>
                        </div>
                        <hr style="margin: 0 0 5px"/>
                        <?php if ($mdl_seg_ > 0) { ?>
                        <div style="width:16%; float:left;">
                            <span>Placa:</span>
                            <select id="txtIdVeiculo" name="txtIdVeiculo" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                                <option value="null" marca="" modelo="" renavam="">SELECIONE</option>
                                <?php $cur = odbc_exec($con, "select id, marca, modelo, placa, renavam from sf_fornecedores_despesas_veiculo
                                where id_fornecedores_despesas = " . $id_pessoa . " or id = " . $idVeiculo);
                                while ($RFP = odbc_fetch_array($cur)) { ?>  
                                    <option value="<?php echo $RFP["id"]; ?>" marca="<?php echo utf8_encode($RFP["marca"]); ?>" modelo="<?php echo utf8_encode($RFP["modelo"]); ?>" renavam="<?php echo utf8_encode($RFP["renavam"]); ?>" <?php if ($idVeiculo == $RFP["id"]) { echo "SELECTED"; } ?>><?php echo utf8_encode($RFP["placa"]); ?></option>
                                <?php } ?>
                            </select>
                        </div>                                                                                         
                        <div style="width:17%; float:left; margin-left: 1%;">
                            <span>Marca:</span>
                            <input id="txtMarca" name="txtMarca" class="input-xlarge" maxlength="8" type="text" style="min-height: 28px;" value="<?php echo $cor; ?>" disabled/>
                        </div>                        
                        <div style="width:33%; float:left; margin-left: 1%;">
                            <span>Modelo:</span>
                            <input id="txtModelo" name="txtModelo" class="input-xlarge" maxlength="8" type="text" style="min-height: 28px;" value="<?php echo $modelo; ?>" disabled/>
                        </div>                                                                 
                        <div style="width:15%; float:left; margin-left: 1%;">
                            <span>RENAVAM:</span>
                            <input id="txtRenavam" name="txtRenavam" class="input-xlarge" maxlength="8" type="text" style="min-height: 28px;" value="<?php echo $renavam; ?>" disabled/>
                        </div>                                                                                                                                  
                        <div style="width:15%; float:left; margin-left: 1%;">
                            <span>Kms:</span>
                            <input id="txtKms" name="txtKms" class="input-xlarge" maxlength="20" type="text" style="min-height: 28px;" value="<?php echo $kms; ?>" <?php echo $disabled; ?>/>
                        </div>                        
                        <div style="clear:both"></div>
                        <?php } ?>
                        <div style="width:20%; float:left">
                            <span>Operador:</span>
                            <input type="text" name="txtOperador" id="txtOperador" class="input-medium" value="<?php echo $operador; ?>" disabled/>
                        </div>
                        <div style="width:25%; float:left; margin-left:1%">
                            <span>Tipo:</span>
                            <select id="txtTipoChamado" name="txtTipoChamado" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                                <option value="T" <?php
                                if ($tipoChamado == "T") {
                                    echo "SELECTED";
                                }
                                ?>>Telefone</option>
                                <option value="E" <?php
                                if ($tipoChamado == "E") {
                                    echo "SELECTED";
                                }
                                ?>>Email</option>
                                <option value="V" <?php
                                if ($tipoChamado == "V") {
                                    echo "SELECTED";
                                }
                                ?>>Visita</option>
                                <option value="S" <?php
                                if ($tipoChamado == "S") {
                                    echo "SELECTED";
                                }
                                ?>>SMS</option>
                                <option value="P" <?php
                                if ($tipoChamado == "P") {
                                    echo "SELECTED";
                                }
                                ?>>Presencial</option>
                                <option value="O" <?php
                                if ($tipoChamado == "O") {
                                    echo "SELECTED";
                                }
                                ?>>Outro</option>
                                <option value="W" <?php
                                if ($tipoChamado == "W") {
                                    echo "SELECTED";
                                }
                                ?>>Whatsapp</option>                                
                                <option value="R" <?php
                                if ($tipoChamado == "R") {
                                    echo "SELECTED";
                                }
                                ?>>Portal</option>                                
                            </select>
                        </div>                    
                        <div style="width:24%; float:left; margin-left:1%">
                            <span>Contato:</span>
                            <input type="text" name="txtNomeCon" id="txtNomeCon" class="input-medium" maxlength="40" value="<?php echo $nomecontato; ?>" <?php echo $disabled; ?>/>
                        </div>
                        <div style="width:16%; float:left; margin-left:1%">
                            <span>Data Cham:</span>
                            <input type="text" name="txtData" id="txtData" class="inputCenter datepicker" maxlength="10" value="<?php echo $data; ?>" <?php echo $disabled; ?>/>
                        </div>
                        <div style="width:11%; float:left; margin-left:1%">
                            <span>Hora Cham:</span>
                            <input type="text" name="txtHora" id="txtHora" class="inputCenter" maxlength="5" value="<?php echo $hora; ?>" <?php echo $disabled; ?>/>
                        </div>
                        <div style="clear:both; height:5px"></div>
                        <div style="margin-bottom:5px">
                            <span>Especificação:</span>
                            <select id="txtEspecificacao" name="txtEspecificacao[]" multiple="multiple" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                                <?php
                                $cur = odbc_exec($con, "select t.id_especificacao,nome_especificacao,ti.id_item from sf_telemarketing_especificacao t left join sf_telemarketing_item ti on t.id_especificacao = ti.id_especificacao and ti.id_telemarketing = '" . $id . "' order by nome_especificacao");
                                while ($RFP = odbc_fetch_array($cur)) {
                                    ?>  
                                    <option value="<?php echo $RFP["id_especificacao"]; ?>" <?php
                                    if ($RFP["id_item"] != null) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP["nome_especificacao"]); ?></option>
                                        <?php } ?>
                            </select>
                        </div>
                        <div style="width:49.5%; float:left">
                            <span>Relato:</span>
                            <textarea name="txtRelato" id="txtRelato" style="width:100%; height:75px" maxlength="1024" <?php echo $disabled; ?>><?php echo $relato; ?></textarea>
                        </div>
                        <div style="width:49.5%; float:left; margin-left:1%">
                            <span>Diagnóstico Provisório:</span>
                            <textarea name="txtObsFechamento" id="txtObsFechamento" style="width:100%; height:75px" <?php echo $disabled; ?>><?php echo $obs_fechamento; ?></textarea>
                        </div>
                        <div style="clear:both; height:5px"></div>
                        <div style="width:35%; float:left">
                            <span>Departamento:</span>
                            <select name="txtDepartamento" id="txtDepartamento" class="input-medium" style="width:100%" <?php echo $disabled; ?>>
                                <option value="null">Selecione</option>
                                <?php
                                $cur = odbc_exec($con, "select id_departamento,nome_departamento from sf_departamentos order by nome_departamento") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) {
                                    ?>
                                    <option value="<?php echo $RFP["id_departamento"] ?>"<?php
                                    if (!(strcmp($RFP["id_departamento"], $departamento))) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP["nome_departamento"]); ?></option>
                                        <?php } ?>
                            </select>
                        </div>
                        <div style="width:35%; float:left; margin-left:1%">
                            <span>Responsável:</span>
                            <select name="txtPara" id="txtPara" class="input-medium" style="width:100%" <?php echo $disabled; ?>>
                                <option value="null">Selecione</option>
                                <?php
                                $cur = odbc_exec($con, "select id_usuario,login_user,nome from sf_usuarios where inativo = 0 order by nome") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) {
                                    ?>
                                    <option value="<?php echo $RFP["id_usuario"] ?>"<?php
                                    if (!(strcmp($RFP["id_usuario"], $para))) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP["nome"]); ?></option>
                                        <?php } ?>
                            </select>
                        </div>                    
                        <div style="width:16%; float:left; margin-left:1%">
                            <span>Data Prazo:</span>
                            <input type="text" name="txtDataProximo" id="txtDataProximo" class="inputCenter datepicker" maxlength="10" value="<?php echo $dataProximo; ?>" <?php echo $disabled; ?>/>
                        </div>
                        <div style="width:11%; float:left; margin-left:1%">
                            <span>Hora Prazo:</span>
                            <input type="text" name="txtHoraProximo" id="txtHoraProximo" class="inputCenter" maxlength="5" value="<?php echo $horaProximo; ?>" <?php echo $disabled; ?>/>
                        </div>
                        <div style="clear:both; height:5px"></div>
                        <div style="width:35%; float:left">
                            <span>Nota Fiscal:</span>
                            <input type="text" name="txtNotaFiscal" id="txtNotaFiscal" maxlength="128" class="input-medium" value="<?php echo $nota_fiscal; ?>" <?php
                            if ($ckb_garantia == 1) {
                                echo $disabled;
                            } else {
                                echo "disabled";
                            }
                            ?>/>
                        </div>
                        <div style="width:16%; float:left; margin-left:1%">
                            <span>Data Nota:</span>
                            <input type="text" name="txtDataNota" id="txtDataNota" class="inputCenter datepicker" value="<?php echo $data_nota; ?>" <?php
                            if ($ckb_garantia == 1) {
                                echo $disabled;
                            } else {
                                echo "disabled";
                            }
                            ?>/>
                        </div>
                        <div style="width:15%; float:left; margin-left:5%; margin-top:16px">
                            <div style="line-height:27px; float:left">
                                <input type="checkbox" name="ckbGarantia" id="ckbGarantia" class="input-medium" onClick="garantia()" <?php
                                if ($ckb_garantia == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/>
                            </div>
                            <div style="line-height:27px; float:left">Garantia</div>
                        </div>
                        <div style="width:23%; float:left; margin-left:5%; margin-top:16px">
                            <div style="line-height:27px; float:left">
                                <input type="checkbox" name="ckbAcompanhar" id="ckbAcompanhar" class="input-medium" <?php
                                if ($ckbAcompanhar == 1) {
                                    echo "checked";
                                }
                                ?> value="1" <?php echo $disabled; ?>/>
                            </div>
                            <div style="line-height:27px; float:left">Acompanhado</div>
                        </div>
                        <div style="clear:both"></div>                        
                    </div>
                    <div class="tab-pane" id="tab2">
                        <div style="width:98%; float:left; margin-left:1%">
                            <span>Observações:</span>
                            <textarea name="txtObs" id="txtObs" style="width:100%; height:340px" <?php echo $disabled; ?>><?php echo $obs; ?></textarea>
                        </div>                        
                    </div>
                    <div class="tab-pane" id="tab3">
                        <?php include "../../Academia/include/Orcamentos.php"; ?>
                    </div>
                    <div class="tab-pane" id="tab4">
                        <?php include "../../Academia/include/Vendas.php"; ?>
                    </div>
                </div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == '') { ?>
                        <button class="btn green" type="submit" name="bntSave" onclick="return validar();" id="bntSave" ><span class="ico-checkmark"></span> Gravar</button>
                        <?php if ($_POST['txtId'] == '') { ?>
                            <button class="btn yellow" onClick="parent.FecharBox(2)" id="bntCancel"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                            <button class="btn yellow" type="submit" name="bntAlterar" id="bntAlterar" ><span class="ico-reply"></span> Cancelar</button>
                            <?php
                        }
                    } else {
                        if (is_numeric($id)) { ?>                            
                            <a style="float: left;" href="./../../../util/ImpressaoPdf.php?id=<?php echo $id . "&tpImp=I&NomeArq=OS&PathArq=../Modulos/Servicos/modelos/Ordem.php&crt=" . $_SESSION["contrato"]; ?>" target="_blank">
                                <button class="btn button-blue" type="button" name="bntBol" value="Imprimir"><span class="icon-print icon-white"></span> Imprimir</button>
                            </a>
                        <?php } ?>
                        <button class="btn green" type="submit" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                        <button class="btn green" type="submit" name="bntEdit" id="bntEdit" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                        <button class="btn red" type="submit" name="bntDelete" id="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
        </form>
    </div>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/bootstrap/bootstrap.min.js'></script>                
    <script type='text/javascript' src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../../js/plugins.js'></script>
    <script type="text/javascript" src="../../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type='text/javascript' src='../../../js/plugins/datatables/fnReloadAjax.js'></script>
    <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../../js/util.js"></script>    
    <script type="text/javascript" src="../../Academia/js/Orcamentos.js"></script>
    <script type="text/javascript" src="../../Academia/js/Vendas.js"></script>     
    <script type="text/javascript">               

        $("#txtData, #txtDataProximo, #txtDataNota").mask(lang["dateMask"]);
        $("#txtHora, #txtHoraProximo").mask("99:99");
        
        $("#txtDepartamento").change(function () {
            if ($(this).val()) {
                $("#txtPara").hide();
                $.getJSON("./../departamento.ajax.php?search=", {txtDepartamento: $(this).val(), ajax: "true"}, function (j) {
                    var options = "<option value=\"null\">Selecione</option>";
                    for (var i = 0; i < j.length; i++) {
                        options += "<option value=\"" + j[i].id_usuario + "\">" + j[i].nome + "</option>";
                    }
                    $("#txtPara").html(options).show();
                });
            } else {
                $("#txtPara").html("<option value=\"null\">Selecione</option>");
            }
        });

        function garantia() {
            if ($('#ckbGarantia').is(':checked')) {
                $('#txtDataNota').prop('disabled', false);
                $('#txtNotaFiscal').prop('disabled', false);
            } else {
                $('#txtDataNota').prop('disabled', true);
                $('#txtNotaFiscal').prop('disabled', true);
            }
        }
        
        function validar() {
            if ($("#txtReservarEstoque:checked").val() === "1" && $("#txtDtValidade").val() === "") {
                bootbox.alert("Para reservar é necessário informar um prazo de reserva!");
                return false;               
            }
            if ($('#txtIdVeiculo').length > 0 && $('#txtIdVeiculo').val() === "null") {
                bootbox.alert("Selecione um veículo, para criar a ordem de serviço!");
                return false;                
            }
            if ($('#txtKms').length > 0 && $('#txtKms').val() === "") {
                bootbox.alert("Preencha o campo de kms do veículo selecionado!");
                return false;                
            }
            return true;
        }
        
        $("#txtIdVeiculo").change(function () {
            getLegend();
        });
        
        function getLegend() {
            $('#txtMarca').val($('#txtIdVeiculo :selected').attr("marca"));
            $('#txtModelo').val($('#txtIdVeiculo :selected').attr("modelo"));
            $('#txtRenavam').val($('#txtIdVeiculo :selected').attr("renavam"));
        }
        
        if ($('#txtIdVeiculo option').length === 2) {
            $('#txtIdVeiculo').val($($('#txtIdVeiculo option')[1]).val()).trigger('change');
        }
        getLegend();
    </script>        
    <?php odbc_close($con); ?>    
</body>
