<?php

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

$allowedExts = array("gif", "jpeg", "jpg", "png", "pdf", "mp4", "GIF", "JPEG", "JPG", "PNG", "PDF", "MP4");
$allowedType = array("image/gif", "image/jpeg", "image/jpg", "image/pjpeg", "image/x-png", "image/png", "application/pdf", "video/mp4");
$allowedSize = 10000000;
$url =  __DIR__ . "./../../../Pessoas/" . $_SESSION["contrato"] . "/" . (isset($_POST['type']) ? $_POST['type'] : "Documentos") . "/" . 
((strlen($_POST["txtId"]) > 0 && !in_array($_POST['type'], ["Regulamentos", "Cotacao"])) ? $_POST["txtId"] . "/" : "");

if (isset($_POST['btnSendFile'])) {
    if (strlen($_POST["txtId"]) > 0) {       
        $sizeFile = $_FILES["file"]["size"];
        $typeFile = $_FILES["file"]["type"];
        $exteFile = explode(".", $_FILES["file"]["name"]);
        $exteFile = $exteFile[count($exteFile) - 1];         
        if (in_array($exteFile, $allowedExts) && in_array($typeFile, $allowedType) && $sizeFile < $allowedSize) {
            if ($_FILES["file"]["error"] == 0) {
                if (!is_dir($url)) {
                    mkdir($url, 0777, true);
                }
                move_uploaded_file($_FILES["file"]["tmp_name"], $url . "/" . (isset($_POST['nameFile']) ? $_POST['nameFile'] : utf8_decode($_FILES["file"]["name"])));
                echo "YES";
            } else {
                echo "Erro ao salvar arquivo!"; exit;   
            }
        } else {
            echo "Formato não suportado para esta operação, Apenas arquivos .gif,.jpeg,.jpg,.png ou pdf!"; exit;
        }
    }
}

if (isset($_POST['btnSendFileMultiple'])) {
    if (strlen($_POST["txtId"]) > 0) {                   
        if (!is_dir($url)) {
            mkdir($url, 0777, true);
        }
        $total = count($_FILES['file']['name']);
        for ($i = 0; $i < $total; $i++) {
            if (strlen($_FILES['file']['name'][$i]) > 0) {
                $sizeFile = $_FILES["file"]["size"][$i];
                $typeFile = $_FILES["file"]["type"][$i];
                $exteFile = explode(".", $_FILES["file"]["name"][$i]);
                $exteFile = $exteFile[count($exteFile) - 1];            
                if (in_array($exteFile, $allowedExts) && in_array($typeFile, $allowedType) && $sizeFile < $allowedSize) {
                    if ($_FILES["file"]["error"][$i] == 0) {
                        move_uploaded_file($_FILES["file"]["tmp_name"][$i], $url . "/" . (isset($_POST['nameFile']) ? $_POST['nameFile'] : utf8_decode($_FILES["file"]["name"][$i])));
                    } else {
                        echo "Erro ao salvar arquivo!"; exit;   
                    }                
                } else {
                    echo "Formato não suportado para esta operação, Apenas arquivos .gif,.jpeg,.jpg,.png ou pdf!"; exit;                
                }
            }
        }
    }
}

if (isset($_POST['btnDelFile'])) {
    if (substr($_POST['txtFileName'], 0, 9) == "vistoria_" && file_exists(str_replace("Documentos", "Assinaturas", $url) . "/" . str_replace(".pdf", ".png", $_POST['txtFileName']))) {
        unlink((str_replace("Documentos", "Assinaturas", $url) . "/" . str_replace(".pdf", ".png", $_POST['txtFileName'])));
    }
    if (file_exists($url . "/" . utf8_decode($_POST['txtFileName']))) {
        unlink($url . "/" . utf8_decode($_POST['txtFileName']));
        echo "YES";
    }       
}
