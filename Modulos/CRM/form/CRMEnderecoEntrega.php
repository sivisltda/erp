<?php

include "../../../Connections/configini.php";

if (isset($_POST['bntSaveEnd'])) {
    if (is_numeric($_POST['txtId'])) {
        if (is_numeric($_POST['txtIdEnt'])) {
            $query = " update sf_fornecedores_despesas_endereco_entrega set " .
                    "nome_end_entrega = " . valoresTexto('txtNomeEnt') . "," .
                    "end_entrega = " . valoresTexto('txtEnderecoEnt') . "," .
                    "numero_entrega = " . valoresTexto('txtNumeroEnt') . "," .
                    "complemento_entrega = " . valoresTexto('txtComplementoEnt') . "," .
                    "bairro_entrega = " . valoresTexto('txtBairroEnt') . "," .
                    "cidade_entrega = " . valoresSelect('txtCidadeEnt') . "," .
                    "estado_entrega = " . valoresSelect('txtEstadoEnt') . "," .
                    "cep_entrega = " . valoresTexto('txtCepEnt') . "," .
                    "telefone_entrega = " . valoresTexto('txtTelefoneEnt') . "," .
                    "referencia_entrega = " . valoresTexto('txtReferenciaEnt') . " " .
                    "where id_endereco = " . $_POST['txtIdEnt'];
        } else {
            $query = "insert into sf_fornecedores_despesas_endereco_entrega(fornecedores_despesas,nome_end_entrega,end_entrega,numero_entrega,complemento_entrega,bairro_entrega,cidade_entrega,estado_entrega,cep_entrega,telefone_entrega,referencia_entrega)values(" . $_POST['txtId'] . "," .
                    valoresTexto('txtNomeEnt') . "," .
                    valoresTexto('txtEnderecoEnt') . "," .
                    valoresTexto('txtNumeroEnt') . "," .
                    valoresTexto('txtComplementoEnt') . "," .
                    valoresTexto('txtBairroEnt') . "," .
                    valoresSelect('txtCidadeEnt') . "," .
                    valoresSelect('txtEstadoEnt') . "," .
                    valoresTexto('txtCepEnt') . "," .
                    valoresTexto('txtTelefoneEnt') . "," .
                    valoresTexto('txtReferenciaEnt') . ")";
        }
        odbc_exec($con, $query);
        echo "YES";
    }
}

if (isset($_POST['bntDelEnd'])) {
    if (is_numeric($_POST['txtIdx'])) {
        odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_endereco_entrega WHERE id_endereco = " . $_POST['txtIdx']);
        echo "YES";
    }
}

if (isset($_REQUEST["bntFind"])) {
    $local = array();
    if (is_numeric($_REQUEST['txtIdx'])) {
        $sql = "select nome_end_entrega, end_entrega, numero_entrega, complemento_entrega, bairro_entrega, estado_entrega, estado_nome, cidade_entrega, cidade_nome, cep_entrega, telefone_entrega, referencia_entrega
        from dbo.sf_fornecedores_despesas_endereco_entrega f left join tb_estados e on f.estado_entrega = e.estado_codigo
        left join tb_cidades c on f.cidade_entrega = c.cidade_codigo where id_endereco = " . $_REQUEST['txtIdx'];
        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $local[] = array('nome' => utf8_encode($RFP['nome_end_entrega']), 'endereco' => utf8_encode($RFP['end_entrega']), 'numero' => utf8_encode($RFP['numero_entrega']), 'complemento' => utf8_encode($RFP['complemento_entrega']), 'bairro' => utf8_encode($RFP['bairro_entrega']), 'estado' => utf8_encode($RFP['estado_entrega']), 'estado_nome' => utf8_encode($RFP['estado_nome']), 'cidade' => utf8_encode($RFP['cidade_entrega']), 'cidade_nome' => utf8_encode($RFP['cidade_nome']), 'cep' => utf8_encode($RFP['cep_entrega']), 'telefone' => utf8_encode($RFP['telefone_entrega']), 'referencia' => utf8_encode($RFP['referencia_entrega']));
        }
    }
    echo(json_encode($local));
}

odbc_close($con);
