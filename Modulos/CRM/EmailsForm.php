<?php
include "../../Connections/configini.php";
include "form/EmailsFormServer.php";
include "./../Comercial/modelos/variaveis.php";
$mdl_wha_ = returnPart($_SESSION["modulos"],16);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <style>
            .ui-dialog .ui-dialog-content, .ui-dialog-titlebar{
                border-left: 0px !important;
                border-right: 0px !important;
                border-top: 0px !important
            }
            .ui-dialog .ui-dialog-buttonpane button{
                background: #C22439 !important;
                border: 1px solid #C22439 !important;
            }
            .ui-dialog .ui-dialog-buttonpane{
                padding: 0 0px !important;
                padding: 0px;
            }
            .dropdown {
                width: 19%;
            }            
            .variaveis .dropdown .dropdown-toggle {
                width: 100%;
            }
            .variaveis .dropdown-menu{
                width: 100%;
                max-height: 420px;
            }            
            .data-fluid {
                overflow-x: auto;    
            }      
            .btn-icon {
                width: 36px;
                height: 25px;
                background-color: gray;
            }
        </style>
    </head>
    <body>
        <form name="frmEnviaDados" id="frmEnviaDados" method="POST" onkeydown="if (event.ctrlKey && event.keyCode === 86) { return false;}" enctype="multipart/form-data">
            <?php if ($image == "1") { ?>
                <div id="dialog-confirm" hidden="hidden">
                    <div>
                        <img name="picImag" id="picImag" src="<?php
                        $source = file_get_contents($pasta . "/" . $id . "/" . $id . ".jpg");
                        echo "data:image/png;base64," . base64_encode($source);
                        ?>" style="width:auto; height:auto; max-width:100%; max-height:275px;"/>
                    </div>
                </div>
            <?php } ?>
            <div class="frmhead">
                <div class="frmtext">Modelo de Disparo</div>
                <div class="frmicon" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input name="txtId" id="txtId" type="hidden" value="<?php echo $id; ?>"/>
                <div style="float:left; width:10%; height: 46px;">
                    <label for="txtTipo">Tipo:</label>
                    <select name="txtTipo" id="txtTipo" class="select" style="width:100%" <?php echo $disabled; ?>>
                        <option value="0" <?php echo ($tipo == 0 ? "selected" : ""); ?>>E-mail</option>
                        <option value="1" <?php echo ($tipo == 1 ? "selected" : ""); ?>>Sms</option>
                        <?php if ($mdl_wha_ > 0) { ?>
                        <option value="2" <?php echo ($tipo == 2 ? "selected" : ""); ?>>Whatsapp</option>
                        <?php } ?>
                    </select>
                </div>                
                <div style="float: left; width: 20%; margin-left:1%;">
                    <label for="txtDescricao">Descrição:</label>                    
                    <input name="txtDescricao" id="txtDescricao" type="text" style="width:100%" maxlength="50" <?php echo $disabled; ?> value="<?php echo $descricao; ?>"/>
                </div>                
                <div style="float: left; width: 20%; margin-left: 1%;">
                    <label for="txtAssunto">Assunto:</label>
                    <input name="txtAssunto" id="txtAssunto" type="text" maxlength="100" style="width:100%" <?php echo $disabled; ?> value="<?php echo $assunto; ?>"/>
                </div>
                <div style="float: left; width: 21%; margin-left: 1%; display:none;">                    
                    <label for="txtEmoticon">Ícones:</label>
                    <?php include './../CRM/include/ItensIcones.php'; ?>                                        
                </div> 
                <div style="float: left; width: 21%; margin-left: 1%; display:none;">
                    <label id="lblarquivo" for="arquivo">Imagem Publicidade:</label>
                    <?php if ($image === "0") { ?>
                        <input class="btn btn-primary" style="height:20px; line-height:21px;width: 100%;" type="file" name="arquivo" id="arquivo" <?php echo $disabled; ?> />
                    <?php } else { ?>
                        <button class="btn  btn-primary" type="button" title="Visualizar" style="width: 100px;"  id="btnVisualizar"> Visualizar </button>
                    <?php } ?>
                </div>
                <div style="float: left; width: 21%; margin-left: 3%; display:none;">
                    <input type="hidden" id="txtNmArquivo" name="txtNmArquivo" value="<?php echo $anexo; ?>"/>
                    <label for="anexo">Arquivo Anexo:</label>
                    <?php if ($isAnexo === "0") { ?>
                        <input class="btn btn-primary" style="height:20px; line-height:21px;width: 100%;" type="file" name="anexo" id="anexo" <?php echo $disabled; ?> />
                    <?php } else { ?>
                        <a href="form/EmailsFormServer.php?abrirArquivo=<?php echo $id; ?>&nmArquivo=<?php echo $anexo; ?>" title="Download File"> <?php echo $anexo; ?></a>
                        <input class="btn red" type="button" value="x" style="margin:0px; padding:2px 4px 3px 4px; line-height:10px;margin-left: 2%;" onClick="bootbox.confirm('Confirma a exclusão do anexo?', function (result) {
                            if (result) {
                                $('#btnDelAnexo').click();
                            }
                        });">
                        <input id="btnDelAnexo" name="btnDelAnexo" type="submit" style="display:none"/>
                    <?php } ?>
                </div>
                <div style="clear:both;"></div>                
                <div class="variaveis">
                    <hr style="margin: 5px 0px 5px 0px;">
                    <?php include './../Comercial/include/ItensMenu.php'; ?>
                </div>  
                <div style="clear:both;"></div>                
                <div style="width:100%;">
                    <hr style="margin:2px 0; border-top:1px solid #DDD">
                </div>
                <div class="data-fluid" id="ckTp1">
                    <label for="ckeditor"></label>
                    <textarea id="ckeditor" name="ckeditor" style="height: 403px;" onkeydown="if (event.ctrlKey && event.keyCode === 86) { return false;}" <?php echo $disabled; ?>><?php echo $mensagem; ?></textarea>
                </div>
                <div class="data-fluid" id="ckTp2" style="display: none;">
                    <label for="ckeditor"></label>
                    <textarea id="ckeditor2" name="ckeditor2" style="height: 403px;" <?php echo $disabled; ?>><?php echo $mensagem; ?></textarea>
                </div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == '') { ?>
                        <button class="btn btn-success" type="submit" name="btnSave" title="Gravar" id="btnSave" onclick="return validaForm();" ><span class="ico-checkmark"></span> Gravar</button>
                        <?php if ($_POST['txtId'] == '') { ?>
                            <button class="btn yellow" title="Cancelar" onClick="parent.FecharBox()" id="btnCancelar"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                            <button class="btn yellow" title="Cancelar" type="submit" name="btnCancelar" id="btnCancelar" ><span class="ico-reply"></span> Cancelar</button>
                            <?php
                        }
                    } else { ?>
                        <button class="btn  btn-success" type="submit" title="Novo" name="btnNew" id="btnNew" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                        <button class="btn  btn-success" type="submit" title="Alterar" name="btnEdit" id="btnEdit" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                        <div class="btn red" style="background-color:#68AF27; margin-right:1%" onClick="bootbox.confirm('Confirma a exclusão do registro?', function (result) {
                            if (result) {
                                $('#btnDelete').click();
                            }
                        });">
                            <div class="icon"><span class=" ico-remove"></span>&nbsp;Excluir</div>
                        </div>
                        <input id="btnDelete" name="btnDelete" type="submit" style="display:none"/>
                    <?php } ?>
                </div>
            </div>
        </form>
        <script type='text/javascript' src='../../js/plugins/jquery-ui-1.11.4/jquery-ui.min.js'></script>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/ckeditorAll/ckeditor.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type='text/javascript' src='js/EmailsForm.js'></script>        
        <?php odbc_close($con); ?>
    </body>
</html>