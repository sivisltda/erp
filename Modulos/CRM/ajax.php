  <?php

include '../../Connections/configini.php';
$texto = utf8_decode($_REQUEST['q']);

$tipo = str_replace("\\", "", $_REQUEST['t']);
$avulso = "";
if (isset($_REQUEST['r'])) {
    $avulso = " or id_fornecedores_despesas = 0";
} else if (isset($_REQUEST['socio'])) {
    $avulso = " AND titular = 1 AND socio = 1";
}
if ($tipo == "A") {
    $row['value'] = "LIGAÇÃO AVULSA";
    $row['id'] = 0;
    $row_set[] = $row;
} else {
    if ($tipo == "GCC") {
        $sql = "select id_grupo_contas id_fornecedores_despesas,descricao razao_social,'' telefone_contato,'' email_contato, '' tipo
                from sf_grupo_contas where deb_cre in ('C') and descricao like '%" . str_replace("'", "", $texto) . "%' order by descricao";
    } elseif ($tipo == "GCD") {
        $sql = "select id_grupo_contas id_fornecedores_despesas,descricao razao_social,'' telefone_contato,'' email_contato, '' tipo
                from sf_grupo_contas where deb_cre in ('D') and descricao like '%" . str_replace("'", "", $texto) . "%' order by descricao";
    } elseif ($tipo == "CMC") {
        $sql = "select sf_contas_movimento.id_contas_movimento id_fornecedores_despesas,sf_contas_movimento.descricao razao_social,sf_grupo_contas.descricao nome_fantasia,'' telefone_contato,'' email_contato, '' tipo
                from sf_contas_movimento inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas where tipo in ('C') and sf_contas_movimento.descricao like '%" . str_replace("'", "", $texto) . "%' order by sf_contas_movimento.descricao";
    } elseif ($tipo == "CMD") {
        $sql = "select sf_contas_movimento.id_contas_movimento id_fornecedores_despesas,sf_contas_movimento.descricao razao_social,sf_grupo_contas.descricao nome_fantasia,'' telefone_contato, '' email_contato, '' tipo
                from sf_contas_movimento inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas where tipo in ('D') and sf_contas_movimento.descricao like '%" . str_replace("'", "", $texto) . "%' order by sf_contas_movimento.descricao";
    } elseif ($tipo == "FPC") {
        $sql = "select id_tipo_documento id_fornecedores_despesas,descricao razao_social, '' telefone_contato, '' email_contato, '' tipo
                from sf_tipo_documento where inativo = 0 and s_tipo in  ('A','C') and descricao like '%" . str_replace("'", "", $texto) . "%' order by descricao";
    } elseif ($tipo == "FPD") {
        $sql = "select id_tipo_documento id_fornecedores_despesas,descricao razao_social, '' telefone_contato, '' email_contato, '' tipo
                from sf_tipo_documento where inativo = 0 and s_tipo in  ('A','D') and descricao like '%" . str_replace("'", "", $texto) . "%' order by descricao";
    } elseif ($tipo == "CCU") {
        $sql = "select id id_fornecedores_despesas,descricao razao_social, '' telefone_contato, '' email_contato, '' tipo
                from sf_centro_custo where descricao like '%" . str_replace("'", "", $texto) . "%' order by descricao";
    } elseif ($tipo == "U") {
        $sql = "select id_usuario id_fornecedores_despesas,nome razao_social,'' telefone_contato, '' email_contato, '' tipo from sf_usuarios where inativo = 0 and (nome like '%" . str_replace("'", "", $texto) . "%') order by nome";
    } elseif ($tipo == "UR") {
        $sql = "select login_user id_fornecedores_despesas,nome razao_social,'' telefone_contato, '' email_contato, '' tipo from sf_usuarios where inativo = 0 and (nome like '%" . str_replace("'", "", $texto) . "%') order by nome";
    } elseif ($tipo == "B") {
        $sql = "select id_fabricante id_fornecedores_despesas, nome_fabricante razao_social,'' telefone_contato, '' email_contato, '' tipo from sf_fabricantes where nome_fabricante like '%" . str_replace("'", "", $texto) . "%' order by nome_fabricante";
    } elseif ($tipo == "D") {
        $sql = "select id_departamento id_fornecedores_despesas, nome_departamento razao_social,'' telefone_contato, '' email_contato, '' tipo from sf_departamentos where nome_departamento like '%" . str_replace("'", "", $texto) . "%' order by nome_departamento";
    } elseif ($tipo == "PFE") {
        $sql = "select id_fornecedores_despesas,razao_social,nome_fantasia, (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato,
                (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = id_fornecedores_despesas) email_contato, tipo
                from sf_fornecedores_despesas where inativo = 0 and ((id_fornecedores_despesas > 0 and tipo in ('E','F'))" . $avulso . ") and (razao_social like '%" . str_replace("'", "", $texto) . "%' or nome_fantasia like '%" . str_replace("'", "", $texto) . "%') ORDER BY razao_social";
    } elseif($tipo == "GP"){
        $sql = "select id_grupo id_fornecedores_despesas, descricao_grupo razao_social, '' nome_fantasia, '' telefone_contato, '' email_contato, '' tipo
        from dbo.sf_grupo_cliente where descricao_grupo like '%".$texto."%' and inativo_grupo = 0 ";
    }else {
        $sql = "select id_fornecedores_despesas,razao_social,nome_fantasia,
                (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato,
                (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = id_fornecedores_despesas) email_contato, tipo
                from sf_fornecedores_despesas where inativo = 0 and ((id_fornecedores_despesas > 0 and tipo in ('" . $tipo . "'))" . $avulso . ") and 
                (razao_social like '%" . str_replace("'", "", $texto) . "%' or nome_fantasia like '%" . str_replace("'", "", $texto) . "%' " . 
                (is_numeric($texto) ? " or id_fornecedores_despesas = " . $texto : "") .  ") " .
                (isset($_REQUEST['s']) ? " and fornecedores_status like '" . $_REQUEST['s'] . "%'" : "") . 
                (isset($_REQUEST['n']) ? " and id_fornecedores_despesas not in (" . $_REQUEST['n'] . ")" : "") . 
                " ORDER BY razao_social";
    }
    //echo $sql; exit;
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $descricao = utf8_encode($RFP['razao_social']);
        if ($RFP['nome_fantasia'] != "") {
            $descricao = $descricao . " (" . utf8_encode($RFP['nome_fantasia']) . ")";
        }
        if ($RFP['tipo'] != "") {
            $descricao = $descricao . " [" . getTipoFornecedor($RFP['tipo']) . "]";
        }
        $row['value'] = $descricao;
        $row['id'] = utf8_encode($RFP['id_fornecedores_despesas']);
        $row['telefone'] = utf8_encode($RFP['telefone_contato']);
        $row['email'] = utf8_encode($RFP['email_contato']);
        $row_set[] = $row;
    }
}

function getTipoFornecedor($tipo) {
    if ($tipo == "P") {
        return "Prospect";
    } elseif ($tipo == "C") {    
        return "Cliente";
    } elseif ($tipo == "E") {    
        return "Funcionário";
    } elseif ($tipo == "F") {    
        return "Fornecedor";
    } elseif ($tipo == "T") {    
        return "Transportadora";
    }
}

echo json_encode($row_set);
odbc_close($con);
