
function validaForm() {

    if ($("#txtEntrada").val() === "") {
        bootbox.alert("Entrada é Obrigatória!");
        return false;
    }

    if ($("#txtSaida").val() === "") {
        bootbox.alert("Texto é Obrigatório!");
        return false;
    }

    return true;
}

function variable(e) {
    let curPos = $("#txtSaida").prop('selectionStart');
    $('#txtSaida').val($("#txtSaida").val().slice(0, curPos) + e + $("#txtSaida").val().slice(curPos));
}