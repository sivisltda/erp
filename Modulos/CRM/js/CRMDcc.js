$("#txtDCCValidade").mask("99/9999");
$("#txtDCCNumCartao").mask("99999999999999999999");

$('#txtDCCNumCartao').change(function () {
    if ($("#txtDCCNumCartao").val() !== "") {
        $('#txtDCCNumCartao').validateCreditCard(function (result) {
            $("#txtDCCNumCartao").removeClass();
            if (result.card_type !== null) {
                $("#txtDCCBandeira").val(result.card_type.code).trigger("change");
            }            
            $('#btnIncluirDCC').attr('disabled', false);
        });
    }
});

$(".selectCr").select2({
    placeholder: "",
    formatResult: function(item) { 
         return '<img src="../../img/cartao/' + item.id + '.png" style="height: 25px;"/>';
    },
    formatSelection: function(item) { 
         return '<img src="../../img/cartao/' + item.id + '.png" style="height: 25px; margin-top: -5px;"/>';
    }
});

var luhn10 = function(a,b,c,d,e) {
  for(d = +a[b = a.length-1], e=0; b--;)
    c = +a[b], d += ++e % 2 ? 2 * c % 10 + (c > 4) : c;
  return !(d%10);
};

function listDCC(id) {
    $('#tblDCC').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "../../Modulos/Academia/form/ClientesDCCServer.php?listDCC=" + $("#txtId").val(),
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });
}

$('#btnIncluirDCC').click(function (e) {
    if ($("#txtDCCNmCartao").val() === "" || $("#txtDCCNumCartao").val() === "" ||
            $("#txtDCCCodVer").val() === "" || $("#txtDCCValidade").val() === "" || $("#txtTipo").val() === null) {
        bootbox.alert("Todos os campos são obrigatórios!");
    } else if (!moment($("#txtDCCValidade").val(), "MM/YYYY", true).isValid()) {
        bootbox.alert("Formato de data inválido!");
    } else if (!$('#btnIncluirDCC').attr('disabled')) {
        $('#btnIncluirDCC').attr('disabled', true);
        $.post("../../Modulos/Academia/form/ClientesDCCServer.php", "AddCartao=S&txtId=" + $("#txtId").val() + 
                "&txtDCCNmCartao=" + $("#txtDCCNmCartao").val() + 
                "&txtDCCNumCartao=" + $("#txtDCCNumCartao").val() +
                "&txtDCCCodSeg=" + $("#txtDCCCodSeg").val() +
                "&txtDCCBandeira=" + $("#txtDCCBandeira").val() +
                "&txtDCCValidade=" + '01_' + $("#txtDCCValidade").val().replace("/", "_") +
                "&txtTipo=" + $("#txtTipo").val()).done(function (data) {
            var tblDCC = $('#tblDCC').dataTable();
            tblDCC.fnReloadAjax("../../Modulos/Academia/form/ClientesDCCServer.php?listDCC=" + $("#txtId").val());
            $("#txtDCCNmCartao, #txtDCCNumCartao, #txtDCCCodSeg, #txtDCCValidade").val("");
            $("#txtDCCBandeira").val(0).trigger("change");
        });
    }
});

function EditarCartao(id) {
    $("#btn_edit_" + id).hide();
    $("#btn_exlr_" + id).hide();
    $("#btn_save_" + id).show();
    $("#btn_canc_" + id).show();
    $("#txt_" + id).attr("disabled", false);
}

function RemoverCartao(id) {
    bootbox.confirm('Confirma a exclusão do Cartão?', function (result) {
        if (result === true) {
            $.post("../../Modulos/Academia/form/ClientesDCCServer.php", "DelCartao=" + id).done(function (data) {
                CancelarCartao();
            });
        }
    });
}

function SalvarCartao(id) {
    bootbox.confirm('Confirma o alteração do Cartão?', function (result) {
        if (result === true) {
            $.post("../../Modulos/Academia/form/ClientesDCCServer.php", "AltCartao=" + id + "&credencial=" + $("#txt_" + id).val()).done(function (data) {
                CancelarCartao();
            });
        }
    });
}

function CancelarCartao() {
    var tblDCC = $('#tblDCC').dataTable();
    tblDCC.fnReloadAjax("../../Modulos/Academia/form/ClientesDCCServer.php?listDCC=" + $("#txtId").val());
}

$('#tblDCC tbody td img').live('click', function () {
    var id = $(this).attr('data-id');
    var nTr = $(this).parents('tr')[0];
    var aData = $('#tblDCC').dataTable().fnGetData(nTr);
    if (id > 0) {
        if ($('#tblDCC').dataTable().fnIsOpen(nTr)) {
            this.src = "./../../img/details_open.png";
            $('#tblDCC').dataTable().fnClose(nTr);
        } else {
            this.src = "./../../img/details_close.png";
            fnFormatDetailsDCC(nTr, id);
        }
    }
});

function fnFormatDetailsDCC(nTr, id) {

    $.getJSON('../../Modulos/Academia/form/ClientesDCCServer.php', {listDCCDetails: id, idList: $("#txtId").val()}, function (j) {
        var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="width: 100%;">';
        for (var i = 0; i < j.length; i++) {
            sOut += '<tr><td style="width: 5%;">' + j[i].id + '</td><td style="width: 95%' +
                    ';">' + j[i].razao_social + '</td></tr>';
        }
        sOut += '</table>';
        $('#tblDCC').dataTable().fnOpen(nTr, sOut, 'details');
    });
}

function tbListaDCC() {
    $('#tbListaDCC').dataTable({
        "iDisplayLength": 20,
        "bProcessing": true,
        "bServerSide": true,
        "ordering": false,
        "bFilter": false,
        "bLengthChange": false,
        "sAjaxSource": final_url_dcc(),
        'oLanguage': {
            'oPaginate': {
                'sFirst': "Primeiro",
                'sLast': "Último",
                'sNext': "Próximo",
                'sPrevious': "Anterior"
            }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
            'sLengthMenu': "Visualização de _MENU_ registros",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers",
        "fnDrawCallback": function () {
            pos_processamento();
        }
    });
}

function final_url_dcc() {
    return "./../../util/dcc/DCC_server_processing.php?imp=0&tcl=1&cli=" + $("#txtId").val() + "&inv=" + ($("#ckb_dcc_invalidos").is(':checked') ? 1 : 0);
}

$("#ckb_dcc_invalidos").change(function () {
    var tbListaDCC = $('#tbListaDCC').dataTable();
    tbListaDCC.fnReloadAjax(final_url_dcc());
});

function ler_saldo() {
    $.getJSON("./../../util/dcc/saldoDCC.php", {ajax: "true"}, function (j) {
        $('#saldoImp').val(j);
        $('#saldoBtn').hide();
        $('#saldoImp').show();
    });
}

function pos_processamento() {
    $('#lbl_dcc_total').html("0");
    $('#lbl_dcc_vbruto').html(numberFormat(0));
    $('#lbl_dcc_vliquido').html(numberFormat(0));
    $('#lbl_dcc_total').html($('#txt_dcc_total').val());
    $('#lbl_dcc_vbruto').html($('#txt_dcc_vbruto').val());
    $('#lbl_dcc_vliquido').html($('#txt_dcc_vliquido').val());
}

function atualizatbListaDCC() {
    var tbListaDCC = $('#tbListaDCC').dataTable();
    tbListaDCC.fnReloadAjax(final_url_dcc());
}
