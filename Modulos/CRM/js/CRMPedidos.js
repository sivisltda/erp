$(document).ready(function () {
    if ($.isNumeric($('#txtId').val())) {
        $('#tbPedidos').dataTable({
            "iDisplayLength": 20,
            "aLengthMenu": [20, 30, 40, 50, 100],
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": URLPedidos(),
            'oLanguage': {
                'oPaginate': {
                    'sFirst': "Primeiro",
                    'sLast': "Último",
                    'sNext': "Próximo",
                    'sPrevious': "Anterior"
                }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                'sLengthMenu': "Visualização de _MENU_ registros",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sSearch': "Pesquisar:",
                'sZeroRecords': "Não foi encontrado nenhum resultado"},
            'fnInitComplete': function (oSettings, json) {
                refreshTotal();
            },
            "sPaginationType": "full_numbers",
            "bSort": false,
            "columnDefs": [{"visible": false, "targets": [0, 5, 6, 7]}]
        });
    }
});

function refreshPed() {
    if ($.isNumeric($('#txtId').val())) {
        var tblCRM = $('#tbPedidos').dataTable();
        tblCRM.fnReloadAjax(URLPedidos());
    }
}

function URLPedidos() {
    var finalURL = '?imp=0';
    if ($("#txtEstPedido").val() !== "") {
        finalURL = finalURL + '&id=' + $("#txtEstPedido").val();
    }
    if ($("#txtTipoBuscaPedido").val() !== "") {
        finalURL = finalURL + '&tp=' + $("#txtTipoBuscaPedido").val();
    }
    if ($("#txtId").val() !== "") {
        finalURL = finalURL + '&Cli=' + $("#txtId").val();
    }
    if ($("#txtUsuarioPedido").val() !== "") {
        finalURL = finalURL + '&td=' + $("#txtUsuarioPedido").val();
    }
    if ($("#txtFilial").val() !== "") {
        finalURL = finalURL + '&filial=' + $("#txtFilial").val();
    }
    if ($('#txt_dt_beginPedido').val() !== "") {
        finalURL = finalURL + "&dti=" + $('#txt_dt_beginPedido').val().replace(/\//g, "_");
    }
    if ($('#txt_dt_endPedido').val() !== "") {
        finalURL = finalURL + "&dtf=" + $('#txt_dt_endPedido').val().replace(/\//g, "_");
    }
    return "../../Modulos/Financeiro/Pedidos_server_processing.php" + finalURL;
}

function refreshTotal() {
    if ($('#qtdparcPedido').val() !== undefined) {
        $('#lblTotParcPedido2').html('Número de Pedidos :   <strong>' + $('#qtdparcPedido').val() + '</strong>').show('slow');
        $('#lblTotParcPedido3').html('Total em Pedidos :<strong>' + numberFormat($('#totparcPedido').val(), 1) + '</strong>').show('slow');
        $('#lblTotParcPedido4').html('Ticket Médio :<strong>' + numberFormat(($('#totparcPedido').val() / $('#qtdparcPedido').val()), 1) + '</strong>').show('slow');
    }
} 