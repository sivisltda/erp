
$(document).ready(function () {
    $("#txtDataHistorico, #txtDataHistoricoPrev").mask(lang["dateMask"]);
    $("#txtHoraHistorico, #txtHoraHistoricoPrev").mask("99:99");

    if ($.isNumeric($('#txtId').val())) {
        $("#historico").dataTable({
            "iDisplayLength": 20,
            "aLengthMenu": [20, 30, 40, 50, 100],
            "bProcessing": true,
            "bServerSide": true,
            "bSort": false,
            "sAjaxSource": "../../Modulos/CRM/Crm_Atendimentos_server_processing.php?imp=0&mdl=1&Cli=" + $('#txtId').val(),
            'oLanguage': {
                'oPaginate': {
                    'sFirst': "Primeiro",
                    'sLast': "Último",
                    'sNext': "Próximo",
                    'sPrevious': "Anterior"
                }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                'sLengthMenu': "Visualização de _MENU_ registros",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sSearch': "Pesquisar:",
                'sZeroRecords': "Não foi encontrado nenhum resultado"
            },
            'initComplete': function (settings, json) {
                if ($.isNumeric($("#txtId").val()) && $.isNumeric($("#txtAt").val())) {
                    AbrirBoxTelemarketing($("#txtAt").val(), $("#txtId").val(), 0, 1);
                }
                if ($("#tab3").hasClass("active")) {
                    limparChamado();
                }                
            },
            "sPaginationType": "full_numbers"
        });
    }    
});

// <editor-fold defaultstate="collapsed" desc="Atendimentos">
function AbrirBoxTelemarketing(idTele, idPess, Tipo, tp) {
    $.getJSON('../../Modulos/CRM/form/CRMFormServerTelemarketing.php', {txtIdx: idTele, bntFind: "S", txtQualificacao: $("#etapa2 .subtxt").text()}, function (j) {
        for (var i = 0; i < j.length; i++) {
            $('#txtIdx').val(j[i].id_tele);
            $('#txtDataHistorico').val(j[i].data_tele);
            $('#txtHoraHistorico').val(j[i].data_teleh);
            $("#txtTipoChamado").select2("val", j[i].especificacao);
            $('#txtDataHistoricoPrev').val(j[i].proximo_contato);
            $('#txtHoraHistoricoPrev').val(j[i].proximo_contatoh);
            $("#txtDeHistorico").select2("val", j[i].de_pessoa);
            $("#txtParaHistorico").select2("val", j[i].para_pessoa);
            $('#textoArea').val(j[i].relato);
            $('#ckbAcompanhar').parents('span').removeClass("checked").end().removeAttr("checked").change();
            if (j[i].acompanhar === "1") {
                $('#ckbAcompanhar').click();
            }
            TrataEnableCamposTele(j[i].pendente, j[i].para_pessoa, j[i].de_pessoa);
        }
    }).done(function () {});
    if (Tipo > 0) {
        if (idPess === null) {
            abrirTelaBox("../../Modulos/CRM/CRMFecharChamado.php?idx=" + $('#txtIdx').val() + "&fx=" + tp + "&tp=2&id=" + Tipo, 360, 600);
        } else {
            abrirTelaBox("../../Modulos/CRM/CRMFecharChamado.php?idx=" + idTele + "&fx=" + tp + "&tp=" + Tipo, 360, 600);
        }
    }
}

function FecharBox(opc, desc, iddesc, idprod, usu, valor) {
    if (desc !== undefined && desc !== 0) {
        rmDec(iddesc);
        descontos.push({idVariavel: iddesc, usuario: usu, valor: valor, idprod: idprod});
    }
    refreshChamado();
    if ($("#servicos").length) {
        refreshChamadoServico();
    }
    $("#newbox").remove();
}

function FecharBoxCanc(opc, desc, iddesc, idprod, usu, valor) {
    if (desc !== undefined && desc !== 0) {
        rmCan(iddesc);
        cancelamentos.push({idVariavel: iddesc, usuario: usu, valor: valor, idprod: idprod});
    }
    refreshChamado();
    if ($("#servicos").length) {
        refreshChamadoServico();
    }
    $("#newbox").remove();
}

function addChamado() {
    if ($('#textoArea').val().length > 0) {
        $.post("../../Modulos/CRM/form/CRMFormServerTelemarketing.php", {txtIdx: $('#txtIdx').val(), txtId: $('#txtId').val(),
            txtData: $('#txtDataHistorico').val(), txtHora: $('#txtHoraHistorico').val(), txtTipoChamado: $('#txtTipoChamado').val(),
            txtDataProximo: $('#txtDataHistoricoPrev').val(), txtHoraProximo: $('#txtHoraHistoricoPrev').val(), txtDeHistorico: $('#txtDeHistorico').val(),
            txtPara: $('#txtParaHistorico').val(), txtRelato: $('#textoArea').val(),
            ckbAcompanhar: $('#ckbAcompanhar').is(':checked') ? 1 : 0, txtQualificacao: $("#etapa2 .subtxt").text(), bntSave: "S"}).done(function (data) {
            if (data === "YES") {
                refreshChamado();
            } else {
                alert(data);
            }
        });
    } else {
        bootbox.alert("Preencha as informações corretamente!");
    }
}

function delChamado(numero) {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/CRM/form/CRMFormServerTelemarketing.php", "bntDelete=S&txtIdx=" + numero).done(function (data) {
                if (data === "YES") {
                    refreshChamado();
                } else {
                    alert(data);
                }
            });
        } else {
            return;
        }
    });
}

function delChamadoMsg(numero) {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/CRM/form/CRMFormServerTelemarketing.php", "bntDelMensagem=S&txtIdx=" + numero).done(function (data) {
                if (data === "YES") {
                    refreshChamado();
                } else {
                    alert(data);
                }
            });
        } else {
            return;
        }
    });
}

function refreshChamado() {
    if ($.isNumeric($('#txtId').val())) {
        var tblCRM = $('#historico').dataTable();
        tblCRM.fnReloadAjax("../CRM/Crm_Atendimentos_server_processing.php?imp=0&mdl=1&Cli=" + $('#txtId').val());
        limparChamado();
        refreshStatus();
    }
}

function TrataEnableCamposTele(aberto, para, de) {
    var idUsu = $("#txtIdUsu").val();
    var permissaoUpdate = $("#permissaoUpdate").val();
    var bloquearDeHistorico = $("#bloquearRemetente").val();
    if ($("#bntSaveMsg").length === 1 && ((aberto === "1" && permissaoUpdate === "1" && bloquearDeHistorico === "0") || (aberto === "1" && de === idUsu))) {
        $("#txtDeHistorico").select2('disable');
        $("#txtDataHistorico, #txtHoraHistorico, #txtDataHistoricoPrev, #txtHoraHistoricoPrev, #textoArea, #ckbAcompanhar, #bntSaveMsg").attr("disabled", false);
        $("#txtTipoChamado, #txtParaHistorico").select2('enable');
    } else {
        $("#txtDataHistorico, #txtHoraHistorico, #txtDataHistoricoPrev, #txtHoraHistoricoPrev, #textoArea, #ckbAcompanhar, #bntSaveMsg").attr("disabled", true);
        $("#txtTipoChamado, #txtDeHistorico, #txtParaHistorico").select2('disable');        
    }
}

function limparChamado() {
    var d = new Date();
    if (d.toLocaleString().length === 19) {
        $('#txtDataHistorico').val(d.toLocaleString().substring(0, 10));
        $('#txtHoraHistorico').val(d.toLocaleString().substring(11, 16));
        $('#txtDataHistoricoPrev').val(d.toLocaleString().substring(0, 10));
        $('#txtHoraHistoricoPrev').val(d.toLocaleString().substring(11, 16));
    }
    $('#txtIdx').val("");
    $('#txtTipoChamado').val("M");
    $("#txtDeHistorico").select2("val", $("#txtIdUsu").val());
    $("#txtParaHistorico").select2("val", $("#txtIdUsu").val());    
    $('#textoArea').val("");
    $('#ckbAcompanhar').parents('span').removeClass("checked").end().removeAttr("checked").change();
    TrataEnableCamposTele("1", $("#txtIdUsu").val(), $("#txtIdUsu").val());  
}

function fnFormatDetails(nTr) {
    var aData = $('#historico').dataTable().fnGetData(nTr);
    if (aData[11]) {
        $.getJSON('../../Modulos/CRM/mensagens.crm.ajax.php?search=', {codCrm: aData[11], ajax: 'true'}, function (j) {
            var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="width: 100%;">';
            for (var i = 0; i < j.length; i++) {
                var abrir1 = '';
                var abrir2 = '';
                var excluir = '';
                if (j[i].id_usuario === j[i].meu_id || j[i].id_usuariop === j[i].meu_id) {
                    abrir1 = '<a href="javascript:void(0)" onClick="AbrirBoxTelemarketing(' + aData[11] + ',null,' + j[i].id + ',1)">';
                    abrir2 = '</a>';
                    excluir = '<span onclick="delChamadoMsg(' + j[i].id + ');" style="float: right;margin-right: 25px;" class="ico-remove"></span>';
                }
                sOut += '<tr><td style="width: 50px;"></td><td style="width: 98px;color: #08c;">' +
                        abrir1 + j[i].data + abrir2 + '</td>' +
                        '<td style="width: 131px;"> De: ' + j[i].login_user + '</td>' +
                        '<td style="width: 133px;">Para: ' + j[i].login_userp + '</td>' +
                        '<td>' + j[i].mensagem + '</td><td>' + excluir + '</td></tr>';
            }
            sOut += '</table>';
            $('#historico').dataTable().fnOpen(nTr, sOut, 'details');
        });
    }
}

$('#historico tbody td img').live('click', function () {
    var nTr = $(this).parents('tr')[0];
    var aData = $('#historico').dataTable().fnGetData(nTr);
    if (aData[11] > 0) {
        if ($('#historico').dataTable().fnIsOpen(nTr)) {
            this.src = "./../../img/details_open.png";
            $('#historico').dataTable().fnClose(nTr);
        } else {
            this.src = "./../../img/details_close.png";
            fnFormatDetails(nTr);
        }
    }
});

// </editor-fold>

function refreshStatus() {
    if ($("#etapa1").length > 0) {   
        $.post("../../Modulos/CRM/form/CRMFormServerTelemarketing.php", "getStatus=" + $('#txtId').val()).done(function (data) {
            let estado = "";
            let cor = "";
            if (data === "Aguardando") {
                estado = "AGUARDANDO";      
                cor = "orange";
            } else if (data === "Negociacao") {
                estado = "EM NEGOCIAÇÃO";     
                cor = "#22b14c";
            } else if (data === "Analise") {
                estado = "EM ANÁLISE";
                cor = "#56acab";
            } else if (data === "Orcamento") {
                estado = "EM ORÇAMENTO";
                cor = "#0099cc";
            } else if (data === "Inativo") {
                estado = "INATIVO";
                cor = "red";
            }
            $(".ttermometro").html(estado);
            $(".ttermometro").css('color', cor);
        });
    }
}