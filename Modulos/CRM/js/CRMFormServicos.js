
$(document).ready(function () {
    if ($.isNumeric($('#txtId').val())) {
        $("#servicos").dataTable({
            "iDisplayLength": 20,
            "aLengthMenu": [20, 30, 40, 50, 100],
            "bProcessing": true,
            "bServerSide": true,
            "bSort": false,
            "sAjaxSource": "../../Modulos/CRM/Crm_Atendimentos_server_processing.php?imp=0&tp=1&mdl=1&Cli=" + $('#txtId').val(),
            'oLanguage': {
                'oPaginate': {
                    'sFirst': "Primeiro",
                    'sLast': "Último",
                    'sNext': "Próximo",
                    'sPrevious': "Anterior"
                }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                'sLengthMenu': "Visualização de _MENU_ registros",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sSearch': "Pesquisar:",
                'sZeroRecords': "Não foi encontrado nenhum resultado"
            },
            "sPaginationType": "full_numbers"
        });
    }
});

function AbrirBoxServicos(idTele, idPess, Tipo) {
    var final_url = "";
    if (idTele !== 0) {
        final_url += "&id=" + idTele;
    }
    if (idPess !== 0) {
        final_url += "&cli=" + idPess;
    }
    abrirTelaBox("../../Modulos/CRM/form/FormTelemarketing.php?tp=" + Tipo + final_url, 550, 800);
}

// <editor-fold defaultstate="collapsed" desc="Servicos">                               

function fnFormatDetailsServicos(nTr) {
    var aData = $('#servicos').dataTable().fnGetData(nTr);
    if (aData[11]) {
        $.getJSON('../../Modulos/CRM/mensagens.crm.ajax.php?search=', {codCrm: aData[11], ajax: 'true'}, function (j) {
            var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="width: 100%;">';
            for (var i = 0; i < j.length; i++) {
                var abrir1 = '';
                var abrir2 = '';
                var excluir = '';
                if (j[i].id_usuario === j[i].meu_id || j[i].id_usuariop === j[i].meu_id) {
                    abrir1 = '<a href="javascript:void(0)" onClick="AbrirBoxTelemarketing(' + aData[11] + ',null,' + j[i].id + ',2)">';
                    abrir2 = '</a>';
                    excluir = '<span onclick="delServicoMsg(' + j[i].id + ');" style="float: right;margin-right: 25px;" class="ico-remove"></span>';
                }
                sOut += '<tr><td style="width: 50px;"></td><td style="width: 98px;color: #08c;">' +
                        abrir1 + j[i].data + abrir2 + '</td>' +
                        '<td style="width: 131px;"> De: ' + j[i].login_user + '</td>' +
                        '<td style="width: 133px;">Para: ' + j[i].login_userp + '</td>' +
                        '<td>' + j[i].mensagem + '</td><td>' + excluir + '</td></tr>';
            }
            sOut += '</table>';
            $('#servicos').dataTable().fnOpen(nTr, sOut, 'details');
        });
    }
}

$('#servicos tbody td img').live('click', function () {
    var nTr = $(this).parents('tr')[0];
    var aData = $('#servicos').dataTable().fnGetData(nTr);
    if (aData[11] > 0) {
        if ($('#servicos').dataTable().fnIsOpen(nTr)) {
            this.src = "./../../img/details_open.png";
            $('#servicos').dataTable().fnClose(nTr);
        } else {
            this.src = "./../../img/details_close.png";
            fnFormatDetailsServicos(nTr);
        }
    }
});

function delServico(numero) {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/CRM/form/CRMFormServerTelemarketing.php", "bntDelete=S&txtIdx=" + numero).done(function (data) {
                if (data.trim() === "VENDA") {
                    bootbox.alert("Nao é possível excluir pois existe um Orçamento/Venda associada!");
                } else if (data === "YES") {
                    refreshChamadoServico();
                } else {
                    alert(data);
                }
            });
        } else {
            return;
        }
    });
}

function delServicoMsg(numero) {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/CRM/form/CRMFormServerTelemarketing.php", "bntDelMensagem=S&txtIdx=" + numero).done(function (data) {
                if (data === "YES") {
                    refreshChamadoServico();
                } else {
                    alert(data);
                }
            });
        } else {
            return;
        }
    });
}

function refreshChamadoServico() {
    if ($.isNumeric($('#txtId').val())) {
        var tblCRM = $('#servicos').dataTable();
        tblCRM.fnReloadAjax("../CRM/Crm_Atendimentos_server_processing.php?imp=0&tp=1&mdl=1&Cli=" + $('#txtId').val());
    }
}

// </editor-fold>