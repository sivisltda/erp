
$(document).ready(function () {

    var marcas;

    //obtendo dados do veiculo
    $.getJSON( 'form/ClientesVeiculosServer.php' , {
       id_fornecedores_despesas : $('#txtId').val()
    }).done(function(data){
        
        if(data){

            //obter os nomes das marcas
            // $.get('http://fipeapi.appspot.com/api/1//marcas.json').done(function(data){

            // });

            var html = '';

            for(i=0 ; i<= (data.length)-1 ;i++){

                html = '';
                
                var id_carro = data[i]['id'];
                var ano_modelo = data[i]['ano_modelo'];
                if(ano_modelo=='32000'){
                    ano_modelo = 'Zero KM';
                }
                var dirige_maior_tempo = data[i]['dirige_maior_tempo'];
                var modelo = data[i]['modelo'];
                var placa = data[i]['placa'];
                var renavam = data[i]['renavam'];
                var sinistro = data[i]['sinistro'];
                var condutores_18 = data[i]['condutores_18'];
                var uso = data[i]['uso'];
                var valor = data[i]['valor'];
                var marca = data[i]['marca'];                

                if(valor!=''){
                    valor = parseFloat(valor);
                    valor = valor.toFixed(2);                    
                }
                
                html +=  '<div class="veicle" id="' + id_carro + '" >';
                    html += '<p> <strong> Modelo: </strong> <span>' + modelo + '</span> <strong class="seemore" onclick="abreDiv(' + id_carro + ');" > Ver mais </strong> </p>' ;
                    html += '<div id="md' + id_carro + '" class="moredata">';

                        //html += '<p id="marca'+i+'" > <strong> Marca: </strong> <span>' + marca + '</span> </p>' ;
                        html += '<p> <strong> Placa: </strong> <span>' + placa + '</span> </p>' ;
                        html += '<p> <strong> Renavam: </strong> <span>' + renavam + '</span> </p>' ;
                        html += '<p> <strong> Valor Tabela FIPE: </strong> <span>R$' + valor + '</span> </p>' ;
                        html += '<p> <strong> Ano: </strong> <span>' + ano_modelo + '</span> </p>' ;
                        html += '<p> <strong> Uso: </strong> <span>' + uso + '</span> </p>' ;
                        html += '<p> <span> <strong>' + dirige_maior_tempo + '</strong> eu quem dirige a maior parte do tempo o carro </span> </p>' ;
                        html += '<p> <span> Eu declaro que <strong>' + condutores_18 + '</strong> existem condutores entre 18 e 25 anos que dirigem o veículo </span> </p>' ;
                        html += '<p> <span> Evento: <strong>' + sinistro + ' </strong> </span> </p>' ;
                    html += '</div>';
                html += '</div>' ;

                $('#divVeiculos').find('#titVeiculos').after( html );

                // $.get('http://fipeapi.appspot.com/api/1/carros/veiculos/'+marca+'.json')
                // .done(function(data){

                //     localStorage.setItem("marca_nome",data[0].fipe_marca);
                //     console.log(localStorage.getItem("marca_nome"));

                // });

                // $('p#marca'+i).html( '<strong> Marca: </strong> <span>' + localStorage.getItem("marca_nome") + '</span>' );

                // localStorage.removeItem("marca_nome");

            }

            // for(i=0 ; i<= (data.length)-1 ;i++){
                
            //     marca_fipe = $.getJSON('http://fipeapi.appspot.com/api/1/carros/veiculos/'+marca+'.json')
            //     .done(function(data){

            //         return data[0].fipe_marca;

            //     });

            //     console.log(marca_fipe);
                
            // }

        }
    });

});

function abreDiv(id){
    $('.moredata').slideUp('fast');
    $('#md'+id).slideToggle('fast');
}