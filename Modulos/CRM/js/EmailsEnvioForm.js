
$(document).ready(function () {
    var ckeditor = CKEDITOR.replace('ckeditor', {height: 254});
    
    ckeditor.on('instanceReady',function(){
        setFirstOption();
    });

    $("#txtEmails").val(parent.listaEmails());
    $('#txtModelo').change(function () {
        ckeditor.setData("");
        $("#txtAssunto").val("");
        $("#linkArq").text("");
        $("#btnUpArquivo, #btnUpAnexo").show();
        $("#btnVerArquivo, #btnVerAnexo").hide();
        ckeditor.setReadOnly(false);
        $("#txtAssunto").attr("disabled", false);
        $("#arquivo").attr("disabled", false);
        $("#anexo").attr("disabled", false);
        $("#txtId").val("");
        if ($(this).val() && $(this).val() !== "null") {
            $("#arquivo").attr("disabled", true);
            $("#anexo").attr("disabled", true);
            $.getJSON("form/EmailsEnvioFormServer.php", {getEmail: $(this).val()}, function (j) { }).always(function (j) {
                if (j !== null) {
                    $("#txtId").val(j[0].id_email);
                    $("#txtAssunto").val(j[0].assunto);
                    ckeditor.setData(j[0].mensagem);
                    if (j[0].isimagem === "1") {
                        $("#btnVerArquivo").show();
                        $("#btnUpArquivo").hide();
                        $('#img').attr('src', j[0].imagem);
                    } else {
                        $("#btnVerArquivo").hide();
                        $("#btnUpArquivo").show();
                    }
                    if (j[0].isanexo === "1") {
                        $("#btnVerAnexo").show();
                        $("#btnUpAnexo").hide();
                        $("#linkArq").text(j[0].anexo);
                        $("#linkArq").attr("href", "form/EmailsFormServer.php?abrirArquivo=" + j[0].id_email + "&nmArquivo=" + j[0].anexo)
                    } else {
                        $("#btnVerAnexo").hide();
                        $("#btnUpAnexo").show();
                    }
                }
                ckeditor.setReadOnly(true);
                $("#txtAssunto").attr("disabled", true);
            });
        }
    });

    $('#btnVisualizar').click(function (e) {
        fnOpenDialog("I");
    });

    $('#btnDestinatários').click(function (e) {
        fnOpenDialog("E");
    });

    $("#btnEnviar").click(function (e) {
        if ($("#txtModelo").val() === "null" && $("#txtAssunto").val() === "") {
            bootbox.alert("É obrigatório definir um modelo ou um assunto 2!");
        } else if ($("#txtEmails").val() === "") {
            bootbox.alert("Não foi encontrado nenhum usúario para enviar email!");
        } else {
            var maxTotal = 0; //50;
            if ($("#arquivo").val() !== "" || $("#anexo").val() !== "") {
                maxTotal = 1; //50;
            } else {
                maxTotal = 7; //50;
            }
            ckeditor.updateElement();
            var formData = new FormData();
            formData.append('btnEnviar', 'S');
            formData.append('txtId', $("#txtId").val());
            formData.append('txtAssunto', $("#txtAssunto").val());
            formData.append('txtEmails', []);            
            formData.append('ckeditor', $("#ckeditor").val());
            formData.append('arquivo', $('#arquivo')[0].files[0]);
            formData.append('anexo', $('#anexo')[0].files[0]);
            var emailTotal = $("#txtEmails").val().split(",");
            var repeticao = 1;
            if (emailTotal.length > maxTotal) {
                repeticao = Math.ceil(emailTotal.length / maxTotal);
            }
            $("#totalde").text(repeticao);
            processingAjax(formData, maxTotal);
        }
    });

    function processingAjax(formData, maxTotal) {
        if (parseInt($("#totalpara").text()) < parseInt($("#totalde").text())) {
            var itemAtual = parseInt($("#totalpara").text()) * maxTotal;
            $("#totalpara").text(parseInt($("#totalpara").text()) + 1);
            var emailTotal = $("#txtEmails").val().split(",");
            var emailrepeticao = [];
            for (var item = itemAtual; item < itemAtual + maxTotal; item++) {
                if (emailTotal[item] !== undefined) {
                    emailrepeticao.push(emailTotal[item]);
                }
            }
            formData.set('txtEmails', emailrepeticao);
            $("#loader", window.parent.document).show();
            $.ajax({
                type: "POST",
                url: "form/EmailsEnvioFormServer.php",
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false
            }).always(function (data) {
                if (data !== null) {
                    if (data[0].qtd_total !== undefined) {
                        var qtderro = data[0].qtd_total - data[0].qtd_sucesso;
                        trataResultado(data[0].qtd_sucesso, qtderro);
                    } else {
                        trataResultado(0, 0);
                    }
                }
                $("#loader", window.parent.document).hide();
                processingAjax(formData, maxTotal);
            });
        }
    }

    var qtdSucesso = 0;
    var qtdFalha = 0;
    function trataResultado(qtdS, qtdE) {
        qtdSucesso = qtdSucesso + qtdS;
        qtdFalha = qtdFalha + qtdE;
        if ($("#totalde").text() === $("#totalpara").text()) {
            $("#loader", window.parent.document).hide();
            bootbox.alert("Foram enviado(s)" + (qtdSucesso + qtdFalha) + " email(s):<br><br><b>* Sucesso(s):" + qtdSucesso + "</b><br><b style='color:red'>* Falha(s):" + qtdFalha + "</b>", function () {
                parent.FecharBox(1);
            });
        }
    }

    $('#btnNovoEmail').click(function (e) {
        if (validateEmail($("#txtNovoEmail").val())) {
            var newArray = $("#txtEmails").val() !== "" ? $("#txtEmails").val().split(',') : [];
            newArray.push($("#txtNovoEmail").val().toUpperCase() + "|0");
            $("#txtEmails").val(newArray);
            atualizaLista();
            $("#txtNovoEmail").val("");
        } else {
            bootbox.alert("Preencha um endereço de e-mail corretamente!");
        }
    });
});

function excluirEmail(position) {
    var newArray = $("#txtEmails").val().split(',');
    newArray.splice(position, 1);
    if (newArray.length > 0) {
        $("#txtEmails").val(newArray);
    } else {
        $("#txtEmails").val("");
    }
    atualizaLista();
}

function atualizaLista() {
    $("#listEmails").find("table").remove();
    if ($("#txtEmails").val() !== "") {
        var listEmails = "<table style=\"height: 237px;overflow-y: auto;overflow-x: hidden;display: block;\">";
        var newArray = $("#txtEmails").val().split(',');
        for (var i = 0; i < newArray.length; i++) {
            if (newArray[i].split('|')[0] !== "") {
                listEmails += "<tr id=\"" + i + "\"> " +
                        "     <td style=\"width: 50%\">" + newArray[i].split('|')[0] + "</td>" +
                        "     <td><span onclick=\"excluirEmail('" + i + "');\" style=\"float: right;margin-right: 19px;cursor: pointer;\" class=\"ico-remove\"></span></td></tr>";
            }
        }
        listEmails += "</table>";
        $("#listEmails").append(listEmails);
    }
}

function fnOpenDialog(tipo) {
    if (tipo === "E") {
        atualizaLista();
        $("#dialog-emails").dialog({
            resizable: false,
            modal: true,
            title: "Lista de Destinatários",
            height: 400,
            width: 340,
            buttons: {
                "Ok": function () {
                    $(this).dialog('close');
                }
            }
        });
    } else {
        $("#dialog-confirm").dialog({
            resizable: false,
            modal: true,
            title: "Imagem Publicidade",
            height: 400,
            width: 500,
            buttons: {
                "Ok": function () {
                    $(this).dialog('close');
                }
            }
        });
    }
}

function setFirstOption() {
    if ($("#txtModelo option").length > 1) {
        $('#txtModelo').val($("#txtModelo option")[1].value).trigger('change');
    }
}