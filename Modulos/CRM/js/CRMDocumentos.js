$(document).ready(function () {
    $('#tbDocumentos').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "bSort": false,
        "sAjaxSource": "../../Modulos/CRM/ajax/Documentos_server_processing.php?id=" + $("#txtId").val(),
        'oLanguage': {
            'oPaginate': {
                'sFirst': "Primeiro",
                'sLast': "Último",
                'sNext': "Próximo",
                'sPrevious': "Anterior"
            }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
            'sLengthMenu': "Visualização de _MENU_ registros",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });
});

function refreshDoc() {
    if ($.isNumeric($('#txtId').val())) {
        var tblCRM = $('#tbDocumentos').dataTable();
        tblCRM.fnReloadAjax("../../Modulos/CRM/ajax/Documentos_server_processing.php?id=" + $("#txtId").val());
    }
}

function AdicionarDoc(e) {
    $("#loader").show();
    var formData;
    if(e === 1){
      formData = new FormData($("#frmCRMForm")[0]);
    } else if (e === 2){
      formData = new FormData($("#frmClientesForm")[0]);
    } else {
      formData = new FormData($("#frmFuncionarioForm")[0]);
    }
    formData.append("btnSendFile", "S");
    $.ajax({
        type: 'POST',
        url: "../../Modulos/CRM/form/CRMDocumentos.php",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    }).done(function (data) {
        $("#loader").hide();
        if (data === "YES") {
            refreshDoc();
        } else {
            bootbox.alert(data);
        }
    });
}

function RemoverDoc(filename) {
    var nomedoc = encodeURIComponent(filename.toString());
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/CRM/form/CRMDocumentos.php", "btnDelFile=S&txtId=" + $("#txtId").val() + "&txtFileName=" + nomedoc).done(function (data) {
                if (data === "YES") {
                    refreshDoc();
                } else {
                    bootbox.alert(data);
                }
            });
        } else {
            return;
        }
    });
}
