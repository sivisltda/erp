$(document).ready(function () {
    var hash = $.trim(window.location.hash);
    if (hash) {
        $('.nav-tabs a[href$="' + hash + '"]').trigger('click');
    }    
    //RespLegal
    $("#txtCpfRespLegal").mask("999.999.999-99");
    $("#txtCepRespLegal").mask("99999-999");
    $("#txtCelRespLeg").mask("(99) 99999-9999");     
    //Convenios
    $("#txtConvDtIni, #txtConvDtFim").mask(lang["dateMask"]);
    
    if ($('#txtId').val() !== "") {
        listListRespLegal();
        listDependente();
        listConvenio();
        if ($("#txtPlanoEF").val() !== "") {
            $("#btnRemPlanoEmpresaFamilia").show();
            $("#btnAddPlanoEmpresaFamilia").hide();
            $("#btnTrocaLider").show();
        } else {
            $("#btnRemPlanoEmpresaFamilia").hide();
            $("#btnAddPlanoEmpresaFamilia").show();
            $("#btnTrocaLider").hide();
        }        
    }
    TrataEnableCampos();
});

//<editor-fold defaultstate="collapsed" desc="Imagem">

function UploadForm() {
    if ($("#txtId").val() !== "") {
        bootbox.dialog({
            message: "Importar Fotos",
            title: "",
            buttons: {
                success: {
                    label: "Camera",
                    className: "btn-success",
                    callback: function () {
                        AbrirBox(10);
                    }
                },
                main: {
                    label: "Arquivo",
                    className: "btn-primary",
                    callback: function () {
                        AbrirBox(11);
                    }
                }
            }
        });
    }
}

$('#btnRemImg').click(function (e) {
    bootbox.confirm('Confirma a exclusão da imagem?', function (result) {
        if (result === true) {
            $.post("../Academia/form/ClientesFormServer.php", "isAjax=S&RemoverImg=" + $("#txtId").val()).done(function (data) {
                if (data.trim() === "YES") {
                    $("#ImgCliente").attr("src", "./../img/img.png");
                    $("#btnRemImg").hide();
                } else {
                    alert(data);
                }
            });
        } else {
            return;
        }
    });
});

//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="EmpresaFamilia">

function DefinirLider(id) {
    $("iFrame").contents().find('span[class="ico-check"]').removeClass("ico-check").addClass("ico-check-empty");
    $("iFrame").contents().find("button[data-id='" + id + "']").find('span').removeClass("ico-check-empty").addClass("ico-check");
    $("iFrame").contents().find("#txtIdLiderFrame").val(id);
}

function SalvaConvLider(idForm) {
    if ($("iFrame").contents().find('span[class="ico-check"]').length === 1) {
        var param = "SalvarConvenio=S";
        if (idForm === 8) {
            param = "AtualizaConvenio=S";
        }
        $.post("../Academia/form/ClientesConveniosServer.php", param + "&txtId=" + $("#txtId").val() + "&txtIdConv=" + $("#txtPlanoEF").val() + "&txtLider=" + $("iFrame").contents().find("#txtIdLiderFrame").val()).done(function (data) {
            if (data.trim() === "YES") {
                $("#txtDescrLider").val($("iFrame").contents().find('span[class="ico-check"]').parent().parent().parent().parent().find("td:first").text().trim());
                $("#txtIdLider").val($("iFrame").contents().find("#txtIdLiderFrame").val());
                FecharBoxCliente(0);
                $("#btnAddPlanoEmpresaFamilia").hide();
                $("#btnRemPlanoEmpresaFamilia").show();
                $("#btnTrocaLider").show();
                $("#txtPlanoEF").select2('disable');
                $("#lblLider").css({'color': '#08c'});
                $("#lblLider").css({'cursor': 'pointer'});
                bootbox.alert("Alteração efetuada com sucesso efetuada com sucesso!");
            }
        });
    } else {
        FecharBoxCliente(0);
        bootbox.alert("É necessário selecionar um líder!");
    }
}

function goToLider() {
    if ($("#txtIdLider").val() !== "") {
        window.location = "ClientesForm.php?id=" + $("#txtIdLider").val();
    }
}

//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Convenios">

$('#btnIncluirConv').click(function (e) {
    if ($("#txtConvenios").val() !== "") {
        var data1 = moment($("#txtConvDtIni").val(), lang["dmy"]);
        var data2 = moment($("#txtConvDtFim").val(), lang["dmy"]);
        if (data2 < data1) {
            bootbox.alert("Periodo inválido!");
        } else {
            $.post("../Academia/form/ClientesConveniosServer.php", "SalvarConvenio=S&txtId=" + $("#txtId").val() + "&txtIdConv=" + $("#txtConvenios").val() + "&dti=" + $("#txtConvDtIni").val().replace(/\//g, "_") + "&dtf=" + $("#txtConvDtFim").val().replace(/\//g, "_")).done(function (data) {
                if (data.trim() === "YES") {
                    var tblConvenio = $('#tblConvenio').dataTable();
                    tblConvenio.fnReloadAjax("../Academia/form/ClientesConveniosServer.php?listConvenios=" + $("#txtId").val());
                } else {
                    bootbox.alert("Erro ao incluir convênio!");
                }
                $("#txtConvDtIni, #txtConvDtFim").val("");
                $("#txtConvenios").select2("val", "");
            });
        }
    } else {
        bootbox.alert("Escolha um convênio!");
    }
});

function RemoverConvenio(idConv) {
    $.post("../Academia/form/ClientesConveniosServer.php", "btnExcluirConvenio=S&txtId=" + $("#txtId").val() + "&txtIdConv=" + idConv).done(function (data) {
        var tblConvenio = $('#tblConvenio').dataTable();
        tblConvenio.fnReloadAjax("../Academia/form/ClientesConveniosServer.php?listConvenios=" + $("#txtId").val());
    });
}

function listConvenio() {
    $('#tblConvenio').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "../Academia/form/ClientesConveniosServer.php?listConvenios=" + $("#txtId").val(),
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });
}

function RemoverPlanoEF() {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            if ($("#txtId").val() === $("#txtIdLider").val()) {
                $.getJSON("../Academia/form/ClientesConveniosServer.php?listIntegrantes=" + $("#txtPlanoEF").val() + "&id=" + $("#txtId").val(), function (j) {
                    if (j.length > 1) {
                        bootbox.alert("Não é possível excluir pois este aluno é lider do plano!");
                    } else {
                        removePlanoId();
                    }
                });
            } else {
                removePlanoId();
            }
        } else {
            return;
        }
    });
}

function removePlanoId() {
    $.post("../Academia/form/ClientesConveniosServer.php", "btnExcluirConvenio=S&txtId=" + $("#txtId").val() + "&txtIdConv=" + $("#txtPlanoEF").val()).done(function (data) {
        if (data.trim() === "YES") {
            $("#btnAddPlanoEmpresaFamilia").show();
            $("#btnRemPlanoEmpresaFamilia").hide();
            $("#btnTrocaLider").hide();
            $("#txtDescrLider").val("");
            $("#txtPlanoEF").select2("val", "");
            $("#txtPlanoEF").select2('enable');
            $("#lblLider").css({'color': '#333'});
            $("#lblLider").css({'cursor': 'auto'});
        } else {
            bootbox.alert("Erro ao excluir!");
        }
    });
}

function preencheConvParticipantes(idForm) {
    var nParc = 1;
    var tbody = "";
    $("iFrame").contents().find("#tblConvParticipantes tbody > tr").remove()
    var param = "listLider=" + $("#txtPlanoEF").val() + "&id=" + $("#txtId").val();
    if (idForm === 8) {
        param = "listIntegrantes=" + $("#txtPlanoEF").val() + "&id=" + $("#txtId").val();
    }
    if ($("iFrame").contents().find("#txtNomeLider").val() !== "") {
        param += "&search=" + $("iFrame").contents().find("#txtNomeLider").val();
    }
    $.getJSON('../Academia/form/ClientesConveniosServer.php?' + param, function (j) {
        for (var i = 0; i < j.length; i++) {
            var btnLider = "<button class=\"btn btn-success\" data-id=\"" + j[i].id + "\" onclick=\"parent.DefinirLider('" + j[i].id + "')\" type=\"button\" style=\"margin:0px; padding:6px 4px 4px 6px;border-radius: 4px;font-size: 13px; line-height:22px;\"><span class=\"ico-check-empty\"></span></button>";
            if ((j[i].id_lider === j[i].id) && idForm === 8) {
                btnLider = "<button class=\"btn btn-success\" data-id=\"" + j[i].id + "\" onclick=\"parent.DefinirLider('" + j[i].id + "')\" type=\"button\" style=\"margin:0px; padding:6px 4px 4px 6px;border-radius: 4px;font-size: 13px; line-height:22px;\"><span class=\"ico-check\"></span></button>";
                $("iFrame").contents().find("#txtIdLiderFrame").val(j[i].id_lider);
            }
            tbody += "<tr id='linhaConvPart_" + nParc + "'> <td style='width: 434px;' valign='middle'>\n\
                        <center>  \n\
                        <input name='txtID_" + nParc + "' id='txtID_" + nParc + "' value='" + j[i].id + "' type='hidden'/> \n\
                        </center>" + j[i].razao_social + " </td>";
            tbody += "<td style='width: 84px;'><center> " + btnLider + "</center></td>";
            tbody += "</tr>";
            nParc = nParc + 1;
        }
    }).done(function (data) {
        $("iFrame").contents().find("#tblConvParticipantes tbody").append(tbody);
    });
}

//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Dependentes">

$('#btnIncluirDep').click(function (e) {
    if ($("#txtCodDep").val() !== "" && $("#txtDependente").val() !== "") {
        $.post("../Academia/form/ClientesDependentesServer.php", "SalvarDependente=S&txtId=" + $("#txtId").val()
                + "&txtIdDep=" + $("#txtCodDep").val()
                + "&txtParentesco=" + $("#txtParentesco").val()
                + "&ckbCompartilhado=" + ($("#ckb_compartilhado").is(":checked") ? "1" : "0") 
                + "&ckbResponsavel=" + ($("#ckb_responsavel").is(":checked") ? "1" : "0")).done(function (data) {
            if (data.trim() === "YES") {
                var tblDependente = $('#tblDependente').dataTable();
                tblDependente.fnReloadAjax("../Academia/form/ClientesDependentesServer.php?listDependentes=" + $("#txtId").val());
            } else {
                bootbox.alert(data.trim() + "!");
            }
            $("#txtCodDep, #txtDependente").val("");
            $("#txtParentesco").select2("val", "null");
        });
    }
});

function RemoverDependente(idDep) {
    $.post("../Academia/form/ClientesDependentesServer.php", "btnExcluirDependente=S&txtId=" + $("#txtId").val() + "&txtIdDep=" + idDep).done(function (data) {
        var tblDependente = $('#tblDependente').dataTable();
        tblDependente.fnReloadAjax("../Academia/form/ClientesDependentesServer.php?listDependentes=" + $("#txtId").val());
    });
}

function listDependente() {
    $('#tblDependente').dataTable({
        "iDisplayLength": 12,
        "aLengthMenu": [12, 20, 100],
        "bProcessing": true,
        "bServerSide": true,
        "ordering": false,
        "sAjaxSource": "../Academia/form/ClientesDependentesServer.php?listDependentes=" + $("#txtId").val(),
        "fnDrawCallback": function (data) {
            setLblDependente(data["aoData"]);
        },
        'oLanguage': {
            'oPaginate': {
                'sFirst': "Primeiro",
                'sLast': "Último",
                'sNext': "Próximo",
                'sPrevious': "Anterior"
            }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
            'sLengthMenu': "Visualização de _MENU_ registros",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });
}

function setLblDependente(data) {
    let resp = "";
    let resp_plus = "";
    $("#lblTitTitle, #lblTitular, #lblDepTitle, #lblDependentes, #imgDepPlus").hide();    
    for (var i = 0; i < data.length; i++) {
        if (i < 6) {
            resp += (i > 0 ? ", " : "") + data[i]["_aData"][0];
        } else {
            resp_plus += (i > 6 ? ", " : "") + data[i]["_aData"][0];
        }
    }
    if (resp !== "") {
        $("#lblDependentes").html(resp);
        $("#lblDependentesMais").html(resp_plus);
        $("#lblDepTitle, #lblDependentes").show();
    } else if ($("#lblTitular").length > 0 && $("#lblTitular").html().length > 0) {
        $("#lblTitTitle, #lblTitular").show();        
    }
    if (resp_plus !== "") {
        $("#imgDepPlus").show();
    }
}

$('#imgDepPlus').live('click', function () {
    $("#lblDependentesMais").toggle("slow");
    if (this.src.indexOf('details_close.png') > 0) {
        this.src = "./../../../img/details_open.png";
    } else {
        this.src = "./../../../img/details_close.png";
    }
});

$(function () {
    $("#txtDependente").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "../../Modulos/CRM/ajax.php",
                dataType: "json",
                data: {
                    q: request.term,
                    t: 'C'
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            $("#txtCodDep").val(ui.item.id);
        }
    });
    
    $("#txtNmRespLegal").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "../../Modulos/CRM/ajax.php",
                dataType: "json",
                data: {
                    q: request.term,
                    t: 'C'
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            AbrirRespLegal(ui.item.id);
        }
    });
    
    $('#txtNmRespLegal').blur(function () {
        if ($(this).val() === "") {
            RefreshRespLegal();
        }
    });
});

//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="RespLegal">

function listListRespLegal() {
    $('#tblListRespLegal').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "../Academia/form/ClientesResponsavelFormServer.php?ListRespLegal=" + $("#txtId").val(),
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        "fnDrawCallback": function (data) {
            setLblResponsavel(data["aoData"]);
        },
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });
}

function setLblResponsavel(data) {    
    let resp = "";
    $("#lblRespTitle, #lblResponsavel").hide();
    for (var i = 0; i < data.length; i++) {
        let nome = data[i]["_aData"][1].split('"');
        let result = data[i]["_aData"][6].split('RemoverRespLegal(');
        let result2 = result[1].split(')');
        let result3 = result2[0].split(',');
        if (result3[1] === "'C'") {
            resp += (i > 0 ? ", " : "") + "<a style=\"color: royalblue;\" href=\"ClientesForm.php?id=" + result3[0] + "\">" + nome[1] + "</a>";
        } else {
            resp += (i > 0 ? ", " : "") + nome[1];
        }
    }    
    if (resp !== "") {
        $("#lblResponsavel").html(resp);
        $("#lblRespTitle, #lblResponsavel").show();
    }
}

function setLblResponsavelDe() {    
    let resp = "";
    $("#lblRespDeTitle, #lblResponsavelDe").hide();
    $.getJSON("../Academia/form/ClientesResponsavelFormServer.php", "isAjax=S&ListRespLegalDe=" + $("#txtId").val(), function (data) {
        console.log(data);
        for (var x = 0; x < data.length; x++) {
            resp += (x > 0 ? ", " : "") + "<a style=\"color: royalblue;\" href=\"ClientesForm.php?id=" + data[x][0] + "\">" + data[x][1] + "</a>";
        }
        if (resp !== "") {
            $("#lblResponsavelDe").html(resp);
            $("#lblRespDeTitle, #lblResponsavelDe").show();
        }        
    });
}

setLblResponsavelDe();

function RefreshRespLegal() {
    $("#txtIdRespLegal, #txtNmRespLegal, #txtCpfRespLegal, #txtRgRespLegal, #txtProfRespLegal, #txtCepRespLegal, #txtEndRespLegal, #txtNumRespLegal, #txtBairroRespLegal, #txtComplRespLegal, #txtCelRespLeg, #txtEmailRespLeg").val("");
    $("#txtEstRespLegal, #txtCidRespLegal").select2("val", "null");
    $("#txtCivilRespLegal").select2("val", "SOLTEIRO(A)");
    $("#txtCivilRespLegal").select2('enable');
    $('#txtCelRespLeg, #txtEmailRespLeg, #txtCpfRespLegal, #txtRgRespLegal, #txtProfRespLegal, #txtCepRespLegal, #txtNumRespLegal, #txtComplRespLegal').attr("disabled", false);
    var tblDocumentos = $('#tblListRespLegal').dataTable();
    tblDocumentos.fnReloadAjax("../Academia/form/ClientesResponsavelFormServer.php?ListRespLegal=" + $("#txtId").val());
}

function AbrirRespLegal(id) {
    $.getJSON("../Academia/form/ClientesResponsavelFormServer.php", "isAjax=S&GetRespLegal=" + id, function (data) {
        for (var x = 0; x < data.length; x++) {
            $("#txtIdRespLegal").val(data[x].id_fornecedores_despesas);
            $("#txtTipoRespLegal").val(data[x].tipo);            
            $("#txtNmRespLegal").val(data[x].razao_social);
            $("#txtCpfRespLegal").val(data[x].cnpj);            
            $("#txtCelRespLeg").val(data[x].cel_resp);
            $("#txtEmailRespLeg").val(data[x].email_resp);
            $("#txtRgRespLegal").val(data[x].inscricao_estadual);
            $("#txtCepRespLegal").val(data[x].cep);
            $("#txtEndRespLegal").val(data[x].endereco);
            $("#txtNumRespLegal").val(data[x].numero);
            $("#txtBairroRespLegal").val(data[x].bairro);
            $("#txtComplRespLegal").val(data[x].complemento);
            $("#txtEstRespLegal").select2("val", data[x].estado);
            carregaCidade(data[x].estado, data[x].cidade, "txtCidRespLegal");
            $("#txtProfRespLegal").val(data[x].profissao);
            $("#txtCivilRespLegal").select2("val", data[x].estado_civil);
            if (data[x].tipo === "C") {
                $("#txtCivilRespLegal").select2('disable');
                $('#txtCelRespLeg, #txtEmailRespLeg, #txtCpfRespLegal, #txtRgRespLegal, #txtProfRespLegal, #txtCepRespLegal, #txtNumRespLegal, #txtComplRespLegal').attr("disabled", true);
            }
        }
    });
}

function RemoverRespLegal(id, tipo) {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../Academia/form/ClientesResponsavelFormServer.php", "isAjax=S&ExcluirRespLegal=" + id + 
                "&tipo=" + tipo + "&IdRespLegal=" + $("#txtId").val()).done(function (data) {
                if (data.trim() === "YES") {
                    RefreshRespLegal();
                } else {
                    alert(data);
                }
            });
        } else {
            return;
        }
    });
}

$("#btnSaveRespLegal").click(function (event) {
    var erro = "";
    if ($("#txtNumRespLegal").val() === "") {
        erro = 'O campo Número é obrigatório!';
    }
    if ($("#txtBairroRespLegal").val() === "") {
        erro = 'O campo Bairro é obrigatório!';
    }    
    if ($("#txtEndRespLegal").val() === "") {
        erro = 'O campo Logradouro é obrigatório!';
    }
    if ($("#txtCepRespLegal").val() === "") {
        erro = 'O campo CEP é obrigatório!';
    }
    if ($("#txtCpfRespLegal").val() === "") {
        erro = 'O campo CPF é obrigatório!';
    }
    if ($("#txtNmRespLegal").val() === "") {
        erro = 'O campo Nome é obrigatório!';
    }
    if ($("#txtIdRespLegal").val() === "" && $("#tblListRespLegal tbody tr").length >= 2) {
        erro = 'Não é possível inserir um novo Responsável Legal, limite de cadastro 2!';
    }
    if($("#txtLojaStatus").val() !== "" ){        
        if($("#txtCpfRespLegal").val() === ""){
            erro = "CPF Responsavel é um campo obrigatório!";
        }else if(!validate_cpf($("#txtCpfRespLegal").val())){
            erro ="CPF inválido!";
            $("#txtCpfRespLegal").focus();
        }
    }
    if (erro === "") {
        $.post("../Academia/form/ClientesResponsavelFormServer.php", "isAjax=S" +
                "&idRespLegal=" + $("#txtIdRespLegal").val() +
                "&TipoRespLegal=" + $("#txtTipoRespLegal").val() +
                "&NmRespLegal=" + $("#txtNmRespLegal").val() +
                "&CelRespLeg=" + $("#txtCelRespLeg").val() +
                "&EmailRespLeg=" + $("#txtEmailRespLeg").val() +
                "&CpfRespLegal=" + $("#txtCpfRespLegal").val() +
                "&RgRespLegal=" + $("#txtRgRespLegal").val() +
                "&CepRespLegal=" + $("#txtCepRespLegal").val() +
                "&EndRespLegal=" + $("#txtEndRespLegal").val() +
                "&NumRespLegal=" + $("#txtNumRespLegal").val() +
                "&BairroRespLegal=" + $("#txtBairroRespLegal").val() +
                "&EstRespLegal=" + $("#txtEstRespLegal").val() +
                "&CidRespLegal=" + $("#txtCidRespLegal").val() +
                "&ComplRespLegal=" + $("#txtComplRespLegal").val() +
                "&CivilRespLegal=" + $("#txtCivilRespLegal").val() +
                "&ProfRespLegal=" + $("#txtProfRespLegal").val() +
                "&SaveRespLegal=" + $("#txtId").val()).done(function (data) {
            if ($.isNumeric(data.trim())) {
                if($('#txtIdCliMU').val() === ''){
                    $('#btnAlterar').click();
                    $('#btnSave').click();
                }else{
                    RefreshRespLegal();
                }
            } else {
                alert(data);
            }
        });
    } else {
        bootbox.alert(erro);
    }
});

//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Outros">

function addSerasa(id, mod) {
    if (id === 1) {
        bootbox.alert("Operação não permitida para este usuário!");
    } else {
        var msg = '';
        var pag = '';
        var block = '';
        if (id === 2) {
            if (mod === 'pag') {
                block = '&blockpg=B';
                msg = 'Confirma o bloqueio de pagamento deste Aluno?';
            } else {
                msg = 'Confirma o bloqueio do SERASA deste Aluno?';
            }
        } else {
            if (mod === 'pag') {
                msg = 'Confirma o desbloqueio de pagamento deste Aluno?';
            } else {
                msg = 'Confirma o desbloqueio do SERASA deste Aluno?';
            }
        }
        bootbox.confirm(msg, function (result) {
            if (result === true) {
                $.post("../Academia/form/ClientesFormServer.php", "&btnSerasa=S&isAjax=S&txtId=" + $("#txtId").val() + "&tipo=" + id + block).done(function (data) {
                    window.location = "ClientesForm.php?id=" + data;
                });
            }
        });
    }
}

function validarGobody() {
    var linhas = $('[id*="tabela_linha_"]').size();
    var email = "";
    var telefone = "";
    for (i = 1; i <= linhas; i++) {
        if ($("#txtTpContato_" + i).val() === "0" || $("#txtTpContato_" + i).val() === "1") {
            telefone = $("#txtTxContato_" + i).val();
        } else if ($("#txtTpContato_" + i).val() === "2") {
            email = $("#txtTxContato_" + i).val();
        }
    }
    if ($("#txtCpf").val() === "") {
        bootbox.alert("O campo CPF é obrigatório!");
        return false;
    } else if ($("#txtNome").val() === "") {
        bootbox.alert("O campo NOME é obrigatório!");
        return false;
    } else if ($("#txtDtNasc").val() === "") {
        bootbox.alert("O campo DATA DE NASCIMENTO é obrigatório!");
        return false;
    } else if ($("#txtSexo").val() === "") {
        bootbox.alert("O campo SEXO é obrigatório!");
        return false;
    } else if (email === "") {
        bootbox.alert("O campo EMAIL é obrigatório!");
        return false;
    } else if (telefone === "") {
        bootbox.alert("O campo TELEFONE é obrigatório!");
        return false;
    }
    return true;
}

function imprimir_email(doc, tp) {
    if ($("#txtId").val() !== "") {
        abrirTelaBox("../CRM/ImprimirEmail.php?id=" + 
        $("#txtId").val() + "&doc=" + doc + "&tp=" + tp, 280, 400);
    }
}

function gymPass() {
    abrirTelaBox("ClientesGymPass.php?id=" + $("#txtId").val(), 480, 700);
}

(function ($) {
    $.fn.extend({
        Segment: function () {
            $(this).each(function () {
                var self = $(this);
                var onchange = self.attr('onchange');
                var wrapper = $("<div>", {class: "ui-segment"});
                $(this).find("option").each(function () {
                    var option = $("<span>", {class: 'option', onclick: onchange, text: $(this).text(), value: $(this).val()});
                    if ($(this).is(":selected")) {
                        option.addClass("active");
                    }
                    wrapper.append(option);
                });
                wrapper.find("span.option").click(function () {
                    if ($("#ckbEmailMarketing").attr("disabled") !== "disabled") {
                        wrapper.find("span.option").removeClass("active");
                        $(this).addClass("active");
                        self.val($(this).attr('value'));
                    }
                });
                $(this).after(wrapper);
                $(this).hide();
            });
        }
    });
})(jQuery);
jQuery(function ($) {
    $(".segment-select").Segment();
});

//</editor-fold>