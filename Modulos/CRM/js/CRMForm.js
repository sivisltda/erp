
$(document).ready(function () {       
    $('#txtNome').ajaxComboBox('../../Modulos/CRM/ClientesAjaxCombobox.php', {
        plugin_type: 'simple',
        lang: 'pt-br',
        primary_key: 'id',
        bind_to: 'foo'
    }).bind('foo', function () {
        window.location = $("#txtPage").val() + '?id=' + $('#txtNome_primary_key').val();
    });    

    $("#txtValCredito").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
    $("#txtQtdItensPagar").val("0");
    $("#txtTotalSemDesconto").val("0");    
    $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);    
    
    TrataEnableCampos();    
    if ($('#txtId').val() !== "") {
        listContatos($('#txtId').val());
        listDCC($('#txtId').val());
        tbListaDCC();                
        listPlanosClientes();
        listHistoricoServicos();
        listHistoricoBoleto();
        listHistoricoLink();        
        listPagamentosDependentes();
        listServicoFavorito();
        atualizaTotCredito();
        PagarTudoItens();        
    } else {
        TrataEnableCamposTele("1", $("#txtIdUsu").val(), $("#txtIdUsu").val());
    }
    
    $("#btnSerasa").hide();    
});

function AbrirBox(op, tp, id) {
    if (id > 0) {
        var myId = "?id=" + id;
    } else {
        var myId = "";
    }
    if (id > 0 && tp !== "") {
        var myTp = "&tp=" + tp;
    } else if (tp !== "") {
        var myTp = "?tp=" + tp;
    } else {
        var myTp = "";
    }
    if (op === 0) {
        $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:667px; height:433px; position:absolute; top:50%; left:50%; margin-left:-333px; margin-top:-252px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:433px' src='../../Modulos/Financeiro/FormLancamentoMovimento.php" + myId + "" + myTp + "&idFornecedor=" + $("#txtId").val() + "'></iframe></div></div></div></div></div>");
    } else if (op === 1) {
        abrirTelaBox("./../Academia/ClientesPagamentoForm.php", 536, 800);
    } else if (op === 2) {
        $("#divPagamentos").hide();
        abrirTelaBox("./../Academia/ClientesAddPlanosForm.php?tp=A", 435, 770);
    } else if (op === 3) {
        $("#divPagamentos").hide();
        abrirTelaBox("./../Academia/ClientesAddPlanosForm.php?tp=R", 435, 770);        
    } else if (op === 4) {
        abrirTelaBox("ClientesCongelamentoForm.php", 438, 770);
    } else if (op === 5) {
        AtualizaPagPlanosClientes(0);
        abrirTelaBox("ClientesPlanosDetForm.php", 410, 670);
    } else if (op === 6) {
        $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:540px; height:210px; position:absolute; top:50%; left:50%; margin-left:-290px; margin-top:-282px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:210px' src='ClientesTransferenciaForm.php'></iframe></div></div></div></div></div>");
    } else if (op === 7 && $("#txtPlanoEF").val() !== "") {
        abrirTelaBox("ClientesLiderForm.php?idForm=" + op, 438, 580);
    } else if (op === 8 && $("#txtPlanoEF").val() !== "") {
        abrirTelaBox("ClientesLiderForm.php?idForm=" + op, 438, 580);
    } else if (op === 9) {
        abrirTelaBox("./../Academia/ClientesHistoricoForm.php", 500, 770);
    } else if (op === 10) {
        document.location = "./../Academia/ClientesWebCam.php?id=" + $("#txtId").val();
    } else if (op === 11) {
        abrirTelaBox("./../Academia/ClientesCropModalForm.php", 539, 770);  
    } else if (op === 12) {
        abrirTelaBox("./../Academia/ClientesPagamentoForm.php?isDCC=S", 536, 800);
    } else if (op === 13) {
        abrirTelaBox("./../Academia/ClientesPagamentoForm.php?isDCC=C", 536, 800);
    } else if (op === 14) {
        abrirTelaBox("./../Academia/ClientesAgendamentoDccForm.php", 400, 430);
    } else if (op === 15) {
        abrirTelaBox("ClientesTurmasForm.php?idAluno=" + $("#txtId").val() + "&nmAluno=" + $("#txtNome").val(), 531, 770);
    } else if (op === 16) {
        abrirTelaBox("/modulos/academia/form/formLiberaRecebimento.php?txtIdx=8&bntFind=S&txtQualificacao=", 360, 600);
    } else if (op === 17) {
        abrirTelaBox("/modulos/academia/form/login_desc.php?idDesc=" + tp + "&idProd=" + id + "&tp=D", 350, 400);
    } else if (op === 18) {
        abrirTelaBox("/modulos/academia/form/login_desc.php?idDesc=" + tp + "&idProd=" + id + "&tp=C", 350, 400);
    } else if (op === 20) {
        abrirTelaBox("./../Financeiro/DetalheCalendario.php" + myId, 425, 760);
    } else if (op === 21) {
        abrirTelaBox("./../Financeiro/FormLanc-Parcelas.php" + myId, 330, 390);
    } else if (op === 30) {
        abrirTelaBox("./../Financeiro/DetalheParcelasPlano.php" + myId, 425, 760);
    }    
}

$('#btnNovo').click(function (e) {
    window.location = $("#txtPage").val();
});

$('#btnSave').click(function (e) {
    if (validaForm()) {
        $.post("../../Modulos/CRM/form/CRMFormServer.php", "isAjax=S&btnSave=S&txtTpCli=" + $("#txtTpCli").val() + 
            "&txtId=" + $("#txtId").val() + "&txtNome=" + $("#txtNome").val().replace(/\&/g, "*") + 
            "&txtTermometro=" + $("#txtTermometro").val() + "&" + $("#tab1 :input").serialize()).done(function (data) {
            if ($.isNumeric(data)) {
                window.location = $("#txtPage").val() + "?id=" + data;
            } else {
                bootbox.alert(data);
            }
        });
    }
});

$('#btnExcluir').click(function (e) {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/CRM/form/CRMFormServer.php", "isAjax=S&btnExcluir=S&txtId=" + $("#txtId").val()).done(function (data) {
                window.location = $("#txtPage").val();
            });
        } else {
            return;
        }
    });
});

function ativaEtapa(id, data) {
    if (id === 4 && data === "" && validaForm()) {
        if ($("#tab0").length > 0 && tbListaVistorias.fnSettings().fnRecordsTotal() === 0) {   
            bootbox.alert("Preencha as informações de vistorias corretamente!");            
        } else if ($("#tab0").length > 0 && tbListaVeiculo.fnSettings().fnRecordsTotal() === 0) {
            bootbox.alert("Preencha as informações de veículos corretamente!"); 
        } else if ($("#txtDtNasc").val() === "") {
            bootbox.alert("Data de Nascimento é Obrigatório!");
        } else {
            bootbox.confirm('Confirma a conversão deste Prospect para Cliente?', function (result) {
                if (result === true) {
                    $.post("form/CRMFormServer.php", "isAjax=S&btnConvCliente=S&txtId=" + $("#txtId").val()).done(function (data) {
                        if ($.isNumeric(data)) {
                            if ($("#mdlAca").val() === "1") {
                                window.location = "./../../modulos/Academia/ClientesForm.php?id=" + data;
                            } else {
                                window.location = $("#txtPage").val() + "?id=" + data;
                            }
                        } else {
                            bootbox.alert(data);
                        }
                    });
                } else {
                    return;
                }
            });
        }
    }
}

function temperatura(id) {
    $("#temps0, #temps1, #temps2, #temps3, #temps4").removeClass("ico-caret-up");
    $("#temps" + id).addClass("ico-caret-up");
    $("#txtTermometro").val(id);
}

temperatura($("#txtTermometro").val());