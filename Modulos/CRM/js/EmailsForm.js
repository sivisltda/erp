$(document).ready(function () {
    CKEDITOR.replace('ckeditor', {height: 332, width: 1086});
    setTipo();
});

$('#txtTipo').change(function () {
    setTipo();
});

function setTipo() {
    if ($("#txtTipo").val() === "0") {
        $("#ckTp1").show();
        $("#ckTp2").hide();
        $("#txtNmArquivo").parent().show();
        $("#lblarquivo").parent().show();
        $("#txtEmoticon").parent().hide();
    } else if ($("#txtTipo").val() === "1") {
        $("#ckTp1").hide();        
        $("#ckTp2").show();
        $("#txtNmArquivo").parent().hide();
        $("#lblarquivo").parent().hide();        
        $("#txtEmoticon").parent().hide();        
    } else if ($("#txtTipo").val() === "2") {
        $("#ckTp1").hide();        
        $("#ckTp2").show();
        $("#txtNmArquivo").parent().hide();
        $("#lblarquivo").parent().hide(); 
        $("#txtEmoticon").parent().show();        
    }
}

function fnOpenDialog() {
    $("#dialog-confirm").dialog({
        resizable: false,
        modal: true,
        title: "Imagem Publicidade",
        height: 400,
        width: 500,
        buttons: {
            "Remover": function () {
                excluirImagem();
                $(this).dialog('close');
            }
        }
    });
}

function excluirImagem() {
    bootbox.confirm('Confirma a exclusão da Imagem?', function (result) {
        if (result === true) {
            $.post("form/EmailsFormServer.php", "DelImage=" + $("#txtId").val()).done(function (data) {
                if (data.trim() === "YES") {
                    document.location.reload();
                } else {
                    bootbox.alert("Erro ao excluir a imagem!");
                }
            });
        } else {
            return;
        }
    });
}

function validaForm() {

    if ($("#txtDescricao").val() === "") {
        bootbox.alert("Descrição é Obrigatória!");
        return false;
    }

    if ($("#txtAssunto").val() === "") {
        bootbox.alert("Assunto é Obrigatório!");
        return false;
    }

    if ($("#txtTipo").val() === "0" && CKEDITOR.instances['ckeditor'].getData() === "" && $("#txtArquivo").val() === "") {
        bootbox.alert("É obrigatório uma imagem ou um texto!");
        return false;
    }

    if ($("#txtTipo").val() === "0" && $('#anexo').val() !== undefined && $('#anexo').val() !== "") {
        if ($('#anexo').val().split('\\').pop().length > 30) {
            bootbox.alert("Nome arquivo muito extenso, máximo 30 caracteres!");
            return false;
        }
    }

    if ($("#txtTipo").val() === "0" && $('#arquivo').val() !== undefined && $('#arquivo').val() !== "") {
        var extension = $('#arquivo').val().replace(/^.*\./, '').toLowerCase();
        if (extension !== 'jpg') {
            bootbox.alert("Extensão inválida, permitido somente jpg!");
            return false;
        }
    }

    return true;
}

function variable(e) {
    if ($("#txtTipo").val() === "0") {
        CKEDITOR.instances['ckeditor'].insertText(e);
    } else { 
        let curPos = $("#ckeditor2").prop('selectionStart');
        $('#ckeditor2').val($("#ckeditor2").val().slice(0, curPos) + e + $("#ckeditor2").val().slice(curPos));
    }
}