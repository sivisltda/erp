function TeclaKey(event) {
    if (event.keyCode === 13) {
        $("#btnPesquisar").click();
    }
}

$(document).ready(function () {
    $("#btnPesquisar").click(function () {
        tbLista.fnReloadAjax(finalFind(0));
    });
});

var tbLista;
tbLista = $('#tblEmails').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": finalFind(0),
    "bFilter": false,
    "aoColumns": [{"bSortable": true},
        {"bSortable": true},
        {"bSortable": false},
        {"bSortable": false}
    ],
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"
    },
    "sPaginationType": "full_numbers",
    "fnInitComplete": function () {
        if ($("#imprimir").val() === "1") {
            $(".body").css("margin-left", 0);
            $("#parametros_busca").hide();
            $("#tblEmails_length").remove();
            $("#tblEmails_filter").remove();
            $("#tblEmails_paginate").remove();
            $("#formPQ").find("> th").css("background-image", "none");
            window.print();
            window.history.back();
        }
    }
});

function AbrirBox(id) {
    var opc = "";
    if (id > 0) {
        opc = "?id=" + id;
    }
    abrirTelaBox("EmailsForm.php" + opc, 650, 1130);
}

function FecharBox() {
    var oTable = $('#tblEmails').dataTable();
    oTable.fnDraw(false);
    $("#newbox").remove();
}

function finalFind(op) {
    var retPrint = "&Search=" + $("#txtBuscar").val() + "&tp=" + $("#txtTipo").val();
    if (op === 0) {
        return "ajax/Emails_server_processing.php?imp=" + $("#imprimir").val() + retPrint;
    } else {
        return "EmailsList.php?imp=1" + retPrint;
    }
}

function RemoverItem(id) {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("form/EmailsFormServer.php", {List: true, btnDelete: true, txtId: id}).done(function (data) {
                if (data.trim() === "YES") {
                    bootbox.alert("Registro excluido com sucesso!");
                    tbLista.fnReloadAjax(finalFind(0));
                } else {
                    bootbox.alert("Erro ao excluir registro!");
                }

            });
        } else {
            return;
        }
    });
}