
$(document).ready(function () {
    
    var columns = [{"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false}
    ];
    if ($.isNumeric($('#txtId').val())) {
        $('#tblContratos').dataTable({
            "iDisplayLength": 20,
            "aLengthMenu": [20, 30, 40, 50, 100],
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": finalFind(0),
            "bFilter": false,
            "aoColumns": columns,
            'oLanguage': {
                'oPaginate': {
                    'sFirst': "Primeiro",
                    'sLast': "Último",
                    'sNext': "Próximo",
                    'sPrevious': "Anterior"
                }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                'sLengthMenu': "Visualização de _MENU_ registros",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sSearch': "Pesquisar:",
                'sZeroRecords': "Não foi encontrado nenhum resultado"},
            "sPaginationType": "full_numbers"
        });
    }
});

function refresh() {
    var oTable = $('#tblContratos').dataTable();
    oTable.fnReloadAjax(finalFind(0));
}

function finalFind(imp) {
    var retorno = "?imp=" + imp + "&tp=2&idsrc=" + $("#txtId").val();
    if ($('#txtBuscar').val() !== "") {
        retorno = retorno + "&Search=" + " ";
    }
    return "./../Financeiro/Lancamento-de-Movimentos_server_processing.php" + retorno;
}

function FecharBox() {
    $("#newbox").remove();
    refresh();
}

function excluir(id) {
    bootbox.confirm("Deseja realmente excluir?", function (e) {
        if (e === true) {
            $.ajax({
                url: "../CRM/ajax/ajaxContratos.php",
                dataType: "json",
                type: 'POST',
                data: {
                    idContrato: id
                },
                success: function (data) {
                    refresh();
                }
            });
        }
    });
}

function abrirContrato(id) {
    $.getJSON('./../CRM/contratos.ajax.php', {txtTipo: "0"}, function (itens) {
        let arrayOptions = [];
        arrayOptions.push({text: 'Selecione', value: 'null'});
        for (var i = 0; i < itens.length; i++) {
            arrayOptions.push({text: itens[i].descricao, value: itens[i].id});
        }
        bootbox.prompt({
            title: "Selecione um modelo e contrato:",
            inputType: 'select',
            inputOptions: arrayOptions,
            callback: function (result) {
                if (result !== 'null') {
                    window.open("../../util/ImpressaoPdf.php?id_plan=" + id + "&idContrato=" + result + "&model=S&tpImp=I&NomeArq=Contrato&PathArq=../Modulos/Comercial/modelos/ModeloContrato.php&emp=1", '_blank');
                } else {
                    bootbox.alert("É necessário selecionar um contrato válido!");
                }
            }
        });
    });
}
