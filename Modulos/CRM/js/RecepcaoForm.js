$("#txtData, #txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
$("#txtHora").mask("99:99");

function atendimento() {
    $("#block").toggle("slow");
    limpar();
}

function AbrirBox(id, cli, tp) {
    $.getJSON('form/Recepcao.ajax.php', {cod: id, ajax: 'true'}, function (j) {
        for (var i = 0; i < j.length; i++) {
            $('#txtId').val(j[i].id);
            $('#txtOperador').val(j[i].operador);
            $('#txtData').val(j[i].data);
            $('#txtHora').val(j[i].hora);
            $('#txtRelato').val(j[i].relato);
            $('#txtDepartamento').val(j[i].departamento);
            $('#txtPara').val(j[i].para);
            $('#txtIdNota').val(j[i].contato);
            listContatos(j[i].contato);
            $('#txtPenCon').val(j[i].tipo);
            $('#txtDesc').val(j[i].nomecontato);
            $('#txtNomeCon').val(j[i].nomecon);
            $('#txtObs').val(j[i].obs);
            $('#uniform-txtservicoA > span').attr("class", (j[i].id_servico === "0" ? "checked" : ""));
            $('#uniform-txtservicoO > span').attr("class", (j[i].id_servico === "1" ? "checked" : ""));
            $('#txtservicoA').attr("checked", (j[i].id_servico === "0" ? true : false));
            $('#txtservicoO').attr("checked", (j[i].id_servico === "1" ? true : false));
            $('#txtPendente').val((tp === 1 ? 0 : j[i].pendente));
            $('#txtEraPendente').val(j[i].pendente);
            if (j[i].pendenteX === 1) {
                $('#msg').show();
            } else {
                $('#msg').hide();
            }
        }
    });
    $("#block").show("slow");
}

function FecharBox() {
    $("#newbox").remove();
}

function limpar() {
    var d = new Date();
    $('#txtId').val("");
    $('#txtOperador').val($("#userLog").val());
    $('#txtRelato').val("");
    $('#txtDepartamento').val(null);
    $('#txtPara').val(null);
    $('#txtIdNota').val("");
    $('#txtPenCon').val("C");
    $('#txtDesc').val("");
    $('#txtNomeCon').val("");
    $('#txtObs').val("");
    $('#uniform-txtservicoA > span').attr("class", "");
    $('#uniform-txtservicoO > span').attr("class", "checked");
    $('#txtservicoA').attr("checked", false);
    $('#txtservicoO').attr("checked", true);
    $('#txtPendente').val("1");
    $('#txtEraPendente').val("1");
    $('#msg').hide();
    $("#divContatos").html("");
    $("#txtQtdContatos").val(0);
    $("#txtTextoContato").val("");
    $("#txtRespContato").val("");
}

$(document).ready(function () {
    $('#example').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": finalFind(0),
        'oLanguage': {
            'oPaginate': {
                'sFirst': "Primeiro",
                'sLast': "Último",
                'sNext': "Próximo",
                'sPrevious': "Anterior"
            }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
            'sLengthMenu': "Visualização de _MENU_ registros",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });

    $("#btnfind").click(function () {
        refresh();
    });

    $("#txtDesc").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "ajax.php",
                dataType: "json",
                data: {
                    q: request.term,
                    t: $("#txtPenCon").val()
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            $('#txtIdNota').val(ui.item.id).show();
            $('#txtDesc').val(ui.item.label).show();
            if (ui.item.id !== "") {
                $.getJSON('fornecedor_divida.ajax.php?search=', {txtParaCon: ui.item.id, ajax: 'true'}, function (j) {
                    for (var i = 0; i < j.length; i++) {
                        if (j[i].bloqueado > 0) {
                            $('#msg').html('Bloqueado').show();
                        } else {
                            if (j[i].total > 0) {
                                $('#msg').html('Pendência Financeira').show();
                            } else {
                                $('#msg').hide();
                            }
                        }
                    }
                });
                listContatos(ui.item.id);
            } else {
                $("#divContatos").html("");
                $('#msg').html('Pendência Financeira').hide();
            }
        }
    });

    $('#txtDepartamento').change(function () {
        if ($(this).val()) {
            $('#txtPara').hide();
            $('#carregando2').show();
            $.getJSON('departamento.ajax.php?search=', {txtDepartamento: $(this).val(), ajax: 'true'}, function (j) {
                var options = '<option value="null">Selecione</option>';
                for (var i = 0; i < j.length; i++) {
                    if (i === 0) {
                        $("#uniform-txtservicoO > span").attr("class", (j[i].tipo > 0 ? "checked" : ""));
                        $("#uniform-txtservicoA > span").attr("class", (j[i].tipo > 0 ? "" : "checked"));
                        $("#txtservicoO").attr("checked", (j[i].tipo > 0 ? true : false));
                        $("#txtservicoA").attr("checked", (j[i].tipo > 0 ? false : true));
                    }
                    options += '<option value="' + j[i].id_usuario + '">' + j[i].nome + '</option>';
                }
                $('#txtPara').html(options).show();
                $('#carregando2').hide();
            });
        } else {
            $('#txtPara').html('<option value="null">Selecione</option>');
        }
    });

    $('#txtPara').change(function () {
        if ($(this).val()) {
            $.getJSON('usuario.ajax.php?search=', {txtPara: $(this).val(), ajax: 'true'}, function (j) {
                for (var i = 0; i < j.length; i++) {
                    $("#txtDepartamento option[value='" + j[i].departamento + "']").prop("selected", true);
                    $("#uniform-txtservicoO > span").attr("class", (j[i].tipo > 0 ? "checked" : ""));
                    $("#uniform-txtservicoA > span").attr("class", (j[i].tipo > 0 ? "" : "checked"));
                    $("#txtservicoO").attr("checked", (j[i].tipo > 0 ? true : false));
                    $("#txtservicoA").attr("checked", (j[i].tipo > 0 ? false : true));
                }
            });
        } else {
            $("#txtDepartamento option[value='null']").prop("selected", true);
        }
    });

    $("#txtTextoContato").mask("(99) 99999-9999");
});

function refresh() {
    var example = $('#example').dataTable();
    example.fnReloadAjax(finalFind(0));
}

function finalFind(imp) {
    var retorno = "?zp=3&imp=" + imp;
    var perfil;
    if ($('#aca_0').css('display') === 'block') {
        perfil = 'acad';
    } else {
        perfil = 'erp';
    }
    if ($('#txtTipoBusca').val() !== "" && $('#txtTipoBusca').val() !== "null") {
        retorno = retorno + "&us=" + $('#txtTipoBusca').val();
    }
    if ($('#txtTipox').val() !== "" && $('#txtTipox').val() !== "null") {
        retorno = retorno + "&tpc=" + $('#txtTipox').val();
    }
    if ($('#txt_dt_begin').val() !== "") {
        retorno = retorno + "&dti=" + $('#txt_dt_begin').val().replace(/\//g, "_");
    }
    if ($('#txt_dt_end').val() !== "") {
        retorno = retorno + "&dtf=" + $('#txt_dt_end').val().replace(/\//g, "_");
    }
    return "./../../Modulos/Servicos/Atendimentos_server_processing.php" + retorno + "&perfil=" + perfil;
}

function imprimir() {
    var pRel = "&NomeArq=" + "Recepção" +
            "&lbl=" + "Data|Cliente|Contato|Tipo|Atendente|Encaminhado|Próx|Período" +
            "&siz=" + "80|130|80|80|70|80|70|110" +
            "&pdf=" + "5|9" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + "Recepção " + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + finalFind(1).replace("?", "&").replace("./../../", "../"); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
}

function listContatos(id) {
    $("#divContatos").html("");
    $.getJSON('../../Modulos/Comercial/FormGerenciador-Clientes.contatos.ajax.php?search=', {cod: id, ajax: 'true'}, function (j) {
        for (var i = 0; i < j.length; i++) {
            addLinhaContato(j[i].tipo_contato, j[i].texto_contatos, j[i].conteudo_contato, j[i].id, j[i].responsavel);
        }
    }).done(function () {
    });
}

function addLinhaContato(idTipo, tipo, conteudo, id, responsavel) {
    var idLinhaContato = eval($('#txtQtdContatos').val()) + 1;
    var btExcluir = '<div class="btnExcluir" style="width:5%; padding:4px 0 3px; float:right;cursor: pointer;" onClick="return removeLinha(\'tabela_linha_' + idLinhaContato + '\');"><span class="ico-remove" style="font-size:20px"></span></div>';
    var linha = "";
    var icoMail = "";
    if (idTipo === "2" && $("#txtId").val() !== "" && $("#txtReadOnly").val() === "0") {
        icoMail = "<span id=\"icoEmail\" title=\"Enviar Email\" class=\"ico-envelope-3\" style=\"font-size: 18px; margin-left: 10px;vertical-align: middle;cursor: pointer;\" onclick=\"abrirTelaEmail('" + conteudo + "');\"></span>";
    } else if (idTipo === "3" && $("#txtReadOnly").val() === "0") {
        icoMail = "<a id=\"icoEmail\" title=\"Abrir Site\" class=\"ico-globe-3\" style=\"margin-left: 25px !important;font-size: 18px;vertical-align: middle;cursor: pointer;\" href=\"http://" + conteudo + "\" target=\"window\"></a>";
    } else if (idTipo === "1" && $("#txtId").val() !== "" && $("#txtReadOnly").val() === "0") {
        icoMail = "<span id=\"icoSms\" title=\"Enviar SMS\" class=\"ico-comment-alt\" style=\"font-size: 19px; margin-left: 7px;vertical-align: middle;cursor: pointer;\" onclick=\"abrirTelaSMS('" + conteudo + "');\"></span>";
    }
    linha = '<div id="tabela_linha_' + idLinhaContato + '" style="padding:5px; border-bottom:1px solid #DDD;height: 25px;overflow: hidden;">\n\
    <input id="txtIdContato_' + idLinhaContato + '" name="txtIdContato_' + idLinhaContato + '" value="' + id + '" type="hidden" />\n\
    <input id="txtTpContato_' + idLinhaContato + '" name="txtTpContato_' + idLinhaContato + '" value="' + idTipo + '" type="hidden" />\n\
    <div style="width:22%; padding-left:1%; line-height:27px; float:left">' + tipo + icoMail + '</div>\n\
    <input id="txtTxContato_' + idLinhaContato + '" name="txtTxContato_' + idLinhaContato + '" value="' + conteudo + '" type="hidden" />\n\
    ' + btExcluir + '<div style="width:48%; line-height:27px; float:left">' + conteudo + '</div> \n\
    <input id="txtTxResponsavel_' + idLinhaContato + '" name="txtTxResponsavel_' + idLinhaContato + '" value="' + responsavel + '" type="hidden" />\n\
    <div style="width:22%; line-height:27px; float:left">' + responsavel + '</div></div>';
    $("#divContatos").prepend(linha);
    $("#txtQtdContatos").val(idLinhaContato);
    $("#txtTextoContato").val("");
    $("#txtRespContato").val("");
}

function alterarMask() {
    $('#txtTextoContato').val("");
    var tipo = $("#txtTipoContato option:selected").val();
    $("#txtTextoContato").unmask();
    if (tipo === "0") {
        $("#txtTextoContato").mask("(99) 9999-9999");
    } else if (tipo === "1") {
        $("#txtTextoContato").mask("(99) 99999-9999");
    }
}

function addContato() {
    var msg = $("#txtTextoContato").val();
    var sel = $("#txtTipoContato").val();
    if (msg !== "") {
        if (sel !== "2" || (sel === "2" && validateEmail($("#txtTextoContato").val()))) {
            addLinhaContato($("#txtTipoContato").val(), $("#txtTipoContato option:selected").text(), $("#txtTextoContato").val(), "0", $("#txtRespContato").val());
        } else {
            bootbox.alert("Preencha um endereço de e-mail corretamente!");
        }
    } else {
        bootbox.alert("Preencha as informações de contato corretamente!");
    }
}

function removeLinha(id) {
    $("#" + id).remove();
    var idLinhaContato = eval($('#txtQtdContatos').val()) - 1;
    $("#txtQtdContatos").val(idLinhaContato);
}

function validar() {
    if ($.isNumeric($("#txtIdNota").val())) {
        return true;
    } else {
        if ($("#txtPenCon").val() === "P" && $("#txtDesc").val() !== "") {
            return true;
        } else {
            bootbox.alert('Selecione um ' + $('#txtPenCon').find('option:selected').text());
            return false;
        }
    }
}

function salvar() {
    if (validar()) {
        $.post("form/Recepcao.ajax.php", "bntSave=S&" + $("#frmRecepcao").serialize()).done(function (data) {
            if ($.isNumeric(data.trim())) {
                $('#txtId').val(data.trim());
                refresh();
                bootbox.alert("Operação efetuada com sucesso!");
            } else {
                bootbox.alert("Não é possível efetuar estar operação!");
            }
        });
    }
}

function RemoverChamado(id) {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("form/Recepcao.ajax.php?Del=" + id).done(function (data) {
                if (data.trim() === "YES") {
                    refresh();
                    if (id === parseInt($('#txtId').val())) {
                        limpar();
                    }
                } else {
                    bootbox.alert("Não é possível excluir este item!");
                }
            });
        }
    });
    return;
}

function abrirTelaEmail() {
}

function abrirTelaSMS() {
}

function abrirTelaWha() {
}
