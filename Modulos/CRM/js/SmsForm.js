$(document).ready(function () {    
    $("#txtSms").val(parent.listaSMS());
    
    $('#txtModelo').change(function () {
        $("#ckeditor").val("");
        $('textarea').prop('readonly', false);
        if ($(this).val() && $(this).val() !== "null") {
            $.getJSON("form/EmailsEnvioFormServer.php", {getEmail: $(this).val()}, function (j) { }).always(function (j) {
                if (j !== null) {
                    $("#ckeditor").val(j[0].mensagem);
                    $('textarea').prop('readonly', true);                    
                    getCaracteres();
                }
            });
        }
        getCaracteres();
    });        
    
    if ($("#txtSms").val() === "") {
        bootbox.alert("Não foram encontrados telefones para envio de mensagem!", function () {
            parent.FecharBox(1);
        });
    }
    if ($("#txtType").val() === "2") {  //2-WHATZAP ANTIGO
        var contato = $("#txtSms").val().split("|")[0];
        contato = contato.replace(/[()-]/g, '');
        contato = "55" + contato.replace(" ", "");
        $("#btnEnviar").click(function (e) {
            window.open("https://api.whatsapp.com/send?phone=" + contato + "&text=" + $("#ckeditor").val().replace(/\s/g, '+'), '_blank');
            parent.FecharBox(1);
        });
    } else { //0-SMS, 1-WHATZAP
        var emailTotal = $("#txtSms").val().split(",");
        $("#pgFim").text(emailTotal.length);        
        $("#btnEnviar").click(function (e) {            
            bootbox.confirm('Confirma o envio destas mensagens?', function (result) {
                if (result === true) {
                    $.post("./../../util/sms/SmsSaldoForm.php", "totTrans=" + emailTotal.length + "&tipo=" + $("#txtType").val()).done(function (data) {
                        if (data > 0) {
                            $("#loader", window.parent.document).show();
                            sendSMSrow(emailTotal, 0, 0);
                        } else {
                            $("#loader", window.parent.document).hide();
                            bootbox.alert("Saldo insuficiente!");
                        }
                    });
                } else {
                    return;
                }
            });
        });
    }
    getCaracteres();
    setFirstOption();
});

function sendSMSrow(lista, sucesso, erro) {
    let PgIni = parseInt($("#pgIni").html());
    let PgFim = parseInt($("#pgFim").html());
    if ((PgIni < PgFim) && PgFim > 0 && $("#ckeditor").is(":visible")) {
        $("#pgIni").html(PgIni + 1);        
        $.ajax({
            type: "POST", url: "./../../util/sms/send_sms.php",
            data: {btnEnviar: 'S',
                txtTipo: $("#txtType").val(),
                txtSendSms: lista[PgIni], 
                ckeditor: $("#ckeditor").val()
            }, dataType: "json",
            success: function (data) {
                let s = sucesso + parseInt(data[0].qtd_sucesso);
                let e = erro + parseInt(data[0].qtd_erro);
                sendSMSrow(lista, s, e);
            },
            error: function (error) {
                $("#loader", window.parent.document).hide();
                bootbox.alert(error.responseText + "!");
            }
        });                        
    } else {
        $("#loader", window.parent.document).hide();
        bootbox.alert(($("#txtType").val() === "0" ? "Foram enviado(s)" + (sucesso + erro) + " sms(s):<br><br><b>* Sucesso(s):" + sucesso + "</b><br><b style='color:red'>* Falha(s):" + erro + "</b>" : 
        "Foram enviado(s)" + (sucesso + erro) + " mensagens para fila de envio!"), function () {
            parent.FecharBox(1);
        });
    }
}

$(document).on("input", "#ckeditor", function () {
    getCaracteres();
});

function getCaracteres() {
    let caracteresMax = parseInt($("#ckeditor").attr("maxlength"));
    let caracteresDigitados = parseInt($("#ckeditor").val().length);
    let caracteresRestantes = caracteresMax - caracteresDigitados;
    $(this).attr('maxlength', caracteresMax);
    $(".caracteres").text(caracteresRestantes);    
}

function setFirstOption() {
    if ($("#txtModelo option").length > 1) {
        $('#txtModelo').val($("#txtModelo option")[1].value).trigger('change');
    }
}