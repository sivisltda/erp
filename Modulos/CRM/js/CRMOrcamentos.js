$(document).ready(function () {
    if ($.isNumeric($('#txtId').val())) {
        $('#tbOrcamentos').dataTable({
            "iDisplayLength": 20,
            "aLengthMenu": [20, 30, 40, 50, 100],
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": URLOrcamentos(),
            'oLanguage': {
                'oPaginate': {
                    'sFirst': "Primeiro",
                    'sLast': "Último",
                    'sNext': "Próximo",
                    'sPrevious': "Anterior"
                }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                'sLengthMenu': "Visualização de _MENU_ registros",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sSearch': "Pesquisar:",
                'sZeroRecords': "Não foi encontrado nenhum resultado"},
            'fnInitComplete': function (oSettings, json) {
                refreshTotal();
            },
            "sPaginationType": "full_numbers",
            "bSort": false,
            "columnDefs": [{"visible": false, "targets": [0, 5, 6, 7]}]
        });
    }
});

function refreshOrc() {
    if ($.isNumeric($('#txtId').val())) {
        var tblCRM = $('#tbOrcamentos').dataTable();
        tblCRM.fnReloadAjax(URLOrcamentos());
    }
}

function URLOrcamentos() {
    var finalURL = '?imp=0';
    if ($("#txtEstOrcamento").val() !== "") {
        finalURL = finalURL + '&id=' + $("#txtEstOrcamento").val();
    }
    if ($("#txtTipoBusca").val() !== "") {
        finalURL = finalURL + '&tp=' + $("#txtTipoBusca").val();
    }
    if ($("#txtId").val() !== "") {
        finalURL = finalURL + '&Cli=' + $("#txtId").val();
    }
    if ($("#txtUsuario").val() !== "") {
        finalURL = finalURL + '&td=' + $("#txtUsuario").val();
    }
    if ($("#txtFilial").val() !== "") {
        finalURL = finalURL + '&filial=' + $("#txtFilial").val();
    }
    if ($('#txt_dt_begin').val() !== "") {
        finalURL = finalURL + "&dti=" + $('#txt_dt_begin').val().replace(/\//g, "_");
    }
    if ($('#txt_dt_end').val() !== "") {
        finalURL = finalURL + "&dtf=" + $('#txt_dt_end').val().replace(/\//g, "_");
    }
    return "../../Modulos/Estoque/Orcamentos_server_processing.php" + finalURL;
}

function refreshTotal() {
    if ($('#qtdparc').val() !== undefined) {
        $('#lblTotParc2').html('Número de Orçamentos :   <strong>' + $('#qtdparc').val() + '</strong>').show('slow');
        $('#lblTotParc3').html('Total em Orçamentos :<strong>' + numberFormat($('#totparc').val(), 1) + '</strong>').show('slow');
        $('#lblTotParc4').html('Ticket Médio :<strong>' + numberFormat(($('#totparc').val() / $('#qtdparc').val()), 1) + '</strong>').show('slow');
    }
} 