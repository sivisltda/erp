$(document).ready(function () {
    $("#txtCepEnt").mask("99999-999");
    $("#txtTelefoneEnt").mask("(99) 9999-9999");
    limparEnd();
    if ($.isNumeric($('#txtId').val())) {
        $("#enderecos").dataTable({
            "iDisplayLength": 20,
            "aLengthMenu": [20, 30, 40, 50, 100],
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "../CRM/ajax/Enderecos_server_processing.php?id=" + $('#txtId').val(),
            'oLanguage': {
                'oPaginate': {
                    'sFirst': "Primeiro",
                    'sLast': "Último",
                    'sNext': "Próximo",
                    'sPrevious': "Anterior"
                }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                'sLengthMenu': "Visualização de _MENU_ registros",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sSearch': "Pesquisar:",
                'sZeroRecords': "Não foi encontrado nenhum resultado"
            },
            "sPaginationType": "full_numbers"
        });
    }
});

$('#txtEstadoEnt').change(function () {
    if ($("#txtEstadoEnt").val() !== "") {
        carregaCidadeEnt($("#txtEstadoEnt").val(), "");
        $("#txtCidadeEnt").select2("val", "");
    } else {
        $('#txtCidadeEnt').html('<option value="">Selecione a cidade</option>');
    }
});

$("#txtCepEnt").change(function () {
    if ($("#txtCepEnt").val() !== "") {
        $("#loader").show();
        $.get("https://viacep.com.br/ws/" + $(this).val() + "/json/",
                function (data) {
                    var str = jQuery.parseJSON(data);
                    $("#txtEnderecoEnt, #txtBairroEnt").attr("readonly", true);
                    $('#txtCidadeEnt, #txtEstadoEnt').select2('disable');
                    if(str.logradouro === undefined) {                        
                        $("#txtCepEnt").val("");
                        bootbox.alert("CEP inválido!");
                    } else if (str.logradouro !== "") {                    
                        var estado = $("#txtEstadoEnt option[uf='" + str.uf.toUpperCase() + "']").val();                        
                        $("#txtEnderecoEnt").val(decodeURIComponent(escape(str.logradouro)));
                        $("#txtBairroEnt").val(decodeURIComponent(escape(str.bairro)));
                        $("#txtCidadeEnt").val(decodeURIComponent(escape(str.localidade)));
                        $("#txtEstadoEnt").select2("val", estado);
                        $("#txtNumeroEnt").focus();
                        carregaCidade(estado, str.localidade);
                    } else if (str.logradouro === "") {
                        $("#txtEnderecoEnt, #txtBairroEnt, #txtCepEnt").val("");
                        $("#txtCidadeEnt, #txtEstadoEnt").select2("val", "");
                        $("#txtEnderecoEnt, #txtBairroEnt").attr("readonly", false);
                        $('#txtCidadeEnt, #txtEstadoEnt').select2('enable');
                        $("#txtCepEnt").focus();
                    }
                }).always(function () {
            $("#loader").hide();
        });
    } else {
        $("#txtEnderecoEnt, #txtBairroEnt").val("");
        $("#txtCidadeEnt, #txtEstadoEnt").select2("val", "");
        $("#txtEnderecoEnt, #txtBairroEnt").attr("readonly", false);
        $('#txtCidadeEnt, #txtEstadoEnt').select2('enable');
    }
});

function carregaCidadeEnt(estado, cidade) {
    $("#s2id_txtCidadeEnt").hide();
    $("#txtCidadeEnt").hide();
    var idCidade = "";
    $.getJSON("../../Modulos/Comercial/locais.ajax.php?search=", {txtEstado: estado, ajax: "true"}, function (j) {
        var options = "<option value=\"\">Selecione a Cidade</option>";
        if (j.length > 0) {
            for (var i = 0; i < j.length; i++) {
                if (cidade === j[i].cidade_nome) {
                    idCidade = j[i].cidade_codigo;
                }
                options = options + "<option value=\"" + j[i].cidade_codigo + "\">" + j[i].cidade_nome + "</option>";
            }
            $("#txtCidadeEnt").html(options);
        }
    }).always(function () {
        $("#txtCidadeEnt").select2("val", idCidade);
        $("#s2id_txtCidadeEnt").show();
    });
}

function validaFormEntrega() {
    if ($('#txtNomeEnt').val() === "") {
        bootbox.alert("O Campo Nome Endereço é Obrigatório!");
        return false;
    }
    if ($('#txtEnderecoEnt').val() === "") {
        bootbox.alert("O Campo Logradouro é Obrigatório!");
        return false;
    }
    return true;
}

function addEnderecoEntrega() {
    if (validaFormEntrega()) {
        $.post("../../Modulos/CRM/form/CRMEnderecoEntrega.php", {txtId: $('#txtId').val(), txtIdEnt: $('#txtIdEnt').val(),
            txtNomeEnt: $('#txtNomeEnt').val(), txtReferenciaEnt: $('#txtReferenciaEnt').val(), txtTelefoneEnt: $('#txtTelefoneEnt').val(),
            txtCepEnt: $('#txtCepEnt').val(), txtEnderecoEnt: $('#txtEnderecoEnt').val(), txtNumeroEnt: $('#txtNumeroEnt').val(), txtBairroEnt: $('#txtBairroEnt').val(),
            txtEstadoEnt: $('#txtEstadoEnt').val(), txtCidadeEnt: $('#txtCidadeEnt').val(), txtComplementoEnt: $('#txtComplementoEnt').val(), bntSaveEnd: "S"}).done(function (data) {
            if (data === "YES") {
                refreshEnderecoEntrega();
            } else {
                alert(data);
            }
        });
    }
}

function refreshEnderecoEntrega() {
    if ($.isNumeric($('#txtId').val())) {
        var tblEnd = $('#enderecos').dataTable();
        tblEnd.fnReloadAjax("../CRM/ajax/Enderecos_server_processing.php?id=" + $('#txtId').val());
        limparEnd();
    }
}

function limparEnd() {
    $('#txtIdEnt').val("");
    $('#txtNomeEnt').val("");
    $('#txtReferenciaEnt').val("");
    $('#txtTelefoneEnt').val("");
    $('#txtCepEnt').val("");
    $('#txtEnderecoEnt').val("");
    $('#txtNumeroEnt').val("");
    $('#txtBairroEnt').val("");
    $('#txtEstadoEnt').val("null");
    $('#txtCidadeEnt').val("null");
    $('#txtComplementoEnt').val("");
}

function editEndereco(numero) {
    $.getJSON("../../Modulos/CRM/form/CRMEnderecoEntrega.php", {txtIdx: numero, bntFind: "S"}, function (j) {
        for (var i = 0; i < j.length; i++) {
            $('#txtIdEnt').val(numero);
            $('#txtNomeEnt').val(j[i].nome);
            $('#txtReferenciaEnt').val(j[i].referencia);
            $('#txtTelefoneEnt').val(j[i].telefone);
            $('#txtCepEnt').val(j[i].cep);
            $('#txtEnderecoEnt').val(j[i].endereco);
            $('#txtNumeroEnt').val(j[i].numero);
            $('#txtBairroEnt').val(j[i].bairro);
            $('#txtComplementoEnt').val(j[i].complemento);
            if (j[i].estado !== "") {
                $("#txtEstadoEnt").select2("val", j[i].estado);
            } else {
                $("#txtEstadoEnt").select2("val", "null");
            }
            cid_cod = j[i].cidade;
            cid_nome = j[i].cidade_nome;
            $("#txtCidadeEnt").select2("container").hide();
            $.getJSON("locais.ajax.php?search=", {txtEstado: $("#txtEstadoEnt").val(), ajax: "true"}, function (j) {
                var options = "<option value=\"null\"></option>";
                for (var i = 0; i < j.length; i++) {
                    options += "<option value=\"" + j[i].cidade_codigo + "\">" + j[i].cidade_nome + "</option>";
                }
                $("#txtCidadeEnt").html(options);
                $("#txtCidadeEnt").select2("container").show();
            }).done(function () {
                if (cid_cod !== "") {
                    $("#txtCidadeEnt").select2("val", cid_cod);
                } else {
                    $("#txtCidadeEnt").select2("val", "null");
                }
            });
        }
    });
}

function delEndereco(numero) {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/CRM/form/CRMEnderecoEntrega.php", "bntDelEnd=S&txtIdx=" + numero).done(function (data) {
                if (data === "YES") {
                    refreshEnderecoEntrega();
                } else {
                    alert(data);
                }
            });
        } else {
            return;
        }
    });
}
