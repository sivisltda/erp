<?php
include '../../Connections/configini.php';
$imprimir = 0;
$tipo2 = '1';
$tipoData = '0';
$contato = '';
$finalUrlServer = '';
$contatoName = '';
$tipoChamado = '';

if (is_numeric($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (is_numeric($_GET['us'])) {
    $contato = $_GET['us'];
    $finalUrlServer .= "&us=" . $_GET['us'];
    $cur = odbc_exec($con, "select id_usuario,login_user,nome from sf_usuarios where inativo = 0 and (id_usuario = " . $_GET['us'] . ") order by nome") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $contatoName = $RFP['nome'];
    }
} else {
    $cur = odbc_exec($con, "select id_usuario,login_user,nome from sf_usuarios where inativo = 0 and (id_usuario = " . $_SESSION["id_usuario"] . ") order by nome") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $contatoName = $RFP['nome'];
    }
}

if (is_numeric($_GET['id'])) {
    $finalUrlServer .= "&id=" . $_GET['id'];
    $cur = odbc_exec($con, "select id_fornecedores_despesas,tipo,razao_social,nome_fantasia from dbo.sf_fornecedores_despesas where id_fornecedores_despesas = " . $_GET['id']) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $tipo = $RFP['tipo'];
        $contatoName = $RFP['razao_social'];
        if ($RFP['razao_social'] != "") {
            $contatoName = $contatoName . " (" . $RFP['nome_fantasia'] . ")";
        }
    }
}
if (is_numeric($_GET['tpc'])) {
    $tipo2 = $_GET['tpc'];
    $finalUrlServer .= "&tpc=" . $_GET['tpc'];
}
if (is_numeric($_GET['frm'])) {
    $tipoChamado = $_GET['frm'];
    $finalUrlServer .= "&frm=" . $_GET['frm'];
}
if (is_numeric($_GET['tpd'])) {
    $tipoData = $_GET['tpd'];
    $finalUrlServer .= "&tpd=" . $_GET['tpd'];
}
if ($_GET['dti'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
    $finalUrlServer .= "&dti=" . $_GET['dti'];
}
if ($_GET['dtf'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
    $finalUrlServer .= "&dtf=" . $_GET['dtf'];
} else {
    $DateEnd = getData("T");
}

if (is_numeric($_GET['Del'])) {
    odbc_exec($con, "DELETE FROM sf_mensagens_telemarketing WHERE id_tele = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_telemarketing_item WHERE id_telemarketing = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_telemarketing WHERE id_tele = " . $_GET['Del']);
    echo "<script>window.top.location.href = 'Telemarketing.php?us=" . $_GET['us'] . "'; </script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>        
        <style>
            .fancybox-custom .fancybox-skin {
                box-shadow: 0 0 50px #069;
            }
            .fancybox-wrap, .fancybox-skin, .fancybox-outer, .fancybox-inner, .fancybox-image,  .fancybox-wrap object, .fancybox-nav, .fancybox-nav span, .fancybox-tmp {
                padding: 0;
                margin: 0;
                border: 0;
                outline: none;
                vertical-align: top;
                background:url(../../img/fundosForms/formBancos.PNG);
            }            
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>CRM<small>Contatos</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="boxfilter">
                            <div style="margin-top: 12px;float:left; margin-bottom: 3px;">
                                <button name="btnPrint" class="button button-blue btn-primary" onClick="imprimir()" type="button" title="Imprimir"><span class="ico-print"></span></button>
                            </div>
                            <div>   
                                <div style="width:10%; float:left;margin-left:0.5%;">
                                    <span>Tipo Pessoa:</span>                                    
                                    <select class="select" name="txtPenCon" id="txtPenCon" style="width: 100%;">
                                        <option value="U" <?php
                                        if ($tipo == 'U') {
                                            echo "SELECTED";
                                        }
                                        ?>>Atendente</option>
                                        <option value="C" <?php
                                        if ($tipo == 'C') {
                                            echo "SELECTED";
                                        }
                                        ?>>Cliente</option>
                                        <option value="E" <?php
                                        if ($tipo == 'E') {
                                            echo "SELECTED";
                                        }
                                        ?>>Funcionario</option>
                                        <option value="F" <?php
                                        if ($tipo == 'F') {
                                            echo "SELECTED";
                                        }
                                        ?>>Fornecedor</option>
                                        <option value="P" <?php
                                        if ($tipo == 'P') {
                                            echo "SELECTED";
                                        }
                                        ?>>Prospect</option>
                                    </select> 
                                </div>
                                <div style="width:11%; float:left;margin-left:0.5%">
                                    <span>Buscar:</span>                                    
                                    <input id="txtValue" name="txtValue" type="hidden" value="<?php echo $contato; ?>"/>
                                    <input type="text" name="txtDesc" id="txtDesc" class="inputbox" value="<?php echo $contatoName; ?>" size="10" style="height: 28px;vertical-align:top;width:150px; color:#000 !important"/>
                                </div>
                                <div style="width:11%; float:left; margin-left:0.5%">
                                    <span>Grupo de Clientes:</span>
                                    <select name="txtGrupoRen[]" id="txtGrupoRen" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                        <?php $cur = odbc_exec($con, "select id_grupo,descricao_grupo from dbo.sf_grupo_cliente where tipo_grupo = 'C' and inativo_grupo = 0 order by descricao_grupo");
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo utf8_encode($RFP["id_grupo"]); ?>"><?php echo utf8_encode($RFP["descricao_grupo"]); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>                                
                                <div style="width:7%; float:left;margin-left:0.5%; margin-bottom: 5px;">
                                    <span>UF:</span>                                                                        
                                    <select id="txtEstado" class="select" style="width: 100%;">
                                        <option value="null">UF</option>
                                        <?php $cur = odbc_exec($con, "select estado_codigo,estado_sigla from tb_estados where estado_codigo > 0 order by estado_nome") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['estado_codigo'] ?>"><?php echo utf8_encode($RFP['estado_sigla']) ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div style="width:9%; float:left;margin-left:0.5%">
                                    <span>Status:</span>                                                                        
                                    <select class="select" style="width: 100%;" name="txtTipo" id="txtTipo">
                                        <option value="null">Todos</option>
                                        <option value="0" <?php
                                        if ($tipo2 == '0') {
                                            echo "SELECTED";
                                        }
                                        ?>>Fechados</option>
                                        <option value="1" <?php
                                        if ($tipo2 == '1') {
                                            echo "SELECTED";
                                        }
                                        ?>>Abertos</option>
                                    </select>
                                </div>
                                <div style="width:9%; float:left;margin-left:0.5%">
                                    <span>Tipo Contato:</span>                                                                        
                                    <select id="txtTipoChamado" class="select" style="width: 100%;">
                                        <option value="null">Todos</option>
                                        <option value="T" <?php
                                        if ($tipoChamado == "T") {
                                            echo "SELECTED";
                                        }
                                        ?>>Telefone</option>
                                        <option value="E" <?php
                                        if ($tipoChamado == "E") {
                                            echo "SELECTED";
                                        }
                                        ?>>Email</option>
                                        <option value="V" <?php
                                        if ($tipoChamado == "V") {
                                            echo "SELECTED";
                                        }
                                        ?>>Visita</option>
                                        <option value="S" <?php
                                        if ($tipoChamado == "S") {
                                            echo "SELECTED";
                                        }
                                        ?>>SMS</option>
                                        <option value="P" <?php
                                        if ($tipoChamado == "P") {
                                            echo "SELECTED";
                                        }
                                        ?>>Presencial</option>
                                        <option value="O" <?php
                                        if ($tipoChamado == "O") {
                                            echo "SELECTED";
                                        }
                                        ?>>Outro</option>
                                        <option value="W" <?php
                                        if ($tipoChamado == "W") {
                                            echo "SELECTED";
                                        }
                                        ?>>Whatsapp</option>                                        
                                        <option value="R" <?php
                                        if ($tipoChamado == "R") {
                                            echo "SELECTED";
                                        }
                                        ?>>Portal</option>                                        
                                    </select>
                                </div>
                                <div style="width:11%; float:left;margin-left:0.5%">
                                    <span>Data Tipo:</span>                                                                        
                                    <select id="txtTipoData" class="select" style="width: 100%;">
                                        <option value="0" <?php
                                        if ($tipoData == "0") {
                                            echo "SELECTED";
                                        }
                                        ?>>Dt.Chamado</option>
                                        <option value="1" <?php
                                        if ($tipoData == "1") {
                                            echo "SELECTED";
                                        }
                                        ?>>Dt.Prazo</option>
                                    </select>
                                </div>
                                <div style="width:6%; float:left;margin-left:0.5%">
                                    <span>Data início:</span>                                                                        
                                    <input type="text" class="datepicker" style="height: 28px;width:79px; margin-bottom:2px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial">
                                </div>
                                <div style="width:6%; float:left;margin-left:0.5%">
                                    <span>Data até:</span>                                                                        
                                    <input type="text" class="datepicker" style="height: 28px;width:79px; margin-bottom:2px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final">
                                </div>
                                <div style="width:6%; float:left; margin-top: 12px; margin-left:0.5%">
                                    <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" onclick="refreshFind();" title="Buscar"><span class="ico-search icon-white"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead" <?php echo $visible; ?>>
                        <div class="boxtext">Contatos</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="example">
                            <thead>
                                <tr id="formPQ">
                                    <th width="10%"><center>Data</center></th>
                                    <th width="25%">Cliente</th>
                                    <th width="10%">Origem</th>
                                    <th width="8%">Tipo</th>
                                    <th width="10%">Atendente</th>
                                    <th width="5%"><center>Pendente</center></th>
                                    <th width="10%">Encaminhado</th>
                                    <th width="10%"><center>Próx. Cont.</center></th>
                                    <th width="15%"><center>Periodo Cont.</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>                
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>      
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">

            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);

            $(document).ready(function () {

                $("#txtPenCon").change(function (event) {
                    $("#txtDesc").val("");
                });

                $("#txtDesc").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "./../CRM/ajax.php",
                            dataType: "json",
                            data: {
                                q: request.term,
                                t: $("#txtPenCon").val()
                            },
                            success: function (data) {
                                response(data);
                            }
                        });
                    },
                    minLength: 3,
                    select: function (event, ui) {
                        $("#txtValue").val(ui.item.id);
                    }
                });

                $('#example').dataTable({
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": finalFind(),
                    'oLanguage': {
                        'oPaginate': {
                            'sFirst': "Primeiro",
                            'sLast': "Último",
                            'sNext': "Próximo",
                            'sPrevious': "Anterior"
                        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                        'sLengthMenu': "Visualização de _MENU_ registros",
                        'sLoadingRecords': "Carregando...",
                        'sProcessing': "Processando...",
                        'sSearch': "Pesquisar:",
                        'sZeroRecords': "Não foi encontrado nenhum resultado"},
                    "sPaginationType": "full_numbers"
                });

                $('#txtTpGrupos').change(function () {
                    if ($(this).val() === "0") {
                        $("#s2id_mscGrupo, #txtProvisao").hide();
                    } else {
                        $("#s2id_mscGrupo, #txtProvisao").show();
                        $.getJSON("../BI/grupos.ajax.php", {txtTipo: $(this).val(), ajax: "true"}, function (j) {
                            var options = "";
                            for (var i = 0; i < j.length; i++) {
                                options += "<option value='" + j[i].conta_produto + "'>" + j[i].descricao + "</option>";
                            }
                            $("#mscGrupo").append(options);
                            $('#mscGrupo').trigger('change');
                        });

                    }
                });
            });

            function refreshFind() {
                var oTable = $('#example').dataTable({
                    "bServerSide": true,
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "sAjaxSource": finalFind(),
                    'oLanguage': {
                        'oPaginate': {
                            'sFirst': "Primeiro",
                            'sLast': "Último",
                            'sNext': "Próximo",
                            'sPrevious': "Anterior"
                        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                        'sLengthMenu': "Visualização de _MENU_ registros",
                        'sLoadingRecords': "Carregando...",
                        'sProcessing': "Processando...",
                        'sSearch': "Pesquisar:",
                        'sZeroRecords': "Não foi encontrado nenhum resultado"},
                    "sPaginationType": "full_numbers",
                    "bDestroy": true
                });
            }
            function finalFind() {
                var perfil;
                if ($('#aca_0').css('display') === 'block') {
                    perfil = 'acad';
                } else {
                    perfil = 'erp';
                }
                var retorno = "";
                if ($('#txtDesc').val() !== "" && $('#txtValue').val() !== "") {
                    if ($("#txtPenCon").val() === "U") {
                        retorno = retorno + "&us=" + $('#txtValue').val();
                    } else {
                        retorno = retorno + "&id=" + $('#txtValue').val();
                    }
                }
                if ($('#txtTipo').val() !== "null") {
                    retorno = retorno + "&tpc=" + $('#txtTipo').val();
                }
                if ($('#txtEstado').val() !== "") {
                    retorno = retorno + "&uf=" + $('#txtEstado').val();
                }
                if ($('#txtGrupoRen').val() !== null) {
                    retorno = retorno + "&grp=" + $('#txtGrupoRen').val();
                }
                if ($('#txtTipoChamado').val() !== "null") {
                    retorno = retorno + "&frm=" + $('#txtTipoChamado').val();
                }
                if ($('#txtTipoData').val() !== "null") {
                    retorno = retorno + "&tpd=" + $('#txtTipoData').val();
                }
                if ($('#txt_dt_begin').val() !== "") {
                    retorno = retorno + "&dti=" + $('#txt_dt_begin').val();
                }
                if ($('#txt_dt_end').val() !== "") {
                    retorno = retorno + "&dtf=" + $('#txt_dt_end').val();
                }
                return "./../Servicos/Atendimentos_server_processing.php?zp=2&tp=0&perfil=" + perfil + "&imp=0" + retorno.replace(/\//g, "_") + "&ord=<?php echo $_POST['ordem']; ?>";
            }

            function AbrirBox(id, gb, op) {
                if (op === 0) {
                    $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:667px; height:500px; position:absolute; top:50%; left:50%; margin-left:-333px; margin-top:-250px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:500px' src='./../CRM/FormTelemarketing.php?id=" + id + "&gb=" + gb + "&jn=o'></iframe></div></div></div></div></div>");
                } else if (op === 7) {
                    var retorno = "";
                    if ($('#txtDesc').val() !== "" && $('#txtValue').val() !== "") {
                        if ($("#txtPenCon").val() === "U") {
                            retorno = retorno + "&us=" + $('#txtValue').val();
                        } else {
                            retorno = retorno + "&id=" + $('#txtValue').val();
                        }
                    }
                    if ($('#txtTipo').val() !== "") {
                        retorno = retorno + "&tpc=" + $('#txtTipo').val();
                    }
                    if ($('#txtEstado').val() !== "") {
                        retorno = retorno + "&uf=" + $('#txtEstado').val();
                    }
                    if ($('#txtTipoChamado').val() !== "null") {
                        retorno = retorno + "&frm=" + $('#txtTipoChamado').val();
                    }
                    if ($('#txtTipoData').val() !== "null") {
                        retorno = retorno + "&tpd=" + $('#txtTipoData').val();
                    }
                    if ($('#txt_dt_begin').val() !== "") {
                        retorno = retorno + "&dti=" + $('#txt_dt_begin').val();
                    }
                    if ($('#txt_dt_end').val() !== "") {
                        retorno = retorno + "&dtf=" + $('#txt_dt_end').val();
                    }
                    window.location = 'Telemarketing.php?imp=1' + retorno.replace(/\//g, "_");
                } else {
                    $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:667px; height:500px; position:absolute; top:50%; left:50%; margin-left:-333px; margin-top:-250px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:500px' src='./../CRM/FormTelemarketing.php?id=" + id + "&gb=" + gb + "&jn=o&fx=s'></iframe></div></div></div></div></div>");
                }
            }

            function FecharBox() {
                $("#newbox").remove();
                var oTable = $('#example').dataTable();
                oTable.fnDraw(false);
            }

            function imprimir() {
                var pRel = "&NomeArq=" + "Contatos" +
                        "&lbl=" + "Data|Cliente|Contato|Tipo|Atendente|Encaminhado|Próx.Con.|Per.Con." +
                        "&siz=" + "70|190|80|60|70|70|70|90" +
                        "&pdf=" + "5|9" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + "Contatos " + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + finalFind().replace("?", "&").replace("./../", "../Modulos/"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }
        </script>
        <?php odbc_close($con); ?>
    </body>
</html>
