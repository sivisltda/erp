<?php
include "../../Connections/configini.php";
$imprimir = 0;
$tipox = '';
$linhaContato = "1";
$usuario = $_SESSION["id_usuario"];
$DateBegin = getData("T");
$DateEnd = getData("T");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>        
        <style>
            .fancybox-custom .fancybox-skin {
                box-shadow: 0 0 50px #069;
            }
            .fancybox-wrap, .fancybox-skin, .fancybox-outer, .fancybox-inner, .fancybox-image,  .fancybox-wrap object, .fancybox-nav, .fancybox-nav span, .fancybox-tmp {
                padding: 0;
                margin: 0;
                border: 0;
                outline: none;
                vertical-align: top;
                background:url(../../img/fundosForms/formBancos.PNG);
            }            
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">      
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>CRM<small>Recepção</small></h1>                    
                    </div>
                    <div class="row-fluid">
                        <div class="boxfilter">                                
                            <div style="margin-top: 11px; float:left;">
                                <button class="button button-green btn-primary" type="button" onClick="atendimento()"><span class=" ico-file-4 icon-white"></span></button>
                                <button name="btnPrint" class="button button-blue btn-primary" onClick="imprimir()" type="button" title="Imprimir"><span class="ico-print"></span></button>                                         
                            </div>
                            <div style="float: right;">
                                <input id="userLog" type="hidden" value="<?php echo $_SESSION["login_usuario"]; ?>"/>
                                <div style="width:30%; float:left;margin-left:0.5%">
                                    <span>Usuário:</span>
                                    <select style="width:100%" class="select" name="txtTipoBusca" id="txtTipoBusca" disabled>                                                                                          
                                        <option value="null">Selecione</option>
                                        <?php
                                        if (is_numeric($usuario)) {
                                            $cur = odbc_exec($con, "select id_usuario,login_user,nome from sf_usuarios where inativo = 0 order by nome") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['id_usuario']; ?>"<?php
                                            if (!(strcmp($RFP['id_usuario'], $usuario))) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP['nome']) ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                    </select>
                                </div>
                                <div style="width:20%; float:left;margin-left:0.5%">
                                    <span>Tipo:</span>                                        
                                    <select style="width:100%" class="select" name="txtTipox" id="txtTipox">
                                        <option value="null">Todos</option>
                                        <option value="1" <?php
                                        if ($tipox == '1') {
                                            echo "SELECTED";
                                        }
                                        ?>>Abertos</option>
                                        <option value="0" <?php
                                        if ($tipox == '0') {
                                            echo "SELECTED";
                                        }
                                        ?>>Fechados</option>
                                    </select>
                                </div>                                            
                                <div style="width:15%; float:left;margin-left:0.5%">
                                    <span>De:</span>
                                    <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial">
                                </div>
                                <div style="width:15%; float:left;margin-left:0.5%">
                                    <span>até:</span>
                                    <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final">                                                                              
                                </div>
                                <div style="width:10%; float:left;  margin-top: 14px; margin-left:0.5%">
                                    <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                </div>                                    
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <?php include "include/RecepcaoForm.php"; ?>
                    <div class="boxhead">
                        <div class="boxtext">Recepção</div>
                    </div>                    
                    <div class="boxtable">                        
                        <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="example">                            
                            <thead>
                                <tr id="formPQ">
                                    <th width="10%"><center>Data</center></th>
                                    <th width="25%">Cliente</th>
                                    <th width="10%">Origem</th>
                                    <th width="5%">Tipo</th>
                                    <th width="20%">Atendente</th>
                                    <th width="5%"><center>Pendente</center></th>
                                    <th width="20%">Encaminhado</th>
                                    <th width="10%"><center>Próx.</center></th>
                                    <th width="14%"><center>Periodo</center></th> 
                                    <th width="5%"><center>Ação:</center></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />  
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type='text/javascript' src='js/RecepcaoForm.js'></script>        
        <?php odbc_close($con); ?>
    </body>
</html>