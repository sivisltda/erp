<?php

include '../../Connections/configini.php';
$aColumns = array('id_fornecedores_despesas', 'razao_social', 'dt_cadastro', 'cnpj', 'cidade', 'estado', 'telefone_contato', 
'email_contato', 'nome_fantasia', 'termometro', 'fornecedores_status', 'login_user');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = " and id_fornecedores_despesas > 0";
$sWhere = "";
$sOrder = " ORDER BY id_fornecedores_despesas desc ";
$sLimit = 20;
$imprimir = 0;
$_GET['sSearch'] = utf8_decode($_GET['sSearch']);

function getColor($temperatura) {
    if ($temperatura == 0) {
        return "#66cff6";
    } else if ($temperatura == 1) {
        return "#bde8fb";
    } else if ($temperatura == 2) {
        return "#fff871";
    } else if ($temperatura == 3) {
        return "#f9b785";
    } else if ($temperatura == 4) {
        return "#f58487";
    }    
}

function getTitle($temperatura) {
    if ($temperatura == 0) {
        return "Muito frio";
    } else if ($temperatura == 1) {
        return "Frio";
    } else if ($temperatura == 2) {
        return "Normal";
    } else if ($temperatura == 3) {
        return "Quente";
    } else if ($temperatura == 4) {
        return "Muito quente";
    }    
}

if (is_numeric($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if ($_GET['pr'] != "") {
    $sWhereX .= " and sf_fornecedores_despesas.procedencia = " . $_GET['pr'];
}

if ($_GET['es'] != "") {
    $sWhereX .= " and estado = " . $_GET['es'];
}

if ($_GET['ci'] != "") {
    $sWhereX .= " and cidade = " . $_GET['ci'];
}

if ($_GET['tp'] != "") {
    $sWhereX .= " and termometro = " . $_GET['tp'];
}

if ($_GET['gp'] != "") {
    $sWhereX .= " and grupo_pessoa = '" . $_GET['gp'] . "'";
}

if (is_numeric($_GET['socio'])) {    
    $sWhereX .= " and sf_fornecedores_despesas.id_fornecedores_despesas in (select id_dependente from sf_fornecedores_despesas_dependentes where id_titular = " . $_GET['socio'] . ")";
}

if (is_numeric($_GET['tipoS'])) {
    $sWhereX .= " and titular = " . $_GET['tipoS'];
}

$inativo = "and sf_fornecedores_despesas.inativo = 0";
$lead = "";
if ($_GET['st'] != "null" && $_GET['st'] != "") {
    if (strpos($_GET['st'], "Bloqueado") !== false) {
        $inativo = " and sf_fornecedores_despesas.bloqueado = 1 ";
    }
    if (strpos($_GET['st'], "Excluido") !== false) {
        $inativo = " and sf_fornecedores_despesas.inativo = 1 ";
    }
    if (strpos($_GET['st'], "Lead") !== false) {
        $lead = " and sf_lead.id_lead is not null";
    }
    $sWhereX .= " and sf_fornecedores_despesas.fornecedores_status in ('" . str_replace(",", "','", $_GET['st']) . "') ";
}
$sWhereX .= $inativo . $lead;

if (isset($_GET['filial']) && $_GET['filial'] != "null") {
    $sWhereX .= " and sf_fornecedores_despesas.empresa in (" . str_replace("\"", "'", str_replace(array("[", "]"), "", $_GET["filial"])) . ")";
} else {
    $sWhereX .= " and sf_fornecedores_despesas.empresa in (select id_filial_f from sf_usuarios_filiais where id_usuario_f = " . $_SESSION["id_usuario"] . ")";    
}

if (isset($_GET['ur'])) {
    $sWhereX .= " and id_user_resp = '" . $_GET['ur'] . "'";
}

if ($contrato == "007" && in_array($_SESSION["id_usuario"], ["161", "10", "290"])) {
    $sWhereX .= " and sf_fornecedores_despesas.id_fornecedores_despesas not in (select id_fornecedores_despesas from sf_fornecedores_despesas where id_user_resp = 12 and id_fornecedores_despesas > 38521) ";
}

if ($crm_pro_ure_ > 0) {
    $sWhereX .= " and (id_usuario = " . $_SESSION["id_usuario"] . " 
    or id_usuario in (select sf_usuarios_dependentes.id_usuario from sf_usuarios_dependentes 
    inner join sf_usuarios on id_fornecedor_despesas = funcionario where sf_usuarios.id_usuario = " . $_SESSION["id_usuario"] . "))";
}

if ($_GET['sSearch'] != "") {
    $sWhereX .= " and (CAST(sf_fornecedores_despesas.id_fornecedores_despesas AS VARCHAR(30)) = '" . $_GET['sSearch'] . "'  or sf_fornecedores_despesas.contato like '%" . $_GET['sSearch'] . "%' or sf_fornecedores_despesas.razao_social like '%" . $_GET['sSearch'] . "%' or sf_fornecedores_despesas.nome_fantasia like '%" . $_GET['sSearch'] . "%' or
    sf_fornecedores_despesas.endereco like '%" . $_GET['sSearch'] . "%' or sf_fornecedores_despesas.cnpj like '%" . $_GET['sSearch'] . "%' or tb_cidades.cidade_nome like '%" . $_GET['sSearch'] . "%' or
    sf_fornecedores_despesas.cnpj like '%" . $_GET['sSearch'] . "%' or sf_fornecedores_despesas.id_fornecedores_despesas in (select fdc.fornecedores_despesas from sf_fornecedores_despesas_contatos fdc where conteudo_contato like '%" . $_GET['sSearch'] . "%'))";
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0 && intval($_GET['iSortCol_' . $i]) < 9) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . "
                                    " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY id_fornecedores_despesas desc";
    }
}

if (isset($_GET['getEstatisticas'])) {
    $sQuery = "select isnull(sum(case when id_lead is not null then 1 else 0 end),0) lead,
    isnull(sum(case when fornecedores_status = 'Aguardando' then 1 else 0 end),0) aguardando,
    isnull(sum(case when fornecedores_status = 'Negociacao' then 1 else 0 end),0) negociacao,
    isnull(sum(case when fornecedores_status = 'Analise' then 1 else 0 end),0) analise,
    isnull(sum(case when fornecedores_status = 'Orcamento' then 1 else 0 end),0) orcamento from (        
    select id_fornecedores_despesas, fornecedores_status, id_lead
    from sf_fornecedores_despesas left join sf_lead on prospect = id_fornecedores_despesas
    left join sf_tipo_documento on forma_pagamento = id_tipo_documento
    left join tb_estados on estado = estado_codigo left join tb_cidades on cidade = cidade_codigo
    left join sf_grupo_cliente on grupo_pessoa = id_grupo left join sf_usuarios on id_user_resp = id_usuario
    WHERE tipo = 'P' $sWhereX ) as x";
    //echo $sQuery; exit;
    $cur2 = odbc_exec($con, $sQuery);
    while ($RFP = odbc_fetch_array($cur2)) {
        $row = array();
        $row["lead"] = $RFP['lead'];
        $row["aguardando"] = $RFP['aguardando'];
        $row["negociacao"] = $RFP['negociacao'];
        $row["analise"] = $RFP['analise'];
        $row["orcamento"] = $RFP['orcamento'];
        echo json_encode($row); 
    }    
    exit;    
}

$sQuery = "SELECT COUNT(*) total FROM sf_fornecedores_despesas
left join sf_lead on prospect = id_fornecedores_despesas
left join sf_tipo_documento on forma_pagamento = id_tipo_documento
left join tb_estados on estado = estado_codigo
left join tb_cidades on cidade = cidade_codigo
left join sf_grupo_cliente on grupo_pessoa = id_grupo
left join sf_usuarios on id_user_resp = id_usuario
WHERE tipo = 'P' $sWhereX " . $sWhere;
//echo $sQuery; exit;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total FROM sf_fornecedores_despesas
left join sf_lead on prospect = id_fornecedores_despesas
left join sf_tipo_documento on forma_pagamento = id_tipo_documento
left join tb_estados on estado = estado_codigo
left join tb_cidades on cidade = cidade_codigo
left join sf_grupo_cliente on grupo_pessoa = id_grupo
left join sf_usuarios on id_user_resp = id_usuario
WHERE tipo = 'P' $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,id_fornecedores_despesas, sf_fornecedores_despesas.razao_social, sf_fornecedores_despesas.dt_cadastro, cnpj, cidade_nome cidade, estado_sigla estado,   
STUFF((SELECT distinct ',' + conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE tipo_contato = 2 and fornecedores_despesas = id_fornecedores_despesas FOR XML PATH('')), 1, 1, '') as email_contato,
STUFF((SELECT distinct ',' + conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE tipo_contato in (0,1) and fornecedores_despesas = id_fornecedores_despesas FOR XML PATH('')), 1, 1, '') as celular_contato,
nome_fantasia, data_nascimento, descricao_grupo, termometro, fornecedores_status, login_user, Observacao, telefone_ps1, telefone_ps2
FROM sf_fornecedores_despesas left join sf_lead on prospect = id_fornecedores_despesas
left join sf_tipo_documento on forma_pagamento = id_tipo_documento
left join tb_estados on estado = estado_codigo
left join tb_cidades on cidade = cidade_codigo
left join sf_grupo_cliente on grupo_pessoa = id_grupo
left join sf_usuarios on id_user_resp = id_usuario
WHERE id_fornecedores_despesas > 0 AND tipo = 'P' " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1=" . $imprimir . " " . $sOrder;
//echo $sQuery1;exit;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $identificacao = utf8_encode($aRow[$aColumns[1]]);
    if (utf8_encode($aRow[$aColumns[8]]) != '') {
        $identificacao = utf8_encode($aRow[$aColumns[1]]) . ' (' . utf8_encode($aRow[$aColumns[8]]) . ')';
    }
    if ($aRow[$aColumns[10]] == "Bloqueado") {
        $row[] = "<center><span title='Bloqueado' style=\"background: #333;display: inline-block;width: 13px;height: 13px;border-radius: 50%;border: 1px solid #D8D8D8;\"></span></center>";        
    } else {
        $row[] = "<center><span title='" . getTitle($aRow[$aColumns[9]]) . "' style=\"background: " . getColor($aRow[$aColumns[9]]) . ";display: inline-block;width: 13px;height: 13px;border-radius: 50%;border: 1px solid #D8D8D8;\"></span></center>";        
    }
    $row[] = "<center>" . escreverDataHora($aRow[$aColumns[2]]) . "</center>";    
    $row[] = "<center>" . utf8_encode($aRow[$aColumns[3]]) . "</center>";    
    $row[] = "<a title=\"Atendimento\" href='ProspectsForm.php?id=" . $aRow[$aColumns[0]] . "' ><div id='formPQ' title='" . $identificacao . "'>" . $identificacao . "</div></a>";            
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[11]]) . "'>" . utf8_encode($aRow[$aColumns[11]]) . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[4]]) . "'>" . utf8_encode($aRow[$aColumns[4]]) . "</div>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[5]]) . "'>" . utf8_encode($aRow[$aColumns[5]]) . "</div></center>";
    $row[] = utf8_encode($aRow["celular_contato"]);    
    $row[] = utf8_encode($aRow[$aColumns[7]]);  
    if ($aRow[$aColumns[10]] == "Aguardando") {
        $row[] = "<center><span style=\"color: orange;\">AGUARDANDO</span></center>";
    } else if ($aRow[$aColumns[10]] == "Negociacao") {
        $row[] = "<center><span style=\"color: #22b14c;\">EM NEGOCIAÇÃO</span></center>";        
    } else if ($aRow[$aColumns[10]] == "Analise") {
        $row[] = "<center><span style=\"color: #56acab;\">EM ANÁLISE</span></center>";        
    } else if ($aRow[$aColumns[10]] == "Orcamento") {
        $row[] = "<center><span style=\"color: #0099cc;\">EM ORÇAMENTO</span></center>";        
    } else if ($aRow[$aColumns[10]] == "Bloqueado") {
        $row[] = "<center><span style=\"color: #333;\">BLOQUEADO</span></center>";
    } else if ($aRow[$aColumns[10]] == "Inativo") {
        $row[] = "<center><span style=\"color: red;\">INATIVO</span></center>";
    } else {
        $row[] = "";        
    }    
    if ($ckb_adm_cli_read_ == 0) {
        if ($aRow[$aColumns[10]] == "Aguardando") {
            $row[] = "<center><a href='ProspectsForm.php?id=" . $aRow[$aColumns[0]] . "&at=0' title='Capturar Prospects'><input name='' type='image' src='../../img/return.png' width='18' height='18' value='Enviar'></a></center>";
        } else if ($aRow[$aColumns[10]] == "Excluido") {
            $row[] = "<center><a title='Reativar'><input name='' type='image' onClick='reativarCli(" . $aRow[$aColumns[0]] . ")' src='../../img/undo_256.png' width='18' height='18' value='Reativar'></a></center>";
        } else {
            $row[] = "";
        }
    } else {
        $row[] = "";
    }
    if (isset($_GET['isExcel'])) {
        $row[] = utf8_encode($aRow["Observacao"]);        
        $row[] = utf8_encode($aRow["telefone_ps1"]);        
        $row[] = utf8_encode($aRow["telefone_ps2"]);        
    }    
    $row[] = utf8_encode($aRow[$aColumns[0]]);
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
