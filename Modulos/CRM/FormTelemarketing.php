<?php
include '../../Connections/configini.php';

$disabled = 'disabled';
$aba1 = "active";
$aba2 = "";
$aba3 = "";
$aba4 = "";
$aba5 = "";

if (isset($_POST['bntSave'])) {
    $pessoaP = "";
    $dataNF = "null";
    $NumNF = "null";

    if (valoresCheck('ckbGarantia') == 1) {
        $dataNF = valoresData('txtDataNota');
        $NumNF = valoresTexto('txtNotaFiscal');
    }

    if (is_numeric($_POST['txtParaCon'])) {
        $pessoaP = utf8_decode($_POST['txtParaCon']);
    } else {
        $pessoaP = utf8_decode($_POST['txtParaConX']);
    }

    $datap = valoresDataHora("txtDataProximo", "txtHoraProximo");            
    $datah = valoresDataHora("txtData", "txtHora");            
    if ($_POST['txtId'] != '') {
        $res = odbc_exec($con, "select count(*) from sf_telemarketing where (de_pessoa = " . $_SESSION["id_usuario"] . " or para_pessoa = " . $_SESSION["id_usuario"] . " or para_pessoa is null) and id_tele = " . $_POST['txtId']) or die(odbc_errormsg());
        if (odbc_result($res, 1) > 0) {
            $queryZ = '';
            if (utf8_decode($_POST['txtPendente']) == "0" && utf8_decode($_POST['txtEraPendente']) == "1") {
                $queryZ = " data_tele_fechamento = GetDate(), de_pessoa_fechamento = " . $_SESSION["id_usuario"] . ",";
            }
            $query = "set dateformat dmy; update sf_telemarketing set " .
                    "para_pessoa = " . utf8_decode($_POST['txtPara']) . "," .
                    "pendente = " . utf8_decode($_POST['txtPendente']) . "," .
                    "proximo_contato = " . $datap . "," . $queryZ .
                    "pessoa = " . $pessoaP . "," .
                    "especificacao = null," .
                    "garantia = " . valoresCheck('ckbGarantia') . "," .
                    "atendimento_servico = " . $_POST['txtservico'] . "," .
                    "procedencia = " . $_POST['txtProcedencia'] . "," .
                    "nota_fiscal = " . $NumNF . "," .
                    "data_tele = " . $datah . "," .
                    "data_nf = " . $dataNF . "," .
                    "valor_orcamento = " . valoresNumericos('txtTaxaOrc') . "," .
                    "relato = " . valoresTexto('txtRelato') . "," .
                    "nomecon = " . valoresTexto('txtNomeCon') . "," .
                    "departamento = " . utf8_decode($_POST['txtDepartamento']) . "," .
                    "dias_garantia = '" . utf8_decode($_POST['txtPrevOrc']) . "'," .
                    "origem = " . utf8_decode($_POST['txtOrigem']) . "," .
                    "numficha = " . valoresTexto('txtNumFicha') . "," .
                    "acompanhar = " . valoresCheck('ckbAcompanhar') . "," .
                    "obs = " . valoresTexto('txtObs') . ", obs_fechamento=" . valoresTexto('txtObsFechamento') . " where id_tele = " . $_POST['txtId'];
            //echo $query;
            odbc_exec($con, $query) or die(odbc_errormsg());
            $query = "0";
            if (isset($_POST['txtEspecificacao'])) {
                for ($i = 0; $i < count($_POST['txtEspecificacao']); $i++) {
                    if ($_POST['txtEspecificacao'][$i] != '') {
                        $query = $query . "," . $_POST['txtEspecificacao'][$i];
                    }
                }
            }
            odbc_exec($con, "delete from sf_telemarketing_item where id_telemarketing = " . $_POST['txtId'] . " and id_especificacao not in (" . $query . ")") or die(odbc_errormsg());
            $ToAdd[0] = "";
            $w = 0;
            $cur = odbc_exec($con, "select te.id_especificacao x ,ti.id_especificacao y from sf_telemarketing_especificacao te left join
            sf_telemarketing_item ti on te.id_especificacao = ti.id_especificacao and ti.id_telemarketing = " . $_POST['txtId'] . "
            where te.id_especificacao in (" . $query . ")");
            while ($RFP = odbc_fetch_array($cur)) {
                if ($RFP['y'] == null) {
                    $ToAdd[$w] = $RFP['x'];
                    $w++;
                }
            }
            for ($j = 0; $j < $w; $j++) {
                $query = "insert into sf_telemarketing_item(id_telemarketing,id_especificacao) values(" . $_POST['txtId'] . "," . $ToAdd[$j] . ")";
                odbc_exec($con, $query);
            }
            echo "<script>parent.FecharBox();</script>";
        } else {
            echo "<script>alert('Não é possível alterar este chamado, o proprietário e o destinatário do chamado estão relacionados a outro usuário!');</script>";
        }
    } else {
        $dataF = 'null';
        $pesF = 'null';
        $ObsF = '';
        if (utf8_decode($_POST['txtPendente']) == "0") {
            $dataF = 'GetDate()';
            $pesF = $_SESSION["id_usuario"];
            $ObsF = utf8_decode($_POST['txtObsFechamento']);
        }
        $query = "set dateformat dmy; insert into sf_telemarketing(de_pessoa,para_pessoa,pendente,proximo_contato,obs,pessoa,data_tele,data_tele_fechamento,obs_fechamento,de_pessoa_fechamento,garantia,atendimento_servico,nota_fiscal,data_nf,valor_orcamento,relato,nomecon,dias_garantia,origem,numficha,acompanhar,especificacao,procedencia,departamento,dt_inclusao)values(" .
                $_SESSION["id_usuario"] . "," .
                utf8_decode($_POST['txtPara']) . "," .
                utf8_decode($_POST['txtPendente']) . "," .
                $datap . "," .
                valoresTexto('txtObs') . "," . $pessoaP . "," . $datah . "," . $dataF . "," . valoresTexto('txtObsFechamento') . "," . $pesF .
                "," . valoresCheck('ckbGarantia') .
                "," . $_POST['txtservico'] .
                "," . $NumNF .
                "," . $dataNF .
                "," . valoresNumericos('txtTaxaOrc') .
                "," . valoresTexto('txtRelato') .
                "," . valoresTexto('txtNomeCon') .
                "," . valoresTexto('txtPrevOrc') .
                "," . $_POST['txtOrigem'] .
                "," . valoresTexto('txtNumFicha') .
                "," . valoresCheck('ckbAcompanhar') .
                ",null," . utf8_decode($_POST['txtProcedencia']) . "," . utf8_decode($_POST['txtDepartamento']) . ",GETDATE())";
        //echo $query;exit();
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_tele from sf_telemarketing order by id_tele desc") or die(odbc_errormsg());
        $id = odbc_result($res, 1);
        if (isset($_POST['txtEspecificacao'])) {
            for ($i = 0; $i < count($_POST['txtEspecificacao']); $i++) {
                if ($_POST['txtEspecificacao'][$i] != '') {
                    $query = "insert into sf_telemarketing_item(id_telemarketing,id_especificacao) values(" . $id . "," . $_POST['txtEspecificacao'][$i] . ")";
                    odbc_exec($con, $query);
                }
            }
        }
        $_POST['txtId'] = $id;
    }
    //}
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_telemarketing_item WHERE id_telemarketing = " . $_POST['txtId']);
        odbc_exec($con, "DELETE FROM sf_mensagens_telemarketing WHERE id_tele = " . $_POST['txtId']);
        odbc_exec($con, "DELETE FROM sf_telemarketing WHERE id_tele = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
        exit();
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select id_tele,u.login_user,para_pessoa,data_tele,pendente,proximo_contato,t.obs,id_fornecedores_despesas,tipo,us.login_user login_user_fechamento,data_tele_fechamento,obs_fechamento,especificacao,razao_social,nome_fantasia,
                            garantia,atendimento_servico,nota_fiscal,data_nf,valor_orcamento,relato,dias_garantia,t.departamento,nomecon,t.procedencia,t.origem,t.numficha,bloqueado,acompanhar 
                            from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa
                            LEFT JOIN sf_usuarios us on us.id_usuario = t.de_pessoa_fechamento
                            LEFT JOIN sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = t.pessoa where id_tele = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_tele']);
        $operador = utf8_encode($RFP['login_user']);
        $data = escreverData($RFP['data_tele']);
        $hora = escreverData($RFP['data_tele'], 'H:i');
        $obs = utf8_encode($RFP['obs']);
        $pendente = utf8_encode($RFP['pendente']);
        $para = utf8_encode($RFP['para_pessoa']);
        $dataProximo = escreverData($RFP['proximo_contato']);
        $horaProximo = escreverData($RFP['proximo_contato'], 'H:i');
        $tipo = utf8_encode($RFP['tipo']);
        $contato = utf8_encode($RFP['id_fornecedores_despesas']);
        $nomecontato = utf8_encode($RFP['razao_social']);
        if ($RFP['nome_fantasia'] != "") {
            $nomecontato = $nomecontato . " (" . utf8_encode($RFP['nome_fantasia']) . ")";
        }
        $operadorFechamento = utf8_encode($RFP['login_user_fechamento']);
        $dataFechamento = escreverData($RFP['data_tele_fechamento']);
        $horaFechamento = escreverData($RFP['data_tele_fechamento'], 'H:i');
        $obsFechamento = utf8_encode($RFP['obs_fechamento']);
        $especificacao = utf8_encode($RFP['especificacao']);
        $ckb_garantia = utf8_encode($RFP['garantia']);
        $id_servico = utf8_encode($RFP['atendimento_servico']);
        $nota_fiscal = utf8_encode($RFP['nota_fiscal']);
        $data_nota = escreverData($RFP['data_nf']);
        $taxaOrc = utf8_encode($RFP['valor_orcamento']);
        $relato = utf8_encode($RFP['relato']);
        $nomecon = utf8_encode($RFP['nomecon']);
        $dias_garantia = utf8_encode($RFP['dias_garantia']);
        $departamento = utf8_encode($RFP['departamento']);
        $procedencia = utf8_encode($RFP['procedencia']);
        $origem = utf8_encode($RFP['origem']);
        $numFicha = utf8_encode($RFP['numficha']);
        $bloqueado = utf8_encode($RFP['bloqueado']);
        $ckbAcompanhar = utf8_encode($RFP['acompanhar']);
    }
    $pendenteFin = 0;
    if (is_numeric($contato)) {
        $cur = odbc_exec($con, "select SUM(total) total from (
        select COUNT(lp.id_lancamento_movimento) total from sf_lancamento_movimento_parcelas lp 
        inner join sf_lancamento_movimento l on lp.id_lancamento_movimento = l.id_lancamento_movimento
        inner join sf_contas_movimento c on c.id_contas_movimento = l.conta_movimento
        where data_pagamento is null and l.status = 'Aprovado' and lp.inativo = 0 and dateadd(day,dias_tolerancia,data_vencimento) < GETDATE() and fornecedor_despesa = " . $contato . "
        union
        select COUNT(sp.solicitacao_autorizacao) total from sf_solicitacao_autorizacao sa 
        inner join sf_solicitacao_autorizacao_parcelas sp on sa.id_solicitacao_autorizacao = sp.solicitacao_autorizacao
        inner join sf_contas_movimento c on c.id_contas_movimento = sa.conta_movimento 
        where data_pagamento is null and status = 'Aprovado' and sp.inativo = 0 and dateadd(day,dias_tolerancia,data_parcela) < GETDATE() and fornecedor_despesa = " . $contato . "
        union
        select count(v.id_venda) total from sf_vendas v inner join sf_venda_parcelas vp on v.id_venda = vp.venda
        where cov in ('C','V') and data_pagamento is null and status = 'Aprovado' and vp.inativo = 0 and data_parcela < GETDATE() and cliente_venda = " . $contato . ") as x");
        while ($RFP = odbc_fetch_array($cur)) {
            if (utf8_encode($RFP['total']) > 0) {
                $pendenteFin = 1;
            }
        }
    }
} else {
    $disabled = '';
    $id = '';
    $operador = $_SESSION["login_usuario"];
    $data = getData("T");
    $hora = date("H:i");
    $obs = '';
    $pendente = 1;
    $para = '';
    $dataProximo = '';
    $horaProximo = '';
    $bloqueado = 0;
    $ckbAcompanhar = 0;
    if (is_numeric($_GET['gb'])) {
        $cur = odbc_exec($con, "select tipo from sf_fornecedores_despesas where id_fornecedores_despesas = '" . $_GET['gb'] . "'") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $tipo = $RFP['tipo'];
        }
        $contato = $_GET['gb'];
    } else {
        $tipo = '';
        $contato = '';
    }
    $operadorFechamento = '';
    $dataFechamento = '';
    $horaFechamento = '';
    $obsFechamento = '';
    $especificacao = '';
    $ckb_garantia = '';
    if ($_GET['jn'] == 's') {
        $id_servico = '1';
    } else {
        $id_servico = '0';
    }
    $nota_fiscal = '';
    $data_nota = '';
    $taxaOrc = '';
    $relato = '';
    $nomecon = '';
    $dias_garantia = '';
    $departamento = '';
    $pendenteFin = 0;
    $origem = 0;
    $numFicha = '';
    $nomecontato = '';

    if (is_numeric($contato)) {
        $cur = odbc_exec($con, "select COUNT(lp.id_lancamento_movimento) total from sf_lancamento_movimento_parcelas lp 
                                inner join sf_lancamento_movimento l on lp.id_lancamento_movimento = l.id_lancamento_movimento
                                where data_pagamento is null and l.status = 'Aprovado' and lp.inativo = 0 and data_vencimento < GETDATE() and fornecedor_despesa = " . $contato . "");
        while ($RFP = odbc_fetch_array($cur)) {
            if (utf8_encode($RFP['total']) > 0) {
                $pendenteFin = 1;
            }
        }
        $cur = odbc_exec($con, "SELECT razao_social,nome_fantasia,bloqueado FROM sf_fornecedores_despesas WHERE id_fornecedores_despesas = " . $contato);
        while ($RFP = odbc_fetch_array($cur)) {
            $bloqueado = $RFP['bloqueado'];
            $nomecontato = utf8_encode($RFP['razao_social']);
            if ($RFP['nome_fantasia'] != "") {
                $nomecontato = $nomecontato . " (" . utf8_encode($RFP['nome_fantasia']) . ")";
            }
        }
    }
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
if ($_GET['fx'] == 't') {
    $disabled = '';
    $aba1 = "active";
    $aba2 = "";
    $aba3 = "";
    $aba4 = "";
    $aba5 = "";
    $para = $_SESSION["id_usuario"];
    $pendente = 1;
} elseif ($_GET['fx'] == 's') {
    $disabled = '';
    $aba1 = "";
    $aba2 = "";
    $aba3 = "active";
    $aba4 = "";
    $aba5 = "";
    $operadorFechamento = $_SESSION["login_usuario"];
    $dataFechamento = getData("T");
    $horaFechamento = date("H:i");
    $pendente = 0;
}
?>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Sivis Business</title>
    <link rel="icon" type="image/ico" href="favicon.ico"/>
    <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>    
    <style type="text/css">
        .labcont {
            width: 100%;
            border: 1px solid transparent;
        }
        .labhead {
            padding: 0;
            border: 1px solid transparent;
        }
        .liscont {
            width: 100%;
            border: 1px solid #EEE;
        }
        .lishead {
            padding: 0;
            background: #EEE;
            border: 1px solid #FFF;
        }
        .lisbody {
            padding: 0 5px;
            line-height: 28px;
            background: #F6F6F6;
            border: 1px solid #FFF;
        }
        .lisfoot {
            padding: 0 5px;
            background: #EEE;
            text-align: right;
            line-height: 28px;
            font-weight: bold;
            border: 1px solid #FFF;
        }
        .lisbody:nth-child(2) { text-align: center; }
        .lisbody:nth-child(3), .lisbody:nth-child(4) { text-align: right; }
        .item .text { color: #FFF }
        .item .text b { color: #EEE }
        select { height: 27px; }
        .select2-container-multi.select2-container-disabled .select2-choices .select2-search-choice {
            background-color: #999;
        }
    </style>
</head> 
<body>
    <form action="FormTelemarketing.php?id=<?php echo $_GET["id"]; ?>&gb=<?php echo $_GET["gb"]; ?>&jn=<?php echo $_GET["jn"]; ?>&fx=<?php echo $_GET["fx"]; ?>" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext"><?php
                if ($id_servico == "1") {
                    echo "Ordem de Serviço";
                } else {
                    echo "Atendimento";
                }
                ?></div>
            <div class="frmicon" style="top:13px" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
        <?php if ($_GET["jn"] != "r") { ?> 
            <input name="txtservico" id="txtservico" value="<?php echo $id_servico; ?>" type="hidden"/>
        <?php } ?>
        <input name="txtEraPendente" id="txtEraPendente" value="<?php
        if ($_GET["fx"] == "s") {
            echo 1;
        } else {
            echo $pendente;
        }
        ?>" type="hidden"/>
        <div class="tabbable frmtabs">
            <ul class="nav nav-tabs" style="margin:0">
                <li class="<?php echo $aba1; ?>"><a href="#tab1" data-toggle="tab">Dados do Chamado</a></li>
                <?php if ($_GET["jn"] != "r") { ?>
                    <li class="<?php echo $aba2; ?>"><a href="#tab2" data-toggle="tab">Histórico de Ações</a></li>
                    <?php if ($ckb_crm_fec_atend_ == 1) { ?>                    
                        <li class="<?php echo $aba3; ?>"><a href="#tab3" data-toggle="tab">Fechamento do Chamado</a></li>
                    <?php
                    }
                }
                ?>
            </ul>
            <div class="tab-content frmcont" style="height:318px">
                <div class="tab-pane <?php echo $aba1; ?>" id="tab1">
                    <table width="100%" border="0" cellpadding="3" class="textosComuns">
                        <tr>
                            <td align="right">Tipo:</td>
                            <td><select <?php
                                if ($_GET["jn"] == "c" || $_GET["jn"] == "s") {
                                    echo "disabled";
                                } else {
                                    echo $disabled;
                                }
                                ?> name="txtPenCon" id="txtPenCon" class="input-medium" style="width:150px">
                                    <option value="" <?php
                                    if ($tipo == "") {
                                        echo "SELECTED";
                                    }
                                    ?>>Selecione</option>
                                    <option value="C" <?php
                                    if ($tipo == "C") {
                                        echo "SELECTED";
                                    }
                                    ?>>Cliente</option>
                                    <?php if ($_GET["jn"] != "r") { ?>
                                        <option value="E" <?php
                                        if ($tipo == "E") {
                                            echo "SELECTED";
                                        }
                                        ?>>Funcionario</option>
                                    <?php } ?>
                                    <option value="F" <?php
                                    if ($tipo == "F") {
                                        echo "SELECTED";
                                    }
                                    ?>>Fornecedor</option>
                                    <option value="P" <?php
                                    if ($tipo == "P") {
                                        echo "SELECTED";
                                    }
                                    ?>>Prospect</option>
                                </select></td>
                            <td align="right">Nome:</td>
                            <td colspan="3">
                                <input name="txtParaConX" id="txtParaConX" value="<?php echo $contato; ?>" type="hidden"/>
                                <input name="txtParaCon" id="txtParaCon" value="<?php echo $contato; ?>" type="hidden"/>
                                <input <?php
                                if ($_GET["jn"] == "c" || $_GET["jn"] == "s") {
                                    echo "disabled";
                                } else {
                                    echo $disabled;
                                }
                                ?> type="text" style="width:100%" name="txtDesc" id="txtDesc" class="inputbox" value="<?php echo $nomecontato; ?>" size="10"/>                               
                            </td>
                        </tr>
                        <tr>
                            <td width="70" align="right">Operador:</td>
                            <td><input type="text" name="txtOperador" id="txtOperador" class="input-medium" value="<?php echo $operador; ?>" disabled></td>
                            <td width="60" align="right">Contato:</td>
                            <td><input type="text" name="txtNomeCon" id="txtNomeCon" maxlength="40" class="input-medium" style="width:120px" value="<?php echo $nomecon; ?>" <?php echo $disabled; ?>></td>
                            <td width="60" align="right">Data/Hora:</td>
                            <td>
                                <input type="text" name="txtData" id="txtData" class="input-medium datepicker" style="width:62%; text-align:center" value="<?php echo $data; ?>" <?php echo $disabled; ?>>
                                <input type="text" name="txtHora" id="txtHora" class="input-medium" style="width:calc(38% - 3px); text-align:center" value="<?php echo $hora; ?>" <?php echo $disabled; ?>>
                            </td>
                        </tr>
                        <?php if ($_GET["jn"] != "r") { ?>
                            <tr>
                                <td align="right">Especificação:</td>
                                <td colspan="5">
                                    <select id="txtEspecificacao" name="txtEspecificacao[]" multiple="multiple" class="select" style="width:100%" class="input-medium" <?php echo $disabled; ?>>
                                        <?php
                                        $cur = odbc_exec($con, "select t.id_especificacao,nome_especificacao,ti.id_item from sf_telemarketing_especificacao t left join sf_telemarketing_item ti on t.id_especificacao = ti.id_especificacao and ti.id_telemarketing = '" . $PegaURL . "' order by nome_especificacao");
                                        while ($RFP = odbc_fetch_array($cur)) { ?>  
                                            <option value="<?php echo $RFP['id_especificacao']; ?>" <?php
                                            if ($RFP["id_item"] != null) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP["nome_especificacao"]); ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>     
                            <tr>
                                <td align="right">Procedência:</td>
                                <td colspan="2">
                                    <select style="width:100%" class="input-medium" <?php echo $disabled; ?> name="txtProcedencia" id="txtProcedencia">
                                        <option value="null">Não Informado</option>
                                        <?php
                                        $cur = odbc_exec($con, "select * from sf_procedencia where tipo_procedencia = 1 order by nome_procedencia") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['id_procedencia'] ?>"<?php
                                            if (!(strcmp($RFP["id_procedencia"], $procedencia))) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP["nome_procedencia"]) ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    Acompanhar: <input <?php echo $disabled; ?> type="checkbox" <?php
                                    if ($ckbAcompanhar == 1) {
                                        echo "checked";
                                    }
                                    ?> name="ckbAcompanhar" value="1"/>
                                </td>
                                <td align="right">
                                    <div <?php
                                    if ($id_servico == 0 || $_GET["jn"] == "r") {
                                        echo "style=\"visibility:hidden\"";
                                    }
                                    ?>>Previsão:</div>
                                </td>
                                <td>
                                    <div <?php
                                    if ($id_servico == 0 || $_GET["jn"] == "r") {
                                        echo "style=\"visibility:hidden\"";
                                    }
                                    ?>>
                                        <input type="text" name="txtPrevOrc" id="txtPrevOrc" maxlength="2" class="input-medium" style="width:40px; text-align:center" value="<?php echo $dias_garantia; ?>" <?php echo $disabled; ?>> (em dias)
                                    </div>
                                </td>
                            </tr> 
                            <tr>
                                <td align="right">Relato:</td>
                                <td colspan="5"><textarea name="txtRelato" id="txtRelato" style="width:100%" maxlength="1024" rows="10" <?php echo $disabled; ?>><?php echo $relato; ?></textarea></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td align="right">Observações:</td>
                            <td colspan="5"><textarea name="txtObs" id="txtObs" style="width:100%" rows="10" <?php echo $disabled; ?>><?php echo $obs; ?></textarea></td>
                        </tr>
                        <tr>
                            <td align="right">Departamento:</td>
                            <td>
                                <select name="txtDepartamento"  id="txtDepartamento" class="input-medium" style="width:100%" <?php echo $disabled; ?>>
                                    <option value="null">Selecione</option>
                                    <?php
                                    $cur = odbc_exec($con, "select id_departamento,nome_departamento from sf_departamentos order by nome_departamento") or die(odbc_errormsg());
                                    while ($RFP = odbc_fetch_array($cur)) {
                                        ?>
                                        <option value="<?php echo $RFP['id_departamento'] ?>"<?php
                                        if (!(strcmp($RFP["id_departamento"], $departamento))) {
                                            echo "SELECTED";
                                        }
                                        ?>><?php echo utf8_encode($RFP["nome_departamento"]); ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td align="right">Responsável:</td>
                            <td><span id="carregando" name="carregando2" style="color:#666; display:none">Aguarde, carregando...</span>
                                <select name="txtPara"  id="txtPara" class="input-medium" style="width:100%" <?php echo $disabled; ?>>
                                    <?php if ($ckb_out_todos_operadores_ == 1) { ?>
                                        <option value="null">Selecione</option>
                                        <?php
                                    } $cur = odbc_exec($con, "select id_usuario,login_user,nome from sf_usuarios where " . ($ckb_out_todos_operadores_ != 1 ? " id_usuario = " . $_SESSION["id_usuario"] . " AND " : "") . " inativo = 0 order by nome") or die(odbc_errormsg());
                                    while ($RFP = odbc_fetch_array($cur)) {
                                        ?>
                                        <option value="<?php echo $RFP['id_usuario'] ?>"<?php
                                        if (!(strcmp($RFP["id_usuario"], $para))) {
                                            echo "SELECTED";
                                        }
                                        ?>><?php echo utf8_encode($RFP["nome"]); ?></option>
                                    <?php } ?>
                                </select>
                            </td>                        
                            <?php if ($_GET["jn"] != "r") { ?>  
                                <td width="60" align="right">Próximo<br/>Contato:</td>
                                <td>
                                    <input type="text" name="txtDataProximo" id="txtDataProximo" class="input-medium datepicker" style="width:62%; text-align:center" value="<?php echo $dataProximo; ?>" <?php echo $disabled; ?>>
                                    <input type="text" name="txtHoraProximo" id="txtHoraProximo" class="input-medium" style="width:calc(38% - 3px); text-align:center" value="<?php echo $horaProximo; ?>" <?php echo $disabled; ?>>
                                </td>
                            <?php } else { ?>
                                <td align="right">Tipo:</td>
                                <td>
                                    <input type="radio" name="txtservico" value="0" <?php
                                    if ($id_servico == "0") {
                                        echo "checked";
                                    }
                                    ?> <?php echo $disabled; ?>>Atendimento<br/>
                                    <input type="radio" name="txtservico" value="1" <?php
                                    if ($id_servico == "1") {
                                        echo "checked";
                                    }
                                    ?> <?php echo $disabled; ?>>Ordem de Serviço
                                </td>
                                <td></td>
                                <td></td>
                            <?php } ?>
                        </tr>
                        <tr <?php
                        if ($id_servico == 0 || $_GET["jn"] == "r") {
                            echo "style=\"visibility:hidden\"";
                        }
                        ?>>
                            <td align="right">Garantia:</td>
                            <td>
                                <span style="width:15%; margin-top:6px; float:left">
                                    <input name="ckbGarantia" id="ckbGarantia" <?php echo $disabled; ?> type="checkbox" onClick="if ($('#ckbGarantia').is(':checked')) {
                                                $('#txtDataNota').prop('disabled', false);
                                                $('#txtNotaFiscal').prop('disabled', false);
                                            } else {
                                                $('#txtDataNota').prop('disabled', true);
                                                $('#txtNotaFiscal').prop('disabled', true);
                                            }" <?php
                                           if ($ckb_garantia == 1) {
                                               echo "checked";
                                           }
                                           ?> value="1" class="input-medium"/>
                                </span>
                                <span style="width:15%; margin-top:6px; font-size:11px; float:left">NF:</span>
                                <span>
                                    <input type="text" name="txtNotaFiscal" id="txtNotaFiscal" maxlength="128" class="input-medium" style="width:70%; text-align:center" value="<?php echo $nota_fiscal; ?>" <?php
                                    if ($ckb_garantia == 1) {
                                        echo $disabled;
                                    } else {
                                        echo "disabled";
                                    }
                                    ?>/>
                                </span>
                            </td>
                            <td align="right">Data:</td>
                            <td><input type="text" name="txtDataNota" id="txtDataNota" class="input-medium datepicker" style="text-align:center" value="<?php echo $data_nota; ?>" <?php
                                if ($ckb_garantia == 1) {
                                    echo $disabled;
                                } else {
                                    echo "disabled";
                                }
                                ?>/>
                            </td>
                            <td align="right">Taxa Orçamento:</td>
                            <td><input type="text" name="txtTaxaOrc" id="txtTaxaOrc" class="input-medium" style="text-align:center" value="<?php
                                if (is_numeric($taxaOrc)) {
                                    echo escreverNumero($taxaOrc);
                                }
                                ?>" <?php echo $disabled; ?>/>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="tab-pane <?php echo $aba2; ?>" id="tab2">
                    <div class="body" style="width:625px; height:318px; margin:0; padding:0; position:fixed">
                        <div class="block">
                            <div class="toolbar dark" style="background:#DCDCDC">
                                <div class="input-prepend input-append">
                                </div>
                                <table width="99%" border="0" cellpadding="3" class="textosComuns">
                                    <tr>
                                        <td colspan="2">
                                            <input id="txtDataHistorico" type="text" class="input-medium datepicker"  style="width:16%; text-align:center" value="<?php echo getData("T"); ?>" <?php echo $disabled; ?>/>
                                            <input id="txtHoraHistorico" type="text" class="input-medium" style="width:10%; text-align:center" value="<?php echo date("H:i"); ?>" <?php echo $disabled; ?>/>
                                            <select id="txtDeHistorico" class="input-medium" style="width:36%" <?php echo $disabled; ?>>
                                                <?php if ($ckb_out_todos_operadores_ == 1) { ?>
                                                    <option value="null">Selecione</option>
                                                    <?php
                                                } $cur = odbc_exec($con, "select id_usuario,login_user,nome from sf_usuarios where " . ($ckb_out_todos_operadores_ != 1 ? " id_usuario = " . $_SESSION["id_usuario"] . " AND " : "") . " inativo = 0 order by nome") or die(odbc_errormsg());
                                                while ($RFP = odbc_fetch_array($cur)) {
                                                    ?>
                                                    <option value="<?php echo $RFP['id_usuario'] ?>"<?php
                                                    if (!(strcmp($RFP["id_usuario"], $_SESSION["id_usuario"]))) {
                                                        echo "SELECTED";
                                                    }
                                                    ?>><?php echo utf8_encode($RFP["nome"]); ?></option>
                                                <?php } ?>
                                            </select>
                                            <select id="txtParaHistorico" class="input-medium" style="width:36%" <?php echo $disabled; ?>>
                                                <?php if ($ckb_out_todos_operadores_ == 1) { ?>
                                                    <option value="null">Selecione</option>
                                                    <?php
                                                } $cur = odbc_exec($con, "select id_usuario,login_user,nome from sf_usuarios where " . ($ckb_out_todos_operadores_ != 1 ? " id_usuario = " . $_SESSION["id_usuario"] . " AND " : "") . " inativo = 0 order by nome") or die(odbc_errormsg());
                                                while ($RFP = odbc_fetch_array($cur)) {
                                                    ?>
                                                    <option value="<?php echo $RFP['id_usuario'] ?>"<?php
                                                    if (!(strcmp($RFP["id_usuario"], $_SESSION["id_usuario"]))) {
                                                        echo "SELECTED";
                                                    }
                                                    ?>><?php echo utf8_encode($RFP["nome"]); ?></option>
                                                <?php } ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><textarea type="text" id="textoArea" class="input-medium" <?php echo $disabled; ?> row="2" style="width:100%"></textarea></td>
                                        <td width="80" align="right" style="line-height:56px">
                                            <button type="button" onclick="addMensagens();" name="bntSaveMsg" <?php echo $disabled; ?> style="height:45px; width:75px" <?php
                                            if ($PegaURL == "") {
                                                echo "DISABLED";
                                            }
                                            ?> class="btn dblue">Incluir <span class="icon-arrow-next icon-white"></span></button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="divMensagens" class="messages" style="height:212px; background:#F7F7F7; border-left:1px solid #DCDCDC; border-bottom:1px solid #DCDCDC; overflow-y:scroll"></div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane <?php echo $aba3; ?>" id="tab3">
                    <table width="100%" border="0" cellpadding="3" class="textosComuns">
                        <tr>
                            <td width="100" align="right">Operador:</td>
                            <td width="300"><input type="text" name="txtOperadorFechamento" id="txtOperadorFechamento" class="input-medium" style="width:300px" value="<?php echo $operadorFechamento; ?>" disabled></td>
                            <td width="100" align="right">Pendente:</td>
                            <td><select name="txtPendente" id="txtPendente" class="input-medium" style="width:100%" <?php echo $disabled; ?>>
                                    <option value="1" <?php
                                    if ($pendente == 1) {
                                        echo "SELECTED";
                                    }
                                    ?>>Sim</option>
                                    <option value="0" <?php
                                    if ($pendente == 0) {
                                        echo "SELECTED";
                                    }
                                    ?>>Não</option>
                                </select></td>
                        </tr>
                        <tr>
                            <td align="right">Data:</td>
                            <td><input type="text" name="txtDataFechamento" id="txtDataFechamento" class="input-medium datepicker" style="width:123px; text-align:center" value="<?php echo $dataFechamento; ?>" disabled></td>
                            <td align="right">Hora:</td>
                            <td><input type="text" name="txtHoraFechamento" id="txtHoraFechamento" class="input-medium" style="text-align:center" value="<?php echo $horaFechamento; ?>" disabled></td>
                        </tr>
                        <tr>
                            <td align="right">Origem Chamado:</td>
                            <td><select name="txtOrigem" id="txtOrigem" class="input-medium" style="width:123px" <?php echo $disabled; ?>>
                                    <option value="0" <?php
                                    if ($origem == 0) {
                                        echo "SELECTED";
                                    }
                                    ?>>Interno</option>
                                    <option value="1" <?php
                                    if ($origem == 1) {
                                        echo "SELECTED";
                                    }
                                    ?>>Externo</option>
                                </select></td>
                            <td align="right">Número OS:</td>
                            <td><input type="text" name="txtNumFicha" id="txtNumFicha" class="input-medium" style="text-align:center" value="<?php echo $numFicha; ?>" <?php echo $disabled; ?>></td>
                        </tr>
                        <tr>
                            <td align="right">Solução:</td>
                            <td colspan="3"><textarea name="txtObsFechamento" id="txtObsFechamento" style="width:100%; height:210px" <?php echo $disabled; ?>><?php echo $obsFechamento; ?></textarea></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="frmfoot">
            <div id="msg" style="float:left; color:#FF0000; margin-left:20px">
                <center><?php
                    if ($bloqueado == 1) {
                        echo "Bloqueado";
                    } else if ($pendenteFin == 1) {
                        echo "Pendência Financeira";
                    }
                    ?></center>
            </div>
            <div class="frmbtn">
                <?php if ($disabled == "") { ?>
                    <button class="btn green" onClick="return validar()" type="submit" name="bntSave"><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST["txtId"] == "") { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar"><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else { ?>
                    <button class="btn green" type="submit" name="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                    <?php if ($ckb_crm_exc_atend_ == 1 && ($operador == $_SESSION["id_usuario"] || $para == $_SESSION["id_usuario"])) { ?>
                        <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="ico-remove"></span> Excluir</button>                                       
                    <?php } if ($id_servico == "1") { ?>
                        <a href="./../Servicos/Ordem.php?id=<?php echo $id . "&crt=" . $_SESSION["contrato"]; ?>" target="_blank">
                            <button class="btn blue" type="button" name="bntBol" value="Imprimir"><span class="icon-print icon-white"></span> Imprimir</button>
                        </a>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
    <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
    <script type="text/javascript" src="../../js/plugins/other/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/epiechart/jquery.easy-pie-chart.js"></script>
    <script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/plugins.js"></script>
    <script type="text/javascript" src="../../js/charts.js"></script>
    <script type="text/javascript" src="../../js/actions.js"></script>
    <script type="text/javascript" src="../../js/app.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>     
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type="text/javascript">
    
        $("#txtData, #txtDataProximo, #txtDataNota, #txtDataHistorico").mask(lang["dateMask"]);
        $("#txtHora, #txtHoraProximo, #txtHoraHistorico").mask("99:99");
        $("#txtTaxaOrc").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
        
        function validar() {
            if (!$.isNumeric($("#txtParaCon").val())) {
                alert("Selecione o destinatário do atendimento!");
                return false;
            }
            if ($("#txtPendente").val() === "1" && $("#txtObsFechamento").val().length > 0) {
                alert("Não é possível informar uma solução para um atendimento pendente!");
                return false;
            }
            if ($("#txtPendente").val() === "1" && $("#txtDataProximo").val().length === 0) {
                alert("Para um atendimento pendente, é necessário informar uma data de próximo contato!");
                return false;
            }
            return true;
        }
        
        $(function () {
            $("#txtDesc").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "ajax.php",
                        dataType: "json",
                        data: {
                            q: request.term,
                            t: $("#txtPenCon").val()
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    $("#txtParaCon").val(ui.item.id).show();
                    $("#txtDesc").val(ui.item.label).show();
                    if (ui.item.id !== "") {
                        $("#msg").hide();
                        $.getJSON("fornecedor_divida.ajax.php?search=", {txtParaCon: ui.item.id, ajax: "true"}, function (j) {
                            var options = "";
                            for (var i = 0; i < j.length; i++) {
                                if (j[i].bloqueado > 0) {
                                    options = "Bloqueado";
                                } else {
                                    if (j[i].total > 0) {
                                        options = "Pendência Financeira";
                                    }
                                }
                            }
                            $("#msg").html(options).show();
                        });
                    } else {
                        $("#msg").html(options).show();
                    }
                }
            });

            $("#txtDepartamento").change(function () {
                if ($(this).val()) {
                    $("#txtPara").hide();
                    $("#carregando2").show();
                    $.getJSON("departamento.ajax.php?search=", {txtDepartamento: $(this).val(), ajax: "true"}, function (j) {
                        var options = "<option value='null'>Selecione</option>";
                        for (var i = 0; i < j.length; i++) {
                            options += "<option value='" + j[i].id_usuario + "'>" + j[i].nome + "</option>";
                        }
                        $("#txtPara").html(options).show();
                        $("#carregando2").hide();
                    });
                } else {
                    $("#txtPara").html("<option value='null'>Selecione</option>");
                }
            });
        });
        
        function ListMensagens() {
            var usuario = "<?php echo $_SESSION['id_usuario']; ?>";
            var conteudo = "";
            $("#divMensagens").html("");
            $.getJSON("mensagens.crm.ajax.php?search=", {
                codCrm: <?php
                if (is_numeric($_GET["id"])) {
                    echo $_GET["id"];
                } else {
                    echo "0";
                }
                ?>,
                ajax: "true"
            }, function (j) {
                for (var i = 0; i < j.length; i++) {
                    conteudo = "<div style=\"padding:5px; margin:5px 0\" class=\"item ";
                    if (usuario === j[i].id_usuario) {
                        conteudo += "blue";
                    } else {
                        conteudo += "dblue out";
                    }
                    conteudo += "\">";
                    conteudo += "<div class=\"text\"><b>" + j[i].data + " - " + j[i].login_user + " fala para " + j[i].login_userp + ":</b>";
                    if (usuario === j[i].id_usuario) {
                        conteudo += "<div style=\"float:right; color:#FFF\" onClick=\"return delMensagens(" + j[i].id + ")\"><b>x</b></div>";
                    }
                    conteudo += "<br />" + j[i].mensagem + "</div></div>";
                    $("#divMensagens").prepend(conteudo);
                }
            });
        }
        
        function addMensagens() {
            if ($("#textoArea").val().length > 0) {
                $.post("mensagens.crm.controler.ajax.php", {
                    id: <?php
                    if (is_numeric($_GET["id"])) {
                        echo $_GET["id"];
                    } else {
                        echo "0";
                    }
                    ?>,
                    data: $("#txtDataHistorico").val(),
                    hora: $("#txtHoraHistorico").val(),
                    de: $("#txtDeHistorico").val(),
                    para: $("#txtParaHistorico").val(),
                    txtMsg: $("#textoArea").val(),
                    acao: 0
                }).done(function (data) {
                    $("#textoArea").val("");
                    ListMensagens();
                });
            } else {
                alert("Preencha as informações de mensagens corretamente!");
            }
        }
        
        function delMensagens(numero) {
            if (confirm("Confirma a exclusão desta mensagem?")) {
                $.post("mensagens.crm.controler.ajax.php", {id: numero, acao: 2}).done(function (data) {
                    ListMensagens();
                });
            }
        }

        ListMensagens();
    </script>        
</body>
<?php odbc_close($con); ?>
</html>