<?php
include "../../Connections/configini.php";
include "form/ChatbotFormServer.php";
include "./../Comercial/modelos/variaveis.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <style>
            .ui-dialog .ui-dialog-content, .ui-dialog-titlebar{
                border-left: 0px !important;
                border-right: 0px !important;
                border-top: 0px !important
            }
            .ui-dialog .ui-dialog-buttonpane button{
                background: #C22439 !important;
                border: 1px solid #C22439 !important;
            }
            .ui-dialog .ui-dialog-buttonpane{
                padding: 0 0px !important;
                padding: 0px;
            }
            .dropdown {
                width: 19%;
            }            
            .variaveis .dropdown .dropdown-toggle {
                width: 100%;
            }
            .variaveis .dropdown-menu{
                width: 100%;
                max-height: 420px;
            }            
            .data-fluid {
                overflow-x: auto;    
            }      
            .btn-icon {
                width: 36px;
                height: 25px;
                background-color: gray;
            }
        </style>
    </head>
    <body>
        <form name="frmEnviaDados" id="frmEnviaDados" method="POST">
            <div class="frmhead">
                <div class="frmtext">Chatbot</div>
                <div class="frmicon" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input name="txtId" id="txtId" type="hidden" value="<?php echo $id; ?>"/>                                
                <input name="txtPai" id="txtPai" type="hidden" value="<?php echo $id_pai; ?>"/>                                
                <div style="float: left; width: 5%;">
                    <label for="txtEntrada">Entrada:</label>                    
                    <input name="txtEntrada" id="txtEntrada" type="text" style="width:100%" maxlength="50" <?php echo $disabled; ?> value="<?php echo $entrada; ?>"/>
                </div>
                <div style="float:left; width:10%; height: 46px; margin-left: 1%;">
                    <label for="txtTipo">Tipo Interação:</label>
                    <select name="txtTipo" id="txtTipo" class="select" style="width:100%" <?php echo $disabled; ?>>
                        <option value="0" <?php echo ($tipo == 0 ? "selected" : ""); ?>>Default</option>
                        <option value="1" <?php echo ($tipo == 1 ? "selected" : ""); ?>>Cliente</option>
                        <option value="2" <?php echo ($tipo == 2 ? "selected" : ""); ?>>Sem Cadastro</option>
                        <option value="3" <?php echo ($tipo == 3 ? "selected" : ""); ?>>Lead</option>
                    </select>
                </div>
                <div style="float:left; width:10%; height: 46px; margin-left: 1%;">
                    <label for="txtRefresh">Fim de Ciclo:</label>
                    <select name="txtRefresh" id="txtRefresh" class="select" style="width:100%" <?php echo $disabled; ?>>
                        <option value="0" <?php echo ($refresh == 0 ? "selected" : ""); ?>>Sim</option>
                        <option value="1" <?php echo ($refresh == 1 ? "selected" : ""); ?>>Não</option>
                    </select>
                </div>
                <div style="float: left; width: 21%; margin-left: 1%;">                    
                    <label for="txtEmoticon">Ícones:</label>
                    <?php include './../CRM/include/ItensIcones.php'; ?>                                        
                </div>                
                <div style="clear:both;"></div>                
                <div class="variaveis">
                    <hr style="margin: 5px 0px 5px 0px;">
                    <?php include './../Comercial/include/ItensMenu.php'; ?>
                </div>  
                <div style="clear:both;"></div>                
                <div style="width:100%;">
                    <hr style="margin:2px 0; border-top:1px solid #DDD">
                </div>
                <div class="data-fluid">
                    <label for="txtSaida"></label>
                    <textarea id="txtSaida" name="txtSaida" style="height: 406px;" <?php echo $disabled; ?>><?php echo $saida; ?></textarea>
                </div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == '') { ?>
                        <button class="btn btn-success" type="submit" name="btnSave" title="Gravar" id="btnSave" onclick="return validaForm();" ><span class="ico-checkmark"></span> Gravar</button>
                        <?php if ($_POST['txtId'] == '') { ?>
                            <button class="btn yellow" title="Cancelar" onClick="parent.FecharBox()" id="btnCancelar"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                            <button class="btn yellow" title="Cancelar" type="submit" name="btnCancelar" id="btnCancelar" ><span class="ico-reply"></span> Cancelar</button>
                            <?php
                        }
                    } else { ?>
                        <button class="btn  btn-success" type="submit" title="Novo" name="btnNew" id="btnNew" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                        <button class="btn  btn-success" type="submit" title="Alterar" name="btnEdit" id="btnEdit" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                        <div class="btn red" style="background-color:#68AF27; margin-right:1%" onClick="bootbox.confirm('Confirma a exclusão do registro?', function (result) {
                            if (result) {
                                $('#btnDelete').click();
                            }
                        });">
                            <div class="icon"><span class=" ico-remove"></span>&nbsp;Excluir</div>
                        </div>
                        <input id="btnDelete" name="btnDelete" type="submit" style="display:none"/>
                    <?php } ?>
                </div>
            </div>
        </form>
        <script type='text/javascript' src='../../js/plugins/jquery-ui-1.11.4/jquery-ui.min.js'></script>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/ckeditorAll/ckeditor.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type='text/javascript' src='js/ChatbotForm.js'></script>        
        <?php odbc_close($con); ?>
    </body>
</html>