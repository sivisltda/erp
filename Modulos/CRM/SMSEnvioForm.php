<?php 
include "../../Connections/configini.php";

$mdl_wha_ = returnPart($_SESSION["modulos"],16);
$title = (isset($_GET["zap"]) ? "Envia Mensagem Whatsapp:" : "Envio de SMS:");
$max = (isset($_GET["zap"]) ? "2000" : "160");
$type = (isset($_GET["zap"]) ? ($mdl_wha_ == 1 ? "1" : "2") : "0");
$where = (isset($_GET["zap"]) ? "2" : "1");
$where2 = (isset($_GET["bol"]) ? "and mensagem like '%||M_%'" : "and mensagem not like '%||M_%'");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <link href="../../js/plugins/jquery-ui-1.11.4/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    </head>
    <body>
        <form name="frmEnviaDados" id="frmEnviaDados" method="POST">                        
            <div class="frmhead">
                <div class="frmtext"><?php echo $title; ?></div>
                <div class="frmicon" style="top:12px" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input name="txtSms" id="txtSms" type="hidden"/>
                <input name="txtType" id="txtType" type="hidden" value="<?php echo $type; ?>"/>
                <div style="height:42px">
                    <label for="txtDescricao">Modelo:</label>
                    <select class="select" id="txtModelo" name="txtModelo" style="width:100%; float:left" <?php if($mdl_wha_ == 0 && $where == "2"){ echo "readonly";}?>>
                        <option value="null">Nenhum</option>
                        <?php $cur = odbc_exec($con, "Select id_email, descricao from sf_emails where tipo = " . $where . " " . $where2. " order by descricao") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option value="<?php echo $RFP['id_email'] ?>"><?php echo utf8_encode($RFP['descricao']); ?></option> 
                        <?php } ?>
                    </select>
                </div>
                <div class="data-fluid" style="overflow-x: inherit;">
                    <label style="float: left;" for="ckeditor">Caracteres restantes: <span class="caracteres"><?php echo $max; ?></span></label>
                    <textarea id="ckeditor" name="ckeditor" maxlength="<?php echo $max; ?>" style="height:193px"></textarea>
                </div>                
            </div>
            <div class="frmfoot">
                <div style="width: 50%;float: left;text-align: left;">
                    Envios
                    <span id="pgIni">0</span>
                    de
                    <span id="pgFim">0</span>
                </div>
                <div class="frmbtn">
                    <button class="btn btn-success" type="button" name="btnEnviar" title="Enviar" id="btnEnviar" value="Enviar"><span class="ico-checkmark"></span> Enviar </button>
                    <button class="btn yellow" title="Cancelar" onClick="parent.FecharBox(1)" id="btnCancelar"><span class="ico-reply"></span> Cancelar</button>
                </div>
            </div>
        </form>
        <script type='text/javascript' src='../../js/plugins/jquery-ui-1.11.4/jquery-ui.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/plugins/ckeditor/ckeditor.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>
        <script type="text/javascript" src="js/SmsForm.js"></script>        
    </body>
</html>