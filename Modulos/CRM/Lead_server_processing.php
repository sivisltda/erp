<?php

require_once(__DIR__ . '/../../Connections/configini.php');
$aColumns = array('id_lead', 'sf_lead.dt_cadastro', 'sf_lead.razao_social', 'telefone_contato', 'email_contato', 'nome_procedencia', 'sf_lead.inativo');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "id_lead > 0 ";
$sWhere = "";
$sLimit = 0;
$sQtd = 20;
$imprimir = 0;
$grupoArray = [];

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (is_numeric($_GET['txtStatus'])) {
    if ($_GET['txtStatus'] == "0") {
        $sWhereX .= " and sf_lead.inativo = 0 and prospect is null ";
    } else if ($_GET['txtStatus'] == "1") {
        $sWhereX .= " and sf_lead.inativo = 0 and tipo = 'P' ";
    } else if ($_GET['txtStatus'] == "2") {
        $sWhereX .= " and sf_lead.inativo = 0 and tipo = 'C' ";
    } else if ($_GET['txtStatus'] == "3") {   
        $sWhereX .= " and sf_lead.inativo = 1 ";        
    }
}

if ($_GET['dtb'] != '' && $_GET['dte'] != '') {
    $sWhereX .= " and " . ($_GET['tpData'] == "0" ? "sf_lead.dt_convert" : ($_GET['tpData'] == "1" ? "sf_lead.dt_cadastro" : "sf_fornecedores_despesas.dt_convert_cliente")) . " between " . valoresDataHora2(str_replace("_", "/", $_GET['dtb']), "00:00:00") . " and " . valoresDataHora2(str_replace("_", "/", $_GET['dte']), "23:59:59");
}

if (isset($_GET['ur'])) {
    $sWhereX .= " and sf_lead.indicador in (select funcionario from sf_usuarios where id_usuario = '" . $_GET['ur'] . "')";
}

if ($crm_lea_ure_ > 0) {
    $sWhereX .= " and (sf_lead.indicador in (select funcionario from sf_usuarios where id_usuario = '" . $_SESSION["id_usuario"] . "')
    or sf_lead.indicador in (select funcionario from sf_usuarios where id_usuario in (select sf_usuarios_dependentes.id_usuario 
    from sf_usuarios_dependentes inner join sf_usuarios on id_fornecedor_despesas = funcionario 
    where sf_usuarios.id_usuario = '" . $_SESSION["id_usuario"] . "')))";
}

if ($_GET['tpr'] == "0" && isset($_GET['tpg'])) {
    $whereX .= " and sf_lead.indicador in (select funcionario from sf_usuarios where id_usuario in (" . str_replace(array("[", "]", "\"", "'"), "", $_GET["tpg"]) . "))";
}

if (is_numeric($_GET['txtProcedencia'])) {
    $sWhereX .= " and id_procedencia = " . $_GET['txtProcedencia'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['fil'])) {
    $sWhereX .= " and sf_lead.empresa in (" . str_replace(array("[", "]", "\"", "'"), "", $_GET["fil"]) . ")";
} else {
    $sWhereX .= " and sf_lead.empresa in (select id_filial from sf_filiais
    inner join sf_usuarios_filiais on id_filial_f = id_filial 
    inner join sf_usuarios on id_usuario_f = id_usuario 
    where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "')";
}

if ($_GET['txtBusca'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= prepareCollum($aColumns[$i], 0) . " LIKE '%" . utf8_decode($_GET['txtBusca']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}
                    
if (isset($_GET['tp']) && $_GET['tp'] == '0') {    
    $output["Status"] = array();
    $output["Indicadores"] = array();        
    $sQuery = "set dateformat dmy; SELECT x.status status, count(x.status) total FROM (
    SELECT case when sf_lead.inativo = 1 then 'INATIVO' when sf_lead.inativo = 0 AND tipo = 'P' then 'CONV.PROSPECT' 
    when sf_lead.inativo = 0 AND tipo = 'C' then 'CONV.CLIENTE' ELSE 'AGUARDANDO' end status 
    FROM sf_lead left join sf_procedencia on id_procedencia = procedencia
    left join sf_fornecedores_despesas on id_fornecedores_despesas = prospect
    WHERE " . $sWhereX . ") AS x group by x.status";
    //echo $sQuery; exit;
    $cur2 = odbc_exec($con, $sQuery);
    while ($aRow = odbc_fetch_array($cur2)) {
        $row = array();
        $row["desc"] = utf8_encode($aRow['status']);
        $row["valor"] = utf8_encode($aRow['total']);
        $output["Status"][] = $row;
    }    
    $sQuery = "set dateformat dmy; select x.indicador, count(x.indicador) total from (
    SELECT isnull(sf_fornecedores_despesas.razao_social, 'SEM INDICADOR') indicador
    FROM sf_lead left join sf_procedencia on id_procedencia = procedencia
    left join sf_fornecedores_despesas on id_fornecedores_despesas = sf_lead.indicador
    WHERE " . $sWhereX . ") as x group by x.indicador order by 2 desc;";
    //echo $sQuery; exit;    
    $cur2 = odbc_exec($con, $sQuery);
    while ($aRow = odbc_fetch_array($cur2)) {
        $row = array();
        $row["title"] = utf8_encode($aRow['indicador']);
        $row["value"] = utf8_encode($aRow['total']);
        $output["Indicadores"][] = $row;
    }    
    echo json_encode($output); exit;
}

$sQuery = "set dateformat dmy;SELECT COUNT(*) total FROM sf_lead left join sf_procedencia on id_procedencia = procedencia
left join sf_fornecedores_despesas on id_fornecedores_despesas = prospect
WHERE " . $sWhereX . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($aRow = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $aRow['total'];
}

$sQuery = "set dateformat dmy;SELECT COUNT(*) total FROM sf_lead left join sf_procedencia on id_procedencia = procedencia
left join sf_fornecedores_despesas on id_fornecedores_despesas = prospect
WHERE " . $sWhereX;
$cur2 = odbc_exec($con, $sQuery);
while ($aRow = odbc_fetch_array($cur2)) {
    $iTotal = $aRow['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "set dateformat dmy;SELECT * FROM(SELECT ROW_NUMBER() OVER (ORDER BY sf_lead.dt_cadastro desc) as row, sf_lead.*, nome_procedencia, tipo,
sf_lead.indicador_lead id_indicador, (select top 1 razao_social from sf_lead ld where ld.id_lead = sf_lead.indicador_lead) indicador_nome,
sf_lead.indicador_cliente id_indicador_cli,(select top 1 razao_social from sf_fornecedores_despesas fn where fn.id_fornecedores_despesas = sf_lead.indicador_cliente) indicador_nome_cli,
(select top 1 razao_social from sf_fornecedores_despesas fn where fn.id_fornecedores_despesas = sf_lead.indicador) indicador_func
FROM sf_lead left join sf_procedencia on id_procedencia = procedencia
left join sf_fornecedores_despesas on id_fornecedores_despesas = prospect
WHERE " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " ORDER BY dt_cadastro desc";
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $id_indicador = (is_numeric($aRow['id_indicador']) ? $aRow['id_indicador'] : $aRow['id_indicador_cli']);
    $indicador_cliente = (strlen(utf8_encode($aRow["indicador_nome"])) > 0 ? utf8_encode($aRow["indicador_nome"]) : utf8_encode($aRow["indicador_nome_cli"]));
    $row = array();
    $row[] = "<center><div id='formPQ' title='" . escreverDataHora($aRow["dt_cadastro"]) . "'>" . escreverDataHora($aRow["dt_cadastro"]) . "</div></center>";
    $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[$aColumns[0]]) . ");'><div id='formPQ' title='" . utf8_encode($aRow["razao_social"]) . "'>" . utf8_encode($aRow["razao_social"]) . "</div></a>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[3]]) . "'>" . utf8_encode($aRow[$aColumns[3]]) . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[4]]) . "'>" . utf8_encode($aRow[$aColumns[4]]) . "</div>";    
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[5]]) . "'>" . utf8_encode($aRow[$aColumns[5]]) . "</div>";
    $row[] = "<center><div id='formPQ' title='" . statusLead($aRow["tipo"], $aRow["inativo"]) . "'>" . statusLead($aRow["tipo"], $aRow["inativo"]) . "</div></center>";    
    $row[] = "<div id='formPQ' title='" . $indicador_cliente . "'>" . $indicador_cliente . "</div>";    
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["indicador_func"]) . "'>" . formatNameCompact(utf8_encode($aRow["indicador_func"])) . "</div>";    
    $row[] = "<center><a title='Excluir' href='Lead.php?Del=" . $aRow[$aColumns[0]] . "'><input name='' type='image' onClick=\"return confirm('Deseja deletar esse registro?')\" src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
    if (isset($_GET['pdf'])) {
        $row[] = $id_indicador;
        $grp = array($id_indicador, (is_numeric($id_indicador) ? $id_indicador . " - " . $indicador_cliente : "SEM INDICADOR"), 8);
        if (!in_array($grp, $grupoArray)) {
            $grupoArray[] = $grp;
        }
    }    
    $output['aaData'][] = $row;
}

function statusLead($tipo, $inativo) {
    if ($inativo == 1) {
        return "INATIVO";    
    } else if ($tipo == "P" && $inativo == 0) {
        return "CONV.PROSPECT";
    } else if ($tipo == "C" && $inativo == 0) {
        return "CONV.CLIENTE";                
    } else {
        return "AGUARDANDO";        
    }
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
} else {
    $output['aaGrupo'] = $grupoArray;
}
odbc_close($con);
