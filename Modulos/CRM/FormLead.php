<?php
include '../../Connections/configini.php';
$disabled = 'disabled';

if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_lead set " .
        "razao_social = " . valoresTexto("txtRazaoSocial") . "," .
        "telefone_contato = " . valoresTexto("txtCelular") . "," .
        "email_contato = " . valoresTexto("txtEmail") . "," .
        "indicador_lead = " . valoresSelect("txtIndicadorLead") . "," .
        "indicador_cliente = " . valoresSelect("txtProfResp") . "," .
        "indicador = " . valoresSelect("txtIndicador") . "," .
        "assunto = " . valoresTexto("txtObs") . "," .
        "procedencia = " . valoresSelect("txtProcedencia") . "," .
        "inativo = " . valoresCheck("txtInativo") . "," .
        "empresa = " . valoresSelect("txtFilial") . " " .
        "where id_lead = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_lead(nome_contato, razao_social, telefone_contato, email_contato, 
        indicador_lead, indicador_cliente, indicador, assunto, procedencia, inativo, dt_cadastro, empresa) values (" .        
        valoresTexto("txtRazaoSocial") . "," .
        valoresTexto("txtRazaoSocial") . "," .
        valoresTexto("txtCelular") . "," .
        valoresTexto("txtEmail") . "," .
        valoresSelect("txtIndicadorLead") . "," .
        valoresSelect("txtProfResp") . "," .
        valoresSelect("txtIndicador") . "," .
        valoresTexto("txtObs") . "," .
        valoresSelect("txtProcedencia") . "," .
        valoresCheck("txtInativo") . "," . 
        "getdate()," . 
        valoresSelect("txtFilial") . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_lead from sf_lead order by id_lead desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}

if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_lead WHERE id_lead = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select le.*, ld.razao_social indicador_lead_nome, p.razao_social indicador_nome,
    (select razao_social from sf_fornecedores_despesas as y where y.id_fornecedores_despesas = le.indicador_cliente) prof_atual 
    from sf_lead le left join sf_lead ld on le.indicador_lead = ld.id_lead 
    left join sf_fornecedores_despesas p on le.indicador = p.id_fornecedores_despesas        
    where le.id_lead = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_lead']);
        $razao_social = utf8_encode($RFP['razao_social']);
        $celular = utf8_encode($RFP['telefone_contato']);
        $email = utf8_encode($RFP['email_contato']);
        $indicadorLead = utf8_encode($RFP['indicador_lead']);
        $indicadorLeadNome = utf8_encode($RFP['indicador_lead_nome']);  
        $indicador = utf8_encode($RFP['indicador']);
        $indicadorNome = utf8_encode($RFP['indicador_nome']);  
        $prospect = utf8_encode($RFP['prospect']);
        $Obs = utf8_encode($RFP['assunto']);
        $procedencia = utf8_encode($RFP['procedencia']);
        $inativo = utf8_encode($RFP['inativo']);
        $ckempresa = utf8_encode($RFP['empresa']);
        $profResp = utf8_encode($RFP['indicador_cliente']);
        $profRespNome = utf8_encode($RFP['prof_atual']); 
    }
} else {
    $disabled = '';
    $id = '';
    $razao_social = '';
    $celular = '';
    $email = '';
    $indicadorLead = '';
    $indicadorLeadNome = '';
    $indicador = '';
    $indicadorNome = '';
    $prospect = '';
    $Obs = '';
    $procedencia = 'null';
    $inativo = '0';
    $ckempresa = '';
    $profResp = '';
    $profRespNome = '';
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}

if (isset($_POST['bntConvert'])) {
    if (is_numeric($PegaURL)) {        
        $query = "insert into sf_fornecedores_despesas (empresa, razao_social, inativo, tipo, dt_cadastro, termometro, juridico_tipo, fornecedores_status, dt_aprov_prospect, procedencia, id_user_resp, prof_resp)
        select empresa, razao_social, 0, 'P', getdate(), 2, 'F', 'Aguardando', getdate(), procedencia, (select max(id_usuario) from sf_usuarios where inativo = 0 and funcionario = sf_lead.indicador) usuario, indicador_cliente from sf_lead where id_lead = " . $PegaURL;
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_fornecedores_despesas from sf_fornecedores_despesas order by id_fornecedores_despesas desc") or die(odbc_errormsg());
        $idCliente = odbc_result($res, 1);
        odbc_exec($con, "insert into sf_fornecedores_despesas_contatos (fornecedores_despesas, tipo_contato, conteudo_contato)
        select " . $idCliente . ", 1, telefone_contato from sf_lead where len(telefone_contato) > 0 and id_lead = " . $PegaURL) or die(odbc_errormsg());
        odbc_exec($con, "insert into sf_fornecedores_despesas_contatos (fornecedores_despesas, tipo_contato, conteudo_contato)
        select " . $idCliente . ", 2, email_contato from sf_lead where len(email_contato) > 0 and id_lead = " . $PegaURL) or die(odbc_errormsg());        
        odbc_exec($con, "update sf_lead set prospect = " . $idCliente . ", dt_convert = getdate() where id_lead = " . $PegaURL) or die(odbc_errormsg());                
        //------------------------------------------------------------------------------------------------------------------------------------------------        
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        values ('sf_fornecedores_despesas', " . $PegaURL . ", '" . $_SESSION["login_usuario"] . "', 'C', 'CONVERSAO - PROSPECT', GETDATE(), " . $idCliente . ")");        
        $count = 0;
        $cur = odbc_exec($con, "select id, id_lead, indicador_lead, conv_led_pro from sf_lead 
        inner join sf_lead_campanha on id_procedencia = procedencia
        where getdate() between data_ini and data_fim and sf_lead_campanha.inativo = 0 and sf_lead.inativo = 0 
        and indicador_lead is not null and id_lead = " . $PegaURL);
        while ($RFP = odbc_fetch_array($cur)) {
            $id = utf8_encode($RFP['id']);
            $id_lead = utf8_encode($RFP['id_lead']);            
            $id_indicador = utf8_encode($RFP['indicador_lead']);
            $count = utf8_encode($RFP['conv_led_pro']);
        }
        for ($i = 0; $i < $count; $i++) {
            $cur = odbc_exec($con, "EXEC dbo.SP_LEAD_CUPOM @campanha = " . $id . ", @lead = " . $id_indicador . ", @lead_origem = " . $id_lead . ";");
            $idcupom = odbc_result($cur, 1);
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------                
        echo "<script>alert('Lead convertido para prospect com sucesso!'); parent.FecharBox();</script>";
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<body>
    <form action="FormLead.php" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Lead</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="tabbable frmtabs">
            <ul class="nav nav-tabs" style="margin:0">
                <li class="active"><a href="#tab1" data-toggle="tab">Geral</a></li>
                <li><a href="#tab2" data-toggle="tab">Descrição</a></li>
            </ul>
            <div class="tab-content frmcont" style="height:247px;">
                <div class="tab-pane active" id="tab1"> 
                    <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                    <div style="width: 73%; float:left;">
                        <span>Nome/Razão Social:</span>
                        <input name="txtRazaoSocial" id="txtRazaoSocial" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $razao_social; ?>"/>
                    </div>
                    <div style="width: 26%; float:left; margin-left: 1%;">
                        <span>Celular:</span>
                        <input name="txtCelular" id="txtCelular" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $celular; ?>"/>
                    </div>
                    <div style="width: 54%; float:left;">
                        <span>E-mail:</span>
                        <input name="txtEmail" id="txtEmail" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $email; ?>"/>
                    </div>
                    <div style="width: 45%; float:left; margin-left:1%">
                        <span>Lead Indicador:</span>
                        <input id="txtIndicadorLead" name="txtIndicadorLead" type="hidden"  value="<?php echo $indicadorLead;?>">
                        <input id="txtIndicadorLeadNome" type="text" style="width:100%" value="<?php echo $indicadorLeadNome;?>" <?php echo $disabled; ?>/>
                    </div>
                    <?php
                    $total = 0;
                    $ckempresa = (!is_numeric($ckempresa) ? $filial : $ckempresa);
                    $cur = odbc_exec($con, "select count(id_filial) total from sf_filiais where ativo = 0");
                    while ($RFP = odbc_fetch_array($cur)) {
                        $total = $RFP['total'];
                    }?>
                    <div style="width: <?php echo ($total > 1 ? "54" : "100"); ?>%; float:left;">
                        <span>Usuário Responsável:</span>
                        <input id="txtIndicador" name="txtIndicador" type="hidden"  value="<?php echo $indicador;?>">
                        <input id="txtIndicadorNome" type="text" style="width:100%" value="<?php echo $indicadorNome;?>" <?php echo $disabled; ?>/>
                    </div>            
                    <?php if ($total > 1) { ?>
                        <div style="width:45%;float:right; margin-left: 1%;">
                            <label>Selecione a Loja: </label>          
                            <select class="select" id="txtFilial" name="txtFilial" style="width:100%;float: left;" <?php echo $disabled; ?>>
                                <?php $cur = odbc_exec($con, "select distinct id_filial, numero_filial, Descricao from sf_filiais 
                                inner join sf_usuarios_filiais on id_filial_f = id_filial
                                inner join sf_usuarios on id_usuario_f = id_usuario
                                where ativo = 0 and (id_usuario_f = '" . $_SESSION["id_usuario"] . "' or id_filial = " . valoresNumericos2($ckempresa) . ");");
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo utf8_encode($RFP['id_filial']); ?>"<?php
                                    if (!(strcmp($RFP['id_filial'], $ckempresa))) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP['numero_filial']) . " - " . utf8_encode($RFP['Descricao']); ?></option>
                                <?php } ?>
                            </select>
                        </div>                                                                        
                    <?php } else { ?>
                        <input name="txtFilial" id="txtFilial" type="hidden" value="<?php echo $ckempresa; ?>"/>                                                            
                    <?php } ?>  
                    <div style="width: 100%; float:left;">
                        <span>Indicador:</span>
                        <input type="hidden" id="txtProfResp" name="txtProfResp" value="<?php echo $profResp;?>">      
                        <input type="text" id="txtProfRespNome" name="txtProfRespNome" style="width:100%;" value="<?php echo $profRespNome;?>" <?php echo $disabled; ?>/>
                    </div>                          
                    <div style="width: 69%; float:left;">
                        <span>Procedência:</span>
                        <select class="select" id="txtProcedencia" name="txtProcedencia" <?php echo $disabled; ?> style="width:100%;">
                            <option value="null">Selecione</option>
                            <?php $cur = odbc_exec($con, "select id_procedencia,nome_procedencia from sf_procedencia where tipo_procedencia = 0 order by nome_procedencia") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP['id_procedencia'] ?>"<?php
                                if (!(strcmp($RFP['id_procedencia'], $procedencia))) {
                                    echo "SELECTED";
                                }
                                ?>><?php echo utf8_encode($RFP['nome_procedencia']); ?>
                                </option> 
                            <?php } ?>
                        </select>
                    </div>
                    <div style="width: 30%; float:left; margin-left:1%">
                        <span>Inativo:</span>
                        <select class="select" id="txtInativo" name="txtInativo" <?php echo $disabled; ?> style="width:100%;">
                            <option value="0" <?php
                            if ($inativo == "0") {
                                echo "SELECTED";
                            }
                            ?>>NÃO</option>
                            <option value="1" <?php
                            if ($inativo == "1") {
                                echo "SELECTED";
                            }
                            ?>>SIM</option>
                        </select>
                    </div>
                    <div style="clear:both;height: 5px;"></div>                    
                </div>        
                <div class="tab-pane" id="tab2"> 
                    <div style="width: 100%; float:left;">
                        <textarea id="txtObs" name="txtObs" <?php echo $disabled; ?>>
                            <?php echo $Obs; ?>
                        </textarea>
                    </div>                    
                </div>        
            </div>        
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" name="bntSave" id="bntSave"><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntCancelar"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntAlterar"><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else { 
                    if (!is_numeric($prospect) && $inativo == '0') { ?>
                        <button class="btn btn-primary"type="submit" name="bntConvert" id="bntConvert" value="Converter" style="float:left; margin-top: 15px;" onClick="return confirm('Deseja converter esse lead para prospect?');"><span class="ico-checkmark"> </span> Converter</button>
                    <?php } ?>                    
                    <button class="btn green" type="submit" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntEdit" value="Alterar"> <span class="ico-edit"></span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class="ico-remove"></span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
    <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
    <script type="text/javascript" src="../../js/plugins/other/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/animatedprogressbar/animated_progressbar.js"></script>
    <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../js/plugins.js"></script>
    <script type="text/javascript" src="../../js/charts.js"></script>
    <script type="text/javascript" src="../../js/actions.js"></script>
    <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../js/jquery.priceformat.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>      
    <script type="text/javascript">

        $(document).ready(function () {
            CKEDITOR.replace("txtObs", {removePlugins: "elementspath", height: "180px"});
        });

        $("#txtCelular").mask("(99) 99999-9999");
        
        $("#txtIndicadorLeadNome").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "../../Modulos/CRM/leads.ajax.php",
                    dataType: "json",
                    data: {txtDesc: request.term},
                    success: function (data) {
                        response(data);
                    }
                });
            }, minLength: 3,
            select: function (event, ui) {
                $("#txtIndicadorLead").val(ui.item.id);
                $("#txtIndicadorLeadNome").val(ui.item.value);        
            }
        });        
        
        $("#txtIndicadorNome").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "../../Modulos/CRM/ajax.php",
                    dataType: "json",
                    data: {q: request.term, r: "S", t: 'PFE'},
                    success: function (data) {
                        response(data);
                    }
                });                
            }, minLength: 3,
            select: function (event, ui) {
                $("#txtIndicador").val(ui.item.id);
                $("#txtIndicadorNome").val(ui.item.value);        
            }
        });    
                
        $("#txtProfRespNome").autocomplete({ 
            source: function (request, response) {
                $.ajax({
                    url: "../../Modulos/CRM/ajax.php",
                    dataType: "json",
                    data: {q: request.term, t: "E','I"},
                    success: function (data) {
                        response(data);
                    }
                });
            }, minLength: 3,
            select: function (event, ui) {
                $("#txtProfResp").val(ui.item.id);
                $("#txtProfRespNome").val(ui.item.value);
            }
        });

        $("#txtProfRespNome").blur(function() {
            if ($("#txtProfRespNome").val() === "") {
                $("#txtProfResp").val("");
            }
        });

    </script>
    <?php odbc_close($con); ?>
</body>

