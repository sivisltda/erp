<?php
include '../../Connections/configini.php';
if ($_GET['Del'] != '') {
    odbc_exec($con, "DELETE FROM sf_lead WHERE id_lead = " . $_GET['Del']);
    echo "<script>window.top.location.href = 'Lead.php'; </script>";
}

$totEquipe = 0;
$cur = odbc_exec($con, "select count(sf_usuarios_dependentes.id_usuario) total from sf_usuarios_dependentes 
inner join sf_usuarios on id_fornecedor_despesas = funcionario
where sf_usuarios.id_usuario = " . $_SESSION["id_usuario"]) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $totEquipe = $RFP['total'];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>         
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>CRM<small>Leads</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div style="margin-top: 11px; float:left;">
                                <button class="button button-green btn-primary" type="button" onClick="AbrirBox(0)"><span class="ico-file-4 icon-white"></span></button>
                                <button class="button button-blue btn-primary" type="button" onclick="imprimir()" id="btnPrint" title="Imprimir"><span class="ico-print icon-white"></span></button>
                            </div>
                            <div>
                                <div style="float:left;margin-left: 0.5%; width: 40%;">
                                    <div width="100%">Selecione a Filial:</div>
                                    <select id="txtFilial" name="txtFilial[]" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                        <?php
                                        $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                        while ($RFP = odbc_fetch_array($cur)) {
                                            ?>
                                            <option value="<?php echo utf8_encode($RFP['id_filial']); ?>" selected><?php echo utf8_encode($RFP['numero_filial']) . " - " . utf8_encode($RFP['Descricao']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>                                    
                                <div style="width:8%; float:left;margin-left:0.5%">
                                    <div width="100%">Usuário Resp.:</div>
                                    <select class="select input-medium" style="width:100%" name="txtUserResp" id="txtUserResp" <?php echo ($crm_lea_ure_ > 0 && $totEquipe == 0 ? "disabled" : ""); ?>>
                                        <option value="null">Selecione</option>
                                        <?php
                                        $cur = odbc_exec($con, "select distinct id_usuario, nome from sf_usuarios 
                                            inner join sf_usuarios_filiais on id_usuario_f = id_usuario 
                                            where sf_usuarios.inativo = 0 and id_filial_f in (select id_filial_f from sf_usuarios_filiais where id_usuario_f = '" . $_SESSION["id_usuario"] . "') order by nome") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) {
                                            ?>
                                            <option value="<?php echo $RFP['id_usuario']; ?>" <?php echo ($crm_lea_ure_ > 0 && $RFP['id_usuario'] == $_SESSION["id_usuario"] ? "selected" : ""); ?>><?php echo formatNameCompact(strtoupper(utf8_encode($RFP['nome']))); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>                                    
                                <div style="width:10%; float:left;margin-left:1%">
                                    <div width="100%">Tp.Data:</div>
                                    <select id="txtTpData" class="select" style="width:100%">
                                        <option value="2">CONV. CLI.</option>
                                        <option value="0">CONV. PRO.</option>
                                        <option value="1">CAD. LEAD</option>
                                    </select>
                                </div>                                    
                                <div style="width:8%; float:left;margin-left:1%">
                                    <div width="100%">Data Inicial:</div>
                                    <input type="text" style="width:100%" id="txt_dt_begin" class="datepicker data-mask inputCenter" value="" placeholder="Data inicial">
                                </div>
                                <div style="width:8%; float:left;margin-left:1%">
                                    <div width="100%">Data Final:</div>
                                    <input type="text" style="width:90%" id="txt_dt_end" class="datepicker data-mask inputCenter" value="" placeholder="Data Final">
                                </div>
                                <div style="float:left; width: 12%;">
                                    <div width="100%">Status:</div>                                    
                                    <select class="select input-medium" style="width:100%" name="txtStatus" id="txtStatus">
                                        <option value="null">Selecione</option>
                                        <option value="0">AGUARDANDO</option>
                                        <option value="1">CONV.PROSPECT</option>
                                        <option value="2">CONV.CLIENTE</option>
                                        <option value="3">INATIVO</option>
                                    </select>                                        
                                </div>                                        
                                <div style="float:left; width: 16%; margin-left: 0.5%;">
                                    <div width="100%">Procedência:</div>                                        
                                    <select class="select input-medium" style="width:100%" name="txtProcedencia" id="txtProcedencia">
                                        <option value="null">Selecione</option>
                                        <?php
                                        $cur = odbc_exec($con, "select id_procedencia,nome_procedencia from sf_procedencia where tipo_procedencia = 0 order by nome_procedencia") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) {
                                            ?>
                                            <option value="<?php echo $RFP['id_procedencia'] ?>"><?php echo utf8_encode($RFP['nome_procedencia']) ?></option>
                                        <?php } ?>
                                    </select>                                    
                                </div>                                        
                                <div style="float:left; width: 12%; margin-left: 1%;">
                                    <div width="100%">Busca:</div>
                                    <input name="txtBusca"  id="txtBusca" maxlength="100" type="text" style="width:100%" onkeypress="TeclaKey(event)" value="" title=""/>
                                </div>
                                <div style="float:left; width: 4%; margin-left: 1%; margin-top: 11px; margin-bottom: 5px;">
                                    <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                <div style="clear:both"></div>
                <div class="boxhead">
                    <div class="boxtext">Leads</div>
                </div>
                <div class="boxtable">
                    <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblLead">
                        <thead>
                            <tr>
                                <th width="8%"><center>Data Cad.:</center></th>
                                <th width="15%">Nome/Razão Social</th>
                                <th width="8%">Tel.:</th>
                                <th width="15%">E-mail:</th>
                                <th width="15%">Procedência:</th>
                                <th width="10%">Status</th>                                    
                                <th width="12%">Indicador:</th>
                                <th width="12%">Usuário Resp.:</th>
                                <th width="5%"><center>Ação:</center></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                    <div style="clear:both"></div>
                </div>
            </div>
        </div>
    </div>       
    <div class="dialog" id="source" title="Source"></div>
    <script type="text/javascript" src="../../fancyapps/source/jquery.fancybox.js?v=2.1.4"></script>
    <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>        
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/charts.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
    <script type="text/javascript" src="../../js/util.js"></script>           
    <script type="text/javascript">

        function TeclaKey(event) {
            if (event.keyCode === 13) {
                $("#btnfind").click();
            }
        }

        function finalFind(imp) {
            var retPrint = "";
            retPrint = '?imp=0';
            if (imp === 1) {
                retPrint = '?imp=1';
            }
            if ($('#txtUserResp').val() !== "" && $('#txtUserResp').val() !== "null") {
                retPrint = retPrint + "&ur=" + $('#txtUserResp').val();
            }
            if ($("#txtTpData").val() !== "null") {
                retPrint = retPrint + '&tpData=' + $("#txtTpData").val();
            }
            if ($('#txt_dt_begin').val() !== "") {
                retPrint = retPrint + "&dtb=" + $('#txt_dt_begin').val();
            }
            if ($('#txt_dt_end').val() !== "") {
                retPrint = retPrint + "&dte=" + $('#txt_dt_end').val();
            }
            if ($("#txtStatus").val() !== "null") {
                retPrint = retPrint + '&txtStatus=' + $("#txtStatus").val();
            }
            if ($("#txtProcedencia").val() !== "null") {
                retPrint = retPrint + '&txtProcedencia=' + $("#txtProcedencia").val();
            }
            if ($("#txtBusca").val() !== "") {
                retPrint = retPrint + '&txtBusca=' + $("#txtBusca").val();
            }
            if ($("#txtFilial").val() !== null) {
                retPrint = retPrint + "&fil=" + $("#txtFilial").val();
            }
            return retPrint.replace(/\//g, "_");
        }

        function AbrirBox(id) {
            abrirTelaBox("FormLead.php" + (id > 0 ? "?id=" + id : ""), 432, 470);
        }

        function FecharBox(opc) {
            var oTable = $('#tblLead').dataTable();
            oTable.fnDraw(false);
            $("#newbox").remove();
        }

        function imprimir() {
            var pRel = "&NomeArq=" + "Leads" +
                    "&lbl=Data Cad.|Nome/Razão Social|Tel.|E-mail|Procedência|Status" +
                    "&siz=80|200|80|120|120|100" +
                    "&pdf=8" + // Colunas do server processing que não irão aparecer no pdf
                    "&filter=" + //Label que irá aparecer os parametros de filtro
                    "&PathArqInclude=" + "../Modulos/CRM/Lead_server_processing.php" + finalFind(1).replace("?", "&"); // server processing
            window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
        }

        listaTable();
        $("#btnfind").click(function () {
            $("#tblLead").dataTable().fnDestroy();
            listaTable();
        });

        function listaTable() {
            $(document).ready(function () {
                $('#tblLead').dataTable({
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "aoColumns": [{"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false}],
                    "sAjaxSource": "Lead_server_processing.php" + finalFind(0),
                    'oLanguage': {
                        'oPaginate': {
                            'sFirst': "Primeiro",
                            'sLast': "Último",
                            'sNext': "Próximo",
                            'sPrevious': "Anterior"
                        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                        'sLengthMenu': "Visualização de _MENU_ registros",
                        'sLoadingRecords': "Carregando...",
                        'sProcessing': "Processando...",
                        'sSearch': "Pesquisar:",
                        'sZeroRecords': "Não foi encontrado nenhum resultado"},
                    "sPaginationType": "full_numbers"
                });
            });
        }
    </script>  
    <?php odbc_close($con); ?>
</body>
</html>