<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include '../../Connections/configini.php';
$id_crm = $_REQUEST['codCrm'];
$local = array();
if (is_numeric($id_crm)) {
    $sql = "SELECT t.id,data,de.login_user,de.id_usuario,para.login_user login_userp,para.id_usuario id_usuariop,t.mensagem, status, privado 
    FROM sf_mensagens_telemarketing t INNER JOIN sf_usuarios de ON de.id_usuario = t.id_usuario_de
    LEFT JOIN sf_usuarios para ON para.id_usuario = t.id_usuario_para
    LEFT JOIN sf_whatsapp ON sf_whatsapp.id = id_whatsapp
    WHERE id_tele = " . $id_crm . " order by data desc";
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('id' => utf8_encode($RFP['id']), 'data' => escreverDataHora($RFP['data'])
        , 'login_user' => utf8_encode($RFP['login_user']), 'id_usuario' => utf8_encode($RFP['id_usuario'])
        , 'login_userp' => utf8_encode($RFP['login_userp']), 'id_usuariop' => utf8_encode($RFP['id_usuariop'])
        , 'mensagem' => str_replace("\n", "<br>", utf8_encode($RFP['mensagem']))
        , 'meu_id' => $_SESSION["id_usuario"]
        , 'privado' => utf8_encode($RFP['privado'])                
        , 'status' => (is_numeric($RFP['status']) ? "<span class='label-" . ($RFP['status'] == "1" ? "Ativo" : ($RFP['status'] == "2" ? "Inativo" : 
        ($RFP['status'] == "3" ? "Serasa" : ($RFP['status'] == "4" ? "Ferias" : ($RFP['status'] == "16" ? "Cancelado" : "Suspenso"))))) . "' title='" . 
        ($RFP['status'] == "1" ? "ENVIADO" : ($RFP['status'] == "2" ? "CANCELADO" : ($RFP['status'] == "3" ? "INVÁLIDO" : 
        ($RFP['status'] == "4" ? "DUPLICADO" : "NÃO ENVIADOS")))) . 
        "' style='display: inline-block;width: 13px;height: 13px;margin-right: 5px;border-radius: 50%!important;'></span>" : ""));
    }
}
echo(json_encode($local));
odbc_close($con);
