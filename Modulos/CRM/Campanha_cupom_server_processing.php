<?php

require_once(__DIR__ . '/../../Connections/configini.php');
$aColumns = array('cupom', 'razao_social', 'telefone_contato', 'email_contato');
$colunas = "";
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = " cupom > 0 ";
$sWhere = "";
$sOrder = " ORDER BY cupom asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (is_numeric($_GET['id'])) {
    $sWhereX .= " and id_campanha = " . $_GET['id'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) < count($aColumns)) {
                $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i])], 0) . " " . $_GET['sSortDir_' . $i] . ", ";
                $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i])], 1) . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY " . $aColumns[0] . " asc";
        $sOrder2 = " ORDER BY " . $aColumns[0] . " asc";
    }
}

if ($_GET['txtBusca'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= prepareCollum($aColumns[$i], 0) . " LIKE '%" . utf8_decode($_GET['txtBusca']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($i == 0) {
        $colunas = $aColumns[$i];
    } else {
        $colunas = $colunas . "," . $aColumns[$i];
    }
}

$sQuery = "SELECT COUNT(*) total FROM sf_lead_campanha_cupom inner join sf_lead on sf_lead.id_lead = sf_lead_campanha_cupom.id_lead WHERE $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total FROM sf_lead_campanha_cupom inner join sf_lead on sf_lead.id_lead = sf_lead_campanha_cupom.id_lead WHERE $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row, " . $colunas . ", (select top 1 razao_social from sf_lead where id_lead = id_lead_origem) origem 
FROM sf_lead_campanha_cupom inner join sf_lead on sf_lead.id_lead = sf_lead_campanha_cupom.id_lead WHERE " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " ";
$cur = odbc_exec($con, $sQuery1);
//echo $sQuery1; exit;
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();      
    $row[] = "<div id='formPQ' title='" . str_pad($aRow[$aColumns[0]], 6, "0", STR_PAD_LEFT) . "'>" . str_pad($aRow[$aColumns[0]], 6, "0", STR_PAD_LEFT) . "</div>";    
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[1]]) . "'>" . utf8_encode($aRow[$aColumns[1]]) . "</div>";    
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[2]]) . "'>" . utf8_encode($aRow[$aColumns[2]]) . "</div>";    
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[3]]) . "'>" . utf8_encode($aRow[$aColumns[3]]) . "</div>";    
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["origem"]) . "'>" . utf8_encode($aRow["origem"]) . "</div>";    
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
