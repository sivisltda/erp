<?php
include '../../Connections/configini.php';

if ($_GET['st'] != '') {
    $status = $_GET['st'];
}
if ($_GET['es'] != '') {
    $estado = $_GET['es'];
}
if ($_GET['ci'] != '') {
    $cidade = $_GET['ci'];
}
if ($_GET['Del'] != '') {
    odbc_exec($con, "DELETE FROM sf_mensagens_fornecedores WHERE id_fornecedores_despesas = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_endereco_entrega WHERE fornecedores_despesas = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_fornecedores_despesas WHERE id_fornecedores_despesas = " . $_GET['Del']);
    echo "<script>window.top.location.href = 'Gerenciador-Prospects.php'; </script>";
}

$totEquipe = 0;
$cur = odbc_exec($con, "select count(sf_usuarios_dependentes.id_usuario) total from sf_usuarios_dependentes 
inner join sf_usuarios on id_fornecedor_despesas = funcionario
where sf_usuarios.id_usuario = " . $_SESSION["id_usuario"]) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $totEquipe = $RFP['total'];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>CRM<small>Prospects</small></h1>
                    </div>                  
                    <div class="topcont" style="clear: both;display:flex; margin-bottom: 10px;">
                        <div class="tophead" style="width:calc(20% - 1px); border:0;">
                            <a href="javascript:void(0)" onclick="selectStatus('Lead')">
                            <div class="topname">Lead</div>
                            <div class="topresp">
                                <div id="lbl_lead" class="topnumb">0</div>
                                <div style="clear:both"></div>
                            </div></a>
                        </div>
                        <div class="tophead" style="width:calc(20% - 1px)">
                            <a href="javascript:void(0)" onclick="selectStatus('Aguardando')">
                            <div class="topname">Aguardando</div>
                            <div class="topresp">
                                <div id="lbl_aguardando" class="topnumb">0</div>
                                <div style="clear:both"></div>
                            </div></a>
                        </div>
                        <div class="tophead" style="width:calc(20% - 1px)">
                            <a href="javascript:void(0)" onclick="selectStatus('Negociacao')">
                            <div class="topname">Em Negociação</div>
                            <div class="topresp">
                                <div id="lbl_negociacao" class="topnumb">0</div>
                                <div style="clear:both"></div>
                            </div></a>
                        </div>
                        <div class="tophead" style="width:calc(20% - 1px)">
                            <a href="javascript:void(0)" onclick="selectStatus('Orcamento')">
                            <div class="topname">Em Orçamento</div>
                            <div class="topresp">
                                <div id="lbl_orcamento" class="topnumb">0</div>
                                <div style="clear:both"></div>
                            </div></a>
                        </div>                        
                        <div class="tophead" style="width:calc(20% - 1px)">
                            <a href="javascript:void(0)" onclick="selectStatus('Analise')">
                            <div class="topname">Em Análise</div>
                            <div class="topresp">
                                <div id="lbl_analise" class="topnumb">0</div>
                                <div style="clear:both"></div>
                            </div></a>
                        </div>
                        <div style="clear:both"></div>
                    </div>                    
                    <div class="row-fluid">
                        <div class="span12">                            
                            <div style="margin-top: 11px; float:left;">
                                <?php if ($ckb_adm_cli_read_ == 0) { ?>                                
                                    <button class="button button-green btn-primary" type="button" onClick="AbrirBox(0, 0)"><span class=" ico-file-4 icon-white"></span></button>
                                <?php } ?>                                    
                                    <button name="btnPrint" class="button button-blue btn-primary" onClick="imprimir('I')" type="button" title="Imprimir"><span class="ico-print"></span></button>
                                    <button name="btnExcel" class="button button-blue btn-primary" onClick="imprimir('E')" type="button" title="Exportar Excel"><span class="ico-download-3"></span></button>
                                <?php if ($ckb_adm_cli_read_ == 0) { ?>                                    
                                    <button id="btnEmail" class="button button-turquoise btn-primary" type="button" onclick="AbrirBox(0, 5)"><span class="ico-envelope-3 icon-white"></span></button>
                                    <button type="button" class="button button-blue btn-primary" onclick="AbrirBox(0, 6);"><span class="ico-comment icon-white"></span></button>
                                    <?php if ($mdl_wha_ > 0) { ?>
                                        <button type="button" class="button button-blue btn-primary" onclick="AbrirBox(0, 7);"><span class="ico-phone-4 icon-white"></span></button>
                                    <?php } if ($use_dist_pro_ > 0) { ?> 
                                        <button id="bntRenegociar" title="Distribuir Prospects" type="button" class="button button-red btn-primary" onclick="AbrirBox(0, 8);"><span class="icon-resize-small icon-white"></span></button>
                                <?php }} ?>
                            </div>
                            <div>
                                <div style="float:left;margin-left: 0.5%; width: 40%;">
                                    <label>Selecione a Filial:</label>
                                    <select id="txtFilial" name="txtFilial[]" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                        <?php
                                        $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo utf8_encode($RFP['id_filial']); ?>" selected><?php echo utf8_encode($RFP['numero_filial']) . " - " . utf8_encode($RFP['Descricao']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>                                 
                                <div style="width:8%; float:left;margin-left:0.5%">
                                    <span>Usuário Resp.:</span>
                                    <select class="input-medium" style="width:100%" name="txtUserResp" id="txtUserResp" <?php echo ($crm_pro_ure_ > 0 && $totEquipe == 0 ? "disabled" : ""); ?>>
                                        <option value="null">Selecione</option>
                                        <?php $cur = odbc_exec($con, "select distinct id_usuario, nome from sf_usuarios 
                                        inner join sf_usuarios_filiais on id_usuario_f = id_usuario 
                                        where sf_usuarios.inativo = 0 and id_filial_f in (select id_filial_f from sf_usuarios_filiais where id_usuario_f = '" . $_SESSION["id_usuario"] . "') order by nome") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['id_usuario']; ?>" <?php echo ($crm_pro_ure_ > 0 && $RFP['id_usuario'] == $_SESSION["id_usuario"] ? "selected" : ""); ?>><?php echo formatNameCompact(strtoupper(utf8_encode($RFP['nome']))); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div style="width:8%; float:left;margin-left:0.5%">
                                    <span>Termômetro:</span>                                        
                                    <select class="input-medium" style="width:100%" name="txtTermometro" id="txtTermometro">
                                        <option value="null">Selecione</option>
                                        <option value="0">Muito frio</option>
                                        <option value="1">Frio</option>
                                        <option value="2">Normal</option>
                                        <option value="3">Quente</option>
                                        <option value="4">Muito quente</option>
                                    </select>
                                </div>
                                <div style="width:10%; float:left;margin-left:0.5%">
                                    <span>Status:</span>                                                                                
                                    <select name="txtStatus[]" id="txtStatus" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                        <option value="Lead">CONV. DE LEAD</option>
                                        <option value="Aguardando">AGUARDANDO</option>
                                        <option value="Negociacao">EM NEGOCIAÇÃO</option>
                                        <option value="Orcamento">EM ORÇAMENTO</option>                                        
                                        <option value="Analise" <?php echo (isset($_GET["id"]) ? "selected" : ""); ?>>EM ANÁLISE</option>
                                        <option value="Inativo">INATIVO</option>
                                        <option value="Bloqueado">BLOQUEADO</option>
                                        <option value="Excluido">EXCLUIDO</option>
                                    </select>
                                </div>
                                <div style="width:11%; float:left;margin-left:0.5%">
                                    <span>Procedência:</span>                                                                                
                                    <select class="input-medium" style="width:100%" name="txtProcedencia" id="txtProcedencia">
                                        <option value="null">Selecione</option>
                                        <?php $cur = odbc_exec($con, "select id_procedencia,nome_procedencia from sf_procedencia where tipo_procedencia = 0 order by nome_procedencia") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['id_procedencia'] ?>"<?php
                                            if (!(strcmp($RFP['id_procedencia'], $pr))) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP['nome_procedencia']) ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>                                
                                <div style="width:7%; float:left;margin-left:0.5%">
                                    <span>Grupo:</span>                                                                                
                                    <select name="txtGrupoPessoas" id="txtGrupoPessoas" class="input-medium" style="width:100%">
                                        <option value="null">Selecione</option>
                                        <?php $cur = odbc_exec($con, "select id_grupo,descricao_grupo from dbo.sf_grupo_cliente where tipo_grupo = 'C' and inativo_grupo = 0 order by descricao_grupo") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['id_grupo'] ?>"<?php
                                            if (!(strcmp($RFP['id_grupo'], $grupopes))) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP['descricao_grupo']) ?></option> 
                                        <?php } ?>
                                    </select>
                                </div>      
                                <?php if ($mdl_clb_ > 0) { ?>
                                    <div style="float:left; width:8%; margin-left:0.5%">
                                        <span>Sócio:</span>                                                                                                                
                                        <select name="txtTipo" id="txtTipo" class="input-medium" style="width:100%">
                                            <option value="null">Selecione</option>
                                            <option value="1">Titular</option>
                                            <option value="0">Dependente</option>
                                        </select>
                                    </div>
                                    <div style="float:left; width: 15%; margin-left: 0.5%;">
                                        <div width="100%">Busca Titular:</div>
                                        <input type="hidden" id="txtFonecedor" name="txtFonecedor" value="">
                                        <input type="text" id="txtFonecedornome" name="txtFonecedornome" style="width:99%" value=""/>
                                    </div>                                
                                <?php } ?>                                
                                <div style="float:left; width: 15%; margin-left: 0.5%;">
                                    <div width="100%">Busca:</div>
                                    <input name="txtBusca" id="txtBusca" maxlength="100" type="text" style="width:100%" onkeypress="TeclaKey(event)" value="" title=""/>
                                </div>
                                <div style="float:left; width: 3%; margin-left: 0.5%; margin-top: 12px; margin-bottom: 3px;">
                                    <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                </div>                                    
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead" <?php echo $visible; ?>>
                        <div class="boxtext">Prospects</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="example">
                            <thead>
                                <tr id="formPQ">
                                    <th width="3%"></th>
                                    <th width="10%">Data Cad.:</th>        
                                    <th width="10%">CPF:</th>                                    
                                    <th width="20%">Nome/Razão Social:</th>                                    
                                    <th width="10%">Usuário Resp.:</th>
                                    <th width="7%">Cidade:</th>
                                    <th width="3%"><center>UF:</center></th>
                                    <th width="10%">Tel.:</th>
                                    <th width="14%">E-mail:</th>
                                    <th width="9%">Status:</th>
                                    <th width="4%"><center>Ação:</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>
        <script type="text/javascript">

            function selectStatus(status) {
                $("#txtStatus").val(status);
                $("#btnfind").click();
            }

            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }
            
            $("#txtFonecedornome").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../../Modulos/CRM/ajax.php",
                        dataType: "json",
                        data: {q: request.term, socio: "S", t: "P"},
                        success: function (data) {
                            response(data);
                        }
                    });
                }, minLength: 3,
                select: function (event, ui) {
                    $("#txtFonecedor").val(ui.item.id);
                    $("#txtFonecedornome").val(ui.item.value);
                }
            });            

            function AbrirBox(id, opc) {
                if (opc === 0) {
                    window.location = "ProspectsForm.php" + (id > 0 ? "?id=" + id : "");
                } else if (opc === 4) {
                    window.open('Gerenciador-Prospects.php' + finalFind(1, 'N'), '_blank');
                } else if (opc === 5) {
                    abrirTelaBox("../CRM/EmailsEnvioForm.php", 650, 720);
                } else if (opc === 6) {
                    abrirTelaBox("../CRM/SMSEnvioForm.php", 400, 400);
                } else if (opc === 7) {
                    abrirTelaBox("../CRM/SMSEnvioForm.php?zap=s", 400, 400);
                } else if (opc === 8) {
                    abrirTelaBox("../CRM/DistribuicaoForm.php", 600, 600);                    
                }
            }

            function FecharBox() {
                $("#newbox").remove();
                var oTable = $('#example').dataTable();
                oTable.fnDraw(false);
            }

            var tbProspect = $('#example').dataTable({
                "iDisplayLength": 20,
                "aLengthMenu": [20, 30, 40, 50, 100],
                "bProcessing": true,
                "bServerSide": true,
                "bSort": false,
                "bFilter": false,
                "sAjaxSource": "Gerenciador-Prospects_server_processing.php" + finalFind(0, 'N'),
                "fnDrawCallback": function (data) {
                    $.getJSON("Gerenciador-Prospects_server_processing.php" + finalFind(0, 'N') + "&getEstatisticas=S", function (json) {
                        $.each(json, function (key, val) {
                            $("#lbl_" + key).html(val);
                        });
                    });
                },
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"},
                "sPaginationType": "full_numbers"
            });

            $("#btnfind").click(function () {
                tbProspect.fnReloadAjax('Gerenciador-Prospects_server_processing.php' + finalFind(0, 'N'));
            });           

            function finalFind(ts, isExcel) {
                var retorno = "?sSearch=" + $("#txtBusca").val();
                if (ts === 1) {
                    retorno += '&imp=1';
                } else {
                    retorno += '&imp=0';
                }
                if (isExcel === "E") {
                    retorno += '&isExcel=S';
                }
                if ($('#txtUserResp').val() !== "" && $('#txtUserResp').val() !== "null") {
                    retorno += "&ur=" + $('#txtUserResp').val();
                }
                if ($('#txtProcedencia').val() !== "" && $('#txtProcedencia').val() !== "null") {
                    retorno += "&pr=" + $('#txtProcedencia').val();
                }             
                if ($('#txtStatus').val() !== "" && $('#txtStatus').val() !== null) {
                    retorno += "&st=" + $('#txtStatus').val();
                }
                if ($('#txtGrupoPessoas').val() !== "" && $('#txtGrupoPessoas').val() !== "null") {
                    retorno += "&gp=" + $('#txtGrupoPessoas').val();
                }
                if ($('#txtTermometro').val() !== "" && $('#txtTermometro').val() !== "null") {
                    retorno += "&tp=" + $('#txtTermometro').val();
                }
                if ($('#txtFilial').val() !== null) {
                    retorno += "&filial=" + $('#txtFilial').val();
                }                
                if ($('#txtFonecedornome').val() !== "" && $('#txtFonecedor').val() !== "") {
                    retorno += "&socio=" + $('#txtFonecedor').val();
                }                
                if ($('#txtTipo').val() !== "null") {
                    retorno += "&tipoS=" + $('#txtTipo').val();
                }                                    
                return retorno.replace(/\//g, "_");
            }

            function listaEmails() {
                var emails = [];
                $.ajax({
                    url: "Gerenciador-Prospects_server_processing.php" + finalFind(1, 'N') + "&emailMarketing=1",
                    async: false,
                    dataType: "json",
                    success: function (j) {
                        for (var x in j.aaData) {
                            if (j.aaData[x][8].trim() !== "") {
                                emails.push(j.aaData[x][8].replace(/ /g, "") + "|" + j.aaData[x][11]);
                            }
                        }
                    }
                });
                return emails;
            }

            function listaSMS() {
                var sms = [];
                $.ajax({
                    url: "Gerenciador-Prospects_server_processing.php" + finalFind(1, 'N') + "&envioSms=1",
                    async: false,
                    dataType: "json",
                    success: function (j) {
                        for (var x in j.aaData) {
                            if (j.aaData[x][7].trim() !== "") {
                                sms.push(j.aaData[x][7].replace(/ /g, "") + "|" + j.aaData[x][11]);
                            }
                        }
                    }
                });
                return sms;
            }

            function imprimir(tp) {
                var pRel = "&NomeArq=" + "Prospects" +
                        "&lbl=" + "Data Cad.|CPF|Nome/Razão Social|Usuário Resp.|Cidade|UF|Tel|Email|Status" + (tp === 'E' ? "|Observações|Contato (Emergência)|Telefone (Emergência)" : "") +
                        "&siz=" + "75|80|135|70|70|20|70|100|80" + (tp === 'E' ? "|100|100|100" : "") +
                        "&pdf=" + "0|10" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + "Prospects " + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/CRM/Gerenciador-Prospects_server_processing.php" + finalFind(1, tp).replace("?", "&"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tp + "&PathArq=GenericModelPDF.php", '_blank');
            }
            
            function reativarCli(idCli) {
                bootbox.confirm('Confirma a reativação?', function (result) {
                    if (result === true) {
                        $.post("../../Modulos/Academia/form/ClientesFormServer.php", "isAjax=S&reativarCadastro=S&idCli=" + idCli).done(function (data) {
                            if (!isNaN(data.trim())) {
                                bootbox.alert("Reativado com sucesso!");
                                $("#btnfind").trigger("click");
                            } else {
                                alert(data);
                            }
                        });
                    } else {
                        return;
                    }
                });
            }
        </script>
        <?php odbc_close($con); ?>
    </body>
</html>
