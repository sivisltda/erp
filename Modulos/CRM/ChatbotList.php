<?php include "../../Connections/configini.php"; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>        
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <link href="../../js/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css"/>        
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <style media="screen">
            .li-orange {
                color: orange;
            }
            .li-blue {
                color: darkturquoise;                
            }
            .li-green {
                color: green;                
            }
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>CRM<small>Chatbot</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="margin-top: 15px;float:left;">
                                    <div style="float:left">
                                        <form method="POST">
                                            <button  class="button button-green btn-primary" type="button" onClick="AbrirBox(0);"><span class="ico-file-4 icon-white"></span></button>
                                        </form>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Chatbot</div>
                    </div>
                    <div class="boxtable">
                        <div id="histPag"></div>
                        <div style="clear:both; height:2px"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type='text/javascript' src="../../js/plugins/jstree/dist/jstree.min.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>     
        <script type="text/javascript">
            
            refresh();
            
            function AddBox(id, tipo) {
                abrirTelaBox("ChatbotForm.php?id_pai=" + id + "&tipo=" + tipo, 650, 1130);
            }
            
            function AbrirBox(id) {
                abrirTelaBox("ChatbotForm.php" + (id > 0 ? "?id=" + id : ""), 650, 1130);
            }

            function FecharBox(opc) {
                $("#newbox").remove();
                refresh();
            }
            
            function refresh() {
                $.post("form/ChatbotFormServer.php", "listChatbot=S").done(function (data) {
                    $("#histPag").html(data.trim()); 
                    $("#histPag").removeClass("jstree jstree-1 jstree-default");
                    $("#histPag").bind("loaded.jstree", function(event, data) { 
                        data.instance.open_all();
                    });
                    $("#histPag").jstree();                    
                });
            }
            
            function excluir(id) {
                bootbox.confirm('Confirma a exclusão do item?', function (result) {
                    if (result === true) {
                        $.post("form/ChatbotFormServer.php", "btnDelete=S&List=S&txtId=" + id).done(function (data) {
                            if (data.trim() === "YES") {
                                refresh();
                            } else {
                                bootbox.alert("Erro ao excluir!");
                            }
                        });
                    } else {
                        return;
                    }
                });
            }
            
        </script>
        <?php odbc_close($con); ?>
    </body>
</html>
