<?php

require_once(__DIR__ . '/../../Connections/configini.php');
$aColumns = array('id', 'titulo', 'data_ini', 'data_fim', 'nome_procedencia', 'inativo');
$colunas = "";
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = " id > 0 ";
$sWhere = "";
$sOrder = " ORDER BY id asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (is_numeric($_GET['txtProcedencia'])) {
    $sWhereX .= " and sf_lead_campanha.id_procedencia = " . $_GET['txtProcedencia'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) < count($aColumns)) {
                $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i])], 0) . " " . $_GET['sSortDir_' . $i] . ", ";
                $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i])], 1) . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY " . $aColumns[0] . " asc";
        $sOrder2 = " ORDER BY " . $aColumns[0] . " asc";
    }
}

if ($_GET['txtBusca'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= prepareCollum($aColumns[$i], 0) . " LIKE '%" . utf8_decode($_GET['txtBusca']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($i == 0) {
        $colunas = $aColumns[$i];
    } else {
        $colunas = $colunas . "," . $aColumns[$i];
    }
}

$sQuery = "SELECT COUNT(*) total FROM sf_lead_campanha left join sf_procedencia on sf_lead_campanha.id_procedencia = sf_procedencia.id_procedencia WHERE $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total FROM sf_lead_campanha left join sf_procedencia on sf_lead_campanha.id_procedencia = sf_procedencia.id_procedencia WHERE $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row, " . $colunas . "
FROM sf_lead_campanha left join sf_procedencia on sf_lead_campanha.id_procedencia = sf_procedencia.id_procedencia WHERE " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " ";
$cur = odbc_exec($con, $sQuery1);
//echo $sQuery1; exit;
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();    
    $status = $aRow[$aColumns[5]] == "1" ? "SIM" : "NÂO";    
    $row[] = "<center><div id='formPQ' title='" . escreverData($aRow[$aColumns[2]]) . "'>" . escreverData($aRow[$aColumns[2]]) . "</div></center>";    
    $row[] = "<center><div id='formPQ' title='" . escreverData($aRow[$aColumns[3]]) . "'>" . escreverData($aRow[$aColumns[3]]) . "</div></center>";    
    $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[$aColumns[0]]) . ");'><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[1]]) . "'>" . utf8_encode($aRow[$aColumns[1]]) . "</div></a>";    
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[4]]) . "'>" . utf8_encode($aRow[$aColumns[4]]) . "</div>";    
    $row[] = "<center><div id='formPQ' title='" . $status . "'>" . $status . "</div></center>";    
    $row[] = "<center><a title='Excluir' href='Lead-Campanha.php?Del=" . $aRow[$aColumns[0]] . "'><input name='' type='image' onClick=\"return confirm('Deseja deletar esse registro?')\" src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
