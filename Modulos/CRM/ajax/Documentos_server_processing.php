<?php

require_once('../../../Connections/configini.php');

$total = 0;
$sLimit = 0;
$sQtd = 20;

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $total,
    "iTotalDisplayRecords" => $total,
    "aaData" => array()
);

function sortFunction($a, $b) {
    return $b[4] - $a[4];
}

if (strlen($_GET['id']) > 0) {
    $pasta = "./../../../Pessoas/" . $contrato . "/" . (isset($_GET['type']) ? $_GET['type'] : "Documentos") . "/" . ((strlen($_GET["id"]) > 0 && $_GET["id"] != "0") ? $_GET["id"] . "/" : "");
    if (file_exists($pasta)) {
        $cdir = scandir($pasta);
        foreach ($cdir as $key => $value) {
            if (!in_array($value, array(".", ".."))) {
                $row = array();
                $row[] = date($lang['dmy'] . " H:i:s", filectime($pasta . $value));
                $row[] = "<a href=\"" . $pasta . $value . "\" target=\"_blank\">" . $value . "</a>";  
                if ($_GET['type'] != "Sinistros") {
                    $row[] = filesize($pasta . $value) . " b";
                }
                if($ckb_aca_documento_ == 1) {
                    $row[] = "<center><input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\"
                    onclick=\"Remover" . ($_GET['type'] == "Cotacao" ? "Link" : "Doc") . "('" . utf8_encode($value) . "')\" /></center>";
                } else {
                    $row[] = "";
                }  
                $row[] = filectime($pasta . $value);
                $output['aaData'][] = $row;
                $total++;
            }   
        }
        usort($output['aaData'], "sortFunction");
    }
}
$output['iTotalRecords'] = $total;
$output['iTotalDisplayRecords'] = $total;

echo json_encode($output);
odbc_close($con);