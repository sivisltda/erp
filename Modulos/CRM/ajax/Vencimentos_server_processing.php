<?php

require_once('../../../Connections/configini.php');
require_once('../../../util/util.php');

$aColumns = array('id', 'dia', 'de', 'ate');
$iTotal = 0;
$iFilteredTotal = 0;
$sOrder = " ORDER BY " . $aColumns[0] . " asc ";
$sOrder2 = " ORDER BY " . $aColumns[0] . " asc ";
$sLimit = 20;

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

$sQuery = "SELECT COUNT(*) total from sf_configuracao_vencimentos";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
    $iFilteredTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,* from sf_configuracao_vencimentos) as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " " . $sOrder2;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<center>" . utf8_encode($aRow[$aColumns[1]]) . "</center>";
    $row[] = "<center>" . utf8_encode($aRow[$aColumns[2]]) . "</center>";
    $row[] = "<center>" . utf8_encode($aRow[$aColumns[3]]) . "</center>";
    $row[] = "<center>" . ($aRow["minimo"] == "1" ? "S" : "N") . "</center>";
    $row[] = "<center>" . ($aRow["pro_rata"] == "1" ? "S" : "N") . "</center>";
    $row[] = "<center><input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\" onclick=\"RemoverVencimentos('" . $aRow[$aColumns[0]] . "')\" /></center>";
    $output['aaData'][] = $row;
}

echo json_encode($output);
odbc_close($con);