<?php

require_once('../../../Connections/configini.php');
require_once('../../../util/util.php');

$aColumns = array('id_email', 'descricao', 'assunto', 'tipo');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY " . $aColumns[0] . " asc ";
$sOrder2 = " ORDER BY " . $aColumns[0] . " asc ";
$sLimit = 20;

function getTipo($tipo) {
    if ($tipo == "0") {
        return "E-mail";
    } else if ($tipo == "1") {
        return "Sms";
    } else if ($tipo == "2") {
        return "Whatsapp";        
    }
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_0']) + 1], 0) . " " . $_GET['sSortDir_' . $i] . ", ";
            $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_0']) + 1], 1) . " " . $_GET['sSortDir_' . $i] . ", ";
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY " . $aColumns[0] . " asc";
        $sOrder2 = " ORDER BY " . $aColumns[0] . " asc";
    }
}

if ($_GET['Search'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= prepareCollum($aColumns[$i], 0) . " LIKE '%" . utf8_decode($_GET['Search']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

if (is_numeric($_GET['tp'])) {
    $sWhere .= " and p.tipo = " . $_GET['tp'];
}

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,*
            from sf_emails p where p." . $aColumns[0] . " > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . $sOrder2;

$cur = odbc_exec($con, $sQuery1);

$sQuery = 'SELECT COUNT(*) total from sf_emails p where p.' . $aColumns[0] . " > 0 $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total from sf_emails p where p." . $aColumns[0] . " > 0 $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[$aColumns[0]]) . ",1)'><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[1]]) . "'>" . utf8_encode($aRow[$aColumns[1]]) . "</div></a>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[2]]) . "'>" . utf8_encode($aRow[$aColumns[2]]) . "</div>";
    $row[] = "<div id='formPQ' title='" . getTipo($aRow[$aColumns[3]]) . "'>" . getTipo($aRow[$aColumns[3]]) . "</div>";
    $row[] = "<div style='text-align: center;'><input name='' type='image' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' onClick=\"RemoverItem(" . $aRow[$aColumns[0]] . ")\" value='Enviar'></div>";
    $output['aaData'][] = $row;
}

echo json_encode($output);
odbc_close($con);
