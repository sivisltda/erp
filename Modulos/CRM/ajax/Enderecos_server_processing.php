<?php

require_once('../../../Connections/configini.php');
require_once('../../../util/util.php');

$aColumns = array('id_endereco', 'nome_end_entrega', 'end_entrega');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = " and fornecedores_despesas = " . (is_numeric($_GET['id']) ? $_GET['id'] : "0") . " ";
$sWhere = "";
$sOrder = " ORDER BY " . $aColumns[0] . " asc ";
$sOrder2 = " ORDER BY " . $aColumns[0] . " asc ";
$sLimit = 20;

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) < 2) {
                $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 0) . " " . $_GET['sSortDir_' . $i] . ", ";
                $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 1) . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY " . $aColumns[0] . " asc";
        $sOrder2 = " ORDER BY " . $aColumns[0] . " asc";
    }
}

if ($_GET['Search'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= prepareCollum($aColumns[$i], 0) . " LIKE '%" . utf8_decode(str_replace("'", "", $_GET['Search'])) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,*
            from sf_fornecedores_despesas_endereco_entrega where " . $aColumns[0] . " > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " " . $sOrder2;
$cur = odbc_exec($con, $sQuery1);

$sQuery = "SELECT COUNT(*) total from sf_fornecedores_despesas_endereco_entrega where " . $aColumns[0] . " > 0 $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total from sf_fornecedores_despesas_endereco_entrega where " . $aColumns[0] . " > 0 $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<a href='javascript:void(0)' onClick='editEndereco(" . utf8_encode($aRow[$aColumns[0]]) . ",1)'><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[1]]) . "'>" . utf8_encode($aRow[$aColumns[1]]) . "</div></a>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[2]]) . "'>" . utf8_encode($aRow[$aColumns[2]]) . "</div>";
    $row[] = "<center><input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\" onclick=\"delEndereco('" . $aRow[$aColumns[0]] . "')\" /></center>";
    $output['aaData'][] = $row;
}

echo json_encode($output);
odbc_close($con);
