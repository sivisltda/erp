<?php
include '../../Connections/configini.php';
include "form/FormLead-Campanha.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>        
    </head>
    <body>
        <form name="frmEnviaDados" id="frmEnviaDados" method="POST" onkeydown="if (event.ctrlKey && event.keyCode === 86) { return false; }" enctype="multipart/form-data">
            <div class="frmhead">
                <div class="frmtext">Captação de Lead</div>
                <div class="frmicon" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input name="txtId" id="txtId" type="hidden" value="<?php echo $id; ?>"/>                
                <div style="width: 30%; float: left;">
                    <label for="txtTitulo">Título:</label>                    
                    <input name="txtTitulo" id="txtTitulo" type="text" style="width:100%" maxlength="500" <?php echo $disabled; ?> value="<?php echo $titulo; ?>"/>
                </div>
                <div style="width:10%; float:left; margin-left:1%">
                    <span>Data Início:</span>
                    <input id="txtDataInicio" name="txtDataInicio" class="datepicker inputCenter" type="text" value="<?php echo $dt_inicio; ?>" <?php echo $disabled; ?>>
                </div>                
                <div style="width:10%; float:left; margin-left:1%">
                    <span>Data Fim:</span>
                    <input id="txtDataFim" name="txtDataFim" class="datepicker inputCenter" type="text" value="<?php echo $dt_fim; ?>" <?php echo $disabled; ?>>
                </div>   
                <div style="width: 25%; float:left; margin-left:1%">
                    <span>Procedência:</span>
                    <select class="select" id="txtProcedencia" name="txtProcedencia" style="width:100%;" <?php echo $disabled; ?>>
                        <option value="">Selecione</option>
                        <?php $cur = odbc_exec($con, "select id_procedencia,nome_procedencia from sf_procedencia where tipo_procedencia = 0 order by nome_procedencia") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option value="<?php echo $RFP['id_procedencia'] ?>"<?php
                            if (!(strcmp($RFP['id_procedencia'], $procedencia))) {
                                echo "SELECTED";
                            }
                            ?>><?php echo utf8_encode($RFP['nome_procedencia']); ?>
                            </option> 
                        <?php } ?>
                    </select>
                </div>                
                <div style="width:10%; float:left; margin-left:1%">
                    <span>Min Range:</span>
                    <input id="txtMinRange" name="txtMinRange" class="inputCenter" type="text" value="<?php echo $min_range; ?>" <?php echo $disabled; ?>>
                </div>
                <div style="width:10%; float:left; margin-left:1%">
                    <span>Max Range:</span>
                    <input id="txtMaxRange" name="txtMaxRange" class="inputCenter" type="text" value="<?php echo $max_range; ?>" <?php echo $disabled; ?>>
                </div>
                <div style="width:10%; float:left;">
                    <span>Conv. Prospect:</span>
                    <input id="txtConvProspect" name="txtConvProspect" class="inputCenter" type="text" value="<?php echo $conv_prospect; ?>" <?php echo $disabled; ?>>
                </div>
                <div style="width:10%; float:left; margin-left:1%">
                    <span>Conv. Cliente:</span>
                    <input id="txtConvCliente" name="txtConvCliente" class="inputCenter" type="text" value="<?php echo $conv_cliente; ?>" <?php echo $disabled; ?>>
                </div>                
                <div style="width: 8%; float:left; margin-left:1%">
                    <span>Inativo:</span>
                    <select class="select" style="width:100%;" id="txtInativo" name="txtInativo" <?php echo $disabled; ?>>
                        <option value="0" <?php
                        if ($inativo == "0") {
                            echo "SELECTED";
                        }
                        ?>>NÃO</option>
                        <option value="1" <?php
                        if ($inativo == "1") {
                            echo "SELECTED";
                        }
                        ?>>SIM</option>
                    </select>
                </div>
                <div style="float: left; width: 25%; margin-left: 1%;">
                    <label id="lblarquivo" for="arquivo">Imagem Publicidade:</label>
                    <?php if ($image == "") { ?>
                        <input class="btn btn-primary" style="height:20px; line-height:21px;width: 100%;" type="file" name="arquivo" id="arquivo" <?php echo $disabled; ?> />
                    <?php } else { ?>
                        <button class="btn btn-primary" type="button" title="Visualizar" onclick="verImg('<?php echo $image; ?>');" style="width: 100px;"> Visualizar </button>
                        <button class="btn btn-danger" type="button" title="Excluir" onclick="excluirImg('<?php echo $image; ?>');" style="width: 100px;"> Excluir </button>
                    <?php } ?>
                </div>
                <div style="float: right; text-align: right; width: 10%; margin-top: 13px;">
                    <?php if (is_numeric($id) && $disabled != '') { ?>
                    <button  class="button button-green btn-primary" type="button" onClick="AbrirBox()" title="Abrir Link"><span class="ico-file-4 icon-white"></span></button>
                    <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir()" title="Imprimir Cupons"><span class="ico-print icon-white"></span></button>
                    <?php } ?>
                </div>                    
                <div style="clear:both;"></div>                
                <div style="width:100%;">
                    <hr style="margin:2px 0; border-top:1px solid #DDD">
                </div>
                <div class="data-fluid" id="ckTp1">
                    <label for="ckeditor"></label>
                    <textarea id="ckeditor" name="ckeditor" style="height: 403px;" onkeydown="if (event.ctrlKey && event.keyCode === 86) { return false; }" <?php echo $disabled; ?>><?php echo $mensagem; ?></textarea>
                </div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == '') { ?>
                        <button class="btn btn-success" type="submit" name="btnSave" title="Gravar" id="btnSave" onclick="return validaForm();" ><span class="ico-checkmark"></span> Gravar</button>
                        <?php if ($_POST['txtId'] == '') { ?>
                            <button class="btn yellow" title="Cancelar" onClick="parent.FecharBox()" id="btnCancelar"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                            <button class="btn yellow" title="Cancelar" type="submit" name="btnCancelar" id="btnCancelar" ><span class="ico-reply"></span> Cancelar</button>
                        <?php }
                    } else { ?>
                        <button class="btn  btn-success" type="submit" title="Novo" name="btnNew" id="btnNew" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                        <button class="btn  btn-success" type="submit" title="Alterar" name="btnEdit" id="btnEdit" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                        <div class="btn red" style="background-color:#68AF27; margin-right:1%" onClick="bootbox.confirm('Confirma a exclusão do registro?', function (result) { if (result) { $('#btnDelete').click(); } });">
                            <div class="icon"><span class=" ico-remove"></span>&nbsp;Excluir</div>
                        </div>
                        <input id="btnDelete" name="btnDelete" type="submit" style="display:none"/>
                    <?php } ?>
                </div>
            </div>
        </form>
        <script type='text/javascript' src='../../js/plugins/jquery-ui-1.11.4/jquery-ui.min.js'></script>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/ckeditorAll/ckeditor.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type='text/javascript'>

            $(document).ready(function () {
                CKEDITOR.replace('ckeditor', {height: 332, width: 1086});
            });
            
            function AbrirBox() {
                window.open("https://portal.sivisweb.com.br/loja/" + parent.$("#txtMnContrato").val() + "/quem-indica/" + $("#txtId").val(), '_blank');                 
            }
            
            function imprimir() {
                var pRel = "&NomeArq=" + "Cupons Emitidos" +
                        "&lbl=Cupom|Nome|Telefone|E-mail|Origem" +
                        "&siz=100|200|100|100|200" +
                        "&pdf=6" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + $("#txtTitulo").val() + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/CRM/Campanha_cupom_server_processing.php&imp=1&id=" + $("#txtId").val();
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');                
            }

            function validaForm() {
                if ($("#txtDescricao").val() === "") {
                    bootbox.alert("Descrição é Obrigatória!");
                    return false;
                }                
                return true;
            }
            
            function verImg(img) {
                window.open("./../../Pessoas/" + parent.$("#txtMnContrato").val() + "/Campanha/" + $("#txtId").val() + "/" + img, '_blank'); 
            }
            
            function excluirImg(img) {
                bootbox.confirm('Confirma a exclusão desta imagem?', function (result) {
                    if (result === true) {
                        $.post("form/FormLead-Campanha.php", "DelImage=" + img + "&List=S&txtId=" + $("#txtId").val()).done(function (data) {
                            location.reload();
                        });                        
                    }
                });
            }
            
        </script>        
        <?php odbc_close($con); ?>
    </body>
</html>