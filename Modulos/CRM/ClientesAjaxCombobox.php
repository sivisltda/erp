<?php

include '../../Connections/configini.php';

$page_num = $_GET['page_num'];
$db_table = $_GET['db_table'];
$page_num = $_GET['page_num'];
$per_page = $_GET['per_page'];
$and_or = $_GET['and_or'];
$order_by = $_GET['order_by'];
$search_field = $_GET['search_field'];
$q_word = $_GET['q_word'];
$descr = "";
$fCod = "";
$row_set = array();

for ($i = 0; $i < sizeof($q_word); $i++) {
    $descr .= $descr !== "" ? " " . $q_word[$i] : $q_word[$i];
}
$sInicioPag = ($page_num - 1) * $per_page;
if(is_numeric($descr)) {
    $fCod = "id_fornecedores_despesas = '" . str_replace("'", "", utf8_decode($descr)) . "' or ";    
}
$cur2 = odbc_exec($con, "select id_campo from sf_configuracao_campos_livres where busca_campo = 1");
while ($RFP = odbc_fetch_array($cur2)) {
    $fCod .= " id_fornecedores_despesas in (select fornecedores_campos from sf_fornecedores_despesas_campos_livres where campos_campos = " . $RFP['id_campo'] ." and conteudo_campo like '%" . str_replace("'", "", utf8_decode($descr)) . "%') or ";
}    
$sQuery = "SELECT COUNT(*) total from sf_fornecedores_despesas where id_fornecedores_despesas > 0 and inativo = 0 and tipo in ('C','P')
and (" . $fCod ." razao_social like '%" . str_replace("'", "", utf8_decode($descr)) . "%' or nome_fantasia like '%" . str_replace("'", "", utf8_decode($descr)) . "%')";
//echo $sQuery; exit;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $qtdTotal = $RFP['total'];
}

$sql = "SELECT * FROM(SELECT ROW_NUMBER() OVER (order by razao_social asc) as row, id_fornecedores_despesas,razao_social,nome_fantasia, tipo
        from sf_fornecedores_despesas
        where id_fornecedores_despesas > 0 and inativo = 0 and tipo in ('C','P')
        and (" . $fCod ." razao_social like '%" . str_replace("'", "", utf8_decode($descr)) . "%' or nome_fantasia like '%" . str_replace("'", "", utf8_decode($descr)) . "%')
        ) as a WHERE a.row > " . $sInicioPag . " and a.row <= " . ($sInicioPag + $per_page);
//echo $sql; exit;
$cur = odbc_exec($con, $sql) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $descricao = utf8_encode($RFP['razao_social']);
    if ($RFP['nome_fantasia'] != "") {
        $descricao = $descricao . " (" . utf8_encode($RFP['nome_fantasia']) . ")";
    }
    $row['id'] = $RFP['id_fornecedores_despesas'];
    $row['name'] = $descricao . ($RFP['tipo'] == "P" ? " (Prospects)" : "");
    $row_set[] = $row;
}

$output = array(
    "cnt_whole" => $qtdTotal,
    "result" => array()
);

$output['result'] = $row_set;
echo json_encode($output);
odbc_close($con);
