<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include '../../Connections/configini.php';

$local = array();

if (is_numeric($_REQUEST['txtTipo'])) {
    $sql = "select id, descricao from sf_contratos where inativo = 0 and tipo = " . $_REQUEST['txtTipo'] . " ORDER BY descricao";
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('id' => $RFP['id'], 'descricao' => utf8_encode($RFP['descricao']));
    }    
}

echo(json_encode($local));
odbc_close($con);
