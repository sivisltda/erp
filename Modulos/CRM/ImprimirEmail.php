<?php
include "../../Connections/configini.php";

$id = "0";
$documento = "0";
$tipo = "0";
$altura = 126;
$impressora = 0;
$mdl_wha_ = returnPart($_SESSION["modulos"],16);

if (is_numeric($_GET["id"])) {
    $id = $_GET["id"];
}

if (is_numeric($_GET["doc"])) {
    $documento = $_GET["doc"];
}

if (is_numeric($_GET["minHeight"])) {
    $altura = $_GET["minHeight"];
}

if (is_numeric($_GET["tp"])) {
    $tipo = $_GET["tp"];
    if ($tipo == "0") {
        $nome = "Contrato";
    } elseif ($tipo == "1") {
        $nome = "Rescisão de Contrato";
    } elseif ($tipo == "2") {
        $nome = "Recibo";
        $cur = odbc_exec($con, "select * from sf_configuracao");
        while ($RFP = odbc_fetch_array($cur)) {
            $impressora = utf8_encode($RFP['ACA_BEMATECH']);
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <link href="../../js/plugins/jquery-ui-1.11.4/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>
        <form name="frmEnviaDados" id="frmEnviaDados" method="POST">
            <div class="frmhead">
                <div class="frmtext"><?php echo $nome; ?></div>
                <div class="frmicon" style="top:12px" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont" style="min-height: <?php echo $altura; ?>px;">
                <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                <input name="txtDoc" id="txtDoc" value="<?php echo $documento; ?>" type="hidden"/>                
                <input name="txtTp" id="txtTp" value="<?php echo $tipo; ?>" type="hidden"/>
                <?php if ($tipo == "0" || $tipo == "1") { ?>
                    <select id="txtContrato" name="txtContrato" class="select" style="width:99.5%">
                        <?php $cur = odbc_exec($con, "select cnt.id, cnt.descricao, cnt.logo, cnt.margem_d 
                        from sf_vendas_planos vp left join sf_produtos p on vp.id_prod_plano = p.conta_produto
                        inner join sf_contratos_grupos cg on cg.id_grupo = p.conta_movimento
                        inner join sf_contratos cnt on cg.id_contrato = cnt.id
                        inner join sf_contratos_filiais cnf on cnf.id_contrato = cnt.id
                        inner join sf_fornecedores_despesas f on vp.favorecido = id_fornecedores_despesas                        
                        where cnt.inativo = 0 and cnf.id_filial = f.empresa
                        and cnt.id not in (select isnull(portal_contrato, 0) from sf_configuracao)
                        and (vp.dt_cancelamento is null and cnt.tipo = 0 or vp.dt_cancelamento is not null and cnt.tipo = 1)
                        and id_plano = " . $documento) or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option value="<?php echo $RFP['id']; ?>" logo="<?php echo $RFP['logo']; ?>" mdi="<?php echo $RFP['margem_d']; ?>"><?php echo utf8_encode($RFP['descricao']); ?></option> 
                        <?php } ?>
                    </select>
                <?php } ?>                
                <input id="txtNovo" style="width: 88%;" type="text" value=""/>
                <button type="button" class="btn dblue" onclick="novo_email()"><span class="icon-plus icon-white"></span></button>
                <div id="lstLista" style="max-height: 90px;overflow-y: scroll;margin-top: 8px;background-color: white;"></div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($tipo == "0") { ?>
                    <button id="btnAssinar" class="button button-orange btn-primary" style="float: left; margin-top: 14px;" type="button" onclick="Assinar();" title="Assinar"><span class="ico-pen icon-white"></span></button>
                    <button id="btnAssinarWhatsApp" class="button button-orange btn-primary" style="float: left; margin-top: 14px;" type="button" onclick="AssinarWhatsApp();" title="Assinar por WhatsApp"><span class="ico-phone-4 icon-white"></span></button>
                    <?php } ?>                    
                    <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="Print();" title="Imprimir"><span class="ico-print icon-white"></span></button>
                    <?php if ($mdl_wha_ > 0 && $tipo == 2) { ?>
                    <button id="btnPrint" class="button button-green btn-primary" type="button" onclick="EnviarWhatsApp();" title="WhatsApp"><span class="ico-phone-4 icon-white"></span></button>
                    <?php } ?>                    
                    <button id="btnEmail" class="button button-turquoise btn-primary" type="button" onclick="enviar_email();" title="E-mail"><span class="ico-envelope-3 icon-white"></span></button>
                </div>
            </div>
        </form>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery-ui-1.11.4/jquery-ui.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/plugins/ckeditor/ckeditor.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>
        <script type="text/javascript">
            
            function Assinar() {
                window.open("../../Modulos/Seguro/FormAssinatura.php?id=" + $("#txtDoc").val() + "&idContrato=" + 
                $("#txtContrato").val() + "&crt=" + parent.$("#txtMnContrato").val() + 
                "&idCliente=" + parent.$("#txtId").val(), '_blank');
                parent.$("#loader").hide();                
            }
            
            function AssinarWhatsApp() {
                enviar_sms("Olá Sr. *" + parent.$("#txtNome").val() + "* \nesse é um link para assinatura digital do seu contrato :\n" + 
                "https://<?php echo $_SERVER['HTTP_HOST']; ?>/Modulos/Seguro/FormAssinatura.php?id=" + $("#txtDoc").val() + "&crt=" + parent.$("#txtMnContrato").val() + 
                "&idContrato=" + $("#txtContrato").val() + 
                "&idCliente=" + parent.$("#txtId").val() + "\nAtt. " + parent.$("#BtnLoja .headerText div").next().html());                
            }
            
            function EnviarWhatsApp() {
                enviar_sms("Segue o link do recibo:\nhttps://<?php echo $_SERVER['HTTP_HOST']; ?>/Modulos/Academia/cupom_plano.php?id=" + 
                $("#txtDoc").val() + "&crt=" + parent.$("#txtMnContrato").val());
            }
            
            function enviar_email() {
                if (getEmails().length > 0) {
                    parent.$("#loader").show();
                    $.post('email.ajax.php', {txtTipo: "F", Modelo: $("#txtTp").val(), txtId: $("#txtDoc").val(), idContrato: $("#txtContrato").val()}, function (data) {
                        if (data === "YES") {                                                                                 
                            $.post('form/EmailsEnvioFormServer.php', {
                                btnEnviar: "S",
                                txtId: "",
                                txtAssunto: $(".frmtext").html(),
                                ckeditor: "Segue em Anexo",
                                txtAnexo: $("#txtDoc").val(),
                                txtRecibo: parent.$("#txtNome").val(),
                                txtEmails: getEmails()}, function (d) {                                
                                parent.$("#loader").hide();
                                if (d !== null) {
                                    var ret = JSON.parse(d);
                                    if (ret[0].qtd_total !== undefined) {
                                        var qtderro = ret[0].qtd_total - ret[0].qtd_sucesso;
                                        trataResultado(ret[0].qtd_sucesso, qtderro);
                                    } else {
                                        trataResultado(0, 0);
                                    }
                                }                                
                            });
                        } else {
                            parent.$("#loader").hide();
                            bootbox.alert("Erro:" + data);
                        }
                        parent.$("#loader").hide();
                    });
                } else {
                    bootbox.alert("Não há email(s) para esta operação!");
                }
            }                        
            
            function enviar_sms(msg) {
                bootbox.confirm('Confirma o envio destas mensagens?', function (result) {
                    if (result === true) {
                        $("#loader", window.parent.document).show();                        
                        $.post("./../../util/sms/SmsSaldoForm.php", "totTrans=1&tipo=1").done(function (data) {                             
                            if (data > 0) {                            
                                let celular = "";
                                $.each(parent.$("input[name^='txtTpContato_']"), function( key, value ) {
                                    if ($(value).val() === "1") {
                                        celular = parent.$("#" + value.id.replace("txtTpContato_", "txtTxContato_")).val();
                                    }
                                });
                                $.ajax({
                                    type: "POST", url: "./../../util/sms/send_sms.php",
                                    data: {btnEnviar: 'S',
                                        txtTipo: "1",
                                        txtSendSms: celular + "|" + $("#txtId").val(),                                         
                                        ckeditor: msg
                                    }, dataType: "json",
                                    success: function (data) {
                                        $("#loader", window.parent.document).hide();
                                        bootbox.alert("Foram enviado(s)" + (parseInt(data[0].qtd_sucesso) + parseInt(data[0].qtd_erro)) + " mensagens para fila de envio!", function () {
                                            parent.FecharBox(1);
                                        });
                                    },
                                    error: function (error) {
                                        $("#loader", window.parent.document).hide();
                                        bootbox.alert(error.responseText + "!");
                                    }
                                });
                            } else {
                                $("#loader", window.parent.document).hide();
                                bootbox.alert("Saldo insuficiente!");
                            }
                        });
                    } else {
                        return;
                    }
                });
            }

            function trataResultado(qtdS, qtdE) {
                bootbox.alert("Foram enviado(s)" + (qtdS + qtdE) + " email(s):<br><br><b>* Sucesso(s):" + qtdS + "</b><br><b style='color:red'>* Falha(s):" + qtdE + "</b>");
            }
            
            function getEmails() {
                var array = "";
                for (i = 0; i < $("[type=text]").size(); i++) {
                    if ($("[type=text]")[i].value !== "" && $("[type=text]")[i].id !== "txtNome" && $('#' + $("[type=text]")[i].id.replace("txt_", "ckb_")).is(':checked')) {
                        array = array + $("[type=text]")[i].value + "|" + $("#txtId").val() + ",";
                    }
                }
                return array.substring(0, (array.length - 1));
            }

            function Print() {
                if ($("#txtTp").val() === "2") {
                    <?php if ($impressora == 0) { ?>
                        window.open("../../Modulos/Academia/cupom_plano.php?id=" + $("#txtDoc").val(), '_blank');
                    <?php } else if ($impressora == 1) { ?>
                        window.open("../../util/impressora/ImpressaoBmt.php?id=" + $("#txtDoc").val() + "&crt=<?php echo $_SESSION["contrato"]; ?>&serv=<?php echo $_SESSION["hostname"]; ?>", "_blank", "width=100,height=100");
                    <?php } ?>
                } else {
                    <?php if($_tipoContrato == 0){ ?>
                        window.open("../../util/ImpressaoPdf.php?id=" + $("#txtDoc").val() + "&idContrato=" + $("#txtContrato").val() + ($("#txtTp").val() === "1" ? "&Modelo=C" : "") + "&tpImp=I&NomeArq=Contrato&PathArq=ContratoAcademia", '_blank');
                    <?php } else { ?>
                        window.open("../../util/ImpressaoPdf.php?id_plan=" + $("#txtDoc").val() + "&idContrato=" + $("#txtContrato").val() + ($("#txtTp").val() === "1" ? "&Modelo=C" : "") + "&model=S&tpImp=I&NomeArq=Contrato&PathArq=../Modulos/Comercial/modelos/ModeloContrato.php&emp=1" + 
                        ($("#txtContrato option:selected").attr("logo") === "1" ? "&logo=n" : "") + ($("#txtContrato option:selected").attr("mdi") !== "0" ? "&red=" + $("#txtContrato option:selected").attr("mdi") : ""), '_blank');
                    <?php } ?>
                }                
            }

            function novo_email() {
                if ($("#txtNovo").val() !== "") {
                    $.post('email.ajax.php', {txtTipo: "S", txtId: $("#txtId").val(), txtConteudo: $("#txtNovo").val()}, function (data) {
                        if (data === "YES") {
                            $("#txtNovo").val("");
                            refreshList();
                        } else {
                            bootbox.alert("Erro:" + data);
                        }
                    });
                } else {
                    bootbox.alert("Preencha o E-mail Corretamente!");
                }
            }

            function editar_email(id) {
                $("#btn_edit_" + id).hide();
                $("#btn_exlr_" + id).hide();
                $("#btn_save_" + id).show();
                $("#btn_canc_" + id).show();
                $("#txt_" + id).attr("disabled", false);
            }

            function cancelar_email() {
                refreshList();
            }

            function salvar_email(id) {
                if ($("#txt_" + id).val() !== "") {
                    $.post('email.ajax.php', {txtTipo: "E", txtIdItem: id, txtConteudo: $("#txt_" + id).val()}, function (data) {
                        if (data === "YES") {
                            $("#btn_edit_" + id).show();
                            $("#btn_exlr_" + id).show();
                            $("#btn_save_" + id).hide();
                            $("#btn_canc_" + id).hide();
                            $("#txt_" + id).attr("disabled", true);
                        } else {
                            bootbox.alert("Erro:" + data);
                        }
                    });
                } else {
                    bootbox.alert("Preencha o E-mail Corretamente!");
                }
            }

            function excluir_email(id) {
                bootbox.confirm('Confirma a exclusão do registro?', function (result) {
                    if (result === true) {
                        $.post('email.ajax.php', {txtTipo: "D", txtIdItem: id}, function (data) {
                            if (data === "YES") {
                                refreshList();
                            } else {
                                bootbox.alert("Erro:" + data);
                            }
                        });
                    }
                });
            }

            function refreshList() {
                $.getJSON('email.ajax.php', {txtTipo: "L", txtId: $("#txtId").val(), ajax: 'true'}, function (j) {
                    var options = '';
                    for (var i = 0; i < j.length; i++) {
                        options += "<div style=\"width: 100%\">" +
                                "<div style=\"float: left;width: 7%;margin-top: 4px;\">" +
                                "    <input id=\"ckb_" + j[i].id_contatos + "\" type=\"checkbox\" value=\"1\" checked/>" +
                                "</div>" +
                                "<div style=\"float: left;width: 78%;\"> " +
                                "    <input id=\"txt_" + j[i].id_contatos + "\" type=\"text\" value=\"" + j[i].conteudo_contato + "\" disabled/>" +
                                "</div>" +
                                "<div style=\"float: left;width: 15%;text-align: center;margin-top: 4px;\">" +
                                "    <a id=\"btn_edit_" + j[i].id_contatos + "\" onclick=\"editar_email(" + j[i].id_contatos + ");\" href=\"javascript:;\" class=\"btn green-haze save\" title=\"Editar\" style=\"padding: 0px 4px;background:#0099cc;\"><span class=\"ico-pencil\"></span></a>" +
                                "    <a id=\"btn_exlr_" + j[i].id_contatos + "\" onclick=\"excluir_email(" + j[i].id_contatos + ");\" href=\"javascript:;\" class=\"btn red-haze cancel\" title=\"Excluir\" style=\"padding: 0px 4px;background:red;\"><span class=\"ico-cancel\"></span></a>" +
                                "    <a id=\"btn_save_" + j[i].id_contatos + "\" onclick=\"salvar_email(" + j[i].id_contatos + ");\" href=\"javascript:;\" class=\"btn green-haze save\" title=\"Salvar\" style=\"padding: 0px 4px;background:#5bb75b;display: none;\"><span class=\"ico-checkmark\"></span></a>" +
                                "    <a id=\"btn_canc_" + j[i].id_contatos + "\" onclick=\"cancelar_email();\" href=\"javascript:;\" class=\"btn red-haze cancel\" title=\"Cancelar\" style=\"padding: 0px 4px;background:#FFAA31;display: none;\"><span class=\"ico-reply\"></span></a>" +
                                "</div>" +
                                "</div>";
                    }
                    $('#lstLista').html(options).show();
                });
            }

            $(document).ready(function () {
                refreshList();
            });

        </script>
    </body>
</html>
