<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include '../../Connections/configini.php';

$contato = $_REQUEST['txtParaCon'];
$local = array();
$total = 0;
$bloqueado = 0;
$sql = "select SUM(total) total from (select COUNT(lp.id_lancamento_movimento) total from sf_lancamento_movimento_parcelas lp 
inner join sf_lancamento_movimento l on lp.id_lancamento_movimento = l.id_lancamento_movimento
inner join sf_contas_movimento c on c.id_contas_movimento = l.conta_movimento                                        
where data_pagamento is null and status = 'Aprovado' and lp.inativo = 0 and dateadd(day,dias_tolerancia,data_vencimento) < GETDATE() and fornecedor_despesa = " . $contato . "
union select COUNT(sp.solicitacao_autorizacao) total from sf_solicitacao_autorizacao sa 
inner join sf_solicitacao_autorizacao_parcelas sp on sa.id_solicitacao_autorizacao = sp.solicitacao_autorizacao
inner join sf_contas_movimento c on c.id_contas_movimento = sa.conta_movimento
where data_pagamento is null and status = 'Aprovado' and sp.inativo = 0 and dateadd(day,dias_tolerancia,data_parcela) < GETDATE() and fornecedor_despesa = " . $contato . "
union select count(v.id_venda) total from sf_vendas v inner join sf_venda_parcelas vp on v.id_venda = vp.venda
where data_pagamento is null and status = 'Aprovado' and vp.inativo = 0 and data_parcela < GETDATE() and cliente_venda = " . $contato . ") as x";
$cur = odbc_exec($con, $sql) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $total = $RFP['total'];
}

$cur2 = odbc_exec($con, "select bloqueado from sf_fornecedores_despesas where id_fornecedores_despesas = " . $contato . "") or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur2)) {
    $bloqueado = $RFP['bloqueado'];
}

$local[] = array('total' => $total, 'bloqueado' => $bloqueado);
echo(json_encode($local));
odbc_close($con);
