<?php
$SERVER_PATH = $_SERVER['PHP_SELF'];
$SERVER_FILE = basename($SERVER_PATH);
?>
<span><b>Documentos</b></span>
<hr style="margin:2px 0; border-top:1px solid #DDD"/>
<div style="width:100%;">    
    <div style="background:#F1F1F1; border:1px solid #DDD; padding:10px;vertical-align: top;">
        <?php if($ckb_adm_cli_read_ == 0){ ?>
            <input type="file" name="file" id="file"/>
            <button class="btn btn-success" type="button" style="float: right;" onclick="AdicionarDoc(<?php echo ($SERVER_FILE == 'ProspectsForm.php' || $SERVER_FILE == 'CRMForm.php' ? '1' : ($SERVER_FILE == 'ClientesForm.php' ? '2' : '3')); ?>)" name="bntSendFile"><span class="ico-checkmark"></span> Enviar</button>
        <?php } ?>
    </div>
</div>
<div style="width:100%; margin-top:10px">
    <table id="tbDocumentos" class="table" cellpadding="0" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th width="30%">Data de Upload</th>
                <th width="50%">Nome do Arquivo</th>
                <th width="10%"><center>Tamanho</center></th>
                <th width="10%"><center>Ação</center></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="4" class="dataTables_empty"></td>
            </tr>
        </tbody>
    </table>
    <div style="clear:both"></div>
</div>