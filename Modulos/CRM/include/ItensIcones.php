<div id="txtEmoticon" class="dropdown">
    <button class="btn btn-primary dropdown-toggle <?php echo $classDisable; ?>" type="button" data-toggle="dropdown"><span class="caret"></span></button>
    <ul class="dropdown-menu" style="overflow: auto;">
        <li>
            <button class="btn btn-icon" onclick="variable(' :)')" style="margin-left: 2.5%;" type="button">😊</button>
            <button class="btn btn-icon" onclick="variable(' (s)')" type="button">💲</button>
            <button class="btn btn-icon" onclick="variable(' (t)')" type="button">✂</button>
            <button class="btn btn-icon" onclick="variable(' (b)')" type="button">🏢</button>
        </li>
        <li>
            <button class="btn btn-icon" onclick="variable(' (*)')" style="margin-left: 2.5%;" type="button">✳️</button>                                
            <button class="btn btn-icon" onclick="variable(' (p)')" type="button">✌</button>
            <button class="btn btn-icon" onclick="variable(' (c)')" type="button">📱</button>
            <button class="btn btn-icon" onclick="variable(' (g)')" type="button">🌐</button>
        </li>
        <li>
            <button class="btn btn-icon" onclick="variable(' (i001)')" style="margin-left: 2.5%;" type="button">✅</button>                                
            <button class="btn btn-icon" onclick="variable(' (i002)')" type="button">▶️</button>
            <button class="btn btn-icon" onclick="variable(' (i003)')" type="button">😀</button>
            <button class="btn btn-icon" onclick="variable(' (i004)')" type="button">👆🏼</button>
        </li>
        <li>
            <button class="btn btn-icon" onclick="variable(' (i005)')" style="margin-left: 2.5%;" type="button">🚘</button>                                
            <button class="btn btn-icon" onclick="variable(' (i006)')" type="button">❌</button>
            <button class="btn btn-icon" onclick="variable(' (i007)')" type="button">🚫</button>
            <button class="btn btn-icon" onclick="variable(' (i008)')" type="button">⚠️</button>
        </li>
        <li>
            <button class="btn btn-icon" onclick="variable(' (i009)')" style="margin-left: 2.5%;" type="button">🕙</button>                                
            <button class="btn btn-icon" onclick="variable(' (i010)')" type="button">🛠</button>
            <button class="btn btn-icon" onclick="variable(' (i011)')" type="button">💵</button>
            <button class="btn btn-icon" onclick="variable(' (i012)')" type="button">👨🏽</button>
        </li>
        <li>
            <button class="btn btn-icon" onclick="variable(' (i013)')" style="margin-left: 2.5%;" type="button">💰</button>                                
            <button class="btn btn-icon" onclick="" type="button"></button>
            <button class="btn btn-icon" onclick="" type="button"></button>
            <button class="btn btn-icon" onclick="" type="button"></button>
        </li>
    </ul>
</div>