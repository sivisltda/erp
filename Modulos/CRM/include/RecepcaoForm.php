<span id="block" style="display: none;width: 100%;">
    <form id="frmRecepcao" method="POST">
        <div class="boxhead">
            <div class="boxtext">Atendimento</div>
        </div>
        <div class="boxtable" style="background: #F6F6F6;">
            <input id="txtId" name="txtId" type="hidden" value=""></input>
            <input id="txtEraPendente" name="txtEraPendente" type="hidden" value="1"></input>
            <input id="txtQtdContatos" name="txtQtdContatos" type="hidden" value="0"></input>
            <span style="float: left; width: 49%">
                <div style="width:35%; float:left;">
                    <span>Operador:</span>
                    <input type="text" name="txtOperador" id="txtOperador" class="input-medium" value="" disabled>
                </div>
                <div style="width:36%; float:left; margin-left:1%">
                    <span>Contato:</span>
                    <input type="text" name="txtNomeCon" id="txtNomeCon" class="input-medium" value="">
                </div>
                <div style="width:27%; float:left; margin-left:1%">
                    <span>Dt/Hr.:</span>
                    <div style="clear:both"></div>
                    <input type="text" name="txtData" id="txtData" value="<?php echo getData("T"); ?>" class="input-medium datepicker" style="text-align:center; width:75px" value="">
                    <input type="text" name="txtHora" id="txtHora" value="<?php echo date("H:i"); ?>" class="input-medium" style="text-align:center; width:45px" value="">
                </div>
                <div style="clear:both"></div>
                <div style="width:100%; float:left">
                    <span>Relato:</span>
                    <textarea name="txtRelato" id="txtRelato" style="width:100%;min-height: 53px;" rows="10"></textarea>
                </div>
                <div style="width:50%; float:left;">
                    <span>Depart.:</span>
                    <select name="txtDepartamento"  id="txtDepartamento" class="input-medium" style="width:100%">
                        <option value="null">Selecione</option>
                        <?php
                        $cur = odbc_exec($con, "select id_departamento,nome_departamento from sf_departamentos order by nome_departamento") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option value="<?php echo $RFP['id_departamento'] ?>"><?php echo utf8_encode($RFP['nome_departamento']); ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div style="width:49%; float:left; margin-left:1%">
                    <span>Resp.:</span>
                    <span id="carregando" name="carregando2" style="color:#666; display:none">Aguarde, carregando...</span>
                    <select name="txtPara"  id="txtPara" class="input-medium" style="width:100%">
                        <option value="null">Selecione</option>
                        <?php
                        $cur = odbc_exec($con, "select id_usuario,login_user,nome from sf_usuarios where inativo = 0 order by nome") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option value="<?php echo $RFP['id_usuario'] ?>"><?php echo utf8_encode($RFP['nome']); ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div style="width:100%; float:left">
                    <span>Obs.:</span>
                    <textarea name="txtObs" id="txtObs" style="width:100%;min-height: 53px;" rows="10"></textarea>
                </div>
                <div style="color: red;margin-top: 14px;display: none" id="msg">Pendência Financeira</div>
            </span>
            <span style="float: left; width: 50%;margin-left:1%">
                <div style="width:26%; float:left;">
                    <span>Tipo:</span>
                    <select name="txtPenCon" id="txtPenCon" class="input-medium" style="width:100%">
                        <option value="">Selecione</option>
                        <option value="C" selected>Cliente</option>
                        <option value="F">Fornecedor</option>
                        <option value="P">Prospect</option>
                        <option value="A">Outros</option>
                    </select>
                </div>
                <div style="width:73%; float:left; margin-left:1%">
                    <span>Nome:</span>
                    <div style="clear:both"></div>
                    <span id="carregando" name="carregando" style="color:#666; display:none">Aguarde, carregando...</span>
                    <span id="sprytextfield3">
                        <input name="txtIdNota" id="txtIdNota" value="" type="hidden"/>
                        <input type="text" style="width:84%" name="txtDesc" id="txtDesc" class="inputbox" value="" size="10" />
                    </span>
                    <button class="button button-blue btn-primary" style="height: 27px;padding: 3px 16px 4px 16px;" onClick="abrirTelaBox('FormGerenciador-Prospects.php', '493', '717')" type="button" title="Novo"><span class="icon-plus-sign icon-white"></span></button>
                </div>
                <span>Contatos:</span>
                <div style="background:#F1F1F1; border:1px solid #DDD; border-bottom:0; padding:10px 10px 0">
                    <div style="float:left; width:19%">
                        <select id="txtTipoContato" onChange="alterarMask()" >
                            <option value="1">Celular</option>
                            <option value="0">Telefone</option>
                            <option value="2">E-mail</option>
                            <option value="3">Site</option>
                            <option value="4">Outros</option>
                        </select>
                    </div>
                    <div style="float:left; width:calc(53% - 35px); margin-left:1%">
                        <input type="text" class="input-medium" id="txtTextoContato" />
                    </div>
                    <div style="float:left; width:25%; margin-left:1%">
                        <input type="text" class="input-medium" id="txtRespContato" />
                    </div>                    
                    <div style="float:left; width:35px; margin-left:1%">
                        <button type="button" onclick="addContato();" class="btn dblue" style="line-height:19px; margin:0;background-color: #308698 !important;"><span class="ico-plus icon-white"></span></button>
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div style="background:#F1F1F1; border:1px solid #DDD; border-top:0; padding:5px 10px 10px">
                    <div id="divContatos" style="height:108px; background:#FFF; border:1px solid #DDD; overflow-y:scroll"></div>
                </div>
                <div style="width:26%; float:left;">
                    <span>Tipo:</span>
                    <div style="clear:both;margin-bottom: 4px;"></div>
                    <input class="none" id="txtservicoA" type='Radio' Name='txtservico' value= '0' checked>Vendas
                    <input class="none" id="txtservicoO" type='Radio' Name='txtservico' value= '1'>Serviços
                </div>
                <div style="width:26%; float:left; margin-left:1%">
                    <span>Pend.:</span>
                    <div style="clear:both"></div>
                    <select name="txtPendente" id="txtPendente" class="input-medium" style="width:100px">
                        <option value="1">Sim</option>
                        <option value="0">Não</option>
                    </select>
                </div>
                <div style="float:right;margin-top: 14px;">
                    <button class="btn btn-success" type="button" id="btnSave" name="btnSave" onclick="salvar();"><span class="ico-checkmark"></span> Gravar</button>
                    <button class="btn yellow" type="button" id="btnCancelar" name="btnCancelar" onclick="limpar();atendimento();"><span class="ico-reply"></span> Cancelar</button>
                </div>
            </span>
            <div style="clear:both"></div>
        </div>
    </form>
</span>
