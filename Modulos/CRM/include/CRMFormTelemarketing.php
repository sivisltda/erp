<div style="width:100%">
    <input name="txtIdx" id="txtIdx" value="<?php echo $idx; ?>" type="hidden"/>
    <div style="background:#F1F1F1; border:1px solid #DDD; padding:10px">
        <div style="float:left; width:calc(99% - 83px)">
            <div style="float:left; width:12.5%">
                <span>Data Cham.:</span>
                <input id="txtDataHistorico" type="text" class="input-medium datepicker" value="<?php echo getData("T"); ?>"/>
            </div>
            <div style="float:left; width:7.5%; margin-left:1%">
                <span>Hora C.:</span>
                <input id="txtHoraHistorico" type="text" class="input-medium" value="<?php echo date("H:i"); ?>"/>
            </div>
            <div style="float:left; width:14%; margin-left:1%">
                <span>Tipo:</span>
                <select id="txtTipoChamado" class="select input-medium" style="width:100%">
                    <option value="T" <?php
                    if ($tipoChamado == "T") {
                        echo "SELECTED";
                    }
                    ?>>Telefone</option>
                    <option value="E" <?php
                    if ($tipoChamado == "E") {
                        echo "SELECTED";
                    }
                    ?>>Email</option>
                    <option value="V" <?php
                    if ($tipoChamado == "V") {
                        echo "SELECTED";
                    }
                    ?>>Visita</option>
                    <option value="S" <?php
                    if ($tipoChamado == "S") {
                        echo "SELECTED";
                    }
                    ?>>SMS</option>
                    <option value="P" <?php
                    if ($tipoChamado == "P") {
                        echo "SELECTED";
                    }
                    ?>>Presencial</option>
                    <option value="O" <?php
                    if ($tipoChamado == "O") {
                        echo "SELECTED";
                    }
                    ?>>Outro</option>
                    <option value="W" <?php
                    if ($tipoChamado == "W") {
                        echo "SELECTED";
                    }
                    ?>>Whatsapp</option>                    
                    <option value="R" <?php
                    if ($tipoChamado == "R") {
                        echo "SELECTED";
                    }
                    ?>>Portal</option>                    
                </select>
            </div>
            <div style="float:left; width:20%; margin-left:1%">
                <input type="hidden" name="permissaoUpdate" id="permissaoUpdate" value="<?php echo $ckb_crm_upt_rem; ?>">
                <input type="hidden" name="bloquearRemetente" id="bloquearRemetente" value="<?php echo $ckb_crm_upt_rem_new; ?>">
                <span>De:</span>
                <select <?php echo ($ckb_crm_upt_rem_new == 1 ? "disabled" : ""); ?> id="txtDeHistorico" class="select input-medium" style="width:100%">
                    <option value="null">Selecione</option>
                    <?php
                    $cur = odbc_exec($con, "select id_usuario,login_user,nome from sf_usuarios where inativo = 0 order by nome") or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) { ?>
                        <option value="<?php echo $RFP['id_usuario'] ?>"<?php
                        if (!(strcmp($RFP['id_usuario'], $_SESSION["id_usuario"]))) {
                            echo "SELECTED";
                        }
                        ?>><?php echo utf8_encode($RFP['nome']); ?></option>
                    <?php } ?>
                </select>
            </div>
            <div style="float:left; width:20%; margin-left:1%">
                <span>Para:</span>
                <select id="txtParaHistorico" class="select input-medium" style="width:100%">
                    <option value="null">Selecione</option>
                    <?php
                    $cur = odbc_exec($con, "select id_usuario,login_user,nome from sf_usuarios where inativo = 0 order by nome") or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) { ?>
                        <option value="<?php echo $RFP['id_usuario'] ?>"<?php
                        if (!(strcmp($RFP['id_usuario'], $_SESSION["id_usuario"]))) {
                            echo "SELECTED";
                        }
                        ?>><?php echo utf8_encode($RFP['nome']); ?></option>
                    <?php } ?>
                </select>
            </div>
            <div style="float:left; width:12.5%; margin-left:1%">
                <span>Prazo:</span>
                <input id="txtDataHistoricoPrev" type="text" class="input-medium datepicker" value="<?php echo getData("T"); ?>">
            </div>
            <div style="float:left; width:7.5%; margin-left:1%">
                <span>Hora P.:</span>
                <input id="txtHoraHistoricoPrev" type="text" class="input-medium" value="<?php echo date("H:i"); ?>">
            </div>
            <div style="clear:both; height:5px"></div>
            <div style="float:left; width:100%">
                <textarea type="text" id="textoArea" class="input-medium" row="2" style="width:100%; height:110px; resize:none"></textarea>
            </div>
        </div>
        <div style="float:left; width:83px; margin-left:1%; margin-top:49px">
            <?php if($ckb_adm_cli_read_ == 0 || $ckb_adm_atendimentos_ == 1){?>
            <button id="bntSaveMsg" type="button" onclick="addChamado()" name="bntSaveMsg" style="line-height:20px; width:83px; background-color:#308698 !important" class="btn dblue">Salvar <span class="icon-arrow-next icon-white"></span></button>
            <?php } ?>
            <button type="button" onclick="limparChamado()" class="btn yellow" style="line-height:20px; width:83px; margin:0; margin-top:5px">Cancelar <span class="ico-reply"></span></button>
        </div>
        <div style="clear:both; height:5px"></div>
        <div style="display: flex;align-items: center;width: calc(99% - 83px);">
            <div style="float:left;width: 30%;"><input id="ckbAcompanhar" name="ckbAcompanhar" type="checkbox" value="1"/> Acompanhar este atendimento</div>
            <?php if ($mdl_srs_ == 1 && is_numeric($id) && $ckb_out_incserasa_ == 1) {
                if ($inativo == 0 && $ckb_out_incserasa_ == 1) {
                    $serasa = 2;
                } elseif ($inativo == 2 && $ckb_out_remserasa_ == 1) {
                    $serasa = 0;
                } else {
                    $serasa = 1;
                } ?>
                <div style="display: flex;align-items: center;justify-content: flex-end;width: 70%;">
                    <button id="btnSerasa" type="button" onclick="addSerasa(<?php echo ($inativo == 3 || $inativo == 2 ? '0' : '2'); ?>);"
                        class="btn" style="width:20%; background-color: #333;"><?php echo ($inativo == 2 || $inativo == 3 ? "Desbloquear" : "Bloquear"); ?>
                    <span class="icon-plus icon-white"></span></button>
                    <?php if ($serasa == 0 || $inativo == 3) { ?>
                        <button id="btnSerasa" type="button" onclick="addSerasa(<?php echo ($inativo == 3) ? "2" : "3" ?>, 'pag');"
                            class="btn green" style="width:20%;margin-left:1%;"><?php echo ($inativo == 2 ? "Desbloquear Pag" : "Bloquear Pag"); ?>
                        <span class="icon-plus icon-white"></span></button>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<div style="width:100%; margin-top:10px">
    <table class="table" style="background:#FFF; border:1px solid #DDD" cellpadding="0" cellspacing="0" width="100%" id="historico">
        <thead>
            <tr>
                <th width="1%"></th>
                <th width="5%"><center>Protocolo</center></th>
                <th width="10%"><center>Data/Hora</center></th>
                <th width="10%">Atendente</th>
                <th width="10%"><center>Tipo</center></th>
                <th width="10%">Encaminhado</th>
                <th width="10%"><center>Prazo</center></th>
                <th width="14%"><center>Período</center></th>
                <th width="19%">Assunto</th>
                <th width="5%"><center>Pendente</center></th>
                <?php if ($imprimir == 0) { ?>
                    <th width="6%"><center>Ação</center></th>
                <?php } ?>
            </tr>
        </thead>
    </table>
    <div style="clear:both"></div>
</div>
