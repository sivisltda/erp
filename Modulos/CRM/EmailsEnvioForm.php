<?php
include "../../Connections/configini.php";
include "form/EmailsEnvioFormServer.php";

$where = "and mensagem " . (isset($_GET["bol"]) ? "" : "not") . " like '%||M_bol_%'";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <link href="../../js/plugins/jquery-ui-1.11.4/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>        
        <style>
            .ui-dialog .ui-dialog-content, .ui-dialog-titlebar{
                border-left: 0px !important;
                border-right: 0px !important;
                border-top: 0px !important
            }
            .ui-dialog .ui-dialog-buttonpane button{
                background: #FFAA31 !important;
                border: 1px solid #FFAA31 !important;
            }
            .ui-dialog .ui-dialog-buttonpane{
                padding: 0 0px !important;
                padding: 0px;
            }
        </style>
    </head>
    <body>
        <form name="frmEnviaDados" id="frmEnviaDados" method="POST" onkeydown="if (event.ctrlKey && event.keyCode === 86) { return false; }" enctype="multipart/form-data">
            <div id="dialog-confirm" hidden="hidden">
                <div>
                    <img name="img" id="img" style="width:auto; height:auto; max-width:100%; max-height:275px"/>
                </div>
            </div>
            <div id="dialog-emails" hidden="hidden">
                <div style="margin-top: 5px;">
                    <input id="txtNovoEmail" style="height: 25px;width: 259px;"/>
                    <button class="button button-turquoise btn-primary" type="button" id="btnNovoEmail"><span class="ico-plus icon-white"></span></button>
                </div>
                <div style="width:100%; margin:10px 0">
                    <hr style="margin:2px 0; border-top:1px solid #DDD">
                </div>
                <div id="listEmails">
                    <table><thead></thead></table>
                </div>
            </div>
            <div class="frmhead">
                <div class="frmtext">Envio de Emails</div>
                <div class="frmicon" style="top:12px" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input name="txtId" id="txtId" type="hidden"/>
                <input name="txtEmails" id="txtEmails" type="hidden"/>
                <div style="height:42px">
                    <label for="txtDescricao">Modelo:</label>
                    <select class="select" id="txtModelo" name="txtModelo" style="width:100%; float:left">
                        <option value="null">Nenhum</option>
                        <?php $cur = odbc_exec($con, "Select id_email, descricao from sf_emails where tipo = 0 " . $where . " order by descricao") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option value="<?php echo $RFP['id_email'] ?>"><?php echo utf8_encode($RFP['descricao']); ?></option> 
                        <?php } ?>
                    </select>
                </div>
                <div style="height:42px; margin-top:5px">
                    <label for="txtAssunto">Assunto:</label><input name="txtAssunto" id="txtAssunto" type="text" maxlength="100" style="width:100%" />
                </div>
                <div style="height:42px;margin-top:5px;width: 100px;float: left;">
                    <label for="arquivo">Destinatários:</label>
                    <button class="btn  btn-primary" type="button" title="Visualizar" style="width: 100px;"  id="btnDestinatários"> Visualizar </button>
                </div>
                <div style="height:42px; margin-top:5px; width:257px; float:left;margin-left: 1%">
                    <label for="arquivo">Imagem Publicidade:</label>
                    <div id="btnUpArquivo">
                        <input class="btn btn-primary" style="height:20px; line-height:21px; width:100%" type="file" name="arquivo" id="arquivo" />
                    </div>
                    <div id="btnVerArquivo" hidden>
                        <button class="btn  btn-primary" type="button" title="Visualizar" style="width:100px" id="btnVisualizar"> Visualizar </button>
                    </div>
                </div>
                <div style="height:42px; margin-top:5px; width:257px; float:left; margin-left:32px">
                    <input type="hidden" id="txtNmArquivo" name="txtNmArquivo" value="<?php echo $anexo; ?>"/>
                    <label for="anexo">Arquivo Anexo:</label>
                    <div id="btnUpAnexo">
                        <input class="btn btn-primary" style="height:20px; line-height:21px; width:100%" type="file" name="anexo" id="anexo" />
                    </div>
                    <div id="btnVerAnexo" hidden>
                        <a id="linkArq" title="Download File"></a>
                    </div>
                </div>
                <div style="clear:both;"></div>
                <div class="data-fluid" style="overflow-x:inherit; margin-top: 5px;">
                    <label for="ckeditor"></label>
                    <textarea id="ckeditor" name="ckeditor" style="height:358px" onkeydown="if (event.ctrlKey && event.keyCode === 86) { return false;}"></textarea>
                </div>
            </div>
            <div class="frmfoot">
                <div style="width: 50%;float: left;text-align: left;">
                    Etapa
                    <span id="totalpara"> 0 </span>
                    de
                    <span id="totalde"> 0 </span>
                </div>
                <div class="frmbtn">
                    <button class="btn btn-success" type="button" name="btnEnviar" title="Enviar" id="btnEnviar" value="Enviar"><span class="ico-checkmark"></span> Enviar </button>
                    <button class="btn yellow" title="Cancelar" onClick="parent.FecharBox(1)" id="btnCancelar"><span class="ico-reply"></span> Cancelar</button>
                </div>
            </div>
        </form>
        <script type='text/javascript' src='../../js/plugins/jquery-ui-1.11.4/jquery-ui.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/plugins/ckeditorAll/ckeditor.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>
        <script type="text/javascript" src="js/EmailsEnvioForm.js"></script>        
        <?php odbc_close($con); ?>
    </body>
</html>