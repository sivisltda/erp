<?php

include '../../Connections/configini.php';
$aColumns = array('data_tele', 'de_pessoa', 'pendente', 'para_pessoa', 'proximo_contato', 'proximo_contato', 'relato');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = " and atendimento_servico = " . (is_numeric($_GET['tp']) ? $_GET['tp'] : "0");
$sWhere = "";
$sOrder = " ORDER BY data_tele desc ";
$sLimit = 0;
$sQtd = 20;
$mdl = 0;

if (is_numeric($_GET['mdl'])) {
    $mdl = $_GET['mdl'];
}

function legenda($i) {
    switch ($i) {
        case "T":
            return "Telefone";
        case "E":
            return "Email";
        case "V":
            return "Visita";
        case "P":
            return "Presencial";
        case "S":
            return "SMS";
        case "W":
            return "Whatsapp";            
        case "R":
            return "Portal";            
    }
    return "Outro";
}

if (is_numeric($_GET['Cli'])) {
    $sWhereX = $sWhereX . " and t.pessoa =" . $_GET['Cli'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if ($_GET['sSearch'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['sSearch']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,id_tele,de_pessoa,para_pessoa,u.login_user as de_npessoa,u2.login_user as para_npessoa,data_tele,pendente,proximo_contato,obs,relato,id_fornecedores_despesas,obs_fechamento,
            (select count(tm.id) from sf_mensagens_telemarketing tm where tm.id_tele = t.id_tele) tel_msg, especificacao, tipo
            from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa
            LEFT JOIN sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = t.pessoa
            LEFT JOIN sf_usuarios u2 on u2.id_usuario = t.para_pessoa
            where pessoa is not null " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " " . $sOrder;
//echo $sQuery1;
$cur = odbc_exec($con, $sQuery1);

$sQuery = "SELECT COUNT(*) total from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa where pessoa is not null $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa where pessoa is not null $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

if ($_GET['Al'] == "1") {
    $X = "X";
}

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $backColor = "";
    $pendente = "NÃO";
    $proxContato = "";
    $dias = "-";
    if ($aRow['proximo_contato'] != null) {
        $proxContato = escreverDataHora($aRow['proximo_contato']);
    }
    if ($aRow['pendente'] == 1) {
        $backColor = " style='color: #08c' ";
        $pendente = utf8_encode("SIM");
        if ($aRow['proximo_contato'] != null) {
            $diferenca = dataDiff(date('Y-m-d'), escreverData($aRow['proximo_contato'], 'Y-m-d'), 'd');
            if ($diferenca > 0) {
                $dias = $diferenca . " dia(s) restantes";
            } else if ($diferenca == 0) {
                $dias = "Hoje";
            } else {
                $backColor = " style='color: red' ";
                $dias = (-1 * $diferenca) . " dia(s) atrasados";
            }
        }
    }
    if ($aRow['tel_msg'] > 0) {
        $row[] = "<center><img src=\"./../../img/details_open.png\"></center>";
        $informativo = "<div style=\"text-align: center; position: absolute;width:13px; height:13px; background:#999;color:white; border-radius:50%\">" . $aRow['tel_msg'] . "</div>";
    } else {
        $row[] = "<center><img src=\"./../../img/details_disabled.png\"></center>";
        $informativo = "";
    }
    $row[] = "<center><div id='formPQ' title='" . $aRow['id_tele'] . "'>" . $aRow['id_tele'] . "</div></center>";
    $row[] = "<center><a title=\"" . escreverDataHora($aRow['data_tele']) . "\" href=\"javascript:void(0)\" onClick=\"" . (is_numeric($_GET['tp']) ? "AbrirBoxServicos" : "AbrirBoxTelemarketing") . $X . "(" . $aRow['id_tele'] . "," . utf8_encode($aRow['id_fornecedores_despesas']) . ",0," . (is_numeric($_GET['tp']) ? "2" : "1") . ")\"><div id='formPQ' title='" . escreverDataHora($aRow['data_tele']) . "'>" . escreverDataHora($aRow['data_tele']) . "</div></a></center>";
    $row[] = "<center><div id='formPQ' " . $backColor . " title='" . utf8_encode($aRow['de_npessoa']) . "'>" . utf8_encode($aRow['de_npessoa']) . "</div></center>";
    if ($mdl == 1) {
        $row[] = "<center><div " . $backColor . ">" . legenda($aRow['especificacao']) . "</div></center>";
    } else {
        $row[] = "<div id='formPQ' " . $backColor . " title='*R:" . utf8_encode($aRow['relato']) . "\n*O:" . utf8_encode($aRow['obs']) . "\n*F:" . utf8_encode($aRow['obs_fechamento']) . "'>" . utf8_encode($aRow['relato']) . "</div>";
    }
    $row[] = "<center><div id='formPQ' " . $backColor . " title='" . utf8_encode($aRow['para_npessoa']) . "'>" . utf8_encode($aRow['para_npessoa']) . "</div></center>";
    $row[] = "<center><div id='formPQ' " . $backColor . " title='" . $proxContato . "'>" . $proxContato . "</div></center>";
    if ($mdl == 0) {
        $row[] = $informativo . "<center><div id='formPQ' " . $backColor . " title='" . $pendente . "'>" . $pendente . "</div></center>";
    }
    $row[] = "<center><div id='formPQ' " . $backColor . " title='" . $dias . "'>" . $dias . "</div></center>";
    if ($mdl == 1) {
        $row[] = "<div id='formPQ' " . $backColor . " title='*R:" . utf8_encode($aRow['relato']) . "\n*O:" . utf8_encode($aRow['obs']) . "\n*F:" . utf8_encode($aRow['obs_fechamento']) . "'>" . (strlen(utf8_encode($aRow['relato'])) > 120 ? utf8_encode(substr($aRow['relato'], 0, 120)) . "..." : utf8_encode($aRow['relato'])) . "</div>";
        $row[] = $informativo . "<center><div id='formPQ' " . $backColor . " title='" . $pendente . "'>" . $pendente . "</div></center>";
    }
    $action = "";
    if ($mdl == 1) {
        $action .= "<a href=\"javascript:void(0)\" onClick=\"AbrirBoxTelemarketing(" . $aRow['id_tele'] . "," . utf8_encode($aRow['id_fornecedores_despesas']) . ",2," . (is_numeric($_GET['tp']) ? "2" : "1") . ")\"><img src=\"../../img/1365123843_onebit_323 copy.PNG\" width='16' height='16' title=\"Histórico de Ações\"/></a>";
        if (($ckb_adm_cli_read_ == 0 || $aRow['tipo'] == "P" || $aRow['de_pessoa'] == $_SESSION["id_usuario"] || $aRow['para_pessoa'] == $_SESSION["id_usuario"]) && $aRow['pendente'] == 1 && $ckb_crm_fec_atend_ == 1) {
            $action .= "<a href=\"javascript:void(0)\" onClick=\"AbrirBoxTelemarketing(" . $aRow['id_tele'] . "," . utf8_encode($aRow['id_fornecedores_despesas']) . ",1," . (is_numeric($_GET['tp']) ? "2" : "1") . ")\"><img src=\"../../img/close.png\" width='16' height='16' title=\"Encerrar Pendência\"/></a>";
        }
        if (($ckb_adm_cli_read_ == 0 || $aRow['tipo'] == "P" || $aRow['de_pessoa'] == $_SESSION["id_usuario"] || $aRow['para_pessoa'] == $_SESSION["id_usuario"]) && $ckb_crm_exc_atend_ == 1 && ($aRow['de_pessoa'] == $_SESSION["id_usuario"] || $aRow['para_pessoa'] == $_SESSION["id_usuario"])) {
            $action .= "<a href=\"javascript:void(0)\" onClick=\"" . (is_numeric($_GET['tp']) ? "delServico" : "delChamado") . "(" . $aRow['id_tele'] . ");\"><img src=\"../../img/1365123843_onebit_33 copy.PNG\" width='16' height='16' title=\"Excluir\"/></a>";
        }
        $row[] = "<center style='display: flex;justify-content: space-around;'>" . $action . "</center>";                        
    } else {
        if ($imprimir == 0) {
            if ($ckb_adm_cli_read_ == 0 || $aRow['tipo'] == "P") {
                if ($aRow['pendente'] == 1) {
                    $action .= "<a href=\"javascript:void(0)\" onClick=\"AbrirBoxTelemarketing" . $X . "(" . $aRow['id_tele'] . "," . utf8_encode($aRow['id_fornecedores_despesas']) . ",1," . (is_numeric($_GET['tp']) ? "2" : "1") . ")\"><img src=\"../../img/close.png\" width='18' height='18' title=\"Encerrar Pendência\" value='Enviar'></a>";
                }
                if ($aRow['de_pessoa'] == $_SESSION["id_usuario"] || $aRow['para_pessoa'] == $_SESSION["id_usuario"]) {
                    if ($_GET['Al'] == "1") {
                        $action .= "<input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\" onClick=\"RemoverCRM(" . $aRow['id_tele'] . ")\")>";
                    } else {
                        $action .= "<a href='CRMForm.php?Delx=" . $aRow['id_tele'] . "&gb=" . $_GET['Cli'] . "'><input name='' type='image' onClick=\"return confirm('Deseja deletar esse registro?')\" src=\"../../img/1365123843_onebit_33 copy.PNG\" width='18' height='18' title=\"Excluir\" value='Enviar'></a>";
                    }
                }
                $row[] = "<center style='display: flex;justify-content: space-evenly;'>" . $action . "</center>";
            } else {
                $row[] = "";
            }            
        }
    }
    if ($aRow['tel_msg'] > 0) {
        $row[] = $aRow['id_tele'];
    } else {
        $row[] = "0";
    }
    $output['aaData'][] = $row;
}
echo json_encode($output);
odbc_close($con);
