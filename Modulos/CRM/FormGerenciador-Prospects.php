<?php
include '../../Connections/configini.php';

$disabled = 'disabled';
$active1 = 'active';
$linhaContato = "1";

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if (isset($_POST['bntSave'])) {
    $idCliente = "";
    $dataNasc = "null";
    $notIn = "";
    odbc_exec($con, "set dateformat dmy;") or die(odbc_errormsg());
    if (is_numeric($_POST['txtId'])) {
        $query = "update sf_fornecedores_despesas set " .
                "razao_social = " . valoresTexto('txtRazao') . "," .
                "cnpj = " . valoresTexto('txtCnpj') . "," .
                "inscricao_estadual = " . valoresTexto('txtInsc') . "," .
                "endereco = " . valoresTexto('txtEndereco') . "," .
                "numero = " . valoresTexto('txtNumero') . "," .
                "complemento = " . valoresTexto('txtComplemento') . "," .
                "bairro = " . valoresTexto('txtBairro') . "," .
                "estado = " . utf8_decode($_POST['txtEstado']) . "," .
                "cidade = " . utf8_decode($_POST['txtCidade']) . "," .
                "cep = " . valoresTexto('txtCep') . "," .
                "referencia_ps1 = " . valoresTexto('txtreferencia_ps1') . "," .
                "telefone_ps1 = " . valoresTexto('txttelefone_ps1') . "," .
                "referencia_ps2 = " . valoresTexto('txtreferencia_ps2') . "," .
                "telefone_ps2 = " . valoresTexto('txttelefone_ps2') . "," .
                "nome_fantasia = " . valoresTexto('txtFantasia') . "," .
                "grupo_pessoa = " . utf8_decode($_POST['txtGrupoPessoas']) . "," .
                "profissao = " . valoresTexto('txtProfissao') . "," .
                "data_nascimento = " . $dataNasc . "," .
                "indicador = " . utf8_decode($_POST['txtIndicador']) . "," .
                "contato = " . valoresTexto('txtContNome') . "," .
                "funcao_contato = " . valoresTexto('txtContFunc') . "," .
                "procedencia = " . utf8_decode($_POST['txtProcedencia']) .
                " where id_fornecedores_despesas = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        $notIn = '0';
        if (isset($_POST['txtQtdContatos'])) {
            $max = $_POST['txtQtdContatos'];
            for ($i = 0; $i <= $max; $i++) {
                if (isset($_POST['txtIdContato_' . $i]) && is_numeric($_POST['txtIdContato_' . $i])) {
                    if ($_POST['txtIdContato_' . $i] != 0) {
                        $notIn = $notIn . "," . $_POST['txtIdContato_' . $i];
                    }
                }
            }
            if ($notIn != "") {
                odbc_exec($con, "delete from sf_fornecedores_despesas_contatos where fornecedores_despesas = " . $_POST['txtId'] . " and id_contatos not in (" . $notIn . ")");
            }
        }
        $idCliente = $_POST['txtId'];
    } else {
        $toSave = true;
        if ($_POST['txtCnpj'] != '') {
            $cur = odbc_exec($con, "select * from sf_fornecedores_despesas where tipo = 'P' and cnpj = " . valoresTexto('txtCnpj') . "");
            while ($RFP = odbc_fetch_array($cur)) {
                $toSave = false;
            }
        }
        if ($toSave == false) {
            echo "<script>alert('Registro de CPF/CNPJ já cadastrado no sistema!');</script>";
        } else {
            $query = "insert into sf_fornecedores_despesas(razao_social,cnpj,inscricao_estadual,endereco,numero,complemento,bairro,estado,cidade,cep,nome_fantasia,grupo_pessoa,profissao,data_nascimento,procedencia,referencia_ps1,telefone_ps1,referencia_ps2,telefone_ps2,contato,funcao_contato,tipo,empresa,dt_cadastro,indicador)values(" .
                    strtoupper(valoresTexto('txtRazao')) . "," .
                    valoresTexto('txtCnpj') . "," .
                    valoresTexto('txtInsc') . "," .
                    valoresTexto('txtEndereco') . "," .
                    valoresTexto('txtNumero') . "," .
                    valoresTexto('txtComplemento') . "," .
                    valoresTexto('txtBairro') . "," .
                    utf8_decode($_POST['txtEstado']) . "," .
                    utf8_decode($_POST['txtCidade']) . "," .
                    valoresTexto('txtCep') . "," .
                    valoresTexto('txtFantasia') . "," .
                    utf8_decode($_POST['txtGrupoPessoas']) . "," .
                    valoresTexto('txtProfissao') . "," .
                    $dataNasc . "," .
                    valoresTexto('txtProcedencia') . "," .
                    valoresTexto('txtreferencia_ps1') . "," .
                    valoresTexto('txttelefone_ps1') . "," .
                    valoresTexto('txtreferencia_ps2') . "," .
                    valoresTexto('txttelefone_ps2') . "," .
                    valoresTexto('txtContNome') . "," .
                    valoresTexto('txtContFunc') . "," .
                    "'P','001',getDate()," . utf8_decode($_POST['txtIndicador']) . ")";
            odbc_exec($con, $query) or die(odbc_errormsg());
            $res = odbc_exec($con, "select top 1 id_fornecedores_despesas from sf_fornecedores_despesas order by id_fornecedores_despesas desc") or die(odbc_errormsg());
            $idCliente = odbc_result($res, 1);
        }
    }
    if ($idCliente !== "") {
        if (isset($_POST['txtQtdContatos'])) {
            $max = $_POST['txtQtdContatos'];
            for ($i = 1; $i < $max; $i++) {
                if (isset($_POST['txtIdContato_' . $i])) {
                    if ($_POST['txtIdContato_' . $i] !== "0") {
                        odbc_exec($con, "update sf_fornecedores_despesas_contatos set tipo_contato = " . $_POST['txtTpContato_' . $i] . ",conteudo_contato = " . valoresTexto('txtTxContato_' . $i) . " where id_contatos = " . $_POST['txtIdContato_' . $i]) or die(odbc_errormsg());
                    } else {
                        odbc_exec($con, "insert into sf_fornecedores_despesas_contatos(fornecedores_despesas,tipo_contato,conteudo_contato) values(" . $idCliente . "," . $_POST['txtTpContato_' . $i] . "," . valoresTexto('txtTxContato_' . $i) . ")") or die(odbc_errormsg());
                    }
                }
            }
        }
        header("Location: FormGerenciador-Prospects.php?id=" . $idCliente);
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_endereco_entrega WHERE fornecedores_despesas = " . $_POST['txtId']);
        odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = " . $_POST['txtId']);
        odbc_exec($con, "DELETE FROM sf_fornecedores_despesas WHERE id_fornecedores_despesas = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso');window.top.location.href = 'Gerenciador-Prospects.php';</script>";
    }
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_fornecedores_despesas where id_fornecedores_despesas = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_fornecedores_despesas']);
        $razao = utf8_encode($RFP['razao_social']);
        $cnpj = utf8_encode($RFP['cnpj']);
        $insc = utf8_encode($RFP['inscricao_estadual']);
        $endereco = utf8_encode($RFP['endereco']);
        $numero = utf8_encode($RFP['numero']);
        $complemento = utf8_encode($RFP['complemento']);
        $bairro = utf8_encode($RFP['bairro']);
        $estado = utf8_encode($RFP['estado']);
        $cidade = utf8_encode($RFP['cidade']);
        $cep = utf8_encode($RFP['cep']);
        $fantasia = utf8_encode($RFP['nome_fantasia']);
        $grupopes = utf8_encode($RFP['grupo_pessoa']);
        $formapg = utf8_encode($RFP['forma_pagamento']);
        $profissao = utf8_encode($RFP['profissao']);
        $data_cadastro = escreverData($RFP['dt_cadastro']);
        $procedencia = utf8_encode($RFP['procedencia']);
        $referencia_ps1 = utf8_encode($RFP['referencia_ps1']);
        $telefone_ps1 = utf8_encode($RFP['telefone_ps1']);
        $referencia_ps2 = utf8_encode($RFP['referencia_ps2']);
        $telefone_ps2 = utf8_encode($RFP['telefone_ps2']);
        $indicador = utf8_encode($RFP['indicador']);
        $contNome = utf8_encode($RFP['contato']);
        $contFunc = utf8_encode($RFP['funcao_contato']);
    }
} else {
    $disabled = '';
    $id = '';
    $razao = '';
    $cnpj = '';
    $insc = '';
    $endereco = '';
    $numero = '';
    $complemento = '';
    $bairro = '';
    $estado = '';
    $cidade = '';
    $cep = '';
    $fantasia = '';
    $grupopes = '';
    $formapg = '';
    $profissao = '';
    $procedencia = '';
    $data_cadastro = '';
    $referencia_ps1 = '';
    $telefone_ps1 = '';
    $referencia_ps2 = '';
    $telefone_ps2 = '';
    $indicador = '';
    $contNome = '';
    $contFunc = '';
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
if (isset($_POST['bntConvert'])) {
    if (is_numeric($PegaURL)) {
        odbc_exec($con, "EXEC dbo.SP_MK_COMISSAO_REGRAS @id_cliente = " . $PegaURL . ", @id_regra = 0;");
        odbc_exec($con, "update sf_fornecedores_despesas set tipo = 'C' where id_fornecedores_despesas = '" . $PegaURL . "'") or die(odbc_errormsg());
        //------------------------------------------------------------------------------------------------------------------------------------------------        
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        values ('sf_fornecedores_despesas', null, '" . $_SESSION["login_usuario"] . "', 'C', 'CONVERSAO - CLIENTE', GETDATE(), " . $PegaURL . ")");        
        $count = 0;
        $cur = odbc_exec($con, "select id, id_lead, indicador_lead, conv_pro_cli from sf_lead 
        inner join sf_lead_campanha on id_procedencia = procedencia
        where getdate() between data_ini and data_fim and sf_lead_campanha.inativo = 0 and sf_lead.inativo = 0 
        and indicador_lead is not null and prospect = " . $_POST['txtId']);
        while ($RFP = odbc_fetch_array($cur)) {
            $id = utf8_encode($RFP['id']);
            $id_lead = utf8_encode($RFP['id_lead']);            
            $id_indicador = utf8_encode($RFP['indicador_lead']);
            $count = utf8_encode($RFP['conv_pro_cli']);
        }
        for ($i = 0; $i < $count; $i++) {
            $cur = odbc_exec($con, "EXEC dbo.SP_LEAD_CUPOM @campanha = " . $id . ", @lead = " . $id_indicador . ", @lead_origem = " . $id_lead . ";");
            $idcupom = odbc_result($cur, 1);
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------                
        echo "<script>window.top.location.href = './../Comercial/Gerenciador-Clientes.php';</script>";
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<link href="../../../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <div class="row-fluid">
        <div id="inline1"  style="height:540px; overflow:hidden; ">
            <div class="block title">
                <div class="head">
                    <div id="topBG" style="margin-bottom:10px;">
                        <h2 class="head"><font color="#FFFFFF"> Prospects</font>
                            <button style="border:none; float:right; width:25px; margin-right:20px; color:#FFFFFF; background-color:#333333" onClick="<?php
                            if ($FinalUrl != "") {
                                echo "parent.FecharBox(3)";
                            } else {
                                echo "parent.FecharBox(0)";
                            }
                            ?>" id="bntOK"><b>x</b></button>
                        </h2>
                    </div>
                </div>
            </div>
            <div>
                <div class="block">
                    <form action="FormGerenciador-Prospects.php?id=<?php echo $_GET['id']; ?>" method="POST" name="frmEnviaDados">
                        <div class="head">
                            <a href="#" onClick="source('tabs'); return false;"><div class="icon"><span class="ico-info" style="color:#fff; margin:0; padding:0;"></span></div></a>
                        </div>
                        <div class="data-fluid tabbable">
                            <ul class="nav nav-tabs">
                                <li class="<?php echo $active1; ?>" style="margin-left:10px"><a href="#tab1" data-toggle="tab">Dados principais</a></li>
                                <li><a href="#tab2" data-toggle="tab">Contatos</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane <?php echo $active1; ?>" id="tab1">
                                    <input name="txtQtdContatos" id="txtQtdContatos" value="<?php echo $linhaContato; ?>" type="hidden"/>
                                    <table width="680" style="margin-bottom:20px" border="0" cellpadding="3" class="textosComuns">
                                        <tr>
                                        <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                                        <td align="right">Nome/Razão Social:</td>
                                        <td colspan="3">
                                            <div style="width:360px; float:left"><input name="txtRazao" id="txtRazao" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="128" value="<?php echo $razao; ?>"/></div>
                                            <div style="width:75px; margin-right:5px; line-height:28px; float:left" align="right">Cadastrado:</div>
                                            <div style="width:106px; float:left"><input id="txtDataCadastro" name="txtDataCadastro" type="text" class="input-medium" value="<?php echo $data_cadastro; ?>" disabled/></div>
                                        </td>
                                        </tr>
                                        <tr>
                                            <td width="122" align="right">Nome Fantasia:</td>
                                            <td width="212"><input name="txtFantasia" id="txtFantasia" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="128" value="<?php echo $fantasia; ?>"/></td>
                                            <td width="100" align="right">Indicação:</td>
                                            <td><select style="width:100%" class="input-medium" <?php echo $disabled; ?> name="txtIndicador" id="txtIndicador" >
                                                    <option value="null">Selecione</option>
                                                    <?php
                                                    $cur = odbc_exec($con, "select id_fornecedores_despesas,razao_social from sf_fornecedores_despesas where id_fornecedores_despesas > 0 and tipo = 'I' order by razao_social") or die(odbc_errormsg());
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo $RFP['id_fornecedores_despesas'] ?>"<?php
                                                        if (!(strcmp($RFP['id_fornecedores_despesas'], $indicador))) {
                                                            echo "SELECTED";
                                                        }
                                                        ?>><?php echo utf8_encode($RFP['razao_social']) ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">Atividade:</td>
                                            <td><input id="txtProfissao" <?php echo $disabled; ?> name="txtProfissao" type="text" class="input-medium" maxlength="30" value="<?php echo $profissao; ?>"/></td>
                                            <td align="right">CNPJ/CPF:</td>
                                            <td><input onBlur="Validar(this)" id="txtCnpj" <?php echo $disabled; ?> name="txtCnpj" type="text" class="input-medium" onkeypress='mascaraMutuario(this, cpfCnpj)' onblur='clearTimeout()' maxlength="18" value="<?php echo $cnpj; ?>"/></td>
                                        </tr>
                                        <tr>
                                            <td align="right">Endereço:</td>
                                            <td colspan="3"><input name="txtEndereco" id="txtEndereco" <?php echo $disabled; ?> type="text" class="input-medium"  maxlength="256" value="<?php echo $endereco; ?>"/></td>
                                        </tr>
                                        <tr>
                                            <td align="right">Número:</td>
                                            <td><input style="width:100px" id="txtNumero" <?php echo $disabled; ?> name="txtNumero" type="text" class="input-medium" maxlength="6" value="<?php echo $numero; ?>"/></td>
                                            <td align="right">Complemento:</td>
                                            <td><input id="txtComplemento" <?php echo $disabled; ?> name="txtComplemento" type="text" class="input-medium" maxlength="20" value="<?php echo $complemento; ?>"/></td>
                                        </tr>
                                        <tr>
                                            <td align="right">Bairro:</td>
                                            <td><input id="txtBairro" name="txtBairro" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="20" value="<?php echo $bairro; ?>"/></td>
                                            <td align="right">Estado:</td>
                                            <td>
                                                <select style="width:100%" class="input-medium" <?php echo $disabled; ?> name="txtEstado" id="txtEstado" >
                                                    <option value="" >Selecione o estado</option>
                                                    <?php
                                                    $cur = odbc_exec($con, "select estado_codigo,estado_nome from tb_estados where estado_codigo > 0 order by estado_nome") or die(odbc_errormsg());
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo $RFP['estado_codigo'] ?>"<?php
                                                        if (!(strcmp($RFP['estado_codigo'], $estado))) {
                                                            echo "SELECTED";
                                                        }
                                                        ?>><?php echo utf8_encode($RFP['estado_nome']) ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">Cidade:</td>
                                            <td><span id="carregando" name="carregando" style="color:#666;display:none">Aguarde, carregando...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                <select style="width:100%" name="txtCidade" id="txtCidade" class="input-medium" <?php echo $disabled; ?> >
                                                    <option value="" >Selecione a cidade</option>
                                                    <?php
                                                    if ($estado != '') {
                                                        $sql = "select cidade_codigo,cidade_nome from tb_cidades where cidade_codigo > 0 AND cidade_codigoEstado = " . $estado . " ORDER BY cidade_nome";
                                                        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                                            <option value="<?php echo $RFP['cidade_codigo'] ?>"<?php
                                                            if (!(strcmp($RFP['cidade_codigo'], $cidade))) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>><?php echo utf8_encode($RFP['cidade_nome']) ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                </select>
                                            </td>
                                            <td align="right">CEP:</td>
                                            <td><input style="width:100px" id="txtCep" name="txtCep" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="9" value="<?php echo $cep; ?>"/></td>
                                        </tr>
                                        <tr>
                                            <td align="right">Procedência:</td>
                                            <td>
                                                <select style="width:100%" class="input-medium" <?php echo $disabled; ?> name="txtProcedencia" id="txtProcedencia" >
                                                    <option value="">Não Informado</option>
                                                    <?php
                                                    $cur = odbc_exec($con, "select * from sf_procedencia where tipo_procedencia = 0 order by nome_procedencia") or die(odbc_errormsg());
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo $RFP['id_procedencia'] ?>"<?php
                                                        if (!(strcmp($RFP['id_procedencia'], $procedencia))) {
                                                            echo "SELECTED";
                                                        }
                                                        ?>><?php echo utf8_encode($RFP['nome_procedencia']) ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                            <td align="right">Grupo:</td>
                                            <td>
                                                <select class="input-medium" style="width:220px;" <?php echo $disabled; ?> name="txtGrupoPessoas" id="txtGrupoPessoas" >
                                                    <option value="" >Selecione o Grupo</option>
                                                    <?php
                                                    $cur = odbc_exec($con, "select id_grupo,descricao_grupo from dbo.sf_grupo_cliente where tipo_grupo = 'C' and inativo_grupo = 0 order by descricao_grupo") or die(odbc_errormsg());
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <option value="<?php echo $RFP['id_grupo'] ?>"<?php
                                                        if (!(strcmp($RFP['id_grupo'], $grupopes))) {
                                                            echo "SELECTED";
                                                        }
                                                        ?>><?php echo utf8_encode($RFP['descricao_grupo']) ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                        <input name="txtContId" id="txtContId" value="<?php echo $contId; ?>" type="hidden"/>
                                        <td align="right">Contato:</td>
                                        <td><input id="txtContNome" <?php echo $disabled; ?> name="txtContNome" type="text" class="input-medium" maxlength="100" value="<?php echo $contNome; ?>"/></td>
                                        <td align="right">Conta Corrente:</td>
                                        <td><input id="txtContFunc" <?php echo $disabled; ?> name="txtContFunc" type="text" class="input-medium" maxlength="100" value="<?php echo $contFunc; ?>"/></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab2">
                                    <div class="toolbar dark" style="background-color: #DCDCDC">
                                        <div class="input-prepend input-append">
                                        </div>
                                        <table width="99%" border="0" cellpadding="3" class="textosComuns">
                                            <tr>
                                                <td>
                                                    <select id="txtTipoContato" name="txtTipoContato" onChange="alterarMask()" <?php echo $disabled; ?>>
                                                        <option value="0">Telefone</option>
                                                        <option value="1">Celular</option>
                                                        <option value="2">E-mail</option>
                                                        <option value="3">Site</option>
                                                        <option value="4">Outros</option>
                                                    </select>
                                                </td>
                                                <td id="sprytextfield10">
                                                    <input id="txtTextoContato" type="text" <?php echo $disabled; ?>/>
                                                </td>
                                                <td width="80" align="right">
                                                    <button type="button" onclick="addContato();" <?php echo $disabled; ?> class="btn dblue">Incluir <span class="icon-arrow-next icon-white"></span></button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="data dark npr npb" style="overflow:hidden">
                                        <div id="divContatos" class="messages" style="height:273px; overflow-y:scroll"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="spanBNT" style="width:739px; border-top:solid 1px #CCC; overflow:hidden; margin:0; padding:0; bottom:1px; position:absolute;">
                            <div class="toolbar bottom tar">
                                <div class="data-fluid" style="overflow:hidden; margin-top:10px; margin-right:10px;" >
                                    <div class="btn-group">
                                        <?php if (is_numeric($_GET['id'])) { ?>
                                            <button class="btn btn-warning" type="button" name="bntVoltar" id="bntVoltar" onClick="history.go(window.top.location.href = 'CRMForm.php?id=<?php echo $_GET['id']; ?>')" value="Atendimentos"><span class="ico-backward"> </span>Atendimentos</button>
                                        <?php } if ($disabled == '') { ?>
                                            <button class="btn btn-success" type="submit" name="bntSave" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                                            <?php if ($_POST['txtId'] == '') { ?>
                                                <button class="btn btn-success" onClick="<?php
                                                if ($FinalUrl != "") {
                                                    echo "parent.FecharBox(3)";
                                                } else {
                                                    echo "parent.FecharBox(0)";
                                                }
                                                ?>" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                                                    <?php } else { ?>
                                                <button class="btn btn-success" type="submit" name="bntAlterar" id="bntOK" ><span class="ico-reply"></span> Cancelar</button>
                                                <?php
                                            }
                                        } else { 
                                            if (($_GET['id'] != '' || $_POST['txtId'] != '') && $ckb_crm_cpc_ == 1) { ?>
                                                <button class="btn btn-primary" onClick="return confirm('Deseja converter esse prospect para cliente?')" style="float:left" type="submit" name="bntConvert" id="bntConvert" value="Converter"><span class="ico-checkmark"></span> Converter</button>
                                            <?php } ?>                                                
                                            <button class="btn btn-success" type="submit" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                                            <button class="btn btn-success" type="submit" name="bntEdit" id="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                                            <button class="btn red" type="submit" name="bntDelete" id="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="block">
                </div>
            </div>
        </div>
    </div>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
    <script src="../../../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/jquery.cpfcnpj.min.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script LANGUAGE="javascript">
        
        var idLinhaContato = $("#txtQtdContatos").val();
        
        function Validar(codigo){
            if validate_cpf(codigo) {
                return true;
            }else if validate_cnpj(codigo) {
                return true;
            }else{
                return false;
            }
        }

        function mascaraMutuario(o, f) {
            v_obj = o;
            v_fun = f;
            setTimeout('execmascara()', 1);
        }

        function execmascara() {
            v_obj.value = v_fun(v_obj.value);
        }

        function cpfCnpj(v) {
            v = v.replace(/\D/g, "");
            if (v.length <= 11) {
                v = v.replace(/(\d{3})(\d)/, "$1.$2");
                v = v.replace(/(\d{3})(\d)/, "$1.$2");
                v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
            } else {
                v = v.replace(/^(\d{2})(\d)/, "$1.$2");
                v = v.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
                v = v.replace(/\.(\d{3})(\d)/, ".$1/$2");
                v = v.replace(/(\d{4})(\d)/, "$1-$2");
            }
            return v;
        }

        function alterarMask() {
            $('#txtTextoContato').val("");
            var tipo = $("#txtTipoContato option:selected").val();
            if (sprytextfield10 !== null) {
                sprytextfield10.reset();
                sprytextfield10.destroy();
                sprytextfield10 = null;
            }
            if (tipo === "0") {
                sprytextfield10 = new Spry.Widget.ValidationTextField("sprytextfield10", "custom", {pattern: "(00) 0000-0000", useCharacterMasking: true, isRequired: false});
            } else if (tipo === "1") {
                sprytextfield10 = new Spry.Widget.ValidationTextField("sprytextfield10", "custom", {pattern: "(00) 00000-0000", useCharacterMasking: true, isRequired: false});
            }
        }
        
        function addContato() {
            var msg = $("#txtTextoContato").val();
            var sel = $("#txtTipoContato").val();
            if (msg !== "") {
                var arb = msg + "@";
                if (sel != 2 || (sel == 2 && arb.match(/@/g).length == 2)) {
                    addLinhaContato($("#txtTipoContato").val(), $("#txtTipoContato option:selected").text(), $("#txtTextoContato").val(), "0");
                } else {
                    bootbox.alert("Preencha um endereço de e-mail corretamente!")
                }
            } else {
                bootbox.alert("Preencha as informações de contato corretamente!")
            }
        }
        
        function addLinhaContato(idTipo, tipo, conteudo, id) {
            var btExcluir = "";
            <?php if ($disabled != "") { ?>
                btExcluir = '<div style="width:5%; padding:4px 0 3px; float:right"></div>';
            <?php } else { ?>
                btExcluir = '<div style="width:5%; padding:4px 0 3px; float:right" onClick="return removeLinha(\'tabela_linha_' + idLinhaContato + '\');"><span class="ico-remove" style="font-size:20px"></span></div>';
            <?php } ?>
            var linha = "";
            linha = '<div id="tabela_linha_' + idLinhaContato + '" style="padding:5px; border-bottom:1px solid #DDD">\n\
                        <input id="txtIdContato_' + idLinhaContato + '" name="txtIdContato_' + idLinhaContato + '" value="' + id + '" type="hidden"></input>\n\
                        <input id="txtTpContato_' + idLinhaContato + '" name="txtTpContato_' + idLinhaContato + '" value="' + idTipo + '" type="hidden"></input>\n\
                        <div style="width:32%; padding-left:1%; line-height:27px; float:left">' + tipo + '</div>\n\
                        <input id="txtTxContato_' + idLinhaContato + '" name="txtTxContato_' + idLinhaContato + '" value="' + conteudo + '" type="hidden"></input>\n\
                        ' + btExcluir + '<div style="width:60%; line-height:27px; float:left">' + conteudo + '</div></div>';
            $("#divContatos").prepend(linha);
            idLinhaContato++;
            $("#txtQtdContatos").val(idLinhaContato);
            $("#txtTextoContato").val("");
        }
        
        function removeLinha(id) {
            $("#" + id).remove();
        }

        $(document).ready(function () {
            listContatos();
        });
        
        function listContatos() {
            $("#divContatos").html("");
            $.getJSON('../Comercial/FormGerenciador-Clientes.contatos.ajax.php?search=', {cod: $("#txtId").val(), ajax: 'true'}, function (j) {
                for (var i = 0; i < j.length; i++) {
                    addLinhaContato(j[i].tipo_contato, j[i].texto_contatos, j[i].conteudo_contato, j[i].id)
                }
            });
        }

        $(function () {
            $('#txtEstado').change(function () {
                if ($(this).val()) {
                    $('#txtCidade').hide();
                    $('#carregando').show();
                    $.getJSON('locais.ajax.php?search=', {txtEstado: $(this).val(), ajax: 'true'}, function (j) {
                        var options = '<option value=""></option>';
                        for (var i = 0; i < j.length; i++) {
                            options += '<option value="' + j[i].cidade_codigo + '">' + j[i].cidade_nome + '</option>';
                        }
                        $('#txtCidade').html(options).show();
                        $('#carregando').hide();
                    });
                } else {
                    $('#txtCidade').html('<option value="">Selecione a cidade</option>');
                }
            });
        });
    </script>    
    <?php odbc_close($con); ?>
</body>
