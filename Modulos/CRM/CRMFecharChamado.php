<?php
include "../../Connections/configini.php";
$id = "";
$dt_inicio = getData("T");
$hora_inicio = date("H:i");
$procedencia = "null";
$assunto = "";
$queryX = "select id_usuario id, nome from sf_usuarios where inativo = 0 order by nome";
$pessoa = $_SESSION["id_usuario"];
$fechar = 0;

if (is_numeric($_GET['fx'])) {
    $fechar = $_GET['fx'];
}

if (is_numeric($_GET['tp'])) {
    if ($_GET['tp'] == 1) {
        $title = "Fechar Chamado";
        $acao = "Solução";
        $tipo = "Motivo";
        $query = "select id_procedencia id, nome_procedencia nome from sf_procedencia where tipo_procedencia = 1 order by nome_procedencia";
        $botao = "bntFecharChamado";
    } else if ($_GET['tp'] == 2) {
        $title = "Histórico de Ações";
        $acao = "Observação";
        $tipo = "Atendente";
        $query = $queryX;
        $procedencia = $procedencia;
        $botao = "bntAddMensagem";
    }
}

if (is_numeric($_GET['id'])) {
    $sql = "SELECT id_usuario_de,id_usuario_para,data, mensagem FROM sf_mensagens_telemarketing where id = " . $_GET['id'];
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $_GET['id'];
        $dt_inicio = escreverData($RFP['data']);
        $hora_inicio = escreverData($RFP['data'], 'H:i');
        $procedencia = $RFP['id_usuario_para'];
        $assunto = utf8_encode($RFP['mensagem']);
    }
}

$pendente = "N";
if (is_numeric($_GET['idx'])) {
    $sql = "select sf_vendas.id_venda,cov,sf_vendas.status, de_pessoa, para_pessoa from sf_telemarketing
        left join sf_vendas on sf_vendas.id_venda = sf_telemarketing.id_venda
        where id_tele = " . $_GET['idx'];
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        if ($pessoa == $RFP['de_pessoa']) {
            $procedencia = $RFP['para_pessoa'];
        } else {
            $procedencia = $RFP['de_pessoa'];
        }
        if ($RFP['cov'] == "O" && $RFP['status'] == "Aguarda") {
            $pendente = "S";
        }
    }
} ?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<style>
    body { font-family: sans-serif; }
    .selected { background: #acbad4 !important;}
</style>
<body>
    <div class="row-fluid">
        <form>
            <div class="frmhead">
                <div class="frmtext"><?php echo $title; ?></div>
                <div class="frmicon" onClick="parent.FecharBox(<?php echo $fechar; ?>)">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                <div style="width:20%; float:left; margin-left:1%">
                    <span>Data Início:</span>
                    <input id="txtDataFechar" class="datepicker inputCenter" type="text" value="<?php echo $dt_inicio; ?>">
                </div>
                <div style="width:15%; float:left; margin-left:1%">
                    <span>Horário:</span>
                    <input id="txtHoraFechar" type="text" class="input-medium" value="<?php echo $hora_inicio; ?>">
                </div>
                <div style="width:61%; float:left; margin-left:1%">
                    <span><?php echo $tipo; ?>:</span>
                    <select id="txtMotivo" style="width:100%" class="select input-medium" <?php echo $disabled; ?>>
                        <option value="null">Não Informado</option>
                        <?php $cur = odbc_exec($con, $query) or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option value="<?php echo $RFP['id']; ?>"<?php
                            if (!(strcmp($RFP['id'], $procedencia))) {
                                echo "SELECTED";
                            }
                            ?>><?php echo utf8_encode($RFP['nome']); ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div style="clear:both; height:5px"></div>
                <div style="width:100%; margin:10px 0 0">
                    <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px"><?php echo $acao; ?></span>
                    <span style="float: right;<?php
                    if ($_GET['tp'] == 2) {
                        echo "display: none;";
                    }
                    ?>"><input id="CkbTransferir" type="checkbox" value="1"> Transferir Atendimento</span>
                    <hr style="margin:2px 0; border-top:1px solid #ddd">
                </div>
                <div class="body" style="margin-left:0; padding:0">
                    <div class="content" style="min-height:120px; margin-top:0">
                        <textarea id="txtObsFechamento" style="width:100%; height:120px"><?php echo $assunto; ?></textarea>
                        <div id="dvNovoChamado" style="display: none;margin-top: 8px;">
                            <div style="width:20%; float:left; margin-left:1%">
                                <span>Prazo:</span>
                                <input id="txtDataPrazo" class="datepicker inputCenter" type="text" value="<?php echo $dt_inicio; ?>">
                            </div>
                            <div style="width:15%; float:left; margin-left:1%">
                                <span>Horário:</span>
                                <input id="txtHoraPrazo" type="text" class="input-medium" value="<?php echo $hora_inicio; ?>">
                            </div>
                            <div style="width:31%; float:left; margin-left:1%">
                                <span>Encaminhado:</span>
                                <select id="txtPara" style="width:100%" class="select input-medium" <?php echo $disabled; ?>>
                                    <option value="null">Não Informado</option>
                                    <?php $cur = odbc_exec($con, $queryX) or die(odbc_errormsg());
                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                        <option value="<?php echo $RFP['id']; ?>"<?php
                                        if (!(strcmp($RFP['id'], $pessoa))) {
                                            echo "SELECTED";
                                        }
                                        ?>><?php echo utf8_encode($RFP['nome']); ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div style="width:29%; float:left; margin-left:1%">
                                <span>Tipo:</span>
                                <select id="txtTipo" class="select input-medium" style="width:100%">
                                    <option value="T" <?php
                                    if ($tipoChamado == "T") {
                                        echo "SELECTED";
                                    }
                                    ?>>Telefone</option>
                                    <option value="E" <?php
                                    if ($tipoChamado == "E") {
                                        echo "SELECTED";
                                    }
                                    ?>>Email</option>
                                    <option value="V" <?php
                                    if ($tipoChamado == "V") {
                                        echo "SELECTED";
                                    }
                                    ?>>Visita</option>
                                    <option value="S" <?php
                                    if ($tipoChamado == "S") {
                                        echo "SELECTED";
                                    }
                                    ?>>SMS</option>
                                    <option value="P" <?php
                                    if ($tipoChamado == "P") {
                                        echo "SELECTED";
                                    }
                                    ?>>Presencial</option>
                                    <option value="O" <?php
                                    if ($tipoChamado == "O") {
                                        echo "SELECTED";
                                    }
                                    ?>>Outro</option>
                                    <option value="W" <?php
                                    if ($tipoChamado == "W") {
                                        echo "SELECTED";
                                    }
                                    ?>>Whatsapp</option>                                    
                                    <option value="R" <?php
                                    if ($tipoChamado == "R") {
                                        echo "SELECTED";
                                    }
                                    ?>>Portal</option>                                    
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <button class="btn btn-success" type="button" onClick="salvar()" id="btnGravarPlano" title="Gravar"><span class="ico-checkmark"></span> Gravar</button>
                    <button class="btn yellow" title="Cancelar" onClick="parent.FecharBox(<?php echo $fechar; ?>)"><span class="ico-reply"></span> Cancelar</button>
                </div>
            </div>
        </form>
    </div>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type="text/javascript">
        
        $("#txtDataPrazo, #txtDataFechar").mask(lang["dateMask"]);
        $("#txtHoraPrazo, #txtHoraFechar").mask("99:99");
        
        $(document).ready(function () {
            $('#CkbTransferir').click(function () {
                if ($(this).is(':checked')) {
                    $("#txtObsFechamento").css("height", "80");
                    $("#dvNovoChamado").show();
                } else {
                    $("#txtObsFechamento").css("height", "120");
                    $("#dvNovoChamado").hide();
                }
            });
        });
        
        function salvar() {
            if ("<?php echo $pendente; ?>" === "N" || <?php echo $_GET['tp']; ?> === 2) {
                if ($("#txtObsFechamento").val() !== "" && parent.$('#txtIdx').val() !== "") {
                    $.post("form/CRMFormServerTelemarketing.php", {txtId: $('#txtId').val(), txtIdx: parent.$('#txtIdx').val(),
                        txtDataFechar: $('#txtDataFechar').val(), txtHoraFechar: $('#txtHoraFechar').val(), txtMotivo: $('#txtMotivo').val(),
                        txtObsFechamento: $('#txtObsFechamento').val(), txtQualificacao: parent.$("#etapa2 .subtxt").text(), <?php echo $botao; ?>: "S"}).done(function (data) {
                        if (data === "YES") {
                            if ($('#CkbTransferir').is(':checked')) {
                                $.post("form/CRMFormServerTelemarketing.php", {txtId: parent.$('#txtId').val(), txtAS: <?php echo ($fechar == 2 ? "1" : "0"); ?>,
                                    txtData: $('#txtDataFechar').val(), txtHora: $('#txtHoraFechar').val(), txtTipoChamado: $('#txtTipo').val(),
                                    txtDataProximo: $('#txtDataPrazo').val(), txtHoraProximo: $('#txtHoraPrazo').val(), txtDeHistorico: <?php echo $pessoa; ?>,
                                    txtPara: $('#txtPara').val(), txtRelato: $('#txtObsFechamento').val(), txtQualificacao: parent.$("#etapa2 .subtxt").text(),
                                    ckbAcompanhar: 0, bntSave: "S"}).done(function (data) {
                                    if (data === "YES") {
                                        parent.FecharBox(<?php echo $fechar; ?>);
                                    } else {
                                        alert(data);
                                    }
                                });
                            } else {
                                parent.FecharBox(<?php echo $fechar; ?>);
                            }
                        } else {
                            alert(data);
                        }
                    });
                } else {
                    bootbox.alert("Preencha as informações de Solução corretamente!");
                }
            } else {
                bootbox.alert("Não é possível encerrar esta pendência, pois existe um orçamento aguardando aprovação!");
            }
        }
                
    </script>    
    <?php odbc_close($con); ?>
</body>
