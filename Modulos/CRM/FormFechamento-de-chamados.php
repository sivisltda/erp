<?php
include '../../Connections/configini.php';
$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_procedencia set 
        nome_procedencia = " . valoresTexto("txtDescricao") . "
        where tipo_procedencia = 1 and id_procedencia = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_procedencia(nome_procedencia,tipo_procedencia)values(" .
        valoresTexto("txtDescricao") . ",1)") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_procedencia from sf_procedencia where tipo_procedencia = 1 order by id_procedencia desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_procedencia WHERE tipo_procedencia = 1 and id_procedencia = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select id_procedencia,nome_procedencia from sf_procedencia where id_procedencia =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_procedencia']);
        $descricao = utf8_encode($RFP['nome_procedencia']);
    }
} else {
    $disabled = '';
    $id = '';
    $descricao = '';
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <form action="FormFechamento-de-chamados.php" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Fechamento de Chamados</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont">
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <input name="txtFinalUrl" id="txtFinalUrl" value="<?php echo $FinalUrl; ?>" type="hidden"/>
            <div style="width: 100%;float: left">
                <span>Descrição:</span>
                <input name="txtDescricao" id="txtDescricao" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $descricao; ?>"/>
            </div>            
            <div style="clear:both;height: 5px;"></div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" name="bntSave" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK" ><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else { ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <?php odbc_close($con); ?>
</body>
