<?php
    include "../../Connections/configini.php";
    include "form/CRMFormServer.php";
    if ($tipo != "J" && $mdl_aca_ == 1) {
        $tipo = "F";
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
        <title>Cadastro de Clientes</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>        
        <link href="../../js/plugins/jquery.ajax-combobox/dist/jquery.ajax-combobox.css" rel="stylesheet">        
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>        
        <link href="css/telemarketing.css" rel="stylesheet" type="text/css"/>
        <style media="screen">
            form #txtDCCNumCartao {
                background-image: url(../../img/card.png);
                background-position: 0px -114px;
                background-size: 100px 391px;
                background-repeat: no-repeat;
                padding-left: 54px;
            }
            form #txtDCCNumCartao.amex {
                background-position: 0px -59px;
            }
            form #txtDCCNumCartao.diners_club_carte_blanche {
                background-position: 0px -87px;
            }
            form #txtDCCNumCartao.diners_club_international {
                background-position: 0px -87px;
            }
            form #txtDCCNumCartao.visa_electron {
                background-position: 0px -186px;
            }
            form #txtDCCNumCartao.visa {
                background-position: 0px -150px;
            }
            form #txtDCCNumCartao.mastercard {
                background-position: 0px -223px;
            }
            form #txtDCCNumCartao.maestro {
                background-position: 0px -259px;
            }
            form #txtDCCNumCartao.banescard {
                background-position: 0px -355px;
            }            
            form #txtDCCNumCartao.discover {
                background-position: 0px -295px;
            }
            form #txtDCCNumCartao.elo {
                background-position: 0px -30px;
            }
            form #txtDCCNumCartao.aura {
                background-position: 0px -1px;
            }
            form #txtDCCNumCartao.hiper {
                background-position: 0px -324px;
            }
            .ttermometro {
                float: left;                 
                width: 100%;
                height: 25px;                
                font-size: initial;
                font-weight: 600;
                text-align: center;                
            }
            .termometro {
                float: left; 
                width: 20%;
                height: 22px;
                text-align: center;
                font-size: x-large;
                line-height: 0;
            }
            .frio {
                float: right; 
                width: 5%; 
                text-align: center; 
                color: #66cff6; 
                margin-top: 27px;
            }
            .quente {
                float: right; 
                width: 5%; 
                text-align: center; 
                color: #f58487; 
                margin-top: 27px;
            }
            #temp0 {
                border-top-left-radius: 31px;
                border-bottom-left-radius: 31px;                
                background-color: #66cff6;
                cursor: pointer;
            }
            #temp1 {
                background-color: #bde8fb;
                cursor: pointer;                
            }
            #temp2 {
                background-color: #fff871;
                cursor: pointer;                
            }
            #temp3 {
                background-color: #f9b785;
                cursor: pointer;                
            }
            #temp4 {
                border-top-right-radius: 31px;
                border-bottom-right-radius: 31px;                
                background-color: #f58487;
                cursor: pointer;                
            }          
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1 style="margin-top:8px"><?php echo ($typePage == "" ? "Clientes" : $typePage);?></h1>
                    </div>
                    <div class="row-fluid">
                        <form name="frmCRMForm" id="frmCRMForm">
                            <div class="span12">
                                <div class="block">
                                    <div id="formulario">
                                        <div style="background:#F6F6F6; border:1px solid #DDD; padding:10px">
                                            <div style="width:150px;height:150px; padding:0 2% 0 1%; float:left">
                                                <input id="btnRemImg" class="btn red" type="button" value="x" style="<?php if(!file_exists("./../../Pessoas/".$contrato."/Clientes/".$id.".png")){ echo "display:none;"; } ?>margin:0px; padding:2px 4px 3px 4px; line-height:10px;position:absolute; left:24px; z-index:700; cursor:pointer">
                                                <img onclick="UploadForm()" id="ImgCliente" src="
                                                <?php
                                                    if($id != "" and file_exists("./../../Pessoas/".$contrato."/Clientes/".$id.".png")){
                                                        $source = file_get_contents("./../../Pessoas/".$contrato."/Clientes/".$id.".png");
                                                        echo "data:image/png;base64,".base64_encode($source);
                                                    } else {
                                                        echo "./../../img/img.png";
                                                    } ?>"
                                                 style="width:100%; height:150px; border-radius:50%; cursor:pointer"/>
                                            </div>                                            
                                            <div style="width:calc(97% - 150px); float:right; padding:8px 0">
                                                <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                                                <input name="txtAt" id="txtAt" value="<?php echo $_GET['at']; ?>" type="hidden"/>
                                                <input name="txtDisabled" id="txtDisabled" value="<?php echo $disabled; ?>" type="hidden"/>                                                
                                                <input name="txtSendEmail" id="txtSendEmail" type="hidden"/>
                                                <input name="txtSendSms" id="txtSendSms" type="hidden"/>
                                                <input name="txtAcessoDesc" id="txtAcessoDesc" type="hidden" value="<?php echo $ckb_out_desconto_; ?>"/>
                                                <input name="txtAcessoAlunBlock" id="txtAcessoAlunBlock" type="hidden" value="<?php echo $ckb_aca_cli_block; ?>"/>                                                
                                                <input name="txtPage" id="txtPage" value="<?php echo $txtPage; ?>" type="hidden"/>                                                
                                                <input name="txtTpCli" id="txtTpCli" value="<?php echo ($typePage == "Prospect" ? "P" : "C"); ?>" type="hidden"/>                                                
                                                <input id="txtReadOnly" value="<?php echo $ckb_adm_cli_read_; ?>" type="hidden"/>                                                                                                
                                                <div style="width:70%; float:left">
                                                    <span style="color:#FF0000">*</span><span id="lblNome">Nome Completo:</span>
                                                    <input name="mdlAca" id="mdlAca" value="<?php echo $mdl_aca_; ?>" type="hidden"/>
                                                    <input id="txtNome" name="txtNome" type="text" value="<?php echo $nome; ?>" style="height: 40px;font-size:18px; font-weight:500; font-family:'Segoe UI', arial, sans-serif;width: 100%" />
                                                </div>
                                                <div style="width:16%; float:left; margin-left:1%">
                                                    <span>Cadastro: <?php echo $usuario_cadastro; ?></span>
                                                    <input type="text" value="<?php
                                                    if ($dtCadastro != "") { echo $dtCadastro; } else { echo getData("T");} ?>" style="height:40px; font-size:18px; font-weight:500; font-family:'Segoe UI', arial, sans-serif" readonly/>
                                                </div>
                                                <div style="width:12%; float:left; margin-left:1%">
                                                    <span>Matricula:</span>
                                                    <input type="text" value="<?php if($id != ""){echo $id; }?>" style="height: 40px;font-size:18px; font-weight:500; font-family:'Segoe UI', arial, sans-serif" readonly />
                                                </div>
                                                <div style="height:10px; clear:both"></div>
                                                <div>
                                                    <div id="etapa1" class="etapa ativa">
                                                        <div class="numero">1</div>
                                                        <div class="texto">Prospect</div>
                                                        <div class="subtxt"><?php echo $dtCadastro; ?></div>
                                                    </div>                                                    
                                                    <div id="etapa4" class="etapa <?php echo ($dtConvCliente != "" ? "ativa" : "");?>" onclick="ativaEtapa(4, '<?php echo $dtConvCliente; ?>')">
                                                        <div class="numero">2</div>
                                                        <div class="texto">Cliente</div>
                                                        <div class="subtxt"><?php echo $dtConvCliente; ?></div>
                                                    </div>
                                                </div>
                                                <input name="txtTermometro" id="txtTermometro" value="<?php echo $termometro; ?>" type="hidden"/>                                                
                                                <?php if ($typePage == "Prospect") { ?>
                                                <div class="quente">Quente</div>
                                                <div style="float: right; width: 25%;">
                                                    <?php if ($statusAluno == "Aguardando") { ?>
                                                        <span class="ttermometro" style="color: orange;">AGUARDANDO</span>
                                                    <?php } else if ($statusAluno == "Negociacao") { ?>
                                                        <span class="ttermometro" style="color: #22b14c;">EM NEGOCIAÇÃO</span>
                                                    <?php } else if ($statusAluno == "Analise") { ?>
                                                        <span class="ttermometro" style="color: #56acab;">EM ANÁLISE</span>
                                                    <?php } else if ($statusAluno == "Orcamento") { ?>
                                                        <span class="ttermometro" style="color: #0099cc;">EM ORÇAMENTO</span>
                                                    <?php } else if ($statusAluno == "Bloqueado") { ?>
                                                        <span class="ttermometro" style="color: #333;">BLOQUEADO</span>
                                                    <?php } else if ($statusAluno == "Inativo") { ?>
                                                        <span class="ttermometro" style="color: red;">INATIVO</span>
                                                    <?php } ?>                                                    
                                                    <div id="temp0" onclick="temperatura(0);" class="termometro"></div>
                                                    <div id="temp1" onclick="temperatura(1);" class="termometro"></div>
                                                    <div id="temp2" onclick="temperatura(2);" class="termometro"></div>
                                                    <div id="temp3" onclick="temperatura(3);" class="termometro"></div>
                                                    <div id="temp4" onclick="temperatura(4);" class="termometro"></div>
                                                    <div id="temps0" onclick="temperatura(0);" class="termometro"></div>
                                                    <div id="temps1" onclick="temperatura(1);" class="termometro"></div>
                                                    <div id="temps2" onclick="temperatura(2);" class="termometro"></div>
                                                    <div id="temps3" onclick="temperatura(3);" class="termometro"></div>
                                                    <div id="temps4" onclick="temperatura(4);" class="termometro"></div>
                                                </div>
                                                <div class="frio">Frio</div>                                                  
                                                <?php } ?>
                                            </div>
                                            <div style="clear:both"></div>
                                        </div>
                                        <div class="data-fluid tabbable" style="margin-top:10px; margin-bottom:15px;">
                                            <ul class="nav nav-tabs" style="margin-bottom:0">
                                                <li <?php echo (isset($_GET["at"]) ? "" : "class=\"active\"") ?>><a href="#tab1" data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Dados Pessoais</b></a></li>
                                                <?php if ($mdl_seg_ > 0) {?>
                                                    <li><a href="#tab0" <?php echo $abas; ?> data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Veículos</b></a></li>
                                                <?php } if ($_SESSION["mod_emp"] != 1) { ?>                                                        
                                                    <?php if ($mdl_seg_ == 0){?>
                                                        <li><a href="#tab22" <?php echo $abas; ?> data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Outros</b></a></li>
                                                    <?php }?>
                                                <?php } ?>
                                                    <li <?php echo (isset($_GET["at"]) ? "class=\"active\"" : "") ?>><a href="#tab2" <?php echo $abas; ?> data-toggle="tab" onClick="$('#tbDCC').hide(); limparChamado();"><b>Atendimentos</b></a></li>                                                    
                                                <?php if ($dtConvCliente != "") { ?>
                                                    <li><a href="#tab3" <?php echo $abas; ?> data-toggle="tab" onClick="$('#tbDCC').show();"><b>DCC</b></a></li>
                                                <?php } if ($aca_prosp_pag > 0) { ?>                                                        
                                                    <li><a href="#tab4" <?php echo $abas; ?> data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Planos</b></a></li>                                                        
                                                <?php } ?>                                                    
                                                    <li><a href="#tab5" <?php echo $abas; ?> data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Documentos</b></a></li>
                                                    <li><a href="#tab17" <?php echo $abas; ?> data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Agendamentos</b></a></li>                                                    
                                                <?php if ($ckb_fin_fix_ == 1 && $dtConvCliente != "" && $_SESSION["mod_emp"] != 1) { ?>
                                                    <li><a href="#tab25" <?php echo $abas; ?> data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Contratos</b></a></li>
                                                <?php } if ($mdl_ser_ > 0 && $ckb_ser_ser_ == 1) { ?>
                                                    <li><a href="#tab18" <?php echo $abas; ?> data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Serviços</b></a></li>
                                                <?php } if ($ckb_est_lorc_ == 1 && $_SESSION["mod_emp"] == 0) { ?>
                                                    <li><a href="#tab24" <?php echo $abas; ?> data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Orçamentos</b></a></li>
                                                <?php } if ($mdl_aca_ == 1 && $mdl_seg_ == 0) { ?>
                                                    <li><a href="#tab6" <?php echo $abas; ?> data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Acesso</b></a></li>
                                                <?php } if($_SESSION["mod_emp"] == 1){ ?>
                                                    <li><a href="#tab7" <?php echo $abas; ?> data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Convênios</b></a></li>
                                                <?php } if ($id != "") { ?>
                                                    <li><a href="#tab9" <?php echo $abas; ?> data-toggle="tab" onClick="$('#tbDCC').hide();"><b>Campos Livres</b></a></li>
                                                <?php } ?>
                                            </ul>
                                            <div class="tab-content" style="overflow:initial; border:1px solid #DDD; border-top:0; padding-top:10px; background:#F6F6F6">
                                                <div class="tab-pane <?php echo (isset($_GET["at"]) ? "" : "active") ?>" id="tab1" style="margin-bottom:5px">
                                                    <?php include "./../Academia/include/DadosPessoais.php";?>                                                    
                                                </div>
                                                <div class="tab-pane <?php echo (isset($_GET["at"]) ? "active" : "") ?>" id="tab2" style="margin-bottom:5px">
                                                    <?php include "include/CRMFormTelemarketing.php"; ?>
                                                </div>
                                                <div class="tab-pane" id="tab3" style="margin-bottom:5px">
                                                    <?php include "./../Academia/include/CartaoDcc.php"; ?>
                                                </div>           
                                                <div class="tab-pane" id="tab4" style="margin-bottom:5px">
                                                    <?php include "./../Academia/include/AcademiaPlanos.php"; ?>
                                                </div>
                                                <div class="tab-pane" id="tab5" style="margin-bottom:5px">
                                                    <?php include "include/Documentos.php"; ?>
                                                </div>                                                
                                                <div class="tab-pane" id="tab6" style="margin-bottom:5px">
                                                    <?php include "./../Academia/include/AcademiaAcesso.php"; ?>
                                                </div>
                                                <div class="tab-pane" id="tab7" style="margin-bottom:5px">
                                                    <?php include "./../Academia/include/Convenios.php"; ?>
                                                </div>
                                                <div class="tab-pane" id="tab9" style="margin-bottom:5px">
                                                    <?php include "./../Academia/include/CamposLivres.php";?>
                                                </div>                                                
                                                <div class="tab-pane" id="tab17" style="margin-bottom:5px">
                                                    <?php include "./../Academia/include/Agendamentos.php";?>
                                                </div>                                                
                                                <div class="tab-pane" id="tab18" style="margin-bottom:5px">
                                                    <?php include "include/CRMFormServicos.php"; ?>
                                                </div>                                                
                                                <div class="tab-pane" id="tab22" style="margin-bottom:5px">
                                                    <span style="color:#000; font-size:14px; text-shadow:0 1px #FFF; padding-left:5px">Endereço de Entrega</span>
                                                    <hr style="margin:2px 0; border-top:1px solid #DDD"/>
                                                    <div style="width:100%;">
                                                        <div style="background:#F1F1F1; border:1px solid #DDD; padding:10px;vertical-align: top;">
                                                            <input id="txtIdEnt" type="hidden" value="" />
                                                            <div style="width:58%; float:left">
                                                                <span>Nome Endereço:</span>
                                                                <input type="text" id="txtNomeEnt" class="input-medium" value=""/>
                                                            </div>
                                                            <div style="width:25%; float:left; margin-left:1%">
                                                                <span>Referência:</span>
                                                                <input type="text" id="txtReferenciaEnt" maxlength="256" class="input-medium" value=""/>
                                                            </div>
                                                            <div style="width:15%; float:left; margin-left:1%">
                                                                <span>Telefone:</span>
                                                                <input type="text" id="txtTelefoneEnt" maxlength="20" class="input-medium" value=""/>
                                                            </div>
                                                            <div style="clear:both; height:5px"></div>
                                                            <div style="width:16%; float:left">
                                                                <span>CEP:</span>
                                                                <input type="text" id="txtCepEnt" class="input-medium" value=""/>
                                                            </div>
                                                            <div style="width:41%; float:left; margin-left:1%">
                                                                <span>Logradouro:</span>
                                                                <input type="text" id="txtEnderecoEnt" maxlength="256" class="input-medium" value="" readonly/>
                                                            </div>
                                                            <div style="width:10%; float:left; margin-left:1%">
                                                                <span>Número:</span>
                                                                <input type="text" id="txtNumeroEnt" maxlength="6" class="input-medium" value=""/>
                                                            </div>
                                                            <div style="width:30%; float:left; margin-left:1%">
                                                                <span>Bairro:</span>
                                                                <input type="text" id="txtBairroEnt" maxlength="20" class="input-medium" value="" readonly/>
                                                            </div>
                                                            <div style="clear:both; height:5px"></div>
                                                            <div style="width:28.5%; float:left">
                                                                <span>Estado:</span>
                                                                <select class="select" id="txtEstadoEnt" style="width:100%" readonly>
                                                                    <option uf="" value="">Selecione o estado</option>
                                                                    <?php  $cur = odbc_exec($con, "select estado_codigo,estado_nome,estado_sigla from tb_estados where estado_codigo > 0 order by estado_nome") or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                        <option uf="<?php echo strtoupper(utf8_encode($RFP['estado_sigla'])); ?>" value="<?php echo $RFP['estado_codigo'] ?>"<?php
                                                                        if (!(strcmp($RFP['estado_codigo'], $estado))) {
                                                                            echo "SELECTED";
                                                                        }
                                                                        ?>><?php echo utf8_encode($RFP['estado_nome']) ?>
                                                                        </option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                            <div style="width:28.5%; float:left; margin-left:1%">
                                                                <span>Cidade:</span>
                                                                <select class="select" id="txtCidadeEnt" style="width:100%" readonly>
                                                                    <option value="">Selecione a cidade</option>
                                                                    <?php
                                                                    if ($estado != "") {
                                                                        $sql = "select cidade_codigo,cidade_nome from tb_cidades where cidade_codigo > 0 AND cidade_codigoEstado = " . $estado . " ORDER BY cidade_nome";
                                                                        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                        while ($RFP = odbc_fetch_array($cur)) {
                                                                            ?>
                                                                            <option value="<?php echo $RFP['cidade_codigo'] ?>"<?php
                                                                            if (!(strcmp($RFP['cidade_codigo'], $cidade))) {
                                                                                echo "SELECTED";
                                                                            }
                                                                            ?>><?php echo utf8_encode($RFP['cidade_nome']) ?>
                                                                            </option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div style="width:calc(40% - 83px); float:left; margin-left:1%">
                                                                <span>Complemento:</span>
                                                                <input type="text" id="txtComplementoEnt" class="input-medium" value=""/>
                                                            </div>
                                                            <div style="width:83px; float:left; margin-left:1%; padding-top:15px">
                                                                <button type="button" onclick="addEnderecoEntrega()" class="btn dblue" style="line-height:19px; width:83px; margin:0; background:#308698 !important">Salvar <span class="icon-arrow-next icon-white"></span></button>
                                                                <button type="button" onclick="limparEnd()" class="btn yellow" style="line-height:19px; width:83px; margin:0;margin-top: 5px;">Cancelar <span class="ico-reply"></span></button>
                                                            </div>
                                                            <div style="clear:both; height:5px"></div>
                                                        </div>
                                                    </div>
                                                    <div style="width:100%; margin-top:10px">
                                                        <table class="table" style="background:#FFF; border:1px solid #DDD" cellpadding="0" cellspacing="0" width="100%" id="enderecos">
                                                            <thead>
                                                                <tr>
                                                                    <th width="20%">Nome</th>
                                                                    <th width="75%">Endereço de Entrega</th>
                                                                    <th width="5%"><center>Ação</center></th>
                                                            </tr>
                                                            </thead>
                                                        </table>
                                                        <div style="clear:both"></div>
                                                    </div>
                                                </div>                                                
                                                <div class="tab-pane" id="tab24" style="margin-bottom:5px" >
                                                    <?php include "./../Academia/include/Orcamentos.php"; ?>
                                                </div>
                                                <div class="tab-pane" id="tab25" style="margin-bottom:5px">
                                                    <div style="background:#F1F1F1;border:1px solid #DDD;padding:10px;">
                                                        <button class="btn btn-success" type="button" title="Novo" name="btnNovoContrato" id="btnNovoContrato" hidden="" onClick="AbrirBox(0, 0, 0)"><span class="ico-plus-sign"></span> Novo</button>
                                                    </div>
                                                    <table class="table dataTable" cellpadding="0" cellspacing="0" width="100%" id="tblContratos" aria-describedby="example_info" style="width: 100%;">
                                                        <thead>
                                                            <tr role="row">
                                                                <th width="3%" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 18px;"></th>
                                                                <th width="5%" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 44px;"><center>Loja</center></th>
                                                                <th width="5%" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 44px;"><center>D/C</center></th>
                                                                <th width="10%" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 110px;">Fornec./Clientes.</th>
                                                                <th width="10%" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 110px;">Grupo:</th>
                                                                <th width="10%" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 110px;">Conta:</th>
                                                                <th width="11%" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 123px;">Histórico:</th>
                                                                <th width="8%" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 84px;">Tipo</th>
                                                                <th width="7%" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 72px;">Últ.Vnc.</th>
                                                                <th width="7%" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 72px;">Max.Parc.:</th>
                                                                <th width="7%" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 72px;">Min.Parc.:</th>
                                                                <th width="10%" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 110px;">Em Aber.:</th>
                                                                <th width="7%" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 72px;"><center>Ação:</center></th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                    <div style="clear:both; height:5px"></div>
                                                </div>
                                                <?php if ($mdl_seg_ > 0) {?>
                                                    <div class="tab-pane" id="tab0" style="margin-bottom:5px">
                                                        <?php include "./../Academia/include/Veiculos.php";?>
                                                    </div>
                                                <?}?>                                                
                                            </div>
                                        </div>
                                        <span id="tbDCC" style="display:none">
                                            <div class="boxhead">
                                                <div class="boxtext">Transações em DCC
                                                    <div style="font-size: 11px;display: inline-block;float: right;color: #666;padding: 0 10px;font-weight: bold;line-height: 30px;">Exibir Inválidos
                                                        <input name="ckb_dcc_invalidos" id="ckb_dcc_invalidos" value="1" type="checkbox" class="input-medium">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="boxtable" style="margin-bottom:0">
                                                <table id="tbListaDCC" class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th><center>Data Mens.</center></th>
                                                            <th>Matricula</th>
                                                            <th>Nm Cartao</th>
                                                            <th><center>Pedido</center></th>
                                                            <th><center>Transação</center></th>
                                                            <th><center>Cartão</center></th>
                                                            <th><center>Estado</center></th>
                                                            <th><center>Valor</center></th>
                                                            <th><center>Cod.Mod.</center></th>
                                                            <th><center>Recor.</center></th>
                                                            <th><center>Data Pgto.</center></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                                <div style="clear:both"></div>
                                            </div>
                                            <div class="boxfoot">
                                                <div class="boxtext">
                                                    <div style="float:left; width:170px">Total de <span id="lbl_dcc_total">0</span> Registros</div>
                                                    <div style="float:left; width:calc(50% - 170px); text-align:center; color:#c22439; font-size:14px">Total Bruto: <span id="lbl_dcc_vbruto"><?php echo escreverNumero(0,1); ?></span></div>
                                                    <div style="float:left; width:calc(50% - 170px); text-align:center; color:#c22439; font-size:14px">Total Líquido: <span id="lbl_dcc_vliquido"><?php echo escreverNumero(0,1); ?></span></div>
                                                    <div style="float:right; width:170px; text-align:right">
                                                        Saldo de Transações:<input type="text" id="saldoImp" class="input-medium" style="width:40px; padding:2px; margin:-3px 0 0 3px; display:none" readonly/>
                                                        <button type="button" id="saldoBtn" class="btn dblue" style="line-height:19px; width:40px; margin:-3px 0 0; background-color:#308698 !important" onClick="ler_saldo()">
                                                            <span class="icon-search icon-white"></span>
                                                        </button>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/highlight/jquery.highlight-4.js'></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/jquery.creditCardValidator.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/amcharts.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/pie.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/serial.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/light.js"></script>
        <script type="text/javascript" src="../../js/moment.min.js"></script>
        <script type="text/javascript" src="../../js/jquery.cpfcnpj.min.js"></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.ajax-combobox/dist/jquery.ajax-combobox.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript" src="./../Academia/js/DadosPessoais.js"></script>        
        <script type="text/javascript" src="js/CRMForm.js"></script>
        <script type="text/javascript" src="./../Academia/js/AcademiaPlanos.js"></script>
        <script type="text/javascript" src="./../Academia/js/AcademiaAcesso.js"></script>
        <script type="text/javascript" src="./../Academia/js/ClientesAgendamentoDccForm.js"></script>
        <script type="text/javascript" src="./../Academia/js/Agendamentos.js"></script>
        <?php if ($ckb_est_lorc_ == 1 && $_SESSION["mod_emp"] == 0) { ?>
        <script type="text/javascript" src="./../Academia/js/Orcamentos.js"></script>
        <?php } ?>
        <script type="text/javascript" src="./../Academia/js/CamposLivres.js"></script>
        <script type="text/javascript" src="./../Academia/js/ComissaoCliente.js"></script>        
        <script type="text/javascript" src="./../Seguro/js/Veiculo.js"></script>        
        <script type="text/javascript" src="./../Seguro/js/Vistoria.js"></script>        
        <script type="text/javascript" src="js/CRMFormTelemarketing.js"></script>
        <script type="text/javascript" src="js/CRMFormServicos.js"></script>
        <script type="text/javascript" src="js/CRMDocumentos.js"></script>
        <script type="text/javascript" src="js/CRMDcc.js"></script>    
        <script type="text/javascript" src="js/CRMConvenios.js"></script>
        <script type="text/javascript" src="js/CRMEnderecoEntrega.js"></script>    
        <script type="text/javascript" src="js/CRMContratos.js"></script>
    </body>
    <?php odbc_close($con); ?>
</html>
