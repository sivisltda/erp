<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include '../../Connections/configini.php';

$id_area = $_REQUEST['txtDepartamento'];
$local = array();

if ($id_area == "null") {
    $sql = "select id_usuario,login_user,nome,2 tipo from sf_usuarios where inativo = 0 order by nome";
} else {
    $sql = "select id_usuario,login_user,nome,isnull(tipo_departamento,2) tipo from sf_usuarios left join sf_departamentos on id_departamento = departamento where inativo = 0 and departamento = '" . $id_area . "' order by nome";
}

$cur = odbc_exec($con, $sql) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $local[] = array('id_usuario' => $RFP['id_usuario'], 'nome' => utf8_encode($RFP['nome']), 'tipo' => utf8_encode($RFP['tipo']));
}

echo(json_encode($local));
odbc_close($con);
