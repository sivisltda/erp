<?php include '../../Connections/configini.php';

$camposDinamicos = array();
$tipoDocumento = "0";
$cur = odbc_exec($con, "select id_tipo_documento from sf_tipo_documento 
where sf_tipo_documento.inativo = 0 and abreviacao in ('CRE','DEB','DCC') order by id_tipo_documento;");
while ($RFP = odbc_fetch_array($cur)) {
    $tipoDocumento .= "," . $RFP['id_tipo_documento'];
}
$cur = odbc_exec($con, "select id_tipo_documento_campos, campo, abreviacao
from sf_tipo_documento inner join sf_tipo_documento_campos on tipo_documento = id_tipo_documento
where sf_tipo_documento.inativo = 0 and abreviacao in ('CRE','DEB','DCC') order by abreviacao;");
while ($RFP = odbc_fetch_array($cur)) {
    $camposDinamicos[] = array(
    utf8_encode($RFP['id_tipo_documento_campos']),
    utf8_encode(ucfirst(strtolower($RFP['campo']))),
    utf8_encode($RFP['abreviacao']));
}
$DateBegin = "";
$DateEnd = "";

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>        
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Financeiro<small>Cartões</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="float:left;width: 5%;margin-top: 15px;">
                                    <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir()" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                </div>
                                <div style="float:right;width: 95%;"> 
                                    <?php if (count($camposDinamicos) > 0) { 
                                        for ($i = 0; $i < count($camposDinamicos); $i++) { ?>
                                        <div class="dynamic" style="float:left;margin-left: 1%;width: <?php echo ((60 - count($camposDinamicos)) / count($camposDinamicos)); ?>%;">                                        
                                            <div width="100%"><?php echo $camposDinamicos[$i][1] . "(" . $camposDinamicos[$i][2] . ")"; ?>:</div> 
                                            <input id="txtCd<?php echo $camposDinamicos[$i][0]; ?>" maxlength="50" type="text" value=""/>
                                        </div>                                              
                                    <?php 
                                        }
                                    } else { ?>
                                        <div style="float:left;margin-left: 1%;width: 60%;"></div> 
                                    <?php } ?>
                                    <div style="float:left;margin-left: 1%;width: 9%;">                                        
                                        <div width="100%">Tipo:</div>                                        
                                        <select style="width: 103px;vertical-align: top;" name="jumpMenu" id="jumpMenu">
                                            <option value="" >Selecione</option>
                                            <option value="1" <?php
                                            if ($F1 == 1) {
                                                echo "selected";
                                            }
                                            ?>>A Receber</option>
                                            <option value="2" <?php
                                            if ($F1 == 2) {
                                                echo "selected";
                                            }
                                            ?>>Recebidas</option>
                                            <option value="7" <?php
                                            if ($F1 == 7) {
                                                echo "selected";
                                            }
                                            ?>>Renegociadas</option>
                                            <option value="3" <?php
                                            if ($F1 == 3) {
                                                echo "selected";
                                            }
                                            ?>>Canceladas</option>                   
                                        </select>   
                                    </div>  
                                    <div style="float:left;margin-left: 1%;width: 22%;">                                        
                                        <div width="100%">Data:</div>                                      
                                        <select id="txtTipoData" style="width: 80px;vertical-align: top;">    
                                            <option value="0" <?php
                                            if ($F4 == "0") {
                                                echo "SELECTED";
                                            }
                                            ?>>Dt.Venc.</option>
                                            <option value="1" <?php
                                            if ($F4 == "1") {
                                                echo "SELECTED";
                                            }
                                            ?>>Dt.Pag.</option>
                                            <option value="2" <?php
                                            if ($F4 == "2") {
                                                echo "SELECTED";
                                            }
                                            ?>>Dt.Cad.</option>
                                        </select>
                                        <input type="text" style="width: 75px;vertical-align: top;" id="txt_dt_begin" class="datepicker" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial">
                                        <input type="text" style="width: 75px;vertical-align: top;" class="datepicker" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final">                                    
                                    </div>                                                                            
                                    <div style="float:left;margin-left: 1%;width: 5%;margin-top: 15px;">
                                        <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Cartões</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblLista">
                            <thead>
                                <tr>
                                    <th width="6%">Dt.Cd</th>
                                    <th width="4%"><div id="formPQ">F.Pg</div></th>
                                    <th width="6%"><div id="formPQ">Vencto</div></th>
                                    <th width="6%"><div id="formPQ">Pagto</div></th>
                                    <th width="5%"><div id="formPQ">Parc</div></th>
                                    <th width="16%"><div id="formPQ">Cliente</div></th>
                                    <th width="16%"><div id="formPQ">Histórico:</div></th>
                                    <th width="4%"><div id="formPQ">Org</div></th>
                                    <th width="8%"><div id="formPQ">Grupo:</div></th>
                                    <th width="9%"><div id="formPQ">Contas:</div></th>
                                    <th style="text-align: left " width="8%"><div id="formPQ">Parc:</div></th>
                                    <th style="text-align: left " width="8%"><div id="formPQ">Baixa:</div></th>
                                    <th width="4%"><div id="formPQ">Aviso</div></th>
                                </tr>                                                                
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div> 
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script> 
        <script type="text/javascript" src="../../js/util.js"></script>
        <script type="text/javascript">
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }
            
            $(document).ready(function () {
                $("#btnfind").click(function () {
                    tbLista.fnReloadAjax(finalFind(0));
                });
            });

            function finalFind(imp) {                
                var retorno = (imp === 1 ? '&imp=1' : '?imp=0');
                if ($('#jumpMenu').val() !== ""){
                    retorno = retorno + "&id=" + $('#jumpMenu').val();
                }                 
                if ($('#txtTipoData').val() !== ""){
                    retorno = retorno + "&tpd=" + $('#txtTipoData').val();
                }
                if ($('#txt_dt_begin').val() !== ""){
                    retorno = retorno + "&dti=" + $('#txt_dt_begin').val();
                }
                if ($('#txt_dt_end').val() !== ""){
                    retorno = retorno + "&dtf=" + $('#txt_dt_end').val();
                }
                $(".dynamic input").each(function(index) {
                    if($(this).val() !== "") {
                        retorno = retorno + "&" + $(this).attr("id") + "=" + $(this).val();
                    }
                });                
                var retorno = retorno + "&rel=1&tp=3&grp=[<?php echo $tipoDocumento; ?>]";
                return (imp === 1 ? '' : '../') + "../Modulos/Contas-a-pagar/Contas-a-Receber_server_processing.php" + retorno.replace(/\//g, "_");
            }
            
            function imprimir() {
                var pRel = "&NomeArq=" + "Cartões" +
                        "&lbl=Dt.Cd|F.Pg|Vencto|Pagto|Parc|Cliente|Histórico|Org|Grupo|Contas|Parc|Baixa" +
                        "&siz=50|30|50|50|30|100|100|30|80|80|50|50" +
                        "&pdf=12" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + finalFind(1); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }

            var tbLista = $('#tblLista').dataTable({
                "iDisplayLength": 20,
                "aLengthMenu": [20, 30, 40, 50, 100],
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,     
                "bSort" : false,                         
                "sAjaxSource": finalFind(0),
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"},
                "sPaginationType": "full_numbers"
            });            
        </script>                           
        <?php odbc_close($con); ?>
    </body>
</html>