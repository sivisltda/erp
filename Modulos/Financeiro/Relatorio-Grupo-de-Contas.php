<?php
include './../../Connections/configini.php';
$DateBegin = getData("B");
$DateEnd = getData("E");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>      
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>                     
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div  class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1>Financeiro<small>Relatório de Consulta por Plano de Contas</small></h1>
                    </div>
                    <div>
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="float: left;">
                                    <span >Loja:</span>
                                    <select name="txtEmpresa" id="txtEmpresa">
                                        <?php
                                        $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo utf8_encode($RFP['id_filial']); ?>"><?php echo utf8_encode($RFP['numero_filial']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div style="margin-left: 1%;float: left;width: 95px;">
                                    <span>Tipo:</span>
                                    <select name="txtTipoB" id="txtTipoB">
                                        <option value="null">--Selecione--</option>                                            
                                        <option value="0">Crédito</option>
                                        <option value="1">Débito</option>
                                    </select>
                                </div>
                                <div style="margin-left: 1%;float: left;width: 160px;">
                                    <span>Relatório:</span>
                                    <select name="txtTipoC" id="txtTipoC">
                                        <option value="0">Sintético</option>                                        
                                        <option value="1">Analítico</option>                                        
                                    </select>
                                </div>
                                <div style="margin-left: 1%;float: left;width: 170px;">
                                    <span style="float: left;width: 98px;">Período</span>
                                    <div style="float: left;">
                                        <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial">
                                    </div>
                                    <div style="float: left;margin-left: 9px;">
                                        <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final">
                                    </div>
                                </div>
                                <div style="margin-top: 15px;float:left;margin-left:0.3%;">
                                    <div style="float:left">
                                        <button name="btnfind" id="btnfind" class="button button-turquoise btn-primary" type="button" onclick="refreshFind()"  title="Buscar"><span class="ico-search icon-white"></span></button>
                                    </div>
                                </div>
                                <div style="margin-top: 15px;float:left;">
                                    <div style="float:left">
                                        <button type="button" name="btnPrint" onclick="imprimir('I')" class="button button-blue btn-primary buttonX" title="Imprimir"><span class="ico-print"></span></button>
                                    </div>
                                </div>
                                <div style="margin-top: 15px;float:left;">
                                    <div style="float:left">
                                        <button id="btnExcel" class="button button-blue btn-primary" type="button" onclick="imprimir('E')" title="Exportar Excel"><span class="ico-download-3"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid"></div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>       
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/uniform/jquery.uniform.min.js'></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">

            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
            
            $("#txtTipoB").change(function () {
                showTable();
            });
            
            $("#txtTipoC").change(function () {
                showTable();
            });

            function refreshFind() {
                $("#loader").show();
                $.ajax({                    
                    url: finalFind(0),
                    async: false,
                    dataType: "json",
                    success: function (j) {
                        $("#loader").hide();                        
                        $(".row-fluid").html("");
                        let centro_custo = '';   
                        let subTot = [];         
                        let subTotI = 0;                        
                        for (var c in j.aaData) {                              
                            if (centro_custo !== j.aaData[c][7]) {
                                let table = '';
                                let totalC = 0.00;
                                let totalD = 0.00;                                                      
                                centro_custo = j.aaData[c][7];
                                makeBase(j.aaData[c][7], j.aaData[c][8]); 
                                let tipo = '';
                                let grupo = '';
                                let subgrupo = '';
                                let item = '';
                                let pessoa = '';
                                let valor = '';
                                let totalTabela = 0.00;                                
                                for (var x in j.aaData) {              
                                    if (centro_custo === j.aaData[x][7]) {                                        
                                        if (j.aaData[x][0] === tipo && j.aaData[x][1] === grupo && j.aaData[x][2] === subgrupo) {
                                            item = j.aaData[x][3] + ' - ' + j.aaData[x][4];
                                            pessoa = j.aaData[x][5];
                                            valor = j.aaData[x][6];            
                                            subTot[subTotI] = subTot[subTotI] + textToNumber(valor);                                    
                                            table += `<tr class="sintetico">
                                                <td colspan="2" style="padding:2px; padding-left:8px"><div>${item}</div></td>
                                                <td style="padding:2px; padding-left:8px;">${pessoa}</td>
                                                <td style="padding:2px;text-align:right;">R$ ${valor}</td>
                                            </tr>`;
                                            totalTabela += textToNumber(valor);
                                            totalC += (tipo === 'C' ? textToNumber(valor) : 0.00);
                                            totalD += (tipo === 'D' ? textToNumber(valor) : 0.00);
                                        } else if (j.aaData[x][0] === tipo && j.aaData[x][1] === grupo && j.aaData[x][2] !== subgrupo) {
                                            subTotI++;
                                            subgrupo = j.aaData[x][2];
                                            item = j.aaData[x][3] + ' - ' + j.aaData[x][4];
                                            pessoa = j.aaData[x][5];
                                            valor = j.aaData[x][6];
                                            subTot[subTotI] = textToNumber(valor);
                                            table += `<tr>
                                                <td colspan="3" style="padding:2px; padding-left:8px; font-weight:bold"><div>${subgrupo}</div></td>
                                                <td style="padding:2px;text-align:right;font-weight:bold" class="st${subTotI}"></td>
                                            </tr>
                                            <tr class="sintetico">
                                                <td colspan="2" style="padding:2px; padding-left:8px"><div>${item}</div></td>
                                                <td style="padding:2px; padding-left:8px;">${pessoa}</td>                                            
                                                <td style="padding:2px;text-align:right;">R$ ${valor}</td>
                                            </tr>`;
                                            totalTabela += textToNumber(valor);                                            
                                            totalC += (tipo === 'C' ? textToNumber(valor) : 0.00);
                                            totalD += (tipo === 'D' ? textToNumber(valor) : 0.00);
                                        } else {
                                            if (grupo !== '') {
                                                table += `</tbody>
                                                    <tbody>
                                                        <tr>
                                                            <th width="5%"><center>${tipo}</center></th>
                                                            <th colspan="2" style="text-align:left; padding-left:8px"> TOTAL</th>
                                                            <th style="padding:2px;text-align:right;">R$ ${numberFormat(totalTabela)}</th>
                                                        </tr>
                                                    </tbody>
                                                </table>`;
                                                subTotI++;
                                                totalTabela = 0.00;                                                        
                                            }
                                            tipo = j.aaData[x][0];
                                            grupo = j.aaData[x][1];
                                            subgrupo = j.aaData[x][2];
                                            item = j.aaData[x][3] + ' - ' + j.aaData[x][4];
                                            pessoa = j.aaData[x][5];
                                            valor = j.aaData[x][6];                                                                        
                                            subTot[subTotI] = textToNumber(valor);
                                            table += `<table width="100%" class="table tp${tipo}" style="margin-bottom:20px">
                                                <thead>
                                                    <tr>
                                                        <th width="5%"><center>${tipo}</center></th>
                                                        <th width="40%" style="text-align:left; padding-left:8px">${grupo}</th>
                                                        <th>NOME</th>
                                                        <th width="20%" colspan="2"><center>VALOR</center></th>
                                                    </tr>                                            
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="3" style="padding:2px; padding-left:8px; font-weight:bold"><div>${subgrupo}</div></td>
                                                        <td style="padding:2px;text-align:right;font-weight:bold" class="st${subTotI}"></td>
                                                    </tr>
                                                    <tr class="sintetico">
                                                        <td colspan="2" style="padding:2px; padding-left:8px"><div>${item}</div></td>
                                                        <td style="padding:2px; padding-left:8px;">${pessoa}</td>    
                                                        <td style="padding:2px;text-align:right;">R$ ${valor}</td>
                                                    </tr>`;                                                                                            
                                            totalTabela += textToNumber(valor);
                                            totalC += (tipo === 'C' ? textToNumber(valor) : 0.00);
                                            totalD += (tipo === 'D' ? textToNumber(valor) : 0.00);                                    
                                        }
                                    }
                                }
                                if (table !== '') {
                                    table += `</tbody>
                                        <tbody>
                                            <tr>
                                                <th width="5%"><center>${tipo}</center></th>
                                                <th colspan="2" style="text-align:left; padding-left:8px;"> TOTAL</th>
                                                <th style="padding:2px;text-align:right;">R$ ${numberFormat(totalTabela)}</th>
                                            </tr>
                                        </tbody>
                                    </table>`;
                                    subTotI++;                                                
                                    totalTabela = 0.00;
                                }                                    
                                table += `<table width="100%" class="table">
                                        <thead>
                                            <tr class="tpC">
                                                <th width="5%">
                                                    <center>C</center>
                                                </th>
                                                <th style="text-align:left; padding-left:8px">SOMATÓRIO TOTAL EM CRÉDITO</th>
                                                <th style="padding:2px;text-align:right;" width="20%" colspan="2">R$ ${numberFormat(totalC)}</th>
                                            </tr>                                            
                                            <tr class="tpD">
                                                <th width="5%">
                                                    <center>D</center>
                                                </th>
                                                <th style="text-align:left; padding-left:8px">SOMATÓRIO TOTAL EM DÉBITO</th>
                                                <th style="padding:2px;text-align:right;" width="20%" colspan="2">R$ ${numberFormat(totalD)}</th>
                                            </tr>
                                            <tr class="tpCD">
                                                <th width="5%">
                                                    <center>T</center>
                                                </th>
                                                <th style="text-align:left; padding-left:8px">DIFERENÇA</th>
                                                <th style="padding:2px;text-align:right;" width="20%" colspan="2">R$ ${numberFormat(totalC - totalD)}</th>
                                            </tr>                              
                                        </thead>                                            
                                    </table>`;
                                $(".data-fluid-" + centro_custo).html(table);                                                                                                            
                            }
                        }
                        $.each(subTot, function( index, value ) {
                            $(".st" + index).html(numberFormat(value, 1));
                        });
                        showTable();
                    }
                });
            }

            function makeBase(item, nome) {
                $(".row-fluid").append(`<div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">${nome}</div>
                    </div>
                    <div class="boxtable">
                        <div class="data-fluid-${item}"></div>
                    </div>
                `);
            }

            function showTable() {
                if ($("#txtTipoB").val() === "0") {
                    $(".tpC").show();
                    $(".tpD, .tpCD").hide();
                } else if ($("#txtTipoB").val() === "1") {
                    $(".tpC, .tpCD").hide();
                    $(".tpD").show();
                } else {
                    $(".tpC, .tpD, .tpCD").show();
                }
                if ($("#txtTipoC").val() === "1") {
                    $(".sintetico").show();
                } else {
                    $(".sintetico").hide();
                }
            }
            
            function finalFind(imp) {
                var retorno = "";                
                if ($('#txtTipoB').val() !== "null") {
                    retorno = retorno + "&tpb=" + $('#txtTipoB').val();
                }                
                if ($('#txtTipoC').val() !== "") {
                    retorno = retorno + "&tpc=" + $('#txtTipoC').val();
                }                
                if ($('#txt_dt_begin').val() !== "") {
                    retorno = retorno + "&dti=" + $('#txt_dt_begin').val();
                }
                if ($('#txt_dt_end').val() !== "") {
                    retorno = retorno + "&dtf=" + $('#txt_dt_end').val();
                }
                return "Relatorio-Grupo-de-Contas_server_processing.php?lc=" + $("#txtEmpresa").val() + "&imp="  + imp + retorno.replace(/\//g, "_");
            }
            
            function imprimir(tp) {             
                var pRel = "&NomeArq=" + "Plano de Contas" +
                        "&lbl=" + "Tipo|" + ($("#txtTipoC").val() === "1" ? (tp === 'E' ? "Grupo|Sub.Grupo|" : "") + "Data|Histórico|Nome|Valor" : "Histórico|Valor Cred.|Valor Deb.") +
                        "&pOri=P" + //parametro de paisagem
                        "&siz=50|" + ($("#txtTipoC").val() === "1" ? (tp === 'E' ? "150|160|80|180|160|80" : "80|250|220|100") : "450|100|100") +
                        "&pdf=7" + // Colunas do server processing que não irão aparecer no pdf 9
                        "&filter=" + "" + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=../Modulos/Financeiro/" + finalFind(1).replace("?", "&"); // server processing            
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tp + "&PathArq=GenericModelPDF.php", '_blank');
            }
            
            refreshFind();
        </script>                
        <?php odbc_close($con); ?>
    </body>
</html>
