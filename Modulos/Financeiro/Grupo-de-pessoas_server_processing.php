<?php

require_once(__DIR__ . '/../../Connections/configini.php');
$aColumns = array('id_grupo', 'descricao_grupo');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY tipo_grupo,descricao_grupo asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}
if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";

    if ($_GET['iSortCol_0'] == 0) {
        $sOrder = " order by tipo_grupo " . $_GET['sSortDir_0'] . ", ";
    }
    if ($_GET['iSortCol_0'] == 1) {
        $sOrder = " order by descricao_grupo " . $_GET['sSortDir_0'] . ", ";
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY tipo_grupo,descricao_grupo asc";
    }
}

if ($_GET['txtBusca'] != "") {
    $sWhereX = $sWhereX . " and (descricao_grupo like '%" . $_GET['txtBusca'] . "%')";
}

if (is_numeric($_GET['at'])) {
    $sWhereX .= " and inativo_grupo = " . $_GET['at'];
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
        $sWhere .= $aColumns[$i] . " AND LIKE '%" . utf8_decode($_GET['sSearch_' . $i]) . "%' ";
    }
}

$sQuery = "SELECT COUNT(*) total FROM sf_grupo_cliente WHERE id_grupo > 0 $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total FROM sf_grupo_cliente WHERE id_grupo > 0 $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,* 
            FROM sf_grupo_cliente WHERE id_grupo > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " ";
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    if ($aRow['tipo_grupo'] == 'C') {
        $tot_adm = "Clientes";
    } elseif ($aRow['tipo_grupo'] == 'F') {
        $tot_adm = "Fornecedores";
    } elseif ($aRow['tipo_grupo'] == 'E') {
        $tot_adm = "Funcionários";
    }
    $row[] = "<div id='formPQ' title='" . $tot_adm . "'>" . $tot_adm . "</div>";
    $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(2," . utf8_encode($aRow[$aColumns[0]]) . ")'><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[1]]) . "'>" . utf8_encode($aRow[$aColumns[1]]) . "</div></a>";
    $row[] = "<center>" . ($aRow['inativo_grupo'] == 1 ? "Sim" : "Não") . "</center>";
    if ($imprimir == 0) {
        if ($aRow['editavel'] == 0) {
            $row[] = "<center><a title='Excluir' href='Grupo-de-pessoas.php?Del=" . $aRow[$aColumns[0]] . "'><input name='' type='image' onClick=\"return confirm('Deseja deletar esse registro?')\" src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
        } else {
            $row[] = "";
        }
    }
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
