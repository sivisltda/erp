<?php

include './../../Connections/configini.php';
$aColumns = array('id_contas_movimento', 'tipo', 'sf_grupo_contas.descricao', 'sf_contas_movimento.descricao');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY descr, descricao asc ";
$sOrder2 = " ORDER BY sf_grupo_contas.descricao, sf_contas_movimento.descricao asc ";
$sLimit = 20;
$imprimir = 0;
$_GET['sSearch'] = $_GET['Search'];

if (is_numeric($_GET['imp']) && $_GET['imp'] == "1") {
    $imprimir = 1;
}

if (is_numeric($_GET['at'])) {
    $sWhereX .= " and sf_contas_movimento.inativa = " . $_GET['at'];
    $toReturnAnd .= "&at=" . $_GET["at"];
}

if (isset($_GET['tp'])) {
    $sWhereX .= " and tipo = '" . $_GET['tp'] . "'";
    $toReturnAnd .= "&tp=" . $_GET["tp"];
}

if (isset($_GET['gc']) && is_numeric($_GET["gc"])) {
    $sWhereX .= " and grupo_conta = " . $_GET["gc"];
    $toReturnAnd .= "&gc=" . $_GET["gc"];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0 && intval($_GET['iSortCol_' . $i]) < 3) {
                $sOrder2 .= $aColumns[intval($_GET['iSortCol_' . $i]) + 1] . " " . $_GET['sSortDir_' . $i] . ", ";
                if (intval($_GET['iSortCol_' . $i]) == 1) {
                    $sOrder .= "descr " . $_GET['sSortDir_' . $i];
                } elseif (intval($_GET['iSortCol_' . $i]) == 2) {
                    $sOrder .= "descricao " . $_GET['sSortDir_' . $i];
                }
            }
        }
    }
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder2 == "ORDER BY") {
        $sOrder = " ORDER BY descr, descricao asc";
        $sOrder2 = " ORDER BY sf_grupo_contas.descricao, sf_contas_movimento.descricao asc";
    }
}

if ($_GET['sSearch'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['sSearch']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

$sQuery = "SELECT COUNT(*) total FROM sf_contas_movimento inner join sf_grupo_contas on grupo_conta = id_grupo_contas where deb_cre in ('C','D') $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total FROM sf_contas_movimento inner join sf_grupo_contas on grupo_conta = id_grupo_contas where deb_cre in ('C','D') $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder2 . ") as row,sf_contas_movimento.*, sf_grupo_contas.descricao as descr,deb_cre,
(select max(descricao) from sf_centro_custo cc where cc.id = sf_grupo_contas.centro_custo) c_descricao
FROM sf_contas_movimento inner join sf_grupo_contas on grupo_conta = id_grupo_contas where deb_cre in ('C','D') " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1=" . $imprimir . "" . $sOrder;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    if ($aRow['editavel'] == 0) {
        $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow["tipo"]) . "'>" . utf8_encode($aRow["tipo"]) . "</div></center>";
    } else {
        $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow["tipo"]) . "'><strong>" . utf8_encode($aRow["tipo"]) . "</strong></div></center>";
    }
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["descr"]) . "'>" . utf8_encode($aRow["descr"]) . "</div>";
    if ($aRow['editavel'] == 0) {
        $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[$aColumns[0]]) . ")'><div id='formPQ' title='" . utf8_encode($aRow["descricao"]) . "'>" . utf8_encode($aRow["descricao"]) . (strlen(utf8_encode($aRow['c_descricao'])) > 0 ? " - " . utf8_encode($aRow['c_descricao']) : "") . "</div></a>";
    } else {
        $row[] = "<div id='formPQ' style=\"color:#08c;\" title='" . utf8_encode($aRow["descricao"]) . "'>" . utf8_encode($aRow["descricao"]) . (strlen(utf8_encode($aRow['c_descricao'])) > 0 ? " - " . utf8_encode($aRow['c_descricao']) : "") . "</div>";
    }
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow["dias_tolerancia"]) . "'>" . utf8_encode($aRow["dias_tolerancia"]) . "</div></center>";
    $row[] = "<center><div id='formPQ' title='" . escreverNumero($aRow["multa"], 1) . "'>" . escreverNumero($aRow["multa"], 1) . "</div></center>";
    $row[] = "<center><div id='formPQ' title='" . escreverNumero($aRow["juros"], 1) . "'>" . escreverNumero($aRow["juros"], 1) . "</div></center>";
    if ($imprimir == 0) {
        if ($aRow['editavel'] == 0) {
            $row[] = "<center><a title='Excluir' href='Contas-de-Movimentos.php?Del=" . $aRow[$aColumns[0]] . $toReturnAnd . "'><input name='' type='image' onClick='return confirm(\"Deseja deletar esse registro?\")' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
        } else {
            $row[] = "";
        }
    }
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
