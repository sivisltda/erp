<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include './../../Connections/configini.php';

$id_area = $_REQUEST['txtBanco'];
$local = array();
$sql = "select sf_carteiras_id,sf_carteira,sf_carteira_descricao,sf_desconto 
from sf_bancos_carteiras where sf_carteiras_bancos = " . $id_area . " ORDER BY sf_carteira_descricao desc";
$cur = odbc_exec($con, $sql) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $local[] = array('sf_carteiras_id' => $RFP['sf_carteiras_id'],
        'sf_carteira_descricao' => utf8_encode($RFP['sf_carteira_descricao']),
        'sf_desconto' => $RFP['sf_desconto']);
}

echo( json_encode($local) );
odbc_close($con);
