<?php
include '../../Connections/configini.php';
$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_grupo_contas set " .
        "descricao = " . valoresTexto("txtDescricao") . "," .
        "centro_custo = " . valoresSelect("txtCentroCusto") . "," .
        "deb_cre = " . valoresTexto("txtTipo") . "," .
        "inativo = " . valoresSelect("txtInativa") . 
        " where id_grupo_contas = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_grupo_contas(descricao,centro_custo,deb_cre,inativo)values(" .
        valoresTexto("txtDescricao")  . "," .
        valoresSelect("txtCentroCusto")  . "," .
        valoresTexto("txtTipo") . "," .
        valoresSelect("txtInativa") . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_grupo_contas from sf_grupo_contas order by id_grupo_contas desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        $cur = odbc_exec($con, "select * from sf_contas_movimento where grupo_conta = " . $_POST['txtId']);
        while ($RFP = odbc_fetch_array($cur)) {
            echo "<script>alert('Não é possível excluir esse registro, existem contas relacionadas ao mesmo!'); parent.FecharBox();</script>";
        }
        odbc_exec($con, "DELETE FROM sf_grupo_contas WHERE id_grupo_contas = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_grupo_contas where id_grupo_contas =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_grupo_contas'];
        $tipo = $RFP['deb_cre'];
        $descricao = $RFP['descricao'];
        $centro_custo = $RFP['centro_custo'];
        $inativa = $RFP["inativo"];        
    }
} else {
    $disabled = '';
    $id = '';
    $tipo = '';
    $descricao = '';
    $centro_custo = '';
    $inativa = "0";
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
$FinalUrl = "";
if (isset($_GET["idx"])) {
    $FinalUrl = $_GET["idx"];
}
if (isset($_POST["txtFinalUrl"])) {
    $FinalUrl = $_POST["txtFinalUrl"];
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<body>
    <form action="FormGrupo-de-contas.php" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Grupo de Contas</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont">
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <input name="txtFinalUrl" id="txtFinalUrl" value="<?php echo $FinalUrl; ?>" type="hidden"/>
            <div style="width: 100%;float: left">
                <span>Descrição:</span>
                <input name="txtDescricao" id="txtDescricao" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo utf8_encode($descricao); ?>"/>
            </div>
            <div style="width: 49%;float: left">
                <span>Tipo:</span><br>
                <select style="width: 100%;" id="txtTipo" name="txtTipo" <?php echo $disabled; ?>>
                    <option value="C" <?php
                    if (!(strcmp('C', $tipo))) {
                        echo "SELECTED";
                    }
                    ?>>Crédito</option>
                    <option value="D" <?php
                    if (!(strcmp('D', $tipo))) {
                        echo "SELECTED";
                    }
                    ?>>Débito</option>
                </select>
            </div>
            <div style="width: 50%; margin-left: 1%; float: left">
                <span>Estado:</span><br>
                <select name="txtInativa" id="txtInativa" class="select" style="width:100%" <?php echo $disabled; ?>>
                    <option value="0" <?php
                    if ($inativa == "0") {
                        echo "SELECTED";
                    }
                    ?>>Ativo</option>
                    <option value="1" <?php
                    if ($inativa == "1") {
                        echo "SELECTED";
                    }
                    ?>>Inativo</option>
                </select>
            </div>
            <div style="width: 100%;float: left">
                <span>Centro de Custos:</span><br>
                <select style="width:100%" class="input-medium" <?php echo $disabled; ?> name="txtCentroCusto" id="txtCentroCusto">
                    <option value="null">Selecione</option>
                <?php $cur = odbc_exec($con, "select id, descricao from sf_centro_custo order by descricao") or die(odbc_errormsg());
                while ($RFP = odbc_fetch_array($cur)) { ?>
                    <option value="<?php echo $RFP['id'] ?>"<?php
                    if (!(strcmp($RFP['id'], $centro_custo))) {
                        echo "SELECTED";
                    }
                    ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                <?php } ?>
                </select> 
            </div>            
            <div style="clear:both;height: 10px;"></div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" name="bntSave" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK" ><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else { ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type="text/javascript" src="../../js/util.js"></script>     
    <?php odbc_close($con); ?>
</body>

