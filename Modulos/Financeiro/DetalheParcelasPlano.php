<?php include "../../Connections/configini.php";?>
<!DOCTYPE html>
<head>
    <link rel="icon" type="image/ico" href="../../../favicon.ico"/>
    <link href="../../../css/stylesheets.css" rel="stylesheet" type="text/css" />
    <link href="../../../css/stylesheet.css" rel="stylesheet" type="text/css" />
    <link href="../../../css/main.css" rel="stylesheet">
    <link href="../../../css/bootbox.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-1.9.1.min.js"></script>    
    <style type="text/css">
        .labcont {
            width: 100%;
            border: 1px solid transparent;
        }
        .labhead {
            padding: 0;
            border: 1px solid transparent;
        }
        .liscont {
            width: 100%;
            border: 1px solid #EEE;
        }
        .lishead {
            padding: 0;
            background: #EEE;
            border: 1px solid #FFF;
        }
        .lisbody {
            padding: 0 5px;
            line-height: 28px;
            background: #F6F6F6;
            border: 1px solid #FFF;
        }
        .lisfoot {
            padding: 0 5px;
            background: #EEE;
            text-align: right;
            line-height: 28px;
            font-weight: bold;
            border: 1px solid #FFF;
        }
        .lisbody:nth-child(3) { text-align: center; }
        .lisbody:nth-child(4), .lisbody:nth-child(5) { text-align: right; }
        .lisbody.selected { background: #acbad4 !important;}
    </style>
</head>
<body>
    <div class="row-fluid">
        <div class="frmhead">
            <div class="frmtext">Detalhamento de Parcelamento</div>
            <div class="frmicon" style="top:13px" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div style="clear:both"></div>
        <div style="margin:0 10px">
            <div class="data-fluid" style="height:355px; overflow:auto">
                <div style="padding-top: 10px;">
                    <table width="100%" bordercolor="black" border="1" cellpadding="5" cellspacing="2">           
                        <thead>
                            <tr class="txtPq">
                                <th width="8%" bgcolor="#e9e9e9" align="left" >Data:</th>                        
                                <th width="7%" bgcolor="#e9e9e9" >PA:</th>
                                <th width="8%" bgcolor="#e9e9e9" align="left" >Nr. Doc.:</th>                        
                                <th width="9%" bgcolor="#e9e9e9" align="left" >Nosso Num.:</th>                                                
                                <th width="8%" bgcolor="#e9e9e9" align="left" >Venc.:</th>
                                <th width="13%" bgcolor="#e9e9e9" align="left" >Valor da Parcela:</th>
                                <th width="12%" bgcolor="#e9e9e9" align="left" >Valor Previsto:</th>
                                <th width="13%" bgcolor="#e9e9e9" align="left" >Valor Pago:</th>
                                <th width="8%" bgcolor="#e9e9e9" align="left" >Pago em:</th>
                                <th width="7%" bgcolor="#e9e9e9" align="left" >Diferença:</th>
                                <th width="7%" bgcolor="#e9e9e9" align="left" >Em %:</th>
                                <th width="7%" bgcolor="#e9e9e9" align="left" >Tipo:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="txtPq">
                                <?php
                                if ($_GET['id'] != '') {
                                    $cur = odbc_exec($con, "select A.*, B.descricao from sf_venda_parcelas A 
                                    inner join sf_tipo_documento B on A.tipo_documento = B.id_tipo_documento 
                                    where A.inativo = 0 and venda in(select id_venda from sf_vendas_itens inner join sf_vendas_planos_mensalidade
                                    on sf_vendas_itens.id_item_venda = sf_vendas_planos_mensalidade.id_item_venda_mens and id_plano_mens = " . $_GET['id'] . ")");
                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                    </tr>
                                    <tr class="txtPq">
                                        <td><?php echo escreverData($RFP['data_cadastro']);?></td>
                                        <td><?php echo utf8_encode($RFP['pa']);?></td>
                                        <td><?php echo utf8_encode($RFP['sa_descricao']);?></td>
                                        <td><?php echo utf8_encode($RFP['bol_nosso_numero']);?></td>
                                        <td><?php echo escreverData($RFP['data_parcela']);?></td>
                                        <td><?php echo escreverNumero($RFP['valor_parcela'], 1); ?></td>
                                        <td><?php echo escreverNumero($RFP['valor_parcela_liquido'], 1); ?></td>
                                        <td><?php echo escreverNumero($RFP['valor_pago'], 1); ?></td>
                                        <td><?php echo escreverData($RFP['data_pagamento']);?></td>
                                        <?php if ($RFP['valor_pago'] > 0) {
                                            $total = ($RFP['valor_pago'] - $RFP['valor_parcela']);
                                            echo "<td " . ($total < 0 ? "style='color: red'" :  "") . ">" . escreverNumero($total) . "</td>";
                                        } else {
                                            echo "<td>". escreverNumero(0) . "</td>";
                                        } if ($RFP['valor_pago'] > 0 && $RFP['valor_parcela'] > 0) {
                                            $total = ($RFP['valor_pago'] * 100 / $RFP['valor_parcela']) - 100;
                                            echo "<td " . ($total < 0 ?  "style='color: red'" :  "") . ">" . escreverNumero($total) . "</td>";
                                        } else {
                                            echo "<td>". escreverNumero(0) . "</td>";
                                        } ?>
                                        <td><?php echo $RFP['descricao']; ?></td>
                                        <?php
                                    }
                                } ?>
                            </tr>
                        </tbody>
                    </table>                    
                </div>
            </div>
        </div>
    </div>    
</body>
<script type="text/javascript" src="../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
<script type="text/javascript" src="../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
<script type="text/javascript" src="../../../js/plugins/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="../../../js/plugins/select/select2.min.js"></script>
<script type="text/javascript" src="../../../js/plugins.js"></script>
<script type="text/javascript" src="../../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
<script type="text/javascript" src="../../../js/plugins/datatables/fnReloadAjax.js"></script>
<script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
<script type="text/javascript" src="../../../js/moment.min.js"></script>
<script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script>
<script type="text/javascript" src="../../../js/util.js"></script>
<?php odbc_close($con); ?>
</html>
