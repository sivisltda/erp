<?php

require_once(__DIR__ . '/../../Connections/configini.php');
$aColumns = array('id_grupo_contas', 'deb_cre', 'descricao');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = " and deb_cre in ('C','D') ";
$sWhere = "";
$sOrder = " ORDER BY descricao asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";

    if ($_GET['iSortCol_0'] == 0) {
        $sOrder = " order by deb_cre " . $_GET['sSortDir_0'] . ", ";
    }
    if ($_GET['iSortCol_0'] == 1) {
        $sOrder = " order by descricao " . $_GET['sSortDir_0'] . ", ";
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY tipo_grupo,descricao_grupo asc";
    }
}

if (is_numeric($_GET['at'])) {
    $sWhereX .= " and sf_grupo_contas.inativo = " . $_GET['at'];
}

if (isset($_GET['tp'])) {
    $sWhereX .= " and deb_cre = '" . $_GET['tp'] . "'";
}

if (is_numeric($_GET['gp'])) {
    $sWhereX .= " and sf_grupo_contas.centro_custo = " . $_GET['gp'];
}

if ($_GET['txtBusca'] != "") {
    $sWhereX .= $sWhereX . " and (descricao like '%" . $_GET['txtBusca'] . "%')";
}

$sQuery = "SELECT COUNT(*) total FROM sf_grupo_contas WHERE id_grupo_contas > 0 $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total FROM sf_grupo_contas WHERE id_grupo_contas > 0 $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT *, (select top 1 descricao from sf_centro_custo where id = centro_custo) centro_custo FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,* 
            FROM sf_grupo_contas WHERE id_grupo_contas > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " ";
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    if ($aRow['editavel'] == 0) {
        $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[1]]) . "'>" . utf8_encode($aRow[$aColumns[1]]) . "</div></center>";
    } else {
        $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[1]]) . "'><strong>" . utf8_encode($aRow[$aColumns[1]]) . "</strong></div></center>";
    }
    if ($aRow['editavel'] == 0) {
        $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(2," . utf8_encode($aRow[$aColumns[0]]) . ")'><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[2]]) . "'>" . utf8_encode($aRow[$aColumns[2]]) . "</div></a>";
    } else {
        $row[] = "<div id='formPQ' style=\"color:#08c;\" title='" . utf8_encode($aRow[$aColumns[2]]) . "'>" . utf8_encode($aRow[$aColumns[2]]) . "</div>";
    }
    if ($aRow['editavel'] == 0) {
        $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["centro_custo"]) . "'>" . utf8_encode($aRow["centro_custo"]) . "</div>";
    } else {
        $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["centro_custo"]) . "'><strong>" . utf8_encode($aRow["centro_custo"]) . "</strong></div>";
    }
    if ($imprimir == 0) {
        if ($aRow['editavel'] == 0) {
            $row[] = "<center><a title='Excluir' href='Grupo-de-contas.php?Del=" . $aRow[$aColumns[0]] . "'><input name='' type='image' onClick=\"return confirm('Deseja deletar esse registro?')\" src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
        } else {
            $row[] = "";
        }
    }
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
