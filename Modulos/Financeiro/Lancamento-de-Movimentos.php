<?php
include './../../Connections/configini.php';

$toReturn = "";
$imprimir = 0;
$tp = "1";

if ($_GET['tp'] != "") {
    $toReturn = "?tp=" . $_GET['tp'];
    $tp = $_GET['tp'];
}

if ($_GET['txtBusca'] != '') {
    $txtBusca = $_GET['txtBusca'];
}

if ($_GET['Del'] != '') {
    $toRet = 0;
    $cur = odbc_exec($con, "select COUNT(*) quantidade from dbo.sf_lancamento_movimento_parcelas where data_pagamento is not null and inativo = 0 and id_lancamento_movimento = " . $_GET['Del']);
    while ($RFP = odbc_fetch_array($cur)) {
        $toRet = $toRet + $RFP['quantidade'];
    }
    if ($toRet > 0) {
        echo "<script>alert('Existem parcelas com baixa relacionadas a esta conta fixa, é necessário seu cancelamento ou exclusão antes de efetuar esta operação!');window.top.location.href = 'Lancamento-de-Movimentos.php" . $toReturn . "';</script>";
    } else {
        odbc_exec($con, "DELETE FROM sf_lancamento_movimento_parcelas WHERE id_lancamento_movimento =" . $_GET['Del']);
        odbc_exec($con, "DELETE FROM sf_lancamento_movimento WHERE id_lancamento_movimento =" . $_GET['Del']);
        echo "<script>window.top.location.href = 'Lancamento-de-Movimentos.php" . $toReturn . "'; </script>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/styleLeo.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <style>
            .button:disabled, .button[disabled]{
                border-color: #dadada!important;
                background-color: #dadada!important;
            }            
            #s2id_mscStat .select2-choices{
                height: 29px!important;
            }
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <input id="txtTp" type="hidden" value="<?php echo $tp; ?>"/>
                    <div class="page-header" <?php echo $visible; ?>>
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1>Financeiro<small><?php echo ($tp == 0 ? "Contratos" : "Contas Fixas"); ?></small></h1>
                    </div>                    
                    <div class="topcont" style="clear: both;display:flex; margin-bottom: 10px;">
                        <div class="tophead" style="width:calc(12% - 1px); border:0;">
                            <div class="topname">Contratos Ativos</div>
                            <div class="topresp">
                                <div class="topnumb">0</div>
                                <div style="clear:both"></div>
                            </div>
                        </div>
                        <div class="tophead" style="width:calc(22% - 1px)">
                            <div class="topname">Valor Mensal em Contratos</div>
                            <div class="topresp">
                                <div class="topnumb"><?php echo escreverNumero(0); ?></div>
                                <div style="clear:both"></div>
                            </div>
                        </div>
                        <div class="tophead" style="width:calc(22% - 1px)">
                            <div class="topname">Ticket Médio</div>
                            <div class="topresp">
                                <div class="topnumb"><?php echo escreverNumero(0); ?></div>
                                <div style="clear:both"></div>
                            </div>
                        </div>
                        <div class="tophead" style="width:calc(22% - 1px)">
                            <div class="topname">Valor Total de Contratos</div>
                            <div class="topresp">
                                <div class="topnumb"><?php echo escreverNumero(0); ?></div>
                                <div style="clear:both"></div>
                            </div>
                        </div>
                        <div class="tophead" style="width:calc(22% - 1px)">
                            <div class="topname">Valor Total em Aberto</div>
                            <div class="topresp">
                                <div class="topnumb"><?php echo escreverNumero(0); ?></div>
                                <div style="clear:both"></div>
                            </div>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="row-fluid" <?php echo $visible; ?>>
                        <div class="span12">
                            <div class="block" style="margin-bottom: 6px;">
                                <div class="boxfilter block">
                                    <div class="filterLeft" style="width: 22%;">
                                        <div style="float:left">
                                            <span style="opacity:0;display: block;">a</span>
                                            <button id="btnNovo" type="button" title="Novo" class="button button-green btn-primary" onClick="AbrirBox(0,<?php echo ($toReturn != "" ? $tp : "-1"); ?>, 0)"><span class="ico-file-4 icon-white"></span></button>
                                            <button id="bntRenegociar" type="button" title="Renegociar" class="button button-red btn-primary" onclick="renegociar()" disabled><span class="icon-resize-small icon-white"></span></button>
                                            <?php if ($tp == 0) { ?>
                                                <button id="bntBoletos" type="button" title="Gerar Boletos Agrupados" class="button button-blue2 btn-primary" onclick="AbrirBox(4,<?php echo ($toReturn != "" ? $tp : "-1"); ?>, getSelecionados())" disabled><span class="ico-barcode-3"></span></button>
                                            <?php } ?>
                                            <button id="btnPrint" type="button" title="Imprimir" class="button button-blue btn-primary" onclick="imprimir()"><span class="ico-print"></span></button>
                                            <button id="btnNegativar" type="button" title="Inativar" class="button button-red btn-primary" onclick="negativar()" disabled><span class="ico-locked"></span></button>                                            
                                            <button id="btnAprov" type="button" class="button button-green btn-success" onclick="aprovar();" title="Aprovar" disabled><span class="ico-ok"></span></button>                                                                                
                                            <button id="btnCancel" type="button" class="button button-red btn-primary buttonX" onClick="reprovar();" title="Reprovar" disabled><span class="ico-ban-circle"></span></button>                                            
                                        </div>
                                    </div>
                                    <div class="filterRight" style="width: 78%;">
                                        <div style="flex: 0.7;">
                                            <span>Início Contrato(de ~ até):</span>
                                            <div class="" style="display:flex;flex-direction:row;">
                                                <input type="text" class="datepicker" id="dtinitCntratIniciado" name="" value="">
                                                <div class=""> ~ </div>
                                                <input type="text" class="datepicker" id="dtendCntratIniciado" name="" value="">
                                            </div>
                                        </div>
                                        <div style="flex: 0.6;">
                                            <span>Fim do Contrato(de ~ até):</span>
                                            <div class="" style="display:flex;flex-direction:row;">
                                                <input type="text" class="datepicker" id="dtPlanFimInit" name="" value="">
                                                <div class=""> ~ </div>
                                                <input type="text" class="datepicker" id="dtPlanFimEnd" name="" value="">
                                            </div>
                                        </div>
                                        <div style="flex: 0.5;">
                                            <span>Val.Parcela(de ~ até):</span>
                                            <div class="" style="display:flex;flex-direction:row;">
                                                <input id="txtValPlanoDe" name="txtValPlanoDe" type="text" value="<?php echo escreverNumero(0); ?>" class="input-medium" style="width:100%">
                                                <div class=""> ~ </div>
                                                <input id="txtValPlanoAte" name="txtValPlanoAte" type="text" value="<?php echo escreverNumero(0); ?>" class="input-medium" style="width:100%">
                                            </div>
                                        </div>
                                        <div style="flex: 0.3;">
                                            <span>Status:</span>
                                            <select style="width: 100%" name="txtStatusAp" id="txtStatusAp">
                                                <option value="null">Selecione</option>
                                                <option value="0">Novo</option>                                                
                                                <option value="1" <?php echo (isset($_GET["id"]) ? "SELECTED" : ""); ?>>Aguardando</option>                                                
                                                <option value="2">Aprovadas</option>                                               
                                                <option value="3">Reprovadas</option>                                                                                               
                                            </select>                                            
                                        </div>                                        
                                        <div style="flex: 0.7;">
                                            <span>Responsável</span>
                                            <select name="itemsStat[]" id="mscStat" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                                <?php
                                                $query = "select id_usuario,nome from dbo.sf_usuarios where inativo = 0";
                                                $res = odbc_exec($con, $query);
                                                while ($RFC = odbc_fetch_array($res)) { ?>
                                                    <option value="<?php echo utf8_encode($RFC["id_usuario"]); ?>"><?php echo formatNameCompact(utf8_encode($RFC["nome"])); ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div style="flex: 0.3;">
                                            <span>Status:</span>
                                            <select class="" name="txtStatus" id="txtStatus">
                                                <option value="">Todos</option>
                                                <option value="0" selected>Ativos</option>
                                                <option value="1">Inativos</option>
                                            </select>
                                        </div>
                                        <div style="flex: 0.4;">
                                            <span>Busca:</span>
                                            <input id="txtBusca" maxlength="100" type="text" value="<?php echo $txtBusca; ?>" title="Nome/Conta"/>
                                        </div>
                                        <div style="flex: 0.2;">
                                            <span style="opacity: 0;">a</span>
                                            <button name="btnfind" class="button button-turquoise btn-primary" type="button" onclick="refreshFind();" title="Buscar"><span class="ico-search icon-white"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext"><?php echo ($tp == 0 ? "Contratos" : "Contas Fixas"); ?></div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblLista">
                            <thead>
                                <tr>
                                    <th width="2%" align="center"><center><input id="checkAll" type="checkbox" class="checkall"/></center></th>
                                    <th width="2%">&nbsp;</th>
                                    <th width="3%"><center>Loja</center></th>
                                    <th width="3%"><center>D/C</center></th>
                                    <th width="10%"><?php echo ($tp == 0 ? "Clientes" : "Fornec./Funcionário"); ?></th>
                                    <th width="10%">Grupo:</th>
                                    <th width="10%">Conta:</th>
                                    <th width="10%">Histórico:</th>
                                    <th width="7%">Tipo</th>
                                    <th width="7%">Últ.Vnc.</th>
                                    <th width="7%">Max.Parc.:</th>
                                    <th width="6%">Min.Parc.:</th>
                                    <th width="8%">Aberto</th>
                                    <th width="7%">Status:</th>
                                    <th width="6%"><center>Ação:</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>                                        
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../fancyapps/source/jquery.fancybox.js?v=2.1.4"></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap3.3.7.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type='text/javascript' src='../../js/plugins/highlight/jquery.highlight-4.js'></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>        
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/jquery.priceformat.min.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script> 
        <script type="text/javascript">

            $("#dtinitCntratIniciado, #dtendCntratIniciado, #dtPlanFimInit, #dtPlanFimEnd").mask(lang["dateMask"]);
            $("#txtValPlanoDe, #txtValPlanoAte").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});            
            contratosAtivos();
                
            function contratosAtivos() {
                var dados = {filial: $('#txtLojaSel').val(), tp: ($("#txtTp").val() === "0" ? "5" : "6"), status: 0};
                $.get("Lancamento-de-Movimentos_server_processing.php", dados).done(function (data) {
                    var dados = JSON.parse(data);
                    $(".topnumb")[0].innerHTML = dados.Ativos;
                    $(".topnumb")[1].innerHTML = dados.Mensal;
                    $(".topnumb")[2].innerHTML = dados.TiketMedio;
                    $(".topnumb")[3].innerHTML = dados.Pago;
                    $(".topnumb")[4].innerHTML = dados.EmAberto;
                });
            }
            
            function finalFind(imp) {
                var finalURL = "&filial=" + $('#txtLojaSel').val();
                if ($("#txtBusca").val() !== "") {
                    finalURL += finalURL + '&txtBusca=' + $("#txtBusca").val();
                }
                if ($("#txtStatus").val() !== "") {
                    finalURL += '&status=' + $("#txtStatus").val();
                }
                if ($("#txtStatusAp").val() !== "") {
                    finalURL += '&statusap=' + $("#txtStatusAp").val();
                }
                if ($("#txtValPlanoDe").val() !== numberFormat(0)) {
                    finalURL += '&valIni=' + $("#txtValPlanoDe").val() + '&valEnd=' + $("#txtValPlanoAte").val();
                }
                if ($("#mscStat").val() !== null) {
                    finalURL += '&users=' + $("#mscStat").val();
                }
                if ($("#dtinitCntratIniciado").val() !== '') {
                    finalURL += '&dtPlanInit=' + $("#dtinitCntratIniciado").val() + '&dtPlanInitEnd=' + $("#dtendCntratIniciado").val();
                }
                if ($("#dtPlanFimInit").val() !== '') {
                    finalURL += '&dtPlanFimInit=' + $("#dtPlanFimInit").val() + '&dtPlanFimEnd=' + $("#dtPlanFimEnd").val();
                }
                return "Lancamento-de-Movimentos_server_processing.php" + (imp === 1 ? "&im=1" : "?im=0") + "&tp=" + $("#txtTp").val() + finalURL;
            }
            
            var tbLista = $('#tblLista').dataTable({
                "iDisplayLength": 20,
                "aLengthMenu": [20, 30, 40, 50, 100],
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": finalFind(0),
                "bSort": false,
                "bFilter": false,
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"},
                "sPaginationType": "full_numbers"
            });

            function refreshFind() {
                if (textToNumber($("#txtValPlanoDe").val()) === 0 && textToNumber($("#txtValPlanoAte").val()) > 0) {
                    bootbox.alert('O valor inicial da parcela tem que ser maior que zero!');
                    return false;
                }
                if (textToNumber($("#txtValPlanoDe").val()) < textToNumber($("#txtValPlanoAte").val()) || textToNumber($("#txtValPlanoAte").val()) === textToNumber($("#txtValPlanoDe").val())) {
                    tbLista.fnReloadAjax(finalFind(0));
                    clickCheck();
                } else {
                    bootbox.alert('O valor inicial da parcela tem que ser menor que a do fim');
                }
            }

            function AbrirBox(op, tp, id) {
                var myId = "";
                if (id > 0) {
                    var myId = "?id=" + id;
                }
                if (id > 0 && tp !== "") {
                    myId = myId + "&tp=" + tp;
                } else if (tp !== "") {
                    myId = myId + "?tp=" + tp;
                }
                if (op === 0) {
                    abrirTelaBox("FormLancamentoMovimento.php" + myId, 433, 667);
                } else if (op === 20) {
                    abrirTelaBox("DetalheCalendario.php" + myId, 425, 760);
                } else if (op === 21) {
                    abrirTelaBox("FormLanc-Parcelas.php" + myId, 330, 390);
                } else if (op === 30) {
                    abrirTelaBox("DetalheParcelasPlano.php" + myId, 425, 760);
                } else if (op === 4) {
                    abrirTelaBox("FormLanc-Parcelas.php" + myId, 300, 390);
                }
            }
            
            function FecharBox() {
                $("#newbox").remove();
                var oTable = $('#tblLista').dataTable();
                oTable.fnDraw(false);
                clickCheck();
            }

            $("#txtBusca").keypress(function (event) {
                if (event.which === 13) {
                    refreshFind();
                }
            });

            function clickCheck() {
                var total = 0;
                var totalContrato = 0;
                $('.itens').each(function (index) {
                    if ($(this).is(':checked')) {
                        total++;
                        if ($(this).hasClass("contrat")) {
                            totalContrato = 1;
                        }
                    }
                });
                $("#bntRenegociar").prop("disabled", (total === 1 ? false : true));
                $("#bntBoletos").prop("disabled", (total > 1 ? false : true));
                $("#bntRenegociar").prop("disabled", (total === 1 ? false : true));
                $("#btnNegativar, #btnAprov, #btnCancel").prop("disabled", (totalContrato === 1 ? false : true));
            }

            function getSelecionados() {
                var retorno = '';
                $('.itens').each(function (index) {
                    if ($(this).is(':checked')) {
                        retorno = retorno + $(this).val() + '|';
                    }
                });
                return retorno;
            }

            function renegociar() {
                window.location.href = "Lancamento-de-Renegociacao.php?id=" + getSelecionados() + "&tp=" + $("#txtTp").val();
            }

            function imprimir() {
                var pRel = "&NomeArq=" + ($("#txtTp").val() === "" ? "Contratos" : "Contas Fixas") +
                        "&lbl=Loja|D/C|Fornec./Clientes.|Grupo|Conta|Histórico|Tipo|Ult. Vnc|Max.Parc|Min.Parc|Em Aber" +
                        "&siz=35|25|100|85|100|80|60|50|55|55|55" +
                        "&pdf=11|12" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/Financeiro/" + finalFind(1); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }

            $("#checkAll").change(function (event) {
                clickCheck();
            });

            function negativar() {
                var selecionados = '0';
                $('.itens').each(function (index) {
                    if ($(this).is(':checked')) {
                        if ($(this).hasClass("contrat")) {
                            selecionados = selecionados + "," + $(this).val();
                        }
                    }
                });
                bootbox.confirm('Deseja realmente inativar o(s) contrato(s)?', function (result) {
                    if (result === true) {
                        $.post("form/LancamentoMovimentoFormServer.php", "isAjax=s&btnInativar=" + selecionados).done(function (data) {
                            if (data.trim() === 'YES') {
                                refreshFind();
                            } else {
                                bootbox.alert("Erro ao inativar pagamento!" + data);
                            }
                        });
                    }
                });
            }

            function aprovar() {
                var selecionados = '0';
                $('.itens').each(function (index) {
                    if ($(this).is(':checked')) {
                        if ($(this).hasClass("contrat")) {
                            selecionados = selecionados + "," + $(this).val();
                        }
                    }
                });
                bootbox.confirm('Deseja realmente aprovar o(s) contrato(s)?', function (result) {
                    if (result === true) {
                        $.post("form/LancamentoMovimentoFormServer.php", "isAjax=s&btnAprovar=" + selecionados).done(function (data) {
                            if (data.trim() === 'YES') {
                                refreshFind();
                            } else {
                                bootbox.alert("Erro ao aprovar pagamento!" + data);
                            }
                        });
                    }
                });
            }

            function reprovar() {
                var selecionados = '0';
                $('.itens').each(function (index) {
                    if ($(this).is(':checked')) {
                        if ($(this).hasClass("contrat")) {
                            selecionados = selecionados + "," + $(this).val();
                        }
                    }
                });
                bootbox.confirm('Deseja realmente reprovar o(s) contrato(s)?', function (result) {
                    if (result === true) {
                        $.post("form/LancamentoMovimentoFormServer.php", "isAjax=s&btnReprovar=" + selecionados).done(function (data) {
                            if (data.trim() === 'YES') {
                                refreshFind();
                            } else {
                                bootbox.alert("Erro ao reprovar pagamento!" + data);
                            }
                        });
                    }
                });
            }
        </script>
    </body>
<?php odbc_close($con); ?>
</html>
