<?php

if (!isset($_GET['pdf'])) {
    include '../../Connections/configini.php';
}
$aColumns = array('id_fornecedores_despesas', 'razao_social', 'endereco', 'bairro', 'cidade', 'estado', 'nome_fantasia', 'cnpj');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY id_fornecedores_despesas asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if ($_GET['es'] != "") {
    $sWhereX .= " and estado = " . $_GET['es'];
}

if ($_GET['ci'] != "") {
    $sWhereX .= " and cidade = " . $_GET['ci'];
}

if (is_numeric($_GET['at'])) {
    $sWhereX .= " and sf_fornecedores_despesas.inativo = " . $_GET['at'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0 && intval($_GET['iSortCol_' . $i]) < 9) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . "
                                    " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY id_fornecedores_despesas asc";
    }
}

if ($_GET['Search'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        if ($i !== 6) {
            $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['Search']) . "%' OR ";
        }
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
        $sWhere .= $aColumns[$i] . " AND LIKE '%" . utf8_decode($_GET['sSearch_' . $i]) . "%' ";
    }
}

$sQuery = "SELECT COUNT(*) total FROM sf_fornecedores_despesas WHERE tipo = 'F' $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total FROM sf_fornecedores_despesas WHERE tipo = 'F' $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);


$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,id_fornecedores_despesas, razao_social, endereco, bairro, cidade_nome cidade, estado_sigla estado, 
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato,
            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = id_fornecedores_despesas) email_contato,
            nome_fantasia, cnpj 
            FROM sf_fornecedores_despesas left join sf_tipo_documento on forma_pagamento = id_tipo_documento left join tb_estados on estado = estado_codigo 
            left join tb_cidades on cidade = cidade_codigo WHERE id_fornecedores_despesas > 0 AND tipo = 'F' " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1=" . $imprimir . "" . $sOrder;
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $identificacao = utf8_encode($aRow[$aColumns[1]]);
    if (utf8_encode($aRow[$aColumns[6]]) != '') {
        $identificacao = utf8_encode($aRow[$aColumns[1]]) . ' (' . utf8_encode($aRow[$aColumns[6]]) . ')';
    }
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[0]]) . "'>" . utf8_encode($aRow[$aColumns[0]]) . "</div></center>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[7]]) . "'>" . utf8_encode($aRow[$aColumns[7]]) . "</div></center>";
    $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[$aColumns[0]]) . ")'><div id='formPQ' title='" . $identificacao . "'>" . $identificacao . "</div></a>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[2]]) . "'>" . utf8_encode($aRow[$aColumns[2]]) . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[3]]) . "'>" . utf8_encode($aRow[$aColumns[3]]) . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[4]]) . "'>" . utf8_encode($aRow[$aColumns[4]]) . "</div>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[5]]) . "'>" . utf8_encode($aRow[$aColumns[5]]) . "</div></center>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow['telefone_contato']) . "'>" . utf8_encode($aRow['telefone_contato']) . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow['email_contato']) . "'>" . utf8_encode($aRow['email_contato']) . "</div>";
    if ($ckb_adm_cli_read_ == 0) {
        $row[] = "<center><a title='Excluir' href='Fornecedores-de-despesas.php?Del=" . $aRow[$aColumns[0]] . "'><input name='' type='image' onClick=\"return confirm('Deseja deletar esse registro?')\" src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
    }
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
