<?php
include '../../Connections/configini.php';
if ($_GET['Del'] != '') {
    odbc_exec($con, "update sf_tipo_documento set inativo = 1 where id_tipo_documento = " . $_GET['Del']);
    $query = "set dateformat dmy;
    BEGIN TRANSACTION
        DELETE FROM sf_tipo_documento_taxas WHERE id_tipo_documento = " . $_GET['Del'] . ";
        DELETE FROM sf_tipo_documento_campos WHERE tipo_documento = " . $_GET['Del'] . ";
        DELETE FROM sf_tipo_documento WHERE id_tipo_documento = " . $_GET['Del'] . ";
    IF @@ERROR = 0
    COMMIT
    ELSE
    ROLLBACK;";
    odbc_exec($con, $query);
    echo "<script>window.top.location.href = 'Tipos-de-Documentos.php'; </script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link rel="stylesheet" type="text/css" href="../../js/plugins/select/select22.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>      
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">      
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Financeiro<small>Formas de Pagamento</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="margin-top: 15px;float:left;">
                                    <div style="float:left">
                                        <form method="POST">
                                            <button class="button button-green btn-primary" type="button" onClick="AbrirBox(1, 0)"><span class="ico-file-4 icon-white"></span></button>
                                            <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir('I')" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                            <button id="btnExcel" class="button button-blue btn-primary" type="button" onclick="imprimir('E')" title="Exportar Excel"><span class="ico-download-3"></span></button>
                                        </form>
                                    </div>
                                </div>
                                <div style="float:right;width: 40%;display: flex;align-items: center;justify-content: flex-end;">
                                    <div style="float:left;margin-left: 1%;">                                    
                                        <div width="100%">Estado:</div>
                                        <select id="txtStatus" class="select">
                                            <option value="0" selected>Ativos</option>                                        
                                            <option value="1">Inativos</option>                                                                            
                                        </select>                                           
                                    </div>                                                                           
                                    <div style="float:left;margin-left: 1%;width: 56%;">
                                        <div width="100%">Busca:</div>
                                        <input id="txtBusca" maxlength="100" type="text" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title=""/>
                                    </div>
                                    <div style="float:left;margin-left: 1%;width: 7%;">
                                        <div width="100%" style="opacity:0;">Busca:</div>
                                        <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Formas de Pagamento</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblTpDocumentos">
                            <thead>
                                <tr>
                                    <th width="10%"><center>Abreviação</center></th>
                                    <th>Descrição</th>
                                    <th width="5%"><center>Tipo</center></th>
                                    <th width="15%"><center>Aceita Parcelamento</center></th>
                                    <th width="12%"><center>Banco p/ Baixa</center></th>
                                    <th width="5%"><center>Ação:</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="5" class="dataTables_empty">Carregando dados ...</td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>
        <script type="text/javascript" src="../../fancyapps/source/jquery.fancybox.js?v=2.1.4"></script>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/select/select22.min.js"></script>        
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type="text/javascript" src="../../js/util.js"></script>          
        <script type="text/javascript">           
                            
            function formatState (state) {
                if (!state.id) { 
                    return state.text; 
                }
                var $state = $('<span><img src="../../img/cartao/' +  state.id + '.png" style="height: 25px;"/> ' + state.text + '</span>');
                return $state;
            };

            $("#txtTeste").select2({
                templateResult: formatState,
                templateSelection: formatState
            });
            //--------------------------------------------------------------------------------
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }
            function finalFind(imp) {
                var retPrint = "";
                retPrint = '?imp=0';
                if (imp === 1) {
                    retPrint = '?imp=1';
                }
                if ($("#txtStatus").val() !== "null") {
                    retPrint = retPrint + '&at=' + $("#txtStatus").val();
                }                
                if ($("#txtBusca").val() !== "") {
                    retPrint = retPrint + '&txtBusca=' + $("#txtBusca").val();
                }
                return retPrint.replace(/\//g, "_");
            }
            function AbrirBox(opc, id) {
                if (opc === 1) {
                    abrirTelaBox("./../Financeiro/FormTipos-de-Documentos.php", 492, 650);
                } else if (opc === 2) {
                    abrirTelaBox("./../Financeiro/FormTipos-de-Documentos.php?id=" + id, 492, 650);
                }
            }
            function FecharBox(opc) {
                var oTable = $('#tblTpDocumentos').dataTable();
                oTable.fnDraw(false);
                $("#newbox").remove();
            }
            function imprimir(tipo) {
                var pRel = "&NomeArq=" + "Tipos de Documentos" +
                        "&lbl=Abreviação|Descrição|Tipo|Aceita Parcelamento|Banco p/ Baixa" + 
                        (tipo === 'E' ? "|Dias Venc|Dias Baixa|Taxa|OnLine|Inativo|Parcelamento|Campos Livres" : "") +                        
                        "&siz=100|200|100|200|100" + 
                        (tipo === 'E' ? "|50|50|50|50|50|150|150" : "") +
                        "&pdf=20" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/Financeiro/Tipos-de-Documentos_server_processing.php" + finalFind(1).replace("?", "&"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tipo + "&PathArq=GenericModelPDF.php", '_blank');
            }
            $(document).ready(function () {
                listaTable();
                $("#btnfind").click(function () {
                    $("#tblTpDocumentos").dataTable().fnDestroy();
                    listaTable();
                });

                function listaTable() {
                    $('#tblTpDocumentos').dataTable({
                        "iDisplayLength": 20,
                        "aLengthMenu": [20, 30, 40, 50, 100],
                        "bProcessing": true,
                        "bServerSide": true,
                        "bFilter": false,
                        "aoColumns": [{"bSortable": true},
                            {"bSortable": true},
                            {"bSortable": true},
                            {"bSortable": true},
                            {"bSortable": true},
                            {"bSortable": false}],
                        "sAjaxSource": "Tipos-de-Documentos_server_processing.php" + finalFind(0),
                        'oLanguage': {
                            'oPaginate': {
                                'sFirst': "Primeiro",
                                'sLast': "Último",
                                'sNext': "Próximo",
                                'sPrevious': "Anterior"
                            }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                            'sLengthMenu': "Visualização de _MENU_ registros",
                            'sLoadingRecords': "Carregando...",
                            'sProcessing': "Processando...",
                            'sSearch': "Pesquisar:",
                            'sZeroRecords': "Não foi encontrado nenhum resultado"},
                        "sPaginationType": "full_numbers"
                    });
                }
            });
        </script>        
        <?php odbc_close($con); ?>
    </body>
</html>
