<?php

include("../../Connections/configSivis.php");
include("../../Connections/funcoesAux.php");

$dados = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$dados) {
    $dados = file_get_contents('php://input');
    if ($dados) {
        $dados = json_decode($dados, 1);
    }
}
    
if (is_numeric($_GET["bd"])) {    
    $cur2 = odbc_exec($con2, "select *,(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = id_fornecedores_despesas) email
    from ca_contratos left join sf_fornecedores_despesas on id_fornecedores_despesas = id_cliente 
    left join tb_estados on estado_codigo = estado
    left join tb_cidades on cidade_codigo = cidade 
    where inativa = 0 and numero_contrato = '" . $_GET["bd"] . "'");
    while ($RFP = odbc_fetch_array($cur2)) {
        $cedente = utf8_encode($RFP['razao_social']);
        $cpf_cnpj = utf8_encode($RFP['cnpj']);
        $endereco = utf8_encode($RFP['endereco']) . " n°" . utf8_encode($RFP['numero']) . "," . utf8_encode($RFP['bairro']);
        $cidade_uf = utf8_encode($RFP['cidade_nome']) . " " . utf8_encode($RFP['estado_sigla']);
        $email = utf8_encode($RFP['email']);
        $site = utf8_encode($RFP['contato']);
        $contrato = utf8_encode($RFP['numero_contrato']);
        $hostname = utf8_encode($RFP['local']);
        $username = utf8_encode($RFP['login']);
        $password = utf8_encode($RFP['senha']);
        $database = utf8_encode($RFP['banco']);
    }
    odbc_close($con2);
    $msg = "Não foi identificado um boleto para este cliente!";
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
    if (isset($dados["text"])) {
        $cur = odbc_exec($con, "select top 1 id_referencia,tp_referencia, isnull(bol_data_parcela, dt_inicio_mens) bol_data_parcela
        from sf_fornecedores_despesas_veiculo inner join sf_vendas_planos on id_veiculo = id
        inner join sf_vendas_planos_mensalidade on id_plano_mens = id_plano
        inner join sf_boleto on id_referencia = id_mens and tp_referencia = 'M'
        where placa = '" . substr($dados["text"], 0, 3) . "-" . substr($dados["text"], 3, 4) . "' and dt_pagamento_mens is null and inativo = 0
        order by dt_inicio_mens asc") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $msg = "Segue o link do boleto com vencimento: *" . escreverData($RFP['bol_data_parcela']) . "* \n\n" .
            "https://sivisweb.com.br/Boletos/Boleto.php?id=" . $RFP['tp_referencia'] . "-" . $RFP['id_referencia'] . "&crt=" . $_GET["bd"];
        }
    } else if (isset($dados["cpf"])) {
        $cur = odbc_exec($con, "select top 1 id_referencia,tp_referencia, isnull(bol_data_parcela, dt_inicio_mens) bol_data_parcela
        from sf_fornecedores_despesas inner join sf_vendas_planos on id_fornecedores_despesas = favorecido
        inner join sf_vendas_planos_mensalidade on id_plano_mens = id_plano
        inner join sf_boleto on id_referencia = id_mens and tp_referencia = 'M'
        where replace(replace(replace(cnpj, '.', ''), '-', ''), '/', '') = '" . str_replace(array(".", "-", "/"), "", $dados["cpf"]) . "' 
        and dt_pagamento_mens is null and sf_boleto.inativo = 0 and sf_fornecedores_despesas.inativo = 0
        order by dt_inicio_mens asc") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $msg = "Segue o link do boleto com vencimento: *" . escreverData($RFP['bol_data_parcela']) . "* \n\n" .
            "https://sivisweb.com.br/Boletos/Boleto.php?id=" . $RFP['tp_referencia'] . "-" . $RFP['id_referencia'] . "&crt=" . $_GET["bd"];
        }       
    }
    $toReturn = [];
    $toReturn["type"] = "INFORMATION";
    $toReturn["text"] = $msg;
    $toReturn["attachments"] = [];
    echo(json_encode($toReturn));    
    odbc_close($con);
}