<?php

require_once(__DIR__ . '/../../Connections/configini.php');
$aColumns = array('id_bancos', 'empresa', 'descricao', 'gerente', 'telefone', 'num_banco', 'agencia', 'conta', 'tipo_bclc');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY descricao asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (is_numeric($_GET['at'])) {
    $sWhereX .= " and ativo = " . $_GET['at'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $sOrder = " order by " . $aColumns[intval($_GET['iSortCol_0']) + 1] . " " . $_GET['sSortDir_0'] . ", ";
        }
    }

    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY d.descricao asc";
    }
}

if ($_GET['txtBusca'] != "") {
    $sWhereX = $sWhereX . " and (empresa like '%" . $_GET['txtBusca'] . "%' or descricao like '%" . $_GET['txtBusca'] . "%' or gerente like '%" . $_GET['txtBusca'] . "%' or telefone like '%" . $_GET['txtBusca'] . "%' or
                             num_banco like '%" . $_GET['txtBusca'] . "%' or agencia like '%" . $_GET['txtBusca'] . "%' or  conta like '%" . $_GET['txtBusca'] . "%')";
}

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,* 
            FROM sf_bancos WHERE id_bancos > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " ";

$cur = odbc_exec($con, $sQuery1);

$sQuery = "SELECT COUNT(*) total FROM sf_bancos WHERE id_bancos > 0 $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total FROM sf_bancos WHERE id_bancos > 0 $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[1]]) . "'>" . utf8_encode($aRow[$aColumns[1]]) . "</div></center>";
    if ($aRow[$aColumns[0]] > 1) {
        $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[$aColumns[0]]) . ")'><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[2]]) . "'>" . utf8_encode($aRow[$aColumns[2]]) . "</div></a>";
    } else {
        $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[2]]) . "'>" . utf8_encode($aRow[$aColumns[2]]) . "</div>";
    }
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[3]]) . "'>" . utf8_encode($aRow[$aColumns[3]]) . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[4]]) . "'>" . utf8_encode($aRow[$aColumns[4]]) . "</div>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[5]]) . "'>" . utf8_encode($aRow[$aColumns[5]]) . "</div></center>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[6]]) . "'>" . utf8_encode($aRow[$aColumns[6]]) . "</div></center>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[7]]) . "'>" . utf8_encode($aRow[$aColumns[7]]) . "</div></center>";    
    $row[] = "<center><div id='formPQ' title='" . ($aRow[$aColumns[8]] == 0 ? "Banco" : "Local de Recebimento") . "'>" . ($aRow[$aColumns[8]] == 0 ? "Banco" : "Local de Recebimento") . "</div></center>";
    $row[] = ($imprimir == 0 && $aRow[$aColumns[0]] > 1 ? "<center><a title='Excluir' href='Bancos.php?Del=" . $aRow[$aColumns[0]] . "'><input name='' type='image' onClick=\"return confirm('Deseja deletar esse registro?')\" src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>" : "");
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
