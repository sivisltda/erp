<?php
include './../../Connections/configini.php';

$toReturn = "?id=" . $_GET['id'];
if ($_GET['tp'] != "") {
    $toReturn = $toReturn . "&tp=" . $_GET['tp'];
}

if (isset($_POST['bntSave'])) {
    $id = str_replace("|", "", $_GET['id']);
    if (is_numeric($id)) {
        $cur = odbc_exec($con, "select * from sf_lancamento_movimento_parcelas where valor_pago = 0 and inativo = 0 and id_lancamento_movimento = " . $id);
        while ($RFP = odbc_fetch_array($cur)) {
            $vencimento = valoresData("txtVenc_" . $RFP['id_parcela']);
            $valorParcela = valoresNumericos("txtParc_" . $RFP['id_parcela']);
            if (is_numeric($valorParcela) && $vencimento != "null") {
                if ($valorParcela > 0) {
                    if (is_numeric($RFP['bol_id_banco'])) {
                        odbc_exec($con, "set dateformat dmy;update sf_lancamento_movimento_parcelas set bol_data_parcela = " . $vencimento . ",bol_valor = " . $valorParcela . " where id_parcela = " . $RFP['id_parcela']) or die(odbc_errormsg());
                    }
                    odbc_exec($con, "set dateformat dmy;update sf_lancamento_movimento_parcelas set data_vencimento = " . $vencimento . ",valor_parcela = " . $valorParcela . ",valor_previsto = " . $valorParcela . ", sa_descricao = " . valoresTexto("txtNDoc_" . $RFP['id_parcela']) . ", tipo_documento = " . valoresSelect("txtTipo_" . $RFP['id_parcela']) . " where id_parcela = " . $RFP['id_parcela']) or die(odbc_errormsg());
                }
            }
        }
    }
}
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script> 
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1>Financeiro<small>Renegociação</small></h1>
                    </div>
                    <div class="row-fluid">
                        <form action="Lancamento-de-Renegociacao.php<?php echo $toReturn; ?>" method="POST" id="frmEnviaDados">
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="block">
                                        <div class="boxhead">
                                            <div class="boxtext">Renegociação</div>
                                        </div>
                                        <div class="data-fluid">
                                            <div style="margin-bottom:0px; border-bottom:solid 6px #005683;">
                                                <table class="table " cellpadding="0" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="8%">Data:</th>
                                                            <th width="5%">PA:</th>
                                                            <th width="9%">Nº Doc.:</th>                        
                                                            <th width="9%">Ns Nº:</th>                        
                                                            <th width="9%">Vencimento:</th>
                                                            <th width="10%">Val. da Parc.:</th>
                                                            <th width="15%">Forma Pag.:</th>
                                                            <th width="9%">Valor Pago:</th>
                                                            <th width="9%">Pago Em:</th>
                                                            <th width="9%">Diferença:</th>
                                                            <th width="8%">Em %:</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $id = str_replace("|", "", $_GET['id']);
                                                        $Total_Parcelas = 0;
                                                        $Total_Previsto = 0;
                                                        $Total_Pago = 0;
                                                        $arrCTP[99] = "";
                                                        $arrDTP[99] = "";
                                                        $i = 0;
                                                        if (is_numeric($id)) {
                                                            $TipoGrupo = "";
                                                            $cur = odbc_exec($con, "select tipo from sf_lancamento_movimento lm inner join dbo.sf_contas_movimento cm on lm.conta_movimento = cm.id_contas_movimento where lm.id_lancamento_movimento = " . $id) or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) {
                                                                $TipoGrupo = $RFP['tipo'];
                                                            }
                                                            $cur = odbc_exec($con, "select id_tipo_documento,descricao from sf_tipo_documento where s_tipo in ('A','" . $TipoGrupo . "') order by descricao") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) {
                                                                $i = $i + 1;
                                                                $arrCTP[$i] = $RFP['id_tipo_documento'];
                                                                $arrDTP[$i] = utf8_encode($RFP['descricao']);
                                                            }
                                                            $cur = odbc_exec($con, "select * from sf_lancamento_movimento_parcelas where inativo = 0 and id_lancamento_movimento = " . $id);
                                                            while ($RFP = odbc_fetch_array($cur)) {
                                                                $Total_Parcelas = $Total_Parcelas + $RFP['valor_parcela'];
                                                                $Total_Pago = $Total_Pago + $RFP['valor_pago'];
                                                                ?>
                                                                <tr>
                                                                    <td style="padding:0px"><input type="text" <?php
                                                                        if ($RFP['valor_pago'] > 0) {
                                                                            echo "style='background-color:#4899ba'";
                                                                        }
                                                                        ?> class="input-medium inputCenter" value="<?php echo escreverData($RFP['data_cadastro']);?>" disabled /></td>
                                                                    <td style="padding:0px"><input type="text" <?php
                                                                        if ($RFP['valor_pago'] > 0) {
                                                                            echo "style='background-color:#4899ba'";
                                                                        }
                                                                        ?> class="input-medium inputCenter" value="<?php echo utf8_encode($RFP['pa']); ?>" disabled /></td>
                                                                    <td style="padding:0px"><input type="text" <?php
                                                                        if ($RFP['valor_pago'] > 0) {
                                                                            echo "style='background-color:#4899ba;text-align:right' disabled";
                                                                        } else {
                                                                            echo "style='text-align:right'";
                                                                        }
                                                                        ?> name="txtNDoc_<?php echo utf8_encode($RFP['id_parcela']); ?>" id="txtNDoc_<?php echo utf8_encode($RFP['id_parcela']); ?>" class="input-medium" maxlength="50" value="<?php echo utf8_encode($RFP['sa_descricao']); ?>"/></td>
                                                                    <td style="padding:0px"><input type="text" <?php
                                                                        if ($RFP['valor_pago'] > 0) {
                                                                            echo "style='background-color:#4899ba;text-align:right'";
                                                                        } else {
                                                                            echo "style='text-align:right'";
                                                                        }
                                                                        ?> class="input-medium" value="<?php echo utf8_encode($RFP['bol_nosso_numero']); ?>" disabled /></td>
                                                                    <td style="padding:0px"><input type="text" <?php
                                                                        if ($RFP['valor_pago'] > 0) {
                                                                            echo "style='background-color:#4899ba;text-align:center' disabled";
                                                                        }
                                                                        ?> name="txtVenc_<?php echo utf8_encode($RFP['id_parcela']); ?>" id="txtVenc_<?php echo utf8_encode($RFP['id_parcela']); ?>" class="datepicker input-medium inputCenter" value="<?php echo escreverData($RFP['data_vencimento']); ?>"/></td>
                                                                    <td style="padding:0px"><input type="text" <?php
                                                                        if ($RFP['valor_pago'] > 0) {
                                                                            echo "style='background-color:#4899ba;text-align:right' disabled";
                                                                        }
                                                                        ?> name="txtParc_<?php echo utf8_encode($RFP['id_parcela']); ?>" id="txtParc_<?php echo utf8_encode($RFP['id_parcela']); ?>" class="money input-medium" style="text-align:right" value="<?php echo escreverNumero($RFP['valor_parcela']);?>"/></td>                                                                                                                                        
                                                                    <td style="padding:0px">
                                                                        <select  name="txtTipo_<?php echo utf8_encode($RFP['id_parcela']); ?>" id="txtTipo_<?php echo utf8_encode($RFP['id_parcela']); ?>" <?php
                                                                        if ($RFP['valor_pago'] > 0) {
                                                                            echo "style='background-color:#4899ba;text-align:right' disabled ";
                                                                        }
                                                                        ?>style="width:100%;">
                                                                            <option value="null">--Selecione--</option>
                                                                            <?php for ($j = 1; $j <= $i; $j++) { ?>
                                                                                <option value="<?php echo $arrCTP[$j]; ?>" <?php
                                                                                if ($arrCTP[$j] == $RFP['tipo_documento']) {
                                                                                    echo "SELECTED";
                                                                                }
                                                                                ?>><?php echo $arrDTP[$j]; ?></option>
                                                                                    <?php } ?>
                                                                        </select>                                                                                                                                                                                                                                                            
                                                                    </td>                                                                                                                                                                                                            
                                                                    <td style="padding:0px"><input type="text" <?php
                                                                        if ($RFP['valor_pago'] > 0) {
                                                                            echo "style='background-color:#4899ba;text-align:right'";
                                                                        } else {
                                                                            echo "style='text-align:right'";
                                                                        }
                                                                        ?> class="input-medium" value="<?php echo escreverNumero($RFP['valor_pago'], 1); ?>" disabled /></td>
                                                                    <td style="padding:0px"><input type="text" <?php
                                                                        if ($RFP['valor_pago'] > 0) {
                                                                            echo "style='background-color:#4899ba'";
                                                                        }
                                                                        ?> class="input-medium inputCenter" value="<?php echo escreverData($RFP['data_pagamento']); ?>" disabled /></td>
                                                                        <?php
                                                                        if ($RFP['valor_pago'] > 0) {
                                                                            $total = ($RFP['valor_pago'] - $RFP['valor_parcela']);
                                                                            echo "<td style=\"padding:0px\"><input type=\"text\" class=\"input-medium\" style=\"text-align:right;background-color:#4899ba;";
                                                                            if ($total < 0) {
                                                                                echo "color:red";
                                                                            }
                                                                            echo "\" value=\"" . escreverNumero($total, 1) . "\" disabled /></td>";
                                                                        } else {
                                                                            echo "<td style=\"padding:0px\"><input type=\"text\" class=\"input-medium\" style=\"text-align:right\" value=\"" . escreverNumero(0, 1) . "\" disabled /></td>";
                                                                        }
                                                                        if ($RFP['valor_pago'] > 0 && $RFP['valor_parcela'] > 0) {
                                                                            $total = ($RFP['valor_pago'] * 100 / $RFP['valor_parcela']) - 100;
                                                                            echo "<td style=\"padding:0px\"><input type=\"text\" class=\"input-medium inputCenter\" style=\"background-color:#4899ba;";
                                                                            if ($total < 0) {
                                                                                echo "color: red";
                                                                            }
                                                                            echo "\" value=\"" . escreverNumero($total) . "%\" disabled /></td>";
                                                                        } else {
                                                                            echo "<td style=\"padding:0px\"><input type=\"text\" class=\"input-medium inputCenter\" value=\"" . escreverNumero(0) . "%\" disabled /></td>";
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span4" style="width:100%; background-color:#f2f2f2; margin:0; padding:8px;">
                                        <table id="lblTotParc" name="lblTotParc" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td style="text-align: right">
                                                    Total em Parcelas: <strong><?php echo escreverNumero($Total_Parcelas); ?></strong>            
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    Total Pago: <strong><?php echo escreverNumero($Total_Pago); ?></strong>            
                                                </td>
                                            </tr>
                                        </table>                                        
                                    </div>
                                </div>
                                <div class="span12">
                                    <div class="block">
                                        <div class="spanBNT2">
                                            <div class="toolbar bottom tar">
                                                <div class="data-fluid">
                                                    <button class="btn btn-success" type="submit" title="Gravar" name="bntSave" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                                                    <button class="btn btn-success" type="button" onClick="window.top.location.href = 'Lancamento-de-Movimentos.php<?php echo ($_GET['tp'] != "" ? "?tp=" . $_GET['tp'] : ""); ?>'" title="Voltar" id="bntOK"><span class="ico-reply"></span> Voltar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>  
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            $(".datepicker").mask(lang["dateMask"]);
            $(".money").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>