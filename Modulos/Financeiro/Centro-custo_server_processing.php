<?php

require_once(__DIR__ . '/../../Connections/configini.php');
$aColumns = array('id', 'descricao');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhere = "";
$sOrder = " ORDER BY descricao asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if ($_GET['txtBusca'] != "") {
    $sWhere = " and (descricao like '%" . $_GET['txtBusca'] . "%')";
}

$sQuery = "SELECT COUNT(*) total FROM sf_centro_custo WHERE id > 0 " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total FROM sf_centro_custo WHERE id > 0 ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,* 
            FROM sf_centro_custo WHERE id > 0 " . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " ";
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(2," . utf8_encode($aRow[$aColumns[0]]) . ")'><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[1]]) . "'>" . utf8_encode($aRow[$aColumns[1]]) . "</div></a>";
    $row[] = "<center><a title='Excluir' href='Centro-custo.php?Del=" . $aRow[$aColumns[0]] . "'><input name='' type='image' onClick=\"return confirm('Deseja deletar esse registro?')\" src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";       
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);