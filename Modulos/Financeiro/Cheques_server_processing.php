<?php

if (!isset($_GET['pdf'])) {
    include ("./../../Connections/configini.php");
}
$aColumns = array('chq_id', 'chq_data', 'id_fornecedores_despesas', 'razao_social', 'chq_banco', 'chq_agencia', 'chq_conta', 'chq_numero', 'chq_proprietario', 'chq_valor', 'chq_recebido');
$table = "sf_cheques";
$PageName = "Cheques";
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY " . $aColumns[0] . " asc ";
$sOrder2 = " ORDER BY " . $aColumns[0] . " asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (is_numeric($_GET['id'])) {
    if($_GET['id'] == "3") {
        $sWhereX = " and chq_repasse is not null and LEN(chq_repasse) > 0 ";
    } else {
        $sWhereX = " and chq_recebido = " . $_GET['id'];    
    }
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) < count($aColumns) - 1) {
                $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 0) . " " . $_GET['sSortDir_' . $i] . ", ";
                $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 1) . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY " . $aColumns[0] . " asc";
        $sOrder2 = " ORDER BY " . $aColumns[0] . " asc";
    }
}

if ($_GET['Search'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= prepareCollum($aColumns[$i], 0) . " LIKE '%" . utf8_decode($_GET['Search']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($i == 0) {
        $colunas = $aColumns[$i];
    } else {
        $colunas = $colunas . "," . $aColumns[$i];
    }
}
   
$query  = "select " . $colunas . " from sf_cheques c
inner join sf_lancamento_movimento_parcelas l on l.id_parcela = c.codigo_relacao and c.tipo = 'C'
inner join sf_lancamento_movimento l2 on l.id_lancamento_movimento = l2.id_lancamento_movimento
inner join sf_fornecedores_despesas f on l2.fornecedor_despesa = f.id_fornecedores_despesas
where " . $aColumns[0] . " > 0 " . $sWhereX . $sWhere . " union all 
select " . $colunas . " from sf_cheques c
inner join sf_solicitacao_autorizacao_parcelas l on l.id_parcela = c.codigo_relacao and c.tipo = 'S'
inner join sf_solicitacao_autorizacao l2 on l.solicitacao_autorizacao = l2.id_solicitacao_autorizacao
inner join sf_fornecedores_despesas f on l2.fornecedor_despesa = f.id_fornecedores_despesas
where " . $aColumns[0] . " > 0 " . $sWhereX . $sWhere . " union all 
select " . $colunas . " from sf_cheques c
inner join sf_venda_parcelas l on l.id_parcela = c.codigo_relacao and c.tipo = 'V'
inner join sf_vendas l2 on l.venda = l2.id_venda
inner join sf_fornecedores_despesas f on l2.cliente_venda = f.id_fornecedores_despesas        
where " . $aColumns[0] . " > 0 " . $sWhereX . $sWhere;
        
$sQuery1 = "SELECT * FROM(SELECT *, ROW_NUMBER() OVER (" . $sOrder . ") as row from ( " . $query .           
") as x) as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder2;
$cur = odbc_exec($con, $sQuery1);

$sQuery = "SELECT COUNT(*) total from (" . $query . ") as x";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
    $iFilteredTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<center>" . escreverData($aRow[$aColumns[1]]) . "</center>";
    $row[] = "<center>" .utf8_encode($aRow[$aColumns[2]]) . "</center>";
    $row[] = utf8_encode($aRow[$aColumns[3]]);
    $row[] = "<center>" . utf8_encode($aRow[$aColumns[4]]) . "</center>";
    $row[] = "<center>" . utf8_encode($aRow[$aColumns[5]]) . "</center>";
    $row[] = "<center>" . utf8_encode($aRow[$aColumns[6]]) . "</center>";
    $row[] = "<center>" . utf8_encode($aRow[$aColumns[7]]) . "</center>";
    $row[] = utf8_encode($aRow[$aColumns[8]]);
    $row[] = "<center>" . escreverNumero($aRow[$aColumns[9]], 1) . "</center>";
    $row[] = "<center>" . ($aRow[$aColumns[10]] == 1 ? "Sim" : "Não") . "</center>";
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
