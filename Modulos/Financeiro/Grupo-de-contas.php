<?php
include '../../Connections/configini.php';
if ($_GET['Del'] != '') {
    $cur = odbc_exec($con, "select * from sf_contas_movimento where grupo_conta = " . $_GET['Del']);
    while ($RFP = odbc_fetch_array($cur)) {
        echo "<script>alert('Não é possível excluir esse registro, existem contas relacionadas ao mesmo!'); window.top.location.href = 'Grupo-de-contas.php'; </script>";
        exit();
    }
    odbc_exec($con, "DELETE FROM sf_grupo_contas WHERE id_grupo_contas =" . $_GET['Del']);
    echo "<script>window.top.location.href = 'Grupo-de-contas.php'; </script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>         
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Administrativo<small>Grupo de Contas</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="margin-top: 15px;float:left;">
                                    <div style="float:left">
                                        <form method="POST">
                                            <button  class="button button-green btn-primary" type="button" onClick="AbrirBox(1, 0)"><span class="ico-file-4 icon-white"></span></button>
                                            <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir()" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                        </form>
                                    </div>
                                </div>
                                <div style="float:right;width: 60%;display: flex;align-items: center;justify-content: flex-end;">
                                    <div style="float: left;width: 115px;margin-right: 5%;">
                                        <span>Tipo:</span>
                                        <select style="width:160px;" name="txtTipo" id="txtTipo" class="select">
                                            <option value="null">Selecione</option>
                                            <option value="C" <?php
                                            if ($F2 == "0") {
                                                echo "SELECTED";
                                            }
                                            ?>>Crédito</option>
                                            <option value="D" <?php
                                            if ($F2 == "1") {
                                                echo "SELECTED";
                                            }
                                            ?>>Débito</option>
                                        </select>
                                    </div>                                    
                                    <div style="float:left;margin-left: 1%;"> 
                                        <span>Centro de Custos:</span>
                                        <select id="txtGrupo" class="select" style="width: 100%;">
                                            <option value="null">Selecione</option>
                                            <?php $sql = "select id, descricao from sf_centro_custo order by descricao";
                                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id'] ?>"><?php echo utf8_encode($RFP['descricao']) ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>                                    
                                    <div style="float:left;margin-left: 1%;">                                    
                                        <div width="100%">Estado:</div>
                                        <select id="txtStatus" class="select">
                                            <option value="0" selected>Ativos</option>                                        
                                            <option value="1">Inativos</option>                                                                            
                                        </select>                                           
                                    </div>
                                    <div style="float:left;margin-left: 1%;">
                                        <div width="100%">Busca:</div>
                                        <input name="txtBusca"  id="txtBusca" maxlength="100" type="text" style="width:175px;" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title="Nome / CPF/ CNPJ / Endereço / Bairro / Cidade / Contato"/>
                                    </div>
                                    <div style="margin-top: 15px;float:left;margin-left:0.3%;">
                                        <div style="float:left">
                                            <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Grupo de Contas</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblGrConta">
                            <thead>
                                <tr>
                                    <th width="5%"><center>D/C</center></th>
                                    <th>Descrição</th>
                                    <th width="20%">Centro de Custos</th>
                                    <th width="5%"><center>Ação:</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script> 
        <script type="text/javascript" src="../../js/util.js"></script>          
        <script type="text/javascript">
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }            
            function finalFind(imp) {
                var retPrint = "";
                retPrint = '?imp=0';
                if (imp === 1) {
                    retPrint = '?imp=1';
                }
                if ($('#txtTipo').val() !== "null") {
                    retPrint = retPrint + "&tp=" + $("#txtTipo").val();
                }                
                if ($("#txtGrupo").val() !== "null") {
                    retPrint = retPrint + '&gp=' + $("#txtGrupo").val();
                }                
                if ($("#txtStatus").val() !== "null") {
                    retPrint = retPrint + '&at=' + $("#txtStatus").val();
                }                
                if ($("#txtBusca").val() !== "") {
                    retPrint = retPrint + '&txtBusca=' + $("#txtBusca").val();
                }
                return retPrint.replace(/\//g, "_");
            }
            function AbrirBox(opc, id) {
                if (opc === 1) {
                    abrirTelaBox("FormGrupo-de-contas.php", 286, 350);
                }
                if (opc === 2) {
                    abrirTelaBox("FormGrupo-de-contas.php?id=" + id + "", 286, 350);
                }
            }
            function FecharBox(opc) {
                var oTable = $('#tblGrConta').dataTable();
                oTable.fnDraw(false);
                $("#newbox").remove();
            }
            function imprimir() {
                var pRel = "&NomeArq=" + "Grupo de Contas" +
                        "&lbl=D/C|Descrição|Centro de Custos" +
                        "&siz=100|400|200" +
                        "&pdf=3" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/Financeiro/Grupo-de-contas_server_processing.php" + finalFind(1).replace("?", "&"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }
            $(document).ready(function () {
                listaTable();
                $("#btnfind").click(function () {
                    $("#tblGrConta").dataTable().fnDestroy();
                    listaTable();
                });
                function listaTable() {
                    $(document).ready(function () {
                        $('#tblGrConta').dataTable({
                            "iDisplayLength": 20,
                            "aLengthMenu": [20, 30, 40, 50, 100],
                            "bProcessing": true,
                            "bServerSide": true,
                            "bFilter": false,
                            "aoColumns": [{"bSortable": true},
                                {"bSortable": true},
                                {"bSortable": false},
                                {"bSortable": false}],
                            "sAjaxSource": "Grupo-de-contas_server_processing.php" + finalFind(0),
                            'oLanguage': {
                                'oPaginate': {
                                    'sFirst': "Primeiro",
                                    'sLast': "Último",
                                    'sNext': "Próximo",
                                    'sPrevious': "Anterior"
                                }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                                'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                                'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                                'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                                'sLengthMenu': "Visualização de _MENU_ registros",
                                'sLoadingRecords': "Carregando...",
                                'sProcessing': "Processando...",
                                'sSearch': "Pesquisar:",
                                'sZeroRecords': "Não foi encontrado nenhum resultado"},
                            "sPaginationType": "full_numbers"
                        });
                    });
                }
            });
        </script>  
        <?php odbc_close($con); ?>
    </body>
</html>