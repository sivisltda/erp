<?php
include './../../Connections/configini.php';

$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "set dateformat dmy;update sf_cheques set " .
                        "chq_data = " . valoresData('txtData') . "," .
                        "chq_agencia = " . valoresTexto('txtAgencia') . "," .
                        "chq_banco = " . valoresTexto('txtBanco') . "," .
                        "chq_conta = " . valoresTexto('txtConta') . "," .
                        "chq_proprietario = " . valoresTexto('txtProprietario') . "," .
                        "chq_numero = " . valoresTexto('txtNumero') . "," .
                        "chq_valor = " . valoresNumericos('txtValor') . "," .
                        "chq_recebido = " . valoresCheck('txtRecebido') . "" .
                        " where chq_id = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        $query = "set dateformat dmy;insert into sf_cheques(chq_data,chq_agencia,chq_banco,chq_conta,chq_proprietario,chq_numero,chq_valor,chq_recebido)values(" .
                valoresData('txtData') . "," .
                valoresTexto('txtAgencia') . "," .
                valoresTexto('txtBanco') . "," .
                valoresTexto('txtConta') . "," .
                valoresTexto('txtProprietario') . "," .
                valoresTexto('txtNumero') . "," .
                valoresNumericos('txtValor') . "," .
                valoresCheck('txtRecebido') . ")";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 chq_id from sf_cheques order by chq_id desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}

if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_cheques_itens WHERE cheque = " . $_POST['txtId']);
        odbc_exec($con, "DELETE FROM sf_cheques WHERE chq_id = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}
if (isset($_POST['Cancelar'])) {
    echo "<script>parent.FecharBox();</script>";
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_cheques where chq_id =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['chq_id']);
        $data = escreverData($RFP['chq_data']);
        $banco = utf8_encode($RFP['chq_banco']);
        $agencia = utf8_encode($RFP['chq_agencia']);
        $conta = utf8_encode($RFP['chq_conta']);
        $numero = utf8_encode($RFP['chq_numero']);
        $proprietario = utf8_encode($RFP['chq_proprietario']);
        $valor = escreverNumero($RFP['chq_valor']);
        $recebido = utf8_encode($RFP['chq_recebido']);
    }
} else {
    $disabled = '';
    $id = '';
    $data = getData("T");
    $banco = '';
    $agencia = '';
    $conta = '';
    $numero = '';
    $proprietario = '';
    $valor = escreverNumero(0);
    $recebido = '0';
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
if (isset($_POST['bntAlterar'])) {
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
    }
}
if (is_numeric($_GET['Del'])) {
    odbc_exec($con, "DELETE FROM sf_cheques WHERE chq_id =" . $_GET['Del']);
    echo "<script>parent.FecharBox();</script>";
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
<link href="../../css/main.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../../css/styleLeo.css"/>
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <form action="FormCheques.php" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Cadastro de Cheques</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont">
            <div class="data-fluid tabbable" style="overflow:hidden;">
                <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                <ul class="nav nav-tabs">
                    <li class="active" style="margin-left:10px;" ><a href="#tab1" data-toggle="tab">Dados Principais</a></li>
                    <li><a href="#tab2" data-toggle="tab">Pagamentos</a></li>
                    <li><a href="#tab3" data-toggle="tab">Banco</a></li>
                    <li><a href="#tab4" data-toggle="tab">Dados do Cliente</a></li>
                </ul>
                <div class="tab-content" style="height: 186px;padding: 5px 0px;">
                    <div class="tab-pane active" id="tab1" >
                        <table style="margin-bottom:5px;" border="0" cellpadding="3" class="textosComuns">
                            <tr>
                                <td align="right">Data:</td>
                                <td><input name="txtData" id="txtData" <?php echo $disabled; ?> maxlength="10" type="text" class="input-medium datepicker" value="<?php echo $data; ?>"/></td>
                            </tr>
                            <tr>
                                <td width="100" align="right">Banco:</td>
                                <td><input name="txtBanco" id="txtBanco" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $banco; ?>"/></td>
                                <td width="80" align="right">Agencia:</td>
                                <td><input name="txtAgencia" id="txtAgencia" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $agencia; ?>"/></td>
                            </tr>
                            <tr>
                                <td align="right">Conta:</td>
                                <td><input name="txtConta" id="txtConta" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $conta; ?>"/></td>
                                <td align="right">Número:</td>
                                <td><input name="txtNumero" id="txtNumero" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $numero; ?>"/></td>
                            </tr>
                            <tr>
                                <td align="right">Proprietário:</td>
                                <td colspan="3"><input name="txtProprietario" id="txtProprietario" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $proprietario; ?>"/></td>
                            </tr>                                        
                            <tr>
                                <td align="right">Valor:</td>
                                <td><input name="txtValor" id="txtValor" <?php echo $disabled; ?> maxlength="12" type="text" class="input-medium" value="<?php echo $valor; ?>"/></td>
                                <td align="right">Recebido:</td>
                                <td><input name="txtRecebido" id="txtRecebido" <?php echo $disabled; ?> value="1" type="checkbox" <?php echo ($recebido == 1 ? "CHECKED" :  ""); ?> class="input-medium"/></td>
                            </tr>
                        </table>
                    </div>
                    <div class="tab-pane" id="tab2" style="overflow:hidden">
                        <table style="font-size: 12px;" cellpadding="7" cellspacing="0" width="100%">
                            <thead>
                                <tr style="background-color: #E9E9E9;">
                                    <th width="10%"><center>Data:</center></th>
                                    <th width="10%"><center>Tipo:</center></th>
                                    <th width="50%"><center>Histórico:</center></th>
                                    <th width="20%"><center>Valor:</center></th>                                    
                                    <th width="10%"><center>Ação:</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (is_numeric($id)) {
                                    $cur = odbc_exec($con, "select id_item_cheque,data_vencimento,'CF' tipo,historico_baixa,valor_parcela from sf_cheques_itens c inner join sf_lancamento_movimento_parcelas l on c.codigo_relacao = l.id_parcela
                                    where c.cheque = " . $id . " and c.tipo = 'C' union all
                                    select id_item_cheque,data_parcela,'SA' tipo,historico_baixa,valor_parcela from sf_cheques_itens c inner join sf_solicitacao_autorizacao_parcelas s on c.codigo_relacao = s.id_parcela
                                    where cheque = " . $id . " and c.tipo = 'S' union all
                                    select id_item_cheque,data_parcela,'VD' tipo,historico_baixa,valor_parcela  from sf_cheques_itens c inner join sf_venda_parcelas v on c.codigo_relacao = v.id_parcela
                                    where cheque = " . $id . " and c.tipo = 'V'");
                                    while ($RFP = odbc_fetch_array($cur)) { ?> 
                                        <tr id="formPQ">              
                                            <td><div id="formPQ"><?php echo escreverData($RFP['data_vencimento']); ?></div></td>
                                            <td><center><?php echo utf8_encode($RFP['tipo']); ?></center></td>
                                            <td><center><?php echo utf8_encode($RFP['historico_baixa']); ?></center></td>
                                            <td><center><?php echo escreverNumero($RFP['valor_parcela'], 1); ?></center></td>                                                    
                                            <td><center><a title="Excluir" href='FormCheques.php?id=<?php echo $id; ?>&Del=<?php echo utf8_encode($RFP['id_item_cheque']); ?>'><input name='' type='image' onClick="return confirm('Deseja deletar esse registro?')" src="../../img/1365123843_onebit_33 copy.PNG" width='18' height='18' value='Enviar'></a></center></td>                             
                                        </tr>
                                    <?php
                                }
                            } ?>                                                                 
                            </tbody>
                        </table>                                    
                    </div>
                    <div class="tab-pane" id="tab3" style="overflow:hidden">
                        <table style="font-size: 12px;" cellpadding="7" cellspacing="0" width="100%">
                            <thead>
                                <tr style="background-color: #E9E9E9;">
                                    <th width="15%"><center>Código:</center></th>
                                    <th width="15%"><center>Banco:</center></th>
                                    <th width="15%"><center>Agência:</center></th>
                                    <th width="15%"><center>Conta:</center></th>                                    
                                    <th width="20%"><center>Razão Social:</center></th>                                    
                                    <th width="20%"><center>Descrição:</center></th>                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (is_numeric($id)) {
                                    $cur = odbc_exec($con, "select  sf_bancos.* from sf_cheques_itens c inner join sf_lancamento_movimento_parcelas l on c.codigo_relacao = l.id_parcela
                                    inner join sf_bancos on sf_bancos.id_bancos = l.id_banco
                                    where c.cheque = " . $id . " and c.tipo = 'C' union all
                                    select  sf_bancos.* from sf_cheques_itens c inner join sf_solicitacao_autorizacao_parcelas s on c.codigo_relacao = s.id_parcela
                                    inner join sf_bancos on sf_bancos.id_bancos = s.id_banco
                                    where cheque = " . $id . " and c.tipo = 'S' union all
                                    select  sf_bancos.* from sf_cheques_itens c inner join sf_venda_parcelas v on c.codigo_relacao = v.id_parcela
                                    inner join sf_bancos on sf_bancos.id_bancos = v.id_banco                                                                    
                                    where cheque = " . $id . " and c.tipo = 'V'");
                                    while ($RFP = odbc_fetch_array($cur)) { ?> 
                                        <tr id="formPQ">              
                                            <td><center><?php echo utf8_encode($RFP['id_bancos']); ?></center></td>
                                            <td><center><?php echo utf8_encode($RFP['num_banco']); ?></center></td>
                                            <td><center><?php echo utf8_encode($RFP['agencia']); ?></center></td>
                                            <td><center><?php echo utf8_encode($RFP['conta']); ?></center></td>
                                            <td><center><?php echo utf8_encode($RFP['razao_social']); ?></center></td>
                                            <td><center><?php echo utf8_encode($RFP['descricao']); ?></center></td>
                                        </tr>
                                    <?php
                                }
                            } ?>                                                                 
                            </tbody>
                        </table>                                    
                    </div>
                    <div class="tab-pane" id="tab4" style="overflow:hidden">
                        <table style="font-size: 12px;" cellpadding="7" cellspacing="0" width="100%">
                            <thead>
                                <tr style="background-color: #E9E9E9;">
                                    <th width="10%"><center>CNPJ:</center></th>
                                    <th width="10%"><center>Nome/Razão Social:</center></th>
                                    <th width="10%"><center>Telefone:</center></th>
                                    <th width="10%"><center>Contato:</center></th>                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (is_numeric($id)) {
                                    $cur = odbc_exec($con, "select cnpj,razao_social,(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato,contato from sf_cheques_itens c inner join sf_lancamento_movimento_parcelas l on c.codigo_relacao = l.id_parcela
                                    inner join sf_lancamento_movimento lm on lm.id_lancamento_movimento = l.id_lancamento_movimento
                                    inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = lm.fornecedor_despesa
                                    where lm.status = 'Aprovado' and c.cheque = " . $id . " and c.tipo = 'C' union all
                                    select cnpj,razao_social,(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato,contato from sf_cheques_itens c inner join sf_solicitacao_autorizacao_parcelas s on c.codigo_relacao = s.id_parcela
                                    inner join sf_solicitacao_autorizacao lm on lm.id_solicitacao_autorizacao = s.solicitacao_autorizacao
                                    inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = lm.fornecedor_despesa                                                                    
                                    where cheque = " . $id . " and c.tipo = 'S' union all
                                    select cnpj,razao_social,(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato,contato from sf_cheques_itens c inner join sf_venda_parcelas v on c.codigo_relacao = v.id_parcela
                                    inner join sf_vendas lm on lm.id_venda = v.venda
                                    inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = lm.cliente_venda                                                                    
                                    where cheque = " . $id . " and c.tipo = 'V'");
                                    while ($RFP = odbc_fetch_array($cur)) { ?> 
                                        <tr id="formPQ">              
                                            <td><center><?php echo utf8_encode($RFP['cnpj']); ?></center></td>
                                            <td><?php echo utf8_encode($RFP['razao_social']); ?></td>
                                            <td><center><?php echo utf8_encode($RFP['telefone_contato']); ?></center></td>
                                            <td><center><?php echo utf8_encode($RFP['contato']); ?></center></td>
                                        </tr>
                                    <?php
                                }
                            } ?>                                                                 
                            </tbody>
                        </table>                                    
                    </div>
                </div>                
            </div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" name="bntSave" id="bntOK"><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else { ?>                  
                    <button class="btn green" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?');"><span class=" ico-remove"></span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>                
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>     
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type="text/javascript">
        
        $("#txtData").mask(lang["dateMask"]);
        $("#txtValor").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
        
    </script>    
    <?php odbc_close($con); ?>    
</body>