<?php
include '../../Connections/configini.php';
$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        $queryLog = "";
        if (valoresSelect('txtBanco') !== $_POST['txtBaixaAnt']) {
            $queryLog = " insert into sf_logs (tabela, id_item, usuario, acao, descricao, data) 
            values ('sf_tipo_documento', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'A', 'BANCO BAIXA | ANT: " . $_POST["txtBaixaAnt"] . " / ATUAL: " . valoresSelect('txtBanco') . "', GETDATE());";
        }
        odbc_exec($con, "update sf_tipo_documento set " .
        "descricao = " . valoresTexto("txtDescricao") . ", " .
        "abreviacao = " . valoresTexto("txtMine") . ", " .
        "dias = " . valoresNumericos("txtDias") . ", " .
        "dias_baixa = " . valoresNumericos("txtDiasBaixa") . ", " .
        "taxa = " . valoresNumericos("txtTaxa") . ", " .
        "taxa_tipo = " . valoresNumericos("txtSinal") . ", " .
        "s_tipo = " . valoresTexto("txtTipo") . ", " .
        "parcelar = " . valoresCheck('txtAceitaParc') . ", " .
        "cartao_dcc = " . valoresSelect('txtCartaoDCC') . ", " .
        "inativo = " . valoresCheck('txtInativo') . ", " .
        "id_banco_baixa = " . valoresSelect('txtBanco') .
        " where id_tipo_documento = " . $_POST['txtId'] . ";" . $queryLog) or die(odbc_errormsg());
        for($i = 0; $i < 7; $i++) {
            if(is_numeric($_POST['txtIdTaxa_' . $i]) && (valoresNumericos("txtDeTaxa_" . $i) == 0 || valoresNumericos("txtAteTaxa_" . $i) == 0 || valoresNumericos("txtTaxa_" . $i) == 0)) {
                odbc_exec($con, "delete from sf_tipo_documento_taxas where id_taxa = " . $_POST['txtIdTaxa_' . $i]) or die(odbc_errormsg());                                
            } else if(is_numeric($_POST['txtIdTaxa_' . $i]) && (valoresNumericos("txtDeTaxa_" . $i) > 0 && valoresNumericos("txtAteTaxa_" . $i) > 0 && valoresNumericos("txtTaxa_" . $i) > 0)) {
                odbc_exec($con, "update sf_tipo_documento_taxas set " .
                "de_mes = " . valoresNumericos("txtDeTaxa_" . $i) . "," .
                "ate_mes = " . valoresNumericos("txtAteTaxa_" . $i) . "," .
                "valor_taxa = " . valoresNumericos("txtTaxa_" . $i) . "," .
                "cartao = " . valoresNumericos("txtCartao_" . $i) . " " .
                "where id_taxa = " . $_POST['txtIdTaxa_' . $i]) or die(odbc_errormsg());
            } else if(valoresNumericos("txtDeTaxa_" . $i) > 0 && valoresNumericos("txtAteTaxa_" . $i) > 0 && valoresNumericos("txtTaxa_" . $i) > 0) {
                odbc_exec($con, "insert into sf_tipo_documento_taxas(id_tipo_documento, de_mes, ate_mes, valor_taxa, cartao) values (" . $_POST['txtId'] . "," .
                valoresNumericos("txtDeTaxa_" . $i) . "," . valoresNumericos("txtAteTaxa_" . $i) . "," . valoresNumericos("txtTaxa_" . $i) . "," . valoresNumericos("txtCartao_" . $i) . ")") or die(odbc_errormsg());                                
            }
        }
    } else {
        odbc_exec($con, "insert into sf_tipo_documento(descricao,dias,dias_baixa,taxa,taxa_tipo,s_tipo,abreviacao,parcelar, cartao_dcc, id_banco_baixa)values(" .
        valoresTexto("txtDescricao") . "," . valoresNumericos("txtDias") . "," . valoresNumericos("txtDiasBaixa") . "," . valoresNumericos("txtTaxa") . "," . valoresNumericos("txtSinal") . "," . 
        valoresTexto("txtTipo") . "," . valoresTexto("txtMine") . "," . valoresCheck('txtAceitaParc') . "," . valoresSelect('txtCartaoDCC') . "," . valoresSelect('txtBanco') . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_tipo_documento from sf_tipo_documento order by id_tipo_documento desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);        
        for($i = 0; $i < 7; $i++) {
            if(valoresNumericos("txtDeTaxa_" . $i) > 0 && valoresNumericos("txtAteTaxa_" . $i) > 0 && valoresNumericos("txtTaxa_" . $i) > 0) {
                odbc_exec($con, "insert into sf_tipo_documento_taxas(id_tipo_documento, de_mes, ate_mes, valor_taxa, cartao) values (" . $_POST['txtId'] . "," .
                valoresNumericos("txtDeTaxa_" . $i) . "," . valoresNumericos("txtAteTaxa_" . $i) . "," . valoresNumericos("txtTaxa_" . $i) . "," . valoresNumericos("txtCartao_" . $i) . ")") or die(odbc_errormsg());                
            }
        }        
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_tipo_documento set inativo = 1 where id_tipo_documento = " . $_POST['txtId']);
        $query = "set dateformat dmy;
        BEGIN TRANSACTION
            DELETE FROM sf_tipo_documento_taxas WHERE id_tipo_documento = " . $_POST['txtId'] . ";
            DELETE FROM sf_tipo_documento_campos WHERE tipo_documento = " . $_POST['txtId'] . ";
            DELETE FROM sf_tipo_documento WHERE id_tipo_documento = " . $_POST['txtId'] . ";
        IF @@ERROR = 0
        COMMIT
        ELSE
        ROLLBACK;";
        odbc_exec($con, $query);
        echo "<script>parent.FecharBox();</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_tipo_documento where id_tipo_documento = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_tipo_documento'];
        $descricao = $RFP['descricao'];
        $abreviacao = $RFP['abreviacao'];
        $dias = $RFP['dias'];
        $diasbaixa = $RFP['dias_baixa'];
        $taxa = escreverNumero($RFP['taxa']);
        $sinal = $RFP['taxa_tipo'];        
        $tipo = $RFP['s_tipo'];
        $parcelar = utf8_encode($RFP['parcelar']);
        $bancobaixa = utf8_encode($RFP['id_banco_baixa']);
        $cartao_dcc = utf8_encode($RFP['cartao_dcc']);
        $editavel = utf8_encode($RFP['editavel']);
        $inativo = utf8_encode($RFP['inativo']);
    }
    $cur = odbc_exec($con, "select * from sf_tipo_documento_taxas where id_tipo_documento = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $taxas[] = array($RFP['id_taxa'], $RFP['de_mes'], $RFP['ate_mes'], escreverNumero($RFP['valor_taxa']), $RFP['cartao']);
    }    
} else {
    $editavel = '0';
    $disabled = '';
    $id = '';
    $descricao = '';
    $dias = '0';
    $diasbaixa = '0';
    $taxa = escreverNumero(0);
    $sinal = '0';
    $tipo = '';
    $parcelar = '0';
    $bancobaixa = "";
    $cartao_dcc = '0';
    $inativo = '0';
    $taxas = array();
}

if (isset($_POST['bntSaveItem'])) {
    if ($id != '') {
        if ($_POST['txtParcelaMakeV'] != '') {
            odbc_exec($con, "insert into sf_tipo_documento_campos(campo,tipo_documento,tamanho,inativo)values('" . utf8_decode($_POST['txtParcelaMakeV']) . "'," . $id . ",50,0)") or die(odbc_errormsg());
        }
    }
}

if ($_GET['Del'] != '') {
    $cur = odbc_exec($con, "select * from sf_solicitacao_autorizacao_campos where tipo_documento_campos = " . $_GET['Del']);
    if (odbc_num_rows($cur) == 0) {
        odbc_exec($con, "DELETE FROM sf_tipo_documento_campos WHERE id_tipo_documento_campos =" . $_GET['Del']);
    } else {
        echo "<script>alert('Não é possível excluir este campo, há solicitação de autorização relacionada!');</script>";
    }
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
$FinalUrl = "";
if (isset($_GET["idx"])) {
    $FinalUrl = $_GET["idx"];
}
if (isset($_POST["txtFinalUrl"])) {
    $FinalUrl = $_POST["txtFinalUrl"];
}
?>
<html>
    <link rel="icon" type="image/ico" href="../../favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
    <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
    <link rel="stylesheet" type="text/css" href="../../js/plugins/select/select22.css"/>    
    <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>    
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    <style>
        .into_table {
            font-size: 11px;     
            margin-bottom: 0px;
        }
        .into_table thead {
            background-color: #E9E9E9;
        }
        .into_table th{
            padding: 5px;
            border-top: none;
        }
        .into_table td {
            padding: 1px;
            border-top: none;
        }
        .into_table input {
            width: 70%;
        }
        .tablex td {
            padding: 4px;
            border-bottom: 1px solid #ddd;
        }
    </style>    
    <body>
        <div class="row-fluid">
            <form action="FormTipos-de-Documentos.php" name="frmEnviaDados" method="POST">
                <div class="frmhead">
                    <div class="frmtext">Formas de Pagamento</div>
                    <div class="frmicon" onClick="parent.FecharBox()">
                        <span class="ico-remove"></span>
                    </div>
                </div>
                <div class="frmcont">
                    <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                    <input name="txtFinalUrl" id="txtFinalUrl" value="<?php echo $FinalUrl; ?>" type="hidden"/>
                    <input name="txtBaixaAnt" id="txtBaixaAnt" value="<?php echo $bancobaixa; ?>" type="hidden"/>
                    <div style="width: 37%;float: left;">
                        <span>Descrição:</span>
                        <input name="txtDescricao" id="txtDescricao" class="input-medium" style="width:100%" <?php
                        if ($editavel == '1') {
                            echo 'readonly';
                        } else {
                            echo $disabled;
                        }
                        ?> maxlength="128" type="text" value="<?php echo utf8_encode($descricao); ?>"/>
                    </div>
                    <div style="width: 10%;float: left;margin-left: 1%">
                        <span>Abreviação:</span>
                        <input name="txtMine" id="txtMine" class="input-medium" style="width:100%" <?php
                        if ($editavel == '1') {
                            echo 'readonly';
                        } else {
                            echo $disabled;
                        }
                        ?> maxlength="3" type="text" value="<?php echo utf8_encode($abreviacao); ?>"/>
                    </div>
                    <div style="width: 10%;float: left;margin-left: 1%">
                        <span>Dias Venc:</span>
                        <input name="txtDias" id="txtDias" class="input-medium" style="width:100%" <?php
                        if ($editavel == '1' && $abreviacao != "CHE") {
                            echo 'readonly';
                        } else {
                            echo $disabled;
                        }
                        ?> maxlength="3" type="text" value="<?php echo utf8_encode($dias); ?>"/>
                    </div>
                    <div style="width: 9%;float: left;margin-left: 1%">
                        <span>Taxa:</span>
                        <input name="txtTaxa" id="txtTaxa" class="input-medium" style="width:100%" <?php
                        if ($editavel == '1' && $abreviacao != "CHE") {
                            echo 'readonly';
                        } else {
                            echo $disabled;
                        }
                        ?> maxlength="5" type="text" value="<?php echo utf8_encode($taxa); ?>"/>
                    </div>
                    <div style="width: 6%;float: left;">
                        <span>&nbsp;</span>
                        <span style="width:100%; display:block;">
                            <div class="btn-group"><button class="btn btn-primary" id="btnTpValor" type="button" <?php
                            if ($editavel == '1' && $abreviacao != "CHE") {
                                echo 'readonly';
                            } else {
                                echo $disabled;
                            }
                            ?> style="width:35px; padding-bottom:3px; text-align:center; background:#308698 !important">
                            <span id="tp_sinal"><?php echo ($sinal == 1 ? "$" : "%"); ?></span></button></div>
                            <input id="txtSinal" name="txtSinal" value="<?php echo utf8_encode($sinal); ?>" type="hidden">
                        </span>
                    </div>                     
                    <div style="width:24%;float:left;margin-left: 1%">
                        <span>Banco p/ Baixa:</span>
                        <select style="width: 100%;" name="txtBanco" id="txtBanco" <?php
                        if ($editavel == '1' && $abreviacao != "CHE") {
                            echo 'disabled';
                        } else {
                            echo $disabled;
                        }
                        ?>>
                            <option value="null">Selecione</option>
                            <?php
                            $cur = odbc_exec($con, "select id_bancos,razao_social from sf_bancos where ativo = 0 order by razao_social") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) {
                                ?>
                                <option value="<?php echo $RFP['id_bancos'] ?>"<?php
                                if ($RFP['id_bancos'] == $bancobaixa) {
                                    echo "SELECTED";
                                }
                                ?>><?php echo utf8_encode($RFP['razao_social']) ?></option>
                                    <?php } ?>
                        </select>
                    </div>
                    <div style="clear:both; height:5px"></div>
                    <div style="width:12%; float:left;">
                        <span>Tipo:</span>
                        <select id="txtTipo" name="txtTipo" style="width:100%" <?php
                        if ($editavel == '1') {
                            echo 'readonly';
                        } else {
                            echo $disabled;
                        }
                        ?>>
                            <option value="A" <?php
                            if ($tipo == "A") {
                                echo "SELECTED";
                            }
                            ?>>C/D</option>
                            <option value="D" <?php
                            if ($tipo == "D") {
                                echo "SELECTED";
                            }
                            ?>>D</option>
                            <option value="C" <?php
                            if ($tipo == "C") {
                                echo "SELECTED";
                            }
                            ?>>C</option>
                        </select>
                    </div>                    
                    <div style="width:13%; float:left; margin-left: 1%;">
                        <span>OnLine:</span>
                        <select id="txtCartaoDCC" name="txtCartaoDCC" style="width:100%" <?php
                        if ($editavel == '1') {
                            echo 'readonly';
                        } else {
                            echo $disabled;
                        }
                        ?>>
                            <option value="0" <?php
                            if ($cartao_dcc == "0") {
                                echo "SELECTED";
                            }
                            ?>>Sem</option>
                            <option value="1" <?php
                            if ($cartao_dcc == "1") {
                                echo "SELECTED";
                            }
                            ?>>DCC</option>
                            <option value="2" <?php
                            if ($cartao_dcc == "2") {
                                echo "SELECTED";
                            }
                            ?>>DCD</option>
                        </select>
                    </div>
                    <div style="width:16%; float:left; margin-left:2%;margin-top: 23px;">
                        <div style="margin-top: -18px;">
                            <div style="float:left; margin-top:-1px">
                                <input type="checkbox" class="input-medium" id="txtAceitaParc" name="txtAceitaParc" <?php
                                if ($abreviacao == "DIN") {
                                    echo 'disabled';
                                } else {
                                    echo $disabled;
                                }
                                ?> <?php
                                if (substr($parcelar, 0, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" />
                            </div>
                            <div style="float:left; margin:2px 5px">Parcelamento</div>
                        </div>
                        <div style="clear: both;">
                            <div style="float:left; margin-top:0px">
                                <input type="checkbox" class="input-medium" id="txtInativo" name="txtInativo" <?php echo $disabled; ?> <?php
                                if (substr($inativo, 0, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1" />
                            </div>
                            <div style="float:left; margin:2px 5px">Inativo</div>
                        </div>
                    </div>
                    <div style="width: 10%;float: left;margin-left: 1%">
                        <span>Dias Baixa:</span>
                        <input name="txtDiasBaixa" id="txtDiasBaixa" class="input-medium" style="width:100%" <?php
                        if ($editavel == '1' && $abreviacao != "CHE") {
                            echo 'readonly';
                        } else {
                            echo $disabled;
                        }
                        ?> maxlength="3" type="text" value="<?php echo utf8_encode($diasbaixa); ?>"/>
                    </div>
                    <?php if ($id != '') { ?>
                        <div style="width:7%; float:left; margin-left:1%;margin-top: 14px;">
                            <button class="btn ico-checkmark" style="height:27px; margin-bottom:2px" id="for-block3" type="button" onClick="$('#divCampo, #divBtn').slideToggle('fast')"></button>
                        </div>
                    <?php } ?>
                    <div style="width:30%; float:left;display:none;" id="divCampo">
                        <span>Campo:</span>
                        <input name="txtParcelaMakeV" id="txtParcelaMakeV" type="text" class="input-medium" value=""/>
                    </div>
                    <div style="float:left; margin-left:1%;margin-top: 14px;display:none;" id="divBtn">
                        <button class="btn btn-success" style="height:27px; margin-bottom:2px;width: 35px;" type="submit" title="Gravar" name="bntSaveItem" id="bntOKItem"><span class="ico-checkmark"></span></button>
                    </div>
                    <div style="clear:both; height:10px"></div>
                    <div style="float: left; width: 60%; min-height: 232px;">
                        <table class="table into_table" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th colspan="4"><b>Taxa de Parcelamento por Adquirente </b></th>
                                </tr>
                            </thead>                            
                            <tbody>
                                <?php for($i = 0; $i < 7; $i++) { ?>
                                <tr>
                                    <td>De <input id="txtIdTaxa_<?php echo $i; ?>" name="txtIdTaxa_<?php echo $i; ?>" type="hidden" value="<?php echo $taxas[$i][0];?>">
                                    <input id="txtDeTaxa_<?php echo $i; ?>" name="txtDeTaxa_<?php echo $i; ?>" style="width: 30px;" maxlength="2" type="text" <?php echo $disabled;?> value="<?php echo $taxas[$i][1];?>"/></td>
                                    <td>até <input id="txtAteTaxa_<?php echo $i; ?>" name="txtAteTaxa_<?php echo $i; ?>"  style="width: 30px;" maxlength="2" type="text" <?php echo $disabled;?> value="<?php echo $taxas[$i][2];?>"/></td>
                                    <td>Taxa <input id="txtTaxa_<?php echo $i; ?>" name="txtTaxa_<?php echo $i; ?>" style="width: 80px;" type="text" <?php echo $disabled;?> value="<?php echo $taxas[$i][3];?>"/></td>
                                    <td>Cartão <select id="txtCartao_<?php echo $i; ?>" name="txtCartao_<?php echo $i; ?>" style="width: 80px;" class="selectCr" <?php echo $disabled;?>>                                        
                                    <?php for($j = 0; $j < 12; $j++) { ?>            
                                        <option value="<?php echo $j; ?>" <?php echo $taxas[$i][4] == $j ? "selected" : "";?>></option>
                                    <?php } ?>                                        
                                    </select> 
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>                                        
                        </table>
                    </div>
                    <div style="float: left; width: 39%; margin-left: 1%; min-height: 213px;">
                        <table class="table into_table tablex" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr role="row">
                                    <th style="width: 90%;"><b>Descrição</b></th>
                                    <th style="width: 10%;"><b>Ação</b></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if (is_numeric($id)) {
                                $cur = odbc_exec($con, "select * from sf_tipo_documento_campos where tipo_documento = " . $id);
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                <tr>
                                    <td><?php echo utf8_encode($RFP["campo"]); ?></td>
                                    <td>
                                        <a onClick="return confirm('Deseja deletar esse registro?')" href="FormTipos-de-Documentos.php?id=<?php echo $id; ?>&Del=<?php echo $RFP["id_tipo_documento_campos"]; ?>"><img src="../../img/1365123843_onebit_33 copy.PNG" width="16" height="16"/></a>
                                    </td>
                                </tr>
                            <?php }
                            } ?>
                            </tbody>                                        
                        </table>
                    </div>
                    <div style="clear:both;"></div>
                </div>
                <div class="frmfoot">
                    <div class="frmbtn">
                        <?php if ($disabled == "") { ?>
                            <button class="btn green" type="submit" name="bntSave"><span class="ico-checkmark"></span> Gravar</button>
                            <?php if ($_POST["txtId"] == "") { ?>
                                <button class="btn yellow" onClick="parent.FecharBox(1)"><span class="ico-reply"></span> Cancelar</button>
                            <?php } else { ?>
                                <button class="btn yellow" type="submit" name="bntAlterar"><span class="ico-reply"></span> Cancelar</button>
                                <?php
                            }
                        } else { ?>
                            <button class="btn green" type="submit" name="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                            <button class="btn green" type="submit" name="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                            <?php if ($editavel != '1') { ?>                        
                                <button class="btn red" type="button" name="bntDelete" value="Excluir" onClick=" bootbox.confirm('Deseja deletar esse registro?', function (result) {
                                    if (result === true) {
                                        $('#bntDelete').click();
                                    } else {
                                        return;
                                    }
                                });"><span class="ico-remove"></span> Excluir</button>
                                <input id="bntDelete" name="bntDelete" type="submit" value="Excluir"style="display:none"/>
                                <?php
                            }
                        } ?>
                    </div>
                </div>
            </form>
        </div>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/jquery.mousewheel.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/animatedprogressbar/animated_progressbar.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select22.min.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/charts.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/jquery.priceformat.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>      
        <script type="text/javascript">
        
            $("#txtTaxa").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"], suffix: ""});            
            
            function formatState (state) {
                if (!state.id) { 
                    return state.text; 
                }
                var $state = $('<span><img src="../../img/cartao/' +  state.id + '.png" style="height: 25px;"/> ' + state.text + '</span>');
                return $state;
            };

            $(".selectCr").select2({
                templateResult: formatState,
                templateSelection: formatState
            });
                        
            $("#btnTpValor").click(function () {
                if ($("#tp_sinal").html() === "%") {
                    $("#tp_sinal").html("$");
                    $("#txtSinal").val("1");
                } else {
                    $("#tp_sinal").html("%");
                    $("#txtSinal").val("0");
                }
            });
            
        </script>        
</html>
<?php odbc_close($con); ?>
</body>