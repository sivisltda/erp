<?php
include './../../Connections/configini.php';
$FinalUrl = "";
$dt_begin = getData("T");
$dt_end = getData("T");
$id_banco = "null";
$imprimir = 0;

if (isset($_GET['id']) != '') {
    $id_banco = $_GET['id'];
    $FinalUrl = $FinalUrl . "?id=" . $id_banco;
}
if (isset($_GET['dtb']) != '') {
    $FinalUrl = $FinalUrl . "&dtb=" . $_GET['dtb'];
    $dt_begin = str_replace("_", "/", $_GET['dtb']);
}
if (isset($_GET['dte']) != '') {
    $FinalUrl = $FinalUrl . "&dte=" . $_GET['dte'];
    $dt_end = str_replace("_", "/", $_GET['dte']);
}
if (is_numeric($_GET['imp']) && $_GET['imp'] == 1) {
    $imprimir = 1;
}

include 'form/FormBoletosRegistrados.php';

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../SpryAssets/SpryValidationTextField.css"/>
        <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
    <div class="dialog" id="source" title="Source"></div>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>    
</head>
<body>
    <?php if ($imprimir == 0) { ?>
        <div id="loader"><img src="../../img/loader.gif"/></div><?php } ?>
    <div class="wrapper">
        <?php
        if ($imprimir == 0) {
            include('../../menuLateral.php');
        }
        ?>
        <div <?php if ($imprimir == 0) { ?>class="body"<?php } ?>>
            <?php
            if ($imprimir == 0) {
                include("../../top.php");
            }
            ?>
            <div class="content">
                <div class="page-header">
                    <?php if ($imprimir == 0) { ?>
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1>Financeiro<small>Boletos Registrados</small></h1>
                    <?php } ?>
                </div>
                <form action="Relatorio-Boletos-Registrados.php<?php echo $FinalUrl; ?>" method="post" enctype="multipart/form-data" name="enviar" id="enviar">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="span12">
                                <div class="block">
                                    <div id="formulario">
                                        <div style="float:left;margin-top:-13px;">
                                            <span>Banco:</span>
                                            <select name="txtBanco" id="txtBanco">
                                                <option value="">--Selecione--</option>
                                                <?php
                                                $cur = odbc_exec($con, "select id_bancos,razao_social from sf_bancos where ativo = 0 order by razao_social") or die(odbc_errormsg());
                                                while ($RFP = odbc_fetch_array($cur)) {
                                                    ?>
                                                    <option value="<?php echo utf8_encode($RFP['id_bancos']); ?>"<?php
                                                    if (!(strcmp($RFP['id_bancos'], $id_banco))) {
                                                        echo "SELECTED";
                                                    }
                                                    ?>><?php echo utf8_encode($RFP['razao_social']) ?></option>
                                                        <?php } ?>
                                            </select>
                                        </div>
                                        <div style="float:left;margin-left:1%;">
                                            <input type="text" style="width:79px;" class="datepicker" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $dt_begin; ?>" placeholder="Data Inicio">
                                            <input type="text" style="width:79px;" class="datepicker" id="txt_dt_end" name="txt_dt_end" value="<?php echo $dt_end; ?>" placeholder="Data Final">
                                            <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" onclick="AbrirBox(0);" value="Buscar"><span class="ico-search icon-white"></span></button>
                                        </div>
                                        <div style="float:right;">
                                            <button class="button button-blue btn-primary buttonX" type="button" name="btnPrint" onclick="AbrirBox(1);" title="Imprimir"><span class="ico-print"></span></button>
                                            <button class="button button-turquoise btn-primary" type="submit" name="bntSaveFile" id="bntSaveFile" onclick="trocaStatusBox()" value="Salvar">Gerar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block">
                                <div class="boxhead">
                                    <div class="boxtext">Remessa de cobrança de Boletos Registrados</div>
                                </div>
                                <div class="boxtable">
                                    <table class="table " cellpadding="0" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th width="5%"></th>
                                                <th width="5%">Dt.Cad.</th>
                                                <th width="10%">Nr.Doc.</th>
                                                <th width="10%"><div id="formPQ">Ns.Num</div></th>
                                                <th width="5%"><div id="formPQ">Vecto</div></th>
                                                <th width="5%"><div id="formPQ">Pagto</div></th>
                                                <th width="5%"><div id="formPQ">Parc:</div></th>
                                                <th width="35%"><div id="formPQ">Cliente</div></th>
                                                <th width="5%"><div id="formPQ">Exp</div></th>
                                                <th width="10%" style="text-align: left"><div id="formPQ">Parc:</div></th>
                                                <th width="5%"><div id="formPQ">Aviso</div></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?
                                            $F1 = "5";
                                            $comandoX1 = " AND valor_pago = 0 ";
                                            $comandoX2 = " AND valor_pago = 0 ";                                            
                                            $comandoX3 = " AND valor_pago = 0 ";
                                            $comandoX4 = " AND valor_total is null ";                                            
                                            $dt_ini = valoresDataHora2($dt_begin, "00:00:00");
                                            $dt_fim = valoresDataHora2($dt_end, "23:59:59");                                            
                                            if ($dt_ini != "null" && $dt_fim != "null") {
                                                $comandoX1 .= " AND " . ($F1 == "5" ? "bol_data_criacao" : "data_vencimento") . " >= " . $dt_ini . " AND " . ($F1 == "5" ? "bol_data_criacao" : "data_vencimento") . " <= " . $dt_fim;
                                                $comandoX2 .= " AND " . ($F1 == "5" ? "bol_data_criacao" : "data_parcela") . " >= " . $dt_ini . " AND " . ($F1 == "5" ? "bol_data_criacao" : "data_parcela") . " <= " . $dt_fim;
                                                $comandoX3 .= " AND " . ($F1 == "5" ? "bol_data_criacao" : "data_parcela") . " >= " . $dt_ini . " AND " . ($F1 == "5" ? "bol_data_criacao" : "data_parcela") . " <= " . $dt_fim;
                                                $comandoX4 .= " AND " . ($F1 == "5" ? "bol_data_criacao" : "dt_inicio_mens") . " >= " . $dt_ini . " AND " . ($F1 == "5" ? "bol_data_criacao" : "dt_inicio_mens") . " <= " . $dt_fim;
                                            }                                            
                                            $comandoX1 .= " AND sf_lancamento_movimento_parcelas.inativo = '0' and sf_lancamento_movimento_parcelas.carteira_id in (select sf_carteiras_id from sf_bancos_carteiras where sf_carteira_registrada = 1 and sf_carteiras_bancos = " . $id_banco . ")";
                                            $comandoX2 .= " AND sf_solicitacao_autorizacao_parcelas.inativo = '0' and sf_solicitacao_autorizacao_parcelas.carteira_id in (select sf_carteiras_id from sf_bancos_carteiras where sf_carteira_registrada = 1 and sf_carteiras_bancos = " . $id_banco . ")";
                                            $comandoX3 .= " AND sf_venda_parcelas.inativo = '0' and sf_venda_parcelas.carteira_id in (select sf_carteiras_id from sf_bancos_carteiras where sf_carteira_registrada = 1 and sf_carteiras_bancos = " . $id_banco . ")";
                                            $comandoX4 .= " AND sf_boleto.inativo = '0' and sf_boleto.carteira_id in (select sf_carteiras_id from sf_bancos_carteiras where sf_carteira_registrada = 1 and sf_carteiras_bancos = " . $id_banco . ")";
                                            
                                            $sQuery1 = "set dateformat dmy;";                                            
                                            $sQuery1 .= "select max(pk) pk, max(Vecto) Vecto, max(pa) pa, max(empresa) empresa, max(razao_social) razao_social, max(email) email, max(historico_baixa) historico_baixa, max(Origem) Origem, max(cm) cm, sum(valor_parcela) valor_parcela, sum(valor_pago) valor_pago, max(gc) gc, max(inativo) inativo, max(documento) documento, max(obs_baixa) obs_baixa, max(bol_id_banco) bol_id_banco, max(abreviacao) abreviacao, max(tdn) tdn, max(id_fornecedores_despesas) id_fornecedores_despesas, bol_nosso_numero, max(data_pagamento) data_pagamento, max(data_cadastro) data_cadastro, max(valor_juros) valor_juros, max(cnpj) cnpj, max(sf_carteira) sf_carteira, max(endereco) endereco, max(bairro) bairro, max(cidade) cidade, max(estado) estado, max(cep) cep, max(bol_desconto) bol_desconto, max(exp_remessa) exp_remessa, max(sf_variacao) sf_variacao from (";                                            
                                            $sQuery1 .= "select 'D-' +cast(sf_lancamento_movimento_parcelas.id_parcela as VARCHAR) as pk ,isnull(bol_data_parcela,data_vencimento) 'Vecto',pa ,sf_lancamento_movimento.empresa empresa,razao_social,(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = id_fornecedores_despesas) email, historico_baixa, 'D' Origem , sf_contas_movimento.descricao 'cm',
                                            bol_valor valor_parcela,valor_pago,sf_grupo_contas.descricao gc,sf_lancamento_movimento_parcelas.inativo inativo, sf_lancamento_movimento_parcelas.sa_descricao documento,obs_baixa,isnull(bol_id_banco,0) bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,bol_nosso_numero,data_pagamento,data_cadastro,valor_juros,cnpj,sf_carteira, endereco, bairro, cidade_nome cidade, estado_sigla estado,cep,bol_desconto, exp_remessa,sf_variacao
                                            from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento
                                            on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
                                            inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
                                            inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas
                                            left join sf_tipo_documento on sf_lancamento_movimento_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento
                                            left join sf_bancos_carteiras on sf_bancos_carteiras.sf_carteiras_id = sf_lancamento_movimento_parcelas.carteira_id
                                            inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas
                                            left join tb_estados on estado = estado_codigo
                                            left join tb_cidades on cidade = cidade_codigo
                                            where sf_lancamento_movimento.status = 'Aprovado' and sf_contas_movimento.tipo = 'C' " . $comandoX1 . "
                                            and (sf_lancamento_movimento_parcelas.bol_nosso_numero is null or sf_lancamento_movimento_parcelas.bol_nosso_numero not in (select bol_nosso_numero from sf_lancamento_movimento_parcelas group by bol_nosso_numero having bol_nosso_numero is not null and count(id_parcela) > 1))
                                            union
                                            select 'Y-' +cast(max(sf_lancamento_movimento_parcelas.id_parcela) as VARCHAR) as pk,MAX(isnull(bol_data_parcela,data_vencimento)) 'Vecto',MAX(pa) pa,MAX(sf_lancamento_movimento.empresa) empresa
                                            ,MAX(razao_social) razao_social,(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = id_fornecedores_despesas) email, MAX(historico_baixa) historico_baixa, 'D' Origem , MAX(sf_contas_movimento.descricao) 'cm',
                                            sum(bol_valor) valor_parcela,sum(valor_pago) valor_pago,MAX(sf_grupo_contas.descricao) gc,MAX(sf_lancamento_movimento_parcelas.inativo) inativo,
                                            MAX(sf_lancamento_movimento_parcelas.sa_descricao) documento,
                                            MAX(obs_baixa) obs_baixa,MAX(isnull(bol_id_banco,0)) bol_id_banco,MAX(sf_tipo_documento.abreviacao)abreviacao,
                                            MAX(sf_tipo_documento.descricao) tdn,MAX(id_fornecedores_despesas) id_fornecedores_despesas,MAX(bol_nosso_numero) bol_nosso_numero,Max(data_pagamento) data_pagamento,Max(data_cadastro) data_cadastro,Max(valor_juros) valor_juros,Max(cnpj) cnpj,Max(sf_carteira) sf_carteira, MAX(endereco) endereco, MAX(bairro) bairro, MAX(cidade_nome) cidade, MAX(estado_sigla) estado,MAX(cep) cep,MAX(bol_desconto) bol_desconto, MAX(exp_remessa) exp_remessa, MAX(sf_variacao) sf_variacao
                                            from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento
                                            on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
                                            inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
                                            inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas
                                            left join sf_tipo_documento on sf_lancamento_movimento_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento
                                            left join sf_bancos_carteiras on sf_bancos_carteiras.sf_carteiras_id = sf_lancamento_movimento_parcelas.carteira_id
                                            inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas
                                            left join tb_estados on estado = estado_codigo
                                            left join tb_cidades on cidade = cidade_codigo
                                            where sf_lancamento_movimento.status = 'Aprovado' and sf_contas_movimento.tipo = 'C' " . $comandoX1 . "
                                            group by bol_nosso_numero,id_fornecedores_despesas having bol_nosso_numero is not null and count(id_parcela) > 1
                                            union
                                            select 'A-' +cast(sf_solicitacao_autorizacao_parcelas.id_parcela as VARCHAR) as pk ,isnull(bol_data_parcela,data_parcela) 'Vecto',pa ,sf_solicitacao_autorizacao.empresa empresa,razao_social,(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = id_fornecedores_despesas) email, historico_baixa,'A' Origem, sf_contas_movimento.descricao 'cm',
                                            bol_valor valor_parcela,valor_pago,sf_grupo_contas.descricao gc,sf_solicitacao_autorizacao_parcelas.inativo inativo,sf_solicitacao_autorizacao_parcelas.sa_descricao documento,obs_baixa,isnull(bol_id_banco,0) bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,bol_nosso_numero,data_pagamento,data_cadastro,valor_juros,cnpj,sf_carteira, endereco, bairro, cidade_nome cidade, estado_sigla estado,cep,bol_desconto, exp_remessa,sf_variacao
                                            from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
                                            on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
                                            inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
                                            inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas
                                            left join sf_tipo_documento on sf_solicitacao_autorizacao_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento
                                            left join sf_bancos_carteiras on sf_bancos_carteiras.sf_carteiras_id = sf_solicitacao_autorizacao_parcelas.carteira_id
                                            inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas
                                            left join tb_estados on estado = estado_codigo
                                            left join tb_cidades on cidade = cidade_codigo
                                            where sf_contas_movimento.tipo = 'C' and status = 'Aprovado' " . $comandoX2 . "
                                            union
                                            select 'V-' +cast(sf_venda_parcelas.id_parcela as VARCHAR) as pk ,isnull(bol_data_parcela,data_parcela) 'Vecto',pa ,sf_vendas.empresa empresa,razao_social,(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = id_fornecedores_despesas) email, sf_vendas.historico_venda historico_baixa,'V' Origem,
                                            'VENDAS' cm, bol_valor valor_parcela,valor_pago,'VENDAS' gc,sf_venda_parcelas.inativo inativo,sf_venda_parcelas.sa_descricao documento,obs_baixa,bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,bol_nosso_numero,data_pagamento,data_cadastro,valor_juros,cnpj,sf_carteira, endereco, bairro, cidade_nome cidade, estado_sigla estado,cep,bol_desconto,exp_remessa,sf_variacao
                                            from sf_venda_parcelas inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda left join sf_tipo_documento on sf_venda_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento
                                            left join sf_bancos_carteiras on sf_bancos_carteiras.sf_carteiras_id = sf_venda_parcelas.carteira_id
                                            inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas
                                            left join tb_estados on estado = estado_codigo
                                            left join tb_cidades on cidade = cidade_codigo
                                            where sf_vendas.cov = 'V' and status = 'Aprovado' " . $comandoX3 . "
                                            union
                                            select 'M-' +cast(id_mens as VARCHAR) as pk ,isnull(bol_data_parcela,dt_inicio_mens) 'Vecto','01/01' pa ,1 empresa,razao_social,(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = id_fornecedores_despesas) email, 'PAG MENSALIDADE' historico_baixa,'M' Origem,
                                            'MENSALIDADE' cm, bol_valor valor_parcela,isnull(valor_total,0) valor_pago,'MENSALIDADE' gc,sf_boleto.inativo,cast(id_mens as VARCHAR) documento, null obs_baixa,bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,bol_nosso_numero,dt_pagamento_mens,bol_data_criacao, bol_juros valor_juros,cnpj, sf_carteira, endereco, bairro, cidade_nome cidade, estado_sigla estado,cep,bol_desconto,exp_remessa,sf_variacao
                                            from sf_boleto inner join sf_vendas_planos_mensalidade on sf_boleto.id_referencia = sf_vendas_planos_mensalidade.id_mens and tp_referencia = 'M' inner join sf_vendas_planos on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens left join sf_vendas_itens on sf_vendas_itens.id_item_venda = sf_vendas_planos_mensalidade.id_item_venda_mens left join sf_tipo_documento on sf_boleto.tipo_documento = sf_tipo_documento.id_tipo_documento
                                            left join sf_bancos_carteiras on sf_bancos_carteiras.sf_carteiras_id = sf_boleto.carteira_id
                                            inner join sf_fornecedores_despesas on sf_vendas_planos.favorecido = sf_fornecedores_despesas.id_fornecedores_despesas
                                            left join tb_estados on estado = estado_codigo
                                            left join tb_cidades on cidade = cidade_codigo 
                                            where sf_boleto.inativo = 0 " . $comandoX4;
                                            $sQuery1 .= ") as x group by bol_nosso_numero order by pk";
                                            //echo $sQuery1; exit;
                                            $cur = odbc_exec($con, $sQuery1);
                                            $i = 0;
                                            while ($RFP = odbc_fetch_array($cur)) {
                                                $i++;
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php
                                                        $backColor = '';
                                                        if ($RFP['valor_pago'] == 0) {
                                                            if ($RFP['Vecto'] != null) {
                                                                if (escreverData($RFP['Vecto'], "Y-m-d") < date("Y-m-d")) {
                                                                    $backColor = " style='color:red' ";
                                                                }
                                                            }
                                                        }
                                                        if ($RFP['inativo'] == '1') {
                                                            $Estado = "inativo";
                                                        } else if ($RFP['valor_pago'] > 0) {
                                                            $Estado = "pago";
                                                        } else {
                                                            $Estado = "apagar";
                                                        }
                                                        $data = escreverData($RFP['Vecto']);
                                                        $dataCriacao = escreverData($RFP['data_cadastro']);
                                                        if ($Estado == "apagar") {
                                                            $pk = utf8_encode($RFP['pk']);
                                                            echo "<center><input id='check' type='checkbox' class='$pk'  name='items[]' " . ($RFP['exp_remessa'] == 0 ? "checked" : "") . " value='"
                                                            . $pk . "|"
                                                            . utf8_encode($RFP['bol_nosso_numero']) . "|"
                                                            . utf8_encode($RFP['sf_carteira']) . "|"
                                                            . utf8_encode($RFP['documento']) . "|"
                                                            . escreverData($RFP['Vecto'], str_replace("/", "", strtolower($lang['dmy']))) . "|"
                                                            . escreverNumero($RFP['valor_parcela']) . "|"
                                                            . escreverNumero($RFP['valor_juros']) . "|"
                                                            . utf8_encode($RFP['cnpj']) . "|"
                                                            . trataTxt(utf8_encode($RFP['razao_social'])) . "|"
                                                            . trataTxt(utf8_encode($RFP['endereco'])) . "|"
                                                            . trataTxt(utf8_encode($RFP['bairro'])) . "|"
                                                            . utf8_encode($RFP['cep']) . "|"
                                                            . trataTxt(utf8_encode($RFP['cidade'])) . "|"
                                                            . utf8_encode($RFP['estado']) . "|"
                                                            . utf8_encode($RFP['bol_desconto']) . "|"
                                                            . utf8_encode($RFP['sf_variacao']) . "|"
                                                            . "'/></center>";
                                                        } else {
                                                            echo "<center><div style=\"border: 1px solid #BEBEBE;width: 13px;height: 13px;\"></div></center>";
                                                        }
                                                        ?>
                                                    </td>
                                                    <td <?php echo $backColor; ?>><center><div id='formPQ' title='<?php echo $dataCriacao; ?>'><?php echo $dataCriacao; ?></div></center></td>
                                            <td <?php echo $backColor; ?>><center><div id='formPQ' title='<?php echo utf8_encode($RFP['documento']); ?>'><?php echo utf8_encode($RFP['documento']); ?></div></center></td>
                                            <td <?php echo $backColor; ?>><center><div id='formPQ' title='<?php echo utf8_encode($RFP['bol_nosso_numero']); ?>'><?php echo utf8_encode($RFP['bol_nosso_numero']); ?></div></center></td>
                                            <td <?php echo $backColor; ?>><center><div id='formPQ' title='<?php echo $data; ?>'><?php echo $data; ?></div></center></td>
                                            <td <?php echo $backColor; ?>><center><div id='formPQ' title='<?php echo $data; ?>'><?php echo $data; ?></div></center></td>
                                            <td <?php echo $backColor; ?>><center><div id='formPQ' title='<?php echo utf8_encode($RFP['pa']); ?>'><?php echo utf8_encode($RFP['pa']); ?></div></center></td>
                                            <td <?php echo $backColor; ?>><div id='formPQ'  style="color: #08c" title='<?php echo utf8_encode($RFP['razao_social']); ?>'><?php echo utf8_encode($RFP['razao_social']); ?></div></td>
                                            <td <?php echo $backColor; ?>><div id='formPQ' style="text-align: center;height: 100%;"> <img src="../../img/1368741588_check.png" style="height:16px;width:16px;margin: 0 auto;display: <?php echo ($RFP['exp_remessa'] == 1 ? "block" : "none"); ?>" id="<?php echo $pk ?>" alt="" style="height:16px;width:16px;"> </div></td>
                                            <td <?php echo $backColor; ?>><div id='formPQ' style="text-align: right" ><?php echo escreverNumero($RFP['valor_parcela'], 1); ?></div></td>
                                            <td <?php echo $backColor; ?>><center><img src="../../img/<?php echo ($RFP['valor_pago'] > 0 ? "pago.PNG" : "apagar.PNG"); ?>" title=""></center></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div>Total de Registros: <?php echo $i; ?></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="../../fancyapps/source/jquery.fancybox.js?v=2.1.4"></script>
    <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>    
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type="text/javascript">

        $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);

        function AbrirBox(opc) {
            var retorno = "?imp=0";
            if (opc === 1) {
                retorno = "?imp=1";
            }
            if ($('#txtBanco').val() !== "") {
                retorno = retorno + "&id=" + $('#txtBanco').val();
            }
            if ($('#txt_dt_begin').val() !== "") {
                retorno = retorno + "&dtb=" + $('#txt_dt_begin').val();
            }
            if ($('#txt_dt_end').val() !== "") {
                retorno = retorno + "&dte=" + $('#txt_dt_end').val();
            }
            window.location = 'Relatorio-Boletos-Registrados.php' + retorno.replace(/\//g, "_");
        }

        function trocaStatusBox() {
            var todasclass = [];
            $('input:checkbox:checked').each(function () {
                $(this).parent().attr("class", "");
                $('#' + $(this).attr('class')).css("display", "block");
                ;
            });
        }

    </script>        
    <?php
    if ($imprimir == 1) {
        echo "<script language='javascript'>$(\"#formulario\").hide();window.print();window.history.back();</script>";
    }
    odbc_close($con);
    ?>
</body>
</html>
