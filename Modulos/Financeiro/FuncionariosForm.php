<?php
include "../../Connections/configini.php";
include "form/FuncionarioFormServer.php";
include "../../util/util.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
        <title>Cadastro de Funcionário</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <link rel="stylesheet" href="../../js/plugins/jquery.ajax-combobox/dist/jquery.ajax-combobox.css">
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    </head>
    <style>
        input {text-transform: uppercase;}
        #tblItensPag td{
            vertical-align: middle;
        }
        .dataTables_scrollHeadInner{
            width: 100% !important;
        }
        .dataTables_scrollHeadInner table.dataTable.no-footer{
            width: 97% !important;
        }
        form #txtDCCNumCartao {
            background-image: url(../../img/card.png);
            background-position: 0px -114px;
            background-size: 100px 391px;
            background-repeat: no-repeat;
            padding-left: 54px;
        }
        form #txtDCCNumCartao.amex {
            background-position: 0px -59px;
        }
        form #txtDCCNumCartao.diners_club_carte_blanche {
            background-position: 0px -87px;
        }
        form #txtDCCNumCartao.diners_club_international {
            background-position: 0px -87px;
        }
        form #txtDCCNumCartao.visa_electron {
            background-position: 0px -186px;
        }
        form #txtDCCNumCartao.visa {
            background-position: 0px -150px;
        }
        form #txtDCCNumCartao.mastercard {
            background-position: 0px -223px;
        }
        form #txtDCCNumCartao.maestro {
            background-position: 0px -259px;
        }
        form #txtDCCNumCartao.banescard {
            background-position: 0px -355px;
        }          
        form #txtDCCNumCartao.discover {
            background-position: 0px -295px;
        }
        form #txtDCCNumCartao.elo {
            background-position: 0px -30px;
        }
        form #txtDCCNumCartao.aura {
            background-position: 0px -1px;
        }
        form #txtDCCNumCartao.hiper {
            background-position: 0px -324px;
        }
        #tbListaDCC td { padding: 5px; }
        #tbListaDCC td:nth-child(7) { text-align: right; }
    </style>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1 style="margin-top:8px">Funcionário</h1>
                    </div>
                    <div class="row-fluid">
                        <form name="frmFuncionarioForm" id="frmFuncionarioForm">
                            <div class="span12">
                                <div class="block">
                                    <div id="formulario">
                                        <div style="background:#F6F6F6; border:1px solid #DDD; padding:10px">
                                            <div style="width:150px;height:150px; padding:0 2% 0 1%; float:left">
                                                <input id="btnRemImg" class="btn red" type="button" value="x" style="<?php
                                                if (!file_exists("./../../Pessoas/" . $contrato . "/Clientes/" . $id . ".png")) {
                                                    echo "display:none;";
                                                }
                                                ?>margin:0px; padding:2px 4px 3px 4px; line-height:10px;position:absolute; left:24px; z-index:700; cursor:pointer">
                                                <img onclick="UploadForm()" id="ImgCliente" src="
                                                <?php
                                                if ($id != "" and file_exists("./../../Pessoas/" . $contrato . "/Clientes/" . $id . ".png")) {
                                                    $source = file_get_contents("./../../Pessoas/" . $contrato . "/Clientes/" . $id . ".png");
                                                    echo "data:image/png;base64," . base64_encode($source);
                                                } else {
                                                    echo "./../../img/img.png";
                                                }
                                                ?>"
                                                     style="width:100%; height:150px; border-radius:50%; cursor:pointer"/>
                                            </div>
                                            <div style="width:calc(97% - 150px); float:right; padding:8px 0">
                                                <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                                                <input name="txtDisabled" id="txtDisabled" value="<?php echo $disabled; ?>" type="hidden"/>
                                                <input name="txtSendEmail" id="txtSendEmail" type="hidden"/>
                                                <input name="txtSendSms" id="txtSendSms" type="hidden"/>
                                                <div style="width:70%; float:left">
                                                    <span><span style="color:red;">*</span> Nome Completo:</span>
                                                    <input id="txtNome" name="txtNome" type="text" value="<?php echo $nome; ?>" style="height: 40px;font-size:18px; font-weight:500; font-family:'Segoe UI', arial, sans-serif;width: 100%" />
                                                </div>
                                                <div style="width:16%; float:left; margin-left:1%">
                                                    <span>Cadastro:</span>
                                                    <input type="text" value="<?php echo $dtCadastro;?>" style="height: 40px;font-size:18px; font-weight:500; font-family:'Segoe UI', arial, sans-serif" readonly />
                                                </div>
                                                <div style="width:12%; float:left; margin-left:1%">
                                                    <span>Matricula:</span>
                                                    <input type="text" value="<?php echo $id;?>" style="height: 40px;font-size:18px; font-weight:500; font-family:'Segoe UI', arial, sans-serif" readonly />
                                                </div>
                                                <div style="clear:both;height: 10px;"></div>
                                                <div style="float:left;width:70%;">&nbsp;</div>                                                                                  
                                                <div style="float:left;width:30%;text-align: right;">
                                                    <div class="button label-Ativo" style="width:100px; margin:0;display: none;" id="lblAtivo">
                                                        <div class="name"><center>ATIVO</center></div>
                                                    </div>
                                                    <div class="button label-Inativo" style="width:100px; margin:0;display: none;" id="lblInativo">
                                                        <div class="name"><center>INATIVO</center></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="clear:both"></div>
                                        </div>
                                        <div class="data-fluid tabbable" style="margin-top:10px; margin-bottom:15px">
                                            <ul class="nav nav-tabs" style="margin-bottom:0px">
                                                <li class="active"><a href="#tab1" data-toggle="tab"><b>Dados Pessoais</b></a></li>
                                                <li><a href="#tab2" data-toggle="tab"><b>Comissão</b></a></li>
                                                <?php if ($id != "") { ?>
                                                <li><a href="#tab3" data-toggle="tab"><b>Atendimentos</b></a></li>
                                                <li><a href="#tab4" data-toggle="tab"><b>Documentos</b></a></li>
                                                <?php if ($mdl_seg_ == 0) { ?>
                                                <li><a href="#tab5" data-toggle="tab"><b>Acesso</b></a></li>
                                                <?php }} ?>
                                            </ul>
                                            <div class="tab-content" style="overflow: initial; border:1px solid #ddd; border-top:0px; padding-top:10px; background:#F6F6F6">
                                                <div class="tab-pane active" id="tab1">
                                                    <?php include "./../Academia/include/DadosPessoais.php"; ?>
                                                </div>
                                                <div class="tab-pane" id="tab2">                                                    
                                                    <div style="width: 50%; float: left; <?php echo ($comissao_forma_ == "1" ? "display: none;" : ""); ?>">
                                                        <div style="width:20%; float:left;">
                                                            <span>Receber Comissão:</span>
                                                            <select class="select" style="width:100%" id="txtRecebeComissao" name="txtRecebeComissao">
                                                                <option value="1" <?php
                                                                if ($recebe_comiss == "1") {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Sim</option>
                                                                <option value="0" <?php
                                                                if ($recebe_comiss == "0") {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Não</option>
                                                            </select>
                                                        </div>
                                                        <div style="clear:both; height:5px"></div>
                                                        <div style="width:35%; float:left;">
                                                            <span>Comissão Produto:</span>
                                                            <select class="select" style="width:100%" id="selComProd" name="selComProd">
                                                                <option value="0" <?php
                                                                if ($tbcomiss_prod == "0") {
                                                                    echo "selected";
                                                                }
                                                                ?>>Não comissionado</option>
                                                                <option value="1" <?php
                                                                if ($tbcomiss_prod == "1") {
                                                                    echo "selected";
                                                                }
                                                                ?>>Comissão por Produtos</option>
                                                                <option value="2" <?php
                                                                if ($tbcomiss_prod == "2") {
                                                                    echo "selected";
                                                                }
                                                                ?>>Comissão por Funcionários</option>
                                                            </select>
                                                        </div>
                                                        <div style="width:25%; float:left;margin-left: 1%;">
                                                            <span>Valor (%):</span>
                                                            <input id="txtComProd" name="txtComProd" style="width:100%" type="text" class="input-medium" maxlength="10" value="<?php echo $comiss_prod; ?>">
                                                        </div>
                                                        <div style="width:25%; float:left;margin-left: 1%;">
                                                            <span>Forma:</span>
                                                            <select class="select" style="width:100%" id="selComProdTipo" name="selComProdTipo">
                                                                <option value="0" <?php
                                                                if ($tbcomiss_prod_tipo == "0") {
                                                                    echo "selected";
                                                                }
                                                                ?>>Por Responsável</option>
                                                                <option value="1" <?php
                                                                if ($tbcomiss_prod_tipo == "1") {
                                                                    echo "selected";
                                                                }
                                                                ?>>Por Associação</option>
                                                            </select>
                                                        </div>                                                        
                                                        <div style="clear:both; height:5px"></div>
                                                        <div style="width:35%; float:left;">
                                                            <span>Comissão Serviço:</span>
                                                            <select class="select" style="width:100%" id="selComServ" name="selComServ">
                                                                <option value="0" <?php
                                                                if ($tbcomiss_serv == "0") {
                                                                    echo "selected";
                                                                }
                                                                ?>>Não comissionado</option>
                                                                <option value="1" <?php
                                                                if ($tbcomiss_serv == "1") {
                                                                    echo "selected";
                                                                }
                                                                ?>>Comissão por Serviços</option>
                                                                <option value="2" <?php
                                                                if ($tbcomiss_serv == "2") {
                                                                    echo "selected";
                                                                }
                                                                ?>>Comissão por Funcionários</option>
                                                            </select>
                                                        </div>
                                                        <div style="width:25%; float:left;margin-left: 1%">
                                                            <span>Valor (%):</span>
                                                            <input id="txtComServ" name="txtComServ" style="width:100%" type="text" class="input-medium" maxlength="10" value="<?php echo $comiss_serv; ?>">
                                                        </div>
                                                        <div style="width:25%; float:left;margin-left: 1%;">
                                                            <span>Forma:</span>
                                                            <select class="select" style="width:100%" id="selComSerTipo" name="selComSerTipo">
                                                                <option value="0" <?php
                                                                if ($tbcomiss_ser_tipo == "0") {
                                                                    echo "selected";
                                                                }
                                                                ?>>Por Responsável</option>
                                                                <option value="1" <?php
                                                                if ($tbcomiss_ser_tipo == "1") {
                                                                    echo "selected";
                                                                }
                                                                ?>>Por Associação</option>
                                                            </select>
                                                        </div>                                                        
                                                        <?php if ($mdl_aca_ == 1) { ?>
                                                            <div style="clear:both; height:5px"></div>
                                                            <div style="width:35%; float:left;">
                                                                <span>Comissão Plano:</span>
                                                                <select class="select" style="width:100%" id="selComPlano" name="selComPlano">
                                                                    <option value="0" <?php
                                                                    if ($tbcomiss_plano == "0") {
                                                                        echo "selected";
                                                                    }
                                                                    ?>>Não comissionado</option>
                                                                    <option value="1" <?php
                                                                    if ($tbcomiss_plano == "1") {
                                                                        echo "selected";
                                                                    }
                                                                    ?>>Comissão por Plano</option>
                                                                    <option value="2" <?php
                                                                    if ($tbcomiss_plano == "2") {
                                                                        echo "selected";
                                                                    }
                                                                    ?>>Comissão por Funcionários</option>
                                                                </select>
                                                            </div>
                                                            <div style="width:25%; float:left;margin-left: 1%">
                                                                <span>Valor Recorrente (%):</span>
                                                                <input id="txtComPlano" name="txtComPlano" style="width:100%" type="text" class="input-medium" maxlength="10" value="<?php echo $comiss_plano; ?>">
                                                            </div>
                                                            <div style="width:25%; float:left;margin-left: 1%">
                                                                <span>Valor 1ª Parcela (%):</span>
                                                                <input id="txtComPlanoPri" name="txtComPlanoPri" style="width:100%" type="text" class="input-medium" maxlength="10" value="<?php echo $comiss_planoPri; ?>">
                                                            </div>
                                                        <?php } ?>
                                                        <div style="clear:both; height:5px"></div>
                                                        <div style="width:35%; float:left;">
                                                            <span>Comissão Plano Gerencia:</span>
                                                            <select class="select" style="width:100%" id="selComGere" name="selComGere">
                                                                <option value="0" <?php
                                                                if ($tbcomiss_plano == "0") {
                                                                    echo "selected";
                                                                }
                                                                ?>>Não comissionado</option>
                                                                <option value="1" <?php
                                                                if ($tbcomiss_plano == "1") {
                                                                    echo "selected";
                                                                }
                                                                ?>>Comissão por Plano</option>
                                                                <option value="2" <?php
                                                                if ($tbcomiss_plano == "2") {
                                                                    echo "selected";
                                                                }
                                                                ?>>Comissão por Funcionários</option>
                                                            </select>
                                                        </div>
                                                        <div style="width:25%; float:left;margin-left: 1%">
                                                            <span>Valor Recorrente (%):</span>
                                                            <input id="txtComGere" name="txtComGere" style="width:100%" type="text" class="input-medium" maxlength="10" value="<?php echo $comiss_gere; ?>">
                                                        </div>
                                                        <div style="width:25%; float:left;margin-left: 1%">
                                                            <span>Valor 1ª Parcela (%):</span>
                                                            <input id="txtComGerePri" name="txtComGerePri" style="width:100%" type="text" class="input-medium" maxlength="10" value="<?php echo $comiss_gerePri; ?>">
                                                        </div>
                                                        <div style="clear:both; height:5px"></div>
                                                    </div>
                                                    <?php if ($id && $data_demissao == "" && $inativo == "0" && $mdl_seg_ == 1) {
                                                            $url = 'https://portal.sivisweb.com.br/loja/'.$contrato.'/';
                                                            $urlLink = strtolower($url."consultor/" . $id);
                                                            $urlQrCode = $UrlAtual.'/util/qrcode/php/qr_img.php?d='.$urlLink.'&s=8';
                                                        ?>
                                                        <div style="width: 49%; float: left; margin-left: 1%;">
                                                            <div style="width:100%; margin:10px 0">
                                                                <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Link do Funcionário</span>
                                                                <hr style="margin:2px 0; border-top:1px solid #ddd">
                                                            </div>
                                                            <div style="width: 30%; float: left;">
                                                                <img  src="<?= $urlQrCode?>" alt="qrcode" style="max-width: 100%; display: block; max-height: 230px;">
                                                            </div>
                                                            <div style="width:68%; float: left; margin-left: 2%;">
                                                                <div style="width: 100%;">
                                                                    <div style="width: 100%; float: left; margin-left: 1%;">
                                                                        <span>Link do Funcionário:</span>
                                                                        <div style="width: 100%;">
                                                                            <div style="width: 62%; float: left;">
                                                                                <input type="text" id="txtLinkFunc" style="width: 100%; text-transform: none !important;" readonly value="<?= $urlLink ?>" <?= $disabled; ?>>
                                                                                <input type="hidden" name="mdl" value="<?= $mdl_seg_ ?>">
                                                                            </div>
                                                                            <div style="width: 32%; float: left; margin-left: 2%;">
                                                                                <button class="btn btn-success" type="button" title="link" id="btnLink" style="width: 100%;">
                                                                                    <span class="ico-plus-sign"> </span> Copiar Link
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div style="width: 100%; margin-top: 5px; float: left; margin-left: 1%;">
                                                                        <a  href="<?= $urlQrCode ?>" class="btn  btn-success" title="Novo" download>
                                                                            <span class="ico-download-alt"> </span> Download do Qrcode
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div style="clear:both; height:5px"></div>
                                                        </div>
                                                    <?php } ?>
                                                    <div style="clear:both; height:5px"></div>
                                                    <div style="width:33%; float:left;">
                                                        <div style="width:100%; margin:10px 0">
                                                            <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Equipe</span>
                                                            <hr style="margin:2px 0; border-top:1px solid #ddd">
                                                        </div>
                                                        <select name="items[]" multiple="multiple" id="mscEquipe">
                                                            <?php
                                                            $id = $id == "" ? -1 : $id;
                                                            $cur = odbc_exec($con, "select u.id_usuario,nome,isnull(ud.id_fornecedor_despesas,0) fd from dbo.sf_usuarios u 
                                                            left join dbo.sf_usuarios_dependentes ud on u.id_usuario = ud.id_usuario and (ud.id_fornecedor_despesas is null or ud.id_fornecedor_despesas = '" . $id . "')
                                                            where u.inativo = 0 order by nome") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) {
                                                                ?>
                                                                <option value="<?php echo $RFP['id_usuario'] ?>"<?php
                                                                if (!(strcmp($RFP['fd'], $id))) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>><?php echo utf8_encode($RFP['nome']); ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div style="width:32%; float:left;margin-left: 1%">
                                                        <div style="width:100%; margin:10px 0">
                                                            <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Serviços</span>
                                                            <hr style="margin:2px 0; border-top:1px solid #ddd">
                                                        </div>
                                                        <select name="itemsGrupo[]" multiple="multiple" id="mscGrupos">
                                                            <?php
                                                            $idUser = $id == '' ? 'null' : $id;
                                                            $cur = odbc_exec($con, "select id_contas_movimento, descricao,
                                                            case when id_contas_movimento in( select id_grupo_servico from sf_fornecedores_despesas_grupo_servicos where id_funcionario = " . $idUser . ") then 'S' else 'N' end GrpUser
                                                            from dbo.sf_contas_movimento where tipo = 'S' and inativa = 0 order by 2 asc") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) {
                                                                ?>
                                                                <option value="<?php echo $RFP['id_contas_movimento'] ?>"<?php
                                                                if ($RFP['GrpUser'] == 'S') {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                                    <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div style="width:33%; float:left; margin-left: 1%">
                                                        <div style="width:100%; margin:10px 0">
                                                            <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">BlackList</span>
                                                            <hr style="margin:2px 0; border-top:1px solid #ddd">
                                                        </div>
                                                        <select name="itemsBlack[]" multiple="multiple" id="mscEquipeBlack">
                                                            <?php
                                                            $id = $id == "" ? -1 : $id;
                                                            $cur = odbc_exec($con, "select u.id_usuario,nome,isnull(ud.id_fornecedor_despesas,0) fd from dbo.sf_usuarios u 
                                                            left join dbo.sf_usuarios_lista_negra ud on u.id_usuario = ud.id_usuario and (ud.id_fornecedor_despesas is null or ud.id_fornecedor_despesas = '" . $id . "')
                                                            where u.inativo = 0 order by nome") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) {
                                                                ?>
                                                                <option value="<?php echo $RFP['id_usuario'] ?>"<?php
                                                                if (!(strcmp($RFP['fd'], $id))) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>><?php echo utf8_encode($RFP['nome']); ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>                                                    
                                                    <div style="clear:both; height:5px"></div>
                                                </div>
                                                <div class="tab-pane" id="tab3">
                                                    <div style="width:18%; float:left;">
                                                        <span>Pendências em aberto Para:</span>
                                                        <select class="select" id="txtTransfPendencias" name="txtTransfPendencias" style="width:100%">
                                                            <option value="">Selecione o Usuário</option>
                                                            <?php
                                                            $cur = odbc_exec($con, "select id_usuario, nome from sf_usuarios where inativo = 0 and id_usuario > 1 and id_usuario not in (select id_usuario from sf_usuarios where funcionario = " . $id . ")") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['id_usuario'] ?>"><?php echo formatNameCompact(strtoupper(utf8_encode($RFP['nome']))); ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div style="width:5%; float:left;margin:16px 0 0 10px">
                                                        <button class="btn btn-success" type="button" title="Transferir" name="btnTransfPendencias" id="btnTransfPendencias"><span class="ico-checkmark"></span></button>
                                                    </div>
                                                    <div style="width:18%; float:left;">
                                                        <span>Usuário Responsável (Cli|Pro) Para:</span>
                                                        <select class="select" id="txtTransfResponsavel" name="txtTransfResponsavel" style="width:100%">
                                                            <option value="">Selecione o Usuário</option>
                                                            <?php
                                                            $cur = odbc_exec($con, "select id_usuario, nome from sf_usuarios where inativo = 0 and id_usuario > 1 and id_usuario not in (select id_usuario from sf_usuarios where funcionario = " . $id . ")") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['id_usuario'] ?>"><?php echo formatNameCompact(strtoupper(utf8_encode($RFP['nome']))); ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div style="width:5%; float:left;margin:16px 0 0 10px;">
                                                        <button class="btn btn-success" type="button" title="Transferir" name="btnTransfResponsavel" id="btnTransfResponsavel"><span class="ico-checkmark"></span></button>
                                                    </div>
                                                    <div style="width:18%; float:left;">
                                                        <span>Usuário Responsável (Lead) Para:</span>
                                                        <select class="select" id="txtTransfLead" name="txtTransfLead" style="width:100%">
                                                            <option value="">Selecione o Usuário</option>
                                                            <?php
                                                            $cur = odbc_exec($con, "select id_usuario, nome from sf_usuarios where inativo = 0 and id_usuario > 1 and id_usuario not in (select id_usuario from sf_usuarios where funcionario = " . $id . ")") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['id_usuario'] ?>"><?php echo formatNameCompact(strtoupper(utf8_encode($RFP['nome']))); ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div style="width:5%; float:left;margin:16px 0 0 10px;">
                                                        <button class="btn btn-success" type="button" title="Transferir" name="btnTransfResponsavelLead" id="btnTransfResponsavelLead"><span class="ico-checkmark"></span></button>
                                                    </div>
                                                    <?php if ($comissao_forma_ > 0 && $ckb_com_forma_ > 0) { ?>
                                                    <div style="width:18%; float:left;">
                                                        <span>Comissão por Cliente Para:</span>
                                                        <select class="select" id="txtTransfComissaoCliente" name="txtTransfComissaoCliente" style="width:100%">
                                                            <option value="">Selecione o Usuário</option>
                                                            <?php
                                                            $cur = odbc_exec($con, "select id_usuario, nome from sf_usuarios where inativo = 0 and id_usuario > 1 and id_usuario not in (select id_usuario from sf_usuarios where funcionario = " . $id . ")") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['id_usuario'] ?>"><?php echo formatNameCompact(strtoupper(utf8_encode($RFP['nome']))); ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div style="width:5%; float:left;margin:16px 0 0 10px;">
                                                        <button class="btn btn-success" type="button" title="Transferir" name="btnTransfComissaoCliente" id="btnTransfComissaoCliente"><span class="ico-checkmark"></span></button>
                                                    </div>
                                                    <?php } ?>
                                                    <div style="clear:both; height:5px"></div>                                                    
                                                    <?php include "../CRM/include/CRMFormTelemarketing.php";?>                                                    
                                                </div>
                                                <div class="tab-pane" id="tab4">
                                                    <?php include "../CRM/include/Documentos.php";?>
                                                </div>
                                                <div class="tab-pane" id="tab5">
                                                    <?php include "./../Academia/include/AcademiaAcesso.php"; ?>
                                                </div>
                                            </div>
                                            <div style="clear:both"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>               
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/highlight/jquery.highlight-4.js'></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/amcharts.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/pie.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/serial.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/light.js"></script>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/jquery.creditCardValidator.js"></script>
        <script type="text/javascript" src="../../js/moment.min.js"></script>
        <script type="text/javascript" src="../../js/jquery.cpfcnpj.min.js"></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.ajax-combobox/dist/jquery.ajax-combobox.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type='text/javascript' src='../../js/plugins/multiselect/jquery.multi-select.min.js'></script> 
        <script type="text/javascript" src="../../js/util.js"></script>   
        <script type="text/javascript" src="../Academia/js/DadosPessoais.js"></script>
        <script type="text/javascript" src="js/FuncionarioForm.js"></script>   
        <script type="text/javascript" src="../CRM/js/CRMFormTelemarketing.js"></script>
        <script type="text/javascript" src="../CRM/js/CRMDocumentos.js"></script>        
        <script type="text/javascript" src="../Academia/js/AcademiaAcesso.js"></script>                        
    </body>
    <?php odbc_close($con); ?>
</html>
