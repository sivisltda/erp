<?php
include './../../Connections/configini.php';

function getTotalGrupo($tp, $gp, $array) {
    $total = 0;
    for ($i = 0; $i < sizeof($array); $i++) {
        if ($tp == $array[$i][0] && $array[$i][1] == $gp) {
            $total = $total + $array[$i][6];
        }
    }
    return $total;
}

function getTotalSubGrupo($tp, $gp, $sg, $array) {
    $total = 0;
    for ($i = 0; $i < sizeof($array); $i++) {
        if ($tp == $array[$i][0] && $array[$i][1] == $gp && $array[$i][2] == $sg) {
            $total = $total + $array[$i][6];
        }
    }
    return $total;
}

function totalPorcentagem($total, $item) {
    $retorno = 0;
    if ($total > 0) {
        $retorno = ($item * 100) / $total;
    }
    return escreverNumero($retorno);
}

$gsg = "";
$totalR = 0;
$totalD = 0;
$F2 = "";
$F3 = 1;
$DateBegin = getData("B");
$DateEnd = getData("E");
$empresa = 1;
if (is_numeric($_GET['tp'])) {
    $F2 = $_GET['tp'];
}
if (is_numeric($_GET['ts'])) {
    $F3 = $_GET['ts'];
}
if ($_GET['dti'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
}
if ($_GET['dtf'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
}
if (is_numeric($_GET['emp'])) {
    $empresa = $_GET['emp'];
}

$query = "set dateformat dmy;select sf_contas_movimento.tipo,sf_grupo_contas.descricao grupo,sf_contas_movimento.descricao subgrupo,data_pagamento,razao_social,p.historico_baixa,valor_pago 
from sf_lancamento_movimento_parcelas p 
inner join sf_lancamento_movimento l on p.id_lancamento_movimento = l.id_lancamento_movimento
inner join sf_contas_movimento on l.conta_movimento = sf_contas_movimento.id_contas_movimento
inner JOIN sf_grupo_contas ON l.grupo_conta = sf_grupo_contas.id_grupo_contas
inner join sf_fornecedores_despesas on l.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas
where l.status = 'Aprovado' and data_pagamento is not null and p.inativo = 0 
and data_pagamento between " . valoresDataHora2($DateBegin, "00:00:00") . " and " . valoresDataHora2($DateEnd, "23:59:59") . " and l.empresa = " . $empresa . " union all
select sf_contas_movimento.tipo,sf_grupo_contas.descricao grupo,sf_contas_movimento.descricao subgrupo,data_pagamento,razao_social,historico_baixa,valor_pago
from sf_solicitacao_autorizacao_parcelas p inner join sf_solicitacao_autorizacao l on l.id_solicitacao_autorizacao = p.solicitacao_autorizacao
inner join sf_contas_movimento on l.conta_movimento = sf_contas_movimento.id_contas_movimento
inner JOIN sf_grupo_contas ON l.grupo_conta = sf_grupo_contas.id_grupo_contas
inner join sf_fornecedores_despesas on l.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas
where status = 'Aprovado' and sf_contas_movimento.tipo in ('C','D') and data_pagamento is not null and p.inativo = 0 
and data_pagamento between " . valoresDataHora2($DateBegin, "00:00:00") . " and " . valoresDataHora2($DateEnd, "23:59:59") . " and l.empresa = " . $empresa . " union all
SELECT case when cov = 'V' then 'C' else 'D' end tipo,sf_grupo_contas.descricao grupo,sf_contas_movimento.descricao subgrupo,data_pagamento,razao_social,historico_baixa,valor_pago 
from sf_venda_parcelas p inner join sf_vendas l on l.id_venda = p.venda 
inner join sf_contas_movimento on l.conta_movimento = sf_contas_movimento.id_contas_movimento
inner JOIN sf_grupo_contas ON l.grupo_conta = sf_grupo_contas.id_grupo_contas
inner join sf_fornecedores_despesas on l.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas
where status = 'Aprovado' and cov in ('V','C') and data_pagamento is not null and p.inativo = 0 
and data_pagamento between " . valoresDataHora2($DateBegin, "00:00:00") . " and " . valoresDataHora2($DateEnd, "23:59:59") . " and l.empresa = " . $empresa . " order by 1,2,3,4,5";
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $arrayLista[] = array(utf8_encode($RFP['tipo']), utf8_encode($RFP['grupo']), utf8_encode($RFP['subgrupo']), escreverData($RFP['data_pagamento']), utf8_encode($RFP['razao_social']), utf8_encode($RFP['historico_baixa']), $RFP['valor_pago']);
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
            body {
                margin:0px;
                padding:0px;
            }
            table {
                margin-bottom: 10px;
                font-size: 8px;
            }
            .titulo {
                font-weight: Bold;
                text-align: center;
            }
            .tabela {
                height: 5px;
                padding-left: 5px;
                padding-right: 5px;
            }            
        </style>
    </head>
    <body>
        <div id="main" style="width: 700px; overflow:hidden;">
            <table width="100%" border="1" cellpadding="2" cellspacing="0">
                <tr>
                    <td width="500px" rowspan="2">
                        <table width="100%" cellpadding="0" border="0" cellspacing="0" style="border:0px solid white;padding-bottom: 15px !important;">
                            <tr>
                                <td width="200px">
                                    <?php if (file_exists("./../Pessoas/" . $_SESSION["contrato"] . "/Empresa/logo_" . $filial_orc . ".jpg")) { ?>
                                        <div style="line-height: 5px;">&nbsp;</div><img src ="./../Pessoas/<?php echo $_SESSION["contrato"]; ?>/Empresa/logo_<?php echo $filial_orc; ?>.jpg" width="150" height="50" align="middle" />
                                    <?php } else { ?>
                                        <div style="line-height: 5px;">&nbsp;</div>
                                    <?php } ?>
                                </td>
                                <td width="265px" >
                                    <div style="text-align:right;font-size: 8px;"><?php echo $_SESSION["cabecalho"]; ?></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="200px" colspan="2" align="center" style="line-height: 25px;font-size:16px;">
                        <b><?php echo "Demonstrativo de Receitas e Despesas"; ?></b>
                    </td>
                </tr>                
                <tr>
                    <td style="padding-left:5px;width: 50px;">Entre:</td>
                    <td align="center" style="width: 150px;"><?php echo $DateBegin . " a " . $DateEnd; ?></td>
                </tr>
            </table>   
            <br><br>
            <table width="700px" >            
                <?php
                for ($l = 1; $l <= 2; $l++) {
                    if ($F2 == "" || ($F2 == "0" && $l == 1) || ($F2 == "1" && $l == 2)) {
                        if ($l == 1) {
                            $tipo = "C";
                        } else {
                            $tipo = "D";
                        }
                        $Grupo = "";
                        $SubGrupo = "";
                        $total = 0;
                        for ($i = 0; $i < sizeof($arrayLista); $i++) {
                            if ($tipo == $arrayLista[$i][0]) {
                                $total = $total + $arrayLista[$i][6];
                            }
                        }
                if ($l == 1) {
                    $totalR = $total;
                } else {
                    $totalD = $total;
                } ?>
            <tr style="background-color: gray;">
                <td style="text-align: center;font-weight: Bold;font-size: 12px;color: white" colspan="8"><?php echo ($tipo == "D" ? "DESPESAS" : "RECEITAS"); ?></td>
            </tr> 
            <tr style="background-color: lightgrey">
                <td style="width: 3%" class="titulo"></td>
                <td colspan="2" style="width: 9%;text-align: right;" class="titulo">DATA</td>
                <td style="width: 37%;font-size: 9px;" class="titulo">FORNECEDOR/CLIENTE</td>
                <td style="width: 30%;font-size: 9px;" class="titulo">HISTÓRICO</td>
                <td style="width: 8%" class="titulo"></td>
                <td style="width: 3%" class="titulo"></td>
                <td style="width: 10%;font-size: 9px;" class="titulo">VALOR</td>
            </tr>                    
        <?php
        for ($i = 0; $i < sizeof($arrayLista); $i++) {
            if ($tipo == $arrayLista[$i][0] && $Grupo != $arrayLista[$i][1]) {
                $Grupo = $arrayLista[$i][1]; ?>
                <tr>
                    <td class="titulo" style="text-align: left;" colspan="6"><?php echo strtoupper($arrayLista[$i][1]); ?></td>
                    <td class="titulo" style="text-align: center;"><?php echo $lang['prefix'];?></td>
                    <td class="titulo" style="text-align: right;"><?php echo escreverNumero(getTotalGrupo($tipo, $Grupo, $arrayLista)); ?></td>
                </tr>                            
                <?php
                for ($j = 0; $j < sizeof($arrayLista); $j++) {
                    $SubGrupo = $arrayLista[$j][2];                                     
                    if ($tipo == $arrayLista[$j][0] && $Grupo == $arrayLista[$j][1] && $SubGrupo == $arrayLista[$j][2] && $gsg != $arrayLista[$j][1] . $arrayLista[$j][2]) {
                        $gsg = $arrayLista[$j][1] . $arrayLista[$j][2]; ?>
                        <tr>
                            <td></td>
                            <td class="titulo" style="text-align: left;" colspan="4"><?php echo strtoupper($arrayLista[$j][2]); ?></td>
                            <td class="titulo" style="text-align: center;"><?php echo totalPorcentagem($total, getTotalSubGrupo($tipo, $Grupo, $SubGrupo, $arrayLista)); ?>%</td>
                            <td colspan="2"></td>
                        </tr>                                    
                        <?php
                        $subTotal = 0;
                        for ($k = 0; $k < sizeof($arrayLista); $k++) {
                            if ($tipo == $arrayLista[$k][0] && $Grupo == $arrayLista[$k][1] && $SubGrupo == $arrayLista[$k][2]) {
                                if ($F3 == 1) { ?>
                                <tr>
                                    <td colspan="3" style="text-align: right;"><?php echo strtoupper($arrayLista[$k][3]); ?></td>
                                    <td><?php echo substr(strtoupper($arrayLista[$k][4]), 0, 41); ?></td>
                                    <td><?php echo strtoupper($arrayLista[$k][5]); ?></td>
                                    <td style="text-align: center;"><?php echo totalPorcentagem($total, $arrayLista[$k][6]); ?>%</td>
                                    <td style="text-align: center;"><?php echo $lang['prefix'];?></td>
                                    <td style="text-align: right;"><?php echo escreverNumero($arrayLista[$k][6]); ?></td>
                                </tr>                                            
                                <?php
                                }
                                $subTotal = $subTotal + $arrayLista[$k][6];
                            }
                        } ?>
                        <tr>
                            <td style="text-align: left;" colspan="5"></td>
                            <td class="titulo" style="text-align: center;">SUBTOTAL</td>
                            <td style="text-align: center;"><?php echo $lang['prefix'];?></td>
                            <td style="text-align: right;"><?php echo escreverNumero($subTotal); ?></td>
                        </tr>   
                        <tr>
                            <td colspan="8"></td>
                        </tr>
                    <?php
                    }
                }
            }
        } ?>
        <tr>
            <td class="titulo" style="text-align: left;" colspan="4"></td>
            <td class="titulo" style="text-align: right;" colspan="2">TOTAL DE <?php echo ($tipo == "D" ? "DESPESAS" : "RECEITAS"); ?></td>
            <td class="titulo" style="text-align: center"><?php echo $lang['prefix'];?></td>
            <td class="titulo" style="text-align: right"><?php echo escreverNumero($total); ?></td>
        </tr>               
    <?php }
} if ($F2 == "") { ?>
    <tr>
        <td colspan="8"></td>
    </tr>                    
    <tr style="background-color: gray">
        <td colspan="8"></td>
    </tr>
    <tr>
        <td style="text-align: left;" colspan="4"></td>
        <td style="text-align: right;font-size: 9px;" colspan="2">TOTAL DE RECEITAS</td>
        <td style="text-align: center"><?php echo $lang['prefix'];?></td>
        <td style="text-align: right;font-size: 9px;"><?php echo escreverNumero($totalR); ?></td>
    </tr>
    <tr>
        <td style="text-align: left;" colspan="4"></td>
        <td style="text-align: right;font-size: 9px;" colspan="2">TOTAL DE DESPESAS</td>
        <td style="text-align: center"><?php echo $lang['prefix'];?></td>
        <td style="text-align: right;font-size: 9px;"><?php echo escreverNumero($totalD); ?></td>
    </tr>
    <tr>
        <td class="titulo" style="text-align: left;" colspan="4"></td>
        <td class="titulo" style="text-align: right;font-size: 12px;" colspan="2">DIFERENÇA</td>
        <td class="titulo" style="text-align: center"><?php echo $lang['prefix'];?></td>
        <td class="titulo" style="text-align: right;font-size: 12px;"><?php echo escreverNumero(($totalR - $totalD)); ?></td>
    </tr>                 
<?php } ?>
    </table>
</div>
    </body>
<?php echo odbc_close($con); ?>
</html>            