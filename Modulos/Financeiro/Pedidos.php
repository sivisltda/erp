<?php
include '../../Connections/configini.php';
$DateBegin = getData("B");
$DateEnd = getData("E");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/bootbox.css"/>        
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <style>                         
            #formulario {
                float: left;
                width: 100%;
            }
            #formulario form .linha {
                float: left;
                height: 26px;
                width: 100%;
                display: block;
                border-bottom-width: 1px;
                border-bottom-style: solid;
                border-bottom-color: #f7f7f7;
                background-color: #f7f7f7;
                border-top-width: 1px;
                border-top-style: solid;
                border-top-color: #FFF;
                padding-top: 10px;
                padding-bottom: 10px;
            }
        </style>          
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <form method="POST" action="Vendas.php">
                        <div class="page-header">
                            <div class="icon"> <span class="ico-arrow-right"></span> </div>
                            <h1>Financeiro<small>Pedidos</small></h1>
                        </div>
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="block">
                                    <div id="formulario">       
                                        <span style="padding-left:0px">
                                            <input id="txtFilial" name="txtFilial" type="hidden" value="<?php echo $idFilial; ?>"/>                                           
                                            <button type="button" name="btnPrint" id="btnPrint" class="button button-blue btn-primary" title="Imprimir"><span class="ico-print"></span></button>
                                            <button type="button" name="btnAprov" id="btnAprov" title="Aprovar" class="button button-green btn-primary" style="display: none" disabled="true"><span class="icon-ok icon-white"></span></button>
                                            <button type="button" name="btnCancel" id="btnCancel" title="Reprovar" class="button button-red btn-primary" style="display: none" disabled="true"><span class="ico-cancel icon-white"></span></button>                                            
                                            <select class="select" style="width:129px" name="txtEstado" id="txtEstado" >
                                                <option value="null">Selecione</option>
                                                <option value="1" selected>Aguardando</option>                                                
                                                <option value="2">Aprovadas</option>                                                
                                                <option value="3">Reprovadas</option>                                                
                                                <option value="5">Ativas</option>                                                
                                                <option value="4">Todas</option>                                                
                                            </select>
                                            <select class="select" style="width:175px" name="txtTipoBusca" id="txtTipoBusca" >
                                                <option value="null">Selecione</option>                                                   
                                                <option value="0">Clientes</option>
                                                <option value="1">Grupo de Conta</option>
                                                <option value="2">Subgrupo de Conta</option>
                                                <option value="3">Forma de Pagamento</option>              
                                                <option value="4">Responsável</option>
                                                <option value="5">Vendedor</option>                                                    
                                            </select>                                             
                                            <input id="txtValue" name="txtValue" type="hidden" value=""/>
                                            <input style="width: 150px;vertical-align: top;color:#000 !important;" type="text" name="txtDesc" id="txtDesc" class="inputbox" value="" size="10" />                                                                                                    
                                            De <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial">
                                            até <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final">
                                            <button id="btnfind" name="btnfind" class="button button-turquoise btn-primary buttonX" type="button" title="Buscar"><span class="ico-search icon-white"></span></button>
                                        </span>                                        
                                    </div>
                                </div>
                            </div>
                            <div style="clear:both"></div>
                            <div class="boxhead">
                                <div class="boxtext">Pedidos</div>
                            </div>
                            <div class="boxtable">
                                <table class="table" cellpadding="0" cellspacing="0" width="100%" id="example">
                                    <thead>
                                        <tr>      
                                            <th width="4%" style="top:0"><input id="checkAll" type="checkbox" class="checkall"/></th>
                                            <th width="10%"><center>Data</center></th>
                                            <th width="4%"><center>Loja</center></th>
                                            <th width="4%"><center>Cod.</center></th>
                                            <th width="4%"><center>Prev.Fech.</center></th>
                                            <th width="19%">Cliente</th>
                                            <th width="12%"><center>Cidade</center></th>
                                            <th width="5%"><center>UF</center></th>                                            
                                            <th width="6%"><center>Vendedor</center></th>                                                    
                                            <th width="8%" style="text-align:right">Tot.Prod.</th>
                                            <th width="8%" style="text-align:right">Tot.Serv.</th>
                                            <th width="8%" style="text-align:right">Tot.Geral</th>
                                            <th width="8%"><center>Status:</center></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                                <table id="lblTotParc" name="lblTotParc" class="table" cellpadding="0" cellspacing="0" width="100%">                                            
                                    <tr>
                                        <td style="text-align: right">
                                            <div id="lblTotParc2" name="lblTotParc2">
                                                Número de Pedidos :<strong>   0</strong>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">
                                            <div id="lblTotParc3" name="lblTotParc3">
                                                Total em Pedidos :<strong><?php echo escreverNumero(0,1); ?></strong>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">
                                            <div id="lblTotParc4" name="lblTotParc4">
                                                Ticket Médio :<strong><?php echo escreverNumero(0,1); ?></strong>
                                            </div>            
                                        </td>
                                    </tr>
                                </table> 
                            </div>                            
                        </div>                                                            
                    </form>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />                        
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>    
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script> 
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>          
        <script type="text/javascript">
            
            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
            
            $(document).ready(function () {
                $('#example').dataTable({
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": finalFind(0),
                    'oLanguage': {
                        'oPaginate': {
                            'sFirst': "Primeiro",
                            'sLast': "Último",
                            'sNext': "Próximo",
                            'sPrevious': "Anterior"
                        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                        'sLengthMenu': "Visualização de _MENU_ registros",
                        'sLoadingRecords': "Carregando...",
                        'sProcessing': "Processando...",
                        'sSearch': "Pesquisar:",
                        'sZeroRecords': "Não foi encontrado nenhum resultado"},
                    "fnDrawCallback": function () {
                        refreshTotal();
                    },
                    "sPaginationType": "full_numbers"
                });

                $("#txtDesc").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "./../CRM/ajax.php",
                            dataType: "json",
                            data: {
                                q: request.term,
                                t: typeSelect()
                            },
                            success: function (data) {
                                response(data);
                            }
                        });
                    },
                    minLength: 3,
                    select: function (event, ui) {
                        $("#txtValue").val(ui.item.id);
                    }
                });

                function typeSelect() {
                    if ($("#txtTipoBusca").val() === "1") {
                        return "GCC";
                    } else if ($("#txtTipoBusca").val() === "2") {
                        return "CMC";
                    } else if ($("#txtTipoBusca").val() === "3") {
                        return "FPC";
                    } else if ($("#txtTipoBusca").val() === "4") {
                        return "UR";
                    } else if ($("#txtTipoBusca").val() === "5") {
                        return "UR";
                    } else {
                        return "C";
                    }
                }

                function finalFind(imp) {
                    if ($("#txtEstado").val() === "1") {
                        $("#btnAprov").show();
                        $("#btnCancel").show();
                        $("#btnNfs").hide();
                    } else if ($("#txtEstado").val() === "2") {
                        $("#btnAprov").hide();
                        $("#btnCancel").hide();
                        $("#btnNfs").show();
                    } else {
                        $("#btnAprov").hide();
                        $("#btnCancel").hide();
                        $("#btnNfs").hide();
                    }
                    var finalURL = 'Pedidos_server_processing.php?imp=' + imp;
                    if ($("#txtEstado").val() !== "") {
                        finalURL = finalURL + '&id=' + jQuery("#txtEstado").val();
                    }
                    if ($("#txtTipoBusca").val() !== "") {
                        finalURL = finalURL + '&tp=' + jQuery("#txtTipoBusca").val();
                    }
                    if ($("#txtValue").val() !== "") {
                        finalURL = finalURL + '&td=' + jQuery("#txtValue").val();
                    }
                    if ($("#txtFilial").val() !== "") {
                        finalURL = finalURL + '&filial=' + jQuery("#txtFilial").val();
                    }
                    finalURL = finalURL + '&dti=' + jQuery("#txt_dt_begin").val();
                    finalURL = finalURL + '&dtf=' + jQuery("#txt_dt_end").val();
                    return finalURL.replace(/\//g, "_");
                }

                function refreshTotal() {
                    $('#lblTotParc2').html('Número de Pedidos :<strong>' + ($('#qtdparc').val() !== undefined ? $('#qtdparc').val() : "0") + '</strong>').show('slow');
                    $('#lblTotParc3').html('Total em Pedidos :<strong>' + numberFormat($('#totparc').val(), 1) + '</strong>').show('slow');
                    $('#lblTotParc4').html('Ticket Médio :<strong>' + numberFormat(($('#totparc').val() / $('#qtdparc').val()), 1) + '</strong>').show('slow');
                    $("table :checkbox").click(function (event) {
                        if ($('input:checkbox:checked:not(".checkall")').length > 0) {
                            $("#btnAprov").attr("disabled", false);
                            $("#btnCancel").attr("disabled", false);
                            $("#btnNfs").attr("disabled", false);
                        } else {
                            $("#btnAprov").attr("disabled", true);
                            $("#btnCancel").attr("disabled", true);
                            $("#btnNfs").attr("disabled", true);
                        }
                    });
                }

                $("#btnPrint").click(function () {
                    var pRel = "&NomeArq=" + "Pedidos" +
                    "&siz=70|30|30|70|100|40|60|60|60|60|60|60" +                            
                    "&lbl=Data|Loja|Cod.|Prev.Fech.|Cliente|Cidade|UF|Vendedor|Tot.Prod.|Tot.Serv.|Total|Status" +
                    "&pdf=0" + // Colunas do server processing que não irão aparecer no pdf
                    "&filter=" + //Label que irá aparecer os parametros de filtro
                    "&PathArqInclude=" + "../Modulos/Financeiro/" + finalFind(1).replace("?", "&"); // server processing
                    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');                                                                  
                });  
                
                $("#btnAprov").click(function () {
                    bootbox.confirm('Confirma a aprovação das vendas selecionadas?', function (result) {
                        if (result === true) {
                            $("#loader").show();
                            $.post("../Estoque/form/VendasServer.php", "btnAprov=S&" + $("form").serialize()).done(function (data) {
                                if (data.trim() === "YES") {
                                    refresh();
                                } else {
                                    bootbox.alert("Erro ao realizar esta operação!");
                                }
                                $("#loader").hide();
                            });
                        }
                    });
                });

                $("#btnCancel").click(function () {
                    bootbox.confirm('Confirma a reprovação das vendas selecionadas?', function (result) {
                        if (result === true) {
                            $("#loader").show();
                            $.post("../Estoque/form/VendasServer.php", "btnCancel=S&" + $("form").serialize()).done(function (data) {
                                if (data.trim() === "YES") {
                                    refresh();
                                } else {
                                    bootbox.alert("Erro ao realizar esta operação!");
                                }
                                $("#loader").hide();
                            });
                        }
                    });
                });

                $("#btnfind").click(function () {
                    refresh();
                });

                function refresh() {
                    $("#checkAll").parent().removeClass('checked');
                    $("#checkAll").attr('checked', false);
                    var tblTurmas = $('#example').dataTable();
                    tblTurmas.fnReloadAjax(finalFind(0));
                }
            });
        </script>        
        <?php odbc_close($con); ?>
    </body>
</html>