<?php
include "../../Connections/configini.php";
include "form/LancamentoMovimentoFormServer.php";

$idFornec = "";

if (isset($_GET['idFornecedor'])) {
    $idFornec = $_GET['idFornecedor'];
}

if ($_GET['tp'] !== "") {
    $TipoGrupo = $_GET['tp'] === "1" ? "D" : "C";
    $tp = $_GET['tp'];
}
?>
<!DOCTYPE html>
<head>
    <link rel="icon" type="image/ico" href="../../../favicon.ico"/>
    <link href="../../../css/stylesheets.css" rel="stylesheet" type="text/css" />
    <link href="../../../css/stylesheet.css" rel="stylesheet" type="text/css" />
    <link href="../../../css/main.css" rel="stylesheet">
    <link href="../../../css/bootbox.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    <style type="text/css">
        .labcont {
            width: 100%;
            border: 1px solid transparent;
        }
        .labhead {
            padding: 0;
            border: 1px solid transparent;
        }
        .liscont {
            width: 100%;
            border: 1px solid #EEE;
        }
        .lishead {
            padding: 0;
            background: #EEE;
            border: 1px solid #FFF;
        }
        .lisbody {
            padding: 0 5px;
            line-height: 28px;
            background: #F6F6F6;
            border: 1px solid #FFF;
        }
        .lisfoot {
            padding: 0 5px;
            background: #EEE;
            text-align: right;
            line-height: 28px;
            font-weight: bold;
            border: 1px solid #FFF;
        }
        .lisbody:nth-child(3) { text-align: center; }
        .lisbody:nth-child(4), .lisbody:nth-child(5) { text-align: right; }
        .lisbody.selected { background: #acbad4 !important;}
    </style>
</head>
<body>
    <div class="row-fluid">
        <form id="frmLancamentoMovimento" name="frmLancamentoMovimento" action="FormLancamentoMovimento.php?tp=<?php echo $tp; ?>" method="POST">
            <div class="frmhead">
                <div class="frmtext">Contas Fixas</div>
                <div class="frmicon" style="top:13px" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <div class="tabbable frmtabs">
                <ul class="nav nav-tabs" style="margin:0">
                    <li class="active"><a href="#tab1" data-toggle="tab">Dados Principais</a></li>
                    <li class=""><a href="#tab2" data-toggle="tab">Produtos</a></li>
                    <li class=""><a href="#tab3" data-toggle="tab">Serviços</a></li>
                    <li class=""><a href="#tab4" data-toggle="tab">Documentos</a></li>
                </ul>
                <div class="tab-content frmcont" style="height:247px; font-size:11px !important; overflow: unset;">
                    <div class="tab-pane active" id="tab1">
                        <div style="width:20%; float:left">
                            <span>Loja:</span>
                            <select name="txtEmpresa" id="txtEmpresa" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                                <?php $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo utf8_encode($RFP['id_filial']); ?>" <?php
                                    if ($empresa == $RFP['id_filial']) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP['Descricao']) . " - " . utf8_encode($RFP['numero_filial']); ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div style="width:43%; float:left; margin-left:1%">
                            <span>Tipo:</span>
                            <select name="txtTipoGrupo" id="txtTipoGrupo" class="select input-medium" style="width:100%" readonly>
                                <option value="null">--Selecione--</option>
                                <option value="D" <?php
                                if ($TipoGrupo == "D") {
                                    echo "SELECTED";
                                }
                                ?>>DÉBITO</option>
                                <option value="C" <?php
                                if ($TipoGrupo == "C") {
                                    echo "SELECTED";
                                }
                                ?>>CRÉDITO</option>
                            </select>
                        </div>
                        <div style="width:15%; float:left; margin-left:1%">
                            <span>Dt. Cadastro:</span>
                            <input class="input-xlarge" disabled type="text" style="min-height: 28px;" value="<?php echo $data; ?>"/>
                        </div>                        
                        <div style="width: 16%;float: left;margin-left: 0%;padding-left: 1%;padding-top: 3%;">
                            <input name="txtInativoOld" id="txtInativoOld" value="<?php echo $inativo; ?>" type="hidden"/>                            
                            <input type="checkbox" name="inativo" <?php echo ($inativo == 1 ? "checked" : ""); ?> id="inativo" value="1" class="itens" <?php echo $disabled; ?>> Inativar
                        </div>
                        <div style="clear:both; height:5px"></div>
                        <div style="width:49.5%; float:left;">
                            <span>Grupo:</span>
                            <select name="txtGrupo" id="txtGrupo" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                                <option value="null">--Selecione--</option>
                                <?php if ($TipoGrupo == "D" || $TipoGrupo == "C") {
                                    $cur = odbc_exec($con, "select id_grupo_contas,descricao, (select descricao from sf_centro_custo where id = centro_custo) cc 
                                    from sf_grupo_contas where deb_cre = '" . $TipoGrupo . "' order by descricao") or die(odbc_errormsg());
                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP["id_grupo_contas"] ?>"<?php
                                    if (!(strcmp($RFP["id_grupo_contas"], $grupo))) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP["descricao"]) . (strlen(utf8_encode($RFP["cc"])) > 0 ? " [" . utf8_encode($RFP["cc"]) . "]" : ""); ?></option>
                                        <?php
                                    }
                                } ?>
                            </select>
                        </div>
                        <div style="width:49.5%; float:left;margin-left: 1%">
                            <span>Conta:</span>
                            <select name="txtConta" id="txtConta" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                                <option value="null">--Selecione--</option>
                                <?php
                                if ($conta != "") {
                                    $sql = "select id_contas_movimento,descricao from sf_contas_movimento where id_contas_movimento > 0 AND id_contas_movimento = " . $conta . " ORDER BY descricao";
                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP["id_contas_movimento"] ?>"<?php
                                    if (!(strcmp($RFP["id_contas_movimento"], $conta))) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                            <?php
                                    }
                                }?>
                            </select>
                        </div>
                        <div style="clear:both; height:5px"></div>
                        <div style="width:100%; float:left;">
                            <span name="lblFonecedor" id="lblFonecedor">Cliente/Fornecedor/Funcionário:</span>
                            <input type="hidden" id="txtFonecedor" name="txtFonecedor" value="<?php echo $fornecedor; ?>">                                     
                            <input type="text" id="txtFonecedorNome" style="width:100%" value="<?php echo $fornecedorNome; ?>" <?php echo ($idFornec !== "" ? "readonly" : ""); ?> <?php echo $disabled; ?>/>                                                          
                        </div>
                        <div style="clear:both; height:5px"></div>
                        <div style="width:63%; float:left">
                            <span>Tipo de Cobrança:</span>
                            <select name="txtTipo" id="txtTipo" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                                <option value="null">--Selecione--</option>
                                <?php $cur = odbc_exec($con, "select id_tipo_documento,descricao from sf_tipo_documento where (inativo = 0 or id_tipo_documento = '" . $tipo . "') order by descricao") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP["id_tipo_documento"] ?>"<?php
                                    if (!(strcmp($RFP["id_tipo_documento"], $tipo))) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div style="width:20%; float:left; margin-left:1%">
                            <span>Solicitado por:</span>
                            <input class="input-xlarge" disabled type="text" style="min-height: 28px;" value="<?php echo $solicitante; ?>"/>
                        </div>                             
                        <div style="width:15%; float:left; margin-left:1%">
                            <span>Status:</span>
                            <input class="input-xlarge" disabled type="text" style="min-height: 28px;" value="<?php echo $status; ?>"/>
                        </div>                             
                        <div style="clear:both; height:5px"></div>
                        <div style="width:63%; float:left;">
                            <span>Histórico:</span>
                            <input name="txtHistorico" id="txtHistorico" type="text" class="input-medium" style="width:100%" value="<?php echo $historico; ?>" <?php echo $disabled; ?>/>
                        </div>
                        <div style="width:36%; float:left; margin-left:1%">
                            <span>Destinatário:</span>
                            <input class="input-xlarge" disabled type="text" style="min-height: 28px;" value="<?php echo $destinatario; ?>"/>
                        </div>                                             
                        <div style="clear:both"></div>
                    </div>
                    <div class="tab-pane" id="tab2">
                        <div class="labcont">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="labhead" style="padding:0 5px">Produtos</td>
                                    <td width="60px" class="labhead" style="text-align:center">Quant.</td>
                                    <td width="90px" class="labhead" style="text-align:center">Valor Unitário</td>
                                    <td width="110px" class="labhead" style="text-align:center">Valor Total</td>
                                    <td width="50px" class="labhead" style="text-align:center">Ação</td>
                                </tr>
                            </table>
                        </div>
                        <div class="liscont">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="lishead">
                                        <select id="txtProduto" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                                            <option value="null">--Selecione--</option>
                                            <?php $sql = "select conta_produto,descricao,codigo_interno from sf_produtos where inativa = 0 and conta_produto > 0 AND tipo = 'P' ORDER BY descricao";
                                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP["conta_produto"] ?>">
                                                    <?php
                                                    echo utf8_encode($RFP["descricao"]);
                                                    if (strlen($RFP["codigo_interno"]) > 0) {
                                                        echo " [" . $RFP["codigo_interno"] . "]";
                                                    } ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                    <td width="60px" class="lishead">
                                        <input id="txtQtdP" type="text" onblur="prodValor()" class="input-medium" style="text-align:center" <?php echo $disabled; ?> />
                                    </td>
                                    <td width="90px" class="lishead">
                                        <input id="txtValUniP" type="text" onblur="prodValor()" class="input-medium" style="text-align:right" <?php echo $disabled; ?> />
                                    </td>
                                    <td width="110px" class="lishead">
                                        <input id="txtValToTP" type="text" disabled class="input-medium" style="text-align:right"/>
                                    </td>
                                    <td width="50px" class="lishead">
                                        <button class="btn green" type="button" onclick="novaLinha()" style="width:100%; height:27px; margin:0"><span class="ico-plus icon-white"></span></button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="liscont" style="height:161px; overflow-y:scroll">
                            <table id="tbProdutos" cellpadding="0" cellspacing="0" width="100%"></table>
                        </div>
                        <div class="liscont" style="border-top:0">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="lisfoot"><div style="float: left;"><button class="btn green" type="button" name="btnDevolucao" id="btnDevolucao" value="devolucao"><span class="ico-plus-sign"></span> Devolução </button></div><div style="float: right;">Total em Produtos:</div></td>
                                    <td width="100px" class="lisfoot"><span id="totalP"><?php echo escreverNumero(0,1); ?></span></td>
                                    <td width="40px" class="lisfoot">&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab3">
                        <div class="labcont">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="labhead" style="padding:0 5px">Serviços</td>
                                    <td width="60px" class="labhead" style="text-align:center">Quant.</td>
                                    <td width="90px" class="labhead" style="text-align:center">Valor Unitário</td>
                                    <td width="110px" class="labhead" style="text-align:center">Valor Total</td>
                                    <td width="50px" class="labhead" style="text-align:center">Ação</td>
                                </tr>
                            </table>
                        </div>
                        <div class="liscont">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="lishead">
                                        <select id="txtServico" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                                            <option value="null">--Selecione--</option>
                                            <?php $sql = "select conta_produto,descricao from sf_produtos where inativa = 0 and conta_produto > 0 AND tipo = 'S' ORDER BY descricao";
                                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP["conta_produto"] ?>"><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                    <td width="60px" class="lishead">
                                        <input id="txtQtdS" type="text" onblur="prodValorS()" class="input-medium" style="text-align:center" <?php echo $disabled; ?>/>
                                    </td>
                                    <td width="90px" class="lishead">
                                        <input id="txtValUniS" type="text" onblur="prodValorS()" class="input-medium" style="text-align:right" <?php echo $disabled; ?>/>
                                    </td>
                                    <td width="110px" class="lishead">
                                        <input id="txtValToTS" type="text" disabled class="input-medium" style="text-align:right"/>
                                    </td>
                                    <td width="50px" class="lishead">
                                        <button class="btn green" type="button" onclick="novaLinhaS()" style="width:100%; height:27px; margin:0"><span class="ico-plus icon-white"></span></button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="liscont" style="height:161px; overflow-y:scroll">
                            <table id="tbServicos" cellpadding="0" cellspacing="0" width="100%">
                                <?php
                                $j = 0;
                                $totals = 0;
                                if (is_numeric($id)) {
                                    $cur = odbc_exec($con, "select id_lancamento_movimento, id_item_lancamento_movimento, conta_produto produto,pr.descricao produtodesc,quantidade,valor_total,unidade_comercial,codigo_interno
                                    from sf_lancamento_movimento_itens lm inner join sf_produtos pr on pr.conta_produto = lm.produto
                                    where pr.tipo = 'S' and id_lancamento_movimento = " . $id);
                                    while ($RFP = odbc_fetch_array($cur)) {
                                        ?>
                                        <tr id="tabela_linhaS_<?php echo $j; ?>">
                                            <td style="display: none;" class="item_count"><?php echo $j; ?></td>
                                            <td class="lisbody">
                                                <input name="txtIPS[]" type="hidden" value="<?php echo $RFP["id_item_lancamento_movimento"]; ?>"/>
                                                <input name="txtPDS[]" type="hidden" value="<?php echo $RFP["produto"]; ?>"/>
                                                <input name="txtVQS[]" type="hidden" value="<?php echo escreverNumero($RFP["quantidade"], 0, 4); ?>"/>
                                                <input name="txtVUS[]" type="hidden" value="<?php
                                                if ($RFP["quantidade"] > 0) {
                                                    $valor_unitario = $RFP["valor_total"] / $RFP["quantidade"];
                                                } else {
                                                    $valor_unitario = 0;
                                                } echo escreverNumero($valor_unitario);
                                                ?>"/>
                                                <input name="txtVPS[]" type="hidden" value="<?php
                                                $totals = $totals + $RFP["valor_total"];
                                                echo escreverNumero($RFP["valor_total"]);
                                                ?>"/><?php echo utf8_encode($RFP["produtodesc"]); ?>
                                            </td>
                                            <td width="50px" class="lisbody">
                                        <center><?php echo escreverNumero($RFP["quantidade"], 0 ,4); ?></center>
                                        </td>
                                        <td width="80px" class="lisbody" style="text-align: right;">
                                            <?php echo escreverNumero($valor_unitario); ?>
                                        </td>
                                        <td width="100px" class="lisbody vtotal" style="text-align: right;">
                                            <?php echo escreverNumero($RFP["valor_total"]); ?>
                                        </td>
                                        <td width="23px" class="lisbody"><center><input <?php echo $disabled; ?> class="btn red" type="button" value="x" onClick="removeLinha('tabela_linhaS_<?php echo $j; ?>')" style="margin:0px; padding:2px 4px 3px 4px; line-height:10px;"></input></center></td>
                                        </tr>
                                        <?php
                                        $j++;
                                    }
                                }
                                ?>
                            </table>
                        </div>
                        <div class="liscont" style="border-top:0">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="lisfoot">Total em Serviços:</td>
                                    <td width="100px" class="lisfoot"><span id="totalS"><?php echo escreverNumero(0,1); ?></span></td>
                                    <td width="40px" class="lisfoot">&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab4">
                        <div style="float: left; width: 80%; margin-right: 8%;">
                            <label id="lblarquivo" for="arquivo">Arquivo:</label>
                            <input class="btn btn-primary" style="height:20px; line-height:21px;width: 100%;" type="file" name="file" id="file" <?php echo $disabled; ?>>
                        </div>                            
                        <div style="float:left; width:11%; margin-left:1%;">                                
                            <button type="button" id="bntAdicionaCarteira" name="bntAdicionaCarteira" onclick="AdicionarDoc(0)" class="btn btn-success" style="height:27px; padding-top:6px; margin-top: 15px;" <?php echo $disabled; ?>><span class="ico-plus"></span></button>
                        </div>                            
                        <table id="tbDocumentos" class="table" cellpadding="0" cellspacing="0" width="100%" <?php echo $disabled; ?>>
                            <thead>
                                <tr>
                                    <th width="30%">Data de Upload</th>
                                    <th width="50%">Nome do Arquivo</th>
                                    <th width="10%"><center>Tamanho</center></th>
                                    <th width="10%"><center>Ação</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table> 
                    </div>
                </div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($status == 'Aguarda' && $_SESSION['login_usuario'] == $destinatario) { ?>
                        <div style="float:left">
                            <button class="btn btn-success" type="submit" name="bntAprov" id="bntOK" ><span class="ico-checkmark"></span> Aprovar</button>
                            <button class="btn red" type="submit" name="bntReprov" id="bntOK"><span class=" ico-remove"></span> Reprovar</button>
                        </div>
                    <?php } if ($disabled == "") { ?>
                        <button class="btn green" type="button" onClick="validaForm();"><span class="ico-checkmark"></span> Gravar</button>
                        <button type="submit" name="bntSave" id="bntSave" style="display:none"><span class="ico-checkmark"></span> </button>
                        <?php if ($_POST["txtId"] == "") { ?>
                            <button class="btn yellow" onClick="parent.FecharBox()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                            <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                            <?php
                        }
                    } else { ?>
                        <button class="btn green" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                        <button class="btn red" type="button" name="bntDelete" id="bntDelete" value="Excluir"><span class=" ico-remove"></span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins.js"></script>
    <script type="text/javascript" src="../../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type="text/javascript" src="../../../js/plugins/datatables/fnReloadAjax.js"></script>
    <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../../js/util.js"></script>                                        
    <script type="text/javascript" src="js/LancamentoMovimentoForm.js"></script> 
    <script type="text/javascript">  
        
        $('#tbDocumentos').dataTable({
            "iDisplayLength": 12,
            "ordering": false,
            "bFilter": false,
            "bInfo": false,
            "sAjaxSource": "../../Modulos/CRM/ajax/Documentos_server_processing.php?id=" + $("#txtId").val() + "&type=ContasFixas",
            "bLengthChange": false,
            "bPaginate": true,
            'oLanguage': {
                'oPaginate': {
                    'sFirst': "Primeiro",
                    'sLast': "Último",
                    'sNext': "Próximo",
                    'sPrevious': "Anterior"
                }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
            'sLengthMenu': "Visualização de _MENU_ registros",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"}
        });

        function refreshDoc() {
            let tblCRM = $('#tbDocumentos').dataTable();
            tblCRM.fnReloadAjax("../../Modulos/CRM/ajax/Documentos_server_processing.php?id=" + $("#txtId").val() + "&type=ContasFixas");
        }

        function AdicionarDoc(e) {
            if ($("#txtId").val().length > 0) {
                parent.$("#loader").show();
                var formData = new FormData();
                formData.append("btnSendFile", "S");
                formData.append("txtId", $("#txtId").val());
                formData.append("type", "ContasFixas");
                formData.append("file", $("#file")[0].files[0]); 
                $.ajax({
                    type: 'POST',
                    url: "../../Modulos/CRM/form/CRMDocumentos.php",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false
                }).done(function (data) {
                    parent.$("#loader").hide();
                    if (data === "YES") {
                        $('#file').val('');
                        refreshDoc();
                    } else {
                        bootbox.alert(data);
                    }
                });
            } else {
                bootbox.alert("É necessário salvar antes de realizar esta operação!");
            }
        }

        function RemoverDoc(filename) {
            var nomedoc = encodeURIComponent(filename.toString());
            bootbox.confirm('Confirma a exclusão do registro?', function (result) {
                if (result === true) {
                    $.post("../../Modulos/CRM/form/CRMDocumentos.php", "btnDelFile=S&txtId=" + $("#txtId").val() + 
                    "&type=ContasFixas&txtFileName=" + nomedoc).done(function (data) {
                        if (data === "YES") {
                            refreshDoc();
                        } else {
                            bootbox.alert(data);
                        }
                    });
                } else {
                    return;
                }
            });
        }
    </script> 
</body>
<?php odbc_close($con); ?>
</html>