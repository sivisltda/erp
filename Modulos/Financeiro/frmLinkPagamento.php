<?php

include __DIR__ . "./../../util/util.php"; 

$link_forma_pagamento = "";
$link_max_parcelas = "";
$link_pri_cor = "";
$link_max_cor = "";
$emp_razao_social = "";
$emp_cnpj = "";
$msg_retorno = "Link Inexistente";

$resp = explode("|", encrypt($_GET["link"], "VipService123", false));
if (count($resp) == 4 && is_numeric($resp[1]) && is_numeric($resp[2])) {
    $contrato = $resp[2];
    $hostname = "148.72.177.101,6000";
    $database = "erp" . $contrato;
    $username = "erp" . $contrato;
    $password = "!Password123";    
    $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $hostname . "; DATABASE=" . $database . ";", $username, $password)or die(odbc_errormsg());
    require_once(__DIR__ . './../../Connections/funcoesAux.php');
    $id_link = $resp[1];     
} else {
    echo "Link com erro!"; exit;
}

$cur = odbc_exec($con, "select link_forma_pagamento, link_max_parcelas, cor_primaria, cor_secundaria from sf_configuracao where id = 1");
while ($RFP = odbc_fetch_array($cur)) {
    $link_forma_pagamento = $RFP['link_forma_pagamento'];
    $link_pri_cor = $RFP['cor_primaria'];    
    $link_max_cor = $RFP['cor_secundaria'];
}

$cur = odbc_exec($con, "select data_parcela, max_parcela, isnull(valor, (select sum(valor_bruto) total 
from sf_link_online_itens bi where bi.id_link = sf_link_online.id_link)) bol_valor,
id_venda, datediff(day, data_parcela, getdate()) dias, inativo 
from sf_link_online where id_link = " . valoresNumericos2($id_link));
while ($RFP = odbc_fetch_array($cur)) {
    $link_max_parcelas = $RFP['max_parcela'];
    if ($resp[0] == escreverDataHora($RFP["data_parcela"]) && $resp[3] == escreverNumero($RFP["bol_valor"])) {
        if ($RFP["inativo"] == 1) {
            $msg_retorno = "Link inativo!";
        } else if ($RFP["dias"] > 0) {
            $msg_retorno = "Link vencido!";
        } else if (is_numeric($RFP["id_venda"])) {
            $msg_retorno = "Link com venda! (PAGO)";
        } else {
            $msg_retorno = "";
        }
    } else {
        $msg_retorno = "Link inválido!";
    }
}

$logoMnuLat = "../../img/logo.png";
if (file_exists("../../Pessoas/" . $contrato . "/Empresa/Menu/logo.png")) {
    $logoMnuLat = "../../Pessoas/" . $contrato . "/Empresa/Menu/logo.png";
}

$cur = odbc_exec($con, "select top 1 id_filial, razao_social_contrato, cnpj from sf_filiais order by 1");
while ($RFP = odbc_fetch_array($cur)) {
    $emp_razao_social = utf8_decode($RFP['razao_social_contrato']);
    $emp_cnpj = utf8_decode($RFP['cnpj']);
}

?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">        
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/bootstrap/bootstrap3.3.7.min.css" rel="stylesheet" type="text/css"/>            
        <link href="../../css/select/select.css" rel="stylesheet" type="text/css"/>
        <link href="../../js/creditCard/card.css" rel="stylesheet" type="text/css"/>         
        <link href="../../js/plugins/sweetAlert/sweetalert2.min.css" rel="stylesheet" type="text/css"/>
        <style type="text/css">
            
            html {
                position: relative;
                min-height: 100%;
            }
            
            body {
                margin-bottom: 60px; /* Margin bottom by footer height */
            }
            
            .footer {
                position: absolute;
                bottom: 0;
                width: 100%;
                height: 60px; /* Set the fixed height of the footer here */
                background-color: #f5f5f5;
            }
            
            body > .container {
                padding: 60px 15px 0;
            }
            
            .container .text-muted {
                margin: 20px 0;
            }
            
            .footer > .container {
                padding-right: 15px;
                padding-left: 15px;
            }
            
            code {
                font-size: 80%;
            }
            
            .border-red{
                border-bottom: 1px solid <?php echo $link_pri_cor;?>;
            }
            
            .logo{
                width: 130px;
                height: 50px;
                margin-top: 20px;
                margin-bottom: 20px;
            }
            
            input {
                display: block;
                width: 100%;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
                background-color: #fff;
                background-image: none;
                border: 1px solid #ccc;
                border-radius: 4px;
                -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
                box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
                -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
                -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
                transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            }
            
            .select2-container .select2-choice {
                height: 34px;
                line-height: 34px;
            }
            
            .select2-container .select2-choice div b {
                background: url('../../img/select/select2.png') no-repeat 0 4px;
            }
            
            .mt-0 {
                margin-top: 0px;
            }
            
        </style>
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <img class="logo" src="<?php echo $logoMnuLat; ?>" alt=""/>   
            </div>
        </nav>
        <div class="container">
            <div class="page-header border-red">
                <h3 class="m-0">Link de Pagamento</h3>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3 class="mt-0"><?php echo $emp_razao_social; ?></h3>
                            <h6>CNPJ <?php echo $emp_cnpj; ?></h6>                              
                            <hr>
                            <?php $cur = odbc_exec($con, "select razao_social, l.inativo, valor, data_parcela from sf_link_online l
                            inner join sf_fornecedores_despesas on id_fornecedores_despesas = id_pessoa
                            where id_link = " . $id_link);
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <h4><?php echo utf8_decode($RFP['razao_social']); ?></h4>
                                <h1 class="mt-0"><b style="color: <?php echo $link_max_cor; ?>;"><?php echo escreverNumero($RFP['valor'], 1); ?></b></h1>
                                <h6>link válido até às 23:59 do dia <?php echo escreverData($RFP['data_parcela']); ?></h6>
                                <hr>
                            <?php } ?>
                            <h4>Itens do Pagamento</h4>
                            <ul>
                                <?php $cur = odbc_exec($con, "select dt_inicio_mens, descricao, placa, quantidade, tipo, valor_total, valor_bruto, 
                                valor_multa from sf_link_online_itens bi inner join sf_produtos on conta_produto = id_produto
                                left join sf_vendas_planos_mensalidade m on m.id_mens = bi.id_mens
                                left join sf_vendas_planos vp on vp.id_plano = m.id_plano_mens
                                left join sf_fornecedores_despesas_veiculo v on v.id = vp.id_veiculo
                                where id_link = " . $id_link);
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <li>
                                        <?php echo ($RFP["tipo"] == "C" ? "MENS." : ($RFP["tipo"] == "S" ? "SER." : "PROD.")) . " " . ($RFP["tipo"] == "C" ? escreverData($RFP["dt_inicio_mens"]) . " - " . utf8_encode($RFP["descricao"]) . " [" . utf8_encode($RFP["placa"]) . "]" : utf8_encode($RFP["descricao"])) . "<br>" .
                                        "  -> " . escreverNumero($RFP["quantidade"], 0, 0) . " x " . "<b style=\"color: " . $link_max_cor . ";\">" . escreverNumero($RFP["valor_bruto"], 1) . "</b>";
                                        ?>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-5 mb-3">
                    <?php if (strlen($msg_retorno) == 0) { ?>
                    <form class="form-cartao" novalidate="">
                        <div style="height: 200px;" class="card-wrapper"></div>
                        <div class="row">
                            <div class="col-xs-8 col-sm-8">
                                <div class="form-group item-adicional">
                                    <label for="txtDCCNumCartao">Número do Cartão:</label>
                                    <input type="text" name="txtDCCNumCartao" id="txtDCCNumCartao" placeholder="•••• •••• •••• ••••" style="padding-left: 60px;" size="20" value="">
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-4">
                                <div class="form-group">
                                    <label for="txtDCCBandeira">Bandeira:</label>
                                    <select class="selectCr" id="txtDCCBandeira" name="txtDCCBandeira" style="width: 100%;">                                        
                                        <?php for ($j = 0; $j < 12; $j++) { ?>            
                                            <option value="<?php echo $j; ?>"></option>
                                        <?php } ?>                                        
                                    </select>
                                </div>                                
                            </div>                            
                        </div>     
                        <div class="row">                            
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label for="txtDCCNmCartao">Nome do Responsável:</label>
                                    <input type="text" name="txtDCCNmCartao" id="txtDCCNmCartao" placeholder="NOME CARTÃO" value="">
                                </div>                                    
                            </div>                                
                        </div>                        
                        <div class="row">                            
                            <div class="col-xs-4 col-md-4">
                                <div class="form-group">
                                    <label for="txtDCCValidade">Vencimento:</label>
                                    <input type="text" name="txtDCCValidade" id="txtDCCValidade" placeholder="••/••••" data-mask-format="00/0000" value="">
                                </div>                                    
                            </div>
                            <div class="col-xs-4 col-md-4">
                                <div class="form-group">
                                    <label for="txtDCCCodVer">CVV:</label>
                                    <input type="text" name="txtDCCCodVer" id="txtDCCCodVer" placeholder="•••" maxlenght="4" minlenght="3" value="">
                                </div>                                    
                            </div>
                            <div class="col-xs-4 col-md-4">
                                <div class="form-group">
                                    <label for="txtDCCParcelas">Parcelas:</label>
                                    <select id="txtDCCParcelas" name="txtDCCParcelas" style="width: 100%; height: 34px; border: 1px solid #ccc;">                                        
                                        <?php for ($j = 1; $j <= $link_max_parcelas; $j++) { ?>            
                                            <option value="<?php echo $j; ?>"><?php echo $j . " x "; ?></option>
                                        <?php } ?>                                        
                                    </select>
                                </div>                                                                                                        
                            </div>                            
                        </div>                            
                        <div class="row">
                            <div class="col-sm-12 mt-2">
                                <button id="btnIncluirDCC" name="btnIncluirDCC" style="background-color: <?php echo $link_max_cor; ?>; color: white;" class="btn btn-destaque text-uppercase btn-block btn-pagar">Pagar agora</button>
                            </div>
                        </div>
                    </form>
                    <?php } else { ?>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 text-center">
                            <h1><?php echo $msg_retorno; ?></h1>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <footer class="footer">
            <div class="container">
                <p class="text-muted">Desenvolvido por <a target="_blank" href="http://www.sivis.com.br/"><b> SIVIS Tecnologia</b></a></p>
            </div>
        </footer>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>        
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap3.3.7.min.js"></script>
        <script type="text/javascript" src="../../js/creditCard/card.js"></script>
        <script type="text/javascript" src="../../js/jquery.creditCardValidator.js"></script>
        <script type="text/javascript" src="../../js/moment.min.js"></script>        
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script> 
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/plugins/sweetAlert/sweetalert2.all.js"></script>
        <?php if (strlen($msg_retorno) == 0) { ?>
        <script type="text/javascript">

            const payment_form = new DatPayment({
                form_selector: '.form-cartao',
                card_container_selector: '.card-wrapper',
                number_selector: '#txtDCCNumCartao',
                date_selector: '#txtDCCValidade',
                cvc_selector: '#txtDCCCodVer',
                name_selector: '#txtDCCNmCartao',
                submit_button_selector: '#btnIncluirDCC',
                placeholders: {
                    number: '•••• •••• •••• ••••',
                    expiry: '••/••••',
                    cvc: '•••',
                    name: 'NOME CARTÃO'
                },
                validators: {
                }
            });
            
            $("#txtDCCNumCartao").mask("9999 9999 9999 9999");
            $("#txtDCCValidade").mask("99/9999");
            $("#txtDCCCodVer").mask("999");

            payment_form.form.addEventListener('payment_form:submit', function (e) {
                payment_form.unlockForm();
                if ($("#txtDCCNmCartao").val() === "" || $("#txtDCCNumCartao").val() === "" || $("#txtDCCCodVer").val() === "" || $("#txtDCCValidade").val() === "") {
                    swal('Atenção!', 'Todos os campos são obrigatórios!', 'warning');
                } else if (!moment($("#txtDCCValidade").val().replace(/\s/g,''), "MM/YYYY", true).isValid()) {
                    swal('Atenção!', 'Formato de data inválido!', 'warning');
                } else if (!$('#btnIncluirDCC').attr('disabled')) {
                    $('#btnIncluirDCC').attr('disabled', true);
                    $("#loader", window.parent.document).show();                    
                    $.post("./form/FormLinkPagamento.php", "btnSalvar=S" + 
                            "&txtCartaoNumero=" + $("#txtDCCNumCartao").val() +
                            "&txtCartaoBandeira=" + $("#txtDCCBandeira").val() +
                            "&txtCartaoNome=" + $("#txtDCCNmCartao").val() +
                            "&txtCartaoValidade=" + $("#txtDCCValidade").val() +
                            "&txtCartaoCodSeg=" + $("#txtDCCCodVer").val() +
                            "&txtParcelas=" + $("#txtDCCParcelas").val() + 
                            "&txtLink=<?php echo $_GET["link"]; ?>").done(function (data) {
                        $("#loader", window.parent.document).hide(); 
                        if (isNaN(data.trim()) && $.isNumeric(data.trim().substring(2)) && data.trim()[0] === "Y") {
                            swal('Sucesso', 'Pagamento efetuado com sucesso!', 'success').then(function() {
                                location.reload();
                            });                                                        
                        } else {
                            swal('Atenção!', 'Atenção ao Pagamento!<br/>' + data.trim(), 'warning');
                            $('#btnIncluirDCC').attr('disabled', false);
                        }
                    });       
                }
            });

            $('#txtDCCNumCartao').change(function () {
                if ($("#txtDCCNumCartao").val() !== "") {
                    $('#txtDCCNumCartao').validateCreditCard(function (result) {
                        $("#txtDCCNumCartao").removeClass();
                        if (result.card_type !== null) {
                            $("#txtDCCBandeira").val(result.card_type.code).trigger("change");
                        }
                        $('#btnIncluirDCC').attr('disabled', false);
                    });
                }
            });

            $(".selectCr").select2({
                placeholder: "",
                formatResult: function (item) {
                    return '<img src="../../img/cartao/' + item.id + '.png" style="height: 30px;"/>';
                },
                formatSelection: function (item) {
                    return '<img src="../../img/cartao/' + item.id + '.png" style="height: 30px; margin-top: -4px;"/>';
                }
            });
            
            $(".select").select2();

        </script>
        <?php } ?>
    </body>
</html>
