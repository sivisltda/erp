<?php

if (isset($_POST['isAjax']) || isset($_GET['isAjax'])) {
    include "../../../Connections/configini.php";
} else {
    include '../../util/util.php';
}

$disabled = 'disabled';
$id = '';
$empresa = '';
$grupo = '';
$conta = '';
$fornecedor = '';
$fornecedorNome = '';
$tipo = '';
$inativo = '';
$historico = '';
$TipoGrupo = '';
$Cliente = '';
$data = getData("T");
$solicitante = $_SESSION['login_usuario'];
$status = 'Novo';
$destinatario = '';

$toReturn = "";
if ($_GET['tp'] != "") {
    $toReturn = "?tp=" . $_GET['tp'];
}
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_lancamento_movimento set " .
        "empresa = " . valoresSelect("txtEmpresa") . "," .
        "grupo_conta = " . valoresSelect("txtGrupo") . "," .
        "inativo = " . valoresCheck("inativo") . "," .
        "conta_movimento = " . valoresSelect("txtConta") . "," .
        "fornecedor_despesa = " . valoresSelect("txtFonecedor") . "," .
        "tipo_documento = " . valoresSelect("txtTipo") . "," .
        "historico = " . valoresTexto("txtHistorico") . "," .
        "data_aprov = null where id_lancamento_movimento = " . valoresNumericos("txtId")) or die(odbc_errormsg());
        $id = $_POST['txtId'];
    } else {
        $query = "insert into sf_lancamento_movimento(empresa,grupo_conta,conta_movimento,inativo,fornecedor_despesa,tipo_documento,historico,status,data_lanc,sys_login,destinatario)values(" .
        valoresSelect("txtEmpresa") . "," .
        valoresSelect("txtGrupo") . "," .
        valoresSelect("txtConta") . "," .
        valoresCheck("inativo") . "," .
        valoresSelect("txtFonecedor") . "," .
        valoresSelect("txtTipo") . "," .
        valoresTexto("txtHistorico") . "," .                
        "'Novo', getdate()," . valoresTexto2($_SESSION['login_usuario']) . ",'')";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_lancamento_movimento from sf_lancamento_movimento order by id_lancamento_movimento desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
        $id = $_POST['txtId'];
        $toReturn2 = "";
        if ($_GET['tp'] != "") {
            $toReturn2 = "&tp=" . $_GET['tp'];
        }
    }    
    if (is_numeric($id) && valoresCheck("inativo") != $_POST['txtInativoOld']) {
        $query = "update sf_lancamento_movimento_parcelas set inativo = " . valoresCheck("inativo") . " where data_pagamento is null and id_lancamento_movimento = " . $id;
        odbc_exec($con, $query) or die(odbc_errormsg());
    }     
    $prod_grp = $_POST['txtPD'];
    $prod_grpS = $_POST['txtPDS'];
    if (is_numeric($id)) {
        $notIn = '0';
        for ($i = 0; $i < sizeof($prod_grp); $i++) {
            if (is_numeric($prod_grp[$i])) {
                if (is_numeric($_POST['txtIP'][$i])) {
                    $notIn .= "," . $_POST['txtIP'][$i];
                }
            }
        }
        for ($i = 0; $i < sizeof($prod_grpS); $i++) {
            if (is_numeric($prod_grpS[$i])) {
                if (is_numeric($_POST['txtIPS'][$i])) {
                    $notIn .= "," . $_POST['txtIPS'][$i];
                }
            }
        }
        odbc_exec($con, "delete from sf_lancamento_movimento_itens where id_lancamento_movimento = " . $id . " and id_item_lancamento_movimento not in (" . $notIn . ")");
        for ($i = 0; $i < sizeof($prod_grp); $i++) {
            if (is_numeric($prod_grp[$i])) {
                if (is_numeric($_POST['txtIP'][$i])) {
                    odbc_exec($con, "update sf_lancamento_movimento_itens set produto = " . $prod_grp[$i] . ",quantidade = " . valoresNumericos2($_POST['txtVQ'][$i]) . " ,valor_total = " . valoresNumericos2($_POST['txtVP'][$i]) . " where id_item_lancamento_movimento = " . $_POST['txtIP'][$i]) or die(odbc_errormsg());
                } else {
                    odbc_exec($con, "insert into sf_lancamento_movimento_itens(id_lancamento_movimento,produto,quantidade,valor_total,id_usuario_inclusao,dt_inclusao) values(" . $id . "," . $prod_grp[$i] . "," . valoresNumericos2($_POST['txtVQ'][$i]) . "," . valoresNumericos2($_POST['txtVP'][$i]) . "," . $_SESSION["id_usuario"] . ",getdate())");
                }
            }
        }
        for ($i = 0; $i < sizeof($prod_grpS); $i++) {
            if (is_numeric($prod_grpS[$i])) {
                if (is_numeric($_POST['txtIPS'][$i])) {
                    odbc_exec($con, "update sf_lancamento_movimento_itens set produto = " . $prod_grpS[$i] . ",quantidade = " . valoresNumericos2($_POST['txtVQS'][$i]) . " ,valor_total = " . valoresNumericos2($_POST['txtVPS'][$i]) . " where id_item_lancamento_movimento = " . $_POST['txtIPS'][$i]) or die(odbc_errormsg());
                } else {
                    odbc_exec($con, "insert into sf_lancamento_movimento_itens(id_lancamento_movimento,produto,quantidade,valor_total,id_usuario_inclusao,dt_inclusao) values(" . $id . "," . $prod_grpS[$i] . "," . valoresNumericos2($_POST['txtVQS'][$i]) . "," . valoresNumericos2($_POST['txtVPS'][$i]) . "," . $_SESSION["id_usuario"] . ",getdate())");
                }
            }
        }
    }
}

if (isset($_POST['bntDelete']) && is_numeric($_POST['bntDelete'])) {
    odbc_exec($con, "DELETE FROM sf_lancamento_movimento WHERE id_lancamento_movimento = " . $_POST['bntDelete']);
    echo "YES";
    exit();
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select sf_lancamento_movimento.*,sf_contas_movimento.*,razao_social,nome_fantasia 
    from sf_lancamento_movimento left join sf_contas_movimento on id_contas_movimento = conta_movimento 
    left join sf_fornecedores_despesas on id_fornecedores_despesas = fornecedor_despesa
    where id_lancamento_movimento = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_lancamento_movimento'];
        $empresa = $RFP['empresa'];
        $grupo = $RFP['grupo_conta'];
        $conta = $RFP['conta_movimento'];
        $fornecedor = $RFP['fornecedor_despesa'];
        $fornecedorNome = utf8_encode($RFP['razao_social']);
        $tipo = $RFP['tipo_documento'];
        $inativo = $RFP['inativo'];
        $historico = utf8_encode($RFP['historico']);
        $TipoGrupo = utf8_encode($RFP['tipo']);
        $Cliente = $RFP['cliente_codigo'];        
        $data = escreverData($RFP['data_lanc']);
        $solicitante = utf8_encode($RFP['sys_login']);
        $status = utf8_encode($RFP['status']);
        $destinatario = utf8_encode($RFP['destinatario']);        
    }
} else {
    $disabled = '';
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}

if (isset($_POST['btnInativar'])) {
    $query = "update sf_lancamento_movimento set inativo = 1 where id_lancamento_movimento in (" . $_POST['btnInativar'] . ")";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}

if (isset($_POST['btnAprovar'])) {
    $query = "update sf_lancamento_movimento set status = 'Aprovado', data_aprov = getdate() where status = 'Aguarda' and destinatario = '" . $_SESSION["login_usuario"] . "' and id_lancamento_movimento in (" . $_POST['btnAprovar'] . ")";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}

if (isset($_POST['btnReprovar'])) {
    $query = "update sf_lancamento_movimento set status = 'Reprovado', data_aprov = getdate() where status = 'Aguarda' and destinatario = '" . $_SESSION["login_usuario"] . "' and id_lancamento_movimento in (" . $_POST['btnReprovar'] . ")";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}

if (isset($_POST['btnDevolucao'])) {
    odbc_exec($con, "set dateformat dmy;") or die(odbc_errormsg());
    $query = "update sf_lancamento_movimento_itens set dt_devolucao = " . valoresData('dtDevolucao') . ", id_usuario_devolucao = " . $_SESSION["id_usuario"] . " where id_item_lancamento_movimento in (" . $_POST['itens'] . ")";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}

if (isset($_GET['listaItens'])) {
    $cur = odbc_exec($con, "select id_item_lancamento_movimento, conta_produto produto,pr.descricao produtodesc, quantidade,valor_total,unidade_comercial,codigo_interno, dt_devolucao
    from sf_lancamento_movimento_itens lm inner join sf_produtos pr on pr.conta_produto = lm.produto
    where pr.tipo = '" . $_GET['tpItem'] . "' and id_lancamento_movimento = " . $_GET['listaItens']);
    while ($RFP = odbc_fetch_array($cur)) {
        $listItens[] = array('id_item_lancamento_movimento' => $RFP['id_item_lancamento_movimento'],
            'produto' => $RFP['produto'],
            'produtodesc' => utf8_encode($RFP['produtodesc']),
            'quantidade' => escreverNumero($RFP['quantidade'], 0, 4),
            'valor_total' => escreverNumero($RFP['valor_total'], 2),
            'unidade_comercial' => $RFP['unidade_comercial'],
            'codigo_interno' => $RFP['codigo_interno'],
            'dt_devolucao' => escreverData($RFP['dt_devolucao']));
    }
    echo json_encode($listItens);
}

if (isset($_POST['bntAprov'])) {
    odbc_exec($con, "update sf_lancamento_movimento set status = 'Aprovado', data_aprov = getdate() where id_lancamento_movimento = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
    $status = 'Aprovado';
} 

if (isset($_POST['bntReprov'])) {
    odbc_exec($con, "update sf_lancamento_movimento set status = 'Reprovado', data_aprov = getdate() where id_lancamento_movimento = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
    $status = 'Reprovado';
}