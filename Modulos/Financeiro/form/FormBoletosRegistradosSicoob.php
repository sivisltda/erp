<?php

namespace CnabPHP\samples;

require_once (__DIR__ . "./../../../util/boleto/autoloader.php");

use \CnabPHP\Remessa;

function limit($palavra, $limite) {
    if (strlen($palavra) >= $limite) {
        $var = substr($palavra, 0, $limite);
    } else {
        $max = (int) ($limite - strlen($palavra));
        $var = $palavra . complementoRegistro($max, "brancos");
    }
    return $var;
}

function complementoRegistro($int, $tipo) {
    if ($tipo == "zeros") {
        $space = '';
        for ($i = 1; $i <= $int; $i++) {
            $space .= '0';
        }
    } else if ($tipo == "brancos") {
        $space = '';
        for ($i = 1; $i <= $int; $i++) {
            $space .= ' ';
        }
    }
    return $space;
}

function trataTxt($text) {
    $utf8 = array(
        '/[áàâãªä]/u' => 'a',
        '/[ÁÀÂÃÄ]/u' => 'A',
        '/[ÍÌÎÏ]/u' => 'I',
        '/[íìîï]/u' => 'i',
        '/[éèêë]/u' => 'e',
        '/[ÉÈÊË]/u' => 'E',
        '/[óòôõºö]/u' => 'o',
        '/[ÓÒÔÕÖ]/u' => 'O',
        '/[úùûü]/u' => 'u',
        '/[ÚÙÛÜ]/u' => 'U',
        '/ç/' => 'c',
        '/Ç/' => 'C',
        '/ñ/' => 'n',
        '/Ñ/' => 'N',
        '/–/' => '-', // UTF-8 hyphen to "normal" hyphen
        '/[’‘‹›‚]/u' => ' ', // Literally a single quote
        '/[“”«»„]/u' => ' ', // Double quote
        '/ /' => ' ', // nonbreaking space (equiv. to 0x160)
    );
    return preg_replace(array_keys($utf8), array_values($utf8), $text);
}

if (isset($_POST['bntSaveFile'])) {
    if (is_numeric($id_banco)) {
        $cur = odbc_exec($con, "SELECT * FROM sf_bancos WHERE id_bancos = " . $id_banco);
        while ($RFP = odbc_fetch_array($cur)) {
            $agencia = explode("-", $RFP['agencia']);
            $contaX = explode("-", $RFP['conta']);
            $conta = str_replace(" ", "", $RFP['cedente']);
            $digito_conta = $RFP['dv_cedente'];
            $remessa = str_pad($RFP['contador_rem'], 7, "0", STR_PAD_LEFT);
            $protApos = $RFP['protesto_apos'];
            $Juros = round($RFP['juros'] * 30);
            $Multa = $RFP['multa'];
            $tipo_bol = $RFP['tipo_bol'];
            $mensagem1 = trataTxt(utf8_encode($RFP['mensagem1']));
            $mensagem2 = trataTxt(utf8_encode($RFP['mensagem2']));
            $mensagem3 = trataTxt(utf8_encode($RFP['mensagem3']));
        }
        odbc_exec($con, "update sf_bancos set contador_rem = contador_rem + 1 where id_bancos = " . $id_banco) or die(odbc_errormsg());                

        $arquivo = new Remessa(756, 'cnab240', array(
            'tipo_inscricao' => '2',
            'numero_inscricao' => $_SESSION["cpf_cnpj"],
            'agencia' => $agencia[0],
            'agencia_dv' => $agencia[1],
            'conta' => limit(str_pad(limit($contaX[0], 7), 8, "0", STR_PAD_LEFT), 8),
            'conta_dv' => $contaX[1],
            'nome_empresa' => $_SESSION["razao_social_contrato"],
            'numero_sequencial_arquivo' => $remessa,
            'codigo_beneficiario' => $conta,
            'codigo_beneficiario_dv' => $digito_conta,
            'situacao_arquivo' => 'T' //T-teste e P-Produção
        ));
        //----------------------------------------------------------------------------------------------------------------------        
        $items = $_POST['items'];
        $lancamento = '0';
        $vendas = '0';
        $solicitaparcela = '0';
        $mensalidades = '0';
        $setor = '';
        for ($i = 0; $i < sizeof($items); $i++) {
            $item = explode("|", $items[$i]);
            $setor = explode("-", $item[0]);
            if ($setor[0] == "D" || $setor[0] == "Y") {
                $aux = explode("-", $item[0]);
                $lancamento .= "," . $aux[1];
            } elseif ($setor[0] == "V") {
                $aux = explode("-", $item[0]);
                $vendas .= "," . $aux[1];
            } elseif ($setor[0] == "A") {
                $aux = explode("-", $item[0]);
                $solicitaparcela .= "," . $aux[1];
            } elseif ($setor[0] == "M") {
                $aux = explode("-", $item[0]);
                $mensalidades .= "," . $aux[1];
            }
            $clientes[] = array($item[0], $item[1], $item[2], $item[3], $item[4], $item[5], $item[6], $item[7], $item[8], $item[9], $item[10], $item[11], $item[12], $item[13], $item[14], $item[15]);
        }
        odbc_exec($con, "update sf_lancamento_movimento_parcelas set exp_remessa = 1 where id_parcela in ($lancamento)") or die(odbc_errormsg());
        odbc_exec($con, "update sf_venda_parcelas set exp_remessa = 1 where id_parcela in ($vendas)") or die(odbc_errormsg());
        odbc_exec($con, "update sf_solicitacao_autorizacao_parcelas set exp_remessa = 1 where id_parcela in ($solicitaparcela)") or die(odbc_errormsg());
        odbc_exec($con, "update sf_boleto set exp_remessa = 1 where tp_referencia = 'M' and id_referencia in ($mensalidades)") or die(odbc_errormsg());

        $lote = $arquivo->addLote(array('tipo_servico' => '1'));
        foreach ($clientes as $cliente) {
            $data = substr($cliente[4], 0, 2) . "/" . substr($cliente[4], 2, 2) . "/" . "20" . substr($cliente[4], 4, 2);
            $lote->inserirDetalhe(array(
                'nosso_numero' => $cliente[1],
                'parcela' => '01',
                'modalidade' => '1',
                'tipo_formulario' => '4',
                'codigo_carteira' => $cliente[2],
                'carteira' => $cliente[15],
                'seu_numero' => $cliente[3],
                'data_vencimento' => escreverDataSoma($data, " +0 days", "Y-m-d"),
                'valor' => valoresNumericos2($cliente[5]),
                'cod_emissao_boleto' => '2',
                'especie_titulo' => "DM",
                'data_emissao' => date('Y-m-d'),                
                'codigo_juros' => '2', // Taxa por mês,
                'data_juros' => escreverDataSoma($data, " +1 days", "Y-m-d"), // data dos juros, mesma do vencimento
                'vlr_juros' => $Juros, // Valor do juros/mora informa 1% e o sistema recalcula a 0,03% por 
                'codigo_multa' => '2', // Taxa por mês
                'data_multa' => escreverDataSoma($data, " +1 days", "Y-m-d"), // data dos juros, mesma do vencimento
                'vlr_multa' => $Multa, // Valor do juros de 2% ao mês
                //'codigo_desconto' => '1',
                //'data_desconto' => escreverDataSoma($data, " +0 days", "Y-m-d"),
                //'vlr_desconto' => valoresNumericos2($cliente[14]),
                //'vlr_IOF' => '0',                
                'protestar' => '1', // 1 = Protestar com (Prazo) dias, 3 = Devolver após (Prazo) dias
                'prazo_protesto' => $protApos, // Informar o numero de dias apos o vencimento para iniciar o protesto
                'identificacao_contrato' => "Contrato " . $conta,                
                'tipo_inscricao' => (strlen($cliente[7]) == 18 ? '2' : '1'),
                'numero_inscricao' => $cliente[7],
                'nome_pagador' => $cliente[8],
                'endereco_pagador' => $cliente[9],
                'bairro_pagador' => $cliente[10],
                'cep_pagador' => $cliente[11],
                'cidade_pagador' => $cliente[12],
                'uf_pagador' => $cliente[13],                
                'mensagem_sc_1' => $mensagem1,
                'mensagem_sc_2' => $mensagem2,
                'mensagem_sc_3' => $mensagem3,
                'mensagem_sc_4' => "",
            ));
        }
        echo utf8_decode($arquivo->getText());
    }
}