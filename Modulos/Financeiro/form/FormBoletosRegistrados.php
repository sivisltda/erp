<?php

$PATH = __DIR__ . "./../../../Pessoas/" . $_SESSION["contrato"] . "/LoteBoletos/";

function limit($palavra, $limite) {
    if (strlen($palavra) >= $limite) {
        $var = substr($palavra, 0, $limite);
    } else {
        $max = (int) ($limite - strlen($palavra));
        $var = $palavra . complementoRegistro($max, "brancos");
    }
    return $var;
}

function sequencial($i) {
    if ($i < 10) {
        return zeros(0, 5) . $i;
    } else if ($i >= 10 && $i < 100) {
        return zeros(0, 4) . $i;
    } else if ($i >= 100 && $i < 1000) {
        return zeros(0, 3) . $i;
    } else if ($i >= 1000 && $i < 10000) {
        return zeros(0, 2) . $i;
    } else if ($i >= 10000 && $i < 100000) {
        return zeros(0, 1) . $i;
    }
}

function zeros($min, $max) {
    $zeros = '';
    $x = ($max - strlen($min));
    for ($i = 0; $i < $x; $i++) {
        $zeros .= '0';
    }
    return $zeros . $min;
}

function complementoRegistro($int, $tipo) {
    if ($tipo == "zeros") {
        $space = '';
        for ($i = 1; $i <= $int; $i++) {
            $space .= '0';
        }
    } else if ($tipo == "brancos") {
        $space = '';
        for ($i = 1; $i <= $int; $i++) {
            $space .= ' ';
        }
    }
    return $space;
}

function trataTxt($text) {
    $utf8 = array(
        '/[áàâãªä]/u' => 'a',
        '/[ÁÀÂÃÄ]/u' => 'A',
        '/[ÍÌÎÏ]/u' => 'I',
        '/[íìîï]/u' => 'i',
        '/[éèêë]/u' => 'e',
        '/[ÉÈÊË]/u' => 'E',
        '/[óòôõºö]/u' => 'o',
        '/[ÓÒÔÕÖ]/u' => 'O',
        '/[úùûü]/u' => 'u',
        '/[ÚÙÛÜ]/u' => 'U',
        '/ç/' => 'c',
        '/Ç/' => 'C',
        '/ñ/' => 'n',
        '/Ñ/' => 'N',
        '/–/' => '-', // UTF-8 hyphen to "normal" hyphen
        '/[’‘‹›‚]/u' => ' ', // Literally a single quote
        '/[“”«»„]/u' => ' ', // Double quote
        '/ /' => ' ', // nonbreaking space (equiv. to 0x160)
    );
    return preg_replace(array_keys($utf8), array_values($utf8), $text);
}

function modulo_11($num, $base = 9, $r = 0) {
    $soma = 0;
    $fator = 2;
    for ($i = strlen($num); $i > 0; $i--) {
        $numeros[$i] = substr($num, $i - 1, 1);
        $parcial[$i] = $numeros[$i] * $fator;
        $soma += $parcial[$i];
        if ($fator == $base) {
            $fator = 1;
        }
        $fator++;
    }
    if ($r == 0) {
        $soma *= 10;
        $digito = $soma % 11;
        if ($digito == 10) {
            $digito = 0;
        }
        return $digito;
    } elseif ($r == 1) {
        $resto = $soma % 11;
        return $resto;
    }
}

function codCarteira($val) {            
    if($val == 101) { //5(101) = RÁPIDA COM REGISTRO(BLOQUETE EMITIDO PELO CLIENTE)        
        return 5;
    } else if($val == 104) { //1(104) = ELETRÔNICA COM REGISTRO
        return 1;        
    } else if($val == 201) { //6(201) = Cobrança penhor rápida com registro        
        return 6;        
    }
}

if (isset($_POST['bntSaveFile'])) {
    if (is_numeric($id_banco)) {
        $fusohorario = 3;
        $cpf_cnpj = str_replace(array("/", "-", "."), "", $_SESSION["cpf_cnpj"]);
        $razao_social = substr(trataTxt($_SESSION["razao_social_contrato"]), 0, 30);
        $timestamp = mktime(date("H") - $fusohorario, date("i"), date("s"), date("m"), date("d"), date("Y"));
        $DATA['DIA'] = gmdate("d", $timestamp);
        $DATA['MES'] = gmdate("m", $timestamp);
        $DATA['ANO'] = gmdate("y", $timestamp);
        define("REMESSA", $PATH, true);
        $filename = REMESSA . "C" . $DATA['DIA'] . $DATA['MES'] . $DATA['ANO'] . "A" . ".txt";
        $conteudo = '';
        $cod_carteira = '';
        $protApos = 0;
        $JurosDia = 0;
        $Multa = 0;
        $tipo_bol = 0;
        $total_pago = 0;        
        $cur = odbc_exec($con, "SELECT *, (select max(sf_carteira) from sf_bancos_carteiras where sf_carteiras_bancos = id_bancos) cod_carteira FROM sf_bancos WHERE id_bancos = " . $id_banco);
        while ($RFP = odbc_fetch_array($cur)) {            
            $banco = $RFP['num_banco'];            
            $agencia = $RFP['agencia'];            
            $conta = str_replace(" ", "", $RFP['cedente']);
            $contaX = limit(str_pad(limit($RFP['conta'], 7), 8, "0", STR_PAD_LEFT), 8);
            $complemento = substr($RFP['conta'], -2);
            $digito_conta = $RFP['dv_cedente'];
            $protApos = $RFP['protesto_apos'];
            $JurosDia = $RFP['juros'];
            $Multa = $RFP['multa'];            
            $tipo_bol = $RFP['tipo_bol'];  
            $cod_carteira = $RFP['cod_carteira'];  
            $mensagem1 = trataTxt(utf8_encode($RFP['mensagem1']));            
            $conteudo .= '0'; // tipo de registro id registro header 001 001 9(01)
            $conteudo .= '1'; // operacao tipo operacao remessa 002 002 9(01)
            $conteudo .= 'REMESSA'; // literal remessa escr. extenso 003 009 X(07)
            $conteudo .= '01'; // codigo servico id tipo servico 010 011 9(02)
            $conteudo .= limit('COBRANCA', 15); // literal cobranca escr. extenso 012 026 X(15)
            $conteudo .= limit($agencia, 4);            
            if($tipo_bol == 0 || $tipo_bol == 8) {
                $conteudo .= $conta;
                $conteudo .= complementoRegistro(16 - strlen($conta), "brancos");
            } else if($tipo_bol == 6 || $tipo_bol == 7 || $tipo_bol == 11) {
                $conteudo .= substr($agencia, 5, 1);
                $conteudo .= str_pad(str_replace("-", "", $contaX), 9, "0", STR_PAD_LEFT);
                $conteudo .= complementoRegistro(6, "zeros");
            } else if($tipo_bol == 5 || $tipo_bol == 10) {
                $conteudo .= limit(str_pad($conta, 8, "0", STR_PAD_LEFT), 8);
                $conteudo .= $contaX;
            } else {
                $conteudo .= complementoRegistro(2, "zeros"); // zeros complemento d registro 031 032 9(02)
                $conteudo .= limit($conta, 5); // conta conta da empresa 033 037 9(05)
                $conteudo .= limit($digito_conta, 1); // dac digito autoconf conta 038 038 9(01)            
                $conteudo .= complementoRegistro(8, "brancos"); // complemento registro 039 046 X(08)
            }
            $conteudo .= limit($razao_social, 30); //nome da empresa 047 076 X(30)            
            $conteudo .= $banco; // codigo banco Nº BANCO CÂMARA COMP. 077 079 9(03)
            $conteudo .= limit(trataTxt($RFP['descricao']), 15); // nome do banco por ext. 080 094 X(15)
            $conteudo .= $DATA['DIA'] . $DATA['MES'] . $DATA['ANO']; //data geracao arquivo 095 100 9(06)            
            if($tipo_bol == 0 || $tipo_bol == 8) {
                $conteudo .= complementoRegistro(289, "brancos"); // complemento de registr 101 394 X(294)                
                $conteudo .= str_pad($RFP['contador_rem'], 5, "0", STR_PAD_LEFT);
            } else if($tipo_bol == 6 || $tipo_bol == 7 || $tipo_bol == 11) {
                $conteudo .= str_pad($RFP['contador_rem'], 7, "0", STR_PAD_LEFT);
                $conteudo .= complementoRegistro(22, "brancos");
                $conteudo .= limit(str_pad($conta, 7, "0", STR_PAD_LEFT), 7);
                $conteudo .= complementoRegistro(258, "brancos");
            } else if($tipo_bol == 5 || $tipo_bol == 10) {                
                $conteudo .= complementoRegistro(16, "zeros"); // zeros complemento d registro 031 032 9(02)
                $conteudo .= complementoRegistro(275, "brancos"); // complemento de registr 101 394 X(294)
                $conteudo .= "279";
            } else {
                $conteudo .= complementoRegistro(294, "brancos"); // complemento de registr 101 394 X(294)    
            }            
            $conteudo .= sequencial(1); // numero sequencial registro no arquivo 395 400 9(06)
            $conteudo .= chr(13) . chr(10); //essa é a quebra de linha
        }
        //----------------------------------------------------------------------------------------------------------------------        
        $items = $_POST['items'];
        $lancamento = '0';
        $vendas = '0';
        $solicitaparcela = '0';
        $mensalidades = '0';
        $setor = '';
        for ($i = 0; $i < sizeof($items); $i++) {
            $item = explode("|", $items[$i]);
            $setor = explode("-", $item[0]);
            if ($setor[0] == "D" || $setor[0] == "Y") {
                $aux = explode("-", $item[0]);
                $lancamento .= "," . $aux[1];
            } elseif ($setor[0] == "V") {
                $aux = explode("-", $item[0]);
                $vendas .= "," . $aux[1];
            } elseif ($setor[0] == "A") {
                $aux = explode("-", $item[0]);
                $solicitaparcela .= "," . $aux[1];
            } elseif ($setor[0] == "M") {
                $aux = explode("-", $item[0]);
                $mensalidades .= "," . $aux[1];
            }
            $clientes[] = array($item[0], $item[1], $item[2], $item[3], $item[4], $item[5], $item[6], $item[7], $item[8], $item[9], $item[10], $item[11], $item[12], $item[13], $item[14], $item[15]);
        }
        odbc_exec($con, "update sf_lancamento_movimento_parcelas set exp_remessa = 1 where id_parcela in ($lancamento)") or die(odbc_errormsg());
        odbc_exec($con, "update sf_venda_parcelas set exp_remessa = 1 where id_parcela in ($vendas)") or die(odbc_errormsg());
        odbc_exec($con, "update sf_solicitacao_autorizacao_parcelas set exp_remessa = 1 where id_parcela in ($solicitaparcela)") or die(odbc_errormsg());
        odbc_exec($con, "update sf_boleto set exp_remessa = 1 where tp_referencia = 'M' and id_referencia in ($mensalidades)") or die(odbc_errormsg());        
        if($tipo_bol == 0 || $tipo_bol == 8 || $tipo_bol == 6 || $tipo_bol == 7 || $tipo_bol == 11) {        
            odbc_exec($con, "update sf_bancos set contador_rem = contador_rem + 1 where id_bancos = " . $id_banco) or die(odbc_errormsg());        
        }
        $i = 2;
        foreach ($clientes as $cliente) {
            if($tipo_bol == 6 || $tipo_bol == 7 || $tipo_bol == 11) { 
                $conteudo .= '7';
            } else {
                $conteudo .= '1'; // tipo registro id registro transacac. 001 001 9(01)                
            }
            $conteudo .= '02'; // codigo inscricao tipo inscricao empresa 002 003 9(02)
            $conteudo .= limit($cpf_cnpj, 14); // cnpj da empresa 004 017 9(14)            
            $conteudo .= limit($agencia, 4); // agencia mantenedora da conta 018 021 9(04)            
            if($tipo_bol == 0 || $tipo_bol == 8) {
                $conteudo .= $conta;
                $conteudo .= '2';
                $conteudo .= complementoRegistro(8 - (strlen($conta) + 1), "zeros"); // complemento registro 039 046 X(08)
            } else if($tipo_bol == 6 || $tipo_bol == 7 || $tipo_bol == 11) {
                $conteudo .= substr($agencia, 5, 1);
                $conteudo .= str_pad(str_replace("-", "", $contaX), 9, "0", STR_PAD_LEFT);                
            } else if($tipo_bol == 5 || $tipo_bol == 10) {
                $conteudo .= limit(str_pad($conta, 8, "0", STR_PAD_LEFT), 8);                          
            } else {
                $conteudo .= '00';
                $conteudo .= limit($conta, 5);
                $conteudo .= limit($digito_conta, 1);
            }            
            if($tipo_bol == 0 || $tipo_bol == 8) {
                $conteudo .= complementoRegistro(8, "zeros"); // CÓD.INSTRUÇÃO/ALEGAÇÃO A SER CANC NOTA 27 034 037 9(04)                                
            } else if($tipo_bol == 6 || $tipo_bol == 7 || $tipo_bol == 11) {
                $conteudo .= limit(str_pad($conta, 7, "0", STR_PAD_LEFT), 7);
            } else if($tipo_bol == 5 || $tipo_bol == 10) {
                $conteudo .= $contaX;            
            } else {
                $conteudo .= complementoRegistro(4, "brancos"); // brancos complemento registro 030 033 X(04)
                $conteudo .= complementoRegistro(4, "zeros"); // CÓD.INSTRUÇÃO/ALEGAÇÃO A SER CANC NOTA 27 034 037 9(04)                
            }                    
            if($tipo_bol == 0 || $tipo_bol == 8) {
                $conteudo .= limit($cliente[0], 19);
                $conteudo .= '14' . limit($cliente[1], 15);
                $conteudo .= complementoRegistro(10, "brancos");
                $conteudo .= $cliente[2];                
                $conteudo .= complementoRegistro(20, "brancos"); // uso do banco ident. oper. no banco 087 107 X(21)
                $conteudo .= '01';    
            } else if($tipo_bol == 6 || $tipo_bol == 7 || $tipo_bol == 11) {
                $conteudo .= limit($cliente[0], 25);
                $conteudo .= limit(str_pad($conta, 7, "0", STR_PAD_LEFT), 7);
                $conteudo .= limit($cliente[1], 10);
                $conteudo .= complementoRegistro(4, "zeros");
                $conteudo .= complementoRegistro(3, "brancos");
                $conteudo .= ' '; //Indicativo de Mensagem ou Sacador/Avalista
                $conteudo .= complementoRegistro(3, "brancos");                
                $conteudo .= limit(str_pad($cliente[15], 3, "0", STR_PAD_LEFT), 3); //Variação da Carteira
                $conteudo .= '0';
                $conteudo .= complementoRegistro(6, "zeros");
                $conteudo .= complementoRegistro(5, "brancos"); //Tipo de Cobrança
                $conteudo .= '17';               
            } else if($tipo_bol == 5 || $tipo_bol == 10) {                
                $conteudo .= complementoRegistro(25, "brancos");                
                $conteudo .= limit(str_pad($cliente[1] . modulo_11($cliente[1], 9, 0), 8, "0", STR_PAD_LEFT), 8); 
                $conteudo .= complementoRegistro(6, "zeros");
                $conteudo .= ' ';
                if ($Multa > 0) { 
                    $conteudo .= '4';                    
                    $conteudo .= str_pad(escreverNumero($Multa, 0, 0), 4, "0", STR_PAD_LEFT);
                    $conteudo .= complementoRegistro(15, "zeros");    
                } else {
                    $conteudo .= complementoRegistro(20, "zeros");    
                }
                $conteudo .= complementoRegistro(4, "brancos");                                
                $conteudo .= complementoRegistro(6, "zeros"); 
                $conteudo .= codCarteira($cod_carteira);
            } else {
                $conteudo .= limit($cliente[0], 25);
                $conteudo .= limit(str_pad($cliente[1], 8, "0", STR_PAD_LEFT), 8);
                $conteudo .= limit('0000000000000', 13);
                $conteudo .= $cliente[2];
                $conteudo .= complementoRegistro(21, "brancos"); // uso do banco ident. oper. no banco 087 107 X(21)
                $conteudo .= 'I'; // carteira codigo da carteira NOTA 5 108 108 X(01)                
            }
            $conteudo .= ($_POST['txtCancel'] == "S" ? '02' : '01'); // codigo ocorrencia / ident da ocorrencia NOTA 6 109 110 9(02)
            if($tipo_bol == 5 || $tipo_bol == 10) { 
                $conteudo .= limit(str_pad($cliente[3], 10, "0", STR_PAD_LEFT), 10);
            } else {
                $conteudo .= limit($cliente[3], 10); // nº documento / nº documento de cobranca NOTA 18 111 120 X(10)                
            }
            $conteudo .= limit($cliente[4], 6); // vencimento data venc. titulo NOTA 7 121 126 9(06)            
            $conteudo .= zeros(str_replace(".", "", str_replace(",", "", $cliente[5])), 13); // valor titulo valor nominal NOTA 8 127 139 9(11)V9(2)
            $conteudo .= $banco; // codigo do banco Nº BANCO CÂMARA COMP. 140 142 9(03)
            if($tipo_bol == 5 || $tipo_bol == 10) { 
                $conteudo .= limit(str_pad(limit($agencia, 4), 5, "0", STR_PAD_LEFT), 5);                
            } else if($tipo_bol == 6 || $tipo_bol == 7 || $tipo_bol == 11) {
                $conteudo .= zeros(0, 4);
                $conteudo .= ' ';                
            } else {                
                $conteudo .= zeros(0, 5); //agencia cobradora / ONDE TÍTULO SERÁ COBRADO NOTA 9 143 147 9(05)
            }            
            $conteudo .= '01'; //15; // especie especie do titulo NOTA 10 148 149 X(02)
            $conteudo .= 'N'; // aceite ident de titutlo aceito (A=aceite,N=nao aceite) 150 150 X(01)
            //---------------------------------------------------------------------------------------------------------------
            $conteudo .= date(str_replace("/", "", strtolower($lang['dmy']))); // data emissao titulo NOTA 31 151 156 9(06)
            $conteudo .= '00'; //'88'; // instrucao 1 NOTA 11 157 158 X(02)
            $conteudo .= '00'; //'86'; // instrucao 2 NOTA 11 159 160 X(02)
            $conteudo .= zeros(str_replace(".", "", str_replace(",", "", escreverNumero((valoresNumericos2($cliente[5]) * $JurosDia) / 100))), 13); // juros de 1 dia valor de mora NOTA 12 161 173 9(11)V9(02)
            if (is_numeric($cliente[14]) && $cliente[14] > 0) {
                $conteudo .= limit(str_replace(".", "", str_replace(",", "", escreverNumero($cliente[4]))), 6);
                $conteudo .= zeros(str_replace(".", "", str_replace(",", "", escreverNumero($cliente[14]))), 13);
            } else {
                $conteudo .= zeros(0, 6); // desconto até data limite p/ descont 174 179 9(06)
                $conteudo .= complementoRegistro(13, "zeros"); // valor desconto a ser concedido NOTA 13 180 192 9(11)V9(02)
            }
            $conteudo .= complementoRegistro(13, "zeros"); // valor I.O.F RECOLHIDO P NOTAS SEGURO NOTA 14 193 205 9(11)V9(02)
            $conteudo .= complementoRegistro(13, "zeros"); // abatimento a ser concedido NOTA 13 206 218 9(11)V9(02)
            if (strlen($cliente[7]) == 18) { // codigo de inscricao tipo de insc. sacado 01=CPF 02=CNPJ 219 220 9(02)
                $conteudo .= '02';
                $conteudo .= str_pad(str_replace("-", "", str_replace("/", "", str_replace(".", "", $cliente[7]))), 14, "0", STR_PAD_LEFT); // numero de inscricao cpf ou cnpj 221 234 9(14)
            } else {
                $conteudo .= '01';
                $conteudo .= '000' . str_pad(str_replace("-", "", str_replace("/", "", str_replace(".", "", $cliente[7]))), 11, "0", STR_PAD_LEFT); // numero de inscricao cpf ou cnpj 221 234 9(14)
            }
            $conteudo .= limit(trataTxt($cliente[8]), 30); // nome nome do sacado NOTA 15 235 264 X(30)
            $conteudo .= complementoRegistro(10, "brancos"); //NOTA 15 complem regist 265 274 X(10)
            $conteudo .= limit(trataTxt($cliente[9]), 40); // logradouro rua numero e compl sacado 275 314 X(40)
            $conteudo .= limit(trataTxt($cliente[10]), 12); // bairro bairro do sacado 315 326 X(12)
            $conteudo .= zeros(str_replace(array("-", ".", " "), "", $cliente[11]), 8); // cep cep do sacado 327 334 9(08)
            $conteudo .= limit(trataTxt($cliente[12]), 15); // cidade cidade do sacado 335 349 X(15)
            $conteudo .= limit($cliente[13], 2); // estado uf do sacado 350 351 X(02)            
            if($tipo_bol == 0 || $tipo_bol == 8) {
                $conteudo .= complementoRegistro(30, "zeros"); // sacador/avalista sacad ou aval. NOTA 16 352 381 X(30)                   
            } else {
                $conteudo .= limit('', 30); // sacador/avalista sacad ou aval. NOTA 16 352 381 X(30)                   
            }            
            if($tipo_bol == 5 || $tipo_bol == 10) {
                $conteudo .= ' ';
                $conteudo .= 'I' . $complemento;
                $conteudo .= complementoRegistro(6, "brancos"); // complemento de regist. 382 385 X(04)
            } else {
                $conteudo .= complementoRegistro(4, "brancos"); // complemento de regist. 382 385 X(04)            
                $conteudo .= zeros(0, 6); // data de mora data de mora 386 391 9(06)                            
            }
            $conteudo .= zeros($protApos, 2); // prazo qtde de dias NOTA 11(A) 392 393 9(02)            
            if($tipo_bol == 0 || $tipo_bol == 8) {
                $conteudo .= '1';
            } else {
                $conteudo .= complementoRegistro(1, "brancos"); // brancos complemento de registr. 394 394 X(01)
            }            
            $conteudo .= sequencial($i); // numero sequencial do registro no arquivo 395 400 9(06)
            $conteudo .= chr(13) . chr(10); //essa é a quebra de linha
            $total_pago = $total_pago + valoresNumericos2($cliente[5]);
            $i++;
            if($tipo_bol == 5 || $tipo_bol == 10) {
                for ($j = 0; $j < 2; $j++) {
                    $conteudo .= '7';                
                    $conteudo .= complementoRegistro(16, "brancos");                
                    $conteudo .= limit($agencia, 4);
                    $conteudo .= limit(str_pad($conta, 8, "0", STR_PAD_LEFT), 8);
                    $conteudo .= $contaX;
                    $conteudo .= complementoRegistro(10, "brancos");
                    $conteudo .= ($j == 0 ? "01" : "02");                
                    $conteudo .= limit(($j == 0 ? $mensagem1 : ""), 48);
                    $conteudo .= ($j == 0 ? "3" : "1");
                    $conteudo .= complementoRegistro(284, "brancos");
                    $conteudo .= 'I' . $complemento;                
                    $conteudo .= complementoRegistro(9, "brancos");                
                    $conteudo .= sequencial($i);                
                    $conteudo .= chr(13) . chr(10);
                    $i++;              
                }
            } else if(($tipo_bol == 6 || $tipo_bol == 7 || $tipo_bol == 11) && $Multa > 0) {
                $conteudo .= '5';                
                $conteudo .= '99';                
                $conteudo .= '2'; //'1' = Valor, '2' = Percentual, '9' = Dispensar Cobrança de Multa               
                $conteudo .= limit($cliente[4], 6); // vencimento data venc.                
                $conteudo .= str_pad(escreverNumero($Multa, 0, 2, "",""), 7, "0", STR_PAD_LEFT); // com cinco inteiros e dois decimais                   
                $conteudo .= complementoRegistro(377, "brancos");              
                $conteudo .= sequencial($i);                
                $conteudo .= chr(13) . chr(10);
                $i++;    
            }
        }
        $conteudo .= '9';
        if($tipo_bol == 0 || $tipo_bol == 8) {
            $conteudo .= complementoRegistro(393, "brancos");
        } else if($tipo_bol == 6 || $tipo_bol == 7 || $tipo_bol == 11) {            
            $conteudo .= complementoRegistro(393, "brancos");
        } else if($tipo_bol == 5 || $tipo_bol == 10) {            
            $conteudo .= zeros(sequencial($i), 6);
            $conteudo .= zeros(str_replace(".", "", str_replace(",", "", escreverNumero($total_pago))), 13);
            $conteudo .= complementoRegistro(374, "zeros");
        } else {
            $conteudo .= complementoRegistro(393, "zeros");
        }        
        $conteudo .= zeros(sequencial($i), 6);
        $conteudo .= chr(13) . chr(10);
        if (!$handle = fopen($filename, 'w+')) {
            erro("Não foi possível abrir o arquivo ($filename)");
        }
        if (fwrite($handle, $conteudo) === FALSE) {
            echo "Não foi possível escrever no arquivo ($filename)";
        }
        fclose($handle);
        header("Content-Type: " . "application/txt");
        header("Content-Length: " . filesize($filename));
        header("Content-Disposition: attachment; filename=" . basename($filename));
        readfile($filename);
        exit;
    } else {
        echo "<script>alert('Informe o banco para geração do arquivo!');</script>";
    }
}
