<?php

require_once(__DIR__ . '/../../Connections/configini.php');
$where = "";
$imprimir = 0;
$grupoArray = [];

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $where .= " and data_pagamento between " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']) . "";
}

if (isset($_GET['lc']) && is_numeric($_GET['lc'])) {
    $where .= " and l.empresa = " . $_GET['lc'];
}

if (isset($_GET['tpb']) && $_GET['tpb'] == '0') {
    $where .= " and c.tipo in ('C')";
} else if (isset($_GET['tpb']) && $_GET['tpb'] == '1') {
    $where .= " and c.tipo in ('D')";
} else {
    $where .= " and c.tipo in ('C','D')";
}

$cur = odbc_exec($con, "set dateformat dmy;");
$sQuery1 = "select g.centro_custo, c.tipo, g.id_grupo_contas, g.descricao grupo, c.descricao, p.data_pagamento, p.historico_baixa, valor_pago, 
c.id_contas_movimento, f.id_fornecedores_despesas, razao_social, cc.descricao centro_custo_desc 
from sf_lancamento_movimento_parcelas p 
inner join sf_lancamento_movimento l on p.id_lancamento_movimento = l.id_lancamento_movimento
inner join sf_fornecedores_despesas f on l.fornecedor_despesa = f.id_fornecedores_despesas
inner join sf_contas_movimento c on l.conta_movimento = c.id_contas_movimento
left join sf_tipo_documento on p.tipo_documento = sf_tipo_documento.id_tipo_documento
left join sf_grupo_contas g on c.grupo_conta = g.id_grupo_contas
left join sf_centro_custo cc on cc.id = g.centro_custo
where l.status = 'Aprovado' and data_pagamento is not null 
and p.inativo = 0 " . $where . " union all
select g.centro_custo, c.tipo, g.id_grupo_contas, g.descricao grupo, c.descricao, p.data_pagamento, p.historico_baixa, valor_pago, 
c.id_contas_movimento, f.id_fornecedores_despesas, razao_social, cc.descricao centro_custo_desc  
from sf_solicitacao_autorizacao_parcelas p 
inner join sf_solicitacao_autorizacao l on l.id_solicitacao_autorizacao = p.solicitacao_autorizacao
inner join sf_fornecedores_despesas f on l.fornecedor_despesa = f.id_fornecedores_despesas
inner join sf_contas_movimento c on l.conta_movimento = c.id_contas_movimento
left join sf_grupo_contas g on c.grupo_conta = g.id_grupo_contas
left join sf_centro_custo cc on cc.id = g.centro_custo
where status = 'Aprovado' and c.tipo in ('C','D') and data_pagamento is not null 
and p.inativo = 0 " . $where . " union all
select g.centro_custo, c.tipo, g.id_grupo_contas, g.descricao grupo, c.descricao, p.data_pagamento,
(STUFF((SELECT distinct ',' + descricao FROM sf_vendas_itens vi inner join sf_produtos p on p.conta_produto = vi.produto WHERE vi.id_venda = l.id_venda FOR XML PATH('')), 1, 1, '')) historico_baixa,
valor_pago, c.id_contas_movimento, f.id_fornecedores_despesas, razao_social, cc.descricao centro_custo_desc  
from sf_venda_parcelas p 
inner join sf_vendas l on l.id_venda = p.venda 
inner join sf_fornecedores_despesas f on l.cliente_venda = f.id_fornecedores_despesas
inner join sf_contas_movimento c on l.conta_movimento = c.id_contas_movimento
left join sf_grupo_contas g on c.grupo_conta = g.id_grupo_contas
left join sf_centro_custo cc on cc.id = g.centro_custo
where status = 'Aprovado' and c.tipo in ('C','D') and data_pagamento is not null 
and p.inativo = 0 " . $where;
if (isset($_GET['tpc']) && $_GET['tpc'] == '0' && $_GET['imp'] == "1") {
    $sQuery1 = "select x.tipo,x.id_grupo_contas,x.grupo,x.descricao, sum(x.valor_pago) valor_pago from (" . $sQuery1 . 
    ") as x group by x.tipo,x.id_grupo_contas,x.grupo,x.descricao order by 1, 2, 3, 4, 5";
} else {
    $sQuery1 .= " order by 1, 2, 3, 4, 5";
}
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($RFP = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = utf8_encode($RFP['tipo']);
    if ($_GET['tpImp'] != "I") {
        $row[] = utf8_encode($RFP['grupo']);
        $row[] = utf8_encode($RFP['descricao']);
    }    
    if (isset($_GET['tpc']) && $_GET['tpc'] == '0' && $_GET['imp'] == "1") {
        $row[] = utf8_encode($RFP['descricao']);        
        $row[] = escreverNumero(($RFP['tipo'] == "C" ? $RFP['valor_pago'] : 0));
        $row[] = escreverNumero(($RFP['tipo'] == "D" ? $RFP['valor_pago'] : 0));
        $row[] = $RFP['id_grupo_contas'];
        $grp = array($RFP['id_grupo_contas'], (is_numeric($RFP['id_grupo_contas']) ? utf8_encode($RFP['grupo']) : "SEM AGRUPAMENTO"), 4);
        if (!in_array($grp, $grupoArray)) {
            $grupoArray[] = $grp;
        }
    } else {
        $row[] = escreverData($RFP['data_pagamento']);
        $row[] = utf8_encode($RFP['historico_baixa']);
        $row[] = utf8_encode($RFP['id_fornecedores_despesas']) . " - " . utf8_encode($RFP['razao_social']);
        $row[] = escreverNumero($RFP['valor_pago']);    
        if (isset($_GET['pdf']) && $_GET['imp'] == "1") {
            $row[] = $RFP['id_contas_movimento'];
            $grp = array($RFP['id_contas_movimento'], 
                (is_numeric($RFP['id_contas_movimento']) ? utf8_encode($RFP['grupo']) . " - " . utf8_encode($RFP['descricao']) : "SEM AGRUPAMENTO"), 5);
            if (!in_array($grp, $grupoArray)) {
                $grupoArray[] = $grp;
            }
        }
    }
    $row[] = utf8_encode($RFP['centro_custo']);
    $row[] = utf8_encode($RFP['centro_custo_desc']);
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
} else {
    if (isset($_GET['tpc']) && $_GET['tpc'] == '0' && $_GET['imp'] == "1") {
        $output['aaTotal'][] = array(2, 2, "Sub Total", "Total", 0, 0, 0);        
        $output['aaTotal'][] = array(3, 3, "", "", 0, 0, 0);        
    } else {
        $output['aaTotal'][] = array(4, 4, "Sub Total", "Total", 0, 0, 0);            
    }
    $output['aaGrupo'] = $grupoArray;
}
odbc_close($con);
//echo json_encode($output); exit;
