<?php
include './../../Connections/configini.php';

$FinalUrl = '';
$linhaContato = "1";
$disabled = 'disabled';
$active1 = 'active';
$active3 = '';
$active6 = '';

$q1_tc_recebidas = 0;
$q1_nc_recebidas = 0;
$q1_tc_receber = 0;
$q1_nc_receber = 0;

$q2_ult_conta = "";
$q2_pri_conta = "";
$q2_ult_venc = "";
$q2_pro_venc = "";
$q2_med_recebido = 0;
$q2_med_receber = 0;

$q3_tot_atraso = 0;
$q3_qtd_atraso = 0;
$q3_med_atraso = 0;
$q3_tot_antecipado = 0;
$q3_qtd_antecipado = 0;
$q3_med_antecipado = 0;

if ($_GET['idx'] != '') {
    $FinalUrl = "id=" . $_GET['idx'];
}
if ($_GET['tpx'] != '') {
    $FinalUrl = $FinalUrl . "&tp=" . $_GET['tpx'];
}
if ($_GET['tdx'] != '') {
    $FinalUrl = $FinalUrl . "&td=" . $_GET['tdx'];
}
if ($_GET['dti'] != '') {
    $FinalUrl = $FinalUrl . "&dti=" . $_GET['dti'];
}
if ($_GET['dtf'] != '') {
    $FinalUrl = $FinalUrl . "&dtf=" . $_GET['dtf'];
}
if ($_POST['txtFinalUrl']) {
    $FinalUrl = $_POST['txtFinalUrl'];
}

if (isset($_POST['bntSave'])) {
    $idCliente = "";
    $notIn = "";
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_fornecedores_despesas set " .
                        "razao_social = '" . strtoupper(utf8_decode($_POST['txtRazao'])) . "'," .
                        "cnpj = '" . utf8_decode($_POST['txtCnpj']) . "'," .
                        "inscricao_estadual = '" . utf8_decode($_POST['txtInsc']) . "'," .
                        "endereco = '" . utf8_decode($_POST['txtEndereco']) . "'," .
                        "numero = '" . utf8_decode($_POST['txtNumero']) . "'," .
                        "complemento = '" . utf8_decode($_POST['txtComplemento']) . "'," .
                        "bairro = '" . utf8_decode($_POST['txtBairro']) . "'," .
                        "estado = " . utf8_decode($_POST['txtEstado']) . "," .
                        "cidade = " . utf8_decode($_POST['txtCidade']) . "," .
                        "cep = '" . utf8_decode($_POST['txtCep']) . "'," .
                        "contato = '" . utf8_decode($_POST['txtContato']) . "'," .
                        "nome_fantasia = '" . utf8_decode($_POST['txtFantasia']) . "'," .
                        "regime_tributario = '" . utf8_decode($_POST['txtRegTributario']) . "'," .
                        "grupo_pessoa = " . utf8_decode($_POST['txtGrupoPessoa']) . "," .
                        "inativo = " . valoresCheck("txtInativo") . " " .
                        "where id_fornecedores_despesas = " . $_POST['txtId']) or die(odbc_errormsg());
        $notIn = '0';
        if (isset($_POST['txtQtdContatos'])) {
            $max = $_POST['txtQtdContatos'];
            for ($i = 0; $i <= $max; $i++) {
                if (isset($_POST['txtIdContato_' . $i]) && is_numeric($_POST['txtIdContato_' . $i])) {
                    if ($_POST['txtIdContato_' . $i] != 0) {
                        $notIn = $notIn . "," . $_POST['txtIdContato_' . $i];
                    }
                }
            }
            if ($notIn != "") {
                odbc_exec($con, "delete from sf_fornecedores_despesas_contatos where fornecedores_despesas = " . $_POST['txtId'] . " and id_contatos not in (" . $notIn . ")");
            }
        }
        $idCliente = $_POST['txtId'];
    } else {
        $toSave = true;
        if ($_POST['txtCnpj'] != '') {
            $cur = odbc_exec($con, "select * from sf_fornecedores_despesas where tipo = 'F' and cnpj = '" . str_replace("'", "", $_POST['txtCnpj']) . "'");
            while ($RFP = odbc_fetch_array($cur)) {
                $toSave = false;
            }
        }
        if ($toSave == false) {
            echo "<script>alert('Registro de CPF/CNPJ já cadastrado no sistema!');</script>";
        } else {
            $query = "insert into sf_fornecedores_despesas(razao_social,cnpj,regime_tributario,inscricao_estadual,
            endereco,numero,complemento,bairro,estado,cidade,cep,contato,nome_fantasia,
            inativo,grupo_pessoa,tipo,empresa,dt_cadastro)values('" .
                    strtoupper(utf8_decode($_POST['txtRazao'])) . "','" .
                    utf8_decode($_POST['txtCnpj']) . "','" .
                    utf8_decode($_POST['txtRegTributario']) . "','" .
                    utf8_decode($_POST['txtInsc']) . "','" .
                    utf8_decode($_POST['txtEndereco']) . "','" .
                    utf8_decode($_POST['txtNumero']) . "','" .
                    utf8_decode($_POST['txtComplemento']) . "','" .
                    utf8_decode($_POST['txtBairro']) . "'," .
                    utf8_decode($_POST['txtEstado']) . "," .
                    utf8_decode($_POST['txtCidade']) . ",'" .
                    utf8_decode($_POST['txtCep']) . "','" .
                    utf8_decode($_POST['txtContato']) . "','" .
                    utf8_decode($_POST['txtFantasia']) . "'," .
                    valoresCheck("txtInativo") . "," .
                    utf8_decode($_POST['txtGrupoPessoa']) . ",'F','001',getDate())";
            odbc_exec($con, $query) or die(odbc_errormsg());
            $res = odbc_exec($con, "select top 1 id_fornecedores_despesas from sf_fornecedores_despesas order by id_fornecedores_despesas desc") or die(odbc_errormsg());
            $idCliente = odbc_result($res, 1);
        }
    }
    if ($idCliente !== "") {
        if (isset($_POST['txtQtdContatos'])) {
            $max = $_POST['txtQtdContatos'];
            for ($i = 1; $i < $max; $i++) {
                if (isset($_POST['txtIdContato_' . $i])) {
                    if ($_POST['txtIdContato_' . $i] !== "0") {
                        odbc_exec($con, "update sf_fornecedores_despesas_contatos set tipo_contato = " . $_POST['txtTpContato_' . $i] . ",conteudo_contato = " . valoresTexto('txtTxContato_' . $i) . " where id_contatos = " . $_POST['txtIdContato_' . $i]) or die(odbc_errormsg());
                    } else {
                        odbc_exec($con, "insert into sf_fornecedores_despesas_contatos(fornecedores_despesas,tipo_contato,conteudo_contato) values(" . $idCliente . "," . $_POST['txtTpContato_' . $i] . "," . valoresTexto('txtTxContato_' . $i) . ")") or die(odbc_errormsg());
                    }
                }
            }
        }
        $_POST['txtId'] = $idCliente;
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = " . $_POST['txtId']);
        odbc_exec($con, "DELETE FROM sf_fornecedores_despesas WHERE id_fornecedores_despesas = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso');parent.FecharBox()</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['Del_M'] != '') {
    $active1 = '';
    $active3 = 'active';
    odbc_exec($con, "DELETE FROM sf_fornecedor_despesas_bancos WHERE id_conta_fornecedor = " . $_GET['Del_M']);
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_fornecedores_despesas where id_fornecedores_despesas = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_fornecedores_despesas']);
        $razao = utf8_encode($RFP['razao_social']);
        $cnpj = utf8_encode($RFP['cnpj']);
        $insc = utf8_encode($RFP['inscricao_estadual']);
        $endereco = utf8_encode($RFP['endereco']);
        $numero = utf8_encode($RFP['numero']);
        $complemento = utf8_encode($RFP['complemento']);
        $bairro = utf8_encode($RFP['bairro']);
        $estado = utf8_encode($RFP['estado']);
        $cidade = utf8_encode($RFP['cidade']);
        $cep = utf8_encode($RFP['cep']);
        $contato = utf8_encode($RFP['contato']);
        $fantasia = utf8_encode($RFP['nome_fantasia']);
        $forgrupopess = utf8_encode($RFP['grupo_pessoa']);
        $data_cadastro = escreverData($RFP['dt_cadastro']);
        $regimetributario = utf8_encode($RFP['regime_tributario']);        
        $inativo = utf8_encode($RFP['inativo']);
    }
} else {
    $disabled = '';
    $id = '';
    $razao = '';
    $cnpj = '';
    $insc = '';
    $endereco = '';
    $numero = '';
    $complemento = '';
    $bairro = '';
    $estado = '';
    $cidade = '';
    $cep = '';
    $contato = '';
    $fantasia = '';
    $forgrupopess = '';
    $data_cadastro = '';
    $regimetributario = "";
    $inativo = '0';
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
if (isset($_POST['salvarBanco'])) {
    if ($_POST['txtbanco_banco'] != '' && $_POST['txtagencia_banco'] != '' && $_POST['txtconta_banco'] != '' && $_POST['txttipo_banco'] != '' && $id != '') {
        $active1 = '';
        $active3 = 'active';
        odbc_exec($con, "INSERT INTO sf_fornecedor_despesas_bancos(id_fornecedor_despesas,banco,agencia,conta,tipo) values(" . $id . ",'" . utf8_decode($_POST['txtbanco_banco']) . "','" . utf8_decode($_POST["txtagencia_banco"]) . "','" . utf8_decode($_POST["txtconta_banco"]) . "','" . utf8_decode($_POST["txttipo_banco"]) . "')");
    }
}
if (isset($_POST['bntSendFile'])) {
    $allowedExts = array("gif", "jpeg", "jpg", "png", "pdf", "GIF", "JPEG", "JPG", "PNG", "PDF");
    $extension = explode(".", $_FILES["file"]["name"]);
    $extension = $extension[count($extension) - 1];
    if ((($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/pjpeg") || ($_FILES["file"]["type"] == "image/x-png") || ($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "application/pdf")) && ($_FILES["file"]["size"] < 10000000) && in_array($extension, $allowedExts)) {
        if ($_FILES["file"]["error"] == 0) {
            move_uploaded_file($_FILES["file"]["tmp_name"], "./../../Pessoas/" . $contrato . "/Documentos/" . $id . "/" . str_replace(" ", "_", $_FILES["file"]["name"]));
        }
    } else {
        echo "<script type='text/javascript'>alert('Erro ao enviar o arquivo.\\nTamanho máximo permitido: 10 mb.\\nFormatos aceitos: gif, jpeg, jpg, png e pdf.');</script>";
    }
    $active1 = '';
    $active3 = '';
    $active6 = 'active';
}
if ($_GET['Del_F'] != '') {
    if (file_exists("./../../Pessoas/" . $contrato . "/Documentos/" . $id . "/" . $_GET['Del_F'])) {
        unlink("./../../Pessoas/" . $contrato . "/Documentos/" . $id . "/" . $_GET['Del_F']);
    } else {
        echo "<script type='text/javascript'>alert('Erro ao excluir o arquivo.\\nEste arquivo não existe em nosso sistema.');</script>";
    }
    $active1 = '';
    $active3 = '';
    $active6 = 'active';
}
$FinalUrl = "";
if (isset($_GET["idx"])) {
    $FinalUrl = $_GET["idx"];
}
if (isset($_POST["txtFinalUrl"])) {
    $FinalUrl = $_POST["txtFinalUrl"];
}

if ($id != '') {
    $cur = odbc_exec($con, "select sum(valor_pago) as valor_pago,sum(n_valor_pago) as qtd_pago from (
        select isnull(sum(valor_pago),0) valor_pago,isnull(count(valor_pago),0) n_valor_pago from sf_lancamento_movimento_parcelas lp
        inner join sf_lancamento_movimento lm on lp.id_lancamento_movimento = lm.id_lancamento_movimento
        inner join sf_contas_movimento c on c.id_contas_movimento = lm.conta_movimento
        where lm.status = 'Aprovado' and fornecedor_despesa = " . $id . " and lp.inativo = 0 and tipo = 'D' and data_pagamento is not null
        union
        select isnull(sum(valor_pago),0) valor_pago,isnull(count(valor_pago),0) n_valor_pago from sf_solicitacao_autorizacao_parcelas sp
        inner join sf_solicitacao_autorizacao sa on sp.solicitacao_autorizacao = sa.id_solicitacao_autorizacao
        inner join sf_contas_movimento c on c.id_contas_movimento = sa.conta_movimento
        where fornecedor_despesa = " . $id . " and sp.inativo = 0 and tipo = 'D' and data_pagamento is not null)as x");
    while ($RFP = odbc_fetch_array($cur)) {
        $q1_tc_recebidas = $RFP['valor_pago'];
        $q1_nc_recebidas = $RFP['qtd_pago'];
        if ($q1_nc_recebidas > 0) {
            $q2_med_recebido = $q1_tc_recebidas / $q1_nc_recebidas;
        }
    }
    $cur = odbc_exec($con, "select sum(valor_pago) as valor_pago,sum(n_valor_pago) as qtd_pago from (
        select isnull(sum(valor_parcela),0) valor_pago,isnull(count(valor_parcela),0) n_valor_pago from sf_lancamento_movimento_parcelas lp
        inner join sf_lancamento_movimento lm on lp.id_lancamento_movimento = lm.id_lancamento_movimento
        inner join sf_contas_movimento c on c.id_contas_movimento = lm.conta_movimento
        where lm.status = 'Aprovado' and fornecedor_despesa = " . $id . " and lp.inativo = 0 and tipo = 'D' and data_pagamento is null
        union
        select isnull(sum(valor_parcela),0) valor_pago,isnull(count(valor_parcela),0) n_valor_pago from sf_solicitacao_autorizacao_parcelas sp
        inner join sf_solicitacao_autorizacao sa on sp.solicitacao_autorizacao = sa.id_solicitacao_autorizacao
        inner join sf_contas_movimento c on c.id_contas_movimento = sa.conta_movimento
        where fornecedor_despesa = " . $id . " and sp.inativo = 0 and tipo = 'D' and data_pagamento is null
        )as x");
    while ($RFP = odbc_fetch_array($cur)) {
        $q1_tc_receber = $RFP['valor_pago'];
        $q1_nc_receber = $RFP['qtd_pago'];
        if ($q1_nc_receber > 0) {
            $q2_med_receber = $q1_tc_receber / $q1_nc_receber;
        }
    }
    $cur = odbc_exec($con, "select max(valor_pago) as ult_conta,min(n_valor_pago) as pri_conta from (
        select max(data_pagamento) valor_pago,min(data_pagamento) n_valor_pago from sf_lancamento_movimento_parcelas lp
        inner join sf_lancamento_movimento lm on lp.id_lancamento_movimento = lm.id_lancamento_movimento
        inner join sf_contas_movimento c on c.id_contas_movimento = lm.conta_movimento
        where lm.status = 'Aprovado' and fornecedor_despesa = " . $id . " and lp.inativo = 0 and tipo = 'D' and data_pagamento is not null
        union
        select max(data_pagamento) valor_pago,min(data_pagamento) n_valor_pago from sf_solicitacao_autorizacao_parcelas sp
        inner join sf_solicitacao_autorizacao sa on sp.solicitacao_autorizacao = sa.id_solicitacao_autorizacao
        inner join sf_contas_movimento c on c.id_contas_movimento = sa.conta_movimento
        where fornecedor_despesa = " . $id . " and sp.inativo = 0 and tipo = 'D' and data_pagamento is not null)as x");
    while ($RFP = odbc_fetch_array($cur)) {
        $q2_ult_conta = $RFP['ult_conta'];
        $q2_pri_conta = $RFP['pri_conta'];
    }
    $cur = odbc_exec($con, "select max(valor_pago) as ult_conta,min(n_valor_pago) as pri_conta from (
        select max(data_vencimento) valor_pago,min(data_vencimento) n_valor_pago from sf_lancamento_movimento_parcelas lp
        inner join sf_lancamento_movimento lm on lp.id_lancamento_movimento = lm.id_lancamento_movimento
        inner join sf_contas_movimento c on c.id_contas_movimento = lm.conta_movimento
        where lm.status = 'Aprovado' and fornecedor_despesa = " . $id . " and lp.inativo = 0 and tipo = 'D' and data_pagamento is null
        union
        select max(data_parcela) valor_pago,min(data_parcela) n_valor_pago from sf_solicitacao_autorizacao_parcelas sp
        inner join sf_solicitacao_autorizacao sa on sp.solicitacao_autorizacao = sa.id_solicitacao_autorizacao
        inner join sf_contas_movimento c on c.id_contas_movimento = sa.conta_movimento
        where fornecedor_despesa = " . $id . " and sp.inativo = 0 and tipo = 'D' and data_pagamento is null)as x");
    while ($RFP = odbc_fetch_array($cur)) {
        $q2_ult_venc = $RFP['ult_conta'];
        $q2_pro_venc = $RFP['pri_conta'];
    }
    $cur = odbc_exec($con, "select sum(valor_pago) as valor_pago,sum(n_valor_pago) as qtd_pago,SUM(dias) as dias from (
        select isnull(sum(valor_pago),0) valor_pago,isnull(count(valor_pago),0) n_valor_pago, SUM(DATEDIFF(DAY,data_vencimento,data_pagamento)) dias from sf_lancamento_movimento_parcelas lp
        inner join sf_lancamento_movimento lm on lp.id_lancamento_movimento = lm.id_lancamento_movimento
        inner join sf_contas_movimento c on c.id_contas_movimento = lm.conta_movimento
        where lm.status = 'Aprovado' and fornecedor_despesa = " . $id . " and lp.inativo = 0 and tipo = 'D' and data_pagamento is not null and data_vencimento < data_pagamento
        union
        select isnull(sum(valor_pago),0) valor_pago,isnull(count(valor_pago),0) n_valor_pago, SUM(DATEDIFF(DAY,data_parcela,data_pagamento)) dias from sf_solicitacao_autorizacao_parcelas sp
        inner join sf_solicitacao_autorizacao sa on sp.solicitacao_autorizacao = sa.id_solicitacao_autorizacao
        inner join sf_contas_movimento c on c.id_contas_movimento = sa.conta_movimento
        where fornecedor_despesa = " . $id . " and sp.inativo = 0 and tipo = 'D' and data_pagamento is not null and data_parcela < data_pagamento)as x");
    while ($RFP = odbc_fetch_array($cur)) {
        $q3_tot_atraso = $RFP['valor_pago'];
        $q3_qtd_atraso = $RFP['qtd_pago'];
        if ($RFP['qtd_pago'] > 0) {
            $q3_med_atraso = $RFP['dias'] / $RFP['qtd_pago'];
        }
    }
    $cur = odbc_exec($con, "select sum(valor_pago) as valor_pago,sum(n_valor_pago) as qtd_pago,SUM(dias) as dias from (
        select isnull(sum(valor_pago),0) valor_pago,isnull(count(valor_pago),0) n_valor_pago, SUM(DATEDIFF(DAY,data_pagamento,data_vencimento)) dias from sf_lancamento_movimento_parcelas lp
        inner join sf_lancamento_movimento lm on lp.id_lancamento_movimento = lm.id_lancamento_movimento
        inner join sf_contas_movimento c on c.id_contas_movimento = lm.conta_movimento
        where lm.status = 'Aprovado' and fornecedor_despesa = " . $id . " and lp.inativo = 0 and tipo = 'D' and data_pagamento is not null and data_vencimento > data_pagamento
        union
        select isnull(sum(valor_pago),0) valor_pago,isnull(count(valor_pago),0) n_valor_pago, SUM(DATEDIFF(DAY,data_pagamento,data_parcela)) dias from sf_solicitacao_autorizacao_parcelas sp
        inner join sf_solicitacao_autorizacao sa on sp.solicitacao_autorizacao = sa.id_solicitacao_autorizacao
        inner join sf_contas_movimento c on c.id_contas_movimento = sa.conta_movimento
        where fornecedor_despesa = " . $id . " and sp.inativo = 0 and tipo = 'D' and data_pagamento is not null and data_parcela > data_pagamento)as x");
    while ($RFP = odbc_fetch_array($cur)) {
        $q3_tot_antecipado = $RFP['valor_pago'];
        $q3_qtd_antecipado = $RFP['qtd_pago'];
        if ($RFP['qtd_pago'] > 0) {
            $q3_med_antecipado = $RFP['dias'] / $RFP['qtd_pago'];
        }
    }
}
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <title>Cadastro de Funcionário</title>
    <link rel="icon" type="image/ico" href="../../favicon.ico"/>
    <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
    <link href="../../css/main.css" rel="stylesheet">
    <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
    <link href="../../../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
    <link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
    <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />    
</head>
<body onLoad="document.frmEnviaDados.txtRazao.focus();">
    <div class="row-fluid">
        <div id="inline1" style="height:520px; overflow:hidden">
            <div class="block title">
                <div class="head">
                    <div class="frmhead">
                        <div class="frmtext">Fornecedores</div>
                        <div class="frmicon" onclick="parent.FecharBox(3)">
                            <span class="ico-remove"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="block">
                    <form action="FormFornecedores-de-despesas.php" name="frmEnviaDados" method="POST" enctype="multipart/form-data">
                        <div class="head">
                            <input name="txtFinalUrl" id="txtFinalUrl" value="<?php echo $FinalUrl; ?>" type="hidden"/>
                            <a href="#" onClick="source('tabs');return false;"><div class="icon"><span class="ico-info" style="color:#fff; margin:0; padding:0;"></span></div></a>
                        </div>
                        <div class="data-fluid tabbable" style="    overflow-x: hidden;">
                            <ul class="nav nav-tabs">
                                <li class="<?php echo $active1; ?>" style="margin-left:10px;" ><a href="#tab1" data-toggle="tab">Fornecedor</a></li>
                                <li><a href="#tab2" data-toggle="tab">Contatos</a></li>
                                <li class="<?php echo $active3; ?>"><a href="#tab3" data-toggle="tab">Dados Financeiros</a></li>
                                <li><a href="#tab4" data-toggle="tab">CRM</a></li>
                                <li><a href="#tab5" data-toggle="tab">Histórico Financeiro</a></li>
                                <li class="<?php echo $active6; ?>"><a href="#tab6" data-toggle="tab">Documentos</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane <?php echo $active1; ?>" id="tab1">
                                    <input name="txtQtdContatos" id="txtQtdContatos" value="<?php echo $linhaContato; ?>" type="hidden"/>
                                    <table width="680" border="0" cellpadding="3" cellspacing="0" style="margin-bottom:40px;" class="textosComuns">
                                        <tr>
                                            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                                            <td width="100" align="right">Nome/Razão Social:</td>
                                            <td colspan="5">
                                                <div id="sprytextfield1" style="width:374px; float:left" class=""><input name="txtRazao" id="txtRazao" type="text" class="input-medium" maxlength="128" <?php echo $disabled; ?> value="<?php echo $razao; ?>"/></div>
                                                <div style="width:75px; margin-right:5px; line-height:28px; float:left" align="right">Cadastrado:</div>
                                                <div style="width:106px; float:left"><input type="text" class="input-medium" value="<?php echo $data_cadastro; ?>" disabled /></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">Nome Fantasia:</td>
                                            <td colspan="5">
                                                <div style="width:374px; float:left" class=""><input name="txtFantasia" id="txtFantasia" type="text" class="input-medium" maxlength="128" <?php echo $disabled; ?> value="<?php echo $fantasia; ?>"/></div>
                                                <div style="width:75px; margin-right:5px; line-height:28px; float:left" align="right">Inativo:</div>
                                                <div style="width:106px; float:left">                                                    
                                                    <select name="txtInativo" id="txtInativo" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                                                        <option value="0" <?php echo ($inativo == "0" ? "SELECTED" : "");?>>Não</option>
                                                        <option value="1" <?php echo ($inativo == "1" ? "SELECTED" : "");?>>Sim</option>
                                                    </select>
                                                </div>                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100" align="right">CNPJ/CPF:</td>
                                            <td width="100"><span id="sprytextfield2">
                                                    <div>
                                                        <input id="txtCnpj" <?php echo $disabled; ?> name="txtCnpj" type="text" class="input-medium"  onkeypress='mascaraMutuario(this, cpfCnpj)' maxlength="18" value="<?php echo $cnpj; ?>"/>
                                                    </div>
                                                </span></td>
                                            <td width="64" align="right">Tipo de Contribuinte: </td>
                                            <td width="100"><span id="sprytextfield3">
                                                    <select name="txtRegTributario" id="txtRegTributario" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                        <option value="1" <?php
                                                        if ($regimetributario == 1) {
                                                            echo "SELECTED";
                                                        }
                                                        ?>>Contribuinte do ICMS</option>
                                                        <option value="2" <?php
                                                        if ($regimetributario == 2) {
                                                            echo "SELECTED";
                                                        }
                                                        ?>>Contribuinte ISENTO</option>
                                                        <option value="9" <?php
                                                        if ($regimetributario == 9) {
                                                            echo "SELECTED";
                                                        }
                                                        ?>>Não Contribuinte</option>
                                                    </select>
                                                </span></td>
                                            <td width="64" align="right">Insc. Est/RG: </td>
                                            <td width="100"><span id="sprytextfield3">
                                                    <input id="txtInsc" <?php echo $disabled; ?> name="txtInsc" type="text" class="input-medium" maxlength="25" value="<?php echo $insc; ?>"/>
                                                </span></td>
                                        </tr>
                                        <tr>
                                            <td align="right">Endereço:</td>
                                            <td colspan="6"><input name="txtEndereco" id="txtEndereco" <?php echo $disabled; ?> type="text" class="input-medium"  maxlength="256" value="<?php echo $endereco; ?>"/></td>
                                        </tr>
                                        <tr>
                                            <td align="right">Numero:</td>
                                            <td><span id="sprytextfield2">
                                                    <div>
                                                        <input id="txtNumero" <?php echo $disabled; ?> name="txtNumero" type="text" class="input-medium" maxlength="6" value="<?php echo $numero; ?>"/>
                                                    </div>
                                                </span></td>
                                            <td align="right">Compl.:</td>
                                            <td colspan="3"><span id="sprytextfield3">
                                                    <input id="txtComplemento" <?php echo $disabled; ?> name="txtComplemento" type="text" class="input-medium" maxlength="20" value="<?php echo $complemento; ?>"/>
                                                </span></td>
                                        </tr>
                                        <tr>
                                            <td align="right" >CEP:</td>
                                            <td>
                                                <div><span id="sprytextfield4">
                                                        <input id="txtCep" name="txtCep" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="9" value="<?php echo $cep; ?>"/>
                                                        <span class="textfieldInvalidFormatMsg">Formato inválido.</span></span></div>
                                            </td>                                            
                                            <td align="right">Bairro:</td>
                                            <td colspan="3"><span id="sprytextfield2">
                                                    <input id="txtBairro" name="txtBairro" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="20" value="<?php echo $bairro; ?>"/>
                                                </span></td>
                                        </tr>                                        
                                        <tr>
                                            <td align="right">Estado:</td>
                                            <td><span id="sprytextfield3">
                                                    <select name="txtEstado" id="txtEstado" class="input-medium" style="width:100%" <?php echo $disabled; ?> >
                                                        <option value="null" >Selecione o estado</option>
                                                        <?php
                                                        $cur = odbc_exec($con, "select estado_codigo,estado_nome from tb_estados where estado_codigo > 0 order by estado_nome") or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) {
                                                            ?>
                                                            <option value="<?php echo $RFP['estado_codigo'] ?>"<?php
                                                            if (!(strcmp($RFP['estado_codigo'], $estado))) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>><?php echo utf8_encode($RFP['estado_nome']) ?></option>
                                                                <?php } ?>
                                                    </select>
                                                </span></td>
                                            <td align="right">Cidade:</td>
                                            <td colspan="3"><span id="carregando" name="carregando" style="color:#666;display:none">Aguarde, carregando...</span>
                                                <select name="txtCidade" id="txtCidade" class="input-medium" style="width:100%" <?php echo $disabled; ?> >
                                                    <option value="null" >Selecione a cidade</option>
                                                    <?php
                                                    if ($estado != '') {
                                                        $sql = "select cidade_codigo,cidade_nome from tb_cidades where cidade_codigo > 0 AND cidade_codigoEstado = " . $estado . " ORDER BY cidade_nome";
                                                        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) {
                                                            ?>
                                                            <option value="<?php echo $RFP['cidade_codigo'] ?>"<?php
                                                            if (!(strcmp($RFP['cidade_codigo'], $cidade))) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>><?php echo utf8_encode($RFP['cidade_nome']) ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                </select>
                                                </span></td>                                                                                            
                                        </tr>                                        
                                        <tr>
                                            <td align="right">Contato:</td>
                                            <td><input id="txtContato" name="txtContato" <?php echo $disabled; ?> type="text"  class="input-medium" maxlength="128" value="<?php echo $contato; ?>"/></td>
                                            <td align="right" style="overflow:hidden;" class="textosComuns">Grupo:</td>
                                            <td colspan="3">
                                                <span id="spryselect1">
                                                    <select class="input-medium" style="width:100%" <?php echo $disabled; ?> name="txtGrupoPessoa" id="txtGrupoPessoa">
                                                        <option value="null" >Selecione o Grupo</option>
                                                        <?php
                                                        $cur = odbc_exec($con, "select * from sf_grupo_cliente where tipo_grupo = 'F' and inativo_grupo = 0 order by descricao_grupo") or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) {
                                                            ?>
                                                            <option value="<?php echo $RFP['id_grupo'] ?>"<?php
                                                            if (!(strcmp($RFP['id_grupo'], $forgrupopess))) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>><?php echo utf8_encode($RFP['descricao_grupo']) ?></option>
                                                                <?php } ?>
                                                    </select>
                                                </span>
                                            </td>
                                        </tr>
                                    </table>            
                                </div>
                                <div class="tab-pane <?php echo $active3; ?>" id="tab3">
                                    <table width="680" border="0" cellpadding="3" class="textosComuns">
                                        <tr>
                                            <td width="40" align="right">Banco:</td>
                                            <td width="130"><input id="txtbanco_banco" <?php echo $disabled; ?> name="txtbanco_banco" type="text" class="input-medium" maxlength="100" value=""/></td>
                                            <td width="50" align="right">Agência:</td>
                                            <td width="70"><input id="txtagencia_banco" <?php echo $disabled; ?> name="txtagencia_banco" type="text" class="input-medium" maxlength="100" value=""/></td>
                                            <td width="40" align="right">Conta:</td>
                                            <td width="100"><input id="txtconta_banco" <?php echo $disabled; ?> name="txtconta_banco" type="text" class="input-medium" maxlength="100" value=""/></td>
                                            <td width="30" align="right">Tipo:</td>
                                            <td>
                                                <select id="txttipo_banco" <?php echo $disabled; ?> name="txttipo_banco" class="input-medium" style="width:100%">
                                                    <option value="null">Selecione</option>
                                                    <option value="C">Corrente</option>
                                                    <option value="P">Poupança</option>
                                                </select>
                                            </td>
                                            <td align="right"><input class="btn btn-primary" id="salvarBanco" <?php
                                                if ($id == '') {
                                                    echo "disabled";
                                                } else {
                                                    echo $disabled;
                                                }
                                                ?> name="salvarBanco" type="submit" value="+" onClick="" style="width:30px; height:30px"/></td>
                                        </tr>
                                    </table>
                                    <table width="680">
                                        <div style="height:35px; width:200px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Banco</b></div>
                                        <div style="height:35px; width:100px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Agência</b></div>
                                        <div style="height:35px; width:150px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Conta</b></div>
                                        <div style="height:35px; width:149px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Tipo</b></div>
                                        <div style="height:35px; width:51px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Ação</b></div>
                                    </table>
                                    <div style="overflow:scroll; height:299px;">
                                        <table width="100%">
                                            <?php
                                            if ($id != '') {
                                                $cur = odbc_exec($con, "select * from sf_fornecedor_despesas_bancos where id_fornecedor_despesas = " . $id);
                                                while ($RFP = odbc_fetch_array($cur)) {
                                                    ?>
                                                    <div style="height:30px; width:205px; background:#FFF; line-height:35px; margin-left:1px; padding-left:3px; float:left;"><?php echo utf8_encode($RFP['banco']); ?></div>
                                                    <div style="height:30px; width:105px; background:#FFF; line-height:35px; margin-left:1px; padding-left:3px; float:left;"><?php echo utf8_encode($RFP['agencia']); ?></div>
                                                    <div style="height:30px; width:154px; background:#FFF; line-height:35px; margin-left:1px; padding-left:3px; float:left;"><?php echo utf8_encode($RFP['conta']); ?></div>
                                                    <div style="height:30px; width:153px; background:#FFF; line-height:35px; margin-left:1px; padding-left:3px; float:left;"><?php
                                                        if ($RFP['tipo'] == "C") {
                                                            echo "Conta Corrente";
                                                        } else {
                                                            echo "Conta Poupança";
                                                        }
                                                        ?></div>
                                                    <div style="height:23px; width:40px; background:#FFF; margin-left:1px; padding-top:7px; padding-left:3px; float:left">
                                                        <a onClick="return confirm('Deseja deletar esse registro?')" href='FormFornecedores-de-despesas.php?id=<?php echo $PegaURL; ?>&Del_M=<?php echo $RFP['id_conta_fornecedor']; ?>' ><img src="../../img/1365123843_onebit_33 copy.PNG" width='16' height='16'/></a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab4">
                                    <table width="100%">
                                        <div style="height:35px; width:60px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Pendente</b></div>
                                        <div style="height:35px; width:90px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Data</b></div>
                                        <div style="height:35px; width:95px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Atendente</b></div>
                                        <div style="height:35px; width:95px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Para</b></div>
                                        <div style="height:35px; width:60px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Próximo</b></div>
                                        <div style="height:35px; width:241px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Observação</b></div>
                                    </table>
                                    <div style="overflow:scroll; height:338px;">
                                        <table width="100%">
                                            <?php
                                            if ($id != '') {
                                                $cur = odbc_exec($con, "select id_tele,u.login_user as de_pessoa,u2.login_user as para_pessoa,data_tele,pendente,proximo_contato,obs
                                                                            from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa
                                                                            LEFT JOIN sf_usuarios u2 on u2.id_usuario = t.para_pessoa
                                                                            where t.pessoa = " . $id . " order by data_tele desc");
                                                while ($RFP = odbc_fetch_array($cur)) {
                                                    $backColor = "";
                                                    if ($RFP['pendente'] == 1) {
                                                        $backColor = " color:red;";
                                                    } else {
                                                        $backColor = "";
                                                    }
                                                    ?>
                                                    <div style="height:30px; width:65px; background:#FFF; line-height:35px; margin-left:1px; padding-left:3px; float:left;<?php echo $backColor; ?>"><center><?php
                                                            if ($RFP['pendente'] == 0) {
                                                                echo utf8_encode("NÃO");
                                                            } else {
                                                                echo "SIM";
                                                            }
                                                            ?></center></div>
                                                    <div style="height:30px; width:95px; background:#FFF; line-height:35px; margin-left:1px; padding-left:3px; float:left;<?php echo $backColor; ?>"><?php echo escreverData($RFP['data_tele']); ?></div>
                                                    <div style="height:30px; width:100px; background:#FFF; line-height:35px; margin-left:1px; padding-left:3px; float:left;<?php echo $backColor; ?>"><?php echo utf8_encode($RFP['de_pessoa']); ?></div>
                                                    <div style="height:30px; width:100px; background:#FFF; line-height:35px; margin-left:1px; padding-left:3px; float:left;<?php echo $backColor; ?>"><?php echo utf8_encode($RFP['para_pessoa']); ?></div>
                                                    <div style="height:30px; width:65px; background:#FFF; line-height:35px; margin-left:1px; padding-left:3px; float:left;<?php echo $backColor; ?>"><?php echo escreverData($RFP['proximo_contato']); ?></div>
                                                    <div style="height:30px; width:230px; background:#FFF; line-height:35px; margin-left:1px; padding-left:3px; float:left;<?php echo $backColor; ?>"><?php echo utf8_encode($RFP['obs']); ?></div>
                                                    <div style="clear:both"></div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab5">
                                    <div style="margin-top:10px;height:360px;">                                        
                                        <table>
                                            <tr style="vertical-align:top">
                                                <td style="width: 20px"></td>
                                                <td style="width:320px">
                                                    <table class="" border="1" bordercolor="#FFFFFF" cellpadding="3" cellspacing="2" style="overflow:auto;width: 300px;">
                                                        <thead>
                                                            <tr class="txtPq">
                                                                <td colspan="2" bgcolor="#e9e9e9"><center>Total Contas Pagas</center></td>
                                            </tr>
                                            </thead>
                                            <tbody class="txtPq">
                                                <tr>
                                                    <td width="60%">Total contas pagas</td>
                                                    <td width="40%" style="text-align: right"><?php echo escreverNumero($q1_tc_recebidas, 1); ?></td>
                                                </tr>
                                                <tr>
                                                    <td width="60%">Num. contas pagas</td>
                                                    <td width="40%" style="text-align: right"><?php echo $q1_nc_recebidas; ?></td>
                                                </tr>
                                                <tr>
                                                    <td width="60%">Total contas a pagar</td>
                                                    <td width="40%" style="text-align: right"><?php echo escreverNumero($q1_tc_receber, 1); ?></td> </tr>
                                                <tr>
                                                    <td width="60%">Num. contas a pagar</td>
                                                    <td width="40%" style="text-align: right"><?php echo $q1_nc_receber; ?></td>
                                                </tr>

                                                </tr>

                                            </tbody>
                                        </table>
                                        </td>
                                        <td>
                                            <table class="" border="1" bordercolor="#FFFFFF" cellpadding="3" cellspacing="2" style="overflow:auto;width: 320px;">
                                                <thead>
                                                    <tr class="txtPq">
                                                        <td colspan="2" bgcolor="#e9e9e9"><center>Relatório Contas Pagas</center></td>
                                        </tr>
                                        </thead>
                                        <tbody class="txtPq">
                                            <tr>
                                                <td width="45">Última conta paga</td>
                                                <td width="65" style="text-align: right"><?php echo escreverData($q2_ult_conta); ?></td>
                                            </tr>
                                            <tr>
                                                <td width="45%">Prim. conta paga</td>
                                                <td width="55%" style="text-align: right"><?php echo escreverData($q2_pri_conta); ?></td>
                                            </tr>
                                            <tr>
                                                <td width="45%">Próximo venc.</td>
                                                <td width="55%" style="text-align: right"><?php echo escreverData($q2_pro_venc); ?></td>
                                            </tr>
                                            <tr>
                                                <td width="45%">Último venc.</td>
                                                <td width="55%" style="text-align: right"><?php echo escreverData($q2_ult_venc); ?></td>
                                            </tr>
                                            <tr>
                                                <td width="45%">Valor médio parc. paga</td>
                                                <td width="55%" style="text-align: right"><?php echo escreverNumero($q2_med_recebido, 1); ?></td>
                                            </tr>
                                            <tr>
                                                <td width="45%">Valor médio parc. a pagar</td>
                                                <td width="55%" style="text-align: right"><?php echo escreverNumero($q2_med_receber, 1); ?></td>

                                            <tr>
                                                <td>
                                                <td>
                                            <tr>
                                                <td>
                                                <td>
                                            <tr>
                                                <td>
                                                <td>
                                            </tr>
                                        </tbody>
                                        </table>
                                        </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20px"></td>
                                            <td>
                                                <table class="" border="1" bordercolor="#FFFFFF" cellpadding="3" cellspacing="2" style="overflow:auto;width: 300px;">
                                                    <thead>
                                                        <tr class="txtPq">
                                                            <td colspan="2" bgcolor="#e9e9e9"><center>Contas Pagas em Atraso</center></td>
                                        </tr>
                                        </thead>
                                        <tbody class="txtPq">
                                            <tr>
                                                <td width="80%">Total contas pagas em atraso</td>
                                                <td width="20%" style="text-align: right"><?php echo escreverNumero($q3_tot_atraso, 1); ?></td>
                                            </tr>
                                            <tr>
                                                <td width="80%">N. contas pagas em atraso</td>
                                                <td width="20%" style="text-align: right"><?php echo $q3_qtd_atraso; ?></td>
                                            </tr>
                                            <tr>
                                                <td width="80%">Num. médio de dias pgmento em atraso</td>
                                                <td width="20%" style="text-align: right"><?php echo $q3_med_atraso; ?></td>
                                            </tr>
                                        </tbody>
                                        </table>
                                        </td>
                                        <td>
                                            <table class="" border="1" bordercolor="#FFFFFF" cellpadding="3" cellspacing="2" style="overflow:auto;width: 320px;">
                                                <thead>
                                                    <tr class="txtPq">
                                                        <td colspan="2" bgcolor="#e9e9e9"><center>Contas Pagas Antecipadas</center></td>
                                        </tr>
                                        </thead>
                                        <tbody class="txtPq">
                                            <tr>
                                                <td width="80%">Total contas pagas antecipadas</td>
                                                <td width="20%" style="text-align: right"><?php echo escreverNumero($q3_tot_antecipado, 1); ?></td>
                                            </tr>
                                            <tr>
                                                <td width="80%">Num. contas pagas antecipadas</td>
                                                <td width="20%" style="text-align: right"><?php echo $q3_qtd_antecipado; ?></td>
                                            </tr>
                                            <tr>
                                                <td width="80%">Média de dias contas pagas antecipadas</td>
                                                <td width="20%" style="text-align: right"><?php echo $q3_med_antecipado; ?></td>
                                            </tr>
                                        </tbody>
                                        </table>
                                        </td>
                                        </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane <?php echo $active6; ?>" id="tab6">
                                    <table width="680" border="0" cellpadding="3" class="textosComuns">
                                        <tr>
                                            <td width="122" height="37" align="right"> Envio de Arquivos: </td>
                                            <td colspan="3"><input <?php echo $disabled; ?>  type="file" name="file" id="file" style="font-size:11px; height:23px; margin-top:4px"/></td>
                                            <td width="100" align="right"><?php if ($id != '') { ?>
                                                    <button class="btn btn-success" <?php echo $disabled; ?> type="submit" name="bntSendFile"><span class="ico-checkmark"></span> Enviar</button>
                                                <?php } ?></td>
                                        </tr>
                                    </table>
                                    <div style="height:35px; width:160px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Data de Upload</b></div>
                                    <div style="height:35px; width:370px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Nome do Arquivo</b></div>
                                    <div style="height:35px; width:78px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Tamanho</b></div>
                                    <div style="height:35px; width:51px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Ação</b></div>
                                    <div style="clear:both"></div>
                                    <div style="overflow:scroll; height:301px">
                                        <?php
                                        if ($id != "") {
                                            $pasta = "./../../Pessoas/" . $contrato . "/Documentos/" . $id . "/";
                                            if (file_exists($pasta)) {
                                                $diretorio = dir($pasta);
                                                while ($arquivo = $diretorio->read()) {
                                                    if ($arquivo != "." && $arquivo != "..") {
                                                        ?>
                                                        <div style="height:30px; width:165px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo date($lang['dmy'] . " H:i:s", filectime($pasta . $arquivo)); ?></div>
                                                        <div style="height:30px; width:375px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo "<a href=\"" . $pasta . $arquivo . "\" target=\"_blank\">" . $arquivo; ?></a></div>
                                                        <div style="height:30px; width:83px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo filesize($pasta . $arquivo) . " bytes"; ?></div>
                                                        <div style="height:23px; width:39px; background:#FFF; margin-left:1px; padding-top:7px; padding-left:3px; float:left">
                                                            <a onClick="return confirm('Deseja deletar esse registro?')" href="FormFornecedores-de-despesas.php?id=<?php echo $PegaURL; ?>&Del_F=<?php echo $arquivo; ?>"><img src="../../img/1365123843_onebit_33 copy.PNG" width="16" height="16"/></a>
                                                        </div>
                                                        <div style="clear:both"></div>
                                                        <?php
                                                    }
                                                }
                                                $diretorio->close();
                                            } else {
                                                mkdir($pasta, 0777);
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab2">
                                    <div class="toolbar dark" style="background-color: #DCDCDC">
                                        <div class="input-prepend input-append">
                                        </div>
                                        <table width="99%" border="0" cellpadding="3" class="textosComuns">
                                            <tr>
                                                <td>
                                                    <select id="txtTipoContato" name="txtTipoContato" onChange="alterarMask()" <?php echo $disabled; ?>>
                                                        <option value="0">Telefone</option>
                                                        <option value="1">Celular</option>
                                                        <option value="2">E-mail</option>
                                                        <option value="3">Site</option>
                                                        <option value="4">Outros</option>
                                                    </select>
                                                </td>
                                                <td id="sprytextfield10">
                                                    <input id="txtTextoContato" type="text" <?php echo $disabled; ?>/>
                                                </td>
                                                <td width="80" align="right">
                                                    <button type="button" onclick="addContato();" <?php echo $disabled; ?> class="btn dblue">Incluir <span class="icon-arrow-next icon-white"></span></button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="data dark npr npb" style="overflow:hidden">
                                        <div id="divContatos" class="messages" style="height:312px; overflow-y:scroll"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="spanBNT" style="width:739px; border-top:solid 1px #CCC; overflow:hidden; margin:0; padding:0; bottom:1px; position:absolute">
                            <div class="toolbar bottom tar">
                                <div class="data-fluid" style="overflow:hidden; margin-top:10px; margin-right:10px;" >
                                    <div class="btn-group">
                                        <?php if ($disabled == '') { ?>
                                            <button class="btn btn-success" type="submit" title="Gravar" onclick="return Validar();" name="bntSave" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                                            <?php if ($_POST['txtId'] == '') { ?>
                                                <button class="btn btn-success" title="Cancelar" onClick="parent.FecharBox(3)" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                                            <?php } else { ?>
                                                <button class="btn btn-success" type="submit" title="Cancelar" name="bntAlterar" id="bntOK" ><span class="ico-reply"></span> Cancelar</button>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <button class="btn  btn-success" type="submit" title="Novo" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"> </span>  Novo</button>
                                            <button class="btn  btn-success" type="submit" title="Alterar" name="bntEdit" id="bntOK" value="Alterar"> <span class="ico-edit"> </span>  Alterar</button>
                                            <button class="btn red" type="submit" title="Excluir" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span>  Excluir</button>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="block"></div>
            </div>
        </div>
    </div>    
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/charts.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>    
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script> 
    <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
    <script type="text/javascript" src="../../../SpryAssets/SpryValidationSelect.js"></script>    
    <script type="text/javascript" src="../../js/util.js"></script>   
    <script type="text/javascript">
        var idLinhaContato = $("#txtQtdContatos").val();
        function Validar() {
            if($("#txtRazao").val() === "") {
                bootbox.alert("O campo nome é obrigatório!");
                return false;
            }            
            var original = $("#txtCnpj").val().replace(".", "").replace("-", "").replace("/", "").replace(".", "");
            if (original === "")
            {
                return true;
            }
            if (((original.length === 11) && (original === 11111111111) || (original === 22222222222) || (original === 33333333333) || (original === 44444444444) || (original === 55555555555) || (original === 66666666666) || (original === 77777777777) || (original === 88888888888) || (original === 99999999999) || (original === 00000000000)))
            {
                bootbox.alert("CPF/CNPJ inválido.");
                $("#txtCnpj").focus();
                return false;
            }
            if (!((original.length === 11) || (original.length === 14)))
            {
                bootbox.alert("CPF/CNPJ inválido.");
                $("#txtCnpj").focus();
                return false;
            }
            var checkOK = "0123456789";
            var checkStr = original;
            var allValid = true;
            var allNum = "";
            for (i = 0; i < checkStr.length; i++)
            {
                ch = checkStr.charAt(i);
                for (j = 0; j < checkOK.length; j++)
                    if (ch === checkOK.charAt(j))
                        break;
                if (j === checkOK.length)
                {
                    allValid = false;
                    break;
                }
                allNum += ch;
            }
            if (!allValid)
            {
                bootbox.alert("Favor preencher somente com dígitos o campo CPF/CNPJ.");
                $("#txtCnpj").focus();
                return false;
            }

            var chkVal = allNum;
            var prsVal = parseFloat(allNum);
            if (chkVal !== "" && !(prsVal > "0"))
            {
                bootbox.alert("CPF zerado !");
                $("#txtCnpj").focus();
                return false;
            }

            if (original.length === 11)
            {
                var tot = 0;
                for (i = 2; i <= 10; i++)
                    tot += i * parseInt(checkStr.charAt(10 - i));
                if ((tot * 10 % 11 % 10) !== parseInt(checkStr.charAt(9)))
                {
                    bootbox.alert("CPF/CNPJ inválido.");
                    $("#txtCnpj").focus();
                    return false;
                }
                tot = 0;
                for (i = 2; i <= 11; i++) {
                    tot += i * parseInt(checkStr.charAt(11 - i));
                }
                if ((tot * 10 % 11 % 10) !== parseInt(checkStr.charAt(10)))
                {
                    bootbox.alert("CPF/CNPJ inválido.");
                    $("#txtCnpj").focus();
                    return false;
                }
            } else {
                var tot = 0;
                var peso = 2;
                for (i = 0; i <= 11; i++)
                {
                    tot += peso * parseInt(checkStr.charAt(11 - i));
                    peso++;
                    if (peso === 10)
                    {
                        peso = 2;
                    }
                }
                if ((tot * 10 % 11 % 10) !== parseInt(checkStr.charAt(12)))
                {
                    bootbox.alert("CPF/CNPJ inválido.");
                    $("#txtCnpj").focus();
                    return false;
                }
                tot = 0;
                peso = 2;
                for (i = 0; i <= 12; i++)
                {
                    tot += peso * parseInt(checkStr.charAt(12 - i));
                    peso++;
                    if (peso === 10)
                    {
                        peso = 2;
                    }
                }
                if ((tot * 10 % 11 % 10) !== parseInt(checkStr.charAt(13)))
                {
                    bootbox.alert("CPF/CNPJ inválido.");
                    $("#txtCnpj").focus();
                    return false;
                }
            }
            return true;
        }
        function mascaraMutuario(o, f) {
            v_obj = o;
            v_fun = f;
            setTimeout('execmascara()', 1);
        }
        function execmascara() {
            v_obj.value = v_fun(v_obj.value);
        }
        function cpfCnpj(v) {
            v = v.replace(/\D/g, "");
            if (v.length <= 11) { //CPF
                v = v.replace(/(\d{3})(\d)/, "$1.$2");
                v = v.replace(/(\d{3})(\d)/, "$1.$2");
                v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
            } else { //CNPJ
                v = v.replace(/^(\d{2})(\d)/, "$1.$2");
                v = v.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
                v = v.replace(/\.(\d{3})(\d)/, ".$1/$2");
                v = v.replace(/(\d{4})(\d)/, "$1-$2");
            }
            return v;
        }
        function alterarMask() {
            $('#txtTextoContato').val("");
            var tipo = $("#txtTipoContato option:selected").val();
            if (sprytextfield10 !== null) {
                sprytextfield10.reset();
                sprytextfield10.destroy();
                sprytextfield10 = null;
            }
            if (tipo === "0") {
                sprytextfield10 = new Spry.Widget.ValidationTextField("sprytextfield10", "custom", {pattern: "(00) 0000-0000", useCharacterMasking: true, isRequired: false});
            } else if (tipo === "1") {
                sprytextfield10 = new Spry.Widget.ValidationTextField("sprytextfield10", "custom", {pattern: "(00) 00000-0000", useCharacterMasking: true, isRequired: false});
            }
        }
        function addContato() {
            var msg = $("#txtTextoContato").val();
            var sel = $("#txtTipoContato").val();
            if (msg !== "") {
                var arb = msg + "@";
                if (sel !== 2 || (sel === 2 && arb.match(/@/g).length === 2)) {
                    addLinhaContato($("#txtTipoContato").val(), $("#txtTipoContato option:selected").text(), $("#txtTextoContato").val(), "0");
                } else {
                    bootbox.alert("Preencha um endereço de e-mail corretamente!");
                }
            } else {
                bootbox.alert("Preencha as informações de contato corretamente!");
            }
        }
        function addLinhaContato(idTipo, tipo, conteudo, id) {
            var btExcluir = "";
            if ($("#txtFantasia").attr("disabled") === "disabled") {
                btExcluir = '<div style="width:5%; padding:4px 0 3px; float:right"></div>';
            } else {
                btExcluir = '<div style="width:5%; padding:4px 0 3px; float:right" onClick="return removeLinha(\'tabela_linha_' + idLinhaContato + '\');"><span class="ico-remove" style="font-size:20px"></span></div>';
            }
            var linha = "<div id=\"tabela_linha_" + idLinhaContato + "\" style=\"padding:5px; border-bottom:1px solid #DDD\">" +
                    "<input id=\"txtIdContato_" + idLinhaContato + "\" name=\"txtIdContato_" + idLinhaContato + "\" value=\"" + id + "\" type=\"hidden\"></input>" +
                    "<input id=\"txtTpContato_" + idLinhaContato + "\" name=\"txtTpContato_" + idLinhaContato + "\" value=\"" + idTipo + "\" type=\"hidden\"></input>" +
                    "<div style=\"width:32%; padding-left:1%; line-height:27px; float:left\">" + tipo + "</div>" +
                    "<input id=\"txtTxContato_" + idLinhaContato + "\" name=\"txtTxContato_" + idLinhaContato + "\" value=\"" + conteudo + "\" type=\"hidden\"></input>" +
                    btExcluir + "<div style=\"width:60%; line-height:27px; float:left\">" + conteudo + "</div></div>";
            $("#divContatos").prepend(linha);
            idLinhaContato++;
            $("#txtQtdContatos").val(idLinhaContato);
        }
        function removeLinha(id) {
            $("#" + id).remove();
        }
        function listContatos() {
            $.getJSON('../Comercial/FormGerenciador-Clientes.contatos.ajax.php?search=', {cod: $("#txtId").val() !== "" ? $("#txtId").val() : "0", ajax: 'true'}, function (j) {
                for (var i = 0; i < j.length; i++) {
                    addLinhaContato(j[i].tipo_contato, j[i].texto_contatos, j[i].conteudo_contato, j[i].id);
                }
            });
        }
        $(function () {
            $('#txtEstado').change(function () {
                if ($(this).val()) {
                    $('#txtCidade').hide();
                    $('#carregando').show();
                    $.getJSON('locais.ajax.php?search=', {txtEstado: $(this).val(), ajax: 'true'}, function (j) {
                        var options = '<option value=""></option>';
                        for (var i = 0; i < j.length; i++) {
                            options += '<option value="' + j[i].cidade_codigo + '">' + j[i].cidade_nome + '</option>';
                        }
                        $('#txtCidade').html(options).show();
                        $('#carregando').hide();
                    });
                } else {
                    $('#txtCidade').html('<option value="">Selecione a cidade</option>');
                }
            });
        });
        $(document).ready(function () {
            listContatos();
        });
        var sprytextfield10 = new Spry.Widget.ValidationTextField("sprytextfield10", "custom", {pattern: "(00) 0000-0000", useCharacterMasking: true, isRequired: false});
    </script>
    <?php odbc_close($con); ?>
</body>
