<?php
include './../../Connections/configini.php';

$toReturn = "";
if ($_GET["tp"] != "") {
    $toReturn = "?tp=" . $_GET["tp"];
}

$disabled = "disabled";
if (isset($_POST['bntSave'])) {
    if ($_POST["txtId"] != "") {
        odbc_exec($con, "update sf_contas_movimento set " .
        "tipo = " . valoresSelectTexto("txtTipo") . "," .
        "descricao = " . valoresTexto("txtDescricao") . "," .
        "multa = " . valoresNumericos("txtMulta") . "," .
        "juros = " . valoresNumericos("txtJuros") . "," .
        "dias_tolerancia = " . valoresNumericos("txtTolerancia") . "," .
        "grupo_conta = " . valoresSelect("txtGrupo") . "," .
        "inativa = " . valoresSelect("txtInativa") . 
        " where id_contas_movimento = " . $_POST["txtId"]) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_contas_movimento(tipo,descricao,grupo_conta,multa,juros,dias_tolerancia,inativa)values(" .
        valoresSelectTexto("txtTipo") . "," .
        valoresTexto("txtDescricao") . "," .
        valoresSelect("txtGrupo") . "," .
        valoresNumericos("txtMulta") . "," .
        valoresNumericos("txtJuros") . "," .
        valoresNumericos("txtTolerancia") . "," .                
        valoresSelect("txtInativa") . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_contas_movimento from sf_contas_movimento order by id_contas_movimento desc") or die(odbc_errormsg());
        $_POST["txtId"] = odbc_result($res, 1);
    }
}
if (isset($_POST["bntDelete"])) {
    if ($_POST["txtId"] != "") {
        odbc_exec($con, "DELETE FROM sf_contas_movimento WHERE id_contas_movimento = " . $_POST["txtId"]);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}
if (isset($_POST["bntNew"])) {
    $_GET["id"] = "";
    $_POST["txtId"] = "";
}
if ($_GET["id"] != "" || $_POST["txtId"] != "") {
    $PegaURL = $_GET["id"];
    if ($_POST["txtId"] != "") {
        $disabled = "disabled";
        $PegaURL = $_POST["txtId"];
    }
    $cur = odbc_exec($con, "select * from sf_contas_movimento where id_contas_movimento =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP["id_contas_movimento"];
        $tipo = $RFP["tipo"];
        $descricao = utf8_encode($RFP["descricao"]);
        $grupo = $RFP["grupo_conta"];
        $multa = escreverNumero($RFP["multa"]);
        $juros = escreverNumero($RFP["juros"]);
        $dias_tolerancia = $RFP["dias_tolerancia"];
        $inativa = $RFP["inativa"];
    }
} else {
    $disabled = "";
    $id = "";
    $tipo = "C";
    $descricao = "";
    $grupo = "";
    $multa = escreverNumero(0);
    $juros = escreverNumero(0);
    $dias_tolerancia = "0";
    $inativa = "0";
}
if (isset($_POST["bntEdit"])) {
    if ($_POST["txtId"] != "") {
        $disabled = "";
    }
}
$FinalUrl = "";
if (isset($_GET["idx"])) {
    $FinalUrl = $_GET["idx"];
}
if (isset($_POST["txtFinalUrl"])) {
    $FinalUrl = $_POST["txtFinalUrl"];
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<link href="../../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<script src="../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="../../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <form action="FormContas-de-Movimentos.php<?php echo $toReturn; ?>" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Subgrupos de Contas</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont" style="height: 165px;">
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <input name="txtFinalUrl" id="txtFinalUrl" value="<?php echo $FinalUrl; ?>" type="hidden"/>            
            <table border="0" style="width: 100%" cellpadding="3" class="textosComuns">
                <tr>
                    <td style="width: 55px;" align="right">Tipo:</td>
                    <td colspan="3"><select style="width:100%" class="input-medium" <?php echo $disabled; ?> name="txtTipo" id="txtTipo">
                            <option value="C" <?php
                            if (!(strcmp('C', $tipo))) {
                                echo "SELECTED";
                            }
                            ?>>Crédito</option>
                            <option value="D" <?php
                            if (!(strcmp('D', $tipo))) {
                                echo "SELECTED";
                            }
                            ?>>Débito</option>
                        </select></td>
                </tr>
                <tr>
                    <td align="right">Descrição:</td>
                    <td colspan="3"><input name="txtDescricao" id="txtDescricao" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $descricao; ?>"/></td>
                </tr>                                
                <tr>
                    <td align="right">Grupo:</td>
                    <td colspan="3"><span id="carregando" name="carregando" style="color:#656; display:none">Aguarde, carregando...</span>
                        <select style="width:100%" class="input-medium" <?php echo $disabled; ?> name="txtGrupo" id="txtGrupo">
                            <?php
                            if ($tipo != '') {                                
                                $sql = "select id_grupo_contas, gc.descricao, cc.descricao c_descricao 
                                from sf_grupo_contas gc left join sf_centro_custo cc on cc.id = gc.centro_custo
                                where deb_cre = '" . $tipo . "' and inativo = 0 ORDER BY gc.descricao";
                                $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) {
                                    ?>
                                    <option value="<?php echo $RFP['id_grupo_contas'] ?>"<?php
                                    if (!(strcmp($RFP['id_grupo_contas'], $grupo))) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP['descricao']) . (strlen(utf8_encode($RFP['c_descricao'])) > 0 ? " - " . utf8_encode($RFP['c_descricao']) : ""); ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                        </select>                            
                    </td>
                </tr>     
                <tr>
                    <td align="right">Estado:</td>
                    <td colspan="3">
                        <select name="txtInativa" id="txtInativa" class="select" style="width:41%" <?php echo $disabled; ?>>
                            <option value="0" <?php
                            if ($inativa == "0") {
                                echo "SELECTED";
                            }
                            ?>>Ativo</option>
                            <option value="1" <?php
                            if ($inativa == "1") {
                                echo "SELECTED";
                            }
                            ?>>Inativo</option>
                        </select>
                    </td>
                </tr> 
                <tr id="txtMul" <?php echo ($tipo == 'C' ? "style='display:none'" : "");?>>
                    <td align="right">Multa %:</td>
                    <td><input type="text" name="txtMulta" <?php echo $disabled; ?> class="input-medium" value="<?php echo $multa; ?>" id="txtMulta"></td>
                    <td align="right">Juros AD %:</td>
                    <td><input type="text" name="txtJuros" <?php echo $disabled; ?> class="input-medium" value="<?php echo $juros; ?>" id="txtJuros"></td>
                </tr>
                <tr id="txtTol" <?php echo ($tipo == 'D' ? "style='display:none'" : "");?>>
                    <td align="right">Tolerância:</td>
                    <td><input type="text" name="txtTolerancia" <?php echo $disabled; ?> maxlength="3" class="input-medium" value="<?php echo $dias_tolerancia; ?>" id="txtTolerancia"></td>
                    <td>em dias.</td>
                    <td></td>
                </tr>                
            </table>            
        </div>    
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" name="bntSave" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK" ><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else { ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                <?php } ?>                                
            </div>
        </div>                             
    </form> 
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/charts.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>
    <script type="text/javascript">          
        
        $("#txtMulta, #txtJuros").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"], suffix: ""});
        
        $('#txtTipo').change(function () {
            if ($(this).val()) {
                $('#txtGrupo').hide();
                $('#carregando').show();
                $.getJSON('grupo.ajax.php?search=', {txtTipo: $(this).val(), ajax: 'true'}, function (j) {
                    var options = '<option value="null"></option>';
                    for (var i = 0; i < j.length; i++) {
                        options += '<option value="' + j[i].id_grupo_contas + '">' + j[i].descricao + '</option>';
                    }
                    $('#txtGrupo').html(options).show();
                    $('#carregando').hide();
                });
                if ($(this).val() === 'D') {
                    $('#txtMul').show();
                    $('#txtTol').hide();
                } else {
                    $('#txtMul').hide();
                    $('#txtTol').show();
                }
            } else {
                $('#txtGrupo').html('<option value="">Selecione o grupo</option>');
            }
        });
    </script>    
    <?php odbc_close($con); ?>
</body>
