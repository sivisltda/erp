<?php
include './../../Connections/configini.php';
$imprimir = 0;
$F1 = "";
$F2 = "";
$F3 = 0;
$F4 = 0;
$DateBegin = getData("T");
$DateEnd = getData("T");

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = 1;
}
if (is_numeric($_GET['ps'])) {
    $F1 = $_GET['ps'];
}
if (is_numeric($_GET['tp'])) {
    $F2 = $_GET['tp'];
}
if (is_numeric($_GET['ts'])) {
    $F3 = $_GET['ts'];
}
if (is_numeric($_GET['or'])) {
    $F4 = $_GET['or'];
}
if ($_GET['dti'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
}
if ($_GET['dtf'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
}

if (is_numeric($_GET['emp'])) {
    $empresa = $_GET['emp'];
}
$contatoName = "";
$idCliente = "";
$whereCli = "";
$tipo = "";

if (is_numeric($_GET["usu"])) {
    $tipo = "U";
    $idCliente = $_GET["usu"];
    $cur = odbc_exec($con, "select id_usuario,login_user,nome from sf_usuarios where inativo = 0 and (id_usuario = " . $_GET["usu"] . ") order by nome") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $contatoName = $RFP["nome"];
        $user = $RFP["login_user"];
    }
}

if (is_numeric($_GET["fab"])) {
    $tipo = "B";
    $idCliente = $_GET["fab"];
    $cur = odbc_exec($con, "select id_fabricante, nome_fabricante from sf_fabricantes where id_fabricante = " . $_GET["fab"]) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $contatoName = $RFP["nome_fabricante"];
    }
}

if (is_numeric($_GET["cli"])) {
    $tipo = "C";
    $idCliente = $_GET['cli'];
    $whereCli = " and sf_vendas.cliente_venda = " . $_GET['cli'];
    $cur = odbc_exec($con, "select id_fornecedores_despesas,tipo,razao_social,nome_fantasia from dbo.sf_fornecedores_despesas where id_fornecedores_despesas = " . $_GET["cli"]) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $contatoName = $RFP["razao_social"];
        if ($RFP["nome_fantasia"] !== "") {
            $contatoName = $contatoName . " (" . $RFP["nome_fantasia"] . ")";
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>       
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body >
        <?php if ($imprimir == 0) { ?>
            <div id="loader"><img src="../../img/loader.gif"/></div>
        <?php } ?>
        <div class="wrapper">
            <?php
            if ($imprimir == 0) {
                include("../../menuLateral.php");
            }?>
                <div <?php
                if ($imprimir == 0) {
                    echo "class=\"body\"";
                }
                ?>>
                    <?php
                    if ($imprimir == 0) {
                        include("../../top.php");
                    }
                    ?>
                <div class="content">
                    <?php if ($imprimir == 0) { ?>
                        <div class="page-header">
                            <div class="icon"> <span class="ico-arrow-right"></span> </div>
                            <h1>Financeiro<small>Relatório de Movimentação de Caixa</small></h1>
                        </div>
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="boxfilter block">
                                    <div style="float: left;">
                                        <span >Loja:</span>
                                        <select name="txtEmpresa" id="txtEmpresa">
                                            <?php $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo utf8_encode($RFP['id_filial']); ?>" <?php
                                                if ($empresa == "") {
                                                    $empresa = $RFP['id_filial'];
                                                }
                                                if ($empresa == $RFP['id_filial']) {
                                                    echo "SELECTED";
                                                }
                                                ?>><?php echo utf8_encode($RFP['numero_filial']); ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div style="margin-left: 1%;float: left;width: 93px;">
                                        <span>Movimentação:</span>
                                        <select name="txtTipoB" id="txtTipoB">
                                            <option value="" >Selecione</option>
                                            <option value="0" <?php
                                            if ($F2 == "0") {
                                                echo "SELECTED";
                                            }
                                            ?>>Crédito
                                            </option>
                                            <option value="1" <?php
                                            if ($F2 == "1") {
                                                echo "SELECTED";
                                            }
                                            ?>>Débito
                                            </option>
                                        </select>
                                    </div>
                                    <div style="margin-left: 1%;float: left;width: 160px;">
                                        <span>Relatório:</span>
                                        <select name="txtTipoC" id="txtTipoC" >
                                            <option value="0" <?php
                                            if ($F3 == "0") {
                                                echo "SELECTED";
                                            }
                                            ?>>Sintético</option>
                                            <option value="1" <?php
                                            if ($F3 == "1") {
                                                echo "SELECTED";
                                            }
                                            ?>>Analítico de Vendas</option>
                                            <option value="2" <?php
                                            if ($F3 == "2") {
                                                echo "SELECTED";
                                            }
                                            ?>>Analítico de Funcionários</option>
                                        </select>
                                    </div>
                                    <div style="margin-left: 1%;float: left;width: 120px;">
                                        <span>Filtrar:</span>
                                        <select name="txtPenCon" id="txtPenCon">
                                            <option value="" >Selecione</option>
                                            <option value="U" <?php
                                            if ($tipo == "U") {
                                                echo "SELECTED";
                                            }
                                            ?>>Usuário</option>
                                            <option value="C" <?php
                                            if ($tipo == "C") {
                                                echo "SELECTED";
                                            }
                                            ?>>Cliente</option>
                                            <option value="B" <?php
                                            if ($tipo == "B") {
                                                echo "SELECTED";
                                            }
                                            ?>>Fabricante</option>
                                        </select>
                                    </div>
                                    <div style="margin-left: 1%;float: left;width: 173px;">
                                        <span>Nome:</span>
                                        <input name="txtValue" id="txtValue" type="hidden" value="<?php echo $idCliente; ?>"/>
                                        <input name="txtDesc" id="txtDesc" type="text" style="width:174px; height:31px" size="10" value="<?php echo $contatoName; ?>"/>
                                    </div>
                                    <div style="margin-left: 1%;float: left;width: 95px;">
                                        <span>Itens:</span>
                                        <select name="txtTipoP" id="txtTipoP" >
                                            <option value="null" >Selecione</option>
                                            <option value="0" <?php
                                            if ($F1 == "0") {
                                                echo "SELECTED";
                                            }
                                            ?>>Produtos</option>
                                            <option value="1" <?php
                                            if ($F1 == "1") {
                                                echo "SELECTED";
                                            }
                                            ?>>Serviços</option>
                                        </select>
                                    </div>
                                    <div style="margin-left: 1%;float: left;width: 80px;">
                                        <span>Ordenar</span>
                                        <select name="txtTipoP" id="txtTipoP" >
                                            <option value="0" <?php
                                            if ($F4 == "0") {
                                                echo "SELECTED";
                                            }
                                            ?>>Data</option>
                                            <option value="1" <?php
                                            if ($F4 == "1") {
                                                echo "SELECTED";
                                            }
                                            ?>>Código</option>
                                        </select>
                                    </div>
                                    <div style="margin-left: 1%;float: left;width: 170px;">
                                        <span style="float: left;width: 98px;">Período</span>
                                        <div style="float: left;">
                                            <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial">
                                        </div>
                                        <div style="float: left;margin-left: 9px;">
                                            <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final">
                                        </div>
                                    </div>
                                    <div style="margin-top: 15px;float:left;margin-left:0.3%;">
                                        <div style="float:left">
                                            <button name="btnfind" id="btnfind" class="button button-turquoise btn-primary" type="button" onclick="AbrirBox(0)"  title="Buscar"><span class="ico-search icon-white"></span></button>
                                        </div>
                                    </div>

                                    <div style="margin-top: 15px;float:left;">
                                        <div style="float:left">
                                            <button name="btnPrint" class="button button-blue btn-primary" type="button" onclick="AbrirBox(1)" title="Imprimir"><span class="ico-print icon-white"></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="row-fluid">                    
                    <div style="clear:both"></div>
                    <?php if ($imprimir == 0) { ?>
                        <div class="boxhead" <?php echo $visible; ?>>
                            <div class="boxtext">Movimentação de Caixa</div>
                        </div>
                        <div class="boxtable">
                                <div class="data-fluid">
                                    <?php }
                                    $totCred = 0;
                                    $totDebt = 0;
                                    if ($imprimir == 1) {
                                        $titulo_pagina = "RELATÓRIO DE MOVIMENTO DE CAIXA " . $user . " - Loja " . $empresa;
                                        include "Cabecalho-Impressao.php";
                                    }
                                    $count = 0;
                                    if (is_numeric($F2)) {
                                        $count = 1;
                                    }
                                    for ($i = $count; $i < 2; $i++) {
                                        if (($count == 1 && $F2 == 0) || $i == 0) {
                                            $texto = "CRÉDITO";
                                            $tp1 = "C";
                                            $tp2 = "V";
                                        } else {
                                            $texto = "DÉBITO";
                                            $tp1 = "D";
                                            $tp2 = "C";
                                        }
                                        ?>
                                        <table width="100%" <?php
                                        if ($imprimir == 0) {
                                            echo "class=\"table\"";
                                        } else {
                                            echo "border=\"1\"";
                                        }
                                        ?> style="margin-bottom:15px">
                                            <tr>
                                                <th width="5%">
                                            <center><?php echo $tp1; ?></center>
                                            </th>
                                            <th style="text-align:left; padding-left:8px"><?php echo $texto . " " . $usuarioSelecionado; ?></th>
                                            <th width="20%" colspan="2"><center>VALOR</center></th>
                                            </tr>
                                            <tbody>
                                                <?php
                                                $total = 0;
                                                $whereX1 = "";
                                                $whereX2 = "";
                                                $whereX3 = "";
                                                if ($user != "") {
                                                    $where = " AND UPPER(vendedor) = UPPER('" . $user . "') ";
                                                }
                                                if (is_numeric($empresa)) {
                                                    $whereX1 = " and sf_lancamento_movimento.empresa = " . $empresa;
                                                    $whereX2 = " and sf_solicitacao_autorizacao.empresa = " . $empresa;
                                                    $whereX3 = " and sf_vendas.empresa = " . $empresa;
                                                }
                                                if (is_numeric($idCliente)) {
                                                    if ($tipo == "C") { //Cliente
                                                        $whereX1 .= " and sf_lancamento_movimento.fornecedor_despesa = " . $idCliente;
                                                        $whereX2 .= " and sf_solicitacao_autorizacao.fornecedor_despesa = " . $idCliente;
                                                        $whereX3 .= " and sf_vendas.cliente_venda = " . $idCliente;
                                                    } else if ($tipo == "B") { //Fabricante
                                                        $whereX1 .= " and sf_lancamento_movimento_parcelas.id_lancamento_movimento in (select id_lancamento_movimento from sf_lancamento_movimento_itens inner join sf_produtos on sf_lancamento_movimento_itens.produto = sf_produtos.conta_produto where cod_fabricante = " . $idCliente . ")";
                                                        $whereX3 .= " and sf_vendas.id_venda in (select id_venda from sf_vendas_itens inner join sf_produtos on sf_vendas_itens.produto = sf_produtos.conta_produto where cod_fabricante = " . $idCliente . ")";
                                                    }
                                                }

                                            //traz só o total
                                            $query = "set dateformat dmy;select x.descricao,sum(x.valor_pago) valor_pago from (select sf_tipo_documento.descricao,sum(valor_pago) valor_pago from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento
                                            on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
                                            inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
                                            inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas
                                            left join sf_tipo_documento on sf_lancamento_movimento_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento
                                            inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas
                                            where sf_lancamento_movimento.status = 'Aprovado' and sf_contas_movimento.tipo = '" . $tp1 . "' " . $whereX1 . " AND valor_pago > 0 AND data_pagamento >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND data_pagamento <= " . valoresDataHora2($DateEnd, "23:59:59") . "
                                            AND sf_lancamento_movimento_parcelas.inativo = '0' group by sf_tipo_documento.descricao union all                                            
                                            select sf_tipo_documento.descricao,sum(valor_parcela) valor_pago from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
                                            on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
                                            inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
                                            inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas
                                            left join sf_tipo_documento on sf_solicitacao_autorizacao_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento
                                            inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas
                                            where status = 'Aprovado' and sf_contas_movimento.tipo = '" . $tp1 . "' " . $whereX2 . " AND valor_parcela > 0 AND data_lanc >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND data_lanc <= " . valoresDataHora2($DateEnd, "23:59:59") . "
                                            AND sf_solicitacao_autorizacao_parcelas.inativo = '0' group by sf_tipo_documento.descricao union all                                            
                                            select sf_tipo_documento.descricao,sum(valor_parcela) valor_pago from sf_venda_parcelas inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda
                                            left join sf_tipo_documento on sf_venda_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento
                                            inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas
                                            where status = 'Aprovado' " . $where . "and sf_vendas.cov = '" . $tp2 . "' " . $whereX3 . " AND valor_parcela > 0 AND data_venda >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND data_venda <= " . valoresDataHora2($DateEnd, "23:59:59") . "
                                            AND sf_venda_parcelas.inativo = '0' group by sf_tipo_documento.descricao) as x group by x.descricao order by x.descricao";
                                            
                                            //echo $query;exit();
                                            $cur2 = odbc_exec($con, $query);
                                            while ($RFP = odbc_fetch_array($cur2)) {
                                                if ($tp1 == "C") {
                                                    $totCred = $totCred + $RFP['valor_pago'];
                                                } elseif ($tp1 == "D") {
                                                    $totDebt = $totDebt + $RFP['valor_pago'];
                                                }
                                                $total = $total + $RFP ['valor_pago'];
                                                ?>
                                                <tr id="formPQ">
                                                    <td colspan="2" style="padding:2px; padding-left:8px"><?php echo utf8_encode($RFP['descricao']); ?></td>
                                                    <td style="padding:2px; text-align:right"><?php echo escreverNumero($RFP['valor_pago'], 1); ?></td>
                                                </tr>
                                                <?php
                                            } ?>
                                            </tbody>
                                            <tr>
                                                <th width="5%">
                                            <center><?php echo $tp1; ?></center>
                                            </th>
                                            <th style="text-align:left; padding-left:8px"> TOTAL</th>
                                            <th style="padding:2px; text-align:right"><?php echo escreverNumero($total, 1); ?></th>
                                            </tr>
                                        </table>
                                        <?php if ($F3 == "1") { ?>
                                            <table width="100%" <?php echo ($imprimir == 0 ? "class=\"table\"" : "border=\"1\"");?> style="margin-bottom:15px">
                                                <tr>
                                                    <th colspan="4" style="text-align:left; padding-left:8px">
                                                        <div style="float:left;">DETALHES DE MOVIMENTAÇÃO DE <?php
                                                            echo $texto . "</div>";
                                                            $a = 0;
                                                            $orderby = "";
                                                            if ($F4 == 0) {
                                                                $orderby = " order by data_venda";
                                                            } else if ($F4 == 1) {
                                                                $orderby = " order by cod_pedido";
                                                            }
                                                            $query = "set dateformat dmy;select id_venda id,cod_pedido,id_fornecedores_despesas,razao_social,nome_fantasia,data_venda, n_produtos, n_servicos from sf_vendas
                                                            inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas
                                                            where status = 'Aprovado' " . $where . "and sf_vendas.cov = '" . $tp2 . "' " . $whereX3 . " AND data_venda >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND data_venda <= " . valoresDataHora2($DateEnd, "23:59:59") . $whereCli . $orderby;
                                                            $cur3 = odbc_exec($con, $query);
                                                            while ($RFP = odbc_fetch_array($cur3)) {
                                                                $ID_[$a] = $RFP["id"];
                                                                $CODIGO_[$a] = $RFP["cod_pedido"];
                                                                $Descricao_[$a] = $RFP["id_fornecedores_despesas"] . " - " . utf8_encode($RFP["razao_social"]);
                                                                if ($RFP["nome_fantasia"] != "") {
                                                                    $Descricao_[$a] = $Descricao_[$a] . " (" . utf8_encode($RFP["nome_fantasia"]) . ")";
                                                                }
                                                                $DATA_[$a] = escreverDataHora($RFP["data_venda"]);
                                                                $n_produtos[$a] = $RFP["n_produtos"];
                                                                $n_servicos[$a] = $RFP["n_servicos"];
                                                                $a++;
                                                            } ?>
                                                            <span style="float:right">Vendas: <?php echo $a; ?></span>
                                                    </th>
                                                </tr>
                                                <?php
                                                for ($j = 0; $j < $a; $j++) {
                                                    $id_item = "";
                                                    ?>
                                                    <tr>
                                                        <td style="width:20%"><strong><?php echo $ID_[$j] . "/" . $CODIGO_[$j]; ?></strong></td>
                                                        <td style="border-left:hidden" colspan="2"><strong><?php echo $Descricao_[$j]; ?></strong></td>
                                                        <td style="width:20%; text-align:right"><strong><?php echo $DATA_[$j]; ?></strong></td>
                                                    </tr>
                                                    <?php
                                                    if (is_numeric($ID_[$j])) {
                                                        $wherePS = "";
                                                        if ($F1 == "0") {
                                                            $wherePS = " AND sf_produtos.tipo = 'P' ";
                                                        } elseif ($F1 == "1") {
                                                            $wherePS = " AND sf_produtos.tipo = 'S' ";
                                                        }
                                                        $whereFab = "";
                                                        if (is_numeric($idCliente) && $tipo == "B") {
                                                            $whereFab .= " and cod_fabricante = " . $idCliente;
                                                        }
                                                        $cur3 = odbc_exec($con, "select sf_produtos.conta_produto, sf_produtos.descricao,vendedor_comissao,valor_total,razao_social, sf_produtos.tipo  from sf_vendas_itens
                                                        inner join sf_produtos on sf_produtos.conta_produto = sf_vendas_itens.produto
                                                        left join sf_fornecedores_despesas on sf_fornecedores_despesas.id_fornecedores_despesas = sf_vendas_itens.vendedor_comissao
                                                        where id_venda = " . $ID_[$j] . $wherePS . $whereFab . " order by 1");
                                                        while ($RFP = odbc_fetch_array($cur3)) {
                                                            $id_item = $RFP["conta_produto"];
                                                            ?>
                                                            <tr style="line-height: 30px;">
                                                                <td colspan="2"><?php echo "   * " . utf8_encode($RFP["descricao"]); ?></td>
                                                                <td style="width:20%; border-left:hidden"><?php echo utf8_encode($RFP["razao_social"]); ?></td>
                                                                <?php
                                                                $valorAtual = escreverNumero($RFP['valor_total'], 1);
                                                                if (($RFP["tipo"] == "P" && $n_produtos[$j] == "1") || ($RFP["tipo"] == "S" && $n_servicos[$j] == "1")) {
                                                                    $valorAtual = escreverNumero(0, 1);
                                                                } ?>
                                                                <td style="width:20%; padding:2px; text-align:right"><?php echo $valorAtual; ?></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        if ($id_item !== "") {
                                                            $cur3 = odbc_exec($con, "select sf_tipo_documento.descricao,data_parcela,valor_parcela valor_pago from sf_venda_parcelas
                                                            inner join sf_tipo_documento on sf_tipo_documento.id_tipo_documento = sf_venda_parcelas.tipo_documento where venda = " . $ID_[$j] . " order by 1");
                                                            while ($RFP = odbc_fetch_array($cur3)) { ?>
                                                                <tr>
                                                                    <td colspan="2"><?php echo " >>" . utf8_encode($RFP["descricao"]); ?></td>
                                                                    <td style="border-left:hidden"><?php echo "BP " . escreverData($RFP["data_parcela"]); ?></td>
                                                                    <td style="padding:2px; text-align:right"><?php echo escreverNumero($RFP['valor_pago'], 1); ?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        } ?>
                                                        <tr id="formPQ">
                                                            <td style="border-left:hidden; border-right:hidden" colspan="4"></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                } ?>
                                            </table>
                                        <?php } elseif ($F3 == "2") { ?>
                                            <table width="100%" <?php echo ($imprimir == 0 ? "class=\"table\"" : "border=\"1\"");?> style="margin-bottom:15px">
                                                <tr>
                                                    <th colspan="6" style="text-align:left; padding-left:8px">
                                                        DETALHES DE MOVIMENTAÇÃO DE <?php
                                                        echo $texto;
                                                        $a = 0;
                                                        $cur3 = odbc_exec($con, "set dateformat dmy;select distinct isnull(vendedor_comissao,0) vendedor_comissao,isnull(razao_social,'SEM COMISSÃO') razao_social
                                                        from sf_vendas inner join sf_vendas_itens on sf_vendas.id_venda = sf_vendas_itens.id_venda left join sf_fornecedores_despesas on sf_fornecedores_despesas.id_fornecedores_despesas = sf_vendas_itens.vendedor_comissao
                                                        where status = 'Aprovado' " . $where . " and sf_vendas.cov = '" . $tp2 . "' " . $whereX3 . " AND data_venda >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND data_venda <= " . valoresDataHora2($DateEnd, "23:59:59") . $whereCli . " order by razao_social");
                                                        while ($RFP = odbc_fetch_array($cur3)) {
                                                            $ID_[$a] = $RFP["vendedor_comissao"];
                                                            $Descricao_[$a] = utf8_encode($RFP["razao_social"]);
                                                            $a++;
                                                        } ?>
                                                        <span style="float:right"></span>
                                                    </th>
                                                </tr>
                                                <?php for ($j = 0; $j < $a; $j++) { ?>
                                                    <tr id="formPQ">
                                                        <td style="width:10%;"><strong></strong></td>
                                                        <td style="border-left: hidden;" colspan="5"><strong><?php echo $Descricao_[$j]; ?></strong></td>
                                                    </tr>
                                                    <?php
                                                    if (is_numeric($ID_[$j])) {
                                                        $subTotal = 0;
                                                        $wherePS = " and vendedor_comissao = " . $ID_[$j];
                                                        if ($ID_[$j] == 0) {
                                                            $wherePS = " and vendedor_comissao is null ";
                                                        }
                                                        if ($F1 == "0") {
                                                            $wherePS .= " AND sf_produtos.tipo = 'P' ";
                                                        } elseif ($F1 == "1") {
                                                            $wherePS .= " AND sf_produtos.tipo = 'S' ";
                                                        }
                                                        
                                                        
                                                        $cur3 = odbc_exec($con, "set dateformat dmy;select sf_vendas.id_venda id,cod_pedido,data_venda,c.razao_social,descricao,quantidade,valor_total
                                                        from sf_vendas inner join sf_vendas_itens on sf_vendas.id_venda = sf_vendas_itens.id_venda
                                                        left join sf_fornecedores_despesas e on e.id_fornecedores_despesas = sf_vendas_itens.vendedor_comissao
                                                        inner join sf_fornecedores_despesas c on c.id_fornecedores_despesas = sf_vendas.cliente_venda
                                                        inner join sf_produtos on sf_produtos.conta_produto = sf_vendas_itens.produto
                                                        where status = 'Aprovado' " . $where . $whereFab . " and sf_vendas.cov = '" . $tp2 . "' " . $whereX3 . " AND data_venda >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND data_venda <= " . valoresDataHora2($DateEnd, "23:59:59") . $wherePS . $whereCli . " order by data_venda");
                                                        while ($RFP = odbc_fetch_array($cur3)) { ?>
                                                            <tr>
                                                                <td style="width:10%;"><?php echo utf8_encode($RFP["id"]) . "/" . utf8_encode($RFP["cod_pedido"]); ?></td>
                                                                <td style="width:15%;"><?php echo escreverDataHora($RFP["data_venda"]); ?></td>
                                                                <td><?php echo utf8_encode($RFP["razao_social"]); ?></td>
                                                                <td><?php echo utf8_encode($RFP["descricao"]); ?></td>
                                                                <td><?php echo $RFP['quantidade']; ?></td>
                                                                <td style="width:15%; padding:2px; text-align:right"><?php
                                                                    $subTotal = $subTotal + $RFP['valor_total'];
                                                                    echo escreverNumero($RFP['valor_total'], 1);
                                                                    ?></td>
                                                            </tr>
                                                        <?php } ?>
                                                        <tr id="formPQ">
                                                            <td style="border-left:hidden; border-right:hidden" colspan="6">
                                                                <span style="float: right;font-weight: bold; padding-right: 16px;"><?php echo escreverNumero($subTotal, 1); ?></span>
                                                                <span style="float: right;font-weight: bold; padding-right: 100px;">Sub Total:</span>
                                                            </td>
                                                            <td style="border-left:hidden; border-right:hidden" colspan="6"></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                } ?>
                                            </table>
                                            <?php
                                        }
                                        if ($F3 == "1" && $tipo === "") {
                                            $minimo = "0";
                                            $maximo = "0";
                                            $comandas = "";
                                            $tcomandas = 0;
                                            $cur3 = odbc_exec($con, "set dateformat dmy;select max(cod_pedido) maximo ,min(cod_pedido) minimo from sf_vendas
                                            inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas
                                            where status = 'Aprovado' " . $where . "and sf_vendas.cov = '" . $tp2 . "' " . $whereX3 . " AND data_venda >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND data_venda <= " . valoresDataHora2($DateEnd, "23:59:59") . " ");
                                            while ($RFP = odbc_fetch_array($cur3)) {
                                                $minimo = $RFP['minimo'];
                                                $maximo = $RFP['maximo'];
                                            }
                                            if (is_numeric($minimo) && $minimo > 0 && is_numeric($maximo) && $maximo > 0 && $maximo > $minimo && ($maximo - $minimo) < 100) {
                                                for ($i = $minimo; $i <= $maximo; $i++) {
                                                    $key = array_search($i, $CODIGO_);
                                                    if (!is_numeric($key)) {
                                                        $comandas .= " | " . $i;
                                                        $tcomandas++;
                                                    }
                                                }
                                            }
                                            if ($comandas != "") { ?>
                                                <table width="100%" <?php
                                                if ($imprimir == 0) {
                                                    echo "class=\"table\"";
                                                } else {
                                                    echo "border=\"1\"";
                                                }
                                                ?> style="margin-bottom:15px">
                                                    <tr>
                                                        <th width="15%">
                                                    <center>CÓDIGOS FALTANTES</center>
                                                    </th>
                                                    <th style="text-align:left; padding-left:8px">
                                                        <?php echo $comandas . " |"; ?>
                                                    </th>
                                                    <th style="padding:2px;text-align:right;" width="10%" colspan="2">Total: <?php echo $tcomandas; ?></th>
                                                    </tr>
                                                </table>
                                                <?php
                                            }
                                        }
                                    } ?>
                                    <table width="100%" <?php echo ($imprimir == 0 ? "class=\"table\"" : "border=\"1\"");?> style="margin-bottom:15px">
                                           <?php if ($F2 == 0 || $F2 == "") { ?>
                                            <tr>
                                                <th width="5%">
                                            <center>C</center>
                                            </th>
                                            <th style="text-align:left; padding-left:8px">
                                                SOMATÓRIO TOTAL EM CRÉDITO
                                            </th>
                                            <th style="padding:2px; text-align:right" width="20%" colspan="2"><?php echo escreverNumero($totCred, 1); ?></th>
                                            </tr>
                                        <?php } if ($F2 == 1 || $F2 == "") { ?>
                                            <tr>
                                                <th width="5%">
                                            <center>D</center>
                                            </th>
                                            <th style="text-align:left; padding-left:8px">
                                                SOMATÓRIO TOTAL EM DÉBITO
                                            </th>
                                            <th style="padding:2px; text-align:right" width="20%" colspan="2"><?php echo escreverNumero($totDebt, 1); ?></th>
                                            </tr>
                                        <?php } if (is_numeric($F2) == false) { ?>
                                            <tr>
                                                <th width="5%">
                                            <center>T</center>
                                            </th>
                                            <th style="text-align:left; padding-left:8px">
                                                DIFERENÇA
                                            </th>
                                            <th style="padding:2px; text-align:right" width="20%" colspan="2"><?php echo escreverNumero(($totCred - $totDebt), 1); ?></th>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                <?php if ($imprimir == 1) { ?>
                                </div>
                            <div class="widgets"></div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/uniform/jquery.uniform.min.js'></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            
            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
            
            $(document).ready(function () {
                
                $("#txtPenCon").change(function (event) {
                    $("#txtDesc").val("");
                });
                
                $(function () {
                    $("#txtDesc").autocomplete({
                        source: function (request, response) {
                            $.ajax({
                                url: "./../CRM/ajax.php",
                                dataType: "json",
                                data: {
                                    q: request.term,
                                    t: $("#txtPenCon").val()
                                },
                                success: function (data) {
                                    response(data);
                                }
                            });
                        },
                        minLength: 3,
                        select: function (event, ui) {
                            $("#txtValue").val(ui.item.id);
                        }
                    });
                });
            });

            function AbrirBox(id) {
                var retPrint = "";
                if ($('#txtTipoB').val() !== "") {
                    retPrint = retPrint + "&tp=" + $('#txtTipoB').val();
                }
                if ($('#txtTipoC').val() !== "") {
                    retPrint = retPrint + "&ts=" + $('#txtTipoC').val();
                }
                if ($('#txtTipoP').val() !== "") {
                    retPrint = retPrint + "&ps=" + $('#txtTipoP').val();
                }
                if ($('#txtOrder').val() !== "") {
                    retPrint = retPrint + "&or=" + $('#txtOrder').val();
                }
                if ($('#txt_dt_begin').val() !== "") {
                    retPrint = retPrint + "&dti=" + $('#txt_dt_begin').val().replace(/\//g, "_");
                }
                if ($('#txt_dt_end').val() !== "") {
                    retPrint = retPrint + "&dtf=" + $('#txt_dt_end').val().replace(/\//g, "_");
                }
                if ($('#txtEmpresa').val() !== "") {
                    retPrint = retPrint + "&emp=" + $('#txtEmpresa').val();
                }
                if ($("#txtDesc").val() !== "" && $("#txtValue").val() !== "") {
                    if ($("#txtPenCon").val() === "B") {
                        retPrint = retPrint + "&fab=" + $("#txtValue").val();
                    } else if ($("#txtPenCon").val() === "U") {
                        retPrint = retPrint + "&usu=" + $("#txtValue").val();
                    } else {
                        retPrint = retPrint + "&cli=" + $("#txtValue").val();
                    }
                }
                if (id === 0) {
                    window.location = "Relatorio-Movimento-Caixa.php?imp=0" + retPrint;
                } else if (id === 1) {
                    window.location = "Relatorio-Movimento-Caixa.php?imp=1" + retPrint;
                }
            }
        </script>        
    </body>
    <?php if ($imprimir == 1) { ?>
        <script type="text/javascript">
            $(window).load(function () {
                window.print();
                window.onafterprint = back;
                function back() {
                    window.history.back();
                }
            });
        </script>
        <?php
    } odbc_close($con); ?>
</html>
