<?php
include './../../Connections/configini.php';
$imprimir = 0;

if (isset($_POST['btnPrint'])) {
    $imprimir = 1;
}

if ($_GET['Del'] != '') {
    odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_mensagens_fornecedores WHERE id_fornecedores_despesas = " . $_GET['Del']) or die(odbc_errormsg());
    odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_endereco_entrega WHERE fornecedores_despesas = " . $_GET['Del']) or die(odbc_errormsg());
    odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = " . $_GET['Del']) or die(odbc_errormsg());
    odbc_exec($con, "DELETE FROM sf_fornecedores_despesas WHERE id_fornecedores_despesas = " . $_GET['Del']) or die(odbc_errormsg());
    echo "<script>window.top.location.href = 'Funcionarios.php'; </script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body>
        <?php if ($imprimir == 0) { ?>
            <div id="loader"><img src="../../img/loader.gif"/></div>
        <?php } ?>
        <div class="wrapper">
            <?php
            if ($imprimir == 0) {
                include('../../menuLateral.php');
            }
            ?>
            <div class="body">
                <?php
                if ($imprimir == 0) {
                    include("../../top.php");
                }
                ?>
                <div class="content">
                    <?php
                    if ($imprimir !== 0) {
                        $visible = 'hidden';
                    }
                    ?>
                    <div class="page-header" <?php echo $visible; ?>>
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Administrativo<small>Funcionários</small></h1>
                    </div>
                    <div class="row-fluid" <?php echo $visible; ?>>
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="margin-top: 15px;float:left;">
                                    <div style="float:left">
                                        <form method="POST">
                                            <?php if ($ckb_adm_cli_read_ == 0) { ?>
                                                <button id="btnNovo" class="button button-green btn-primary" type="button" onclick="AbrirBox(1, 0)"><span class="ico-file-4 icon-white"></span></button>
                                            <?php } ?>
                                            <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="AbrirBox(4, 0)" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                        </form>
                                    </div>
                                </div>
                                <div style="float:right;width: 40%;display: flex;align-items: center;justify-content: flex-end;">
                                <?php
                                    $total = 0;
                                    $cur = odbc_exec($con, "select count(id_filial) total from sf_filiais where ativo = 0");
                                    while ($RFP = odbc_fetch_array($cur)) {
                                        $total = $RFP['total'];
                                    }
                                    if ($total > 1) { ?>
                                        <div style="float:left; margin-right: 1%;">
                                            <label>Selecione a Loja: </label>          
                                            <select class="select" id="txtFilial" name="txtFilial" style="width:175px;float: left;">
                                                <option value="null">Selecione</option>
                                                <?php $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                                    <option value="<?php echo utf8_encode($RFP['id_filial']); ?>"><?php echo utf8_encode($RFP['numero_filial']) . " - " . utf8_encode($RFP['Descricao']); ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>                                                                        
                                    <?php } ?>                                    
                                    <div style="float:left;margin-left: 1%;">                                    
                                        <div width="100%">Estado:</div>
                                        <select id="txtStatus" class="select">
                                            <option value="0" selected>Ativos</option>                                        
                                            <option value="1">Inativos</option>                                                                            
                                        </select>                                           
                                    </div>                                                                           
                                    <div style="float:left;margin-left: 1%;width: 56%;">
                                        <div width="100%">Busca:</div>
                                        <input id="txtBusca" maxlength="100" type="text" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title=""/>
                                    </div>
                                    <div style="float:left;margin-left: 1%;width: 7%;">
                                        <div width="100%" style="opacity:0;">Busca:</div>
                                        <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar" onclick="refresh()"><span class="ico-search icon-white"></span></button>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead" <?php echo $visible; ?>>
                        <div class="boxtext">Funcionários</div>
                    </div>
                    <div <?php
                    if ($imprimir == 0) {
                        echo "class=\"boxtable\"";
                    }
                    ?>>
                            <?php
                            if ($imprimir == 1) {
                                $titulo_pagina = "RELATÓRIO DE CONSULTA<br/>FUNCIONÁRIOS";
                                include "../Financeiro/Cabecalho-Impressao.php";
                            }
                            ?>
                        <table <?php
                        if ($imprimir == 0) {
                            echo "class=\"table\"";
                        } else {
                            echo "border=\"1\"";
                        }
                        ?> cellpadding="0" cellspacing="0" width="100%" id="tbFuncionarios">
                            <thead>
                                <tr id="formPQ">
                                    <th width="3%">Cod:</th>
                                    <th width="8%">CPF:</th>
                                    <th width="17%">Nome completo:</th>
                                    <th width="9%">RG: </th>
                                    <th width="8%">Cidade:</th>
                                    <th width="3%"><center>UF:</center></th>
                                    <th width="8%">Telefone:</th>
                                    <th width="13%">E-mail:</th>
                                    <th width="7%">Dt. Demissão</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="5" class="dataTables_empty">Carregando dados do Funcionários</td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                    <div class="widgets"></div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script> 
        <script type="text/javascript" src="../../js/util.js"></script>                
        <script type="text/javascript">

            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }
            function refresh() {
                var oTable = $('#tbFuncionarios').dataTable();
                oTable.fnReloadAjax(finalURL(0));
            }
            function finalURL(imp) {
                var retPrint = "";
                retPrint = "Funcionarios_server_processing.php?imp=" + imp;
                if ($("#txtFilial").val() !== "null") {
                    retPrint = retPrint + '&txtFilial=' + $("#txtFilial").val();
                }
                if ($("#txtStatus").val() !== "null") {
                    retPrint = retPrint + '&at=' + $("#txtStatus").val();
                }
                if ($("#txtBusca").val() !== "") {
                    retPrint = retPrint + '&txtBusca=' + $("#txtBusca").val();
                }
                return retPrint;
            }
            $(document).ready(function () {
                $('#tbFuncionarios').dataTable({
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "aoColumns": [{"bSortable": true},
                        {"bSortable": true},
                        {"bSortable": true},
                        {"bSortable": true},
                        {"bSortable": true},
                        {"bSortable": true},
                        {"bSortable": false},
                        {"bSortable": true},
                        {"bSortable": true}],
                    "sAjaxSource": finalURL(0),
                    'oLanguage': {
                        'oPaginate': {
                            'sFirst': "Primeiro",
                            'sLast': "Último",
                            'sNext': "Próximo",
                            'sPrevious': "Anterior"
                        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                        'sLengthMenu': "Visualização de _MENU_ registros",
                        'sLoadingRecords': "Carregando...",
                        'sProcessing': "Processando...",
                        'sSearch': "Pesquisar:",
                        'sZeroRecords': "Não foi encontrado nenhum resultado"},
                    "sPaginationType": "full_numbers",
                    <?php if ($imprimir == 1) { ?>
                        "fnInitComplete": function (oSettings, json) {
                            window.print();
                            window.history.back();
                        }
                    <?php } ?>
                });
            });

            function AbrirBox(opc, id) {
                if (opc === 1) {
                    window.location = './../Financeiro/FuncionariosForm.php';
                }
                if (opc === 2) {
                    window.location = './../Financeiro/FuncionariosForm.php?id=' + id;
                }
                if (opc === 4) {
                    var pRel = "&NomeArq=" + "Funcionarios" +
                            "&lbl=" + "Cod.|CPF|Nome|RG|Cidade|UF|Tel|Contato|Dt Demissao" +
                            "&siz=" + "30|70|190|80|90|30|70|70|70" +
                            "&pdf=" + "9" +
                            "&filter=" + "" +
                            "&PathArqInclude=" + "../Modulos/Financeiro/" + finalURL(1).replace("?", "&"); // server processing
                    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
                }
            }

            function FecharBox(opc) {
                var oTable = $('#tbFuncionarios').dataTable();
                oTable.fnDraw(false);
                $("#newbox").remove();
            }
            <?php if ($imprimir == 1) { ?>
                $(window).load(function () {
                    $(".body").css("margin-left", 0);
                    $("#tbFuncionarios_length").remove();
                    $("#tbFuncionarios_filter").remove();
                    $("#tbFuncionarios_paginate").remove();
                    $("#formPQ > th").css("background-image", "none");
                });
            <?php } ?>
        </script>
        <?php odbc_close($con); ?>
    </body>
</html>
