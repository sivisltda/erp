<?php
include './../../Connections/configini.php';

$id = "";
$ContasDesc[0] = "";
$formaPagto = "null";
$disabled = "";
$toReturn = "";
$toReturnAnd = "";
$tipo = "C";

if ($_GET['tp'] != "") {
    $toReturn = "?tp=" . $_GET['tp'];
    $toReturnAnd = "&tp=" . $_GET['tp'];
}

if ($_GET['id'] != '') {
    $id = $_GET['id'];
    $j = 0;
    $count = explode("|", $_GET['id']);
    for ($fab = 0; $fab <= count($count) - 1; $fab++) {
        if (is_numeric($count[$fab])) {
            $cur = odbc_exec($con, "select descricao,historico,destinatario,tipo_documento,tipo
            from sf_lancamento_movimento l inner join sf_contas_movimento c on l.conta_movimento = c.id_contas_movimento 
            where id_lancamento_movimento = " . $count[$fab]);
            while ($RFP = odbc_fetch_array($cur)) {
                $ContasDesc[$fab] = utf8_encode($RFP['descricao']);
                $historico = utf8_encode($RFP['historico']);
                $destinatario = utf8_encode($RFP['destinatario']);
                $formaPagto = utf8_encode($RFP['tipo_documento']);
                $tipo = utf8_encode($RFP['tipo']);
            }
            $j = $j + 1;
        }
    }
    if ($j > 1) {
        $historico = utf8_encode("GRUPO");
        $destinatario = $_SESSION['login_usuario'];
        $disabled = 'disabled';
    }
}

if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        $nossoNumero = "null";
        $bol_id_banco = "null";
        $bol_data_criacao = "";
        $bol_juros = "null";
        $bol_multa = "null";
        $bol_valor = "null";
        $desconto = "null";
        $BancoCarteira = "null";
        
        $count = explode("|", $_POST['txtId']);
        for ($i = 0; $i <= count($count) - 1; $i++) {
            if (is_numeric($count[$i])) {
                odbc_exec($con, "update sf_lancamento_movimento set
                status = 'Aguarda', destinatario = " . valoresSelectTexto("txtDestinatario") . "
                where id_lancamento_movimento = " . $count[$i] . " and status not in ('Aprovado')");
            }
        }         
        
        if (isset($_POST['txtBanco'])) {
            if ($_POST['txtBanco'] != "null") {               
                $cur = odbc_exec($con, "select isnull(MAX(x.bol_nosso_numero),0) vm, isnull(nosso_numero,0) nn from (
                select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_boleto p on p.bol_id_banco = b.id_bancos
                where id_bancos = '" . $_POST['txtBanco'] . "' union all                        
                select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_solicitacao_autorizacao_parcelas p on p.bol_id_banco = b.id_bancos
                where id_bancos = '" . $_POST['txtBanco'] . "' union all
                select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_venda_parcelas p on p.bol_id_banco = b.id_bancos
                where id_bancos = '" . $_POST['txtBanco'] . "' union all
                select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_lancamento_movimento_parcelas l on l.bol_id_banco = b.id_bancos
                where id_bancos = '" . $_POST['txtBanco'] . "') as x group by x.nosso_numero") or die(odbc_errormsg());                
                while ($RFP = odbc_fetch_array($cur)) {
                    if ($RFP['nn'] > $RFP['vm']) {
                        $nossoNumero = $RFP['nn'];
                    } else {
                        $nossoNumero = bcadd($RFP['vm'], 1);
                    }
                }
                $bol_id_banco = $_POST['txtBanco'];
                $bol_data_criacao = getData("T");
                $bol_juros = "0.00";
                $bol_multa = "0.00";
                if (is_numeric($_POST['txtBancoCarteira'])) {
                    $BancoCarteira = $_POST['txtBancoCarteira'];
                }
                if (valoresNumericos('txtDesconto') > 0) {
                    $desconto = valoresNumericos('txtDesconto');
                }
            }
        }
        $quantiadeParcela = valoresNumericos('txtQuantidade');
        if (is_numeric($quantiadeParcela)) {
            if ($quantiadeParcela > 0 and $quantiadeParcela < 61) {
                $valorParcela = "";
                $NossosNumeros = "0";
                $historico = "GRUPO";
                $count = explode("|", $_GET['id']);
                for ($fab = 0; $fab <= count($count) - 1; $fab++) {
                    if (is_numeric($count[$fab])) {
                        $valorParcela = $valorParcela + valoresNumericos('txtValor' . $count[$fab]);
                    }
                }
                $vencimento = valoresData('txtVencimento');
                if ($disabled == '') {
                    $historico = $_POST['txtHistorico'];
                }
                if (is_numeric($valorParcela) && $vencimento != "null") {
                    if ($valorParcela > 0) {
                        for ($i = 1; $i <= $quantiadeParcela; $i++) {
                            for ($fab = 0; $fab <= count($count) - 1; $fab++) {
                                if (is_numeric($count[$fab])) {
                                    $valorParcela = valoresNumericos('txtValor' . $count[$fab]);
                                    if ($bol_id_banco != "null") {
                                        $bol_valor = $valorParcela;
                                    } else {
                                        $bol_valor = "null";
                                    }
                                    $query = "set dateformat dmy; insert into sf_lancamento_movimento_parcelas(id_lancamento_movimento,numero_parcela,data_vencimento,valor_parcela,valor_previsto,historico_baixa,pa,bol_id_banco,bol_data_criacao,bol_valor,bol_juros,bol_multa,sa_descricao,bol_nosso_numero,data_cadastro,tipo_documento,carteira_id, bol_desconto) values(" .
                                    $count[$fab] . "," . $i . "," . valoresData2(escreverDataSoma($_POST['txtVencimento'], " + " . ($i - 1) . " " . $_POST['txtTipo'] . " ")) . 
                                    "," . $valorParcela . "," . $valorParcela . "," . valoresTexto2($historico) .
                                    "," . valoresTexto2(str_pad($i, 2, "0", STR_PAD_LEFT) . "/" . str_pad($quantiadeParcela, 2, "0", STR_PAD_LEFT)) . "," .
                                    $bol_id_banco . "," . valoresData2($bol_data_criacao) . "," . $bol_valor . "," . $bol_juros . "," . $bol_multa . "," .
                                    valoresTexto("txtDocNum") . "," . $nossoNumero . ",GETDATE()," . $formaPagto . "," . $BancoCarteira . "," . $desconto . ")";
                                    odbc_exec($con, $query) or die(odbc_errormsg());
                                }
                            }
                            if ($nossoNumero != "null") {
                                $NossosNumeros = $NossosNumeros . "," . $nossoNumero;
                                $nossoNumero = bcadd($nossoNumero, 1);
                            }
                        }
                    }
                }
                if ($_POST['txtPrint'] == "1") {
                    $returnTo = "";
                    $cur = odbc_exec($con, "select COUNT(id_parcela) qtd, max(id_parcela) id_parcela from sf_lancamento_movimento_parcelas where inativo = 0 and bol_nosso_numero is not null and bol_nosso_numero in (" . $NossosNumeros . ") group by bol_nosso_numero") or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) {
                        if ($RFP['qtd'] > 1) {
                            $returnTo = $returnTo . "Y-" . $RFP['id_parcela'] . "|";
                        } else {
                            $returnTo = $returnTo . "D-" . $RFP['id_parcela'] . "|";
                        }
                    }
                    if ($returnTo != "") {
                        echo "<script>window.open('./../../Boletos/Boleto.php?id=" . $returnTo . "&crt=" . $_SESSION["contrato"] . "','_blank');parent.FecharBox();</script>";
                    } else {
                        echo "<script>alert('" . $NossosNumeros . "-Parcela(s) criada(s) com sucesso!');parent.FecharBox();</script>";
                    }
                } else {
                    echo "<script>alert('" . $quantiadeParcela . "-Parcela(s) criada(s) com sucesso!');parent.FecharBox();</script>";
                }
            }
        }
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Parcelas</title>
        <link rel="icon" type="image/ico" href="favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet"/>        
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />    
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>        
    </head>
    <body style="overflow:hidden">
        <form action="FormLanc-Parcelas.php?id=<?php echo $_GET['id'] . $toReturnAnd; ?>" name="frmEnviaDados" method="POST">
            <table style="margin:10px">                
                <tr style="height:32px">
                    <?php if ($tipo == "C") { ?>                        
                    <td style="text-align:right">Selecione o Banco:</td>
                    <td colspan="2" style="padding-left:5px">
                        <select name="txtBanco" style="width:212px;<?php echo ($PegaURL != "" ? " display:none" : ""); ?>" id="txtBanco">
                            <option value="null">--Selecione--</option>
                            <?php $cur = odbc_exec($con, "select id_bancos,razao_social from sf_bancos where ativo = 0 order by razao_social") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP['id_bancos'] ?>"><?php echo utf8_encode($RFP['razao_social']) ?></option>
                            <?php } ?>
                        </select>
                    </td>
                    <?php } ?>                
                    <button class="btncloselancparcelas" style="margin-top: 20;" onClick="parent.FecharBox()" id="bntOK"><b>x</b></button>
                </tr>
                <tr style="height:32px">
                    <td width="100" style="text-align:right">Venc. Inicial:</td>
                    <td width="190" colspan="2" style="padding-left:5px">
                        <input id="txtVencimento" name="txtVencimento" type="text" style="width:90px" class="datepicker inputCenter" value="<?php echo getData("T"); ?>"/>                                                                        
                        <?php if ($tipo == "C") { ?>                
                            <select id="txtBancoCarteira" name="txtBancoCarteira" style="width:119px;<?php echo ($PegaURL != "" ? " display:none" : ""); ?>">
                                <option value="null" dsc="0">--Selecione--</option>
                            </select>                         
                        <?php } ?>                
                    </td>
                </tr>
                <?php
                if ($_GET['id'] != "") {
                    $id = $_GET['id'];
                    $count = explode("|", $_GET['id']);
                    for ($fab = 0; $fab <= count($count) - 1; $fab++) {
                        if (is_numeric($count[$fab])) { ?>
                            <tr style="height:32px">
                                <td style="text-align:right"><?php
                                    if ($disabled == "disabled") {
                                        echo $ContasDesc[$fab] . " " . $lang['prefix'];
                                    } else {
                                        echo "Valor da Parcela";
                                    }
                                    ?>:</td>
                                <td style="padding-left:5px">
                                    <input id="txtValor" name="txtValor<?php echo $count[$fab]; ?>" type="text" style="width:90px" value="<?php echo escreverNumero(0); ?>"/>                                                                                                
                                    Desconto
                                    <input style="width:70px" id="txtDesconto" name="txtDesconto" type="text" value="<?php echo escreverNumero(0); ?>"/>                                    
                                </td>
                            </tr>
                            <?php
                        }
                    }
                } ?>
                <tr style="height:32px">
                    <td style="text-align:right">Qtd. Parcelas:</td>
                    <td style="padding-left:5px">
                        <input style="width:90px" id="txtQuantidade" name="txtQuantidade" type="text" value="1"/>
                        <select class="input-medium" style="width:119px;" name="txtTipo" id="txtTipo" <?php echo $disabled; ?>>
                            <option value="month">Mensal</option>
                            <option value="week">Semanal</option>
                            <option value="day">Diário</option>
                        </select>                            
                    </td>
                </tr>
                <tr style="height:32px">
                    <td style="text-align:right">N° Documento:</td>
                    <td style="padding-left:5px"><input style="width:212px" id="txtDocNum" name="txtDocNum" type="text" value=""/></td>
                </tr>
                <tr style="height:32px">
                    <td style="text-align:right">Histórico:</td>
                    <td colspan="2" style="padding-left:5px"><input style="width:212px" id="txtHistorico" name="txtHistorico" type="text" value="<?php echo $historico; ?>" <?php echo $disabled; ?>/></td>
                </tr>
                <tr style="height:32px">
                    <td style="text-align:right">Destinatário:</td>
                    <td colspan="2" style="padding-left:5px">
                        <select class="input-medium" style="width:212px;" name="txtDestinatario" id="txtDestinatario" <?php echo $disabled; ?>>
                            <option value="null">--Selecione--</option>
                            <?php
                            $where_bloq = '';
                            if ($disabled == "disabled") {
                                $where_bloq = " and login_user  = '" . $destinatario . "'";
                            }
                            $cur = odbc_exec($con, "select login_user,nome from sf_usuarios where inativo = 0 and login_user != 'Admin' and (master = 0 or master in (select id_permissoes from sf_usuarios_permissoes where fin_afix = 1)) " . $where_bloq . " order by nome") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP['login_user'] ?>"<?php
                                if (!(strcmp($RFP['login_user'], $destinatario))) {
                                    echo "SELECTED";
                                }
                                ?>><?php echo (utf8_encode($RFP['nome']) == "" ? utf8_encode($RFP['login_user']) : formatNameCompact(utf8_encode($RFP['nome']))); ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>                
                <?php if ($tipo == "C") { ?>                
                    <tr style="height:32px">
                        <td style="text-align:right">Imprimir ao Gerar:</td>
                        <td colspan="2" style="padding-left:5px"><input id="txtPrint" type="checkbox" name="txtPrint" value="1"/></td>
                    </tr>
                <?php } ?>                                
            </table>
            <div class="toolbar bottom tar" style="margin:10px; margin-top: 40px; border-top:solid 1px #CCC; overflow:hidden">
                <div class="data-fluid" style='overflow:hidden; padding-bottom:10px;'>
                    <div class="btn-group" style="margin-top:5px;">
                        <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                        <button class="btn btn-success" type="submit" onclick="return validar();" name="bntSave" id="bntSave"><span class="ico-checkmark"></span> Gravar</button>
                    </div>
                </div>
            </div>
        </form>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>    
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>        
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>                
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>            
        <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>           
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>     
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/tagsinput/jquery.tagsinput.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/multiselect/jquery.multi-select.min.js'></script>                     
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>        
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type="text/javascript" src="../../js/util.js"></script>                                        
        <script type="text/javascript">            
            
            $("#txtVencimento").mask(lang["dateMask"]);
            $("#txtValor, #txtDesconto").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
            
            $('#txtBanco').change(function () {
                if ($(this).val()) {
                    $('#txtBancoCarteira').hide();
                    $.getJSON('bancoCarteira.ajax.php?search=', {txtBanco: $(this).val(), ajax: 'true'}, function (j) {
                        var selItem = "null";
                        options = '<option value="null" dsc="0">--Selecione--</option>';
                        for (var i = 0; i < j.length; i++) {
                            options += '<option dsc="' + j[i].sf_desconto + '" value="' + j[i].sf_carteiras_id + '">' + j[i].sf_carteira_descricao + '</option>';
                            selItem = j[i].sf_carteiras_id;
                        }
                        $('#txtBancoCarteira').html(options).show();
                        $('#txtBancoCarteira').val(selItem);
                    });
                } else {
                    $('#txtBancoCarteira').html('<option value="null" dsc="0">--Selecione--</option>');
                }
            });

            $("#txtValor, #txtBancoCarteira").change(function () {
                descontoGeral();
            });

            function descontoGeral() {
                var des = textToNumber($("#txtBancoCarteira option:selected").attr("dsc"));
                if (des > 0) {
                    var total = (textToNumber($("#txtValor").val()) * des) / 100;
                    $("#txtDesconto").val(numberFormat(total));
                } else {
                    $("#txtDesconto").val(numberFormat(0));
                }
            }

            function validar() {
                if ($("#txtVencimento").val() === "") {
                    bootbox.alert("Preencha o campo Venc. Inicial corretamente!");
                    return false;
                }
                if ($.isNumeric($("#txtBancoCarteira").val()) && $("#txtDocNum").val() === "") {
                    bootbox.alert("Preencha o campo N° Documento corretamente!");
                    return false;
                }
                if ($("#txtValor").val() === "") {
                    bootbox.alert("Preencha o campo Valor da Parcela corretamente!");
                    return false;
                }
                if ($("#txtQuantidade").val() === "") {
                    bootbox.alert("Preencha o campo Qtd. Parcelas corretamente!");
                    return false;
                }
                if ($("#txtDestinatario").val() === "null") {
                    bootbox.alert("Preencha o campo Destinatário corretamente!");
                    return false;
                }
                return true;
            }
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>                