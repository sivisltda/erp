<?php

require_once(__DIR__ . '/../../Connections/configini.php');
$aColumns = array('id_tipo_documento', 'abreviacao', 'd.descricao', 's_tipo', 'parcelar', 'b.descricao');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY d.descricao asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $sOrder = " order by " . $aColumns[intval($_GET['iSortCol_0']) + 1] . " " . $_GET['sSortDir_0'] . ", ";
        }
    }

    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY d.descricao asc";
    }
}

if ($_GET['txtBusca'] != "") {
    $sWhereX = $sWhereX . " and (d.descricao like '%" . $_GET['txtBusca'] . "%' or b.descricao like '%" . $_GET['txtBusca'] . "%')";
}

if (is_numeric($_GET['at'])) {
    $sWhereX .= " and inativo = " . $_GET['at'];
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
        $sWhere .= $aColumns[$i] . " AND LIKE '%" . utf8_decode($_GET['sSearch_' . $i]) . "%' ";
    }
}

$sQuery = "SELECT COUNT(*) total FROM sf_tipo_documento d left join sf_bancos b on d.id_banco_baixa = b.id_bancos WHERE id_tipo_documento > 0 $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total FROM sf_tipo_documento d left join sf_bancos b on d.id_banco_baixa = b.id_bancos WHERE id_tipo_documento > 0 $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,d.*, b.descricao descr_banco,
STUFF((SELECT distinct '|' + cast(tx.de_mes as varchar) + 'x' + cast(tx.ate_mes as varchar) + '-' + cast(tx.valor_taxa  as varchar) from sf_tipo_documento_taxas tx where tx.id_tipo_documento = d.id_tipo_documento FOR XML PATH('')), 1, 1, '') as taxas,
STUFF((SELECT distinct '|' + dc.campo from sf_tipo_documento_campos dc where dc.tipo_documento = id_tipo_documento FOR XML PATH('')), 1, 1, '') as campos    
FROM sf_tipo_documento d left join sf_bancos b on d.id_banco_baixa = b.id_bancos
WHERE d.id_tipo_documento > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " ";
//echo $sQuery1;exit;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    if ($aRow['editavel'] == 0) {
        $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow['abreviacao']) . "'>" . utf8_encode($aRow['abreviacao']) . "</div></center>";
    } else {
        $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow['abreviacao']) . "'><strong>" . utf8_encode($aRow['abreviacao']) . "</strong></div></center>";
    }
    $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(2," . utf8_encode($aRow['id_tipo_documento']) . ")'><div id='formPQ' title='" . utf8_encode($aRow['descricao']) . "'>" . utf8_encode($aRow['descricao']) . "</div></a>";
    $row[] = "<center><div id='formPQ' title='" . str_replace("A", "C/D", utf8_encode($aRow['s_tipo'])) . "'>" . str_replace("A", "C/D", utf8_encode($aRow['s_tipo'])) . "</div></center>";
    $row[] = "<center>" . ($aRow['parcelar'] == 1 ? "Sim" : "Não") . "</center>";   
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow['descr_banco']) . "'>" . utf8_encode($aRow['descr_banco']) . "</div></center>";
    if ($imprimir == 0) {
        if ($aRow['editavel'] == 0) {
            $row[] = "<center><a title='Excluir' href='Tipos-de-Documentos.php?Del=" . $aRow['id_tipo_documento'] . "'><input name='' type='image' onClick=\"return confirm('Deseja deletar esse registro?')\" src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
        } else {
            $row[] = "";
        }
    }
    if ($_GET['tpImp'] == "E") {
        $row[] = $aRow['dias'];
        $row[] = $aRow['dias_baixa'];
        $row[] = ($aRow['taxa_tipo'] == 1 ? "R$" : "%") . " " . escreverNumero($aRow['taxa']);
        $row[] = ($aRow['cartao_dcc'] == 0 ? "Sem" : ($aRow['cartao_dcc'] == 1 ? "DCC" : "DCD"));
        $row[] = ($aRow['inativo'] == 1 ? "Sim" : "Não");
        $row[] = utf8_encode($aRow['taxas']);
        $row[] = utf8_encode($aRow['campos']);
    }
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
