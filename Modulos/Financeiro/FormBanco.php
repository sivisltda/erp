<?php
include '../../Connections/configini.php';
$disabled = 'disabled';

if (isset($_POST['bntSave'])) {
    $tipo_bol2 = "null";
    if ($_POST['txtTipoBol'] == "2") {
        $tipo_bol2 = "3";    
    }else if ($_POST['txtTipoBol'] == "11") {
        $tipo_bol2 = "7";
    }
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_bancos set empresa = '001', " .
        "razao_social = " . valoresTexto('txtRazao') . "," .
        "tipo_bol = " . valoresTexto('txtTipoBol') . "," .
        "tipo_bol2 = " . $tipo_bol2 . "," .
        "tipo_bclc = " . valoresTexto('txtTipo') . "," .
        "descricao = " . valoresTexto('txtDescricao') . "," .
        "gerente = " . valoresTexto('txtGerente') . "," .
        "telefone = " . valoresTexto('txtTelefone') . "," .
        "num_banco = " . valoresTexto('txtBanco') . "," .
        "agencia = " . valoresTexto('txtAgencia') . "," .
        "conta = " . valoresTexto('txtConta') . "," .
        "email = " . valoresTexto('txtEmail') . "," .
        "carteira = " . valoresTexto('txtCarteira') . "," .
        "especie_doc = " . valoresTexto('txtEspecie') . "," .
        "mensagem1 = " . valoresTexto('txtMsg1') . "," .
        "mensagem2 = " . valoresTexto('txtMsg2') . "," .
        "mensagem3 = " . valoresTexto('txtMsg3') . "," .
        "mensagem4 = " . valoresTexto('txtMsg4') . "," .
        "mensagem5 = " . valoresTexto('txtMsg5') . "," .
        "mensagem6 = " . valoresTexto('txtMsg6') . "," .
        "protesto_apos = " . valoresTexto('txtProtesto_apos') . "," .
        "juros = " . valoresNumericos("txtJuros") . "," .
        "multa = " . valoresNumericos("txtMulta") . "," .
        "credito = " . valoresNumericos("txtCredito") . "," .
        "ativo = " . valoresNumericos("txtInativo") . "," .
        "nosso_numero = " . valoresTexto('txtNossoNumero') . "," . 
        "cedente = " . valoresTexto('txtCedente') . "," . 
        "dv_cedente = " . valoresTexto('txtDv') . "," . 
        "inst_cob = " . valoresTexto('txtInst_cob') . "," .
        "acrescimo = " . valoresNumericos('txtAcrescimo') . "," .
        "contador_rem = " . valoresNumericos('txtContador') . "," .
        "msg_email = " . valoresTexto('ckeditor') . " " .
        "where id_bancos = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_bancos(empresa,razao_social,descricao,gerente,telefone,num_banco,agencia,conta,email,carteira,protesto_apos,juros,multa,credito,
        ativo,nosso_numero, cedente, dv_cedente, especie_doc, mensagem1, mensagem2, mensagem3,mensagem4, mensagem5, mensagem6,tipo_bol,tipo_bol2,tipo_bclc, inst_cob, acrescimo, contador_rem, msg_email)
        values('001'," . valoresTexto('txtRazao') . "," .
        valoresTexto('txtDescricao') . "," .
        valoresTexto('txtGerente') . "," .
        valoresTexto('txtTelefone') . "," .
        valoresTexto('txtBanco') . "," .
        valoresTexto('txtAgencia') . "," .
        valoresTexto('txtConta') . "," .
        valoresTexto('txtEmail') . "," .
        valoresTexto('txtCarteira') . "," .
        valoresTexto('txtProtesto_apos') . "," .
        valoresNumericos("txtJuros") . "," .
        valoresNumericos("txtMulta") . "," .
        valoresNumericos("txtCredito") . "," .
        valoresNumericos("txtInativo") . "," .
        valoresTexto('txtNossoNumero') . "," .
        valoresTexto('txtCedente') . "," .
        valoresTexto('txtDv') . "," .
        valoresTexto('txtEspecie') . "," .
        valoresTexto('txtMsg1') . "," .
        valoresTexto('txtMsg2') . "," .
        valoresTexto('txtMsg3') . "," .
        valoresTexto('txtMsg4') . "," .
        valoresTexto('txtMsg5') . "," .
        valoresTexto('txtMsg6') . "," .
        valoresTexto('txtTipoBol') . "," .
        $tipo_bol2 . "," .
        valoresTexto('txtTipo') . "," .
        valoresTexto('txtInst_cob') . "," .
        valoresNumericos('txtAcrescimo') . "," .
        valoresNumericos('txtContador') . "," .
        valoresTexto('ckeditor') . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_bancos from sf_bancos order by id_bancos desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_bancos WHERE id_bancos = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}
if (isset($_POST['Cancelar'])) {
    echo "<script>window.top.location.href = 'Bancos.php';</script>";
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_bancos where id_bancos =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_bancos']);
        $banco = utf8_encode($RFP['num_banco']);
        $agencia = utf8_encode($RFP['agencia']);
        $conta = utf8_encode($RFP['conta']);
        $razao = utf8_encode($RFP['razao_social']);
        $descricao = utf8_encode($RFP['descricao']);
        $gerente = utf8_encode($RFP['gerente']);
        $telefone = utf8_encode($RFP['telefone']);
        $email = utf8_encode($RFP['email']);
        $carteira = utf8_encode($RFP['carteira']);
        $protesto_apos = utf8_encode($RFP['protesto_apos']);
        $juros = utf8_encode($RFP['juros']);
        $multa = utf8_encode($RFP['multa']);
        $inst_cob = utf8_encode($RFP['inst_cob']);
        $credito = utf8_encode($RFP['credito']);
        $NossoNumero = utf8_encode($RFP['nosso_numero']);
        $Cedente = utf8_encode($RFP['cedente']);
        $Dv = utf8_encode($RFP['dv_cedente']);
        $Especie = utf8_encode($RFP['especie_doc']);
        $Msg1 = utf8_encode($RFP['mensagem1']);
        $Msg2 = utf8_encode($RFP['mensagem2']);
        $Msg3 = utf8_encode($RFP['mensagem3']);
        $Msg4 = utf8_encode($RFP['mensagem4']);
        $Msg5 = utf8_encode($RFP['mensagem5']);
        $Msg6 = utf8_encode($RFP['mensagem6']);
        $inativo = utf8_encode($RFP['ativo']);
        $tipo_bol = utf8_encode($RFP['tipo_bol']);
        $tipo = utf8_encode($RFP['tipo_bclc']);
        $acrescimo = utf8_encode($RFP['acrescimo']);
        $mensagem = utf8_encode($RFP['msg_email']);     
        $contador = utf8_encode($RFP['contador_rem']);     
    }
    $cur = odbc_exec($con, "select isnull(MAX(x.bol_nosso_numero),0) vm, isnull(nosso_numero,0) nn from (
    select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_boleto p on p.bol_id_banco = b.id_bancos
    where id_bancos = '" . $PegaURL . "' union all        
    select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_solicitacao_autorizacao_parcelas p on p.bol_id_banco = b.id_bancos
    where id_bancos = '" . $PegaURL . "' union all
    select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_venda_parcelas p on p.bol_id_banco = b.id_bancos
    where id_bancos = '" . $PegaURL . "' union all                    
    select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_lancamento_movimento_parcelas l on l.bol_id_banco = b.id_bancos
    where id_bancos = '" . $PegaURL . "') as x group by x.nosso_numero") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        if (((int) $RFP['nn']) > ((int) $RFP['vm'])) {
            $NossoNumero = $RFP['nn'];
        } else {
            $NossoNumero = str_pad(bcadd($RFP['vm'], 1), strlen($RFP['nn']), "0", STR_PAD_LEFT);
        }
    }
} else {
    $disabled = '';
    $id = '';
    $razao = '';
    $descricao = '';
    $gerente = '';
    $telefone = '';
    $banco = '';
    $agencia = '';
    $conta = '';
    $email = '';
    $carteira = '';
    $protesto_apos = '';
    $juros = 0;
    $multa = 0;
    $inst_cob = '';
    $credito = 0;
    $NossoNumero = '';
    $Cedente = '';
    $Dv = '';
    $Especie = '';
    $Msg1 = '';
    $Msg2 = '';
    $Msg3 = '';
    $Msg4 = '';
    $Msg5 = '';
    $Msg6 = '';
    $inativo = '';
    $tipo_bol = "null";
    $tipo = 'null';
    $acrescimo = 0;
    $mensagem = '';
    $contador = '';
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}

if (isset($_POST['bntAlterar'])) {
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
    }
}

if (isset($_POST['bntSaveEnd'])) {
    if ($id != '' && $_POST['txtCarteira'] != '' && $_POST["txtDescCarteira"] != '') {
        odbc_exec($con, "insert into sf_bancos_carteiras(sf_carteiras_bancos,sf_carteira,sf_carteira_descricao,sf_carteira_registrada,sf_desconto,sf_variacao) values(" . $id . "," .
        valoresTexto("txtCarteira") . "," . valoresTexto("txtDescCarteira") . "," . valoresCheck("ckb_referencia") . "," . valoresNumericos("txtDesconto") . "," . valoresNumericos("txtVariacao") . ")");
    }
}

if ($_GET['Del'] != '') {
    odbc_exec($con, "DELETE FROM sf_bancos_carteiras WHERE sf_carteiras_id = " . $_GET['Del']);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Banco</title>
    </head>
    <link rel="icon" type="image/ico" href="../../favicon.ico"/>
    <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
    <link href="../../css/main.css" rel="stylesheet"/>
    <link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css"/>
    <script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/graficos.js"></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    <body onLoad="document.frmEnviaDados.txtBanco.focus()">
        <form action="FormBanco.php" name="frmEnviaDados" method="POST">
            <div class="frmhead">
                <div class="frmtext">Cadastro de Bancos</div>
                <div class="frmicon" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="tabbable frmtabs">
                <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                <ul class="nav nav-tabs" style="margin:0">
                    <li class="active"><a href="#tab1" data-toggle="tab">Dados principais</a></li>
                    <li><a href="#tab2" data-toggle="tab">Dados para boleto</a></li>
                    <li><a href="#tab3" data-toggle="tab">Instruções para boleto</a></li>
                    <li><a href="#tab4" data-toggle="tab">E-mail boleto</a></li>                    
                </ul>
                <div class="tab-content frmcont" style="height:290px">
                    <div class="tab-pane active" id="tab1" style="overflow:hidden; height:290px">
                        <div style="width: 34%;float:left;">
                            <span style="width: 50%">Banco:</span>
                            <input id="txtBanco" name="txtBanco" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="4" value="<?php echo $banco; ?>"/>
                        </div>
                        <div style="width: 32%;float:left;margin-left: 1%">
                            <span style="width: 50%">Agência:</span>
                            <input id="txtAgencia" name="txtAgencia" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="10" value="<?php echo $agencia; ?>"/>
                        </div>
                        <div style="width: 32%;float:left;margin-left: 1%">
                            <span style="width: 50%">Conta:</span>
                            <input id="txtConta" name="txtConta" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="12" value="<?php echo $conta; ?>"/>
                        </div>
                        <div style="clear:both;height: 5px;"></div>
                        <div style="width: 50%;float:left;">
                            <span style="width: 50%">Nome/Razão Social:</span>
                            <input name="txtRazao" id="txtRazao" maxlength="31" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $razao; ?>"/>
                        </div>
                        <div style="width: 49%;float:left;margin-left: 1%">
                            <span style="width: 50%">Descrição:</span>
                            <input name="txtDescricao" id="txtDescricao" maxlength="128" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $descricao; ?>"/>
                        </div>
                        <div style="clear:both;height: 5px;"></div>
                        <div style="width: 50%;float:left;">
                            <span style="width: 50%">Gerente:</span>
                            <input id="txtGerente" name="txtGerente" maxlength="42" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $gerente; ?>"/>
                        </div>
                        <div style="width: 49%;float:left;margin-left: 1%">
                            <span style="width: 50%">Telefone:</span>
                            <input name="txtTelefone" type="text" class="input-medium" id="txtTelefone" value="<?php echo $telefone ?>" maxlength="30" <?php echo $disabled; ?>/>
                        </div>
                        <div style="clear:both;height: 5px;"></div>
                        <div style="width: 67%;float:left;">
                            <span style="width: 50%">E-mail:</span>
                            <input id="txtEmail" name="txtEmail" maxlength="75" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $email; ?>"/>
                        </div>
                        <div style="width: 32%;float:left;margin-left: 1%">
                            <span style="width: 50%">Limite de Crédito:</span>
                            <input id="txtCredito" name="txtCredito" maxlength="10" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo escreverNumero($credito); ?>"/>
                        </div>
                        <div style="width: 30%;float:left;">
                            <span style="width: 50%">Tipo:</span>
                            <select class="input-medium" style="width:100%" <?php echo $disabled; ?> name="txtTipo" id="txtTipo">
                                <option value="0" <?php if (0 == $tipo) { echo "SELECTED";} ?>>Banco</option>                                
                                <option value="1" <?php if (1 == $tipo) { echo "SELECTED";} ?>>Local de Recebimento</option>                                                                
                            </select>
                        </div>                        
                        <div style="width: 20%; float:left; margin-left: 1%;">
                            <span style="width: 50%">Inativo:</span>
                            <select class="input-medium" style="width:100%" <?php echo $disabled; ?> name="txtInativo" id="txtInativo">
                                <option value="1" <?php if (1 == $inativo) { echo "SELECTED";} ?>>SIM</option>                                
                                <option value="0" <?php if (0 == $inativo) { echo "SELECTED";} ?>>NÃO</option>                                                                
                            </select>
                        </div>                        
                        <div style="clear:both;height: 5px;"></div>
                    </div>
                    <div class="tab-pane" id="tab2" style="height:290px; overflow:hidden">
                        <div style="width: 23%;float:left;">
                            <span style="width: 50%">Nosso Número:</span>
                            <input id="txtNossoNumero" name="txtNossoNumero" maxlength="17" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $NossoNumero; ?>"/>
                        </div>
                        <div style="width: 10%;float:left;margin-left: 1%">
                            <span style="width: 50%">Protesto:</span>
                            <input id="txtProtesto_apos" name="txtProtesto_apos" maxlength="9" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $protesto_apos; ?> "/>
                        </div>
                        <div style="width: 15%;float:left;margin-left: 1%">
                            <span style="width: 50%">Inst. Cob:</span>
                            <input id="txtInst_cob" name="txtInst_cob" maxlength="9" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $inst_cob; ?>"/>
                        </div>
                        <div style="width: 16%;float:left;margin-left: 1%">
                            <span style="width: 50%">Multa %:</span>
                            <input id="txtMulta" name="txtMulta" maxlength="9" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo escreverNumero($multa, 0, 3); ?>"/>
                        </div>
                        <div style="width: 15%;float:left;margin-left: 1%">
                            <span style="width: 50%">Juro ao Dia %:</span>
                            <input id="txtJuros" name="txtJuros" maxlength="9" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo escreverNumero($juros, 0, 3); ?>"/>
                        </div>
                        <div style="width: 16%;float:left;margin-left: 1%">
                            <span style="width: 50%">Espécie:</span>
                            <input id="txtEspecie" name="txtEspecie" maxlength="10" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $Especie; ?>"/>
                        </div>
                        <div style="clear:both;height: 5px;"></div>
                        <div style="width: 34%;float:left;">
                            <span style="width: 50%">Tipo:</span>
                            <select class="input-medium" style="width:100%" <?php echo $disabled; ?> name="txtTipoBol" id="txtTipoBol">
                                <option value="0" <?php
                                if (0 == $tipo_bol) {
                                    echo "SELECTED";
                                }
                                ?>>Boleto CEF</option>
                                <option value="8" <?php
                                if (8 == $tipo_bol) {
                                    echo "SELECTED";
                                }
                                ?>>Boleto CEF, carnê</option>                                     
                                <option value="1" <?php
                                if (1 == $tipo_bol) {
                                    echo "SELECTED";
                                }
                                ?>>Boleto ITAU</option>
                                <option value="3" <?php
                                if (3 == $tipo_bol) {
                                    echo "SELECTED";
                                }
                                ?>>Boleto ITAU, carnê</option>
                                <option value="2" <?php
                                if (2 == $tipo_bol) {
                                    echo "SELECTED";
                                }
                                ?>>Boleto ITAU, meia-folha</option>                                                              
                                <option value="4" <?php
                                if (4 == $tipo_bol) {
                                    echo "SELECTED";
                                }
                                ?>>Boleto Bradesco</option>
                                <option value="9" <?php
                                if (9 == $tipo_bol) {
                                    echo "SELECTED";
                                }
                                ?>>Boleto Bradesco, carnê</option>                                 
                                <option value="5" <?php
                                if (5 == $tipo_bol) {
                                    echo "SELECTED";
                                }
                                ?>>Boleto Santander</option>
                                <option value="10" <?php
                                if (10 == $tipo_bol) {
                                    echo "SELECTED";
                                }
                                ?>>Boleto Santander, carnê</option>                                
                                <option value="6" <?php
                                if (6 == $tipo_bol) {
                                    echo "SELECTED";
                                }
                                ?>>Boleto BB</option>
                                <option value="7" <?php
                                if (7 == $tipo_bol) {
                                    echo "SELECTED";
                                }
                                ?>>Boleto BB, carnê</option>                           
                                <option value="11" <?php
                                if (11 == $tipo_bol) {
                                    echo "SELECTED";
                                }
                                ?>>Boleto BB, meia-folha</option>                                                              
                                <option value="12" <?php
                                if (12 == $tipo_bol) {
                                    echo "SELECTED";
                                }
                                ?>>Boleto SICOOB</option>
                                <option value="13" <?php
                                if (13 == $tipo_bol) {
                                    echo "SELECTED";
                                }
                                ?>>Boleto SICOOB, carnê</option>                                
                                <option value="14" <?php
                                if (14 == $tipo_bol) {
                                    echo "SELECTED";
                                }
                                ?>>Boleto SICREDI</option>
                                <option value="15" <?php
                                if (15 == $tipo_bol) {
                                    echo "SELECTED";
                                }
                                ?>>Boleto SICREDI, carnê</option>                                
                            </select>
                        </div>
                        <div style="width: 15%;float:left;margin-left: 1%">
                            <span style="width: 50%">Conta Cedente:</span>
                            <input id="txtCedente" name="txtCedente" maxlength="16" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $Cedente; ?>"/>
                        </div>
                        <div style="width: 16%;float:left;margin-left: 1%">
                            <span style="width: 50%">Dv:</span>
                            <input id="txtDv" name="txtDv" maxlength="2" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $Dv; ?>"/>
                        </div>
                        <div style="width: 15%;float:left;margin-left: 1%">
                            <span style="width: 50%">Acréscimo R$:</span>
                            <input id="txtAcrescimo" name="txtAcrescimo" maxlength="10" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo escreverNumero($acrescimo); ?>"/>
                        </div>
                        <div style="width: 16%;float:left;margin-left: 1%">
                            <span style="width: 50%">Contador REM:</span>
                            <input id="txtContador" name="txtContador" maxlength="10" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $contador; ?>"/>
                        </div>
                        <div style="clear:both;height: 5px;"></div>
                        <div style="width: 13%;float:left;">
                            <span style="width: 50%">Cod. Carteira:</span>
                            <input id="txtCarteira" name="txtCarteira" maxlength="10" type="text" <?php echo $disabled; ?> class="input-medium" value=""/>
                        </div>                        
                        <div style="width: 31%;float:left;margin-left: 1%">
                            <span style="width: 50%">Desc. Carteira:</span>
                            <input id="txtDescCarteira" name="txtDescCarteira" maxlength="50" type="text" <?php echo $disabled; ?> class="input-medium" value=""/>
                        </div>                        
                        <div style="width: 14%;float:left;margin-left: 1%">
                            <span style="width: 50%">% Desconto:</span>
                            <input id="txtDesconto" name="txtDesconto" maxlength="6" type="text" <?php echo $disabled; ?> class="input-medium" value=""/>
                        </div>
                        <div style="width: 10%;float:left;margin-left: 1%">
                            <span style="width: 50%">Variação:</span>
                            <input id="txtVariacao" name="txtVariacao" maxlength="10" type="text" <?php echo $disabled; ?> class="input-medium" value=""/>
                        </div>                        
                        <div style="width: 28%;float:left;margin-left: 1%;margin-top: 14px;">
                            <input name="ckb_referencia" id="ckb_referencia" style="margin-top: 6px;" <?php echo $disabled; ?> type="checkbox" value="1" class="input-medium"/>
                            <span style="width: 50%;">Registrada:</span>
                            <button class="btn btn-success" style="height:27px;float: right;" <?php
                            if (is_numeric($PegaURL)) {
                                echo $disabled;
                            } else {
                                echo "disabled";
                            }
                            ?> type="submit" name="bntSaveEnd"><span class="ico-checkmark"></span> Enviar</button>
                        </div>
                        <div style="clear:both;height: 10px;"></div>
                        <table width="100%">
                            <div style="height:35px; width:60px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Carteira</b></div>
                            <div style="height:35px; width:186px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Desc. Carteira</b></div>
                            <div style="height:35px; width:75px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>% Desconto</b></div>
                            <div style="height:35px; width:60px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Variação</b></div>
                            <div style="height:35px; width:70px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Registrada</b></div>
                            <div style="height:35px; width:53px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Ação</b></div>
                        </table>
                        <?php
                        if (is_numeric($PegaURL)) {
                            $cur = odbc_exec($con, "select * from sf_bancos_carteiras where sf_carteiras_bancos = " . $PegaURL);
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <div style="height:30px; width:65px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo utf8_encode($RFP['sf_carteira']); ?></div>
                                <div style="height:30px; width:191px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo utf8_encode($RFP['sf_carteira_descricao']); ?></div>
                                <div style="height:30px; width:80px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo escreverNumero($RFP['sf_desconto']) . "%"; ?></div>
                                <div style="height:30px; width:65px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo utf8_encode($RFP['sf_variacao']); ?></div>
                                <div style="height:30px; width:75px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo ($RFP['sf_carteira_registrada'] == 1 ? "Sim" : "Não"); ?></div>
                                <div style="height:23px; width:58px; background:#FFF; margin-left:1px; padding-top:7px; padding-left:3px; float:left">
                                    <a onClick="return confirm('Deseja deletar esse registro?')" href='FormBanco.php?id=<?php echo $PegaURL; ?>&Del=<?php echo utf8_encode($RFP['sf_carteiras_id']); ?>' ><img src="../../img/1365123843_onebit_33 copy.PNG" width='16' height='16'/></a>
                                </div>
                                <div style="clear:both"></div>
                                <?php
                            }
                        }
                        ?>
                        <div style="clear:both;height: 5px;"></div>
                    </div>
                    <div class="tab-pane" id="tab3" style="height:290px; overflow:hidden">
                        <div style="width: 100%;float:left;">
                            <span style="width: 50%">Compensação 1:</span>
                            <input id="txtMsg1" name="txtMsg1" maxlength="128" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $Msg1; ?>"/>
                        </div>
                        <div style="clear:both;height: 5px;"></div>
                        <div style="width: 100%;float:left;">
                            <span style="width: 50%">Compensação 2:</span>
                            <input id="txtMsg2" name="txtMsg2" maxlength="128" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $Msg2; ?>"/>
                        </div>
                        <div style="clear:both;height: 5px;"></div>
                        <div style="width: 100%;float:left;">
                            <span style="width: 50%">Compensação 3:</span>
                            <input id="txtMsg3" name="txtMsg3" maxlength="128" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $Msg3; ?>"/>
                        </div>
                        <div style="clear:both;height: 5px;"></div>
                        <div style="width: 100%;float:left;">
                            <span style="width: 50%">Recibo 1:</span>
                            <input id="txtMsg4" name="txtMsg4" maxlength="128" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $Msg4; ?>"/>
                        </div>
                        <div style="clear:both;height: 5px;"></div>
                        <div style="width: 100%;float:left;">
                            <span style="width: 50%">Recibo 2:</span>
                            <input id="txtMsg5" name="txtMsg5" maxlength="128" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $Msg5; ?>"/>
                        </div>
                        <div style="clear:both;height: 5px;"></div>
                        <div style="width: 100%;float:left;">
                            <span style="width: 50%">Recibo 3:</span>
                            <input id="txtMsg6" name="txtMsg6" maxlength="128" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $Msg6; ?>"/>
                        </div>
                        <div style="clear:both;height: 5px;"></div>
                    </div>
                    <div class="tab-pane" id="tab4" style="overflow:hidden;height:290px">
                        <div style="width:90%;float: left;">
                            <span style="width: 50%; margin-bottom: 10px;">Mensagem adicional para envio do boleto por email:</span>                            
                            <textarea id="ckeditor" name="ckeditor" <?php echo $disabled; ?>> <?php echo $mensagem; ?></textarea>
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == '') { ?>
                        <button class="btn green" type="submit" title="Gravar" name="bntSave" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                        <?php if ($_POST['txtId'] == '') { ?>
                            <button class="btn yellow" title="Cancelar" onClick="parent.FecharBox()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                            <button class="btn yellow" title="Cancelar" type="submit" name="bntAlterar" id="bntOK" ><span class="ico-reply"></span> Cancelar</button>
                            <?php
                        }
                    } else { ?>
                        <button class="btn green" type="submit" title="Novo" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" title="Alterar" type="submit" name="bntEdit" id="bntOK" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                        <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
            <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
            <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
            <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
            <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
            <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
            <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
            <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
            <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
            <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
            <script type='text/javascript' src='../../js/plugins/ckeditor/ckeditor.js'></script>            
            <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>            
            <script type='text/javascript' src='../../js/plugins.js'></script>
            <script type='text/javascript' src='../../js/charts.js'></script>
            <script type='text/javascript' src='../../js/actions.js'></script> 
            <script type="text/javascript" src="../../js/util.js"></script>            
            <script type="text/javascript">
                
                $("#txtCredito, #txtAcrescimo").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});                
                $("#txtMulta, #txtJuros").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"], suffix: "%", centsLimit: 3});
                $("#txtDesconto").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], suffix: "%"});
                
                $(document).ready(function () {
                    var ckeditor = CKEDITOR.replace('ckeditor', {removePlugins: 'elementspath', height: 190});
                    ckeditor.config.width = 556;
                    if ($('#txtNome').attr("disabled") === "disabled") {
                        $('.ms-list').find('li').addClass("disabled");
                    }
                });

            </script>            
        </form>
        <?php odbc_close($con); ?>
    </body>
</html>