<?php
include './../../Connections/configini.php';
$imprimir = 0;
$F2 = "";
$F3 = 0;
$DateBegin = getData("B");
$DateEnd = getData("E");
if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = 1;
}
if (is_numeric($_GET['tp'])) {
    $F2 = $_GET['tp'];
}
if (is_numeric($_GET['ts'])) {
    $F3 = $_GET['ts'];
}
if ($_GET['dti'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
}
if ($_GET['dtf'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
}
if (is_numeric($_GET['emp'])) {
    $empresa = $_GET['emp'];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body>
        <?php if ($imprimir == 0) { ?>
            <div id="loader"><img src="../../img/loader.gif"/></div>
        <?php } ?>
        <div class="wrapper">
            <?php
            if ($imprimir == 0) {
                include("../../menuLateral.php");
            }
            ?>
            <div class="body">
                <?php
                if ($imprimir == 0) {
                    include("../../top.php");
                }
                ?>
                <div class="content">
                    <?php if ($imprimir == 0) { ?>
                        <div class="page-header">
                            <div class="icon"> <span class="ico-arrow-right"></span> </div>
                            <h1>Financeiro<small>Relatório de Consulta por Formas de Pagamento</small></h1>
                        </div>
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="block">
                                    <select style="width: 60px;" name="txtEmpresa" id="txtEmpresa">
                                        <?php
                                        $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo utf8_encode($RFP['id_filial']); ?>" <?php
                                            if ($empresa == "") {
                                                $empresa = $RFP['id_filial'];
                                            }
                                            if ($empresa == $RFP['id_filial']) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP['numero_filial']); ?></option>
                                        <?php } ?>
                                    </select>
                                    <select style="width: 160px;" name="txtTipoB" id="txtTipoB">
                                        <option value="" >--Selecione--</option>
                                        <option value="0"
                                        <?php
                                        if ($F2 == "0") {
                                            echo "SELECTED";
                                        }
                                        ?>>Crédito</option>
                                        <option value="1"
                                        <?php
                                        if ($F2 == "1") {
                                            echo "SELECTED";
                                        }
                                        ?>>Débito</option>
                                    </select>  
                                    <select style="width: 160px;" name="txtTipoC" id="txtTipoC" >
                                        <option value="0" <?php
                                        if ($F3 == "0") {
                                            echo "SELECTED";
                                        }
                                        ?>>Sintético</option>
                                        <option value="1" <?php
                                        if ($F3 == "1") {
                                            echo "SELECTED";
                                        }
                                        ?>>Analítico</option>
                                    </select>                                    
                                    De <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial">
                                    até <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final">
                                    <button id="btnfind" name="btnfind" class="button button-turquoise btn-primary buttonX" onclick="AbrirBox(0)" type="button" title="Buscar"><span class="ico-search icon-white"></span></button>
                                    <button type="button" name="btnPrint" id="btnPrint" class="button button-blue btn-primary" onclick="AbrirBox(1)" title="Imprimir"><span class="ico-print"></span></button>                                    
                                </div>
                            </div>      	
                        </div>
                        <div class="row-fluid">                        
                            <div style="clear:both"></div>
                            <div class="boxhead">
                                <div class="boxtext">Formas de Pagamento</div>
                            </div>
                            <div class="boxtable">
                                <div class="data-fluid">
                        <?php   }
                                $totCred = 0;
                                $totDebt = 0;
                                if ($imprimir == 1) {
                                    $titulo_pagina = "RELATÓRIO DE CONSULTA<br/>Formas de Pagamento";
                                    include "Cabecalho-Impressao.php";
                                }
                                $count = 0;
                                if (is_numeric($F2)) {
                                    $count = 1;
                                }
                                for ($i = $count; $i < 2; $i++) {
                                    if (($count == 1 && $F2 == 0) || $i == 0) {
                                        $texto = "CRÉDITO";
                                        $tp1 = "C";
                                        $tp2 = "V";
                                    } else {
                                        $texto = "DÉBITO";
                                        $tp1 = "D";
                                        $tp2 = "C";
                                    }
                                    ?>
                                    <table width="100%" <?php
                                    if ($imprimir == 0) {
                                        echo "class=\"table\"";
                                    } else {
                                        echo "border=\"1\"";
                                    }
                                    ?> style="margin-bottom:20px">
                                        <thead>
                                            <tr>
                                                <th width="5%">
                                        <center><?php echo $tp1; ?></center>
                                        </th>
                                        <th style="text-align:left; <?php
                                        if ($imprimir == 1) {
                                            echo "padding-top:4px;padding-bottom:4px;padding-left:8px;";
                                        } else {
                                            echo "padding-left:8px";
                                        }
                                        ?>"><?php echo $texto; ?>
                                        </th>
                                        <th width="20%" colspan="2"><center>VALOR</center></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $total = 0;
                                            if (is_numeric($empresa)) {
                                                $whereX1 = " and sf_lancamento_movimento.empresa = " . $empresa;
                                                $whereX2 = " and sf_solicitacao_autorizacao.empresa = " . $empresa;
                                                $whereX3 = " and sf_vendas.empresa = " . $empresa;
                                            }
                                            $query = "set dateformat dmy;select x.descricao,sum(x.valor_pago) valor_pago from (select sf_tipo_documento.descricao,sum(valor_pago) valor_pago from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento 
                                            on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
                                            inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
                                            inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
                                            left join sf_tipo_documento on sf_lancamento_movimento_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento                                                                        
                                            inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas 
                                            where sf_lancamento_movimento.status = 'Aprovado' and sf_contas_movimento.tipo = '" . $tp1 . "' AND valor_pago > 0 AND data_pagamento >= " . valoresDataHora2($DateBegin, "00:00") . " AND data_pagamento <= " . valoresDataHora2($DateEnd, "23:59") . $whereX1 . "
                                            AND sf_lancamento_movimento_parcelas.inativo = '0' group by sf_tipo_documento.descricao union all
                                            select sf_tipo_documento.descricao,sum(valor_pago) valor_pago from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
                                            on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao                                                                        
                                            inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
                                            inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas
                                            left join sf_tipo_documento on sf_solicitacao_autorizacao_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento                                                                                                                                                
                                            inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas 
                                            where status = 'Aprovado' and sf_contas_movimento.tipo = '" . $tp1 . "' AND valor_pago > 0 AND data_pagamento >= " . valoresDataHora2($DateBegin, "00:00") . " AND data_pagamento <= " . valoresDataHora2($DateEnd, "23:59") . $whereX2 . "  
                                            AND sf_solicitacao_autorizacao_parcelas.inativo = '0' group by sf_tipo_documento.descricao union all                                             
                                            select sf_tipo_documento.descricao,sum(valor_pago) valor_pago from sf_venda_parcelas inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda 
                                            left join sf_tipo_documento on sf_venda_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento                                                                                                                                                                                                    
                                            inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas 
                                            where status = 'Aprovado' and sf_vendas.cov = '" . $tp2 . "' AND valor_pago > 0 AND data_pagamento >= " . valoresDataHora2($DateBegin, "00:00") . " AND data_pagamento <= " . valoresDataHora2($DateEnd, "23:59") . $whereX3 . "
                                            AND sf_venda_parcelas.inativo = '0' group by sf_tipo_documento.descricao) as x group by x.descricao order by x.descricao";
                                            //echo $query;exit;
                                            $cur2 = odbc_exec($con, $query);
                                            while ($RFP = odbc_fetch_array($cur2)) {
                                                if ($tp1 == "C") {
                                                    $totCred = $totCred + $RFP['valor_pago'];
                                                } elseif ($tp1 == "D") {
                                                    $totDebt = $totDebt + $RFP['valor_pago'];
                                                }
                                                $total = $total + $RFP ['valor_pago'];
                                                ?>
                                                <tr id="formPQ">
                                                    <td colspan="2" style="padding:2px; padding-left:8px;"><?php echo utf8_encode($RFP['descricao']); ?></td>
                                                    <td style="padding:2px;text-align:right;"><?php echo escreverNumero($RFP['valor_pago'], 1); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                        <tr>
                                            <th width="5%">
                                        <center><?php echo $tp1; ?></center>
                                        </th>
                                        <th style="text-align:left; <?php
                                        if ($imprimir == 1) {
                                            echo "padding-top:4px;padding-bottom:4px;padding-left:8px;";
                                        } else {
                                            echo "padding-left:8px";
                                        }
                                        ?>"> TOTAL
                                        </th>
                                        <th style="padding:2px;text-align:right;"><?php echo escreverNumero($total, 1); ?></th>
                                        </tr>
                                    </table>
                                    <?php if ($F3 == "1") { ?>
                                        <table width="100%" <?php
                                        if ($imprimir == 0) {
                                            echo "class=\"table\"";
                                        } else {
                                            echo "border=\"1\"";
                                        }
                                        ?> style="margin-bottom:20px">
                                            <thead>
                                                <tr>
                                                    <th colspan="4" style="text-align:left; <?php
                                                    if ($imprimir == 1) {
                                                        echo "padding-top:4px;padding-bottom:4px;padding-left:8px;";
                                                    } else {
                                                        echo "padding-left:8px";
                                                    }
                                                    ?>">
                                                        DETALHES DE MOVIMENTAÇÃO DE <?php
                                                        echo $texto;
                                                        $a = 0;
                                                        $cur3 = odbc_exec($con, "set dateformat dmy;select id_venda id,cod_pedido,id_fornecedores_despesas,razao_social,nome_fantasia from sf_vendas
                                                        inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas 
                                                        where status = 'Aprovado' and sf_vendas.cov = '" . $tp2 . "' AND data_venda >= " . valoresDataHora2($DateBegin, "00:00") . " AND data_venda <= " . valoresDataHora2($DateEnd, "23:59") . $whereX3);
                                                        while ($RFP = odbc_fetch_array($cur3)) {
                                                            $ID_[$a] = $RFP["id"];
                                                            $CODIGO_[$a] = $RFP["cod_pedido"];
                                                            $Descricao_[$a] = $RFP["id_fornecedores_despesas"] . " - " . utf8_encode($RFP["razao_social"]);
                                                            if ($RFP["nome_fantasia"] != "") {
                                                                $Descricao_[$a] = $Descricao_[$a] . " (" . utf8_encode($RFP["nome_fantasia"]) . ")";
                                                            }
                                                            $a++;
                                                        }
                                                        ?>
                                                    </th>
                                                </tr>
                                                <?php for ($j = 0; $j < $a; $j++) { ?>
                                                    <tr id="formPQ">
                                                        <td style="width:20%;"><strong><?php echo $ID_[$j] . "/" . $CODIGO_[$j]; ?></strong></td>
                                                        <td style="border-left: hidden;" colspan="3"><strong><?php echo $Descricao_[$j]; ?></strong></td>
                                                    </tr>
                                                    <?php
                                                    if (is_numeric($ID_[$j])) {
                                                        $cur3 = odbc_exec($con, "select sf_produtos.descricao,vendedor_comissao,valor_total,razao_social from sf_vendas_itens                                                     
                                                        inner join sf_produtos on sf_produtos.conta_produto = sf_vendas_itens.produto 
                                                        left join sf_fornecedores_despesas on sf_fornecedores_despesas.id_fornecedores_despesas = sf_vendas_itens.vendedor_comissao
                                                        where id_venda = " . $ID_[$j] . " order by 1");
                                                        while ($RFP = odbc_fetch_array($cur3)) { ?>
                                                            <tr>
                                                                <td colspan="2"><?php echo "   * " . utf8_encode($RFP["descricao"]); ?></td>
                                                                <td style="width:20%;border-left: hidden;"><?php echo utf8_encode($RFP["razao_social"]); ?></td>
                                                                <td style="width:20%;padding:2px;text-align:right;"><?php echo escreverNumero($RFP['valor_total'], 1); ?></td>
                                                            </tr>
                                                            <?php
                                                        } $cur3 = odbc_exec($con, "select sf_tipo_documento.descricao,data_pagamento,valor_pago from sf_venda_parcelas
                                                        inner join sf_tipo_documento on sf_tipo_documento.id_tipo_documento = sf_venda_parcelas.tipo_documento where venda = " . $ID_[$j] . " order by 1");
                                                        while ($RFP = odbc_fetch_array($cur3)) { ?>
                                                            <tr>
                                                                <td colspan="2"><?php echo " >>" . utf8_encode($RFP["descricao"]); ?></td>
                                                                <td style="border-left: hidden;"><?php echo "BP " . escreverData($RFP["data_pagamento"]); ?></td>
                                                                <td style="padding:2px;text-align:right;"><?php echo escreverNumero($RFP['valor_pago'], 1); ?></td>
                                                            </tr>
                                                        <?php } ?>
                                                        <tr id="formPQ">
                                                            <td style="border-left: hidden;border-right: hidden" colspan="4"></td>
                                                        </tr>                                                        
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </thead>                                        
                                        </table>
                                        <?php
                                    }
                                }
                                ?>
                                <table width="100%" <?php
                                if ($imprimir == 0) {
                                    echo "class=\"table\"";
                                } else {
                                    echo "border=\"1\"";
                                }
                                ?> style="margin-bottom:20px">
                                    <thead>
                                        <?php if ($F2 == 0 || $F2 == "") { ?>
                                            <tr>
                                                <th width="5%">
                                        <center>C</center>
                                        </th>
                                        <th style="text-align:left; <?php
                                        if ($imprimir == 1) {
                                            echo "padding-top:4px;padding-bottom:4px;padding-left:8px;";
                                        } else {
                                            echo "padding-left:8px";
                                        }
                                        ?>">
                                            SOMATÓRIO TOTAL EM CRÉDITO
                                        </th>
                                        <th style="padding:2px;text-align:right;" width="20%" colspan="2"><?php echo escreverNumero($totCred, 1); ?></th>
                                        </tr>                                            
                                    <?php } if ($F2 == 1 || $F2 == "") { ?>
                                        <tr>
                                            <th width="5%">
                                        <center>D</center>
                                        </th>
                                        <th style="text-align:left; <?php
                                        if ($imprimir == 1) {
                                            echo "padding-top:4px;padding-bottom:4px;padding-left:8px;";
                                        } else {
                                            echo "padding-left:8px";
                                        }
                                        ?>">
                                            SOMATÓRIO TOTAL EM DÉBITO
                                        </th>
                                        <th style="padding:2px;text-align:right;" width="20%" colspan="2"><?php echo escreverNumero($totDebt, 1); ?></th>
                                        </tr>  
                                    <?php } if (is_numeric($F2) == false) { ?>
                                        <tr>
                                            <th width="5%">
                                        <center>T</center>
                                        </th>
                                        <th style="text-align:left; <?php
                                        if ($imprimir == 1) {
                                            echo "padding-top:4px;padding-bottom:4px;padding-left:8px;";
                                        } else {
                                            echo "padding-left:8px";
                                        }
                                        ?>">
                                            DIFERENÇA
                                        </th>
                                        <th style="padding:2px;text-align:right;" width="20%" colspan="2"><?php echo escreverNumero(($totCred - $totDebt), 1); ?></th>
                                        </tr>                              
                                    <?php } ?>
                                    </thead>                                            
                                </table> 
                                <?php if ($imprimir == 1) { ?>
                                </div>
                            </div>           
                        <?php } ?>
                    </div>                        
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/uniform/jquery.uniform.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>        
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            
            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
            
            function AbrirBox(id) {
                var retPrint = "";
                if ($('#txtTipoB').val() !== "") {
                    retPrint = retPrint + "&tp=" + $('#txtTipoB').val();
                }
                if ($('#txtTipoC').val() !== "") {
                    retPrint = retPrint + "&ts=" + $('#txtTipoC').val();
                }
                if ($('#txt_dt_begin').val() !== "") {
                    retPrint = retPrint + "&dti=" + $('#txt_dt_begin').val().replace(/\//g, "_");
                }
                if ($('#txt_dt_end').val() !== "") {
                    retPrint = retPrint + "&dtf=" + $('#txt_dt_end').val().replace(/\//g, "_");
                }
                if ($('#txtEmpresa').val() !== "") {
                    retPrint = retPrint + "&emp=" + $('#txtEmpresa').val();
                }
                if (id === 0) {
                    window.location = "Relatorio-Tipos-de-Documentos.php?imp=0" + retPrint;
                } else if (id === 1) {
                    window.location = "Relatorio-Tipos-de-Documentos.php?imp=1" + retPrint;
                }
            }
        </script>                
    <?php if ($imprimir == 1) { ?>
        <script type="text/javascript">
            $(window).load(function () {
                $(".body").css("margin-left", 0);
                window.print();
                window.onafterprint = back;
                function back() {
                    window.history.back();
                }                
            });
        </script>
        <?php
    } odbc_close($con); ?>
    </body>        
</html>