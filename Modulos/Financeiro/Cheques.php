<?php
include '../../Connections/configini.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>        
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Financeiro<small>Cheques</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="float:left;width: 10%;margin-top: 15px;">
                                    <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir()" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                </div>
                                <div style="float:right;width: 40%;">                                    
                                    <div style="float:left;margin-left: 1%;width: 32%;">                                    
                                        <div width="100%">Tipo:</div>
                                        <select id="txtTipo" class="select">
                                            <option value="null">Selecione</option>                                     
                                            <option value="0" selected>Não Recebidos</option>                                                                                                                    
                                            <option value="1">Recebidos</option>                                        
                                            <option value="2">Trocados</option>                                                                        
                                            <option value="3">Repassado</option>                                                                        
                                        </select>                                           
                                    </div>                                                                           
                                    <div style="float:left;margin-left: 1%;width: 55%;">
                                        <div width="100%">Busca:</div>
                                        <input id="txtBusca" maxlength="100" type="text" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title=""/>
                                    </div>
                                    <div style="float:left;margin-left: 1%;width: 7%;margin-top: 15px;">
                                        <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Cheques</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblLista">
                            <thead>
                                <tr>
                                    <th width="8%">Data</th>
                                    <th width="6%">Código</th>
                                    <th width="19%">Cliente</th>
                                    <th width="8%">Banco</th>
                                    <th width="8%">Agencia</th>
                                    <th width="9%">Conta</th>
                                    <th width="9%">Número</th>
                                    <th width="17%">Proprietário</th>
                                    <th width="9%">Valor</th>
                                    <th width="7%">Recebido</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div> 
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script> 
        <script type="text/javascript" src="../../js/util.js"></script>                      
        <script type="text/javascript">
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }
            
            $(document).ready(function () {
                $("#btnfind").click(function () {
                    tbLista.fnReloadAjax(finalFind(0));
                });
            });

            function finalFind(imp) {
                var retPrint = (imp === 1 ? '&imp=1' : '?imp=0');
                if ($("#txtTipo").val() !== "null") {
                    retPrint = retPrint + '&id=' + $("#txtTipo").val();
                }
                if ($("#txtBusca").val() !== "") {
                    retPrint = retPrint + '&Search=' + $("#txtBusca").val();
                }
                return "Cheques_server_processing.php" + retPrint.replace(/\//g, "_");
            }
            
            function imprimir() {
                var pRel = "&NomeArq=" + "Cheques" +
                        "&lbl=Data|Banco|Agencia|Conta|Número|Proprietário|Valor|Recebido" +
                        "&siz=70|80|80|80|90|170|70|60" +
                        "&pdf=8" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/Financeiro/" + finalFind(1); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }

            var tbLista = $('#tblLista').dataTable({
                "iDisplayLength": 20,
                "aLengthMenu": [20, 30, 40, 50, 100],
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "aaSorting": [[0, 'asc']],
                "aoColumns": [
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false}],
                "sAjaxSource": finalFind(0),
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"},
                "sPaginationType": "full_numbers"
            });            
        </script>             
        <?php odbc_close($con); ?>
    </body>
</html>