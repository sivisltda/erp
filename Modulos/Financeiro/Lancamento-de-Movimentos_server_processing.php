<?php

if (!isset($_GET['pdf'])) {
    include "../../Connections/configini.php";
    require_once('../../util/util.php');
}

$aColumns = array('id_lancamento_movimento', 'numero_filial', 'sf_contas_movimento.tipo tipo', 'razao_social', 'sf_grupo_contas.descricao gdescricao', 'sf_contas_movimento.descricao cdescricao', 'historico', 'sf_tipo_documento.descricao tdescricao', 'nome_fantasia', 'uv', 'mp', 'ea');
$iTotal = 0;
$iFilteredTotal = 0;
$sOrder2 = " ORDER BY total_aberto asc ";
$mdl_aca_ = returnPart($_SESSION["modulos"], 6);
$whereContrato1 = '';
$wherePlan2 = '';
$sLimit = 20;
$havingContrat = "";
$sWhere = "";
$parcelaAberto = '';
$queryPlano = "";
$totalAtivos = 0;
$mensal = 0;
$tiketMedio = 0;
$saldoAberto = 0;
$saldoPago = 0;
$imprimir = 0;

function redirectPageLink($tipo, $acad) {
    if ($tipo == "C" && $acad == 1) {
        return "Academia/ClientesForm.php";
    } elseif ($tipo == "E") {
        return "Financeiro/FuncionariosForm.php";
    } elseif ($tipo == "F") {
        return "Financeiro/Fornecedores-de-despesas.php";
    } else {
        return "CRM/CRMForm.php";
    }
}

if ($_GET['im'] != '') {
    $imprimir = $_GET['im'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if ($_GET['tp'] == "0" || $_GET['tp'] == "5") {
    $whereContrato1 .= " and cm.tipo = 'C'";
} elseif ($_GET['tp'] == "1" || $_GET['tp'] == "6") {
    $whereContrato1 .= " and cm.tipo = 'D'";
}

if (isset($_GET['status'])) {
    $whereContrato1 .= " and l.inativo = " . $_GET['status'];
    if ($_GET['status'] == 0) {
        $wherePlan2 .= " and dt_cancelamento is null";
    } else {
        $wherePlan2 .= " and dt_cancelamento is not null";
    }
}

if (isset($_GET['statusap'])) {   
    if ($_GET['statusap'] == "0") {
        $whereContrato1 .= " AND status = 'Novo'";
    } elseif ($_GET['statusap'] == "1") {
        $whereContrato1 .= " AND status = 'Aguarda'";
    } elseif ($_GET['statusap'] == "2") {
        $whereContrato1 .= " AND status = 'Aprovado'";
    } elseif ($_GET['statusap'] == "3") {
        $whereContrato1 .= " AND status = 'Reprovado'";
    }
}

if (isset($_GET['idsrc'])) {
    $whereContrato1 .= " and f.id_fornecedores_despesas = " . (is_numeric($_GET['idsrc']) ? $_GET['idsrc'] : 0);
    $wherePlan2 .= " and f.id_fornecedores_despesas = " . (is_numeric($_GET['idsrc']) ? $_GET['idsrc'] : 0);
}

if (isset($_GET['users'])) {
    $whereContrato1 .= " and f.id_user_resp in('" . str_replace(",", "','", $_GET['users']) . "')";
    $wherePlan2 .= " and f.id_user_resp in('" . str_replace(",", "','", $_GET['users']) . "')";
}

if (isset($_GET['valIni'])) {
    $whereContrato1 .= " and valor_parcela between " . valoresNumericos2($_GET['valIni']) . " and " . valoresNumericos2($_GET['valEnd']);
    $wherePlan2 .= " and valor_mens between " . valoresNumericos2($_GET['valIni']) . " and " . valoresNumericos2($_GET['valEnd']);
}

if ($_GET['txtBusca'] != "") {
    $whereContrato1 .= " and (f.razao_social like '%" . $_GET['txtBusca'] . "%' or f.nome_fantasia like '%" . $_GET['txtBusca'] . "%' or gp.descricao like '%" . $_GET['txtBusca'] . "%' or cm.descricao like '%" . $_GET['txtBusca'] . "%' or l.historico like '%" . $_GET['txtBusca'] . "%')";
    $wherePlan2 .= " and (f.razao_social like '%" . $_GET['txtBusca'] . "%' or f.nome_fantasia like '%" . $_GET['txtBusca'] . "%' or p.descricao like '%" . $_GET['txtBusca'] . "%')";
}

if (isset($_GET['dtPlanInit'])) {
    $havingContrat .= " having min(data_vencimento) between " . valoresData2($_GET['dtPlanInit']) . " and " . valoresData2($_GET['dtPlanInitEnd']) ;
    $wherePlan2 .= " and dt_inicio between " . valoresData2($_GET['dtPlanInit']) . " and " . valoresData2($_GET['dtPlanInitEnd']);
}

if (isset($_GET['dtPlanFimInit'])) {
    $condAnd = "";
    if (isset($_GET['dtPlanInit'])) {
        $condAnd = " and ";
    } else {
        $condAnd = " having ";
    }
    $havingContrat .= $condAnd . "max(data_vencimento) between " . valoresData2($_GET['dtPlanFimInit']) . " and " . valoresData2($_GET['dtPlanFimEnd']);
    $wherePlan2 .= " and dt_fim between " . valoresData2($_GET['dtPlanFimInit']) . " and " . valoresData2($_GET['dtPlanFimEnd']);
}

if ($_GET["tp"] != "") {
    $myTp = $_GET["tp"];
} else {
    $myTp = "-1";
}

if (isset($_GET['filial'])) {
    $whereContrato1 .= " and l.empresa = " . $_GET['filial'];
}

$queryBase = "select l.id_lancamento_movimento,l.empresa,deb_cre,f.id_fornecedores_despesas,razao_social,f.tipo,gp.descricao gdescricao,cm.descricao,
max(data_vencimento) ult_venc, max(valor_parcela) maior_parc,min(valor_parcela) menor_parc, max(historico) historico, l.status,
sum(case when lp.inativo = 0 and data_pagamento is null then valor_parcela else 0 end) valor_aberto,sum(lp.valor_pago) pago, td.descricao tdescricao,
SUM(case when lp.inativo = 0 and data_pagamento is null then 1 else 0 end) parcela_aberto,
SUM(case when lp.inativo = 0 and month(getdate()) = month(data_vencimento) and year(getdate()) = YEAR(data_vencimento) then valor_parcela else 0 end) parcela_mes,
(case when max(data_vencimento) < getdate() or max(data_vencimento) is null then 1 else 0 end) cor
from sf_lancamento_movimento l left join sf_lancamento_movimento_parcelas lp on l.id_lancamento_movimento = lp.id_lancamento_movimento
inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = l.fornecedor_despesa
inner join sf_grupo_contas gp on gp.id_grupo_contas = l.grupo_conta
inner join sf_contas_movimento cm on cm.id_contas_movimento = l.conta_movimento
inner join sf_tipo_documento td on td.id_tipo_documento = l.tipo_documento
where l.id_lancamento_movimento > 0 " . $whereContrato1 . ($_GET['tp'] == 5 || $_GET['tp'] == 6 ? " and l.status = 'Aprovado' " : "") . "  
group by l.id_lancamento_movimento,l.empresa,deb_cre,f.id_fornecedores_despesas,razao_social,f.tipo,
gp.descricao,cm.descricao, td.descricao, l.status" . $havingContrat;
//echo $queryBase;exit;
$query = "set dateformat dmy;SELECT * FROM(SELECT ROW_NUMBER() OVER (order by cor desc,ult_venc asc,valor_aberto asc) as row,*  from (";
$query .= $queryBase;
$query .= ") as x) as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " order by cor desc,ult_venc asc,valor_aberto asc";

$queryTotal = "set dateformat dmy;select count(id_lancamento_movimento) total, sum(case when tem.pago is null then 0 else tem.pago end) pago, sum(case when tem.valor_aberto is null then 0 else tem.valor_aberto end) valor, sum(parcela_mes) parcela_mes from(";
$queryTotal .= $queryBase;
$queryTotal .= ") as tem";

if ($_GET['tp'] == 5 || $_GET['tp'] == 6) {
    $cur2 = odbc_exec($con, $queryTotal);
    while ($RFP = odbc_fetch_array($cur2)) {
        $totalAtivos = $totalAtivos + $RFP['total'];
        $mensal = $mensal + $RFP['parcela_mes'];
        $saldoAberto = $saldoAberto + $RFP["valor"];
        $saldoPago = $saldoPago + $RFP["pago"];
    }
    $tiketMedio = ($totalAtivos > 0 ? $mensal / $totalAtivos : 0);
    $output = array(
        "Ativos" => $totalAtivos,
        "Mensal" => escreverNumero($mensal), "TiketMedio" => escreverNumero($tiketMedio),
        "EmAberto" => escreverNumero($saldoAberto), "Pago" => escreverNumero($saldoPago));
    echo json_encode($output);
    exit();
}

$cur2 = odbc_exec($con, $queryTotal);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $iTotal + $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iTotal,
    "aaData" => array()
);

$res = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($res)) {
    $row = array();
    if ($RFP['cor'] == 1) {
        $aling = "color:orange;";
    } else {
        $aling = "";
    }
    if ($imprimir == 0) {
        if (!isset($_GET['idsrc'])) {
            $row[] = "<center><input type=\"checkbox\" class=\"itens contrat\" onclick=\"clickCheck()\" value='" . utf8_encode($RFP['id_lancamento_movimento']) . "'/></center>";
        }
        $row[] = "<center><a title=\"Parcelas\" href=\"javascript:void(0)\" onClick=\"AbrirBox(" . ($RFP['deb_cre'] != "P" ? "20" : "30") . "," . $myTp . "," . utf8_encode($RFP['id_lancamento_movimento']) . ")\"><center><img src='../../img/1367947965_schedule.png' width='16' height='16'></center></a></center>";
    }
    $row[] = "<center><div id='formPQ' style='" . $aling . "' title='" . utf8_encode($RFP['empresa']) . "'>" . str_pad($RFP['empresa'], 3, "0", STR_PAD_LEFT) . "</div></center>";
    $row[] = "<center><div id='formPQ' style='" . $aling . "' title='" . utf8_encode($RFP['deb_cre']) . "'>" . utf8_encode($RFP['deb_cre']) . "</div></center>";
    $nome = utf8_encode($RFP['razao_social']);
    if (utf8_encode($RFP['nome_fantasia']) != "") {
        $nome = $nome . " (" . utf8_encode($RFP['nome_fantasia']) . ")";
    }
    if (utf8_encode($RFP['cnpj']) != "") {
        $nome .= " (" . utf8_encode($RFP['cnpj']) . ")";
    }
    if (!isset($_GET['idsrc'])) {
        $row[] = "<div id='formPQ'><a href='./../" . redirectPageLink($RFP['tipo'], $mdl_aca_) . "?id=" . $RFP['id_fornecedores_despesas'] . "' >" . $nome . "</a></div>";
    } else {
        $row[] = "<div id='formPQ' style='" . $aling . "' title='" . utf8_encode($RFP['descricao']) . "'>" . $nome . "</div>";
    }
    $row[] = "<div id='formPQ' style='" . $aling . "' title='" . utf8_encode($RFP['gdescricao']) . "'>" . utf8_encode($RFP['gdescricao']) . "</div>";
    if ($RFP['deb_cre'] == 'P') {
        $row[] = "<div id='formPQ' style='" . $aling . "' title='" . utf8_encode($RFP['descricao']) . "'>" . utf8_encode($RFP['descricao']) . "</div>";
    } else {
        $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(0," . $myTp . "," . utf8_encode($RFP['id_lancamento_movimento']) . ")'><div id='formPQ' title='" . utf8_encode($RFP['descricao']) . "'>" . utf8_encode($RFP['descricao']) . "</div></a>";
    }
    $row[] = "<div id='formPQ' style='" . $aling . "' title='" . utf8_encode($RFP['historico']) . "'>" . utf8_encode($RFP['historico']) . "</div>";
    $row[] = "<div id='formPQ' style='" . $aling . "' title='" . utf8_encode($RFP['tdescricao']) . "'>" . utf8_encode($RFP['tdescricao']) . "</div>";
    $row[] = "<center><div id='formPQ' style='" . $aling . "' title='" . escreverData($RFP['ult_venc']) . "'>" . escreverData($RFP['ult_venc']) . "</div></center>";
    $row[] = "<div id='formPQ' style='text-align:right;" . $aling . "' title='" . escreverNumero($RFP['maior_parc'], 1) . "'>" . escreverNumero($RFP['maior_parc'], 1) . "</div>";
    $row[] = "<div id='formPQ' style='text-align:right;" . $aling . "' title='" . escreverNumero($RFP['menor_parc'], 1) . "'>" . escreverNumero($RFP['menor_parc'], 1) . "</div>";
    $row[] = "<div id='formPQ' style='text-align:right;" . $aling . "' title='" . escreverNumero($RFP['valor_aberto'], 1) . "'>" . escreverNumero($RFP['valor_aberto'], 1) . "</div>";
    $color = "style=\"color:";
    if (utf8_encode($RFP['status']) == 'Novo') {
        $color = $color . "#bc9f00\"";        
    } elseif (utf8_encode($RFP['status']) == 'Aguarda') {
        $color = $color . "#bc9f00\"";
    } elseif (utf8_encode($RFP['status']) == 'Aprovado') {
        $color = $color . "#0066cc\"";
    } elseif (utf8_encode($RFP['status']) == 'Reprovado') {
        $color = $color . "#f00\"";
    } else {
        $color = $color . "black\"";
    }    
    $row[] = "<div id='formPQ' " . $color . " title=\"" . $titleStatus . "\"><center>" . utf8_encode($RFP['status']) . "</center></div>";
    $btnGerar = "";
    if ($RFP['deb_cre'] != 'P') {
        $btnGerar = "<a title=\"Gerar Parcelas\" href=\"javascript:void(0)\" onClick=\"AbrirBox(21," . $myTp . "," . utf8_encode($RFP['id_lancamento_movimento']) . ")\"><img src=\"../../img/parcelamento.PNG\" alt=\"Parcelar\" width=\"18\" height=\"18\" hspace=\"3\" title=\"Gerar Parcelas\"></a>";
    }
    if ($imprimir == 0 && $RFP['deb_cre'] !== "P") {
        $row[] = "<center><div style=\"margin-top:4px;" . $aling . "\">" . $btnGerar .
                (!isset($_GET['idsrc']) ? "<a title=\"Excluir\" href='Lancamento-de-Movimentos.php?Del=" . utf8_encode($RFP['id_lancamento_movimento']) . "&tp=" . $_GET['tp'] . "' ><input name='' type='image' onClick=\"return confirm('Deseja deletar esse registro?')\" src=\"../../img/1365123843_onebit_33 copy.PNG\" width='18' height='18' value='Enviar'></a>" : "") .
                "</div></center>";
    } else {
        $row[] = "";
    }
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
