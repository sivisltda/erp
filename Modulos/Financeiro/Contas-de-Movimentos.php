<?php
include '../../Connections/configini.php';
$imprimir = 0;
$toReturn = "?imp=0";
$F2 = "";
$F3 = "";
$tipo = "";

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = 1;
    $toReturn = "?imp=1";
}
if (is_numeric($_GET["tp"])) {
    $F2 = $_GET["tp"];
    $toReturn .= "&tp=" . $_GET["tp"];
    if ($_GET["tp"] == 0) {
        $tipo = "C";
    } else {
        $tipo = "D";
    }
}
if (is_numeric($_GET["gc"])) {
    $F3 = $_GET["gc"];
    $toReturn .= "&gc=" . $_GET["gc"];
}
if (is_numeric($_GET['Del'])) {
    odbc_exec($con, "DELETE FROM sf_contas_movimento WHERE id_contas_movimento =" . $_GET["Del"]);
    echo "<script>window.top.location.href = 'Contas-de-Movimentos.php" . $toReturn . "'; </script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script> 
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Administrativo<small>Subgrupos de Contas</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="margin-top: 15px;float:left;">
                                    <div style="float:left">
                                        <form method="POST">
                                            <button  class="button button-green btn-primary" type="button" onclick="AbrirBox(0)"><span class="ico-file-4 icon-white"></span></button>
                                            <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir()" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                        </form>
                                    </div>
                                </div>
                                <div style="float:right;width: 60%;display: flex;align-items: center;justify-content: flex-end;">
                                    <div style="float: left;width: 115px;margin-right: 5%;">
                                        <span>Tipo:</span>
                                        <select style="width:160px;" name="txtTipo" id="txtTipo" class="select">
                                            <option value="null">--Selecione--</option>
                                            <option value="C" <?php
                                            if ($F2 == "0") {
                                                echo "SELECTED";
                                            }
                                            ?>>Crédito</option>
                                            <option value="D" <?php
                                            if ($F2 == "1") {
                                                echo "SELECTED";
                                            }
                                            ?>>Débito</option>
                                        </select>
                                    </div>
                                    <div style="width: 115px;margin-right: 5%; margin-left: 1%;">
                                        <span>Grupo:</span>
                                        <span id="carregando" name="carregando" style="color:#656; display:none">Aguarde, carregando...</span>
                                        <select name="txtGrupo" id="txtGrupo" class="select">
                                            <option value="null">Selecione o grupo</option>
                                            <?php
                                            if ($F2 != "") {
                                                $sql = "select id_grupo_contas,descricao from sf_grupo_contas where deb_cre = '" . $tipo . "' ORDER BY descricao";
                                                $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                                    <option value="<?php echo $RFP['id_grupo_contas'] ?>"<?php
                                                    if (!(strcmp($RFP['id_grupo_contas'], $F3))) {
                                                        echo "SELECTED";
                                                    }
                                                    ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                    <?php
                                                }
                                            } ?>
                                        </select>
                                    </div>                                        
                                    <div style="float:left;margin-left: 1%;">                                    
                                        <div width="100%">Estado:</div>
                                        <select id="txtStatus" class="select">
                                            <option value="0" selected>Ativos</option>                                        
                                            <option value="1">Inativos</option>                                                                            
                                        </select>                                           
                                    </div>
                                    <div style="float:left;margin-left: 1%;">
                                        <div width="100%">Busca:</div>
                                        <input name="txtBuscar" id="txtBuscar" maxlength="100" type="text" style="width:175px;" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title="Nome / CPF/ CNPJ / Endereço / Bairro / Cidade / Contato"/>
                                    </div>
                                    <div style="margin-top: 15px;float:left;margin-left:0.3%;">
                                        <div style="float:left">
                                            <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" onclick="refresh()" title="Buscar"><span class="ico-search icon-white"></span></button>
                                        </div>
                                    </div>
                                </div>                                        
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead" <?php echo $visible; ?>>
                        <div class="boxtext">Subgrupos de Contas</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblContasMovimento">
                            <thead>
                                <tr>
                                    <th width="3%"><center>D/C</center></th>
                                    <th width="22%">Descrição do grupo</th>
                                    <th width="40%">Descrição do Subgrupo de Conta</th>
                                    <th width="10%"><center>Tolerância</center></th>
                                    <th width="10%"><center>% Multa</center></th>
                                    <th width="10%"><center>% Juros AD</center></th>
                                    <th width="6%"><center>Ação:</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="5" class="dataTables_empty">Carregando dados ...</td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>          
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script> 
        <script type="text/javascript" src="../../js/util.js"></script>      
        <script type="text/javascript">
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    refresh();
                }
            }
            
            $("#txtBuscar").click(function () {
                refresh();
            });
            
            $("#txtTipo").change(function () {
                if ($(this).val()) {
                    $("#txtGrupo").val("null");
                    $.getJSON("grupo.ajax.php?search=", {txtTipo: $(this).val(), ajax: "true"}, function (j) {
                        var options = "<option value='null'>Selecione o grupo</option>";
                        for (var i = 0; i < j.length; i++) {
                            options += "<option value='" + j[i].id_grupo_contas + "'>" + j[i].descricao + "</option>";
                        }
                        $("#txtGrupo").html(options);
                    });
                }
            });
            
            var columns = [{"bSortable": true},
                {"bSortable": true},
                {"bSortable": true},
                {"bSortable": true},
                {"bSortable": true},
                {"bSortable": true},
                {"bSortable": false}];
            $('#tblContasMovimento').dataTable({
                "iDisplayLength": 20,
                "aLengthMenu": [20, 30, 40, 50, 100],
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": finalFind(0),
                "bFilter": false,
                "aoColumns": columns,
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"},
                "sPaginationType": "full_numbers"
            });

            function AbrirBox(id) {
                abrirTelaBox("FormContas-de-Movimentos.php" + (id > 0 ? "?id=" + id : ""), 315, 500);
            }

            function refresh() {
                var oTable = $('#tblContasMovimento').dataTable();
                oTable.fnReloadAjax(finalFind(0));
            }

            function finalFind(imp) {
                var retPrint = "?imp=" + imp;
                if ($("#txtStatus").val() !== "null") {
                    retPrint = retPrint + '&at=' + $("#txtStatus").val();
                }                
                if ($('#txtTipo').val() !== "null") {
                    retPrint = retPrint + "&tp=" + $("#txtTipo").val();
                }
                if ($('#txtGrupo').val() !== "null") {
                    retPrint = retPrint + "&gc=" + $("#txtGrupo").val();
                }
                if ($('#txtBuscar').val() !== "") {
                    retPrint = retPrint + "&Search=" + $("#txtBuscar").val();
                }
                return "./../Financeiro/Contas-de-Movimentos_server_processing.php" + retPrint;
            }

            function FecharBox() {
                $("#newbox").remove();
                refresh();
            }

            function imprimir() {
                var pRel = "&NomeArq=" + "Subgrupos de Contas" +
                "&lbl=" + "D/C|Grupo|Subgrupo|Tolerância|Multa|Juros" +
                "&siz=" + "40|200|250|70|70|70" +
                "&pdf=" + "6" + // Colunas do server processing que não irão aparecer no pdf
                "&filter=" + "Cenários " + //Label que irá aparecer os parametros de filtro
                "&PathArqInclude=" + finalFind(1).replace("?", "&").replace("./../", "../Modulos/"); // server processing            
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>