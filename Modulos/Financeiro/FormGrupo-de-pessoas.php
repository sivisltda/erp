<?php
include '../../Connections/configini.php';

$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_grupo_cliente set " .
        "descricao_grupo = " . valoresTexto("txtdescricao_grupo") . "," .
        "inativo_grupo = " . valoresSelect("txtInativo") . "," .
        "tipo_grupo = " . valoresTexto("txtTipo") . " where id_grupo = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_grupo_cliente(descricao_grupo, inativo_grupo,tipo_grupo)values(" .
        valoresTexto("txtdescricao_grupo") . "," .
        valoresSelect("txtInativo") . "," .
        valoresTexto("txtTipo") . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_grupo from sf_grupo_cliente order by id_grupo desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_grupo_cliente WHERE id_grupo = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_grupo_cliente where id_grupo =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_grupo'];
        $tipo = $RFP['tipo_grupo'];
        $descricao_grupo = $RFP['descricao_grupo'];
        $inativo_grupo = $RFP['inativo_grupo'];
    }
} else {
    $disabled = '';
    $id = '';
    $tipo = '';
    $descricao_grupo = '';
    $inativo_grupo = '';
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<body>
    <form action="FormGrupo-de-pessoas.php" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Grupos</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont">
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <div style="width: 100%;float: left">
                <span>Descrição:</span>
                <input name="txtdescricao_grupo" id="txtdescricao_grupo" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="128" value="<?php echo utf8_encode($descricao_grupo); ?>"/>
            </div>
            <div style="clear:both;height: 5px;"></div>
            <div style="width: 50%;float: left">
                <span>Utilização:</span><br>
                <select class="input-medium" <?php echo $disabled; ?> id="txtTipo" name="txtTipo">
                    <option value="C" <?php
                    if (!(strcmp('C', $tipo))) {
                        echo "SELECTED";
                    }
                    ?>>Clientes</option>
                    <option value="F" <?php
                    if (!(strcmp('F', $tipo))) {
                        echo "SELECTED";
                    }
                    ?>>Fornecedores</option>
                    <option value="E" <?php
                    if (!(strcmp('E', $tipo))) {
                        echo "SELECTED";
                    }
                    ?>>Funcionários</option>
                </select>
            </div>
            <div style="width: 49%;float: left; margin-left: 1%;">
                <span>Inativo:</span><br>
                <select class="input-medium" <?php echo $disabled; ?> id="txtInativo" name="txtInativo">
                    <option value="1" <?php if (1 == $inativo_grupo) { echo "SELECTED";} ?>>SIM</option>                                
                    <option value="0" <?php if (0 == $inativo_grupo) { echo "SELECTED";} ?>>NÃO</option>    
                </select>
            </div>
            <div style="clear:both;height: 5px;"></div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" name="bntSave" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK" ><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else {
                    ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <?php odbc_close($con); ?>
</body>
