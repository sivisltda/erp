$(document).ready(function () {
    $("#txtComProd, #txtComServ, #txtComPlano, #txtComGere, #txtComPlanoPri, #txtComGerePri").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
    trocaCref();
    $("#mscEquipe").multiSelect({
        selectableHeader: "<div class=\"multipleselect-header\">Equipe (Todos)</div>",
        selectionHeader: "<div class=\"multipleselect-header\">Equipe (Selecionados)</div>"
    });
    $("#mscGrupos").multiSelect({
        selectableHeader: "<div class='multipleselect-header'>Serviços (Todos)</div>",
        selectionHeader: "<div class='multipleselect-header'>Serviços (Selecionados)</div>"
    });
    $("#mscEquipeBlack").multiSelect({
        selectableHeader: "<div class='multipleselect-header'>Serviços (Todos)</div>",
        selectionHeader: "<div class='multipleselect-header'>Serviços (Selecionados)</div>"
    });

    $("#lblAtivo").hide();
    $("#lblInativo").hide();
    if ($('#txtId').val() !== "") {
        if ($("#txtDtDemissao").val() !== "") {
            $("#lblInativo").show();
        } else {
            $("#lblAtivo").show();
        }
        listContatos($('#txtId').val());
        $('#tblAtendimentos').dataTable({
            "iDisplayLength": 20,
            "aLengthMenu": [20, 30, 40, 50, 100],
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "form/FuncionarioFormServer.php?listAtendimento=S&txtId=" + $("#txtId").val(),
            "bFilter": false,
            "bSort": false,
            "bPaginate": false,
            'oLanguage': {
                'sInfo': "",
                'sInfoEmpty': "",
                'sInfoFiltered': "",
                'sLengthMenu': "",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sSearch': "Pesquisar:",
                'sZeroRecords': "Não foi encontrado nenhum resultado"},
            "sPaginationType": "full_numbers"
        });
    }
    TrataEnableCampos();
    $(document).on('click', '#btnLink', function (event) {
        event.preventDefault();
        $('#txtLinkFunc').removeAttr("disabled");        
        $('#txtLinkFunc').select();
        $(this).tooltip({
            placement: 'top',
            title: 'Copiado'
        });
        document.execCommand("copy");
    });
});

function FecharBox(opc) {
    $("#newbox").remove();
}

$('#txtGrupo').change(function () {
    trocaCref();
});

function trocaCref() {
    if ($('#txtGrupo').val() === "16") {
        $("#txtCref").parent().show();
    } else {
        $("#txtCref").parent().hide();
    }
}

function trataCheck() {
    if ($("#selComProd").val() !== 0 || $("#selComServ").val() !== 0 || $("#selComProd").val() !== 0 || ($("#selComPlano").length && $("#selComPlano").val() !== 0)) {
        $("#txtRecebeComissao").select2("val", "1");
    }
}

$("#selComProd").change(function () {
    trataCheck();
    if ($(this).val() === "2") {
        $("#txtComProd").prop("disabled", false);
    } else {
        $("#txtComProd").prop("disabled", true);
    }
});

$("#selComServ").change(function () {
    trataCheck();
    if ($(this).val() === "2") {
        $("#txtComServ").prop("disabled", false);
    } else {
        $("#txtComServ").prop("disabled", true);
    }
});

$("#selComGere").change(function () {
    trataCheck();
    if ($(this).val() === "2") {
        $("#txtComGere, #txtComGerePri").prop("disabled", false);
    } else {
        $("#txtComGere, #txtComGerePri").prop("disabled", true);
    }
});

$("#selComPlano").change(function () {
    trataCheck();
    if ($(this).val() === "2") {
        $("#txtComPlano, #txtComPlanoPri").prop("disabled", false);
    } else {
        $("#txtComPlano, #txtComPlanoPri").prop("disabled", true);
    }
});

function AtualizaTblAtendimento() {
    var tblAtendimentos = $("#tblAtendimentos").dataTable();
    tblAtendimentos.fnReloadAjax("form/FuncionarioFormServer.php?listAtendimento=S&txtId=" + $("#txtId").val());
}

// <editor-fold defaultstate="collapsed" desc="Botoes">

$('#btnNovo').click(function (e) {
    window.location = "FuncionariosForm.php";
});

$('#btnSave').click(function (e) {
    if ($("#txtDtDemissao").val() !== "") {
        if (validaForm()) {
            if ($('#txtLojaStatus').val() !== "") {
                $("#btnSave").hide();
            }
            $.post("form/FuncionarioFormServer.php", "isAjax=S&btnSave=S&txtId=" + $("#txtId").val() + "&txtNome=" + $("#txtNome").val() + "&" + $(".tab-content :input").serialize()).done(function (data) {
                if (!isNaN(data)) {
                    window.location = "FuncionariosForm.php?id=" + data;
                } else {
                    $("#loader").hide();
                    bootbox.alert(data);
                }
            });
        }
    } else {
        if (validaForm()) {
            $("#loader").show();
            if ($('#txtLojaStatus').val() !== "") {
                $("#btnSave").hide();
            }
            $.post("form/FuncionarioFormServer.php", "isAjax=S&btnSave=S&txtId=" + $("#txtId").val() + "&txtNome=" + $("#txtNome").val() + "&" + $(".tab-content :input").serialize()).done(function (data) {
                if (!isNaN(data)) {
                    window.location = "FuncionariosForm.php?id=" + data;
                } else {
                    $("#loader").hide();
                    bootbox.alert(data);
                }
            });
        }
    }
});

$("#btnExcluir").click(function (e) {
    bootbox.confirm('Confirma a demissão do funcionário?', function (result) {
        if (result === true) {
            $.post("form/FuncionarioFormServer.php", "isAjax=S&verificaPendencia=S&txtId=" + $("#txtId").val()).done(function (data) {
                if (data.trim() > 0) {
                    $("#loader").hide();
                    bootbox.alert("Existem pendencias em aberto com o usuario, transfira para outro usuario antes de prosseguir!");
                } else {
                    $.post("form/FuncionarioFormServer.php", "isAjax=S&excluirFunc=S&txtId=" + $("#txtId").val()).done(function (data) {
                        $("#loader").hide();
                        if (data.trim() !== "YES") {
                            bootbox.alert("Erro ao atualizar registro!");
                        } else {
                            window.location = "Funcionarios.php";
                        }
                    });
                }
            });
        }
    });
});

$("#btnTransfPendencias").click(function (e) {
    bootbox.confirm('Essa transferecia nao poderá ser desfeita. Confirma a transferencia das pendências em aberto?', function (result) {
        if (result === true) {
            $("#loader").show();
            $.post("form/FuncionarioFormServer.php", "isAjax=S&transfPendencia=S&txtIdDe=" + $("#txtId").val() + "&txtIdPara=" + $("#txtTransfPendencias").val()).done(function (data) {
                $("#loader").hide();
                if (data.trim() !== "YES") {
                    bootbox.alert("Erro ao atualizar registro!");
                } else {
                    AtualizaTblAtendimento();
                }
            });
        }
    });
});

$("#btnTransfResponsavel").click(function (e) {
    bootbox.confirm('Essa transferecia nao poderá ser desfeita. Confirma a transferencia do Usuário Responsável?', function (result) {
        if (result === true) {
            $("#loader").show();
            $.post("form/FuncionarioFormServer.php", "isAjax=S&transfResponsavel=S&txtIdDe=" + $("#txtId").val() + "&txtIdPara=" + $("#txtTransfResponsavel").val()).done(function (data) {
                $("#loader").hide();
                if (data.trim() !== "YES") {
                    bootbox.alert("Erro ao atualizar registro!");
                } else {
                    bootbox.alert("Transferência realizada com sucesso!");
                }
            });
        }
    });
});

$("#btnTransfResponsavelLead").click(function (e) {
    bootbox.confirm('Essa transferecia nao poderá ser desfeita. Confirma a transferencia do Usuário Responsável?', function (result) {
        if (result === true) {
            $("#loader").show();
            $.post("form/FuncionarioFormServer.php", "isAjax=S&transfLead=S&txtIdDe=" + $("#txtId").val() + "&txtIdPara=" + $("#txtTransfLead").val()).done(function (data) {
                $("#loader").hide();
                if (data.trim() !== "YES") {
                    bootbox.alert("Erro ao atualizar registro!");
                } else {
                    bootbox.alert("Transferência realizada com sucesso!");
                }
            });
        }
    });
});

$("#btnTransfComissaoCliente").click(function (e) {
    bootbox.confirm('Essa transferecia nao poderá ser desfeita. Confirma a transferencia da Comissão por Cliente do Usuário?', function (result) {
        if (result === true) {
            $("#loader").show();
            $.post("form/FuncionarioFormServer.php", "isAjax=S&transfComissaoCliente=S&txtIdDe=" + $("#txtId").val() + "&txtIdPara=" + $("#txtTransfComissaoCliente").val()).done(function (data) {
                $("#loader").hide();
                if (data.trim() !== "YES") {
                    bootbox.alert("Erro ao atualizar registro!");
                } else {
                    bootbox.alert("Transferência realizada com sucesso!");
                }
            });
        }
    });
});

//</editor-fold>

// <editor-fold defaultstate="collapsed" desc="Foto">
function UploadForm() {
    if ($("#txtId").val() !== "") {
        bootbox.dialog({
            message: "Importar Fotos",
            title: "",
            buttons: {
                success: {
                    label: "Camera",
                    className: "btn-success",
                    callback: function () {
                        AbrirBox(1);
                    }
                },
                main: {
                    label: "Arquivo",
                    className: "btn-primary",
                    callback: function () {
                        AbrirBox(2);
                    }
                }
            }
        });
    }
}

$('#btnRemImg').click(function (e) {
    bootbox.confirm('Confirma a exclusão da imagem?', function (result) {
        if (result === true) {
            $.post("../../Modulos/Academia/form/ClientesFormServer.php", "isAjax=S&RemoverImg=" + $("#txtId").val()).done(function (data) {
                if (data.trim() === "YES") {
                    $("#ImgCliente").attr("src", "./../../img/img.png");
                    $("#btnRemImg").hide();
                } else {
                    alert(data);
                }
            });
        } else {
            return;
        }
    });
});

function AbrirBox(opc) {
    if (opc === 1) {
        document.location = "../Academia/ClientesWebCam.php?id=" + $("#txtId").val();
    } else if (opc === 2) {
        abrirTelaBox("../Academia/ClientesCropModalForm.php", 539, 770);
    }
}
//</editor-fold>