
$(document).ready(function () {
    if ($("#txtEmpresa").is('[disabled]')) {
        $("#btnDevolucao").show();
    } else {
        $("#btnDevolucao").hide();
    }
    if ($("#txtId").val() !== "") {
        listaProdutos();
    }
    $('#bntDelete').click(function (e) {
        bootbox.confirm('Confirma a exclusão do registro?', function (result) {
            if (result === true) {
                $.post("form/LancamentoMovimentoFormServer.php", "isAjax=S&bntDelete=" + $("#txtId").val()).done(function (data) {
                    if (data.trim() === "YES") {
                        bootbox.alert('Registro Excluído.', function () {
                            parent.FecharBox(0);
                        });
                    } else {
                        alert(data);
                    }
                });
            } else {
                return;
            }
        });
    });

    $("#txtProduto").change(function () {
        if ($(this).val()) {
            $.getJSON("./../../Modulos/Estoque/conta_a.ajax.php?search=", {txtConta: $(this).val(), txtFilial: $("#txtEmpresa").val(), ajax: "true"}, function (j) {
                var grupo = "null";
                var preco = numberFormat(0);
                var qtdcom = "1";
                for (var i = 0; i < j.length; i++) {
                    if (j[i].nao_vender_sem_estoque === "1" && j[i].estoque_atual <= 0) {
                        bootbox.alert("Produto sem estoque!");
                        $("#txtProduto").select2("val", "null");
                    } else {
                        if (j[i].preco_venda !== "") {
                            preco = j[i].preco_venda;
                        }
                        if (j[i].quantidade_comercial !== "") {
                            if (parseFloat(j[i].quantidade_comercial) > 0) {
                                qtdcom = parseInt(j[i].quantidade_comercial);
                            } else {
                                qtdcom = 1;
                            }
                        }
                    }

                }
                $("#txtQtdP").val(qtdcom);
                $("#txtValUniP").val(numberFormat(preco));
                $("#txtValToTP").val(numberFormat(preco));
            });
        } else {
            $("#txtQtdP").html("1");
            $("#txtValUniP").html(numberFormat(0));
            $("#txtValToTP").html(numberFormat(0));
        }
    });

    $("#txtServico").change(function () {
        if ($(this).val()) {
            $.getJSON("./../../Modulos/Estoque/conta_a.ajax.php?search=", {txtConta: $(this).val(), ajax: "true"}, function (j) {
                var grupo = "null";
                var preco = numberFormat(0);
                var qtdcom = "1";
                for (var i = 0; i < j.length; i++) {
                    if (j[i].preco_venda !== "") {
                        preco = j[i].preco_venda;
                    }
                    if (j[i].quantidade_comercial !== "") {
                        if (parseFloat(j[i].quantidade_comercial) > 0) {
                            qtdcom = parseInt(j[i].quantidade_comercial);
                        } else {
                            qtdcom = 1;
                        }
                    }
                }
                $("#txtQtdS").val(qtdcom);
                $("#txtValUniS").val(numberFormat(preco));
                $("#txtValToTS").val(numberFormat(preco));
            });
        } else {
            $("#txtQtdS").html("1");
            $("#txtValUniS").html(numberFormat(0));
            $("#txtValToTS").html(numberFormat(0));
        }
    });

    $("#txtTipoGrupo").change(function () {
        if ($(this).val()) {
            $("#txtConta").select2("val", "");
            if ($(this).val() === "C") {
                $("#lblFonecedor").html("Cliente:").show();
            } else {
                $("#lblFonecedor").html("Fornecedor/Funcionário:").show();
            }
            $.getJSON("grupo.ajax.php?search=", {txtTipo: $(this).val(), ajax: "true"}, function (j) {
                var options = "<option value=''>--Selecione--</option>";
                for (var i = 0; i < j.length; i++) {
                    options += "<option value='" + j[i].id_grupo_contas + "'>" + j[i].descricao + "</option>";
                }
                $("#txtGrupo").html(options);
            });
            $.getJSON("fornecedor.ajax.php?search=", {txtTipo: $(this).val(), ajax: "true"}, function (j) {
                var options = "<option value=''>--Selecione--</option>";
                for (var i = 0; i < j.length; i++) {
                    options += "<option value='" + j[i].id_grupo_contas + "'>" + j[i].descricao + "</option>";
                }
                $("#txtFonecedor").html(options);
            });
            $.getJSON("tipo.ajax.php?search=", {txtTipo: $(this).val(), ajax: "true"}, function (j) {
                var options = "<option value=''>--Selecione--</option>";
                for (var i = 0; i < j.length; i++) {
                    options += "<option value='" + j[i].id_grupo_contas + "'>" + j[i].descricao + "</option>";
                }
                $("#txtTipo").html(options);
            });
        } else {
            $("#txtGrupo").html("<option value=''>--Selecione--</option>");
            $("#txtConta").html("<option value=''>--Selecione--</option>");
            $("#txtTipo").html("<option value=''>--Selecione--</option>");
            $("#lblFonecedor").html("Fornecedor/Funcionário:").show();
        }
    });

    $("#txtGrupo").change(function () {
        $("#txtConta").select2("val", "");
        if ($(this).val()) {
            $.getJSON("conta.ajax.php?search=", {txtGrupo: $(this).val(), ajax: "true"}, function (j) {
                var options = "<option value=''>--Selecione--</option>";
                for (var i = 0; i < j.length; i++) {
                    options += "<option value='" + j[i].id_contas_movimento + "'>" + j[i].descricao + "</option>";
                }
                $("#txtConta").html(options);
            });
        } else {
            $("#txtConta").select2("val", "");
        }
    });
    
    $('#btnDevolucao').click(function (e) {
        var itens = [];
        $('#tbProdutos > tbody  > tr').each(function () {
            if ($('>td', $(this)).hasClass("selected")) {
                itens.push($('>td:nth-child(2)', $(this)).find('input[name="txtIP[]"]').val());
            }
        });
        if (itens.length === 0) {
            bootbox.alert("Selecione um produto na lista para devolução!");
        } else {
            bootbox.dialog({
                message: BootboxContent,
                title: "Informe a data de devolução",
                buttons: {
                    success: {
                        label: "Confirmar",
                        className: "btn-success",
                        callback: function () {
                            if ($('#txtDtDevolucao').val() !== "") {
                                $.post("form/LancamentoMovimentoFormServer.php", "isAjax=S&btnDevolucao=S&itens=" + itens + "&dtDevolucao=" + $('#txtDtDevolucao').val().replace(/\//g, "_")).done(function (data) {
                                    if (data.trim() !== "YES") {
                                        alert(data);
                                    }
                                }).done(function () {
                                    listaProdutos();
                                });
                            } else {
                                bootbox.alert("Para devolução é necessário informar uma data!")
                            }
                        }
                    },
                    main: {
                        label: "Cancelar",
                        className: "btn-primary",
                        callback: function () {}
                    }
                }
            });
        }
    });
});

function BootboxContent() {
    var frm_str = '<form id="some-form"><div class="form-group"><input id="txtDtDevolucao" class="date span2 form-control input-sm" size="16" placeholder="' + lang["dmy"] + '" type="text"></div></form>';
    var object = $('<div/>').html(frm_str).contents();
    object.find('#txtDtDevolucao').datepicker({
        dateFormat: lang["dPickerFormat"],
        autoclose: true
    }).on('changeDate', function (ev) {
        $(this).blur();
        $(this).datepicker('hide');
    });
    object.find('#txtDtDevolucao').datepicker('setDate', 'today');
    return object;
}

function listaProdutos() {
    $('#tbProdutos tbody').remove();
    $.getJSON('form/LancamentoMovimentoFormServer.php?isAjax=S', {listaItens: $("#txtId").val(), tpItem: 'P'}, function (j) {
        if (j !== null) {
            for (var i = 0; i < j.length; i++) {
                var valUnidade = textToNumber(j[i].valor_total) / textToNumber(j[i].quantidade);
                addlinha(i, j[i].id_item_lancamento_movimento, j[i].produto, j[i].quantidade, numberFormat(valUnidade), j[i].valor_total, j[i].produtodesc, j[i].dt_devolucao);
            }
        }
    }).done(function () {
        $('#tbProdutos tbody').on('click', 'tr', function () {
            if ($(this).find('td').hasClass('selected')) {
                $(this).find('td').removeClass('selected');
            } else {
                $(this).find('td').addClass('selected');
            }
        });
        somaTotal();
    });
}

function addlinha(conta, idItem, produto, quantidade, valUnidade, valTotal, descrProd, dtDevolucao) {
    var disabled = $("#txtEmpresa").is('[disabled]') ? "disabled" : "";
    var color = dtDevolucao !== "" ? "color:green;" : "";
    var title = dtDevolucao !== "" ? "title=\"Data de devolução: " + dtDevolucao + "\"" : "";
    $("#tbProdutos").append("<tr id=\"tabela_linha_" + conta + "\" style=\"" + color + "\" " + title + "><td style=\"display: none;\" class=\"item_count\" >" + conta + "</td><td class=\"lisbody\"><input name=\"txtIP[]\" type=\"hidden\" value=\"" + idItem + "\"><input name=\"txtPD[]\" type=\"hidden\" value=\"" + produto + "\"><input name=\"txtVQ[]\" type=\"hidden\" value=\"" + quantidade + "\"><input name=\"txtVU[]\" type=\"hidden\" value=\"" + valUnidade + "\"><input name=\"txtVP[]\" type=\"hidden\" value=\"" + valTotal + "\">" + descrProd + "</td><td class=\"lisbody\" width=\"50px\" style=\"text-align: center;\">" + quantidade + "</td><td class=\"lisbody\" width=\"80px\" style=\"text-align: right;\">" + valUnidade + "</td><td class=\"lisbody vtotal\" width=\"100px\" style=\"text-align: right;\">" + valTotal + "</td><td class=\"lisbody\" width=\"23px\"><center><input class=\"btn red\" type=\"button\" value=\"x\" " + disabled + " onclick=\"removeLinha('tabela_linha_" + conta + "')\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\"></center></td></tr>");
}

function novaLinha() {
    if ($("#txtProduto").val() === "null" || $("#txtQtdP").val() === "" || $("#txtValUniP").val() === "") {
        bootbox.alert("Escolha um produto para adicionar!");
    } else {
        var conta = 1;
        $("#tbProdutos").find("tr > .item_count").each(function () {
            if (parseFloat($(this).html()) >= conta) {
                conta = parseFloat($(this).html()) + 1;
            }
        });
        addlinha(conta, "", $("#txtProduto").val(), $("#txtQtdP").val(), $("#txtValUniP").val(), $("#txtValToTP").val(), $("#txtProduto option:selected").text(), "");
        somaTotal();
    }
}

function novaLinhaS() {
    if ($("#txtServico").val() === "null" || $("#txtQtdS").val() === "" || $("#txtValUniS").val() === "") {
        bootbox.alert("Escolha um serviço para adicionar!");
    } else {
        var conta = 1;
        $("#tbServicos").find("tr > .item_count").each(function () {
            if (parseFloat($(this).html()) >= conta) {
                conta = parseFloat($(this).html()) + 1;
            }
        });
        $("#tbServicos").append("<tr id=\"tabela_linhaS_" + conta + "\"><td style=\"display: none;\" class=\"item_count\">" + conta + "</td><td class=\"lisbody\"><input name=\"txtIPS[]\" type=\"hidden\" value=\"\"><input name=\"txtGRS[]\" type=\"hidden\" value=\"" + $("#txtGrupoS").val() + "\"><input name=\"txtPDS[]\" type=\"hidden\" value=\"" + $("#txtServico").val() + "\"><input name=\"txtVQS[]\" type=\"hidden\" value=\"" + $("#txtQtdS").val() + "\"><input name=\"txtVUS[]\" type=\"hidden\" value=\"" + $("#txtValUniS").val() + "\"><input name=\"txtVPS[]\" type=\"hidden\" value=\"" + $("#txtValToTS").val() + "\">" + $("#txtServico option:selected").text() + "</td><td class=\"lisbody\" width=\"50px\" style=\"text-align: right;\">" + $("#txtQtdS").val() + "</td><td class=\"lisbody\" width=\"80px\" style=\"text-align: right;\">" + $("#txtValUniS").val() + "</td><td class=\"lisbody vtotal\" width=\"100px\" style=\"text-align: right;\">" + $("#txtValToTS").val() + "</td><td class=\"lisbody\" width=\"23px\"><center><input class=\"btn red\" type=\"button\" value=\"x\" onclick=\"removeLinha('tabela_linhaS_" + conta + "')\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\"></center></td></tr>");
        somaTotal();
    }
}

function removeLinha(linha) {
    $("#" + linha).remove();
    somaTotal();
}

function somaTotal() {
    var totalProdutos = 0.00;
    var totalServicos = 0.00;
    $("#tbProdutos tr").each(function () {
        totalProdutos = totalProdutos + textToNumber($(this).find(".vtotal").html());
    });
    $("#tbServicos tr").each(function () {
        totalServicos = totalServicos + textToNumber($(this).find(".vtotal").html());
    });
    $("#totalP").html(numberFormat(totalProdutos, 1));
    $("#totalS").html(numberFormat(totalServicos, 1));
}

function prodValor() {
    var UpdQtd = textToNumber($("#txtQtdP").val());
    var UpdVal = textToNumber($("#txtValUniP").val());
    $("#txtValToTP").val(numberFormat(UpdVal * UpdQtd));
}

function prodValorS() {
    var UpdQtd = textToNumber($("#txtQtdS").val());
    var UpdVal = textToNumber($("#txtValUniS").val());
    $("#txtValToTS").val(numberFormat(UpdVal * UpdQtd));
}

function validaForm() {
    var erro = '';
    if ($("#txtGrupo").val() === 'null' || $("#txtGrupo").val() === '') {
        erro = "Campo grupo é obrigatório.";
    } else if ($("#txtConta").val() === 'null' || $("#txtConta").val() === '') {
        erro = "Campo conta é obrigatório.";
    } else if ($("#txtFonecedor").val() === '' || $("#txtFonecedor").val() === 'null') {
        erro = "Campo cliente é obrigatório.";
    } else if ($("#txtTipo").val() === '' || $("#txtTipo").val() === 'null') {
        erro = "Campo tipo de cobrança é obrigatório.";
    } else if ($("#txtHistorico").val() === '' || $("#txtHistorico").val() === 'null') {
        erro = "Campo histórico é obrigatório.";
    }
    if (erro === '') {
        var itens = [];
        $('#tbProdutos > tbody  > tr').each(function () {
            itens.push($(this).find('input[name="txtPD[]"]').val());
        });
        if (itens.length > 0) {
            $.post("./../../Modulos/Estoque/form/ProdutosServer.php", "isAjax=S&VerEstoqueListaProdutos=S&Itens=" + itens + "&txtFilial=" + $("#txtEmpresa").val()).done(function (data) {
                if (data.trim() !== "") {
                    bootbox.alert("O produto: " + data + " não possui estoque disponível");
                } else {
                    $('#bntSave').click();
                }
            });
        } else {
            $('#bntSave').click();
        }
    } else {
        bootbox.alert(erro);
    }
}

$("#txtFonecedorNome").autocomplete({
    source: function (request, response) {
        $.ajax({
            url: "../../Modulos/CRM/ajax.php",
            dataType: "json",
            data: {q: request.term, t: "C','E','F"},
            success: function (data) {
                response(data);
            }
        });
    }, minLength: 3,
    select: function (event, ui) {
        $("#txtFonecedor").val(ui.item.id);
        $("#txtFonecedorNome").val(ui.item.value);
    }
}); 