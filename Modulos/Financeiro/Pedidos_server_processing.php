<?php

if (!isset($_GET['pdf'])) {
    include '../../Connections/configini.php';
}
$aColumns = array('id_venda', 'data_venda', 'numero_filial', 'razao_social', 'vendedor', 'historico_venda', 'pa', 'tp', 'ts', 'ts', 'status');
$iTotal = 0;
$iFilteredTotal = 0;
$totparc = 0;
$sWhere = "";
$sOrder = " ORDER BY data_venda desc ";

$imprimir = 0;
$F1 = "";
$F2 = "";
$F3 = "";
$comando = "";

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = $_GET['imp'];
}

if (is_numeric($_GET['id'])) {
    $F1 = $_GET['id'];
}

if (is_numeric($_GET['tp'])) {
    $F2 = $_GET['tp'];
}

if ($_GET['td'] != '') {
    $F3 = $_GET['td'];
}

if ($_GET['filial'] != '') {
    $idFilial = $_GET['filial'];
}

if ($_GET['dti'] != '') {
    $comando .= " AND data_venda >= " . valoresDataHora2(str_replace("_", "/", $_GET['dti']), "00:00:00");
}

if ($_GET['dtf'] != '') {
    $comando .= " AND data_venda <= " . valoresDataHora2(str_replace("_", "/", $_GET['dtf']), "23:59:59");
}

if ($F1 == "1") {
    $comando .= " and status = 'Aguarda' ";
} elseif ($F1 == "2") {
    $comando .= " and status = 'Aprovado' ";
} elseif ($F1 == "3") {
    $comando .= " and status = 'Reprovado' ";
} elseif ($F1 == "5") {
    $comando .= " and (status = 'Aprovado' or status = 'Aguarda') ";
}

if ($F3 != "") {
    if ($F2 == "0") {
        $comando .= " AND cliente_venda = " . $F3;
    } elseif ($F2 == "1") {
        $comando .= " AND grupo_conta = " . $F3;
    } elseif ($F2 == "2") {
        $comando .= " AND conta_movimento = " . $F3;
    } elseif ($F2 == "3") {
        $comando .= " AND id_venda in (select venda from sf_venda_parcelas where inativo = 0 and tipo_documento = " . $F3 . ") ";
    } elseif ($F2 == "4") {
        $comando .= " AND destinatario = '" . $F3 . "'";
    } elseif ($F2 == "5") {
        $comando .= " AND UPPER(vendedor) = UPPER('" . $F3 . "')";
    }
}

if ($_GET['com'] != '') {
    $comando .= " AND cod_pedido = " . valoresTexto2($_GET['com']) . "";
}

if ($_GET['usr'] != '' && $_GET['usr'] != 'null') {
    $comando .= " AND UPPER(vendedor) = UPPER(" . valoresTexto2($_GET['usr']) . ")";
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . "
                " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY data_venda desc";
    }
}

if ($_GET['sSearch'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        if ($i >= 2 & $i <= 7) {
            $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['sSearch']) . "%' OR ";
        }
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

if (is_numeric($idFilial)) {
    $comando .= " AND sf_vendas.empresa =" . $idFilial;
}

$sQuery1 = "set dateformat dmy; select *, ROW_NUMBER() OVER (" . $sOrder . ") as row from (select id_venda,data_venda,razao_social,nome_fantasia,UPPER(vendedor) vendedor,historico_venda,status,ISNULL((select SUM(valor_total) from sf_vendas_itens v inner join sf_produtos c on v.produto = c.conta_produto 
            where tipo = 'P' and id_venda = sf_vendas.id_venda),0) as tp, ISNULL((select SUM(valor_total) from sf_vendas_itens v inner join sf_produtos c on v.produto = c.conta_produto 
            where tipo in ('S','C') and id_venda = sf_vendas.id_venda),0) as ts, ISNULL((select COUNT(id_parcela) from sf_venda_parcelas where venda = sf_vendas.id_venda and inativo = 0),0) as pa,destinatario 
            ,data_aprov,numero_filial, cod_pedido,(select isnull(max(cod_nfe),0) from sf_nfe_notas where id_venda_nfe = id_venda) nfs 
            from sf_vendas left join sf_filiais on sf_filiais.id_filial = sf_vendas.empresa 
            inner join sf_fornecedores_despesas f on sf_vendas.cliente_venda = f.id_fornecedores_despesas 
            left join tb_estados on f.estado = estado_codigo left join tb_cidades on f.cidade = cidade_codigo            
            where cov = 'P' " . $comando . ") as x where id_venda is not null " . $sWhere;
//echo $sQuery1;exit();
$cur = odbc_exec($con, $sQuery1);
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ($RFP = odbc_fetch_array($cur)) {
    $iFilteredTotal = $iFilteredTotal + 1;
    if (($RFP['row'] > $sLimit && $RFP['row'] <= ($sLimit + $sQtd)) or $imprimir == 1) {
        $row = array();
        if ($_GET['comanda']) {
            $row[] = utf8_encode($RFP['id_venda']);
            $row[] = utf8_encode($RFP['cod_pedido']);
            $row[] = escreverData($RFP['data_venda']);
            $row[] = utf8_encode($RFP['razao_social']);
            $row[] = utf8_encode($RFP['vendedor']);
            $row[] = escreverNumero(($RFP['tp'] + $RFP['ts']), 1);
        } else {
            $row[] = "<center><input type=\"checkbox\" class=\"caixa\" name=\"items[]\" value=\"" . $RFP['id_venda'] . "\" " .
                    ($F1 == "1" && $imprimir == 0 && !is_numeric($F4) && utf8_encode($RFP['destinatario']) == $_SESSION["login_usuario"] ? "" : "disabled") . "/></center>";
            $row[] = "<div id='formPQ'><center><a href='./../Financeiro/FormPedidos.php?id=" . $RFP['id_venda'] . "' >" . escreverDataHora($RFP['data_venda']) . "</a></center></div>";
            $row[] = "<div id='formPQ'><center>" . utf8_encode($RFP['numero_filial']) . "</center></div>";
            $row[] = "<div id='formPQ'><center>" . utf8_encode($RFP['id_venda']) . "</center></div>";
            $row[] = "<div id='formPQ'>" . utf8_encode($RFP['validade']) . "</div>";
            $nome_fantasia = "";
            if (utf8_encode($RFP['nome_fantasia']) != '') {
                $nome_fantasia = " (" . utf8_encode($RFP['nome_fantasia']) . ")";
            }
            $row[] = "<div id=\"formPQ\">" . utf8_encode($RFP['razao_social']) . $nome_fantasia . "</div>";
            $row[] = "<div id='formPQ'>" . utf8_encode($RFP['cidade_nome']) . "</div>";
            $row[] = "<div id='formPQ'><center>" . utf8_encode($RFP['estado_sigla']) . "</center></div>";
            $row[] = "<div id='formPQ'><center>" . utf8_encode($RFP['vendedor']) . "</center></div>";
            $row[] = "<div id='formPQ' style=\"text-align:right\">" . escreverNumero($RFP['tp'], 1) . "</div>";
            $row[] = "<div id='formPQ' style=\"text-align:right\">" . escreverNumero($RFP['ts'], 1) . "</div>";
            $row[] = "<div id='formPQ' style=\"text-align:right\">" . escreverNumero(($RFP['tp'] + $RFP['ts']), 1) . "</div>";
            $color = "style=\"color:";
            if (utf8_encode($RFP['status']) == 'Aguarda') {
                $color = $color . "#bc9f00\"";
            } elseif (utf8_encode($RFP['status']) == 'Aprovado') {
                $color = $color . "#0066cc\"";
            } elseif (utf8_encode($RFP['status']) == 'Reprovado') {
                $color = $color . "#f00\"";
            } else {
                $color = $color . "black\"";
            }
            if ($RFP['data_aprov'] != "") {
                $titleStatus = utf8_encode($RFP['status'] . " por " . $RFP['destinatario'] . ": " . escreverDataHora($RFP['data_aprov']));
            } else {
                $titleStatus = utf8_encode($RFP['status']);
            }
            $row[] = "<div id='formPQ' " . $color . " title=\"" . $titleStatus . "\"><center>" . utf8_encode($RFP['status']) . "</center></div>";
        }
        $output['aaData'][] = $row;
    }
    $totparc = $totparc + $RFP['tp'] + $RFP['ts'];
}

if (!isset($_GET['pdf'])) {
    if ($iFilteredTotal > 0) {
        $row = $output['aaData'][0];
        $row[1] = "<input type='hidden' name='totparc' id='totparc' value='" . $totparc . "'/>" .
        "<input type='hidden' name='qtdparc' id='qtdparc' value='" . $iFilteredTotal . "'/>" . $row[1];
        $output['aaData'][0] = $row;
    }
    $output['iTotalRecords'] = $iFilteredTotal;
    $output['iTotalDisplayRecords'] = $iFilteredTotal;
    echo json_encode($output);
}
odbc_close($con);
