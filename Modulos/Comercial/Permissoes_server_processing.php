<?php

if (!isset($_GET['pdf'])) {
    include ("./../../Connections/configini.php");
    include ("./../../util/util.php");
}

$aColumns = array('id_permissoes', 'nome_permissao');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY nome_permissao asc ";
$sLimit = 20;
$_GET['sSearch'] = $_GET['Search'];
$imprimir = 0;

$mdl_adm_ = returnPart($_SESSION["modulos"], 0);
$mdl_fin_ = returnPart($_SESSION["modulos"], 1);
$mdl_est_ = returnPart($_SESSION["modulos"], 2);
$mdl_nfe_ = returnPart($_SESSION["modulos"], 3);
$mdl_crm_ = returnPart($_SESSION["modulos"], 4);
$mdl_ser_ = returnPart($_SESSION["modulos"], 5);
$mdl_aca_ = returnPart($_SESSION["modulos"], 6);
$mdl_tur_ = returnPart($_SESSION["modulos"], 7);
$mdl_gbo_ = returnPart($_SESSION["modulos"], 8);
$mdl_srs_ = returnPart($_SESSION["modulos"], 9);
$mdl_bod_ = returnPart($_SESSION["modulos"], 11);
$mdl_clb_ = returnPart($_SESSION["modulos"], 12);
$mdl_cnt_ = returnPart($_SESSION["modulos"], 13);
$mdl_sau_ = returnPart($_SESSION["modulos"], 15);
$mdl_wha_ = returnPart($_SESSION["modulos"], 16);
$mdl_cht_ = returnPart($_SESSION["modulos"], 17);

if (is_numeric($_GET['imp']) && $_GET['imp'] == "1") {
    $imprimir = 1;
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0 && intval($_GET['iSortCol_' . $i]) < 9) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY nome_permissao asc";
    }
}

if ($_GET['Search'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        if ($i !== 6) {
            $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['Search']) . "%' OR ";
        }
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,*
            FROM sf_usuarios_permissoes WHERE id_permissoes > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1=" . $imprimir . " " . $sOrder;
//echo $sQuery1;exit();
$cur = odbc_exec($con, $sQuery1);

$sQuery = "SELECT COUNT(*) total FROM sf_usuarios_permissoes WHERE id_permissoes > 0 $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total FROM sf_usuarios_permissoes WHERE id_permissoes > 0 $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ($aRow = odbc_fetch_array($cur)) {
    $tot_adm = $aRow['adm_for'] + $aRow['adm_cli'] + $aRow['adm_fun'] + $aRow['adm_usu'] + $aRow['adm_per'] + $aRow['adm_gru'] + $aRow['adm_dep'] + $aRow['fin_doc'] + $aRow['fin_ban'] + $aRow['fin_con'] + $aRow['fin_mov'] + $aRow['crm_cli'] + $aRow['crm_esp'] + $aRow['crm_ind'] + $aRow['adm_config'] + $aRow['crm_fch'] + $aRow['adm_agenda'] + $aRow['adm_parentesco'] + $aRow['adm_contratos'] + $aRow['adm_atendimentos'];
    $tot_fin = $aRow['fin_fix'] + $aRow['fin_afix'] + $aRow['fin_flu'] + $aRow['fin_lsol'] + $aRow['fin_asol'] + $aRow['fin_pag'] + $aRow['fin_rec'] + $aRow['est_com'] + 
    $aRow['est_lven'] + $aRow['est_aven'] + $aRow['fin_dmp'] + $aRow['fin_rel_gct'] + $aRow['fin_chq'] + $aRow['fin_bol'] + 
    $aRow['fin_tdd'] + $aRow['est_lorc'] + $aRow['est_aorc'] + $aRow['fin_rel_mov_caixa'] + $aRow['fin_bol'] + $aRow['exc_cpagar'] + $aRow['exc_creceber'] + $aRow['fin_lpg'] + $aRow['com_forma'];        
    $tot_est = $aRow['est_mov'] + $aRow['est_ccu'] + $aRow['est_pro'] + $aRow['est_loc'] + $aRow['est_flu'] + $aRow['est_cor'] + $aRow['est_tam'] + $aRow['est_fab'] + $aRow['est_pla']  + $aRow['est_lcom'] + $aRow['est_acom']  + $aRow['fin_lped']  + $aRow['fin_lcot'] + $aRow['fin_aped'] + $aRow['est_ent_sai'] + $aRow['est_aent_sai'] + $aRow['est_jus'];
    $tot_nfe = $aRow['nfe_cen'] + $aRow['nfe_tra'] + $aRow['nfe_msg'] + $aRow['nfe_not'] + $aRow['nfe_enq'] + $aRow['nfe_ess'] + $aRow['nfe_est'] + $aRow['nfe_cid'];
    $tot_crm = $aRow['crm_pro'] + $aRow['crm_tel'] + $aRow['crm_rec'] + $aRow['crm_cpc'] + $aRow['crm_exc_atend'] + $aRow['crm_fec_atend'] + $aRow['adm_disparos'] + $aRow['adm_email'] + $aRow['use_dist_pro'];
    $tot_out = $aRow['out_ges'] + $aRow['out_aju'] + $aRow['out_term'] + $aRow['out_terc'] + $aRow['out_rban'] + $aRow['out_rexc'] + $aRow['out_todos_operadores'] + $aRow['out_desconto'] + $aRow['fin_crt'] + $aRow['out_pag_avancada'] + $aRow['aca_estudio'];
    $tot_ser = $aRow['ser_cli'] + $aRow['ser_ser'] + $aRow['est_ser'] + $aRow['est_spc'];
    $tot_aca = $aRow['aca_hor'] + $aRow['aca_conv'] + $aRow['aca_cred'] + $aRow['aca_leif'] + $aRow['aca_catr'] + $aRow['aca_amb'] + $aRow['aca_fer'] + $aRow['aca_fer_par'] + $aRow['aca_fer_exc'] + $aRow['aca_pla'] + $aRow['aca_ser'] + $aRow['aca_alt_val_mens'] + $aRow['aca_est_mens'] + $aRow['aca_exc_mens'] + $aRow['aca_inf_gerenciais'] + $aRow['aca_add_convenio'] +
    $aRow['aca_add_credencial'] + $aRow['aca_dtf_credencial'] + $aRow['aca_add_bloqueio'] + $aRow['aca_cancelar_dcc'] + $aRow['aca_rel_acesso'] + $aRow['aca_plan_comp'] + $aRow['aca_turmas'] + $aRow['aca_pgr'] +
    $aRow['aca_abonar_mens'] + $aRow['aca_infoger_caixa'] + $aRow['aca_infoger_stats'] + $aRow['aca_infoger_contr'] + $aRow['aca_infoger_filav'] + $aRow['aca_infoger_renov'] + $aRow['aca_infoger_dcc'] + $aRow['aca_infoger_acess'] + $aRow['aca_rem_plano'] + $aRow['aca_can_plano'] + $aRow['aca_usu_resp'] +
    $aRow['aca_pro_rata_mens'] + $aRow['aca_documento'] + $aRow['aca_abonar_cre'] + $aRow['aca_abonar_deb'] + $aRow['aca_pg_prod'] + $aRow['aca_can_item'] + $aRow['aca_data_inicio'] + $aRow['aca_agenda_regras'] + + $aRow['aca_perm_desc'];
    if ($tot_adm == (19 + $mdl_cnt_)) {
        $tot_adm = "Total";
    } else {
        if ($tot_adm == 0) {
            $tot_adm = "Nenhum";
        } else {
            $tot_adm = "Parcial";
        }
    }
    if ($tot_fin == 23) {
        $tot_fin = "Total";
    } else {
        if ($tot_fin == 0) {
            $tot_fin = "Nenhum";
        } else {
            $tot_fin = "Parcial";
        }
    }
    if ($tot_est == 17) {
        $tot_est = "Total";
    } else {
        if ($tot_est == 0) {
            $tot_est = "Nenhum";
        } else {
            $tot_est = "Parcial";
        }
    }
    if ($tot_nfe == 8) {
        $tot_nfe = "Total";
    } else {
        if ($tot_nfe == 0) {
            $tot_nfe = "Nenhum";
        } else {
            $tot_nfe = "Parcial";
        }
    }
    if ($tot_crm == 9) {
        $tot_crm = "Total";
    } else {
        if ($tot_crm == 0) {
            $tot_crm = "Nenhum";
        } else {
            $tot_crm = "Parcial";
        }
    }
    if ($tot_ser == 4) {
        $tot_ser = "Total";
    } else {
        if ($tot_ser == 0) {
            $tot_ser = "Nenhum";
        } else {
            $tot_ser = "Parcial";
        }
    }
    if ($tot_aca == 43) {
        $tot_aca = "Total";
    } else {
        if ($tot_aca == 0) {
            $tot_aca = "Nenhum";
        } else {
            $tot_aca = "Parcial";
        }
    }
    if ($tot_out == 11) {
        $tot_out = "Total";
    } else {
        if ($tot_out == 0) {
            $tot_out = "Nenhum";
        } else {
            $tot_out = "Parcial";
        }
    }
    $row = array();
    $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[$aColumns[0]]) . ")'><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[1]]) . "'>" . utf8_encode($aRow[$aColumns[1]]) . "</div></a>";
    if ($mdl_adm_ > 0) {
        $row[] = "<center><div id='formPQ' title='" . $tot_adm . "'>" . $tot_adm . "</div></center>";
    }
    if ($mdl_fin_ > 0) {
        $row[] = "<center><div id='formPQ' title='" . $tot_fin . "'>" . $tot_fin . "</div></center>";
    }
    if ($mdl_est_ > 0) {
        $row[] = "<center><div id='formPQ' title='" . $tot_est . "'>" . $tot_est . "</div></center>";
    }
    if ($mdl_nfe_ > 0) {
        $row[] = "<center><div id='formPQ' title='" . $tot_nfe . "'>" . $tot_nfe . "</div></center>";
    }
    if ($mdl_crm_ > 0) {
        $row[] = "<center><div id='formPQ' title='" . $tot_crm . "'>" . $tot_crm . "</div></center>";
    }
    if ($mdl_ser_ > 0) {
        $row[] = "<center><div id='formPQ' title='" . $tot_ser . "'>" . $tot_ser . "</div></center>";
    }
    if ($mdl_aca_ > 0) {
        $row[] = "<center><div id='formPQ' title='" . $tot_aca . "'>" . $tot_aca . "</div></center>";
    }
    $row[] = "<center><div id='formPQ' title='" . $tot_out . "'>" . $tot_out . "</div></center>";
    if ($imprimir == 0) {
        $row[] = "<center><a title='Excluir' onClick=\"return confirm('Deseja deletar esse registro?')\" href='Permissoes.php?Del=" . $aRow[$aColumns[0]] . "'><input name='' type='image' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
    }
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
