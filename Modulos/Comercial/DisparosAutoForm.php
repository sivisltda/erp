<?php include "form/DisparosAutoFormServer.php"; ?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src='../../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<body>
    <form action="DisparosAutoForm.php" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Disparos Automáticos</div>
            <div class="frmicon" onClick="parent.FecharBox(1);">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont" style="height: 220px;">
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <div style="width: 26%;float: left">
                <span>Tipo:</span>
                <select id="txtTipo" name="txtTipo" class="select" style="width:100%" <?php echo $disabled; ?>>                                
                    <option value="E" <?php echo ($tipo == "E" ? "selected" : "");?>>EMAIL</option>
                    <option value="S" <?php echo ($tipo == "S" ? "selected" : "");?>>SMS</option>
                    <?php if($mdl_wha_ == 1) { ?>  
                    <option value="W" <?php echo ($tipo == "W" ? "selected" : "");?>>Whatsapp</option>
                    <?php } ?>
                    <option value="L" <?php echo ($tipo == "L" ? "selected" : "");?>>Atendimento</option>                                
                </select>
            </div>
            <div style="width:73%; float:left; margin-left:1%">
                <span>Regra:</span>
                <select id="txtRegra" name="txtRegra" class="select" style="width:100%" <?php echo $disabled; ?>>
                    <option value="null">Selecione</option>
                    <?php $cur = odbc_exec($con, "select * from sf_disparo_auto where tp_contrato_disparo_auto in('T','" . $tpfiltroDisp . "')") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                        <option value="<?php echo $RFP['id_disparo_auto'] ?>"<?php
                        if (!(strcmp($RFP['id_disparo_auto'], $regra))) {
                            echo "SELECTED";
                        } ?>><?php echo utf8_encode($RFP['descr_disparo_auto']);?></option>
                    <?php } ?>
                </select>
            </div>            
            <div style="width:39%; float:left;">
                <span>Responsável:</span>
                <select id="txtResponsavel" name="txtResponsavel" class="select" style="width:100%" <?php echo $disabled; ?>>
                    <option value="null">Selecione</option>
                    <?php $cur = odbc_exec($con, "select * from sf_usuarios where id_usuario > 1 and inativo = 0 " . ($tipo == "E" ? " and email_login <> '' and email_porta <> '' and email_host <> '' and email_senha <> ''" : "")) or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                        <option value="<?php echo $RFP['id_usuario'] ?>"<?php
                        if (!(strcmp($RFP['id_usuario'], $responsavel))) {
                            echo "SELECTED";
                        } ?>><?php echo utf8_encode($RFP['nome']);?></option>
                    <?php } ?>
                </select>
            </div>             
            <div style="width:20%; float:left; margin-left:1%">
                <span>Loja:</span>
                <select id="txtLoja" name="txtLoja" class="select" style="width:100%" <?php echo $disabled; ?>>
                    <option value="0">Todas</option>
                    <?php $cur = odbc_exec($con, "select id_filial,numero_filial from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                        <option value="<?php echo $RFP['id_filial'] ?>"<?php
                        if (!(strcmp($RFP['id_filial'], $loja))) {
                            echo "SELECTED";
                        } ?>><?php echo utf8_encode($RFP['numero_filial']);?></option>
                    <?php } ?>
                </select>
            </div>             
            <div style="width:19%; float: left; margin-left: 1%;" >
                <span>Dias:</span>
                <input id="txtDias" name="txtDias" <?php echo $disabled; ?> type="text" class="input-medium" style="width:100%;" maxlength="2" value="<?php echo $dias; ?>"/>
            </div>
            <div style="width:18%; float:left; margin:18px 0px 0% 2%;">
                <div style="float:left; margin-top:0">
                    <input type="checkbox" class="input-medium" <?php echo $disabled; ?> id="txtBloqueio" name="txtBloqueio" value="1" <?php if ($bloqueio == 1) { echo "checked";} ?>/>
                </div>
                <div style="float:left; margin:2px 5px">Bloqueado</div>
            </div>
            <div style="width:100%; float:left; <?php echo ($tipo == "L" ? "display: none;" : ""); ?>">
                <span>Modelos de Disparos:</span>
                <select id="txtModelo" name="txtModelo" class="select" style="width:100%" <?php echo $disabled; ?>>
                    <option value="null">Selecione</option>
                    <?php $cur = odbc_exec($con, "select id_email, descricao from sf_emails where tipo = " . ($tipo == "E" ? "0" : ($tipo == "S" ? "1" : ($tipo == "W" ? "2" : "3")))) or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                        <option value="<?php echo $RFP['id_email'] ?>"<?php
                        if (!(strcmp($RFP['id_email'], $modelo))) {
                            echo "SELECTED";
                        } ?>><?php echo utf8_encode($RFP['descricao']);?></option>
                    <?php } ?>
                </select>
            </div>            
            <div style="width: 100%;float: left; <?php echo ($tipo != "L" ? "display: none;" : ""); ?>">
                <span>Assunto:</span>
                <input name="txtAssunto" id="txtAssunto" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="128" value="<?php echo $assunto; ?>"/>
            </div>            
            <div style="width: 100%;float: left; <?php echo ($tipo != "L" ? "display: none;" : ""); ?>">
                <span>Descrição:</span>
                <textarea name="txtDescricao" id="txtDescricao" <?php echo $disabled; ?> type="text" style="height:65px;"><?php echo $descricao; ?></textarea>
            </div>            
            <div style="clear:both;"></div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" name="bntSave" id="bntSave" onclick="return validaForm();" ><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox(1);" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntAlterar" ><span class="ico-reply"></span> Cancelar</button>
                    <?php }
                } else { ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntEdit" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../js/plugins.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type="text/javascript" src="js/DisparosAutoForm.js"></script>
    <?php odbc_close($con); ?>
</body>
