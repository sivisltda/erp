<?php

require_once("../../../Connections/configini.php");
require_once("../../../util/util.php");

if (isset($_GET['listUsuarios'])) {
    $whereEnvio = "";
    if (isset($_GET['envioEmail']) && $_GET['envioEmail'] == "S") {
        $whereEnvio = " and email_login <> '' and email_porta <> '' and email_host <> '' and email_senha <> ''";
    }
    $cur = odbc_exec($con, "select * from sf_usuarios where id_usuario > 1 and inativo = 0 " . $whereEnvio);
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('id_usuario' => tableFormato($RFP['id_usuario'], 'T', '', ''), 'nome' => tableFormato($RFP['nome'], 'T', '', ''));
    }
    echo json_encode($local);
}

if (isset($_POST['btnSave'])) {
    $query = "update sf_usuarios set senha = " . valoresTexto2(encrypt($_POST['txtSenha'], "VipService123", true)) .
    " where id_usuario = " . valoresNumericos("txtId") . ";";
    $query .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
    ('sf_usuarios', " . valoresNumericos("txtId") . ", " . valoresTexto2($_SESSION["login_usuario"]) . ", 'A', 'ALTERACAO DE SENHA USUARIO " . valoresNumericos("txtId") . "', GETDATE(), null);";    
    odbc_exec($con, $query);    
    echo "YES";
}

odbc_close($con);
