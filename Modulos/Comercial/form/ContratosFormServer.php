<?php

include "../../Connections/configini.php";

$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        $query = "update sf_contratos set " .
                "descricao = UPPER(" . valoresTexto('txtDescricao') . ")," .
                "tipo = " . valoresSelect('txtTipo') . "," .                
                "inativo = " . valoresCheck('txtInativo') . "," .
                "portal = " . valoresCheck('txtPortal') . "," .
                "logo = " . valoresCheck('txtLogo') . "," .
                "margem_d = " . valoresNumericos('txtRecuo') . "," .
                "assinatura = " . valoresCheck('txtAssTermos') . "," .                
                "marcar = " . valoresCheck('txtMarcarTermos') . "," .   
                "id_contrato_individual = " . valoresSelect('txtIndividual') . 
                " where id = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
    } else {
        $query = "set dateformat dmy;insert into sf_contratos(descricao, tipo, inativo, portal, logo, margem_d, assinatura,
                marcar, dt_criacao, usu_criacao, id_contrato_individual)values(" .
                valoresTextoUpper('txtDescricao') . "," .
                valoresSelect('txtTipo') . "," .
                valoresCheck('txtInativo') . "," .                
                valoresCheck('txtPortal') . "," .
                valoresCheck('txtLogo') . "," .
                valoresNumericos('txtRecuo') . "," .
                valoresCheck('txtAssTermos') . "," .
                valoresCheck('txtMarcarTermos') . "," .
                "getdate(),'" . $_SESSION["nome_usuario"] . "'," . 
                valoresSelect('txtIndividual') . ")";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id from sf_contratos where id > 0 order by id desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
    if (isset($_POST['txtFilial'])) {
        $inValue = "0";
        for ($i = 0; $i < count($_POST['txtFilial']); $i++) {
            if ($_POST['txtFilial'][$i] != '') {
                $inValue .= "," . $_POST['txtFilial'][$i];
            }
        }
        odbc_exec($con, "delete from sf_contratos_filiais where id_contrato = " . $_POST['txtId'] . " and id_filial not in (" . $inValue . ")") or die(odbc_errormsg());        
        for ($i = 0; $i < count($_POST['txtFilial']); $i++) {        
            odbc_exec($con, "IF NOT EXISTS (SELECT id_contrato, id_filial FROM sf_contratos_filiais WHERE id_contrato = " . $_POST['txtId'] . " AND id_filial = " . $_POST['txtFilial'][$i] . ")
            BEGIN INSERT INTO sf_contratos_filiais(id_contrato, id_filial) VALUES (" . $_POST['txtId'] . "," . $_POST['txtFilial'][$i] . "); END");
        }
    }
    if (isset($_POST['txtGrupos'])) {
        $inValue = "0";
        for ($i = 0; $i < count($_POST['txtGrupos']); $i++) {
            if ($_POST['txtGrupos'][$i] != '') {
                $inValue .= "," . $_POST['txtGrupos'][$i];
            }
        }
        odbc_exec($con, "delete from sf_contratos_grupos where id_contrato = " . $_POST['txtId'] . " and id_grupo not in (" . $inValue . ")") or die(odbc_errormsg());        
        for ($i = 0; $i < count($_POST['txtGrupos']); $i++) {        
            odbc_exec($con, "IF NOT EXISTS (SELECT id_contrato, id_grupo FROM sf_contratos_grupos WHERE id_contrato = " . $_POST['txtId'] . " AND id_grupo = " . $_POST['txtGrupos'][$i] . ")
            BEGIN INSERT INTO sf_contratos_grupos(id_contrato, id_grupo) VALUES (" . $_POST['txtId'] . "," . $_POST['txtGrupos'][$i] . "); END");
        }
    }
}

if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        odbc_exec($con, "DELETE FROM sf_contratos WHERE id = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso!'); parent.FecharBox(1);</script>";
        exit();
    }
}

if (isset($_POST['bntDuplicar'])) {
    if (is_numeric($_POST['txtId'])) {
        odbc_exec($con, "insert into sf_contratos (descricao, tipo, dt_criacao, usu_criacao, inativo, portal, logo, margem_d, assinatura, marcar, id_contrato_individual)
        select descricao + '_1', tipo, getdate(), '" . $_SESSION["nome_usuario"] . "', inativo, portal, logo, margem_d, assinatura, marcar, id_contrato_individual 
        from sf_contratos where id = " . $_POST['txtId']);        
        $res = odbc_exec($con, "select top 1 id from sf_contratos where id > 0 order by id desc") or die(odbc_errormsg());
        $id = odbc_result($res, 1);
        odbc_exec($con, "insert into sf_contratos_filiais (id_filial, id_contrato)
        select id_filial, " . $id . " from sf_contratos_filiais where id_contrato = " . $_POST['txtId']);          
        odbc_exec($con, "insert into sf_contratos_grupos (id_grupo, id_contrato)
        select id_grupo, " . $id . " from sf_contratos_grupos where id_contrato = " . $_POST['txtId']);          
        odbc_exec($con, "insert into sf_contratos_item (id_contrato, ordem, tipo, conteudo, align, fonte_size, margin)
        select " . $id . ", ordem, tipo, conteudo, align, fonte_size, margin from sf_contratos_item where id_contrato = " . $_POST['txtId']);                 
        echo "<script>alert('Registro duplicado com sucesso!'); parent.FecharBox(1);</script>";
        exit();        
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_contratos where id =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id'];
        $descricao = utf8_encode($RFP['descricao']);
        $tipo = utf8_encode($RFP['tipo']);
        $inativo = utf8_encode($RFP['inativo']);
        $logo = utf8_encode($RFP['logo']);
        $recuo = utf8_encode($RFP['margem_d']);
        $portal = utf8_encode($RFP['portal']);
        $assTermos = utf8_encode($RFP['assinatura']);
        $marcarTermos = utf8_encode($RFP['marcar']);
        $individual = utf8_encode($RFP['id_contrato_individual']);
    }
} else {
    $disabled = '';
    $id = '';
    $descricao = '';
    $tipo = '';
    $inativo = '';
    $logo = '';
    $recuo = '';
    $portal = '';
    $assTermos = '';
    $marcarTermos = '';
    $individual = '';
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
