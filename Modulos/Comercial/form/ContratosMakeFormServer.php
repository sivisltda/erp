<?php

require_once(__DIR__ . './../../../Connections/configini.php');

$imprimir = 0;
$disabled = 'disabled';
$id = '';
$descricao = '';

if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        $query = "update sf_contratos_item set " .
        "tipo = " . valoresSelect('txtTipo') . "," .
        "conteudo = " . valoresTexto('saveContent') . "," .
        "align = " . valoresNumericos('txtAlign') . "," .
        "fonte_size = " . valoresSelect('txtFontSize') . "," .
        "margin = " . valoresSelect('txtMarginBottom') .
        " where id = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
    } else {
        $ordem = 0;
        $query = "select top 1 ordem from sf_contratos_item where id_contrato =" . $_POST["id_contrato"] . " order by ordem desc";
        $cur = odbc_exec($con, $query) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $ordem = $RFP['ordem'] + 1;
        }
        $query = "insert into sf_contratos_item(ordem, tipo, conteudo, id_contrato, align, fonte_size, margin) values("
        . $ordem . ", " .
        valoresSelect('txtTipo') . ", " .
        valoresTexto('saveContent') . ", " .
        valoresTexto('id_contrato') . ", " .
        valoresNumericos('txtAlign') . ", " .
        valoresSelect('txtFontSize') . ", " .
        valoresSelect('txtMarginBottom') . ")";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id from sf_contratos_item where id > 0 and id_contrato = " . $_POST["id_contrato"] . " order by id desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}

if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        if (isset($_POST['List'])) {
            include "../../../Connections/configini.php";
        }
        odbc_exec($con, "DELETE FROM sf_contratos_item WHERE id = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox(1);</script>";
        exit();
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select *, substring(conteudo,1,4000) conteudo1, substring(conteudo,4001,4000) conteudo2, substring(conteudo,8001,4000) conteudo3, substring(conteudo,12001,4000) conteudo4, substring(conteudo,16001,4000) conteudo5 from sf_contratos_item where id =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id'];
        $tipo = utf8_encode($RFP['tipo']);
        $conteudo = utf8_encode($RFP["conteudo1"] . $RFP["conteudo2"] . $RFP["conteudo3"] . $RFP["conteudo4"] . $RFP["conteudo5"]);
        $align = $RFP['align'];
        $fontSize = $RFP['fonte_size'];
        $marginBottom = $RFP['margin'];
    }
} else {
    $disabled = '';
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
