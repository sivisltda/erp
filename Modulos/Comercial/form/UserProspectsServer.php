<?php

include "../../../Connections/configini.php";

if (isset($_POST["btnSalvar"])) {
    $query = "insert into sf_configuracao_usuarios(id_usuario) values (" . valoresNumericos("txtId") . ");";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";    
}

if (isset($_POST["btnExcluir"])) {
    $query = "delete from sf_configuracao_usuarios where id = " . valoresNumericos("txtId") . ";";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES"; 
}

if (isset($_GET["list"])) {
    $total = 0;
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );      
    $cur = odbc_exec($con, "select c.id, u.nome from sf_configuracao_usuarios c
    inner join sf_usuarios u on u.id_usuario = c.id_usuario
    order by c.id") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row[] = formatNameCompact(strtoupper(utf8_encode($RFP['nome'])));
        $row[] = "<center><input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\" onclick=\"RemoverUserProspects('" . utf8_encode($RFP['id']) . "')\" /></center>";
        $output['aaData'][] = $row;
        $total++;
    }    
    $output['iTotalRecords'] = $total;
    $output['iTotalDisplayRecords'] = $total; 
    echo json_encode($output);
}

if (isset($_POST["btnDessinc"])) {
    $query = "update sf_fornecedores_despesas_veiculo set id_externo = null where id_externo is not null;";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES"; 
}

if (isset($_POST["btnDessincRa"])) {
    $query = "update sf_fornecedores_despesas_veiculo set id_externo_rast = null, status_ext_veic = null where id_externo_rast is not null;";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES"; 
}

odbc_close($con);