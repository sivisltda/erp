<?php

require_once('../../../Connections/configini.php');
$total = 0;
$sLimit = 0;
$sQtd = 20;
$output = array();
$pasta = "./../../../Pessoas/" . $contrato . "/Empresa/Contratacao/" . (isset($_GET["type"]) ? $_GET["type"] . "/" : "");
if (file_exists($pasta)) {
    $diretorio = dir($pasta);
    $files = array();
    while ($arquivo = $diretorio->read()) {
        if ($arquivo != "." && $arquivo != ".." && !is_dir($pasta . $arquivo)) {
            if ($total >= $sLimit && $total <= ($sLimit + $sQtd)) {
                $files[filectime($pasta . $arquivo) . $total] = $arquivo;
            }
            $total++;
        }
    }
    $diretorio->close();
    ksort($files);
    foreach ($files as $arquivo) {
        $row = array();
        $row[] = $pasta . str_replace(array("", ""), array("‡", "ƒ"), utf8_encode($arquivo)) . "?rd=" . rand(1, 100000);
        $row[] = filesize($pasta . $arquivo) . " bytes";
        $row[] = "<input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px; position: absolute; ;top: 0;right: -15px;\" onclick=\"
        RemoverFotoContratacao('" . str_replace(array("", ""), array("‡", "ƒ"), utf8_encode($arquivo)) . "')\" />";
        $output[] = $row;
    }
}
echo json_encode($output);
odbc_close($con);
