<?php

if (isset($_POST['List'])) {
    include "../../../Connections/configini.php";
}

$disabled = 'disabled';
$id = '';
$grupo = 'null';
$inativo = '0';
$obrigatorio = '0';
$descricao = '';        
$imprimir = 0;

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = 1;
}

if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {              
        odbc_exec($con, "update sf_configuracao_termos set " .
        "termo = " . valoresTexto('ckeditor') . "," .
        "grupo = " . valoresSelect('txtGrupo') . "," .
        "inativo = " . valoresSelect('txtInativo') . "," .
        "obrigatorio = " . valoresSelect('txtObrigatorio') . " " .
        "where id = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_configuracao_termos(termo,grupo,inativo,obrigatorio) values (" . valoresTexto('ckeditor') . "," . 
        valoresSelect('txtGrupo') . "," . valoresSelect('txtInativo') . "," . valoresSelect('txtObrigatorio') . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id from sf_configuracao_termos order by id desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}

if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        odbc_exec($con, "DELETE FROM sf_configuracao_termos WHERE id = " . $_POST['txtId']) or die(odbc_errormsg());
        if (isset($_POST['List'])) {
            echo "YES";
        } else {
            echo "<script>alert('Registro excluido com sucesso'); parent.FecharTermos();</script>";    
        }
        exit();
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_configuracao_termos where id =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id']);
        $grupo = utf8_encode($RFP['grupo']);
        $inativo = utf8_encode($RFP['inativo']);
        $obrigatorio = utf8_encode($RFP['obrigatorio']);
        $descricao = utf8_encode($RFP['termo']);
    }
} else {
    $disabled = '';
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}