<?php

require_once('../../../Connections/configini.php');
if ($_POST['btnExcluir'] != '') {    
    $delTitularDependente = "";
    $cur = odbc_exec($con, "select * from sf_configuracao");
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['ACA_DEL_TITU_DEP'] > 0) {
            $delTitularDependente = " or id_fornecedores_despesas in (select id_dependente from sf_fornecedores_despesas_dependentes where id_titular = " .  $_POST['idCli'] . ")";
        }
    }     
    odbc_exec($con, "UPDATE sf_fornecedores_despesas set inativo = 1 where id_fornecedores_despesas = " . $_POST['idCli'] . $delTitularDependente);
    odbc_exec($con, "UPDATE sf_fornecedores_despesas set fornecedores_status = dbo.FU_STATUS_CLI(" . $_POST['idCli'] . ") where id_fornecedores_despesas = " . $_POST['idCli'] . $delTitularDependente);
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    values ('sf_clientes', " . $_POST['idCli'] . ", '" . $_SESSION["login_usuario"] . "', 'E', 'EXCLUIR - CLIENTE', GETDATE(), " . $_POST['idCli'] . ")");
    echo "YES";
}

odbc_close($con);
