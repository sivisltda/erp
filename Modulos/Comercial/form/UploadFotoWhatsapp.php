<?php

require_once('../../../Connections/configini.php');
if (isset($_POST['btnPhotoWhatsapp'])) {
    $allowedExts = array("jpg", "JPG", "png", "PNG");
    $extension = explode(".", $_FILES["fotoWhatsapp"]["name"]);
    $extension = $extension[count($extension) - 1];
    if ((($_FILES["fotoWhatsapp"]["type"] == "image/jpeg") || ($_FILES["fotoWhatsapp"]["type"] == "image/jpg") || ($_FILES["fotoWhatsapp"]["type"] == "image/png")) && 
    ($_FILES["fotoWhatsapp"]["size"] < 10000000) && in_array($extension, $allowedExts)) {
        if ($_FILES["fotoWhatsapp"]["error"] == 0) {
            if (!is_dir("./../../../Pessoas/" . $contrato . "/Empresa/Whatsapp/" . (isset($_POST["type"]) ? $_POST["type"] . "/" : ""))) {
                mkdir("./../../../Pessoas/" . $contrato . "/Empresa/Whatsapp/" . (isset($_POST["type"]) ? $_POST["type"] . "/" : ""), 0777, true);
            }
            $nomeDoc = "logo." . $extension;
            move_uploaded_file($_FILES["fotoWhatsapp"]["tmp_name"], "./../../../Pessoas/" . $contrato . "/Empresa/Whatsapp/"  . (isset($_POST["type"]) ? $_POST["type"] . "/" : "") . $nomeDoc);
            echo "YES";
        }
    } else {
        echo 'Verifique se o arquivo é jpg/png ou possue tamanho menor do que 10 MB';
    }
}

if (isset($_POST['btnDelFile'])) {
    if (file_exists("./../../../Pessoas/" . $contrato . "/Empresa/Whatsapp/" . (isset($_POST["type"]) ? $_POST["type"] . "/" : "") . $_POST['txtFileName'])) {
        unlink("./../../../Pessoas/" . $contrato . "/Empresa/Whatsapp/" . (isset($_POST["type"]) ? $_POST["type"] . "/" : "") . $_POST['txtFileName']);
        echo "YES";
    }
}