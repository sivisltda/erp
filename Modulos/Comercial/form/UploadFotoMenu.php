<?php

require_once('../../../Connections/configini.php');
if (isset($_POST['btnFotoMenu'])) {
    $allowedExts = array("jpg", "JPG", "png", "PNG");
    $extension = explode(".", $_FILES["fotoLogoMenu"]["name"]);
    $extension = $extension[count($extension) - 1];
    if ((($_FILES["fotoLogoMenu"]["type"] == "image/jpeg") || ($_FILES["fotoLogoMenu"]["type"] == "image/jpg") || ($_FILES["fotoLogoMenu"]["type"] == "image/png")) && 
    ($_FILES["fotoLogoMenu"]["size"] < 10000000) && in_array($extension, $allowedExts)) {
        if ($_FILES["fotoLogoMenu"]["error"] == 0) {
            if (!is_dir("./../../../Pessoas/" . $contrato . "/Empresa/Menu/" . (isset($_POST["type"]) ? $_POST["type"] . "/" : ""))) {
                mkdir("./../../../Pessoas/" . $contrato . "/Empresa/Menu/" . (isset($_POST["type"]) ? $_POST["type"] . "/" : ""), 0777, true);
            }                
            $nomeDoc = "logo." . $extension;                
            move_uploaded_file($_FILES["fotoLogoMenu"]["tmp_name"], "./../../../Pessoas/" . $contrato . "/Empresa/Menu/"  . (isset($_POST["type"]) ? $_POST["type"] . "/" : "") . $nomeDoc);
            echo "YES";
        }
    } else {
        echo 'Verifique se o arquivo é jpg/png ou possue tamanho menor do que 10 MB';
    }
}

if (isset($_POST['btnDelFile'])) {
    if (file_exists("./../../../Pessoas/" . $contrato . "/Empresa/Menu/" . (isset($_POST["type"]) ? $_POST["type"] . "/" : "") . $_POST['txtFileName'])) {
        unlink("./../../../Pessoas/" . $contrato . "/Empresa/Menu/" . (isset($_POST["type"]) ? $_POST["type"] . "/" : "") . $_POST['txtFileName']);
        echo "YES";
    }
}