<?php

include "../../../Connections/configini.php";

if (isset($_POST['btnSave'])) {
    if (is_numeric($_POST['txtIdAcrSer'])) {
        $query = "UPDATE sf_configuracao_convenios SET " .
                "id_convenio = " . valoresSelect("txtServicoAcrSer") . "," .
                "min_dep = " . valoresNumericos("txtQtdMinAcrSer") . "," .
                "max_dep = " . valoresNumericos("txtQtdMaxAcrSer") . " " .
                "WHERE id = " . $_POST['txtIdAcrSer'];
    } else {
        $query = "INSERT INTO sf_configuracao_convenios(id_convenio,min_dep,max_dep) VALUES(" .
                valoresSelect("txtServicoAcrSer") . "," .
                valoresNumericos("txtQtdMinAcrSer") . "," .
                valoresNumericos("txtQtdMaxAcrSer") . ")";
    }
    odbc_exec($con, $query);
    echo "YES";
}

if (isset($_POST['btnDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        $query = "delete from sf_configuracao_convenios where id = " . $_POST['txtId'];
        odbc_exec($con, $query);
        echo "YES";
    }
}

if (isset($_POST['getAcrescimo'])) {
    $local = array();    
    $query = "select * from sf_configuracao_convenios where id = " . $_POST['getAcrescimo'];
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array(
            'id' => $RFP['id'], 
            'id_convenio' => $RFP['id_convenio'], 
            'min_dep' => $RFP['min_dep'], 
            'max_dep' => $RFP['max_dep']
        );
    }
    echo(json_encode($local));
}

if (isset($_GET['listAcrescimo'])) {
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    $query = "select * from sf_configuracao_convenios 
    inner join sf_convenios on sf_convenios.id_convenio = sf_configuracao_convenios.id_convenio";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row[] = "<a href=\"javascript:void(0)\" onClick=\"BuscarAcrescimoSer(" . $RFP["id"] . ")\">" . utf8_encode($RFP["descricao"]) . "</a>";
        $row[] = utf8_encode($RFP["min_dep"]);
        $row[] = utf8_encode($RFP["max_dep"]);
        $row[] = "<center><img src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' onClick=\"RemoverAcrescimoSer(" . $RFP["id"] . ")\"></center>";
        $output['aaData'][] = $row;
    }
    echo json_encode($output);
}

odbc_close($con);
