<?php

$imprimir = 0;
$disabled = 'disabled';
$id = '';
$descricao = '';
$horario = '';
$dias = '0';
$online = '';
$usuario = '';
$id_permissoes = '';
$inativo = '';
$excecao = '0';
$servico = '';
$pgObrig = '0';

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = 1;
}

if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_agenda set " .
        "descricao_agenda = UPPER(" . valoresTexto('txtDescricao') . ")," .
        "id_turno = " . valoresSelect('txtHorario') . "," .
        "terminal = " . valoresCheck('ckbOnline') . "," .
        "dias = " . valoresNumericos('txtDias') . "," .
        "user_resp = " . valoresSelect('txtUsuario') . "," .
        "user_permissao = " . valoresSelect('txtPermissoes') . "," .
        "inativo = " . valoresCheck('ckbInativo') . "," .
        "excecao = " . valoresCheck('ckbExcecao') . "," .
        "id_servico = " . valoresSelect('txtServico') . "," .
        "pg_obrig = " . valoresCheck('ckbPgObrig') . " " .                
        "where id_agenda = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_agenda(descricao_agenda,id_turno,terminal,dias,user_resp, 
        user_permissao,inativo,excecao,id_servico,pg_obrig)values(" .
        "UPPER(" . valoresTexto('txtDescricao') . ")," .
        valoresSelect('txtHorario') . "," .
        valoresCheck('ckbOnline') . "," .
        valoresNumericos('txtDias') . "," .
        valoresSelect('txtUsuario') . "," .
        valoresSelect('txtPermissoes') . "," .
        valoresCheck('ckbInativo') . "," .
        valoresCheck('ckbExcecao') . "," .
        valoresSelect('txtServico') . "," .
        valoresCheck('ckbPgObrig') . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_agenda from sf_agenda order by id_agenda desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}

if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        if (isset($_POST['List'])) {
            include "../../../Connections/configini.php";
        }
        odbc_exec($con, "DELETE FROM sf_agenda WHERE id_agenda = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox(1);</script>";
        exit();
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_agenda where id_agenda = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_agenda'];
        $descricao = utf8_encode($RFP['descricao_agenda']);
        $horario = utf8_encode($RFP['id_turno']);
        $dias = utf8_encode($RFP['dias']);
        $online = utf8_encode($RFP['terminal']);
        $usuario = utf8_encode($RFP['user_resp']);
        $id_permissoes = utf8_encode($RFP['user_permissao']);
        $inativo = utf8_encode($RFP['inativo']);
        $excecao = utf8_encode($RFP['excecao']);
        $servico = utf8_encode($RFP['id_servico']);
        $pgObrig = utf8_encode($RFP['pg_obrig']);
    }
} else {
    $disabled = '';
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}