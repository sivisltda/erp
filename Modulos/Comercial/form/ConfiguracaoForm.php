<?php

if (basename(str_replace("\\", "/", getcwd())) == "form") {
    $url = "../../../";
} else {
    $url = "../../";
}
require_once($url . "Connections/configini.php");
include __DIR__ . '../../../../util/nfe/vendor/autoload.php';

use NFePHP\NFe\ToolsNFe;
use NFePHP\Common\Configure\Configure;
use NFePHP\Common\Files\FilesFolders;

$nfeTools = "";
$nmArquivo = "";
$camposLivres;
$filename = './../../Pessoas/' . $contrato . '/Emitentes/*.pfx';

if (is_array(glob($filename))) {
    if (file_exists($url . 'Pessoas/' . $contrato . '/Emitentes/config.json')) {
        $nfeTools = new ToolsNFe($url . 'Pessoas/' . $contrato . '/Emitentes/config.json');
    }
}

if (isset($_POST['bntSave'])) {
    for ($i = 0; $i < 10; $i++) {
        $busca = $_POST["opbCLivres"] == $i ? "1" : "0";
        if (is_numeric($_POST["txtidCLivres" . $i])) {
            $query = "update sf_configuracao_campos_livres set descricao_campo = " . valoresTexto("txtCLivres" . $i) .
                    ",busca_campo = " . $busca . ",ativo_campo = " . valoresCheck("ckbCLivres" . $i) . " where id_campo = " . $_POST["txtidCLivres" . $i];
            odbc_exec($con, $query) or die(odbc_errormsg());
        } elseif ($_POST["txtCLivres" . $i] != "") {
            $query = "insert into sf_configuracao_campos_livres(descricao_campo,busca_campo,ativo_campo) values(" .
                    valoresTexto("txtCLivres" . $i) . "," . $busca . "," . valoresCheck("ckbCLivres" . $i) . ")";
            odbc_exec($con, $query) or die(odbc_errormsg());
        }
    }
    $defaultconfigfolder = $url . 'Pessoas/' . $contrato . '/Emitentes/';
    $tpAmb = valoresSelect('txtAmbiente');
    $certPfxName = $_POST['txtNmArquivo'];
    $certPassword = $_POST['txtSenhaCert'];
    $razaosocial = $_SESSION['txtRazaoSocial'];
    $txtTokenId = $_POST['txtTokenId'];
    $txtToken = $_POST['txtToken'];
    $cnpj = preg_replace('/[^0-9]/', '', $_SESSION["cpf_cnpj"]);
    //$dirPath = "C:\\Inetpub\\vhosts\\dragon296.startdedicated.com\\httpdocs\\finance\\Pessoas\\";
    $dirPath = "C:\\Inetpub\\vhosts\\sivisweb.com.br\\httpdocs\\finance\\Pessoas\\";
    if ($certPfxName !== "") {
        $aDocFormat = array(
            'format' => $nfeTools->aConfig['format'],
            'paper' => $nfeTools->aConfig['paper'],
            'southpaw' => $nfeTools->aConfig['southpaw'],
            'pathLogoFile' => $nfeTools->aConfig['pathLogoFile'],
            'logoPosition' => $nfeTools->aConfig['logoPosition'],
            'font' => $nfeTools->aConfig['font'],
            'printer' => $nfeTools->aConfig['printer']
        );
        $aMailConf = array(
            'mailAuth' => $nfeTools->aConfig['mailAuth'],
            'mailFrom' => $nfeTools->aConfig['mailFrom'],
            'mailSmtp' => $nfeTools->aConfig['mailSmtp'],
            'mailUser' => $nfeTools->aConfig['mailUser'],
            'mailPass' => $nfeTools->aConfig['mailPass'],
            'mailProtocol' => $nfeTools->aConfig['mailProtocol'],
            'mailPort' => $nfeTools->aConfig['mailPort'],
            'mailFromMail' => $nfeTools->aConfig['mailFromMail'],
            'mailFromName' => $nfeTools->aConfig['mailFromName'],
            'mailReplayToMail' => $nfeTools->aConfig['mailReplayToMail'],
            'mailReplayToName' => $nfeTools->aConfig['mailReplayToName'],
            'mailImapHost' => $nfeTools->aConfig['mailImapHost'],
            'mailImapPort' => $nfeTools->aConfig['mailImapPort'],
            'mailImapSecurity' => $nfeTools->aConfig['mailImapSecurity'],
            'mailImapNocerts' => $nfeTools->aConfig['mailImapNocerts'],
            'mailImapBox' => $nfeTools->aConfig['mailImapBox']
        );
        $aProxyConf = array(
            'proxyIp' => $nfeTools->aConfig['proxyIp'],
            'proxyPort' => $nfeTools->aConfig['proxyPort'],
            'proxyUser' => $nfeTools->aConfig['proxyUser'],
            'proxyPass' => $nfeTools->aConfig['proxyPass']
        );
        $aConfig = array(
            'atualizacao' => date('Y-m-d h:i:s'),
            'tpAmb' => $tpAmb !== "" ? $tpAmb : $nfeTools->aConfig['tpAmb'],
            'pathXmlUrlFileNFe' => $nfeTools->aConfig['pathXmlUrlFileNFe'],
            'pathXmlUrlFileCTe' => $nfeTools->aConfig['pathXmlUrlFileCTe'],
            'pathXmlUrlFileMDFe' => $nfeTools->aConfig['pathXmlUrlFileMDFe'],
            'pathXmlUrlFileCLe' => $nfeTools->aConfig['pathXmlUrlFileCLe'],
            'pathXmlUrlFileNFSe' => $nfeTools->aConfig['pathXmlUrlFileNFSe'],
            'pathNFeFiles' => $dirPath . $contrato . "\\NFE",
            'pathCTeFiles' => $dirPath . $contrato . "\\cte",
            'pathMDFeFiles' => $dirPath . $contrato . "\\mdfe",
            'pathCLeFiles' => $dirPath . $contrato . "\\cle",
            'pathNFSeFiles' => $dirPath . $contrato . "\\nfse",
            'pathCertsFiles' => $dirPath . $contrato . "\\Emitentes\\",
            'siteUrl' => $nfeTools->aConfig['siteUrl'],
            'schemesNFe' => $nfeTools->aConfig['schemesNFe'],
            'schemesCTe' => $nfeTools->aConfig['schemesCTe'],
            'schemesMDFe' => $nfeTools->aConfig['schemesMDFe'],
            'schemesCLe' => $nfeTools->aConfig['schemesCLe'],
            'schemesNFSe' => $nfeTools->aConfig['schemesNFSe'],
            'razaosocial' => $razaosocial,
            'siglaUF' => $nfeTools->aConfig['siglaUF'],
            'cnpj' => $cnpj !== "" ? $cnpj : $nfeTools->aConfig['cnpj'],
            'tokenIBPT' => $nfeTools->aConfig['tokenIBPT'],
            'tokenNFCe' => $txtToken !== "" ? $txtToken : $nfeTools->aConfig['tokenNFCe'],
            'tokenNFCeId' => $txtTokenId !== "" ? $txtTokenId : $nfeTools->aConfig['tokenNFCeId'],
            'certPfxName' => $certPfxName !== "" ? $certPfxName : $nfeTools->aConfig['certPfxName'],
            'certPassword' => $certPassword !== "" ? $certPassword : $nfeTools->aConfig['certPassword'],
            'certPhrase' => $nfeTools->aConfig['certPhrase'],
            'aDocFormat' => $nfeTools->aConfig['aDocFormat'],
            'aMailConf' => $nfeTools->aConfig['aMailConf'],
            'aProxyConf' => $nfeTools->aConfig['aProxyConf']
        );
        $content = json_encode($aConfig);
        if (!$resdefault = FilesFolders::saveFile($defaultconfigfolder, 'config.json', $content)) {
            echo "Erro ao salvar";
            exit(0);
        }
    }
    $query = "UPDATE sf_configuracao SET
    FIN_VEN_COMANDA_OBRIGATORIA = " . valoresCheck('txtComandaObrigatoria') . ",
    FIN_VEN_CLIENTE_OBRIGATORIO = " . valoresCheck('txtClienteObrigatorio') . ",
    FIN_VEN_COMISS_OBRIG = " . valoresCheck('txtComissionadoObrigatorio') . ",
    FIN_VEN_COMANDA_UNICA = " . valoresCheck('txtComandaUnica') . ",
    FIN_VEN_COM_UN_GER = " . valoresCheck('txtComandaUnicaGerar') . ",
    FIN_VEN_QTD_DEC = " . valoresCheck('txtQuantidadeDecimal') . ",
    FIN_VEN_COMANDA_PERSONALIZADA = " . valoresCheck('txtComandaPersonalizada') . ",
    FIN_VEN_INDICADOR = " . valoresCheck('txtIndicadorComanda') . ",
    FIN_VEN_FILTRAR_COMISS_FILIAL = " . valoresCheck('txtFiltroComissionado') . ",
    FIN_ORC_FOTO = " . valoresCheck('txtOrcamentoImagem') . ",
    FIN_ORC_CODIGO_PRODUTO = " . valoresCheck('txtCompraCodProd') . ",
    FIN_VEN_COMANDA_MASCARA = UPPER(" . valoresTexto('txtComandaMascara') . ") ,
    ACA_PLN_DIAS_RENOVACAO = " . valoresNumericos('txtDiasRenovacao') . ",
    ACA_DIAS_SUSPENSO = " . valoresNumericos('txtDiasSuspenso') . ",
    ACA_PLN_DIAS_TOLERANCIA = " . valoresNumericos('txtDiasTolerancia') . ",
    ACA_PLN_DIAS_TOLERANCIA_CRED = " . valoresNumericos('txtDiasToleranciaCredencial') . ",    
    ACA_ORDEM_MENS = " . valoresSelect('txtOrdemMens') . ",
    ACA_RENOVACAO_AUTO = " . valoresCheck('txtMensAuto') . ",
    ACA_BEMATECH = " . valoresCheck('txtBemaMp20') . ",
    ACA_DEL_TITU_DEP = " . valoresCheck('txtDelTituDep') . ",
    ACA_DOC_NOVO_ALU = " . valoresSelect('txtDocNovoAluno') . ",
    ACA_PROSP_CLI = " . valoresCheck('txtCadProspCli') . ",
    ACA_AT_GYMWEB = " . valoresCheck('txtBloqAtGymWeb') . ",
    ACA_EX_VEN_PR = " . valoresCheck('txtVenConExProRata') . ",
    ACA_EX_PJ = " . valoresCheck('txtExcecaoPJ') . ",
    DCC_EMPRESA = " . valoresSelect('txtEmpresaDCC') . ",
    DCC_DESCRICAO = " . valoresTexto('txtDescricaoDcc') . ",
    DCC_CHAVE = " . valoresTexto('txtChaveDCC') . ",
    DCC_FORMA_PAGAMENTO = " . valoresSelect('txtDCCFormaPagamento') . ",
    DCC_EMPRESA2 = " . valoresSelect('txtEmpresaDCC2') . ",
    DCC_DESCRICAO2 = " . valoresTexto('txtDescricaoDcc2') . ",
    DCC_CHAVE2 = " . valoresTexto('txtChaveDCC2') . ",
    DCC_FORMA_PAGAMENTO2 = " . valoresSelect('txtDCCFormaPagamento2') . ",
    ESTUD_LIMIT_AGND = " . valoresTexto('txtlimitAgnd') . ",
    SMS_USUARIO = " . valoresTexto('txtUsuarioSMS') . ",
    SMS_IDENTIFICADOR = " . valoresTexto('txtIdentificadorSMS') . ",
    SMS_SENHA = " . valoresTexto('txtSenhaSMS') . ",
    Nfs_Ambiente = " . valoresSelect('txtNfsAmbiente') . ",
    Nfs_Empresa = " . valoresTexto('txtNfsEmpresa') . ",
    Nfs_Chave = " . valoresTexto('txtNfsChave') . ",
    CMP_FORMA = " . valoresSelect('txtForma_Preco') . ",
    CMP_VALOR = " . valoresNumericos('txtMargem_lucro') . ",
    Faixa_I1 = " . valoresNumericos('txtFaixaI1') . ",
    Faixa_F1 = " . valoresNumericos('txtFaixaF1') . ",
    Faixa_I2 = " . valoresNumericos('txtFaixaI2') . ",
    Faixa_F2 = " . valoresNumericos('txtFaixaF2') . ",
    Faixa_I3 = " . valoresNumericos('txtFaixaI3') . ",
    Faixa_F3 = " . valoresNumericos('txtFaixaF3') . ",
    Faixa_I4 = " . valoresNumericos('txtFaixaI4') . ",
    Faixa_F4 = " . valoresNumericos('txtFaixaF4') . ",
    Faixa_I5 = " . valoresNumericos('txtFaixaI5') . ",
    Faixa_F5 = " . valoresNumericos('txtFaixaF5') . ",
    gyp_recid = " . valoresTexto('txtRecid') . ",
    gyp_ptype = " . valoresTexto('txtPtype') . ",
    gyp_token = " . valoresTexto('txtAuthToken') . ",
    gyp_recid2 = " . valoresTexto('txtRecid2') . ",
    gyp_ptype2 = " . valoresTexto('txtPtype2') . ",
    gyp_token2 = " . valoresTexto('txtAuthToken2') . ",
    adm_msg_cotacao = " . valoresTexto('txtMsgProposta') . ",
    CONF_REGIONAIS = " . valoresSelectTexto('txtConfRegionais') . ",
    cat_topdata_lista = " . valoresCheck('txtTopDataDigi') . ",
    ACA_TIPO_VENCIMENTO = " . valoresSelect('txtTipoVencimento') . ",
    ACA_DIA_VENCIMENTO = " . valoresNumericos('txtDiaVencimento') . ",
    ACA_TIPO_MULTA = " . valoresSelect('txtTipoMulta') . ",
    ACA_FORMA_MULTA = " . valoresSelect('txtFormaMulta') . ",
    ACA_DIAS_LIMITE = " . valoresNumericos('txtDiasLimite') . ",
    ACA_TAXA_MULTA = " . valoresNumericos('txtTaxaInicial') . ",
    ACA_TAXA_MULTA_DIAS = " . valoresNumericos('txtTaxaInicialDias') . ",
    ACA_MORA_MULTA = " . valoresNumericos('txtMoraDiaria') . ",
    ACA_TOLERANCIA_MULTA = " . valoresNumericos('txtToleranciaMulta') . ",
    ACA_DESC_TIPO = " . valoresNumericos('txtTipoDesconto') . ",
    ACA_DESC_DIAS = " . valoresNumericos('txtDescontoDias') . ",
    ACA_DESC_VALOR = " . valoresNumericos('txtDescontoValor') . ",    
    use_cnt = " . valoresCheck('txtUseContrato') . ",        
    adm_dependente_parentesco = " . valoresCheck('txtDependenteParentesco') . ",
    adm_plano_unico = " . valoresCheck('txtPlanoUnico') . ",
    adm_terminal_venda = " . valoresCheck('txtTerminalVenda') . ",        
    adm_venda_online = " . valoresCheck('txtContratacaoVenda') . ",        
    adm_agen_hmarc = " . valoresCheck('txtAdmAgenHmarc') . ",        
    adm_agen_umamar = " . valoresCheck('txtAdmAgenUmamar') . ",
    operadora_dcc_site = " . valoresSelect('txtDccOperadora') . ",
    fin_ccontas_movimento = " . valoresSelect('txtGruposSubCred') . ",
    fin_dcontas_movimento = " . valoresSelect('txtGruposSubDeb') . ",
    pag_avancada_adm = " . valoresNumericos('txt_out_pag_avancada') . ",
    clb_dias_convite = " . valoresNumericos('txtConvitesDias') . ",        
    clb_limite_convite = " . valoresNumericos('txtConvitesDep') . ",        
    clb_mensalidades_aberto = " . valoresNumericos('txtMensalidadesAberto') . ",        
    DCC_FILIAL = " . valoresSelect('txtFilialDCC') . ",
    DCC_FILIAL2 = " . valoresSelect('txtFilialDCC2') . ",
    FIN_GRUPO_BOLETO = " . valoresCheck('txtGrupoBoleto') . ",
    FIN_GRUPO_BOLETO_MOD = " . valoresCheck('txtGrupoBoletoMod') . ",
    ACA_SERVICO_ADESAO = " . valoresCheck('txtServicoAdesao') . ",
    DCC_RECORRENCIA = " . valoresNumericos('txtRecorrencia') . ",        
    adm_agen_intervalo = " . valoresSelect('txtAgenIntervalo') . ",
    adm_agen_semanal = " . valoresNumericos('txtAgenSemanal') . ",        
    adm_num_agendamento = " . valoresNumericos('txtAgenPeriodo') . ",        
    adm_agen_um_tipo = " . valoresSelect('txtAgenUmTipo') . ",        
    clb_pagamento_agrupado = " . valoresCheck('txtPagamentoAgrupado') . ",
    ACA_DIAS_TOLERANCIA_PARENTESCO = " . valoresNumericos('txtToleranciaParentesco') . ",
    adm_excecao_baixa = " . valoresCheck('txtExcecaoBaixa') . ",
    adm_agen_ativo = " . valoresCheck('txtAdmCliAtivos') . ",
    dcc_dias_boleto = " . valoresNumericos('txtDiasBoleto') . ",              
    aca_dias_tolerancia_status = " . valoresNumericos('txtDiasToleranciaStatus') . ",
    adm_atualizar_mens = " . valoresCheck('txtAtualizarMens') . ",        
    adm_catraca_urna = " . valoresCheck('txtCatracaUrna') . ",        
    bol_baixa_enc = " . valoresCheck('txtBolBaixaEnc') . ",        
    bol_cart_padrao = " . valoresSelect('txtCarteira') . ",        
    wa_tipo = " . valoresSelect('txtWaTipo') . ",             
    wa_ambiente = " . valoresSelect('txtWaAmbiente') . ",
    wa_login = " . valoresTexto('txtWaLogin') . ",        
    wa_senha = " . valoresTexto('txtWaSenha') . ",
    wa_chave = " . valoresTexto('txtWaChave') . ",
    wa_empresa = " . valoresTexto('txtWaEmpresa') . ",
    ra_tipo = " . valoresSelect('txtRaTipo') . ",             
    ra_ambiente = " . valoresSelect('txtRaAmbiente') . ",
    ra_chave = " . valoresTexto('txtRaChave') . ",
    cor_primaria = " . valoresTexto('cor_primaria') . ",
    cor_secundaria = " . valoresTexto('cor_secundaria') . ",
    cor_destaque = " . valoresTexto('cor_destaque') . ",
    dcc_pagarme = " . valoresTexto('txtChavePgme') . ",
    dcc_galaxpay = " . valoresTexto('txtChaveGalax') . ",
    dcc_galaxId = " . valoresTexto('txtChaveGalaxId') . ",
    dcc_galaxTeste = " . valoresSelect('txtGalaxTeste') . ",        
    dcc_boleto_tipo = " . valoresSelect('txtBolTipo') . ",
    dcc_pix_tipo = " . valoresSelect('txtPixTipo') . ",
    dcc_tef = " . valoresSelect('txtDccTef') . ",
    dcc_multa = " . valoresCheck('txtExcecaoBaixaDcc') . ",
    com_forma = " . valoresSelect('txtComissaoForma') . ",
    adm_func_usuario = " . valoresSelect('txtFuncUsuario') . ",
    seg_piso_franquia = " . valoresNumericos('txtPisoFranquia') . ",            
    recibo_tipo = " . valoresSelect('txtRecibo') . ",            
    convite_tipo = " . valoresSelect('txtConvite') . ",            
    recibo_vias = " . valoresNumericos('txtReciboVias') . ",            
    ACA_TOL_ASSISTENCIA = " . valoresNumericos('txtDiasToleranciaAssist') . ",
    adm_bloq_deb_cart = " . valoresCheck('txtBloqDebCarteira') . ",
    adm_bloq_cart_vencida = " . valoresCheck('txtBloqCarteiraVencida') . ",
    aca_adesao_dep = " . valoresCheck('txtAdesaoDep') . ",
    aca_add_dep = " . valoresCheck('txtAddDep') . ",
    aca_pes_cot_tip = " . valoresSelect('txtPessoaCotacaoTipo') . ",
    aca_prosp_pag = " . valoresCheck('txtProspPag') . ",
    aca_atv_aberto = " . valoresCheck('txtAtvAberto') . ",
    seg_cota_defaut = " . valoresNumericos('txtCotaDefault') . ",
    seg_ano_veiculo_piso = " . valoresNumericos('txtLimiteAnos') . ",
    seg_cota_protegido = " . valoresNumericos('txtCotaProtegido') . ",                
    fin_baixa_menor_carteira = " . valoresCheck('txtBxLayCarteira') . ",
    seg_vei_plan_obrig = " . valoresCheck('txtVeiPlanObrig') . ",
    seg_vei_cotac_person = " . valoresCheck('txtVeiCotPersonal') . ",
    fin_venda_subprod = " . valoresNumericos('txtVendaSubProd') . ",
    fin_venda_subserv = " . valoresNumericos('txtVendaSubServ') . ",
    fin_venda_subplan = " . valoresNumericos('txtVendaSubPlan') . ",
    fin_venda_subout = " . valoresNumericos('txtVendaSubOut') . ",        
    fin_venda_filial = " . valoresSelect('txtIdFilialVenda') . ",        
    aca_dias_cancelamento = " . valoresNumericos('txtDiasCancelamento') . ",        
    adm_agrupar_plan_item = " . valoresCheck('txtAgruparPlanItem') . ",        
    adm_plan_mens_plan_contrat = " . valoresCheck('txtPlanMensPlanContrat') . ",
    adm_msg_boas_vindas = " . valoresTexto('txtMsgBoasVindas') . ",
    adm_msg_taxa_adesao = " . valoresTexto('txtTaxaAdesao') . ",
    adm_msg_termos_email = " . valoresTexto('txtMsgTermosEmail') . ",
    adm_msg_termos_whats = " . valoresTexto('txtMsgTermosWhats') . ",
    adm_msg_link_pag = " . valoresTexto('txtLinkWhatsPag') . ",
    adm_nao_alterar_tp_pag = " . valoresCheck('txtNaoAlterarTipoPag') . ",  
    adm_nao_proc_pag_online = " . valoresCheck('txtNaoProcPagOnline') . ",  
    PIX_CHAVE = " . valoresTexto('txtChavePix') . ",
    PIX_FORMA_PAGAMENTO = " . valoresSelect('txtDCCFormaPagamentoPix') . ",        
    seg_placa_tp = " . valoresNumericos('txtSegPlacaTp') . ",        
    seg_placa_cod = " . valoresTexto('txtSegPlacaCod') . ",        
    BOL_FORMA_PAGAMENTO = " . valoresSelect('txtDCCFormaPagamentoBol') . ",        
    adm_dep_online = " . valoresSelect('txtDepOnline') . ",        
    adm_desc_vencimento = " . valoresCheck('txtDescVencimento') . ",
    adm_doc_assinatura = " . valoresCheck('txtDocAssinatura') . ",        
    adm_cad_car_fil = " . valoresCheck('txtCadCarFil') . ",
    seg_dia_vist = " . valoresNumericos('txtAposDia') . ",        
    seg_modelo_vist = " . valoresSelect('txtModeloVistoria') . ",  
    seg_modelo_vist_moto = " . valoresSelect('txtModeloVistoriaMoto') . ",        
    seg_modelo_disparo = " . valoresSelect('txtModeloDisparo') . ",
    wa_padrao_portal = " . valoresSelect('txtPortalWhatsapp') . ",
    adm_associado = " . valoresTexto('txtAssociado') . ",
    mens_n_ant = " . valoresNumericos('txtMensAnt') . ",         
    mens_n_apos = " . valoresNumericos('txtMensPost') . ",     
    adm_desc_isencao_ie = " . valoresSelect('txtIsencaoIE') . ",         
    adm_msg_contratacao = " . valoresTexto('txtMsgContratacao') . ",        
    adm_mens_acess_soma = " . valoresCheck('txtMensPlanAcess') . ",        
    adm_nao_valor_print = " . valoresCheck('txtNaoValorPrint') . ",
    adm_desconto_range = " . valoresCheck('txtDescontoPorRange') . ",
    adm_cli_ordem = " . valoresSelect('txtCliOrdem') . ",
    adm_atv_bol = " . valoresSelect('txtAtvBol') . ",
    adm_baixa_layout = " . valoresCheck('txtBaixaLayoutDesl') . ",         
    link_forma_pagamento = " . valoresSelect('txtLinkFormaPagamento') . ",        
    portal_contrato = " . valoresSelect('txtContratoAgrp') . ",        
    link_max_parcelas = " . valoresNumericos('txtLinkMaxParc') . ",
    clb_pagamento_credencial = " . valoresCheck('txtPagamentoCredencial') . ",
    clb_limite_credencial = " . valoresNumericos('txtCredencialDep') . ",
    clb_dias_credencial = " . valoresNumericos('txtCredencialDias') . ",     
    add_whatsapp = " . valoresNumericos('txtAddWhatsapp') . ",     
    desconto_decimal = " . valoresNumericos('txtDescDecimal') . ";";       
    $cur = odbc_exec($con, "select id, descricao, conteudo from sf_mensagens_acesso order by id") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        if (isset($_POST["txtMensagem_" . $RFP["id"]])) {
            $query .= "UPDATE sf_mensagens_acesso SET conteudo = " . valoresTexto("txtMensagem_" . $RFP["id"]) . " where id = ". valoresTexto2($RFP["id"]) . ";";
        }
    }    
    //echo $query; exit;    
    odbc_exec($con, $query) or die(odbc_errormsg());
}

if (isset($_GET['UploadCertificado'])) {
    if ($_FILES['arquivo']['name']) {
        $nomArq = $_FILES['arquivo']['name'];
        $tmpArq = $_FILES['arquivo']['tmp_name'];
    }
    $filename = "../../../Pessoas/" . $contrato . "/Emitentes/" . $nomArq;
    if (move_uploaded_file($tmpArq, $filename)) {
        echo $nomArq;
    } else {
        echo "NO";
    }
}

if (isset($_POST['RemoveCertificado'])) {
    if (file_exists("./../../../Pessoas/" . $contrato . "/Emitentes/" . $_POST['nmArquivo'])) {
        if (unlink("./../../../Pessoas/" . $contrato . "/Emitentes/" . $_POST['nmArquivo'])) {
            echo "YES";
        } else {
            echo "NO";
        }
    } else {
        echo "NO";
    }
}

if (isset($_POST['btnLimpar'])) {
    $query = "update sf_configuracao set tp_operacao_dcc = null, last_crd_dcc = null,last_sqt_dcc = null,last_sdt_dcc = null where id = 1";
    odbc_exec($con, $query) or die(odbc_errormsg());
}

if (isset($_POST['TestaSenha'])) {
    $cnpj = preg_replace('/[^0-9]/', '', $_SESSION["cpf_cnpj"]);
    $pathCertsFiles = "./../../../Pessoas/" . $contrato . "/Emitentes/";
    $certPfxName = $_POST['nmArquivo'];
    $certPassword = $_POST['txtSenhaCert'];
    $aResp = Configure::checkCerts($cnpj, $pathCertsFiles, $certPfxName, $certPassword);
    echo json_encode($aResp);
}

$cur = odbc_exec($con, "select * from sf_configuracao where id = 1");
while ($RFP = odbc_fetch_array($cur)) {
    $CheckComandaObr = $RFP['FIN_VEN_COMANDA_OBRIGATORIA'];
    $CheckClienteObr = $RFP['FIN_VEN_CLIENTE_OBRIGATORIO'];
    $CheckComissionadoObr = $RFP['FIN_VEN_COMISS_OBRIG'];
    $CheckComandaUni = $RFP['FIN_VEN_COMANDA_UNICA'];
    $CheckComandaUniGerar = $RFP['FIN_VEN_COM_UN_GER'];
    $QuantidadeDecimal = $RFP['FIN_VEN_QTD_DEC'];
    $CheckComandaPers = $RFP['FIN_VEN_COMANDA_PERSONALIZADA'];
    $CheckIndicador = $RFP['FIN_VEN_INDICADOR'];
    $CheckFiltroComissionado = $RFP['FIN_VEN_FILTRAR_COMISS_FILIAL'];
    $CheckOrcamentoImagem = $RFP['FIN_ORC_FOTO'];
    $CheckCompraCodProd = $RFP['FIN_ORC_CODIGO_PRODUTO'];
    $txtComandaMascara = $RFP['FIN_VEN_COMANDA_MASCARA'];
    $txtDiasRenovacao = $RFP['ACA_PLN_DIAS_RENOVACAO'];
    $txtDiasSuspenso = $RFP['ACA_DIAS_SUSPENSO'];
    $txtDiasTolerancia = $RFP['ACA_PLN_DIAS_TOLERANCIA'];
    $txtDiasToleranciaCredencial = $RFP['ACA_PLN_DIAS_TOLERANCIA_CRED'];
    $txtOrdemMens = $RFP['ACA_ORDEM_MENS'];
    $txtMensAuto = $RFP['ACA_RENOVACAO_AUTO'];
    $txtBemaMp20 = $RFP['ACA_BEMATECH'];
    $txtDelTituDep = $RFP['ACA_DEL_TITU_DEP'];
    $txtDocNovoAluno = $RFP['ACA_DOC_NOVO_ALU'];
    $txtCadProspCli = $RFP['ACA_PROSP_CLI'];
    $txtBloqAtGymWeb = $RFP['ACA_AT_GYMWEB'];    
    $txtVenConExProRata = $RFP['ACA_EX_VEN_PR'];
    $txtExcecaoPJ = $RFP['ACA_EX_PJ'];
    if ($nfeTools !== "") {
        $nmArquivo = $nfeTools->aConfig['certPfxName'];
        $tpAmb = $nfeTools->aConfig['tpAmb'];
        if (!file_exists($url . "Pessoas/" . $contrato . "/Emitentes/" . $nmArquivo)) {
            $nmArquivo = "";
        }
        $txtTokenId = $nfeTools->aConfig['tokenNFCeId'];
        $txtToken = $nfeTools->aConfig['tokenNFCe'];
    }
    $txtEmpresaDCC = $RFP['DCC_EMPRESA'];
    $txtDescricaoDcc = $RFP['DCC_DESCRICAO'];
    $txtChaveDcc = $RFP['DCC_CHAVE'];
    $formaPagamento = $RFP['DCC_FORMA_PAGAMENTO'];
    $txtEmpresaDCC2 = $RFP['DCC_EMPRESA2'];
    $txtDescricaoDcc2 = $RFP['DCC_DESCRICAO2'];
    $txtChaveDcc2 = $RFP['DCC_CHAVE2'];
    $formaPagamento2 = $RFP['DCC_FORMA_PAGAMENTO2'];
    $txtUsuarioSMS = $RFP['SMS_USUARIO'];
    $txtSenhaSMS = $RFP['SMS_SENHA'];
    $txtIdentificadorSMS = $RFP['SMS_IDENTIFICADOR'];
    $tpNfsAmb = $RFP['Nfs_Ambiente'];
    $txtNfsEmpresa = $RFP['Nfs_Empresa'];
    $txtNfsChave = $RFP['Nfs_Chave'];
    $formaPreco = $RFP['CMP_FORMA'];
    $txtlimitAgnd = $RFP['ESTUD_LIMIT_AGND'];
    $margem_lucro = escreverNumero($RFP['CMP_VALOR']);
    $txtFaixaI1 = $RFP['Faixa_I1'];
    $txtFaixaF1 = $RFP['Faixa_F1'];
    $txtFaixaI2 = $RFP['Faixa_I2'];
    $txtFaixaF2 = $RFP['Faixa_F2'];
    $txtFaixaI3 = $RFP['Faixa_I3'];
    $txtFaixaF3 = $RFP['Faixa_F3'];
    $txtFaixaI4 = $RFP['Faixa_I4'];
    $txtFaixaF4 = $RFP['Faixa_F4'];
    $txtFaixaI5 = $RFP['Faixa_I5'];
    $txtFaixaF5 = $RFP['Faixa_F5'];
    $txtConfRegionais = $RFP['CONF_REGIONAIS'];
    $txtRecid = $RFP['gyp_recid'];
    $txtPtype = $RFP['gyp_ptype'];
    $txtAuthToken = $RFP['gyp_token'];
    $txtRecid2 = $RFP['gyp_recid2'];
    $txtPtype2 = $RFP['gyp_ptype2'];
    $txtAuthToken2 = $RFP['gyp_token2'];
    $txtMsgProposta = utf8_encode($RFP['adm_msg_cotacao']);
    $txtTopDataDigi = $RFP['cat_topdata_lista'];
    $txtTipoVencimento = $RFP['ACA_TIPO_VENCIMENTO'];
    $txtDiaVencimento = is_numeric($RFP['ACA_DIA_VENCIMENTO']) ? $RFP['ACA_DIA_VENCIMENTO'] : "1";
    $txtTaxaInicial = escreverNumero($RFP['ACA_TAXA_MULTA'], 0, 4);
    $txtTaxaInicialDias = $RFP['ACA_TAXA_MULTA_DIAS'];    
    $txtMoraDiaria = escreverNumero($RFP['ACA_MORA_MULTA'], 0, 4);    
    $txtTipoDesconto = $RFP['ACA_DESC_TIPO'];    
    $txtDescontoDias = $RFP['ACA_DESC_DIAS'];    
    $txtDescontoValor = escreverNumero($RFP['ACA_DESC_VALOR'], 0, 2);    
    $txtFormaMulta = $RFP['ACA_FORMA_MULTA'];
    $txtTipoMulta = $RFP['ACA_TIPO_MULTA'];
    $txtDiasLimite = $RFP['ACA_DIAS_LIMITE'];
    $txtToleranciaMulta = $RFP['ACA_TOLERANCIA_MULTA'];
    $txtUseContrato = $RFP['use_cnt'];    
    $txtDependenteParentesco = $RFP['adm_dependente_parentesco'];
    $txtPlanoUnico = $RFP['adm_plano_unico'];
    $txtTerminalVenda = $RFP['adm_terminal_venda'];    
    $txtContratacaoVenda = $RFP['adm_venda_online'];    
    $txtAdmAgenHmarc = $RFP['adm_agen_hmarc'];    
    $txtAdmAgenUmamar = $RFP['adm_agen_umamar'];
    $operadora_dcc = $RFP['operadora_dcc_site'];
    $txt_out_pag_avancada = $RFP['pag_avancada_adm'];
    $txtConvitesDias = $RFP['clb_dias_convite'];
    $txtConvitesDep = $RFP['clb_limite_convite'];        
    $txtMensalidadesAberto = $RFP['clb_mensalidades_aberto'];
    $filialDCC = $RFP['DCC_FILIAL'];
    $filialDCC2 = $RFP['DCC_FILIAL2'];
    $txtGrupoBoleto = $RFP['FIN_GRUPO_BOLETO'];
    $txtGrupoBoletoMod = $RFP['FIN_GRUPO_BOLETO_MOD'];
    $txtPagamentoAgrupado = $RFP['clb_pagamento_agrupado'];
    $txtServicoAdesao = $RFP['ACA_SERVICO_ADESAO'];
    $txtRecorrencia = $RFP['DCC_RECORRENCIA'];
    $txtToleranciaParentesco = $RFP['ACA_DIAS_TOLERANCIA_PARENTESCO'];    
    $txtAgenIntervalo = $RFP['adm_agen_intervalo'];
    $txtAgenSemanal = $RFP['adm_agen_semanal'];
    $txtAgenPeriodo = $RFP['adm_num_agendamento'];    
    $txtAgenUmTipo = $RFP['adm_agen_um_tipo'];    
    $txtAdmCliAtivos = $RFP['adm_agen_ativo'];
    $txtExcecaoBaixa = $RFP['adm_excecao_baixa'];
    $txtDiasBoleto = $RFP['dcc_dias_boleto'];
    $txtDiasToleranciaStatus = $RFP['aca_dias_tolerancia_status'];
    $txtAtualizarMens = $RFP['adm_atualizar_mens'];    
    $txtCatracaUrna = $RFP['adm_catraca_urna'];    
    $txtBolBaixaEnc = $RFP['bol_baixa_enc'];       
    $txtBolCartPadrao = $RFP['bol_cart_padrao'];    
    $txtWaTipo = utf8_encode($RFP['wa_tipo']);
    $txtWaLogin = utf8_encode($RFP['wa_login']);
    $txtWaAmbiente = utf8_encode($RFP['wa_ambiente']);
    $txtWaSenha = utf8_encode($RFP['wa_senha']);
    $txtWaChave = utf8_encode($RFP['wa_chave']);    
    $txtRaTipo = utf8_encode($RFP['ra_tipo']);
    $txtRaAmbiente = utf8_encode($RFP['ra_ambiente']);
    $txtRaChave = utf8_encode($RFP['ra_chave']);    
    $txtWaEmpresa = utf8_encode($RFP['wa_empresa']);
    $txtFuncUsuario = $RFP['adm_func_usuario'];    
    $txtDccTef = $RFP['dcc_tef'];        
    $txtChavePgme = $RFP['dcc_pagarme'];
    $txtChaveGalax = $RFP['dcc_galaxpay'];
    $txtChaveGalaxId = $RFP['dcc_galaxId'];
    $txtGalaxTeste = $RFP['dcc_galaxTeste'];    
    $txtBolTipo = $RFP['dcc_boleto_tipo'];    
    $txtPixTipo = $RFP['dcc_pix_tipo'];    
    $txtExcecaoBaixaDcc = $RFP['dcc_multa'];
    $cor_primaria = $RFP['cor_primaria'];
    $cor_secundaria = $RFP['cor_secundaria'];
    $cor_destaque = $RFP['cor_destaque'];
    $txtComissaoForma = $RFP['com_forma'];
    $txtPisoFranquia = escreverNumero($RFP['seg_piso_franquia']);
    $txtBloqDebCarteira = $RFP['adm_bloq_deb_cart'];
    $txtBloqCarteiraVencida = $RFP['adm_bloq_cart_vencida'];
    $txtDiasToleranciaAssist = $RFP['ACA_TOL_ASSISTENCIA'];
    $txtAtvAberto = $RFP['aca_atv_aberto'];    
    $txtAdesaoDep = $RFP['aca_adesao_dep'];
    $txtAddDep = $RFP['aca_add_dep'];
    $txtPessoaCotacaoTipo = $RFP['aca_pes_cot_tip'];
    $txtRecibo = $RFP['recibo_tipo'];    
    $txtConvite = $RFP['convite_tipo'];    
    $txtReciboVias = $RFP['recibo_vias'];    
    $txtProspPag = $RFP['aca_prosp_pag'];
    $txtCotaDefault = escreverNumero($RFP['seg_cota_defaut']);
    $txtLimiteAnos = $RFP['seg_ano_veiculo_piso'];
    $txtCotaProtegido = $RFP['seg_cota_protegido'];
    $txtBxLayCarteira = $RFP['fin_baixa_menor_carteira'];
    $txtVeiPlanObrig = $RFP['seg_vei_plan_obrig'];
    $txtVeiCotPersonal = $RFP['seg_vei_cotac_person'];
    $txtDiasCancelamento = $RFP['aca_dias_cancelamento'];    
    $portal_whatsapp = $RFP['wa_padrao_portal'];
    $txtAgruparPlanItem = $RFP['adm_agrupar_plan_item'];
    $txtPlanMensPlanContrat = $RFP['adm_plan_mens_plan_contrat'];
    $txtVendaSubProd = $RFP['fin_venda_subprod'];
    $txtVendaSubServ = $RFP['fin_venda_subserv'];    
    $txtVendaSubPlan = $RFP['fin_venda_subplan'];    
    $txtMsgBoasVindas = utf8_encode($RFP['adm_msg_boas_vindas']);
    $txtTaxaAdesao = utf8_encode($RFP['adm_msg_taxa_adesao']);    
    $txtMsgTermosEmail = utf8_encode($RFP['adm_msg_termos_email']);    
    $txtMsgTermosWhats = utf8_encode($RFP['adm_msg_termos_whats']);    
    $txtLinkWhatsPag = utf8_encode($RFP['adm_msg_link_pag']);    
    $txtNaoAlterarTipoPag = $RFP['adm_nao_alterar_tp_pag'];    
    $txtNaoProcPagOnline = $RFP['adm_nao_proc_pag_online'];    
    $txtChavePix = $RFP['PIX_CHAVE'];
    $formaPagamentoPix = $RFP['PIX_FORMA_PAGAMENTO'];
    $formaPagamentoBol = $RFP['BOL_FORMA_PAGAMENTO'];
    $txtVendaSubOut = $RFP['fin_venda_subout'];        
    $txtIdFilialVenda = $RFP['fin_venda_filial'];        
    $txtSegPlacaTp = $RFP['seg_placa_tp'];        
    $txtSegPlacaCod = $RFP['seg_placa_cod'];        
    $txtDescVencimento = $RFP['adm_desc_vencimento'];        
    $txtDepOnline = $RFP['adm_dep_online'];
    $txtDocAssinatura = $RFP['adm_doc_assinatura'];     
    $txtCadCarFil = $RFP['adm_cad_car_fil'];     
    $txtAposDia = $RFP['seg_dia_vist']; 
    $txtModeloVistoria = $RFP['seg_modelo_vist']; 
    $txtModeloVistoriaMoto = $RFP['seg_modelo_vist_moto'];     
    $txtModeloDisparo = $RFP['seg_modelo_disparo']; 
    $txtAssociado = utf8_encode($RFP['adm_associado']);    
    $txtMensAnt = $RFP['mens_n_ant'];   
    $txtMensPost = $RFP['mens_n_apos'];         
    $txtIsencaoIE = $RFP['adm_desc_isencao_ie'];    
    $txtMsgContratacao = utf8_encode($RFP['adm_msg_contratacao']);    
    $txtMensPlanAcess = utf8_encode($RFP['adm_mens_acess_soma']);    
    $txtNaoValorPrint = $RFP['adm_nao_valor_print'];
    $txtDescontoPorRange = $RFP['adm_desconto_range'];
    $txtCliOrdem = $RFP['adm_cli_ordem'];
    $txtAtvBol = $RFP['adm_atv_bol'];
    $txtBaixaLayoutDesl = $RFP['adm_baixa_layout'];    
    $linkFormaPagamento = $RFP['link_forma_pagamento'];
    $txtContratoAgrp = $RFP['portal_contrato'];   
    $txtLinkMaxParc = $RFP['link_max_parcelas'];   
    $txtPagamentoCredencial = $RFP['clb_pagamento_credencial'];
    $txtCredencialDep = $RFP['clb_limite_credencial'];
    $txtCredencialDias = $RFP['clb_dias_credencial'];    
    $txtAddWhatsapp = $RFP['add_whatsapp'];
    $txtDescDecimal = $RFP['desconto_decimal'];
}

$idRefreshWa = "";
$cur = odbc_exec($con, "select id, id_externo from sf_fornecedores_despesas_veiculo 
inner join sf_vendas_planos on id_veiculo = id
inner join sf_fornecedores_despesas on favorecido = sf_fornecedores_despesas.id_fornecedores_despesas
where status = 'Aprovado' and ((id_externo is null and planos_status in ('Ativo') and fornecedores_status not in ('AtivoEmAberto')) or (id_externo is not null and fornecedores_status in ('AtivoEmAberto')) or (id_externo is not null and planos_status not in ('Ativo')
and id_veiculo not in (select x.id_veiculo from sf_vendas_planos x where x.id_veiculo is not null and x.planos_status in ('Ativo'))
and datediff(day,dbo.FU_PLANO_FIM(id_plano), getdate()) > (select ACA_TOL_ASSISTENCIA from sf_configuracao)))
and id in (select id_veiculo from sf_vistorias where data_aprov is not null)
union select id, id_externo from sf_fornecedores_despesas_veiculo where status = 'Reprovado' and id_externo is not null") or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $idRefreshWa .= $RFP["id"] . "|" . $RFP["id_externo"] . ",";        
}

$idRefreshRa = "";
$cur = odbc_exec($con, "select id, id_externo_rast, status_ext_veic from sf_fornecedores_despesas_veiculo 
inner join sf_vendas_planos on id_veiculo = id
inner join sf_fornecedores_despesas on favorecido = sf_fornecedores_despesas.id_fornecedores_despesas
where status = 'Aprovado' and (
(id_externo_rast is null and planos_status in ('Ativo') and fornecedores_status not in ('AtivoEmAberto')) or 
(id_externo_rast is not null and status_ext_veic = 'N' and planos_status in ('Ativo') and fornecedores_status not in ('AtivoEmAberto')) or 
(id_externo_rast is not null and status_ext_veic = 'S' and fornecedores_status in ('AtivoEmAberto')) or 
(id_externo_rast is not null and status_ext_veic = 'S' and planos_status not in ('Ativo') and id_veiculo not in (select x.id_veiculo from sf_vendas_planos x where x.id_veiculo is not null and x.planos_status in ('Ativo')) 
and datediff(day,dbo.FU_PLANO_FIM(id_plano), getdate()) > (select ACA_TOL_ASSISTENCIA from sf_configuracao)))
and id in (select id_veiculo from sf_vistorias where data_aprov is not null)
and id_rastreador is not null and id_operadora is not null and len(imei) > 0 and len(chip) > 0
union select id, id_externo_rast, status_ext_veic from sf_fornecedores_despesas_veiculo where status = 'Reprovado' and id_externo_rast is not null and status_ext_veic = 'S'") or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $idRefreshRa .= $RFP["id"] . "|" . $RFP["id_externo_rast"] . "|" . $RFP["status_ext_veic"] . ",";        
}

$cur = odbc_exec($con, "select * from sf_configuracao_campos_livres");
while ($RFP = odbc_fetch_array($cur)) {
    $row = array();
    $row["id_campo"] = utf8_encode($RFP['id_campo']);
    $row["descricao_campo"] = utf8_encode($RFP['descricao_campo']);
    $row["busca_campo"] = utf8_encode($RFP['busca_campo']);
    $row["ativo_campo"] = utf8_encode($RFP['ativo_campo']);
    $camposLivres[] = $row;
}

$cur = odbc_exec($con, "select cc.id_contas_movimento id_cred,cc.grupo_conta grup_cred,
cd.id_contas_movimento id_deb,cd.grupo_conta grup_deb from sf_configuracao
left join sf_contas_movimento cc on cc.id_contas_movimento = fin_ccontas_movimento and cc.inativa = 0 
left join sf_contas_movimento cd on cd.id_contas_movimento = fin_dcontas_movimento and cd.inativa = 0") or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $txtGruposCred = $RFP["grup_cred"];
    $txtSubGruposCred = $RFP["id_cred"];
    $txtGruposDeb = $RFP["grup_deb"];
    $txtSubGruposDeb = $RFP["id_deb"];
}

$cur = odbc_exec($con, "select sf_carteiras_bancos from sf_configuracao 
inner join sf_bancos_carteiras on bol_cart_padrao = sf_carteiras_id");
while ($RFP = odbc_fetch_array($cur)) {
    $txtBolBancPadrao = $RFP['sf_carteiras_bancos']; 
}