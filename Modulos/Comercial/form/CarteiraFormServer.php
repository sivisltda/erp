<?php

if (isset($_POST['List'])) {
    include "../../../Connections/configini.php";
}

$disabled = 'disabled';
$id = '';
$descricao = '';
$grupo = 'null';
$mensagem = '';
$imageFrente = '';
$imageVerso = '';
$margin_top = '0';
$margin_left = '0';
$list_dep = '0';

$pasta = "./../../Pessoas/" . $contrato . "/Empresa/Cartao/";

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = 1;
}

if (isset($_POST['btnSave'])) {
    $anexo = "";
    if ($_FILES['arquivoFrente']['name']) {
        $anexo .= ", foto_capa='" . utf8_decode($_FILES['arquivoFrente']['name']) . "'";
    }
    if ($_FILES['arquivoVerso']['name']) {
        $anexo .= ", foto_verso='" . utf8_decode($_FILES['arquivoVerso']['name']) . "'";
    }
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_carteirinhas set " .
        "descricao = " . valoresTexto('txtDescricao') . "," .
        "margin_top = " . valoresTexto('txtMarginTop') . "," .
        "margin_left = " . valoresTexto('txtMarginLeft') . "," .
        "id_grupo = " . valoresSelect('txtGrupo') . "," .
        "list_dep = " . valoresSelect('txtListDep') . "," .
        "mensagem = " . valoresTexto('ckeditor') . " " . $anexo .
        "where id = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_carteirinhas(descricao, margin_top, margin_left, id_grupo, list_dep, mensagem, foto_capa, foto_verso) values (" .
        valoresTexto('txtDescricao') . "," . valoresTexto('txtMarginTop') . "," . valoresTexto('txtMarginLeft') . "," . 
        valoresSelect('txtGrupo') . "," .  valoresSelect('txtListDep') . "," . valoresTexto('ckeditor') . ", '" . utf8_decode($_FILES['arquivoFrente']['name']) . "', '" . utf8_decode($_FILES['arquivoVerso']['name']) . "')") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id from sf_carteirinhas order by id desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
    if (!is_dir($pasta . "/" . $_POST['txtId'] . "/")) {
        mkdir($pasta . "/" . $_POST['txtId'] . "/", 0777, true);
    }
    if ($_FILES['arquivoFrente']['name']) {
        $nome = utf8_decode($_FILES['arquivoFrente']['name']);
        $tmp = $_FILES['arquivoFrente']['tmp_name'];
        if ($tmp) {
            move_uploaded_file($tmp, $pasta . "/" . $_POST['txtId'] . "/" . $nome);
        }
    }
    if ($_FILES['arquivoVerso']['name']) {
        $nome = utf8_decode($_FILES['arquivoVerso']['name']);
        $tmp = $_FILES['arquivoVerso']['tmp_name'];
        if ($tmp) {
            move_uploaded_file($tmp, $pasta . "/" . $_POST['txtId'] . "/" . $nome);
        }
    }
}

if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        odbc_exec($con, "DELETE FROM sf_carteirinhas WHERE id = " . $_POST['txtId']) or die(odbc_errormsg());
        deleteDirectory($pasta . "/" . $_POST['txtId']);
        if (isset($_POST['List'])) {
            echo "YES";
        } else {
            echo "<script>alert('Registro excluido com sucesso'); parent.refreshCarteira();</script>";
        }
        exit();
    }
}

if (isset($_POST['DelImage'])) {
    if (is_numeric($_POST['txtId'])) {
        odbc_exec($con, "update sf_carteirinhas set " . $_POST['txtTipo'] . " = '' WHERE id = " . $_POST['txtId']) or die(odbc_errormsg());
        $arquivo = "." . $pasta . "/" . $_POST['txtId'] . "/" . $_POST['DelImage'];
        if (file_exists($arquivo)) {
            unlink($arquivo);
        }
    }
}

if (isset($_POST['btnNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_carteirinhas where id =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id']);
        $descricao = utf8_encode($RFP['descricao']);
        $grupo = utf8_encode($RFP['id_grupo']);
        $mensagem = utf8_encode($RFP['mensagem']);
        $imageFrente = utf8_encode($RFP['foto_capa']);
        $imageVerso = utf8_encode($RFP['foto_verso']);
        $margin_top = utf8_encode($RFP['margin_top']);
        $margin_left = utf8_encode($RFP['margin_left']);
        $list_dep = utf8_encode($RFP['list_dep']);
    }
} else {
    $disabled = '';
}

if (isset($_POST['btnEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}