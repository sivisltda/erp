<?php

require_once('../../../Connections/configini.php');
if (isset($_POST['btnFotoContratacao'])) {
    $allowedExts = array("jpg", "JPG", "png", "PNG");
    $extension = explode(".", $_FILES["fotoContratacao"]["name"]);
    $extension = $extension[count($extension) - 1];
    if ((($_FILES["fotoContratacao"]["type"] == "image/jpeg") || ($_FILES["fotoContratacao"]["type"] == "image/jpg") || ($_FILES["fotoContratacao"]["type"] == "image/png")) && 
    ($_FILES["fotoContratacao"]["size"] < 10000000) && in_array($extension, $allowedExts)) {
        if ($_FILES["fotoContratacao"]["error"] == 0) {
            if (!is_dir("./../../../Pessoas/" . $contrato . "/Empresa/Contratacao/" . (isset($_POST["type"]) ? $_POST["type"] . "/" : ""))) {
                mkdir("./../../../Pessoas/" . $contrato . "/Empresa/Contratacao/" . (isset($_POST["type"]) ? $_POST["type"] . "/" : ""), 0777, true);
            }
            $nomeDoc = "logo." . $extension;
            move_uploaded_file($_FILES["fotoContratacao"]["tmp_name"], "./../../../Pessoas/" . $contrato . "/Empresa/Contratacao/"  . (isset($_POST["type"]) ? $_POST["type"] . "/" : "") . $nomeDoc);
            echo "YES";
        }
    } else {
        echo 'Verifique se o arquivo é jpg/png ou possue tamanho menor do que 10 MB';
    }
}

if (isset($_POST['btnDelFile'])) {
    if (file_exists("./../../../Pessoas/" . $contrato . "/Empresa/Contratacao/" . (isset($_POST["type"]) ? $_POST["type"] . "/" : "") . $_POST['txtFileName'])) {
        unlink("./../../../Pessoas/" . $contrato . "/Empresa/Contratacao/" . (isset($_POST["type"]) ? $_POST["type"] . "/" : "") . $_POST['txtFileName']);
        echo "YES";
    }
}