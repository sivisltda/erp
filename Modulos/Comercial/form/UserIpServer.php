<?php

include "../../../Connections/configini.php";

if (isset($_POST["btnSalvar"])) {
    $query = "insert into sf_login_white(ip, descricao, id_usuario, data_cad) values (" . valoresTexto("txtIp") . "," . 
    valoresTexto("txtIpDescricao") . "," . valoresSelect("txtIpUser") . ", getdate());";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";    
}

if (isset($_POST["btnExcluir"])) {
    $query = "delete from sf_login_white where ip = " . valoresTexto("txtIp") . ";";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES"; 
}

if (isset($_GET["list"])) {
    $total = 0;
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );      
    $cur = odbc_exec($con, "select l.*, login_user from sf_login_white l
    left join sf_usuarios u on l.id_usuario = u.id_usuario order by data_cad desc") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        if (file_exists("../../../Pessoas/" . $contrato . "/Ips/". str_replace(".", "_",$RFP['ip']) .".jpg")) {
            $row[] = "<center><a href=\"" . "../../../Pessoas/" . $contrato . "/Ips/". str_replace(".", "_",$RFP['ip']) .".jpg" . "\" target=\"_blank\">" . utf8_encode($RFP['ip']) . "</a></center>";
        } else {
            $row[] = "<center>" . utf8_encode($RFP['ip']) . "</center>";    
        }
        $row[] = "<span title=\"" . escreverDataHora($RFP['data_cad']) . "\">" . utf8_encode($RFP['descricao']) . "</span>";
        $row[] = utf8_encode($RFP['login_user']);
        $row[] = "<center><input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\" onclick=\"RemoverIp('" . utf8_encode($RFP['ip']) . "')\" /></center>";
        $output['aaData'][] = $row;
        $total++;
    }    
    $output['iTotalRecords'] = $total;
    $output['iTotalDisplayRecords'] = $total; 
    echo json_encode($output);
}

odbc_close($con);