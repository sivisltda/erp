<?php

require_once(__DIR__ . '/../../../Connections/configini.php');

$disabled = 'disabled';

if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_disparo_auto_item set " .
        "tp_disparo_item = " . valoresSelectTexto('txtTipo') . "," .
        "id_disparo_auto_item = " . valoresSelect('txtRegra') . "," .
        "id_user_resp_item = " . valoresSelect('txtResponsavel') . "," .
        "loja_disparo_auto = " . valoresSelect('txtLoja') . "," .
        "dias_tolerancia_item = " . valoresNumericos('txtDias') . "," .
        "bloqueio_disparo_item = " . valoresCheck('txtBloqueio') . "," .
        "id_email_item = " . valoresSelect('txtModelo') . "," .
        "assunto_item = " . valoresTexto('txtAssunto') . "," .
        "texto_item = " . valoresTexto('txtDescricao') . " " .
        "where id_disparo_item = " . $_POST['txtId']) or die(odbc_errormsg());
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data) 
        values ('sf_disparo_auto_item', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'A', 'ALTERACAO DISPARO', GETDATE())");        
    } else {
        odbc_exec($con, "insert into sf_disparo_auto_item(tp_disparo_item, id_disparo_auto_item, id_user_resp_item,
        loja_disparo_auto, dias_tolerancia_item, bloqueio_disparo_item, id_email_item, assunto_item, texto_item) values (" .                
        valoresSelectTexto('txtTipo') . "," .
        valoresSelect('txtRegra') . "," .
        valoresSelect('txtResponsavel') . "," .
        valoresSelect('txtLoja') . "," .
        valoresNumericos('txtDias') . "," .
        valoresCheck('txtBloqueio') . "," .
        valoresSelect('txtModelo') . "," .
        valoresTexto('txtAssunto') . "," .
        valoresTexto('txtDescricao') . ");") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_disparo_item from sf_disparo_auto_item order by id_disparo_item desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data) 
        values ('sf_disparo_auto_item', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'I', 'INCLUSAO DISPARO', GETDATE())");        
    }
}

if (isset($_POST['excluirDisparo'])) {
    odbc_exec($con, "DELETE FROM sf_disparo_auto_item WHERE id_disparo_item = " . $_POST['excluirDisparo']) or die(odbc_errormsg());
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data) 
    values ('sf_disparo_auto_item', " . $_POST['excluirDisparo'] . ", '" . $_SESSION["login_usuario"] . "', 'R', 'EXCLUSAO DISPARO', GETDATE())") or die(odbc_errormsg());
    echo "YES"; exit;
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_disparo_auto_item where id_disparo_item = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id =  utf8_encode($RFP['id_disparo_item']);
        $tipo =  utf8_encode($RFP['tp_disparo_item']);
        $regra =  utf8_encode($RFP['id_disparo_auto_item']);
        $responsavel =  utf8_encode($RFP['id_user_resp_item']);
        $loja =  utf8_encode($RFP['loja_disparo_auto']);
        $dias =  utf8_encode($RFP['dias_tolerancia_item']);
        $bloqueio =  utf8_encode($RFP['bloqueio_disparo_item']);
        $modelo =  utf8_encode($RFP['id_email_item']);
        $assunto =  utf8_encode($RFP['assunto_item']);
        $descricao =  utf8_encode($RFP['texto_item']);
    }
} else {
    $disabled = '';
    $id = '';
    $tipo = 'E';
    $regra = '';
    $responsavel = '';
    $loja = '';
    $dias = '1';
    $bloqueio = '';
    $modelo = '';
    $assunto = '';
    $descricao = '';
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}

$mdl_aca_ = returnPart($_SESSION["modulos"], 6);
$mdl_wha_ = returnPart($_SESSION["modulos"],16);
$tpfiltroDisp = 'E';
if ($mdl_aca_ === "1") {
    $tpfiltroDisp = 'A';
}