<?php

include "../../../Connections/configini.php";

if (isset($_POST["btnSalvar"])) {
    $query = "insert into sf_login_black(ip, descricao) values (" . valoresTexto("txtIp") . "," . valoresTexto("txtIpDescricao") . ");";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";    
}

if (isset($_POST["btnExcluir"])) {
    $query = "delete from sf_login_black where ip = " . valoresTexto("txtIp") . ";";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES"; 
}

if (isset($_GET["list"])) {
    $total = 0;
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );      
    $cur = odbc_exec($con, "select ip, descricao from sf_login_black order by descricao") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row[] = "<center>" . utf8_encode($RFP['ip']) . "</center>";
        $row[] = utf8_encode($RFP['descricao']);
        $row[] = "<center><input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\" onclick=\"RemoverIpBlack('" . utf8_encode($RFP['ip']) . "')\" /></center>";
        $output['aaData'][] = $row;
        $total++;
    }    
    $output['iTotalRecords'] = $total;
    $output['iTotalDisplayRecords'] = $total; 
    echo json_encode($output);
}

odbc_close($con);