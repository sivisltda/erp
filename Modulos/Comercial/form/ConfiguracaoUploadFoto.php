<?php

require_once('../../../Connections/configini.php');
if (isset($_POST['btnSendPhoto'])) {
    if (isset($_POST["txtIdFilial"]) && $_POST["txtIdFilial"] != null) {
        $allowedExts = array("jpg", "JPG", "png", "PNG");
        $extension = explode(".", $_FILES["fotoFilial"]["name"]);
        $extension = $extension[count($extension) - 1];
        if ((($_FILES["fotoFilial"]["type"] == "image/jpeg") || ($_FILES["fotoFilial"]["type"] == "image/jpg") || ($_FILES["fotoFilial"]["type"] == "image/png")) && 
        ($_FILES["fotoFilial"]["size"] < 10000000) && in_array($extension, $allowedExts)) {
            if ($_FILES["fotoFilial"]["error"] == 0) {
                if (!is_dir("./../../../Pessoas/" . $contrato . "/Empresa/" . (isset($_POST["type"]) ? $_POST["type"] . "/" : ""))) {
                    mkdir("./../../../Pessoas/" . $contrato . "/Empresa/" . (isset($_POST["type"]) ? $_POST["type"] . "/" : ""), 0777, true);
                }                
                $nomeDoc = (isset($_POST["type"]) ? "" : "logo_") . $_POST['txtIdFilial'] . "." . $extension;                
                move_uploaded_file($_FILES["fotoFilial"]["tmp_name"], "./../../../Pessoas/" . $contrato . "/Empresa/"  . (isset($_POST["type"]) ? $_POST["type"] . "/" : "") . $nomeDoc);
                echo "YES";
            }
        } else {
            echo 'Verifique se o arquivo é jpg/png ou possue tamanho menor do que 10 MB';
        }
    }
}

if (isset($_POST['btnDelFile'])) {
    if (file_exists("./../../../Pessoas/" . $contrato . "/Empresa/" . (isset($_POST["type"]) ? $_POST["type"] . "/" : "") . $_POST['txtFileName'])) {
        unlink("./../../../Pessoas/" . $contrato . "/Empresa/" . (isset($_POST["type"]) ? $_POST["type"] . "/" : "") . $_POST['txtFileName']);
        echo "YES";
    }
}