<?php

require_once(__DIR__ . '/../../../Connections/configini.php');

$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    $adm_agen_umamar = 0;
    $adm_agen_um_tipo = 0;
    $adm_agen_hmarc = 0;
    $adm_agen_semanal = 0;
    $adm_agen_ativo = 0;    
    $adm_agen_excecao = 0; 
    
    $sql = "select * from sf_configuracao";
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $adm_agen_umamar = utf8_encode($RFP['adm_agen_umamar']);
        $adm_agen_um_tipo = utf8_encode($RFP['adm_agen_um_tipo']);
        $adm_agen_hmarc = utf8_encode($RFP['adm_agen_hmarc']);
        $adm_agen_semanal = utf8_encode($RFP['adm_agen_semanal']);
        $adm_agen_ativo = utf8_encode($RFP['adm_agen_ativo']);
    }
    
    $sql = "select excecao from sf_agenda where id_agenda = " . valoresSelect("txtAgenda");
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $adm_agen_excecao = utf8_encode($RFP['excecao']);    
    }    
    
    if ($adm_agen_umamar == 1 && $ckb_aca_agenda_regras_ == 0 && $adm_agen_excecao == 0) {
        $query = "set dateformat dmy;select count(id_agendamento) total from sf_agendamento 
        where inativo = 0 and id_fornecedores = " . valoresNumericos("txtAluno") .
        ($adm_agen_um_tipo == 0 ? " and cast(data_inicio as date) = " . valoresData("txtDataInicio") : 
        " and month(data_inicio) = month(" . valoresData("txtDataInicio") . ") and year(data_inicio) = year(" . valoresData("txtDataInicio") . ")");
        if (is_numeric($_POST['txtId'])) {
            $query .= " and id_agendamento not in (" . $_POST['txtId'] . ")";
        }
        $cur = odbc_exec($con, $query) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            if ($RFP['total'] > 0) {
                echo "Não é Permitido mais de um agendamento " . ($adm_agen_um_tipo == 0 ? "diário" : "mensal") . " por cliente!";
                exit();
            }
        }
    }
    if ($adm_agen_hmarc == 1 && $ckb_aca_agenda_regras_ == 0 && $adm_agen_excecao == 0) {
        $query = "set dateformat dmy;select count(id_agendamento) total from sf_agendamento 
        where inativo = 0 and id_agenda = " . valoresSelect("txtAgenda") .
                " and ((" . valoresDataHora("txtDataInicio", "txtHoraInicio") . " between data_inicio and data_fim) or
        (" . valoresDataHora("txtDataFim", "txtHoraFim") . " between data_inicio and data_fim))";
        if (is_numeric($_POST['txtId'])) {
            $query .= " and id_agendamento not in (" . $_POST['txtId'] . ")";
        }
        $cur = odbc_exec($con, $query) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            if ($RFP['total'] > 0) {
                echo "Não é permitido agendamento com horário marcado!";
                exit();
            }
        }
    }
    if ($adm_agen_semanal > 0 && $ckb_aca_agenda_regras_ == 0 && $adm_agen_excecao == 0) {
        $query = "set dateformat dmy;select count(id_agendamento) total from sf_agendamento 
        where inativo = 0 and id_fornecedores = " . valoresNumericos("txtAluno") .
                " and DATEPART(week, data_inicio) = DATEPART(week, " . valoresData("txtDataInicio") . ")";
        if (is_numeric($_POST['txtId'])) {
            $query .= " and id_agendamento not in (" . $_POST['txtId'] . ")";
        }
        $cur = odbc_exec($con, $query) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            if ($RFP['total'] >= $adm_agen_semanal) {
                echo "Não é permitido cadastro semanal maior que " . $adm_agen_semanal . " por cliente!";
                exit();
            }
        }
    }
    if ($adm_agen_ativo == 1 && $ckb_aca_agenda_regras_ == 0 && $adm_agen_excecao == 0) {
        $query = "set dateformat dmy;
        select dbo.FU_STATUS_CLI(id_fornecedores_despesas) status, dbo.FU_STATUS_CLI(id_titular) status_tituar, tipo, isnull(responsavel, 0) responsavel
        from sf_fornecedores_despesas left join sf_fornecedores_despesas_dependentes on id_fornecedores_despesas = id_dependente
        where id_fornecedores_despesas = " . valoresNumericos("txtAluno");
        $cur = odbc_exec($con, $query) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            if ($RFP['status'] == "AtivoEmAberto" || (substr($RFP['status'], 0, 5) != "Ativo" && $RFP['tipo'] == "C")) {
                if ($RFP['responsavel'] == 0 || $RFP['status_tituar'] == "AtivoEmAberto" || (substr($RFP['status_tituar'], 0, 5) != "Ativo")) {
                    echo "Não foi possível efetuar o agendamento. Favor entrar em contato com o SAC!";
                    exit();                
                }
            }            
        }
    }
    if (is_numeric($_POST['txtId'])) {
        $query = "SET DATEFORMAT DMY;" .
                "UPDATE sf_agendamento SET " .
                "id_agenda = " . valoresSelect("txtAgenda") .
                ",id_fornecedores = " . valoresNumericos("txtAluno") .
                ",data_inicio = " . valoresDataHora("txtDataInicio", "txtHoraInicio") .
                ",data_fim = " . valoresDataHora("txtDataFim", "txtHoraFim") .
                ",assunto = " . valoresTexto("txtAssunto") .
                ",obs = " . valoresTexto("txtObs") .
                ",data_agendamento = getdate()" .
                ",agendado_por = " . valoresTexto2($_SESSION["login_usuario"]) .
                ",inativo = 0, concluido = " . valoresCheck("txtConcluido") .
                ",id_item_venda = " . valoresSelect("txtVendaItem") .                
                " WHERE id_agendamento = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        values ('sf_agendamento', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'A',
        'ALTERAR - AGENDAMENTO de " . str_replace("'", "", valoresDataHora("txtDataInicio", "txtHoraInicio")) . " - " .
        str_replace("'", "", valoresDataHora("txtDataFim", "txtHoraFim")) . "', GETDATE()," . valoresNumericos("txtAluno") . ")");
    } else {
        $query = "SET DATEFORMAT DMY;" .
        "INSERT INTO sf_agendamento(id_agenda,id_fornecedores,data_inicio,data_fim,assunto,obs,
        data_agendamento,agendado_por,inativo,concluido,id_item_venda) VALUES (" . valoresSelect("txtAgenda") .
                "," . valoresNumericos("txtAluno") .
                "," . valoresDataHora("txtDataInicio", "txtHoraInicio") .
                "," . valoresDataHora("txtDataFim", "txtHoraFim") .
                "," . valoresTexto("txtAssunto") .
                "," . valoresTexto("txtObs") .
                ",getdate()" .
                "," . valoresTexto2($_SESSION["login_usuario"]) .
                ",0," . valoresCheck("txtConcluido") . 
                "," . valoresSelect("txtVendaItem") . ")";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_agendamento from sf_agendamento order by id_agendamento desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
        
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        values ('sf_agendamento', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'I',
        'INCLUIR - AGENDAMENTO de " . str_replace("'", "", valoresDataHora("txtDataInicio", "txtHoraInicio")) . " - " .
        str_replace("'", "", valoresDataHora("txtDataFim", "txtHoraFim")) . "', GETDATE()," . valoresNumericos("txtAluno") . ")");
    }
    echo $_POST['txtId'];
    exit();
}

if (isset($_POST['bntExcluir'])) {
    if (is_numeric($_POST['txtId'])) {
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        values ('sf_agendamento', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'E', 'EXCLUIR - AGENDAMENTO', GETDATE(), 
        (select id_fornecedores FROM sf_agendamento where id_agendamento = " . $_POST['txtId'] . "))");
        $query = "DELETE FROM sf_agendamento WHERE id_agendamento = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        echo "YES";
    }
}

if (isset($_POST['bntCancelar'])) {
    if (is_numeric($_POST['txtId'])) {
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        values ('sf_agendamento', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'E', 'CANCELAMENTO - AGENDAMENTO', GETDATE(), 
        (select id_fornecedores FROM sf_agendamento where id_agendamento = " . $_POST['txtId'] . "))");
        $query = "SET DATEFORMAT DMY;" .
                "UPDATE sf_agendamento SET inativo = 1" .
                "WHERE id_agendamento = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        echo "YES";
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled ';
        $PegaURL = $_POST['txtId'];
    }
    $sql = "SELECT sf_agendamento.*,sf_fornecedores_despesas.*,(select max(conteudo_contato) celular from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) celular, 
    (select max(conteudo_contato) email from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) email, 
    dbo.FU_STATUS_CLI(sf_fornecedores_despesas.id_fornecedores_despesas) statusAluno, v.status,
    (select max(id_servico) from sf_agenda a where a.id_agenda = sf_agendamento.id_agenda) id_servico
    FROM sf_agendamento inner join sf_fornecedores_despesas on sf_fornecedores_despesas.id_fornecedores_despesas = sf_agendamento.id_fornecedores 
    left join sf_vendas_itens vi on sf_agendamento.id_item_venda = vi.id_item_venda
    left join sf_vendas v on v.id_venda = vi.id_venda
    where id_agendamento = " . $PegaURL;
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_agendamento']);
        $id_agenda = utf8_encode($RFP['id_agenda']);
        if ($RFP['id_fornecedores_despesas'] > 0) {
            $pessoa = utf8_encode($RFP['id_fornecedores_despesas']);
            $pessoaNome = utf8_encode($RFP['razao_social']);
            $pessoaCel = utf8_encode($RFP['celular']);
            $pessoaEmail = utf8_encode($RFP['email']);
            $statusAluno = utf8_encode($RFP['statusAluno']);
        }
        $dt_inicio = escreverData($RFP['data_inicio']);
        $hora_inicio = escreverData($RFP['data_inicio'], 'H:i');
        $dt_fim = escreverData($RFP['data_fim']);
        $hora_fim = escreverData($RFP['data_fim'], 'H:i');
        $assunto = utf8_encode($RFP['assunto']);
        $obs = utf8_encode($RFP['obs']);
        $concluido = utf8_encode($RFP['concluido']);
        $id_servico = utf8_encode($RFP['id_servico']);
        $id_item_venda = ($RFP['status'] == "Aprovado" ? $RFP['id_item_venda'] : "");
    }
} else {
    $disabled = "";
    $id = "";
    $id_agenda = "";
    $pessoa = "";
    $pessoaNome = "";
    $pessoaCel = "";
    $pessoaEmail = "";
    $statusAluno = "";
    $dt_inicio = getData("T");
    $hora_inicio = date("H:i");
    $dt_fim = getData("T");
    $hora_fim = date("H:i", strtotime('+1 hour'));
    $assunto = "";
    $obs = "";
    $sms = "0";
    $concluido = 0;
    $id_servico = "";
    $id_item_venda = "";

    if (is_numeric($_GET['al'])) {
        $sql = "SELECT *,(select max(conteudo_contato) celular from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) celular, 
        (select max(conteudo_contato) email from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) email, 
        dbo.FU_STATUS_CLI(sf_fornecedores_despesas.id_fornecedores_despesas) statusAluno FROM sf_fornecedores_despesas where id_fornecedores_despesas = " . $_GET['al'];
        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $pessoa = utf8_encode($RFP['id_fornecedores_despesas']);
            $pessoaNome = utf8_encode($RFP['razao_social']);
            $pessoaCel = utf8_encode($RFP['celular']);
            $pessoaEmail = utf8_encode($RFP['email']);
            $statusAluno = utf8_encode($RFP['statusAluno']);
        }
    }
}

$cur = odbc_exec($con, "select SMS_USUARIO,SMS_SENHA from sf_configuracao") or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    if (strlen($RFP['SMS_USUARIO']) > 0 && strlen($RFP['SMS_SENHA']) > 0) {
        $sms = "1";
    }
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}

if (isset($_GET['txtPagamentos'])) {
    $local = array();    
    $sQuery = "select id_item_venda into #tempAgendamento from sf_agendamento where id_fornecedores = " . $_GET['txtPessoa'] . " and id_item_venda is not null and id_agendamento not in (" . valoresNumericos2($_GET['txtAgendamento']) . ")";
    odbc_exec($con, $sQuery) or die(odbc_errormsg());    
    $query = "select id_item_venda, data_venda, descricao, vendedor_comissao, quantidade 
    from sf_vendas_itens vi inner join sf_vendas v on v.id_venda = vi.id_venda inner join sf_produtos p on p.conta_produto = vi.produto
    where p.conta_produto in (select id_servico from sf_agenda where id_agenda = " . valoresNumericos2($_GET['txtAgenda']) . ") and v.status = 'Aprovado' and cliente_venda = " . $_GET['txtPessoa'] . " 
    and id_item_venda not in (select id_item_venda from #tempAgendamento) order by data_venda desc";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) { 
        $local[] = array('id' => $RFP['id_item_venda'], 'descricao' => escreverDataHora($RFP['data_venda']) . " Qtd. " . escreverNumero($RFP['quantidade'], 0, 0) . " [" . utf8_encode($RFP['descricao']) . "]");
    }
    echo(json_encode($local));
}