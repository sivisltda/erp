<?php
include "./form/FormAgenda.php";
if ($_GET["dt"] != "") {
    $dt_inicio = escreverData($_GET["dt"]);
    $dt_fim = escreverData($_GET["dt"]);
}
?>
<link rel="icon" type="image/ico" href="../../../favicon.ico"/>
<link href="../../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../../css/main.css" rel="stylesheet">
<link href="../../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' src='../../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<style>
    body { font-family: sans-serif; }
    .selected { background: #acbad4 !important;}
</style>
<body>
    <form action="FormAgenda.php" name="frmEnviaDados" id="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Agendar</div>
            <div class="frmicon" onClick="parent.FecharBoxF()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="tabbable frmtabs">
            <ul class="nav nav-tabs" style="margin:0">
                <li class="active"><a href="#tab1" data-toggle="tab">Dados principais</a></li>
                <?php if (is_numeric($id) && is_numeric($id_servico)) { ?>
                <li><a href="#tab2" data-toggle="tab">Links</a></li>
                <?php } ?>
            </ul>
            <div class="tab-content frmcont" style="height:275px">
                <div class="tab-pane active" id="tab1" style="overflow:hidden; height:275px;">
                    <input id="txtId" name="txtId" value="<?php echo $id; ?>" type="hidden"/>
                    <input id="txtSMSUser" name="txtSMSUser" value="<?php echo $sms; ?>" type="hidden"/>
                    <div style="width:71%; float:left; margin-left:1%">
                        <span>Agenda:</span>
                        <select id="txtAgenda" name="txtAgenda" <?php echo $disabled; ?>>
                            <option value="null">Selecione</option>
                            <?php $cur = odbc_exec($con, "select id_agenda,descricao_agenda,pg_obrig from sf_agenda where inativo = 0
                                and ((user_resp is null or user_resp = " . $_SESSION["id_usuario"] . ") or (user_permissao is null or user_permissao in (select isnull(master, 0) from sf_usuarios where id_usuario = " . $_SESSION["id_usuario"] . ")))
                                order by descricao_agenda") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP['id_agenda'] ?>"<?php
                                if (!(strcmp($RFP['id_agenda'], $id_agenda))) {
                                    echo "SELECTED";
                                } ?> pg="<?php echo $RFP['pg_obrig']; ?>"><?php echo utf8_encode($RFP['descricao_agenda']);?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div style="width:24%; float:left; margin-left:4%;margin-top: 15px;">
                        <span>Concluído:</span>
                        <input id="txtConcluido" name="txtConcluido" type="checkbox" value="1" <?php echo ($concluido == 1 ? "checked" : ""); ?> style="width:100%" <?php echo $disabled; ?>/>
                    </div>                
                    <div style="clear:both;"></div>
                    <div style="width:71%; float:left; margin-left:1%">
                        <span>Cliente / Prospect:</span>
                        <input type="hidden" id="txtAluno" name="txtAluno" value="<?php echo $pessoa;?>">
                        <input type="hidden" id="txtAlunoCel" value="<?php echo $pessoaCel;?>">
                        <input type="hidden" id="txtAlunoEmail" value="<?php echo $pessoaEmail;?>">
                        <input type="text" id="txtAlunoNome" style="width:100%" value="<?php echo $pessoaNome;?>" <?php echo $disabled; ?>/>
                    </div>
                    <div style="width:26%; float:left; margin-left:1%">
                        <div id="statusCli">
                            <div class="button label-<?php echo $statusAluno; ?>" style="width:102px; margin-top:14px;">
                                <div class="name"><center><?php echo strpos($statusAluno, 'Cred') ? 'ATIVO' : ($statusAluno == 'Ferias' ? 'CONGELADO' : strtoupper($statusAluno)); ?></center></div>
                            </div>
                        </div>
                    </div>                
                    <div style="clear:both;"></div>
                    <div style="width:41%; float:left; margin-left:1%">
                        <span><?php echo $tipo; ?>Assunto:</span>
                        <input id="txtAssunto" name="txtAssunto" type="text" value="<?php echo $assunto; ?>" style="width:100%" <?php echo $disabled; ?>/>
                    </div>            
                    <div style="width:17%; float:left; margin-left:1%">
                        <span>Data Início:</span>
                        <input id="txtDataInicio" name="txtDataInicio" class="datepicker inputCenter" type="text" value="<?php echo $dt_inicio; ?>" <?php echo $disabled; ?>>
                    </div>
                    <div style="width:10%; float:left; margin-left:1%">
                        <span>Horário:</span>
                        <input id="txtHoraInicio" name="txtHoraInicio" type="text" class="input-medium" value="<?php echo $hora_inicio; ?>" <?php echo $disabled; ?>>
                    </div>
                    <div style="width:17%; float:left; margin-left:1%">
                        <span>Data Fim:</span>
                        <input id="txtDataFim" name="txtDataFim" class="datepicker inputCenter" type="text" value="<?php echo $dt_fim; ?>" <?php echo $disabled; ?>>
                    </div>
                    <div style="width:10%; float:left; margin-left:1%">
                        <span>Horário:</span>
                        <input id="txtHoraFim" name="txtHoraFim" type="text" class="input-medium" value="<?php echo $hora_fim; ?>" <?php echo $disabled; ?>>
                    </div>                
                    <div style="clear:both; height:5px"></div>            
                    <div style="width:99%; float:left; margin-left:1%">
                        <span>Pagamento de Serviço:</span>
                        <select id="txtVendaItem" name="txtVendaItem" venda="<?php echo $id_item_venda; ?>" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>
                            <option value="null">Selecione</option>
                        </select>
                    </div>            
                    <div style="clear:both; height:5px"></div>
                    <div class="body" style="margin-left:1%; padding:0">
                        <span>Observação:</span>
                        <div class="content" style="min-height:70px; margin-top:0">
                            <textarea id="txtObs" name="txtObs" style="width:100%; height:70px" <?php echo $disabled; ?>><?php echo $obs; ?></textarea>
                        </div>
                    </div>            
                    <div style="clear:both;height: 5px;"></div>
                </div>
                <div class="tab-pane" id="tab2" style="overflow:hidden; height:275px">
                    <div class="body" style="margin-left: 0px; padding: 0 0px">                                            
                        <button style="float: left;" type="button" id="btnNovoLink" title="Gerar Link" class="button button-green btn-primary"><span class="ico-barcode icon-white"></span> Gerar Link</button>
                        <span style="float: right;">                                           
                            De <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin_link" value="<?php echo getData("T"); ?>" placeholder="Data inicial">
                            até <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end_link" value="<?php echo getData("E"); ?>" placeholder="Data Final">
                            <button id="btnfindLink" class="button button-turquoise btn-primary buttonX" type="button" title="Buscar"><span class="ico-search icon-white"></span></button>
                        </span>
                        <div class="content">
                            <table class="table" style="width: 100%;" cellpadding="0" cellspacing="0" id="tblLinkHistorico">
                                <thead>
                                    <th style="width: 15%">Dt. Venc.</th>
                                    <th style="width: 15%">Valor</th>
                                    <th style="width: 15%">Nº Max.</th>
                                    <th style="width: 15%">Status.</th>
                                    <th width="5%">Ação</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>                            
                        <div style="clear:both; height:5px"></div>
                    </div>
                </div>
            </div>                
        </div>            
        <div class="frmfoot">
            <input name="txtSms" id="txtSms" type="hidden"/>
            <div class="frmbtn">                
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="button" name="bntSave" id="bntSave" onClick="salvar()"><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBoxF()" name="bntCancelar" id="bntCancelar"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntAlterar" ><span class="ico-reply"></span> Cancelar</button>
                    <?php
                    }
                } else { ?>                                            
                    <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="Print();" title="Imprimir" style="margin-top: 15px;height: 28px;float: left;"><span class="ico-print icon-white"></span></button>
                    <button id="btnEmail" class="button button-turquoise btn-primary" type="button" onclick="enviar_email();" title="E-mail" style="margin-top: 15px;height: 28px;float: left;"><span class="ico-envelope-3 icon-white"></span></button>                        
                    <button id="btnSms" class="button button-blue btn-primary buttonX" type="button" onclick="enviarSMS();" title="SMS" style="margin-top: 15px;height: 28px;float: left;"><span class="ico-comment icon-white"></span></button>                        
                    <button id="btnContrato" class="button button-yellow btn-primary buttonX" type="button" onclick="abrirContrato();" title="Contrato" style="margin-top: 15px;height: 28px;float: left;"><span class="ico-file-4 icon-white"></span></button>                                                                                            
                    <button class="btn green" type="submit" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntEdit" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>                    
                    <button class="btn red" type="button" name="bntDelete" id="bntDelete" value="Excluir" onClick="excluir();" ><span class=" ico-remove"> </span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>        
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/multiselect/jquery.multi-select.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery.filter_input.js'></script>
    <script type='text/javascript' src='../../js/plugins/ckeditor/ckeditor.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script> 
    <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>    
    <script type="text/javascript" src="../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>    
    <script type="text/javascript" src="../../js/util.js"></script>        
    <script type="text/javascript">
            
        var qtdSucesso = 0;
        var qtdFalha = 0;
        
        if($('#txtAgenda').val() === "null"){
            $('#txtAgenda').val(parent.$('#txtAgenda').val());
        }
                
        $("#txtDataInicio, #txtDataFim").mask(lang["dateMask"]);
        $("#txtHoraInicio, #txtHoraFim").mask("99:99");        
        $("#txtAlunoNome").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "../../Modulos/CRM/ajax.php",
                    dataType: "json",
                    data: {q: request.term, t: "C','P"},
                    success: function (data) {
                        response(data);
                    }
                });
            }, minLength: 3,
            select: function (event, ui) {
                $("#txtAluno").val(ui.item.id);
                $("#txtAlunoNome").val(ui.item.value);
                $("#txtAlunoEmail").val(ui.item.email);
                $("#txtAlunoCel").val(ui.item.telefone);
                atualizaStatusCli(ui.item.id);                    
                atualizaPagamentosCli(ui.item.id);                    
            }
        });
        
        atualizaPagamentosCli($("#txtAluno").val());
        
        $('#txtAgenda').change(function () {
            atualizaPagamentosCli($("#txtAluno").val());
        });
                
        $('#txtHoraInicio').blur(function () {
            var time = moment($(this).val(),'HH:mm');
            time.add(60,'m');
            $("#txtHoraFim").val(time.format("HH:mm"));
        });

        $('#txtDataInicio').change(function () {
            if (moment($("#txtDataInicio").val(), "DD/MM/YYYY").diff(moment($("#txtDataFim").val(), "DD/MM/YYYY"), 'days') > 0) {
                $("#txtDataFim").val($("#txtDataInicio").val());
            }
        });

        $('#txtDataFim').change(function () {
            if (moment($("#txtDataInicio").val(), "DD/MM/YYYY").diff(moment($("#txtDataFim").val(), "DD/MM/YYYY"), 'days') > 0) {
                $("#txtDataInicio").val($("#txtDataFim").val());
            }
        });                     

        function salvar() {
            if ($('#txtDataInicio').val() !== "" && $('#txtAgenda').val() !== "" && $('#txtAgenda').val() !== "null" && $('#txtAssunto').val() !== "") {
                if (moment($("#txtDataInicio").val(), "DD/MM/YYYY").diff(moment($("#txtDataFim").val(), "DD/MM/YYYY"), 'days') <= 0) {
                    if (!($("#txtAgenda option:selected").attr("pg") === "1" && $("#txtVendaItem").val() === "null")) {
                        $.post("form/FormAgenda.php", {txtId: $('#txtId').val(), txtAgenda: $('#txtAgenda').val(),
                            txtDataInicio: $('#txtDataInicio').val(), txtHoraInicio: $('#txtHoraInicio').val(), txtVendaItem: $("#txtVendaItem").val(),
                            txtAluno: $('#txtAluno').val(), txtDataFim: $('#txtDataFim').val(), txtHoraFim: $('#txtHoraFim').val(),
                            txtAssunto: $('#txtAssunto').val(), txtObs: $('#txtObs').val(), txtConcluido: ($('#txtConcluido').is(':checked') ? "1" : "0"),
                            bntSave: "S"}).done(function (data) {
                            if ($.isNumeric(data)) {
                                window.location.href = "FormAgenda.php?id=" + data;
                            } else {
                                bootbox.alert("Erro: " + data);
                            }
                        });
                    } else {
                        bootbox.alert("Pagamento obrigatório para este agendamento!");                        
                    }
                } else {
                    bootbox.alert("A data início não deve ser maior que a data fim do agendamento!");
                }
            } else {
                bootbox.alert("Verifique o preenchimento dos campos!");
            }
        }        
            
        function atualizaPagamentosCli(id) {
            if (id !== "") {
                $.getJSON("./form/FormAgenda.php", {txtPagamentos: "S", txtPessoa: id, txtAgendamento: $("#txtId").val(), txtAgenda: $("#txtAgenda").val()}, function (j) {
                    let options = "<option value=\"null\">Selecione</option>";
                    let selecionar = ($("#txtVendaItem").attr("venda") === "" ? "null" : $("#txtVendaItem").attr("venda"));
                    if (j !== null && j.length > 0) {
                        for (var i = 0; i < j.length; i++) {
                            options = options + "<option value=\"" + j[i].id + "\">" + j[i].descricao + "</option>";
                        }
                    }
                    $("#txtVendaItem").html(options);
                    $("#txtVendaItem").select2("val", selecionar);
                });
            }
        }

        function excluir() {
            if ($('#txtId').val() !== "") {
                bootbox.confirm('Confirma a exclusão deste agendamento?', function (result) {
                    if (result === true) {
                        $.post("form/FormAgenda.php", {txtId: $('#txtId').val(),
                            bntCancelar: "S"}).done(function (data) {
                            if (data === "YES") {
                                parent.FecharBoxF();
                                return true;
                            } else {
                                bootbox.alert("Erro: " + data);
                            }
                        });
                    }
                });
            }
            return false;
        }

        function Print() {
            if($("#txtId").val() !== "") {
                window.open("../../Modulos/Academia/cupom_agendamento.php?id=" + $("#txtId").val(), '_blank');
            }
        }
        
        function enviar_email() {
            if ($("#txtAlunoEmail").val().length > 0) {
                bootbox.confirm('Deseja enviar um E-mail para este agendamento?', function (result) {
                    if (result === true) {
                        parent.$("#loader").show();
                        $.post('../../Modulos/CRM/email.ajax.php', {txtTipo: "F", Modelo: "3", txtId: $("#txtId").val()}, function (data) {
                            if (data === "YES") {                                                                                 
                                $.post('../../Modulos/CRM/form/EmailsEnvioFormServer.php', {
                                    btnEnviar: "S",
                                    txtId: "",
                                    txtAssunto: $("#txtAssunto").val(),
                                    ckeditor: "Segue em Anexo",
                                    txtAnexo: $("#txtId").val(),
                                    txtRecibo: $("#txtAlunoNome").val(),
                                    txtEmails: $("#txtAlunoEmail").val()}, function (d) {                                
                                    parent.$("#loader").hide();
                                    if (d !== null) {
                                        var ret = JSON.parse(d);
                                        if (ret[0].qtd_total !== undefined) {
                                            var qtderro = ret[0].qtd_total - ret[0].qtd_sucesso;
                                            trataResultado(ret[0].qtd_sucesso, qtderro);
                                        } else {
                                            trataResultado(0, 0);
                                        }
                                    }                                
                                });
                            } else {
                                parent.$("#loader").hide();
                                bootbox.alert("Erro:" + data);
                            }
                            parent.$("#loader").hide();
                        });
                    }
                });                
            } else {
                bootbox.alert("Não há email(s) para esta operação!");
            }
        }
        
        var qtdSucesso = 0;
        var qtdFalha = 0;
        function trataResultado(qtdS, qtdE) {
            qtdSucesso = qtdSucesso + qtdS;
            qtdFalha = qtdFalha + qtdE;
            if ($("#totalde").text() === $("#totalpara").text()) {
                $("#loader", window.parent.document).hide();
                bootbox.alert("Foram enviado(s)" + (qtdSucesso + qtdFalha) + " email(s):<br><br><b>* Sucesso(s):" + qtdSucesso + "</b><br><b style='color:red'>* Falha(s):" + qtdFalha + "</b>");
            }
        }   
        
        function atualizaStatusCli(id) {
            $.post("../Academia/ajax/Clientes_Status_serve_processing.php", {idCli: id}).done(function (data) {
                $("#statusCli .button").attr("class", " ");
                $("#statusCli div").first().addClass("button");
                $(".name center").html(data.toUpperCase());
                if (data.trim().toUpperCase() === "SUSPENSO") {
                    $("#statusCli div").first().addClass("label-Suspenso");
                } else if (data.trim().toUpperCase() === "CANCELADO") {
                    $("#statusCli div").first().addClass("label-Cancelado");
                } else if (data.trim().toUpperCase() === "INATIVO") {
                    $("#statusCli div").first().addClass("label-Inativo");
                } else if (data.trim().toUpperCase() === "ATIVOEMABERTO") {
                    $("#statusCli div").first().addClass("label-AtivoEmAberto");                    
                    $(".name center").html("ATIVO EM ABERTO");                    
                } else if (data.trim().toUpperCase() === "ATIVOABONO") {
                    $("#statusCli div").first().addClass("label-AtivoAbono");                    
                    $(".name center").html("ATIVO ABONO");
                } else if (data.trim().toUpperCase().substring(0, 5) === "ATIVO") {
                    $("#statusCli div").first().addClass("label-Ativo");
                } else if (data.trim().toUpperCase() === "FERIAS") {
                    $("#statusCli div").first().addClass("label-Ferias");
                    $(".name center").html("EM FÉRIAS");
                } else if (data.trim().toUpperCase() === "DEPENDENTE") {
                    $("#statusCli div").first().addClass("label-Dependente");
                    $(".name center").html("DEPENDENTE");            
                } else if (data.trim().toUpperCase() === "DESLIGADO") {
                    $("#statusCli div").first().addClass("label-Desligado");
                    $(".name center").html("DESLIGADO");            
                } else if (data.trim().toUpperCase() === "DESLIGADOEMABERTO") {
                    $("#statusCli div").first().addClass("label-DesligadoEmAberto");
                    $(".name center").html("DESLIGADO EM ABERTO");            
                }
            });
        }       

        function enviarSMS() {
            $("#txtSms").val($("#txtAlunoCel").val() + '|' + $("#txtAluno").val());
            var qtdsms = $("#txtSms").val().split(",").length;
            if ($("#txtSMSUser").val() === "1" && qtdsms > 0 && $("#txtId").val() === "" && $("#txtAluno").val() > 0 && $("#txtAlunoCel").val().length > 0 && $("#txtAssunto").val().length > 0) {
                $("#totalde").text(qtdsms);
                bootbox.confirm('Deseja enviar um SMS para este agendamento?', function (result) {
                    if (result === true) {
                        $("#loader", window.parent.document).show();
                        $.post("./../../util/sms/SmsSaldoForm.php", "totTrans=" + qtdsms).done(function (data) {
                            if (data > 0) {
                                var formData = new FormData();
                                formData.append('btnEnviar', 'S');
                                formData.append('ckeditor', $("#txtDataInicio").val() + ' ' + $("#txtHoraInicio").val() + ' ' + $("#txtAssunto").val());
                                var emailTotal = $("#txtSms").val().split(",");
                                var emailrepeticao = [];
                                $("#loader", window.parent.document).show();
                                emailrepeticao = [];
                                for (item = 0; item < emailTotal.length; item++) {
                                    if (emailTotal[item] !== undefined) {
                                        emailrepeticao.push(emailTotal[item]);
                                    }
                                }
                                formData.append('txtSendSms', emailrepeticao);
                                $.ajax({
                                    type: "POST",
                                    url: "./../../util/sms/send_sms.php",
                                    data: formData,
                                    dataType: "json",
                                    processData: false,
                                    contentType: false
                                }).always(function (data) {
                                    $("#loader", window.parent.document).hide();
                                    if (data[0].msg_erro !== undefined) {
                                        bootbox.alert(data[0].msg_erro);
                                    } else {
                                        if (data[0].qtd_total !== undefined) {
                                            var qtderro = data[0].qtd_total - data[0].qtd_sucesso;
                                            bootbox.alert("Foram enviado " + data[0].qtd_sucesso + " sms(s) com sucesso e " + qtderro + " sms(s) com falha!", function() {
                                                parent.FecharBoxF();
                                            });
                                        } else {
                                            bootbox.alert("Foram enviado 0 sms(s) com sucesso e 0 sms(s) com falha!", function() {
                                                parent.FecharBoxF();
                                            });
                                        }
                                    }
                                });
                            } else {
                                $("#loader", window.parent.document).hide();
                                bootbox.alert("Saldo insuficiente!");
                            }
                        });
                    }
                });
            }
        }
    
        function abrirContrato() {
            $.getJSON('./../CRM/contratos.ajax.php', {txtTipo: "2"}, function (itens) {
                let arrayOptions = [];                
                arrayOptions.push({text: 'Selecione', value: 'null'});                
                for (var i = 0; i < itens.length; i++) {
                    arrayOptions.push({text: itens[i].descricao, value: itens[i].id});
                }                
                bootbox.prompt({
                    title: "Selecione um modelo e contrato:",
                    inputType: 'select',
                    inputOptions: arrayOptions,
                    callback: function (result) {
                        if (result !== 'null') {
                            window.open("../../util/ImpressaoPdf.php?id_plan=" + $("#txtId").val() + "&idContrato=" + result + "&model=A&tpImp=I&NomeArq=Contrato&PathArq=../Modulos/Comercial/modelos/ModeloContrato.php&emp=1", '_blank');
                        } else {
                            bootbox.alert("É necessário selecionar um contrato válido!");
                        }
                    }
                });
            });            
        }
        
        $('#tblLinkHistorico').dataTable({
            "iDisplayLength": 20,
            "aLengthMenu": [20, 30, 40, 50, 100],
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "./../Academia/form/ClientesPlanosServer.php?listPagamentosLink=S" + 
            "&agendamento=" + $("#txtId").val() + "&dti=" + $("#txt_dt_begin_link").val() + "&dtf=" + $("#txt_dt_end_link").val(),
            "bFilter": false,
            "bSort": false,
            "bPaginate": false,
            'oLanguage': {
                'sInfo': "",
                'sInfoEmpty': "",
                'sInfoFiltered': "",
                'sLengthMenu': "",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sSearch': "Pesquisar:",
                'sZeroRecords': "Não foi encontrado nenhum resultado"},
            "sPaginationType": "full_numbers"
        });
        
        function AtualizaListHistoricoLink() {
            var tblPlanosBoleto = $("#tblLinkHistorico").dataTable();
            tblPlanosBoleto.fnReloadAjax("./../Academia/form/ClientesPlanosServer.php?listPagamentosLink=S" + 
            "&agendamento=" + $("#txtId").val() + "&dti=" + $("#txt_dt_begin_link").val() + "&dtf=" + $("#txt_dt_end_link").val());            
        }    
        
        function cancelarLinkOnline(id_boleto) {
            bootbox.confirm('Confirma o cancelamento do Link?', function (value) {
                if (value === true) {
                    $("#loader").show();
                    $.post("../../../util/dcc/makeLinkOnline.php", "cancelarLink=S" + 
                            "&txtIdLink=" + id_boleto).done(function (data) {
                        if (!isNaN(data.trim())) {
                            console.log(data.trim());
                            AtualizaListHistoricoLink();
                        } else {
                            bootbox.alert("Erro ao cancelar o Link!<br/>" + data.trim());
                        }
                        $("#loader").hide();                                                
                    });
                } else {
                    return;
                }
            });
        }

        function excluirLinkOnline(id_boleto) {
            bootbox.confirm('Confirma a exclusão do Link?', function (value) {
                if (value === true) {
                    $("#loader").show();
                    $.post("../../../util/dcc/makeLinkOnline.php", "excluirLink=S" + 
                            "&txtIdLink=" + id_boleto).done(function (data) {
                        if (!isNaN(data.trim())) {
                            console.log(data.trim());
                            AtualizaListHistoricoLink();                                               
                        } else {
                            bootbox.alert("Erro ao excluir o Link!<br/>" + data.trim());
                        }
                        $("#loader").hide();                                                
                    });
                } else {
                    return;
                }
            });
        }
        
        $("#btnfindLink").click(function (event) {
            AtualizaListHistoricoLink();
        });        
                
        $("#btnNovoLink").click(function (event) {
            bootbox.confirm('Confirma a geração do Link com os itens selecionados?', function (value) {
                if (value === true) {
                    $("#loader").show();
                    $.post("../../../util/dcc/makeLinkOnline.php", "salvaLink=S" + 
                            "&txtIdCli=" + $("#txtAluno").val() +
                            "&txtAgendamento=" + $("#txtId").val()).done(function (data) {
                        if (!isNaN(data.trim())) {
                            console.log(data.trim());
                            AtualizaListHistoricoLink();                                                
                        } else {
                            bootbox.alert("Erro ao gerar o Link!<br/>" + data.trim());
                        }
                        $("#loader").hide();                                                
                    });
                } else {
                    return;
                }
            });
        });        
                
    </script>
    <?php odbc_close($con); ?>
</body>