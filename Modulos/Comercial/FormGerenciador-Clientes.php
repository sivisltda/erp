<?php
include "../../Connections/configini.php";
$FinalUrl = '';
$returnPage = "Gerenciador-Clientes.php";
$linhaContato = "1";

$disabled = 'disabled';
$active1 = 'active';
$active4 = '';
$active5 = '';
$active8 = '';

$q1_tc_recebidas = 0;
$q1_nc_recebidas = 0;
$q1_tc_receber = 0;
$q1_nc_receber = 0;

$q2_ult_conta = "";
$q2_pri_conta = "";
$q2_ult_venc = "";
$q2_pro_venc = "";
$q2_med_recebido = 0;
$q2_med_receber = 0;

$q3_tot_atraso = 0;
$q3_qtd_atraso = 0;
$q3_med_atraso = 0;
$q3_tot_antecipado = 0;
$q3_qtd_antecipado = 0;
$q3_med_antecipado = 0;

if (isset($_POST['bntSave'])) {
    $notIn = "";
    odbc_exec($con, "set dateformat dmy;") or die(odbc_errormsg());
    if ($_POST['txtId'] != '') {
        $query = " update sf_fornecedores_despesas set " .
                "razao_social = " . valoresTexto("txtRazao") . "," .
                "cnpj = " . valoresTexto("txtCnpj") . "," .
                "inscricao_estadual = " . valoresTexto("txtInsc") . "," .
                "endereco = " . valoresTexto("txtEndereco") . "," .
                "numero = " . valoresTexto("txtNumero") . "," .
                "complemento = " . valoresTexto("txtComplemento") . "," .
                "bairro = " . valoresTexto("txtBairro") . "," .
                "estado = " . valoresSelect('txtEstado') . "," .
                "cidade = " . valoresSelect('txtCidade') . "," .
                "cep = " . valoresTexto("txtCep") . "," .
                "contato = " . valoresTexto("txtContato") . "," .
                "nome_fantasia = " . valoresTexto("txtFantasia") . "," .
                "profissao = " . valoresTexto("txtProfissao") . "," .
                "data_nascimento = " . valoresData("txtDataNascimento") . "," .
                "grupo_pessoa = " . valoresSelect('txtGrupoPessoas') . "," .
                "procedencia = " . valoresSelect('txtProcedencia') . "," .
                "naturalidade = " . valoresTexto("txtnaturalidade") . "," .
                "nacionalidade = " . valoresTexto("txtnacionalidade") . "," .
                "estado_civil = " . valoresTexto("txtestado_civil") . "," .
                "nome_mae = " . valoresTexto("txtnome_mae") . "," .
                "nome_pai = " . valoresTexto("txtnome_pai") . "," .
                "referencia_ps1 = " . valoresTexto("txtreferencia_ps1") . "," .
                "telefone_ps1 = " . valoresTexto("txttelefone_ps1") . "," .
                "referencia_ps2 = " . valoresTexto("txtreferencia_ps2") . "," .
                "telefone_ps2 = " . valoresTexto("txttelefone_ps2") . "," .
                "trabalha_empresa = " . valoresTexto("txttrabalha_empresa") . "," .
                "regime_tributario = " . valoresTexto("txtContribuinte") . "," .
                "inscricao_municipal = " . valoresTexto("txtMunicipal") . "," .
                "ie_tributario = " . valoresTexto("txtSuframa") . "," .
                "bloqueado = " . valoresCheck('ckbInativo') . " " .
                "where id_fornecedores_despesas = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        $id = $_POST['txtId'];
        if ($_POST['txtOBS'] != '' and $id != '' and $_SESSION["login_usuario"] != '') {
            odbc_exec($con, "insert into sf_mensagens_fornecedores(id_fornecedores_despesas,mensagem,user_send,data_send) values(" . $id . "," . valoresTexto("txtOBS") . ",'" . $_SESSION["login_usuario"] . "',getDate())");
        }
        $notIn = '0';
        if (isset($_POST['txtQtdContatos'])) {
            $max = $_POST['txtQtdContatos'];
            for ($i = 0; $i <= $max; $i++) {
                if (isset($_POST['txtIdContato_' . $i]) && is_numeric($_POST['txtIdContato_' . $i])) {
                    if ($_POST['txtIdContato_' . $i] != 0) {
                        $notIn = $notIn . "," . $_POST['txtIdContato_' . $i];
                    }
                }
            }
            if ($notIn != "") {
                odbc_exec($con, "delete from sf_fornecedores_despesas_contatos where fornecedores_despesas = " . $_POST['txtId'] . " and id_contatos not in (" . $notIn . ")");
            }
        }
    } else {
        $query = "insert into sf_fornecedores_despesas(razao_social,cnpj,inscricao_estadual,endereco,numero,complemento,bairro,estado,cidade,cep,contato,nome_fantasia,profissao,data_nascimento,procedencia,naturalidade,nacionalidade,estado_civil,nome_mae,nome_pai,referencia_ps1,telefone_ps1,referencia_ps2,telefone_ps2,trabalha_empresa,grupo_pessoa,tipo,empresa,dt_cadastro,regime_tributario,inscricao_municipal,ie_tributario,bloqueado)values(" .
                strtoupper(valoresTexto("txtRazao")) . "," .
                valoresTexto("txtCnpj") . "," .
                valoresTexto("txtInsc") . "," .
                valoresTexto("txtEndereco") . "," .
                valoresTexto("txtNumero") . "," .
                valoresTexto("txtComplemento") . "," .
                valoresTexto("txtBairro") . "," .
                valoresSelect('txtEstado') . "," .
                valoresSelect('txtCidade') . "," .
                valoresTexto("txtCep") . "," .
                valoresTexto("txtContato") . "," .
                valoresTexto("txtFantasia") . "," .
                valoresTexto("txtProfissao") . "," .
                valoresData("txtDataNascimento") . "," .
                valoresSelect('txtProcedencia') . "," .
                valoresTexto("txtnaturalidade") . "," .
                valoresTexto("txtnacionalidade") . "," .
                valoresTexto("txtestado_civil") . "," .
                valoresTexto("txtnome_mae") . "," .
                valoresTexto("txtnome_pai") . "," .
                valoresTexto("txtreferencia_ps1") . "," .
                valoresTexto("txttelefone_ps1") . "," .
                valoresTexto("txtreferencia_ps2") . "," .
                valoresTexto("txttelefone_ps2") . "," .
                valoresTexto("txttrabalha_empresa") . "," .
                valoresSelect('txtGrupoPessoas') . ",'C','001',getDate()," .
                valoresTexto("txtContribuinte") . "," .
                valoresTexto("txtMunicipal") . "," .
                valoresTexto("txtSuframa") . "," .
                valoresCheck('ckbInativo') . ")";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_fornecedores_despesas from sf_fornecedores_despesas order by id_fornecedores_despesas desc") or die(odbc_errormsg());
        $id = odbc_result($res, 1);
        if ($_POST['txtOBS'] != '' and $id != '' and $_SESSION["login_usuario"] != '') {
            odbc_exec($con, "insert into sf_mensagens_fornecedores(id_fornecedores_despesas,mensagem,user_send,data_send) values(" . $id . "," . valoresTexto("txtOBS") . ",'" . $_SESSION["login_usuario"] . "',getDate())");
        }
    }

    if ($id !== "") {
        if (isset($_POST['txtQtdContatos'])) {
            $max = $_POST['txtQtdContatos'];
            for ($i = 1; $i < $max; $i++) {
                if (isset($_POST['txtIdContato_' . $i])) {
                    if ($_POST['txtIdContato_' . $i] !== "0") {
                        odbc_exec($con, "update sf_fornecedores_despesas_contatos set tipo_contato = " . $_POST['txtTpContato_' . $i] . ",conteudo_contato = " . valoresTexto('txtTxContato_' . $i) . " where id_contatos = " . $_POST['txtIdContato_' . $i]) or die(odbc_errormsg());
                    } else {
                        odbc_exec($con, "insert into sf_fornecedores_despesas_contatos(fornecedores_despesas,tipo_contato,conteudo_contato) values(" . $id . "," . $_POST['txtTpContato_' . $i] . "," . valoresTexto('txtTxContato_' . $i) . ")") or die(odbc_errormsg());
                    }
                }
            }
        }
        $_POST['txtId'] = $id;
    }
}
if (isset($_POST['bntSaveEnd'])) {
    if ($_POST['txtId'] != '') {
        $active1 = '';
        $active4 = 'active';
        $active5 = '';
        $active8 = '';
        if ($end != '') {
            $query = " update sf_fornecedores_despesas_endereco_entrega set " .
                    "nome_end_entrega = " . valoresTexto("txtnome_end_entrega") . "," .
                    "end_entrega = " . valoresTexto("txtend_entrega") . "," .
                    "numero_entrega = " . valoresTexto("txtnumero_entrega") . "," .
                    "complemento_entrega = " . valoresTexto("txtcomplemento_entrega") . "," .
                    "bairro_entrega = " . valoresTexto("txtbairro_entrega") . "," .
                    "cidade_entrega = " . valoresTexto("txtcidade_entrega") . "," .
                    "estado_entrega = " . valoresTexto("txtestado_entrega") . "," .
                    "cep_entrega = " . valoresTexto("txtcep_entrega") . "," .
                    "telefone_entrega = " . valoresTexto("txttelefone_entrega") . "," .
                    "referencia_entrega = " . valoresTexto("txtreferencia_entrega") . " " .
                    "where id_endereco = " . $end;
        } else {
            $query = "insert into sf_fornecedores_despesas_endereco_entrega(fornecedores_despesas,nome_end_entrega,end_entrega,numero_entrega,complemento_entrega,bairro_entrega,cidade_entrega,estado_entrega,cep_entrega,telefone_entrega,referencia_entrega)values(" .
                    $_POST['txtId'] . "," .
                    valoresTexto("txtnome_end_entrega") . "," .
                    valoresTexto("txtend_entrega") . "," .
                    valoresTexto("txtnumero_entrega") . "," .
                    valoresTexto("txtcomplemento_entrega") . "," .
                    valoresTexto("txtbairro_entrega") . "," .
                    valoresTexto("txtcidade_entrega") . ", " .
                    valoresTexto("txtestado_entrega") . "," .
                    valoresTexto("txtcep_entrega") . ", " .
                    valoresTexto("txttelefone_entrega") . "," .
                    valoresTexto("txtreferencia_entrega") . ")";
        }
        odbc_exec($con, $query);
    }
}
if ($_GET['Del_E'] != '') {
    $active1 = '';
    $active4 = 'active';
    $active5 = '';
    $active8 = '';
    odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_endereco_entrega WHERE id_endereco = " . $_GET['Del_E']);
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_fornecedores_despesas where id_fornecedores_despesas = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_fornecedores_despesas']);
        $razao = utf8_encode($RFP['razao_social']);
        $cnpj = utf8_encode($RFP['cnpj']);
        $insc = utf8_encode($RFP['inscricao_estadual']);
        $endereco = utf8_encode($RFP['endereco']);
        $numero = utf8_encode($RFP['numero']);
        $complemento = utf8_encode($RFP['complemento']);
        $bairro = utf8_encode($RFP['bairro']);
        $estado = utf8_encode($RFP['estado']);
        $cidade = utf8_encode($RFP['cidade']);
        $cep = utf8_encode($RFP['cep']);
        $contato = utf8_encode($RFP['contato']);
        $fantasia = utf8_encode($RFP['nome_fantasia']);
        $grupopes = utf8_encode($RFP['grupo_pessoa']);
        $profissao = utf8_encode($RFP['profissao']);
        $data_nascimento = escreverData($RFP['data_nascimento']);
        $data_cadastro = escreverData($RFP['dt_cadastro']);
        $procedencia = utf8_encode($RFP['procedencia']);
        $naturalidade = utf8_encode($RFP['naturalidade']);
        $nacionalidade = utf8_encode($RFP['nacionalidade']);
        $estado_civil = utf8_encode($RFP['estado_civil']);
        $nome_mae = utf8_encode($RFP['nome_mae']);
        $nome_pai = utf8_encode($RFP['nome_pai']);
        $referencia_ps1 = utf8_encode($RFP['referencia_ps1']);
        $telefone_ps1 = utf8_encode($RFP['telefone_ps1']);
        $referencia_ps2 = utf8_encode($RFP['referencia_ps2']);
        $telefone_ps2 = utf8_encode($RFP['telefone_ps2']);
        $trabalha_empresa = utf8_encode($RFP['trabalha_empresa']);
        $contribuinte = utf8_encode($RFP['regime_tributario']);
        $municipal = utf8_encode($RFP['inscricao_municipal']);
        $suframa = utf8_encode($RFP['ie_tributario']);
        $inativo = utf8_encode($RFP['bloqueado']);
    }
} else {
    $disabled = '';
    $id = '';
    $razao = '';
    $cnpj = '';
    $insc = '';
    $endereco = '';
    $numero = '';
    $complemento = '';
    $bairro = '';
    $estado = '';
    $cidade = '';
    $cep = '';
    $contato = '';
    $fantasia = '';
    $grupopes = '';
    $profissao = '';
    $data_nascimento = '';
    $data_cadastro = '';
    $procedencia = '';
    $naturalidade = '';
    $nacionalidade = '';
    $estado_civil = '';
    $nome_mae = '';
    $nome_pai = '';
    $referencia_ps1 = '';
    $telefone_ps1 = '';
    $referencia_ps2 = '';
    $telefone_ps2 = '';
    $trabalha_empresa = '';
    $contribuinte = 2;
    $municipal = '';
    $suframa = '';
    $inativo = 0;
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_mensagens_fornecedores WHERE id_fornecedores_despesas = " . $_POST['txtId']);
        odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_endereco_entrega WHERE fornecedores_despesas = " . $_POST['txtId']);
        odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = " . $_POST['Del']);
        odbc_exec($con, "DELETE FROM sf_fornecedores_despesas WHERE id_fornecedores_despesas = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso');parent.FecharBox(0);</script>";
    }
}
if (isset($_POST['bntSaveMsg'])) {
    if ($_POST['txtOBS'] != '' and $id != '' and $_SESSION["login_usuario"] != '') {
        $active1 = '';
        $active4 = '';
        $active5 = 'active';
        $active8 = '';
        odbc_exec($con, "insert into sf_mensagens_fornecedores(id_fornecedores_despesas,mensagem,user_send,data_send) values(" . $id . "," . valoresTexto("txtOBS") . ",'" . $_SESSION["login_usuario"] . "',getDate())");
    }
}
if ($_GET['Del_M'] != '') {
    $active1 = '';
    $active4 = '';
    $active5 = 'active';
    $active8 = '';
    odbc_exec($con, "DELETE FROM sf_mensagens_fornecedores WHERE id_mensagem = " . $_GET['Del_M']);
}
if (isset($_POST['bntSendFile'])) {
    $allowedExts = array("gif", "jpeg", "jpg", "png", "pdf", "GIF", "JPEG", "JPG", "PNG", "PDF");
    $extension = explode(".", $_FILES["file"]["name"]);
    $extension = $extension[count($extension) - 1];
    if ((($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/pjpeg") || ($_FILES["file"]["type"] == "image/x-png") || ($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "application/pdf")) && ($_FILES["file"]["size"] < 10000000) && in_array($extension, $allowedExts)) {
        if ($_FILES["file"]["error"] == 0) {
            move_uploaded_file($_FILES["file"]["tmp_name"], "./../../Pessoas/" . $contrato . "/Documentos/" . $id . "/" . $_FILES["file"]["name"]);
        }
    } else {
        echo "<script type='text/javascript'>alert('Erro ao enviar o arquivo.\\nTamanho máximo permitido: 10 mb.\\nFormatos aceitos: gif, jpeg, jpg, png e pdf.');</script>";
    }
    $active1 = '';
    $active4 = '';
    $active5 = '';
    $active8 = 'active';
}
if ($_GET['Del_F'] != '') {
    if (file_exists("./../../Pessoas/" . $contrato . "/Documentos/" . $id . "/" . $_GET['Del_F'])) {
        unlink("./../../Pessoas/" . $contrato . "/Documentos/" . $id . "/" . $_GET['Del_F']);
    } else {
        echo "<script type='text/javascript'>alert('Erro ao excluir o arquivo.\\nEste arquivo não existe em nosso sistema.');</script>";
    }
    $active1 = '';
    $active4 = '';
    $active5 = '';
    $active8 = 'active';
}
$FinalUrl = "";
if (isset($_GET["idx"])) {
    $FinalUrl = $_GET["idx"];
}
if (isset($_POST["txtFinalUrl"])) {
    $FinalUrl = $_POST["txtFinalUrl"];
}

if ($id != '') {
    $cur = odbc_exec($con, "select sum(valor_pago) as valor_pago,sum(n_valor_pago) as qtd_pago from (
    select isnull(sum(valor_pago),0) valor_pago,isnull(count(valor_pago),0) n_valor_pago from sf_lancamento_movimento_parcelas lp
    inner join sf_lancamento_movimento lm on lp.id_lancamento_movimento = lm.id_lancamento_movimento
    inner join sf_contas_movimento c on c.id_contas_movimento = lm.conta_movimento
    where fornecedor_despesa = " . $id . " and lm.status = 'Aprovado' and lp.inativo = 0 and tipo = 'C' and data_pagamento is not null
    union
    select isnull(sum(valor_pago),0) valor_pago,isnull(count(valor_pago),0) n_valor_pago from sf_solicitacao_autorizacao_parcelas sp
    inner join sf_solicitacao_autorizacao sa on sp.solicitacao_autorizacao = sa.id_solicitacao_autorizacao
    inner join sf_contas_movimento c on c.id_contas_movimento = sa.conta_movimento
    where fornecedor_despesa = " . $id . " and sp.inativo = 0 and tipo = 'C' and data_pagamento is not null)as x");
    while ($RFP = odbc_fetch_array($cur)) {
        $q1_tc_recebidas = $RFP['valor_pago'];
        $q1_nc_recebidas = $RFP['qtd_pago'];
        if ($q1_nc_recebidas > 0) {
            $q2_med_recebido = $q1_tc_recebidas / $q1_nc_recebidas;
        }
    }
    $cur = odbc_exec($con, "select sum(valor_pago) as valor_pago,sum(n_valor_pago) as qtd_pago from (
    select isnull(sum(valor_parcela),0) valor_pago,isnull(count(valor_parcela),0) n_valor_pago from sf_lancamento_movimento_parcelas lp
    inner join sf_lancamento_movimento lm on lp.id_lancamento_movimento = lm.id_lancamento_movimento
    inner join sf_contas_movimento c on c.id_contas_movimento = lm.conta_movimento
    where fornecedor_despesa = " . $id . " and lm.status = 'Aprovado' and lp.inativo = 0 and tipo = 'C' and data_pagamento is null
    union
    select isnull(sum(valor_parcela),0) valor_pago,isnull(count(valor_parcela),0) n_valor_pago from sf_solicitacao_autorizacao_parcelas sp
    inner join sf_solicitacao_autorizacao sa on sp.solicitacao_autorizacao = sa.id_solicitacao_autorizacao
    inner join sf_contas_movimento c on c.id_contas_movimento = sa.conta_movimento
    where fornecedor_despesa = " . $id . " and sp.inativo = 0 and tipo = 'C' and data_pagamento is null
    )as x");
    while ($RFP = odbc_fetch_array($cur)) {
        $q1_tc_receber = $RFP['valor_pago'];
        $q1_nc_receber = $RFP['qtd_pago'];
        if ($q1_nc_receber > 0) {
            $q2_med_receber = $q1_tc_receber / $q1_nc_receber;
        }
    }
    $cur = odbc_exec($con, "select max(valor_pago) as ult_conta,min(n_valor_pago) as pri_conta from (
    select max(data_pagamento) valor_pago,min(data_pagamento) n_valor_pago from sf_lancamento_movimento_parcelas lp
    inner join sf_lancamento_movimento lm on lp.id_lancamento_movimento = lm.id_lancamento_movimento
    inner join sf_contas_movimento c on c.id_contas_movimento = lm.conta_movimento
    where fornecedor_despesa = " . $id . " and lm.status = 'Aprovado' and lp.inativo = 0 and tipo = 'C' and data_pagamento is not null
    union
    select max(data_pagamento) valor_pago,min(data_pagamento) n_valor_pago from sf_solicitacao_autorizacao_parcelas sp
    inner join sf_solicitacao_autorizacao sa on sp.solicitacao_autorizacao = sa.id_solicitacao_autorizacao
    inner join sf_contas_movimento c on c.id_contas_movimento = sa.conta_movimento
    where fornecedor_despesa = " . $id . " and sp.inativo = 0 and tipo = 'C' and data_pagamento is not null)as x");
    while ($RFP = odbc_fetch_array($cur)) {
        $q2_ult_conta = $RFP['ult_conta'];
        $q2_pri_conta = $RFP['pri_conta'];
    }
    $cur = odbc_exec($con, "select max(valor_pago) as ult_conta,min(n_valor_pago) as pri_conta from (
    select max(data_vencimento) valor_pago,min(data_vencimento) n_valor_pago from sf_lancamento_movimento_parcelas lp
    inner join sf_lancamento_movimento lm on lp.id_lancamento_movimento = lm.id_lancamento_movimento
    inner join sf_contas_movimento c on c.id_contas_movimento = lm.conta_movimento
    where fornecedor_despesa = " . $id . " and lm.status = 'Aprovado' and lp.inativo = 0 and tipo = 'C' and data_pagamento is null
    union
    select max(data_parcela) valor_pago,min(data_parcela) n_valor_pago from sf_solicitacao_autorizacao_parcelas sp
    inner join sf_solicitacao_autorizacao sa on sp.solicitacao_autorizacao = sa.id_solicitacao_autorizacao
    inner join sf_contas_movimento c on c.id_contas_movimento = sa.conta_movimento
    where fornecedor_despesa = " . $id . " and sp.inativo = 0 and tipo = 'C' and data_pagamento is null)as x");
    while ($RFP = odbc_fetch_array($cur)) {
        $q2_ult_venc = $RFP['ult_conta'];
        $q2_pro_venc = $RFP['pri_conta'];
    }
    $cur = odbc_exec($con, "select sum(valor_pago) as valor_pago,sum(n_valor_pago) as qtd_pago,SUM(dias) as dias from (
    select isnull(sum(valor_pago),0) valor_pago,isnull(count(valor_pago),0) n_valor_pago, SUM(DATEDIFF(DAY,data_vencimento,data_pagamento)) dias from sf_lancamento_movimento_parcelas lp
    inner join sf_lancamento_movimento lm on lp.id_lancamento_movimento = lm.id_lancamento_movimento
    inner join sf_contas_movimento c on c.id_contas_movimento = lm.conta_movimento
    where fornecedor_despesa = " . $id . " and lm.status = 'Aprovado' and lp.inativo = 0 and tipo = 'C' and data_pagamento is not null and data_vencimento < data_pagamento
    union
    select isnull(sum(valor_pago),0) valor_pago,isnull(count(valor_pago),0) n_valor_pago, SUM(DATEDIFF(DAY,data_parcela,data_pagamento)) dias from sf_solicitacao_autorizacao_parcelas sp
    inner join sf_solicitacao_autorizacao sa on sp.solicitacao_autorizacao = sa.id_solicitacao_autorizacao
    inner join sf_contas_movimento c on c.id_contas_movimento = sa.conta_movimento
    where fornecedor_despesa = " . $id . " and sp.inativo = 0 and tipo = 'C' and data_pagamento is not null and data_parcela < data_pagamento)as x");
    while ($RFP = odbc_fetch_array($cur)) {
        $q3_tot_atraso = $RFP['valor_pago'];
        $q3_qtd_atraso = $RFP['qtd_pago'];
        if ($RFP['qtd_pago'] > 0) {
            $q3_med_atraso = $RFP['dias'] / $RFP['qtd_pago'];
        }
    }
    $cur = odbc_exec($con, "select sum(valor_pago) as valor_pago,sum(n_valor_pago) as qtd_pago,SUM(dias) as dias from (
    select isnull(sum(valor_pago),0) valor_pago,isnull(count(valor_pago),0) n_valor_pago, SUM(DATEDIFF(DAY,data_pagamento,data_vencimento)) dias from sf_lancamento_movimento_parcelas lp
    inner join sf_lancamento_movimento lm on lp.id_lancamento_movimento = lm.id_lancamento_movimento
    inner join sf_contas_movimento c on c.id_contas_movimento = lm.conta_movimento
    where fornecedor_despesa = " . $id . " and lm.status = 'Aprovado' and lp.inativo = 0 and tipo = 'C' and data_pagamento is not null and data_vencimento > data_pagamento
    union
    select isnull(sum(valor_pago),0) valor_pago,isnull(count(valor_pago),0) n_valor_pago, SUM(DATEDIFF(DAY,data_pagamento,data_parcela)) dias from sf_solicitacao_autorizacao_parcelas sp
    inner join sf_solicitacao_autorizacao sa on sp.solicitacao_autorizacao = sa.id_solicitacao_autorizacao
    inner join sf_contas_movimento c on c.id_contas_movimento = sa.conta_movimento
    where fornecedor_despesa = " . $id . " and sp.inativo = 0 and tipo = 'C' and data_pagamento is not null and data_parcela > data_pagamento)as x");
    while ($RFP = odbc_fetch_array($cur)) {
        $q3_tot_antecipado = $RFP['valor_pago'];
        $q3_qtd_antecipado = $RFP['qtd_pago'];
        if ($RFP['qtd_pago'] > 0) {
            $q3_med_antecipado = $RFP['dias'] / $RFP['qtd_pago'];
        }
    }
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
<title>Cadastro de Funcionário</title>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<link href="../../../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <div class="row-fluid">
        <div id="inline1" style="height:540px; overflow:hidden">
            <div class="block title">
                <div class="head">
                    <div class="frmhead">
                        <div class="frmtext">Clientes</div>
                        <div class="frmicon" onclick="<?php
                        if ($FinalUrl != "") {
                            echo "parent.FecharBox(3)";
                        } else {
                            echo "parent.FecharBox(0)";
                        }
                        ?>">
                            <span class="ico-remove"></span>
                        </div>
                    </div>                                        
                </div>
            </div>
            <div class="block">
                <form id="frmEnviaDados" action="FormGerenciador-Clientes.php" method="POST" name="frmEnviaDados" enctype="multipart/form-data">
                    <div class="head"> <a href="#" onClick="source('tabs'); return false;">
                            <input name="txtFinalUrl" id="txtFinalUrl" value="<?php echo $FinalUrl; ?>" type="hidden"/>
                            <div class="icon"><span class="ico-info" style="color:#fff; margin:0; padding:0;"></span></div>
                        </a> </div>
                    <div class="data-fluid tabbable">
                        <ul class="nav nav-tabs">
                            <li class="<?php echo $active1; ?>" style="margin-left:7px;" ><a href="#tab1" data-toggle="tab">Dados Principais</a></li>
                            <li><a href="#tab9" data-toggle="tab">Contatos</a></li>
                            <?php if ($ckb_adm_cli_read_ == 0) { ?>
                                <li><a href="#tab2" data-toggle="tab">Cobrança</a></li>
                                <li class="<?php echo $active4; ?>"><a href="#tab4" data-toggle="tab">Entrega</a></li>
                            <?php } ?>
                            <li class="<?php echo $active5; ?>"><a href="#tab5" data-toggle="tab">OBS</a></li>
                            <li><a href="#tab6" data-toggle="tab">CRM</a></li>
                            <?php if ($ckb_adm_cli_read_ == 0) { ?>
                                <li><a href="#tab7" data-toggle="tab">Histórico</a></li>
                            <?php } ?>
                            <li class="<?php echo $active8; ?>"><a href="#tab8" data-toggle="tab">Documentos</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane <?php echo $active1; ?>" id="tab1">
                                <input name="txtQtdContatos" id="txtQtdContatos" value="<?php echo $linhaContato; ?>" type="hidden"/>
                                <table width="680" border="0" cellpadding="3" cellspacing="0" style="margin-bottom:80px;" class="textosComuns">
                                    <tr>
                                        <td width="122" align="right"><input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>Nome/Razão Social:</td>
                                        <td colspan="3">
                                            <div id="sprytextfield1" style="width:360px; float:left" class=""><input name="txtRazao" id="txtRazao" type="text" class="input-medium" maxlength="128" <?php echo $disabled; ?> value="<?php echo $razao; ?>"/></div>
                                            <div style="width:75px; margin-right:5px; line-height:28px; float:left" align="right">Cadastrado:</div>
                                            <div style="width:106px; float:left"><input type="text" class="input-medium" value="<?php echo $data_cadastro; ?>" disabled /></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Nome Fantasia:</td>
                                        <td colspan="3"><input name="txtFantasia" id="txtFantasia" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="128" value="<?php echo $fantasia; ?>"/></td>
                                    </tr>
                                    <tr>
                                        <td align="right">CNPJ/CPF:</td>
                                        <td width="209"><input onBlur="Validar(this)" id="txtCnpj" <?php echo $disabled; ?> name="txtCnpj" type="text" class="input-medium" onkeypress='mascaraMutuario(this, cpfCnpj)' onblur='clearTimeout()' maxlength="18" value="<?php echo $cnpj; ?>"/></td>
                                        <td width="100" align="right">Insc. Est/RG:</td>
                                        <td width="215"><input id="txtInsc" <?php echo $disabled; ?> name="txtInsc" type="text" class="input-medium" maxlength="25" value="<?php echo $insc; ?>"/></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Profissão / Atividade:</td>
                                        <td><input id="txtProfissao" <?php echo $disabled; ?> name="txtProfissao" type="text" class="input-medium" maxlength="100" value="<?php echo $profissao; ?>"/></td>
                                        <td align="right">Data nascimento / Fundação:</td>
                                        <td><input style="width:100px" id="txtDataNascimento" <?php echo $disabled; ?> name="txtDataNascimento" type="text" class="input-medium" maxlength="10" value="<?php echo $data_nascimento; ?>"/><span class="textfieldInvalidFormatMsg">X</span></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Endereço:</td>
                                        <td colspan="3"><input name="txtEndereco" id="txtEndereco" <?php echo $disabled; ?> type="text" class="input-medium"  maxlength="256" value="<?php echo $endereco; ?>"/></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Numero:</td>
                                        <td><input style="width:100px" id="txtNumero" <?php echo $disabled; ?> name="txtNumero" type="text" class="input-medium" maxlength="6" value="<?php echo $numero; ?>"/></td>
                                        <td align="right">Complemento:</td>
                                        <td><input id="txtComplemento" <?php echo $disabled; ?> name="txtComplemento" type="text" class="input-medium" maxlength="20" value="<?php echo $complemento; ?>"/></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Bairro:</td>
                                        <td><input id="txtBairro" name="txtBairro" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="20" value="<?php echo $bairro; ?>"/></td>
                                        <td align="right">Estado:</td>
                                        <td><select class="input-medium" style="width:220px;" <?php echo $disabled; ?> name="txtEstado" id="txtEstado" >
                                                <option value="null" >Selecione o estado</option>
                                                <?php
                                                $cur = odbc_exec($con, "select estado_codigo,estado_nome from tb_estados where estado_codigo > 0 order by estado_nome") or die(odbc_errormsg());
                                                while ($RFP = odbc_fetch_array($cur)) {
                                                    ?>
                                                    <option value="<?php echo $RFP['estado_codigo'] ?>"<?php
                                                    if (!(strcmp($RFP['estado_codigo'], $estado))) {
                                                        echo "SELECTED";
                                                    }
                                                    ?>><?php echo utf8_encode($RFP['estado_nome']) ?></option>
                                                        <?php } ?>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Cidade:</td>
                                        <td><span id="carregando" name="carregando" style="color:#666;display:none">Aguarde, carregando...</span>
                                            <select name="txtCidade" id="txtCidade" class="input-medium" style="width:220px;" <?php echo $disabled; ?> >
                                                <option value="null" >Selecione a cidade</option>
                                                <?php
                                                if ($estado != '') {
                                                    $sql = "select cidade_codigo,cidade_nome from tb_cidades where cidade_codigo > 0 AND cidade_codigoEstado = " . $estado . " ORDER BY cidade_nome";
                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                        ?>
                                                        <option value="<?php echo $RFP['cidade_codigo'] ?>"<?php
                                                        if (!(strcmp($RFP['cidade_codigo'], $cidade))) {
                                                            echo "SELECTED";
                                                        }
                                                        ?>><?php echo utf8_encode($RFP['cidade_nome']) ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                            </select>
                                            </span></td>
                                        <td align="right">CEP:</td>
                                        <td id="sprytextfield5"><input style="width:100px" id="txtCep" name="txtCep" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="9" value="<?php echo $cep; ?>"/>
                                            <span class="textfieldInvalidFormatMsg">Formato inválido.</span></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Contato:</td>
                                        <td><input name="txtContato" id="txtContato" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="128" value="<?php echo $contato; ?>"/></td>
                                        <td align="right">Grupo.:</td>
                                        <td><select class="input-medium" style="width:220px;" <?php echo $disabled; ?> name="txtGrupoPessoas" id="txtGrupoPessoas" >
                                                <option value="null" >Selecione o Grupo</option>
                                                <?php
                                                $cur = odbc_exec($con, "select id_grupo,descricao_grupo from dbo.sf_grupo_cliente where tipo_grupo = 'C' and inativo_grupo = 0 order by descricao_grupo") or die(odbc_errormsg());
                                                while ($RFP = odbc_fetch_array($cur)) {
                                                    ?>
                                                    <option value="<?php echo $RFP['id_grupo'] ?>"<?php
                                                    if (!(strcmp($RFP['id_grupo'], $grupopes))) {
                                                        echo "SELECTED";
                                                    }
                                                    ?>><?php echo utf8_encode($RFP['descricao_grupo']) ?></option>
                                                        <?php } ?>
                                            </select></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tab-pane" id="tab9">
                                <div class="toolbar dark" style="background-color: #DCDCDC">
                                    <div class="input-prepend input-append">
                                    </div>
                                    <table width="99%" border="0" cellpadding="3" class="textosComuns">
                                        <tr>
                                            <td>
                                                <select id="txtTipoContato" name="txtTipoContato" onChange="alterarMask()" <?php echo $disabled; ?>>
                                                    <option value="0">Telefone</option>
                                                    <option value="1">Celular</option>
                                                    <option value="2">E-mail</option>
                                                    <option value="3">Site</option>
                                                    <option value="4">Outros</option>
                                                </select>
                                            </td>
                                            <td id="sprytextfield2">
                                                <input id="txtTextoContato" type="text" <?php echo $disabled; ?>/>
                                            </td>
                                            <td width="80" align="right">
                                                <button type="button" onclick="addContato();" <?php echo $disabled; ?> class="btn dblue">Incluir <span class="icon-arrow-next icon-white"></span></button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="data dark npr npb" style="overflow:hidden">
                                    <div id="divContatos" class="messages" style="height:310px; overflow-y:scroll"></div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab2" style="height:380px">
                                <div style="overflow:auto;">
                                    <div class="accordion">
                                        <h3>Dados Pessoais</h3>
                                        <div>
                                            <p>
                                            <table class="textosComuns" width="100%" border="0" cellpadding="3" cellspacing="0" style="overflow:auto; border:none 0 transparent;">
                                                <tr>
                                                    <td width="106" align="right">Procedência:</td>
                                                    <td><select class="input-medium" <?php echo $disabled; ?> name="txtProcedencia" id="txtProcedencia" >
                                                            <option value="NULL">Não Informado</option>
                                                            <?php
                                                            $cur = odbc_exec($con, "select * from dbo.sf_procedencia where tipo_procedencia = 0 order by nome_procedencia") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) {
                                                                ?>
                                                                <option value="<?php echo $RFP['id_procedencia'] ?>"<?php
                                                                if (!(strcmp($RFP['id_procedencia'], $procedencia))) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>><?php echo utf8_encode($RFP['nome_procedencia']) ?></option>
                                                                    <?php } ?>
                                                        </select></td>
                                                    <td width="106" align="right">Estado Civil:</td>
                                                    <td><input id="txtestado_civil" <?php echo $disabled; ?> name="txtestado_civil" type="text" class="input-medium" maxlength="100" value="<?php echo $estado_civil; ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td width="106" align="right">Naturalidade:</td>
                                                    <td><input id="txtnaturalidade" <?php echo $disabled; ?> name="txtnaturalidade" type="text" class="input-medium" maxlength="100" value="<?php echo $naturalidade; ?>"/></td>
                                                    <td width="106" align="right">Nacionalidade:</td>
                                                    <td><input id="txtnacionalidade" <?php echo $disabled; ?> name="txtnacionalidade" type="text" class="input-medium" maxlength="100" value="<?php echo $nacionalidade; ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td width="106" align="right">Nome da Mãe:</td>
                                                    <td colspan="3"><input id="txtnome_mae" style="width:547px" <?php echo $disabled; ?> name="txtnome_mae" type="text" class="input-medium" maxlength="100" value="<?php echo $nome_mae; ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td width="106" align="right">Nome do Pai:</td>
                                                    <td colspan="3"><input id="txtnome_pai" style="width: 547px" <?php echo $disabled; ?> name="txtnome_pai" type="text" class="input-medium" maxlength="100" value="<?php echo $nome_pai; ?>"/></td>
                                                </tr>
                                            </table>
                                            </p>
                                        </div>
                                        <h3>Dados Fiscais</h3>
                                        <div>
                                            <p>
                                            <table class="textosComuns" width="100%" border="0" cellpadding="3" cellspacing="0" style="overflow:auto; border:none 0 transparent;">
                                                <tr>
                                                    <td width="106" align="right">Inscrição SUFRAMA:</td>
                                                    <td><input id="txtSuframa" <?php echo $disabled; ?> name="txtSuframa" type="text" class="input-medium" maxlength="100" value="<?php echo $suframa; ?>"/></td>
                                                    <td width="106" align="right">Inscrição Municipal:</td>
                                                    <td><input id="txtMunicipal" <?php echo $disabled; ?> name="txtMunicipal" type="text" class="input-medium" maxlength="100" value="<?php echo $municipal; ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td width="106" align="right">Tipo de Contribuinte:</td>
                                                    <td>
                                                        <select style="width:100%" class="input-medium" <?php echo $disabled; ?> name="txtContribuinte" id="txtContribuinte">
                                                            <option value="1" <?php
                                                            if ($contribuinte == 1) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>>Contribuinte do ICMS</option>
                                                            <option value="2" <?php
                                                            if ($contribuinte == 2) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>>Contribuinte ISENTO</option>
                                                            <option value="9" <?php
                                                            if ($contribuinte == 9) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>>Não Contribuinte</option>
                                                        </select>
                                                    </td>
                                                    <td width="106" align="right"></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                            </p>
                                        </div>
                                        <h3>Referências</h3>
                                        <div>
                                            <p>
                                            <table class="textosComuns" width="100%" border="0" cellpadding="3" cellspacing="0" style="overflow:auto; border:none 0 transparent;">
                                                <tr>
                                                    <td width="106" align="right">Referência Pessoal 1:</td>
                                                    <td><input id="txtreferencia_ps1" <?php echo $disabled; ?> name="txtreferencia_ps1" type="text" class="input-medium" maxlength="100" value="<?php echo $referencia_ps1; ?>"/></td>
                                                    <td width="106" align="right">Telefone Pessoal 1:</td>
                                                    <td><input id="txttelefone_ps1" <?php echo $disabled; ?> name="txttelefone_ps1" type="text" class="input-medium" maxlength="100" value="<?php echo $telefone_ps1; ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td width="106" align="right">Referência Pessoal 2:</td>
                                                    <td><input id="txtreferencia_ps2" <?php echo $disabled; ?> name="txtreferencia_ps2" type="text" class="input-medium" maxlength="100" value="<?php echo $referencia_ps2; ?>"/></td>
                                                    <td width="106" align="right">Telefone Pessoal 2:</td>
                                                    <td><input id="txttelefone_ps2" <?php echo $disabled; ?> name="txttelefone_ps2" type="text" class="input-medium" maxlength="100" value="<?php echo $telefone_ps2; ?>"/></td>
                                                </tr>
                                            </table>
                                            </p>
                                        </div>                                            
                                        <h3>Outras Informações</h3>
                                        <div>
                                            <p>
                                            <table class="textosComuns" width="100%" border="0" cellpadding="3" cellspacing="0" style="overflow:auto; border:none 0 transparent;">
                                                <tr>
                                                    <td width="106" align="right">Cliente Bloqueado:</td>
                                                    <td>
                                                        <input name="ckbInativo" id="ckbInativo" type="checkbox" <?php
                                                        if ($inativo == "1") {
                                                            echo "checked";
                                                        }
                                                        ?> class="input-medium" <?php echo $disabled; ?> value="1" style="opacity: 0;"/>
                                                    </td>
                                                    <td width="106" align="right">Nome da Empresa:</td>
                                                    <td><input id="txttrabalha_empresa" <?php echo $disabled; ?> name="txttrabalha_empresa" type="text" class="input-medium" maxlength="100" value="<?php echo $trabalha_empresa; ?>"/></td>
                                                </tr>
                                            </table>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane <?php echo $active4; ?>" id="tab4">
                                <table width="680" border="0" cellpadding="3" cellspacing="0" class="textosComuns" style="margin-bottom:10px;">
                                    <tr>
                                    <input name="txtEditEnd" id="txtEditEnd" value="" type="hidden"/>
                                    <td width="122" align="right">Nome Endereço:</td>
                                    <td colspan="3"><input style="width:546px" name="txtnome_end_entrega" id="txtnome_end_entrega" <?php echo $disabled; ?> type="text" class="input-medium"  maxlength="170" value=""/></td>
                                    </tr>
                                    <tr>
                                        <td width="122" align="right">Endereço:</td>
                                        <td colspan="3"><input style="width:546px" name="txtend_entrega" id="txtend_entrega" <?php echo $disabled; ?> type="text" class="input-medium"  maxlength="170" value=""/></td>
                                    </tr>
                                    <tr>
                                        <td width="122" align="right">Número:</td>
                                        <td width="204" ><input id="txtnumero_entrega" <?php echo $disabled; ?> name="txtnumero_entrega" type="text" class="input-medium" maxlength="100" value=""/></td>
                                        <td width="122" align="right">Compl.:</td>
                                        <td><input id="txtcomplemento_entrega" <?php echo $disabled; ?> name="txtcomplemento_entrega" type="text" class="input-medium" maxlength="100" value=""/></td>
                                    </tr>
                                    <tr>
                                        <td width="122" align="right">Bairro:</td>
                                        <td><input id="txtbairro_entrega" <?php echo $disabled; ?> name="txtbairro_entrega" type="text" class="input-medium" maxlength="100" value=""/></td>
                                        <td width="122" align="right">Estado:</td>
                                        <td><select class="input-medium" style="width:100%" <?php echo $disabled; ?> name="txtestado_entrega" id="txtestado_entrega" >
                                                <option value="null" >Selecione o estado</option>
                                                <?php
                                                $cur = odbc_exec($con, "select estado_codigo,estado_nome from tb_estados where estado_codigo > 0 order by estado_nome") or die(odbc_errormsg());
                                                while ($RFP = odbc_fetch_array($cur)) {
                                                    ?>
                                                    <option value="<?php echo $RFP['estado_codigo'] ?>"><?php echo utf8_encode($RFP['estado_nome']) ?></option>
                                                <?php } ?>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td width="122" align="right">Cidade:</td>
                                        <td><span id="carregando22" name="carregando22" style="color:#666;display:none">Aguarde, carregando...</span>
                                            <select name="txtcidade_entrega" id="txtcidade_entrega" class="input-medium" style="width:100%" <?php echo $disabled; ?> >
                                                <option value="null" >Selecione a cidade</option>
                                            </select>
                                        </td>
                                        <td width="122" align="right">CEP:</td>
                                        <td><input id="txtcep_entrega" <?php echo $disabled; ?> name="txtcep_entrega" type="text" class="input-medium" maxlength="100" value=""/></td>
                                    </tr>
                                    <tr>
                                        <td width="122" align="right">Telefone:</td>
                                        <td colspan="3"><input style="width:546px" id="txttelefone_entrega" <?php echo $disabled; ?> name="txttelefone_entrega" type="text" class="input-medium" maxlength="100" value=""/></td>
                                    </tr>
                                    <tr>
                                        <td width="122" align="right">Referencia:</td>
                                        <td colspan="3">
                                            <div style="float:left; width:465px">
                                                <input name="txtreferencia_entrega" id="txtreferencia_entrega" <?php echo $disabled; ?> type="text" class="input-medium"  maxlength="256" value=""/>
                                            </div>
                                            <div style="float:right">
                                                <button <?php
                                                if ($id == "") {
                                                    echo "disabled";
                                                }
                                                ?> class="btn btn-success" style="height:27px" <?php echo $disabled; ?> type="submit" name="bntSaveEnd">
                                                    <span class="ico-checkmark"></span> Gravar</button>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <div style="height:35px; width:100px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Nome</b></div>
                                    <div style="height:35px; width:517px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Endereço de Entrega</b></div>
                                    <div style="height:35px; width:51px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Ação</b></div>
                                </table>
                                <div style="overflow:scroll; height:94px;">
                                    <table width="100%">
                                        <?php
                                        if ($id != "") {
                                            $cur = odbc_exec($con, "select * from sf_fornecedores_despesas_endereco_entrega where fornecedores_despesas = " . $id);
                                            while ($RFP = odbc_fetch_array($cur)) {
                                                ?>
                                                <a href="#" onClick="EditEnd(<?php echo $RFP['id_endereco']; ?>)">
                                                    <div style="height:30px; width:105px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo utf8_decode($RFP['nome_end_entrega']); ?></div>
                                                    <div style="height:30px; width:522px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left">
                                                        <?php
                                                        if ($RFP['end_entrega'] != "") {
                                                            echo utf8_encode($RFP['end_entrega']);
                                                        }
                                                        if ($RFP['numero_entrega'] != "") {
                                                            echo " N° " . utf8_encode($RFP['numero_entrega']);
                                                        }
                                                        if ($RFP['complemento_entrega'] != "") {
                                                            echo " " . utf8_encode($RFP['complemento_entrega']);
                                                        }
                                                        if ($RFP['bairro_entrega'] != "") {
                                                            echo ", " . utf8_encode($RFP['bairro_entrega']);
                                                        }
                                                        if ($RFP['cidade_entrega'] != "") {
                                                            $sql = "select cidade_nome from tb_cidades where cidade_codigo = " . $RFP['cidade_entrega'];
                                                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                            while ($ASR = odbc_fetch_array($cur)) {
                                                                echo " - " . utf8_encode($ASR['cidade_nome']);
                                                            }
                                                        }
                                                        if ($RFP['estado_entrega'] != "") {
                                                            $sql = "select estado_sigla from tb_estados where estado_codigo = " . $RFP['estado_entrega'];
                                                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                            while ($ASR = odbc_fetch_array($cur)) {
                                                                echo "/" . utf8_encode($ASR['estado_sigla']);
                                                            }
                                                        }
                                                        if ($RFP['cep_entrega'] != "") {
                                                            echo ", Cep: " . utf8_encode($RFP['cep_entrega']);
                                                        }
                                                        ?></a>
                                        </div>
                                        <div style="height:23px; width:40px; background:#FFF; margin-left:1px; padding-top:7px; padding-left:3px; float:left">
                                            <a onClick="return confirm('Deseja deletar esse registro?')" href='FormGerenciador-Clientes.php?id = <?php echo $PegaURL;
                                                        ?>&Del_E=<?php echo $RFP['id_endereco']; ?>' ><img src="../../img/1365123843_onebit_33 copy.PNG" width='16' height='16'/></a>
                                        </div>
                                        <div style="clear:both"></div>
                                        <?php
                                    }
                                }
                                ?>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane <?php echo $active5; ?>" id="tab5">
                            <table width="680" border="0" cellpadding="3" class="textosComuns">
                                <tr>
                                    <td width="122" align="right"> Observação: </td>
                                    <td colspan="3"><input name="txtOBS" id="txtOBS" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="128" value=""/></td>
                                    <td width="100" align="right"><?php if ($id != '') { ?>
                                            <button class="btn btn-success" <?php echo $disabled; ?> type="submit" name="bntSaveMsg"><span class="ico-checkmark"></span> Gravar</button>
                                        <?php } ?></td>
                                </tr>
                            </table>
                            <table width="100%">
                                <div style="height:35px; width:160px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Data</b></div>
                                <div style="height:35px; width:390px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Descrição</b></div>
                                <div style="height:35px; width:58px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Usuário</b></div>
                                <div style="height:35px; width:51px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Ação</b></div>
                            </table>
                            <div style="overflow:scroll; height:305px;">
                                <table width="100%">
                                    <?php
                                    if ($id != '') {
                                        $cur = odbc_exec($con, "select * from sf_mensagens_fornecedores where id_fornecedores_despesas = " . $id . " order by data_send desc");
                                        while ($RFP = odbc_fetch_array($cur)) {
                                            ?>
                                            <div style="height:30px; width:165px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left">
                                                <?php echo escreverDataHora($RFP['data_send']); ?>
                                            </div>
                                            <div style="height:30px; width:395px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo utf8_encode($RFP['mensagem']); ?></div>
                                            <div style="height:30px; width:63px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo utf8_encode($RFP['user_send']); ?></div>
                                            <div style="height:23px; width:40px; background:#FFF; margin-left:1px; padding-top:7px; padding-left:3px; float:left">
                                                <a onClick="return confirm('Deseja deletar esse registro?')" href='FormGerenciador-Clientes.php?id=<?php echo $PegaURL; ?>&Del_M=<?php echo $RFP['id_mensagem']; ?>' ><img src="../../img/1365123843_onebit_33 copy.PNG" width='16' height='16'/></a>
                                            </div>
                                            <div style="clear:both"></div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab6" >
                            <table width="100%">
                                <div style="height:35px; width:60px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Pendente</b></div>
                                <div style="height:35px; width:90px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Data</b></div>
                                <div style="height:35px; width:95px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Atendente</b></div>
                                <div style="height:35px; width:95px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Para</b></div>
                                <div style="height:35px; width:60px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Próximo</b></div>
                                <div style="height:35px; width:241px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Observação</b></div>
                            </table>
                            <div style="overflow:scroll; height:338px;">
                                <table width="100%">
                                    <?php
                                    if ($id != '') {
                                        $cur = odbc_exec($con, "select id_tele,u.login_user as de_pessoa,u2.login_user as para_pessoa,data_tele,pendente,proximo_contato,obs
                                                                from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa
                                                                LEFT JOIN sf_usuarios u2 on u2.id_usuario = t.para_pessoa
                                                                where t.pessoa = " . $id . " order by data_tele desc");
                                        while ($RFP = odbc_fetch_array($cur)) {
                                            $backColor = "";
                                            if ($RFP['pendente'] == 1) {
                                                $backColor = " color:red;";
                                            } else {
                                                $backColor = "";
                                            }
                                            ?>
                                            <div style="height:30px; width:65px; background:#FFF; line-height:35px; margin-left:1px; padding-left:3px; float:left;<?php echo $backColor; ?>"><center><?php
                                                    if ($RFP['pendente'] == 0) {
                                                        echo utf8_encode("NÃO");
                                                    } else {
                                                        echo "SIM";
                                                    }
                                                    ?></center></div>
                                            <div style="height:30px; width:95px; background:#FFF; line-height:35px; margin-left:1px; padding-left:3px; float:left;<?php echo $backColor; ?>"><?php echo escreverData($RFP['data_tele']); ?></div>
                                            <div style="height:30px; width:100px; background:#FFF; line-height:35px; margin-left:1px; padding-left:3px; float:left;<?php echo $backColor; ?>"><?php echo utf8_encode($RFP['de_pessoa']); ?></div>
                                            <div style="height:30px; width:100px; background:#FFF; line-height:35px; margin-left:1px; padding-left:3px; float:left;<?php echo $backColor; ?>"><?php echo utf8_encode($RFP['para_pessoa']); ?></div>
                                            <div style="height:30px; width:65px; background:#FFF; line-height:35px; margin-left:1px; padding-left:3px; float:left;<?php echo $backColor; ?>"><?php echo escreverData($RFP['proximo_contato']); ?></div>
                                            <div style="height:30px; width:230px; background:#FFF; line-height:35px; margin-left:1px; padding-left:3px; float:left;<?php echo $backColor; ?>"><?php echo utf8_encode($RFP['obs']); ?></div>
                                            <div style="clear:both"></div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab7" style="height:380px">
                            <div style="margin-top:10px;height:360px;">                                
                                <table>
                                    <tr style="vertical-align:top">
                                        <td style="width: 20px"></td>
                                        <td style="width:320px">
                                            <table class="" border="1" bordercolor="#FFFFFF" cellpadding="3" cellspacing="2" style="overflow:auto;width: 300px;">
                                                <thead>
                                                    <tr class="txtPq">
                                                        <td colspan="2" bgcolor="#e9e9e9"><center>Total Contas Recebidas</center></td>
                                    </tr>
                                    </thead>
                                    <tbody class="txtPq">
                                        <tr>
                                            <td width="70%">Total contas recebidas</td>
                                            <td width="30%" style="text-align: right"><?php echo escreverNumero($q1_tc_recebidas, 1); ?></td>
                                        </tr>
                                        <tr>
                                            <td width="70%">Num. contas recebidas</td>
                                            <td width="30%" style="text-align: right"><?php echo $q1_nc_recebidas; ?></td>
                                        </tr>
                                        <tr>
                                            <td width="70%">Total contas a receber</td>
                                            <td width="30%" style="text-align: right"><?php echo escreverNumero($q1_tc_receber, 1); ?></td> </tr>
                                        <tr>
                                            <td width="70%">Num. contas a receber</td>
                                            <td width="30%" style="text-align: right"><?php echo $q1_nc_receber; ?></td>
                                        </tr>

                                        </tr>

                                    </tbody>
                                </table>
                                </td>
                                <td>
                                    <table class="" border="1" bordercolor="#FFFFFF" cellpadding="3" cellspacing="2" style="overflow:auto;width: 320px;">
                                        <thead>
                                            <tr class="txtPq">
                                                <td colspan="2" bgcolor="#e9e9e9"><center>Relatório Contas Recebidas</center></td>
                                </tr>
                                </thead>
                                <tbody class="txtPq">
                                    <tr>
                                        <td width="70%">Última conta recebida</td>
                                        <td width="30%" style="text-align: right"><?php echo escreverData($q2_ult_conta); ?></td>
                                    </tr>
                                    <tr>
                                        <td width="70%">Prim. conta recebida</td>
                                        <td width="30%" style="text-align: right"><?php echo escreverData($q2_pri_conta); ?></td>
                                    </tr>
                                    <tr>
                                        <td width="70%">Próximo venc.</td>
                                        <td width="30%" style="text-align: right"><?php echo escreverData($q2_pro_venc); ?></td>                                            
                                    </tr>
                                    <tr>
                                        <td width="70%">Último venc.</td>
                                        <td width="30%" style="text-align: right"><?php echo escreverData($q2_ult_venc); ?></td>                                           
                                    </tr>
                                    <tr>
                                        <td width="70%">Valor médio parc. recebida</td>
                                        <td width="30%" style="text-align: right"><?php echo escreverNumero($q2_med_recebido, 1); ?></td>
                                    </tr>
                                    <tr>
                                        <td width="70%">Valor médio parc. a receber</td>
                                        <td width="30%" style="text-align: right"><?php echo escreverNumero($q2_med_receber, 1); ?></td>
                                    <tr>
                                        <td>
                                        <td>
                                    <tr>
                                        <td>
                                        <td>
                                    <tr>
                                        <td>
                                        <td>
                                    </tr>
                                </tbody>
                                </table>
                                </td>
                                </tr>
                                <tr>
                                    <td style="width: 20px"></td>
                                    <td>
                                        <table class="" border="1" bordercolor="#FFFFFF" cellpadding="3" cellspacing="2" style="overflow:auto;width: 300px;">
                                            <thead>
                                                <tr class="txtPq">
                                                    <td colspan="2" bgcolor="#e9e9e9"><center>Contas Recebidas em Atraso</center></td>
                                </tr>
                                </thead>
                                <tbody class="txtPq">
                                    <tr>
                                        <td width="80%">Total contas recebidas em atraso</td>
                                        <td width="20%" style="text-align: right"><?php echo escreverNumero($q3_tot_atraso, 1); ?></td>
                                    </tr>
                                    <tr>
                                        <td width="80%">N. contas recebidas em atraso</td>
                                        <td width="20%" style="text-align: right"><?php echo $q3_qtd_atraso; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="80%">Num. médio de dias recebidas em atraso</td>
                                        <td width="20%" style="text-align: right"><?php echo $q3_med_atraso; ?></td>
                                    </tr>
                                </tbody>
                                </table>
                                </td>
                                <td>
                                    <table class="" border="1" bordercolor="#FFFFFF" cellpadding="3" cellspacing="2" style="overflow:auto;width: 320px;">
                                        <thead>
                                            <tr class="txtPq">
                                                <td colspan="2" bgcolor="#e9e9e9"><center>Contas Recebidas Antecipadas</center></td>
                                </tr>
                                </thead>
                                <tbody class="txtPq">
                                    <tr>
                                        <td width="80%">Total contas recebidas antecipadas</td>
                                        <td width="20%" style="text-align: right"><?php echo escreverNumero($q3_tot_antecipado, 1); ?></td>
                                    </tr>
                                    <tr>
                                        <td width="80%">Num. contas recebidas antecipadas</td>
                                        <td width="20%" style="text-align: right"><?php echo $q3_qtd_antecipado; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="80%">Média de dias contas recebidas antecipadas</td>
                                        <td width="20%" style="text-align: right"><?php echo $q3_med_antecipado; ?></td>
                                    </tr>
                                </tbody>
                                </table>
                                </td>
                                </tr>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane <?php echo $active8; ?>" id="tab8" >
                            <table width="680" border="0" cellpadding="3" class="textosComuns">
                                <tr>
                                    <td width="122" height="37" align="right"> Envio de Arquivos: </td>
                                    <td colspan="3"><input <?php echo $disabled; ?>  type="file" name="file" id="file" style="font-size:11px; height:23px; margin-top:4px"/></td>
                                    <td width="100" align="right"><?php if ($id != '') { ?>
                                            <button class="btn btn-success" <?php echo $disabled; ?> type="submit" name="bntSendFile"><span class="ico-checkmark"></span> Enviar</button>
                                        <?php } ?></td>
                                </tr>
                            </table>
                            <div style="height:35px; width:160px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Data de Upload</b></div>
                            <div style="height:35px; width:370px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Nome do Arquivo</b></div>
                            <div style="height:35px; width:78px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Tamanho</b></div>
                            <div style="height:35px; width:51px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Ação</b></div>
                            <div style="clear:both"></div>
                            <div style="overflow:scroll; height:301px;">
                                <?php
                                if ($id != "") {
                                    $pasta = "./../../Pessoas/" . $contrato . "/Documentos/" . $id . "/";
                                    if (file_exists($pasta)) {
                                        $diretorio = dir($pasta);
                                        while ($arquivo = $diretorio->read()) {
                                            if ($arquivo != "." && $arquivo != "..") {
                                                ?>
                                                <div style="height:30px; width:165px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo date($lang['dmy'] . " H:i:s", filectime($pasta . $arquivo)); ?></div>
                                                <div style="height:30px; width:375px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo "<a href=\"" . $pasta . $arquivo . "\" target=\"_blank\">" . $arquivo; ?></a></div>
                                                <div style="height:30px; width:83px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo filesize($pasta . $arquivo) . " bytes"; ?></div>
                                                <div style="height:23px; width:40px; background:#FFF; margin-left:1px; padding-top:7px; padding-left:3px; float:left">
                                                    <a onClick="return confirm('Deseja deletar esse registro?')" href="FormGerenciador-Clientes.php?id=<?php echo $PegaURL; ?>&Del_F=<?php echo $arquivo; ?>"><img src="../../img/1365123843_onebit_33 copy.PNG" width="16" height="16"/></a>
                                                </div>
                                                <div style="clear:both"></div>
                                                <?php
                                            }
                                        }
                                        $diretorio->close();
                                    } else {
                                        mkdir($pasta, 0777);
                                    }
                                }?>
                            </div>
                        </div>
                    </div>
                    <div class="spanBNT" style="width:739px; border-top:solid 1px #CCC; overflow:hidden; margin:0; padding:0; bottom:1px; position:absolute;">
                        <div class="toolbar bottom tar">
                            <div class="data-fluid" style="overflow:hidden; margin-top:10px; margin-right:10px;" >
                                <div class="btn-group" style="overflow:hidden;">
                                    <?php if ($disabled == '') { ?>
                                        <button class="btn btn-success" type="submit" name="bntSave" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                                        <?php if ($_POST['txtId'] == '') { ?>
                                            <button class="btn btn-success" onClick="<?php
                                            if ($FinalUrl != "") {
                                                echo "parent.FecharBox(3)";
                                            } else {
                                                echo "parent.FecharBox(0)";
                                            }
                                            ?>" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                                                <?php } else { ?>
                                            <button class="btn btn-success" type="submit" name="bntAlterar" id="bntOK" ><span class="ico-reply"></span> Cancelar</button>
                                            <?php
                                        }
                                    } else {
                                        if ($ckb_adm_cli_read_ == 0) {
                                            ?>
                                            <button class="btn  btn-success" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                                            <button class="btn  btn-success" type="submit" name="bntEdit" id="bntOK" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                                            <button class="btn red" type="submit" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </form>
        </div>
        <div class="block"></div>
    </div>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/charts.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
    <script type="text/javascript" src="../../../SpryAssets/SpryValidationSelect.js"></script>    
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type="text/javascript">

        $("#txtDataNascimento").mask(lang["dateMask"]);

        var idLinhaContato = $("#txtQtdContatos").val();
        ListMensagens();
        function alterarMask() {
            $('#txtTextoContato').val("");
            var tipo = $("#txtTipoContato option:selected").val();
            if (sprytextfield2 !== null) {
                sprytextfield2.reset();
                sprytextfield2.destroy();
                sprytextfield2 = null;
            }
            if (tipo === "0") {
                sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "custom", {pattern: "(00) 0000-0000", useCharacterMasking: true, isRequired: false});
            } else if (tipo === "1") {
                sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "custom", {pattern: "(00) 00000-0000", useCharacterMasking: true, isRequired: false});
            }
        }

        function legenda(tipo) {
            if (tipo === "0") {
                return "Telefone";
            } else if (tipo === "1") {
                return "Celular";
            } else if (tipo === "2") {
                return "E-mail";
            } else if (tipo === "3") {
                return "Site";
            } else if (tipo === "4") {
                return "Outros";
            } else {
                return "";
            }
        }

        function ListMensagens() {
            var conteudo = "";
            $("#divMensagens").html('');
            $.getJSON('FormGerenciador-Clientes.contatos.ajax.php?search=', {cod: $("#txtId").val() !== "" ? $("#txtId").val() : "0", ajax: 'true'}, function (j) {
                for (var i = 0; i < j.length; i++) {
                    conteudo = '<div style="padding:5px; border-bottom:1px solid #DDD">';
                    conteudo += '<div style="width:29%; padding-left:1%; line-height:27px; float:left">' + legenda(j[i].tipo_contato) + '</div>';
                    conteudo += '<div style="width:65%; line-height:27px; float:left">' + j[i].conteudo_contato + '</div>';
                    conteudo += '<div style="width:5%; padding:4px 0 3px; float:right" onClick="return delMensagens(' + j[i].id + ');"><span class="ico-remove" style="font-size:20px"></span></div></div>';
                    $("#divMensagens").prepend(conteudo);
                }
            });
        }

        function addMensagens() {
            var msg = $("#textoArea").val();
            var sel = $("#txtDeHistorico").val();
            if (msg.length > 0) {
                var arb = msg + "@";
                if (sel !== 2 || (sel === 2 && arb.match(/@/g).length < 3 && arb.match(/@/g).length > 1)) {
                    $.post("FormGerenciador-Clientes.controler.ajax.php", {id: $("#txtId").val() !== "" ? $("#txtId").val() : "0", tipo: sel, txtMsg: msg, acao: 0}).done(function (data) {
                        $("#textoArea").val("");
                        ListMensagens();
                    });
                } else {
                    alert("Preencha um endereço de e-mail corretamente!");
                }
            } else {
                alert("Preencha as informações de mensagens corretamente!");
            }
        }

        function delMensagens(numero) {
            if (confirm('Confirma a exclusão desta mensagem?')) {
                $.post("FormGerenciador-Clientes.controler.ajax.php", {id: numero, acao: 2}).done(function (data) {
                    ListMensagens();
                });
            }
        }

        function addContato() {
            var msg = $("#txtTextoContato").val();
            var sel = $("#txtTipoContato").val();
            if (msg !== "") {
                var arb = msg + "@";
                if (sel !== 2 || (sel === 2 && arb.match(/@/g).length === 2)) {
                    addLinhaContato($("#txtTipoContato").val(), $("#txtTipoContato option:selected").text(), $("#txtTextoContato").val(), "0");
                } else {
                    bootbox.alert("Preencha um endereço de e-mail corretamente!");
                }
            } else {
                bootbox.alert("Preencha as informações de contato corretamente!");
            }
        }
        function addLinhaContato(idTipo, tipo, conteudo, id) {
            var btExcluir = "";
            if ($("#txtFantasia").attr("disabled") === "disabled") {
                btExcluir = '<div style="width:5%; padding:4px 0 3px; float:right"></div>';
            } else {
                btExcluir = '<div style="width:5%; padding:4px 0 3px; float:right" onClick="return removeLinha(\'tabela_linha_' + idLinhaContato + '\');"><span class="ico-remove" style="font-size:20px"></span></div>';
            }
            var linha = "<div id=\"tabela_linha_" + idLinhaContato + "\" style=\"padding:5px; border-bottom:1px solid #DDD\">" +
                    "<input id=\"txtIdContato_" + idLinhaContato + "\" name=\"txtIdContato_" + idLinhaContato + "\" value=\"" + id + "\" type=\"hidden\"></input>" +
                    "<input id=\"txtTpContato_" + idLinhaContato + "\" name=\"txtTpContato_" + idLinhaContato + "\" value=\"" + idTipo + "\" type=\"hidden\"></input>" +
                    "<div style=\"width:32%; padding-left:1%; line-height:27px; float:left\">" + tipo + "</div>" +
                    "<input id=\"txtTxContato_" + idLinhaContato + "\" name=\"txtTxContato_" + idLinhaContato + "\" value=\"" + conteudo + "\" type=\"hidden\"></input>" +
                    btExcluir + "<div style=\"width:60%; line-height:27px; float:left\">" + conteudo + "</div></div>";
            $("#divContatos").prepend(linha);
            idLinhaContato++;
            $("#txtQtdContatos").val(idLinhaContato);
        }

        function removeLinha(id) {
            $("#" + id).remove();
        }

        $(document).ready(function () {
            listContatos();
        });
        function listContatos() {
            $.getJSON('../Comercial/FormGerenciador-Clientes.contatos.ajax.php?search=', {cod: $("#txtId").val() !== "" ? $("#txtId").val() : "0", ajax: 'true'}, function (j) {
                for (var i = 0; i < j.length; i++) {
                    addLinhaContato(j[i].tipo_contato, j[i].texto_contatos, j[i].conteudo_contato, j[i].id);
                }
            });
        }

        function Validar(theCPF)
        {
            var original = theCPF.value.replace(".", "").replace("-", "").replace("/", "").replace(".", "");
            if (original === "")
            {
                return (true);
            }
            if (((original.length === 11) && (original === 11111111111) || (original === 22222222222) || (original === 33333333333) || (original === 44444444444) || (original === 55555555555) || (original === 66666666666) || (original === 77777777777) || (original === 88888888888) || (original === 99999999999) || (original === 00000000000)))
            {
                alert("CPF/CNPJ inválido.");
                theCPF.focus();
                return (false);
            }
            if (!((original.length === 11) || (original.length === 14)))
            {
                alert("CPF/CNPJ inválido.");
                theCPF.focus();
                return (false);
            }
            var checkOK = "0123456789";
            var checkStr = original;
            var allValid = true;
            var allNum = "";
            for (i = 0; i < checkStr.length; i++)
            {
                ch = checkStr.charAt(i);
                for (j = 0; j < checkOK.length; j++)
                    if (ch === checkOK.charAt(j))
                        break;
                if (j === checkOK.length)
                {
                    allValid = false;
                    break;
                }
                allNum += ch;
            }
            if (!allValid)
            {
                alert("Favor preencher somente com dígitos o campo CPF/CNPJ.");
                theCPF.focus();
                return (false);
            }

            var chkVal = allNum;
            var prsVal = parseFloat(allNum);
            if (chkVal !== "" && !(prsVal > "0"))
            {
                alert("CPF zerado !");
                theCPF.focus();
                return (false);
            }

            if (original.length === 11)
            {
                var tot = 0;
                for (i = 2; i <= 10; i++)
                    tot += i * parseInt(checkStr.charAt(10 - i));
                if ((tot * 10 % 11 % 10) !== parseInt(checkStr.charAt(9)))
                {
                    alert("CPF/CNPJ inválido.");
                    theCPF.focus();
                    return (false);
                }
                tot = 0;
                for (i = 2; i <= 11; i++)
                    tot += i * parseInt(checkStr.charAt(11 - i));
                if ((tot * 10 % 11 % 10) !== parseInt(checkStr.charAt(10)))
                {
                    alert("CPF/CNPJ inválido.");
                    theCPF.focus();
                    return (false);
                }
            } else {
                var tot = 0;
                var peso = 2;
                for (i = 0; i <= 11; i++)
                {
                    tot += peso * parseInt(checkStr.charAt(11 - i));
                    peso++;
                    if (peso === 10)
                    {
                        peso = 2;
                    }
                }
                if ((tot * 10 % 11 % 10) !== parseInt(checkStr.charAt(12)))
                {
                    alert("CPF/CNPJ inválido.");
                    theCPF.focus();
                    return (false);
                }
                tot = 0;
                peso = 2;
                for (i = 0; i <= 12; i++)
                {
                    tot += peso * parseInt(checkStr.charAt(12 - i));
                    peso++;
                    if (peso === 10)
                    {
                        peso = 2;
                    }
                }
                if ((tot * 10 % 11 % 10) !== parseInt(checkStr.charAt(13)))
                {
                    alert("CPF/CNPJ inválido.");
                    theCPF.focus();
                    return (false);
                }
            }
            if (theCPF !== "") {
                $.getJSON("cpf_cnpj.ajax.php", {txtCpf: $("#txtCnpj").val(), ajax: "true"}, function (j) {
                    if (j[0].existe > 0) {
                        $("#txtCnpj").css("background-color", "orange");
                    } else {
                        $("#txtCnpj").css("background-color", "white");
                    }
                });
            }
            return(true);
        }

        function mascaraMutuario(o, f) {
            v_obj = o;
            v_fun = f;
            setTimeout('execmascara()', 1);
        }

        function execmascara() {
            v_obj.value = v_fun(v_obj.value);
        }

        function cpfCnpj(v) {
            v = v.replace(/\D/g, "");
            if (v.length <= 11) { //CPF
                v = v.replace(/(\d{3})(\d)/, "$1.$2");
                v = v.replace(/(\d{3})(\d)/, "$1.$2");
                v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
            } else { //CNPJ
                v = v.replace(/^(\d{2})(\d)/, "$1.$2");
                v = v.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
                v = v.replace(/\.(\d{3})(\d)/, ".$1/$2");
                v = v.replace(/(\d{4})(\d)/, "$1-$2");
            }
            return v;
        }

        $(function () {
            $('#txtEstado').change(function () {
                if ($(this).val()) {
                    $('#txtCidade').hide();
                    $('#carregando').show();
                    $.getJSON('locais.ajax.php?search=', {txtEstado: $(this).val(), ajax: 'true'}, function (j) {
                        var options = "<option value=\"\"></option>";
                        for (var i = 0; i < j.length; i++) {
                            options += "<option value=\"" + j[i].cidade_codigo + "\">" + j[i].cidade_nome + "</option>";
                        }
                        $('#txtCidade').html(options).show();
                        $('#carregando').hide();
                    });
                } else {
                    $('#txtCidade').html('<option value="">Selecione a cidade</option>');
                }
            });
        });

        function EditEnd(MyEndId) {
            $.getJSON("../NFE/endereco.ajax.php?search=", {txtFonecedor: MyEndId, txtTabela: "entrega", ajax: "true"}, function (j) {
                for (var i = 0; i < j.length; i++) {
                    if (MyEndId !== "") {
                        $("#txtEditEnd").val(MyEndId);
                    } else {
                        $("#txtEditEnd").val("");
                    }
                    if (j[i].nome !== "") {
                        $("#txtnome_end_entrega").val(j[i].nome);
                    } else {
                        $("#txtnome_end_entrega").val("");
                    }
                    if (j[i].endereco !== "") {
                        $("#txtend_entrega").val(j[i].endereco);
                    } else {
                        $("#txtend_entrega").val("");
                    }
                    if (j[i].numero !== "") {
                        $("#txtnumero_entrega").val(j[i].numero);
                    } else {
                        $("#txtnumero_entrega").val("");
                    }
                    if (j[i].complemento !== "") {
                        $("#txtcomplemento_entrega").val(j[i].complemento);
                    } else {
                        $("#txtcomplemento_entrega").val("");
                    }
                    if (j[i].cep !== "") {
                        $("#txtcep_entrega").val(j[i].cep);
                    } else {
                        $("#txtcep_entrega").val("");
                    }
                    if (j[i].bairro !== "") {
                        $("#txtbairro_entrega").val(j[i].bairro);
                    } else {
                        $("#txtbairro_entrega").val("");
                    }
                    if (j[i].telefone !== "") {
                        $("#txttelefone_entrega").val(j[i].telefone);
                    } else {
                        $("#txttelefone_entrega").val("");
                    }
                    if (j[i].referencia !== "") {
                        $("#txtreferencia_entrega").val(j[i].referencia);
                    } else {
                        $("#txtreferencia_entrega").val("");
                    }
                    if (j[i].estado !== "") {
                        $("#txtestado_entrega").val(j[i].estado);
                        $("#s2id_txtestado_entrega > .select2-choice > span").html(j[i].estado_nome);
                    } else {
                        $("#txtestado_entrega").val("");
                        $("#s2id_txtestado_entrega > .select2-choice > span").html("Selecione o estado");
                    }
                    cid_cod = j[i].cidade;
                    cid_nome = j[i].cidade_nome;
                    $.getJSON("locais.ajax.php?search=", {txtEstado: $("#txtestado_entrega").val(), ajax: "true"}, function (j) {
                        var options = "<option value=\"\"></option>";
                        for (var i = 0; i < j.length; i++) {
                            options += "<option value=\"" + j[i].cidade_codigo + "\">" + j[i].cidade_nome + "</option>";
                        }
                        $("#txtcidade_entrega").html(options).show();
                    }).done(function () {
                        if (cid_cod !== "") {
                            $("#txtcidade_entrega").val(cid_cod);
                            $("#s2id_txtcidade_entrega > .select2-choice > span").html(cid_nome);
                        } else {
                            $("#txtcidade_entrega").val("");
                            $("#s2id_txtcidade_entrega > .select2-choice > span").html("Selecione a cidade");
                        }
                    });
                }
            });
        }
        $(function () {
            $('#txtestado_entrega').change(function () {
                if ($(this).val()) {
                    $('#txtcidade_entrega').hide();
                    $('#carregando22').show();
                    $.getJSON('locais.ajax.php?search=', {txtEstado: $(this).val(), ajax: 'true'}, function (j) {
                        var options = '<option value=""></option>';
                        for (var i = 0; i < j.length; i++) {
                            options += "<option value=\"" + j[i].cidade_codigo + "\">" + j[i].cidade_nome + "</option>";
                        }
                        $('#txtcidade_entrega').html(options).show();
                        $('#carregando22').hide();
                    });
                } else {
                    $('#txtcidade_entrega').html('<option value="">Selecione a cidade</option>');
                }
            });
        });
        var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
        var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "custom", {pattern: "(00) 0000-0000", useCharacterMasking: true, isRequired: false});
        var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3", "custom", {pattern: "00000-000", useCharacterMasking: true, isRequired: false});
        var sprytextfield5 = new Spry.Widget.ValidationTextField("txtcep_entrega", "custom", {pattern: "00000-000", useCharacterMasking: true, isRequired: false});
    </script>
</div>
<?php odbc_close($con); ?>
</body>
