<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include "../../Connections/configini.php";
$tipo = $_REQUEST['txtTipo'];
$tipos = array();

if ($tipo == 0) {
    $sql = "select * from sf_agenda where inativo = 0 
    and ((user_resp is null or user_resp = " . $_SESSION["id_usuario"] . ") or (user_permissao is null or user_permissao in (select isnull(master, 0) from sf_usuarios where id_usuario = " . $_SESSION["id_usuario"] . ")))
    order by descricao_agenda";
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $tipos[] = array('id' => $RFP['id_agenda'],
            'descricao' => utf8_encode($RFP['descricao_agenda']));
    }
} elseif ($tipo == 1) {
    $sql = "select id_turma,descricao from sf_turmas where inativo = 0 order by descricao";
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $tipos[] = array('id' => $RFP['id_turma'],
            'descricao' => utf8_encode($RFP['descricao']));
    }
}

echo(json_encode($tipos));
odbc_close($con);
