<?php

/*if ($_GET["PathArq"] != 'ContratoAcademia') {
    include_once(__DIR__ . './../../../Connections/configini.php');  
}*/
include "variaveis.php";

$id = 0;
$i = 0;
$numero_filial = "001";

if (is_numeric($_GET["id"])) {
    $id = $_GET["id"];
}

function prepareText($text) {
    $br = "<div class=\"separador\"></div>";
    $par = "<span class=\"space\">PPPPPPP</span>";
    $pon = "<span class=\"space\">.</span>";
    $toReturn = substr(substr_replace($text, "", -6), 3);
    return str_replace(array("<p>&nbsp;", "<p>", "</p>", "<li>", "</li>", "<ul>", "</ul>", "</strong>"), array($pon, "", $br, $par, $br, "", "", " </strong>"), $toReturn);
}

function prepareTable($text) {
    $toReturn = explode("<tbody>", $text);
    return $toReturn[1];
}

function textAlign($type) {
    switch ($type) {
        case 0:
            return "justify";
        case 1:
            return "center";
        case 2:
            return "left";
        case 3:
            return "right";
    }
}

function MontaTexto($texto, $dicionario) {
    for ($i = 0; $i < count($dicionario); $i++) {
        $texto = str_replace("||" . $dicionario[$i][0] . "||", $dicionario[$i][1], $texto);
    }
    return $texto;
}

function MakeItem($tipo, $conteudo, $fonte_size, $align, $margin, $i, $dicionario) {
    $conteudo = MontaTexto($conteudo, $dicionario);
    if ($tipo == 1) {
        return "<span style=\"font-size:" . $fonte_size . "pt;text-align:" . $align . ";\">" . prepareText($conteudo) . "</span><div class=\"separador\" style=\"line-height:" . $margin . "px;\"></div>";
    } else if ($tipo == 2 || $tipo == 3) {
        $border = $tipo == 3 ? "0" : "1";
        $conteudo = "<table style=\"font-size:" . $fonte_size . "pt;width: 100%;\" border=\"+$border+\"><tbody>" . prepareTable($conteudo);
        return processRows($conteudo, $dicionario) . "<div class=\"separador space\" style=\"line-height:" . $margin . "px;\">&nbsp;</div>";        
    }
    return "";
}

function str_replace_first($from, $to, $content) {
    $from = '/' . preg_quote($from, '/') . '/';
    return preg_replace($from, $to, $content, 1);
}

function processRows($content, $dicTotal) {
    $lines = array();
    $dicionario = array();     
    for ($i = 91; $i <= 95; $i++) {
        $dicionario[] = $dicTotal[$i];
    }        
    for ($i = 229; $i <= 231; $i++) {
        $dicionario[] = $dicTotal[$i];
    }    
    $toArray = explode("<tbody>", $content);
    $toProcess = explode("</tbody>", $toArray[1]);
    $begin = $toArray[0] . "<tbody>";
    $end = "</tbody>" . $toProcess[1];
    $toProcessLine = explode("<tr", $toProcess[0]);
    for ($i = 1; $i < count($toProcessLine); $i++) {
        $lines[] = "<tr" . $toProcessLine[$i];
    }
    $toReturn = $begin;
    for ($i = 0; $i < count($lines); $i++) {
        $toReturn .= processDynamicRows($lines[$i], $dicionario);
    }
    return $toReturn . $end;
}

function processDynamicRows($content, $arrayDic) {
    $return = dynamicRows($content, $arrayDic);
    if ($return[0] && $return[1] > 0) {
        $content = str_replace(array("|NT|", "|NP|", "|NA|", "|NV|", "|AR|"), "||", $content);
        $arrayDicFilter = filterArrayDic($arrayDic, $return[2]);
        $toReturn = "";
        $total = ceil((count($arrayDicFilter[$return[3]][1]) / $return[1]));
        for ($i = 0; $i < $total; $i++) { //$i -> posição do array de valores
            $value = $content;
            for ($j = 0; $j < count($arrayDicFilter); $j++) {
                for ($k = 0; $k < $return[1]; $k++) {
                    $toRep = $arrayDicFilter[$j][1];
                    $value = str_replace_first("||" . $arrayDicFilter[$j][0] . "||", $toRep[$i + ($k * $total)], $value);
                }
            }
            $toReturn .= $value;
        }
        return $toReturn;
    } else {
        return $content;
    }
}

function dynamicRows($content, $arrayDic) {
    $tipo = "";
    $rep = 0;
    $line = 0;
    for ($i = 0; $i < count($arrayDic); $i++) {
        if (strpos($content, "|" . $arrayDic[$i][0] . "|") && substr_count($content, "|" . $arrayDic[$i][0] . "|") > $rep) {
            $rep = substr_count($content, "|" . $arrayDic[$i][0] . "|");
            $parte = explode("||" . $arrayDic[$i][0] . "|", $content);
            $tipo = substr($parte[1], 0, 2);
            $line = $i;
        }
    }
    return array(($rep > 0 ? true : false), $rep, $tipo, $line);
}

function filterArrayDic($arrayDic, $type) {
    if ($type == "NP" || $type == "NA" || $type == "NV") {
        $arrayTeste = $arrayDic[4][1];
        for ($i = 0; $i < count($arrayDic); $i++) {
            $arrayFilter = array();
            $arrayReplace = $arrayDic[$i][1];
            for ($j = 0; $j < count($arrayReplace); $j++) {
                if (($type == "NP" && $arrayTeste[$j] != "") || ($type == "NA" && $arrayTeste[$j] == "") || ($type == "NV" && $arrayTeste[$j] == "")) {
                    $arrayFilter[] = $arrayReplace[$j];
                }
            }
            $arrayDic[$i][1] = $arrayFilter;
        }
    }
    return $arrayDic;
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
            body {
                margin:0px;
                padding:0px;
                line-height: 0;
            }
            span, table {
                line-height:1.5;
            }
            .separador {
                line-height: 0px;
            }
            .space {
                color: white;
            }
        </style>
    </head>
    <body lang=PT-BR link=blue>
        <font face="courier">
        <div class="separador"></div>  
        <?php if ((!isset($_GET['logo']) || $_GET['logo'] == "S") && file_exists(dirname(__FILE__) . "./../../../Pessoas/" . $_SESSION["contrato"] . "/Empresa/logo_" . $numero_filial . ".jpg")) { ?>        
            <table width="100%">
                <tr>
                    <td style="width:100%;vertical-align: top;">                        
                        <img src = "<?php echo dirname(__FILE__); ?>./../../../Pessoas/<?php echo $_SESSION["contrato"]; ?>/Empresa/logo_<?php echo $numero_filial; ?>.jpg" height="45" />
                    </td>
                </tr>
            </table>            
        <?php } else if ((!isset($_GET['logo']) || $_GET['logo'] == "S") && file_exists(dirname(__FILE__) . "./../../../Pessoas/" . $_SESSION["contrato"] . "/Empresa/logo_" . $numero_filial . ".png")) { ?>        
            <table width="100%">
                <tr>
                    <td style="width:100%;vertical-align: top;">                        
                        <img src = "<?php echo dirname(__FILE__); ?>./../../../Pessoas/<?php echo $_SESSION["contrato"]; ?>/Empresa/logo_<?php echo $numero_filial; ?>.png" height="45" />
                    </td>
                </tr>
            </table>            
        <?php } ?>      
        <div class="separador"></div>        
        <?php
        $query =  "select *, substring(conteudo,1,4000) conteudo1, substring(conteudo,4001,4000) conteudo2, substring(conteudo,8001,4000) conteudo3, substring(conteudo,12001,4000) conteudo4, substring(conteudo,16001,4000) conteudo5 from sf_contratos_item where id_contrato = " . $id . " order by ordem";
        $cur = odbc_exec($con,$query);
        while ($RFP = odbc_fetch_array($cur)) {
            echo MakeItem($RFP["tipo"], utf8_encode($RFP["conteudo1"] . $RFP["conteudo2"] . $RFP["conteudo3"] . $RFP["conteudo4"] . $RFP["conteudo5"]), $RFP["fonte_size"], textAlign($RFP["align"]), $RFP["margin"], $i, $dicionario);
            $i++;
        }
        ?>
        </font>
    </body>        
</html>