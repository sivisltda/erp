<?php

$id_plan = 0;
$id_cliente = 0;
$id_agendamento = 0;
$id_venda_item = 0;
$id_evento = 0;
$id_vistoria = 0;
$id_boleto_on = 0;
$id_get = "";
$mundi_id = "";
    
if (isset($_GET['model']) && isset($_GET['exemple'])) {
    $query = "select top 1 vp.id_plano, vp.favorecido from dbo.sf_contratos c
    inner join sf_contratos_grupos cg on cg.id_contrato = c.id
    inner join sf_produtos p on cg.id_grupo = p.conta_movimento
    left join sf_vendas_planos vp on p.conta_produto = vp.id_prod_plano
    where id = " . $_GET["id"] . " order by id_plano desc";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $id_plan = $RFP['id_plano'];
        $id_cliente = $RFP['favorecido'];
    }
} else if (isset($_GET['model']) && $_GET['model'] == "A") {
    $query = "select id_agendamento, id_fornecedores from sf_agendamento where id_agendamento = " . $_GET["id_plan"] . " order by id_agendamento desc";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $_GET["id"] = $_GET["idContrato"];        
        $id_agendamento = $RFP['id_agendamento'];
        $id_cliente = $RFP['id_fornecedores'];
    }
} else if (isset($_GET['id_mensalidade'])) {
    $cur = odbc_exec($con, "select id_plano_mens, favorecido from sf_vendas_planos_mensalidade inner join sf_vendas_planos on id_plano_mens = sf_vendas_planos.id_plano where id_mens = " . $_GET['id_mensalidade']);
    while ($RFP = odbc_fetch_array($cur)) {
        $id_plan = $RFP['id_plano_mens'];
        $id_cliente = $RFP['favorecido'];
    }    
} else if (isset($_GET['id_contrato'])) {    
    $cur = odbc_exec($con, "select id_parcela, fornecedor_despesa, bol_nosso_numero from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento where id_parcela = " . $_GET['id_contrato']);
    while ($RFP = odbc_fetch_array($cur)) {
        $id_cliente = $RFP['fornecedor_despesa'];
        $id_get = (strlen($RFP['bol_nosso_numero']) > 0 ? "D-" . $RFP['id_parcela'] : "");
    }    
} else if (isset($_GET['id_variavel'])) {
    $cur = odbc_exec($con, "select id_parcela, fornecedor_despesa, bol_nosso_numero from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao where id_parcela = " . $_GET['id_variavel']);
    while ($RFP = odbc_fetch_array($cur)) {
        $id_cliente = $RFP['fornecedor_despesa'];
        $id_get = (strlen($RFP['bol_nosso_numero']) > 0 ? "A-" . $RFP['id_parcela'] : "");
    }    
} else if (isset($_GET['id_venda'])) {    
    $cur = odbc_exec($con, "select id_parcela, cliente_venda, bol_nosso_numero from sf_venda_parcelas inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda where id_parcela = " . $_GET['id_venda']);
    while ($RFP = odbc_fetch_array($cur)) {
        $id_cliente = $RFP['cliente_venda'];            
        $id_get = (strlen($RFP['bol_nosso_numero']) > 0 ? "V-" . $RFP['id_parcela'] : "");
    }
} else if (isset($_GET['id_evento'])) { 
    $query = "select pessoa, p.id_plano, t.id_tele from sf_telemarketing t inner join sf_fornecedores_despesas_veiculo v on t.id_veiculo = v.id
    left join sf_vendas_planos p on p.id_veiculo = v.id where id_tele = " . $_GET["id_evento"];
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {    
        $id_plan = $RFP['id_plano'];
        $id_cliente = $RFP['pessoa'];
        $id_evento = $RFP['id_tele'];
    }
} else if (isset($_GET['id_vistoria'])) { 
    $query = "select top 1 vi.id id_vistoria, ve.id_fornecedores_despesas pessoa, p.id_plano
    from sf_vistorias vi inner join sf_fornecedores_despesas_veiculo ve on vi.id_veiculo = ve.id
    left join sf_vendas_planos p on p.id_veiculo = ve.id
    where vi.id = " . $_GET["id_vistoria"] . " order by id_plano desc";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {    
        $id_plan = $RFP['id_plano'];
        $id_cliente = $RFP['pessoa'];
        $id_vistoria = $RFP['id_vistoria'];
    }
} else if (isset($_GET['id_pessoa'])) {
    $query = "select isnull(id_plano,0) id_plano, id_fornecedores_despesas from sf_fornecedores_despesas
    left join sf_vendas_planos on favorecido = id_fornecedores_despesas and dt_cancelamento is null
    where id_fornecedores_despesas = " . $_GET["id_pessoa"] . " order by id_plano desc";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $id_plan = $RFP['id_plano'];
        $id_cliente = $RFP['id_fornecedores_despesas'];
    }
} else if (isset($_GET['id_plano'])) {
    $query = "select id_plano, favorecido from sf_vendas_planos where id_plano = " . $_GET["id_plano"] . " order by id_plano desc";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $id_plan = $RFP['id_plano'];
        $id_cliente = $RFP['favorecido'];
    }
} else if (isset($_GET['id_boleto_on'])) {
    $query = "select id_boleto, id_pessoa from sf_boleto_online where id_boleto = " . $_GET["id_boleto_on"] . " and inativo = 0";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $id_boleto_on = $RFP['id_boleto'];
        $id_cliente = $RFP['id_pessoa'];
    }
} else if (isset($_GET["id_plan"])) {
    $query = "select cnt.id, vp.favorecido, id_plano 
              from sf_vendas_planos vp left join sf_produtos p on vp.id_prod_plano = p.conta_produto
              inner join sf_contratos_grupos cg on cg.id_grupo = p.conta_movimento
              inner join sf_contratos cnt on cg.id_contrato = cnt.id
              inner join sf_contratos_filiais cnf on cnf.id_contrato = cnt.id
              inner join sf_fornecedores_despesas f on vp.favorecido = id_fornecedores_despesas              
              where cnt.inativo = 0 and cnf.id_filial = f.empresa 
              and (vp.dt_cancelamento is null and cnt.tipo = 0 or vp.dt_cancelamento is not null and cnt.tipo = 1)
              and id_plano in (" . $_GET["id_plan"] . ") " . (isset($_GET['idContrato']) ? " and cnt.id = " . $_GET['idContrato'] : "");
    //echo $query;exit();
    $cur = odbc_exec($con, $query);
    $total = odbc_num_rows($cur);
    if ($total == 0) {
        $query = "select top 1 id from sf_contratos where id not in (select id_contrato from sf_contratos_grupos) and inativo = 0". (isset($_GET['idContrato']) ? " and cnt.id = " . $_GET['idContrato'] : "");
        $cur = odbc_exec($con, $query);
        while ($RFP = odbc_fetch_array($cur)) {
            $_GET["id"] = $RFP['id'];
        }
    } else {
        while ($RFP = odbc_fetch_array($cur)) {
            $_GET["id"] = $RFP['id'];
            $id_cliente = $RFP['favorecido'];
            $id_plan = $RFP['id_plano'];
        }
    }
    if ($_GET["id"] == '') {
        echo "Não existe contrato cadastrado para essa modalidade!";
    }
}

$dicionario = array();
// x = 0 and x = 59
$query = "select top 1 C.id_fornecedores_despesas id_cli,C.razao_social Nm_cli, C.cnpj cpf_cli, C.inscricao_estadual rg_cli, 
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas = C.id_fornecedores_despesas) 'telefone_cli',
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = C.id_fornecedores_despesas) 'celular_cli',
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = C.id_fornecedores_despesas) 'email_cli',
C.dt_cadastro dt_cadastro_cli,C.observacao obs_cli, C.data_nascimento dt_nasc_cli, C.sexo sexo_cli, C.tipo_sang tipo_sang_cli, CG.descricao_grupo grupo_cli,
C.estado_civil estado_civil_cli,C.profissao profissao_cli, 
C.cep cep_cli, C.endereco end_cli, C.numero end_num_cli, 
C.complemento compl_end, C.bairro bairro_cli, 
D.cidade_nome cidade_cli, E.estado_sigla uf_cli,
AR.id_fornecedores_despesas id_resp_legal, AR.razao_social nm_resp_legal, AR.cnpj cpf_resp_legal, AR.inscricao_estadual rg_resp_legal, 
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas = AR.id_fornecedores_despesas) 'telefone_resp_legal',
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = AR.id_fornecedores_despesas) 'celular_resp_legal',
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = AR.id_fornecedores_despesas) 'email_resp_legal',
AR.dt_cadastro dt_cadastro_resp_legal, AR.data_nascimento dt_nasc_resp_legal, AR.sexo sexo_resp_legal, AR.tipo_sang tipo_sang_resp_legal, AG.descricao_grupo grupo_resp_legal,
AR.estado_civil estcivil_resp_legal, AR.profissao profissao_resp_legal, AR.observacao obs_resp_legal, 
AR.cep cep_resp_legal, AR.endereco end_resp_legal, AR.numero num_resp_legal,
AR.complemento compl_resp_legal, AR.bairro bairro_resp_legal, 
GR.cidade_nome cidade_resp_legal, HR.estado_sigla estado_resp_legal, C.empresa filial, 
C.gran_esp, C.nip, C.depto, C.om, C.tel_om, C.ramal, C.local_cobranca, C.funcao_contato, C.trancarData, C.trancarMotivo,
C.num_cnh, C.cat_cnh, C.ven_cnh, C.pri_cnh
from sf_fornecedores_despesas C
left join tb_cidades D on C.cidade = D.cidade_codigo
left join tb_estados E on D.cidade_codigoEstado = E.estado_codigo
left join sf_grupo_cliente CG on C.grupo_pessoa = CG.id_grupo
left join sf_fornecedores_despesas_responsavel fr on fr.id_dependente = C.id_fornecedores_despesas
left join sf_fornecedores_despesas AR on fr.id_responsavel = AR.id_fornecedores_despesas
left join tb_cidades GR on AR.cidade = GR.cidade_codigo
left join tb_estados HR on AR.estado = HR.estado_codigo
left join sf_grupo_cliente AG on AR.grupo_pessoa = AG.id_grupo
where C.id_fornecedores_despesas = " . $id_cliente;
$cur = odbc_exec($con, $query);
$total = odbc_num_rows($cur);
$cur2 = odbc_exec($con, "select * from sf_configuracao");
while ($RFP = odbc_fetch_array(($total > 0 ? $cur : $cur2))) {
    $id_filial_cli = utf8_encode($RFP['filial']);
    $dicionario[] = array("A_mat", utf8_encode($RFP['id_cli']), "Matricula");
    $dicionario[] = array("A_nom", utf8_encode($RFP['Nm_cli']), "Nome Completo");
    $dicionario[] = array("A_nomc", formatNameCompact(utf8_encode($RFP['Nm_cli'])), "Nome Compacto");    
    $dicionario[] = array("A_nomp", array_shift(explode(' ', utf8_encode($RFP['Nm_cli']))), "Primeiro Nome");
    $dicionario[] = array("A_cpf", utf8_encode($RFP['cpf_cli']), "CPF");
    $dicionario[] = array("A_rg", utf8_encode($RFP['rg_cli']), "RG");
    $dicionario[] = array("A_celular", utf8_encode($RFP['celular_cli']), "Celular");
    $dicionario[] = array("A_telefone", utf8_encode($RFP['telefone_cli']), "Telefone");
    $dicionario[] = array("A_email", utf8_encode($RFP['email_cli']), "E-Mail");
    $dicionario[] = array("A_dt_cad", escreverData($RFP['dt_cadastro_cli']), "Data Cadastro");   
    $dicionario[] = array("A_dt_nasc", escreverData($RFP['dt_nasc_cli']), "Data de Nascimento");    
    $dicionario[] = array("A_sexo", utf8_encode($RFP['sexo_cli']), "Sexo");
    $dicionario[] = array("A_tipo_sang", utf8_encode($RFP['tipo_sang_cli']), "Tipo sanguíneo");
    $dicionario[] = array("A_des_grupo", utf8_encode($RFP['grupo_cli']), "Grupo");
    $dicionario[] = array("A_est_civil", utf8_encode($RFP['estado_civil_cli']), "Estado Civil");
    $dicionario[] = array("A_profissao", utf8_encode($RFP['profissao_cli']), "Profissão");
    $dicionario[] = array("A_obs", utf8_encode($RFP['obs_cli']), "Observações");
    $dicionario[] = array("A_cep", utf8_encode($RFP['cep_cli']), "CEP");
    $dicionario[] = array("A_lograd", utf8_encode($RFP['end_cli']), "Logradouro");
    $dicionario[] = array("A_end_num", utf8_encode($RFP['end_num_cli']), "Numero endereço");    
    $dicionario[] = array("A_comp_end", utf8_encode($RFP['compl_end']), "Comp. Endereço");    
    $dicionario[] = array("A_bairro", utf8_encode($RFP['bairro_cli']), "Bairro");
    $dicionario[] = array("A_cidade", utf8_encode($RFP['cidade_cli']), "Cidade");
    $dicionario[] = array("A_uf", utf8_encode($RFP['uf_cli']), "Estado");
    $dicionario[] = array("A_grad", utf8_encode($RFP['gran_esp']), "GRAD/ESP");
    $dicionario[] = array("A_nip", utf8_encode($RFP['nip']), "NIP");
    $dicionario[] = array("A_depto", utf8_encode($RFP['depto']), "Depto");
    $dicionario[] = array("A_om", utf8_encode($RFP['om']), "OM");
    $dicionario[] = array("A_tel_om", utf8_encode($RFP['tel_om']), "TEL. OM");
    $dicionario[] = array("A_ramal", utf8_encode($RFP['ramal']), "Ramal");    
    $dicionario[] = array("A_loc_cob", utf8_encode($RFP['local_cobranca']), "Local Cobrança");    
    $dicionario[] = array("A_conta_cor", utf8_encode($RFP['funcao_contato']), "Conta Corrente");     
    $dicionario[] = array("A_trancar_data", utf8_encode($RFP['trancarData']), "Data Trancamento"); 
    $dicionario[] = array("A_trancar_motivo", utf8_encode($RFP['trancarMotivo']), "Motivo Trancamento");     
    $dicionario[] = array("A_mat_resp", utf8_encode($RFP['id_resp_legal']), "Matricula Responsavel");
    $dicionario[] = array("A_nm_resp", utf8_encode($RFP['nm_resp_legal']), "Nome Responsavel");
    $dicionario[] = array("A_cpf_resp", utf8_encode($RFP['cpf_resp_legal']), "CPF Responsavel");
    $dicionario[] = array("A_rg_resp", utf8_encode($RFP['rg_resp_legal']), "RG Responsavel");
    $dicionario[] = array("A_celular_resp", utf8_encode($RFP['celular_resp_legal']), "Celular Responsavel");
    $dicionario[] = array("A_telefone_resp", utf8_encode($RFP['telefone_resp_legal']), "Telefone Responsavel");    
    $dicionario[] = array("A_email_resp", utf8_encode($RFP['email_resp_legal']), "E-Mail Responsavel");    
    $dicionario[] = array("A_dt_cad_resp", escreverData($RFP['dt_cadastro_resp_legal']), "Data Cadastro Responsavel");
    $dicionario[] = array("A_dt_nasc_resp", escreverData($RFP['dt_nasc_resp_legal']), "Dt. Nasc. Responsavel");
    $dicionario[] = array("A_sexo_resp", utf8_encode($RFP['sexo_resp_legal']), "Sexo Responsavel");
    $dicionario[] = array("A_tipo_sang_resp", utf8_encode($RFP['tipo_sang_resp_legal']), "Tipo sanguíneo Responsavel");
    $dicionario[] = array("A_des_grupo_resp", utf8_encode($RFP['grupo_resp_legal']), "Grupo Responsavel");
    $dicionario[] = array("A_est_civil_resp", utf8_encode($RFP['estcivil_resp_legal']), "Estado Civil Responsavel");
    $dicionario[] = array("A_prof_resp", utf8_encode($RFP['profissao_resp_legal']), "Profissão Responsavel");
    $dicionario[] = array("A_obs_resp", utf8_encode($RFP['obs_resp_legal']), "Observações Responsavel");
    $dicionario[] = array("A_cep_resp", utf8_encode($RFP['cep_resp_legal']), "CEP Responsavel");    
    $dicionario[] = array("A_lograd_resp", utf8_encode($RFP['end_resp_legal']), "Logradouro Responsavel");    
    $dicionario[] = array("A_num_resp", utf8_encode($RFP['num_resp_legal']), "Num endereco Responsavel");
    $dicionario[] = array("A_comp_end_resp", utf8_encode($RFP['compl_resp_legal']), "Comp. End. Responsavel");
    $dicionario[] = array("A_bairro_resp", utf8_encode($RFP['bairro_resp_legal']), "Bairro Responsavel");
    $dicionario[] = array("A_cid_resp", utf8_encode($RFP['cidade_resp_legal']), "Cidade Responsavel");
    $dicionario[] = array("A_est_resp", utf8_encode($RFP['estado_resp_legal']), "Estado Responsavel");
    $dicionario[] = array("A_num_cnh", utf8_encode($RFP['num_cnh']), "Número da CNH");
    $dicionario[] = array("A_cat_cnh", utf8_encode($RFP['cat_cnh']), "Categoria");
    $dicionario[] = array("A_ven_cnh", escreverData($RFP['ven_cnh']), "Vencimento CNH");
    $dicionario[] = array("A_pri_cnh", escreverData($RFP['pri_cnh']), "1ª Habilitação");
}

//dados do plano // x = 60 and x = 76
$query = "select top 1 A.dt_cadastro dt_cadastro_plan, A.id_plano id_plan,g.descricao desc_plan,I.descricao grupo_plan, 
(select min(dt_inicio_mens) from sf_vendas_planos_mensalidade where id_plano_mens = A.id_plano) dt_inicio_plan,
(select max(dt_fim_mens) from sf_vendas_planos_mensalidade where id_plano_mens = A.id_plano) dt_fim_plan,
A.dias_ferias dias_ferias_plan,A.dt_cancelamento dt_cancel_plan,A.obs_cancelamento obs_cancelamento_plan,
(select login_user from sf_usuarios where id_usuario = A.usuario_resp) usuario_resp_plan, 
(select login_user from sf_usuarios where id_usuario = A.usuario_canc) usuario_canc_plan,
(select max(valor_mens) from sf_vendas_planos_mensalidade where id_plano_mens = A.id_plano) valor_plan, 
(select valor_unico from sf_produtos_parcelas where id_produto = A.id_prod_plano and parcela = 1) valor_01_plan, 
(select (valor_unico - (valor_unico * valor_desconto)/100) from sf_produtos_parcelas where id_produto = A.id_prod_plano and parcela = 6)/6 valor_06_plan, 
(select (valor_unico - (valor_unico * valor_desconto)/100) from sf_produtos_parcelas where id_produto = A.id_prod_plano and parcela = 12)/12 valor_12_plan, 
(select COUNT(*) from sf_vendas_planos_mensalidade where id_plano_mens = A.id_plano) quant_parc_plan,
H.razao_social NmIndicador_plan, H.cnpj cpf_indicador_plan
from sf_vendas_planos A inner join sf_fornecedores_despesas F on A.favorecido = F.id_fornecedores_despesas
left join sf_usuarios U on A.usuario_resp = U.id_usuario
left join sf_fornecedores_despesas H on U.funcionario = H.id_fornecedores_despesas
inner join sf_produtos G on A.id_prod_plano = G.conta_produto
left join sf_contas_movimento I on G.conta_movimento = I.id_contas_movimento
where id_plano = " . $id_plan;
$cur = odbc_exec($con, $query);
$total = odbc_num_rows($cur);
$cur2 = odbc_exec($con, "select * from sf_configuracao");
while ($RFP = odbc_fetch_array(($total > 0 ? $cur : $cur2))) {
    $dicionario[] = array("P_desc", utf8_encode($RFP['desc_plan']), "Nome Plano");
    $dicionario[] = array("P_grup_desc", utf8_encode($RFP['grupo_plan']), "Grupo do Plano");    
    $dicionario[] = array("P_dt_cad", escreverData($RFP['dt_cadastro_plan']), "Data Cadastro Plano");
    $dicionario[] = array("P_dt_ini", escreverData($RFP['dt_inicio_plan']), "Data Inicio Plano");
    $dicionario[] = array("P_dt_fim", escreverData($RFP['dt_fim_plan']), "Data Fim Plano");
    $dicionario[] = array("P_dt_canc", escreverData($RFP['dt_cancel_plan']), "Data Cancelamento Plano");
    $dicionario[] = array("P_dias_f", utf8_encode($RFP['dias_ferias_plan']), "Dias de Férias");    
    $dicionario[] = array("P_obs_canc", utf8_encode($RFP['obs_cancelamento_plan']), "Obs. de Cancelamento");
    $dicionario[] = array("P_usu_cad", utf8_encode($RFP['usuario_resp_plan']), "Úsuario Cadastro Plano");
    $dicionario[] = array("P_usu_canc", utf8_encode($RFP['usuario_canc_plan']), "Úsuario Cancelou Plano");
    $dicionario[] = array("P_val", escreverNumero($RFP['valor_plan'], 1), "Valor Plano");
    $dicionario[] = array("P_val_1", escreverNumero($RFP['valor_01_plan'], 1), "Valor Plano Mensal");
    $dicionario[] = array("P_val_6", escreverNumero($RFP['valor_06_plan'], 1), "Valor Plano Semestral");
    $dicionario[] = array("P_val_12", escreverNumero($RFP['valor_12_plan'], 1), "Valor Plano Anual");
    $dicionario[] = array("P_count_parc", utf8_encode($RFP['quant_parc_plan']), "Quantidade de Parcelas");
    $dicionario[] = array("P_nm_indic", utf8_encode($RFP['NmIndicador_plan']), "Nome Indicador");
    $dicionario[] = array("P_cpf_indic", utf8_encode($RFP['cpf_indicador_plan']), "CPF Indicador");
}

//dados de filial // x = 77 and x = 88
$query = "select top 1 Descricao desc_emp, endereco end_emp, numero num_end_emp,
bairro bairro_end_emp, cidade_nome cidade_end_emp, estado estado_end_emp,
cep cep_end_emp,cnpj cnpj_emp, inscricao_estadual inscricao_est_emp,
telefone tel_emp, site site_emp, razao_social_contrato razao_social_emp,
nome_fantasia_contrato nome_fant_emp
from sf_filiais where numero_filial =" . $id_filial_cli;
$cur = odbc_exec($con, $query);
$total = odbc_num_rows($cur);
$cur2 = odbc_exec($con, "select * from sf_configuracao");
while ($RFP = odbc_fetch_array(($total > 0 ? $cur : $cur2))) {
    $dicionario[] = array("E_razao_social", utf8_encode($RFP['razao_social_emp']), "Razão Social");
    $dicionario[] = array("E_nom_fantasia", utf8_encode($RFP['nome_fant_emp']), "Nome Fantasia");
    $dicionario[] = array("E_cnpj", utf8_encode($RFP['cnpj_emp']), "CNPJ");
    $dicionario[] = array("E_insc_est", utf8_encode($RFP['inscricao_est_emp']), "Inscrição Estadual");
    $dicionario[] = array("E_log", utf8_encode($RFP['end_emp']), "Logradouro");
    $dicionario[] = array("E_num_end", utf8_encode($RFP['num_end_emp']), "Numero Endereço");
    $dicionario[] = array("E_bairro", utf8_encode($RFP['bairro_end_emp']), "Bairro");
    $dicionario[] = array("E_cidade", utf8_encode($RFP['cidade_end_emp']), "Cidade");
    $dicionario[] = array("E_estado", utf8_encode($RFP['estado_end_emp']), "Estado");
    $dicionario[] = array("E_cep", utf8_encode($RFP['cep_end_emp']), "Cep");
    $dicionario[] = array("E_tel", utf8_encode($RFP['tel_emp']), "Telefone");
    $dicionario[] = array("E_site", utf8_encode($RFP['site_emp']), "Site");
}

//x = 89 and x = 90
$aIn = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$aPt = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
setlocale(LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese');
$today = str_replace($aIn, $aPt, escreverData(date('Y-m-d'), "d-F-Y"));
$today = str_replace("-", " de ", $today);
$dicionario[] = array("O_day", date('d/m/Y'), "Data Atual");
$dicionario[] = array("O_write_day", $today, "Data Atual por extenso");

// x = 91 and x = 95
$vBg = 91;
$dicionario[] = array("M_dt_inic", array(), "Data inicio Mensalidade");
$dicionario[] = array("M_dt_fim", array(), "Data fim Mensalidade");
$dicionario[] = array("M_val_apag", array(), "Valor Apagar");
$dicionario[] = array("M_val_pag", array(), "Valor Pago");
$dicionario[] = array("M_dt_pag", array(), "Data Venda");
$query = "select vpm.id_mens, b.id_boleto, b.mundi_id, vpm.dt_inicio_mens dt_inicio_mens_parc, vpm.dt_fim_mens dt_fim_mens_parc, 
vi.valor_total val_pag_mens, vi.valor_bruto val_bruto_mens, v.data_Venda, vpm.valor_mens, bo.id_boleto id_boleto_online 
from sf_vendas_planos_mensalidade vpm
left join sf_vendas_itens vi on vpm.id_item_venda_mens = vi.id_item_venda
left join sf_vendas v on vi.id_Venda = v.id_venda 
left join sf_boleto b on b.id_referencia = vpm.id_mens and b.inativo = 0
left join sf_boleto_online_itens bo on bo.id_mens = vpm.id_mens
where id_plano_mens = " . $id_plan . (isset($_GET['id_mensalidade']) ? " and vpm.id_mens = " . $_GET['id_mensalidade'] :  "") . " order by dt_inicio_mens_parc";
$cur = odbc_exec($con, $query);
$total = odbc_num_rows($cur);
$cur2 = odbc_exec($con, "select * from sf_configuracao");
while ($RFP = odbc_fetch_array(($total > 0 ? $cur : $cur2))) {
    $dicionario["91"]["1"][] = escreverData($RFP['dt_inicio_mens_parc']);
    $dicionario["92"]["1"][] = escreverData($RFP['dt_fim_mens_parc']);
    $dicionario["93"]["1"][] = escreverNumero($RFP['valor_mens'], 1);
    $dicionario["94"]["1"][] = escreverNumero($RFP['val_pag_mens'], 1);
    $dicionario["95"]["1"][] = escreverData($RFP['data_Venda']);
    if (is_numeric($RFP['id_boleto'])) {
        $id_get = "M-" . $RFP['id_mens'];
        $mundi_id = $RFP['mundi_id'];
    } else if (is_numeric($RFP['id_boleto_online'])) {
        $id_boleto_on = $RFP['id_boleto_online'];
    }    
}
//--------------------------------------------------------------------------------------------
//x = 96 and x = 99
$query = "select nome_cartao, legenda_cartao, validade_cartao, 
(select case when credencial = 0 then DCC_DESCRICAO when credencial = 1 then DCC_DESCRICAO2 end from sf_configuracao) operadora_cartao 
from sf_fornecedores_despesas_cartao where id_fornecedores_despesas = " . $id_cliente;
$cur = odbc_exec($con, $query);
$total = odbc_num_rows($cur);
$cur2 = odbc_exec($con, "select * from sf_configuracao");
while ($RFP = odbc_fetch_array(($total > 0 ? $cur : $cur2))) {
    $dicionario[] = array("A_nome_cartao", utf8_encode($RFP['nome_cartao']), "Nome");
    $dicionario[] = array("A_legenda_cartao", utf8_encode($RFP['legenda_cartao']), "Núm Cartão");
    $dicionario[] = array("A_validade_cartao", escreverData($RFP['validade_cartao']), "Validade");
    $dicionario[] = array("A_operadora_cartao", utf8_encode($RFP['operadora_cartao']), "Operadora");
}
//--------------------------------------------------------------------------------------------
//x = 100 and x = 116
$query = "select tipo_veiculo, placa, marca, chassi, modelo, replace(ano_modelo, '32000', 'Zero KM') ano_modelo, ano_fab, valor, renavam, franquia_tp,
case when porcentagem > 0 then ((valor * 100)/porcentagem) else 0 end valor_fipe, combustivel, franquia, imei, chip, dt_instalacao, codigo_fipe,
(select top 1 descricao from sf_veiculo_tipos where id = tipo_veiculo) t_veiculo, nome_produto_cor
from sf_vendas_planos inner join sf_fornecedores_despesas_veiculo on id_veiculo = id
left join sf_produtos_cor on cor = id_produto_cor where id_plano = " . $id_plan;
$cur = odbc_exec($con, $query);
$total = odbc_num_rows($cur);
$cur2 = odbc_exec($con, "select * from sf_configuracao");
while ($RFP = odbc_fetch_array(($total > 0 ? $cur : $cur2))) {
    $dicionario[] = array("V_tipo", utf8_encode($RFP['t_veiculo']), "Tipo");
    $dicionario[] = array("V_placa", utf8_encode($RFP['placa']), "Placa");
    $dicionario[] = array("V_marca", utf8_encode($RFP['marca']), "Marca");
    $dicionario[] = array("V_chassi", utf8_encode($RFP['chassi']), "Chassi");
    $dicionario[] = array("V_renavam", utf8_encode($RFP['renavam']), "Renavam");
    $dicionario[] = array("V_codigo_fipe", utf8_encode($RFP['codigo_fipe']), "Código Fipe");
    $dicionario[] = array("V_modelo", utf8_encode($RFP['modelo']), "Modelo");
    $dicionario[] = array("V_ano_modelo", utf8_encode($RFP['ano_modelo']), "Ano Modelo");
    $dicionario[] = array("V_ano_fab", utf8_encode($RFP['ano_fab']), "Ano Fabricação");
    $dicionario[] = array("V_valor_fipe", escreverNumero($RFP['valor_fipe'], 1), "Valor Fipe");
    $dicionario[] = array("V_valor", escreverNumero($RFP['valor'], 1), "Valor Protegido");
    $dicionario[] = array("V_combustivel", utf8_encode($RFP['combustivel']), "Combustível");
    $dicionario[] = array("V_cor", utf8_encode($RFP['nome_produto_cor']), "Cor");    
    $dicionario[] = array("V_franquia", escreverNumero(($RFP['franquia_tp'] == 1 ? $RFP['franquia'] : ($RFP['valor_fipe'] * $RFP['franquia'] / 100)), 1), "Cota de Participação");    
    $dicionario[] = array("V_imei", utf8_encode($RFP['imei']), "Imei");
    $dicionario[] = array("V_chip", utf8_encode($RFP['chip']), "Chip");
    $dicionario[] = array("V_dt_instal", escreverData($RFP['dt_instalacao']), "Data Instalação");
}
//x = 117 and x = 121
$query = "select top 1 (select top 1 day(dateadd(day,1,dt_fim_mens)) from sf_vendas_planos_mensalidade m1 where m1.id_plano_mens = m.id_plano_mens order by dt_inicio_mens desc) dia, 
dbo.VALOR_REAL_MENSALIDADE(id_mens) total_liquido, 
valor_mens + (dbo.FU_VALOR_ACESSORIOS(m.id_plano_mens) * case when parcela < 1 then 1 else parcela end) total_bruto, 
parcela, (select isnull(dbo.VALOR_REAL_MENSALIDADE(max(id_mens)), 0) from sf_vendas_planos_mensalidade m1 where m1.id_plano_mens = m.id_plano_mens) total
from sf_vendas_planos_mensalidade m inner join sf_produtos_parcelas on id_parc_prod_mens = id_parcela
left join sf_vendas_itens on id_item_venda_mens = sf_vendas_itens.id_item_venda
where id_plano_mens = " . $id_plan . "
order by dt_inicio_mens desc";
$cur = odbc_exec($con, $query);
$total = odbc_num_rows($cur);
$cur2 = odbc_exec($con, "select * from sf_configuracao");
while ($RFP = odbc_fetch_array(($total > 0 ? $cur : $cur2))) {
    $dicionario[] = array("V_dia", utf8_encode($RFP['dia']), "Dia Vencimento");
    $dicionario[] = array("V_total", escreverNumero($RFP['total'], 1), "Valor Pagamento");
    $dicionario[] = array("V_total_desc", escreverNumero(($RFP['total_bruto'] - $RFP['total_liquido']), 1), "Valor Desconto");
    $dicionario[] = array("V_total_bruto", escreverNumero($RFP['total_bruto'], 1), "Valor Bruto");
    $dicionario[] = array("V_parcela", legendaPlano($RFP['parcela']), "Forma de Pagamento");
}
//x = 122 and x = 122
$acessorios = "";
$query = "select descricao from sf_vendas_planos_acessorios
inner join sf_produtos on conta_produto = id_servico_acessorio
where sf_produtos.tipo = 'A' and id_venda_plano = " . $id_plan;
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $acessorios .= utf8_encode($RFP['descricao']) . " + ";
}
$dicionario[] = array("V_acessorios", substr_replace($acessorios, "", -3), "Acessórios");
//x = 123 and x = 123
$servicos = "";
$query = "select descricao from sf_vendas_planos 
inner join sf_produtos_materiaprima on id_prod_plano = id_produtomp
inner join sf_produtos on id_materiaprima = conta_produto 
where tipo = 'S' and id_plano = " . $id_plan;
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $servicos .= utf8_encode($RFP['descricao']) . " + ";
}
$dicionario[] = array("V_servicos", substr_replace($servicos, "", -3), "Serviços");
//--------------------------------------------------------------------------------------------
// x = 124 and x = 133
for ($i = 1; $i <= 10; $i++) {
    $dicionario[] = array("V_campoL_" . $i, "", "");
}
$cur = odbc_exec($con, "select id_campo, descricao_campo, conteudo_campo from sf_configuracao_campos_livres 
left join sf_fornecedores_despesas_campos_livres on campos_campos = id_campo 
and fornecedores_campos = " . $id_cliente . " where ativo_campo = 1");
while ($RFP = odbc_fetch_array($cur)) {
    $dicionario[123 + $RFP['id_campo']]["1"] = utf8_encode($RFP['conteudo_campo']);
    $dicionario[123 + $RFP['id_campo']]["2"] = utf8_encode($RFP['descricao_campo']);
}
//--------------------------------------------------------------------------------------------
//x = 134 and x = 142
$t_turma = "";
$t_professor = "";
$t_hor_ = array();
$query = "select descricao, razao_social, 
faixa1_ini, faixa1_fim, dia_semana1, 
faixa2_ini, faixa2_fim, dia_semana2,
faixa3_ini, faixa3_fim, dia_semana3,
faixa4_ini, faixa4_fim, dia_semana4,
faixa5_ini, faixa5_fim, dia_semana5
from sf_fornecedores_despesas_turmas 
inner join sf_turmas on sf_turmas.id_turma = sf_fornecedores_despesas_turmas.id_turma
inner join sf_fornecedores_despesas on sf_fornecedores_despesas.id_fornecedores_despesas = professor
inner join sf_turnos on horario = cod_turno
where id_plano = " . $id_plan;
$cur = odbc_exec($con, $query);
$total = odbc_num_rows($cur);
$cur2 = odbc_exec($con, "select * from sf_configuracao");
while ($RFP = odbc_fetch_array(($total > 0 ? $cur : $cur2))) {
    $t_turma .= utf8_encode($RFP['descricao']) . ", ";
    $t_professor .= utf8_encode($RFP['razao_social']) . ", ";
    for ($j = 0; $j < 7; $j++) {
        $t_hor_[$j] .= (substr($RFP['dia_semana1'], $j, 1) == "1" ? "[" . $RFP['faixa1_ini'] . " - " . $RFP['faixa1_fim'] . "] " : "") .
        (substr($RFP['dia_semana2'], $j, 1) == "1" ? "[" . $RFP['faixa2_ini'] . " - " . $RFP['faixa2_fim'] . "] " : "") .
        (substr($RFP['dia_semana3'], $j, 1) == "1" ? "[" . $RFP['faixa3_ini'] . " - " . $RFP['faixa3_fim'] . "] " : "") .
        (substr($RFP['dia_semana4'], $j, 1) == "1" ? "[" . $RFP['faixa4_ini'] . " - " . $RFP['faixa4_fim'] . "] " : "") .
        (substr($RFP['dia_semana5'], $j, 1) == "1" ? "[" . $RFP['faixa5_ini'] . " - " . $RFP['faixa5_fim'] . "] " : "");            
    }
}
$dicionario[] = array("T_turma", substr_replace($t_turma, "", -2), "Descrição Turma");
$dicionario[] = array("T_professor", substr_replace($t_professor, "", -2), "Professor Turma");
$dicionario[] = array("T_hor_dom", $t_hor_[0], "Horário de DOMINGO");
$dicionario[] = array("T_hor_seg", $t_hor_[1], "Horário de SEGUNDA");
$dicionario[] = array("T_hor_ter", $t_hor_[2], "Horário de TERÇA");
$dicionario[] = array("T_hor_qua", $t_hor_[3], "Horário de QUARTA");
$dicionario[] = array("T_hor_qui", $t_hor_[4], "Horário de QUINTA");
$dicionario[] = array("T_hor_sex", $t_hor_[5], "Horário de SEXTA");
$dicionario[] = array("T_hor_sab", $t_hor_[6], "Horário de SÁBADO");
//--------------------------------------------------------------------------------------------
//x = 143 and x = 147
$dicionario[] = array("M_bol_valor", "", "Boleto Valor");
$dicionario[] = array("M_bol_data", "", "Boleto Vencimento");
$dicionario[] = array("M_bol_link", "", "Boleto Link");
$dicionario[] = array("M_bol_codigo", "", "Boleto Código");
$dicionario[] = array("M_bol_numero", "", "Boleto N. Número");
if (strlen($mundi_id) > 0) {
    $cur = odbc_exec($con, "select bol_valor, bol_data_parcela, bol_descricao, sa_descricao, bol_nosso_numero 
    from sf_boleto where inativo = 0 and mundi_id = " . valoresTexto2($mundi_id));
    while ($RFP = odbc_fetch_array($cur)) {
        $dicionario["143"]["1"] = escreverNumero($RFP['bol_valor']);
        $dicionario["144"]["1"] = escreverData($RFP['bol_data_parcela']);
        $dicionario["145"]["1"] = utf8_encode($RFP['bol_descricao']);
        $dicionario["146"]["1"] = utf8_encode($RFP['sa_descricao']);
        $dicionario["147"]["1"] = utf8_encode($RFP['bol_nosso_numero']);
    }
} else if ($id_boleto_on > 0) {
    $cur = odbc_exec($con, "select bol_valor, bol_data_parcela, bol_link, bol_linha, galaxy_id
    from sf_boleto_online where id_boleto = " . $id_boleto_on . " and inativo = 0");
    while ($RFP = odbc_fetch_array($cur)) {
        $dicionario["143"]["1"] = escreverNumero($RFP['bol_valor']);
        $dicionario["144"]["1"] = escreverData($RFP['bol_data_parcela']);
        $dicionario["145"]["1"] = utf8_encode($RFP['bol_link']);
        $dicionario["146"]["1"] = utf8_encode($RFP['bol_linha']);
        $dicionario["147"]["1"] = utf8_encode($RFP['galaxy_id']);
    }    
} else if (strlen($id_get) > 0) {
    include_once (__DIR__ . "./../../../Boletos/include/funcoes.php");
    include(__DIR__ . "./../../../Boletos/include/processamento_dados.php");    
    $dicionario["143"]["1"] = $dadosboleto["valor_boleto"];
    $dicionario["144"]["1"] = $dadosboleto["data_vencimento"];
    $dicionario["145"]["1"] = "https://sivisweb.com.br/Boletos/Boleto.php?id=" . $id_get . "&crt=" . $contrato;
    $dicionario["146"]["1"] = str_replace(array(".", " "), "", $dadosboleto["linha_digitavel"]);
    $dicionario["147"]["1"] = $dadosboleto["nosso_numero"];
}
//--------------------------------------------------------------------------------------------
//x = 148 and x = 158
$query = "select id_agendamento, descricao_agenda, data_agendamento, agendado_por, data_inicio, data_fim, assunto, obs, id_item_venda, agendado_por 
from sf_agendamento inner join sf_agenda on sf_agenda.id_agenda = sf_agendamento.id_agenda
where id_agendamento = " . $id_agendamento . "
order by data_agendamento desc";
$cur = odbc_exec($con, $query);
$total = odbc_num_rows($cur);
$cur2 = odbc_exec($con, "select * from sf_configuracao");
while ($RFP = odbc_fetch_array(($total > 0 ? $cur : $cur2))) {    
    $id_venda_item = $RFP['id_item_venda'];
    $dicionario[] = array("age_cod", str_pad($RFP['id_agendamento'], 6, "0", STR_PAD_LEFT), "Código");
    $dicionario[] = array("age_nome", utf8_encode($RFP['descricao_agenda']), "Nome da Agenda");
    $dicionario[] = array("age_data", escreverData($RFP['data_agendamento']), "Data Agendamento");
    $dicionario[] = array("age_login", utf8_encode($RFP['agendado_por']), "Agendado por");
    $dicionario[] = array("age_dt_ini", escreverData($RFP['data_inicio']), "Data Início");
    $dicionario[] = array("age_dt_fim", escreverData($RFP['data_fim']), "Data Fim");
    $dicionario[] = array("age_hr_ini", escreverData($RFP['data_inicio'], "H:i"), "Hora Início");
    $dicionario[] = array("age_hr_fim", escreverData($RFP['data_fim'], "H:i"), "Hora Fim");
    $dicionario[] = array("age_responsavel", utf8_encode($RFP['agendado_por']), "Responsável");
    $dicionario[] = array("age_assunto", utf8_encode($RFP['assunto']), "Assunto");
    $dicionario[] = array("age_obs", utf8_encode($RFP['obs']), "Observação");
}
//--------------------------------------------------------------------------------------------
for ($i = $vBg; $i <= ($vBg + 4); $i++) {
    $NT = "";
    $NA = "";
    $NP = "";
    $NV = "";
    for ($j = 0; $j < count($dicionario[$i][1]); $j++) {
        $NT .= $dicionario[$i][1][$j] . ", ";
        if ($dicionario[($vBg + 4)][1][$j] == "") {
            $NA .= $dicionario[$i][1][$j] . ", ";
        } else {
            $NP .= $dicionario[$i][1][$j] . ", ";
        }
        if ($dicionario[($vBg + 4)][1][$j] == "" && (geraTimestamp($dicionario[$vBg][1][$j]) - geraTimestamp(getData("T"))) <= 0) {
            $NV .= $dicionario[$i][1][$j] . ", ";
        }
    }
    $dicionario[] = array($dicionario[$i][0] . "|NT", substr_replace($NT, "", -2), $dicionario[$i][2]);
    $dicionario[] = array($dicionario[$i][0] . "|NA", substr_replace($NA, "", -2), $dicionario[$i][2]);
    $dicionario[] = array($dicionario[$i][0] . "|NP", substr_replace($NP, "", -2), $dicionario[$i][2]);
    $dicionario[] = array($dicionario[$i][0] . "|NV", substr_replace($NV, "", -2), $dicionario[$i][2]);
}
//--------------------------------------------------------------------------------------------
//dados Dependente // x = 179 and x = 209
$dicionario[] = array("A_mat_dep", array(), "Matricula Dependente");
$dicionario[] = array("A_nom_dep", array(), "Nome Completo Dep.");
$dicionario[] = array("A_nomc_dep", array(), "Nome Compacto Dep.");    
$dicionario[] = array("A_nomp_dep", array(), "Primeiro Nome Dep.");
$dicionario[] = array("A_cpf_dep", array(), "CPF Dependente");
$dicionario[] = array("A_rg_dep", array(), "RG Dependente");
$dicionario[] = array("A_celular_dep", array(), "Celular Dependente");
$dicionario[] = array("A_telefone_dep", array(), "Telefone Dependente");
$dicionario[] = array("A_email_dep", array(), "E-Mail Dependente");
$dicionario[] = array("A_dt_cad_dep", array(), "Data Cadastro Dep.");
$dicionario[] = array("A_dt_nasc_dep", array(), "Data de Nascimento Dep.");
$dicionario[] = array("A_sexo_dep", array(), "Sexo Dependente");
$dicionario[] = array("A_tipo_sang_dep", array(), "Tipo sanguíneo Dep.");
$dicionario[] = array("A_des_grupo_dep", array(), "Grupo Dependente");
$dicionario[] = array("A_est_civil_dep", array(), "Estado Civil Dependente");
$dicionario[] = array("A_profissao_dep", array(), "Profissão Dependente");
$dicionario[] = array("A_obs_dep", array(), "Observações Dependente");
$dicionario[] = array("A_cep_dep", array(), "CEP Dependente");
$dicionario[] = array("A_lograd_dep", array(), "Logradouro Dep.");
$dicionario[] = array("A_end_num_dep", array(), "Numero endereço Dep.");
$dicionario[] = array("A_comp_end_dep", array(), "Comp. Endereço Dep.");
$dicionario[] = array("A_bairro_dep", array(), "Bairro Dependente");
$dicionario[] = array("A_cidade_dep", array(), "Cidade Dependente");
$dicionario[] = array("A_uf_dep", array(), "Estado Dependente");
$dicionario[] = array("A_grad_dep", array(), "GRAD/ESP Dependente");
$dicionario[] = array("A_nip_dep", array(), "NIP Dependente");
$dicionario[] = array("A_depto_dep", array(), "Depto Dependente");
$dicionario[] = array("A_om_dep", array(), "OM Dependente");
$dicionario[] = array("A_tel_om_dep", array(), "TEL. OM Dependente");
$dicionario[] = array("A_ramal_dep", array(), "Ramal Dependente");
$dicionario[] = array("A_loc_cob_dep", array(), "Local Cobrança Dep.");
$query = "select C.id_fornecedores_despesas id_cli,C.razao_social Nm_cli, C.cnpj cpf_cli, C.inscricao_estadual rg_cli, 
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas = C.id_fornecedores_despesas) 'telefone_cli',
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = C.id_fornecedores_despesas) 'celular_cli',
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = C.id_fornecedores_despesas) 'email_cli',
C.dt_cadastro dt_cadastro_cli,C.observacao obs_cli, C.data_nascimento dt_nasc_cli, C.sexo sexo_cli, C.tipo_sang tipo_sang_cli, CG.descricao_grupo grupo_cli,
C.estado_civil estado_civil_cli,C.profissao profissao_cli, 
C.cep cep_cli, C.endereco end_cli, C.numero end_num_cli, 
C.complemento compl_end, C.bairro bairro_cli, 
D.cidade_nome cidade_cli, E.estado_sigla uf_cli, C.empresa filial, 
C.gran_esp, C.nip, C.depto, C.om, C.tel_om, C.ramal, C.local_cobranca, C.funcao_contato
from sf_fornecedores_despesas_dependentes fd inner join sf_fornecedores_despesas C on fd.id_dependente = c.id_fornecedores_despesas
left join tb_cidades D on C.cidade = D.cidade_codigo
left join tb_estados E on D.cidade_codigoEstado = E.estado_codigo
left join sf_grupo_cliente CG on C.grupo_pessoa = CG.id_grupo
where " . (isset($_GET['id_dependente']) ? "fd.id_dependente = " . $_GET['id_dependente'] : "fd.id_titular = " . $id_cliente . " or fd.id_dependente = " . $id_cliente);
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {   
    $dicionario["179"]["1"][] = utf8_encode($RFP['id_cli']);
    $dicionario["180"]["1"][] = utf8_encode($RFP['Nm_cli']);
    $dicionario["181"]["1"][] = formatNameCompact(utf8_encode($RFP['Nm_cli']));    
    $dicionario["182"]["1"][] = array_shift(explode(' ', utf8_encode($RFP['Nm_cli'])));
    $dicionario["183"]["1"][] = utf8_encode($RFP['cpf_cli']);
    $dicionario["184"]["1"][] = utf8_encode($RFP['rg_cli']);
    $dicionario["185"]["1"][] = utf8_encode($RFP['celular_cli']);
    $dicionario["186"]["1"][] = utf8_encode($RFP['telefone_cli']);
    $dicionario["187"]["1"][] = utf8_encode($RFP['email_cli']);    
    $dicionario["188"]["1"][] = escreverData($RFP['dt_cadastro_cli']);    
    $dicionario["189"]["1"][] = escreverData($RFP['dt_nasc_cli']);
    $dicionario["190"]["1"][] = utf8_encode($RFP['sexo_cli']);    
    $dicionario["191"]["1"][] = utf8_encode($RFP['tipo_sang_cli']);
    $dicionario["192"]["1"][] = utf8_encode($RFP['grupo_cli']);
    $dicionario["193"]["1"][] = utf8_encode($RFP['estado_civil_cli']);
    $dicionario["194"]["1"][] = utf8_encode($RFP['profissao_cli']);
    $dicionario["195"]["1"][] = utf8_encode($RFP['obs_cli']);    
    $dicionario["196"]["1"][] = utf8_encode($RFP['cep_cli']);
    $dicionario["197"]["1"][] = utf8_encode($RFP['end_cli']);
    $dicionario["198"]["1"][] = utf8_encode($RFP['end_num_cli']);
    $dicionario["199"]["1"][] = utf8_encode($RFP['compl_end']);
    $dicionario["200"]["1"][] = utf8_encode($RFP['bairro_cli']);
    $dicionario["201"]["1"][] = utf8_encode($RFP['cidade_cli']);
    $dicionario["202"]["1"][] = utf8_encode($RFP['uf_cli']);    
    $dicionario["203"]["1"][] = utf8_encode($RFP['gran_esp']);
    $dicionario["204"]["1"][] = utf8_encode($RFP['nip']);
    $dicionario["205"]["1"][] = utf8_encode($RFP['depto']);
    $dicionario["206"]["1"][] = utf8_encode($RFP['om']);
    $dicionario["207"]["1"][] = utf8_encode($RFP['tel_om']);
    $dicionario["208"]["1"][] = utf8_encode($RFP['ramal']);
    $dicionario["209"]["1"][] = utf8_encode($RFP['local_cobranca']);
}
//--------------------------------------------------------------------------------------------
//x = 210 and x = 212
$dicionario[] = array("A_ass_cli", "", "Assinatura Cliente");
$dicionario[] = array("A_ass_cpd", "", "Assinatura Cliente Padrão");
$dicionario[] = array("A_ass_res", "", "Assinatura Responsável");
if (file_exists(dirname(__FILE__) . "./../../../Pessoas/" . $contrato . "/Assinaturas/" . $id_cliente . "/" . $id_plan . "_" . $_GET["idContrato"] . ".png")) {
    $dicionario["210"]["1"] = "<img src = \"" . dirname(__FILE__) . "./../../../Pessoas/" . $contrato . "/Assinaturas/" . $id_cliente . "/" . $id_plan . "_" . $_GET["idContrato"] . ".png" . "\" height=\"50\" />";              
} else if (file_exists(dirname(__FILE__) . "./../../../Pessoas/" . $contrato . "/Assinaturas/" . $id_cliente . "/" . $_GET["id_plan"] . "_" . $_GET["idContrato"] . ".png")) {
    $dicionario["210"]["1"] = "<img src = \"" . dirname(__FILE__) . "./../../../Pessoas/" . $contrato . "/Assinaturas/" . $id_cliente . "/" . $_GET["id_plan"] . "_" . $_GET["idContrato"] . ".png" . "\" height=\"50\" />";              
} else if (file_exists(dirname(__FILE__) . "./../../../Pessoas/" . $contrato . "/Assinaturas/" . $id_cliente . "/vistoria_" . $_GET["id_vistoria"] . ".png")) {
    $dicionario["210"]["1"] = "<img src = \"" . dirname(__FILE__) . "./../../../Pessoas/" . $contrato . "/Assinaturas/" . $id_cliente . "/vistoria_" . $_GET["id_vistoria"] . ".png" . "\" height=\"50\" />";              
} else if (file_exists(dirname(__FILE__) . "./../../../Pessoas/" . $contrato . "/Assinaturas/" . $id_cliente . "/evento_" . $_GET["id_evento"] . ".png")) {
    $dicionario["210"]["1"] = "<img src = \"" . dirname(__FILE__) . "./../../../Pessoas/" . $contrato . "/Assinaturas/" . $id_cliente . "/evento_" . $_GET["id_evento"] . ".png" . "\" height=\"50\" />";              
}
if (file_exists(dirname(__FILE__) . "./../../../Pessoas/" . $contrato . "/Assinaturas/" . $id_cliente . "/base_ass.png")) {
    $dicionario["211"]["1"] = "<img src = \"" . dirname(__FILE__) . "./../../../Pessoas/" . $contrato . "/Assinaturas/" . $id_cliente . "/base_ass.png\" height=\"50\" />";              
} else if (strlen($dicionario["210"]["1"]) > 0) {
    $dicionario["211"]["1"] = $dicionario["210"]["1"];
}
if (file_exists(dirname(__FILE__) . "./../../../Pessoas/" . $contrato . "/Empresa/Assinatura/responsavel.png")) {
    $dicionario["212"]["1"] = "<img src = \"" . dirname(__FILE__) . "./../../../Pessoas/" . $contrato . "/Empresa/Assinatura/responsavel.png\" height=\"50\" />";              
}
//--------------------------------------------------------------------------------------------
// x = 213 and x = 217
$query = "select v.id_venda, data_venda, valor_total, p.descricao,
STUFF((SELECT distinct '|' + td.descricao FROM sf_venda_parcelas vp inner join sf_tipo_documento td on vp.tipo_documento = td.id_tipo_documento WHERE vp.venda = v.id_venda FOR XML PATH('')), 1, 1, '') as abreviacao
from sf_vendas_itens vi inner join sf_vendas v on v.id_venda = vi.id_venda
inner join sf_produtos p on p.conta_produto = vi.produto where v.status = 'Aprovado' and id_item_venda = " . valoresNumericos2($id_venda_item);
$cur = odbc_exec($con, $query);
$total = odbc_num_rows($cur);
$cur2 = odbc_exec($con, "select * from sf_configuracao");
while ($RFP = odbc_fetch_array(($total > 0 ? $cur : $cur2))) {
    $dicionario[] = array("I_id_venda", utf8_encode($RFP['id_venda']), "Código Venda");
    $dicionario[] = array("I_dta_venda", escreverData($RFP['data_venda']), "Data Venda");
    $dicionario[] = array("I_val_venda", escreverNumero($RFP['valor_total']), "Valor Venda");
    $dicionario[] = array("I_dsc_venda", utf8_encode($RFP['descricao']), "Item Venda");
    $dicionario[] = array("I_tip_venda", utf8_encode($RFP['abreviacao']), "Forma de Pagamento");
}
//--------------------------------------------------------------------------------------------
// x = 218 and x = 226
$query = "select id_tele, data_tele, proximo_contato, relato, obs, obs_fechamento, nome, nome_especificacao,
STUFF((SELECT distinct '|' + mt.mensagem FROM sf_mensagens_telemarketing mt WHERE mt.id_tele = t.id_tele FOR XML PATH('')), 1, 1, '') as chamados
from sf_telemarketing t
left join sf_usuarios u on para_pessoa = id_usuario
left join sf_telemarketing_especificacao e on e.id_especificacao = t.id_especificacao
where id_tele = " . valoresNumericos2($id_evento);
$cur = odbc_exec($con, $query);
$total = odbc_num_rows($cur);
$cur2 = odbc_exec($con, "select * from sf_configuracao");
while ($RFP = odbc_fetch_array(($total > 0 ? $cur : $cur2))) {
    $dicionario[] = array("Ev_codigo", utf8_encode($RFP['id_tele']) . "/" . escreverData($RFP['data_tele'], "Y"), "Código OS");
    $dicionario[] = array("Ev_data", escreverData($RFP['data_tele']), "Aviso de Evento");
    $dicionario[] = array("Ev_prazo", escreverData($RFP['proximo_contato']), "Data Prazo");
    $dicionario[] = array("Ev_resp", utf8_encode($RFP['nome']), "Técnico Responsável");
    $dicionario[] = array("Ev_acao", utf8_encode($RFP['nome_especificacao']), "Ação do Evento");
    $dicionario[] = array("Ev_desc", utf8_encode($RFP['relato']), "Descrição da Ocorrência");
    $dicionario[] = array("Ev_obs", utf8_encode($RFP['obs']), "Observação da Ocorrência");
    $dicionario[] = array("Ev_solucao", utf8_encode($RFP['obs_fechamento']), "Solução");
    $dicionario[] = array("Ev_hist", utf8_encode($RFP['chamados']), "Histórico de Atendimento");
}
//--------------------------------------------------------------------------------------------
// x = 227 and x = 228
$danos_avarias = "";
if ($id_vistoria > 0) {
    $ImagensDanos = [];    
    $pasta = dirname(__FILE__) . "./../../../Pessoas/" . $contrato . "/Danos/" . $id_vistoria . "/";
    if (file_exists($pasta)) {
        $diretorio = dir($pasta);
        while ($arquivo = $diretorio->read()) {
            if ($arquivo != "." && $arquivo != "..") {
                $ImagensDanos[] = $arquivo;
            }
        }
    }
    $i_vist = 1;
    $query = "select id, observacao from sf_vistorias_danos where id_vistoria = " . $id_vistoria . " order by id";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $danos_avarias .= $i_vist . " - " . strtoupper(utf8_encode($RFP['observacao'])) . "<br>";        
        foreach ($ImagensDanos as $value) {
            $k = explode(".", $value);
            if ($RFP['id'] == $k[0]) {
                $danos_avarias .= "<img src = \"" . dirname(__FILE__) . "./../../../Pessoas/" . $contrato . 
                "/Danos/" . $id_vistoria . "/" . $value . "\" height=\"150\" /><br><br>";
            }
        }
        $i_vist++;
    }
}

$a_vist_link = "";
$query = "select vi.id, v.placa, v.modelo
from sf_fornecedores_despesas B 
inner join sf_fornecedores_despesas_veiculo v on v.id_fornecedores_despesas = B.id_fornecedores_despesas
inner join sf_vistorias vi on vi.id_veiculo = v.id
where b.inativo = 0 and b.tipo = 'C' and v.status = 'Aprovado' and vi.data_aprov is null 
and B.id_fornecedores_despesas = " . $id_cliente;
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $a_vist_link .= utf8_encode($RFP['placa']) . " - *" . utf8_encode($RFP['modelo']) . "*%0ahttps://vistoria.sivisweb.com.br/" . $RFP['id'] . "/" . $contrato . "%0a";
}

$dicionario[] = array("Vis_danos", $danos_avarias, "Lista Danos e Avarias");
$dicionario[] = array("A_vist_link", $a_vist_link, "Links de Vistorias");
//--------------------------------------------------------------------------------------------
// x = 229 and x = 233
$g_placas = [];
$g_valor_mens = [];
$g_valor_anual = [];
$g_total_mens = 0;
$g_total_anual = 0;
$query = "select placa, ano_modelo, dbo.VALOR_REAL_MENSALIDADE(id_mens) valor_mensal
from sf_vendas_planos vp inner join sf_vendas_planos_mensalidade vm on vp.id_plano = vm.id_plano_mens 
and id_mens = (select max(id_mens) from sf_vendas_planos_mensalidade mm where mm.id_plano_mens = vp.id_plano)
inner join sf_fornecedores_despesas_veiculo v on vp.id_veiculo = v.id where id_plano in (" . $_GET["id_plan"] . ")";
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $g_placas[] = ($RFP["ano_modelo"] == "32000" ? "Zero Km" : $RFP['placa']);
    $g_valor_mens[] = escreverNumero($RFP['valor_mensal'], 1);
    $g_valor_anual[] = escreverNumero(($RFP['valor_mensal'] * 12), 1);
    $g_total_mens += $RFP['valor_mensal'];
    $g_total_anual += ($RFP['valor_mensal'] * 12);
}
$dicionario[] = array("G_placas", $g_placas, "Placas de Planos");
$dicionario[] = array("G_valor_mens", $g_valor_mens, "Valor Mens Planos");
$dicionario[] = array("G_valor_anual", $g_valor_anual, "Valor Anual Planos");
$dicionario[] = array("G_total_mens", escreverNumero($g_total_mens, 1), "Valor Total Mensal");
$dicionario[] = array("G_total_anual", escreverNumero($g_total_anual, 1), "Valor Total Anual");
//echo json_encode($dicionario); exit;