<?php

include "../../Connections/configini.php";
require_once('../../util/util.php');

if (isset($_GET['listFiliais'])) {
    $cur = odbc_exec($con, "select * from sf_filiais");
    $cont = 0;
    $records = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    while ($RFP = odbc_fetch_array($cur)) {
        $cont = $cont + 1;
        $row = array();
        if(isset($_GET['limp'])){
          $row[] = strtoupper(utf8_encode($RFP["id_filial"]));
          $row[] = strtoupper(utf8_encode($RFP["Descricao"]));
          $row[] = strtoupper(utf8_encode($RFP["razao_social_contrato"]));
          $row[] = strtoupper(utf8_encode($RFP["nome_fantasia_contrato"]));
        }else{          
          $btnEst = "<a href=\"javascript:;\" data-id=\"" . $RFP['id_filial'] . "\" id=\"btnAlt\"><span class=\"ico-edit\" title=\"Alterar\" style=\"font-size:18px;\"> </span></a>";
          $row[] = strtoupper(utf8_encode($RFP["numero_filial"]));
          $row[] = "<div id=\"txtDescricao_" . $RFP['id_filial'] . "\">" . strtoupper(utf8_encode($RFP["Descricao"])) . "</div>";
          $row[] = "<div id=\"txtRazao_" . $RFP['id_filial'] . "\">" . strtoupper(utf8_encode($RFP["razao_social_contrato"])) . "</div>";
          $row[] = "<div id=\"txtFantasia_" . $RFP['id_filial'] . "\">" . strtoupper(utf8_encode($RFP["nome_fantasia_contrato"])) . "</div>";
          $row[] = "<div id=\"txtCnpj_" . $RFP['id_filial'] . "\">" . strtoupper(utf8_encode($RFP["cnpj"])) . "</div>";
          $row[] = "<div id=\"txtInscricao_" . $RFP['id_filial'] . "\">" . strtoupper(utf8_encode($RFP["inscricao_estadual"])) . "</div>";
          $row[] = "<div id=\"txtEndereco_" . $RFP['id_filial'] . "\">" . strtoupper(utf8_encode($RFP["endereco"])) . "</div>";
          $row[] = "<div id=\"txtNumero_" . $RFP['id_filial'] . "\">" . strtoupper(utf8_encode($RFP["numero"])) . "</div>";
          $row[] = "<div id=\"txtBairro_" . $RFP['id_filial'] . "\">" . strtoupper(utf8_encode($RFP["bairro"])) . "</div>";
          $row[] = "<div id=\"txtCidade_" . $RFP['id_filial'] . "\">" . strtoupper(utf8_encode($RFP["cidade_nome"])) . "</div>";
          $row[] = "<div id=\"txtEstado_" . $RFP['id_filial'] . "\">" . strtoupper(utf8_encode($RFP["estado"])) . "</div>";
          $row[] = "<div id=\"txtCep_" . $RFP['id_filial'] . "\">" . strtoupper(utf8_encode($RFP["cep"])) . "</div>";
          $row[] = "<div id=\"txtTelefone_" . $RFP['id_filial'] . "\">" . strtoupper(utf8_encode($RFP["telefone"])) . "</div>";
          $row[] = "<div id=\"txtSite_" . $RFP['id_filial'] . "\">" . strtoupper(utf8_encode($RFP["site"])) . "</div>";
          $row[] = "<center>" . $btnEst . "</center>";
        }
        $records['aaData'][] = $row;
    }
    $records["iTotalRecords"] = $cont;
    $records["iTotalDisplayRecords"] = $cont;
    echo json_encode($records);
}

if (isset($_POST['AtualizaFilial'])) {
    $query = "UPDATE sf_filiais SET " .
            "descricao = UPPER('" . utf8_decode($_POST['txtDescricao']) . "'), " .
            "razao_social_contrato = UPPER('" . utf8_decode($_POST['txtRazao']) . "'), " .
            "nome_fantasia_contrato = UPPER('" . utf8_decode($_POST['txtFantasia']) . "'), " .
            "cnpj = UPPER('" . utf8_decode($_POST['txtCnpj']) . "'), " .
            "inscricao_estadual = UPPER('" . utf8_decode($_POST['txtInscricao']) . "'), " .
            "endereco = UPPER('" . utf8_decode($_POST['txtEndereco']) . "'), " .
            "numero = UPPER('" . utf8_decode($_POST['txtNumero']) . "'), " .
            "bairro = UPPER('" . utf8_decode($_POST['txtBairro']) . "'), " .
            "cidade_nome = UPPER('" . utf8_decode($_POST['txtCidade']) . "'), " .
            "estado = UPPER('" . utf8_decode($_POST['txtEstado']) . "'), " .
            "cep = UPPER('" . utf8_decode($_POST['txtCep']) . "'), " .
            "telefone = UPPER('" . utf8_decode($_POST['txtTelefone']) . "'), " .
            "site = UPPER('" . utf8_decode($_POST['txtSite']) . "') " .
            "WHERE id_filial = " . $_POST['AtualizaFilial'];
    //echo $query;
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}
odbc_close($con);