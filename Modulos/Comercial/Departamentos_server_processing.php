<?php

require_once(__DIR__ . '/../../Connections/configini.php');
$aColumns = array('id_departamento', 'nome_departamento');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY nome_departamento asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";

    if ($_GET['iSortCol_0'] == 0) {
        $sOrder = " order by nome_departamento " . $_GET['sSortDir_0'] . ", ";
    }
    if ($_GET['iSortCol_0'] == 1) {
        $sOrder = " order by tipo_departamento " . $_GET['sSortDir_0'] . ", ";
    }

    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY id_contas_movimento asc";
    }
}

if ($_GET['txtBusca'] != "") {
    $sWhereX = $sWhereX . " and (nome_departamento like '%" . $_GET['txtBusca'] . "%')";
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
        $sWhere .= $aColumns[$i] . " AND LIKE '%" . utf8_decode($_GET['sSearch_' . $i]) . "%' ";
    }
}

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,* 
            FROM sf_departamentos WHERE id_departamento > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1=" . $imprimir . " ";
$cur = odbc_exec($con, $sQuery1);

$sQuery = "SELECT COUNT(*) total FROM sf_departamentos WHERE id_departamento > 0 $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total FROM sf_departamentos WHERE id_departamento > 0 $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    if ($aRow['tipo_departamento'] == "0") {
        $tot_adm = "Atendimento";
    } elseif ($aRow['tipo_departamento'] == "1") {
        $tot_adm = "Ordem de Serviço";
    } elseif ($aRow['tipo_departamento'] == "2") {
        $tot_adm = "Ambos";
    }
    $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(2," . utf8_encode($aRow[$aColumns[0]]) . ")'><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[1]]) . "'>" . utf8_encode($aRow[$aColumns[1]]) . "</div></a>";
    $row[] = "<center><div id='formPQ' title='" . $tot_adm . "'>" . $tot_adm . "</div></center>";
    if ($imprimir == 0) {
        $row[] = "<center><a title='Excluir' onClick=\"return confirm('Deseja deletar esse registro?')\" href='Departamentos.php?Del=" . $aRow[$aColumns[0]] . "'><input name='' type='image' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
    }
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);