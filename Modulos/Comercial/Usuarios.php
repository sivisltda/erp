<?php include '../../Connections/configini.php'; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>        
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Administrativo<small>Usuários</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="float:left;width: 10%;margin-top: 15px;">
                                    <?php if ($ckb_adm_cli_read_ == 0) { ?>
                                        <button class="button button-green btn-primary" type="button" onClick="AbrirBox(0, 0)"><span class="ico-file-4 icon-white"></span></button>
                                    <?php } ?>
                                    <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir()" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                </div>
                                <div style="float:right;width: 60%;display: flex;align-items: center;justify-content: flex-end;">
                                    <div style="float:left;margin-left: 1%;">                                    
                                        <div width="100%">Tipo:</div>
                                        <select id="txtTipo" class="select">
                                            <option value="null">Selecione</option>  
                                            <option value="E" selected>Funcionários</option>
                                            <option value="C">Clientes</option>
                                            <option value="P">Prospects</option>
                                            <option value="I">Indicadores</option>
                                            <option value="0">Sem Responsável</option>
                                        </select>                                           
                                    </div>                                                                           
                                    <div style="float:left;margin-left: 1%;">                                    
                                        <div width="100%">Estado:</div>
                                        <select id="txtStatus" class="select">
                                            <option value="0" selected>Ativos</option>                                        
                                            <option value="1">Inativos</option>                                                                            
                                        </select>                                           
                                    </div>                                                                           
                                    <div style="float:left;margin-left: 1%;">                                    
                                        <div width="100%">Perfil de acesso:</div>
                                        <select name="txtPerfil" id="txtPerfil" class="select">
                                            <option value="null">Selecione</option>
                                            <option value="0">Administrador</option>
                                            <?php $cur = odbc_exec($con, "select id_permissoes,nome_permissao from sf_usuarios_permissoes") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id_permissoes'] ?>"><?php echo utf8_encode($RFP['nome_permissao']) ?></option>
                                            <?php } ?>
                                        </select>                                          
                                    </div>                                                                           
                                    <div style="float:left;margin-left: 1%;">                                    
                                        <div width="100%">Departamento:</div>
                                        <select name="txtDepartamento" id="txtDepartamento" class="select">
                                            <option value="null">Selecione</option>
                                            <?php $cur = odbc_exec($con, "select id_departamento,nome_departamento from sf_departamentos") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id_departamento'] ?>"><?php echo utf8_encode($RFP['nome_departamento']) ?></option>
                                            <?php } ?>
                                        </select>                                          
                                    </div>                                                                           
                                    <div style="float:left;margin-left: 1%;width: 56%;">
                                        <div width="100%">Busca:</div>
                                        <input id="txtBusca" maxlength="100" type="text" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title=""/>
                                    </div>
                                    <div style="float:left;margin-left: 1%;width: 7%;">
                                        <div width="100%" style="opacity:0;">Busca:</div>
                                        <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Usuários</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblLista">
                            <thead>
                                <tr>
                                    <th width="3%">Foto</th>
                                    <th width="25%">Nome</th>
                                    <th width="10%">Dt.Cadastro</th>
                                    <th width="12%">Usuário</th>
                                    <th width="12%">E-mail</th>
                                    <th width="10%">Departamento</th>
                                    <th width="13%"><center>Perfil de acesso</center></th>
                                    <th width="12%">Ult.Acesso</th>
                                    <th width="3%"><center>Ação</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div> 
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script> 
        <script type="text/javascript" src="../../js/util.js"></script>                      
        <script type="text/javascript">
            $(document).ready(function () {
                $("#btnfind").click(function () {
                    tbLista.fnReloadAjax(finalFind(0));
                });                                
            });

            function finalFind(imp) {
                var retPrint = (imp === 1 ? '&imp=1' : '?imp=0');                
                if ($("#txtTipo").val() !== "null") {
                    retPrint = retPrint + '&tp=' + $("#txtTipo").val();
                }
                if ($("#txtStatus").val() !== "null") {
                    retPrint = retPrint + '&at=' + $("#txtStatus").val();
                }
                if ($("#txtPerfil").val() !== "null") {
                    retPrint = retPrint + '&pr=' + $("#txtPerfil").val();
                }
                if ($("#txtDepartamento").val() !== "null") {
                    retPrint = retPrint + '&dp=' + $("#txtDepartamento").val();
                }
                if ($("#txtBusca").val() !== "") {
                    retPrint = retPrint + '&txtBusca=' + $("#txtBusca").val();
                }                
                if ($("#txtSinc").val() > 0) {
                    retPrint = retPrint+'&errorMU='+$("#txtSinc").val()
                }
                return "Usuarios_server_processing.php" + retPrint.replace(/\//g, "_");
            }

            function AbrirBox(id, tl) {
                if(tl === 0) {
                    abrirTelaBox("FormUsuarios.php" + (id > 0 ? "?id=" + id : ""), 453, 600);
                }else if(tl === 1) {
                    abrirTelaBox("FormUsuariosSenha.php?id=" + id, 235, 380);                    
                }
            }

            function FecharBox() {
                $("#newbox").remove();
                tbLista.fnReloadAjax(finalFind(0));
            }

            function imprimir() {
                var pRel = "&NomeArq=" + "Usuários" +
                "&lbl=Nome|Dt.Cadastro|Usuário|E-mail|Departamento|Perfil de acesso" +
                "&siz=170|80|100|130|110|110" +
                "&pdf=0|7|8" + // Colunas do server processing que não irão aparecer no pdf
                "&filter=" + //Label que irá aparecer os parametros de filtro
                "&PathArqInclude=" + "../Modulos/Comercial/" + finalFind(1); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }

            var tbLista = $('#tblLista').dataTable({
                "iDisplayLength": 20,
                "aLengthMenu": [20, 30, 40, 50, 100],
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "aaSorting": [[1,'asc']],
                "aoColumns": [
                    {"bSortable": false},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": false},
                    {"bSortable": false}],
                "sAjaxSource": finalFind(0),
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"},
                "sPaginationType": "full_numbers"
            });
                        
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }
        </script>        
        <?php odbc_close($con); ?>
    </body>
</html>