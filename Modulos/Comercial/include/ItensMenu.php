<?php $mdl_seg_ = returnPart($_SESSION["modulos"], 14); ?>
<div class="allDropDown" style="display: flex; justify-content: space-between;">
    <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle  <?php echo $classDisable; ?>" type="button" data-toggle="dropdown">Cliente
            <span class="caret"></span></button>
        <ul class="dropdown-menu">
            <?php for ($x = 0; $x <= 0; $x++) { ?>
                <li><a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a></li>
            <?php } ?>            
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Nome</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 1; $x <= 3; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>            
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dados Comp.</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 24; $x <= 33; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                        <?php for ($x = 55; $x <= 59; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>                            
                    </li>
                </ul>
            </li>            
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dados Básicos</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 4; $x <= 16; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>                                                
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Endereço</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 17; $x <= 23; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>            
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Resp. Legal</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 34; $x <= 43; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>            
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Resp. Legal Comp.</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 44; $x <= 48; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>            
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Resp. Legal End.</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 49; $x <= 55; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>            
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dependente</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 179; $x <= 187; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>            
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dependente Comp.</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 188; $x <= 195; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>            
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dependente End.</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 196; $x <= 202; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>            
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dependente Outros</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 203; $x <= 209; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>                            
                    </li>
                </ul>
            </li>            
        </ul>
    </div>    
    <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle <?php echo $classDisable; ?>" type="button" data-toggle="dropdown">Plano
            <span class="caret"></span></button>
        <ul class="dropdown-menu">
            <?php for ($x = 60; $x <= 61; $x++) { ?>
                <li><a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a></li>
            <?php } ?>
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Datas</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 62; $x <= 65; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>                
            <?php for ($x = 66; $x <= 69; $x++) { ?>
                <li><a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a></li>
            <?php } ?>
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Valores</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 70; $x <= 73; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>                
            <?php for ($x = 74; $x <= 76; $x++) { ?>
                <li><a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a></li>
            <?php } ?>            
        </ul>        
    </div>                       
    <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle  <?php echo $classDisable; ?>" type="button" data-toggle="dropdown">Mensalidade
            <span class="caret"></span></button>
        <ul class="dropdown-menu">
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Todas</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 91; $x <= 95; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "|NT|"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Todas Pagas</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 91; $x <= 95; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "|NP|"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Todas em Aberto</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 91; $x <= 95; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "|NA|"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Todas Vencidas</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 91; $x <= 95; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "|NV|"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Boletos</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 143; $x <= 147; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle <?php echo $classDisable; ?>" type="button" data-toggle="dropdown">Outros
            <span class="caret"></span></button>
        <ul class="dropdown-menu">
            <?php for ($x = 89; $x <= 90; $x++) { ?>
                <li><a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a></li>
            <?php } ?>                                                  
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Cartão</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 96; $x <= 99; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>                                    
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Campos Livres</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 124; $x <= 133; $x++) { 
                            if (strlen($dicionario[$x][2]) > 0) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2]; ?></a>
                        <?php }} ?>
                    </li>
                </ul>
            </li>
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Turmas</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 134; $x <= 142; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>            
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Agendamento</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 148; $x <= 158; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>            
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Venda</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 213; $x <= 217; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>            
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Assinaturas</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 210; $x <= 212; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>            
        </ul>
    </div>        
    <?php if ($mdl_seg_ > 0) { ?>
    <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle  <?php echo $classDisable; ?>" type="button" data-toggle="dropdown">Veículo
            <span class="caret"></span></button>
        <ul class="dropdown-menu">
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Principal</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 100; $x <= 113; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>            
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Outros</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 114; $x <= 123; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>            
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Evento</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 218; $x <= 226; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>                           
            <li class="menu-item dropdown dropdown-submenu" style="width:78%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Vistoria</a>
                <ul class="dropdown-menu submenu">
                    <li>
                        <?php for ($x = 227; $x <= 227; $x++) { ?>
                            <a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a>
                        <?php } ?>
                    </li>
                </ul>
            </li>                           
        </ul>
    </div>
    <?php } ?>             
    <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle <?php echo $classDisable; ?>" type="button" data-toggle="dropdown">Empresa
            <span class="caret"></span></button>
        <ul class="dropdown-menu">
            <?php for ($x = 77; $x <= 88; $x++) { ?>
                <li><a href="#" onclick="variable('<?php echo "||" . $dicionario[$x][0] . "||"; ?>')"><?php echo $dicionario[$x][2] ?></a></li>
            <?php } ?>                                                                            
        </ul>
    </div>    
</div>
