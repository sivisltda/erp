<?php
include "../../Connections/configini.php";
include "../../util/util.php";

function resize($img, $w, $h, $nome) {
    if (!extension_loaded('gd') && !extension_loaded('gd2')) {
        trigger_error("GD não foi carregado", E_USER_WARNING);
        return false;
    }
    $imgInfo = getimagesize($img);
    switch ($imgInfo[2]) {
        case 1: $im = imagecreatefromgif($img);
            break;
        case 2: $im = imagecreatefromjpeg($img);
            break;
        case 3: $im = imagecreatefrompng($img);
            break;
        default: trigger_error('Formato não suportado!', E_USER_WARNING);
            break;
    }
    $ajuste = 0;
    if ($imgInfo[0] <= $w && $imgInfo[1] <= $h) {
        $nHeight = $imgInfo[1];
        $nWidth = $imgInfo[0];
    } else {
        if ($w / $imgInfo[0] > $h / $imgInfo[1]) {
            $nWidth = $w;
            $nHeight = $imgInfo[1] * ($w / $imgInfo[0]);
        } else {
            $nWidth = $imgInfo[0] * ($h / $imgInfo[1]);
            $nHeight = $h;
            $ajuste = (167 - $nWidth) / 2;
        }
    }

    $nWidth = round($nWidth);
    $nHeight = round($nHeight);
    $newImg = imagecreatetruecolor(167, 194);
    imagealphablending($newImg, false);
    imagesavealpha($newImg, true);
    $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
    imagefilledellipse($newImg, 0, 0, 167, 194, $transparent);
    imagefilledrectangle($newImg, 0, 0, 167, 194, $transparent);
    imagecopyresampled($newImg, $im, $ajuste, 0, 0, 0, $nWidth, $nHeight, $imgInfo[0], $imgInfo[1]);
    switch (3) {
        case 1: imagegif($newImg, $nome);
            break;
        case 2: imagejpeg($newImg, $nome);
            break;
        case 3: imagepng($newImg, $nome);
            break;
        default: trigger_error('Falhou ao redimensionar a imagem!', E_USER_WARNING);
            break;
    }
    return $nome;
}

$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    $ckb_aut = "false";
    $ckb_inativo = "0";
    $id_find = 0;
    if (utf8_decode($_POST['ckbEmailAuth']) == "true") {
        $ckb_aut = "true";
    }
    if (utf8_decode($_POST['ckbInativo']) == "true") {
        $ckb_inativo = "1";
    }
    $cur = odbc_exec($con, "select id_usuario from sf_usuarios where login_user = '" . utf8_decode($_POST['txtUsuario']) . "' and id_usuario not in (" . valoresNumericos("txtId") . ")");
    while ($RFP = odbc_fetch_array($cur)) {
        $id_find = utf8_encode($RFP['id_usuario']);
    }
    if ($id_find == 0) {
        if ($_POST['txtId'] != '') {
            $cur = odbc_exec($con, "select * from sf_usuarios where id_usuario =" . $_POST['txtId']);
            while ($RFP = odbc_fetch_array($cur)) {
                $usuario_ant = utf8_encode($RFP['login_user']);
                $inativo_ant = utf8_encode($RFP['inativo']);
            }
            $queryLog = "";
            $queryLog .= $usuario_ant !== utf8_decode($_POST['txtUsuario']) ? " login_user_ant: " . $usuario_ant : "";
            $queryLog .= $inativo_ant !== $ckb_inativo ? " inativo_ant: " . $inativo_ant : "";

            $img = '';
            $allowedExts = array("gif", "jpeg", "jpg", "png");
            $extension = explode(".", $_FILES["file"]["name"]);
            $extension = $extension[count($extension) - 1];
            if ((($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/pjpeg") || ($_FILES["file"]["type"] == "image/x-png") || ($_FILES["file"]["type"] == "image/png")) && ($_FILES["file"]["size"] < 2000000) && in_array($extension, $allowedExts)) {
                if ($_FILES["file"]["error"] > 0) {
                    echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
                } else {
                    $nomeImg = $_POST['txtId'] . ".png";
                    move_uploaded_file($_FILES["file"]["tmp_name"], "./../../Pessoas/" . $contrato . "/" . $nomeImg);
                    resize("./../../Pessoas/" . $contrato . "/" . $nomeImg, 167, 194, "./../../Pessoas/" . $contrato . "/" . $nomeImg);
                    $img = ",imagem = '" . $nomeImg . "'";
                }
            }            
            $conditionalADM_MU =  ($_SESSION["id_usuario"] == 1 ? "adm_mu = " . valoresCheck('ckbAdmMu') . "," : "");            
            $query = "update sf_usuarios set " .
                    "login_user = '" . utf8_decode($_POST['txtUsuario']) . "'," .
                    "nome = '" . utf8_decode($_POST['txtNome']) . "'," .
                    "master = " . utf8_decode($_POST['txtAdmin']) . "," .
                    "email = '" . utf8_decode($_POST['txtEmail']) . "'," .
                    "assinatura = '" . utf8_decode($_POST['ckeditor']) . "'," .
                    "email_desc = '" . utf8_decode($_POST['txtEmailDesc']) . "'," .
                    "email_host = '" . utf8_decode($_POST['txtEmailHost']) . "'," .
                    "email_porta = '" . utf8_decode($_POST['txtEmailPorta']) . "'," .
                    "email_login = '" . utf8_decode($_POST['txtEmailLogin']) . "'," .
                    "email_senha = '" . utf8_decode($_POST['txtEmailSenha']) . "'," .
                    "email_ckb = '" . $ckb_aut . "'," . 
                    "email_ssl = " . valoresSelect('ckbSSL') . "," .
                    "email_smtp = " . valoresCheck('ckbSmtp') . "," .
                    $conditionalADM_MU .
                    "inativo = " . $ckb_inativo . "," .
                    "copyMe = " . valoresCheck('ckbCopyMe') . "," .
                    "funcionario = " . valoresSelect('txtFunc') . "," .
                    "departamento = " . utf8_decode($_POST['txtDepartamento']) . "," .
                    "id_comissao_padrao = " . valoresSelect('txtComissao') . " " .
                    $img . " where id_usuario = " . $_POST['txtId'];
            odbc_exec($con, $query) or die(odbc_errormsg());
            if ($queryLog !== "") {
                $queryLog = "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
                ('sf_usuarios', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'A', 'ALTERACAO " . $queryLog . "', GETDATE(), null);";
                odbc_exec($con, $queryLog) or die(odbc_errormsg());
            }
            if (isset($_POST['items'])) {
                $query = "delete from sf_usuarios_filiais where id_usuario_f = " . $_POST['txtId'];
                odbc_exec($con, $query);
                for ($i = 0; $i < count($_POST['items']); $i++) {
                    $query = "insert into sf_usuarios_filiais(id_filial_f,id_usuario_f) values(" . $_POST['items'][$i] . "," . $_POST['txtId'] . ")";
                    odbc_exec($con, $query);
                }
            }            
        } else {            
            $query = "INSERT INTO sf_usuarios (login_user,senha,nome,master,email,funcionario,email_desc,email_host,email_porta,email_login,email_senha,email_ckb,email_ssl,email_smtp,imagem,departamento,adm_mu,inativo,copyMe,assinatura,filial,data_cadastro,id_comissao_padrao) VALUES('" .
            utf8_decode($_POST['txtUsuario']) . "'," .
            valoresTexto2(encrypt($_POST['txtSenha'], "VipService123", true)) . ",'" .
            utf8_decode($_POST['txtNome']) . "'," .
            utf8_decode($_POST['txtAdmin']) . ",'" .
            utf8_decode($_POST['txtEmail']) . "'," .
            valoresSelect('txtFunc') . ",'" .
            utf8_decode($_POST['txtEmailDesc']) . "','" .
            utf8_decode($_POST['txtEmailHost']) . "','" .
            utf8_decode($_POST['txtEmailPorta']) . "','" .
            utf8_decode($_POST['txtEmailLogin']) . "','" .
            utf8_decode($_POST['txtEmailSenha']) . "','" .
            $ckb_aut . "'," . valoresSelect('ckbSSL') . "," . valoresCheck('ckbSmtp') . ",'" .
            utf8_decode($_POST['textPhoto']) . "'," . 
            utf8_decode($_POST['txtDepartamento']) . ",'" . 
            valoresCheck('ckbAdmMu') . "','" .
            $ckb_inativo . "','" . 
            valoresCheck('ckbCopyMe') . "','" .
            valoresTexto('ckeditor') . "',1,getdate()," . 
            valoresSelect('txtComissao') . ")";   

            odbc_exec($con, $query) or die(odbc_errormsg());
            $res = odbc_exec($con, "select top 1 id_usuario from sf_usuarios order by id_usuario desc") or die(odbc_errormsg());
            $_POST['txtId'] = odbc_result($res, 1);

            if (isset($_POST['items'])) {
                for ($i = 0; $i < count($_POST['items']); $i++) {
                    $query = "insert into sf_usuarios_filiais(id_filial_f,id_usuario_f) values(" . $_POST['items'][$i] . "," . $_POST['txtId'] . ")";
                    odbc_exec($con, $query);
                }
            } else {
                $query = "select id_filial from dbo.sf_filiais";
                $res = odbc_exec($con, $query) or die(odbc_errormsg());
                $total = 0;
                $idloja = 0;
                while ($aRow = odbc_fetch_array($res)) {
                    $idloja = $aRow["id_filial"];
                    $total = $total + 1;
                }
                if ($total < 2) {
                    $query = "insert into sf_usuarios_filiais(id_usuario_f, id_filial_f) values ('" . $_POST['txtId'] . "','" . $idloja . "')";
                    odbc_exec($con, $query) or die(odbc_errormsg());
                }
            }
        }           
        $query = "update sf_usuarios set filial = isnull((select max(id_filial_f) from sf_usuarios_filiais where id_usuario_f = id_usuario),1)
        where id_usuario = '" . $_POST['txtId'] . "' and filial not in (select id_filial_f from sf_usuarios_filiais where id_usuario_f = id_usuario)";
        odbc_exec($con, $query) or die(odbc_errormsg());        
    } else {
        $nome = $_POST['txtNome'];
        $usuario = $_POST['txtUsuario'];
        $email = $_POST['txtEmail'];
        $funcionarioNome = $_POST['txtFuncNome'];
        $senha = $_POST['txtSenha'];
        $senha2 = $_POST['txtSenha2'];
        $admin = $_POST['txtAdmin'];
        $departamento = $_POST['txtDepartamento'];        
        echo "<script> (function() { setTimeout(function(){ bootbox.alert('Não é possível registrar esse usuário ".$nome.", usuário já cadastrado!', function(){  }); }, 2000);  })();</script>";                
    }
}

if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "UPDATE sf_usuarios set inativo = 1 WHERE id_usuario = " . $_POST['txtId']);
        $query = "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
        ('sf_usuarios', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'A', 'ALTERACAO USUARIO INATIVO: 1', GETDATE(), null);";
        odbc_exec($con, $query);    
        echo "<script> (function() { setTimeout(function(){ bootbox.alert('Registro inativo!', function(){ parent.FecharBox() }); }, 2000);  })();</script>";
    }
}

if (isset($_POST['Cancelar'])) {
    echo "<script>window.top.location.href = 'Usuarios.php';</script>";
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select *, sf_usuarios.inativo us_inativo, sf_usuarios.departamento us_departamento 
    from sf_usuarios left join sf_fornecedores_despesas on funcionario = id_fornecedores_despesas
    where id_usuario = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_usuario']);
        $nome = utf8_encode($RFP['nome']);
        $dt_cadastro = escreverData($RFP['data_cadastro']);
        $usuario = utf8_encode($RFP['login_user']);
        $admin = utf8_encode($RFP['master']);
        $imagemx = utf8_encode($RFP['imagem']);        
        $email = utf8_encode($RFP['email']);
        $email_desc = utf8_encode($RFP['email_desc']);
        $email_host = utf8_encode($RFP['email_host']);
        $email_porta = utf8_encode($RFP['email_porta']);
        $email_login = utf8_encode($RFP['email_login']);
        $email_senha = utf8_encode($RFP['email_senha']);
        $email_ckb = utf8_encode($RFP['email_ckb']);
        $email_ssl = utf8_encode($RFP['email_ssl']);
        $email_smtp = utf8_encode($RFP['email_smtp']);        
        $copyMe = utf8_encode($RFP['copyMe']);
        $mensagem = utf8_encode($RFP['assinatura']);                
        $departamento = utf8_encode($RFP['us_departamento']);
        $funcionario = utf8_encode($RFP['funcionario']);
        $descricao = utf8_encode($RFP['razao_social']);
        $chkadmMU = utf8_encode($RFP['adm_mu']);
        $mu_id = utf8_encode($RFP['mu_id']);
        $pendenciasMU = utf8_encode($RFP['pendencias_mu']);
        if ($RFP['nome_fantasia'] != "") {
            $descricao = $descricao . " (" . utf8_encode($RFP['nome_fantasia']) . ")";
        }
        $funcionarioNome = $descricao;        
        $inativo = utf8_encode($RFP['us_inativo']);
        $comissao = utf8_encode($RFP['id_comissao_padrao']);
    }
} else {
    $disabled = '';
    $id = '';
    //$nome = '';
    $dt_cadastro = getData("T");
    //$usuario = '';
    //$senha = '';
    //$senha2 = '';
    //$admin = '';
    $imagemx = '';    
    //$email = '';    
    $email_desc = '';
    $email_host = '';
    $email_porta = '';
    $email_login = '';
    $email_senha = '';
    $email_ckb = 'false';
    $email_ssl = 0;
    $email_smtp = 0;
    $copyMe = 0;
    $mensagem = '';    
    //$departamento = '';
    //$funcionario = '';
    //$funcionarioNome = '';
    $inativo = 0;
    $comissao = '';
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
if (isset($_POST['bntAlterar'])) {
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="./../../css/main.css" rel="stylesheet">
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<style media="screen">
    .ms-container .ms-selectable li.disabled, .ms-container .ms-selection li.disabled{
        pointer-events: none;
    }
    
    .frmfoot .frmbtn{
        display: flex;
        align-items: center;
        justify-content: flex-end;
        height: 100%;
    }
    
    .gabiSubmit{
        width: 0;
        margin: 0;
        padding: 0;
        pointer-events: none;
    }
    
    .boxCenter{
        display: flex;
        align-items: center;
        justify-content: center;
    }


</style>
<body>
    <form action="FormUsuarios.php" name="frmEnviaDados" id="frmEnviaDados" method="POST" enctype="multipart/form-data">
        <div class="frmhead">
            <div class="frmtext">Usuário</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="tabbable frmtabs">
            <ul class="nav nav-tabs" style="margin:0">
                <li class="active"><a href="#tab1" data-toggle="tab">Dados principais</a></li>
                <li><a href="#tab2" data-toggle="tab">Configuração de E-mail</a></li>
                <li><a href="#tab3" data-toggle="tab">Assinatura</a></li>
                <li><a href="#tab4" data-toggle="tab">Lojas</a></li>
                <li><a href="#tab5" data-toggle="tab">Log de Acessos</a></li>
            </ul>
            <div class="tab-content frmcont" style="height:264px">
                <div class="tab-pane active" id="tab1" style="overflow:hidden; height:264px">
                    <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/></td>
                    <div style="float: left;width: 20%">
                        <div>
                            <img src="<?php
                            if ($imagemx != '') {
                                echo "./../../Pessoas/" . $contrato . "/" . $imagemx;
                            } else {
                                echo "./../../img/dmitry_m.gif";
                            }
                            ?>" width="101" height="auto" />
                        </div>                                                
                    </div>
                    <div style="float: left;width: 80%">
                        <div style="display:flex">
                            <div style="width: 55%;float:left;">
                                <span style="width: 50%">Nome Completo:</span>
                                <input id="txtNome" name="txtNome" maxlength="75" required style="width:100%" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $nome; ?>"/>
                            </div>
                            <div style="width: 18%;float:left;margin-left: 1%;">
                                <span style="width: 50%">Dt.Cadastro:</span>
                                <input style="width:100%" type="text" disabled class="input-medium" value="<?php echo $dt_cadastro; ?>"/>
                            </div>
                            <div style="float:left;margin-left: 1%;width: 25%;display: flex;flex-direction: column;justify-content: center;">
                                <div style="display:flex;justify-content: space-between;">
                                    <span>Inativo:</span>
                                    <input name="ckbInativo" id="ckbInativo" type="checkbox" <?php
                                    if ($inativo == "1") {
                                        echo "checked";
                                    }
                                    ?> class="input-medium" <?php echo $disabled; ?> value="true" style="opacity: 0;"/>
                                </div>                                
                            </div>                       
                        </div>
                        <div style="clear:both;"></div>
                        <div style="width: 44%;float:left;">
                            <span>Usuário:</span>
                            <input id="txtUsuario" required style="width:100%" name="txtUsuario" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="128" value="<?php echo $usuario; ?>"/>
                        </div>                        
                        <?php if(!is_numeric($id)) { ?>
                        <div style="width: 27%;float:left;margin-left: 1%">                            
                            <span>Senha:</span>
                            <input id="txtSenha" required style="width:100%" name="txtSenha" type="password" <?php echo $disabled; ?> class="input-medium" maxlength="30" value="<?php echo $senha; ?>"/>
                        </div>
                        <div style="width: 27%;float:left;margin-left: 1%">
                            <span>Confirmar:</span>
                            <input id="txtSenha2" required style="width:100%" name="txtSenha2" type="password" <?php echo $disabled; ?> class="input-medium" maxlength="30" value="<?php echo $senha2; ?>"/>
                        </div>
                        <?php } ?>
                        <div style="clear:both;"></div>
                        <div style="width: 44%;float:left;">
                            <span>E-mail:</span>
                            <input id="txtEmail" name="txtEmail" maxlength="75" style="width:100%"  type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $email; ?>"/>
                        </div>
                        <div style="width: 55%;float:left;margin-left: 1%">
                            <span>Funcionário/Indicador:</span>
                            <input type="hidden" id="txtFunc" name="txtFunc" value="<?php echo $funcionario;?>">                                     
                            <input type="text" id="txtFuncNome" name="txtFuncNome" style="width:100%" value="<?php echo $funcionarioNome;?>" <?php  echo $disabled; ?>/>                                                                                                                  
                        </div>
                        <div style="clear:both;"></div>
                        <div style="width: 44%;float:left;">
                            <span>Perfil de acesso:</span>
                            <select name="txtAdmin" <?php echo $disabled; ?> id="txtAdmin" class="input-medium" style="width:100%">
                                <option value="0" <?php
                                if ($admin == 0) {
                                    echo "SELECTED";
                                }
                                ?>>Administrador</option>
                                        <?php
                                        $cur = odbc_exec($con, "select id_permissoes,nome_permissao from sf_usuarios_permissoes ") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) {
                                            ?>
                                    <option value="<?php echo $RFP['id_permissoes'] ?>"<?php
                                    if (!(strcmp($RFP['id_permissoes'], $admin))) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP['nome_permissao']) ?></option>
                                        <?php } ?>
                            </select>
                        </div>
                        <div style="width: 55%;float:left;margin-left: 1%">
                            <span>Departamento:</span>
                            <select name="txtDepartamento" <?php echo $disabled; ?> id="txtDepartamento" class="input-medium" style="width:100%">
                                <option value="null">Selecione</option>
                                <?php
                                $cur = odbc_exec($con, "select id_departamento,nome_departamento from sf_departamentos ") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) {
                                    ?>
                                    <option value="<?php echo $RFP['id_departamento'] ?>"<?php
                                    if (!(strcmp($RFP['id_departamento'], $departamento))) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP['nome_departamento']) ?></option>
                                        <?php } ?>
                            </select>
                        </div>
                        <?php if ($comissao_forma_ > 0 && $ckb_com_forma_ > 0) { ?>
                        <div style="width: 44%;float:left;">
                            <span>Comissão padrão por Cliente:</span>
                            <select name="txtComissao" <?php echo $disabled; ?> id="txtComissao" class="input-medium" style="width:100%">
                                <option value="null">Selecione</option>
                                <?php $cur = odbc_exec($con, "select * from sf_comissao_cliente_padrao where inativo = 0 order by descricao") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP['id'] ?>"<?php
                                    if (!(strcmp($RFP['id'], $comissao))) {
                                        echo "SELECTED";
                                    }?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                <?php } ?>
                            </select>
                        </div> 
                        <?php if(is_numeric($id)) { ?>                        
                        <div style="width: 44%;float:left; margin-left: 1%;">
                            <button type="button" style="margin-top: 15px;" class="btn btn-primary" onClick="atualizarRegras()" <?php echo $disabled; ?>><span class="ico-refresh"></span> Atualizar Regras</button>
                        </div>                             
                        <?php }} ?>                        
                        <div style="clear:both;"></div>
                        <div style="width: 44%;float:left;">
                            <span>Foto:</span>
                            <input <?php echo $disabled; ?> type="file" name="file" id="file"/>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab2" style="overflow:hidden; height:264px">
                    <div style="width:100%;">
                        <span style="width: 50%">Descrição:</span>
                        <input id="txtEmailDesc" name="txtEmailDesc" maxlength="100" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $email_desc; ?>"/>
                    </div>
                    <div style="clear:both;height: 5px;"></div>
                    <div style="width:50%;float: left">
                        <span style="width: 50%">Host (Servidor):</span>
                        <input id="txtEmailHost" name="txtEmailHost" maxlength="100" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $email_host; ?>"/>
                    </div>
                    <div style="width:19%;float: left;margin-left: 1%">
                        <span style="width: 50%">Porta:</span>
                        <input id="txtEmailPorta" name="txtEmailPorta" maxlength="5" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $email_porta; ?>"/>
                    </div>
                    <div style="width:29%;float: left;margin-left: 1%">
                        <span style="width: 50%">Segurança:</span>
                        <select id="ckbSSL" name="ckbSSL" style="width: 100%;" <?php echo $disabled; ?>>
                            <option value="0" <?php if ($email_ssl == "0") { echo "selected";}?>>Sem Protocolo</option>
                            <option value="1" <?php if ($email_ssl == "1") { echo "selected";}?>>SSL</option>
                            <option value="2" <?php if ($email_ssl == "2") { echo "selected";}?>>TLS</option>
                        </select>
                    </div>
                    <div style="clear:both;height: 5px;"></div>
                    <div style="width:50%;float: left">
                        <span style="width: 50%">Login do E-mail:</span>
                        <input id="txtEmailLogin" name="txtEmailLogin" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $email_login; ?>"/>
                    </div>
                    <div style="width:49%;float: left;margin-left: 1%">
                        <span style="width: 50%">Senha do E-mail:</span>
                        <input id="txtEmailSenha" name="txtEmailSenha" type="password" <?php echo $disabled; ?> class="input-medium" value="<?php echo $email_senha; ?>"/>
                    </div>
                    <div style="clear:both;height: 10px;"></div>
                    <div style="width:49%;float: left;">
                        <input name="ckbEmailAuth" id="ckbEmailAuth" <?php echo $disabled; ?> type="checkbox" <?php
                        if ($email_ckb == "true") {
                            echo "checked";
                        }
                        ?> class="input-medium" value="true" style="opacity:0"/>
                        <span style="width: 50%">Autenticação:</span>
                    </div>
                    <div style="clear:both;height: 5px;"></div>
                    <div style="width:49%;float: left;">
                        <input name="ckbSmtp" id="ckbSmtp" <?php echo $disabled; ?> type="checkbox" <?php
                        if ($email_smtp == "1") {
                            echo "checked";
                        }
                        ?> class="input-medium" value="1" style="opacity:0"/>
                        <span style="width: 50%">Desativar SMTP:</span>
                    </div>
                    <div style="clear:both;height: 5px;"></div>                     
                    <div style="width:49%;float: left;">
                        <input name="ckbCopyMe" id="ckbCopyMe" <?php echo $disabled; ?> type="checkbox" <?php
                        if ($copyMe == "1") {
                            echo "checked";
                        }
                        ?> class="input-medium" value="1" style="opacity:0"/>
                        <span style="width: 50%">Me enviar cópia:</span>
                    </div>                   
                </div>
                <div class="tab-pane" id="tab3" style="overflow:hidden;height:264px">
                    <div style="width:90%;float: left;">
                        <textarea id="ckeditor" name="ckeditor" <?php echo $disabled; ?> onkeydown="if (event.ctrlKey && event.keyCode === 86) {return false;}"> <?php echo $mensagem; ?></textarea>
                    </div>
                </div>
                <div class="tab-pane" id="tab4" style="overflow:hidden; height:264px">                   
                    <div style="width:100%;float: left;">
                        <select name="items[]" multiple="multiple" id="msc" multiselect-disabled="true">
                            <?php
                            $query = "select numero_filial,Descricao,id_usuario_f,id_filial, case when sf_usuarios_filiais.id_usuario_f in(id_usuario_f) then 'S' else 'N' end grp from sf_filiais
                            left join sf_usuarios_filiais on sf_filiais.id_filial = sf_usuarios_filiais.id_filial_f
                            and (sf_usuarios_filiais.id_usuario_f is null or sf_usuarios_filiais.id_usuario_f = '" . $PegaURL . "')
                            order by numero_filial";
                            $cur = odbc_exec($con, $query) or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP['id_filial']; ?>" <?php echo ($RFP['grp'] == 'S' ? "selected" : ""); ?>><?php echo utf8_encode($RFP['Descricao']); ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="tab-pane" id="tab5" style="overflow:hidden; height:264px">  
                    <div class="body" style="margin-left: 0px; padding: 0 0px">
                        <div class="content">
                            <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblLista">
                                <thead>
                                    <tr>
                                        <th width="20%">Dt.Cadastro</th>
                                        <th width="50%">Usuário</th>
                                        <th width="30%">IP</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            <div style="clear:both"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="button" title="Gravar" onclick="validar();" name="BtnSalvar" id="BtnSalvar" ><span class="ico-checkmark"></span> Gravar</button>
                    <button class="btn green gabiSubmit" type="submit" title="Gravar" name="bntSave" id="bntSave" ><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" title="Cancelar" onClick="parent.FecharBox()" id="bntCancelar"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" title="Cancelar" type="submit" name="bntAlterar" id="bntAlterar" ><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else { ?>
                    <button class="btn green" type="submit" title="Novo" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                    <button class="btn green" title="Alterar" type="submit" name="bntEdit" id="bntEdit" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                    <button class="btn red" type="button" name="btnDel" value="Excluir" onClick="confirmar()"><span class="ico-remove"></span> Excluir</button>
                    <button class="btn red gabiSubmit" type="submit" name="bntDelete" id="bntDelete" value="Excluir"><span class="ico-remove"></span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <div class="dialog" id="source" style="display: none;" title="Processando">
        <input id="txtTotalSucesso" type="hidden" value="0"/>
        <input id="txtTotalErro" type="hidden" value="0"/>        
        <div id="progressbar"></div>
        <div style="display: flex;align-items: center;justify-content: center;margin-top: 20px;">
            <div id="pgIni">0</div><div style="padding: 5px;">até</div><div id="pgFim">0</div>            
        </div>            
    </div>     
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/multiselect/jquery.multi-select.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery.filter_input.js'></script>
    <script type='text/javascript' src='../../js/plugins/ckeditor/ckeditor.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script> 
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>    
    <script type="text/javascript" src="../../js/util.js"></script>    
    <?php if($disabled!=''){
        if(!empty($pendenciasMU)){?>
            <script type="text/javascript">
                bootbox.alert('<b style="color:red; text-align:center;"><?php echo $pendenciasMU;?></b>');
            </script>
        <?}
    }?>
    <script type="text/javascript">
        var jaUsado = false;
        //$('#txtUsuario').filter_input({regex: '[a-zA-Z0-9_]'});
        var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3");
        $("#msc").multiSelect({
            selectableHeader: "<div class='multipleselect-header'>Selecione</div>",
            selectionHeader: "<div class='multipleselect-header'>Selecionados</div>"
        });
        $(document).ready(function () {
            var ckeditor = CKEDITOR.replace('ckeditor', {removePlugins: 'elementspath', height: 190});
            /*ckeditor.on('paste', function () {
                return false;
            }, ckeditor.element.$);*/
            ckeditor.config.width = 556;
            if ($('#txtNome').attr("disabled") === "disabled") {
                $('.ms-list').find('li').addClass("disabled");
            }
        });
        
        function atualizarRegras() {
            if ($("#txtComissao").val() === "null") {
                bootbox.alert("Selecione uma regra em comissão padrão por Cliente!");
            } else {
                bootbox.confirm('Confirma a alteração de regra padrão deste usuário?', function (result) {
                    if (result === true) {
                        $.post("./../Academia/form/ComissaoCliente.php", "btnListar=S&idUsuario=" + $("#txtId").val()).done(function (data) {
                            var response = jQuery.parseJSON(data);
                            $("#source").dialog({modal: true});
                            $("#progressbar").progressbar({value: 0});
                            $("#pgIni").html("0");
                            $("#pgFim").html(response.length);
                            $("#txtTotalSucesso, #txtTotalErro").val(0);
                            processarRegras(response);
                        });
                    }
                });
            }
        }
        
        function processarRegras(lista) {
            let PgIni = parseInt($("#pgIni").html());
            let PgFim = parseInt($("#pgFim").html());
            if ((PgIni < PgFim) && PgFim > 0 && $('#source').dialog('isOpen')) {
                $("#progressbar").progressbar({value: (((PgIni + 1) * 100) / PgFim)});
                $("#pgIni").html(PgIni + 1);
                $.ajax({
                    type: "POST", url: "./../Academia/form/ComissaoCliente.php",
                    data: "bntLinhaPadrao=S&txtId=" + lista[PgIni] + "&txtIdComissao=" + $("#txtComissao").val(),
                    success: function (data) {
                        if (data === "YES") {
                            $("#txtTotalSucesso").val(parseInt($("#txtTotalSucesso").val()) + 1);
                        } else {
                            $("#txtTotalErro").val(parseInt($("#txtTotalErro").val()) + 1);
                        }
                        processarRegras(lista);
                    },
                    error: function (error) {
                        bootbox.alert(error.responseText + "!");
                    }
                });
            } else {
                $("#source").dialog('close');
                bootbox.alert("Transações: " + $("#txtTotalSucesso").val() + " Aprovada(s) e " + $("#txtTotalErro").val() + " com erro(s)!");
            }
        }
        
        function confirmar(){
            bootbox.confirm("Deseja inativar esse registro?", function(result){ 
                if(result == true){
                    $( "#bntDelete" ).click();
                }
            });
        }
        
        function validar() {
            var error = '';
            if($("#txtNome").val() == ""){
                error = "Digite o nome";
            }else if($("#txtUsuario").val() == ""){
                error = "Digite o nome do usúario";
            }if($("#txtId").val() == ""){
                if($("#txtSenha").val().length < 6){
                    error = "Campo senha deve conter no minimo 6 caracteres";
                }else if ($("#txtSenha").val() !== $("#txtSenha2").val()) {
                    error = "As senhas devem ser iguais!";
                }
            }else if (parent.$("#txtLojaSel").children().length > 1) {
                if ($(".ms-selection .ms-elem-selection").length === 0) {
                    error = "Vincule uma loja ao usuário!";
                }
            }else if(parent.$('#txtLojaStatus').val() != ""){
                if($("#txtEmail").val() == ''){
                    error = "Campo E-mail é um campo Obrigatório!";
                }
            }             
            if (error === '' ) {                
                $("#bntSave").click();
                $("#BtnSalvar").hide();
            } else {
                bootbox.alert(error);
            }            
        }
        
        $("#txtFuncNome").autocomplete({ 
            source: function (request, response) {
                $.ajax({
                    url: "../../Modulos/CRM/ajax.php",
                    dataType: "json",
                    data: {q: request.term, t: "E','I"},
                    success: function (data) {
                        response(data);
                    }
                });
            }, minLength: 3,
            select: function (event, ui) {
                $("#txtFunc").val(ui.item.id);
                $("#txtFuncNome").val(ui.item.value);
            }
        }); 
        
        $("#txtFuncNome").blur(function() {
            if ($("#txtFuncNome").val() === "") {
                $("#txtFunc").val("");
            }
        });
        
        var tbLista = $('#tblLista').dataTable({
            "iDisplayLength": 5,
            "aLengthMenu": [5, 10],
            "bProcessing": true,
            "bServerSide": true,
            "bFilter": false,
            "aaSorting": [[1,'asc']],
            "aoColumns": [
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false}],
            "sAjaxSource": "Usuarios_log_server_processing.php?usr=" + $("#txtUsuario").val(),
            'oLanguage': {
                'oPaginate': {
                    'sFirst': "Primeiro",
                    'sLast': "Último",
                    'sNext': "Próximo",
                    'sPrevious': "Anterior"
                }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                'sLengthMenu': "Visualização de _MENU_ registros",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sSearch': "Pesquisar:",
                'sZeroRecords': "Não foi encontrado nenhum resultado"},
            "sPaginationType": "full_numbers"
        });

    </script>
    <?php odbc_close($con); ?>
</body>
