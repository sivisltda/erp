<?php include '../../Connections/configini.php'; ?>
<link rel="icon" type="image/ico" href="../../../favicon.ico"/>
<link href="../../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../../css/main.css" rel="stylesheet">
<link href="../../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' src='../../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <div class="row-fluid">
        <form method="POST" id="frmEnviaDados" action="FormUsuariosSenha.php">
            <div class="frmhead">
                <div class="frmtext">Alterar Senha</div>
                <div class="frmicon" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input id="txtId" name="txtId" value="<?php echo $_GET['id']; ?>" type="hidden"/>
                <div style="float: left; width: 99%;">
                    <span>Senha:</span>
                    <input id="txtSenha" required style="width:100%" name="txtSenha" type="password" class="input-medium" maxlength="30" value=""/>
                </div>                
                <div style="float: left; width: 99%;">
                    <span>Confirmar:</span>
                    <input id="txtSenha2" required style="width:100%" name="txtSenha2" type="password" class="input-medium" maxlength="30" value=""/>
                </div>                                
                <div style="clear:both;"></div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <button class="btn btn-success" type="button" onClick="salvar();" id="btnGravarPlano" title="Gravar"><span class="ico-checkmark"></span> Gravar</button>
                    <button class="btn yellow" title="Cancelar" onClick="parent.FecharBox()"><span class="ico-reply"></span> Cancelar</button>
                </div>
            </div>
        </form>
    </div>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../../js/plugins.js'></script>
    <script type="text/javascript" src="../../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type='text/javascript' src='../../../js/plugins/datatables/fnReloadAjax.js'></script>
    <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script> 
    <script type='text/javascript' src='../../../js/jquery.priceformat.min.js'></script>
    <script type="text/javascript" src="../../js/util.js"></script>
    <script type="text/javascript">

        function validar() {
            if ($('#txtSenha').val() === "" || $('#txtSenha').val().length < 6) {
                bootbox.alert("Preencha o campo senha com um valor válido, Mínimo de 6 caracteres!");
                return false;            
            } else if ($('#txtSenha').val() !== $('#txtSenha2').val()) {
                bootbox.alert("Preencha o campo senha com um valor válido, Senhas não conferem!");
                return false;
            }
            return true;
        }

        function salvar() {
            if (validar()) {
                $.post("form/UsuariosFormServer.php", "btnSave=S&" + 
                $("#frmEnviaDados").serialize()).done(function (data) {
                    if (data === "YES") {
                        parent.FecharBox();
                    } else {
                        bootbox.alert("Erro: " + data);
                    }
                });
            }
        }
    </script>
    <?php odbc_close($con); ?>
</body>
