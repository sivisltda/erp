<?php
include "../../Connections/configini.php";
include "form/Gerenciador-AgendaFormServer.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <style>
            body { font-family: sans-serif; }
            div label input {
                margin-right: 100px;
            }
            label {
                float: left;
                width: 26px;
                cursor: pointer;
                background: #EFEFEF;
                margin: 2px 0 2px 1px;
                border: 1px solid #D0D0D0;
            }
            label span {
                display: block;
                padding: 3px 0;
                text-align: center;
            }
            label input {
                top: -20px;
                position: absolute;
            }
            input:checked + span {
                color: #FFF;
                background: #186554;
            }
        </style>
    </head>
    <body>
        <form action="Gerenciador-AgendaForm.php" name="frmEnviaDados" id="frmEnviaDados" method="POST">
            <div class="frmhead">
                <div class="frmtext">Agenda</div>
                <div class="frmicon" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input name="txtId" id="txtId" type="hidden" value="<?php echo $id; ?>"/>
                <div style="height:42px">
                    <div style="width: 58%; float: left">
                        <span>Descrição:</span>
                        <input name="txtDescricao" id="txtDescricao" type="text" style="width:100%" <?php echo $disabled; ?> value="<?php echo $descricao; ?>"/>
                    </div>
                    <div style="width: 20%; float: left; margin-left: 1%">
                        <span>N° Dias Online:</span>
                        <input name="txtDias" id="txtDias" type="text" maxlength="3" style="width:100%" <?php echo $disabled; ?> value="<?php echo $dias; ?>"/>
                    </div>                        
                    <div style="width: 20%; float: left; margin-left: 1%">
                        <span>Online:</span>
                        <select name="ckbOnline" id="ckbOnline" class="select" <?php echo $disabled; ?>>
                            <option value="0" <?php
                            if ($online == 0) {
                                echo "selected";
                            }
                            ?>>NÃO</option>
                            <option value="1" <?php
                            if ($online == 1) {
                                echo "selected";
                            }
                            ?>>SIM</option>
                        </select>
                    </div>                    
                </div>
                <div style="height:42px">
                    <div style="width: 58%; float: left">                        
                        <span>Horário:</span>
                        <select name="txtHorario" id="txtHorario" class="select" <?php echo $disabled; ?>>
                            <option value="null">Selecione</option>
                            <?php
                            $cur = odbc_exec($con, "select cod_turno, nome_turno from sf_turnos order by cod_turno") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP['cod_turno']; ?>"<?php
                                if (!(strcmp($RFP['cod_turno'], $horario))) {
                                    echo "SELECTED";
                                }
                                ?>><?php echo utf8_encode($RFP['nome_turno']) ?></option>
                            <?php } ?>
                        </select>                    
                    </div>
                    <div style="width: 20%; float: left; margin-left: 1%">
                        <span>Inativo:</span>
                        <select name="ckbInativo" id="ckbInativo" class="select" <?php echo $disabled; ?>>
                            <option value="0" <?php
                            if ($inativo == 0) {
                                echo "selected";
                            }
                            ?>>NÃO</option>
                            <option value="1" <?php
                            if ($inativo == 1) {
                                echo "selected";
                            }
                            ?>>SIM</option>
                        </select>
                    </div>
                    <div style="width: 20%; float: left; margin-left: 1%">
                        <span>Sem Regras?</span>
                        <select name="ckbExcecao" id="ckbExcecao" class="select" <?php echo $disabled; ?>>
                            <option value="0" <?php
                            if ($excecao == 0) {
                                echo "selected";
                            }
                            ?>>NÃO</option>
                            <option value="1" <?php
                            if ($excecao == 1) {
                                echo "selected";
                            }
                            ?>>SIM</option>
                        </select>
                    </div>                    
                </div>
                <div style="height:42px">
                    <div style="width: 58%; float: left">                        
                        <span>Usuário:</span>
                        <select name="txtUsuario" id="txtUsuario" class="select" <?php echo $disabled; ?>>
                            <option value="null">Selecione</option>
                            <?php
                            $cur = odbc_exec($con, "select id_usuario,nome from sf_usuarios where inativo = 0 and id_usuario > 1 order by nome") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) {
                                ?>
                                <option value="<?php echo $RFP['id_usuario']; ?>"<?php
                                if (!(strcmp($RFP['id_usuario'], $usuario))) {
                                    echo "SELECTED";
                                }
                                ?>><?php echo utf8_encode($RFP['nome']) ?></option>
                                    <?php } ?>
                        </select>                    
                    </div>                                        
                    <div style="width: 41%; float: left; margin-left: 1%">                        
                        <span>Perfil de acesso:</span>
                        <select name="txtPermissoes" id="txtPermissoes" class="select" <?php echo $disabled; ?>>
                            <option value="null">Selecione</option>
                            <?php
                            $cur = odbc_exec($con, "select id_permissoes,nome_permissao from sf_usuarios_permissoes order by id_permissoes") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) {
                                ?>
                                <option value="<?php echo $RFP['id_permissoes']; ?>"<?php
                                if (!(strcmp($RFP['id_permissoes'], $id_permissoes))) {
                                    echo "SELECTED";
                                }
                                ?>><?php echo utf8_encode($RFP['nome_permissao']) ?></option>
                                    <?php } ?>
                        </select>                    
                    </div>                                                                                
                </div>
                <div style="height:42px">
                    <div style="width: 79%; float: left">                        
                        <span>Serviço para Pagamento:</span>
                        <select name="txtServico" id="txtServico" class="select" <?php echo $disabled; ?>>
                            <option value="null">Selecione</option>
                            <?php
                            $cur = odbc_exec($con, "select conta_produto, descricao from sf_produtos where tipo = 'S' and inativa = 0 and ativar_convite = 1 order by descricao") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP['conta_produto']; ?>"<?php
                                if (!(strcmp($RFP['conta_produto'], $servico))) {
                                    echo "SELECTED";
                                }
                                ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                            <?php } ?>
                        </select>                    
                    </div>
                    <div style="width: 20%; float: left; margin-left: 1%">
                        <span>Pg. Obrigatório:</span>
                        <select name="ckbPgObrig" id="ckbPgObrig" class="select" <?php echo $disabled; ?>>
                            <option value="0" <?php
                            if ($pgObrig == 0) {
                                echo "selected";
                            }
                            ?>>NÃO</option>
                            <option value="1" <?php
                            if ($pgObrig == 1) {
                                echo "selected";
                            }
                            ?>>SIM</option>
                        </select>
                    </div>                                        
                </div>                
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php
                    if ($disabled == "") {
                        if ($_POST["txtId"] == "") {
                            $btnType = "onClick=\"parent.FecharBox()\"";
                        } else {
                            $btnType = "type=\"submit\" name=\"bntAlterar\"";
                        }
                        ?>
                        <button class="btn green" type="submit" name="bntSave" onClick="return validaForm()"><span class="ico-checkmark"></span> Gravar</button>
                        <button class="btn yellow" title="Cancelar" <?php echo $btnType; ?>><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn green" type="submit" name="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" type="submit" name="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                        <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
        </form>
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>        
    <script type='text/javascript' src='js/AgendasForm.js'></script>    
    <?php odbc_close($con); ?>    
    </body>        
</html>