<?php include "../../Connections/configini.php"; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>        
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <style>
            h4 {
                margin: 10px 0px 0px 15px;
            }
            .columnX {
                float: left; 
                border-radius: 12px; 
                background-color: #f1f2f4; 
                width: 260px;
                margin-right: 1%;
            }
            .column {
                width: 100%;
                float: left;
                background-color: #f1f2f4;
                min-height: 50px;
                border-radius: 12px;
            }
            .portlet {
                margin: 1em 1em 1em 1em;
                padding: 0.3em;
            }
            .portlet-header {
                padding: 0.2em 0.3em;
                margin-bottom: 0.5em;
                position: relative;
            }
            .portlet-toggle {
                position: absolute;
                top: 50%;
                right: 0;
                margin-top: -8px;
            }
            .portlet-content {
                padding: 0.4em;
            }
            .portlet-placeholder {
                border: 1px dotted black;
                margin: 1em 1em 1em 1em;
                height: 50px;
            }
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>CRM<small>Disparos Automáticos</small></h1>
                    </div>  
                    <div class="columnX">
                        <h4>PLANEJADO</h4>
                        <div class="column">
                            <div class="portlet">
                                <div class="portlet-header">Feeds</div>
                                <div class="portlet-content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
                            </div>
                            <div class="portlet">
                                <div class="portlet-header">News</div>
                                <div class="portlet-content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
                            </div>
                        </div>                        
                    </div>
                    <div class="columnX">
                        <h4>PRIORIZADO</h4>
                        <div class="column">
                            <div class="portlet">
                                <div class="portlet-header">Shopping</div>
                                <div class="portlet-content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
                            </div>
                        </div>
                    </div>
                    <div class="columnX">
                        <h4>EM PROGRESSO</h4>
                        <div class="column">
                            <div class="portlet">
                                <div class="portlet-header">Links</div>
                                <div class="portlet-content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
                            </div>
                        </div>
                    </div>
                    <div class="columnX">
                        <h4>EM TESTE</h4>
                        <div class="column">                            
                        </div>
                    </div>
                    <div class="columnX">
                        <h4>PRODUÇÃO</h4>
                        <div class="column">                        
                            <div class="portlet">
                                <div class="portlet-header">Images</div>
                                <div class="portlet-content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script> 
        <script type="text/javascript" src="../../js/util.js"></script> 
        <script>
            $(function () {
                $(".column").sortable({
                    connectWith: ".column",
                    handle: ".portlet-header",
                    cancel: ".portlet-toggle",
                    placeholder: "portlet-placeholder ui-corner-all"
                });
                $(".portlet").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
                .find(".portlet-header").addClass("ui-widget-header ui-corner-all")
                .prepend("<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");
                $(".portlet-toggle").on("click", function () {
                    var icon = $(this);
                    icon.toggleClass("ui-icon-minusthick ui-icon-plusthick");
                    icon.closest(".portlet").find(".portlet-content").toggle();
                });
            });
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>