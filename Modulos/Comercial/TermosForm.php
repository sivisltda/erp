<?php
include "../../Connections/configini.php";
include "form/TermosFormServer.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>        
    </head>
    <body>
        <form name="frmEnviaDados" id="frmEnviaDados" method="POST" onkeydown="if (event.ctrlKey && event.keyCode === 86) { return false;}" enctype="multipart/form-data">
            <div class="frmhead">
                <div class="frmtext">Cadastro de Termo</div>
                <div class="frmicon" onClick="parent.FecharTermos()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input name="txtId" id="txtId" type="hidden" value="<?php echo $id; ?>"/>
                <div style="width: 50%;float: left;">
                    <span>Grupo de Planos:</span>
                    <select name="txtGrupo" id="txtGrupo" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                        <option value="null">Selecione</option>
                        <?php $cur = odbc_exec($con, "select id_contas_movimento, descricao from sf_contas_movimento p where tipo = 'L' and inativa = 0 order by descricao") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option value="<?php echo $RFP['id_contas_movimento'] ?>"<?php
                            if (!(strcmp($RFP['id_contas_movimento'], $grupo))) {
                                echo "SELECTED";
                            }
                            ?>><?php echo utf8_encode($RFP['descricao']); ?></option>
                        <?php } ?>
                    </select>                
                </div>
                <div style="width: 24%;float: left; margin-left: 1%">
                    <span>Inativo:</span>
                    <select name="txtInativo" id="txtInativo" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                        <option value="0" <?php echo ($inativo == "0" ? "SELECTED" : "");?>>Não</option>
                        <option value="1" <?php echo ($inativo == "1" ? "SELECTED" : "");?>>Sim</option>
                    </select>                
                </div>
                <div style="width: 24%;float: left; margin-left: 1%">
                    <span>Obrigatório:</span>
                    <select name="txtObrigatorio" id="txtObrigatorio" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                        <option value="0" <?php echo ($obrigatorio == "0" ? "SELECTED" : "");?>>Não</option>
                        <option value="1" <?php echo ($obrigatorio == "1" ? "SELECTED" : "");?>>Sim</option>
                    </select>                
                </div>
                <div class="data-fluid">
                    <span>Termo:</span>
                    <textarea id="ckeditor" name="ckeditor" style="height: 400px;" onkeydown="if (event.ctrlKey && event.keyCode === 86) { return false;}" <?php echo $disabled; ?>>
                        <?php echo $descricao; ?>
                    </textarea>
                </div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == "") { ?>
                        <button class="btn green" type="submit" name="bntSave" onClick="return validaForm()"><span class="ico-checkmark"></span> Gravar</button>
                        <?php if ($_POST["txtId"] == "") { ?>
                            <button class="btn yellow" onClick="parent.FecharTermos()"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                            <button class="btn yellow" type="submit" name="bntAlterar"><span class="ico-reply"></span> Cancelar</button>
                            <?php
                        }
                    } else { ?>
                        <button class="btn green" type="submit" name="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" type="submit" name="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                        <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
        </form>
        <script type='text/javascript' src='../../js/plugins/jquery-ui-1.11.4/jquery-ui.min.js'></script>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/ckeditorAll/ckeditor.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type='text/javascript' src='js/TermosForm.js'></script>        
        <?php odbc_close($con); ?>
    </body>
</html>