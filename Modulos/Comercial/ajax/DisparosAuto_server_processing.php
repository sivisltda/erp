<?php

require_once('../../../Connections/configini.php');
require_once('../../../util/util.php');

$aColumns = array('id_disparo_item', 'descr_disparo_auto', 'legenda_disparo_item', 'tp_disparo_item',
    'dt_ultimo_disparo_item', 'bloqueio_disparo_item', 'dias_tolerancia_item', 'loja_disparo_auto');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY " . $aColumns[1] . " asc ";
$sOrder2 = " ORDER BY " . $aColumns[1] . " asc ";
$sLimit = 20;

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iColumns']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if ($sOrder == "ORDER BY  ") {
                $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_0'])], 0) . " " . $_GET['sSortDir_0'] . ", ";
                $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_0'])], 1) . " " . $_GET['sSortDir_0'] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
}

if ($_GET['Search'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= prepareCollum($aColumns[$i], 0) . " LIKE '%" . utf8_decode(str_replace("'", "", $_GET['Search'])) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,*
            from sf_disparo_auto_item A 
            inner join sf_disparo_auto B on A.id_disparo_auto_item = B.id_disparo_auto 
            where " . $aColumns[0] . " > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " " . $sOrder2;
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);

$sQuery = "SELECT COUNT(*) total from sf_disparo_auto_item A inner join sf_disparo_auto B on A.id_disparo_auto_item = B.id_disparo_auto where " . $aColumns[0] . " > 0 $sWhereX " . $sWhere;
//echo $sQuery; exit;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total from sf_disparo_auto_item A inner join sf_disparo_auto B on A.id_disparo_auto_item = B.id_disparo_auto where " . $aColumns[0] . " > 0 ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ($aRow = odbc_fetch_array($cur)) {
    $tpDisparo = "Email";
    if ($aRow[$aColumns[3]] == "S") {
        $tpDisparo = "SMS";
    } else if ($aRow[$aColumns[3]] == "W") {
        $tpDisparo = "Whatsapp";
    } else if ($aRow[$aColumns[3]] == "L") {
        $tpDisparo = "Ligação";
    }
    $row = array();
    $row[] = "<div id='formPQ' >" . utf8_encode($aRow[$aColumns[0]]) . "</div>";
    $row[] = "<div id='formPQ' >" . ($aRow[$aColumns[7]] == 0 ? "Todas" : str_pad($aRow[$aColumns[7]], 3, "0", STR_PAD_LEFT)) . "</div>";
    $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[$aColumns[0]]) . ")'><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[1]]) . "'>" . utf8_encode($aRow[$aColumns[1]]) . "</div></a>";
    $row[] = "<div id='formPQ' >" . utf8_encode(str_replace('@dias_tolerancia_item', $aRow[$aColumns[6]], $aRow[$aColumns[2]])) . "</div>";
    $row[] = "<div id='formPQ' >" . $tpDisparo . "</div>";
    $row[] = "<div id='formPQ' >" . escreverDataHora($aRow[$aColumns[4]]) . "</div>";
    $row[] = "<div id='formPQ' >" . ($aRow[$aColumns[5]] == 0 ? "Ativo" : "Inativo") . "</div>";
    $row[] = "<center><input name='' type='image' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' onClick=\"RemoverItem(" . $aRow[$aColumns[0]] . ")\"></center>";
    $output['aaData'][] = $row;
}
echo json_encode($output);
odbc_close($con);
