<?php

session_start();
$output = array("online" => "0", "img" => "", "data_file" => "", "pagina" => "", "estado" => "", "ciclo" => "", "mensagem" => "", "data" => "");

if (is_numeric($_SESSION["contrato"])) {
    require_once('../../../Connections/configSivis.php');
    $cur = odbc_exec($con2, "select wha_atv from ca_contratos where numero_contrato = '" . $_SESSION["contrato"] . "';");
    while ($RFP = odbc_fetch_array($cur)) {
        $output["online"] = utf8_encode($RFP['wha_atv']);
    }
    odbc_close($con2);

    $filename = "./../../../Pessoas/" . $_SESSION["contrato"] . "/Emitentes/";
    if (file_exists($filename . "qrCode.txt")) {
        $output["img"] = file_get_contents($filename . "qrCode.txt");
        $output["data_file"] = date("d/m/Y H:i:s", filemtime($filename . "qrCode.txt"));
    }

    if (file_exists($filename . "whatsapp.txt")) {
        $info = json_decode(str_replace(array("\n", "\r"), "", file_get_contents($filename . "whatsapp.txt")), true);
        $output["pagina"] = $info["pagina"];
        $output["estado"] = $info["estado"];
        $output["ciclo"] = $info["ciclo"];
        $output["mensagem"] = $info["mensagem"];
        $output["celular"] = $info["celular"];        
        $output["data"] = $info["data"];        
    }
}

echo json_encode($output);