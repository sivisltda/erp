<?php

if (!isset($_GET['pdf'])) {
    require_once('../../../Connections/configini.php');
    require_once('../../../util/util.php');
}

$aColumns = array('id_agenda', 'descricao_agenda', 'nome_turno', 'terminal', 'p.inativo inativo', 'nome', 'dias', 'excecao');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "where p." . $aColumns[0] . " > 0 ";
$sWhere = "";
$sOrder = " ORDER BY " . $aColumns[0] . " asc ";
$sOrder2 = " ORDER BY " . $aColumns[0] . " asc ";
$sLimit = 20;
$_GET['sSearch'] = $_GET['Search'];
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) < 1) {
                $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 0) . " " . $_GET['sSortDir_' . $i] . ", ";
                $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 1) . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY " . $aColumns[0] . " asc";
        $sOrder2 = " ORDER BY " . $aColumns[0] . " asc";
    }
}

if ($_GET['Search'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= prepareCollum($aColumns[$i], 0) . " LIKE '%" . utf8_decode($_GET['Search']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

if (is_numeric($_GET['status'])) {
    $sWhereX .= " and p.inativo = " . $_GET['status'];
}

$sQuery = "SELECT COUNT(*) total from sf_agenda p left join sf_usuarios on p.user_resp = sf_usuarios.id_usuario $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total from sf_agenda p left join sf_usuarios on p.user_resp = sf_usuarios.id_usuario $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,p.*,nome,nome_turno,nome_permissao
from sf_agenda p left join sf_usuarios on p.user_resp = sf_usuarios.id_usuario 
left join sf_turnos on p.id_turno = sf_turnos.cod_turno 
left join sf_usuarios_permissoes up on up.id_permissoes = p.user_permissao
" . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder2;
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<div id='formPQ'><center>" . utf8_encode($aRow[$aColumns[0]]) . "</center></div>";
    $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[$aColumns[0]]) . ",1)'><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[1]]) . "'>" . utf8_encode($aRow[$aColumns[1]]) . "</div></a>";
    $row[] = "<div id='formPQ'>" . utf8_encode($aRow[$aColumns[2]]) . "</div>";
    $row[] = "<div id='formPQ'>" . utf8_encode($aRow[$aColumns[6]]) . "</div>";    
    $row[] = "<div id='formPQ'><center>" . ($aRow["terminal"] == 1 ? "Sim" : "Não") . "</center></div>";    
    $row[] = "<div id='formPQ'>" . utf8_encode($aRow[$aColumns[5]]) . "</div>";
    $row[] = "<div id='formPQ'>" . utf8_encode($aRow["nome_permissao"]) . "</div>";
    $row[] = "<div id='formPQ'><center>" . ($aRow["inativo"] == 1 ? "Sim" : "Não") . "</center></div>";
    $row[] = "<div id='formPQ'><center>" . ($aRow["excecao"] == 1 ? "Sim" : "Não") . "</center></div>";
    if ($imprimir == 0) {
        $row[] = "<center><input name='' type='image' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' onClick=\"RemoverItem(" . $aRow[$aColumns[0]] . ")\" value='Enviar'></center>";
    }
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
