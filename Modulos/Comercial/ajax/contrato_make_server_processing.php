<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');
require_once(__DIR__ . '/../../../util/excel/exportaExcel.php');

if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        odbc_exec($con, "DELETE FROM sf_contratos_item WHERE id = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox(1);</script>"; exit();
    }
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => 0,
    "iTotalDisplayRecords" => 0,
    "aaData" => array()
);

if (isset($_GET['listItems'])) {
    $query = "select *, substring(conteudo,1,4000) conteudo1, substring(conteudo,4001,4000) conteudo2, substring(conteudo,8001,4000) conteudo3, substring(conteudo,12001,4000) conteudo4, substring(conteudo,16001,4000) conteudo5 from sf_contratos_item where id_contrato=" . $_GET["id_contrato"] . " order by ordem";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row[] = "<center><input type='radio' name='row' id='row' value='" . $RFP['id'] . "'/></center>";
        $row[] = "<center>" . ($RFP['tipo'] == "1" ? "Texto Livre" : "Tabela") . "</center>";
        $row[] = utf8_encode($RFP["conteudo1"] . $RFP["conteudo2"] . $RFP["conteudo3"] . $RFP["conteudo4"] . $RFP["conteudo5"]);
        $row[] = "<center style='display: flex;align-items: center;justify-content: center;'> <span class='ico-edit' style='font-size:23px;' onClick=\"addItem(" . $RFP['id'] . "," . $RFP['ordem'] . ")\"></span> <input name='' type='image' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' onClick=\"RemoverItem(" . $RFP['id'] . ")\" value='Enviar'></center>";
        $output['aaData'][] = $row;
        $i++;
    }
    $output['iTotalRecords'] = $i;
    $output['iTotalDisplayRecords'] = $i;
}

if (isset($_POST['reordernar'])) {
    $query = "select * from sf_contratos_item where id_contrato =" . $_POST["id_contrato"] . " and id=" . $_POST["id_item"];
    $cur = odbc_exec($con, $query) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['ordem'] == 1 && $_POST['op'] == 2) {
            return;
        }
        $ordem = $RFP['ordem'];
    }
    $query = "select COUNT(*) AS TOTAL from sf_contratos_item where id_contrato =" . $_POST["id_contrato"];
    $cur = odbc_exec($con, $query) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['TOTAL'] == $ordem && $_POST['op'] == 1) {
            return;
        }
    }
    if ($_POST['op'] == 1) {
        $next = $ordem + 1;
    } else {
        $next = $ordem - 1;
    }
    $query = "UPDATE sf_contratos_item set ordem = 0
    WHERE id_contrato = " . $_POST['id_contrato'] . " and id = " . $_POST['id_item'];
    odbc_exec($con, $query) or die(odbc_errormsg());
    $query = "UPDATE sf_contratos_item set ordem = " . $ordem . "
    WHERE id_contrato = " . $_POST['id_contrato'] . " and ordem = " . $next;
    odbc_exec($con, $query) or die(odbc_errormsg());
    $query = "UPDATE sf_contratos_item set ordem = " . $next . "
    WHERE id_contrato = " . $_POST['id_contrato'] . " and id = " . $_POST['id_item'];
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}
echo json_encode($output);
odbc_close($con);
