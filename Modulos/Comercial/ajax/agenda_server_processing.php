<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');
require_once(__DIR__ . '/../../../util/excel/exportaExcel.php');

$dia_semana = array();
$aColumns = array('id_agendamento', 'id_agenda', 'id_fornecedores', 'data_inicio', 'data_fim', 'assunto', 'obs', 'data_agendamento', 'agendado_por', 'inativo', 'razao_social', 'concluido');
$myArray = array();

$output = array(
    "sEcho" => isset($_GET['sEcho']) ? intval($_GET['sEcho']): 0,
    "iTotalRecords" => 0,
    "iTotalDisplayRecords" => 0,
    "aaData" => array()
);

if ($_GET["tipo"] == 0) {
    $where = " WHERE sf_agendamento.inativo = 0 AND id_agenda = " . (is_numeric($_GET["id"]) ? $_GET["id"] : 0);
    if ($_GET["start"] != "" && $_GET["end"] != "") {
        $dt_ini = explode("-", $_GET["start"]);
        $dt_fim = explode("-", $_GET["end"]);
        $where .= " AND data_inicio between '" . $dt_ini[2] . "/" . $dt_ini[1] . "/" . $dt_ini[0] . "' and '" . $dt_fim[2] . "/" . $dt_fim[1] . "/" . $dt_fim[0] . "'";
    }
    $sQuery1 = "set dateformat dmy;select sf_agendamento.*,sf_fornecedores_despesas.razao_social,
    (select id_servico from sf_agenda a where a.id_agenda = sf_agendamento.id_agenda) servico_venda
    from sf_agendamento inner join sf_fornecedores_despesas on id_fornecedores_despesas = id_fornecedores " . $where;
    $cur = odbc_exec($con, $sQuery1);
    while ($aRow = odbc_fetch_array($cur)) {
        $evento = array();
        $evento["id"] = utf8_encode($aRow[$aColumns[0]]);
        $evento["title"] = utf8_encode($aRow[$aColumns[5]]) . ($aRow[$aColumns[2]] > 0 ? " - " . utf8_encode($aRow[$aColumns[10]]) : "");
        $evento["description"] = $aRow[$aColumns[2]] . " - " . utf8_encode($aRow[$aColumns[10]]);
        $evento["start"] = escreverData($aRow[$aColumns[3]], 'Y-m-d') . "T" . escreverData($aRow[$aColumns[3]], 'H:i:s');
        $evento["end"] = escreverData($aRow[$aColumns[4]], 'Y-m-d') . "T" . escreverData($aRow[$aColumns[4]], 'H:i:s');
        $evento["completed"] = utf8_encode($aRow[$aColumns[11]]);
        $evento["pg_pendente"] = (strlen($aRow["servico_venda"]) > 0 && strlen($aRow["id_item_venda"]) == 0 ? "1" : "0");
        $myArray[] = $evento;
    }
} elseif ($_GET["tipo"] == 1) {
    $sQuery1 = "set dateformat dmy;select * from sf_turmas 
    inner join sf_turnos on sf_turnos.cod_turno = sf_turmas.horario
    where id_turma = " . (is_numeric($_GET["id"]) ? $_GET["id"] : 0);
    $cur = odbc_exec($con, $sQuery1);
    while ($aRow = odbc_fetch_array($cur)) {
        $dia_semana[0] = array(utf8_encode($aRow["dia_semana1"]), $aRow["faixa1_ini"], $aRow["faixa1_fim"]);
        $dia_semana[1] = array(utf8_encode($aRow["dia_semana2"]), $aRow["faixa2_ini"], $aRow["faixa2_fim"]);
        $dia_semana[2] = array(utf8_encode($aRow["dia_semana3"]), $aRow["faixa3_ini"], $aRow["faixa3_fim"]);
        $dia_semana[3] = array(utf8_encode($aRow["dia_semana4"]), $aRow["faixa4_ini"], $aRow["faixa4_fim"]);
        $dia_semana[4] = array(utf8_encode($aRow["dia_semana5"]), $aRow["faixa5_ini"], $aRow["faixa5_fim"]);
    }
    $diff = (strtotime($_GET["end"]) - strtotime($_GET["start"])) / 24 / 3600;
    if ($diff > 0) {
        for ($i = 0; $i < $diff; $i++) {
            $data = date("Y-m-d", strtotime($_GET["start"] . " +" . $i . " day"));
            $diaSemana = date("w", strtotime($_GET["start"] . " +" . $i . " day"));
            for ($j = 0; $j < 5; $j++) {
                if (substr($dia_semana[$j][0], $diaSemana, 1) == 1) {
                    $evento = array();
                    $evento["id"] = "0";
                    $evento["title"] = "Horário";
                    $evento["start"] = $data . "T" . $dia_semana[$j][1] . ":00";
                    $evento["end"] = $data . "T" . $dia_semana[$j][2] . ":00";
                    $myArray[] = $evento;
                }
            }
        }
    }
}

if (isset($_GET["tpImp"]) && $_GET["tpImp"] == "I") {
    foreach ($myArray as $evento) {
        $row = array();
        $row[] = $evento["id"];
        $row[] = escreverDataHora($evento["start"]);
        $row[] = escreverDataHora($evento["end"]);
        $row[] = $evento["title"];        
        $output['aaData'][] = $row;
    }
} else {
    echo json_encode($myArray);
}
odbc_close($con);
