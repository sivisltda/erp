<?php

require_once('../../../Connections/configini.php');

$aColumns = array('id', 'termo', 'grupo', 'inativo');
$iTotal = 0;
$sLimit = 20;
$sQtd = 0;
$sOrder = " ORDER BY " . $aColumns[0] . " asc ";

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

$sQuery = "SELECT COUNT(*) total from sf_configuracao_termos";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,* from sf_configuracao_termos) as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " " . $sOrder;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<a href='javascript:void(0)' onClick='AbrirTermo(" . utf8_encode($aRow[$aColumns[0]]) . ")'><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[1]]) . "'>" . utf8_encode($aRow[$aColumns[1]]) . "</div></a>";
    $row[] = "<center><input class=\"btn red\" type=\"button\" value=\"x\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\" onclick=\"RemoverTermo('" . utf8_encode($aRow[$aColumns[0]]) . "')\" /></center>";    
    $output['aaData'][] = $row;
}

echo json_encode($output);
odbc_close($con);