<?php
include "form/ContratosFormServer.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <style>
            body { font-family: sans-serif; }
            div label input {
                margin-right: 100px;
            }
            label {
                float: left;
                width: 26px;
                cursor: pointer;
                background: #EFEFEF;
                margin: 2px 0 2px 1px;
                border: 1px solid #D0D0D0;
            }
            label span {
                display: block;
                padding: 3px 0;
                text-align: center;
            }
            label input {
                top: -20px;
                position: absolute;
            }
            input:checked + span {
                color: #FFF;
                background: #186554;
            }         
            .select2-container-multi.select2-container-disabled .select2-choices .select2-search-choice {
                background-color: #999;
            }            
        </style>
    </head>
    <body>
        <form action="ContratosForm.php" name="frmEnviaDados" id="frmEnviaDados" method="POST">
            <div class="frmhead">
                <div class="frmtext">Contrato</div>
                <div class="frmicon" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="tabbable frmtabs">
                <ul class="nav nav-tabs" style="margin:0">
                    <li class="active"><a href="#tab1" data-toggle="tab">Dados principais</a></li>
                    <li><a href="#tab2" data-toggle="tab">Filial</a></li>
                    <li><a href="#tab3" data-toggle="tab">Grupos</a></li>
                    <li><a href="#tab4" data-toggle="tab">Contrato Agrupado</a></li>
                </ul>
                <div class="tab-content frmcont" style="height:230px">
                    <div class="tab-pane active" id="tab1" style="overflow:hidden; height:230px">
                        <input name="txtId" id="txtId" type="hidden" value="<?php echo $id; ?>"/>
                        <div style="width:72%; float: left;">
                            <span>Descrição:</span>
                            <input name="txtDescricao" id="txtDescricao" type="text" style="width:100%" <?php echo $disabled; ?> value="<?php echo $descricao; ?>"/>
                        </div>
                        <div style="width:10%; float: left; margin-left: 1%;">
                            <span>Recuo:</span>
                            <input name="txtRecuo" id="txtRecuo" type="text" style="width:100%" <?php echo $disabled; ?> value="<?php echo $recuo; ?>"/>
                        </div>
                        <div style="width:16%; float: left; margin-left: 1%; margin-top: 2px;">                    
                            <input type="checkbox" class="input-medium" id="txtInativo" name="txtInativo" <?php echo $disabled; ?> <?php
                            if (substr($inativo, 0, 1) == 1) {
                                echo "checked";
                            }
                            ?> value="1"/>                   
                            <span>Inativo</span>
                            <input type="checkbox" class="input-medium" id="txtPortal" name="txtPortal" <?php echo $disabled; ?> <?php
                            if (substr($portal, 0, 1) == 1) {
                                echo "checked";
                            }
                            ?> value="1"/>                   
                            <span>Portal</span>
                        </div>                
                        <div style="width:36%; float: left;">
                            <span>Tipo de Contrato:</span>
                            <select id="txtTipo" name="txtTipo" class="select" <?php echo $disabled; ?> style="width:100%">
                                <option value="null">Selecione:</option>
                                <option value="0" <?php echo ($tipo == "0" ? "selected" : ""); ?>>AQUISIÇÃO</option>                    
                                <option value="1" <?php echo ($tipo == "1" ? "selected" : ""); ?>>RESCISÃO</option>
                                <option value="2" <?php echo ($tipo == "2" ? "selected" : ""); ?>>AGENDAMENTO</option>
                                <option value="3" <?php echo ($tipo == "3" ? "selected" : ""); ?>>EVENTOS</option>
                                <option value="4" <?php echo ($tipo == "4" ? "selected" : ""); ?>>VISTORIA</option>
                            </select>
                        </div>                        
                        <div style="width:17%; float: left; margin-left: 1%; margin-top: 16px;">                    
                            <input type="checkbox" class="input-medium" id="txtLogo" name="txtLogo" <?php echo $disabled; ?> <?php
                            if (substr($logo, 0, 1) == 1) {
                                echo "checked";
                            }
                            ?> value="1"/>                   
                            <span>S/Logo</span>
                        </div>
                        <div style="width:22%; float: left; margin-left: 1%; margin-top: 16px;">                    
                            <input type="checkbox" class="input-medium" id="txtMarcarTermos" name="txtMarcarTermos" <?php echo $disabled; ?> <?php
                            if (substr($marcarTermos, 0, 1) == 1) {
                                echo "checked";
                            }
                            ?> value="1"/>                   
                            <span>Marcar Termos</span>
                        </div>
                        <div style="width:22%; float: left; margin-left: 1%; margin-top: 16px;">                    
                            <input type="checkbox" class="input-medium" id="txtAssTermos" name="txtAssTermos" <?php echo $disabled; ?> <?php
                            if (substr($assTermos, 0, 1) == 1) {
                                echo "checked";
                            }
                            ?> value="1"/>                   
                            <span>Assinar Termos</span>
                        </div>                                                      
                    </div>            
                    <div class="tab-pane" id="tab2" style="overflow:hidden; height:230px">
                        <div style="width:100%; float: left;">
                            <span>Filial:</span>
                            <select id="txtFilial" name="txtFilial[]" multiple="multiple" class="select" style="width:100%" class="input-medium" <?php echo $disabled; ?>>
                                <?php $cur = odbc_exec($con, "select t.id_filial,t.Descricao,ti.id_contrato from sf_filiais t 
                                left join sf_contratos_filiais ti on t.id_filial = ti.id_filial 
                                and ti.id_contrato = " . valoresSelect2($id) . " order by t.Descricao") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP['id_filial'] ?>"<?php
                                    if ($RFP["id_contrato"] != null) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP['Descricao']) ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>            
                    <div class="tab-pane" id="tab3" style="overflow:hidden; height:230px">
                        <div style="width:100%; float: left;">
                            <span>Grupos:</span>
                            <select id="txtGrupos" name="txtGrupos[]" multiple="multiple" class="select" style="width:100%" class="input-medium" <?php echo $disabled; ?>>
                                <?php $cur = odbc_exec($con, "select t.id_contas_movimento,t.Descricao,ti.id_contrato from sf_contas_movimento t 
                                left join sf_contratos_grupos ti on t.id_contas_movimento = ti.id_grupo 
                                and ti.id_contrato = " . valoresSelect2($id) . " where tipo = 'L' ORDER BY descricao") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP['id_contas_movimento'] ?>"<?php
                                    if ($RFP["id_contrato"] != null) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP['Descricao']) ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>            
                    <div class="tab-pane" id="tab4" style="overflow:hidden; height:230px">
                        <div style="width:100%; float: left;">
                            <span>Contrato Individual (ao gerar):</span>
                            <select name="txtIndividual" id="txtIndividual" class="select" <?php echo $disabled; ?> style="width:100%">
                                <option value="null">Selecione</option>
                                <?php $sql = "select id, descricao from sf_contratos where inativo = 0
                                and assinatura = 1 and id not in (" . valoresSelect2($id) . ") order by descricao";
                                $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP['id'] ?>"<?php
                                    if (!(strcmp($RFP['id'], $individual))) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                <?php } ?>
                            </select>
                        </div>                        
                    </div>            
                </div>            
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == '') { ?>
                        <button class="btn green" type="submit" title="Gravar" onclick="return validaForm();" name="bntSave" id="bntSave" ><span class="ico-checkmark"></span> Gravar</button>
                        <?php if ($_POST['txtId'] == '') { ?>
                            <button class="btn yellow" title="Cancelar" onClick="parent.FecharBox()" id="bntCancelar"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                            <button class="btn yellow" title="Cancelar" type="submit" name="bntAlterar" id="bntAlterar" ><span class="ico-reply"></span> Cancelar</button>
                            <?php
                        }
                    } else { ?>
                        <button class="btn btn-info" type="submit" title="Duplicar" name="bntDuplicar" id="bntDuplicar" value="Duplicar" onClick="return confirm('Confirma a ação de duplicar este contrato?')" style="margin-top: 15px; float: left;"><span class="ico-plus-sign"></span> Duplicar</button>                            
                        <button class="btn green" type="submit" title="Novo" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" title="Alterar" type="submit" name="bntEdit" id="bntEdit" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                        <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja inativar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                    <?php } ?>                                        
                </div>
            </div>
        </form>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/jquery.mousewheel.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/epiechart/jquery.easy-pie-chart.js"></script>
        <script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/charts.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="../../js/app.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>     
        <script type="text/javascript" src="../../js/util.js"></script> 
        <script type='text/javascript' src='js/ContratosForm.js'></script>
        <?php odbc_close($con); ?>
    </body>
</html>
