<?php
include "../../Connections/configini.php";
include "../../util/util.php";

function resize($img, $w, $h, $nome) {
    if (!extension_loaded('gd') && !extension_loaded('gd2')) {
        trigger_error("GD não foi carregado", E_USER_WARNING);
        return false;
    }
    $imgInfo = getimagesize($img);
    switch ($imgInfo[2]) {
        case 1: $im = imagecreatefromgif($img);
            break;
        case 2: $im = imagecreatefromjpeg($img);
            break;
        case 3: $im = imagecreatefrompng($img);
            break;
        default: trigger_error('Formato não suportado!', E_USER_WARNING);
            break;
    }
    $ajuste = 0;
    if ($imgInfo[0] <= $w && $imgInfo[1] <= $h) {
        $nHeight = $imgInfo[1];
        $nWidth = $imgInfo[0];
    } else {
        if ($w / $imgInfo[0] > $h / $imgInfo[1]) {
            $nWidth = $w;
            $nHeight = $imgInfo[1] * ($w / $imgInfo[0]);
        } else {
            $nWidth = $imgInfo[0] * ($h / $imgInfo[1]);
            $nHeight = $h;
            $ajuste = (167 - $nWidth) / 2;
        }
    }
    $nWidth = round($nWidth);
    $nHeight = round($nHeight);
    $newImg = imagecreatetruecolor(167, 194);
    imagealphablending($newImg, false);
    imagesavealpha($newImg, true);
    $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
    imagefilledellipse($newImg, 0, 0, 167, 194, $transparent);
    imagefilledrectangle($newImg, 0, 0, 167, 194, $transparent);
    imagecopyresampled($newImg, $im, $ajuste, 0, 0, 0, $nWidth, $nHeight, $imgInfo[0], $imgInfo[1]);
    switch (3) {
        case 1: imagegif($newImg, $nome);
            break;
        case 2: imagejpeg($newImg, $nome);
            break;
        case 3: imagepng($newImg, $nome);
            break;
        default: trigger_error('Falhou ao redimensionar a imagem!', E_USER_WARNING);
            break;
    }
    return $nome;
}

if (isset($_POST['bntLogin'])) {
    $nomeImg = "";
    $allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
    $extension = explode(".", $_FILES["file"]["name"]);
    $extension = $extension[count($extension) - 1];
    if ((($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/pjpeg") || ($_FILES["file"]["type"] == "image/x-png") || ($_FILES["file"]["type"] == "image/png")) && ($_FILES["file"]["size"] < 2000000) && in_array($extension, $allowedExts)) {
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
        } else {
            $nomeImg = $_SESSION["id_usuario"] . ".png";
            move_uploaded_file($_FILES["file"]["tmp_name"], "./../../Pessoas/" . $contrato . "/" . $nomeImg);
            resize("./../../Pessoas/" . $contrato . "/" . $nomeImg, 167, 194, "./../../Pessoas/" . $contrato . "/" . $nomeImg);
        }
    }
    if ($_POST['senha_nova1'] == $_POST['senha_nova2'] && strlen($_POST['senha_nova1']) >= 6) {
        $cur = odbc_exec($con, "SELECT * FROM sf_usuarios WHERE login_user = " . valoresTexto2($_SESSION["login_usuario"]) . " AND senha = " . valoresTexto2(encrypt($_POST['senha_atual'], "VipService123", true)));       
        if(odbc_num_rows($cur) > 0){
            while ($RFP = odbc_fetch_array($cur)) {
                $query = "UPDATE sf_usuarios SET senha = " . valoresTexto2(encrypt($_POST['senha_nova1'], "VipService123", true)) . " WHERE id_usuario = " . $RFP['id_usuario'] . ";";
                $query .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
                ('sf_usuarios', " . $RFP['id_usuario'] . ", " . valoresTexto2($_SESSION["login_usuario"]) . ", 'A', 'ALTERACAO DE SENHA USUARIO " . $RFP['id_usuario'] . "', GETDATE(), null);";            
                odbc_exec($con, $query);       
                echo "<script> (function() { setTimeout(function(){ bootbox.alert('Senha alterada com sucesso!'); }, 3000);  })();</script>";                
            }
        }else{
             echo "<script> (function() { setTimeout(function(){ bootbox.alert('Senha atual invalida!'); }, 3000);  })();</script>";
        }
    } 
    if ($nomeImg != "") {
        odbc_exec($con, "UPDATE sf_usuarios SET imagem = " . valoresTexto2($nomeImg) . " WHERE id_usuario = " . $_SESSION["id_usuario"]);
        echo "<script> (function() { setTimeout(function(){ bootbox.alert('Imagem alterada com sucesso!'); }, 3000);  })();</script>";
    }
}
if ($_SESSION["login_usuario"] != "") {
    $cur = odbc_exec($con, "SELECT imagem FROM sf_usuarios WHERE login_user = " . valoresTexto2($_SESSION["login_usuario"]));
    while ($RFP = odbc_fetch_array($cur)) {
        $imagemx = utf8_encode($RFP['imagem']);
    }
} else {
    $imagemx = "";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1>Administrativo<small>Trocar Senha</small></h1>
                    </div>
                    <div class="row-fluid">                       
                        <form action="MudarSenha.php" method="post" enctype="multipart/form-data">
                            <div style="float:left; width:200px">
                                <div class="row-form">
                                    <input type="password" value="" name="senha_atual" id="senha_atual" placeholder="Senha Atual"/>
                                </div>
                                <div class="row-form">
                                    <input type="password" value="" name="senha_nova1" id="senha_nova1" placeholder="Senha Nova"/>
                                </div>
                                <div class="row-form">
                                    <input type="password" value="" name="senha_nova2" id="senha_nova2" placeholder="Confirmar Senha"/>
                                </div>
                                <div class="row-form">
                                    <button class="btn" name="bntLogin" onclick="return valida()">Salvar Alterações <span class="icon-arrow-next icon-white"></span></button>
                                </div>  
                            </div>
                            <div style="float:left">
                                <div class="row-form">
                                    <img src="<?php
                                    if ($imagemx != '') {
                                        echo "./../../Pessoas/" . $contrato . "/" . $imagemx;
                                    } else {
                                        echo "./../../img/dmitry_m.gif";
                                    }
                                    ?>" width="99" height="118" />
                                </div>
                                <div class="row-form">
                                    <input <?php echo $disabled; ?>  type="file" name="file" id="file"/>
                                </div>
                            </div>   
                        </form>       
                    </div>                        
                </div>
            </div>
        </div>
        <div class="dialog" id="source" style="display:none ;" title="Source"></div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type="text/javascript" src="../../js/util.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script> 
        <script>
            function valida(){
                var error = "";                
                if($("#senha_atual").val() !== "" || $("#senha_nova1").val() !== "" || $("#senha_nova2").val() !== ""){
                    if($("#senha_atual").val() === ""){
                        error = "Digite sua senha Atual!";
                    }else if($("#senha_nova1").val() === ""){
                        error = "Digite a nova senha!";
                    }else if($("#senha_nova2").val() === ""){
                        error = "Confirme a nova senha!";
                    }else if($("#senha_nova1").val() !== $("#senha_nova2").val()){
                        error = "A senha e confirmação de senha não estão iguais!";
                    }else if($("#senha_nova1").val().length < 6){
                        error = "A senha tem que ter mais de 6 caracters!";
                    }
                }                
                if(error !== ""){
                    bootbox.alert(error);
                    return false;
                }else{
                    return true;
                }           
            }
        </script>
        <?php odbc_close($con); ?>
    </body>
</html>