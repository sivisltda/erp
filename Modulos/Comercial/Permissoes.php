<?php
include '../../Connections/configini.php';
if ($_GET['Del'] != '') {
    odbc_exec($con, "DELETE FROM sf_usuarios_permissoes WHERE id_permissoes =" . $_GET['Del']);
    echo "<script>window.top.location.href = 'Permissoes.php'; </script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>        
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Administrativo<small>Permissões</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="float:left;width: 10%;margin-top: 15px;">
                                    <button class="button button-green btn-primary" type="button" onClick="AbrirBox(0)"><span class="ico-file-4 icon-white"></span></button>
                                    <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir()" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                </div>
                                <div style="float:right;width: 30%;">                                                                                                                                               
                                    <div style="float:left;margin-left: 1%;width: 85%;">
                                        <div width="100%">Busca:</div>
                                        <input id="txtBusca" maxlength="100" type="text" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title=""/>
                                    </div>
                                    <div style="float:left;margin-left: 1%;width: 7%;margin-top: 15px;">
                                        <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Permissões</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblLista">
                            <thead>
                                <tr>
                                    <th width="20%">Nome da Permissão</th>
                                    <?php if ($mdl_adm_ > 0) { ?>
                                        <th width="10.71%"><center>Administrativo</center></th>
                                    <?php } if ($mdl_fin_ > 0) { ?>
                                        <th width="10.71%"><center>Financeiro</center></th>
                                    <?php } if ($mdl_est_ > 0) { ?>
                                        <th width="10.71%"><center>Estoque</center></th>
                                    <?php } if ($mdl_nfe_ > 0) { ?>
                                        <th width="10.71%"><center>NFE</center></th>
                                    <?php } if ($mdl_crm_ > 0) { ?>
                                        <th width="10.71%"><center>CRM</center></th>
                                    <?php } if ($mdl_ser_ > 0) { ?>
                                        <th width="10.71%"><center>Serviços</center></th>
                                    <?php } if ($mdl_aca_ > 0) { ?>
                                        <th width="10.71%"><center><?php echo ($mdl_clb_ == 1 ? "Configuração" : "Academia")?></center></th>
                                    <?php } ?>
                                    <th width="10.71%"><center>Outros</center></th>
                                    <th width="5%"><center>Ação</center></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div> 
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script> 
        <script type="text/javascript" src="../../js/util.js"></script>                       
        <script type="text/javascript">
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }
            
            $(document).ready(function () {
                $("#btnfind").click(function () {
                    tbLista.fnReloadAjax(finalFind(0));
                });              
            });

            function finalFind(imp) {
                var retPrint = (imp === 1 ? '&imp=1' : '?imp=0');                
                if ($("#txtBusca").val() !== "") {
                    retPrint = retPrint + '&Search=' + $("#txtBusca").val();
                }
                return "Permissoes_server_processing.php" + retPrint.replace(/\//g, "_");
            }

            function AbrirBox(id) {
                abrirTelaBox("FormPermissoes.php" + (id > 0 ? "?id=" + id : ""), 620, 750);
            }

            function FecharBox() {
                $("#newbox").remove();
                tbLista.fnReloadAjax(finalFind(0));
            }

            function imprimir() {
                var pRel = "&NomeArq=" + "Permissões" +
                "&lbl=Nome da Permissão|Admin.|Financeiro|Estoque|NFE|CRM|Serviços|<?php echo ($mdl_clb_ == 1 ? "Configuração" : "Academia")?>|Outros" +
                "&siz=180|65|65|65|65|65|65|65|65" +
                "&pdf=9" + // Colunas do server processing que não irão aparecer no pdf
                "&filter=" + //Label que irá aparecer os parametros de filtro
                "&PathArqInclude=" + "../Modulos/Comercial/" + finalFind(1); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }

            var tbLista = $('#tblLista').dataTable({
                "iDisplayLength": 20,
                "aLengthMenu": [20, 30, 40, 50, 100],
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "bSort" : false,
                "sAjaxSource": finalFind(0),
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"},
                "sPaginationType": "full_numbers"
            });                        
        </script>        
        <?php odbc_close($con); ?>
    </body>
</html>