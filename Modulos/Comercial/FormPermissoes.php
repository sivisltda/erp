<?php
include '../../Connections/configini.php';
$disabled = 'disabled ';

$mdl_adm_ = returnPart($_SESSION["modulos"], 0);
$mdl_fin_ = returnPart($_SESSION["modulos"], 1);
$mdl_est_ = returnPart($_SESSION["modulos"], 2);
$mdl_nfe_ = returnPart($_SESSION["modulos"], 3);
$mdl_crm_ = returnPart($_SESSION["modulos"], 4);
$mdl_ser_ = returnPart($_SESSION["modulos"], 5);
$mdl_aca_ = returnPart($_SESSION["modulos"], 6);
$mdl_tur_ = returnPart($_SESSION["modulos"], 7);
$mdl_gbo_ = returnPart($_SESSION["modulos"], 8);
$mdl_srs_ = returnPart($_SESSION["modulos"], 9);
$mdl_esd_ = returnPart($_SESSION["modulos"],10);
$mdl_bod_ = returnPart($_SESSION["modulos"],11);
$mdl_clb_ = returnPart($_SESSION["modulos"],12);
$mdl_cnt_ = returnPart($_SESSION["modulos"],13);
$mdl_seg_ = returnPart($_SESSION["modulos"],14);
$mdl_sau_ = returnPart($_SESSION["modulos"],15);
$mdl_wha_ = returnPart($_SESSION["modulos"],16);
$mdl_cht_ = returnPart($_SESSION["modulos"],17);

if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        $query = "update sf_usuarios_permissoes set " .
        "adm_cli = " . valoresCheck('ckb_adm_cli') . "," .
        "adm_for = " . valoresCheck('ckb_adm_for') . "," .
        "adm_fun = " . valoresCheck('ckb_adm_fun') . "," .
        "adm_usu = " . valoresCheck('ckb_adm_usu') . "," .
        "adm_per = " . valoresCheck('ckb_adm_per') . "," .
        "adm_gru = " . valoresCheck('ckb_adm_gru') . "," .
        "adm_dep = " . valoresCheck('ckb_adm_dep') . "," .
        "adm_config = " . valoresCheck('ckb_adm_config') . "," .
        "adm_email = " . valoresCheck('ckb_adm_email') . "," .
        "adm_disparos = " . valoresCheck('ckb_adm_disparos') . "," .
        "adm_agenda = " . valoresCheck('ckb_adm_agenda') . "," .
        "adm_contratos = " . valoresCheck('ckb_adm_contratos') . "," .
        "adm_atendimentos = " . valoresCheck('ckb_adm_atendimentos') . "," .
        "adm_parentesco = " . valoresCheck('ckb_adm_parentesco') . "," .                
        "fin_doc = " . valoresCheck('ckb_fin_doc') . "," .
        "fin_ban = " . valoresCheck('ckb_fin_ban') . "," .
        "fin_con = " . valoresCheck('ckb_fin_con') . "," .
        "fin_mov = " . valoresCheck('ckb_fin_mov') . "," .
        "fin_fix = " . valoresCheck('ckb_fin_fix') . "," .
        "fin_afix = " . valoresCheck('ckb_fin_afix') . "," .
        "fin_flu = " . valoresCheck('ckb_fin_flu') . "," .
        "fin_lsol = " . valoresCheck('ckb_fin_lsol') . "," .
        "fin_asol = " . valoresCheck('ckb_fin_asol') . "," .
        "fin_pag = " . valoresCheck('ckb_fin_pag') . "," .
        "fin_rec = " . valoresCheck('ckb_fin_rec') . "," .
        "est_mov = " . valoresCheck('ckb_est_mov') . "," .
        "est_ccu = " . valoresCheck('ckb_est_ccu') . "," .
        "est_pro = " . valoresCheck('ckb_est_pro') . "," .
        "est_ser = " . valoresCheck('ckb_est_ser') . "," .
        "est_spc = " . valoresCheck('ckb_ser_spc') . "," .
        "est_loc = " . valoresCheck('ckb_est_loc') . "," .
        "est_lven = " . valoresCheck('ckb_est_lven') . "," .
        "est_lven_read = " . valoresCheck('ckb_est_lven_read') . "," .
        "est_aven = " . valoresCheck('ckb_est_aven') . "," .
        "est_lorc = " . valoresCheck('ckb_est_lorc') . "," .
        "est_aorc = " . valoresCheck('ckb_est_aorc') . "," .
        "fin_lped = " . valoresCheck('ckb_fin_lped') . "," .
        "fin_lcot = " . valoresCheck('ckb_fin_lcot') . "," .
        "fin_aped = " . valoresCheck('ckb_fin_aped') . "," .
        "est_lcom = " . valoresCheck('ckb_est_lcom') . "," .
        "est_acom = " . valoresCheck('ckb_est_acom') . "," .
        "est_flu = " . valoresCheck('ckb_est_flu') . "," .
        "est_com = " . valoresCheck('ckb_est_com') . "," .
        "est_cor = " . valoresCheck('ckb_est_cor') . "," .
        "est_tam = " . valoresCheck('ckb_est_tam') . "," .
        "est_fab = " . valoresCheck('ckb_est_fab') . "," .
        "est_pla = " . valoresCheck('ckb_est_pla') . "," .                
        "est_jus = " . valoresCheck('ckb_est_jus') . "," .
        "est_ent_sai = " . valoresCheck('ckb_est_ent_sai') . "," .
        "est_aent_sai = " . valoresCheck('ckb_est_aent_sai') . "," .                
        "nfe_cen = " . valoresCheck('ckb_nfe_cen') . "," .
        "nfe_tra = " . valoresCheck('ckb_nfe_tra') . "," .
        "nfe_msg = " . valoresCheck('ckb_nfe_msg') . "," .
        "nfe_emi = " . valoresCheck('ckb_nfe_emi') . "," .
        "nfe_est = " . valoresCheck('ckb_nfe_est') . "," .
        "nfe_cid = " . valoresCheck('ckb_nfe_cid') . "," .
        "nfe_enq = " . valoresCheck('ckb_nfe_enq') . "," .
        "nfe_not = " . valoresCheck('ckb_nfe_not') . "," .
        "nfe_ess = " . valoresCheck('ckb_nfe_ess') . "," .
        "crm_cli = " . valoresCheck('ckb_crm_cli') . "," .
        "crm_fch = " . valoresCheck('ckb_crm_fch') . "," .
        "crm_pro = " . valoresCheck('ckb_crm_pro') . "," .
        "crm_tel = " . valoresCheck('ckb_crm_tel') . "," .
        "crm_esp = " . valoresCheck('ckb_crm_esp') . "," .
        "crm_ind = " . valoresCheck('ckb_crm_ind') . "," .
        "crm_rec = " . valoresCheck('ckb_crm_rec') . "," .
        "out_ges = " . valoresCheck('ckb_out_ges') . "," .
        "out_aju = " . valoresCheck('ckb_out_aju') . "," .
        "out_term = " . valoresCheck('ckb_out_term') . "," .
        "out_terc = " . valoresCheck('ckb_out_terc') . "," .
        "out_rban = " . valoresCheck('ckb_out_rban') . "," .
        "out_rexc = " . valoresCheck('ckb_out_rexc') . "," .
        "out_ecob = " . valoresCheck('ckb_out_ecob') . "," .
        "out_todos_operadores = " . valoresCheck('ckb_out_todos_operadores') . "," .
        "out_desconto = " . valoresCheck('ckb_out_desconto') . "," .
        "out_pag_avancada = " . valoresCheck('ckb_out_pag_avancada') . "," .
        "pag_avancada = " . valoresNumericos('txt_out_pag_avancada') . "," .
        "ser_cli = " . valoresCheck('ckb_ser_cli') . "," .
        "ser_ser = " . valoresCheck('ckb_ser_ser') . "," .
        "fin_dmp = " . valoresCheck('ckb_fin_dmp') . "," .
        "crm_cpc = " . valoresCheck('ckb_crm_cpc') . "," .
        "crm_exc_atend = " . valoresCheck('ckb_crm_exc_atend') . "," .
        "crm_fec_atend = " . valoresCheck('ckb_crm_fec_atend') . "," .
        "adm_cli_read = " . valoresCheck('ckb_adm_cli_read') . "," .
        "adm_cli_read_obs = " . valoresCheck('ckb_adm_cli_read_obs') . "," .
        "fin_rel_gct = " . valoresCheck('ckb_fin_rel_gct') . "," .
        "fin_tdd = " . valoresCheck('ckb_fin_rel_tdd') . "," .
        "fin_crt = " . valoresCheck('ckb_fin_crt') . "," .
        "fin_bol = " . valoresCheck('ckb_fin_bol') . "," .
        "fin_chq = " . valoresCheck('ckb_fin_chq') . "," .
        "fin_rcrt = " . valoresCheck('ckb_fin_rcrt') . "," .
        "fin_rel_mov_caixa = " . valoresCheck('ckb_fin_rel_mov_caixa') . "," .
        "siv_cadacad = " . valoresCheck('ckb_siv_cadAcad') . "," .
        "siv_caderp = " . valoresCheck('ckb_siv_cadErp') . "," .
        "siv_gersenha = " . valoresCheck('ckb_siv_GerSenha') . "," .
        "siv_usersite = " . valoresCheck('ckb_siv_UserSite') . "," .
        "siv_dcc = " . valoresCheck('ckb_siv_DCC') . "," .
        "siv_cregb = " . valoresCheck('ckb_siv_CreGB') . "," .
        "siv_pengb = " . valoresCheck('ckb_siv_PenGB') . "," .
        "siv_logerp = " . valoresCheck('ckb_siv_LogERP') . "," .
        "aca_hor = " . valoresCheck('ckb_aca_hor') . "," .
        "aca_conv = " . valoresCheck('ckb_aca_conv') . "," .
        "aca_cred = " . valoresCheck('ckb_aca_cred') . "," .
        "aca_catr = " . valoresCheck('ckb_aca_catr') . "," .
        "aca_leif = " . valoresCheck('ckb_aca_leif') . "," .
        "aca_amb = " . valoresCheck('ckb_aca_amb') . "," .
        "aca_fer = " . valoresCheck('ckb_aca_fer') . "," .
        "aca_fer_par = " . valoresCheck('ckb_aca_fer_par') . "," .
        "aca_fer_exc = " . valoresCheck('ckb_aca_fer_exc') . "," .
        "aca_pla = " . valoresCheck('ckb_aca_pla') . "," .
        "aca_ser = " . valoresCheck('ckb_aca_ser') . "," .
        "aca_alt_val_mens = " . valoresCheck('ckb_aca_alt_val_mens') . "," .                
        "aca_pro_rata_mens = " . valoresCheck('ckb_aca_pro_rata_mens') . "," .
        "aca_documento = " . valoresCheck('ckb_aca_documento') . "," .
        "aca_est_mens = " . valoresCheck('ckb_aca_est_mens') . "," .
        "aca_exc_mens = " . valoresCheck('ckb_aca_exc_mens') . "," .
        "aca_exc_mret = " . valoresCheck('ckb_aca_exc_mret') . "," .
        "aca_inf_gerenciais = " . valoresCheck('ckb_aca_inf_gerenciais') . "," .
        "aca_add_convenio = " . valoresCheck('ckb_aca_add_convenio') . "," .
        "aca_add_credencial = " . valoresCheck('ckb_aca_add_credencial') . "," .
        "aca_dtf_credencial = " . valoresCheck('ckb_aca_dtf_credencial') . "," .
        "aca_add_bloqueio = " . valoresCheck('ckb_aca_add_bloqueios') . "," .
        "aca_cancelar_dcc = " . valoresCheck('ckb_aca_cancelar_dcc') . "," .
        "aca_plan_comp = " . valoresCheck('ckb_aca_plan_comp') . "," .
        "aca_pgr = " . valoresCheck('ckb_aca_pg_retro') . "," .
        "aca_rel_acesso = " . valoresCheck('ckb_aca_rel_acesso') . "," .
        "aca_turmas = " . valoresCheck('ckb_aca_turmas') . "," .
        "aca_estudio = " . valoresCheck('ckb_aca_estudio') . "," .
        "aca_documentos = " . valoresCheck('ckb_aca_documentos') . "," .
        "adm_email_obrig = " . valoresCheck('ckb_adm_emlobg') . "," .
        "adm_tel_obrig = " . valoresCheck('ckb_adm_telobg') . "," .                
        "adm_cel_obrig = " . valoresCheck('ckb_adm_celobg') . "," .
        "adm_end_obrig = " . valoresCheck('ckb_adm_endobg') . "," .
        "adm_comissao = " . valoresCheck('ckb_adm_comissao') . "," .
        "clb_tit_obrig = " . valoresCheck('ckb_clb_titobr') . "," .                                
        "adm_proc_obrig = " . valoresCheck('ckb_adm_procobg') . "," .                                
        "adm_sexo_obrig = " . valoresCheck('ckb_adm_sexoobg') . "," .                                
        "aca_abonar_mens = " . valoresCheck('ckb_aca_abomens') . "," .                
        "aca_abonar_cre = " . valoresCheck('ckb_aca_abocre') . "," .
        "aca_abonar_deb = " . valoresCheck('ckb_aca_abodeb') . "," .                                
        "aca_pg_prod = " . valoresCheck('ckb_aca_pg_prod') . "," .                                
        "aca_can_item = " . valoresCheck('ckb_aca_can_item') . "," .                                
        "aca_data_inicio = " . valoresCheck('ckb_aca_data_inicio') . "," .                                
        "aca_agenda_regras = " . valoresCheck('ckb_aca_agenda_regras') . "," .                                
        "aca_infoger_caixa = " . valoresCheck('ckb_aca_igCaixa') . "," .
        "aca_infoger_stats = " . valoresCheck('ckb_aca_igStatus') . "," .
        "aca_infoger_contr = " . valoresCheck('ckb_aca_Contrato') . "," .
        "aca_infoger_filav = " . valoresCheck('ckb_aca_filAvn') . "," .
        "aca_infoger_renov = " . valoresCheck('ckb_aca_renovacoes') . "," .
        "aca_infoger_dcc = " . valoresCheck('ckb_aca_dcc') . "," .
        "aca_infoger_bol = " . valoresCheck('ckb_aca_bol') . "," .
        "aca_infoger_pix = " . valoresCheck('ckb_aca_pix') . "," .
        "aca_body = " . valoresCheck('ckb_aca_body') . "," .
        "aca_infoger_acess = " . valoresCheck('ckb_aca_acesso') . "," .
        "aca_rem_plano = " . valoresCheck('ckb_aca_rem_plano') . "," .
        "aca_can_plano = " . valoresCheck('ckb_aca_can_plano') . "," .
        "out_incluir_serasa = " . valoresCheck('ckb_out_incserasa') . "," .
        "out_remover_serasa = " . valoresCheck('ckb_out_remserasa') . "," .
        "aca_body_treino = " . valoresCheck('ckb_aca_body_trei') . "," .
        "aca_body_post = " . valoresCheck('ckb_aca_body_post') . "," .
        "aca_body_estag = " . valoresCheck('ckb_aca_body_estag') . "," .
        "aca_usu_resp = " . valoresCheck('ckb_aca_usu_resp') . "," .
        "adm_cpf_obrig = " . valoresCheck('ckb_adm_cpf_obrig') . "," .                
        "adm_cpf_limite = " . valoresNumericos('txt_adm_cpf_limite') . "," .
        "fin_desc_plano = " . valoresCheck('ckb_pro_desc_plano') . "," .                
        "fin_desc_plano_tot = " . valoresNumericos('txt_pro_desc_plano') . "," .
        "fin_desc_serv = " . valoresCheck('ckb_pro_desc_servico') . "," .                
        "fin_desc_serv_tot = " . valoresNumericos('txt_pro_desc_servico') . "," .                
        "aca_cli_block = " . valoresCheck('ckb_aca_cli_block') . "," .
        "crm_upt_rem = " . valoresCheck('ckb_crm_upt_rem') . "," .
        "crm_upt_rem_new = " . valoresCheck('ckb_crm_upt_rem_new') . "," .
        "crm_pro_ure = " . valoresCheck('ckb_crm_pro_ure') . "," .
        "crm_cli_ure = " . valoresCheck('ckb_crm_cli_ure') . "," .
        "use_dist_pro = " . valoresCheck('ckb_use_dist_pro') . "," .
        "crm_chatbot = " . valoresCheck('ckb_crm_chatbot') . "," .
        "crm_lea_ure = " . valoresCheck('ckb_crm_lea_ure') . "," .
        "crm_lead = " . valoresCheck('ckb_crm_lead') . "," .
        "crm_lead_camp = " . valoresCheck('ckb_crm_lead_camp') . "," .
        "aca_cntt_dtCanc = " . valoresCheck('ckb_aca_cntt_dtCanc') . "," .
        "fin_cancel_multa = " . valoresCheck('ckb_fin_cancel_multa') . "," .
        "fin_cx_ticket = " . valoresCheck('ckb_fin_cx_ticket') . "," .
        "fin_cx_mat = " . valoresCheck('ckb_fin_cx_mat') . "," .
        "fin_cx_mod = " . valoresCheck('ckb_fin_cx_mod') . "," .                    
        "fin_lpg = " . valoresCheck('ckb_fin_lpg') . "," .                    
        "com_forma = " . valoresCheck('ckb_com_forma') . "," . 
        "ckb_pro_sinistro = " . valoresCheck('ckb_pro_sinistro') . "," . 
        "ckb_pro_cotas = " . valoresCheck('ckb_pro_cotas') . "," . 
        "ckb_pro_fipe = " . valoresCheck('ckb_pro_fipe') . "," . 
        "ckb_cpagar_read = " . valoresCheck('ckb_cpagar_read') . "," . 
        "ckb_creceber_read = " . valoresCheck('ckb_creceber_read') . "," . 
        "ckb_fcaixa_read = " . valoresCheck('ckb_fcaixa_read') . "," .                 
        "clb_locacoes = " . valoresCheck('ckb_clb_loc') . "," .
        "clb_eventos = " . valoresCheck('ckb_clb_eventos') . "," .
        "clb_categoria = " . valoresCheck('ckb_clb_categoria') . "," .
        "clb_tipo = " . valoresCheck('ckb_clb_tipo') . "," .
        "clb_layout_remessa = " . valoresCheck('ckb_clb_layout_remessa') . "," .
        "clb_layout_retorno = " . valoresCheck('ckb_clb_layout_retorno') . "," .
        "clb_excecao_vencimento = " . valoresCheck('ckb_clb_excecao_vencimento') . "," .
        "clb_gestao_documentos = " . valoresCheck('ckb_clb_gestao_documentos') . "," .
        "clb_gestao_folha = " . valoresCheck('ckb_clb_gestao_folha') . "," .
        "clb_excluir_convites = " . valoresCheck('ckb_clb_excluir_convites') . "," .
        "clb_baixar_layout = " . valoresCheck('ckb_clb_baixar_layout') . "," .
        "clb_gboleto = " . valoresCheck('ckb_clb_gboleto') . "," .
        "clb_cboleto = " . valoresCheck('ckb_clb_cboleto') . "," .                
        "exc_cpagar = " . valoresCheck('ckb_exc_cpagar') . "," .
        "exc_creceber = " . valoresCheck('ckb_exc_creceber') . "," .                
        "clb_gconvite = " . valoresCheck('ckb_clb_gconvite') . "," .
        "clb_rel_eventos = " . valoresCheck('ckb_clb_rel_eventos') . "," .
        "clb_rel_socios = " . valoresCheck('ckb_clb_rel_socios') . "," .        
        "ckb_pro_grp_acess = " . valoresCheck('ckb_pro_grp_acess') . "," .
        "ckb_pro_classificacao = " . valoresCheck('ckb_pro_classificacao') . "," .
        "ckb_pro_montadoras = " . valoresCheck('ckb_pro_montadoras') . "," .
        "ckb_pro_modelos = " . valoresCheck('ckb_pro_modelos') . "," .
        "ckb_pro_serv_mod = " . valoresCheck('ckb_pro_serv_mod') . "," .
        "ckb_pro_acess = " . valoresCheck('ckb_pro_acess') . "," .
        "ckb_pro_tipo = " . valoresCheck('ckb_pro_tipo') . "," .
        "ckb_pro_item_vis = " . valoresCheck('ckb_pro_item_vis') . "," .
        "ckb_pro_veiculo = " . valoresCheck('ckb_pro_veiculo') . "," .
        "ckb_pro_vistoria = " . valoresCheck('ckb_pro_vistoria') . "," .                                
        "ckb_pro_grp_cidade = " . valoresCheck('ckb_pro_grp_cidade') . "," .                                
        "ckb_pro_grp_fabricantes = " . valoresCheck('ckb_pro_grp_fabricantes') . "," .                                
        "ckb_pro_apv_veic = " . valoresCheck('ckb_pro_apv_veic') . "," .                              
        "ckb_pro_apv_sin = " . valoresCheck('ckb_pro_apv_sin') . "," .                              
        "ckb_pro_rastreador = " . valoresCheck('ckb_pro_rastreador') . "," .                              
        "ckb_pro_oficina = " . valoresCheck('ckb_pro_oficina') . "," .                              
        "ckb_pro_exc_vei = " . valoresCheck('ckb_pro_exc_vei') . "," .                                
        "ckb_pro_cad_veic = " . valoresCheck('ckb_pro_cad_veic') . "," .                                
        "aca_cntt_dtCanc_limit = " . valoresCheck('ckb_aca_cntt_dtCanc_limit') . "," . 
        "aca_perm_desc = " . valoresCheck('ckb_aca_perm_desc') . "," .                
        "aca_one_item = " . valoresCheck('ckb_aca_one_item') . "," .                
        "clb_trancamento = " . valoresCheck('ckb_clb_trancar_tit') . "," . 
        "bi_auditoria = " . valoresCheck('ckb_bi_auditoria') . "," .
        "bi_mat_ren = " . valoresCheck('ckb_bi_mat_ren') . "," .
        "bi_gve_sfin = " . valoresCheck('ckb_bi_gve_sfin') . "," .
        "bi_retencao = " . valoresCheck('ckb_bi_retencao') . "," .
        "bi_evasao = " . valoresCheck('ckb_bi_evasao') . "," .
        "bi_inadimplencia = " . valoresCheck('ckb_bi_inadimplencia') . "," .
        "bi_prev_fin = " . valoresCheck('ckb_bi_prev_fin') . "," .
        "bi_cen_custo = " . valoresCheck('ckb_bi_cen_custo') . "," .
        "bi_fluxo_caixa = " . valoresCheck('ckb_bi_fluxo_caixa') . "," .
        "bi_faturamento = " . valoresCheck('ckb_bi_faturamento') . "," .
        "bi_com_faturamento = " . valoresCheck('ckb_bi_com_faturamento') . "," .
        "bi_leads = " . valoresCheck('ckb_bi_leads') . "," .
        "bi_formas_pagamento = " . valoresCheck('ckb_bi_formas_pagamento') . "," .
        "bi_gestao_vendas = " . valoresCheck('ckb_bi_gestao_vendas') . "," .
        "bi_gestao_mapa = " . valoresCheck('ckb_bi_gestao_mapa') . "," .
        "bi_produtividade = " . valoresCheck('ckb_bi_produtividade') . "," .
        "bi_com_produtividade = " . valoresCheck('ckb_bi_com_produtividade') . "," .
        "bi_com_comissionado = " . valoresCheck('ckb_bi_com_comissionado') . "," .
        "bi_atendimentos = " . valoresCheck('ckb_bi_atendimentos') . "," .
        "bi_prospects = " . valoresCheck('ckb_bi_prospects') . "," .
        "bi_agendamentos = " . valoresCheck('ckb_bi_agendamentos') . "," .
        "bi_turmas = " . valoresCheck('ckb_bi_turmas') . "," .
        "bi_rel_acessos = " . valoresCheck('ckb_bi_rel_acessos') . "," .
        "ckb_pro_rastreadores = " . valoresCheck('ckb_pro_rastreadores') . "," .
        "ckb_pro_terceiros = " . valoresCheck('ckb_pro_terceiros') . "," .
        "ckb_pro_operadoras = " . valoresCheck('ckb_pro_operadoras') . "," .
        "nome_permissao = '" . utf8_decode($_POST['txtNomePerm']) . "' where id_permissoes = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
    } else {    
        $query = "INSERT INTO sf_usuarios_permissoes(nome_permissao,adm_cli,adm_for,adm_fun,adm_usu,adm_per,adm_gru,adm_dep,adm_config,adm_email,adm_disparos,adm_agenda,adm_contratos,adm_atendimentos,adm_parentesco,
        fin_doc,fin_ban,fin_con,fin_mov,fin_fix,fin_afix,fin_flu,fin_lsol,fin_asol,fin_pag,fin_rec,fin_rel_mov_caixa,est_mov,est_ccu,est_pro,est_ser,est_spc,est_loc,est_lven,est_lven_read,est_aven,
        est_lorc,est_aorc,fin_lped,fin_lcot,fin_aped,est_lcom,est_acom,est_flu,est_com,est_cor,est_tam,est_fab,est_pla,est_ent_sai,est_aent_sai,est_jus,crm_cli,crm_fch,crm_pro,crm_tel,crm_esp,crm_ind,crm_rec,out_ges,
        nfe_tra,nfe_msg,nfe_emi,nfe_est,nfe_cid,nfe_enq,nfe_not,nfe_ess,nfe_cen,ser_cli,ser_ser,out_aju, out_term,out_terc,out_rban,out_rexc,out_ecob,out_todos_operadores,out_desconto,out_pag_avancada,pag_avancada,fin_dmp,crm_cpc,crm_exc_atend,crm_fec_atend,
        adm_cli_read,adm_cli_read_obs,fin_chq,fin_rcrt,fin_crt,fin_bol,fin_rel_gct,fin_tdd,siv_cadacad, siv_caderp, siv_gersenha, siv_usersite, siv_dcc, siv_cregb, siv_pengb,siv_logerp,
        aca_hor,aca_conv,aca_cred,aca_catr,aca_leif,aca_amb,aca_fer,aca_fer_par,aca_fer_exc,aca_pla,aca_ser, aca_alt_val_mens,aca_pro_rata_mens,aca_documento,aca_est_mens,aca_exc_mens,aca_exc_mret,aca_inf_gerenciais,aca_add_convenio, ACA_ADD_CREDENCIAL, aca_dtf_credencial, aca_add_bloqueio, aca_cancelar_dcc, aca_plan_comp,aca_pgr,aca_rel_acesso,aca_turmas,aca_estudio, aca_documentos,
        adm_email_obrig,adm_tel_obrig,adm_cel_obrig,adm_end_obrig,adm_comissao,clb_tit_obrig,adm_proc_obrig,adm_sexo_obrig,aca_abonar_mens,aca_abonar_cre,aca_abonar_deb,aca_pg_prod,aca_can_item,aca_data_inicio,aca_agenda_regras,aca_perm_desc,aca_one_item,aca_infoger_caixa,aca_infoger_stats,aca_infoger_contr,aca_infoger_filav,aca_infoger_renov,aca_infoger_dcc,aca_infoger_bol,aca_infoger_pix,aca_infoger_acess,aca_rem_plano,aca_can_plano,out_incluir_serasa,out_remover_serasa,aca_body_treino,aca_body_post,aca_body_estag,aca_usu_resp,adm_cpf_obrig,
        adm_cpf_limite, fin_desc_plano, fin_desc_plano_tot, fin_desc_serv, fin_desc_serv_tot, aca_body,aca_cli_block,
        crm_upt_rem,crm_upt_rem_new,crm_pro_ure,crm_cli_ure,use_dist_pro,crm_chatbot,crm_lea_ure,crm_lead,crm_lead_camp,aca_cntt_dtCanc,fin_cancel_multa,fin_cx_ticket,fin_cx_mat,fin_cx_mod,fin_lpg,com_forma,ckb_pro_sinistro,ckb_pro_cotas,ckb_pro_fipe,ckb_cpagar_read,ckb_creceber_read,ckb_fcaixa_read,
        clb_locacoes,clb_eventos,clb_categoria,clb_tipo,clb_layout_remessa,clb_layout_retorno,clb_excecao_vencimento,clb_gestao_documentos,clb_gestao_folha,clb_excluir_convites,clb_baixar_layout,clb_gboleto,clb_cboleto,exc_cpagar,exc_creceber,clb_gconvite,clb_rel_eventos,clb_rel_socios,aca_cntt_dtCanc_limit,
        ckb_pro_grp_acess,ckb_pro_classificacao,ckb_pro_montadoras,ckb_pro_modelos,ckb_pro_serv_mod,ckb_pro_acess, ckb_pro_tipo, ckb_pro_item_vis, ckb_pro_veiculo, ckb_pro_vistoria, ckb_pro_grp_cidade, ckb_pro_grp_fabricantes, ckb_pro_apv_veic, ckb_pro_apv_sin, ckb_pro_rastreador, ckb_pro_oficina, ckb_pro_exc_vei, ckb_pro_cad_veic, clb_trancamento, bi_auditoria,bi_mat_ren,bi_gve_sfin,bi_retencao,bi_evasao,bi_inadimplencia,bi_prev_fin,bi_cen_custo,bi_fluxo_caixa,bi_faturamento,bi_com_faturamento,bi_leads,bi_formas_pagamento,
        bi_gestao_vendas,bi_gestao_mapa,bi_produtividade,bi_com_produtividade,bi_com_comissionado,bi_atendimentos,bi_prospects,bi_agendamentos,bi_turmas,bi_rel_acessos,ckb_pro_rastreadores,ckb_pro_terceiros,ckb_pro_operadoras) VALUES ('" .
        utf8_decode($_POST['txtNomePerm']) . "'," .
        valoresCheck('ckb_adm_cli') . "," .
        valoresCheck('ckb_adm_for') . "," .
        valoresCheck('ckb_adm_fun') . "," .
        valoresCheck('ckb_adm_usu') . "," .
        valoresCheck('ckb_adm_per') . "," .
        valoresCheck('ckb_adm_gru') . "," .
        valoresCheck('ckb_adm_dep') . "," .
        valoresCheck('ckb_adm_config') . "," .
        valoresCheck('ckb_adm_email') . "," .
        valoresCheck('ckb_adm_disparos') . "," .
        valoresCheck('ckb_adm_agenda') . "," .
        valoresCheck('ckb_adm_contratos') . "," .                
        valoresCheck('ckb_adm_atendimentos') . "," .                
        valoresCheck('ckb_adm_parentesco') . "," .
        valoresCheck('ckb_fin_doc') . "," .
        valoresCheck('ckb_fin_ban') . "," .
        valoresCheck('ckb_fin_con') . "," .
        valoresCheck('ckb_fin_mov') . "," .
        valoresCheck('ckb_fin_fix') . "," .
        valoresCheck('ckb_fin_afix') . "," .
        valoresCheck('ckb_fin_flu') . "," .
        valoresCheck('ckb_fin_lsol') . "," .
        valoresCheck('ckb_fin_asol') . "," .
        valoresCheck('ckb_fin_pag') . "," .
        valoresCheck('ckb_fin_rec') . "," .
        valoresCheck('ckb_fin_rel_mov_caixa') . "," .
        valoresCheck('ckb_est_mov') . "," .
        valoresCheck('ckb_est_ccu') . "," .
        valoresCheck('ckb_est_pro') . "," .
        valoresCheck('ckb_est_ser') . "," .
        valoresCheck('ckb_ser_spc') . "," .
        valoresCheck('ckb_est_loc') . "," .
        valoresCheck('ckb_est_lven') . "," .
        valoresCheck('ckb_est_lven_read') . "," .
        valoresCheck('ckb_est_aven') . "," .
        valoresCheck('ckb_est_lorc') . "," .
        valoresCheck('ckb_est_aorc') . "," .
        valoresCheck('ckb_fin_lped') . "," .
        valoresCheck('ckb_fin_lcot') . "," .
        valoresCheck('ckb_fin_aped') . "," .
        valoresCheck('ckb_est_lcom') . "," .
        valoresCheck('ckb_est_acom') . "," .
        valoresCheck('ckb_est_flu') . "," .
        valoresCheck('ckb_est_com') . "," .
        valoresCheck('ckb_est_cor') . "," .
        valoresCheck('ckb_est_tam') . "," .
        valoresCheck('ckb_est_fab') . "," .
        valoresCheck('ckb_est_pla') . "," .                
        valoresCheck('ckb_est_ent_sai') . "," .
        valoresCheck('ckb_est_aent_sai') . "," .
        valoresCheck('ckb_est_jus') . "," .                
        valoresCheck('ckb_crm_cli') . "," .
        valoresCheck('ckb_crm_fch') . "," .
        valoresCheck('ckb_crm_pro') . "," .
        valoresCheck('ckb_crm_tel') . "," .
        valoresCheck('ckb_crm_esp') . "," .
        valoresCheck('ckb_crm_ind') . "," .
        valoresCheck('ckb_crm_rec') . "," .
        valoresCheck('ckb_out_ges') . "," .
        valoresCheck('ckb_nfe_cen') . "," .
        valoresCheck('ckb_nfe_tra') . "," .
        valoresCheck('ckb_nfe_msg') . "," .
        valoresCheck('ckb_nfe_emi') . "," .
        valoresCheck('ckb_nfe_est') . "," .
        valoresCheck('ckb_nfe_cid') . "," .
        valoresCheck('ckb_nfe_enq') . "," .
        valoresCheck('ckb_nfe_not') . "," .
        valoresCheck('ckb_nfe_ess') . "," .
        valoresCheck('ckb_ser_cli') . "," .
        valoresCheck('ckb_ser_ser') . "," .
        valoresCheck('ckb_out_aju') . "," .
        valoresCheck('ckb_out_term') . "," .
        valoresCheck('ckb_out_terc') . "," .
        valoresCheck('ckb_out_rban') . "," .
        valoresCheck('ckb_out_rexc') . "," .
        valoresCheck('ckb_out_ecob') . "," .
        valoresCheck('ckb_out_todos_operadores') . "," .
        valoresCheck('ckb_out_desconto') . "," .
        valoresCheck('ckb_out_pag_avancada') . "," .
        valoresNumericos('txt_out_pag_avancada') . "," .
        valoresCheck('ckb_fin_dmp') . "," .
        valoresCheck('ckb_crm_cpc') . "," .
        valoresCheck('ckb_crm_exc_atend') . "," .
        valoresCheck('ckb_crm_fec_atend') . "," .
        valoresCheck('ckb_adm_cli_read') . "," .
        valoresCheck('ckb_adm_cli_read_obs') . "," .
        valoresCheck('ckb_fin_chq') . "," .
        valoresCheck('ckb_fin_rcrt') . "," .
        valoresCheck('ckb_fin_crt') . "," .
        valoresCheck('ckb_fin_bol') . "," .
        valoresCheck('ckb_fin_rel_gct') . "," .
        valoresCheck('ckb_fin_rel_tdd') . "," .
        valoresCheck('ckb_siv_cadAcad') . "," .
        valoresCheck('ckb_siv_cadErp') . "," .
        valoresCheck('ckb_siv_GerSenha') . "," .
        valoresCheck('ckb_siv_UserSite') . "," .
        valoresCheck('ckb_siv_DCC') . "," .
        valoresCheck('ckb_siv_CreGB') . "," .
        valoresCheck('ckb_siv_PenGB') . "," .
        valoresCheck('ckb_siv_LogERP') . "," .
        valoresCheck('ckb_aca_hor') . "," .
        valoresCheck('ckb_aca_conv') . "," .
        valoresCheck('ckb_aca_cred') . "," .
        valoresCheck('ckb_aca_catr') . "," .
        valoresCheck('ckb_aca_leif') . "," .
        valoresCheck('ckb_aca_amb') . "," .
        valoresCheck('ckb_aca_fer') . "," .
        valoresCheck('ckb_aca_fer_par') . "," .
        valoresCheck('ckb_aca_fer_exc') . "," .
        valoresCheck('ckb_aca_pla') . "," .
        valoresCheck('ckb_aca_ser') . "," .
        valoresCheck('ckb_aca_alt_val_mens') . "," .                
        valoresCheck('ckb_aca_pro_rata_mens') . "," .
        valoresCheck('ckb_aca_documento') . "," .                
        valoresCheck('ckb_aca_est_mens') . "," .
        valoresCheck('ckb_aca_exc_mens') . "," .
        valoresCheck('ckb_aca_exc_mret') . "," .
        valoresCheck('ckb_aca_inf_gerenciais') . "," .
        valoresCheck('ckb_aca_add_convenio') . "," .
        valoresCheck('ckb_aca_add_credencial') . "," .
        valoresCheck('ckb_aca_dtf_credencial') . "," .
        valoresCheck('ckb_aca_add_bloqueios') . "," .
        valoresCheck('ckb_aca_cancelar_dcc') . "," .
        valoresCheck('ckb_aca_plan_comp') . "," .
        valoresCheck('ckb_aca_pg_retro') . "," .
        valoresCheck('ckb_aca_rel_acesso') . "," .
        valoresCheck('ckb_aca_turmas') . "," .
        valoresCheck('ckb_aca_estudio') . "," .
        valoresCheck('ckb_aca_documentos') . "," .
        valoresCheck('ckb_adm_emlobg') . "," .
        valoresCheck('ckb_adm_telobg') . "," .
        valoresCheck('ckb_adm_celobg') . "," .
        valoresCheck('ckb_adm_endobg') . "," .
        valoresCheck('ckb_adm_comissao') . "," .
        valoresCheck('ckb_clb_titobr') . "," .                
        valoresCheck('ckb_adm_procobg') . "," .                
        valoresCheck('ckb_adm_sexoobg') . "," .                
        valoresCheck('ckb_aca_abomens') . "," .                
        valoresCheck('ckb_aca_abocre') . "," .
        valoresCheck('ckb_aca_abodeb') . "," .                                
        valoresCheck('ckb_aca_pg_prod') . "," .                                
        valoresCheck('ckb_aca_can_item') . "," .                                
        valoresCheck('ckb_aca_data_inicio') . "," .                                
        valoresCheck('ckb_aca_agenda_regras') . "," .                                
        valoresCheck('ckb_aca_perm_desc') . "," .
        valoresCheck('ckb_aca_one_item') . "," .
        valoresCheck('ckb_aca_igCaixa') . "," .
        valoresCheck('ckb_aca_igStatus') . "," .
        valoresCheck('ckb_aca_Contrato') . "," .
        valoresCheck('ckb_aca_filAvn') . "," .
        valoresCheck('ckb_aca_renovacoes') . "," .
        valoresCheck('ckb_aca_dcc') . "," .
        valoresCheck('ckb_aca_bol') . "," .
        valoresCheck('ckb_aca_pix') . "," .
        valoresCheck('ckb_aca_acesso') . "," .
        valoresCheck('ckb_aca_rem_plano') . "," .
        valoresCheck('ckb_aca_can_plano') . "," .
        valoresCheck('ckb_out_incserasa') . "," .
        valoresCheck('ckb_out_remserasa') . "," .
        valoresCheck('ckb_aca_body_trei') . "," .
        valoresCheck('ckb_aca_body_post') . "," .
        valoresCheck('ckb_aca_body_estag') . "," .
        valoresCheck('ckb_aca_usu_resp') . "," .
        valoresCheck('ckb_adm_cpf_obrig') . "," .                
        valoresNumericos('txt_adm_cpf_limite') . "," .
        valoresCheck('ckb_pro_desc_plano') . "," .                
        valoresNumericos('txt_pro_desc_plano') . "," .
        valoresCheck('ckb_pro_desc_servico') . "," .                
        valoresNumericos('txt_pro_desc_servico') . "," .                
        valoresCheck('ckb_aca_body') . "," .
        valoresCheck('ckb_aca_cli_block') . "," .
        valoresCheck('ckb_crm_upt_rem') . "," .
        valoresCheck('ckb_crm_upt_rem_new') . "," .                 
        valoresCheck('ckb_crm_pro_ure') . "," .                 
        valoresCheck('ckb_crm_cli_ure') . "," .                 
        valoresCheck('ckb_use_dist_pro') . "," . 
        valoresCheck('ckb_crm_chatbot') . "," .                
        valoresCheck('ckb_crm_lea_ure') . "," .                 
        valoresCheck('ckb_crm_lead') . "," .                 
        valoresCheck('ckb_crm_lead_camp') . "," .                 
        valoresCheck('ckb_aca_cntt_dtCanc') . "," .                
        valoresCheck('ckb_fin_cancel_multa') . "," .
        valoresCheck('ckb_fin_cx_ticket') . "," .
        valoresCheck('ckb_fin_cx_mat') . "," .                
        valoresCheck('ckb_fin_cx_mod') . "," .                  
        valoresCheck('ckb_fin_lpg') . "," .                  
        valoresCheck('ckb_com_forma') . "," .
        valoresCheck('ckb_pro_sinistro') . "," .
        valoresCheck('ckb_pro_cotas') . "," .
        valoresCheck('ckb_pro_fipe') . "," .
        valoresCheck('ckb_cpagar_read') . "," .
        valoresCheck('ckb_creceber_read') . "," .
        valoresCheck('ckb_fcaixa_read') . "," .                
        valoresCheck('ckb_clb_loc') . "," .
        valoresCheck('ckb_clb_eventos') . "," .
        valoresCheck('ckb_clb_categoria') . "," .
        valoresCheck('ckb_clb_tipo') . "," .
        valoresCheck('ckb_clb_layout_remessa') . "," .
        valoresCheck('ckb_clb_layout_retorno') . "," .
        valoresCheck('ckb_clb_excecao_vencimento') . "," .
        valoresCheck('ckb_clb_gestao_documentos') . "," .
        valoresCheck('ckb_clb_gestao_folha') . "," .
        valoresCheck('ckb_clb_excluir_convites') . "," .
        valoresCheck('ckb_clb_baixar_layout') . "," .
        valoresCheck('ckb_clb_gboleto') . "," .
        valoresCheck('ckb_clb_cboleto') . "," .                
        valoresCheck('ckb_exc_cpagar') . "," .
        valoresCheck('ckb_exc_creceber') . "," .
        valoresCheck('ckb_clb_gconvite') . "," .
        valoresCheck('ckb_clb_rel_eventos') . "," .
        valoresCheck('ckb_clb_rel_socios') . "," .
        valoresCheck('ckb_aca_cntt_dtCanc_limit') . "," .
        valoresCheck('ckb_pro_grp_acess') . "," .
        valoresCheck('ckb_pro_classificacao') . "," .
        valoresCheck('ckb_pro_montadoras') . "," .
        valoresCheck('ckb_pro_modelos') . "," .
        valoresCheck('ckb_pro_serv_mod') . "," .
        valoresCheck('ckb_pro_acess') . "," .
        valoresCheck('ckb_pro_tipo') . "," .
        valoresCheck('ckb_pro_item_vis') . "," .
        valoresCheck('ckb_pro_veiculo') . "," .
        valoresCheck('ckb_pro_vistoria') . "," .                       
        valoresCheck('ckb_pro_grp_cidade') . "," .                       
        valoresCheck('ckb_pro_grp_fabricantes') . "," .                       
        valoresCheck('ckb_pro_apv_veic') . "," .                       
        valoresCheck('ckb_pro_apv_sin') . "," .                       
        valoresCheck('ckb_pro_rastreador') . "," .                       
        valoresCheck('ckb_pro_oficina') . "," .                       
        valoresCheck('ckb_pro_exc_vei') . "," .                       
        valoresCheck('ckb_pro_cad_veic') . "," .                       
        valoresCheck('ckb_clb_trancar_tit') . "," .                   
        valoresCheck('ckb_bi_auditoria') . "," .
        valoresCheck('ckb_bi_mat_ren') . "," .
        valoresCheck('ckb_bi_gve_sfin') . "," .
        valoresCheck('ckb_bi_retencao') . "," .
        valoresCheck('ckb_bi_evasao') . "," .
        valoresCheck('ckb_bi_inadimplencia') . "," .
        valoresCheck('ckb_bi_prev_fin') . "," .
        valoresCheck('ckb_bi_cen_custo') . "," .
        valoresCheck('ckb_bi_fluxo_caixa') . "," .
        valoresCheck('ckb_bi_faturamento') . "," .
        valoresCheck('ckb_bi_com_faturamento') . "," .
        valoresCheck('ckb_bi_leads') . "," .
        valoresCheck('ckb_bi_formas_pagamento') . "," .
        valoresCheck('ckb_bi_gestao_vendas') . "," .
        valoresCheck('ckb_bi_gestao_mapa') . "," .
        valoresCheck('ckb_bi_produtividade') . "," .
        valoresCheck('ckb_bi_com_produtividade') . "," .
        valoresCheck('ckb_bi_com_comissionado') . "," .
        valoresCheck('ckb_bi_atendimentos') . "," .
        valoresCheck('ckb_bi_prospects') . "," .
        valoresCheck('ckb_bi_agendamentos') . "," .
        valoresCheck('ckb_bi_turmas') . "," .
        valoresCheck('ckb_bi_rel_acessos') . "," .                
        valoresCheck('ckb_pro_rastreadores') . "," .
        valoresCheck('ckb_pro_terceiros') . "," .
        valoresCheck('ckb_pro_operadoras') . ")";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_permissoes from sf_usuarios_permissoes order by id_permissoes desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);                       
    }
}
if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        odbc_exec($con, "DELETE FROM sf_usuarios_permissoes WHERE id_permissoes = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled ';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_usuarios_permissoes where id_permissoes = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_permissoes']);
        $nomeperm = utf8_encode($RFP['nome_permissao']);
        $ckb_adm_cli = utf8_encode($RFP['adm_cli']);
        $ckb_adm_for = utf8_encode($RFP['adm_for']);
        $ckb_adm_fun = utf8_encode($RFP['adm_fun']);
        $ckb_adm_usu = utf8_encode($RFP['adm_usu']);
        $ckb_adm_per = utf8_encode($RFP['adm_per']);
        $ckb_adm_gru = utf8_encode($RFP['adm_gru']);
        $ckb_adm_dep = utf8_encode($RFP['adm_dep']);
        $ckb_adm_config = utf8_encode($RFP['adm_config']);
        $ckb_adm_email = utf8_encode($RFP['adm_email']);
        $ckb_adm_disparos = utf8_encode($RFP['adm_disparos']);
        $ckb_adm_agenda = utf8_encode($RFP['adm_agenda']);
        $ckb_adm_contratos = utf8_encode($RFP['adm_contratos']);
        $ckb_adm_atendimentos = utf8_encode($RFP['adm_atendimentos']);
        $ckb_adm_parentesco = utf8_encode($RFP['adm_parentesco']);
        $ckb_fin_doc = utf8_encode($RFP['fin_doc']);
        $ckb_fin_ban = utf8_encode($RFP['fin_ban']);
        $ckb_fin_con = utf8_encode($RFP['fin_con']);
        $ckb_fin_mov = utf8_encode($RFP['fin_mov']);
        $ckb_fin_fix = utf8_encode($RFP['fin_fix']);
        $ckb_fin_afix = utf8_encode($RFP['fin_afix']);
        $ckb_fin_flu = utf8_encode($RFP['fin_flu']);
        $ckb_fin_lsol = utf8_encode($RFP['fin_lsol']);
        $ckb_fin_asol = utf8_encode($RFP['fin_asol']);
        $ckb_fin_pag = utf8_encode($RFP['fin_pag']);
        $ckb_fin_rec = utf8_encode($RFP['fin_rec']);
        $ckb_est_mov = utf8_encode($RFP['est_mov']);
        $ckb_est_ccu = utf8_encode($RFP['est_ccu']);
        $ckb_est_pro = utf8_encode($RFP['est_pro']);
        $ckb_est_ser = utf8_encode($RFP['est_ser']);
        $ckb_ser_spc = utf8_encode($RFP['est_spc']);
        $ckb_est_loc = utf8_encode($RFP['est_loc']);
        $ckb_est_lven = utf8_encode($RFP['est_lven']);
        $ckb_est_lven_read = utf8_encode($RFP['est_lven_read']);
        $ckb_est_aven = utf8_encode($RFP['est_aven']);
        $ckb_est_lorc = utf8_encode($RFP['est_lorc']);
        $ckb_est_aorc = utf8_encode($RFP['est_aorc']);
        $ckb_fin_lped = utf8_encode($RFP['fin_lped']);
        $ckb_fin_lcot = utf8_encode($RFP['fin_lcot']);
        $ckb_fin_aped = utf8_encode($RFP['fin_aped']);
        $ckb_est_lcom = utf8_encode($RFP['est_lcom']);
        $ckb_est_acom = utf8_encode($RFP['est_acom']);
        $ckb_est_flu = utf8_encode($RFP['est_flu']);
        $ckb_est_com = utf8_encode($RFP['est_com']);
        $ckb_est_cor = utf8_encode($RFP['est_cor']);
        $ckb_est_tam = utf8_encode($RFP['est_tam']);
        $ckb_est_fab = utf8_encode($RFP['est_fab']);
        $ckb_est_pla = utf8_encode($RFP['est_pla']);        
        $ckb_est_jus = utf8_encode($RFP['est_jus']);
        $ckb_est_ent_sai = utf8_encode($RFP['est_ent_sai']);
        $ckb_est_aent_sai = utf8_encode($RFP['est_aent_sai']);        
        $ckb_nfe_cen = utf8_encode($RFP['nfe_cen']);
        $ckb_nfe_tra = utf8_encode($RFP['nfe_tra']);
        $ckb_nfe_msg = utf8_encode($RFP['nfe_msg']);
        $ckb_nfe_emi = utf8_encode($RFP['nfe_emi']);
        $ckb_nfe_est = utf8_encode($RFP['nfe_est']);
        $ckb_nfe_cid = utf8_encode($RFP['nfe_cid']);
        $ckb_nfe_enq = utf8_encode($RFP['nfe_enq']);
        $ckb_nfe_not = utf8_encode($RFP['nfe_not']);
        $ckb_nfe_ess = utf8_encode($RFP['nfe_ess']);
        $ckb_crm_cli = utf8_encode($RFP['crm_cli']);
        $ckb_crm_fch = utf8_encode($RFP['crm_fch']);
        $ckb_crm_pro = utf8_encode($RFP['crm_pro']);
        $ckb_crm_tel = utf8_encode($RFP['crm_tel']);
        $ckb_crm_esp = utf8_encode($RFP['crm_esp']);
        $ckb_crm_ind = utf8_encode($RFP['crm_ind']);
        $ckb_crm_rec = utf8_encode($RFP['crm_rec']);
        $ckb_out_ges = utf8_encode($RFP['out_ges']);
        $ckb_out_aju = utf8_encode($RFP['out_aju']);
        $ckb_out_term = utf8_encode($RFP['out_term']);
        $ckb_out_terc = utf8_encode($RFP['out_terc']);
        $ckb_out_rban = utf8_encode($RFP['out_rban']);
        $ckb_out_rexc = utf8_encode($RFP['out_rexc']);
        $ckb_out_ecob = utf8_encode($RFP['out_ecob']);
        $ckb_out_todos_operadores = utf8_encode($RFP['out_todos_operadores']);
        $ckb_out_desconto = utf8_encode($RFP['out_desconto']);
        $ckb_out_pag_avancada = utf8_encode($RFP['out_pag_avancada']);
        $txt_out_pag_avancada = utf8_encode($RFP['pag_avancada']);
        $ckb_ser_cli = utf8_encode($RFP['ser_cli']);
        $ckb_ser_ser = utf8_encode($RFP['ser_ser']);
        $ckb_fin_dmp = utf8_encode($RFP['fin_dmp']);
        $ckb_crm_cpc = utf8_encode($RFP['crm_cpc']);
        $ckb_crm_exc_atend = utf8_encode($RFP['crm_exc_atend']);
        $ckb_crm_fec_atend = utf8_encode($RFP['crm_fec_atend']);
        $ckb_adm_cli_read = utf8_encode($RFP['adm_cli_read']);
        $ckb_adm_cli_read_obs = utf8_encode($RFP['adm_cli_read_obs']);
        $ckb_fin_rel_gct = utf8_encode($RFP['fin_rel_gct']);
        $ckb_fin_rel_tdd = utf8_encode($RFP['fin_tdd']);
        $ckb_fin_chq = utf8_encode($RFP['fin_chq']);
        $ckb_fin_rcrt = utf8_encode($RFP['fin_rcrt']);
        $ckb_fin_crt = utf8_encode($RFP['fin_crt']);
        $ckb_fin_bol = utf8_encode($RFP['fin_bol']);
        $ckb_fin_rel_mov_caixa = utf8_encode($RFP['fin_rel_mov_caixa']);
        $ckb_siv_cadAcad = utf8_encode($RFP['siv_cadacad']);
        $ckb_siv_cadErp = utf8_encode($RFP['siv_caderp']);
        $ckb_siv_GerSenha = utf8_encode($RFP['siv_gersenha']);
        $ckb_siv_UserSite = utf8_encode($RFP['siv_usersite']);
        $ckb_siv_DCC = utf8_encode($RFP['siv_dcc']);
        $ckb_siv_CreGB = utf8_encode($RFP['siv_cregb']);
        $ckb_siv_PenGB = utf8_encode($RFP['siv_pengb']);
        $ckb_siv_LogERP = utf8_encode($RFP['siv_logerp']);
        $ckb_aca_hor = utf8_encode($RFP['aca_hor']);
        $ckb_aca_conv = utf8_encode($RFP['aca_conv']);
        $ckb_aca_cred = utf8_encode($RFP['aca_cred']);
        $ckb_aca_catr = utf8_encode($RFP['aca_catr']);
        $ckb_aca_leif = utf8_encode($RFP['aca_leif']);
        $ckb_aca_amb = utf8_encode($RFP['aca_amb']);
        $ckb_aca_fer = utf8_encode($RFP['aca_fer']);
        $ckb_aca_fer_par = utf8_encode($RFP['aca_fer_par']);
        $ckb_aca_fer_exc = utf8_encode($RFP['aca_fer_exc']);
        $ckb_aca_pla = utf8_encode($RFP['aca_pla']);
        $ckb_aca_ser = utf8_encode($RFP['aca_ser']);
        $ckb_aca_alt_val_mens = utf8_encode($RFP['aca_alt_val_mens']);        
        $ckb_aca_pro_rata_mens = utf8_encode($RFP['aca_pro_rata_mens']);
        $ckb_aca_documento = utf8_encode($RFP['aca_documento']);        
        $ckb_aca_est_mens = utf8_encode($RFP['aca_est_mens']);
        $ckb_aca_exc_mens = utf8_encode($RFP['aca_exc_mens']);
        $ckb_aca_exc_mret = utf8_encode($RFP['aca_exc_mret']);
        $ckb_aca_inf_gerenciais = utf8_encode($RFP['aca_inf_gerenciais']);
        $ckb_aca_add_convenio = utf8_encode($RFP['aca_add_convenio']);
        $ckb_aca_add_credencial = utf8_encode($RFP['aca_add_credencial']);
        $ckb_aca_dtf_credencial = utf8_encode($RFP['aca_dtf_credencial']);
        $ckb_aca_add_bloqueios = utf8_encode($RFP['aca_add_bloqueio']);
        $ckb_aca_cancelar_dcc = utf8_encode($RFP['aca_cancelar_dcc']);
        $ckb_aca_plan_comp = utf8_encode($RFP['aca_plan_comp']);
        $ckb_aca_pg_retro = utf8_encode($RFP['aca_pgr']);
        $ckb_aca_rel_acesso = utf8_encode($RFP['aca_rel_acesso']);
        $ckb_aca_turmas = utf8_encode($RFP['aca_turmas']);
        $ckb_aca_estudio = utf8_encode($RFP['aca_estudio']);
        $ckb_aca_documentos_ = utf8_encode($RFP['aca_documentos']);
        $ckb_adm_emlobg = utf8_encode($RFP['adm_email_obrig']);
        $ckb_adm_telobg = utf8_encode($RFP['adm_tel_obrig']);
        $ckb_adm_celobg = utf8_encode($RFP['adm_cel_obrig']);
        $ckb_adm_endobg = utf8_encode($RFP['adm_end_obrig']);
        $ckb_adm_comissao = utf8_encode($RFP['adm_comissao']);
        $ckb_clb_titobr = utf8_encode($RFP['clb_tit_obrig']);        
        $ckb_adm_procobg = utf8_encode($RFP['adm_proc_obrig']);        
        $ckb_adm_sexoobg = utf8_encode($RFP['adm_sexo_obrig']);        
        $ckb_aca_abomens = utf8_encode($RFP['aca_abonar_mens']);        
        $ckb_aca_abocre = utf8_encode($RFP['aca_abonar_cre']);
        $ckb_aca_abodeb = utf8_encode($RFP['aca_abonar_deb']);   
        $ckb_aca_pg_prod = utf8_encode($RFP['aca_pg_prod']);
        $ckb_aca_can_item = utf8_encode($RFP['aca_can_item']);
        $ckb_aca_data_inicio = utf8_encode($RFP['aca_data_inicio']);
        $ckb_aca_agenda_regras = utf8_encode($RFP['aca_agenda_regras']);
        $ckb_aca_perm_desc = utf8_encode($RFP['aca_perm_desc']);
        $ckb_aca_one_item = utf8_encode($RFP['aca_one_item']);
        $ckb_aca_igCaixa = utf8_encode($RFP['aca_infoger_caixa']);
        $ckb_aca_igStatus = utf8_encode($RFP['aca_infoger_stats']);
        $ckb_aca_Contrato = utf8_encode($RFP['aca_infoger_contr']);
        $ckb_aca_filAvn = utf8_encode($RFP['aca_infoger_filav']);
        $ckb_aca_renovacoes = utf8_encode($RFP['aca_infoger_renov']);
        $ckb_aca_dcc = utf8_encode($RFP['aca_infoger_dcc']);
        $ckb_aca_bol = utf8_encode($RFP['aca_infoger_bol']);
        $ckb_aca_pix = utf8_encode($RFP['aca_infoger_pix']);
        $ckb_aca_acesso = utf8_encode($RFP['aca_infoger_acess']);
        $ckb_aca_rem_plano = utf8_encode($RFP['aca_rem_plano']);
        $ckb_aca_can_plano = utf8_encode($RFP['aca_can_plano']);
        $ckb_out_incserasa = utf8_encode($RFP['out_incluir_serasa']);
        $ckb_out_remserasa = utf8_encode($RFP['out_remover_serasa']);
        $ckb_aca_body = utf8_encode($RFP['aca_body']);
        $ckb_aca_body_trei = utf8_encode($RFP['aca_body_treino']);
        $ckb_aca_body_post = utf8_encode($RFP['aca_body_post']);
        $ckb_aca_body_estag = utf8_encode($RFP['aca_body_estag']);
        $ckb_aca_usu_resp = utf8_encode($RFP['aca_usu_resp']);
        $ckb_adm_cpf_obrig = utf8_encode($RFP['adm_cpf_obrig']);        
        $txt_adm_cpf_limite = utf8_encode($RFP['adm_cpf_limite']);         
        $ckb_pro_desc_plano = utf8_encode($RFP['fin_desc_plano']);
        $txt_pro_desc_plano = utf8_encode($RFP['fin_desc_plano_tot']);
        $ckb_pro_desc_servico = utf8_encode($RFP['fin_desc_serv']);         
        $txt_pro_desc_servico = utf8_encode($RFP['fin_desc_serv_tot']);
        $ckb_pro_rastreadores = utf8_encode($RFP['ckb_pro_rastreadores']);
        $ckb_pro_terceiros = utf8_encode($RFP['ckb_pro_terceiros']);
        $ckb_pro_operadoras = utf8_encode($RFP['ckb_pro_operadoras']);
        $ckb_aca_cli_block = utf8_encode($RFP['aca_cli_block']);
        $ckb_aca_cntt_dtCanc = utf8_encode($RFP['aca_cntt_dtCanc']);
        $ckb_fin_cancel_multa = utf8_encode($RFP['fin_cancel_multa']);
        $ckb_crm_upt_rem = utf8_encode($RFP['crm_upt_rem']);
        $ckb_crm_upt_rem_new = utf8_encode($RFP['crm_upt_rem_new']);
        $ckb_crm_pro_ure = utf8_encode($RFP['crm_pro_ure']);
        $ckb_crm_cli_ure = utf8_encode($RFP['crm_cli_ure']);
        $ckb_use_dist_pro = utf8_encode($RFP['use_dist_pro']);
        $ckb_crm_chatbot = utf8_encode($RFP['crm_chatbot']);
        $ckb_crm_lea_ure = utf8_encode($RFP['crm_lea_ure']);
        $ckb_crm_lead = utf8_encode($RFP['crm_lead']);
        $ckb_crm_lead_camp = utf8_encode($RFP['crm_lead_camp']);        
        $ckb_fin_cx_ticket = utf8_encode($RFP['fin_cx_ticket']);
        $ckb_fin_cx_mat = utf8_encode($RFP['fin_cx_mat']);
        $ckb_fin_cx_mod = utf8_encode($RFP['fin_cx_mod']);        
        $ckb_fin_lpg = utf8_encode($RFP['fin_lpg']);        
        $ckb_com_forma = utf8_encode($RFP['com_forma']);
        $ckb_pro_sinistro = utf8_encode($RFP['ckb_pro_sinistro']);
        $ckb_pro_cotas = utf8_encode($RFP['ckb_pro_cotas']);
        $ckb_pro_fipe = utf8_encode($RFP['ckb_pro_fipe']);
        $ckb_cpagar_read = utf8_encode($RFP['ckb_cpagar_read']);
        $ckb_creceber_read = utf8_encode($RFP['ckb_creceber_read']);
        $ckb_fcaixa_read = utf8_encode($RFP['ckb_fcaixa_read']);
        $ckb_clb_loc = utf8_encode($RFP['clb_locacoes']);
        $ckb_clb_eventos = utf8_encode($RFP['clb_eventos']);
        $ckb_clb_categoria = utf8_encode($RFP['clb_categoria']);
        $ckb_clb_tipo = utf8_encode($RFP['clb_tipo']);        
        $ckb_clb_layout_remessa = utf8_encode($RFP['clb_layout_remessa']);
        $ckb_clb_layout_retorno = utf8_encode($RFP['clb_layout_retorno']);
        $ckb_clb_excecao_vencimento = utf8_encode($RFP['clb_excecao_vencimento']);
        $ckb_clb_gestao_documentos = utf8_encode($RFP['clb_gestao_documentos']);
        $ckb_clb_gestao_folha = utf8_encode($RFP['clb_gestao_folha']);
        $ckb_clb_excluir_convites = utf8_encode($RFP['clb_excluir_convites']);
        $ckb_clb_baixar_layout = utf8_encode($RFP['clb_baixar_layout']);    
        $ckb_clb_gboleto = utf8_encode($RFP['clb_gboleto']);
        $ckb_clb_cboleto = utf8_encode($RFP['clb_cboleto']);
        $ckb_exc_cpagar = utf8_encode($RFP['exc_cpagar']);
        $ckb_exc_creceber = utf8_encode($RFP['exc_creceber']);
        $ckb_clb_gconvite = utf8_encode($RFP['clb_gconvite']);
        $ckb_clb_rel_eventos = utf8_encode($RFP['clb_rel_eventos']);
        $ckb_clb_rel_socios = utf8_encode($RFP['clb_rel_socios']);
        $ckb_clb_trancar_tit = utf8_encode($RFP['clb_trancamento']);
        $ckb_aca_cntt_dtCanc_limit = utf8_encode($RFP['aca_cntt_dtCanc_limit']);                
        $ckb_pro_grp_acess = utf8_encode($RFP['ckb_pro_grp_acess']);
        $ckb_pro_classificacao = utf8_encode($RFP['ckb_pro_classificacao']);
        $ckb_pro_montadoras = utf8_encode($RFP['ckb_pro_montadoras']);
        $ckb_pro_modelos = utf8_encode($RFP['ckb_pro_modelos']);
        $ckb_pro_serv_mod = utf8_encode($RFP['ckb_pro_serv_mod']);
        $ckb_pro_acess = utf8_encode($RFP['ckb_pro_acess']);
        $ckb_pro_tipo = utf8_encode($RFP['ckb_pro_tipo']);
        $ckb_pro_item_vis = utf8_encode($RFP['ckb_pro_item_vis']);
        $ckb_pro_veiculo = utf8_encode($RFP['ckb_pro_veiculo']);
        $ckb_pro_vistoria = utf8_encode($RFP['ckb_pro_vistoria']);
        $ckb_pro_grp_cidade = utf8_encode($RFP['ckb_pro_grp_cidade']);
        $ckb_pro_grp_fabricantes = utf8_encode($RFP['ckb_pro_grp_fabricantes']);
        $ckb_pro_apv_veic = utf8_encode($RFP['ckb_pro_apv_veic']);
        $ckb_pro_apv_sin = utf8_encode($RFP['ckb_pro_apv_sin']);
        $ckb_pro_rastreador = utf8_encode($RFP['ckb_pro_rastreador']);
        $ckb_pro_oficina = utf8_encode($RFP['ckb_pro_oficina']);
        $ckb_pro_exc_vei = utf8_encode($RFP['ckb_pro_exc_vei']);       
        $ckb_pro_cad_veic = utf8_encode($RFP['ckb_pro_cad_veic']);       
        $ckb_bi_auditoria = utf8_encode($RFP['bi_auditoria']);
        $ckb_bi_mat_ren = utf8_encode($RFP['bi_mat_ren']);
        $ckb_bi_gve_sfin = utf8_encode($RFP['bi_gve_sfin']);
        $ckb_bi_retencao = utf8_encode($RFP['bi_retencao']);
        $ckb_bi_evasao = utf8_encode($RFP['bi_evasao']);
        $ckb_bi_inadimplencia = utf8_encode($RFP['bi_inadimplencia']);
        $ckb_bi_prev_fin = utf8_encode($RFP['bi_prev_fin']);
        $ckb_bi_cen_custo = utf8_encode($RFP['bi_cen_custo']);
        $ckb_bi_fluxo_caixa = utf8_encode($RFP['bi_fluxo_caixa']);
        $ckb_bi_faturamento = utf8_encode($RFP['bi_faturamento']);
        $ckb_bi_com_faturamento = utf8_encode($RFP['bi_com_faturamento']);
        $ckb_bi_leads = utf8_encode($RFP['bi_leads']);
        $ckb_bi_formas_pagamento = utf8_encode($RFP['bi_formas_pagamento']);
        $ckb_bi_gestao_vendas = utf8_encode($RFP['bi_gestao_vendas']);
        $ckb_bi_gestao_mapa = utf8_encode($RFP['bi_gestao_mapa']);
        $ckb_bi_produtividade = utf8_encode($RFP['bi_produtividade']);
        $ckb_bi_com_produtividade = utf8_encode($RFP['bi_com_produtividade']);
        $ckb_bi_com_comissionado = utf8_encode($RFP['bi_com_comissionado']);
        $ckb_bi_atendimentos = utf8_encode($RFP['bi_atendimentos']);
        $ckb_bi_prospects = utf8_encode($RFP['bi_prospects']);
        $ckb_bi_agendamentos = utf8_encode($RFP['bi_agendamentos']);  
        $ckb_bi_turmas = utf8_encode($RFP['bi_turmas']);  
        $ckb_bi_rel_acessos = utf8_encode($RFP['bi_rel_acessos']);  
    }
} else {
    $disabled = '';
    $id = '';
    $nomeperm = '';
    $ckb_adm_cli = '';
    $ckb_adm_for = '';
    $ckb_adm_fun = '';
    $ckb_adm_usu = '';
    $ckb_adm_per = '';
    $ckb_adm_gru = '';
    $ckb_adm_dep = '';
    $ckb_adm_config = '';
    $ckb_adm_disparos = '';
    $ckb_adm_agenda = '';
    $ckb_adm_contratos = '';
    $ckb_adm_atendimentos = '';
    $ckb_adm_parentesco = '';
    $ckb_fin_doc = '';
    $ckb_fin_ban = '';
    $ckb_fin_con = '';
    $ckb_fin_mov = '';
    $ckb_fin_fix = '';
    $ckb_fin_afix = '';
    $ckb_fin_flu = '';
    $ckb_fin_lsol = '';
    $ckb_fin_asol = '';
    $ckb_fin_pag = '';
    $ckb_fin_rec = '';
    $ckb_est_mov = '';
    $ckb_est_ccu = '';
    $ckb_est_pro = '';
    $ckb_est_ser = '';
    $ckb_ser_spc = '';
    $ckb_est_loc = '';
    $ckb_est_lven = '';
    $ckb_est_lven_read = '';
    $ckb_est_aven = '';
    $ckb_est_lorc = '';
    $ckb_est_aorc = '';
    $ckb_fin_lped = '';
    $ckb_fin_lcot = '';
    $ckb_fin_aped = '';
    $ckb_est_lcom = '';
    $ckb_est_acom = '';
    $ckb_est_flu = '';
    $ckb_est_com = '';
    $ckb_est_cor = '';
    $ckb_est_tam = '';
    $ckb_est_fab = '';
    $ckb_est_pla = '';
    $ckb_est_jus = '';
    $ckb_est_ent_sai = '';
    $ckb_est_aent_sai = '';  
    $ckb_nfe_cen = '';
    $ckb_nfe_tra = '';
    $ckb_nfe_msg = '';
    $ckb_nfe_emi = '';
    $ckb_nfe_est = '';
    $ckb_nfe_cid = '';
    $ckb_nfe_enq = '';
    $ckb_nfe_not = '';
    $ckb_nfe_ess = '';
    $ckb_crm_cli = '';
    $ckb_crm_fch = '';
    $ckb_crm_pro = '';
    $ckb_crm_tel = '';
    $ckb_crm_esp = '';
    $ckb_crm_ind = '';
    $ckb_crm_rec = '';
    $ckb_out_ges = '';
    $ckb_out_aju = '';
    $ckb_out_term = '';
    $ckb_out_terc = '';
    $ckb_out_rban = '';
    $ckb_out_rexc = '';
    $ckb_out_ecob = '';
    $ckb_out_todos_operadores = '';
    $ckb_out_desconto = '';
    $ckb_out_pag_avancada = '1';
    $txt_out_pag_avancada = '1';   
    $ckb_ser_cli = '';
    $ckb_ser_ser = '';
    $ckb_fin_dmp = '';
    $ckb_crm_cpc = '';
    $ckb_crm_exc_atend = '';
    $ckb_crm_fec_atend = '';
    $ckb_adm_cli_read = '';
    $ckb_adm_cli_read_obs = '';
    $ckb_fin_rel_gct = '';
    $ckb_fin_rel_tdd = '';
    $ckb_fin_chq = '';
    $ckb_fin_rcrt = '';
    $ckb_fin_crt = '';
    $ckb_fin_bol = '';
    $ckb_fin_rel_mov_caixa = '';
    $ckb_siv_cadAcad = '';
    $ckb_siv_cadErp = '';
    $ckb_siv_GerSenha = '';
    $ckb_siv_UserSite = '';
    $ckb_siv_DCC = '';
    $ckb_siv_CreGB = '';
    $ckb_siv_PenGB = '';
    $ckb_siv_LogERP = '';
    $ckb_aca_hor = '';
    $ckb_aca_conv = '';
    $ckb_aca_cred = '';
    $ckb_aca_catr = '';
    $ckb_aca_leif = '';
    $ckb_aca_amb = '';
    $ckb_aca_fer = '';
    $ckb_aca_fer_par = '';
    $ckb_aca_fer_exc = '';
    $ckb_aca_pla = '';
    $ckb_aca_ser = '';
    $ckb_aca_alt_val_mens = '';
    $ckb_aca_pro_rata_mens = '';
    $ckb_aca_documento = '';    
    $ckb_aca_est_mens = '';
    $ckb_aca_exc_mens = '';
    $ckb_aca_exc_mret = '';
    $ckb_aca_inf_gerenciais = '';
    $ckb_aca_add_convenio = '';
    $ckb_aca_add_credencial = '';
    $ckb_aca_dtf_credencial = '';
    $ckb_aca_add_bloqueios = '';
    $ckb_aca_cancelar_dcc = '';
    $ckb_aca_plan_comp = '';
    $ckb_aca_pg_retro = '';
    $ckb_aca_rel_acesso = '';
    $ckb_aca_turmas = '';
    $ckb_aca_estudio = '';
    $ckb_aca_documentos_ = '';
    $ckb_adm_emlobg = '';
    $ckb_adm_telobg = '';
    $ckb_adm_celobg = '';
    $ckb_adm_endobg = '';
    $ckb_adm_comissao = '';
    $ckb_clb_titobr = '';
    $ckb_adm_procobg = '';
    $ckb_adm_sexoobg = '';
    $ckb_aca_abomens = '';    
    $ckb_aca_abocre = '';
    $ckb_aca_abodeb = '';  
    $ckb_aca_pg_prod = '';
    $ckb_aca_can_item = '';
    $ckb_aca_data_inicio = '';
    $ckb_aca_agenda_regras = '';
    $ckb_aca_perm_desc = '';
    $ckb_aca_one_item = '';
    $ckb_aca_igCaixa = '';
    $ckb_aca_igStatus = '';
    $ckb_aca_Contrato = '';
    $ckb_aca_filAvn = '';
    $ckb_aca_renovacoes = '';
    $ckb_aca_dcc = '';
    $ckb_aca_bol = '';
    $ckb_aca_pix = '';
    $ckb_aca_acesso = '';
    $ckb_aca_rem_plano = '';
    $ckb_aca_can_plano = '';
    $ckb_out_incserasa = '';
    $ckb_out_remserasa = '';
    $ckb_aca_usu_resp = '';
    $ckb_adm_cpf_obrig = '';        
    $txt_adm_cpf_limite = '';    
    $ckb_pro_desc_plano = '';
    $txt_pro_desc_plano = '';
    $ckb_pro_desc_servico = '';
    $txt_pro_desc_servico = '';
    $ckb_pro_rastreadores = '';
    $ckb_pro_terceiros = '';
    $ckb_pro_operadoras = '';
    $ckb_aca_body = '';
    $ckb_aca_cli_block = '';
    $ckb_crm_upt_rem = '';
    $ckb_crm_upt_rem_new = '';
    $ckb_crm_pro_ure = '';
    $ckb_crm_cli_ure = '';
    $ckb_use_dist_pro = '';
    $ckb_crm_chatbot = '';
    $ckb_crm_lea_ure = '';
    $ckb_crm_lead = '';
    $ckb_crm_lead_camp = '';        
    $ckb_aca_cntt_dtCanc = '';
    $ckb_fin_cancel_multa = '';
    $ckb_fin_cx_ticket = '';
    $ckb_fin_cx_mat = '';
    $ckb_fin_cx_mod = '';    
    $ckb_fin_lpg = ''; 
    $ckb_com_forma = '';   
    $ckb_pro_sinistro = '';
    $ckb_pro_cotas = '';
    $ckb_pro_fipe = '';
    $ckb_cpagar_read = '';
    $ckb_creceber_read = '';
    $ckb_fcaixa_read = '';     
    $ckb_clb_loc = '';
    $ckb_clb_eventos = '';
    $ckb_clb_categoria = '';
    $ckb_clb_tipo = '';
    $ckb_clb_layout_remessa = '';
    $ckb_clb_layout_retorno = '';
    $ckb_clb_excecao_vencimento = '';
    $ckb_clb_gestao_documentos = '';
    $ckb_clb_gestao_folha = '';    
    $ckb_clb_excluir_convites = '';
    $ckb_clb_baixar_layout = '';
    $ckb_clb_gboleto = '';
    $ckb_clb_cboleto = '';
    $ckb_exc_cpagar = '';
    $ckb_exc_creceber = '';
    $ckb_clb_gconvite = '';
    $ckb_clb_rel_eventos = '';
    $ckb_clb_rel_socios = '';
    $ckb_clb_trancar_tit = '';        
    $ckb_pro_grp_acess = '';    
    $ckb_pro_classificacao = '';
    $ckb_pro_montadoras = '';
    $ckb_pro_modelos = '';
    $ckb_pro_serv_mod = '';    
    $ckb_pro_acess = '';
    $ckb_pro_tipo = '';
    $ckb_pro_item_vis = '';
    $ckb_pro_veiculo = '';
    $ckb_pro_vistoria = '';
    $ckb_pro_grp_cidade = '';
    $ckb_pro_grp_fabricantes = '';
    $ckb_pro_apv_veic = '';
    $ckb_pro_apv_sin = '';
    $ckb_pro_rastreador = '';
    $ckb_pro_oficina = '';
    $ckb_pro_exc_vei = '';    
    $ckb_pro_cad_veic = '';    
    $ckb_bi_auditoria = '';
    $ckb_bi_mat_ren = '';
    $ckb_bi_gve_sfin = '';
    $ckb_bi_retencao = '';
    $ckb_bi_evasao = '';
    $ckb_bi_inadimplencia = '';
    $ckb_bi_prev_fin = '';
    $ckb_bi_cen_custo = '';
    $ckb_bi_fluxo_caixa = '';
    $ckb_bi_faturamento = '';
    $ckb_bi_com_faturamento = '';
    $ckb_bi_leads = '';
    $ckb_bi_formas_pagamento = '';
    $ckb_bi_gestao_vendas = '';
    $ckb_bi_gestao_mapa = '';
    $ckb_bi_produtividade = '';
    $ckb_bi_com_produtividade = '';
    $ckb_bi_com_comissionado = '';
    $ckb_bi_atendimentos = '';
    $ckb_bi_prospects = '';
    $ckb_bi_agendamentos = '';
    $ckb_bi_turmas = '';
    $ckb_bi_rel_acessos = '';
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<link href="../../../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="../../../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body onLoad="document.frmEnviaDados.txtNomePerm.focus()">
    <div class="frmhead">
        <div class="frmtext">Permissões</div>
        <div class="frmicon" onClick="parent.FecharBox()">
            <span class="ico-remove"></span>
        </div>
    </div>
    <div class="tabbable frmtabs">
        <form action="FormPermissoes.php" name="frmEnviaDados" method="POST">
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <table width="680" border="0" cellpadding="3" class="textosComuns">
                <tr>
                    <td width="122" align="right">Nome da Permissão:</td>
                    <td><input name="txtNomePerm" id="txtNomePerm" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $nomeperm; ?>"/></td>
                </tr>
            </table>
            <div class="head">
                <a href="#" onClick="source('tabs');return false;"><div class="icon"><span class="ico-info" style="color:#fff; margin:0; padding:0"></span></div></a>
            </div>
            <div class="data-fluid tabbable" style="height: 490px;">
                <ul class="nav nav-tabs">
                    <li class="active" style="margin-left:10px;" ><a href="#tab1" data-toggle="tab">Administrativo</a></li>
                    <li><a href="#tab12" data-toggle="tab">BI</a></li>                    
                    <?php if ($mdl_aca_ > 0) { ?>
                    <li><a href="#tab2" data-toggle="tab"><?php echo ($mdl_clb_ == 1 || $mdl_seg_ == 1 ? "Configuração" : "Academia")?></a></li>                    
                    <?php } ?>                                            
                    <li><a href="#tab3" data-toggle="tab">Financeiro</a></li>
                    <li><a href="#tab4" data-toggle="tab">Estoque</a></li>
                    <li><a href="#tab5" data-toggle="tab">NFE</a></li>
                    <li><a href="#tab6" data-toggle="tab">CRM</a></li>
                    <?php if ($mdl_ser_ > 0) { ?>
                    <li><a href="#tab7" data-toggle="tab">Serviços</a></li>
                    <?php } if ($contrato == "003") { ?>
                    <li><a href="#tab8" data-toggle="tab">Sivis</a></li>
                    <?php } if ($mdl_clb_ > 0) { ?>                        
                    <li><a href="#tab9" data-toggle="tab">Clube</a></li>
                    <?php } ?>                    
                    <li><a href="#tab10" data-toggle="tab">Outros</a></li>
                    <?php if ($mdl_seg_ > 0) { ?>                    
                    <li><a href="#tab11" data-toggle="tab">Proteção</a></li>
                    <?php } ?>                    
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                        <table width="680" style="margin-bottom:270px" border="0" cellpadding="3" class="textosComuns">
                            <tr>
                                <td width="50" height="33"><div style="float:right"><input name="ckb_adm_all" id="ckb_adm_all" <?php echo $disabled; ?> type="checkbox" class="input-medium"/></div></td>
                                <td colspan="4">Inverter Seleção</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_adm_cli" id="ckb_adm_cli" <?php
                                        echo $disabled;
                                        if ($ckb_adm_cli == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Clientes</td>
                                <td height="33"><div style="float:right"><input name="ckb_adm_for" id="ckb_adm_for" <?php
                                        echo $disabled;
                                        if ($ckb_adm_for == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Fornecedores</td>
                                <td height="33"><div style="float:right">
                                        <input name="ckb_adm_cli_read_obs" id="ckb_adm_cli_read_obs" <?php
                                        echo $disabled;
                                        if ($ckb_adm_cli_read_obs == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Somente Leitura Observações de Clientes</td>                                
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_adm_usu" id="ckb_adm_usu" <?php
                                        echo $disabled;
                                        if ($ckb_adm_usu == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Usuários</td>
                                <td height="33"><div style="float:right"><input name="ckb_adm_per" id="ckb_adm_per" <?php
                                        echo $disabled;
                                        if ($ckb_adm_per == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Permissões</td>
                                <td height="33"><div style="float:right">
                                        <input name="ckb_adm_cli_read" id="ckb_adm_cli_read" <?php
                                        echo $disabled;
                                        if ($ckb_adm_cli_read == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Somente Leitura Clientes</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_fin_doc" id="ckb_fin_doc" <?php
                                        echo $disabled;
                                        if ($ckb_fin_doc == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Tipos de Documentos</td>
                                <td height="33"><div style="float:right"><input name="ckb_fin_ban" id="ckb_fin_ban" <?php
                                        echo $disabled;
                                        if ($ckb_fin_ban == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Bancos</td>
                                <td height="33"><div style="float:right"><input name="ckb_adm_emlobg" id="ckb_adm_emlobg" <?php
                                        echo $disabled;
                                        if ($ckb_adm_emlobg == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>E-mail Obrigatório</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_fin_mov" id="ckb_fin_mov" <?php
                                        echo $disabled;
                                        if ($ckb_fin_mov == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Subgrupos de Contas</td>
                                <td height="33"><div style="float:right">
                                        <input name="ckb_adm_dep" id="ckb_adm_dep" <?php
                                        echo $disabled;
                                        if ($ckb_adm_dep == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Departamentos</td>
                                <td height="33"><div style="float:right"><input name="ckb_adm_telobg" id="ckb_adm_telobg" <?php
                                        echo $disabled;
                                        if ($ckb_adm_telobg == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Telefone Obrigatório</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_crm_cli" id="ckb_crm_cli" <?php
                                        echo $disabled;
                                        if ($ckb_crm_cli == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Procedência de Clientes</td>                                
                                <td height="33"><div style="float:right"><input name="ckb_crm_esp" id="ckb_crm_esp" <?php
                                        echo $disabled;
                                        if ($ckb_crm_esp == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Especificações</td>
                                <td height="33"><div style="float:right"><input name="ckb_adm_celobg" id="ckb_adm_celobg" <?php
                                        echo $disabled;
                                        if ($ckb_adm_celobg == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Celular Obrigatório</td>                                                               
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_adm_config" id="ckb_adm_config" <?php
                                        echo $disabled;
                                        if ($ckb_adm_config == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Configuração de Parâmetro do Sistema</td>
                                <td height="33"><div style="float:right"><input name="ckb_crm_ind" id="ckb_crm_ind" <?php
                                        echo $disabled;
                                        if ($ckb_crm_ind == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Indicadores</td>                                
                                <td height="33"><div style="float:right"><input name="ckb_adm_endobg" id="ckb_adm_endobg" <?php
                                        echo $disabled;
                                        if ($ckb_adm_endobg == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Endereço Obrigatório</td> 
                            </tr>
                            <tr>                                
                                <td height="33"><div style="float:right"><input name="ckb_crm_fch" id="ckb_crm_fch" <?php
                                        echo $disabled;
                                        if ($ckb_crm_fch == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Modelos de Fechamento de Chamados</td>                                
                                <td height="33"><div style="float:right"><input name="ckb_fin_con" id="ckb_fin_con" <?php
                                        echo $disabled;
                                        if ($ckb_fin_con == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Grupos de Contas</td>
                                <td height="33"><div style="float:right"><input name="ckb_adm_comissao" id="ckb_adm_comissao" <?php
                                        echo $disabled;
                                        if ($ckb_adm_comissao == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Comissão Obrigatória</td>                                
                            </tr>
                            <tr>                                
                                <td height="33"><div style="float:right"><input name="ckb_adm_fun" id="ckb_adm_fun" <?php
                                        echo $disabled;
                                        if ($ckb_adm_fun == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Funcionários</td> 
                                <td height="33"><div style="float:right"><input name="ckb_adm_parentesco" id="ckb_adm_parentesco" <?php
                                        echo $disabled;
                                        if ($ckb_adm_parentesco == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Parentesco</td>
                                <td height="33"><div style="float:right"><input name="ckb_adm_cpf_obrig" id="ckb_adm_cpf_obrig" <?php
                                        echo $disabled;
                                        if ($ckb_adm_cpf_obrig == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>CPF Obrig. a partir de
                                    <input name="txt_adm_cpf_limite" id="txt_adm_cpf_limite" style="width: 28px;" type="text" value="<?php echo $txt_adm_cpf_limite;?>" <?php echo $disabled;?>/> anos.
                                </td>                                
                            </tr>
                            <?php if($mdl_cnt_ == 1) { ?>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_adm_contratos" id="ckb_adm_contratos" <?php
                                        echo $disabled;
                                        if ($ckb_adm_contratos == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Contratos</td>
                                <td height="33"><div style="float:right"><input name="ckb_adm_atendimentos" id="ckb_adm_atendimentos" <?php
                                        echo $disabled;
                                        if ($ckb_adm_atendimentos == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Lançamento de Atendimentos</td>
                                <td height="33"><div style="float:right"><input name="ckb_clb_titobr" id="ckb_clb_titobr" <?php
                                        echo $disabled;
                                        if ($ckb_clb_titobr == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Titularidade Obrigatória</td>                                
                            </tr>                                
                            <tr>                                
                                <td height="33"><div style="float:right"><input name="ckb_adm_agenda" id="ckb_adm_agenda" <?php
                                        echo $disabled;
                                        if ($ckb_adm_agenda == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Agendas</td>
                                <td height="33"><div style="float:right"><input name="ckb_adm_gru" id="ckb_adm_gru" <?php
                                        echo $disabled;
                                        if ($ckb_adm_gru == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Grupos</td> 
                                <td height="33"><div style="float:right"><input name="ckb_adm_procobg" id="ckb_adm_procobg" <?php
                                        echo $disabled;
                                        if ($ckb_adm_procobg == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Procedência Obrigatória</td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_est_ccu" id="ckb_est_ccu" <?php
                                        echo $disabled;
                                        if ($ckb_est_ccu == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Centro de Custos</td>                                
                                <td height="33"><div style="float:right"><input name="ckb_aca_add_convenio" id="ckb_aca_add_convenio" <?php
                                        echo $disabled;
                                        if ($ckb_aca_add_convenio == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Adicionar Convenio</td>
                                <td height="33"><div style="float:right"><input name="ckb_adm_sexoobg" id="ckb_adm_sexoobg" <?php
                                        echo $disabled;
                                        if ($ckb_adm_sexoobg == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Sexo Obrigatório</td>                               
                            </tr>
                        </table>
                    </div>
                    <div class="tab-pane" id="tab2">
                      <ul class="nav nav-tabs">
                          <li class="active" style="margin-left:10px;" ><a href="#subtabAcad1" data-toggle="tab">Geral</a></li>
                          <li><a href="#subtabAcad2" data-toggle="tab">Clientes</a></li>
                          <li><a href="#subtabAcad3" data-toggle="tab">Informações Gerenciais</a></li>
                          <?php if($mdl_bod_ == 1) {?>
                          <li><a href="#subtabAcad4" data-toggle="tab">Body Web</a></li>
                          <?php } ?>
                      </ul>
                      <div class="tab-content">
                        <div class="tab-pane active" id="subtabAcad1">
                          <table width="680" style="margin-bottom:270px;" border="0" cellpadding="3" class="textosComuns">
                              <tr>
                                  <td width="50" height="33"><div style="float:right"><input name="ckb_aca_all_gel" id="ckb_aca_all_gel" <?php echo $disabled; ?> type="checkbox" class="input-medium"/></div></td>
                                  <td>Inverter Seleção</td>
                              </tr>
                              <tr>
                                  <td height="33"><div style="float:right"><input name="ckb_aca_hor" id="ckb_aca_hor" <?php
                                          echo $disabled;
                                          if ($ckb_aca_hor == "1") {
                                              echo "CHECKED";
                                          }
                                          ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                  <td>Horários</td>
                                  <td height="33"><div style="float:right"><input name="ckb_aca_cred" id="ckb_aca_cred" <?php
                                          echo $disabled;
                                          if ($ckb_aca_cred == "1") {
                                              echo "CHECKED";
                                          }
                                          ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                  <td>Credenciais</td>
                                  <td height="33"><div style="float:right"><input name="ckb_aca_pla" id="ckb_aca_pla" <?php
                                          echo $disabled;
                                          if ($ckb_aca_pla == "1") {
                                              echo "CHECKED";
                                          }
                                          ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                  <td>Planos</td>
                              </tr>
                              <tr>
                                  <td height="33"><div style="float:right"><input name="ckb_aca_catr" id="ckb_aca_catr" <?php
                                          echo $disabled;
                                          if ($ckb_aca_catr == "1") {
                                              echo "CHECKED";
                                          }
                                          ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                  <td>Catracas</td>
                                  <td height="33"><div style="float:right"><input name="ckb_aca_leif" id="ckb_aca_leif" <?php
                                          echo $disabled;
                                          if ($ckb_aca_leif == "1") {
                                              echo "CHECKED";
                                          }
                                          ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                  <td>Leitores Faciais</td>
                                  <td height="33"><div style="float:right"><input name="ckb_aca_amb" id="ckb_aca_amb" <?php
                                          echo $disabled;
                                          if ($ckb_aca_amb == "1") {
                                              echo "CHECKED";
                                          }
                                          ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                  <td>Ambientes</td>
                              </tr>
                              <tr>
                                  <td height="33"><div style="float:right"><input name="ckb_aca_conv" id="ckb_aca_conv" <?php
                                          echo $disabled;
                                          if ($ckb_aca_conv == "1") {
                                              echo "CHECKED";
                                          }
                                          ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                  <td>Convênios</td>
                                  <td height="33"><div style="float:right"><input name="ckb_aca_turmas" id="ckb_aca_turmas" <?php
                                          echo $disabled;
                                          if ($ckb_aca_turmas == "1") {
                                              echo "CHECKED";
                                          }
                                          ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                  <td>Turmas</td>
                                  <td height="33"><div style="float:right"><input name="ckb_aca_documentos" id="ckb_aca_documentos" <?php
                                          echo $disabled;
                                          if ($ckb_aca_documentos_ == "1") {
                                              echo "CHECKED";
                                          }
                                          ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                  <td>Documentos</td>
                              </tr>
                              <tr>
                                  <td height="33"><div style="float:right"><input name="ckb_aca_ser" id="ckb_aca_ser" <?php
                                          echo $disabled;
                                          if ($ckb_aca_ser == "1") {
                                              echo "CHECKED";
                                          }
                                          ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                  <td>Serviços</td>                                  
                                  <td height="33"><div style="float:right"><input name="ckb_aca_rel_acesso" id="ckb_aca_rel_acesso" <?php
                                          echo $disabled;
                                          if ($ckb_aca_rel_acesso == "1") {
                                              echo "CHECKED";
                                          }
                                          ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                  <td>Relatório de Acessos</td>                                
                              </tr>
                          </table>
                        </div>                        
                        <div class="tab-pane" id="subtabAcad2">
                          <table width="680" style="margin-bottom:270px;" border="0" cellpadding="3" class="textosComuns">
                              <tr>
                                  <td width="50" height="33"><div style="float:right"><input name="ckb_aca_all_cli" id="ckb_aca_all_cli" <?php echo $disabled; ?> type="checkbox" class="input-medium"/></div></td>
                                  <td>Inverter Seleção</td>
                              </tr>
                              <tr>
                                <td height="33"><div style="float:right"><input name="ckb_aca_abomens" id="ckb_aca_abomens" <?php
                                        echo $disabled;
                                        if ($ckb_aca_abomens == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Abonar Mensalidades</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_exc_mens" id="ckb_aca_exc_mens" <?php
                                        echo $disabled;
                                        if ($ckb_aca_exc_mens == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Excluir Mensalidade</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_rem_plano" id="ckb_aca_rem_plano" <?php
                                        echo $disabled;
                                        if ($ckb_aca_rem_plano == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Remover Plano</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_aca_est_mens" id="ckb_aca_est_mens" <?php
                                        echo $disabled;
                                        if ($ckb_aca_est_mens == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Estornar Mensalidade</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_add_credencial" id="ckb_aca_add_credencial" <?php
                                        echo $disabled;
                                        if ($ckb_aca_add_credencial == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Adicionar Credencial ao Aluno</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_dtf_credencial" id="ckb_aca_dtf_credencial" <?php
                                        echo $disabled;
                                        if ($ckb_aca_dtf_credencial == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Alterar Data Fim da Credencial</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_aca_pg_retro" id="ckb_aca_pg_retro" <?php
                                        echo $disabled;
                                        if ($ckb_aca_pg_retro == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Permitir Pag. Retroativo</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_alt_val_mens" id="ckb_aca_alt_val_mens" <?php
                                        echo $disabled;
                                        if ($ckb_aca_alt_val_mens == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Alterar Valores de Mensalidade</td>                                
                                <td height="33"><div style="float:right"><input name="ckb_aca_pro_rata_mens" id="ckb_aca_pro_rata_mens" <?php
                                        echo $disabled;
                                        if ($ckb_aca_pro_rata_mens == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Pro Rata de Mensalidade</td>                                                                
                            </tr>
                            <?php if ($_SESSION["mod_emp"] != 1) { ?>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_aca_fer_exc" id="ckb_aca_fer_exc" <?php
                                        echo $disabled;
                                        if ($ckb_aca_fer_exc == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Excluir Congelamento</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_add_convenio" id="ckb_aca_add_convenio" <?php
                                        echo $disabled;
                                        if ($ckb_aca_add_convenio == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Adicionar Convenio ao Aluno</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_exc_mret" id="ckb_aca_exc_mret" <?php
                                        echo $disabled;
                                        if ($ckb_aca_exc_mret == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Excluir Mensalidade Retroativa</td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_aca_fer_par" id="ckb_aca_fer_par" <?php
                                        echo $disabled;
                                        if ($ckb_aca_fer_par == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Parar Congelamento</td>                                
                                <td height="33"><div style="float:right"><input name="ckb_aca_usu_resp" id="ckb_aca_usu_resp" <?php
                                        echo $disabled;
                                        if ($ckb_aca_usu_resp == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Alterar Usuário Responsável</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_cancelar_dcc" id="ckb_aca_cancelar_dcc" <?php
                                        echo $disabled;
                                        if ($ckb_aca_cancelar_dcc == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cancelar DCC</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_aca_fer" id="ckb_aca_fer" <?php
                                        echo $disabled;
                                        if ($ckb_aca_fer == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Gerar Congelamento</td>                                
                                <td height="33"><div style="float:right"><input name="ckb_aca_add_bloqueios" id="ckb_aca_add_bloqueios" <?php
                                        echo $disabled;
                                        if ($ckb_aca_add_bloqueios == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Bloqueio Aluno Por Período</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_plan_comp" id="ckb_aca_plan_comp" <?php
                                        echo $disabled;
                                        if ($ckb_aca_plan_comp == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Plano Compartilhado</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_aca_cli_block" id="ckb_aca_cli_block" <?php
                                        echo $disabled;
                                        if ($ckb_aca_cli_block  == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Bloquear Aluno Indeterminado</td>                                
                                <td height="33"><div style="float:right"><input name="ckb_fin_cancel_multa" id="ckb_fin_cancel_multa" <?php
                                        echo $disabled;
                                        if ($ckb_fin_cancel_multa == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cancelar Multa</td>                                
                                <td height="33"><div style="float:right"><input name="ckb_aca_documento" id="ckb_aca_documento" <?php
                                        echo $disabled;
                                        if ($ckb_aca_documento == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Controle de Documentos</td>  
                            </tr>                                
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_aca_abocre" id="ckb_aca_abocre" <?php
                                        echo $disabled;
                                        if ($ckb_aca_abocre  == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Abonar Crédito</td>                                
                                <td height="33"><div style="float:right"><input name="ckb_aca_abodeb" id="ckb_aca_abodeb" <?php
                                        echo $disabled;
                                        if ($ckb_aca_abodeb == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Abonar Carteira</td>                                    
                                <td height="33"><div style="float:right"><input name="ckb_aca_pg_prod" id="ckb_aca_pg_prod" <?php
                                        echo $disabled;
                                        if ($ckb_aca_pg_prod == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Pagamento Produtos/Serviços</td>                                  
                            </tr>    
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_aca_can_item" id="ckb_aca_can_item" <?php
                                        echo $disabled;
                                        if ($ckb_aca_can_item == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cancelamento de Item de Pagamento</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_data_inicio" id="ckb_aca_data_inicio" <?php
                                        echo $disabled;
                                        if ($ckb_aca_data_inicio == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Data Início de Contrato Variável</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_agenda_regras" id="ckb_aca_agenda_regras" <?php
                                        echo $disabled;
                                        if ($ckb_aca_agenda_regras == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Agendamento sem Regras</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_aca_perm_desc" id="ckb_aca_perm_desc" <?php
                                        echo $disabled;
                                        if ($ckb_aca_perm_desc == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Permitir Desconto no Serviço</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_one_item" id="ckb_aca_one_item" <?php
                                        echo $disabled;
                                        if ($ckb_aca_one_item == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Pagar Apenas Uma Mensalidade Por Vez</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_can_plano" id="ckb_aca_can_plano" <?php
                                        echo $disabled;
                                        if ($ckb_aca_can_plano == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cancelar Plano</td>                                
                            </tr>
                            <?php if ($_SESSION["contrato"] == "018") { ?>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_aca_cntt_dtCanc" id="ckb_aca_cntt_dtCanc" <?php
                                        echo $disabled;
                                        if ($ckb_aca_cntt_dtCanc == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Bloquear data Recisão Plano - Solicitação</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_cntt_dtCanc_limit" id="ckb_aca_cntt_dtCanc_limit" <?php
                                        echo $disabled;
                                        if ($ckb_aca_cntt_dtCanc_limit == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Bloquear data Recisão Plano - Limite de Acesso </td>
                                <td height="33"></td>
                                <td></td>
                            </tr>                                
                            <?php } ?>
                          </table>
                        </div>
                        <div class="tab-pane" id="subtabAcad3">
                            <table width="680" style="margin-bottom:270px;" border="0" cellpadding="3" class="textosComuns">
                              <tr>
                                  <td width="50" height="33"><div style="float:right"><input name="ckb_aca_all_inf" id="ckb_aca_all_inf" <?php echo $disabled; ?> type="checkbox" class="input-medium"/></div></td>
                                  <td>Inverter Seleção</td>
                              </tr>
                              <tr>
                                <td height="33"><div style="float:right"><input name="ckb_aca_inf_gerenciais" id="ckb_aca_inf_gerenciais" <?php
                                        echo $disabled;
                                        if ($ckb_aca_inf_gerenciais == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Informações Gerenciais</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_acesso" id="ckb_aca_acesso" <?php
                                        echo $disabled;
                                        if ($ckb_aca_acesso == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Info.Ger. Acesso</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_renovacoes" id="ckb_aca_renovacoes" <?php
                                        echo $disabled;
                                        if ($ckb_aca_renovacoes == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Info.Ger. Renovações</td>
                              </tr>
                              <tr>
                                <td height="33"><div style="float:right"><input name="ckb_aca_Contrato" id="ckb_aca_Contrato" <?php
                                        echo $disabled;
                                        if ($ckb_aca_Contrato == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Info.Ger. Contrato</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_filAvn" id="ckb_aca_filAvn" <?php
                                        echo $disabled;
                                        if ($ckb_aca_filAvn == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Info.Ger. Fil.Avan.</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_igCaixa" id="ckb_aca_igCaixa" <?php
                                        echo $disabled;
                                        if ($ckb_aca_igCaixa == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Info.Ger. Caixa</td>
                              </tr>
                              <tr>
                                <td height="33"><div style="float:right"><input name="ckb_aca_dcc" id="ckb_aca_dcc" <?php
                                        echo $disabled;
                                        if ($ckb_aca_dcc == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Info.Ger. DCC</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_igStatus" id="ckb_aca_igStatus" <?php
                                        echo $disabled;
                                        if ($ckb_aca_igStatus == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Info.Ger. Status</td>
                                <td height="33"><div style="float:right"><input name="ckb_fin_cx_mod" id="ckb_fin_cx_mod" <?php
                                        echo $disabled;
                                        if ($ckb_fin_cx_mod == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Exibir Modalidades Caixa</td>
                              </tr>
                              <tr>
                                <td height="33"><div style="float:right"><input name="ckb_fin_cx_ticket" id="ckb_fin_cx_ticket" <?php
                                        echo $disabled;
                                        if ($ckb_fin_cx_ticket == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Exibir Ticket Médio Caixa</td>
                                <td height="33"><div style="float:right"><input name="ckb_fin_cx_mat" id="ckb_fin_cx_mat" <?php
                                        echo $disabled;
                                        if ($ckb_fin_cx_mat == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Exibir Matrículas Caixa</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_bol" id="ckb_aca_bol" <?php
                                        echo $disabled;
                                        if ($ckb_aca_bol == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Info.Ger. BOL</td>
                              </tr>
                              <tr>
                                <td height="33"><div style="float:right"><input name="ckb_aca_pix" id="ckb_aca_pix" <?php
                                    echo $disabled;
                                    if ($ckb_aca_pix == "1") {
                                        echo "CHECKED";
                                    }
                                    ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Info.Ger. PIX</td>
                                <td height="33"></td>
                                <td></td>
                                <td height="33"></td>
                                <td></td>
                              </tr>
                            </table>
                        </div>                      
                        <div class="tab-pane" id="subtabAcad4">
                            <table width="680" style="margin-bottom:270px;" border="0" cellpadding="3" class="textosComuns">
                                <tr>
                                    <td width="50" height="33"><div style="float:right"><input name="ckb_aca_all_bweb" id="ckb_aca_all_bweb" <?php echo $disabled; ?> type="checkbox" class="input-medium"/></div></td>
                                    <td>Inverter Seleção</td>
                                </tr>
                                <tr>
                                    <td height="33"><div style="float:right"><input name="ckb_aca_body" id="ckb_aca_body" <?php
                                        echo $disabled;
                                        if ($ckb_aca_body == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                    <td>Permite acesso</td>
                                    <td height="33"><div style="float:right"><input name="ckb_aca_body_trei" id="ckb_aca_body_trei" <?php
                                        echo $disabled;
                                        if ($ckb_aca_body_trei == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                    <td>Treino Padrão</td>                                    
                                </tr>
                                <tr>
                                    <td height="33"><div style="float:right"><input name="ckb_aca_body_post" id="ckb_aca_body_post" <?php
                                        echo $disabled;
                                        if ($ckb_aca_body_post == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                    <td>Publicar Post</td>
                                    <td height="33"><div style="float:right"><input name="ckb_aca_body_estag" id="ckb_aca_body_estag" <?php
                                        echo $disabled;
                                        if ($ckb_aca_body_estag == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                    <td>Estagiário</td>                                    
                                </tr>                                                                                                                         
                            </table>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane" id="tab3">
                        <table width="680" style="margin-bottom:270px;" border="0" cellpadding="3" class="textosComuns">
                            <tr>
                                <td width="50" height="33"><div style="float:right"><input name="ckb_fin_all" id="ckb_fin_all" <?php echo $disabled; ?> type="checkbox" class="input-medium"/></div></td>
                                <td colspan="4">Inverter Seleção</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_fin_fix" id="ckb_fin_fix" <?php
                                        echo $disabled;
                                        if ($ckb_fin_fix == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Lanc. de Contratos / Contas Fixas</td>
                                <td height="33"><div style="float:right"><input name="ckb_fin_afix" id="ckb_fin_afix" <?php
                                        echo $disabled;
                                        if ($ckb_fin_afix == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Aprov. de Contratos / Contas Fixas</td>
                                <td height="33"><div style="float:right"><input name="ckb_fin_flu" id="ckb_fin_flu" <?php
                                        echo $disabled;
                                        if ($ckb_fin_flu == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Lançamento de Fluxo de Caixa</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_fin_lsol" id="ckb_fin_lsol" <?php
                                        echo $disabled;
                                        if ($ckb_fin_lsol == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Lançamento de Contas Variáveis</td>
                                <td height="33"><div style="float:right"><input name="ckb_fin_asol" id="ckb_fin_asol" <?php
                                        echo $disabled;
                                        if ($ckb_fin_asol == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Aprovação em Contas Variáveis</td>
                                <td height="33"><div style="float:right"><input name="ckb_fin_pag" id="ckb_fin_pag" <?php
                                        echo $disabled;
                                        if ($ckb_fin_pag == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Lançamento de Contas à Pagar</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_fin_rec" id="ckb_fin_rec" <?php
                                        echo $disabled;
                                        if ($ckb_fin_rec == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Lançamento de Contas à Receber</td>
                                <td height="33"><div style="float:right"><input name="ckb_est_lven" id="ckb_est_lven" <?php
                                        echo $disabled;
                                        if ($ckb_est_lven == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Lançamento de Vendas</td>
                                <td height="33"><div style="float:right"><input name="ckb_est_aven" id="ckb_est_aven" <?php
                                        echo $disabled;
                                        if ($ckb_est_aven == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Autorização de Vendas</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_est_lven_read" id="ckb_est_lven_read" <?php
                                        echo $disabled;
                                        if ($ckb_est_lven_read == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Somente Leitura Vendas</td>
                                <td height="33"><div style="float:right"><input name="ckb_est_lorc" id="ckb_est_lorc" <?php
                                        echo $disabled;
                                        if ($ckb_est_lorc == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Lançamento de Orçamentos</td>
                                <td height="33"><div style="float:right"><input name="ckb_est_aorc" id="ckb_est_aorc" <?php
                                        echo $disabled;
                                        if ($ckb_est_aorc == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Autorização de Orçamentos</td>
                            </tr>
                            <tr>                                
                                <td height="33"><div style="float:right"><input name="ckb_est_com" id="ckb_est_com" <?php
                                        echo $disabled;
                                        if ($ckb_est_com == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Lançamento de Comissões</td>
                                <td height="33"><div style="float:right"><input name="ckb_fin_dmp" id="ckb_fin_dmp" <?php
                                        echo $disabled;
                                        if ($ckb_fin_dmp == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Desconto maior que o Limite em %</td>
                                <td height="33"><div style="float:right"><input name="ckb_fin_rel_gct" id="ckb_fin_rel_gct" <?php
                                        echo $disabled;
                                        if ($ckb_fin_rel_gct == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Relatório de Plano de Contas</td>
                            </tr>
                            <tr>                                
                                <td height="33"><div style="float:right"><input name="ckb_fin_rel_tdd" id="ckb_fin_rel_tdd" <?php
                                        echo $disabled;
                                        if ($ckb_fin_rel_tdd == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Relatório de Tipos de Documentos</td>
                                <td height="33"><div style="float:right"><input name="ckb_fin_rel_mov_caixa" id="ckb_fin_rel_mov_caixa" <?php
                                        echo $disabled;
                                        if ($ckb_fin_rel_mov_caixa == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Rel. de Movimentação de Caixa</td>
                                <td height="33"><div style="float:right"><input name="ckb_fin_chq" id="ckb_fin_chq" <?php
                                        echo $disabled;
                                        if ($ckb_fin_chq == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Relatório de Cheques</td>
                            </tr>
                            <tr>                                
                                <td height="33"><div style="float:right"><input name="ckb_fin_rcrt" id="ckb_fin_rcrt" <?php
                                        echo $disabled;
                                        if ($ckb_fin_rcrt == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Relatório de Cartões</td>
                                <td height="33"><div style="float:right"><input name="ckb_fin_bol" id="ckb_fin_bol" <?php
                                        echo $disabled;
                                        if ($ckb_fin_bol == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Remessa de Cobrança</td>
                                <td height="33"><div style="float:right"><input name="ckb_clb_gboleto" id="ckb_clb_gboleto" <?php
                                    echo $disabled;
                                    if ($ckb_clb_gboleto == "1") {
                                        echo "CHECKED";
                                    }
                                    ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Gerar Boletos</td>
                            </tr>
                            <tr>                                
                                <td height="33"><div style="float:right"><input name="ckb_clb_cboleto" id="ckb_clb_cboleto" <?php
                                    echo $disabled;
                                    if ($ckb_clb_cboleto == "1") {
                                        echo "CHECKED";
                                    }
                                    ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cancelar Boletos</td>
                                <td height="33"><div style="float:right"><input name="ckb_exc_cpagar" id="ckb_exc_cpagar" <?php
                                    echo $disabled;
                                    if ($ckb_exc_cpagar == "1") {
                                        echo "CHECKED";
                                    }
                                    ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Excluir Contas a pagar</td>
                                <td height="33"><div style="float:right"><input name="ckb_exc_creceber" id="ckb_exc_creceber" <?php
                                    echo $disabled;
                                    if ($ckb_exc_creceber == "1") {
                                        echo "CHECKED";
                                    }
                                    ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Excluir Contas a Receber</td>  
                            </tr>
                            <tr>                                
                                <td height="33"><div style="float:right"><input name="ckb_fin_lpg" id="ckb_fin_lpg" <?php
                                        echo $disabled;
                                        if ($ckb_fin_lpg == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Lançamento de Pagamentos</td>                              
                                <td height="33"><div style="float:right"><input name="ckb_com_forma" id="ckb_com_forma" <?php
                                        echo $disabled;
                                        if ($ckb_com_forma == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Comissão de Clientes</td>                                
                                <td height="33"><div style="float:right"><input name="ckb_cpagar_read" id="ckb_cpagar_read" <?php
                                    echo $disabled;
                                    if ($ckb_cpagar_read == "1") {
                                        echo "CHECKED";
                                    }
                                    ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Somente Leitura Contas à Pagar</td>                      
                            </tr>                            
                            <tr>                                
                                <td height="33"><div style="float:right"><input name="ckb_creceber_read" id="ckb_creceber_read" <?php
                                        echo $disabled;
                                        if ($ckb_creceber_read == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Somente Leitura Contas à Receber</td>                              
                                <td height="33"><div style="float:right"><input name="ckb_fcaixa_read" id="ckb_fcaixa_read" <?php
                                        echo $disabled;
                                        if ($ckb_fcaixa_read == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Somente Leitura Fluxo de Caixa</td>                                
                                <td height="33"></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                    <div class="tab-pane" id="tab4">
                        <table width="680" style="margin-bottom:270px;" border="0" cellpadding="3" class="textosComuns">
                            <tr>
                                <td width="120" height="33"><div style="float:right"><input name="ckb_est_all" id="ckb_est_all" <?php echo $disabled; ?> type="checkbox" class="input-medium"/></div></td>
                                <td colspan="3">Inverter Seleção</td>
                            </tr>
                            <tr>
                                <td width="120" height="33"><div style="float:right"><input name="ckb_est_mov" id="ckb_est_mov" <?php
                                        echo $disabled;
                                        if ($ckb_est_mov == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Cadastro de Subgrupos de Contas</td>
                                <td width="60" height="33"><div style="float:right"><input name="ckb_est_pro" id="ckb_est_pro" <?php
                                        echo $disabled;
                                        if ($ckb_est_pro == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Produtos</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_est_pla" id="ckb_est_pla" <?php
                                        echo $disabled;
                                        if ($ckb_est_pla == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Planos</td>
                                <td height="33"><div style="float:right"><input name="ckb_est_loc" id="ckb_est_loc" <?php
                                        echo $disabled;
                                        if ($ckb_est_loc == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Locais</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_est_cor" id="ckb_est_cor" <?php
                                        echo $disabled;
                                        if ($ckb_est_cor == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Cores</td>
                                <td height="33"><div style="float:right"><input name="ckb_est_tam" id="ckb_est_tam" <?php
                                        echo $disabled;
                                        if ($ckb_est_tam == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Tamanhos</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_fin_lped" id="ckb_fin_lped" <?php
                                        echo $disabled;
                                        if ($ckb_fin_lped == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Lançamento de Requisições</td>
                                <td height="33"><div style="float:right"><input name="ckb_fin_aped" id="ckb_fin_aped" <?php
                                        echo $disabled;
                                        if ($ckb_fin_aped == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Autorização de Requisições</td>
                            </tr>                            
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_fin_lcot" id="ckb_fin_lcot" <?php
                                        echo $disabled;
                                        if ($ckb_fin_lcot == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Lançamento de Cotação</td>                                
                                <td height="33"><div style="float:right"><input name="ckb_est_jus" id="ckb_est_jus" <?php
                                        echo $disabled;
                                        if ($ckb_est_jus == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Justificativas</td>
                            </tr>
                            <tr> 
                                <td height="33"><div style="float:right"><input name="ckb_est_lcom" id="ckb_est_lcom" <?php
                                        echo $disabled;
                                        if ($ckb_est_lcom == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Lançamento de Compras</td>
                                <td height="33"><div style="float:right"><input name="ckb_est_acom" id="ckb_est_acom" <?php
                                        echo $disabled;
                                        if ($ckb_est_acom == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Autorização de Compras</td>
                            </tr>                                
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_est_ent_sai" id="ckb_est_ent_sai" <?php
                                        echo $disabled;
                                        if ($ckb_est_ent_sai == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Lançamento de Entradas e Saídas</td>
                                <td height="33"><div style="float:right"><input name="ckb_est_aent_sai" id="ckb_est_aent_sai" <?php
                                        echo $disabled;
                                        if ($ckb_est_aent_sai == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Autorização de Entradas e Saídas</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_est_flu" id="ckb_est_flu" <?php
                                        echo $disabled;
                                        if ($ckb_est_flu == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Lançamento de Fluxo de Estoque</td>
                                <td height="33"><div style="float:right"><input name="ckb_est_fab" id="ckb_est_fab" <?php
                                        echo $disabled;
                                        if ($ckb_est_fab == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Fabricantes</td>
                            </tr>                            
                        </table>
                    </div>
                    <div class="tab-pane" id="tab5">
                        <table width="680" style="margin-bottom:270px;" border="0" cellpadding="3" class="textosComuns">
                            <tr>
                                <td width="120" height="33"><div style="float:right"><input name="ckb_nfe_all" id="ckb_nfe_all" <?php echo $disabled; ?> type="checkbox" class="input-medium"/></div></td>
                                <td colspan="3">Inverter Seleção</td>
                            </tr>
                            <tr>
                                <td width="120" height="33"><div style="float:right"><input name="ckb_nfe_cen" id="ckb_nfe_cen" <?php
                                        echo $disabled;
                                        if ($ckb_nfe_cen == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Cadastro de Cenários</td>
                                <td width="60" height="33"><div style="float:right"><input name="ckb_nfe_tra" id="ckb_nfe_tra" <?php
                                        echo $disabled;
                                        if ($ckb_nfe_tra == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Transportadoras</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_nfe_msg" id="ckb_nfe_msg" <?php
                                        echo $disabled;
                                        if ($ckb_nfe_msg == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Mensagens</td>
                                <td height="33"><div style="float:right"><input name="ckb_nfe_emi" id="ckb_nfe_emi" <?php
                                        echo $disabled;
                                        if ($ckb_nfe_emi == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Emitentes</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_nfe_est" id="ckb_nfe_est" <?php
                                        echo $disabled;
                                        if ($ckb_nfe_est == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Estados</td>
                                <td height="33"><div style="float:right"><input name="ckb_nfe_cid" id="ckb_nfe_cid" <?php
                                        echo $disabled;
                                        if ($ckb_nfe_cid == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Cidades</td>
                                <td height="33"></td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_nfe_enq" id="ckb_nfe_enq" <?php
                                        echo $disabled;
                                        if ($ckb_nfe_enq == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Enquadramento do Serviço</td>
                                <td height="33"><div style="float:right"><input name="ckb_nfe_not" id="ckb_nfe_not" <?php
                                        echo $disabled;
                                        if ($ckb_nfe_not == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Emiss.Notas Vendas</td>
                                <td height="33"></td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_nfe_ess" id="ckb_nfe_ess" <?php
                                        echo $disabled;
                                        if ($ckb_nfe_ess == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Emiss.Notas Serviços</td>
                                <td height="33"></td>
                                <td></td>
                                <td height="33"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="tab-pane" id="tab6">
                        <table width="680" style="margin-bottom:290px;" border="0" cellpadding="3" class="textosComuns">
                            <tr>
                                <td width="120" height="33"><div style="float:right"><input name="ckb_crm_all" id="ckb_crm_all" <?php echo $disabled; ?> type="checkbox" class="input-medium"/></div></td>
                                <td colspan="3">Inverter Seleção</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_adm_email" id="ckb_adm_email" <?php
                                        echo $disabled;
                                        if ($ckb_adm_email == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Modelos de Disparos</td>
                                <td height="33"><div style="float:right"><input name="ckb_adm_disparos" id="ckb_adm_disparos" <?php
                                        echo $disabled;
                                        if ($ckb_adm_disparos == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Disparos Automáticos</td>
                            </tr>                                
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_crm_pro" id="ckb_crm_pro" <?php
                                        echo $disabled;
                                        if ($ckb_crm_pro == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Lançamento em Prospects</td>
                                <td height="33"><div style="float:right"><input name="ckb_crm_tel" id="ckb_crm_tel" <?php
                                        echo $disabled;
                                        if ($ckb_crm_tel == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Lançamento em CRM</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_crm_rec" id="ckb_crm_rec" <?php
                                        echo $disabled;
                                        if ($ckb_crm_rec == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Lançamento em Recepção</td>
                                <td height="33"><div style="float:right"><input name="ckb_crm_cpc" id="ckb_crm_cpc" <?php
                                        echo $disabled;
                                        if ($ckb_crm_cpc == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Converter Prospect em Cliente</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_crm_exc_atend" id="ckb_crm_exc_atend" <?php
                                        echo $disabled;
                                        if ($ckb_crm_exc_atend == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Excluir Atendimento</td>
                                <td height="33"><div style="float:right"><input name="ckb_crm_fec_atend" id="ckb_crm_fec_atend" <?php
                                        echo $disabled;
                                        if ($ckb_crm_fec_atend == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Fechar Atendimento</td>
                            </tr>
                            <tr>
                              <td height="33"><div style="float:right"><input name="ckb_crm_upt_rem" id="ckb_crm_upt_rem" <?php
                                      echo $disabled;
                                      if ($ckb_crm_upt_rem== "1") {
                                          echo "CHECKED";
                                      }
                                      ?> value="1" type="checkbox" class="input-medium"/></div></td>
                              <td>Alterar Atendimento</td>
                              <td height="33"><div style="float:right"><input name="ckb_crm_upt_rem_new" id="ckb_crm_upt_rem_new" <?php
                                      echo $disabled;
                                      if ($ckb_crm_upt_rem_new == "1") {
                                          echo "CHECKED";
                                      }
                                      ?> value="1" type="checkbox" class="input-medium"/></div></td>
                              <td>Bloquear Alterar Atendimento de Outro Usuário</td>
                            </tr>
                            <tr>
                              <td height="33"><div style="float:right"><input name="ckb_crm_lead" id="ckb_crm_lead" <?php
                                      echo $disabled;
                                      if ($ckb_crm_lead== "1") {
                                          echo "CHECKED";
                                      }
                                      ?> value="1" type="checkbox" class="input-medium"/></div></td>
                              <td>Leads</td>
                              <td height="33"><div style="float:right"><input name="ckb_crm_lead_camp" id="ckb_crm_lead_camp" <?php
                                      echo $disabled;
                                      if ($ckb_crm_lead_camp == "1") {
                                          echo "CHECKED";
                                      }
                                      ?> value="1" type="checkbox" class="input-medium"/></div></td>
                              <td>Captação de Leads</td>
                            </tr>
                            <tr>
                              <td height="33"><div style="float:right"><input name="ckb_crm_pro_ure" id="ckb_crm_pro_ure" <?php
                                      echo $disabled;
                                      if ($ckb_crm_pro_ure== "1") {
                                          echo "CHECKED";
                                      }
                                      ?> value="1" type="checkbox" class="input-medium"/></div></td>
                              <td>Filtrar Prospects p/ Usuário Resp.</td>
                              <td height="33"><div style="float:right"><input name="ckb_crm_lea_ure" id="ckb_crm_lea_ure" <?php
                                      echo $disabled;
                                      if ($ckb_crm_lea_ure== "1") {
                                          echo "CHECKED";
                                      }
                                      ?> value="1" type="checkbox" class="input-medium"/></div></td>
                              <td>Filtrar Leads p/ Usuário Resp.</td>
                            </tr>
                            <tr>
                              <td height="33"><div style="float:right"><input name="ckb_crm_cli_ure" id="ckb_crm_cli_ure" <?php
                                      echo $disabled;
                                      if ($ckb_crm_cli_ure== "1") {
                                          echo "CHECKED";
                                      }
                                      ?> value="1" type="checkbox" class="input-medium"/></div></td>
                              <td>Filtrar Cliente p/ Usuário Resp.</td>
                              <td height="33"><div style="float:right"><input name="ckb_use_dist_pro" id="ckb_use_dist_pro" <?php
                                      echo $disabled;
                                      if ($ckb_use_dist_pro== "1") {
                                          echo "CHECKED";
                                      }
                                      ?> value="1" type="checkbox" class="input-medium"/></div></td>
                              <td>Distribuir Prospects para Usuários</td>
                            </tr>
                            <?php if ($mdl_cht_ == 1) { ?>
                            <tr>
                              <td height="33"><div style="float:right"><input name="ckb_crm_chatbot" id="ckb_crm_chatbot" <?php
                                      echo $disabled;
                                      if ($ckb_crm_chatbot== "1") {
                                          echo "CHECKED";
                                      }
                                      ?> value="1" type="checkbox" class="input-medium"/></div></td>
                              <td>Chatbot</td>
                              <td height="33"><div style="float:right"></div></td>
                              <td></td>
                            </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <div class="tab-pane" id="tab7">
                        <table width="680" style="margin-bottom:270px;" border="0" cellpadding="3" class="textosComuns">
                            <tr>
                                <td width="120" height="33"><div style="float:right"><input name="ckb_ser_all" id="ckb_ser_all" <?php echo $disabled; ?> type="checkbox" class="input-medium"/></div></td>
                                <td colspan="3">Inverter Seleção</td>
                            </tr>
                            <tr>
                                <td width="120" height="33"><div style="float:right">
                                        <input name="ckb_ser_cli" id="ckb_ser_cli" <?php
                                        echo $disabled;
                                        if ($ckb_ser_cli == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Clientes</td>
                                <td width="60" height="33">
                                    <div style="float:right">
                                        <input name="ckb_ser_ser" id="ckb_ser_ser" <?php
                                        echo $disabled;
                                        if ($ckb_ser_ser == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Serviços</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_est_ser" id="ckb_est_ser" <?php
                                        echo $disabled;
                                        if ($ckb_est_ser == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Serviços</td>
                                <td width="120" height="33"><div style="float:right">
                                        <input name="ckb_ser_spc" id="ckb_ser_spc" <?php
                                        echo $disabled;
                                        if ($ckb_ser_spc == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Rel. Pacotes</td>
                            </tr>
                        </table>
                    </div>
                    <div class="tab-pane" id="tab8">
                        <table width="680" style="margin-bottom:270px;" border="0" cellpadding="3" class="textosComuns">
                            <tr>
                                <td width="120" height="33"><div style="float:right"><input name="ckb_siv_all" id="ckb_siv_all" <?php echo $disabled; ?> type="checkbox" class="input-medium"/></div></td>
                                <td colspan="3">Inverter Seleção</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_siv_cadAcad" id="ckb_siv_cadAcad" <?php
                                        echo $disabled;
                                        if ($ckb_siv_cadAcad == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cad Acad</td>
                                <td height="33"><div style="float:right"><input name="ckb_siv_CreGB" id="ckb_siv_CreGB" <?php
                                        echo $disabled;
                                        if ($ckb_siv_CreGB == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Crédito GoBody </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_siv_cadErp" id="ckb_siv_cadErp" <?php
                                        echo $disabled;
                                        if ($ckb_siv_cadErp == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cad ERP</td>

                                <td height="33"><div style="float:right"><input name="ckb_siv_PenGB" id="ckb_siv_PenGB" <?php
                                        echo $disabled;
                                        if ($ckb_siv_PenGB == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cad Pendentes GoBody </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_siv_GerSenha" id="ckb_siv_GerSenha" <?php
                                        echo $disabled;
                                        if ($ckb_siv_GerSenha == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Gerar Senha </td>
                                <td width="60" height="33"><div style="float:right"><input name="ckb_out_ecob" id="ckb_out_ecob" <?php
                                        echo $disabled;
                                        if ($ckb_out_ecob == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>E-mail de Cobrança</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_siv_UserSite" id="ckb_siv_UserSite" <?php
                                        echo $disabled;
                                        if ($ckb_siv_UserSite == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Usuários Site </td>
                                <td width="60" height="33"><div style="float:right"><input name="ckb_siv_LogERP" id="ckb_siv_LogERP" <?php
                                        echo $disabled;
                                        if ($ckb_siv_LogERP == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Log. Acesso Remoto ERP</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_siv_DCC" id="ckb_siv_DCC" <?php
                                        echo $disabled;
                                        if ($ckb_siv_DCC == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Saldo DCC </td>
                                <td height="33"></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                    <div class="tab-pane" id="tab9">
                        <table width="680" style="margin-bottom:270px;" border="0" cellpadding="3" class="textosComuns">
                            <tr>
                                <td width="120" height="33"><div style="float:right"><input name="ckb_clb_all" id="ckb_clb_all" <?php echo $disabled; ?> type="checkbox" class="input-medium"/></div></td>
                                <td colspan="3">Inverter Seleção</td>
                            </tr>
                            <tr>
                                <td width="120" height="33"><div style="float:right">
                                    <input name="ckb_clb_loc" id="ckb_clb_loc" <?php
                                    echo $disabled;
                                    if ($ckb_clb_loc == "1") {
                                        echo "CHECKED";
                                    }
                                    ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Locações</td>
                                <td width="60" height="33">
                                    <div style="float:right">
                                        <input name="ckb_clb_eventos" id="ckb_clb_eventos" <?php
                                        echo $disabled;
                                        if ($ckb_clb_eventos == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Eventos</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_clb_categoria" id="ckb_clb_categoria" <?php
                                        echo $disabled;
                                        if ($ckb_clb_categoria == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Categorias de Sócios</td>
                                <td width="120" height="33"><div style="float:right">
                                        <input name="ckb_clb_tipo" id="ckb_clb_tipo" <?php
                                        echo $disabled;
                                        if ($ckb_clb_tipo == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Tipo de Sócios</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_clb_layout_remessa" id="ckb_clb_layout_remessa" <?php
                                        echo $disabled;
                                        if ($ckb_clb_layout_remessa == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Layout de Remessa</td>
                                <td width="120" height="33"><div style="float:right">
                                        <input name="ckb_clb_layout_retorno" id="ckb_clb_layout_retorno" <?php
                                        echo $disabled;
                                        if ($ckb_clb_layout_retorno == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Layout de Retorno</td>
                            </tr>
                            <tr>     
                                <td height="33"><div style="float:right"><input name="ckb_clb_trancar_tit" id="ckb_clb_trancar_tit" <?php
                                        echo $disabled;
                                        if ($ckb_clb_trancar_tit == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Desligar Titularidade</td>                                
                                <td width="120" height="33"><div style="float:right">
                                        <input name="ckb_clb_gconvite" id="ckb_clb_gconvite" <?php
                                        echo $disabled;
                                        if ($ckb_clb_gconvite == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Gestão de Agenda</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_clb_rel_eventos" id="ckb_clb_rel_eventos" <?php
                                        echo $disabled;
                                        if ($ckb_clb_rel_eventos == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Rel. Eventos Marcados</td>
                                <td width="120" height="33"><div style="float:right">
                                        <input name="ckb_clb_rel_socios" id="ckb_clb_rel_socios" <?php
                                        echo $disabled;
                                        if ($ckb_clb_rel_socios == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Rel. de Sócios</td>
                            </tr>                            
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_clb_baixar_layout" id="ckb_clb_baixar_layout" <?php
                                        echo $disabled;
                                        if ($ckb_clb_baixar_layout == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Baixar por Layout</td>
                                <td width="120" height="33"><div style="float:right"><input name="ckb_clb_excecao_vencimento" id="ckb_clb_excecao_vencimento" <?php
                                        echo $disabled;
                                        if ($ckb_clb_excecao_vencimento == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Exceção de Vencimentos</td>
                            </tr>                            
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_clb_gestao_documentos" id="ckb_clb_gestao_documentos" <?php
                                        echo $disabled;
                                        if ($ckb_clb_gestao_documentos == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Gestão de Documentos</td>
                                <td width="120" height="33"><div style="float:right"><input name="ckb_clb_excluir_convites" id="ckb_clb_excluir_convites" <?php
                                        echo $disabled;
                                        if ($ckb_clb_excluir_convites == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Excluir Convites</td>
                            </tr>                            
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_clb_gestao_folha" id="ckb_clb_gestao_folha" <?php
                                        echo $disabled;
                                        if ($ckb_clb_gestao_folha == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Gestão de Folha</td>
                                <td width="120" height="33"></td>
                                <td width="200"></td>
                            </tr>                            
                        </table>
                    </div>                                            
                    <div class="tab-pane" id="tab10">
                        <table width="680" style="margin-bottom:270px;" border="0" cellpadding="3" class="textosComuns">
                            <tr>
                                <td width="120" height="33"><div style="float:right"><input name="ckb_out_all" id="ckb_out_all" <?php echo $disabled; ?> type="checkbox" class="input-medium"/></div></td>
                                <td colspan="3">Inverter Seleção</td>
                            </tr>
                            <tr>
                                <td width="120" height="33"><div style="float:right"><input name="ckb_out_term" id="ckb_out_term" <?php
                                        echo $disabled;
                                        if ($ckb_out_term == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Terminal de Vendas</td>
                                <td width="60" height="33"><div style="float:right"><input name="ckb_out_terc" id="ckb_out_terc" <?php
                                        echo $disabled;
                                        if ($ckb_out_terc == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Ter. Vendas (Caixa)</td>
                            </tr>
                            <tr>
                                <td width="120" height="33"><div style="float:right"><input name="ckb_out_rexc" id="ckb_out_rexc" <?php
                                        echo $disabled;
                                        if ($ckb_out_rexc == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Retorno Banco Excel</td>
                                <td width="120" height="33"><div style="float:right"><input name="ckb_out_ges" id="ckb_out_ges" <?php
                                        echo $disabled;
                                        if ($ckb_out_ges == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">BI - Gestão de Resultados</td>
                            </tr>
                            <tr>
                                <td width="60" height="33"><div style="float:right"><input name="ckb_out_rban" id="ckb_out_rban" <?php
                                        echo $disabled;
                                        if ($ckb_out_rban == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Retorno de Cobrança</td>
                                <td width="120" height="33">
                                    <div style="float:right">
                                        <input name="ckb_out_aju" id="ckb_out_aju" <?php
                                        echo $disabled;
                                        if ($ckb_out_aju == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/>
                                    </div>
                                </td>
                                <td width="200">Ajuda</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_fin_crt" id="ckb_fin_crt" <?php
                                        echo $disabled;
                                        if ($ckb_fin_crt == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Vendas em carteira</td>
                                <td height="33"><div style="float:right"><input name="ckb_out_todos_operadores" id="ckb_out_todos_operadores" <?php
                                        echo $disabled;
                                        if ($ckb_out_todos_operadores == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Visualizar todos operadores</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_out_desconto" id="ckb_out_desconto" <?php
                                    echo $disabled;
                                    if ($ckb_out_desconto == "1") {
                                        echo "CHECKED";
                                    }
                                    ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Desconto em Pagamentos</td>
                                <td height="33"><div style="float:right"><input name="ckb_aca_estudio" id="ckb_aca_estudio" <?php
                                    echo $disabled;
                                    if ($ckb_aca_estudio == "1") {
                                        echo "CHECKED";
                                    }
                                    ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Controle de Estudio</td>                                
                            </tr>
                            <?php if ($mdl_srs_ == 1) { ?>
                                <tr>
                                    <td height="33"><div style="float:right"><input name="ckb_out_incserasa" id="ckb_out_incserasa" <?php
                                        echo $disabled;
                                        if ($ckb_out_incserasa == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                    <td>Incluir Serasa</td>
                                    <td height="33"><div style="float:right"><input name="ckb_out_remserasa" id="ckb_out_remserasa" <?php
                                        echo $disabled;
                                        if ($ckb_out_remserasa == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                    <td>Remover Serasa</td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_out_pag_avancada" id="ckb_out_pag_avancada" <?php 
                                    echo $disabled;
                                    if ($ckb_out_pag_avancada == "1") {
                                        echo "CHECKED";
                                    }
                                    ?> value="1" type="checkbox" class="input-medium"/></div>
                                </td>
                                <td>Página Avançada</td>                                
                                <td height="33" style="text-align: right;">Página Inicial:</td>
                                <td>
                                    <select name="txt_out_pag_avancada" id="txt_out_pag_avancada" style="width: 70%;" <?php if ($ckb_out_pag_avancada == "1") { echo $disabled; } else { echo "disabled"; } ?>>
                                        <option value="0" <?php echo ($txt_out_pag_avancada == "0" ? "SELECTED" : ""); ?>>Página Básica</option>
                                        <option value="1" <?php echo ($txt_out_pag_avancada == "1" ? "SELECTED" : ""); ?>>Página Avançada</option>
                                        <option value="2" <?php echo ($txt_out_pag_avancada == "2" ? "SELECTED" : ""); ?>>Página Dashboard</option>
                                    </select>
                                </td>
                            </tr>    
                        </table>
                    </div>                    
                    <div class="tab-pane" id="tab11">
                        <table width="680" style="margin-bottom:270px;" border="0" cellpadding="3" class="textosComuns">
                            <tr>
                                <td width="50" height="33"><div style="float:right"><input name="ckb_pro_all" id="ckb_pro_all" <?php echo $disabled; ?> type="checkbox" class="input-medium"/></div></td>
                                <td colspan="4">Inverter Seleção</td>
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_pro_grp_acess" id="ckb_pro_grp_acess" <?php
                                        echo $disabled;
                                        if ($ckb_pro_grp_acess == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Grupo de Acessórios</td>                                
                                <td height="33"><div style="float:right"><input name="ckb_pro_tipo" id="ckb_pro_tipo" <?php
                                        echo $disabled;
                                        if ($ckb_pro_tipo == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Tipo de Veículos</td>
                                <td height="33"><div style="float:right"><input name="ckb_pro_oficina" id="ckb_pro_oficina" <?php
                                        echo $disabled;
                                        if ($ckb_pro_oficina == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Oficinas</td>
                            </tr>
                            <tr>                                
                                <td height="33"><div style="float:right"><input name="ckb_pro_acess" id="ckb_pro_acess" <?php
                                        echo $disabled;
                                        if ($ckb_pro_acess == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Acessórios</td>
                                <td height="33"><div style="float:right"><input name="ckb_pro_item_vis" id="ckb_pro_item_vis" <?php
                                        echo $disabled;
                                        if ($ckb_pro_item_vis == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Itens de Vistoria</td>
                                <td height="33"><div style="float:right"><input name="ckb_pro_classificacao" id="ckb_pro_classificacao" <?php
                                        echo $disabled;
                                        if ($ckb_pro_classificacao == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Classificação</td>                                
                            </tr>
                            <tr>                                
                                <td height="33"><div style="float:right"><input name="ckb_pro_veiculo" id="ckb_pro_veiculo" <?php
                                        echo $disabled;
                                        if ($ckb_pro_veiculo == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Veículos</td>
                                <td height="33"><div style="float:right"><input name="ckb_pro_vistoria" id="ckb_pro_vistoria" <?php
                                        echo $disabled;
                                        if ($ckb_pro_vistoria == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Vistorias</td>
                                <td height="33"><div style="float:right"><input name="ckb_pro_montadoras" id="ckb_pro_montadoras" <?php
                                        echo $disabled;
                                        if ($ckb_pro_montadoras == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Montadoras</td>
                            </tr>
                            <tr>                                
                                <td height="33"><div style="float:right"><input name="ckb_pro_grp_cidade" id="ckb_pro_grp_cidade" <?php
                                        echo $disabled;
                                        if ($ckb_pro_grp_cidade == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Grupo de Cidades</td>
                                <td height="33"><div style="float:right"><input name="ckb_pro_grp_fabricantes" id="ckb_pro_grp_fabricantes" <?php
                                        echo $disabled;
                                        if ($ckb_pro_grp_fabricantes == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Grupo de Fabricantes</td>
                                <td height="33"><div style="float:right"><input name="ckb_pro_modelos" id="ckb_pro_modelos" <?php
                                        echo $disabled;
                                        if ($ckb_pro_modelos == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Modelos</td>                                
                            </tr>
                            <tr>                                
                                <td height="33"><div style="float:right"><input name="ckb_pro_apv_veic" id="ckb_pro_apv_veic" <?php
                                        echo $disabled;
                                        if ($ckb_pro_apv_veic == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Aprovar Veículos</td>                                
                                <td height="33"><div style="float:right"><input name="ckb_pro_exc_vei" id="ckb_pro_exc_vei" <?php
                                        echo $disabled;
                                        if ($ckb_pro_exc_vei == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Excluir Veículos</td>
                                <td height="33"><div style="float:right"><input name="ckb_pro_serv_mod" id="ckb_pro_serv_mod" <?php
                                        echo $disabled;
                                        if ($ckb_pro_serv_mod == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Serviços por Modelos</td>                                
                            </tr>
                            <tr>                                
                                <td height="33"><div style="float:right"><input name="ckb_pro_cad_veic" id="ckb_pro_cad_veic" <?php
                                        echo $disabled;
                                        if ($ckb_pro_cad_veic == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastrar Veículos</td>                               
                                <td height="33"><div style="float:right"><input name="ckb_pro_fipe" id="ckb_pro_fipe" <?php
                                        echo $disabled;
                                        if ($ckb_pro_fipe == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Buscar (Fipe|Personalizada)</td> 
                                <td height="33"><div style="float:right"><input name="ckb_pro_rastreador" id="ckb_pro_rastreador" <?php
                                        echo $disabled;
                                        if ($ckb_pro_rastreador == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Dados de Rastreador</td>                                 
                            </tr>
                            <tr>                                
                                <td height="33"><div style="float:right"><input name="ckb_pro_sinistro" id="ckb_pro_sinistro" <?php
                                    echo $disabled;
                                    if ($ckb_pro_sinistro == "1") {
                                        echo "CHECKED";
                                    }
                                    ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Eventos</td>                                
                                <td height="33"><div style="float:right"><input name="ckb_pro_apv_sin" id="ckb_pro_apv_sin" <?php
                                    echo $disabled;
                                    if ($ckb_pro_apv_sin == "1") {
                                        echo "CHECKED";
                                    }
                                    ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Aprovar Evento</td>  
                                <td height="33"><div style="float:right"><input name="ckb_pro_terceiros" id="ckb_pro_terceiros" <?php
                                    echo $disabled;
                                    if ($ckb_pro_terceiros == "1") {
                                        echo "CHECKED";
                                    }
                                    ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Cadastro de Terceiros</td>                                 
                            </tr>
                            <tr>                                
                                <td height="33"><div style="float:right"><input name="ckb_pro_rastreadores" id="ckb_pro_rastreadores" <?php
                                    echo $disabled;
                                    if ($ckb_pro_rastreadores == "1") {
                                        echo "CHECKED";
                                    }
                                    ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Rastreadores</td>                                
                                <td height="33"><div style="float:right"><input name="ckb_pro_operadoras" id="ckb_pro_operadoras" <?php
                                    echo $disabled;
                                    if ($ckb_pro_operadoras == "1") {
                                        echo "CHECKED";
                                    }
                                    ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Operadoras</td>   
                                <td height="33"></td>
                                <td></td>                                                                
                            </tr>
                            <tr>
                                <td height="33"><div style="float:right"><input name="ckb_pro_desc_plano" id="ckb_pro_desc_plano" <?php
                                    echo $disabled;
                                    if ($ckb_pro_desc_plano == "1") {
                                        echo "CHECKED";
                                    }
                                ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Desconto Plano
                                    <input name="txt_pro_desc_plano" id="txt_pro_desc_plano" style="width: 37px;" maxlength="3" type="text" value="<?php echo $txt_pro_desc_plano;?>" <?php echo $disabled;?>/> %.
                                </td>
                                <td height="33"><div style="float:right"><input name="ckb_pro_desc_servico" id="ckb_pro_desc_servico" <?php
                                    echo $disabled;
                                    if ($ckb_pro_desc_servico == "1") {
                                        echo "CHECKED";
                                    }
                                ?> value="1" type="checkbox" class="input-medium"/></div></td>                                
                                <td>Desconto Serviço
                                    <input name="txt_pro_desc_servico" id="txt_pro_desc_servico" style="width: 37px;" maxlength="3" type="text" value="<?php echo $txt_pro_desc_servico;?>" <?php echo $disabled;?>/> %.
                                </td>
                                <td height="33"></td>
                                <td></td>                                
                            </tr>                            
                        </table>
                    </div>                    
                    <div class="tab-pane" id="tab12">
                        <table width="680" style="margin-bottom:270px;" border="0" cellpadding="3" class="textosComuns">
                            <tr>
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_all" id="ckb_bi_all" <?php echo $disabled; ?> type="checkbox" class="input-medium"/></div></td>
                                <td colspan="5">Inverter Seleção</td>
                            </tr>
                            <tr>
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_auditoria" id="ckb_bi_auditoria" <?php
                                        echo $disabled;
                                        if ($ckb_bi_auditoria == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Auditoria</td>                                
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_mat_ren" id="ckb_bi_mat_ren" <?php
                                        echo $disabled;
                                        if ($ckb_bi_mat_ren == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Matrículas x Renovações</td>
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_gve_sfin" id="ckb_bi_gve_sfin" <?php
                                        echo $disabled;
                                        if ($ckb_bi_gve_sfin == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Gest. Vend. (Sem Financeiro)</td>                               
                            </tr>                                                        
                            <tr>
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_retencao" id="ckb_bi_retencao" <?php
                                        echo $disabled;
                                        if ($ckb_bi_retencao == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Retenção</td>                                
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_prev_fin" id="ckb_bi_prev_fin" <?php
                                        echo $disabled;
                                        if ($ckb_bi_prev_fin == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Previsão Financeira</td>
                                <td height="33"><div style="float:right"><input name="ckb_bi_evasao" id="ckb_bi_evasao" <?php
                                        echo $disabled;
                                        if ($ckb_bi_evasao == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Evasão</td>                                
                            </tr>                                                        
                            <tr>
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_cen_custo" id="ckb_bi_cen_custo" <?php
                                        echo $disabled;
                                        if ($ckb_bi_cen_custo == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Centro de Custos</td>                                
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_fluxo_caixa" id="ckb_bi_fluxo_caixa" <?php
                                        echo $disabled;
                                        if ($ckb_bi_fluxo_caixa == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Fluxo de Caixa</td>
                                <td height="33"><div style="float:right"><input name="ckb_bi_inadimplencia" id="ckb_bi_inadimplencia" <?php
                                        echo $disabled;
                                        if ($ckb_bi_inadimplencia == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Inadimplência</td>                               
                            </tr>                                                        
                            <tr>
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_faturamento" id="ckb_bi_faturamento" <?php
                                        echo $disabled;
                                        if ($ckb_bi_faturamento == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Faturamento</td>                                
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_com_faturamento" id="ckb_bi_com_faturamento" <?php
                                        echo $disabled;
                                        if ($ckb_bi_com_faturamento == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Comp. Faturamento</td>
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_leads" id="ckb_bi_leads" <?php
                                        echo $disabled;
                                        if ($ckb_bi_leads == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Leads</td>                             
                            </tr>                            
                            <tr>
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_formas_pagamento" id="ckb_bi_formas_pagamento" <?php
                                        echo $disabled;
                                        if ($ckb_bi_formas_pagamento == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Formas de Pagamento</td>                                
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_gestao_vendas" id="ckb_bi_gestao_vendas" <?php
                                        echo $disabled;
                                        if ($ckb_bi_gestao_vendas == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Gestão de Vendas</td>                                
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_gestao_mapa" id="ckb_bi_gestao_mapa" <?php
                                        echo $disabled;
                                        if ($ckb_bi_gestao_mapa == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Mapa de Equipe</td>
                            </tr>                                                        
                            <tr>
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_produtividade" id="ckb_bi_produtividade" <?php
                                        echo $disabled;
                                        if ($ckb_bi_produtividade == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Produtividade</td>                                
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_com_produtividade" id="ckb_bi_com_produtividade" <?php
                                        echo $disabled;
                                        if ($ckb_bi_com_produtividade == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Comp. Produtividade</td>
                                <td height="33"><div style="float:right"><input name="ckb_pro_cotas" id="ckb_pro_cotas" <?php
                                        echo $disabled;
                                        if ($ckb_pro_cotas == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Dem. Result. do Exercício</td>                                
                            </tr>                                                        
                            <tr>
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_com_comissionado" id="ckb_bi_com_comissionado" <?php
                                        echo $disabled;
                                        if ($ckb_bi_com_comissionado == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Comp. Comissionado</td>                                
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_atendimentos" id="ckb_bi_atendimentos" <?php
                                        echo $disabled;
                                        if ($ckb_bi_atendimentos == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Atendimentos</td>
                                <td height="33"></td>
                                <td></td>                                
                            </tr>                                                        
                            <tr>
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_prospects" id="ckb_bi_prospects" <?php
                                        echo $disabled;
                                        if ($ckb_bi_prospects == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Gestão de Clientes</td>                                
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_agendamentos" id="ckb_bi_agendamentos" <?php
                                        echo $disabled;
                                        if ($ckb_bi_agendamentos == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Agendamentos</td>
                                <td height="33"></td>
                                <td></td>                                
                            </tr>                            
                            <tr>
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_turmas" id="ckb_bi_turmas" <?php
                                        echo $disabled;
                                        if ($ckb_bi_turmas == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td width="200">Turmas</td>                                                                
                                <td width="60" height="33"><div style="float:right"><input name="ckb_bi_rel_acessos" id="ckb_bi_rel_acessos" <?php
                                        echo $disabled;
                                        if ($ckb_bi_rel_acessos == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" type="checkbox" class="input-medium"/></div></td>
                                <td>Rel. Acessos</td>
                                <td height="33"></td>
                                <td></td>                                
                            </tr>                                                        
                        </table>
                    </div>                    
                </div>
            </div>
            <div class="spanBNT" style="width:772px; border-top:solid 1px #CCC; overflow:hidden; margin:0; padding:0; bottom:1px; position:absolute">
                <div class="toolbar bottom tar">
                    <div class="data-fluid" style="overflow:hidden; margin-top:10px; margin-right:10px">
                        <div class="btn-group">
                            <?php if ($disabled == '') { ?>
                                <button class="btn btn-success" type="submit" title="Gravar" name="bntSave" id="bntSave" ><span class="ico-checkmark"></span> Gravar</button>
                                <?php if ($_POST['txtId'] == '') { ?>
                                    <button class="btn btn-success" title="Cancelar" onClick="parent.FecharBox()" id="bntCancelar"><span class="ico-reply"></span> Cancelar</button>
                                <?php } else { ?>
                                    <button class="btn btn-success" title="Cancelar" type="submit" name="bntAlterar" id="bntAlterar" ><span class="ico-reply"></span> Cancelar</button>
                                    <?php
                                }
                            } else { ?>
                                <button class="btn  btn-success" title="Novo" type="submit" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                                <button class="btn  btn-success" type="submit" title="Alterar" name="bntEdit" id="bntEdit" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                                <button class="btn red" type="submit" title="Excluir" name="bntDelete" id="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class=" ico-remove"> </span> Excluir</button>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="block"></div>
    </div>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/charts.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/util.js"></script>
    <script type="text/javascript">
        $("#frmEnviaDados").validate({
            errorPlacement: function (error, element) {
                return true;
            },
            rules: {
                txtNomePerm: {required: true}
            }
        });
        $("#ckb_adm_all").change(function () {
            $("#ckb_adm_cli, #ckb_adm_for, #ckb_adm_fun, #ckb_adm_usu, #ckb_adm_per, #ckb_adm_gru, #ckb_fin_doc, #ckb_fin_ban, #ckb_fin_con, #ckb_fin_mov, #ckb_adm_dep, #ckb_crm_cli, #ckb_crm_fch, #ckb_crm_esp, #ckb_crm_ind, #ckb_adm_config, #ckb_adm_agenda, #ckb_adm_contratos, #ckb_adm_atendimentos, #ckb_adm_parentesco").click();
        });
        $("#ckb_fin_all").change(function () {
            $("#ckb_fin_fix, #ckb_fin_afix, #ckb_fin_flu, #ckb_fin_lsol, #ckb_fin_asol, #ckb_fin_pag, #ckb_fin_rec, #ckb_est_com, #ckb_est_lven, #ckb_est_aven, #ckb_est_lorc, #ckb_est_aorc, #ckb_fin_dmp, #ckb_fin_rel_gct, #ckb_fin_rel_tdd, #ckb_fin_chq, #ckb_fin_rcrt, #ckb_fin_bol, #ckb_fin_rel_mov_caixa, #ckb_out_ecob, #ckb_clb_gboleto, #ckb_clb_cboleto, #ckb_exc_cpagar, #ckb_exc_creceber, #ckb_fin_lpg, #ckb_com_forma").click();
        });
        $("#ckb_est_all").change(function () {
            $("#ckb_est_mov, #ckb_est_ccu, #ckb_est_pro, #ckb_est_loc, #ckb_est_flu, #ckb_est_cor, #ckb_est_tam, #ckb_est_fab, #ckb_est_pla, #ckb_est_lcom, #ckb_est_acom, #ckb_fin_lped, #ckb_fin_aped, #ckb_fin_lcot, #ckb_est_jus, #ckb_est_ent_sai, #ckb_est_aent_sai").click();
        });
        $("#ckb_nfe_all").change(function () {
            $("#ckb_nfe_cen, #ckb_nfe_tra, #ckb_nfe_msg, #ckb_nfe_emi, #ckb_nfe_est, #ckb_nfe_cid, #ckb_nfe_enq, #ckb_nfe_not, #ckb_nfe_ess").click();
        });
        $("#ckb_crm_all").change(function () {
            $("#ckb_adm_email, #ckb_adm_disparos, #ckb_crm_pro, #ckb_crm_tel, #ckb_crm_upt_rem,#ckb_crm_upt_rem_new,#ckb_crm_pro_ure,#ckb_crm_cli_ure,#ckb_use_dist_pro,#ckb_crm_chatbot,#ckb_crm_lea_ure,#ckb_crm_lead,#ckb_crm_lead_camp,#ckb_crm_rec, #ckb_crm_cpc, #ckb_crm_exc_atend, #ckb_crm_fec_atend").click();
        });
        $("#ckb_out_all").change(function () {
            $("#ckb_out_ges, #ckb_out_aju, #ckb_out_term, #ckb_out_terc, #ckb_out_rban, #ckb_out_rexc, #ckb_fin_crt, #ckb_out_todos_operadores, #ckb_out_desconto, #ckb_out_pag_avancada, #ckb_out_incserasa, #ckb_out_remserasa, #ckb_aca_estudio").click();
        });
        $("#ckb_pro_all").change(function () {
            $("#ckb_pro_grp_acess, #ckb_pro_acess, #ckb_pro_tipo, #ckb_pro_item_vis, #ckb_pro_veiculo, #ckb_pro_vistoria, #ckb_pro_grp_cidade, #ckb_pro_grp_fabricantes, #ckb_pro_apv_veic, #ckb_pro_apv_sin, #ckb_pro_rastreador, #ckb_pro_oficina, #ckb_pro_exc_vei, #ckb_pro_cad_veic, #ckb_pro_sinistro, #ckb_pro_cotas, #ckb_pro_fipe, #ckb_pro_desc_plano, #ckb_pro_desc_servico, #ckb_pro_classificacao, #ckb_pro_montadoras, #ckb_pro_modelos, #ckb_pro_serv_mod, #ckb_pro_rastreadores, #ckb_pro_operadoras, #ckb_pro_terceiros").click();
        });
        $("#ckb_ser_all").change(function () {
            $("#ckb_ser_cli, #ckb_est_ser, #ckb_ser_spc, #ckb_ser_ser").click();
        });
        $("#ckb_aca_all_gel").change(function () {
            $(" #ckb_aca_hor, #ckb_aca_cred, #ckb_aca_pla, #ckb_aca_catr, #ckb_aca_ser, #ckb_aca_amb, #ckb_aca_conv, #ckb_aca_turmas, #ckb_aca_documentos, #ckb_aca_rel_acesso").click();
        });
        $("#ckb_aca_all_inf").change(function () {
            $("#ckb_fin_cx_ticket,#ckb_fin_cx_mat,#ckb_fin_cx_mod,#ckb_aca_inf_gerenciais, #ckb_aca_acesso, #ckb_aca_renovacoes, #ckb_aca_Contrato, #ckb_aca_filAvn, #ckb_aca_igCaixa, #ckb_aca_dcc, #ckb_aca_bol, #ckb_aca_pix, #ckb_aca_igStatus").click();
        });
        $("#ckb_aca_all_bweb").change(function () {
            $("#ckb_aca_body, #ckb_aca_body_trei, #ckb_aca_body_post, #ckb_aca_body_estag").click();
        });
        $("#ckb_clb_all").change(function () {
            $("#ckb_clb_loc, #ckb_clb_eventos, #ckb_clb_categoria, #ckb_clb_tipo, #ckb_clb_gconvite, #ckb_clb_rel_eventos, #ckb_clb_rel_socios, #ckb_clb_trancar_tit, #ckb_clb_layout_remessa, #ckb_clb_layout_retorno, #ckb_clb_excecao_vencimento, #ckb_clb_gestao_documentos, #ckb_clb_gestao_folha, #ckb_clb_excluir_convites, #ckb_clb_baixar_layout").click();
        });
        $("#ckb_aca_all_cli").change(function () {
            $("#ckb_aca_abomens, #ckb_aca_abocre, #ckb_aca_abodeb, #ckb_aca_pg_prod, #ckb_aca_can_item, #ckb_aca_data_inicio, #ckb_aca_agenda_regras, #ckb_aca_cli_block, #ckb_fin_cancel_multa, #ckb_aca_exc_mens, #ckb_aca_rem_plano, #ckb_aca_can_plano, #ckb_aca_est_mens, #ckb_aca_add_credencial, #ckb_aca_dtf_credencial, #ckb_aca_pg_retro, #ckb_aca_alt_val_mens, #ckb_aca_documento, #ckb_aca_pro_rata_mens, #ckb_aca_fer_par, #ckb_aca_fer_exc, #ckb_aca_add_convenio, #ckb_aca_exc_mret, #ckb_aca_usu_resp, #ckb_aca_cancelar_dcc, #ckb_aca_fer, #ckb_aca_add_bloqueios, #ckb_aca_plan_comp, #ckb_aca_perm_desc").click();
        });
        $("#ckb_siv_all").change(function () {
            $("#ckb_siv_cadAcad, #ckb_siv_CreGB, #ckb_siv_cadErp, #ckb_siv_PenGB, #ckb_siv_GerSenha, #ckb_siv_UserSite, #ckb_siv_DCC, #ckb_siv_LogERP").click();
        });
        $("#ckb_bi_all").change(function () {
            $("#ckb_bi_auditoria, #ckb_bi_mat_ren, #ckb_bi_retencao, #ckb_bi_evasao, #ckb_bi_inadimplencia, #ckb_bi_prev_fin, #ckb_bi_cen_custo, #ckb_bi_fluxo_caixa, #ckb_bi_faturamento, #ckb_bi_com_faturamento, #ckb_bi_leads, #ckb_bi_formas_pagamento, #ckb_bi_gestao_vendas, #$ckb_bi_gestao_mapa, #ckb_bi_produtividade, #ckb_bi_com_produtividade, #ckb_bi_com_comissionado, #ckb_bi_atendimentos, #ckb_bi_prospects, #ckb_bi_agendamentos, #ckb_bi_turmas, #ckb_bi_rel_acessos").click();
        });
        $("#ckb_out_pag_avancada").change(function () {
            $("#txt_out_pag_avancada").val(0); 
            $('#txt_out_pag_avancada').attr('disabled', !$("#ckb_out_pag_avancada").is(':checked'));
        });
    </script>
    <?php odbc_close($con); ?>
</body>
