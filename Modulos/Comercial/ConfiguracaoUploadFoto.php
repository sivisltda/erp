<?php

include "../../Connections/configini.php";

if (isset($_POST['btnSendPhoto'])) {

    if (isset($_POST["txtIdFilial"])) {        

        $allowedExts = array("gif", "jpeg", "jpg", "png", "pdf", "GIF", "JPEG", "JPG", "PNG");
        $extension = explode(".", $_FILES["fotoFilial"]["name"]);
        $extension = $extension[count($extension) - 1];                
        if ((($_FILES["file"]["type"] == "image/gif") || 
                ($_FILES["fotoFilial"]["type"] == "image/jpeg") || 
                ($_FILES["fotoFilial"]["type"] == "image/jpg") || 
                ($_FILES["fotoFilial"]["type"] == "image/pjpeg") || 
                ($_FILES["fotoFilial"]["type"] == "image/x-png") || 
                ($_FILES["fotoFilial"]["type"] == "image/png")) && ($_FILES["fotoFilial"]["size"] < 10000000) && in_array($extension, $allowedExts)) {            
            if ($_FILES["fotoFilial"]["error"] == 0) {
                if (!is_dir("./../../Pessoas/" . $contrato . "/Empresa/")) {
                    mkdir("./../../Pessoas/" . $contrato . "/Empresa/", 0777);
                }
                $nomeDoc = "logo_" . $_POST['txtIdFilial'];                
                move_uploaded_file($_FILES["fotoFilial"]["tmp_name"], "./../../Pessoas/" . $contrato . "/Empresa/" . $nomeDoc);
                echo "YES";
            }
        }
    }
}