<?php
include "../../Connections/configini.php";

$prospectIr = 0;
$imprimir = 0;
$totEquipe = 0;
$cur = odbc_exec($con, "select ACA_PROSP_CLI from sf_configuracao where id = 1");
while ($RFP = odbc_fetch_array($cur)) {
    $prospectIr = $RFP['ACA_PROSP_CLI'];
}
if (isset($_GET['imp']) && $_GET['imp'] !== 0) {
    $imprimir = 1;
}
if ($_GET['dti'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
}
if ($_GET['dtf'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
}
if ($_GET['es'] != '') {
    $estado = $_GET['es'];
}
if ($_GET['ci'] != '') {
    $cidade = $_GET['ci'];
}
if ($_GET['txtBusca'] != '') {
    $txtBusca = $_GET['txtBusca'];
}
if ($_GET['txtStatus']) {
    $txtStatus = $_GET['txtStatus'];
}
$cur = odbc_exec($con, "select count(sf_usuarios_dependentes.id_usuario) total from sf_usuarios_dependentes 
inner join sf_usuarios on id_fornecedor_despesas = funcionario
where sf_usuarios.id_usuario = " . $_SESSION["id_usuario"]) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $totEquipe = $RFP['total'];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body>
        <?php if ($imprimir == 0) { ?>
            <div id="loader"><img src="../../img/loader.gif"/></div><?php } ?>
        <div class="wrapper">
            <?php
            if ($imprimir == 0) {
                include('../../menuLateral.php');
            }
            ?>
            <div class="body">
                <?php
                if ($imprimir == 0) {
                    include("../../top.php");
                }
                ?>
                <div class="content">
                    <?php
                    if ($imprimir !== 0) {
                        $visible = 'hidden';
                    }
                    ?>
                    <div class="page-header" <?php echo $visible; ?>>
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Administrativo<small>Clientes</small></h1>
                    </div>
                    <div class="row-fluid" <?php echo $visible; ?>>
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="margin-top: 15px;float:left;">
                                    <div style="float:left">
                                        <form method="POST">
                                            <?php if ($ckb_adm_cli_read_ == 0) { ?>
                                                <button id="btnNovo" class="button button-green btn-primary" type="button" onclick="AbrirBox(1, 0)"><span class="ico-file-4 icon-white"></span></button>
                                            <?php } ?>                                                
                                                <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir('I')" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                                <button id="btnExcel" class="button button-blue btn-primary" type="button" onclick="imprimir('E')" title="Exportar Excel"><span class="ico-download-3"></span></button>
                                            <?php if ($ckb_adm_cli_read_ == 0) { ?>
                                                <button id="btnEmail" class="button button-turquoise btn-primary" type="button" onclick="AbrirBox(5, 0)"><span class="ico-envelope-3 icon-white"></span></button>
                                                <button type="button" class="button button-blue btn-primary" onclick="AbrirBox(6, 0);"><span class="ico-comment icon-white"></span></button>
                                                <?php if ($mdl_wha_ > 0) { ?>
                                                <button type="button" class="button button-blue btn-primary" onclick="AbrirBox(7, 0);"><span class="ico-phone-4 icon-white"></span></button>
                                                <?php } ?>
                                            <?php } if ($contrato == "008" && $nomeperm_ == "Administrador") { ?>   
                                                <button id="bntMigrar" type="button" title="Migrar Dados" class="button button-red btn-primary" onclick="window.location = './../Seguro/Gerenciador-Clientes.php'"><span class="icon-resize-small icon-white"></span></button>
                                            <?php } ?>                                                
                                        </form>
                                    </div>
                                </div>
                                <div style="float: right; display: flex; align-items: center;">
                                    <div style="float:left;">
                                        <label>Filial:</label>
                                        <select id="txtFilial" name="txtFilial[]" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                            <?php
                                            $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo utf8_encode($RFP['id_filial']); ?>" selected><?php echo utf8_encode($RFP['numero_filial']) . " - " . utf8_encode($RFP['Descricao']); ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>                                    
                                    <div style="float:left;margin-left: 1%;width: 150px;">
                                        <span>Usuário Resp.:</span>                                        
                                        <select name="txtUserResp" id="txtUserResp" style="width:100%" class="select" <?php echo ($crm_cli_ure_ > 0 && $totEquipe == 0 ? "disabled" : "");?>>
                                            <option value="null">Selecione</option>
                                            <?php $cur = odbc_exec($con, "select distinct id_usuario, nome from sf_usuarios 
                                            inner join sf_usuarios_filiais on id_usuario_f = id_usuario 
                                            where sf_usuarios.inativo = 0 and id_filial_f in (select id_filial_f from sf_usuarios_filiais where id_usuario_f = '" . $_SESSION["id_usuario"] . "') order by nome") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id_usuario']; ?>" <?php echo ($crm_cli_ure_ > 0 && $RFP['id_usuario'] == $_SESSION["id_usuario"] ? "selected" : "");?>><?php echo formatNameCompact(strtoupper(utf8_encode($RFP['nome']))); ?></option>
                                            <?php } ?>
                                        </select>                                        
                                    </div>
                                    <div style="float:left;margin-left: 1%;width: 150px;">
                                        <span>Grupo de Clientes :</span>
                                        <select name="txtContaMov" id="txtContaMov" style="width: 100%;" class="select" <?php echo (is_numeric($filial_grupo_filter) ? "disabled" : "");?>>
                                            <option value="null">Selecione</option>
                                            <?php
                                            $cur = odbc_exec($con, "select id_grupo,descricao_grupo from dbo.sf_grupo_cliente
                                            where tipo_grupo = 'C' and inativo_grupo = 0 " . (is_numeric($filial_grupo_filter) ? " and id_grupo = " . $filial_grupo_filter : "") . " order by descricao_grupo") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id_grupo']; ?>"<?php
                                                if (!(strcmp($RFP['id_grupo'], (is_numeric($filial_grupo_filter) ? $filial_grupo_filter : $grupopes)))) {
                                                    echo "SELECTED";
                                                }
                                                ?>><?php echo utf8_encode($RFP['descricao_grupo']); ?>
                                                </option><?php } ?>
                                            <option value="-1">Sem Grupo</option>                                                
                                        </select>
                                    </div>
                                    <div style="float:left;margin-left: 1%;width: 270px;">
                                        <div style="width: 100%;">Dia/Mês Aniv.</div>
                                        <input name="txtDtIni" id="txtDtIni" maxlength="128" type="text" style="width:79px;height: 31px;text-align:center;float: left;" value="<?php echo $DateBegin; ?>" placeholder="Dia/Mês"/>
                                        <div style="float: left;padding: 5px;">até</div>
                                        <input name="txtDtFim" id="txtDtFim" maxlength="128" type="text" style="width:79px;height: 31px;text-align:center" value="<?php echo $DateEnd; ?>" placeholder="Dia/Mês"/>
                                    </div>
                                    <div style="float:left;width: <?php echo ($mdl_aca_ == 0 ? "10" : "20");?>%;margin-left: 1%;">
                                        <div width="100%">Status:</div>
                                        <select name="txtStatus[]" id="txtStatus" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                            <option value="Ativo"    <?php
                                            if ($txtStatus === "Ativo") {
                                                echo "SELECTED";
                                            }
                                            ?>>Ativo</option>
                                            <option value="AtivoCred" <?php
                                            if ($txtStatus === "AtivoCred") {
                                                echo "SELECTED";
                                            }
                                            ?>>Ativo Credencial</option>
                                            <option value="AtivoAbono" <?php
                                            if ($txtStatus === "AtivoAbono") {
                                                echo "SELECTED";
                                            }
                                            ?>>Ativo Abono</option>
                                            <option value="AtivoEmAberto" <?php
                                            if ($txtStatus === "AtivoEmAberto") {
                                                echo "SELECTED";
                                            }
                                            ?>>Ativo Em Aberto</option>
                                            <option value="Suspenso" <?php
                                            if ($txtStatus === "Suspenso") {
                                                echo "SELECTED";
                                            }
                                            ?>>Suspenso</option>
                                            <option value="Cancelado"  <?php
                                            if ($txtStatus === "Cancelado") {
                                                echo "SELECTED";
                                            }
                                            ?>>Cancelado</option>
                                            <option value="Inativo"  <?php
                                            if ($txtStatus === "Inativo") {
                                                echo "SELECTED";
                                            }
                                            ?>>Inativo</option>
                                            <?php if ($mdl_srs_ === "1") { ?>
                                                <option value="Serasa"  <?php
                                                if ($txtStatus === "Serasa") {
                                                    echo "SELECTED";
                                                }
                                                ?>>Serasa</option>
                                            <?php } if ($mdl_clb_ === "1") { ?>
                                                <option value="Dependente"  <?php
                                                if ($txtStatus === "Dependente") {
                                                    echo "SELECTED";
                                                }
                                                ?>>Dependente</option>
                                                <option value="Desligado"  <?php
                                                if ($txtStatus === "Desligado") {
                                                    echo "SELECTED";
                                                }
                                                ?>>Desligado</option>
                                                <option value="DesligadoEmAberto"  <?php
                                                if ($txtStatus === "DesligadoEmAberto") {
                                                    echo "SELECTED";
                                                }
                                                ?>>Desligado Em Aberto</option>
                                            <?php } ?>
                                            <option value="Bloqueado"  <?php
                                            if ($txtStatus === "Bloqueado") {
                                                echo "SELECTED";
                                            } ?>>Bloqueado</option>                                                                                                                                   
                                            <option value="Excluido"  <?php
                                            if ($txtStatus === "Excluido") {
                                                echo "SELECTED";
                                            } ?>>Excluido</option>                                                                                                                                   
                                        </select>
                                    </div>
                                    <div style="float:left;margin-left: 1%;">
                                        <div width="100%">Busca:</div>
                                        <input name="txtBusca"  id="txtBusca" maxlength="100" type="text" style="width:175px;height: 31px;" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title="Nome / CPF/ CNPJ / Endereço / Bairro / Cidade / Contato"/>
                                    </div>
                                    <div style="margin-top: 15px;float:left;margin-left:0.3%;">
                                        <div style="float:left">
                                            <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead" <?php echo $visible; ?>>
                        <div class="boxtext">Clientes</div>
                    </div>
                    <div <?php
                        if ($imprimir == 0) {
                            echo "class=\"boxtable\"";
                        }
                        ?>>
                            <?php
                            if ($imprimir == 1) {
                                $titulo_pagina = "RELATÓRIO DE CONSULTA<br/>CLIENTES";
                                include "../Financeiro/Cabecalho-Impressao.php";
                            }
                            ?>
                        <table <?php
                        if ($imprimir == 0) {
                            echo "class=\"table\"";
                        } else {
                            echo "border=\"1\"";
                        }
                        ?> style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbClientes">
                            <thead>
                                <tr id="formPQ">
                                    <th width="2%"></th>
                                    <th width="3%"><?php
                                        if ($mdl_aca_ > 0) {
                                            echo "Matricula";
                                        } else {
                                            echo "Cod.";
                                        }
                                        ?></th>
                                    <th width="12%">CPF/CNPJ</th>
                                    <?php if ($mdl_aca_ > 0) { ?>
                                        <th width="22%">Nome</th>
                                        <th width="15%">Usuário Responsável:</th>
                                    <?php } else { ?>
                                        <th width="20%">Nome/Razão Social:</th>
                                        <th width="19%">Contato:</th>
                                    <?php } ?>
                                    <th width="10%">Bairro</th>
                                    <th width="9%">Cidade</th>
                                    <th width="3%"><center>UF</center></th>
                                    <th width="9%">Tel.</th>
                                    <th width="10%">E-mail</th>
                                <?php if ($imprimir == 0) { ?>
                                    <th width="5%"><center>Ação:</center></th>
                                <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="9" class="dataTables_empty">Carregando dados do Cliente</td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/multiselect/jquery.multi-select.min.js'></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/util.js'></script>
        <script type="text/javascript">
          
            $("#txtDtIni, #txtDtFim").mask("99/99");

            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }

            function finalFind(ts, isExcel) {
                var finalURL;
                if (ts === 1) {
                    finalURL = '?imp=1';
                } else if (ts === 3) {
                    finalURL = '&imp=1';
                } else {
                    finalURL = '?imp=0';
                }
                if (isExcel === "S") {
                    finalURL = finalURL + '&isExcel=S';
                }
                if ($("#txtContaMov").length && $("#txtContaMov").val() !== "null") {
                    finalURL = finalURL + '&id=' + $("#txtContaMov").val();
                }
                if ($("#txtUserResp").length && $("#txtUserResp").val() !== "null") {
                    finalURL = finalURL + '&usu=' + $("#txtUserResp").val();
                }
                if ($("#txtEstado").length && $("#txtEstado").val() !== "") {
                    finalURL = finalURL + '&es=' + $("#txtEstado").val();
                }
                if ($("#txtCidade").length && $("#txtCidade").val() !== "") {
                    finalURL = finalURL + '&ci=' + $("#txtCidade").val();
                }
                if ($("#txtDtIni").val() !== "") {
                    finalURL = finalURL + '&dti=' + $("#txtDtIni").val();
                }
                if ($("#txtDtFim").val() !== "") {
                    finalURL = finalURL + '&dtf=' + $("#txtDtFim").val();
                }                
                if ($("#txtBusca").val() !== "") {
                    finalURL = finalURL + '&txtBusca=' + $("#txtBusca").val();
                }
                if ($("#txtStatus").length && $("#txtStatus").val() !== "") {
                    finalURL = finalURL + '&txtStatus=' + $("#txtStatus").val();
                }
                if ($('#txtFilial').val() !== null) {
                    finalURL = finalURL + "&filial=" + $('#txtFilial').val();
                }
                return finalURL.replace(/\//g, "_");
            }

            function validaDt() {
                var result = true;
                if (($("#txtDtIni").val() !== "" && $("#txtDtFim").val() === "") ||
                        ($("#txtDtIni").val() === "" && $("#txtDtFim").val() !== "")) {
                    result = false;
                }
                if (result === false) {
                    bootbox.alert("Período inválido");
                    $(".bootbox-alert > div").removeClass("modal-dialog");
                    $(".bootbox-alert").css("width", "300px");
                }
                return result;
            }

            function listaEmails() {
                var emails = [];
                $.ajax({
                    url: "Gerenciador-Clientes_server_processing.php?emailMarketing=1" + finalFind(3, 'N'),
                    async: false,
                    dataType: "json",
                    success: function (j) {
                        for (var x in j.aaData) {
                            if (j.aaData[x][8].trim() !== "") {
                                emails.push(j.aaData[x][8].replace(/ /g, "") + "|" + j.aaData[x][10]);
                            }
                        }
                    }
                });
                return emails;
            }

            function imprimir(tipo) {
                var pRel = "&NomeArq=" + "Clientes" +
                        "&lbl=" + "Mat.|CPF|Nome|Responsavel|" + ($("#txtDtIni").val() !== "" && $("#txtDtFim").val() !== "" ? "Dt.Nascimento" : "Bairro") + "|Cidade|UF|Tel|Email" + (tipo === "E" ? "|Status|Grupo de Clientes|Email Marketing|Cel. Marketing" : "") + 
                        "&siz=" + "30|70|130|80|90|100|20|70|110" + (tipo === "E" ? "|100|100|50|50" : "") + 
                        "&pdf=" + "0|10|11" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + "Alunos " + $("#txtStatus option:selected").text() + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/Comercial/Gerenciador-Clientes_server_processing.php" + finalFind(3, 'N'); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tipo + "&PathArq=GenericModelPDF.php", '_blank');
            }

            function listaSMS() {
                var sms = [];
                $.ajax({
                    url: "Gerenciador-Clientes_server_processing.php?envioSms=1" + finalFind(3, 'N'),
                    async: false,
                    dataType: "json",
                    success: function (j) {
                        for (var x in j.aaData) {
                            if (j.aaData[x][7].trim() !== "") {
                                sms.push(j.aaData[x][7].replace(/ /g, "") + "|" + j.aaData[x][10]);
                            }
                        }
                    }
                });
                return sms;
            }

            $(document).ready(function () {
                var columns = "";
                columns = [{"bSortable": false},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false}];
                var tbClientes = $('#tbClientes').dataTable({
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": 'Gerenciador-Clientes_server_processing.php' + finalFind(<?php echo $imprimir; ?>, 'N'),
                    "bFilter": false,
                    "aoColumns": columns,
                    'oLanguage': {
                        'oPaginate': {
                            'sFirst': "Primeiro",
                            'sLast': "Último",
                            'sNext': "Próximo",
                            'sPrevious': "Anterior"
                        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                        'sLengthMenu': "Visualização de _MENU_ registros",
                        'sLoadingRecords': "Carregando...",
                        'sProcessing': "Processando...",
                        'sSearch': "Pesquisar:",
                        'sZeroRecords': "Não foi encontrado nenhum resultado"},
                    "sPaginationType": "full_numbers"
                });

                $("#btnfind").click(function () {
                    if (validaDt()) {
                        tbClientes.fnReloadAjax('Gerenciador-Clientes_server_processing.php' + finalFind(0, 'N'));
                    }
                });

                $('#txtEstado').change(function () {
                    if ($(this).val()) {
                        $('#txtCidade').hide();
                        $('#carregando').show();
                        $.getJSON('locais.ajax.php?search=', {txtEstado: $(this).val(), ajax: 'true'}, function (j) {
                            var options = '<option value="">Selecione a cidade</option>';
                            for (var i = 0; i < j.length; i++) {
                                options += '<option value="' + j[i].cidade_codigo + '">' + j[i].cidade_nome + '</option>';
                            }
                            $('#txtCidade').html(options).show();
                            $('#carregando').hide();
                        });
                    } else {
                        $('#txtCidade').html('<option value="">Selecione a cidade</option>');
                    }
                });
            });

            function reativarCli(idCli) {
                bootbox.confirm('Confirma a reativação?', function (result) {
                    if (result === true) {
                        $.post("../../Modulos/Academia/form/ClientesFormServer.php", "isAjax=S&reativarCadastro=S&idCli=" + idCli).done(function (data) {
                            if (!isNaN(data.trim())) {
                                bootbox.alert("Reativado com sucesso!");
                                $("#btnfind").trigger("click");
                            } else {
                                alert(data);
                            }
                        });
                    } else {
                        return;
                    }
                });
            }

            function excluirCli(idCli) {
                bootbox.confirm('Confirma a exclusão?', function (result) {
                    if (result === true) {
                        $.post("../../Modulos/Comercial/form/GerenciadorClientesForm.php", "btnExcluir=S&isAjax=S&tpSelect=C&idCli=" + idCli).done(function (data) {
                            if (data.trim() === "YES") {
                                bootbox.alert("Excluido com sucesso!");
                                $("#btnfind").trigger("click");
                            } else {
                                alert(data);
                            }
                        });
                    } else {
                        return;
                    }
                });
            }

            function AbrirBox(opc, id) {
                if (opc === 1) {
                <?php if ($prospectIr == 1) { ?>
                    window.location = './../CRM/ProspectsForm.php';
                <?php } elseif ($mdl_aca_ > 0) { ?>
                    window.location = './../Academia/ClientesForm.php';
                <?php } else { ?>
                    window.location = './../CRM/CRMForm.php';
                <?php } ?>
                }
                if (opc === 2) {
                <?php if ($mdl_aca_ > 0) { ?>
                    $("#loader").show();
                    window.location = './../Academia/ClientesForm.php?id=' + id;
                <?php } else { ?>
                    $("#loader").show();
                    window.location = './../CRM/CRMForm.php?id=' + id;
                <?php } ?>
                }
                if (opc === 4) {
                    window.open('Gerenciador-Clientes.php' + finalFind(1, 'N'), '_blank');
                }
                if (opc === 5) {
                    abrirTelaBox("../CRM/EmailsEnvioForm.php", 650, 720);
                }
                if (opc === 6) {
                    abrirTelaBox("../CRM/SMSEnvioForm.php", 400, 400);
                }
                if (opc === 7) {
                    abrirTelaBox("../CRM/SMSEnvioForm.php?zap=s", 400, 400);
                }
            }

            function FecharBox(opc) {
                var oTable = $('#tbClientes').dataTable();
                oTable.fnDraw(false);
                $("#newbox").remove();
            }

            <?php if ($imprimir == 1) { ?>
            $(window).load(function () {
                $("#tbClientes_length").remove();
                $("#tbClientes_filter").remove();
                $("#tbClientes_paginate").remove();
                $("#formPQ > th").css("background-image", "none");
                var intervalo = window.setTimeout(geraRel, 2000);
                function geraRel() {
                    $(".body").css("margin-left", 0);
                    window.print();
                    window.close();
                }
            });
        <?php } ?>
        </script>
        <?php odbc_close($con); ?>
    </body>
</html>
