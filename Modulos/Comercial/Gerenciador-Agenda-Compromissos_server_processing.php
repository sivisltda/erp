<?php

if (!isset($_GET['pdf'])) {
    include './../../Connections/configini.php';
}
$aColumns = array('id_agendamento', 'data_inicio', 'data_fim', 'razao_social', 'assunto', 'obs', 'concluido', 'agendado_por', 'descricao_agenda');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = " and ((user_resp is null or user_resp = " . $_SESSION["id_usuario"] . ") or (user_permissao is null or user_permissao in (select isnull(master, 0) from sf_usuarios where id_usuario = " . $_SESSION["id_usuario"] . "))) ";
$sWhere = "";
$imprimir = 0;
$sLimit = 20;
$sOrder = " ORDER BY data_inicio asc ";

if (is_numeric($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
    $sWhereX .= " and (data_inicio between " . valoresDataHora2($DateBegin, "00:00:00") . " AND " . valoresDataHora2($DateEnd, "23:59:59") . " or data_fim between " . valoresDataHora2($DateBegin, "00:00:00") . " AND " . valoresDataHora2($DateEnd, "23:59:59") . ")";
}

if (isset($_GET['fq']) && $_GET['fq'] != "null" && $_GET['fq'] != "") {
    $sWhereX .= " and sf_agendamento.id_agenda in (" . $_GET['fq'] . ")";
}

if (is_numeric($_GET['ps']) && $_GET['ps'] > 0) {
    $sWhereX .= " and id_fornecedores_despesas = '" . $_GET['ps'] . "' ";
}

if ($_GET['cl'] != "") {
    $sWhereX .= " and concluido = '" . $_GET['cl'] . "' ";
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0 && intval($_GET['iSortCol_' . $i]) < 9) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . "
                                    " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY data_inicio asc";
    }
}

if ($_GET['Search'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['Search']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
        $sWhere .= $aColumns[$i] . " AND LIKE '%" . utf8_decode($_GET['sSearch_' . $i]) . "%' ";
    }
}

$sQuery = "SET DATEFORMAT DMY;SELECT COUNT(*) total from sf_agendamento 
inner join sf_fornecedores_despesas on id_fornecedores_despesas = id_fornecedores
inner join sf_agenda on sf_agenda.id_agenda = sf_agendamento.id_agenda
WHERE sf_agendamento.inativo = 0 " . $sWhereX . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SET DATEFORMAT DMY;SELECT COUNT(*) total from sf_agendamento 
inner join sf_fornecedores_despesas on id_fornecedores_despesas = id_fornecedores
inner join sf_agenda on sf_agenda.id_agenda = sf_agendamento.id_agenda
WHERE sf_agendamento.inativo = 0 " . $sWhereX;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SET DATEFORMAT DMY;SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,
id_agendamento, data_inicio, data_fim, descricao_agenda, razao_social, assunto, obs, concluido, agendado_por, id_fornecedores_despesas 
from sf_agendamento inner join sf_fornecedores_despesas on id_fornecedores_despesas = id_fornecedores
inner join sf_agenda on sf_agenda.id_agenda = sf_agendamento.id_agenda
WHERE sf_agendamento.inativo = 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder;
//echo $sQuery1;exit;
$cur = odbc_exec($con, $sQuery1);

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = escreverDataHora($aRow[$aColumns[1]]);
    $row[] = escreverDataHora($aRow[$aColumns[2]]);
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[8]]) . "'>" . utf8_encode($aRow[$aColumns[8]]) . "</div>";    
    if (isset($_GET["al"]) && $_GET["al"] == "S") {    
        $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[3]]) . "'>" . utf8_encode($aRow[$aColumns[3]]) . "</div>";
    } else {
        $row[] = "<a href='javascript:void(0)' onClick='AbrirBoxAgenda(" . utf8_encode($aRow[$aColumns[0]]) . ")'><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[3]]) . "'>" . utf8_encode($aRow[$aColumns[3]]) . "</div></a>";
    }
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[4]]) . "'>" . utf8_encode($aRow[$aColumns[4]]) . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[5]]) . "'>" . utf8_encode($aRow[$aColumns[5]]) . "</div>";
    $row[] = "<center>" . ($aRow[$aColumns[6]] == 1 ? "SIM" : "NÃO") . "</center>";
    $row[] = utf8_encode($aRow[$aColumns[7]]);    
    if (isset($_GET['pdf'])) {
        $row[] = escreverData($aRow[$aColumns[1]]);
        $grp = array(escreverData($aRow[$aColumns[1]]), escreverData($aRow[$aColumns[1]]), 8);
        if (!in_array($grp, $grupoArray)) {
            $grupoArray[] = $grp;
        }
    }    
    if (isset($_GET["al"]) && $_GET["al"] == "S") {
        $row[] = "<center><a onclick=\"abrirContrato(" . utf8_encode($aRow[$aColumns[0]]) . ");\" href=\"javascript:;\" class=\"btn green-haze\" title=\"Contrato\" style=\"padding: 3px 4px 0px 4px;background:#d6ab28;\"><span class=\"ico-file-4 icon-white\"></span></a></center>";
    } else {
        $row[] = "<center><a onclick=\"selectConvite(" . utf8_encode($aRow[$aColumns[0]]) . ",'" . utf8_encode($aRow[$aColumns[4]]) . "','" . escreverData($aRow[$aColumns[1]]) . "','" . escreverData($aRow[$aColumns[2]]) . "'," . utf8_encode($aRow["id_fornecedores_despesas"]) . ",'" . utf8_encode($aRow[$aColumns[3]]) . "');\" href=\"javascript:;\" class=\"btn green-haze\" title=\"Convites\" style=\"padding: 4px 4px 0px 4px;background:#d6ab28;\"><span class=\"ico-plus icon-white\"></span></a></center>";
    }    
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
} else {
    $output['aaGrupo'] = $grupoArray;
}
odbc_close($con);
