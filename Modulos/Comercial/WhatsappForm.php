<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/ico" href="../../../favicon.ico"/>
        <link href="../../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../../css/main.css" rel="stylesheet">
        <link href="../../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    </head>
    <body>
        <form name="frmEnviaDados" id="frmEnviaDados" method="POST">
            <div class="frmhead">
                <div class="frmtext">WhatsApp Web</div>
                <div class="frmicon" onClick="parent.FecharTermos();">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont" style="float: left; width: 55%; height: 265px;">
                <img id="imgQrCode" src="" alt="#" style="display: none;">
            </div>
            <div class="frmcont" style="float: left; width: 29%; margin-left: 0px; height: 265px;">
                <button id="bntSave" class="btn btn-success" style="width: 100%; display: none;" type="button" onclick="ligarSistema();"><span class="icon-ok icon-white"></span> Ativar</button>
                <br><p id="txtDados"></p>
                <a id="txtDesconectar" style="width: 100%; display: none;" href="javascript:void(0)" onclick="desconectarSistema()">Desconectar</a>
            </div>
            <div style="float: left; width: 100%;">
                <p id="txtData" style="text-align: center;"></p>
            </div>
        </form>
        <script type='text/javascript' src='../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
        <script type='text/javascript' src='../../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../../js/plugins.js'></script>
        <script type='text/javascript' src='../../../js/actions.js'></script>
        <script type='text/javascript' src='../../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='../../../js/jquery.validate.min.js'></script>
        <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../../js/util.js"></script>
        <script type="text/javascript" src="../../../js/moment.min.js"></script>        
        <script type="text/javascript">
            
            refresh();
            
            function ligarSistema() {
                $("#bntSave").attr('disabled', true);
                $.get("./../Sivis/ws_erp.php", {WinWhatsapp: 'S', Contrato: parent.$("#txtMnContrato").val(), 
                    save: ($("#bntSave").hasClass("btn-success") ? '1' : '0')}, function(data){
                    if(data === "YES") {
                        if ($("#bntSave").hasClass("btn-success")) {
                            $("#bntSave").removeClass("btn-success").addClass("yellow");
                            $("#bntSave").html("<span class=\"ico-off icon-white\"></span> Parar");
                        } else {
                            $("#bntSave").removeClass("yellow").addClass("btn-success");
                            $("#bntSave").html("<span class=\"icon-ok icon-white\"></span> Iniciar");
                        }
                    }
                    $("#bntSave").attr('disabled', false);
                });
            }
            
            function desconectarSistema() {
                bootbox.confirm('Confirma desconectar o whatsapp?', function (result) {
                    if (result === true) {
                        $("#txtDesconectar").hide();
                        $.get("./../Sivis/ws_erp.php", {WinWhatsapp: 'S', Contrato: parent.$("#txtMnContrato").val(), save: "2"}, function(data) {
                            if(data === "YES") {}
                        });                        
                    }
                });
            }
            
            function refresh() {
                $.getJSON('./../Comercial/ajax/Whatsapp_server_processing.php', {ajax: 'true'}, function (j) {
                    $("#bntSave").show();
                    if (j.online === "1" && $("#bntSave").hasClass("btn-success")) {
                        $("#bntSave").removeClass("btn-success").addClass("yellow");
                        $("#bntSave").html("<span class=\"ico-off icon-white\"></span> Parar");
                    } else if (j.online === "0" && $("#bntSave").hasClass("yellow")) {
                        $("#bntSave").removeClass("yellow").addClass("btn-success");
                        $("#bntSave").html("<span class=\"icon-ok icon-white\"></span> Iniciar");
                    }
                    if (j.online === "1" && j.pagina === "LOGIN" && j.img !== "") {
                        $("#imgQrCode").show();
                        $("#imgQrCode").attr("src", j.img);
                    } else {
                        $("#imgQrCode").hide();    
                    }
                    if (j.online === "1" && j.pagina === "PRINCIPAL") {                    
                        $("#txtDesconectar").show();
                    } else {
                        $("#txtDesconectar").hide();
                    }
                    if (j.data === "") {
                        $("#txtData").html("(Não Iniciado)");
                    } else {
                        $("#txtData").html("Visto por último: " + j.data);
                    }
                    let dados = "";
                    if (j.online === "1") {
                        if (j.pagina === "LOGIN") {
                            dados += "<b>Status:</b><br>Aguardando Login<br>";                            
                        } else if (j.pagina === "PRINCIPAL") {
                            dados += "<b>Status:</b><br>Pronto para envio<br>";
                        } else {
                            dados += "<b>Status:</b><br>Processando<br>";    
                        }
                        if(parent.$("#txtUserLog").val() === "ADMIN") {
                            dados += "<b>Página:</b><br>" + j.pagina + "<br>";                            
                            dados += "<b>Estado:</b><br>" + j.estado + "<br>";
                            dados += "<b>Ciclo:</b><br>" + j.ciclo + "<br>";
                            dados += (j.celular && j.celular.length > 0 ? "<b>Celular:</b><br>" + j.celular + "<br>" : "");
                            dados += (j.mensagem && j.mensagem.length > 0 ? "<b>Mensagem:</b><br>" + j.mensagem + "<br>" : "");
                        }
                    }
                    $("#txtDados").html(dados);
                    window.setTimeout(function () {
                        refresh();
                    }, 3000);                    
                });
            }
            
        </script>                
    </body>
</html>