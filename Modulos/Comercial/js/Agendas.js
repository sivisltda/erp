$(document).ready(function () {
    $("#btnfind").click(function () {
        tbLista.fnReloadAjax(finalFind(0));
    });
});

function finalFind(imp) {
    var retPrint = (imp === 1 ? '&imp=1' : '?imp=0');
    if ($("#txtStatus").val()!== "null") { 
        retPrint = retPrint + '&status=' + $("#txtStatus").val(); 
    }    
    if ($("#txtBusca").val() !== "") {
        retPrint = retPrint + '&Search=' + $("#txtBusca").val();
    }
    return "ajax/Gerenciador-Agenda_server_processing.php" + retPrint.replace(/\//g, "_");
}

function AbrirBox(id) {
    abrirTelaBox("Gerenciador-AgendaForm.php" + (id > 0 ? "?id=" + id : ""), 320, 460);
}

function FecharBox() {
    $("#newbox").remove();
    tbLista.fnReloadAjax(finalFind(0));
}

function imprimir() {
    var pRel = "&NomeArq=" + "Agendas" +
            "&lbl=Id|Descrição|Horário|N° Dias Online|Online|Usuário|Perfil de acesso|Inativo|Sem Regras?" +
            "&siz=40|150|90|70|50|80|90|65|65" +
            "&pdf=10" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + "../Modulos/Comercial/" + finalFind(1); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
}

function RemoverItem(id) {
    bootbox.confirm('Deseja deletar esse registro?', function (result) {
        if (result === true) {
            $.post("form/Gerenciador-AgendaFormServer.php", {List: true, bntDelete: true, txtId: id}).done(function (data) {
                tbLista.fnReloadAjax(finalFind(0));
            });
        } else {
            return;
        }
    });
}

var tbLista = $('#tblLista').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bFilter": false,
    "aaSorting": [[0, 'asc']],
    "aoColumns": [
        {"bSortable": true},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false}],
    "sAjaxSource": finalFind(0),
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

function TeclaKey(event) {
    if (event.keyCode === 13) {
        $("#btnfind").click();
    }
}