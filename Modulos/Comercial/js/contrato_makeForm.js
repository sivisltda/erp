$(document).ready(function () {
    var id_contrato = parent.$("#id_contrato").val();
    $("#id_contrato").val(id_contrato);
    tabelaClick();
});

function validaForm() {
    salvar();
    var value = CKEDITOR.instances['ckeditor'].getData();
    if ($("#txtTipo").val() === "null" || (value === "" && $('#txtTipo').val() === "1")) {
        bootbox.alert("Existe campo obrigatório sem preencher!");
        return false;
    }
    if ($('#txtTipo').val() === "1") {
        $("#saveContent").val(value);
    } else if ($('#txtTipo').val() === "2" || $('#txtTipo').val() === "3") {
        $("#saveContent").val($("#content").html());
    }
    return true;
}

CKEDITOR.on("instanceReady", function (event) {
    $(".cke_contents").height("295px");
});

function variable(e) {
    if ($('#txtTipo').val() === "1") {
        CKEDITOR.instances['ckeditor'].insertText(e);
    } else if ($('#txtTipo').val() === "2" || $('#txtTipo').val() === "3") {
        var texto = $("#selText").val();
        var cursorPosition = $("#selText").prop("selectionStart");
        var texto1 = texto.substring(0, cursorPosition);
        var textoAinserir = e;
        var texto2 = texto.substring(cursorPosition);
        var textoTotal = texto1 + textoAinserir + texto2;
        $("#selText").val(textoTotal);
    }
}

$('#txtTipo').change(function () {
    visibleOptions();
});

function visibleOptions() {
    if ($('#txtTipo').val() === "1") {
        $("#divTable").hide();
        $("#content").hide();
        $("#ckeditorCont").show();
        if (!$("#txtTipo").attr('disabled')) {
            $("#txtAlign").attr('disabled', false);
        }
    } else if ($('#txtTipo').val() === "2" || $('#txtTipo').val() === "3") {
        $("#divTable").css("display", "flex");
        $("#content").show();
        $("#ckeditorCont").hide();
        $("#txtAlign").attr('disabled', true);
    }
}

visibleOptions();

function criarTabela() {
    var border = $('#txtTipo').val() === "3" ? 0 : 1;
    var table = "<table border=\""+border+"\" style=\"line-height: 16px;width: 100%;\">";
    $("#content").html("");
    for (i = 0; i < $("#line").val(); i++) {
        table += "<tr>";
        for (j = 0; j < $("#coll").val(); j++) {
            table += "<td>" + i + "-" + j + "</td>";
        }
        table += "</tr>";
    }
    table += "</table>";
    $("#content").html(table);
    tabelaClick();
}

function tabelaClick() {
    $("td").click(function () {
        loadValues($(this));
    });
}

function loadValues(celula) {
    if ($("#txtTipo").attr("disabled") === undefined) {
        var line = parseInt(celula.parent().children().index(celula));
        var coll = parseInt(celula.parent().parent().children().index(celula.parent()));
        if ((line !== parseInt($("#selLine").val()) || coll !== parseInt($("#selColl").val())) && !$("#detail").is(":visible")) {
            $("#selLine").val(line);
            $("#selColl").val(coll);
            var texto = $("table").children().children().eq($("#selColl").val()).children().eq($("#selLine").val()).text();
            $("table").children().children().eq($("#selColl").val()).children().eq($("#selLine").val()).html("<input id=\"selText\" type=\"text\" value=\"" + texto + "\"/>");
            $("#selWidth").val($("table").children().children().eq($("#selColl").val()).children().eq($("#selLine").val())[0].style.width.replace("%", ""));
            $("#selGroup").val($("table").children().children().eq($("#selColl").val()).children().eq($("#selLine").val()).attr("colspan"));
            $("#selTextAlign").val($("table").children().children().eq($("#selColl").val()).children().eq($("#selLine").val())[0].style["text-align"]);
            $("#selFontText").val($("table").children().children().eq($("#selColl").val()).children().eq($("#selLine").val())[0].style["font-weight"]);
            if( $("#selBackground").val()) $("#selBackground").val($("table").children().children().eq($("#selColl").val()).children().style["background-color"]);
            $("#detail").css({top: ($("#selText").position().top + 28), left: ($("#selText").position().left)});
            $("#detail").show();
        }
    }
}

function salvar() {
    if ($('#selText').val()) {
        $("table").children().children().eq($("#selColl").val()).children().eq($("#selLine").val()).html($("#selText").val());
        if ($("#selWidth").val() !== "" || $("#selTextAlign").val() !== "" || $("#selFontText").val() !== "" || $('#selBackground').val() !== "") {
            $("table").children().children().eq($("#selColl").val()).children().eq($("#selLine").val()).css("width", $("#selWidth").val() + "%");
            $("table").children().children().eq($("#selColl").val()).children().eq($("#selLine").val()).css("text-align", ($("#selTextAlign").val() === "right" ? "right" : $("#selTextAlign").val() === "center" ? "center" : ""));
            $("table").children().children().eq($("#selColl").val()).children().eq($("#selLine").val()).css("font-weight", $("#selFontText").val());
            $("table").children().children().eq($("#selColl").val()).children().css("background-color", $('#selBackground').val());
        } else {
            $("table").children().children().eq($("#selColl").val()).children().eq($("#selLine").val()).removeAttr("style");
        }
        collSpan();
        Limpar();
    }
}

function collSpan() {
    var colspanAnt = parseInt($("table").children().children().eq($("#selColl").val()).children().eq($("#selLine").val()).attr("colspan"));
    if (!isNaN(colspanAnt)) {
        for (i = 1; i < colspanAnt; i++) {
            $("table").children().children().eq($("#selColl").val()).children().eq((parseInt($("#selLine").val()))).parent().append('<td>' + $("#selColl").val() + '-' + (parseInt($("#selLine").val()) + i) + '</td>');
        }
    }
    $("table").children().children().eq($("#selColl").val()).children().eq($("#selLine").val()).removeAttr("colspan");
    if ($("#selGroup").val() !== "") {
        for (i = (parseInt($("#selGroup").val()) - 1); i >= 1; i--) {
            $("table").children().children().eq($("#selColl").val()).children().eq((parseInt($("#selLine").val()) + i)).remove();
        }
        $("table").children().children().eq($("#selColl").val()).children().eq($("#selLine").val()).attr('colspan', $("#selGroup").val());
    }
}

function Limpar() {
    $("#selColl").val("");
    $("#selLine").val("");
    $("#selText").val("");
    $("#selWidth").val("");
    $("#selGroup").val("");
    $("#detail").hide();
    tabelaClick();
}
