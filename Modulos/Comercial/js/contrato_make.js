
var tbLista = $("#tblContratos").dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "ordering": false,
    "bInfo": false,
    "bDestroy": true,
    "bServerSide": true,
    "sAjaxSource": finalFind(0),
    "bFilter": false,
    "bLengthChange": false,
    "bPaginate": false,
    "aoColumns": [
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false}],
    "oLanguage": {
        "oPaginate": {
            "sFirst": "Primeiro",
            "sLast": "Último",
            "sNext": "Próximo",
            "sPrevious": "Anterior"
        }, "sEmptyTable": "Não foi encontrado nenhum resultado",
        "sInfo": "Visualização do registro _START_ ao _END_ de _TOTAL_",
        "sInfoEmpty": "Visualização do registro 0 ao 0 de 0",
        "sInfoFiltered": "(filtrado de _MAX_ registros totais)",
        "sLengthMenu": "Visualização de _MENU_ registros",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sSearch": "Pesquisar:",
        "sZeroRecords": "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers",
    "fnInitComplete": function (oSettings, json) {
    }
});

function addItem(id, ord) {
    var concat = "?id_contrato=" + $("#id_contrato").val();
    if (id !== undefined) {
        concat += "&id=" + id + "&order=" + ord;
    }
    abrirTelaBox("ContratoMakeForm.php" + concat, 650, 1000);
}

function FecharBox() {
    var oTable = $("#tblContratos").dataTable();
    oTable.fnDraw(false);
    $("#newbox").remove();
}

function RemoverItem(id) {
    if (confirm("Deseja deletar esse registro?")) {
        $.post("ajax/contrato_make_server_processing.php", {List: true, bntDelete: true, txtId: id}).done(function (data) {
            tbLista.fnReloadAjax(finalFind(0));
        });
    }
}

function finalFind(op, id) {
    if (op === 0) {
        return "ajax/contrato_make_server_processing.php?listItems=S&id_contrato=" + $("#id_contrato").val();
    } else {
        return "Contrato_make.php?id=" + id;
    }
}

function reordernar(op) {
    var marcado = $('input[name=row]:checked').val();
    $.post("ajax/contrato_make_server_processing.php", {reordernar: true, id_contrato: $("#id_contrato").val(), id_item: $('input[name=row]:checked').val(), op: op}).done(function (data) {
        $("#tblContratos").dataTable().fnDestroy();
        var tblrefresh = $("#tblContratos").dataTable(
          {
            "ordering": false,
            "bFilter": false,
            "bLengthChange": false,
            "bPaginate": false,
            "aoColumns": [
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false}]
          }
        );
        tblrefresh.fnReloadAjax(finalFind(0), function () {
            $("input[name=row][value=" + marcado + "]").prop('checked', true);
        }, null);
    });
}

function AbrirBox() {
    window.open("../../util/ImpressaoPdf.php?id=" + $("#id_contrato").val() +'&model=S&exemple=S'+
    "&tpImp=I&NomeArq=Contrato&PathArq=../Modulos/Comercial/modelos/ModeloContrato.php&emp=1", '_blank');
}
