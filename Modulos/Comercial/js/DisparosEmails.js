
function sendEmailrow(id, lista) {
    var PgIni = parseInt($("#pgIni").html());
    var PgFim = parseInt($("#pgFim").html());
    var lote = "";
    if ((PgIni < PgFim) && PgFim > 0 && $('#source').dialog('isOpen')) {
        for (i = PgIni; i < PgIni + 1; i++) {
            if ($.isArray(lista[i])) {
                lote += lista[i][9] + "|" + lista[i][10] + "||" + lista[i][11] + "|" + lista[i][12] + ",";
            }
        }
        lote = lote.substring(0, (lote.length - 1));
        $("#progressbar").progressbar({value: (((PgIni + lote.split(",").length) * 100) / PgFim)});
        $("#pgIni").html(PgIni + lote.split(",").length);
        $.ajax({
            type: "GET", url: "./../CRM/form/EmailsEnvioFormServer.php",
            data: {btnEnviar: 'S', txtId: id, txtEmails: lote}, dataType: "json",
            success: function (data) {
                $("#txtTotalSucesso").val(parseInt($("#txtTotalSucesso").val()) + parseInt(data[0].qtd_sucesso));
                $("#txtTotalErro").val(parseInt($("#txtTotalErro").val()) + parseInt(data[0].invalidos));
                sendEmailrow(id, lista);
            },
            error: function (error) {
                bootbox.alert(error.responseText + "!");
                refreshTableBol();
            }
        });
    } else {
        $("#source").dialog('close');
        bootbox.alert("Enviados: " + $("#txtTotalSucesso").val() + " Aprovada(s) e " + $("#txtTotalErro").val() + " com erro(s)!");
        refreshTableBol();
    }
}