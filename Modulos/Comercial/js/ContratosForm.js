
function validaForm() {
    if ($("#txtDescricao").val() === "" || $("#txtFilial").val() === "null" || $("#txtTipo").val() === "null") {
        bootbox.alert("Existe campo obrigatório sem preencher!");
        return false;
    }
    return true;
}
