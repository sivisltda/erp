$(document).ready(function () {

    carregaLogo(); 
    carregaLogoWhatsapp();
    carregaLogoMenu();
    carregaFotoContratacao();    
    
    $("#txtDiasRenovacao, #txtDiaVencimento, #txtDescontoDias").mask('00');
    $(".clsCnpj").mask("99.999.999/9999-99");
    $("#txtMargem_lucro, #txtPisoFranquia, #txtCotaDefault, #txtDescontoValor").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
    $("#txtTaxaInicial, #txtMoraDiaria").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"], centsLimit: 4});
    trataMask();

    $('#txtComandaPersonalizada').change(function () {
        trataMask();
    });

    $('#txtComandaMascara').bind('keypress', function (event) {
        var tecla = String.fromCharCode(!event.charCode ? event.which : event.charCode).toUpperCase();
        if (tecla !== "S" && tecla !== "0") {
            event.preventDefault();
        }
    });

    var tblFiliais = $('#tblFiliais').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "FormConfiguracao_server_processing.php?listFiliais=S",
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        'oLanguage': {
            'sInfo': "",
            'sInfoEmpty': "",
            'sInfoFiltered': "",
            'sLengthMenu': "",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers"
    });

    var nEditing = null;
    var idEditing = "";
    $("body").on("click", "#tblFiliais #btnAlt", function () {
        var nRow = $(this).parents('tr')[0];
        if (nEditing !== null) {
            restoreRow(nEditing, idEditing);
        } else {
            var id = $(this).attr("data-id");
            nEditing = nRow;
            idEditing = id;
            var jqTds = $('>td', nRow);
            jqTds[1].innerHTML = '<input id=\"txtDescricao_' + id + '\" type="text" maxlength=\"512\" class="form-control input" value="' + $("#txtDescricao_" + id).text() + '">';
            jqTds[2].innerHTML = '<input id=\"txtRazao_' + id + '\" type="text" maxlength=\"128\" class="form-control input" value="' + $("#txtRazao_" + id).text() + '">';
            jqTds[3].innerHTML = '<input id=\"txtFantasia_' + id + '\" type="text" maxlength=\"128\" class="form-control input" value="' + $("#txtFantasia_" + id).text() + '">';
            jqTds[4].innerHTML = '<input id=\"txtCnpj_' + id + '\" type="text" maxlength=\"18\" class="form-control clsCnpj input" value="' + $("#txtCnpj_" + id).text() + '">';
            jqTds[5].innerHTML = '<input id=\"txtInscricao_' + id + '\" type="text" maxlength=\"25\" class="form-control input" value="' + $("#txtInscricao_" + id).text() + '">';
            jqTds[6].innerHTML = '<input id=\"txtEndereco_' + id + '\" type="text" maxlength=\"256\" class="form-control input" value="' + $("#txtEndereco_" + id).text() + '">';
            jqTds[7].innerHTML = '<input id=\"txtNumero_' + id + '\" type="text" maxlength=\"6\" class="form-control input" value="' + $("#txtNumero_" + id).text() + '">';
            jqTds[8].innerHTML = '<input id=\"txtBairro_' + id + '\" type="text" maxlength=\"20\" class="form-control input" value="' + $("#txtBairro_" + id).text() + '">';
            jqTds[9].innerHTML = '<input id=\"txtCidade_' + id + '\" type="text" maxlength=\"50\" class="form-control input" value="' + $("#txtCidade_" + id).text() + '">';
            jqTds[10].innerHTML = '<input id=\"txtEstado_' + id + '\" type="text" maxlength=\"2\" class="form-control input" value="' + $("#txtEstado_" + id).text() + '">';
            jqTds[11].innerHTML = '<input id=\"txtCep_' + id + '\" type="text" maxlength=\"9\" class="form-control input" value="' + $("#txtCep_" + id).text() + '">';
            jqTds[12].innerHTML = '<input id=\"txtTelefone_' + id + '\" type="text" maxlength=\"20\" class="form-control input" value="' + $("#txtTelefone_" + id).text() + '">';
            jqTds[13].innerHTML = '<input id=\"txtSite_' + id + '\" type="text" maxlength=\"100\" class="form-control input" value="' + $("#txtSite_" + id).text() + '">';
            jqTds[14].innerHTML = '<center><a href="javascript:;" data-id="' + id + '" class="btn green-haze save" title="Salvar" style=\"padding: 0px 4px;background:#5bb75b;\"><span class="ico-checkmark"></span></a> \n\
                                  <a href="javascript:;" data-id="' + id + '" class="btn red-haze cancel" title="Cancelar" style=\"padding: 0px 4px;background:red;\"><span class="ico-reply"></span></a> <center>';
        }
        $(".clsCnpj").mask("99.999.999/9999-99");
    });

    tblFiliais.on('click', '.cancel', function (e) {
        var id = $(this).attr("data-id");
        var nRow = $(this).parents('tr')[0];
        restoreRow(nRow, id);
    });

    tblFiliais.on('click', '.save', function (e) {
        var id = $(this).attr("data-id");
        var nRow = $(this).parents('tr')[0];
        if (validate_cnpj($("#txtCnpj_" + id).val())) {
            bootbox.confirm('Confirma o alteração do registro?', function (result) {
                if (result === true) {
                    var dados = {
                        "AtualizaFilial": id,
                        "txtDescricao": $("#txtDescricao_" + id).val(),
                        "txtRazao": $("#txtRazao_" + id).val(),
                        "txtFantasia": $("#txtFantasia_" + id).val(),
                        "txtCnpj": $("#txtCnpj_" + id).val(),
                        "txtInscricao": $("#txtInscricao_" + id).val(),
                        "txtEndereco": $("#txtEndereco_" + id).val(),
                        "txtNumero": $("#txtNumero_" + id).val(),
                        "txtBairro": $("#txtBairro_" + id).val(),
                        "txtCidade": $("#txtCidade_" + id).val(),
                        "txtEstado": $("#txtEstado_" + id).val(),
                        "txtCep": $("#txtCep_" + id).val(),
                        "txtTelefone": $("#txtTelefone_" + id).val(),
                        "txtSite": $("#txtSite_" + id).val()
                    };
                    var request = $.ajax({
                        type: 'POST',
                        url: 'FormConfiguracao_server_processing.php',
                        data: dados,
                        dataType: 'json'
                    });
                    request.done(function (data) {
                        if (data.trim() === "YES") {
                            restoreRow(nRow, id);
                        }
                    });
                    request.fail(function (data, textStatus) {
                        if (data.responseText === "YES") {
                            restoreRow(nRow, id);
                        }
                    });
                }
            });
        } else {
            bootbox.alert("Campo CNPJ inválido!");
            return false;
        }
    });

    function restoreRow(nRow, id) {
        nEditing = null;
        tblFiliais.fnReloadAjax("FormConfiguracao_server_processing.php?listFiliais=S");
    }

// <editor-fold defaultstate="collapsed" desc="NotaFiscal">
    $('#arquivoCertificado').live('change', function () {
        var formData = new FormData();
        formData.append('arquivo', $('#arquivoCertificado')[0].files[0]);
        $.ajax({
            type: "POST",
            url: "./form/ConfiguracaoForm.php?UploadCertificado=S",
            data: formData,
            processData: false, // tell jQuery not to process the data
            contentType: false  // tell jQuery not to set contentType
        }).always(function (data) {
            if (data.trim() !== "NO") {
                $("#nmArquivo").text(data);
                $("#txtNmArquivo").val(data);
                $("#btnUpArquivo").hide();
                $("#lblUpArquivo").show();
                $("#arquivoCertificado").val("");
            } else {
                $("#btnUpArquivo").show();
                $("#lblUpArquivo").hide();
            }
        });
    });

    $("#btnDelArquivo").click(function () {
        $.post("./form/ConfiguracaoForm.php", {RemoveCertificado: 'S', nmArquivo: $("#nmArquivo").text()}).done(function (data) {
            if (data.trim() === "YES") {
                $("#btnUpArquivo").show();
                $("#lblUpArquivo").hide();
                $("#arquivoCertificado").val("");
            } else {
                $("#btnUpArquivo").hide();
                $("#lblUpArquivo").show();
            }
        });
    });

    $("#btnTestar").click(function () {
        $.ajax({
            url: "./form/ConfiguracaoForm.php",
            type: 'post',
            dataType: 'json',
            data: {
                TestaSenha: 'S', nmArquivo: $("#nmArquivo").text(), txtSenhaCert: $("#txtSenhaCert").val()
            },
            success: function (data) {
                var jArray = JSON.parse(JSON.stringify(data));
                for (var key in jArray) {
                    var resp = jArray[key];
                    if (resp['status'] !== "true") {
                        bootbox.alert(resp['msg']);
                    } else {
                        bootbox.alert("Certificado Válido");
                    }
                }
            }
        });
    });
// </editor-fold>
    
    $("#txtForma_Preco").change(function () {
        if ($("#txtForma_Preco").val() === "0") {
            $("#txtMargem_lucro").val(numberFormat(0));
            $("#txtMargem_lucro").attr("disabled", true);
        } else {
            $("#txtMargem_lucro").attr("disabled", false);
        }
    });
    
    $("#txtSegPlacaTp").change(function () {
        if ($("#txtSegPlacaTp").val() === "1") {
            $("#txtSegPlacaCod").attr("disabled", false);
        } else {
            $("#txtSegPlacaCod").attr("disabled", true);
        }
    });

    $("#txtTipoVencimento").change(function () {
        $("#txtDiaDeVencimento, #txtDiaAteVencimento, #btnDiaDeVencimento, #tbVencimentos_wrapper, #txtTpVeMin, #txtTpVePr").parent().hide();
        $("#txtDiaVencimento").attr("disabled", false);
        $("#txtDiaVencimento, #txtDiaDeVencimento").val("1");
        $("#txtDiaAteVencimento").val("31");
        if ($("#txtTipoVencimento").val() === "0") {
            $("#txtDiaVencimento").attr("disabled", true);
        } else if ($("#txtTipoVencimento").val() === "2") {
            $("#txtDiaDeVencimento, #txtDiaAteVencimento, #btnDiaDeVencimento, #tbVencimentos_wrapper, #txtTpVeMin, #txtTpVePr").parent().show();
        }        
    });

    $("#txtTipoMulta").change(function () {
        tipoMulta();
    });    
    $("#txtTipoDesconto").change(function () {
        tipoDesconto();
    });
    
    function tipoMulta() {
        $("#txtTaxaInicial").attr("disabled", true);
        $("#txtTaxaInicialDias").attr("disabled", true);
        $("#txtMoraDiaria").attr("disabled", true);
        if ($("#txtTipoMulta").val() === "0") { //SEM MULTA
            $("#txtTaxaInicial").val(numberFormat(0) + "00");
            $("#txtTaxaInicialDias").val(0);
            $("#txtMoraDiaria").val(numberFormat(0) + "00");
        } else if ($("#txtTipoMulta").val() === "1") { //MORA DIARIA
            $("#txtTaxaInicial").val(numberFormat(0) + "00");
            $("#txtTaxaInicialDias").val(0);
            $("#txtMoraDiaria").attr("disabled", false);
        } else if ($("#txtTipoMulta").val() === "2") { //TAXA + MORA
            $("#txtTaxaInicial").attr("disabled", false);
            $("#txtTaxaInicialDias").attr("disabled", false);
            $("#txtMoraDiaria").attr("disabled", false);
        } else if ($("#txtTipoMulta").val() === "3") { //TAXA INICIAL
            $("#txtTaxaInicial").attr("disabled", false);
            $("#txtTaxaInicialDias").attr("disabled", false);
            $("#txtMoraDiaria").val(numberFormat(0) + "00");
        }
    }
    
    function tipoDesconto() {
        $("#txtDescontoDias").attr("disabled", true);
        $("#txtDescontoValor").attr("disabled", true);
        if ($("#txtTipoDesconto").val() === "1") { //DESC. GERAL
            $("#txtDescontoDias").attr("disabled", false);
            $("#txtDescontoValor").attr("disabled", false);
        } else { //SEM DESCONTO, DESC. INDIVIDUAL
            $("#txtDescontoDias").val(0);
            $("#txtDescontoValor").val(numberFormat(0));
        }
    }    
    
    tipoMulta();
    tipoDesconto();    
    $("#lblRefreshWa").html("Veículos para Sincronia: " + ($("#idRefreshWa").val() !== "" ? $("#idRefreshWa").val().split(',').length : 0));
    $("#lblRefreshRa").html("Veículos para Sincronia: " + ($("#idRefreshRa").val() !== "" ? $("#idRefreshRa").val().split(',').length : 0));
});

function validaForm() {
    if ($("#txtDiasRenovacao").val() === "") {
        $("#txtDiasRenovacao").val("0");
    }
    if ($("#txtComandaPersonalizada").is(':checked') && $('#txtComandaMascara').val() === "") {
        bootbox.alert("Máscara obrigatória!");
        return false;
    }
    if ($("#txtTipoVencimento").val() === "1" && (textToNumber($('#txtDiaVencimento').val()) < 1 || textToNumber($('#txtDiaVencimento').val()) > 28)) {
        bootbox.alert("Dia de Vencimento inválido!");
        return false;
    }
    if (!$.isNumeric($("#txtRecorrencia").val()) || textToNumber($("#txtRecorrencia").val()) < 0 || textToNumber($("#txtRecorrencia").val()) > 10) {
        bootbox.alert("Retentativas de DCC, deve ser um valor entre 0 e 10 dias!");
        return false;        
    }
    if (!$("#txtComandaPersonalizada").is(':checked')) {
        $('#txtComandaMascara').val("");
    }
    return true;
}

function trataMask() {
    if ($("#txtComandaPersonalizada").is(':checked')) {
        $("#comandaMasc").show();
    } else {
        $("#comandaMasc").hide();
    }
}

function ajustarDias(de, para) {
    var valor = 0;
    if ($.isNumeric($("#" + de).val())) {
        valor = parseInt($("#" + de).val());
        $("#" + para).val(valor + 1);
    } else {
        $("#" + de).val(0);
    }
    $("#" + para).val(valor + 1);
}

function ativarCampo(id) {
    $("#txtCLivres" + id).attr("readonly", !$("#ckbCLivres" + id).is(':checked'));
}

$("#txtGruposCred").change(function () {
    if ($(this).val()) {
        $.getJSON('./../Contas-a-pagar/conta.ajax.php', {txtGrupo: $(this).val(), ajax: 'true'}, function (j) {
            var options = '<option value="null">Selecione</option>';
            for (var i = 0; i < j.length; i++) {
                options += '<option value="' + j[i].id_contas_movimento + '">' + j[i].descricao + '</option>';
            }
            $('#txtGruposSubCred').html(options);
            $("#txtGruposSubCred").val("null");
        });
    } else {
        $('#txtGruposSubCred').html('<option value="null">Selecione</option>');
    }
});

$("#txtGruposDeb").change(function () {
    if ($(this).val()) {
        $.getJSON('./../Contas-a-pagar/conta.ajax.php', {txtGrupo: $(this).val(), ajax: 'true'}, function (j) {
            var options = '<option value="null">Selecione</option>';
            for (var i = 0; i < j.length; i++) {
                options += '<option value="' + j[i].id_contas_movimento + '">' + j[i].descricao + '</option>';
            }
            $('#txtGruposSubDeb').html(options);
            $("#txtGruposSubDeb").val("null");
        });
    } else {
        $('#txtGruposSubDeb').html('<option value="null">Selecione</option>');
    }
});

function novaLinhaAcrSer() {
    $("#txtIdAcrSer").val("");
    $("#txtServicoAcrSer").val("null");
    $("#txtQtdMinAcrSer").val("1");
    $("#txtQtdMaxAcrSer").val("1");
}

function addLinhaAcrSer() {
    var dados = {
        btnSave: "S",
        txtIdAcrSer: $("#txtIdAcrSer").val(),
        txtServicoAcrSer: $("#txtServicoAcrSer").val(),
        txtQtdMinAcrSer: $("#txtQtdMinAcrSer").val(),
        txtQtdMaxAcrSer: $("#txtQtdMaxAcrSer").val()
    };
    $.post("form/PlanosAcrescimoServicoFormServer.php", dados).done(function (data) {
        if (data.trim() === "YES") {
            novaLinhaAcrSer();
            refreshAcrescimoSer();
        }
    });
}

function finalFindAcrescimoSer() {
    return "form/PlanosAcrescimoServicoFormServer.php?listAcrescimo=" + $("#txtId").val();
}

function BuscarAcrescimoSer(id) {
    $.post("form/PlanosAcrescimoServicoFormServer.php", {getAcrescimo: id}).done(function (json) {
        var data = JSON.parse(json)[0];
        $("#txtIdAcrSer").val(data.id);
        $("#txtServicoAcrSer").val(data.id_convenio);
        $("#txtQtdMinAcrSer").val(data.min_dep);
        $("#txtQtdMaxAcrSer").val(data.max_dep);
    });
}

function RemoverAcrescimoSer(id) {
    bootbox.confirm('Deseja realmente excluir esse acréscimo?', function (result) {
        if (result === true) {
            $.post("form/PlanosAcrescimoServicoFormServer.php", {btnDelete: "S", txtId: id}).done(function (data) {
                if (data.trim() !== "YES") {
                    bootbox.alert("Erro ao excluir registro!");
                } else {
                    refreshAcrescimoSer();
                }
            });
        }
    });
}

$('#tbPlanoAcrescimoSer').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    bPaginate: false, bFilter: false, bInfo: false, bSort: false,
    "sAjaxSource": finalFindAcrescimoSer(),
    'oLanguage': {
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sZeroRecords': "Não foi encontrado nenhum resultado"
    }
});

function refreshAcrescimoSer() {
    var tblAcrescimoSer = $('#tbPlanoAcrescimoSer').dataTable();
    tblAcrescimoSer.fnReloadAjax(finalFindAcrescimoSer());
}

novaLinhaAcrSer();

$("#btnRefreshWa").click(function () {
    $("#source").dialog({modal: true});
    $("#progressbar").progressbar({value: 0});
    $("#pgIni").html("0");
    $("#pgFim").html($("#idRefreshWa").val().split(',').length);
    processar_consulta();
});

function processar_consulta() {
    let PgIni = parseInt($("#pgIni").html());
    let PgFim = parseInt($("#pgFim").html());
    if ((PgIni < PgFim) && PgFim > 0 && $('#source').dialog('isOpen')) {
        const IdChk = $("#idRefreshWa").val().split(',')[PgIni];
        const [idVeiculo, idSinc] = IdChk.split('|');
        const funcao = (idSinc && $.isNumeric(idSinc)) ? 'alterarVeiculo&excluir=S' : 'criarNovoVeiculo';
        $("#progressbar").progressbar({value: ((PgIni * 100) / PgFim)});
        $("#pgIni").html(PgIni + 1);        
        $.ajax({
            type: "POST", url: "./../../Modulos/Seguro/ajax/assistencia.php",
            data: "functionPage="+funcao+"&id_veiculo="+idVeiculo, dataType: "text",
            success: function (data) {
                if (data !== "OK") {
                    console.log("Erro id:" + idVeiculo + ", erro " + data);
                }
                processar_consulta();
            },
            error: function (error) {
                bootbox.alert(error.responseText + "!");                
            }
        });                
    } else {
        $("#source").dialog('close');
    }
}

$("#btnRefreshRa").click(function () {
    $("#source").dialog({modal: true});
    $("#progressbar").progressbar({value: 0});
    $("#pgIni").html("0");
    $("#pgFim").html($("#idRefreshRa").val().split(',').length);
    processar_consultaRa();
});

function processar_consultaRa() {
    let PgIni = parseInt($("#pgIni").html());
    let PgFim = parseInt($("#pgFim").html());
    if ((PgIni < PgFim) && PgFim > 0 && $('#source').dialog('isOpen')) {
        const IdChk = $("#idRefreshRa").val().split(',')[PgIni];
        const [idVeiculo, idSinc, statusExt] = IdChk.split('|');
        const funcao = ((idSinc && $.isNumeric(idSinc) && statusExt === "S") ? 'alterarVeiculo&excluir=S' : 'criarNovoVeiculo');
        $("#progressbar").progressbar({value: ((PgIni * 100) / PgFim)});
        $("#pgIni").html(PgIni + 1);        
        $.ajax({
            type: "POST", url: "./../../Modulos/Seguro/ajax/rastreamento.php",
            data: "functionPage=" + funcao + "&id_veiculo=" + idVeiculo, dataType: "text",
            success: function (data) {
                if (data !== "OK") {
                    console.log("Erro id:" + idVeiculo + ", erro " + data);
                }
                processar_consultaRa();
            },
            error: function (error) {
                bootbox.alert(error.responseText + "!");                
            }
        });                
    } else {
        $("#source").dialog('close');
    }
}

$('#txtBanco').change(function () {
    $("txtCarteira").attr('disabled', true);
    if ($(this).val()) {
        $.getJSON('./../Financeiro/bancoCarteira.ajax.php?search=', {txtBanco: $(this).val(), ajax: 'true'}, function (j) {
            options = '<option value="null">Selecione</option>';
            for (var i = 0; i < j.length; i++) {
                options += '<option value="' + j[i].sf_carteiras_id + '">' + j[i].sf_carteira_descricao + '</option>';
            }
            $('#txtCarteira').html(options).show();
            $("txtCarteira").attr('disabled', false);
        });
    } else {
        $('#txtCarteira').html('<option value="null">Selecione</option>');
        $("txtCarteira").attr('disabled', false);
    }
});

$('#cor_primaria, #cor_secundaria, #cor_destaque').colpick({
    onSubmit:function(hsb,hex,rgb,el,bySetColor) {
        $(el).val('#'+hex);
        $(el).colpickHide();
    }
});

$('#tbDocumentos').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bSort": false,
    "bLengthChange": false,
    "bFilter": false,  
    "sAjaxSource": "../../Modulos/CRM/ajax/Documentos_server_processing.php?id=0&type=Regulamentos",
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

function refreshDoc() {
    let tblCRM = $('#tbDocumentos').dataTable();
    tblCRM.fnReloadAjax("../../Modulos/CRM/ajax/Documentos_server_processing.php?id=0&type=Regulamentos");
}

function AdicionarDoc(e) {
    $("#loader").show();
    var formData = new FormData();
    formData.append("btnSendFile", "S");
    formData.append("txtId", "0");
    formData.append("type", "Regulamentos");
    formData.append("file", $("#file")[0].files[0]); 
    $.ajax({
        type: 'POST',
        url: "../../Modulos/CRM/form/CRMDocumentos.php",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    }).done(function (data) {
        $("#loader").hide();
        if (data === "YES") {
            $('#file').val('');
            refreshDoc();
        } else {
            bootbox.alert(data);
        }
    });
}

function RemoverDoc(filename) {
    var nomedoc = encodeURIComponent(filename.toString());
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/CRM/form/CRMDocumentos.php", "btnDelFile=S&txtId=0&type=Regulamentos&txtFileName=" + nomedoc).done(function (data) {
                if (data === "YES") {
                    refreshDoc();
                } else {
                    bootbox.alert(data + "!");
                }
            });
        } else {
            return;
        }
    });
}

$('#tbCotacao').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bSort": false,
    "bLengthChange": false,
    "bFilter": false,  
    "sAjaxSource": "../../Modulos/CRM/ajax/Documentos_server_processing.php?id=0&type=Cotacao",
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

function refreshLink() {
    let tblCRM = $('#tbCotacao').dataTable();
    tblCRM.fnReloadAjax("../../Modulos/CRM/ajax/Documentos_server_processing.php?id=0&type=Cotacao");
}

function AdicionarLink(e) {
    $("#loader").show();
    var formData = new FormData();
    formData.append("btnSendFile", "S");
    formData.append("txtId", "0");
    formData.append("type", "Cotacao");
    formData.append("nameFile", $("#txtCotacaoTipo").val());
    formData.append("file", $("#fileCotacao")[0].files[0]); 
    $.ajax({
        type: 'POST',
        url: "../../Modulos/CRM/form/CRMDocumentos.php",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    }).done(function (data) {
        $("#loader").hide();
        if (data === "YES") {
            $('#file').val('');
            refreshLink();
        } else {
            bootbox.alert(data);
        }
    });
}

function RemoverLink(filename) {
    var nomedoc = encodeURIComponent(filename.toString());
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/CRM/form/CRMDocumentos.php", "btnDelFile=S&type=Cotacao&txtFileName=" + nomedoc).done(function (data) {
                if (data === "YES") {
                    refreshLink();
                } else {
                    bootbox.alert(data + "!");
                }
            });
        } else {
            return;
        }
    });
}

function AdicionarFotoFilial() {
    if ($("#txtIdFilial").val() !== "null") {
        $("#loader").show();
        var formData;
        formData = new FormData($("#frmEnviaDados")[0]);
        formData.append("btnSendPhoto", "S");
        $.ajax({
            type: 'POST',
            url: "../../Modulos/Comercial/form/ConfiguracaoUploadFoto.php",
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            $("#loader").hide();
            if (data === "YES") {
                carregaLogo();
            } else {
                bootbox.alert(data + "!");
            }
        });
    } else {
        bootbox.alert("Selecione uma Filial!");
    }
}

function carregaLogo() {
    $.getJSON("../../Modulos/Comercial/form/ConfiguracaoFilialLogo.php").done(function (data) {
        $("#loader").hide();
        if (data !== "") {
            var content = "";
            $.each(data, function (i, item) {
                content += "<div style='margin-right: 30px; position: relative'>\n\
                <a href=" + item[0] + " data-width='800' data-height='600' data-fancybox data-caption='This image has a caption'>\n\
                    <img src=" + item[0] + " style='width: 160px;'/>\n\
                </a>" + item[2] + "</div>";
            });
            $(".imglist").html(content);
        } else {
            bootbox.alert(data + "!");
        }
    });
}

function RemoverLogo(filename) {
    var nomedoc = filename.toString();
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/Comercial/form/ConfiguracaoUploadFoto.php", "btnDelFile=S&txtFileName=" + nomedoc).done(function (data) {
                if (data === "YES") {
                    carregaLogo();
                } else {
                    bootbox.alert(data + "!");
                }
            });
        } else {
            return;
        }
    });
}

// <editor-fold defaultstate="collapsed" desc="FotoWhatsapp">

function AdicionarFotoWhatsapp() {
    $("#loader").show();
    var formData;
    formData = new FormData($("#frmEnviaDados")[0]);
    formData.append("btnPhotoWhatsapp", "S");
    $.ajax({
        type: 'POST',
        url: "../../Modulos/Comercial/form/UploadFotoWhatsapp.php",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    }).done(function (data) {
        $("#loader").hide();
        if (data === "YES") {
            carregaLogoWhatsapp();
        } else {
            bootbox.alert(data + "!");
        }
    });
}

function carregaLogoWhatsapp() {
    $.getJSON("../../Modulos/Comercial/form/ConfiguracaoLogoWhatsapp.php").done(function (data) {
        $("#loader").hide();
        if (data !== "") {
            var content = "";
            $.each(data, function (i, item) {
                content += "<div style='margin-right: 30px; position: relative'>\n\
                <a href=" + item[0] + " data-width='800' data-height='600' data-fancybox data-caption='This image has a caption'>\n\
                    <img src=" + item[0] + " style='width: 160px;'/>\n\
                </a>" + item[2] + "</div>";
            });
            $(".imglist2").html(content);
        } else {
            bootbox.alert(data + "!");
        }
    });
}

function RemoverLogoWhatsapp(filename) {
    var nomedoc = filename.toString();
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/Comercial/form/UploadFotoWhatsapp.php", "btnDelFile=S&txtFileName=" + nomedoc).done(function (data) {
                if (data === "YES") {
                    carregaLogoWhatsapp();
                } else {
                    bootbox.alert(data + "!");
                }
            });
        } else {
            return;
        }
    });
}

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="FotoMenu">

function AdicionarFotoMenu() {
    $("#loader").show();
    var formData;
    formData = new FormData($("#frmEnviaDados")[0]);
    formData.append("btnFotoMenu", "S");
    $.ajax({
        type: 'POST',
        url: "../../Modulos/Comercial/form/UploadFotoMenu.php",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    }).done(function (data) {
        $("#loader").hide();
        if (data === "YES") {
            carregaLogoMenu();
        } else {
            bootbox.alert(data + "!");
        }
    });
}

function carregaLogoMenu() {
    $.getJSON("../../Modulos/Comercial/form/ConfiguracaoLogoMenu.php").done(function (data) {
        $("#loader").hide();
        if (data !== "") {
            var content = "";
            $.each(data, function (i, item) {
                content += "<div style='margin-right: 30px; position: relative'>\n\
                <a href=" + item[0] + " data-width='800' data-height='600' data-fancybox data-caption='This image has a caption'>\n\
                    <img src=" + item[0] + " style='width: 160px;'/>\n\
                </a>" + item[2] + "</div>";
            });
            $(".imglist3").html(content);
        } else {
            bootbox.alert(data + "!");
        }
    });
}

function RemoverLogoMenu(filename) {
    var nomedoc = filename.toString();
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/Comercial/form/UploadFotoMenu.php", "btnDelFile=S&txtFileName=" + nomedoc).done(function (data) {
                if (data === "YES") {
                    carregaLogoMenu();
                } else {
                    bootbox.alert(data + "!");
                }
            });
        } else {
            return;
        }
    });
}

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="FotoContratacao">

function AdicionarFotoContratacao() {
    $("#loader").show();
    var formData;
    formData = new FormData($("#frmEnviaDados")[0]);
    formData.append("btnFotoContratacao", "S");
    $.ajax({
        type: 'POST',
        url: "../../Modulos/Comercial/form/UploadFotoContratacao.php",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    }).done(function (data) {
        $("#loader").hide();
        if (data === "YES") {
            carregaFotoContratacao();
        } else {
            bootbox.alert(data + "!");
        }
    });
}

function carregaFotoContratacao() {
    $.getJSON("../../Modulos/Comercial/form/ConfiguracaoLogoContratacao.php").done(function (data) {
        $("#loader").hide();
        if (data !== "") {
            var content = "";
            $.each(data, function (i, item) {
                content += "<div style='margin-right: 30px; position: relative'>\n\
                <a href=" + item[0] + " data-width='800' data-height='600' data-fancybox data-caption='This image has a caption'>\n\
                    <img src=" + item[0] + " style='width: 160px;'/>\n\
                </a>" + item[2] + "</div>";
            });
            $(".imglist4").html(content);
        } else {
            bootbox.alert(data + "!");
        }
    });
}

function RemoverFotoContratacao(filename) {
    var nomedoc = filename.toString();
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/Comercial/form/UploadFotoContratacao.php", "btnDelFile=S&txtFileName=" + nomedoc).done(function (data) {
                if (data === "YES") {
                    carregaFotoContratacao();
                } else {
                    bootbox.alert(data + "!");
                }
            });
        } else {
            return;
        }
    });
}

// </editor-fold>

$('#tbVencimentos').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bSort": false,
    "bLengthChange": false,
    "bFilter": false,  
    "sAjaxSource": "../../Modulos/CRM/ajax/Vencimentos_server_processing.php",
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

function refreshVencimentos() {
    let tblCRM = $('#tbVencimentos').dataTable();
    tblCRM.fnReloadAjax("../../Modulos/CRM/ajax/Vencimentos_server_processing.php");
}

function AdicionarVencimentos() {
    $("#loader").show();
    var formData = new FormData();
    formData.append("bntSave", "S");
    formData.append("txtDiaVencimento", $("#txtDiaVencimento").val());
    formData.append("txtDiaDeVencimento", $("#txtDiaDeVencimento").val());
    formData.append("txtDiaAteVencimento", $("#txtDiaAteVencimento").val());
    formData.append("txtTpVeMin", $("#txtTpVeMin").val());
    formData.append("txtTpVePr", $("#txtTpVePr").val());
    $.ajax({
        type: 'POST',
        url: "../../Modulos/CRM/form/FormVencimentos.php",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    }).done(function (data) {
        $("#loader").hide();
        if (data === "YES") {
            $("#txtDiaVencimento").val("1");
            $("#txtDiaDeVencimento").val("1");
            $("#txtDiaAteVencimento").val("31");
            $("#txtTpVeMin").val("1");
            $("#txtTpVePr").val("1");
            refreshVencimentos();
        } else {
            bootbox.alert(data);
        }
    });
}

function RemoverVencimentos(filename) {
    var nomedoc = filename.toString();
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/CRM/form/FormVencimentos.php", "btnDel=S&txtId=" + nomedoc).done(function (data) {
                if (data === "YES") {
                    refreshVencimentos();
                } else {
                    bootbox.alert(data + "!");
                }
            });
        } else {
            return;
        }
    });
}

$('#tbTermos').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bSort": false,
    "bLengthChange": false,
    "bFilter": false,
    "sAjaxSource": "../../Modulos/Comercial/ajax/Termos_server_processing.php",
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

function refreshTermos() {
    let tblTermos = $('#tbTermos').dataTable();
    tblTermos.fnReloadAjax("../../Modulos/Comercial/ajax/Termos_server_processing.php");
}

function AbrirTermo(id) {
    abrirTelaBox("TermosForm.php" + (id > 0 ? "?id=" + id : ""), 450, 770);
}

function FecharTermos() {
    $("#newbox").remove();
    refreshTermos();
}

function RemoverTermo(id) {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/Comercial/form/TermosFormServer.php", "bntDelete=S&List=S&txtId=" + id).done(function (data) {
                if (data === "YES") {
                    refreshTermos();
                } else {
                    bootbox.alert(data + "!");
                }
            });
        } else {
            return;
        }
    });
}

$('#tbCarteira').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bSort": false,
    "bLengthChange": false,
    "bFilter": false,
    "sAjaxSource": "../../Modulos/Comercial/ajax/Carteira_server_processing.php",
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

function refreshCarteira() {
    let tblCarteira = $('#tbCarteira').dataTable();
    tblCarteira.fnReloadAjax("../../Modulos/Comercial/ajax/Carteira_server_processing.php");
}

function AbrirCarteira(id) {
    abrirTelaBox("CarteiraForm.php" + (id > 0 ? "?id=" + id : ""), 650, 1130);
}

function FecharCarteira() {
    $("#newbox").remove();
    refreshCarteira();
}

function RemoverCarteira(id) {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/Comercial/form/CarteiraFormServer.php", "bntDelete=S&List=S&txtId=" + id).done(function (data) {
                if (data === "YES") {
                    refreshCarteira();
                } else {
                    bootbox.alert(data + "!");
                }
            });
        } else {
            return;
        }
    });
}

$('#tbAssinatura').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bSort": false,
    "bLengthChange": false,
    "bFilter": false,
    "sAjaxSource": "../../Modulos/Academia/form/ClientesPlanosServer.php?listAssinaturasResp=S",
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

function refreshAssinatura() {
    let tblAssinatura = $('#tbAssinatura').dataTable();
    tblAssinatura.fnReloadAjax("../../Modulos/Academia/form/ClientesPlanosServer.php?listAssinaturasResp=S");
}

function AbrirAssinatura(id) {
    window.open("../../Modulos/Seguro/FormAssinatura.php?id=" + id + "&idContrato=" + id + "&crt=" + $("#txtMnContrato").val() + "&idCliente=" + id, '_blank');
}

function RemoverAssinatura(id) {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/Academia/form/ClientesPlanosServer.php", "btnDelAssResp=S&List=S&txtId=" + id).done(function (data) {
                if (data === "YES") {
                    refreshAssinatura();
                } else {
                    bootbox.alert(data + "!");
                }
            });
        } else {
            return;
        }
    });
}

// <editor-fold defaultstate="collapsed" desc="WhiteList">

$('#tbAddIp').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bSort": false,
    "bLengthChange": false,
    "bFilter": false,
    "sAjaxSource": "../../Modulos/Comercial/form/UserIpServer.php?list=S",
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

function refreshIp() {
    let tblAssinatura = $('#tbAddIp').dataTable();
    tblAssinatura.fnReloadAjax("../../Modulos/Comercial/form/UserIpServer.php?list=S");
}

function AddIp() {
    $.post("../../Modulos/Comercial/form/UserIpServer.php", 
        "btnSalvar=S" +
        "&txtIp=" + $("#txtIp").val() + 
        "&txtIpUser=" + $("#txtIpUser").val() + 
        "&txtIpDescricao=" + $("#txtIpDescricao").val()).done(function (data) {
        if (data === "YES") {
            $("#txtIp").val('');
            $("#txtIpDescricao").val('');
            $("#txtIpUser").select2("val", "null");
            refreshIp();
        } else {
            bootbox.alert(data + "!");
        }
    });
}

function RemoverIp(id) {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/Comercial/form/UserIpServer.php", "btnExcluir=S&txtIp=" + id).done(function (data) {
                if (data === "YES") {
                    refreshIp();
                } else {
                    bootbox.alert(data + "!");
                }
            });
        } else {
            return;
        }
    });
}

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="BlackList">

$('#tbAddIpBlack').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bSort": false,
    "bLengthChange": false,
    "bFilter": false,
    "sAjaxSource": "../../Modulos/Comercial/form/UserIpBlackServer.php?list=S",
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

function refreshIpBlack() {
    let tblAssinatura = $('#tbAddIpBlack').dataTable();
    tblAssinatura.fnReloadAjax("../../Modulos/Comercial/form/UserIpBlackServer.php?list=S");
}

function AddIpBlack() {
    $.post("../../Modulos/Comercial/form/UserIpBlackServer.php", 
        "btnSalvar=S&txtIp=" + $("#txtIpBlack").val() + 
        "&txtIpDescricao=" + $("#txtIpBlackDescricao").val()).done(function (data) {
        if (data === "YES") {
            $("#txtIpBlack").val('');
            $("#txtIpBlackDescricao").val('');
            refreshIpBlack();
        } else {
            bootbox.alert(data + "!");
        }
    });
}

function RemoverIpBlack(id) {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/Comercial/form/UserIpBlackServer.php", "btnExcluir=S&txtIp=" + id).done(function (data) {
                if (data === "YES") {
                    refreshIpBlack();
                } else {
                    bootbox.alert(data + "!");
                }
            });
        } else {
            return;
        }
    });
}

// </editor-fold>

$('#tbUserProspects').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bSort": false,
    "bLengthChange": false,
    "bFilter": false,
    "sAjaxSource": "../../Modulos/Comercial/form/UserProspectsServer.php?list=S",
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

function refreshUserProspects() {
    let tblAssinatura = $('#tbUserProspects').dataTable();
    tblAssinatura.fnReloadAjax("../../Modulos/Comercial/form/UserProspectsServer.php?list=S");
}

function AddUsuario() {
    $.post("../../Modulos/Comercial/form/UserProspectsServer.php", 
        "btnSalvar=S&txtId=" + $("#txtUsuario").val()).done(function (data) {
        if (data === "YES") {
            refreshUserProspects();
        } else {
            bootbox.alert(data + "!");
        }
    });
}

function RemoverUserProspects(id) {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/Comercial/form/UserProspectsServer.php", "btnExcluir=S&txtId=" + id).done(function (data) {
                if (data === "YES") {
                    refreshUserProspects();
                } else {
                    bootbox.alert(data + "!");
                }
            });
        } else {
            return;
        }
    });
}

function Dessincronizar() {
    bootbox.confirm('Confirma a ação de dessincronizar todos os veículos com a assistência?', function (result) {
        if (result === true) {
            $.post("../../Modulos/Comercial/form/UserProspectsServer.php", "btnDessinc=S").done(function (data) {
                if (data === "YES") {
                    location.reload();
                } else {
                    bootbox.alert(data + "!");
                }
            });
        } else {
            return;
        }
    });
}

function DessincronizarRa() {
    bootbox.confirm('Confirma a ação de dessincronizar todos os veículos com rastreamento?', function (result) {
        if (result === true) {
            $.post("../../Modulos/Comercial/form/UserProspectsServer.php", "btnDessincRa=S").done(function (data) {
                if (data === "YES") {
                    location.reload();
                } else {
                    bootbox.alert(data + "!");
                }
            });
        } else {
            return;
        }
    });
}