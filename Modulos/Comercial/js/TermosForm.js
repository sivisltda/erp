$(document).ready(function () {
    CKEDITOR.replace('ckeditor', {removePlugins: 'elementspath', height: 140, width: 725});
});

function validaForm() {
    if (CKEDITOR.instances['ckeditor'].getData() === "") {
        bootbox.alert("Termo é Obrigatório!");
        return false;
    }
    return true;
}

function variable(e) {
    CKEDITOR.instances['ckeditor'].insertText(e);
}