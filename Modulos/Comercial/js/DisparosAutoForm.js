
$('#txtTolerancia').mask("##", {reverse: true});

$('#txtTipo').change(function () {
    $.getJSON("form/UsuariosFormServer.php", {listUsuarios: 'S', envioEmail: ($(this).val() === "E" ? "S" : "N")}, function (j) {
        var options = "<option value=\"null\">Selecione</option>";
        if (j !== null && j.length > 0) {
            for (var i = 0; i < j.length; i++) {
                options = options + "<option value=\"" + j[i].id_usuario + "\">" + j[i].nome + "</option>";
            }
        }
        $("#txtResponsavel").html(options);
        $("#txtResponsavel").select2("val", "null");
    });
    if ($(this).val() === "L") {
        $("#txtAssunto, #txtDescricao").parent().show();
        $("#txtModelo").parent().hide();
    } else {
        $("#txtAssunto, #txtDescricao").parent().hide();
        $("#txtModelo").parent().show();
        $.getJSON("../CRM/form/EmailsFormServer.php", {listModelosEmails: 'S', tipo: ($(this).val() === "E" ? "0" : ($(this).val() === "S" ? "1" : ($(this).val() === "W" ? "2" : "3")))}, function (j) {
            var options = "<option value=\"null\">Selecione</option>";
            if (j !== null && j.length > 0) {
                for (var i = 0; i < j.length; i++) {
                    options = options + "<option value=\"" + j[i].id_email + "\">" + j[i].descricao + "</option>";
                }
            }
            $("#txtModelo").html(options);
            $("#txtModelo").select2("val", "null");
        });
    }
});

$("#btnExcluir").click(function () {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("form/DisparosAutoFormServer.php", "excluirDisparo=" + $("#txtId").val()).done(function (data) {
                if (data.trim() === "YES") {
                    bootbox.alert('Registro Excluido com Sucesso!.', function () {
                        parent.FecharBox(1);
                    });
                } else {
                    bootbox.alert("Erro ao Excluir");
                }
            });
        }
    });
});

function validaForm() {

    if ($("#txtRegra").val() === "null" || $("#txtResponsavel").val() === "null" || $("#txtDias").val() === "") {
        bootbox.alert("Preencha os campos Obrigátorios!");
        return false;
    }

    if ($("#txtTipo").val() !== "L" && $("#txtModelo").val() === "null") {
        bootbox.alert("Modelos de Disparos é Obrigatório!");
        return false;
    }

    if ($("#txtTipo").val() === "L" && ($("#txtAssunto").val() === "" || $("#txtDescricao").val() === "")) {
        bootbox.alert("Assunto e Descrição são Obrigatórios!");
        return false;
    }
    
    return true;
}