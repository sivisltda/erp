var tbLista = $("#example").dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": finalFind(),
    "bFilter": false,
    "oLanguage": {
        "oPaginate": {
            "sFirst": "Primeiro",
            "sLast": "Último",
            "sNext": "Próximo",
            "sPrevious": "Anterior"
        }, "sEmptyTable": "Não foi encontrado nenhum resultado",
        "sInfo": "Visualização do registro _START_ ao _END_ de _TOTAL_",
        "sInfoEmpty": "Visualização do registro 0 ao 0 de 0",
        "sInfoFiltered": "(filtrado de _MAX_ registros totais)",
        "sLengthMenu": "Visualização de _MENU_ registros",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sSearch": "Pesquisar:",
        "sZeroRecords": "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

$(document).ready(function () {
    $("#btnPesquisar").click(function(){
        tbLista.fnReloadAjax(finalFind());
    });
});

function AbrirBox(id){
    var myId ="";
    if(id > 0){ myId = "?id="+id;}
    abrirTelaBox("DisparosAutoForm.php"+myId,370,550);
}

function FecharBox(opc){
    $("#newbox").remove();
    if(opc === 1){
        tbLista.fnReloadAjax(finalFind());
    }
}

function finalFind() {
    return "ajax/DisparosAuto_server_processing.php?Search="+$("#txtBuscar").val();
}

function RemoverItem(idDisparo){
    bootbox.confirm('Confirma a exclusão do registro?', function(result) {
        if (result === true) {
            $.post( "form/DisparosAutoFormServer.php", "excluirDisparo="+idDisparo).done(function( data ) {
                if(data.trim() === "YES"){
                    bootbox.alert('Registro Excluido com Sucesso!.', function() {
                        tbLista.fnReloadAjax(finalFind());
                    });
                }else{
                    bootbox.alert("Erro ao Excluir");
                }
            });
        }
    });
}