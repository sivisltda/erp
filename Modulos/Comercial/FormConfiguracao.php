<?php include "form/ConfiguracaoForm.php"; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/colpick.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>      
        <style>
            .inputDireito {
                text-align:right;
            }
            #formulario {
                float: left;
                width: 100%;
            }
            .linha {
                float: left;
                width: 100%;
                display: block;
                padding-top: 10px;
                padding-bottom: 10px;                       
            }            
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1 style="margin-top:8px">Configuração</h1>
                    </div>
                    <div class="row-fluid">
                        <form action="FormConfiguracao.php" method="POST" name="frmEnviaDados" id="frmEnviaDados">
                            <div class="span12">
                                <div class="block">
                                    <div id="formulario">
                                        <div class="data-fluid tabbable">
                                            <ul class="nav nav-tabs" style="margin-bottom:0">
                                                <?php if ($_SESSION["mod_emp"] != 1) { ?>
                                                    <li class="active"><a href="#tab1" data-toggle="tab"><b>Principal</b></a></li>
                                                    <li><a href="#tab2" data-toggle="tab"><b>Filial</b></a></li>
                                                <?php } if ($mdl_aca_ == 1 && $_SESSION["mod_emp"] != 1) { ?>
                                                    <li><a href="#tab3" data-toggle="tab"><b>Tolerâncias</b></a></li>
                                                <?php } if ($mdl_clb_ == 1 && $_SESSION["mod_emp"] != 1) { ?>
                                                    <li><a href="#tab4" data-toggle="tab"><b>Clube</b></a></li>
                                                <?php } ?>
                                                    <li <?php echo ($_SESSION["mod_emp"] == 1 ? 'class="active"' : ''); ?>><a href="#tab5" data-toggle="tab"><b>Nota Fiscal</b></a></li>
                                                    <li><a href="#tab6" data-toggle="tab"><b>Portal</b></a></li>                                                    
                                                    <li><a href="#tab7" data-toggle="tab"><b>Online</b></a></li>                                                    
                                                    <li><a href="#tab8" data-toggle="tab"><b>Gerais</b></a></li>                                                    
                                                    <li><a href="#tab9" data-toggle="tab"><b>Campos Livres</b></a></li>
                                            </ul>
                                        </div>
                                        <div class="tab-content" style="min-height: 380px;border:1px solid #ddd; border-top:0; background:#F6F6F6">
                                            <div class="tab-pane <?php echo ($_SESSION["mod_emp"] != 1 ? 'active' : ''); ?>" id="tab1" style="margin-bottom:5px">
                                                <div style="margin-top:5px;width: 25%;float: left">
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Terminal de Vendas</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtComandaObrigatoria" id="txtComandaObrigatoria" value="1" type="checkbox" <?php
                                                                    if ($CheckComandaObr == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Comanda Obrigatória</div>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtClienteObrigatorio" id="txtClienteObrigatorio" value="1" type="checkbox" <?php
                                                                    if ($CheckClienteObr == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Cliente Obrigatório</div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtComissionadoObrigatorio" id="txtComissionadoObrigatorio" value="1" type="checkbox" <?php
                                                                    if ($CheckComissionadoObr == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Comissionado Obrigatório</div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table style="margin-top:10px">
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Configurações de Comanda (Terminal de Vendas)</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtComandaUnicaGerar" id="txtComandaUnicaGerar" value="1" type="checkbox" <?php
                                                                    if ($CheckComandaUniGerar == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Gerar Comanda Única</div>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtComandaUnica" id="txtComandaUnica" value="1" type="checkbox" <?php
                                                                    if ($CheckComandaUni == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Validar Comanda Única</div>                                                                
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtComandaPersonalizada" id="txtComandaPersonalizada" value="1" type="checkbox" <?php
                                                                    if ($CheckComandaPers == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Comanda Personalizada</div>
                                                                <div style="margin-top:27px" hidden id="comandaMasc">
                                                                    <label>Máscara <i>(S para letras e 0 para números)</i></label>
                                                                    <input type="text" id="txtComandaMascara" name="txtComandaMascara" maxlength="10" style="text-transform:uppercase" value="<?php echo $txtComandaMascara; ?>"/>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table style="margin-top:10px">
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Outras Configurações (Terminal de Vendas)</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtIndicadorComanda" id="txtIndicadorComanda" value="1" type="checkbox" <?php
                                                                    if ($CheckIndicador == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Exibir opção para informar Indicador</div>
                                                            </td>                                                           
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtFiltroComissionado" id="txtFiltroComissionado" value="1" type="checkbox" <?php
                                                                    if ($CheckFiltroComissionado == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Filtrar comissionado por empresa</div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtQuantidadeDecimal" id="txtQuantidadeDecimal" value="1" type="checkbox" <?php
                                                                    if ($QuantidadeDecimal == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Valor Decimal (Quantidade) </div>
                                                            </td>
                                                        </tr>                                                    
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Funcionários</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label>Cadastrar usuário com perfil:</label>
                                                                <select id="txtFuncUsuario" name="txtFuncUsuario" style="width: 300px">
                                                                    <option value="null">Selecione</option>
                                                                    <?php $sql = "select id_permissoes,nome_permissao from sf_usuarios_permissoes";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                        <option value="<?php echo $RFP["id_permissoes"] ?>" <?php
                                                                        if (!(strcmp($RFP['id_permissoes'], $txtFuncUsuario))) {
                                                                            echo "SELECTED";
                                                                        } ?>><?php echo utf8_encode($RFP["nome_permissao"]) ?></option>
                                                                    <?php } ?>
                                                                </select>                                                                 
                                                            </td>
                                                        </tr>                                                        
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Clientes</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label>Ordenar por:</label>
                                                                <select id="txtCliOrdem" name="txtCliOrdem" style="width: 300px">
                                                                    <option value="0" <?php echo ($txtCliOrdem == "0" ? "SELECTED" : "");?>>Matrícula Decrescente</option>                                                                    
                                                                    <option value="1" <?php echo ($txtCliOrdem == "1" ? "SELECTED" : "");?>>Nome Cliente</option>                                                                    
                                                                    <option value="2" <?php echo ($txtCliOrdem == "2" ? "SELECTED" : "");?>>Titular/Dependentes</option>                                                                    
                                                                </select>                                                                 
                                                            </td>
                                                        </tr>                                                        
                                                    </table>                                                        
                                                </div>
                                                <div style="margin-top:5px;width: 25%;float: left;">
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Compras: Código Produto</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtCompraCodProd" id="txtCompraCodProd" value="1" type="checkbox" <?php
                                                                    if ($CheckCompraCodProd == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Exibir código do produto na busca </div>
                                                            </td>
                                                        </tr>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Compras: Produto Novos (Importar Dados de XML)</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span style="width: 50%">Controle de Preço:</span><br>
                                                                <select name="txtForma_Preco" id="txtForma_Preco" style="width: 196px">
                                                                    <option value="0" <?php
                                                                    if ($formaPreco == 0) {
                                                                        echo "SELECTED";
                                                                    }
                                                                    ?>>Por valor Fixo</option>
                                                                    <option value="1" <?php
                                                                    if ($formaPreco == 1) {
                                                                        echo "SELECTED";
                                                                    }
                                                                    ?>>Por margem de Lucro</option>
                                                                </select>
                                                                <input style="margin-left: 1%;width: 27%;text-align:right" name="txtMargem_lucro" id="txtMargem_lucro" type="text" class="input-medium" maxlength="100" <?php
                                                                if ($formaPreco == 0) {
                                                                    echo "disabled";
                                                                }
                                                                ?> value="<?php echo $margem_lucro; ?>"/>
                                                            </td>
                                                        </tr>
                                                    </table>                                                    
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Vendas: RECEITAS BRUTA (Subgrupos de Contas)</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label>Venda exclusiva de Produto:</label>
                                                                <select id="txtVendaSubProd" name="txtVendaSubProd" style="width: 300px">
                                                                    <option value="null">Selecione</option>
                                                                    <?php 
                                                                    $sql = "select id_contas_movimento, descricao from sf_contas_movimento where inativa = 0 and grupo_conta = 14 order by descricao";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                        <option value="<?php echo $RFP["id_contas_movimento"] ?>" <?php
                                                                        if (!(strcmp($RFP['id_contas_movimento'], $txtVendaSubProd))) {
                                                                            echo "SELECTED";
                                                                        } ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                                                    <?php }?>
                                                                </select>                                                                 
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label>Venda exclusiva de Serviço:</label>
                                                                <select id="txtVendaSubServ" name="txtVendaSubServ" style="width: 300px">
                                                                    <option value="null">Selecione</option>
                                                                    <?php 
                                                                    $sql = "select id_contas_movimento, descricao from sf_contas_movimento where inativa = 0 and grupo_conta = 14 order by descricao";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                        <option value="<?php echo $RFP["id_contas_movimento"] ?>" <?php
                                                                        if (!(strcmp($RFP['id_contas_movimento'], $txtVendaSubServ))) {
                                                                            echo "SELECTED";
                                                                        } ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                                                    <?php }?>
                                                                </select>                                                                 
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label>Venda exclusiva de Plano:</label>
                                                                <select id="txtVendaSubPlan" name="txtVendaSubPlan" style="width: 300px">
                                                                    <option value="null">Selecione</option>
                                                                    <?php 
                                                                    $sql = "select id_contas_movimento, descricao from sf_contas_movimento where inativa = 0 and grupo_conta = 14 order by descricao";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                        <option value="<?php echo $RFP["id_contas_movimento"] ?>" <?php
                                                                        if (!(strcmp($RFP['id_contas_movimento'], $txtVendaSubPlan))) {
                                                                            echo "SELECTED";
                                                                        } ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                                                    <?php }?>
                                                                </select>                                                                 
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label>Outras formas de Vendas:</label>
                                                                <select id="txtVendaSubOut" name="txtVendaSubOut" style="width: 300px">
                                                                    <option value="null">Selecione</option>
                                                                    <?php 
                                                                    $sql = "select id_contas_movimento, descricao from sf_contas_movimento where inativa = 0 and grupo_conta = 14 order by descricao";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                        <option value="<?php echo $RFP["id_contas_movimento"] ?>" <?php
                                                                        if (!(strcmp($RFP['id_contas_movimento'], $txtVendaSubOut))) {
                                                                            echo "SELECTED";
                                                                        } ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                                                    <?php }?>
                                                                </select>                                                                 
                                                            </td>
                                                        </tr>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Lançamento de Vendas</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label>Filial:</label>
                                                                <select name="txtIdFilialVenda" id="txtIdFilialVenda" style="width: 300px">
                                                                    <option value="null">Variável</option>
                                                                    <?php $sql = "select id_filial, numero_filial from sf_filiais where ativo = 0";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());                                                                    
                                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                        <option value="<?php echo $RFP["id_filial"] ?>" <?php
                                                                        if (!(strcmp($RFP['id_filial'], $txtIdFilialVenda))) {
                                                                            echo "SELECTED";
                                                                        } ?>><?php echo utf8_encode($RFP["numero_filial"]) ?></option>
                                                                    <?php }?>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div style="margin-top:5px;width: 25%;float: left;">
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Lançamentos do Caixa (Contas Variáveis)</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label>Grupos de Contas (Entrada):</label>
                                                                <select id="txtGruposCred" name="txtGruposCred" style="width: 300px">
                                                                    <option value="null">Selecione</option>
                                                                    <?php $sql = "select id_grupo_contas, descricao from sf_grupo_contas where deb_cre = 'C' and inativo = 0 order by descricao";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                        <option value="<?php echo $RFP["id_grupo_contas"] ?>" <?php
                                                                        if (!(strcmp($RFP['id_grupo_contas'], $txtGruposCred))) {
                                                                            echo "SELECTED";
                                                                        } ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                                                    <?php } ?>
                                                                </select>                                                                 
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label>Subgrupos de Contas (Entrada):</label>
                                                                <select id="txtGruposSubCred" name="txtGruposSubCred" style="width: 300px">
                                                                    <option value="null">Selecione</option>
                                                                    <?php 
                                                                    if (is_numeric($txtGruposCred)) {
                                                                    $sql = "select id_contas_movimento, descricao from sf_contas_movimento where inativa = 0 and grupo_conta = " . $txtGruposCred . " order by descricao";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                        <option value="<?php echo $RFP["id_contas_movimento"] ?>" <?php
                                                                        if (!(strcmp($RFP['id_contas_movimento'], $txtSubGruposCred))) {
                                                                            echo "SELECTED";
                                                                        } ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                                                    <?php }}?>
                                                                </select>                                                                
                                                            </td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>
                                                                <label>Grupos de Contas (Saída):</label>
                                                                <select id="txtGruposDeb" name="txtGruposDeb" style="width: 300px">
                                                                    <option value="null">Selecione</option>
                                                                    <?php $sql = "select id_grupo_contas, descricao from sf_grupo_contas where deb_cre = 'D' and inativo = 0 order by descricao";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                        <option value="<?php echo $RFP["id_grupo_contas"] ?>" <?php
                                                                        if (!(strcmp($RFP['id_grupo_contas'], $txtGruposDeb))) {
                                                                            echo "SELECTED";
                                                                        } ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                                                    <?php } ?>
                                                                </select>                                                                 
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label>Subgrupos de Contas (Saída):</label>
                                                                <select id="txtGruposSubDeb" name="txtGruposSubDeb" style="width: 300px">
                                                                    <option value="null">Selecione</option>
                                                                    <?php 
                                                                    if (is_numeric($txtGruposDeb)) {
                                                                    $sql = "select id_contas_movimento, descricao from sf_contas_movimento where inativa = 0 and grupo_conta = " . $txtGruposDeb . " and inativa = 0 order by descricao";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                        <option value="<?php echo $RFP["id_contas_movimento"] ?>" <?php
                                                                        if (!(strcmp($RFP['id_contas_movimento'], $txtSubGruposDeb))) {
                                                                            echo "SELECTED";
                                                                        } ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                                                    <?php }}?>
                                                                </select>                                                                
                                                            </td>
                                                        </tr>                                                        
                                                    </table>
                                                    <div style="clear:both;"></div>                                                     
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Documento</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div>
                                                                    <label>Adicionar documento <i>no ato do cadastro do cliente</i></label>                                                        
                                                                </div>                                                                                                                    
                                                                <select name="txtDocNovoAluno" id="txtDocNovoAluno" style="width: 300px">
                                                                    <option value="null">Selecione</option>
                                                                    <?php $cur = odbc_exec($con, "select id_doc, descricao_doc from sf_documentos") or die(odbc_errormsg());
                                                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                        <option value="<?php echo $RFP['id_doc'] ?>"<?php
                                                                        if (!(strcmp($RFP['id_doc'], $txtDocNovoAluno))) {
                                                                            echo "SELECTED";
                                                                        }
                                                                        ?>><?php echo utf8_encode($RFP['descricao_doc']) ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </td>                                                        
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label style="margin-top: 6px;">Página Inicial (Usuário Administrador)</label> 
                                                                <select name="txt_out_pag_avancada" id="txt_out_pag_avancada">
                                                                    <option value="0" <?php echo ($txt_out_pag_avancada == "0" ? "SELECTED" : ""); ?>>PÁGINA BÁSICA</option>
                                                                    <option value="1" <?php echo ($txt_out_pag_avancada == "1" ? "SELECTED" : ""); ?>>PÁGINA AVANÇADA</option>
                                                                    <option value="2" <?php echo ($txt_out_pag_avancada == "2" ? "SELECTED" : ""); ?>>PÁGINA DASHBOARD</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Whatsapp (Automação)</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label>após 6:30h (Minutos):</label>
                                                                <input style="text-align:right; width: 33%;" name="txtAddWhatsapp" id="txtAddWhatsapp" type="text" class="input-medium" maxlength="2" value="<?php echo $txtAddWhatsapp; ?>"/>
                                                            </td>
                                                        </tr>                                                        
                                                    </table>
                                                </div>                                                
                                                <div style="margin-top:5px;width: 25%;float: left;">
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Impressão</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtOrcamentoImagem" id="txtOrcamentoImagem" value="1" type="checkbox" <?php
                                                                    if ($CheckOrcamentoImagem == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Exibir imagem ao imprimir relatório de orçamento</div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div style="float: left; width: 33%;">
                                                                    <label>Recibo</label>
                                                                    <select id="txtRecibo" name="txtRecibo">
                                                                        <option value="0" <?php echo ($txtRecibo == "0" ? "SELECTED" : "");?>>Bobina</option>
                                                                        <option value="1" <?php echo ($txtRecibo == "1" ? "SELECTED" : "");?>>Meia Folha</option>
                                                                        <option value="2" <?php echo ($txtRecibo == "2" ? "SELECTED" : "");?>>Bobina Duplicada</option>
                                                                    </select>
                                                                </div>
                                                                <div style="float: left; width: 33%; margin-left: 1%;">
                                                                    <label>Convite</label>
                                                                    <select id="txtConvite" name="txtConvite">
                                                                        <option value="0" <?php echo ($txtConvite == "0" ? "SELECTED" : "");?>>Bobina</option>
                                                                        <option value="1" <?php echo ($txtConvite == "1" ? "SELECTED" : "");?>>Argox</option>
                                                                    </select>
                                                                </div>
                                                                <div style="float: left; width: 32%; margin-left: 1%;">
                                                                    <label>N° Vias</label>
                                                                    <input id="txtReciboVias" name="txtReciboVias" type="text" class="input-medium" maxlength="2" value="<?php echo $txtReciboVias; ?>"/>
                                                                </div>                                                                
                                                            </td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>
                                                                <b>Boleto</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtBolBaixaEnc" id="txtBolBaixaEnc" value="1" type="checkbox" <?php
                                                                    if ($txtBolBaixaEnc == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Baixar Boleto (exibir apenas os encontrados)</div>
                                                            </td>
                                                        </tr>                                                  
                                                        <tr>
                                                            <td>
                                                                <label>Banco Padrão (Boleto Online)</label>
                                                                <select name="txtBanco" id="txtBanco" class="input-medium" style="width:50%; float: left;">
                                                                    <option value="null">Selecione</option>
                                                                    <?php $cur = odbc_exec($con, "select id_bancos,razao_social from sf_bancos where ativo = 0 order by razao_social") or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                        <option value="<?php echo $RFP['id_bancos']; ?>" <?php echo ($txtBolBancPadrao == $RFP['id_bancos'] ? "selected" : "");?>><?php echo utf8_encode($RFP['razao_social']) ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                                <select name="txtCarteira" id="txtCarteira" class="input-medium" style="width:49%; float: left; margin-left: 1%;">
                                                                    <option value="null">Selecione</option>
                                                                    <?php 
                                                                    if (is_numeric($txtBolBancPadrao)) {
                                                                    $cur = odbc_exec($con, "select sf_carteiras_id, sf_carteira_descricao from sf_bancos_carteiras where sf_carteiras_bancos = " . $txtBolBancPadrao . " order by sf_carteira_descricao") or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                        <option value="<?php echo $RFP['sf_carteiras_id']; ?>" <?php echo ($txtBolCartPadrao == $RFP['sf_carteiras_id'] ? "selected" : "");?>><?php echo utf8_encode($RFP['sf_carteira_descricao']) ?></option>
                                                                    <?php }} ?>
                                                                </select>                                                                
                                                            </td>                                                                                                                        
                                                        </tr>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Pagamentos (Desconto)</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label>Casas decimais:</label>
                                                                <input style="text-align:right; width: 33%;" name="txtDescDecimal" id="txtDescDecimal" type="text" class="input-medium" maxlength="2" value="<?php echo $txtDescDecimal; ?>"/>
                                                            </td>
                                                        </tr>                                                        
                                                    </table>                                                                                           
                                                    <div style="clear:both;"></div>                                                                                                                                                            
                                                    <?php if ($mdl_seg_ == 1) { ?>
                                                    <table>
                                                        <tbody>
                                                            <tr style="line-height:25px">
                                                                <td>
                                                                    <b>Proteção veicular</b>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <div style="float: left; width: 33%;">
                                                        <label>Piso cota mínima (R$)</label>
                                                        <input style="margin-left: 1%;text-align:right" name="txtPisoFranquia" id="txtPisoFranquia" type="text" class="input-medium" value="<?php echo $txtPisoFranquia; ?>"/>
                                                    </div>
                                                    <div style="float: left; width: 32%; margin-left: 1%;">
                                                        <label>Cota default (%)</label>
                                                        <input style="margin-left: 1%;text-align:right" name="txtCotaDefault" id="txtCotaDefault" type="text" class="input-medium" value="<?php echo $txtCotaDefault; ?>"/>
                                                    </div>
                                                    <div style="float: left; width: 32%; margin-left: 1%;">
                                                        <label>Limite anos default</label>
                                                        <input style="margin-left: 1%;text-align:right" name="txtLimiteAnos" id="txtLimiteAnos" type="text" class="input-medium" maxlength="2" value="<?php echo $txtLimiteAnos; ?>"/>
                                                    </div>                                                    
                                                    <div style="float: left; width: 33%;">
                                                        <label>Tipo busca Placa</label>
                                                        <select id="txtSegPlacaTp" name="txtSegPlacaTp">
                                                            <option value="0" <?php echo ($txtSegPlacaTp == 0 ? "SELECTED" : "");?>>Planoauto</option>
                                                            <option value="1" <?php echo ($txtSegPlacaTp == 1 ? "SELECTED" : "");?>>Youse</option>
                                                            <option value="2" <?php echo ($txtSegPlacaTp == 2 ? "SELECTED" : "");?>>PlacaIpva</option>
                                                        </select>
                                                    </div>                                                    
                                                    <div style="float: left; width: 65%; margin-left: 1%;">
                                                        <label>Código busca Placa</label>
                                                        <input style="text-align:right" name="txtSegPlacaCod" id="txtSegPlacaCod" type="text" class="input-medium" <?php
                                                            if ($txtSegPlacaTp == 0 || $txtSegPlacaTp == 2) {
                                                                echo "disabled";
                                                            } ?> value="<?php echo $txtSegPlacaCod; ?>"/>
                                                    </div>                                                        
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td>
                                                                <label>Consulta de cotas</label>
                                                                <select id="txtCotaProtegido" name="txtCotaProtegido">
                                                                    <option value="0" <?php echo ($txtCotaProtegido == 0 ? "SELECTED" : "");?>>Valor Fipe</option>
                                                                    <option value="1" <?php echo ($txtCotaProtegido == 1 ? "SELECTED" : "");?>>Valor Protegido (Total)</option>
                                                                    <option value="2" <?php echo ($txtCotaProtegido == 2 ? "SELECTED" : "");?>>Valor Protegido (% FIPE)</option>
                                                                </select>
                                                            </td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtVeiPlanObrig" id="txtVeiPlanObrig" value="1" type="checkbox" <?php
                                                                    if ($txtVeiPlanObrig == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Cadastrar veículo, plano Obrigatório</div>
                                                            </td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtVeiCotPersonal" id="txtVeiCotPersonal" value="1" type="checkbox" <?php
                                                                    if ($txtVeiCotPersonal == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Ativar Cotação Personalizada</div>
                                                            </td>
                                                        </tr>                                                        
                                                    </table>                                                    
                                                    <?php } ?>                                                                 
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab2" style="margin-bottom:5px">
                                                <div style="margin-top:5px;overflow: scroll; height: 160px; margin-bottom: 1%;">
                                                    <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblFiliais">
                                                        <thead>
                                                            <tr>
                                                                <th width="5%" style="width:5%">Filial</th>
                                                                <th width="5%" style="width:5%">Descrição</th>
                                                                <th width="7%" style="width:5%">R.Social</th>
                                                                <th width="7%" style="width:5%">N.Fantasia</th>
                                                                <th width="5%" style="width:5%">CNPJ</th>
                                                                <th width="5%" style="width:5%">Inscrição</th>
                                                                <th width="5%" style="width:5%">Endereço</th>
                                                                <th width="3%" style="width:5%">Num</th>
                                                                <th width="10%" style="width:10%">Bairro</th>
                                                                <th width="10%" style="width:10%">Cidade</th>
                                                                <th width="3%" style="width:5%">UF</th>
                                                                <th width="10%" style="width:10%">CEP</th>
                                                                <th width="10%" style="width:10%">Telefone</th>
                                                                <th width="10%" style="width:10%">Site</th>
                                                                <th width="5%" style="width:5%"><center>Editar</center></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                    <div style="clear:both"></div>
                                                </div>
                                                <div style="float: left; width: 50%;">
                                                    <div style="background:#F1F1F1; border:1px solid #DDD; padding:10px;vertical-align: top;">
                                                        <div>
                                                            <div style="float: left; width: 35%;">
                                                                <label>Filial: </label>
                                                                <select name="txtIdFilial" id="txtIdFilial" class="input-medium">
                                                                    <option value="null">Selecione</option>
                                                                    <?php $sql = "select numero_filial from sf_filiais where ativo = 0";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                        <option value="<?php echo $RFP["numero_filial"] ?>"><?php echo utf8_encode($RFP["numero_filial"]) ?></option>
                                                                    <?php } ?>
                                                                </select>                                                     
                                                            </div>                                                            
                                                            <div style="float: left;width: 64%; margin-left: 1%;">
                                                                <label>Arquivo: </label>
                                                                <input type="file" name="fotoFilial" id="fotoFilial">
                                                                <button class="btn btn-success" type="button" onclick="AdicionarFotoFilial()" name="btnSendPhoto"><span class="ico-checkmark"></span> Enviar</button>
                                                            </div>                                                        
                                                            <div style="clear:both; height:9px"></div> 
                                                        </div>
                                                    </div>                                                
                                                    <div>
                                                        <p class="imglist" style="padding-top: 10px; display: flex;"></p>
                                                    </div>
                                                </div>
                                                <div style="float: left; width: 24%; margin-left: 1%;">
                                                    <div style="background:#F1F1F1; border:1px solid #DDD; padding:10px;vertical-align: top;">
                                                        <div>                                                                                                                        
                                                            <div style="float: left;width: 64%; margin-left: 1%;">
                                                                <label>Imagem logo no Whatsapp (Quadrada):</label>
                                                                <input type="file" name="fotoWhatsapp" id="fotoWhatsapp">
                                                                <button class="btn btn-success" type="button" onclick="AdicionarFotoWhatsapp()" name="btnSendPWhatsapp"><span class="ico-checkmark"></span> Enviar</button>
                                                            </div>                                                        
                                                            <div style="clear:both; height:9px"></div> 
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <p class="imglist2" style="padding-top: 10px; display: flex;"></p>
                                                    </div>
                                                </div>
                                                <div style="float: left; width: 24%; margin-left: 1%;">
                                                    <div style="background:#F1F1F1; border:1px solid #DDD; padding:10px;vertical-align: top;">
                                                        <div>                                                                                                                        
                                                            <div style="float: left;width: 64%; margin-left: 1%;">
                                                                <label>Logo Menu (Portal):</label>
                                                                <input type="file" name="fotoLogoMenu" id="fotoLogoMenu">
                                                                <button class="btn btn-success" type="button" onclick="AdicionarFotoMenu()" name="btnFotoLogoMenu"><span class="ico-checkmark"></span> Enviar</button>
                                                            </div>                                                        
                                                            <div style="clear:both; height:9px"></div> 
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <p class="imglist3" style="padding-top: 10px; display: flex;"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab3" style="margin-bottom:5px">
                                                <div style="margin-top:5px;width: 25%;float: left">
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Dias</b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div style="float: left; width: 100%">
                                                        <label>Dias para Renovação <i>(Após este dia será considerado Retorno)</i></label>
                                                        <input type="text" id="txtDiasRenovacao" name="txtDiasRenovacao" maxlength="2" style="width:62px" value="<?php echo $txtDiasRenovacao; ?>">
                                                    </div>
                                                    <div style="float: left; width: 100%">
                                                        <label>Dias para Suspenso <i>(Após este dia será considerado Inativo)</i></label>
                                                        <input type="text" id="txtDiasSuspenso" name="txtDiasSuspenso" maxlength="3" style="width:62px" value="<?php echo $txtDiasSuspenso; ?>">
                                                    </div>
                                                    <div style="float: left; width: 100%">
                                                        <label>Cancelamento automático de plano <i>(0 dias sistema inativo)</i></label>
                                                        <input type="text" id="txtDiasCancelamento" name="txtDiasCancelamento" maxlength="3" style="width:62px" value="<?php echo $txtDiasCancelamento; ?>">
                                                    </div>
                                                    <div style="float: left; width: 20%">
                                                        <label>Tolerância</label>
                                                        <input type="text" id="txtDiasTolerancia" name="txtDiasTolerancia" maxlength="3" style="width:62px" value="<?php echo $txtDiasTolerancia; ?>">
                                                    </div>
                                                    <div style="float: left; width: 20%">
                                                        <label>Tol.Cred.</label>
                                                        <input type="text" id="txtDiasToleranciaCredencial" name="txtDiasToleranciaCredencial" maxlength="3" style="width:62px" value="<?php echo $txtDiasToleranciaCredencial; ?>">
                                                    </div>
                                                    <div style="float: left; width: 20%">
                                                        <label>Tol.Multa</label>
                                                        <input type="text" id="txtToleranciaMulta" name="txtToleranciaMulta" maxlength="3" style="width:62px" value="<?php echo $txtToleranciaMulta; ?>">
                                                    </div>
                                                    <?php if ($mdl_seg_ == 1) { ?>
                                                    <div style="float: left; width: 20%">
                                                        <label>Tol.Assist.</label>
                                                        <input type="text" id="txtDiasToleranciaAssist" name="txtDiasToleranciaAssist" maxlength="3" style="width:62px" value="<?php echo $txtDiasToleranciaAssist; ?>">
                                                    </div>
                                                    <?php } ?>
                                                    <div style="float: left; width: 20%">
                                                        <label>Tol.Status</label>
                                                        <input type="text" id="txtDiasToleranciaStatus" name="txtDiasToleranciaStatus" maxlength="3" style="width:62px" value="<?php echo $txtDiasToleranciaStatus; ?>">
                                                    </div>                                                    
                                                    <div style="clear:both; height:9px"></div>
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Vencimento</b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div>
                                                        <div style="float: left; width: 90px">
                                                            <label>Tipo</label>
                                                            <select name="txtTipoVencimento" id="txtTipoVencimento" style="width:90px">
                                                                <option value="0" <?php echo ($txtTipoVencimento == "0" ? "SELECTED" : "");?>>Variável</option>
                                                                <option value="2" <?php echo ($txtTipoVencimento == "2" ? "SELECTED" : "");?>>Condição</option>
                                                            </select>
                                                        </div>
                                                        <div style="float: left; width: 30px; margin-left: 1%;">
                                                            <label>Dia</label>
                                                            <input style="margin-left: 1%;width:30px;" name="txtDiaVencimento" id="txtDiaVencimento" type="text" class="input-medium" maxlength="2" 
                                                            <?php echo ($txtTipoVencimento == 0 ? "disabled" : ""); ?> value="<?php echo $txtDiaVencimento; ?>"/>
                                                        </div>
                                                        <div style="float: left; width: 30px; margin-left: 1%; <?php echo ($txtTipoVencimento == 2 ? "" : "display: none;"); ?>">
                                                            <label>De</label>
                                                            <input style="margin-left: 1%;width:30px;" id="txtDiaDeVencimento" type="text" class="input-medium" maxlength="2" value="1"/>
                                                        </div>
                                                        <div style="float: left; width: 30px; margin-left: 1%; <?php echo ($txtTipoVencimento == 2 ? "" : "display: none;"); ?>">
                                                            <label>até</label>
                                                            <input style="margin-left: 1%;width:30px;" id="txtDiaAteVencimento" type="text" class="input-medium" maxlength="2" value="31"/>
                                                        </div>                                                        
                                                        <div style="float: left; width: 45px; margin-left: 1%; <?php echo ($txtTipoVencimento == 2 ? "" : "display: none;"); ?>">
                                                            <label title="Mínimo de Dias">D.Min</label>
                                                            <select name="txtTpVeMin" id="txtTpVeMin" style="width:45px">
                                                                <option value="1">S</option>                                                                
                                                                <option value="0">N</option>
                                                            </select>
                                                        </div>                                                        
                                                        <div style="float: left; width: 45px; margin-left: 1%; <?php echo ($txtTipoVencimento == 2 ? "" : "display: none;"); ?>">
                                                            <label title="Pro-Rata">Pr.RT</label>
                                                            <select name="txtTpVePr" id="txtTpVePr" style="width:45px">
                                                                <option value="1">S</option>                                                                
                                                                <option value="0">N</option>
                                                            </select>                                                           
                                                        </div>                                                        
                                                        <div style="float: left; width: 40px; margin-left: 1%; <?php echo ($txtTipoVencimento == 2 ? "" : "display: none;"); ?>">
                                                            <button class="btn btn-success" type="button" style="margin-top: 15px;" onclick="AdicionarVencimentos()" id="btnDiaDeVencimento"><span class="ico-checkmark"></span></button>
                                                        </div>                                                                                                                
                                                        <div style="float: left; margin-left: 1%; width: 95%; <?php echo ($txtTipoVencimento == 2 ? "" : "display: none;"); ?>">
                                                            <table id="tbVencimentos" class="table" cellpadding="0" cellspacing="0">
                                                                <thead>
                                                                    <tr>                                                                    
                                                                        <th width="16%"><center>Dia</center></th>
                                                                        <th width="16%"><center>De</center></th>
                                                                        <th width="16%"><center>até</center></th>
                                                                        <th width="16%"><center>D.Min</center></th>
                                                                        <th width="16%"><center>Pr.RT</center></th>
                                                                        <th width="20%"><center>Ação</center></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                            </table>                                                        
                                                        </div>
                                                        <?php if ($mdl_esd_ == 1) { ?>
                                                            <div style="float: left; width: 30%; margin-left: 1%;">
                                                                <label>Lim.Agend.<i>(Estudio)</i></label>
                                                                <input type="text" id="txtlimitAgnd" name="txtlimitAgnd" maxlength="3" style="width:70px" value="<?php echo $txtlimitAgnd; ?>">
                                                            </div>
                                                        <?php } ?>                                                            
                                                    </div>
                                                    <div style="clear:both; height:9px"></div>                                                    
                                                </div>
                                                <div style="margin-top:5px;width: 25%;float: left">
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Número de Dias de Ausência (Faixas de Risco de Evasão)</b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table>
                                                        <tr>
                                                            <td></td>
                                                            <td>Início</td>
                                                            <td>Fim</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Faixa 1</td>
                                                            <td><input type="text" id="txtFaixaI1" name="txtFaixaI1" maxlength="4" style="width:70px" value="<?php echo $txtFaixaI1; ?>"></td>
                                                            <td><input type="text" id="txtFaixaF1" onchange="ajustarDias('txtFaixaF1', 'txtFaixaI2');" name="txtFaixaF1" maxlength="4" style="width:70px" value="<?php echo $txtFaixaF1; ?>"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Faixa 2</td>
                                                            <td><input type="text" id="txtFaixaI2" name="txtFaixaI2" maxlength="4" style="width:70px" readonly value="<?php echo $txtFaixaI2; ?>"></td>
                                                            <td><input type="text" id="txtFaixaF2" onchange="ajustarDias('txtFaixaF2', 'txtFaixaI3');" name="txtFaixaF2" maxlength="4" style="width:70px" value="<?php echo $txtFaixaF2; ?>"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Faixa 3</td>
                                                            <td><input type="text" id="txtFaixaI3" name="txtFaixaI3" maxlength="4" style="width:70px" readonly value="<?php echo $txtFaixaI3; ?>"></td>
                                                            <td><input type="text" id="txtFaixaF3" onchange="ajustarDias('txtFaixaF3', 'txtFaixaI4');" name="txtFaixaF3" maxlength="4" style="width:70px" value="<?php echo $txtFaixaF3; ?>"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Faixa 4</td>
                                                            <td><input type="text" id="txtFaixaI4" name="txtFaixaI4" maxlength="4" style="width:70px" readonly value="<?php echo $txtFaixaI4; ?>"></td>
                                                            <td><input type="text" id="txtFaixaF4" onchange="ajustarDias('txtFaixaF4', 'txtFaixaI5');" name="txtFaixaF4" maxlength="4" style="width:70px" value="<?php echo $txtFaixaF4; ?>"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Faixa 5</td>
                                                            <td><input type="text" id="txtFaixaI5" name="txtFaixaI5" maxlength="4" style="width:70px" readonly value="<?php echo $txtFaixaI5; ?>"></td>
                                                            <td><input type="text" id="txtFaixaF5" name="txtFaixaF5" maxlength="4" style="width:70px" value="<?php echo $txtFaixaF5; ?>"></td>
                                                        </tr>
                                                    </table>
                                                    <div style="clear:both; height:9px"></div>                                                                                                        
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Gerais</b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div>
                                                        <div style="float:left; margin-top:0">
                                                            <input name="txtCadProspCli" id="txtCadProspCli" value="1" type="checkbox" <?php
                                                            if ($txtCadProspCli == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Ao cadastrar novo Cliente, sempre cadastrar um Prospect</div>
                                                    </div>
                                                    <div style="clear:both;"></div>
                                                    <div>
                                                        <div style="float:left; margin-top:0">
                                                            <input name="txtProspPag" id="txtProspPag" value="1" type="checkbox" <?php
                                                            if ($txtProspPag == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Ativar pagamento de serviço para Prospect</div>
                                                    </div>
                                                    <div style="clear:both;"></div>
                                                    <div>
                                                        <div style="float:left; margin-top:0">
                                                            <input name="txtGrupoBoleto" id="txtGrupoBoleto" value="1" type="checkbox" <?php
                                                            if ($txtGrupoBoleto == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Agrupar cobranças de titulares e dependentes</div>
                                                    </div>
                                                    <div style="clear:both;"></div>                                                    
                                                    <div>
                                                        <div style="float:left; margin-top:0">
                                                            <input name="txtGrupoBoletoMod" id="txtGrupoBoletoMod" value="1" type="checkbox" <?php
                                                            if ($txtGrupoBoletoMod == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Agrupar planos de titulares em um mesmo boleto</div>
                                                    </div>
                                                    <div style="clear:both;"></div>                                                    
                                                    <div>
                                                        <div style="float:left; margin-top:0">
                                                            <input name="txtBloqAtGymWeb" id="txtBloqAtGymWeb" value="1" type="checkbox" <?php
                                                            if ($txtBloqAtGymWeb == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Permitir acesso somente de clientes Ativos no Bodyweb</div>
                                                    </div>
                                                    <div style="clear:both;"></div>                                                    
                                                    <div>
                                                        <div style="float:left; margin-top:0">
                                                            <input name="txtVenConExProRata" id="txtVenConExProRata" value="1" type="checkbox" <?php
                                                            if ($txtVenConExProRata == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Exceção de pro-rata em Vencimento Condicional</div>
                                                    </div>
                                                    <div style="clear:both;"></div>
                                                    <div>
                                                        <div style="float:left; margin-top:0">
                                                            <input name="txtAtvAberto" id="txtAtvAberto" value="1" type="checkbox" <?php
                                                            if ($txtAtvAberto == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Status <b>Ativo em Aberto</b> quando a parcela for vencida.</div>
                                                    </div>
                                                    <div style="clear:both;"></div>                                                    
                                                    <div>
                                                        <div style="float:left; margin-top:0">
                                                            <input name="txtTopDataDigi" id="txtTopDataDigi" value="1" type="checkbox" <?php
                                                            if ($txtTopDataDigi == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Lista de exceções de digitais <b>(TopData Inner Revolution)</b></div>
                                                    </div>
                                                    <div style="clear:both;"></div>                                                    
                                                    <div>
                                                        <div style="float:left; margin-top:0">
                                                            <input name="txtMensPlanAcess" id="txtMensPlanAcess" value="1" type="checkbox" <?php
                                                            if ($txtMensPlanAcess == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Contas a Receber > Mens. Valor Plano + <b>Acessórios</b></div>
                                                    </div>
                                                    <div style="clear:both;"></div>                                                    
                                                    <div>
                                                        <div style="float:left; margin-top:0">
                                                            <input name="txtBaixaLayoutDesl" id="txtBaixaLayoutDesl" value="1" type="checkbox" <?php
                                                            if ($txtBaixaLayoutDesl == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Baixa por Layout desconsiderar (<b>Desligados</b>)</div>
                                                    </div>
                                                    <div style="clear:both; height:9px"></div>                                                    
                                                </div>
                                                <div style="margin-top:5px;width: 25%;float: left">                                                                                                        
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Multa</b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div>
                                                        <div style="float: left; width: 100%">
                                                            <label>Tipo</label>
                                                            <select name="txtTipoMulta" id="txtTipoMulta" style="width:143px">
                                                                <option value="0" <?php echo ($txtTipoMulta == "0" ? "SELECTED" : "");?>>SEM MULTA</option>
                                                                <option value="1" <?php echo ($txtTipoMulta == "1" ? "SELECTED" : "");?>>MORA DIÁRIA</option>
                                                                <option value="2" <?php echo ($txtTipoMulta == "2" ? "SELECTED" : "");?>>TAXA + MORA</option>
                                                                <option value="3" <?php echo ($txtTipoMulta == "3" ? "SELECTED" : "");?>>TAXA INICIAL</option>
                                                            </select>
                                                        </div>
                                                        <div style="float: left; width: 24%;">
                                                            <label>a partir de (dias)</label>
                                                            <input style="margin-left: 1%;text-align:right" name="txtTaxaInicialDias" id="txtTaxaInicialDias" type="text" class="input-medium" maxlength="100" <?php
                                                            if ($txtTipoMulta == 0) {
                                                                echo "disabled";
                                                            }
                                                            ?> value="<?php echo $txtTaxaInicialDias; ?>"/>
                                                        </div>                                                        
                                                        <div style="float: left; width: 24%;margin-left: 1%;">
                                                            <label>Taxa Inicial</label>
                                                            <input style="margin-left: 1%;text-align:right" name="txtTaxaInicial" id="txtTaxaInicial" type="text" class="input-medium" maxlength="100" <?php
                                                            if ($txtTipoMulta == 0) {
                                                                echo "disabled";
                                                            }
                                                            ?> value="<?php echo $txtTaxaInicial; ?>"/>
                                                        </div>                                                        
                                                        <div style="float: left; width: 24%;margin-left: 1%;">
                                                            <label>Mora Diária</label>
                                                            <input style="margin-left: 1%;text-align:right" name="txtMoraDiaria" id="txtMoraDiaria" type="text" class="input-medium" maxlength="100" <?php
                                                            if ($txtTipoMulta == 0) {
                                                                echo "disabled";
                                                            }
                                                            ?> value="<?php echo $txtMoraDiaria; ?>"/>
                                                        </div>                                                        
                                                        <div style="float: left; width: 24%; margin-left: 1%; margin-bottom: 5px;">
                                                            <label>Forma</label>
                                                            <select name="txtFormaMulta" id="txtFormaMulta" style="width:100%">
                                                                <option value="0" <?php echo ($txtFormaMulta == "0" ? "SELECTED" : "");?>>%</option>
                                                                <option value="1" <?php echo ($txtFormaMulta == "1" ? "SELECTED" : "");?>>R$</option>
                                                            </select>
                                                        </div>
                                                        <div style="clear:both;"></div>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <b>Pagamento</b>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div style="float: left; width: 49%;">
                                                            <label>Ordenação Mens. <i>(Dt. Venc.)</i></label>
                                                            <select name="txtOrdemMens" id="txtOrdemMens" style="width: 100%;">
                                                                <option value="0" <?php
                                                                if ($txtOrdemMens == "0") {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Crescente</option>
                                                                <option value="1" <?php
                                                                if ($txtOrdemMens == "1") {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Decrescente</option>
                                                            </select>
                                                        </div>
                                                        <div style="float: left; width: 49%; margin-left: 1%;">
                                                            <label>Comissão Forma:</label>
                                                            <select name="txtComissaoForma" id="txtComissaoForma" style="width: 100%;">
                                                                <option value="0" <?php
                                                                if ($txtComissaoForma == "0") {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Por Funcionário</option>
                                                                <option value="1" <?php
                                                                if ($txtComissaoForma == "1") {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>>Por Cliente</option>
                                                            </select>
                                                        </div>
                                                        <div style="clear:both; height:5px"></div>
                                                        <table>
                                                            <tr style="line-height:25px">
                                                                <td>
                                                                    <b>Desconto (BOLETO – PIX)</b>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div>
                                                            <div style="float: left; width: 43%">
                                                                <label>Tipo</label>
                                                                <select name="txtTipoDesconto" id="txtTipoDesconto" style="width:100%;">
                                                                    <option value="0" <?php echo ($txtTipoDesconto == "0" ? "SELECTED" : "");?>>SEM DESCONTO</option>
                                                                    <option value="1" <?php echo ($txtTipoDesconto == "1" ? "SELECTED" : "");?>>DESC. GERAL</option>
                                                                    <option value="2" <?php echo ($txtTipoDesconto == "2" ? "SELECTED" : "");?>>DESC. INDIVIDUAL</option>
                                                                </select>
                                                            </div>                                                            
                                                            <div style="float: left; width: 30%;margin-left: 1%;">
                                                                <label title="Quantidade de dias que o desconto será válido.Valores válidos de 0 a 20.&#10;0 = desconto valerá até a data do vencimento.&#10;1 a 20 = desconto válido até x dias antes do vencimento."><span class="ico-question-sign informacao"></span> a partir de (dias)</label>
                                                                <input style="margin-left: 1%;text-align:right" name="txtDescontoDias" id="txtDescontoDias" type="text" class="input-medium" maxlength="100" <?php
                                                                if ($txtTipoDesconto == 0) {
                                                                    echo "disabled";
                                                                } ?> value="<?php echo $txtDescontoDias; ?>"/>
                                                            </div>                                                        
                                                            <div style="float: left; width: 25%;margin-left: 1%;">
                                                                <label>Valor (%)</label>
                                                                <input style="margin-left: 1%;text-align:right" name="txtDescontoValor" id="txtDescontoValor" type="text" class="input-medium" maxlength="100" <?php
                                                                if ($txtTipoDesconto == 0) {
                                                                    echo "disabled";
                                                                } ?> value="<?php echo $txtDescontoValor; ?>"/>
                                                            </div>
                                                        </div>
                                                        <div style="clear:both; height:9px"></div>                                                        
                                                        <b>Contratos (Assinatura Responsável)</b><br>
                                                        <button type="button" name="bntAddAssinatura" onclick="AbrirAssinatura(0)" class="btn btn-success" style="height:27px; margin:0; padding-top:6px;float: left;"><span class="ico-plus"></span></button>
                                                        <button type="button" name="bntAddAssinatura" onclick="refreshAssinatura()" class="btn btn-primary" style="height:27px; margin:0; padding-top:6px;float: left; margin-left: 1%;"><span class="ico-refresh"></span></button>
                                                        <table id="tbAssinatura" class="table" cellpadding="0" cellspacing="0" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th width="90%">Data Cadastro</th>
                                                                    <th width="10%"><center>Ação</center></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td colspan="2" class="dataTables_empty"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <div style="clear:both"></div>                                                                                                                                                                                                                        
                                                    </div>                                                    
                                                    <div style="clear:both; height:9px"></div>                                                                                                                                                            
                                                </div>
                                                <div style="margin-top:5px;width: 25%;float: left">                                                                                                        
                                                    <table> 
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <b>Outros</b>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>                                                        
                                                    <div>
                                                        <div style="float:left; margin-top:0">
                                                            <input name="txtMensAuto" id="txtMensAuto" value="1" type="checkbox" <?php
                                                            if ($txtMensAuto == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Gerar Mensalidade Automaticamente</div>
                                                    </div>
                                                    <div style="clear:both;"></div>                                                        
                                                    <div>
                                                        <div style="float:left; margin-top:0;">
                                                            <input name="txtBloqDebCarteira" id="txtBloqDebCarteira" value="1" type="checkbox" <?php
                                                            if ($txtBloqDebCarteira == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Bloqueio por Débito em Carteira em Vencimento</div>                                                                                                                
                                                    </div>                                                        
                                                    <div style="clear:both;"></div>                                                        
                                                    <div>
                                                        <div style="float:left; margin-top:0;">
                                                            <input name="txtBloqCarteiraVencida" id="txtBloqCarteiraVencida" value="1" type="checkbox" <?php
                                                            if ($txtBloqCarteiraVencida == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Bloqueio por Carteira Vencida</div>                                                                                                                
                                                    </div>                                                        
                                                    <div style="clear:both;">
                                                        <div style="float:left; margin-top:0">
                                                            <input name="txtAtualizarMens" id="txtAtualizarMens" value="1" type="checkbox" <?php
                                                            if ($txtAtualizarMens == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Atualizar Mensalidades (Exceto as Vencidas)</div>
                                                    </div>
                                                    <div style="clear:both;"></div>                                                        
                                                    <?php if($mdl_cnt_ == 1) { ?>
                                                    <div>
                                                        <div style="float:left; margin-top:0;">
                                                            <input name="txtUseContrato" id="txtUseContrato" value="1" type="checkbox" <?php
                                                            if ($txtUseContrato == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px;">Usar o contrato do modulo</div>                                                                                                                    
                                                    </div>
                                                    <div style="clear:both;"></div>                                                                                                            
                                                    <?php } ?>
                                                    <div>
                                                        <div style="float:left; margin-top:0;">
                                                            <input name="txtServicoAdesao" id="txtServicoAdesao" value="1" type="checkbox" <?php
                                                            if ($txtServicoAdesao == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Ativar cobrança de serviços de adesão</div>                                                                                                                
                                                    </div>
                                                    <div style="clear:both;"></div>                                                                                                                
                                                    <div>
                                                        <div style="float:left; margin-top:0;">
                                                            <input name="txtCatracaUrna" id="txtCatracaUrna" value="1" type="checkbox" <?php
                                                            if ($txtCatracaUrna == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Ativar catraca Urna</div>                                                                                                                
                                                    </div>
                                                    <div style="clear:both;"></div>                                                        
                                                    <div>
                                                        <div style="float:left; margin-top:0;">
                                                            <input name="txtExcecaoBaixa" id="txtExcecaoBaixa" value="1" type="checkbox" <?php
                                                            if ($txtExcecaoBaixa == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Ativar exceção de vencimento em baixa automática</div>                                                                                                                
                                                    </div>                                                    
                                                    <div style="clear:both;"></div>                                                        
                                                    <div>
                                                        <div style="float:left; margin-top:0;">
                                                            <input name="txtExcecaoBaixaDcc" id="txtExcecaoBaixaDcc" value="1" type="checkbox" <?php
                                                            if ($txtExcecaoBaixaDcc == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Ativar exceção de Multa e Juros em DCC</div>                                                                                                                
                                                    </div>                                                                                                                                                                
                                                    <div style="clear:both;"></div>                                                        
                                                    <div>
                                                        <div style="float:left; margin-top:0;">
                                                            <input name="txtBxLayCarteira" id="txtBxLayCarteira" value="1" type="checkbox" <?php
                                                            if ($txtBxLayCarteira == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Baixa de layout com valor menor, lançar em carteira</div>                                                                                                                
                                                    </div>
                                                    <div style="clear:both;"></div>
                                                    <div>
                                                        <div style="float:left; margin-top:0;">
                                                            <input name="txtDescVencimento" id="txtDescVencimento" value="1" type="checkbox" <?php
                                                            if ($txtDescVencimento == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Desconto sempre <b>até vencimento</b></div>                                                                                                                
                                                    </div>
                                                    <div style="clear:both;"></div>
                                                    <div>
                                                        <div style="float:left; margin-top:0;">
                                                            <input name="txtDocAssinatura" id="txtDocAssinatura" value="1" type="checkbox" <?php
                                                            if ($txtDocAssinatura == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Exibir documentos antes da <b>assinatura</b> de um contrato</div>                                                                                                                
                                                    </div>
                                                    <div style="clear:both;"></div>
                                                    <div>
                                                        <div style="float:left; margin-top:0;">
                                                            <input name="txtCadCarFil" id="txtCadCarFil" value="1" type="checkbox" <?php
                                                            if ($txtCadCarFil == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Cadastrar cartão filtrar por Filial</div>                                                                                                                
                                                    </div>
                                                    <table style="margin-top:25px"> 
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <b>Vistoria de Reabilitação</b>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <div>
                                                        <div style="float: left; width: 25%; margin-left: 1%;">
                                                            <label>Pg. Após Dia(s)</label>
                                                            <input style="width:100%;" name="txtAposDia" id="txtAposDia" type="text" class="input-medium" maxlength="2" value="<?php echo $txtAposDia; ?>" autocomplete="off">
                                                        </div>     
                                                        <div style="float: left; width: 73%; margin-left: 1%;">
                                                            <label>Modelo de Disparo (Whatsapp)</label>
                                                            <select name="txtModeloDisparo" id="txtModeloDisparo" style="width:100%;">
                                                                <option value="null">Selecione</option>
                                                                <?php $cur = odbc_exec($con, "select id_email, descricao from sf_emails where tipo = 2 order by id_email") or die(odbc_errormsg());
                                                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                    <option value="<?php echo $RFP['id_email'] ?>"<?php
                                                                    if (!(strcmp($RFP['id_email'], $txtModeloDisparo))) {
                                                                        echo "SELECTED";
                                                                    }
                                                                    ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                                <?php } ?>                                                                 
                                                            </select>
                                                        </div>                                                        
                                                        <div style="float: left; width: 99%; margin-left: 1%;">
                                                            <label>Modelo de Vistoria - Carro</label>
                                                            <select name="txtModeloVistoria" id="txtModeloVistoria" style="width:100%;">
                                                                <option value="null">Selecione</option>
                                                                <?php $cur = odbc_exec($con, "select id, descricao from sf_vistorias_tipo where sinistro = 0 and inativo = 0") or die(odbc_errormsg());
                                                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                    <option value="<?php echo $RFP['id'] ?>"<?php
                                                                    if (!(strcmp($RFP['id'], $txtModeloVistoria))) {
                                                                        echo "SELECTED";
                                                                    }
                                                                    ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                                <?php } ?>                                                                 
                                                            </select>
                                                        </div>
                                                        <div style="float: left; width: 99%; margin-left: 1%;">
                                                            <label>Modelo de Vistoria - Moto</label>
                                                            <select name="txtModeloVistoriaMoto" id="txtModeloVistoriaMoto" style="width:100%;">
                                                                <option value="null">Selecione</option>
                                                                <?php $cur = odbc_exec($con, "select id, descricao from sf_vistorias_tipo where sinistro = 0 and inativo = 0") or die(odbc_errormsg());
                                                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                    <option value="<?php echo $RFP['id'] ?>"<?php
                                                                    if (!(strcmp($RFP['id'], $txtModeloVistoriaMoto))) {
                                                                        echo "SELECTED";
                                                                    }
                                                                    ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                                <?php } ?>                                                                 
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab4" style="margin-bottom:5px">
                                                <div style="margin-top:5px;width: 27%;float: left">
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Dependentes</b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div style="margin-top: 10px;">
                                                        <div style="float:left; margin-top:0">
                                                            <input name="txtDelTituDep" id="txtDelTituDep" value="1" type="checkbox" <?php
                                                            if ($txtDelTituDep == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Ao excluir um Titular, excluir seus Dependentes</div>
                                                    </div>
                                                    <div style="clear:both; height:9px"></div>
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Convites</b>
                                                            </td>
                                                        </tr>
                                                    </table>                                                    
                                                    <div style="float: left; width: 100%">
                                                        <label>Número de convites gratuitos por alunos</label>
                                                        <input type="text" id="txtConvitesDep" name="txtConvitesDep" maxlength="2" style="width:70px" value="<?php echo $txtConvitesDep; ?>">
                                                        <label style="display: contents;"> a cada </label>
                                                        <input type="text" id="txtConvitesDias" name="txtConvitesDias" maxlength="2" style="width:70px" value="<?php echo $txtConvitesDias; ?>">
                                                        <label style="display: contents;"> dia(s) </label>
                                                    </div>                                                    
                                                    <div style="float: left; width: 100%">
                                                        <label>Número de mensalidades em aberto por cliente</i></label>
                                                        <input type="text" id="txtMensalidadesAberto" name="txtMensalidadesAberto" maxlength="2" style="width:70px" value="<?php echo $txtMensalidadesAberto; ?>">
                                                    </div>                                                    
                                                    <div style="float: left; width: 100%">
                                                        <label>Dias de tolerância para cancelamento de vínculo de parentesco</i></label>
                                                        <input type="text" id="txtToleranciaParentesco" name="txtToleranciaParentesco" maxlength="2" style="width:70px" value="<?php echo $txtToleranciaParentesco; ?>">
                                                    </div>                                                    
                                                    <div style="clear:both; height:9px"></div>
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Credencial</b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div style="margin-top: 10px;">
                                                        <div style="float:left; margin-top:0">
                                                            <input name="txtPagamentoCredencial" id="txtPagamentoCredencial" value="1" type="checkbox" <?php
                                                            if ($txtPagamentoCredencial == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Obrigatoriedade de Pagamento para Credencial</div>
                                                    </div>                                                    
                                                    <div style="float: left; width: 100%; margin-top: 10px;">
                                                        <label>Número de credenciais gratuitas por cliente (Titular)</label>
                                                        <input type="text" id="txtCredencialDep" name="txtCredencialDep" maxlength="2" style="width:70px" value="<?php echo $txtCredencialDep; ?>">
                                                        <label style="display: contents;"> a cada </label>
                                                        <input type="text" id="txtCredencialDias" name="txtCredencialDias" maxlength="2" style="width:70px" value="<?php echo $txtCredencialDias; ?>">
                                                        <label style="display: contents;"> dia(s) </label>
                                                    </div>                                                     
                                                    <div style="clear:both; height:9px"></div>
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Gerais</b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div style="margin-top: 10px;">
                                                        <div style="float:left; margin-top:0">
                                                            <input name="txtPagamentoAgrupado" id="txtPagamentoAgrupado" value="1" type="checkbox" <?php
                                                            if ($txtPagamentoAgrupado == 1) {
                                                                echo "CHECKED";
                                                            }
                                                            ?> class="input-medium"/>
                                                        </div>
                                                        <div style="float:left; margin:2px 5px">Agrupar seleção de itens de pagamento por mês</div>
                                                    </div>
                                                    <div style="clear:both;"></div>                                                                                                     
                                                </div>
                                                <div style="margin-top:5px;width: 40%;float: left">
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Convênios por números de planos</b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <input name="txtIdAcrSer" id="txtIdAcrSer" value="" type="hidden"/>
                                                    <div style="width: 62%;float:left;">
                                                        <span>Convênios</span>
                                                        <select id="txtServicoAcrSer" name="txtServicoAcrSer" <?php echo $disabled; ?>>
                                                            <option value="null">Todos</option>
                                                            <?php $cur = odbc_exec($con, "select id_convenio, descricao from sf_convenios where tipo = 1 and convenio_especifico = 1 and status = 0 and ativo = 0 and id_conv_plano is null") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['id_convenio'] ?>"><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                                            <?php } ?>                            
                                                        </select>
                                                    </div>
                                                    <div style="width: 13%;float:left; margin-left: 1%;">
                                                        <span>Qtd. Min.</span>
                                                        <input id="txtQtdMinAcrSer" name="txtQtdMinAcrSer" value="" <?php echo $disabled; ?> maxlength="2" type="text" class="input-medium"/>
                                                    </div>
                                                    <div style="width: 13%;float:left; margin-left: 1%;">
                                                        <span>Qtd. Max.</span>
                                                        <input id="txtQtdMaxAcrSer" name="txtQtdMaxAcrSer" value="" <?php echo $disabled; ?> maxlength="2" type="text" class="input-medium"/>
                                                    </div>      
                                                    <div style="width: 9%;float:left; margin-left: 1%; margin-top: 16px;">                   
                                                        <button type="button" name="bntAdicionaAcrSer" onclick="addLinhaAcrSer()" <?php echo $disabled; ?> class="btn btn-success" style="height:27px; margin:0; padding-top:6px;float: left;"><span class="ico-plus"></span></button>
                                                    </div> 
                                                    <div style="clear:both;height: 15px;"></div>                    
                                                    <table id="tbPlanoAcrescimoSer" width="100%" cellpadding="8" style="font-size:11px; margin:0 0px;" border="1" bordercolor="#FFF">
                                                        <thead style="background:#E9E9E9;">
                                                            <td style="line-height:18px; "><b>Convênios</b></td>
                                                            <td style="line-height:18px; width:55px"><b>Qtd. Min.</b></td>
                                                            <td style="line-height:18px; width:55px"><b>Qtd. Max.</b></td>
                                                            <td style="line-height:18px; width:30px"><b>Ação</b></td>                            
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </div>              
                                                <div style="margin-top:5px;width: 27%;float: left; margin-left: 4%;">
                                                    <b>Mensagem padrão contratação</b><br>
                                                    <textarea style="width:100%; height: 275px;" id="txtMsgContratacao" name="txtMsgContratacao"><?php echo $txtMsgContratacao; ?></textarea><br>
                                                    <b>Imagem padrão contratação</b><br>
                                                    <div style="background:#F1F1F1; border:1px solid #DDD; padding:10px;vertical-align: top;">
                                                        <div>                                                                                                                        
                                                            <div style="float: left;width: 64%; margin-left: 1%;">
                                                                <label>Contratação (Portal):</label>
                                                                <input type="file" name="fotoContratacao" id="fotoContratacao">
                                                                <button class="btn btn-success" type="button" onclick="AdicionarFotoContratacao()" name="btnFotoContratacao"><span class="ico-checkmark"></span> Enviar</button>
                                                            </div>                                                        
                                                            <div style="clear:both; height:9px"></div> 
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <p class="imglist4" style="padding-top: 10px; display: flex;"></p>
                                                    </div>                                                    
                                                </div>
                                            </div>
                                            <div class="tab-pane <?php echo ($_SESSION["mod_emp"] == 1 ? 'active' : ''); ?>" id="tab5" style="margin-bottom:5px">
                                                <div style="margin-top:5px;width: 33%;float: left">
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>NFE - Certificado Digital A1</b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div>
                                                        <label>Ambiente</label>
                                                        <select name="txtAmbiente" id="txtAmbiente" style="width:292px">
                                                            <option value="2" <?php
                                                            if ($tpAmb == "2") {
                                                                echo "SELECTED";
                                                            }
                                                            ?>>Homologação</option>
                                                            <option value="1" <?php
                                                            if ($tpAmb == "1") {
                                                                echo "SELECTED";
                                                            }
                                                            ?>>Produção</option>
                                                        </select>
                                                    </div>
                                                    <div style="clear:both; height:5px"></div>
                                                    <div style="float:left">
                                                        <label>Razão Social do Emitente</label>
                                                        <input type="text" readonly id="txtRazaoSocial" name="txtRazaoSocial" maxlength="10" style="width:293px" value="<?php echo $_SESSION["razao_social_contrato"]; ?>">
                                                    </div>
                                                    <div style="float:left;padding-left: 1%;">
                                                        <label>CNPJ do Emitente</label>
                                                        <input type="text" readonly id="txtCNPJ" name="txtCNPJ" maxlength="10" style="width:127px" value="<?php echo $_SESSION["cpf_cnpj"]; ?>">
                                                    </div>
                                                    <div style="clear:both; height:5px"></div>
                                                    <div style="float:left;min-width: 268px;">
                                                        <label>Arquivo</label>
                                                        <div id="btnUpArquivo" <?php
                                                        if ($nmArquivo !== "") {
                                                            echo "hidden";
                                                        }
                                                        ?>>
                                                            <input class="btn btn-primary" style="height:20px; line-height:21px; width:100%" type="file" name="arquivoCertificado" id="arquivoCertificado">
                                                        </div>
                                                        <div id="lblUpArquivo" <?php
                                                        if ($nmArquivo === "") {
                                                            echo "hidden";
                                                        }
                                                        ?> >
                                                            <label id="nmArquivo" name="nmArquivo" style="float: left;margin-top: 5px;margin-right: 5px;color: #008DC4;"><?php echo $nmArquivo; ?></label>
                                                            <input id="txtNmArquivo" name="txtNmArquivo" type="hidden" value="<?php echo $nmArquivo; ?>"/>
                                                            <button class="btn btn-danger" type="button" id="btnDelArquivo" style="padding: 3px 6px;height: 26px;width: 26px;"><span class="icon-remove icon-white"></span></button>
                                                        </div>
                                                    </div>
                                                    <div style="float:left;margin-left: 6%">
                                                        <label>Senha</label>
                                                        <input type="password" id="txtSenhaCert" name="txtSenhaCert" maxlength="10" autocomplete="off" autocorrect="off" spellcheck="false" style="width:72px" value="<?php echo $txtSenhaCert; ?>">
                                                        <button type="button" class="btn" id="btnTestar" style="height: 25px;margin-bottom: 0px;">Testar</button>
                                                    </div>
                                                    <div style="clear:both; height:5px"></div>
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>NFC-E - Token</b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div style="float:left;">
                                                        <label>Id</label>
                                                        <input type="text" id="txtTokenId" name="txtTokenId" maxlength="6" style="width:127px" value="<?php echo $txtTokenId; ?>">
                                                    </div>
                                                    <div style="float:left;padding-left: 1%;">
                                                        <label>Token</label>
                                                        <input type="text" id="txtToken" name="txtToken" maxlength="37" style="width:296px" value="<?php echo $txtToken; ?>">
                                                    </div>
                                                    <div style="clear:both; height:5px"></div>
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>NFS-E - CREDENCIAIS</b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div>
                                                        <label>Ambiente</label>
                                                        <select name="txtNfsAmbiente" id="txtNfsAmbiente" style="width:292px">
                                                            <option value="0" <?php
                                                            if ($tpNfsAmb == "0") {
                                                                echo "SELECTED";
                                                            }
                                                            ?>>Homologação</option>
                                                            <option value="1" <?php
                                                            if ($tpNfsAmb == "1") {
                                                                echo "SELECTED";
                                                            }
                                                            ?>>Produção</option>
                                                        </select>
                                                    </div>
                                                    <div style="float:left;">
                                                        <label>Id da Empresa</label>
                                                        <input type="text" id="txtNfsEmpresa" name="txtNfsEmpresa" maxlength="40" style="width:435px" value="<?php echo $txtNfsEmpresa; ?>">
                                                    </div>
                                                    <div style="clear:both; height:5px"></div>
                                                    <div style="float:left;">
                                                        <label>Chave</label>
                                                        <input type="text" id="txtNfsChave" name="txtNfsChave" maxlength="50" style="width:435px" value="<?php echo $txtNfsChave; ?>">
                                                    </div>
                                                    <div style="clear:both; height:5px"></div>
                                                </div>
                                                <?php if ($contrato == "003" || $contrato == "008") { ?>                                                 
                                                <div style="margin-top:5px;width: 33%;float: left">
                                                    <div style="clear:both; height:5px"></div>
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>IPs - Whitelist</b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div style="float:left; width:25%;">
                                                        <label>Ip</label>
                                                        <input type="text" id="txtIp" maxlength="16" style="width:100%" value="">
                                                    </div>
                                                    <div style="float:left;padding-left: 1%; width: 33%;">
                                                        <label>Descrição</label>
                                                        <input type="text" id="txtIpDescricao" maxlength="256" style="width:100%;" value="">
                                                    </div>
                                                    <div style="float:left;padding-left: 1%; width: 30%;">
                                                        <label>Usuário</label>
                                                        <select id="txtIpUser" style="width:100%" class="select">
                                                            <option value="null">Selecione</option>
                                                            <?php $cur = odbc_exec($con, "select distinct id_usuario, nome from sf_usuarios 
                                                            inner join sf_usuarios_filiais on id_usuario_f = id_usuario 
                                                            where sf_usuarios.inativo = 0 order by nome") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['id_usuario']; ?>"><?php echo formatNameCompact(strtoupper(utf8_encode($RFP['nome']))); ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div style="float:left;padding-left: 1%; width: 9%; margin-top: 14px">
                                                        <button type="button" name="bntAddIp" onclick="AddIp(0)" style="height:27px; padding-top:6px;" class="btn btn-success"><span class="ico-plus"></span></button>
                                                    </div>
                                                    <table id="tbAddIp" class="table" cellpadding="0" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th width="25%">IP</th>
                                                                <th width="35%">Descrição</th>
                                                                <th width="30%">Usuário</th>                                                                
                                                                <th width="10%"><center>Ação</center></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td colspan="4" class="dataTables_empty"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>                                                    
                                                    <div style="clear:both"></div>
                                                </div>
                                                <div style="margin-top:5px;width: 34%;float: left">
                                                    <div style="clear:both; height:5px"></div>
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>IPs - Blacklist</b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div style="float:left; width:30%;">
                                                        <label>Ip</label>
                                                        <input type="text" id="txtIpBlack" maxlength="16" style="width:100%" value="">
                                                    </div>
                                                    <div style="float:left;padding-left: 1%; width: 59%;">
                                                        <label>Descrição</label>
                                                        <input type="text" id="txtIpBlackDescricao" maxlength="256" style="width:100%;" value="">
                                                    </div>
                                                    <div style="float:left;padding-left: 1%; width: 9%; margin-top: 14px">
                                                        <button type="button" name="bntAddIpBlack" onclick="AddIpBlack(0)" style="height:27px; padding-top:6px;" class="btn btn-success"><span class="ico-plus"></span></button>
                                                    </div>
                                                    <table id="tbAddIpBlack" class="table" cellpadding="0" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th width="30%">IP</th>
                                                                <th width="60%">Descrição</th>
                                                                <th width="10%"><center>Ação</center></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td colspan="3" class="dataTables_empty"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>                                                    
                                                    <div style="clear:both"></div>
                                                </div>
                                                <?php } ?>
                                                <div style="clear:both; height:5px"></div>
                                            </div>
                                            <div class="tab-pane" id="tab6" style="margin-bottom:5px">
                                                <div style="margin-top:5px;width: 25%;float: left">
                                                    <b>Cores (Primária, Secundária e Destaque)</b><br>
                                                    <input type="color" id="cor_primaria" style="height: 25px;" name="cor_primaria" value="<?php echo $cor_primaria; ?>">
                                                    <input type="color" id="cor_secundaria" style="height: 25px;" name="cor_secundaria" value="<?php echo $cor_secundaria; ?>">
                                                    <input type="color" id="cor_destaque" style="height: 25px;" name="cor_destaque" value="<?php echo $cor_destaque; ?>"><br>                                                    
                                                    <div style="clear:both"></div>                                                    
                                                    <b>Mensalidades</b><br>
                                                    <div style="width: 24%; float: left;">
                                                        <label>N° Mens. Ant.</label>
                                                        <input id="txtMensAnt" name="txtMensAnt" type="text" class="input-medium" maxlength="2" value="<?php echo $txtMensAnt; ?>"/>                                                        
                                                    </div>
                                                    <div style="width: 24%; float: left; margin-left: 1%">
                                                        <label>N° Mens. Post.</label>
                                                        <input id="txtMensPost" name="txtMensPost" type="text" class="input-medium" maxlength="4" value="<?php echo $txtMensPost; ?>"/>                                                        
                                                    </div>                                                        
                                                    <div style="width: 50%; float: left; margin-left: 1%">
                                                        <label>ISS Retido?(desconto)</label>
                                                        <select id="txtIsencaoIE" name="txtIsencaoIE">
                                                            <option value="null">Selecione</option>
                                                            <?php $cur = odbc_exec($con, "select id_convenio, descricao from sf_convenios where id_conv_plano is null and ativo = 0 order by descricao") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['id_convenio'] ?>"<?php
                                                                if (!(strcmp($RFP['id_convenio'], $txtIsencaoIE))) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>><?php echo utf8_encode($RFP['descricao']); ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>   
                                                    <div style="width: 49%; float: left;">
                                                        <label><b>Possibilitar Cotação para:</b></label>
                                                        <select id="txtPessoaCotacaoTipo" name="txtPessoaCotacaoTipo">
                                                            <option value="0" <?php
                                                            if ($txtPessoaCotacaoTipo == "0") {
                                                                echo "SELECTED";
                                                            }
                                                            ?>>Pessoa Física</option>
                                                            <option value="1" <?php
                                                            if ($txtPessoaCotacaoTipo == "1") {
                                                                echo "SELECTED";
                                                            }
                                                            ?>>Pessoa Jurídica</option>
                                                            <option value="2" <?php
                                                            if ($txtPessoaCotacaoTipo == "2") {
                                                                echo "SELECTED";
                                                            }
                                                            ?>>Pessoa Física e Jurídica</option>
                                                        </select>
                                                    </div>
                                                    <div style="width: 50%; float: left; margin-left: 1%">
                                                        <label><b>Modelo padrão whatsapp</b></label>
                                                        <select id="txtPortalWhatsapp" name="txtPortalWhatsapp">
                                                            <option value="null">Selecione</option>
                                                            <?php $cur = odbc_exec($con, "select id_email, descricao from sf_emails where tipo = 2") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['id_email'] ?>"<?php
                                                                if (!(strcmp($RFP['id_email'], $portal_whatsapp))) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <b>Assinatura de Contrato</b><br>                                                                                                            
                                                    <div style="width: 100%; float: left;">
                                                        <label>Contrato Agrupado</label>
                                                        <select id="txtContratoAgrp" name="txtContratoAgrp">
                                                            <option value="null">Selecione</option>
                                                            <?php $cur = odbc_exec($con, "select id, descricao from sf_contratos where assinatura = 1 and inativo = 0 order by descricao") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['id'] ?>"<?php
                                                                if (!(strcmp($RFP['id'], $txtContratoAgrp))) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>><?php echo utf8_encode($RFP['descricao']); ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>                                                    
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td>
                                                                <b>Agenda</b>
                                                            </td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top: 5px">
                                                                    <input name="txtAdmAgenUmamar" id="txtAdmAgenUmamar" value="1" type="checkbox" <?php
                                                                    if ($txtAdmAgenUmamar == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Apenas um agendamento                                                                  
                                                                <select name="txtAgenUmTipo" id="txtAgenUmTipo" style="width: 75px; margin-top: -6px;">
                                                                    <option value="0" <?php echo ($txtAgenUmTipo == "0" ? "SELECTED" : "");?>>Diário</option>
                                                                    <option value="1" <?php echo ($txtAgenUmTipo == "1" ? "SELECTED" : "");?>>Mensal</option>
                                                                </select>
                                                                por cliente</div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtAdmAgenHmarc" id="txtAdmAgenHmarc" value="1" type="checkbox" <?php
                                                                    if ($txtAdmAgenHmarc == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Não permitir agendamento com horários marcados</div>
                                                            </td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtAdmCliAtivos" id="txtAdmCliAtivos" value="1" type="checkbox" <?php
                                                                    if ($txtAdmCliAtivos == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Permitir apenas agendamentos por clientes ativos</div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div style="float: left; width: 32%;">
                                                                    <label>N° Agend. p/período</label>
                                                                    <input id="txtAgenPeriodo" name="txtAgenPeriodo" type="text" class="input-medium" maxlength="2" value="<?php echo $txtAgenPeriodo; ?>"/>
                                                                </div>
                                                                <div style="float: left; width: 33%; margin-left: 1%;">
                                                                    <label>Intervalo de horários</label>
                                                                    <select id="txtAgenIntervalo" name="txtAgenIntervalo">
                                                                        <option value="0" <?php echo ($txtAgenIntervalo == "0" ? "SELECTED" : "");?>>Indefinindo</option>
                                                                        <option value="30" <?php echo ($txtAgenIntervalo == "30" ? "SELECTED" : "");?>>30 min</option>
                                                                        <option value="60" <?php echo ($txtAgenIntervalo == "60" ? "SELECTED" : "");?>>Por hora</option>
                                                                    </select>
                                                                </div>
                                                                <div style="float: left; width: 33%; margin-left: 1%;">
                                                                    <label>N° Agend. p/semana</label>
                                                                    <input id="txtAgenSemanal" name="txtAgenSemanal" type="text" class="input-medium" maxlength="4" value="<?php echo $txtAgenSemanal; ?>"/>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>                                                                                                        
                                                </div>                                                
                                                <div style="margin-top:5px;width: 33%;float: left; margin-left: 1%;">
                                                    <table>
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Geral</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtContratacaoVenda" id="txtContratacaoVenda" value="1" type="checkbox" <?php
                                                                    if ($txtContratacaoVenda == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Ativar portal de contratação online (clientes)</div>
                                                            </td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtTerminalVenda" id="txtTerminalVenda" value="1" type="checkbox" <?php
                                                                    if ($txtTerminalVenda == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Ativar terminal de pagamento online (Cartão)</div>
                                                            </td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtDependenteParentesco" id="txtDependenteParentesco" value="1" type="checkbox" <?php
                                                                    if ($txtDependenteParentesco == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Cadastro de dependentes apenas por regras de parentesco</div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtPlanoUnico" id="txtPlanoUnico" value="1" type="checkbox" <?php
                                                                    if ($txtPlanoUnico == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Permitir apenas um único plano por cliente</div>
                                                            </td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtExcecaoPJ" id="txtExcecaoPJ" value="1" type="checkbox" <?php
                                                                    if ($txtExcecaoPJ == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Exceção de pagamentos para pessoas jurídicas</div>
                                                            </td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtAdesaoDep" id="txtAdesaoDep" value="1" type="checkbox" <?php
                                                                    if ($txtAdesaoDep == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Taxa de adesão para novos dependentes</div>
                                                            </td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtAddDep" id="txtAddDep" value="1" type="checkbox" <?php
                                                                    if ($txtAddDep == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Não adicionar novos dependentes</div>
                                                            </td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtAgruparPlanItem" id="txtAgruparPlanItem" value="1" type="checkbox" <?php
                                                                    if ($txtAgruparPlanItem == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Ativar portal on demand</div>
                                                            </td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtPlanMensPlanContrat" id="txtPlanMensPlanContrat" value="1" type="checkbox" <?php
                                                                    if ($txtPlanMensPlanContrat == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Plano mensal (com itens), Plano contratual (sem itens)</div>
                                                            </td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtDescontoPorRange" id="txtDescontoPorRange" value="1" type="checkbox" <?php
                                                                    if ($txtDescontoPorRange == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Desconto por Range (Desconto / Comissão)</div>
                                                            </td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtNaoAlterarTipoPag" id="txtNaoAlterarTipoPag" value="1" type="checkbox" <?php
                                                                    if ($txtNaoAlterarTipoPag == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Não alterar Tipo de Pagamento</div>
                                                            </td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtNaoProcPagOnline" id="txtNaoProcPagOnline" value="1" type="checkbox" <?php
                                                                    if ($txtNaoProcPagOnline == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Não processar Pagamento Online</div>
                                                            </td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>
                                                                <div style="float:left; margin-top:0">
                                                                    <input name="txtNaoValorPrint" id="txtNaoValorPrint" value="1" type="checkbox" <?php
                                                                    if ($txtNaoValorPrint == 1) {
                                                                        echo "CHECKED";
                                                                    }
                                                                    ?> class="input-medium"/>
                                                                </div>
                                                                <div style="float:left; margin:2px 5px">Não exibir valores dos itens na proposta (pdf)</div>
                                                            </td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>
                                                                
                                                            </td>
                                                        </tr>                                                                                                                                            
                                                    </table>
                                                </div>
                                                <div style="margin-top:5px;width: 41%;float: left">
                                                    <div class="data-fluid tabbable">
                                                        <ul class="nav nav-tabs" style="margin-bottom:0">
                                                            <li class="active"><a href="#tab51" data-toggle="tab"><b>Rótulos</b></a></li>
                                                            <li><a href="#tab52" data-toggle="tab"><b>Documentos</b></a></li>
                                                            <li><a href="#tab53" data-toggle="tab"><b>Cotação</b></a></li>
                                                            <li><a href="#tab54" data-toggle="tab"><b>Carteirinha</b></a></li>
                                                            <li><a href="#tab55" data-toggle="tab"><b>Termos</b></a></li>
                                                            <li><a href="#tab56" data-toggle="tab"><b>Distribuição</b></a></li>
                                                       </ul>
                                                    </div>
                                                    <div class="tab-content" style="min-height: 300px;border:1px solid #ddd; border-top:0; background:#F6F6F6">
                                                        <div class="tab-pane active" id="tab51" style="margin-bottom:5px">
                                                            <b>Mensagem de Boas Vindas</b><br>
                                                            <input type="text" id="txtMsgBoasVindas" name="txtMsgBoasVindas" maxlength="512" value="<?php echo $txtMsgBoasVindas; ?>">
                                                            <div style="float: left; width: 49%;">
                                                                <b>Taxa de Adesão</b><br>
                                                                <input type="text" id="txtTaxaAdesao" name="txtTaxaAdesao" maxlength="512" value="<?php echo $txtTaxaAdesao; ?>">
                                                            </div>
                                                            <div style="float: left; width: 50%; margin-left: 1%;">
                                                                <b>Associado</b><br>
                                                                <input type="text" id="txtAssociado" name="txtAssociado" maxlength="512" value="<?php echo $txtAssociado; ?>">
                                                            </div>
                                                            <b>Mensagem para Proposta</b><br>
                                                            <textarea style="width:100%;" id="txtMsgProposta" name="txtMsgProposta"><?php echo $txtMsgProposta; ?></textarea>
                                                            <br>                                                     
                                                            <b>Termos E-mail/SMS</b><br>
                                                            <input type="text" id="txtMsgTermosEmail" name="txtMsgTermosEmail" maxlength="512" value="<?php echo $txtMsgTermosEmail; ?>"><br>
                                                            <b>Termos WhatsApp</b><br>
                                                            <input type="text" id="txtMsgTermosWhats" name="txtMsgTermosWhats" maxlength="512" value="<?php echo $txtMsgTermosWhats; ?>"><br>                                                                                                    
                                                            <b>Link ATENDIMENTO (Meus Pagamentos)</b><br>
                                                            <input type="text" id="txtLinkWhatsPag" name="txtLinkWhatsPag" maxlength="1024" value="<?php echo $txtLinkWhatsPag; ?>"><br>                                                                                                                                                                                                            
                                                        </div>
                                                        <div class="tab-pane" id="tab52" style="margin-bottom:5px">
                                                            <?php include "../CRM/include/Documentos.php";?>   
                                                        </div>
                                                        <div class="tab-pane" id="tab53" style="margin-bottom:5px">
                                                            <span><b>Imprimir Cotação (.pdf)</b></span>
                                                            <hr style="margin:2px 0; border-top:1px solid #DDD"/>
                                                            <div style="width:100%;">    
                                                                <div style="background:#F1F1F1; border:1px solid #DDD; padding:10px;vertical-align: top;">
                                                                    <select name="txtCotacaoTipo" id="txtCotacaoTipo" style="width: 150px;">
                                                                        <option value="anterior.pdf">Anterior (.pdf)</option>
                                                                        <option value="marca-d-agua.png">Marca-d'água (.png)</option>
                                                                        <option value="posterior.pdf">Posterior (.pdf)</option>
                                                                    </select>
                                                                    <input type="file" name="fileCotacao" id="fileCotacao"/>
                                                                    <button class="btn btn-success" title="Enviar" type="button" style="float: right;" onclick="AdicionarLink();" name="bntSendCotacao"><span class="ico-checkmark"></span></button>
                                                                </div>
                                                            </div>
                                                            <div style="width:100%;">
                                                                <table id="tbCotacao" class="table" cellpadding="0" cellspacing="0" width="100%">
                                                                    <thead>
                                                                        <tr>
                                                                            <th width="30%">Data de Upload</th>
                                                                            <th width="50%">Nome do Arquivo</th>
                                                                            <th width="10%"><center>Tamanho</center></th>
                                                                            <th width="10%"><center>Ação</center></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td colspan="4" class="dataTables_empty"></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <div style="clear:both"></div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab54" style="margin-bottom:5px">
                                                            <span><b>Carteira do Associado</b></span>
                                                            <br>
                                                            <button type="button" name="bntAdicionaCarteira" onclick="AbrirCarteira(0)" class="btn btn-success" style="height:27px; margin:0; padding-top:6px;float: left;"><span class="ico-plus"></span></button>
                                                            <table id="tbCarteira" class="table" cellpadding="0" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th width="90%">Descrição</th>
                                                                        <th width="10%"><center>Ação</center></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="2" class="dataTables_empty"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="tab-pane" id="tab55" style="margin-bottom:5px">
                                                            <b>Controle de Termos</b><br>
                                                            <button type="button" name="bntAdicionaAcrSer" onclick="AbrirTermo(0)" class="btn btn-success" style="height:27px; margin:0; padding-top:6px;float: left;"><span class="ico-plus"></span></button>
                                                            <table id="tbTermos" class="table" cellpadding="0" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th width="90%">Nome do Termo</th>
                                                                        <th width="10%"><center>Ação</center></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="2" class="dataTables_empty"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="tab-pane" id="tab56" style="margin-bottom:5px">
                                                            <b>Distribuição automática de Prospects</b><br>
                                                            <select id="txtUsuario" name="txtUsuario" style="float: left; width: 89%; margin-right: 1%;">
                                                                <option value="null">Selecione</option>
                                                                <?php $cur = odbc_exec($con, "select id_usuario, nome from sf_usuarios where inativo = 0 and id_usuario > 1 order by nome") or die(odbc_errormsg());
                                                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                    <option value="<?php echo $RFP['id_usuario']; ?>"><?php echo formatNameCompact(strtoupper(utf8_encode($RFP['nome']))); ?></option>
                                                                <?php } ?>
                                                            </select>
                                                            <button type="button" name="bntAddUsuario" onclick="AddUsuario(0)" class="btn btn-success" style="width: 10%; height:27px; margin:0; padding-top:6px;float: left;"><span class="ico-plus"></span></button>
                                                            <table id="tbUserProspects" class="table" cellpadding="0" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th width="90%">Usuário</th>
                                                                        <th width="10%"><center>Ação</center></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="2" class="dataTables_empty"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>                                                    
                                            </div>
                                            <div class="tab-pane" id="tab7" style="margin-bottom:5px">
                                                <div style="margin-top:5px;float: left;width: 69%">
                                                    <div style="float:left;width: 99%;margin-top: 10px;">
                                                        <b>Gerenciador (DCC)</b>
                                                    </div>
                                                    <div style="float: left;width: 30%;">
                                                        <label>Empresa (01)</label>
                                                        <select name="txtEmpresaDCC" id="txtEmpresaDCC">
                                                            <option value="1" <?php
                                                            if ($txtEmpresaDCC == "1") {
                                                                echo "SELECTED";
                                                            }
                                                            ?>>1 - MUNDIPAGG</option>
                                                        </select>
                                                    </div>
                                                    <div style="float:left;width: 30%;margin-left: 1%;">
                                                        <label>Forma de Pagamento (01)</label>
                                                        <select name="txtDCCFormaPagamento" id="txtDCCFormaPagamento">
                                                            <option value="null" >Selecione</option>
                                                            <?php $cur = odbc_exec($con, "select id_tipo_documento,descricao from sf_tipo_documento where cartao_dcc in (1,2) and inativo = 0") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['id_tipo_documento'] ?>"<?php
                                                                if (!(strcmp($RFP['id_tipo_documento'], $formaPagamento))) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div style="float:left;width: 27%;margin-left: 1%;">
                                                        <label>Descrição (01)</label>
                                                        <input type="text" id="txtDescricaoDcc" name="txtDescricaoDcc" maxlength="20" value="<?php echo $txtDescricaoDcc; ?>">
                                                    </div>
                                                    <div style="float: left;width: 10%;margin-left: 1%;">
                                                        <label>Filial (01)</label>
                                                        <select name="txtFilialDCC" id="txtFilialDCC">                                                                                                                       
                                                            <?php $cur = odbc_exec($con, "select * from sf_filiais");
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo utf8_encode($RFP['id_filial']); ?>"<?php
                                                                if (!(strcmp($RFP['id_filial'], $filialDCC))) {
                                                                    echo "SELECTED";
                                                                } ?>><?php echo utf8_encode($RFP['numero_filial']); ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>                                                    
                                                    <div style="float: left;width: 30%;">
                                                        <label>Empresa (02)</label>
                                                        <select name="txtEmpresaDCC2" id="txtEmpresaDCC2">
                                                            <option value="1" <?php
                                                            if ($txtEmpresaDCC2 == "1") {
                                                                echo "SELECTED";
                                                            }
                                                            ?>>1 - MUNDIPAGG</option>
                                                        </select>
                                                    </div>
                                                    <div style="float:left;width: 30%;margin-left: 1%;">
                                                        <label>Forma de Pagamento (02)</label>
                                                        <select name="txtDCCFormaPagamento2" id="txtDCCFormaPagamento2">
                                                            <option value="null" >Selecione</option>
                                                            <?php $cur = odbc_exec($con, "select id_tipo_documento,descricao from sf_tipo_documento where cartao_dcc in (1,2) and inativo = 0") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['id_tipo_documento'] ?>"<?php
                                                                if (!(strcmp($RFP['id_tipo_documento'], $formaPagamento2))) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>                                                                                                        
                                                    <div style="float:left;width: 27%;margin-left: 1%;">
                                                        <label>Descrição (02)</label>
                                                        <input type="text" id="txtDescricaoDcc2" name="txtDescricaoDcc2" maxlength="20" value="<?php echo $txtDescricaoDcc2; ?>">
                                                    </div>                                                    
                                                    <div style="float: left;width: 10%;margin-left: 1%;">
                                                        <label>Filial (02)</label>
                                                        <select name="txtFilialDCC2" id="txtFilialDCC2">                                                                                                                       
                                                            <?php $cur = odbc_exec($con, "select * from sf_filiais");
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo utf8_encode($RFP['id_filial']); ?>"<?php
                                                                if (!(strcmp($RFP['id_filial'], $filialDCC2))) {
                                                                    echo "SELECTED";
                                                                } ?>><?php echo utf8_encode($RFP['numero_filial']); ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>                                                    
                                                    <div style="float: left;width: 30%;">
                                                        <label>Operadora Padrão</label>
                                                        <select name="txtDccOperadora" id="txtDccOperadora" class="input-medium" <?php echo $disabled; ?> style="width:100%;">
                                                            <option value="null">Selecione</option>
                                                            <?php $sql = "select dcc_descricao, dcc_descricao2 from sf_configuracao";
                                                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) {
                                                                if($RFP["dcc_descricao"] != "") { ?>
                                                                <option value="0"<?php if ($operadora_dcc == "0") {echo "SELECTED";}
                                                                ?>><?php echo utf8_encode($RFP["dcc_descricao"]) ?></option>
                                                                <?php } if($RFP["dcc_descricao2"] != "") { ?>
                                                                <option value="1"<?php if ($operadora_dcc == "1") {echo "SELECTED";}
                                                                ?>><?php echo utf8_encode($RFP["dcc_descricao2"]) ?></option>
                                                            <?php }} ?>
                                                        </select>
                                                    </div>
                                                    <div style="float: left;width: 12%;margin-left: 1%;">
                                                        <label>Ativar TEF</label>
                                                        <select name="txtDccTef" id="txtDccTef" class="input-medium" <?php echo $disabled; ?> style="width:100%;">
                                                            <option value="0" <?php if ($txtDccTef == "0") {echo "SELECTED";} ?>>Não</option>
                                                            <option value="1" <?php if ($txtDccTef == "1") {echo "SELECTED";} ?>>Sim</option>
                                                        </select>
                                                    </div>                                                                                                        
                                                    <div style="float: left;width: 17%;margin-left: 1%;">
                                                        <label>Retentativas (0 a 10 Dias)</label>
                                                        <input id="txtRecorrencia" name="txtRecorrencia" type="text" maxlength="2" style="width: 100%;" value="<?php echo $txtRecorrencia; ?>">
                                                    </div> 
                                                    <div style="float:left;width: 27%;margin-left: 1%;">
                                                        <label>Link de Pagamento</label>
                                                        <select name="txtLinkFormaPagamento" id="txtLinkFormaPagamento">
                                                            <option value="null" >Selecione</option>
                                                            <?php $cur = odbc_exec($con, "select id_tipo_documento,descricao from sf_tipo_documento where inativo = 0") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['id_tipo_documento'] ?>"<?php
                                                                if (!(strcmp($RFP['id_tipo_documento'], $linkFormaPagamento))) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div style="float: left;width: 10%;margin-left: 1%;">
                                                        <label>Max. Parcel. Link</label>
                                                        <input id="txtLinkMaxParc" name="txtLinkMaxParc" type="text" maxlength="2" style="width: 100%;" value="<?php echo $txtLinkMaxParc; ?>">
                                                    </div>                                                     
                                                    <div style="float:left;width: 99%;margin-top: 10px;">
                                                        <b>Gerenciador (BOL)</b>
                                                    </div>
                                                    <div style="float: left;width: 30%;">
                                                        <label>Empresa</label>
                                                        <select name="txtBolTipo" id="txtBolTipo">
                                                            <option value="0" <?php
                                                            if ($txtBolTipo == "0") {
                                                                echo "SELECTED";
                                                            }
                                                            ?>>0 - MUNDIPAGG</option>
                                                            <option value="1" <?php
                                                            if ($txtBolTipo == "1") {
                                                                echo "SELECTED";
                                                            }
                                                            ?>>1 - PAGAR.ME</option>
                                                            <option value="2" <?php
                                                            if ($txtBolTipo == "2") {
                                                                echo "SELECTED";
                                                            }
                                                            ?>>2 - GALAXPAY</option>
                                                        </select>
                                                    </div>
                                                    <div style="float:left;width: 30%;margin-left: 1%;">
                                                        <label>Forma de Pagamento</label>
                                                        <select name="txtDCCFormaPagamentoBol" id="txtDCCFormaPagamentoBol">
                                                            <option value="null" >Selecione</option>
                                                            <?php $cur = odbc_exec($con, "select id_tipo_documento,descricao from sf_tipo_documento where inativo = 0") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['id_tipo_documento'] ?>"<?php
                                                                if (!(strcmp($RFP['id_tipo_documento'], $formaPagamentoBol))) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div style="float: left;width: 13%;margin-left: 1%;">
                                                        <label>Ger. Antes Venc.</label>
                                                        <input id="txtDiasBoleto" name="txtDiasBoleto" type="text" maxlength="2" style="width: 100%;" value="<?php echo $txtDiasBoleto; ?>">
                                                    </div>                                                    
                                                    <div style="float: left;width: 13%;margin-left: 1%;">
                                                        <label>Prazo máximo</label>
                                                        <input id="txtDiasLimite" name="txtDiasLimite" type="text" maxlength="2" style="width: 100%;" value="<?php echo $txtDiasLimite; ?>">
                                                    </div> 
                                                    <div style="float: left;width: 10%;margin-left: 1%;">
                                                        <label>Ativar AUTO</label>
                                                        <select name="txtAtvBol" id="txtAtvBol" class="input-medium" <?php echo $disabled; ?> style="width:100%;">
                                                            <option value="0" <?php if ($txtAtvBol == "0") {echo "SELECTED";} ?>>Não</option>
                                                            <option value="1" <?php if ($txtAtvBol == "1") {echo "SELECTED";} ?>>Sim</option>
                                                        </select>
                                                    </div>                                                    
                                                    <div style="float:left;width: 99%;margin-top: 10px;">
                                                        <b>Gerenciador (PIX)</b>
                                                    </div>
                                                    <div style="float: left;width: 30%;">
                                                        <label>Empresa</label>
                                                        <select name="txtPixTipo" id="txtPixTipo">
                                                            <option value="1" <?php
                                                            if ($txtPixTipo == "1") {
                                                                echo "SELECTED";
                                                            }
                                                            ?>>1 - U4CRYPTO</option>
                                                            <option value="2" <?php
                                                            if ($txtPixTipo == "2") {
                                                                echo "SELECTED";
                                                            }
                                                            ?>>2 - GALAXPAY</option>
                                                        </select>
                                                    </div>
                                                    <div style="float:left;width: 30%;margin-left: 1%;">
                                                        <label>Forma de Pagamento</label>
                                                        <select name="txtDCCFormaPagamentoPix" id="txtDCCFormaPagamentoPix">
                                                            <option value="null" >Selecione</option>
                                                            <?php $cur = odbc_exec($con, "select id_tipo_documento,descricao from sf_tipo_documento where inativo = 0") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['id_tipo_documento'] ?>"<?php
                                                                if (!(strcmp($RFP['id_tipo_documento'], $formaPagamentoPix))) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div> 
                                                    <div style="float:left;width: 99%;margin-top: 10px;">
                                                        <b>Notificações (Webhook)</b>
                                                    </div>
                                                    <div style="float: left;width: 30%;">
                                                        <label>Departamento</label>
                                                        <select name="txtDepOnline" id="txtDepOnline">
                                                            <option value="null" >Selecione</option>
                                                            <?php $cur = odbc_exec($con, "select id_departamento,nome_departamento from sf_departamentos") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['id_departamento'] ?>"<?php
                                                                if (!(strcmp($RFP['id_departamento'], $txtDepOnline))) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>><?php echo utf8_encode($RFP['nome_departamento']) ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>                                                    
                                                </div>
                                                <div style="margin-top:5px;float: left;width: 30%;margin-left: 1%">
                                                    <div style="float:left;width: 99%;margin-top: 10px;">
                                                        <b>Chaves Clientes</b>
                                                    </div>                                                    
                                                    <div style="float:left;width: 100%">
                                                        <label>Mundipagg (01)</label>
                                                        <input type="text" id="txtChaveDCC" name="txtChaveDCC" maxlength="50" value="<?php echo $txtChaveDcc; ?>">
                                                    </div>                                                    
                                                    <div style="float:left;width: 100%">
                                                        <label>Mundipagg (02)</label>
                                                        <input type="text" id="txtChaveDCC2" name="txtChaveDCC2" maxlength="50" value="<?php echo $txtChaveDcc2; ?>">
                                                    </div>
                                                    <div style="float:left;width: 100%">
                                                        <label>Pagar.me</label>
                                                        <input type="text" id="txtChavePgme" name="txtChavePgme" maxlength="50" value="<?php echo $txtChavePgme; ?>">
                                                    </div>
                                                    <div style="float:left;width: 100%">
                                                        <label>U4crypto</label>
                                                        <input type="text" id="txtChavePix" name="txtChavePix" maxlength="50" value="<?php echo $txtChavePix; ?>">
                                                    </div>                                                    
                                                    <div style="float:left;width: 60%">
                                                        <label>Galaxpay</label>
                                                        <input type="text" id="txtChaveGalax" name="txtChaveGalax" maxlength="50" value="<?php echo $txtChaveGalax; ?>">
                                                    </div>                                                    
                                                    <div style="float:left;width: 15%;margin-left: 1%">
                                                        <label>GalaxId</label>
                                                        <input type="text" id="txtChaveGalaxId" name="txtChaveGalaxId" maxlength="6" value="<?php echo $txtChaveGalaxId; ?>">
                                                    </div>
                                                    <div style="float: left;width: 23%;margin-left: 1%;">
                                                        <label>Teste</label>
                                                        <select name="txtGalaxTeste" id="txtGalaxTeste" class="input-medium" <?php echo $disabled; ?> style="width:100%;">
                                                            <option value="0" <?php if ($txtGalaxTeste == "0") {echo "SELECTED";} ?>>Não</option>
                                                            <option value="1" <?php if ($txtGalaxTeste == "1") {echo "SELECTED";} ?>>Sim</option>
                                                        </select>
                                                    </div>                                                    
                                                </div>
                                                <div style="clear:both; height:5px"></div>                                                                                                                                                                                                    
                                            </div>
                                            <div class="tab-pane" id="tab8" style="margin-bottom:5px">
                                                <div style="width: 49%; float: left;">
                                                    <div style="margin-top:5px">
                                                        <table>
                                                            <tr style="line-height:25px">
                                                                <td>
                                                                    <b>Configurações de GYMPASS</b>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div style="float: left; width: 50%;">
                                                            <label>Gym Number</label>
                                                            <input type="text" id="txtRecid" name="txtRecid" maxlength="10" style="width: 100%;" value="<?php echo $txtRecid; ?>">
                                                        </div>
                                                        <div style="float:left; width: 49%; margin-left: 1%;">
                                                            <label>Pass Type Number</label>
                                                            <input type="text" id="txtPtype" name="txtPtype" maxlength="2" style="width: 100%;" value="<?php echo $txtPtype; ?>">
                                                        </div>
                                                        <div style="float: left; width: 100%;">
                                                            <label>Auth Token</label>
                                                            <input type="text" id="txtAuthToken" name="txtAuthToken" maxlength="32" style="width: 100%;" value="<?php echo $txtAuthToken; ?>">
                                                        </div>
                                                        <div style="clear:both; height:5px"></div>
                                                    </div>                                                    
                                                    <div style="margin-top:5px">
                                                        <table>
                                                            <tr style="line-height:25px">
                                                                <td>
                                                                    <b>Configurações de SMS</b>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div style="float: left; width: 50%;">
                                                            <label>Usuário</label>
                                                            <input type="text" id="txtUsuarioSMS" name="txtUsuarioSMS" maxlength="50" style="width: 100%;" value="<?php echo $txtUsuarioSMS; ?>">
                                                        </div>
                                                        <div style="float: left; width: 49%; margin-left: 1%;">
                                                            <label>Senha</label>
                                                            <input type="password" id="txtSenhaSMS" name="txtSenhaSMS" maxlength="50" style="width: 100%;" value="<?php echo $txtSenhaSMS; ?>">
                                                        </div>
                                                        <div style="float: left; width: 100%;">
                                                            <label>Identificador</label>
                                                            <input type="text" id="txtIdentificadorSMS" name="txtIdentificadorSMS" maxlength="15" style="width: 100%;" value="<?php echo $txtIdentificadorSMS; ?>">
                                                        </div>
                                                        <div style="clear:both; height:5px"></div>
                                                    </div>
                                                    <table style="margin-top: 10px;">
                                                        <tr style="line-height:25px">
                                                            <td>
                                                                <b>Configurações Regionais</b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div>
                                                        <label>Datas e Valores Monetários</label>
                                                        <select name="txtConfRegionais" id="txtConfRegionais" style="width:143px">
                                                            <option value="pt" <?php echo ($txtConfRegionais == "pt" ? "SELECTED" : "");?>>Português</option>
                                                            <option value="an" <?php echo ($txtConfRegionais == "an" ? "SELECTED" : "");?>>Português - Angola</option>
                                                            <option value="en" <?php echo ($txtConfRegionais == "en" ? "SELECTED" : "");?>>Inglês</option>
                                                        </select>
                                                    </div>
                                                </div>                                                
                                                <div style="width: 49%; margin-left: 1%; float: left;">
                                                    <div style="margin-top:5px">
                                                        <table>
                                                            <tr style="line-height:25px">
                                                                <td>
                                                                    <b>Configurações de GYMPASS 2</b>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div style="float: left; width: 50%;">
                                                            <label>Gym Number</label>
                                                            <input type="text" id="txtRecid2" name="txtRecid2" maxlength="10" style="width: 100%;" value="<?php echo $txtRecid2; ?>">
                                                        </div>
                                                        <div style="float:left; width: 49%; margin-left: 1%;">
                                                            <label>Pass Type Number</label>
                                                            <input type="text" id="txtPtype2" name="txtPtype2" maxlength="2" style="width: 100%;" value="<?php echo $txtPtype2; ?>">
                                                        </div>
                                                        <div style="float: left; width: 100%;">
                                                            <label>Auth Token</label>
                                                            <input type="text" id="txtAuthToken2" name="txtAuthToken2" maxlength="32" style="width: 100%;" value="<?php echo $txtAuthToken2; ?>">
                                                        </div>
                                                        <div style="clear:both; height:5px"></div>
                                                    </div>                                                    
                                                    <div style="margin-top:5px">
                                                        <table style="width: 100%;">
                                                            <tr style="line-height:25px">
                                                                <td>
                                                                    <b>Assistência 24h</b>
                                                                </td>
                                                                <td style="text-align: right;">
                                                                    <a href="javascript:void(0)" onclick="Dessincronizar()">dessincronizar</a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div style="float:left; width: 25%;">
                                                            <label>Sistema</label>
                                                            <select id="txtWaTipo" name="txtWaTipo" style="width: 100%">
                                                                <option value="0" <?php echo ($txtWaTipo == "0" ? "selected" : ""); ?>>Infornet</option>
                                                                <option value="1" <?php echo ($txtWaTipo == "1" ? "selected" : ""); ?>>Assistme</option>
                                                                <option value="2" <?php echo ($txtWaTipo == "2" ? "selected" : ""); ?>>Synchrony</option>
                                                                <option value="3" <?php echo ($txtWaTipo == "3" ? "selected" : ""); ?>>Coletiva</option>
                                                                <option value="4" <?php echo ($txtWaTipo == "4" ? "selected" : ""); ?>>Veniti</option>
                                                                <option value="5" <?php echo ($txtWaTipo == "5" ? "selected" : ""); ?>>Amparo</option>
                                                                <option value="6" <?php echo ($txtWaTipo == "6" ? "selected" : ""); ?>>Hinova</option>
                                                                <option value="7" <?php echo ($txtWaTipo == "7" ? "selected" : ""); ?>>Ln Soft</option>
                                                                <option value="8" <?php echo ($txtWaTipo == "8" ? "selected" : ""); ?>>Delta Global</option>
                                                                <option value="9" <?php echo ($txtWaTipo == "9" ? "selected" : ""); ?>>Facilita Vitae</option>
                                                            </select>
                                                        </div>                                                        
                                                        <div style="float:left; width: 24%; margin-left: 1%;">
                                                            <label>Ambiente</label>
                                                            <select id="txtWaAmbiente" name="txtWaAmbiente" style="width: 100%">
                                                                <option value="0" <?php echo ($txtWaAmbiente == "0" ? "selected" : ""); ?>>Ambiente de Teste</option>
                                                                <option value="1" <?php echo ($txtWaAmbiente == "1" ? "selected" : ""); ?>>Ambiente de Produção</option>
                                                            </select>
                                                        </div>                                                        
                                                        <div style="float: left; width: 24%; margin-left: 1%;">
                                                            <label>Empresa</label>
                                                            <input type="text" id="txtWaEmpresa" name="txtWaEmpresa" maxlength="512" style="width: 100%" value="<?php echo $txtWaEmpresa; ?>">
                                                        </div>
                                                        <div style="float: left; width: 24%; margin-left: 1%;">
                                                            <label>Login</label>
                                                            <input type="text" id="txtWaLogin" name="txtWaLogin" maxlength="512" style="width: 100%" value="<?php echo $txtWaLogin; ?>">
                                                        </div>
                                                        <div style="float: left; width: 50%;">
                                                            <label>Senha</label>
                                                            <input type="text" id="txtWaSenha" name="txtWaSenha" maxlength="512" style="width: 100%" value="<?php echo $txtWaSenha; ?>">
                                                        </div>                                                        
                                                        <div style="float: left; width: 49%; margin-left: 1%;">
                                                            <label>Chave</label>
                                                            <input type="text" id="txtWaChave" name="txtWaChave" maxlength="512" style="width: 100%" value="<?php echo $txtWaChave; ?>">
                                                        </div>
                                                        <div style="float: left; margin-top: 2%; width: 100%;">
                                                            <input id="idRefreshWa" name="idRefreshWa" type="hidden" value="<?php echo substr_replace($idRefreshWa, "", -1); ?>"/>
                                                            <label id="lblRefreshWa" style="width: 51%;float: left;">Veículos para Sincronia 0</label>                                                            
                                                            <button id="btnProdutosWa" onclick="window.open('../Seguro/ajax/assistencia.php?functionPage=getNegociacoes','_blank')" style="float: right;" class="btn yellow" title="Listar Negociações" type="button"><span class="ico-box"></span></button>
                                                            <button id="btnVeiculosWa" onclick="window.open('../Seguro/ajax/assistencia.php?functionPage=getTodosBeneficiarios','_blank')" style="float: right; margin-right: 1%;" class="btn btn-success" title="Listar Beneficiários" type="button"><span class="ico-box"></span></button>
                                                            <button id="btnRefreshWa" style="float: right; margin-right: 1%;" name="btnRefreshWa" class="btn turquoise" title="Atualizar" type="button"><span class="ico-refresh"></span> Atualizar</button>
                                                        </div>                                                        
                                                        <div style="clear:both; height:5px"></div>
                                                    </div>                                                    
                                                    <div style="margin-top:5px">
                                                        <table style="width: 100%;">
                                                            <tr style="line-height:25px">
                                                                <td>
                                                                    <b>Rastreamento</b>
                                                                </td>
                                                                <td style="text-align: right;">
                                                                    <a href="javascript:void(0)" onclick="DessincronizarRa()">dessincronizar</a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div style="float:left; width: 25%;">
                                                            <label>Sistema</label>
                                                            <select id="txtRaTipo" name="txtRaTipo" style="width: 100%">
                                                                <option value="0" <?php echo ($txtRaTipo == "0" ? "selected" : ""); ?>>Hapolo</option>
                                                            </select>
                                                        </div>                                                        
                                                        <div style="float:left; width: 24%; margin-left: 1%;">
                                                            <label>Ambiente</label>
                                                            <select id="txtRaAmbiente" name="txtRaAmbiente" style="width: 100%">
                                                                <option value="0" <?php echo ($txtRaAmbiente == "0" ? "selected" : ""); ?>>Ambiente de Teste</option>
                                                                <option value="1" <?php echo ($txtRaAmbiente == "1" ? "selected" : ""); ?>>Ambiente de Produção</option>
                                                            </select>
                                                        </div>                                                                                                                
                                                        <div style="float: left; width: 49%; margin-left: 1%;">
                                                            <label>Token</label>
                                                            <input type="text" id="txtRaChave" name="txtRaChave" maxlength="512" style="width: 100%" value="<?php echo $txtRaChave; ?>">
                                                        </div>
                                                        <div style="float: left; margin-top: 2%; width: 100%;">
                                                            <input id="idRefreshRa" name="idRefreshRa" type="hidden" value="<?php echo substr_replace($idRefreshRa, "", -1); ?>"/>
                                                            <label id="lblRefreshRa" style="width: 51%;float: left;">Veículos para Sincronia 0</label>                                                            
                                                            <button id="btnVeiculosRa" onclick="window.open('../Seguro/ajax/rastreamento.php?functionPage=getTipoVeiculos','_blank')" style="float: right;" class="btn yellow" title="Tipo de Veículos" type="button"><span class="ico-box"></span></button>
                                                            <button id="btnRastreadoresRa" onclick="window.open('../Seguro/ajax/rastreamento.php?functionPage=getTodosRastreadores','_blank')" style="float: right; margin-right: 1%;" class="btn btn-success" title="Listar Rastreadores" type="button"><span class="ico-box"></span></button>
                                                            <button id="btnOperadorasRa" onclick="window.open('../Seguro/ajax/rastreamento.php?functionPage=getTodosOperadoras','_blank')" style="float: right; margin-right: 1%;" class="btn btn-danger" title="Listar Operadoras" type="button"><span class="ico-box"></span></button>
                                                            <button id="btnRefreshRa" style="float: right; margin-right: 1%;" name="btnRefreshRa" class="btn turquoise" title="Atualizar" type="button"><span class="ico-refresh"></span> Atualizar</button>
                                                        </div>                                                        
                                                        <div style="clear:both; height:5px"></div>
                                                    </div>
                                                </div>                                                                                                
                                            </div>
                                            <div class="tab-pane" id="tab9" style="margin-bottom:5px">
                                                <div style="margin-top:5px;float: left;width: 49%">
                                                    <div style="float:left;width: 99%;margin-bottom: 25px;">
                                                        <b>Cadastro de Campos Livres</b>
                                                    </div>
                                                    <?php for($i = 0; $i < 10; $i++) { ?>
                                                    <div style="float: left;width: 100%;margin-bottom: 5px;">
                                                        <input id="txtidCLivres<?php echo $i;?>" name="txtidCLivres<?php echo $i;?>" type="hidden" value="<?php echo $camposLivres[$i]["id_campo"];?>"/><span style="margin-right: 10px;font-weight: bold;">Campo <?php echo str_pad(($i + 1), 2, "0", STR_PAD_LEFT);?></span>
                                                        <input id="ckbCLivres<?php echo $i;?>" name="ckbCLivres<?php echo $i;?>" <?php echo ($camposLivres[$i]["ativo_campo"] == "1" ? "checked" : "");?> onchange="ativarCampo(<?php echo $i;?>)" value="1" type="checkbox"/><span>Ativo</span>
                                                        <input name="opbCLivres" value="<?php echo $i;?>" type="radio" <?php echo ($camposLivres[$i]["busca_campo"] == "1" ? "checked" : "");?>/><span>Buscar</span>
                                                        <input id="txtCLivres<?php echo $i;?>" name="txtCLivres<?php echo $i;?>" value="<?php echo $camposLivres[$i]["descricao_campo"];?>" <?php echo ($camposLivres[$i]["ativo_campo"] == "1" ? "" : "readonly");?> type="text" style="margin-left: 10px;width: 50%" maxlength="64"/>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                                <div style="margin-top:5px;float: left;width: 49%">
                                                    <div style="float:left;width: 99%;margin-bottom: 25px;">
                                                        <b>Mensagens para tela de acesso</b>
                                                    </div>
                                                    <?php $cur = odbc_exec($con, "select id, descricao, conteudo from sf_mensagens_acesso order by id") or die(odbc_errormsg());
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <div style="float: left; width: 100%; margin-bottom: 5px;">                                                    
                                                            <div style="float: left; width: 27%; text-align: right; margin-top: 5px;">
                                                                <span style="font-weight: bold;"><?php echo utf8_encode($RFP["descricao"]); ?></span>
                                                            </div>        
                                                            <div style="float: left;width: 73%;">
                                                                <input id="txtMensagem_<?php echo $RFP["id"]; ?>" name="txtMensagem_<?php echo $RFP["id"]; ?>" value="<?php echo utf8_encode($RFP["conteudo"]); ?>" autocomplete="off" type="text" style="margin-left: 10px;width: 70%" maxlength="64"/>
                                                            </div>                                                                                                                                
                                                        </div>                                                        
                                                    <?php } ?>                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="linha" style="margin-top:8px; border:1px solid #ddd">
                                        <div class="btn-group" style="width:99%" align="right">
                                            <button class="btn btn-success" type="submit" name="bntSave" id="bntOK" style="margin-right:5px" onclick="return validaForm()"><span class="icon-ok icon-white"></span> Gravar</button>
                                            <button class="btn yellow" onClick="window.top.location.href = 'FormConfiguracao.php'" id="bntCancel"><span class="icon-backward icon-white"></span> Cancelar</button>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" style="display: none;" title="Processando">
            <div id="progressbar"></div>
            <div style="display: flex;align-items: center;justify-content: center;margin-top: 20px;">
                <div id="pgIni">0</div><div style="padding: 5px;">até</div><div id="pgFim">0</div>            
            </div>            
        </div>        
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/colpick.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type="text/javascript" src="../../js/jquery.cpfcnpj.min.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>
        <script type="text/javascript" src="js/ConfiguracaoForm.js"></script>
    </body>
</html>