<?php
require_once(__DIR__ . './../../Connections/configini.php');
include "form/ContratosMakeFormServer.php";
include "modelos/variaveis.php";
$classDisable = ($disabled != "" ? "disableCntt" : "");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <style>
            body { font-family: sans-serif; }
            div label input {
                margin-right: 100px;
            }
            label {
                float: left;
                width: 26px;
                cursor: pointer;
                background: #EFEFEF;
                margin: 2px 0 2px 1px;
                border: 1px solid #D0D0D0;
                width: 96%;
            }
            label span {
                display: block;
                padding: 3px 0;
                text-align: center;
            }
            label input {
                top: -20px;
                position: absolute;
            }
            input:checked + span {
                color: #FFF;
                background: #186554;
            }
            .aux{
                position:absolute;
                width:120px;
                background-color: lightgray;
                z-index:100;
            }
            #selText {
                background-color: yellow;
                width: 100%;
            }
            .disableCntt{
                background: #eee;
                color: #b6b6b6;
                pointer-events: none;
            }      
            .variaveis .dropdown{
                width: 19%;
            }
            .variaveis .dropdown .dropdown-toggle{
                width: 100%;
            }
            .variaveis .dropdown-menu{
                width: 100%;
                max-height: 420px;
            }
            .dropdown-submenu {
                position:relative;
            }
            .dropdown-submenu>.dropdown-menu {
                top:0;
                left:100%;
                margin-top:-5px;
                margin-left: 40px;
                -webkit-border-radius:0 6px 6px 6px;
                -moz-border-radius:0 6px 6px 6px;
                border-radius:0 6px 6px 6px;
            }
            .dropdown-submenu:hover>.dropdown-menu {
                display:block;
            }
            .dropdown-submenu>a:after {
                display:block;
                content:" ";
                float:right;
                width:0;
                height:0;
                border-color:transparent;
                border-style:solid;
                border-width:5px 0 5px 5px;
                border-left-color:#cccccc;
                margin-top:5px;
                margin-right:-10px;
            }
            .dropdown-submenu:hover>a:after {
                border-left-color:#ffffff;
            }
            .dropdown-submenu.pull-left {
                float:none;
            }
            .dropdown-submenu.pull-left>.dropdown-menu {
                left:-100%;
                margin-left:10px;
                -webkit-border-radius:6px 0 6px 6px;
                -moz-border-radius:6px 0 6px 6px;
                border-radius:6px 0 6px 6px;
            }
            table.table-sem-linha {

            }
        </style>
    </head>
    <body>
        <form action="ContratoMakeForm.php" name="frmEnviaDados" id="frmEnviaDados" method="POST">
            <div class="frmhead">
                <div class="frmtext">Contrato</div>
                <div class="frmicon" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input name="txtId" id="txtId" type="hidden" value="<?php echo $id; ?>"/>
                <input name="id_contrato" id="id_contrato" type="hidden" value="<?php echo $_GET["id_contrato"]; ?>"/>
                <div style="height:42px;display:flex;justify-content: space-between;">
                    <div style="width:50%;display:flex;justify-content: space-between;">
                        <div class="" style="width:20%;">
                            <span>Tipo:</span>
                            <select name="txtTipo" id="txtTipo" class="select" <?php echo $disabled; ?> style="width:100%">
                                <option value="1" <?php echo ($tipo == 1 ? "selected" : ""); ?>>Texto Livre</option>
                                <option value="2" <?php echo ($tipo == 2 ? "selected" : ""); ?>>Tabela</option>
                                <option value="3" <?php echo ($tipo == 3 ? "selected" : ""); ?>>Tabela Sem Bordas</option>
                            </select>
                        </div>
                        <div class="" style="width:30%;">
                            <span>Alinhar:</span>
                            <select name="txtAlign" id="txtAlign" class="select" <?php echo $disabled; ?> style="width:100%">
                                <option value="0" <?php echo ($align == 0 ? "selected" : ""); ?>>Justificado</option>
                                <option value="1" <?php echo ($align == 1 ? "selected" : ""); ?>>Centro</option>
                                <option value="2" <?php echo ($align == 2 ? "selected" : ""); ?>>Esquerda</option>
                                <option value="3" <?php echo ($align == 3 ? "selected" : ""); ?>>Direita</option>
                            </select>
                        </div>
                        <div class="" style="width:20%;">
                            <span>Tamanho Fonte:</span>
                            <select name="txtFontSize" id="txtFontSize" class="select" <?php echo $disabled; ?> style="width:100%">
                                <option value="6" <?php echo ($fontSize == 6 ? "selected" : ""); ?>>6</option>
                                <option value="7" <?php echo ($fontSize == 7 ? "selected" : ""); ?>>7</option>
                                <option value="8" <?php echo ($fontSize == 8 ? "selected" : ""); ?>>8</option>
                                <option value="10" <?php echo ($fontSize == 10 ? "selected" : ""); ?>>10</option>
                                <option value="12" <?php echo ($fontSize == 12 ? "selected" : ""); ?>>12</option>
                                <option value="14" <?php echo ($fontSize == 14 ? "selected" : ""); ?>>14</option>
                            </select>
                        </div>
                        <div class="" style="width:20%;">
                            <span>Espaço Abaixo:</span>
                            <select name="txtMarginBottom" id="txtMarginBottom" class="select" <?php echo $disabled; ?> style="width:100%">
                                <?php for($i = 0; $i <= 40; $i++) { ?>
                                <option value="<?php echo ($i * 11); ?>" <?php echo ($marginBottom == ($i * 11) ? "selected" : ""); ?>><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div id="divTable" style="display:flex;display: none; width:25%; justify-content: space-between;">
                        <div class="" style="width:35%;">
                            <span>Colunas:</span>
                            <input id="coll" type="text" value="4" <?php echo $disabled; ?>/>
                        </div>
                        <div class="" style="width:35%;">
                            <span>Linhas:</span>
                            <input id="line" type="text" value="3" <?php echo $disabled; ?>/>
                        </div>
                        <div style="width:20%;">
                            <button style="margin-top: 10px;" class="button button-green btn-primary" type="button" onclick="criarTabela()" <?php echo $disabled; ?>><span class="ico-plus icon-white"></span></button>
                        </div>
                    </div>
                </div>
                <div style="height:42px" class="variaveis">
                    <span style="margin: 5px 3px; display: block;">Variaveis:</span>
                    <hr style="margin: 0px 5px 7px 6px;">
                    <?php include 'include/ItensMenu.php'; ?>
                </div>
                <hr style="margin: 21px 0px;">
                <input id="saveContent" name="saveContent" type="hidden" value=""/>
                <div id="ckeditorCont">
                    <textarea id="ckeditor" name="ckeditor" class="ckeditor" style="height: 300px;" <?php echo $disabled; ?>><?php echo $conteudo; ?></textarea>
                </div>
                <div id="content" style="display: none; height: 365px;" class="content"><?php echo $conteudo; ?></div>
                <div id="detail" class="aux" style="display: none;">
                    <input id="selColl" type="hidden" value=""/>
                    <input id="selLine" type="hidden" value=""/>
                    <div>
                        <label for="selWidth">Tamanho(%):</label>
                        <input id="selWidth" type="text" value=""/>
                    </div>
                    <div>
                        <label for="selFontText">Texto:</label>
                        <select id="selFontText">
                            <option value="">Normal</option>
                            <option value="bold">Negrito</option>
                        </select>
                    </div>
                    <div>
                        <label for="selBackground">Cor de Fundo:</label>
                        <select id="selBackground">
                            <option value="">Sem Cor</option>
                            <option value="#ccc">Cor cinza</option>
                        </select>
                    </div>
                    <div>
                        <label for="selTextAlign">Alinhamento:</label>
                        <select id="selTextAlign">
                            <option value="">Esquerda</option>
                            <option value="right">Direita</option>
                            <option value="center">Centro</option>
                        </select>
                    </div>
                    <div>
                        <label for="selWidth">Agrupar Colunas:</label>
                        <input id="selGroup" type="text" value=""/>
                    </div>
                    <div>
                        <button class="button button-green btn-primary" style="width: 98%;" type="button" onclick="salvar()" <?php echo $disabled; ?>><span class="ico-checkmark icon-white"></span></button>
                    </div>
                </div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == '') { ?>
                        <button class="btn green" type="submit" title="Gravar" onclick="return validaForm();" name="bntSave" id="bntSave" ><span class="ico-checkmark"></span> Gravar</button>
                        <?php if ($_POST['txtId'] == '') { ?>
                            <button class="btn yellow" title="Cancelar" onClick="parent.FecharBox()" id="bntCancelar"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                            <button class="btn yellow" title="Cancelar" type="submit" name="bntAlterar" id="bntAlterar" ><span class="ico-reply"></span> Cancelar</button>
                            <?php
                        }
                    } else {
                        ?>
                        <button class="btn green" type="submit" title="Novo" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" title="Alterar" type="submit" name="bntEdit" id="bntEdit" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                        <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja inativar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
        </form>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type='text/javascript' src='../../js/plugins/ckeditor/ckeditor.js'></script>
        <script type="text/javascript" src="../../js/util.js"></script>
        <script type='text/javascript' src='js/contrato_makeForm.js'></script>
        <?php odbc_close($con); ?>
    </body>
</html>
