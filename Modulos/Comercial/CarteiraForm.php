<?php
include "../../Connections/configini.php";
include "form/CarteiraFormServer.php";
include "./../Comercial/modelos/variaveis.php";
$mdl_wha_ = returnPart($_SESSION["modulos"],16);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <style>
            .ui-dialog .ui-dialog-content, .ui-dialog-titlebar{
                border-left: 0px !important;
                border-right: 0px !important;
                border-top: 0px !important
            }
            .ui-dialog .ui-dialog-buttonpane button{
                background: #C22439 !important;
                border: 1px solid #C22439 !important;
            }
            .ui-dialog .ui-dialog-buttonpane{
                padding: 0 0px !important;
                padding: 0px;
            }
            .dropdown {
                width: 19%;
            }            
            .variaveis .dropdown .dropdown-toggle {
                width: 100%;
            }
            .variaveis .dropdown-menu{
                width: 100%;
                max-height: 420px;
            }            
            .data-fluid {
                overflow-x: auto;    
            }      
        </style>
    </head>
    <body>
        <form name="frmEnviaDados" id="frmEnviaDados" method="POST" onkeydown="if (event.ctrlKey && event.keyCode === 86) { return false;}" enctype="multipart/form-data">
            <div class="frmhead">
                <div class="frmtext">Carteira do Associado</div>
                <div class="frmicon" onClick="parent.FecharCarteira()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input name="txtId" id="txtId" type="hidden" value="<?php echo $id; ?>"/>                                
                <div style="float: left; width: 15%;">
                    <label for="txtDescricao">Descrição:</label>                    
                    <input name="txtDescricao" id="txtDescricao" type="text" style="width:100%" maxlength="50" <?php echo $disabled; ?> value="<?php echo $descricao; ?>"/>
                </div>
                <div style="float: left; width: 4%; margin-left:0.5%;">
                    <label for="txtMarginTop">M-E:</label>                    
                    <input name="txtMarginTop" id="txtMarginTop" type="text" style="width:100%" maxlength="4" <?php echo $disabled; ?> value="<?php echo $margin_top; ?>"/>
                </div>
                <div style="float: left; width: 4%; margin-left:0.5%;">
                    <label for="txtMarginLeft">M-T:</label>                    
                    <input name="txtMarginLeft" id="txtMarginLeft" type="text" style="width:100%" maxlength="4" <?php echo $disabled; ?> value="<?php echo $margin_left; ?>"/>
                </div>
                <div style="float:left; width: 13%; margin-left:0.5%;">                        
                    <span>Grupo:</span>
                    <select name="txtGrupo" id="txtGrupo" class="select" <?php echo $disabled; ?> style="width:100%">
                        <option value="null" >Selecione o grupo:</option>
                        <?php
                        $sql = "select id_contas_movimento,descricao from sf_contas_movimento where tipo = 'L' and inativa = 0 ORDER BY descricao";
                        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) {
                            ?>
                            <option value="<?php echo $RFP['id_contas_movimento'] ?>"<?php
                            if (!(strcmp($RFP['id_contas_movimento'], $grupo))) {
                                echo "SELECTED";
                            }
                            ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                <?php } ?>
                    </select>
                </div>                
                <div style="float:left; width: 10%; margin-left:0.5%;">                        
                    <span>Todos Dependentes:</span>
                    <select name="txtListDep" id="txtListDep" class="select" <?php echo $disabled; ?> style="width:100%">
                        <option value="0" <?php
                        if ($list_dep == "0") {
                            echo "SELECTED";
                        }
                        ?>>Sim</option>
                        <option value="1" <?php
                        if ($list_dep == "1") {
                            echo "SELECTED";
                        }
                        ?>>Não</option>
                    </select>
                </div>                
                <div style="float: left; width: 24%; margin-left: 1%;">
                    <label id="lblarquivo" for="arquivo">Imagem Frente:</label>
                    <?php if ($imageFrente === "") { ?>
                        <input class="btn btn-primary" style="height:20px; line-height:21px;width: 100%;" type="file" name="arquivoFrente" id="arquivoFrente" <?php echo $disabled; ?> />
                    <?php } else { ?>
                        <button class="btn btn-primary" type="button" title="Visualizar" onclick="verImg('<?php echo $imageFrente ; ?>');" style="width: 100px;"> Visualizar </button>
                        <button class="btn btn-danger" type="button" title="Excluir" onclick="excluirImg('<?php echo $imageFrente ; ?>', 'foto_capa');" style="width: 100px;"> Excluir </button>
                    <?php } ?>
                </div>                
                <div style="float: left; width: 24%; margin-left: 3%;">
                    <label id="lblarquivo" for="arquivo">Imagem Verso:</label>
                    <?php if ($imageVerso === "") { ?>
                        <input class="btn btn-primary" style="height:20px; line-height:21px;width: 100%;" type="file" name="arquivoVerso" id="arquivoVerso" <?php echo $disabled; ?> />
                    <?php } else { ?>
                        <button class="btn btn-primary" type="button" title="Visualizar" onclick="verImg('<?php echo $imageVerso ; ?>');" style="width: 100px;"> Visualizar </button>
                        <button class="btn btn-danger" type="button" title="Excluir" onclick="excluirImg('<?php echo $imageVerso ; ?>', 'foto_verso');" style="width: 100px;"> Excluir </button>
                    <?php } ?>
                </div>                
                <div style="clear:both;"></div>                
                <div class="variaveis">
                    <hr style="margin: 5px 0px 5px 0px;">
                    <?php include './../Comercial/include/ItensMenu.php'; ?>
                </div>  
                <div style="clear:both;"></div>                
                <div style="width:100%;">
                    <hr style="margin:2px 0; border-top:1px solid #DDD">
                </div>
                <div class="data-fluid" id="ckTp1">
                    <label for="ckeditor"></label>
                    <textarea id="ckeditor" name="ckeditor" style="height: 403px;" onkeydown="if (event.ctrlKey && event.keyCode === 86) { return false;}" <?php echo $disabled; ?>><?php echo $mensagem; ?></textarea>
                </div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == '') { ?>
                        <button class="btn btn-success" type="submit" name="btnSave" title="Gravar" id="btnSave" onclick="return validaForm();" ><span class="ico-checkmark"></span> Gravar</button>
                        <?php if ($_POST['txtId'] == '') { ?>
                            <button class="btn yellow" title="Cancelar" onClick="parent.FecharCarteira()" id="btnCancelar"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                            <button class="btn yellow" title="Cancelar" type="submit" name="btnCancelar" id="btnCancelar" ><span class="ico-reply"></span> Cancelar</button>
                            <?php
                        }
                    } else { ?>
                        <button class="btn  btn-success" type="submit" title="Novo" name="btnNew" id="btnNew" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                        <button class="btn  btn-success" type="submit" title="Alterar" name="btnEdit" id="btnEdit" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                        <div class="btn red" style="background-color:#68AF27; margin-right:1%" onClick="bootbox.confirm('Confirma a exclusão do registro?', function (result) { if (result) { $('#bntDelete').click(); } });">
                            <div class="icon"><span class="ico-remove"></span>&nbsp;Excluir</div>
                        </div>
                        <input id="bntDelete" name="bntDelete" type="submit" style="display:none"/>
                    <?php } ?>
                </div>
            </div>
        </form>
        <script type='text/javascript' src='../../js/plugins/jquery-ui-1.11.4/jquery-ui.min.js'></script>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/ckeditorAll/ckeditor.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>               
        <script type='text/javascript'>
            
            parent.$("#loader").hide();
            $(document).ready(function () {
                CKEDITOR.replace('ckeditor', {height: 332, width: 1086});
            });
                        
            function validaForm() {
                if ($("#txtDescricao").val() === "") {
                    bootbox.alert("Descrição é Obrigatória!");
                    return false;
                } else {
                    parent.$("#loader").show();     
                }                
                return true;
            }

            function variable(e) {
                CKEDITOR.instances['ckeditor'].insertText(e);
            }
            
            function verImg(img) {
                window.open("./../../Pessoas/" + parent.$("#txtMnContrato").val() + "/Empresa/Cartao/" + $("#txtId").val() + "/" + img, '_blank'); 
            }
            
            function excluirImg(img, tipo) {
                bootbox.confirm('Confirma a exclusão desta imagem?', function (result) {
                    if (result === true) {
                        $.post("form/CarteiraFormServer.php", "DelImage=" + img + "&List=S&txtId=" + $("#txtId").val() + "&txtTipo=" + tipo).done(function (data) {
                            location.reload();
                        });                        
                    }
                });
            }
            
        </script>        
        <?php odbc_close($con); ?>
    </body>
</html>