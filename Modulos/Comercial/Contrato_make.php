<?php include "../../Connections/configini.php";?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <style>
            #tblContratos td {
                padding: 5px;
                line-height: 16px;
            }
            .tablecntt th{
              line-height: 40px !important;
              vertical-align: middle !important;
            }
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php");?>
            <div class="body">
                <?php include("../../top.php");?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Academia<small>Contratos</small></h1>
                    </div>
                    <div id="parametros_busca" class="row-fluid">
                        <div class="span12">
                            <div class="block" style="font-size:13px">
                                <input type="hidden" name="id_contrato" id="id_contrato" value="<?php echo $_GET["id"]; ?>">
                                <button id="btnNovo" class="button button-green btn-primary" type="button" onclick="addItem()"><span class="ico-file-4 icon-white"></span></button>
                                <button id="btnPrint" class="button button-blue btn-primary" type="button" onClick="AbrirBox();"><span class="ico-print icon-white"></span></button>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Contratos</div>
                    </div>
                    <div class="boxtable">
                        <table class="table tablecntt" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tblContratos">
                            <thead>
                                <tr style="height:40px;">
                                    <th width="10%">
                                        <div style="display: flex;justify-content: center;">
                                            <button type="button" name="button" style="margin-left: 12px;" onclick="reordernar(2)"><span class="icon-chevron-up"></span></button>                                            
                                            <button type="button" name="button" onclick="reordernar(1)"><span class="icon-chevron-down"></span></button>
                                        </div>
                                    </th>
                                    <th width="10%">Tipo</th>
                                    <th width="70%">Conteúdo</th>
                                    <th width="10%"><center>Ação:</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>
        <script type="text/javascript" src="js/contrato_make.js" ></script>
        <?php odbc_close($con); ?>
    </body>
</html>
