<?php

require_once(__DIR__ . '/../../Connections/configini.php');
$aColumns = array('id_fornecedores_despesas', 'cnpj', 'razao_social', 'nome_fantasia', 'bairro', 'cidade', 'estado', 'telefone', 'email', 'contato');

$mdl_aca_ = returnPart($_SESSION["modulos"], 6);
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY A." . ($adm_cli_ordem_ == 0 ? "id_fornecedores_despesas desc " : ($adm_cli_ordem_ == 1 ? "razao_social asc " : "titular desc "));
$sOrder1 = $sOrder;
$sLimit = 20;
$imprimir = 0;
$_GET['txtBusca'] = utf8_decode($_GET['txtBusca']);

if (is_numeric($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if ($_GET['id'] == "-1") {
    $sWhereX .= " and A.grupo_pessoa is null ";
} else if ($_GET['id'] != "") {
    $sWhereX .= " and A.grupo_pessoa = " . $_GET['id'];
}

if (isset($_GET['filial']) && $_GET['filial'] != "null") {
    $sWhereX .= " and A.empresa in (" . str_replace("\"", "'", str_replace(array("[", "]"), "", $_GET["filial"])) . ")";
} else {
    $sWhereX .= " and A.empresa in (select id_filial_f from sf_usuarios_filiais where id_usuario_f = " . $_SESSION["id_usuario"] . ")";
}

if ($_GET['usu'] != "") {
    $sWhereX .= " and A.id_user_resp = '" . $_GET['usu'] . "'";
} 

if ($crm_cli_ure_ > 0) {
    $sWhereX .= " and (id_usuario = " . $_SESSION["id_usuario"] . " 
    or id_usuario in (select sf_usuarios_dependentes.id_usuario from sf_usuarios_dependentes 
    inner join sf_usuarios on id_fornecedor_despesas = funcionario where sf_usuarios.id_usuario = " . $_SESSION["id_usuario"] . "))";
}

if ($_GET['txtBusca'] != "") {
    $orCampo = "";
    $cur2 = odbc_exec($con, "select id_campo from sf_configuracao_campos_livres where busca_campo = 1");
    while ($RFP = odbc_fetch_array($cur2)) {
        $orCampo = " or A.id_fornecedores_despesas in (select fornecedores_campos from sf_fornecedores_despesas_campos_livres where campos_campos = " . $RFP['id_campo'] ." and conteudo_campo like '%" . $_GET['txtBusca'] . "%')";
    }    
    $sWhereX .= " and (CAST(A.id_fornecedores_despesas AS VARCHAR(30)) = '" . $_GET['txtBusca'] . "' or A.razao_social like '%" . $_GET['txtBusca'] . "%'
    or A.nome_fantasia like '%" . $_GET['txtBusca'] . "%' or A.cnpj like '%" . $_GET['txtBusca'] . "%'" . $orCampo . ")";
}

$inativo = "and A.inativo = 0";

if ($_GET['txtStatus'] != "null" && $_GET['txtStatus'] != "") {    
    if (strpos($_GET['txtStatus'] . ",", "Ativo,") !== false) {
        $_GET['txtStatus'] = $_GET['txtStatus'] . ",Ativo Ausente 1,Ativo Ausente 2,Ativo Ausente 3,Ativo Ausente 4,Ativo Ausente 5";
    }
    if (strpos($_GET['txtStatus'], "Bloqueado") !== false) {
        $inativo = " and A.bloqueado = 1 ";
    }
    if (strpos($_GET['txtStatus'], "Excluido") !== false) {
        $inativo = " and A.inativo = " . ($contrato == "007" ? "-" : "") . "1 ";
    }
    $sWhereX .= " and A.fornecedores_status in ('" . str_replace(",", "','", $_GET['txtStatus']) . "') ";
}
$sWhereX = $sWhereX . $inativo;

$tpLista = '';
if ($_GET['dti'] != "" && $_GET['dtf'] != "") {
    $tpLista = 'A';
    $dataAux = explode("_", $_GET['dti']);
    $dtini = $dataAux[1] . $dataAux[0];
    $dataAux = explode("_", $_GET['dtf']);
    $dtfim = $dataAux[1] . $dataAux[0];
    $sWhereX = $sWhereX . " AND SUBSTRING(REPLACE(CONVERT(VARCHAR(10), a.data_nascimento, 1), '/', ''),1,4) BETWEEN '" . $dtini . "' and '" . $dtfim . "' ";
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $mAca = 0;
    if ($mdl_aca_ == 1) {
        $mAca = -1;
    }
    if ($_GET['iSortCol_0'] == 4 && $mdl_aca_ == 1) {
        $sOrder .= "A.prof_resp " . $_GET['sSortDir_0'] . ", ";
    } elseif ($_GET['iSortCol_0'] == 3 && $mdl_aca_ == 0) {
        $sOrder .= "A.contato " . $_GET['sSortDir_0'] . ", ";
    } else {
        for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
            if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                $sOrder .= "A." . $aColumns[intval($_GET['iSortCol_0'] + $mAca)] . " " . $_GET['sSortDir_0'] . ", ";
            }
        }
    }

    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY A." . ($adm_cli_ordem_ == 0 ? "id_fornecedores_despesas desc " : ($adm_cli_ordem_ == 1 ? "razao_social asc " : "titular desc "));
    }
    if ($_GET['iSortCol_0'] == 4 && $mdl_aca_ == 1) {
        $sOrder1 = "ORDER BY P.razao_social " . $_GET['sSortDir_0'];
    } else {
        $sOrder1 = $sOrder;
    }
}

if ($_GET['sSearch'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        if ($i != 6 && $i != 7) {
            $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['sSearch']) . "%' OR ";
        }
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

$sQuery = "SELECT COUNT(*) total FROM sf_fornecedores_despesas A 
left join tb_cidades on cidade = cidade_codigo 
left join sf_fornecedores_despesas P on A.prof_resp = P.id_fornecedores_despesas 
left join sf_usuarios on A.id_user_resp = id_usuario
WHERE A.id_fornecedores_despesas > 0 and A.tipo = 'C' $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
    $iFilteredTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder1 . ") as row,A.id_fornecedores_despesas, A.razao_social, A.contato, A.bairro, cidade_nome cidade, estado_sigla estado,
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas = A.id_fornecedores_despesas) telefone,
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = A.id_fornecedores_despesas) telefone_celular,
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = A.id_fornecedores_despesas) email,
(select count(id_planos_ferias) total from sf_vendas_planos_ferias f inner join sf_vendas_planos p on p.id_plano = f.id_plano where f.dt_cancelamento is null and GETDATE() between f.dt_inicio and f.dt_fim and favorecido = A.id_fornecedores_despesas) ferias,
A.nome_fantasia, A.sexo,A.cnpj, A.cep,A.data_nascimento, descricao_grupo, P.razao_social prof_resp, isnull(A.titular,0) titular,
A.fornecedores_status status_aluno, A.estado_civil,A.inscricao_estadual, A.endereco, A.numero, A.complemento, login_user, A.email_marketing, A.celular_marketing
FROM sf_fornecedores_despesas A left join sf_tipo_documento on forma_pagamento = id_tipo_documento
left join tb_estados on estado = estado_codigo left join tb_cidades on cidade = cidade_codigo
left join sf_grupo_cliente on grupo_pessoa = id_grupo
left join sf_fornecedores_despesas P on A.prof_resp = P.id_fornecedores_despesas AND P.id_fornecedores_despesas > 0
left join sf_usuarios on A.id_user_resp = id_usuario
WHERE A.id_fornecedores_despesas > 0 AND A.tipo = 'C' " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1=" . $imprimir . " " . $sOrder;
$cur = odbc_exec($con, $sQuery1);

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $identificacao = strtoupper(utf8_encode($aRow[$aColumns[2]]));
    if (utf8_encode($aRow[$aColumns[3]]) != '') {
        $identificacao = strtoupper(utf8_encode($aRow[$aColumns[2]])) . ' (' . strtoupper(utf8_encode($aRow[$aColumns[3]])) . ')';
    }
    if ($_GET['emailMarketing'] !== '1' && !isset($_GET['envioSms'])) {
        $row[] = "<center><span title='" . $aRow['status_aluno'] . "' class='label-" . ($aRow['ferias'] > 0 ? "Ferias" : $aRow['status_aluno']) . "' style='display: inline-block;width: 13px;height: 13px;border-radius: 50%!important;'></span></center>";
    }
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow['id_fornecedores_despesas']) . "'>" . utf8_encode($aRow['id_fornecedores_despesas']) . "</div></center>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[1]]) . "'>" . utf8_encode($aRow[$aColumns[1]]) . "</div></center>";
    if ($aRow['status_aluno'] == 'Excluido') {
        $row[] = "<div id='formPQ' title='" . $identificacao . "'>" . $identificacao . "</div>";
    } else {
        $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(2," . utf8_encode($aRow['id_fornecedores_despesas']) . ")'><div id='formPQ' title='" . $identificacao . "'>" . $identificacao . "</div></a>";
    }
    if ($mdl_aca_ == 0) {
        $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[9]]) . "'>" . utf8_encode($aRow[$aColumns[9]]) . "</div>";
    } else {
        $row[] = "<div id='formPQ' title='" . strtoupper(utf8_encode($aRow['login_user'])) . "'>" . strtoupper(utf8_encode($aRow['login_user'])) . "</div>";
    }
    if ($_GET['dti'] != "" && $_GET['dtf'] != "") {
        $row[] = escreverData($aRow["data_nascimento"]);
    } else {
        $row[] = "<div id='formPQ' title='" . strtoupper(utf8_encode($aRow[$aColumns[4]])) . "'>" . strtoupper(utf8_encode($aRow[$aColumns[4]])) . "</div>";    
    }
    $row[] = "<div id='formPQ' title='" . strtoupper(utf8_encode($aRow[$aColumns[5]])) . "'>" . strtoupper(utf8_encode($aRow[$aColumns[5]])) . "</div>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[6]]) . "'>" . utf8_encode($aRow[$aColumns[6]]) . "</div></center>";
    if (isset($_GET['envioSms'])) {
        $row[] = utf8_encode($aRow["telefone_celular"]);
    } else {
        if ($aRow["telefone_celular"] != "") {
            if ($aRow[$aColumns[7]] != "") {
                $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["telefone_celular"]) . "1&#013;" . utf8_encode($aRow[$aColumns[7]]) . "'>" . utf8_encode($aRow["telefone_celular"]) . "</div>";
            } else {
                $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["telefone_celular"]) . "'>" . utf8_encode($aRow["telefone_celular"]) . "</div>";
            }
        } else {
            $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[7]]) . "'>" . utf8_encode($aRow[$aColumns[7]]) . "</div>";
        }
    }
    $row[] = utf8_encode($aRow[$aColumns[8]]);
    if ($ckb_adm_cli_read_ == 0) {
        if ($_GET['txtStatus'] === "Excluido") {
            $row[] = "<center><a title='Reativar'><input name='' type='image' onClick='reativarCli(" . $aRow['id_fornecedores_despesas'] . ")' src='../../img/undo_256.png' width='18' height='18' value='Reativar'></a></center>";
        } else {
            $row[] = "<center><a title='Excluir'><input name='' type='image' onClick='excluirCli(" . $aRow['id_fornecedores_despesas'] . ")' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Excluir'></a></center>";
        }
    } else {
        $row[] = "<center></center>";
    }
    $row[] = $aRow['id_fornecedores_despesas'];
    $row[] = utf8_encode($aRow['ferias'] > 0 ? "Ferias" : $aRow['status_aluno']);    
    $row[] = utf8_encode($aRow['descricao_grupo']);
    if ($contrato == "007") {
        $row[] = "SIM";
        $row[] = "SIM"; 
    } else {
        $row[] = ($aRow['email_marketing'] == 1 ? "SIM" : "NÃO");
        $row[] = ($aRow['celular_marketing'] == 1 ? "SIM" : "NÃO");    
    }
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
