<?php
include '../../Connections/configini.php';
$disabled = 'disabled';

if (isset($_POST['bntSave'])) {
    $ckb_aut = "false";
    if (utf8_decode($_POST['ckbEmailAuth']) == "true") {
        $ckb_aut = "true";
    }    
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_departamentos set " .
        "nome_departamento = '" . utf8_decode($_POST['txtdescricao_cor']) . "'," .
        "tipo_departamento = " . utf8_decode($_POST['txtservico']) . "," .                
        "email = '" . utf8_decode($_POST['txtEmail']) . "'," .
        "email_desc = '" . utf8_decode($_POST['txtEmailDesc']) . "'," .
        "email_host = '" . utf8_decode($_POST['txtEmailHost']) . "'," .
        "email_porta = '" . utf8_decode($_POST['txtEmailPorta']) . "'," .
        "email_login = '" . utf8_decode($_POST['txtEmailLogin']) . "'," .
        "email_senha = '" . utf8_decode($_POST['txtEmailSenha']) . "'," .
        "email_ckb = '" . $ckb_aut . "'," . 
        "email_ssl = " . valoresSelect('ckbSSL') . "," .
        "email_smtp = " . valoresCheck('ckbSmtp') . "," .
        "copyMe = " . valoresCheck('ckbCopyMe') . "," .   
        "assinatura = '" . utf8_decode($_POST['ckeditor']) . "' " .                
        "where id_departamento = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_departamentos(nome_departamento,tipo_departamento, 
        email,email_desc,email_host,email_porta,email_login,email_senha,email_ckb,email_ssl,email_smtp,copyMe,assinatura            
        )values('" . utf8_decode($_POST['txtdescricao_cor']) . "','" . 
        $_POST['txtservico'] . "','" .
        utf8_decode($_POST['txtEmail']) . "','" .
        utf8_decode($_POST['txtEmailDesc']) . "','" .
        utf8_decode($_POST['txtEmailHost']) . "','" .
        utf8_decode($_POST['txtEmailPorta']) . "','" .
        utf8_decode($_POST['txtEmailLogin']) . "','" .
        utf8_decode($_POST['txtEmailSenha']) . "','" .
        $ckb_aut . "'," . 
        valoresSelect('ckbSSL') . "," .
        valoresCheck('ckbSmtp') . "," .
        valoresCheck('ckbCopyMe') . ",'" .   
        utf8_decode($_POST['ckeditor']) . "')") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_departamento from sf_departamentos order by id_departamento desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_departamentos WHERE id_departamento = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_departamentos where id_departamento =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_departamento'];
        $nome_departamento = utf8_encode($RFP['nome_departamento']);
        $id_servico = utf8_encode($RFP['tipo_departamento']);
        $email = utf8_encode($RFP['email']);
        $email_desc = utf8_encode($RFP['email_desc']);
        $email_host = utf8_encode($RFP['email_host']);
        $email_porta = utf8_encode($RFP['email_porta']);
        $email_login = utf8_encode($RFP['email_login']);
        $email_senha = utf8_encode($RFP['email_senha']);
        $email_ckb = utf8_encode($RFP['email_ckb']);
        $email_ssl = utf8_encode($RFP['email_ssl']);
        $email_smtp = utf8_encode($RFP['email_smtp']);        
        $copyMe = utf8_encode($RFP['copyMe']);
        $mensagem = utf8_encode($RFP['assinatura']);        
    }
} else {
    $disabled = '';
    $id = '';
    $nome_departamento = '';
    $id_servico = "0";
    $email = '';    
    $email_desc = '';
    $email_host = '';
    $email_porta = '';
    $email_login = '';
    $email_senha = '';
    $email_ckb = 'false';
    $email_ssl = 0;
    $email_smtp = 0;
    $copyMe = 0;
    $mensagem = '';    
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script> 
<style type="text/css">
    div.radio {
        margin-top: 2px;
    }
</style>   
<body>
    <form action="FormDepartamentos.php" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Departamento</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="tabbable frmtabs">
            <ul class="nav nav-tabs" style="margin:0">
                <li class="active"><a href="#tab1" data-toggle="tab">Dados principais</a></li>
                <li><a href="#tab2" data-toggle="tab">Configuração de E-mail</a></li>
                <li><a href="#tab3" data-toggle="tab">Assinatura</a></li>
            </ul>
            <div class="tab-content frmcont" style="height:264px">
                <div class="tab-pane active" id="tab1" style="overflow:hidden; height:264px">   
                    <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                    <div style="width: 100%;float: left">
                        <span>Nome Departamento:</span>
                        <input name="txtdescricao_cor" id="txtdescricao_cor" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="128" value="<?php echo utf8_encode($nome_departamento); ?>"/>
                    </div>
                    <div style="clear:both;height: 5px;"></div>
                    <div style="width: 100%;float: left">
                        <span>Tipo</span><br>
                        <div style="float:left; margin:4px"><input type='Radio' name='txtservico' value='0' <?php
                            if ($id_servico == "0") {
                                echo "checked";
                            }
                            ?> <?php echo $disabled; ?>></div>
                        <div style="float:left; margin:6px; margin-left:0px">Atendimento</div>
                        <div style="float:left; margin:4px"><input type='Radio' name='txtservico' value='1' <?php
                            if ($id_servico == "1") {
                                echo "checked";
                            }
                            ?> <?php echo $disabled; ?>></div>
                        <div style="float:left; margin:6px; margin-left:0px">Ordem de Serviço</div>
                        <div style="float:left; margin:4px"><input type='Radio' name='txtservico' value='2' <?php
                            if ($id_servico == "2") {
                                echo "checked";
                            }
                            ?> <?php echo $disabled; ?>></div>
                        <div style="float:left; margin:6px; margin-left:0px">Ambos</div>
                    </div>
                    <div style="clear:both;height: 5px;"></div>
                </div>
                <div class="tab-pane" id="tab2" style="overflow:hidden; height:264px">
                    <div style="width:100%;">
                        <span style="width: 50%">Descrição:</span>
                        <input id="txtEmailDesc" name="txtEmailDesc" maxlength="100" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $email_desc; ?>"/>
                    </div>
                    <div style="clear:both;height: 5px;"></div>
                    <div style="width:50%;float: left">
                        <span style="width: 50%">Host (Servidor):</span>
                        <input id="txtEmailHost" name="txtEmailHost" maxlength="100" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $email_host; ?>"/>
                    </div>
                    <div style="width:19%;float: left;margin-left: 1%">
                        <span style="width: 50%">Porta:</span>
                        <input id="txtEmailPorta" name="txtEmailPorta" maxlength="5" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $email_porta; ?>"/>
                    </div>
                    <div style="width:29%;float: left;margin-left: 1%">
                        <span style="width: 50%">Segurança:</span>
                        <select id="ckbSSL" name="ckbSSL" style="width: 100%;" <?php echo $disabled; ?>>
                            <option value="0" <?php if ($email_ssl == "0") { echo "selected";}?>>Sem Protocolo</option>
                            <option value="1" <?php if ($email_ssl == "1") { echo "selected";}?>>SSL</option>
                            <option value="2" <?php if ($email_ssl == "2") { echo "selected";}?>>TLS</option>
                        </select>
                    </div>
                    <div style="clear:both;height: 5px;"></div>
                    <div style="width:50%;float: left">
                        <span style="width: 50%">Login do E-mail:</span>
                        <input id="txtEmailLogin" name="txtEmailLogin" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $email_login; ?>"/>
                    </div>
                    <div style="width:49%;float: left;margin-left: 1%">
                        <span style="width: 50%">Senha do E-mail:</span>
                        <input id="txtEmailSenha" name="txtEmailSenha" type="password" <?php echo $disabled; ?> class="input-medium" value="<?php echo $email_senha; ?>"/>
                    </div>
                    <div style="clear:both;height: 10px;"></div>
                    <div style="width:49%;float: left;">
                        <input name="ckbEmailAuth" id="ckbEmailAuth" <?php echo $disabled; ?> type="checkbox" <?php
                        if ($email_ckb == "true") {
                            echo "checked";
                        }
                        ?> class="input-medium" value="true" style="opacity:0"/>
                        <span style="width: 50%">Autenticação:</span>
                    </div>
                    <div style="clear:both;height: 5px;"></div>
                    <div style="width:49%;float: left;">
                        <input name="ckbSmtp" id="ckbSmtp" <?php echo $disabled; ?> type="checkbox" <?php
                        if ($email_smtp == "1") {
                            echo "checked";
                        }
                        ?> class="input-medium" value="1" style="opacity:0"/>
                        <span style="width: 50%">Desativar SMTP:</span>
                    </div>
                    <div style="clear:both;height: 5px;"></div>                     
                    <div style="width:49%;float: left;">
                        <input name="ckbCopyMe" id="ckbCopyMe" <?php echo $disabled; ?> type="checkbox" <?php
                        if ($copyMe == "1") {
                            echo "checked";
                        }
                        ?> class="input-medium" value="1" style="opacity:0"/>
                        <span style="width: 50%">Me enviar cópia:</span>
                    </div>                   
                </div>
                <div class="tab-pane" id="tab3" style="overflow:hidden;height:264px">
                    <div style="width:90%;float: left;">
                        <textarea id="ckeditor" name="ckeditor" <?php echo $disabled; ?> onkeydown="if (event.ctrlKey && event.keyCode === 86) {return false;}"> <?php echo $mensagem; ?></textarea>
                    </div>
                </div>                
            </div>                
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" name="bntSave" onclick="return validar();" id="bntSave" ><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntCancel"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntAlterar" ><span class="ico-reply"></span> Cancelar</button>
                    <?php
                    }
                } else {
                    if (is_numeric($id)) { ?>
                        <button class="btn button-blue" style="float: left; margin-top: 14px;" onclick="AtualizarEmail();" type="button" name="bntRefresh" value="Aplicar configurações de E-mail"><span class="ico-refresh"></span> Aplicar configurações de E-mail</button>
                <?php } ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntEdit" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/multiselect/jquery.multi-select.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery.filter_input.js'></script>
    <script type='text/javascript' src='../../js/plugins/ckeditor/ckeditor.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script> 
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>    
    <script type="text/javascript" src="../../js/util.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var ckeditor = CKEDITOR.replace('ckeditor', {removePlugins: 'elementspath', height: 190});
            /*ckeditor.on('paste', function () {
                return false;
            }, ckeditor.element.$);*/
            ckeditor.config.width = 556;
            if ($('#txtNome').attr("disabled") === "disabled") {
                $('.ms-list').find('li').addClass("disabled");
            }
        });
        
        function validar() {
            if ($("#txtdescricao_cor").val() === "") {
                bootbox.alert("Nome Departamento inválido!");
                return false;
            }
            return true;
        }
        
        function AtualizarEmail() {
            bootbox.confirm('Confirma a atualização dos dados de email dos usuários deste departamento?', function (result) {
                if (result === true) {
                    $.get("../Estoque/form/PlanosFormServer.php?isAjax=S&refreshEmail=" + $("#txtId").val()).done(
                    function (data) {
                        if (data.trim() === "YES") {
                            bootbox.alert("Usuários atualizadas com Sucesso!");
                        } else {
                            bootbox.alert("Erro ao atualizar Usuários!");
                        }
                    });
                }
            });
        }
    </script>        
    <?php odbc_close($con); ?>    
</body>
