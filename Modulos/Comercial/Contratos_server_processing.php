<?php

require_once(__DIR__ . '/../../Connections/configini.php');
$aColumns = array('id', 'descricao', 'tipo', 'usu_criacao', 'dt_criacao', 'inativo');
$table = "sf_contratos";
$PageName = "Contratos";
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY " . $aColumns[0] . " asc ";
$sOrder2 = " ORDER BY " . $aColumns[0] . " asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (is_numeric($_GET['at'])) {
    $sWhereX .= " and inativo = " . $_GET['at'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i])], 0) . " " . $_GET['sSortDir_' . $i] . ", ";
            $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i])], 1) . " " . $_GET['sSortDir_' . $i] . ", ";
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY " . $aColumns[0] . " asc";
        $sOrder2 = " ORDER BY " . $aColumns[0] . " asc";
    }
}

if ($_GET['txtBusca'] != "") {
    $sWhereX = $sWhereX . " and (" . prepareCollum($aColumns[1], 0) ." like '%" . $_GET['txtBusca'] . "%')";
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($i == 0) {
        $colunas = $aColumns[$i];
    } else {
        $colunas = $colunas . "," . $aColumns[$i];
    }
}

$sQuery = "SELECT COUNT(*) total from " . $table . " where " . $aColumns[0] . " > 0 $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total from " . $table . " where " . $aColumns[0] . " > 0 $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row, " . $colunas .",
STUFF((SELECT distinct ',' + descricao from sf_contratos_grupos cg inner join sf_contas_movimento c ON c.id_contas_movimento = cg.id_grupo WHERE cg.id_contrato = sf_contratos.id FOR XML PATH('')), 1, 1, '') as grupos
from " . $table . " where " . $aColumns[0] . " > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder2;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<center>" . utf8_encode($aRow[prepareCollum($aColumns[0], 1)]) . "</center>";
    $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[prepareCollum($aColumns[0], 1)]) . ")'><div id='formPQ' title='" . utf8_encode($aRow[prepareCollum($aColumns[1], 1)]) . "'>" . utf8_encode($aRow[prepareCollum($aColumns[1], 1)]) . "</div></a>";
    $row[] = $aRow[prepareCollum($aColumns[2], 1)] == 1 ? "RESCISÃO" : ($aRow[prepareCollum($aColumns[2], 1)] == 2 ? "AGENDAMENTO" : ($aRow[prepareCollum($aColumns[2], 1)] == 3 ? "EVENTOS" : ($aRow[prepareCollum($aColumns[2], 1)] == 4 ? "VISTORIA" : "AQUISIÇÃO")));
    $row[] = utf8_encode($aRow["grupos"]);
    $row[] = utf8_encode($aRow[prepareCollum($aColumns[3], 1)]);
    $row[] = "<center>" . escreverData($aRow[prepareCollum($aColumns[4], 1)]). "</center>";
    $row[] = "<center>" . ($aRow[prepareCollum($aColumns[5], 1)] == 1 ? "SIM" : "NÃO") . "</center>";
    if ($imprimir == 0) {
        $btnGerar = "<a title=\"Conteúdo do Contrato\" href=\"Contrato_make.php?id=" . $aRow[prepareCollum($aColumns[0], 1)] . "\"><img src=\"../../img/parcelamento.PNG\" alt=\"Parcelar\" width=\"18\" height=\"18\" hspace=\"3\" title=\"Conteúdo do Contrato\"></a>";
        $row[] = "<center>" . $btnGerar . "<a title='Excluir' onClick=\"return confirm('Deseja excluir esse registro?');\" href='" . $PageName . ".php?Del=" . $aRow[$aColumns[0]] . $toReturnAnd . "'><input name='' type='image' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
    }
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
