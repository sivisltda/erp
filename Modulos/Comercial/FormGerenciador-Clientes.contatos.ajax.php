<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include "../../Connections/configini.php";

$id = $_REQUEST['cod'];
$local = array();
if (is_numeric($id)) {
    $sql = "select fc.*, case 
    WHEN tipo_contato = 0 then 'Telefone' 
    WHEN tipo_contato = 1 then 'Celular' 
    WHEN tipo_contato = 2 then 'E-mail'  
    WHEN tipo_contato = 3 then 'Site' 
    WHEN tipo_contato = 4 then 'Outros' end texto_contatos, f.id_user_resp, f.tipo,
    (select count(sf_usuarios_dependentes.id_usuario) total from sf_usuarios_dependentes 
    inner join sf_usuarios on id_fornecedor_despesas = funcionario
    where sf_usuarios.id_usuario = " . $_SESSION["id_usuario"] . " and sf_usuarios_dependentes.id_usuario = id_user_resp) total
    from sf_fornecedores_despesas_contatos fc inner join sf_fornecedores_despesas f on fc.fornecedores_despesas = f.id_fornecedores_despesas 
    where fornecedores_despesas = " . $id . " order by tipo_contato";
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {        
        $local[] = array('id' => utf8_encode($RFP['id_contatos']), 
            'tipo_contato' => utf8_encode($RFP['tipo_contato']), 
            'conteudo_contato' => (($RFP['tipo_contato'] == "1" && $RFP['tipo'] == "P" && $crm_pro_ure_ > 0 && $RFP['id_user_resp'] != $_SESSION["id_usuario"] && $RFP['total'] == 0) ? ocultar_celular(utf8_encode($RFP['conteudo_contato']), true) : (
            ($RFP['tipo_contato'] == "1" && $RFP['tipo'] == "C" && $crm_cli_ure_ > 0 && $RFP['id_user_resp'] != $_SESSION["id_usuario"] && $RFP['total'] == 0) ? ocultar_celular(utf8_encode($RFP['conteudo_contato']), true) : (                
            ($RFP['tipo_contato'] == "2" && $RFP['tipo'] == "P" && $crm_pro_ure_ > 0 && $RFP['id_user_resp'] != $_SESSION["id_usuario"] && $RFP['total'] == 0) ? ocultar_email(utf8_encode($RFP['conteudo_contato']), true) : (            
            ($RFP['tipo_contato'] == "2" && $RFP['tipo'] == "C" && $crm_cli_ure_ > 0 && $RFP['id_user_resp'] != $_SESSION["id_usuario"] && $RFP['total'] == 0) ? ocultar_email(utf8_encode($RFP['conteudo_contato']), true) : (   
            utf8_encode($RFP['conteudo_contato'])))))), 'texto_contatos' => utf8_encode($RFP['texto_contatos']), 'responsavel' => utf8_encode($RFP['resp_contato']));
    }
}
echo(json_encode($local));
odbc_close($con);