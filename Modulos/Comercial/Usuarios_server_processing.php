<?php

if (!isset($_GET['pdf'])) {
    include "../../Connections/configini.php";
}
$aColumns = array('id_usuario', 'nome', 'data_cadastro', 'login_user', 'email', 'nome_departamento', 'nome_permissao', 'imagem', 'inativo', 'ultimo_acesso');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY id_usuario asc ";
$sLimit = 20;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['tp']) && $_GET['tp'] == "0") {
    $sWhereX .= " and u.funcionario is null";
} else if (isset($_GET['tp'])) {
    $sWhereX .= " and u.funcionario in (select id_fornecedores_despesas from sf_fornecedores_despesas where tipo = '" . $_GET['tp'] . "')";
}

if (is_numeric($_GET['at'])) {
    $sWhereX .= " and u.inativo = " . $_GET['at'];
}

if (is_numeric($_GET['pr'])) {
    $sWhereX .= " and u.master = " . $_GET['pr'];
}

if (is_numeric($_GET['dp'])) {
    $sWhereX .= " and u.departamento = " . $_GET['dp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $sOrder .= $aColumns[intval($_GET['iSortCol_0'] + $mAca)] . " " . $_GET['sSortDir_0'] . ", ";
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY id_usuario asc";
    }
}

if ($_GET['txtBusca'] != "") {
    $sWhereX = $sWhereX . " and (u.nome like '%" . $_GET['txtBusca'] . "%' or u.login_user like '%" . $_GET['txtBusca'] . "%' or u.email like '%" . $_GET['txtBusca'] . "%'  or d.nome_departamento like '%" . $_GET['txtBusca'] . "%' or isnull(nome_permissao,'Administrador') like '%" . $_GET['txtBusca'] . "%')";
}

if(isset($_GET['errorMU']) && $_GET['errorMU'] > 0){
    $sWhereX = $sWhereX.($_GET['errorMU'] == 1 ? "and mu_id is null " : "and mu_id is not null and pendencias_mu is not null and pendencias_mu <> '' ");
}

$sQuery = "SELECT COUNT(*) total FROM sf_usuarios u 
           left join sf_usuarios_permissoes on sf_usuarios_permissoes.id_permissoes = u.master 
           left join sf_departamentos d on d.id_departamento = u.departamento WHERE id_usuario > 1 $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total FROM sf_usuarios u 
           left join sf_usuarios_permissoes on sf_usuarios_permissoes.id_permissoes = u.master 
           left join sf_departamentos d on d.id_departamento = u.departamento WHERE id_usuario > 1 $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,u.id_usuario,u.nome,u.login_user,u.email,u.imagem,u.data_cadastro,d.nome_departamento,isnull(nome_permissao,'Administrador') nome_permissao,case when inativo = 1 then 'SIM' ELSE 'NÃO' END inativo, ultimo_acesso
            FROM sf_usuarios u
            left join sf_usuarios_permissoes on sf_usuarios_permissoes.id_permissoes = u.master 
            left join sf_departamentos d
            on d.id_departamento = u.departamento WHERE id_usuario > 1 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder;
//echo $sQuery1;exit();
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $identificacao = utf8_encode($aRow[$aColumns[1]]);
    if ($aRow[$aColumns[7]] != '') {
        $row[] = "<center> <img src=\"./../../Pessoas/" . $contrato . "/" . $aRow[$aColumns[7]] . "\" width=\"40px\" align=\"left\" style=\"border-left:solid 3px #FFF; float:left;\"/> </center>";
    } else {
        $row[] = "<center> <img src=\"./../../img/dmitry_m.gif\" width=\"40px\" align=\"left\" style=\"border-left:solid 3px #FFF; float:left;\"/> </center>";
    }
    $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[$aColumns[0]]) . ",0)'><div id='formPQ' title='" . $identificacao . "'>" . $identificacao . "</div></a>";
    $row[] = "<center><div id='formPQ' title='" . escreverData($aRow[$aColumns[2]]) . "'>" . escreverData($aRow[$aColumns[2]]) . "</div></center>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[3]]) . "'>" . utf8_encode($aRow[$aColumns[3]]) . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[4]]) . "'>" . utf8_encode($aRow[$aColumns[4]]) . "</div>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[5]]) . "'>" . utf8_encode($aRow[$aColumns[5]]) . "</div></center>";
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[6]]) . "'>" . utf8_encode($aRow[$aColumns[6]]) . "</div></center>";
    $row[] = "<center><div id='formPQ' title='" . escreverDataHora($aRow[$aColumns[9]]) . "'>" . escreverDataHora($aRow[$aColumns[9]]) . "</div></center>";    
    $row[] = "<center><a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[$aColumns[0]]) . ",1)' title='Alterar Senha'><input name='' type='image' src='../../img/1365123843_onebit_323 copy.PNG' width='18' height='18' value=''></a></center>";
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
