<?php
include "../../Connections/configini.php";

$id = "";
if ($_GET['id'] != '') {
    $id = str_replace("-", "", $_GET['id']);
}

?>
<!DOCTYPE html>
<head>
    <link rel="icon" type="image/ico" href="../../../favicon.ico"/>
    <link href="../../../css/stylesheets.css" rel="stylesheet" type="text/css" />
    <link href="../../../css/stylesheet.css" rel="stylesheet" type="text/css" />
    <link href="../../../css/main.css" rel="stylesheet">
    <link href="../../../css/bootbox.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
</head>
<body>
    <div class="row-fluid">
        <form id="frmLancamentoMovimento" name="frmLancamentoMovimento" action="" method="POST">
            <div class="frmhead">
                <div class="frmtext">Anexo de Arquivo: <?php echo $id; ?></div>
                <div class="frmicon" style="top:13px" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <div class="tabbable frmtabs">
                <ul class="nav nav-tabs" style="margin:0">
                    <li class="active"><a href="#tab1" data-toggle="tab">Documentos</a></li>
                </ul>
                <div class="tab-content frmcont" style="height:247px; font-size:11px !important; overflow: unset;">
                    <div class="tab-pane active" id="tab1">
                        <div style="float: left; width: 80%; margin-right: 8%;">
                            <label id="lblarquivo" for="arquivo">Arquivo:</label>
                            <input class="btn btn-primary" style="height:20px; line-height:21px;width: 100%;" type="file" name="file" id="file" <?php echo $disabled; ?>>
                        </div>                            
                        <div style="float:left; width:11%; margin-left:1%;">                                
                            <button type="button" id="bntAdicionaCarteira" name="bntAdicionaCarteira" onclick="AdicionarDoc(0)" class="btn btn-success" style="height:27px; padding-top:6px; margin-top: 15px;" <?php echo $disabled; ?>><span class="ico-plus"></span></button>
                        </div>                            
                        <table id="tbDocumentos" class="table" cellpadding="0" cellspacing="0" width="100%" <?php echo $disabled; ?>>
                            <thead>
                                <tr>
                                    <th width="30%">Data de Upload</th>
                                    <th width="45%">Nome do Arquivo</th>
                                    <th width="15%"><center>Tamanho</center></th>
                                    <th width="10%"><center>Ação</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table> 
                    </div>
                </div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">                    
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins.js"></script>
    <script type="text/javascript" src="../../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type="text/javascript" src="../../../js/plugins/datatables/fnReloadAjax.js"></script>
    <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../../js/util.js"></script> 
    <script type="text/javascript">  
        
        $('#tbDocumentos').dataTable({
            "iDisplayLength": 12,
            "ordering": false,
            "bFilter": false,
            "bInfo": false,
            "sAjaxSource": "../../Modulos/CRM/ajax/Documentos_server_processing.php?id=" + $("#txtId").val() + "&type=A_Pagar",
            "bLengthChange": false,
            "bPaginate": true,
            'oLanguage': {
                'oPaginate': {
                    'sFirst': "Primeiro",
                    'sLast': "Último",
                    'sNext': "Próximo",
                    'sPrevious': "Anterior"
                }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
            'sLengthMenu': "Visualização de _MENU_ registros",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"}
        });

        function refreshDoc() {
            let tblCRM = $('#tbDocumentos').dataTable();
            tblCRM.fnReloadAjax("../../Modulos/CRM/ajax/Documentos_server_processing.php?id=" + $("#txtId").val() + "&type=A_Pagar");
        }

        function AdicionarDoc(e) {
            if ($("#txtId").val().length > 0) {
                parent.$("#loader").show();
                var formData = new FormData();
                formData.append("btnSendFile", "S");
                formData.append("txtId", $("#txtId").val());
                formData.append("type", "A_Pagar");
                formData.append("file", $("#file")[0].files[0]); 
                $.ajax({
                    type: 'POST',
                    url: "../../Modulos/CRM/form/CRMDocumentos.php",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false
                }).done(function (data) {
                    parent.$("#loader").hide();
                    if (data === "YES") {
                        $('#file').val('');
                        refreshDoc();
                    } else {
                        bootbox.alert(data);
                    }
                });
            } else {
                bootbox.alert("É necessário salvar antes de realizar esta operação!");
            }
        }

        function RemoverDoc(filename) {
            var nomedoc = encodeURIComponent(filename.toString());
            bootbox.confirm('Confirma a exclusão do registro?', function (result) {
                if (result === true) {
                    $.post("../../Modulos/CRM/form/CRMDocumentos.php", "btnDelFile=S&txtId=" + $("#txtId").val() + 
                    "&type=A_Pagar&txtFileName=" + nomedoc).done(function (data) {
                        if (data === "YES") {
                            refreshDoc();
                        } else {
                            bootbox.alert(data);
                        }
                    });
                } else {
                    return;
                }
            });
        }
    </script> 
</body>
<?php odbc_close($con); ?>
</html>