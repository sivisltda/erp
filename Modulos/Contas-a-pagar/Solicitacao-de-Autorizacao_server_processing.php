<?php

if (!isset($_GET['pdf'])) {
    include '../../Connections/configini.php';
}
$aColumns = array('id_solicitacao_autorizacao', 'sf_solicitacao_autorizacao.empresa', 'sys_login', 'data_lanc', 'destinatario', 'historico', 'razao_social', 'sf_contas_movimento.tipo', 'sf_contas_movimento.descricao', 'destaprov', 'destaprov2', 'status');
$sOrder = " ORDER BY data_lanc desc ";
$iFilteredTotal = 0;
$sWhere = "";
$WhereX = " WHERE status IS NOT NULL ";
$sLimit = 20;
$imprimir = 0;

if ($_GET['imp'] != "") {
    $imprimir = $_GET['imp'];
}

if ($_GET['F1'] == "1") {
    $WhereX .= " AND status = 'Aguarda'";
} elseif ($_GET['F1'] == "2") {
    $WhereX .= " AND status = 'Aprovado'";
} elseif ($_GET['F1'] == "3") {
    $WhereX .= " AND status = 'Reprovado'";
} elseif ($_GET['F1'] == "4") {
    $WhereX .= " AND status <> 'Aprovado'";
}

if ($_GET['F2'] == "0") {
    $WhereX .= " AND sf_contas_movimento.tipo = 'C'";
} elseif ($_GET['F2'] == "1") {
    $WhereX .= " AND sf_contas_movimento.tipo = 'D'";
}

if (is_numeric($_GET['filial'])) {
    $WhereX .= " AND sf_solicitacao_autorizacao.empresa = " . $_GET['filial'];
}

if ($_GET['F3'] != "null") {
    $WhereX .= " AND sys_login = '" . $_GET['F3'] . "'";
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . " " . $_GET['sSortDir_' . $i] . ", ";
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY data_lanc desc";
    }
}

if ($_GET['Search'] != "") {
    $sWhere = " AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['Search']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

$query = "id_solicitacao_autorizacao,sf_solicitacao_autorizacao.empresa empresa,sys_login,
data_lanc,destinatario,historico,razao_social,sf_contas_movimento.tipo tipo,
sf_contas_movimento.descricao cdescricao,destaprov,destaprov2,status,
destinatario2,fornecedor_despesa,conta_movimento,sf_grupo_contas.descricao gdescricao,sf_tipo_documento.descricao tdescricao
,isnull((SELECT min(data_parcela) FROM sf_solicitacao_autorizacao_parcelas E WHERE E.inativo = 0 and sf_solicitacao_autorizacao.id_solicitacao_autorizacao = E.solicitacao_autorizacao),'') uv
,isnull((SELECT count(valor_parcela) FROM sf_solicitacao_autorizacao_parcelas E WHERE E.inativo = 0 and sf_solicitacao_autorizacao.id_solicitacao_autorizacao = E.solicitacao_autorizacao),'0') mp
,isnull((SELECT SUM(valor_parcela) FROM sf_solicitacao_autorizacao_parcelas E WHERE E.inativo = 0 and data_parcela is not null AND sf_solicitacao_autorizacao.id_solicitacao_autorizacao = E.solicitacao_autorizacao), 0.00) ea                                                                         
,data_aprov,numero_filial from sf_solicitacao_autorizacao inner join sf_grupo_contas on grupo_conta = id_grupo_contas
inner join sf_contas_movimento on conta_movimento = id_contas_movimento
inner join sf_fornecedores_despesas on fornecedor_despesa = id_fornecedores_despesas
left join sf_filiais on sf_filiais.id_filial = sf_solicitacao_autorizacao.empresa
inner join sf_tipo_documento on tipo_documento = id_tipo_documento " . $WhereX . $sWhere;

$sQuery = "SELECT COUNT(*) total FROM (SELECT " . $query . ") as x ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iFilteredTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row, " . $query .
        ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder;
//echo $sQuery1; exit();

$cur = odbc_exec($con, $sQuery1);
while ($RFP = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<center><input id=\"check\" type=\"checkbox\" class=\"caixa\" name=\"items[]\" value=\"" . $RFP['id_solicitacao_autorizacao'] . "\" " . (((utf8_encode($RFP['destinatario']) == $_SESSION["login_usuario"] && $RFP['destaprov'] == 0) || (utf8_encode($RFP['destinatario2']) == $_SESSION["login_usuario"]) && $RFP['destaprov2'] == 0) ? "" : "disabled") . "/></center>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($RFP['numero_filial']) . "'><center>" . utf8_encode($RFP['numero_filial']) . "</center></div>";
    $row[] = "<div id='formPQ'><center><a href='FormSolicitacao-de-Autorizacao.php?id=" . $RFP['id_solicitacao_autorizacao'] . "' >" . escreverData($RFP['data_lanc']) . "</a></center></div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($RFP['sys_login']) . "'><center>" . utf8_encode($RFP['sys_login']) . "</center></div>";
    $row[] = "<div id='formPQ'><center>" . escreverData($RFP['uv']) . "</center></div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($RFP['destinatario']) . "'><center>" . utf8_encode($RFP['destinatario']) . "</center></div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($RFP['historico']) . "'>" . utf8_encode(substr($RFP['historico'], 0, 40)) . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($RFP['razao_social']) . "'>" . utf8_encode(substr($RFP['razao_social'], 0, 30)) . "</div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($RFP['tipo']) . "'><center>" . utf8_encode($RFP['tipo']) . "</center></div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($RFP['cdescricao']) . "'>" . utf8_encode($RFP['cdescricao']) . "</div>";
    $row[] = "<div id='formPQ' title='" . $RFP['mp'] . "'><center>" . $RFP['mp'] . "</center></div>";
    $row[] = "<div id='formPQ' title='" . escreverNumero($RFP['ea'], 1) . "'>" . escreverNumero($RFP['ea'], 1) . "</div>";
    $color = "style=\"color:";
    if (utf8_encode($RFP['status']) == 'Aguarda') {
        $color = $color . "#bc9f00\"";
    } elseif (utf8_encode($RFP['status']) == 'Aprovado') {
        $color = $color . "#0066cc\"";
    } elseif (utf8_encode($RFP['status']) == 'Reprovado') {
        $color = $color . "#f00\"";
    } else {
        $color = $color . "black\"";
    }
    if ($RFP['data_aprov'] != "") {
        $titleStatus = utf8_encode($RFP['status'] . " por " . $RFP['destinatario'] . ": " . escreverDataHora($RFP['data_aprov']));
    } else {
        $titleStatus = utf8_encode($RFP['status']);
    }
    $row[] = "<div id='formPQ' " . $color . " title=\"" . $titleStatus . "\"><center>" . utf8_encode($RFP['status']) . "</center></div>";
    $row[] = "<center><a title=\"Parcelas\" href=\"javascript:void(0)\" onClick=\"AbrirBox(" . $RFP['id_solicitacao_autorizacao'] . ")\"><center><img src=\"../../img/1367947965_schedule.png\" width=\"15\" height=\"15\"></a></center>";
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
