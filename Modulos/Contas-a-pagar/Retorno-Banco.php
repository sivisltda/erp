<?php
include "../../Connections/configini.php";

$nome = "";
$msg = "";
$banco = "";
$pasta = "./../../Pessoas/" . $contrato . "/Retorno/";

$legenda["02"] = "ENTRADA CONFIRMADA";
$legenda["03"] = "ENTRADA REJEITADA (NOTA 23 - TABELA 1)";
$legenda["04"] = "ALTERAÇÃO DE DADOS – NOVA ENTRADA OU ALTERAÇÃO/EXCLUSÃOA DOS ACATADA";
$legenda["05"] = "ALTERAÇÃO DE DADOS – BAIXA";
$legenda["06"] = "LIQUIDAÇÃO NORMAL";
$legenda["08"] = "LIQUIDAÇÃO EM CARTÓRIO";
$legenda["09"] = "BAIXA SIMPLES";
$legenda["10"] = "BAIXA POR TER SIDO LIQUIDADO";
$legenda["11"] = "EM SER (SÓ NO RETORNO MENSAL)";
$legenda["12"] = "ABATIMENTO CONCEDIDO";
$legenda["13"] = "ABATIMENTO CANCELADO";
$legenda["14"] = "VENCIMENTO ALTERADO";
$legenda["15"] = "BAIXAS REJEITADAS (NOTA 23 - TABELA 4)";
$legenda["16"] = "INSTRUÇÕES REJEITADAS (NOTA 23 – TABELA 3)";
$legenda["17"] = "ALTERAÇÃO/EXCLUSÃO DE DADOS REJEITADA (NOTA 23 - TABELA 2)";
$legenda["18"] = "COBRANÇA CONTRATUAL – INSTRUÇÕES/ALTERAÇÕES REJEITADAS/PENDENTES (NOTA 23 - TABELA 5)";
$legenda["19"] = "CONFIRMAÇÃO RECEBIMENTO DE INSTRUÇÃO DE PROTESTO";
$legenda["20"] = "CONFIRMAÇÃO RECEBIMENTO DE INSTRUÇÃO DE SUSTAÇÃO DE PROTESTO /TARIFA";
$legenda["21"] = "CONFIRMAÇÃO RECEBIMENTO DE INSTRUÇÃO DE NÃO PROTESTAR";
$legenda["23"] = "PROTESTO ENVIADO A CARTÓRIO/TARIFA";
$legenda["24"] = "INSTRUÇÃO DE PROTESTO SUSTADA (NOTA 23 - TABELA 7)";
$legenda["25"] = "ALEGAÇÕES DO PAGADOR (NOTA 23 - TABELA 6)";
$legenda["26"] = "TARIFA DE AVISO DE COBRANÇA";
$legenda["27"] = "TARIFA DE EXTRATO POSIÇÃO (B40X)";
$legenda["28"] = "TARIFA DE RELAÇÃO DAS LIQUIDAÇÕES";
$legenda["29"] = "TARIFA DE MANUTENÇÃO DE TÍTULOS VENCIDOS";
$legenda["30"] = "DÉBITO MENSAL DE TARIFAS (PARA ENTRADAS E BAIXAS)";
$legenda["32"] = "BAIXA POR TER SIDO PROTESTADO";
$legenda["33"] = "CUSTAS DE PROTESTO";
$legenda["34"] = "CUSTAS DE SUSTAÇÃO";
$legenda["35"] = "CUSTAS DE CARTÓRIO DISTRIBUIDOR";
$legenda["36"] = "CUSTAS DE EDITAL";
$legenda["37"] = "TARIFA DE EMISSÃO DE BOLETO/TARIFA DE ENVIO DE DUPLICATA";
$legenda["38"] = "TARIFA DE INSTRUÇÃO";
$legenda["39"] = "TARIFA DE OCORRÊNCIAS";
$legenda["40"] = "TARIFA MENSAL DE EMISSÃO DE BOLETO/TARIFA MENSAL DE ENVIO DE DUPLICATA";
$legenda["41"] = "DÉBITO MENSAL DE TARIFAS – EXTRATO DE POSIÇÃO (B4EP/B4OX)";
$legenda["42"] = "DÉBITO MENSAL DE TARIFAS – OUTRAS INSTRUÇÕES";
$legenda["43"] = "DÉBITO MENSAL DE TARIFAS – MANUTENÇÃO DE TÍTULOS VENCIDOS";
$legenda["44"] = "DÉBITO MENSAL DE TARIFAS – OUTRAS OCORRÊNCIAS";
$legenda["45"] = "DÉBITO MENSAL DE TARIFAS – PROTESTO";
$legenda["46"] = "DÉBITO MENSAL DE TARIFAS – SUSTAÇÃO DE PROTESTO";
$legenda["47"] = "BAIXA COM TRANSFERÊNCIA PARA DESCONTO";
$legenda["48"] = "CUSTAS DE SUSTAÇÃO JUDICIAL";
$legenda["51"] = "TARIFA MENSAL REFERENTE A ENTRADAS BANCOS CORRESPONDENTES NA CARTEIRA";
$legenda["52"] = "TARIFA MENSAL BAIXAS NA CARTEIRA";
$legenda["53"] = "TARIFA MENSAL BAIXAS EM BANCOS CORRESPONDENTES NA CARTEIRA";
$legenda["54"] = "TARIFA MENSAL DE LIQUIDAÇÕES NA CARTEIRA";
$legenda["55"] = "TARIFA MENSAL DE LIQUIDAÇÕES EM BANCOS CORRESPONDENTES NA CARTEIRA";
$legenda["56"] = "CUSTAS DE IRREGULARIDADE";
$legenda["57"] = "INSTRUÇÃO CANCELADA (NOTA 23 – TABELA 8)";
$legenda["60"] = "ENTRADA REJEITADA CARNÊ (NOTA 20 – TABELA 1)";
$legenda["61"] = "TARIFA EMISSÃO AVISO DE MOVIMENTAÇÃO DE TÍTULOS (2154)";
$legenda["62"] = "DÉBITO MENSAL DE TARIFA – AVISO DE MOVIMENTAÇÃO DE TÍTULOS (2154)";
$legenda["63"] = "TÍTULO SUSTADO JUDICIALMENTE";
$legenda["74"] = "INSTRUÇÃO DE NEGATIVAÇÃO EXPRESSA REJEITADA (NOTA 25 – TABELA 3)";
$legenda["75"] = "CONFIRMA O RECEBIMENTO DE INSTRUÇÃO DE ENTRADA EM NEGATIVAÇÃO EXPRESSA";
$legenda["77"] = "CONFIRMA O RECEBIMENTO DE INSTRUÇÃO DE EXCLUSÃO DE ENTRADA EM NEGATIVAÇÃO EXPRESSA";
$legenda["78"] = "CONFIRMA O RECEBIMENTO DE INSTRUÇÃO DE CANCELAMENTO DA NEGATIVAÇÃO EXPRESSA";
$legenda["79"] = "NEGATIVAÇÃO EXPRESSA INFORMACIONAL (NOTA 25 – TABELA 12)";
$legenda["80"] = "CONFIRMAÇÃO DE ENTRADA EM NEGATIVAÇÃO EXPRESSA – TARIFA";
$legenda["82"] = "CONFIRMAÇÃO O CANCELAMENTO DE NEGATIVAÇÃO EXPRESSA - TARIFA";
$legenda["83"] = "CONFIRMAÇÃO DA EXCLUSÃO/CANCELAMENTO DA NEGATIVAÇÃO EXPRESSA POR LIQUIDAÇÃO - TARIFA";
$legenda["85"] = "TARIFA POR BOLETO (ATÉ 03 ENVIOS) COBRANÇA ATIVA ELETRÔNICA";
$legenda["86"] = "TARIFA EMAIL COBRANÇA ATIVA ELETRÔNICA";
$legenda["87"] = "TARIFA SMS COBRANÇA ATIVA ELETRÔNICA";
$legenda["88"] = "TARIFA MENSAL POR BOLETO (ATÉ 03 ENVIOS) COBRANÇA ATIVA ELETRÔNICA";
$legenda["89"] = "TARIFA MENSAL EMAIL COBRANÇA ATIVA ELETRÔNICA";
$legenda["90"] = "TARIFA MENSAL SMS COBRANÇA ATIVA ELETRÔNICA";
$legenda["91"] = "TARIFA MENSAL DE EXCLUSÃO DE ENTRADA EM NEGATIVAÇÃO EXPRESSA";
$legenda["92"] = "TARIFA MENSAL DE CANCELAMENTO DE NEGATIVAÇÃO EXPRESSA";
$legenda["93"] = "TARIFA MENSAL DE EXCLUSÃO/CANCELAMENTO DE NEGATIVAÇÃO EXPRESSA POR LIQUIDAÇÃO";
$legenda["94"] = "CONFIRMA RECEBIMENTO DE INSTRUÇÃO DE NÃO NEGATIVAR";

$bol_baixa_enc_ = 0;
$cur = odbc_exec($con, "select bol_baixa_enc from sf_configuracao where id = 1");
while ($RFP = odbc_fetch_array($cur)) {
    $bol_baixa_enc_ = $RFP["bol_baixa_enc"];
}

if (isset($_POST['txtNome'])) {
    $nome = $_POST['txtNome'];
}
if (isset($_POST['txtBanco'])) {
    $banco = $_POST['txtBanco'];
}

if (isset($_POST['bntSave'])) {
    if ($_POST['txtBanco'] == "") {
        echo "<script>alert('Selecione um Banco, para efetuar esta operação!');</script>";
    } else {
        $items = $_POST['items'];
        for ($i = 0; $i < sizeof($items); $i++) {
            $Datas = valoresData('txtDt_' . $items[$i]);
            $valorParcela = $_POST['txtVl_' . $items[$i]];
            $NossoNumeros = $_POST['txtNn_' . $items[$i]];
            $valorDesconto = $_POST['txtCs_' . $items[$i]];
            $valorMulta = 0;
            $valorJuros = 0;
            if (is_numeric($valorParcela) && $Datas != "null") {
                if ($valorParcela > 0) {
                    $count = explode("|", $items[$i]);
                    if (count($count) == 2) {
                        $item = explode("-", $count[0]);
                        if (count($item) == 2) {
                            if ($item[0] == 'A') {
                                odbc_exec($con, "set dateformat dmy; update sf_solicitacao_autorizacao_parcelas set obs_baixa = '" . $nome . "', id_banco = " . $_POST['txtBanco'] . ", bol_desconto = " . $valorDesconto . ", valor_pago = " . $valorParcela . ", data_pagamento = " . $Datas . ", valor_multa = " . $valorMulta . ", valor_juros = " . $valorJuros . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela = '" . $item[1] . "'") or die(odbc_errormsg());
                            } else if ($item[0] == 'D') {
                                odbc_exec($con, "set dateformat dmy; update sf_lancamento_movimento_parcelas set obs_baixa = '" . $nome . "', id_banco = " . $_POST['txtBanco'] . ", bol_desconto = " . $valorDesconto . ", valor_pago = " . $valorParcela . ", data_pagamento = " . $Datas . ", valor_multa = " . $valorMulta . ", valor_juros = " . $valorJuros . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela = '" . $item[1] . "'") or die(odbc_errormsg());
                            } else if ($item[0] == 'V') {
                                odbc_exec($con, "set dateformat dmy; update sf_venda_parcelas set obs_baixa = '" . $nome . "', id_banco = " . $_POST['txtBanco'] . ", bol_desconto = " . $valorDesconto . ", valor_pago = " . $valorParcela . ", data_pagamento = " . $Datas . ", valor_multa = " . $valorMulta . ", valor_juros = " . $valorJuros . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela = '" . $item[1] . "'") or die(odbc_errormsg());
                            } else if ($item[0] == 'M') {
                                $query = "set dateformat dmy; BEGIN TRANSACTION ";            
                                $query .= "insert into sf_vendas(vendedor, cliente_venda, historico_venda, data_venda, sys_login, empresa, destinatario, status, tipo_documento, cov,
                                descontop, descontos, descontotp, descontots, n_servicos, data_aprov, grupo_conta, conta_movimento, favorito, reservar_estoque, n_produtos)
                                values ('" . $_SESSION["login_usuario"] . "',(select max(favorecido) from sf_vendas_planos_mensalidade inner join sf_vendas_planos on id_plano_mens = sf_vendas_planos.id_plano where id_mens = " . $item[1] . ")," . 
                                valoresTexto2('PAG PLANO / SERVIÇO') . "," . $Datas . ",'" . $_SESSION["login_usuario"] . "',1,'" . $_SESSION["login_usuario"] . "','Aprovado',10,'V',0.00,0.00,0,0,0," . $Datas . ",14,1,0,0,0);
                                DECLARE @idVenda BIGINT;SELECT @idVenda = SCOPE_IDENTITY();";                                
                                $query .= "insert into sf_vendas_itens(id_venda, grupo, produto, quantidade, valor_total, valor_bruto, valor_multa)
                                values (@idVenda,null,(select max(id_prod_plano) from sf_vendas_planos_mensalidade inner join sf_vendas_planos on id_plano_mens = sf_vendas_planos.id_plano where id_mens = " . $item[1] . "),1," . $valorParcela . "," . $valorParcela . "," . $valorMulta . ");                                    
                                DECLARE @idItemVenda BIGINT;SELECT @idItemVenda = SCOPE_IDENTITY();";                                
                                $query .= "update sf_vendas_itens set grupo = sf_produtos.conta_movimento from sf_vendas_itens
                                inner join sf_produtos on sf_produtos.conta_produto = sf_vendas_itens.produto
                                where id_item_venda = @idItemVenda;";
                                //------------------------------------------------------------------------------------------------------------------------
                                $query .= "insert into sf_vendas_itens(id_venda, grupo, produto, quantidade, valor_total, valor_bruto, valor_multa)
                                select @idVenda, conta_movimento, conta_produto, 1, preco_venda, preco_venda, 0 from sf_boleto b
                                inner join sf_boleto_adicional ba on b.id_boleto = ba.id_boleto
                                inner join sf_produtos on conta_produto = id_produto
                                where id_referencia = " . $item[1] . " and inativo = 0 and tp_referencia = 'M';";
                                $query .= "update sf_vendas_itens set valor_total = valor_total - isnull((select sum(valor_total) from sf_vendas_itens y where y.id_venda = sf_vendas_itens.id_venda and id_item_venda not in(@idItemVenda)),0),
                                valor_bruto = valor_bruto - isnull((select sum(valor_bruto) from sf_vendas_itens y where y.id_venda = sf_vendas_itens.id_venda and id_item_venda not in(@idItemVenda)),0) where id_item_venda = @idItemVenda;";
                                //------------------------------------------------------------------------------------------------------------------------
                                $query .= "insert into sf_venda_parcelas(venda, numero_parcela, data_parcela, valor_parcela, valor_pago, data_pagamento, id_banco,pa,
                                inativo, historico_baixa, valor_multa, valor_juros, syslogin, tipo_documento, valor_parcela_liquido, data_cadastro, exp_remessa, somar_rel, obs_baixa)
                                values(@idVenda, '1'," . $Datas . ", " . $valorParcela . "," . $valorParcela . "," . $Datas . "," . $_POST['txtBanco'] . ",'01/01',
                                0," . valoresTexto2('PAG PLANO / SERVIÇO') . "," . $valorMulta . "," . $valorJuros . ",'" . $_SESSION["login_usuario"] . "',10,0.00, GETDATE(),0,1," . valoresTexto2($nome) . ");";
                                $query .= "update sf_vendas_planos_mensalidade set id_item_venda_mens = @idItemVenda, dt_pagamento_mens = " . $Datas . " where id_mens = " . $item[1] . ";";                                                                  
                                $query .= "update sf_venda_parcelas set valor_parcela_liquido = 
                                [dbo].[FU_TAXA_FORMA_PAGAMENTO](sf_venda_parcelas.tipo_documento,sf_venda_parcelas.venda, 0, valor_parcela)
                                where venda = @idVenda;";                                
                                $query .= "update sf_vendas set conta_movimento = [dbo].[FU_SUB_GRUPO_VENDA](id_venda) where cov = 'V' and id_venda = @idVenda;";
                                $query .= "insert into sf_vendas_planos_mensalidade (id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens)
                                select id_plano_mens, dateadd(month ,1,dt_inicio_mens), dateadd(day,-1,dateadd(month,2,dt_inicio_mens)), id_parc_prod_mens, GETDATE(),valor_unico valor_mens 
                                from sf_vendas_planos_mensalidade m1 inner join sf_produtos_parcelas on id_parc_prod_mens = id_parcela 
                                inner join sf_produtos on sf_produtos.conta_produto = sf_produtos_parcelas.id_produto
                                inner join sf_vendas_planos on id_plano = id_plano_mens                                
                                where inativar_renovacao = 0 and id_mens = " . $item[1] . " and dt_cancelamento is null and parcela in (-1,0,1) 
                                and (select COUNT(*) from sf_vendas_planos_mensalidade m2
                                where m2.id_plano_mens = m1.id_plano_mens and m2.id_parc_prod_mens = m1.id_parc_prod_mens
                                and month(m2.dt_inicio_mens) = month(dateadd(month ,1,m1.dt_inicio_mens))
                                and year(m2.dt_inicio_mens) = year(dateadd(month ,1,m1.dt_inicio_mens))) = 0;";                                   
                                $query .= "update sf_vendas_planos set dt_fim = dbo.FU_PLANO_FIM(id_plano) where id_plano in(
                                select id_plano_mens from sf_vendas_planos_mensalidade where id_mens = " . $item[1] . ")";                    
                                $query .= "IF @@ERROR = 0 COMMIT ELSE ROLLBACK;";
                                //echo $query; exit;
                                odbc_exec($con, $query) or die(odbc_errormsg());                                                                                                
                            } else if ($item[0] == 'Y') {
                                $cur2 = odbc_exec($con, "select id_parcela,valor_parcela from sf_lancamento_movimento_parcelas where id_parcela in
                                    (select id_parcela from sf_lancamento_movimento_parcelas where bol_nosso_numero = '" . $NossoNumeros . "')") or die(odbc_errormsg());
                                while ($RFP2 = odbc_fetch_array($cur2)) {
                                    $indice = (($RFP2['valor_parcela'] * 100) / $valorParcela) / 100;
                                    if ($indice > 0) {
                                        odbc_exec($con, "set dateformat dmy; update sf_lancamento_movimento_parcelas set obs_baixa = '" . $nome . "', id_banco = " . $_POST['txtBanco'] . ", valor_pago = " . ($valorParcela * $indice) . ", data_pagamento = " . $Datas . ", valor_multa = " . $valorMulta . ", valor_juros = " . $valorJuros . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela = '" . $RFP2['id_parcela'] . "'") or die(odbc_errormsg());
                                    }
                                }
                            }
                        }
                    } else {
                        if (count($count) > 2) {
                            $j = 0;   
                            $array = [];                          
                            $cur = odbc_exec($con, "set dateformat dmy; 
                            select 'D-' +cast(sf_lancamento_movimento_parcelas.id_parcela as VARCHAR) as pk ,data_vencimento 'Vecto',pa ,sf_lancamento_movimento.empresa empresa,razao_social, historico_baixa, 'D' Origem , sf_contas_movimento.descricao 'cm',
                            valor_parcela,valor_pago,sf_grupo_contas.descricao gc,sf_lancamento_movimento_parcelas.inativo inativo, sf_lancamento_movimento_parcelas.sa_descricao documento,obs_baixa,isnull(bol_id_banco,0) bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,bol_nosso_numero,data_pagamento,data_cadastro from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento 
                            on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
                            inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
                            inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
                            left join sf_tipo_documento on sf_lancamento_movimento_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento                                                                        
                            inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_lancamento_movimento.status = 'Aprovado' and sf_contas_movimento.tipo = 'C' and sf_lancamento_movimento_parcelas.bol_nosso_numero = '" . $NossoNumeros . "'
                            and (sf_lancamento_movimento_parcelas.bol_nosso_numero is null or sf_lancamento_movimento_parcelas.bol_nosso_numero not in (select bol_nosso_numero from sf_lancamento_movimento_parcelas group by bol_nosso_numero having bol_nosso_numero is not null and count(id_parcela) > 1))                                                                            
                            union
                            select 'Y-' +cast(max(sf_lancamento_movimento_parcelas.id_parcela) as VARCHAR) as pk,MAX(data_vencimento) 'Vecto',MAX(pa) pa,MAX(sf_lancamento_movimento.empresa) empresa
                            ,MAX(razao_social) razao_social, MAX(historico_baixa) historico_baixa, 'D' Origem , MAX(sf_contas_movimento.descricao) 'cm',
                            sum(valor_parcela) valor_parcela,sum(valor_pago) valor_pago,MAX(sf_grupo_contas.descricao) gc,MAX(sf_lancamento_movimento_parcelas.inativo) inativo, 
                            MAX(sf_lancamento_movimento_parcelas.sa_descricao) documento,
                            MAX(obs_baixa) obs_baixa,MAX(isnull(bol_id_banco,0)) bol_id_banco,MAX(sf_tipo_documento.abreviacao)abreviacao,
                            MAX(sf_tipo_documento.descricao) tdn,MAX(id_fornecedores_despesas) id_fornecedores_despesas,MAX(bol_nosso_numero) bol_nosso_numero, MAX(data_pagamento) data_pagamento, Max(data_cadastro) data_cadastro
                            from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento 
                            on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
                            inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
                            inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
                            left join sf_tipo_documento on sf_lancamento_movimento_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento                                                                        
                            inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_lancamento_movimento.status = 'Aprovado' and sf_contas_movimento.tipo = 'C' and sf_lancamento_movimento_parcelas.bol_nosso_numero = '" . $NossoNumeros . "'
                            group by bol_nosso_numero having bol_nosso_numero is not null and count(id_parcela) > 1
                            union
                            select 'A-' +cast(sf_solicitacao_autorizacao_parcelas.id_parcela as VARCHAR) as pk ,data_parcela 'Vecto',pa ,sf_solicitacao_autorizacao.empresa empresa,razao_social, historico_baixa,'A' Origem, sf_contas_movimento.descricao 'cm',
                            valor_parcela,valor_pago,sf_grupo_contas.descricao gc,sf_solicitacao_autorizacao_parcelas.inativo inativo,sf_solicitacao_autorizacao_parcelas.sa_descricao documento,obs_baixa,isnull(bol_id_banco,0) bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,bol_nosso_numero,data_pagamento,data_cadastro from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
                            on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao                                                                        
                            inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
                            inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas
                            left join sf_tipo_documento on sf_solicitacao_autorizacao_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento                                                                                                                                                
                            inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'C' and status = 'Aprovado' and sf_solicitacao_autorizacao_parcelas.bol_nosso_numero = '" . $NossoNumeros . "'
                            union    
                            select 'V-' +cast(sf_venda_parcelas.id_parcela as VARCHAR) as pk ,data_parcela 'Vecto',pa ,sf_vendas.empresa empresa,razao_social, sf_vendas.historico_venda historico_baixa,'V' Origem, 
                            'VENDAS' cm,valor_parcela,valor_pago,'VENDAS' gc,sf_venda_parcelas.inativo inativo,sf_venda_parcelas.sa_descricao documento,obs_baixa,bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,bol_nosso_numero,data_pagamento,data_cadastro   
                            from sf_venda_parcelas inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda left join sf_tipo_documento on sf_venda_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento                                                                                                                                                                                                    
                            inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas where sf_vendas.cov = 'V' and status = 'Aprovado' and sf_venda_parcelas.bol_nosso_numero = '" . $NossoNumeros . "'
                            union    
                            select 'M-' +cast(id_mens as VARCHAR) as pk ,isnull(bol_data_parcela,dt_inicio_mens) 'Vecto','01/01' pa ,1 empresa,razao_social, 'PAG MENSALIDADE' historico_baixa,'M' Origem, 
                            'MENSALIDADE' cm,bol_valor valor_parcela,isnull(valor_total,0) valor_pago,'MENSALIDADE' gc,sf_boleto.inativo inativo,'' documento,null obs_baixa,bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,bol_nosso_numero,dt_pagamento_mens,bol_data_criacao   
                            from sf_boleto inner join sf_vendas_planos_mensalidade on sf_boleto.id_referencia = sf_vendas_planos_mensalidade.id_mens and tp_referencia = 'M' inner join sf_vendas_planos on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens left join sf_vendas_itens on sf_vendas_itens.id_item_venda = sf_vendas_planos_mensalidade.id_item_venda_mens left join sf_tipo_documento on sf_boleto.tipo_documento = sf_tipo_documento.id_tipo_documento
                            inner join sf_fornecedores_despesas on sf_vendas_planos.favorecido = sf_fornecedores_despesas.id_fornecedores_despesas 
                            where sf_boleto.inativo = 0 and bol_nosso_numero = '" . $NossoNumeros . "'");
                            while ($RFP = odbc_fetch_array($cur)) {
                                $array[$j]['pk'] = $RFP['pk'];
                                $array[$j]['valor'] = $RFP['valor_parcela'];
                                $j += 1;
                            }       
                            $query = "set dateformat dmy;";                              
                            for ($k = 0; $k <= count($array); $k++) {
                                $indice = (($array[$k]['valor'] * 100) / $valorParcela) / 100;
                                if ($indice > 0) {
                                    $item = explode("-", $array[$k]['pk']);
                                    if (count($item) == 2) {
                                        if ($item[0] == 'A') {
                                            $query = $query . "update sf_solicitacao_autorizacao_parcelas set obs_baixa = '" . $nome . "', id_banco = " . $_POST['txtBanco'] . ", valor_pago = " . ($valorParcela * $indice) . ", data_pagamento = " . $Datas . ", valor_multa = " . $valorMulta . ", valor_juros = " . $valorJuros . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela = '" . $item[1] . "';";
                                        } else if ($item[0] == 'D') {
                                            $query = $query . "update sf_lancamento_movimento_parcelas set obs_baixa = '" . $nome . "', id_banco = " . $_POST['txtBanco'] . ", valor_pago = " . ($valorParcela * $indice) . ", data_pagamento = " . $Datas . ", valor_multa = " . $valorMulta . ", valor_juros = " . $valorJuros . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela = '" . $item[1] . "';";
                                        } else if ($item[0] == 'V') {
                                            $query = $query . "update sf_venda_parcelas set obs_baixa = '" . $nome . "', id_banco = " . $_POST['txtBanco'] . ", valor_pago = " . ($valorParcela * $indice) . ", data_pagamento = " . $Datas . ", valor_multa = " . $valorMulta . ", valor_juros = " . $valorJuros . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela = '" . $item[1] . "';";
                                        } else if ($item[0] == 'M') {                                                     
                                            $query .= " BEGIN TRANSACTION insert into sf_vendas(vendedor, cliente_venda, historico_venda, data_venda, sys_login, empresa, destinatario, status, tipo_documento, cov,
                                            descontop, descontos, descontotp, descontots, n_servicos, data_aprov, grupo_conta, conta_movimento, favorito, reservar_estoque, n_produtos)
                                            values ('" . $_SESSION["login_usuario"] . "',(select max(favorecido) from sf_vendas_planos_mensalidade inner join sf_vendas_planos on id_plano_mens = sf_vendas_planos.id_plano where id_mens = " . $item[1] . ")," . 
                                            valoresTexto2('PAG PLANO / SERVIÇO') . "," . $Datas . ",'" . $_SESSION["login_usuario"] . "',1,'" . $_SESSION["login_usuario"] . "','Aprovado',10,'V',0.00,0.00,0,0,0," . $Datas . ",14,1,0,0,0);
                                            DECLARE @idVenda" . $k . " BIGINT;SELECT @idVenda" . $k . " = SCOPE_IDENTITY();";                                                       
                                            $query .= "insert into sf_vendas_itens(id_venda, grupo, produto, quantidade, valor_total, valor_bruto, valor_multa)
                                            values (@idVenda" . $k . ",null,(select max(id_prod_plano) from sf_vendas_planos_mensalidade inner join sf_vendas_planos on id_plano_mens = sf_vendas_planos.id_plano where id_mens = " . $item[1] . "),1," . ($valorParcela * $indice) . "," . ($valorParcela * $indice) . "," . $valorMulta . ");                                    
                                            DECLARE @idItemVenda" . $k . " BIGINT;SELECT @idItemVenda" . $k . " = SCOPE_IDENTITY();";                                                                                                                                                                                
                                            $query .= "update sf_vendas_itens set grupo = sf_produtos.conta_movimento from sf_vendas_itens
                                            inner join sf_produtos on sf_produtos.conta_produto = sf_vendas_itens.produto
                                            where id_item_venda = @idItemVenda" . $k . ";";                                            
                                            $query .= "insert into sf_venda_parcelas(venda, numero_parcela, data_parcela, valor_parcela, valor_pago, data_pagamento, id_banco,pa,
                                            inativo, historico_baixa, valor_multa, valor_juros, syslogin, tipo_documento, valor_parcela_liquido, data_cadastro, exp_remessa, somar_rel, obs_baixa)
                                            values(@idVenda" . $k . ", '1'," . $Datas . ", " . ($valorParcela * $indice) . "," . ($valorParcela * $indice) . "," . $Datas . "," . $_POST['txtBanco'] . ",'01/01',
                                            0," . valoresTexto2('PAG PLANO / SERVIÇO') . "," . $valorMulta . "," . $valorJuros . ",'" . $_SESSION["login_usuario"] . "',10,0.00, GETDATE(),0,1," . valoresTexto2($nome) . ");";                                            
                                            $query .= "update sf_vendas_planos_mensalidade set id_item_venda_mens = @idItemVenda" . $k . ", dt_pagamento_mens = " . $Datas . " where id_mens = " . $item[1] . ";";                                                                               
                                            $query .= "update sf_venda_parcelas set valor_parcela_liquido = 
                                            [dbo].[FU_TAXA_FORMA_PAGAMENTO](sf_venda_parcelas.tipo_documento,sf_venda_parcelas.venda, 0, valor_parcela)
                                            where venda = @idVenda" . $k . ";";                                            
                                            $query .= "update sf_vendas set conta_movimento = [dbo].[FU_SUB_GRUPO_VENDA](id_venda) where cov = 'V' and id_venda = @idVenda" . $k . ";";
                                            $query .= "insert into sf_vendas_planos_mensalidade (id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens)
                                            select id_plano_mens, dateadd(month ,1,dt_inicio_mens), dateadd(day,-1,dateadd(month,2,dt_inicio_mens)), id_parc_prod_mens, GETDATE(),valor_unico valor_mens 
                                            from sf_vendas_planos_mensalidade m1 inner join sf_produtos_parcelas on id_parc_prod_mens = id_parcela 
                                            inner join sf_produtos on sf_produtos.conta_produto = sf_produtos_parcelas.id_produto
                                            inner join sf_vendas_planos on id_plano = id_plano_mens                                            
                                            where inativar_renovacao = 0 and id_mens = " . $item[1] . " and dt_cancelamento is null and parcela in (-1,0,1) 
                                            and (select COUNT(*) from sf_vendas_planos_mensalidade m2
                                            where m2.id_plano_mens = m1.id_plano_mens and m2.id_parc_prod_mens = m1.id_parc_prod_mens
                                            and month(m2.dt_inicio_mens) = month(dateadd(month ,1,m1.dt_inicio_mens))
                                            and year(m2.dt_inicio_mens) = year(dateadd(month ,1,m1.dt_inicio_mens))) = 0;";                                               
                                            $query .= "update sf_vendas_planos set dt_fim = dbo.FU_PLANO_FIM(id_plano) where id_plano in(
                                            select id_plano_mens from sf_vendas_planos_mensalidade where id_mens = " . $item[1] . ")";                    
                                            $query .= "IF @@ERROR = 0 COMMIT ELSE ROLLBACK;";                                                                                       
                                        } else if ($item[0] == 'Y') {
                                            $cur2 = odbc_exec($con, "select id_parcela,valor_parcela from sf_lancamento_movimento_parcelas where id_parcela in
                                                (select id_parcela from sf_lancamento_movimento_parcelas where bol_nosso_numero = '" . $NossoNumeros . "')") or die(odbc_errormsg());
                                            while ($RFP2 = odbc_fetch_array($cur2)) {
                                                $indiceX = (($RFP2['valor_parcela'] * 100) / $valorParcela) / 100;
                                                if ($indiceX > 0) {
                                                    $query = $query . "update sf_lancamento_movimento_parcelas set obs_baixa = '" . $nome . "', id_banco = " . $_POST['txtBanco'] . ", valor_pago = " . ($valorParcela * $indiceX) . ", data_pagamento = " . $Datas . ", valor_multa = " . $valorMulta . ", valor_juros = " . $valorJuros . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela = '" . $RFP2['id_parcela'] . "';";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            odbc_exec($con, $query) or die(odbc_errormsg());
                        }
                    }
                }
            }
        }
        $itemsPapen = $_POST['itemsPapen'];
        for ($i = 0; $i < sizeof($itemsPapen); $i++) {
            $itens = explode("|", $itemsPapen[$i]);
            $query = "set dateformat dmy; BEGIN TRANSACTION ";            
            $query .= "insert into sf_vendas(vendedor, cliente_venda, historico_venda, data_venda, sys_login, empresa, destinatario, status, tipo_documento, cov,
            descontop, descontos, descontotp, descontots, n_servicos, data_aprov, grupo_conta, conta_movimento, favorito, reservar_estoque, n_produtos)
            values ('" . $_SESSION["login_usuario"] . "'," . $itens[2] . "," . valoresTexto2('PAG PLANO / SERVIÇO') . ", GETDATE(),'" . $_SESSION["login_usuario"] . "',1,'" . $_SESSION["login_usuario"] . "','Aprovado',8,'V',
            0.00,0.00,0,0,0,GETDATE(),14,1,0,0,0);
            DECLARE @idVenda BIGINT;SELECT @idVenda = SCOPE_IDENTITY();";            
            $query .= "insert into sf_vendas_itens(id_venda, grupo, produto, quantidade, valor_total, valor_bruto, valor_multa)
            values (@idVenda," . $itens[3] . "," . $itens[4] . ",1," . $itens[1] . "," . $itens[1] . ",0.00);
            DECLARE @idItemVenda BIGINT;SELECT @idItemVenda = SCOPE_IDENTITY();";            
            $query .= "insert into sf_venda_parcelas(venda, numero_parcela, data_parcela, valor_parcela, valor_pago, data_pagamento, id_banco,pa,
            inativo, historico_baixa, valor_multa, valor_juros, syslogin, tipo_documento, valor_parcela_liquido, data_cadastro, exp_remessa, somar_rel)
            values(@idVenda, '1', cast(GETDATE() as date), " . $itens[1] . "," . $itens[1] . ",GETDATE()," . $_POST['txtBanco'] . ",'01/01',
            0," . valoresTexto2('PAG PLANO / SERVIÇO') . ",0.00,0.00,'" . $_SESSION["login_usuario"] . "',8,0.00, GETDATE(),0,1);";
            $query .= "update sf_vendas_planos_mensalidade set id_item_venda_mens = @idItemVenda, dt_pagamento_mens = CAST(getdate() as date) where id_mens = " . $itens[0] . ";";                                               
            $query .= "update sf_venda_parcelas set valor_parcela_liquido = 
            [dbo].[FU_TAXA_FORMA_PAGAMENTO](sf_venda_parcelas.tipo_documento,sf_venda_parcelas.venda, 0, valor_parcela)
            where venda = @idVenda;";            
            $query .= "update sf_vendas set conta_movimento = [dbo].[FU_SUB_GRUPO_VENDA](id_venda) where cov = 'V' and id_venda = @idVenda;";            
            $query .= "insert into sf_vendas_planos_mensalidade (id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens)
            select id_plano_mens, dateadd(month ,1,dt_inicio_mens), dateadd(day,-1,dateadd(month,2,dt_inicio_mens)), id_parc_prod_mens, GETDATE(),valor_unico valor_mens 
            from sf_vendas_planos_mensalidade m1 inner join sf_produtos_parcelas on id_parc_prod_mens = id_parcela 
            inner join sf_produtos on sf_produtos.conta_produto = sf_produtos_parcelas.id_produto
            inner join sf_vendas_planos on id_plano = id_plano_mens            
            where inativar_renovacao = 0 and id_mens = " . $itens[0] . " and dt_cancelamento is null and parcela in (-1,0,1) 
            and (select COUNT(*) from sf_vendas_planos_mensalidade m2
            where m2.id_plano_mens = m1.id_plano_mens and m2.id_parc_prod_mens = m1.id_parc_prod_mens
            and month(m2.dt_inicio_mens) = month(dateadd(month ,1,m1.dt_inicio_mens))
            and year(m2.dt_inicio_mens) = year(dateadd(month ,1,m1.dt_inicio_mens))) = 0;";
            
            $query .= "update sf_vendas_planos set dt_fim = dbo.FU_PLANO_FIM(id_plano) where id_plano in(
            select id_plano_mens from sf_vendas_planos_mensalidade where id_mens = " . $itens[0] . ")";                    
            $query .= "IF @@ERROR = 0 COMMIT ELSE ROLLBACK;";  
            //echo $query; exit;
            odbc_exec($con, $query) or die(odbc_errormsg());
        }
    }
    $msg = "Conta(s) " . sizeof($items) . " encontradas baixadas com sucesso!";    
    echo "<script>alert('" . $msg . "');</script>";   
}

if (isset($_POST['bntLer'])) {
    $nome = $_FILES['arquivo']['name'];
    $type = $_FILES['arquivo']['type'];
    $size = $_FILES['arquivo']['size'];
    $tmp = $_FILES['arquivo']['tmp_name'];
    if ($tmp) {
        move_uploaded_file($tmp, $pasta . "/" . $nome);
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../SpryAssets/SpryValidationTextField.css"/>
        <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>        
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1>Financeiro<small>Boletos Registrados</small></h1>
                    </div>
                    <form action="Retorno-Banco.php" method="post" enctype="multipart/form-data" name="enviar" id="enviar">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="boxfilter block">
                                    <div style="float:left;">
                                        <div width="100%">Arquivo Retorno:</div>
                                        <input class="btn  btn-primary" type="file" name="arquivo" id="arquivo" style="height:20px; line-height:21px; width:100%"/>
                                    </div>
                                    <div style="margin-top: 15px;float:left;margin-left: 1%;">
                                        <div style="float:left">
                                            <button class="button button-turquoise btn-primary" type="submit" name="bntLer" id="bntLer" value="Ler" style="height: 29px;"><span>Ler</span></button>
                                        </div>
                                    </div>
                                    <div style="margin-top: 15px;float: right;margin-left: 1%;">                                    
                                        <select id="txtLayoutStatus" name="txtLayoutStatus" style="width:200px">
                                            <option value="null">Todos</option>
                                            <option value="inativo">Não identificados</option>
                                            <option value="apagar">A pagar</option>                                            
                                            <option value="apagarx">A pagar Divergentes</option>
                                            <option value="pago">Pagos</option>
                                            <option value="pagox">Pagos Divergentes</option>                                            
                                        </select>                                        
                                        <select name="txtBanco" style="width: 200px;" id="txtBanco">
                                            <option value="null">--Selecione--</option>
                                            <?php $cur = odbc_exec($con, "select id_bancos,razao_social from sf_bancos where ativo = 0 order by razao_social") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id_bancos'] ?>"<?php
                                                if (!(strcmp($RFP['id_bancos'], $banco))) {
                                                    echo "SELECTED";
                                                }
                                                ?>><?php echo utf8_encode($RFP['razao_social']) ?></option>
                                            <?php } ?>
                                        </select>
                                        <button class="button button-turquoise btn-primary" onclick="return baixar();" type="submit" name="bntSave" id="bntSave" <?php
                                        if ($msg != "") {
                                            echo "DISABLED";
                                        }
                                        ?> value="Salvar">Baixar</button> 
                                    </div>
                                </div>
                            </div>
                            <div style="clear:both"></div>
                            <div class="boxhead">
                                <div class="boxtext">Retorno de pagamentos efetuados em boletos</div>
                            </div>
                            <div class="boxtable">
                                <table id="tbRetorno" class="table" cellpadding="0" cellspacing="0" width="100%">                                    
                                    <thead>
                                        <tr>
                                            <th width="3%" style="text-align: center;"><input id="checkAll" type="checkbox" class="checkall"/></th>
                                            <th width="8%" style="text-align: center;">Arquivo</th>
                                            <th width="9%" style="text-align: center;">Ns. Núm.</th>
                                            <th width="4%" style="text-align: center;">Hist.</th>
                                            <th width="9%" style="text-align: center;">Dt. Venc.</th>                                                        
                                            <th width="8%" style="text-align: center;">Dt. Pagto</th> 
                                            <th width="9%" style="text-align: center;">Valor Bol.</th>                                                                                                                
                                            <th width="9%" style="text-align: center;">Valor Pag.</th>
                                            <th width="6%" style="text-align: center;">Val.Custo</th>
                                            <th width="8%" style="text-align: center;">Valor Pag.</th>
                                            <th width="20%" style="text-align: left;">Nome do Cliente</th>
                                            <th width="4%" style="text-align: center;">Parcela</th>
                                            <th width="3%" style="text-align: center;"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($nome != "") {
                                            $lendo = @fopen($pasta . "/" . $nome, "r");
                                            if (!$lendo) {
                                                echo "Erro ao abrir a URL.";
                                                exit;
                                            }
                                            $i = 0;
                                            $j = 0;
                                            $siccob = false;                                            
                                            $sicredi = false;                                            
                                            $bradesco = false;                                            
                                            while (!feof($lendo)) {
                                                $i++;
                                                $linha = fgets($lendo, 9999);
                                                $t_u_segmento = substr($linha, 13, 1);
                                                $t_tipo_reg = substr($linha, 7, 1);
                                                $t_tipo_historico = "02";
                                                $u_v_custo = 0;
                                                $clb_cpf = "";
                                                $clb_val = 0;
                                                $clb_data = "";
                                                $clb_nome = "";
                                                if ($t_u_segmento == 'T' && substr($linha, 0, 3) != "756") {
                                                    $t_id_titulo_banco = substr(substr($linha, 41, 15), 3, 10);
                                                } elseif ($t_u_segmento == 'T' && substr($linha, 0, 3) == "756") {        
                                                    $t_id_titulo_banco = substr($linha, 39, 7);
                                                } elseif ($t_u_segmento == 'U' && substr($linha, 0, 3)) {                                                   
                                                    $u_v_pago = substr(substr($linha, 77, 15), 0, 13) . '.' . substr(substr($linha, 77, 15), 13, 2);
                                                    $u_dt_ocorencia = substr(substr($linha, 137, 8), 0, 2) . '/' . substr(substr($linha, 137, 8), 2, 2) . '/' . substr(substr($linha, 137, 8), 4, 4);
                                                } elseif (substr($linha, 82, 7) == "SICREDI" && substr($linha, 2, 7) == "RETORNO") {                                                  
                                                    $sicredi = true;
                                                } elseif ($sicredi) { 
                                                    $t_id_titulo_banco = substr($linha, 50, 5);
                                                    $t_tipo_historico = substr($linha, 108, 2);
                                                    if (is_numeric(substr($linha, 334, 2)) && is_numeric(substr($linha, 332, 2)) && is_numeric(substr($linha, 330, 2))) {
                                                        $u_dt_ocorencia = substr($linha, 334, 2) . "/" . substr($linha, 332, 2) . "/20" . substr($linha, 330, 2);
                                                    } else if (is_numeric(substr($linha, 146, 2)) && is_numeric(substr($linha, 148, 2)) && is_numeric(substr($linha, 150, 2))) {
                                                        $u_dt_ocorencia = substr($linha, 146, 2) . "/" . substr($linha, 148, 2) . "/20" . substr($linha, 150, 2);
                                                    }                                                    
                                                    if (is_numeric(substr($linha, 253, 11)) && is_numeric(substr($linha, 264, 2))) { //254 266                                                   
                                                        $u_v_pago = substr($linha, 253, 11) . '.' . substr($linha, 264, 2);
                                                        $u_v_desconto = substr($linha, 240, 11) . '.' . substr($linha, 251, 2); //241 253
                                                    }                                                    
                                                    if (is_numeric(substr($linha, 175, 11)) && is_numeric(substr($linha, 186, 2))) { //176 188
                                                        $u_v_custo = substr($linha, 175, 11) . '.' . substr($linha, 186, 2);
                                                    }                                                                                                        
                                                } elseif (is_numeric(substr($linha, 76, 3)) && substr($linha, 76, 3) == "756") {   
                                                    $siccob = true;
                                                } elseif ($siccob) { 
                                                    $t_id_titulo_banco = substr($linha, 66, 7);
                                                    $t_tipo_historico = substr($linha, 108, 2);
                                                    if (is_numeric(substr($linha, 110, 2)) && is_numeric(substr($linha, 112, 2)) && is_numeric(substr($linha, 114, 2))) {
                                                        $u_dt_ocorencia = substr($linha, 110, 2) . "/" . substr($linha, 112, 2) . "/20" . substr($linha, 114, 2);
                                                    }
                                                    if (is_numeric(substr($linha, 253, 11)) && is_numeric(substr($linha, 264, 2))) { //254 266                                                   
                                                        $u_v_pago = substr($linha, 253, 11) . '.' . substr($linha, 264, 2);
                                                        $u_v_desconto = substr($linha, 240, 11) . '.' . substr($linha, 251, 2); //241 253
                                                    }
                                                    if (is_numeric(substr($linha, 182, 4)) && is_numeric(substr($linha, 186, 2))){
                                                        $u_v_custo = substr($linha, 182, 4) . '.' . substr($linha, 186, 2);
                                                    }
                                                } elseif (substr($linha, 79, 8) == "BRADESCO" && substr($linha, 2, 7) == "RETORNO") {   
                                                    $bradesco = true;
                                                } elseif ($bradesco) {                                                   
                                                    $t_id_titulo_banco = substr($linha, 70, 11);
                                                    $t_tipo_historico = substr($linha, 108, 2);
                                                    if (is_numeric(substr($linha, 110, 2)) && is_numeric(substr($linha, 112, 2)) && is_numeric(substr($linha, 114, 2))) {
                                                        $u_dt_ocorencia = substr($linha, 110, 2) . "/" . substr($linha, 112, 2) . "/20" . substr($linha, 114, 2);
                                                    }
                                                    if (is_numeric(substr($linha, 253, 11)) && is_numeric(substr($linha, 264, 2))) { //254 266                                                   
                                                        $u_v_pago = substr($linha, 253, 11) . '.' . substr($linha, 264, 2);
                                                        $u_v_desconto = substr($linha, 240, 11) . '.' . substr($linha, 251, 2); //241 253
                                                    }
                                                    if (is_numeric(substr($linha, 175, 11)) && is_numeric(substr($linha, 186, 2))) { //176 188
                                                        $u_v_custo = substr($linha, 175, 11) . '.' . substr($linha, 186, 2);
                                                    }                                                      
                                                } elseif (is_numeric(substr($linha, 56, 2)) && substr($linha, 56, 2) == "14") {
                                                    $t_id_titulo_banco = substr($linha, 58, 15);
                                                    $t_tipo_historico = "06";
                                                    if (is_numeric(substr($linha, 110, 2)) && is_numeric(substr($linha, 112, 2)) && is_numeric(substr($linha, 114, 2))) {
                                                        $u_dt_ocorencia = substr($linha, 110, 2) . "/" . substr($linha, 112, 2) . "/20" . substr($linha, 114, 2);
                                                    }
                                                    if (is_numeric(substr($linha, 253, 11)) && is_numeric(substr($linha, 264, 2))) { //254 266                                                   
                                                        $u_v_pago = substr($linha, 253, 11) . '.' . substr($linha, 264, 2);
                                                        $u_v_desconto = substr($linha, 240, 11) . '.' . substr($linha, 251, 2); //241 253
                                                    }
                                                    if (is_numeric(substr($linha, 182, 4)) && is_numeric(substr($linha, 186, 2))){
                                                        $u_v_custo = substr($linha, 182, 4) . '.' . substr($linha, 186, 2);
                                                    }                                                                                      
                                                } elseif (is_numeric(substr($linha, 62, 8)) && substr($linha, 0, 3) != "756") {
                                                    if (substr($linha, 62, 1) == " ") {
                                                        $t_id_titulo_banco = substr($linha, 70, 10);
                                                    } else {
                                                        $t_id_titulo_banco = substr($linha, 62, 8);   
                                                    }                                                    
                                                    $t_tipo_historico = substr($linha, 108, 2);
                                                    if (is_numeric(substr($linha, 110, 2)) && is_numeric(substr($linha, 112, 2)) && is_numeric(substr($linha, 114, 2))) {
                                                        $u_dt_ocorencia = substr($linha, 110, 2) . "/" . substr($linha, 112, 2) . "/20" . substr($linha, 114, 2);
                                                    }
                                                    if (is_numeric(substr($linha, 253, 11)) && is_numeric(substr($linha, 264, 2))) { //254 266                                                   
                                                        $u_v_pago = substr($linha, 253, 11) . '.' . substr($linha, 264, 2);
                                                        $u_v_desconto = substr($linha, 240, 11) . '.' . substr($linha, 251, 2); //241 253
                                                    }
                                                    if (is_numeric(substr($linha, 182, 4)) && is_numeric(substr($linha, 186, 2))){
                                                        $u_v_custo = substr($linha, 182, 4) . '.' . substr($linha, 186, 2);
                                                    }                                                                                                       
                                                } elseif (is_numeric(substr($linha, 58, 11)) && is_numeric(substr($linha, 93, 6)) && is_numeric(substr($linha, 105, 4))) {
                                                    $clb_cpf = substr($linha, 58, 11);
                                                    $clb_val = substr($linha, 93, 6);
                                                    $clb_data = substr($linha, 105, 4);
                                                    $clb_nome = substr($linha, 28, 30);
                                                }
                                                if (is_numeric($t_id_titulo_banco) && $t_id_titulo_banco > 0 && $t_tipo_historico != "00" && $u_dt_ocorencia != "") { //&& $u_v_pago > 0
                                                    $j++;
                                                    $pk = "";
                                                    $NomeCliente = 'Não encontrado';
                                                    $DataVencimento = 'Não encontrado';
                                                    $Parcela = '00/00';
                                                    $Valor = 0;
                                                    $Estado = 1;
                                                    $ValorPago = 0;

                                                    $query = "set dateformat dmy; select 'D-' +cast(sf_lancamento_movimento_parcelas.id_parcela as VARCHAR) as pk ,data_vencimento 'Vecto',pa ,sf_lancamento_movimento.empresa empresa,razao_social, historico_baixa, 'D' Origem , sf_contas_movimento.descricao 'cm',
                                                    valor_parcela,valor_pago,sf_grupo_contas.descricao gc,sf_lancamento_movimento_parcelas.inativo inativo, sf_lancamento_movimento_parcelas.sa_descricao documento,obs_baixa,isnull(bol_id_banco,0) bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,bol_nosso_numero,data_pagamento,data_cadastro from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento 
                                                    on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
                                                    inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
                                                    inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
                                                    left join sf_tipo_documento on sf_lancamento_movimento_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento                                                                        
                                                    inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_lancamento_movimento.status = 'Aprovado' and sf_contas_movimento.tipo = 'C' and sf_lancamento_movimento_parcelas.bol_nosso_numero = '" . $t_id_titulo_banco . "'
                                                    and (sf_lancamento_movimento_parcelas.bol_nosso_numero is null or sf_lancamento_movimento_parcelas.bol_nosso_numero not in (select bol_nosso_numero from sf_lancamento_movimento_parcelas group by bol_nosso_numero having bol_nosso_numero is not null and count(id_parcela) > 1))                                                                            
                                                    union
                                                    select 'Y-' +cast(max(sf_lancamento_movimento_parcelas.id_parcela) as VARCHAR) as pk,MAX(data_vencimento) 'Vecto',MAX(pa) pa,MAX(sf_lancamento_movimento.empresa) empresa
                                                    ,MAX(razao_social) razao_social, MAX(historico_baixa) historico_baixa, 'D' Origem , MAX(sf_contas_movimento.descricao) 'cm',
                                                    sum(valor_parcela) valor_parcela,sum(valor_pago) valor_pago,MAX(sf_grupo_contas.descricao) gc,MAX(sf_lancamento_movimento_parcelas.inativo) inativo, 
                                                    MAX(sf_lancamento_movimento_parcelas.sa_descricao) documento,
                                                    MAX(obs_baixa) obs_baixa,MAX(isnull(bol_id_banco,0)) bol_id_banco,MAX(sf_tipo_documento.abreviacao)abreviacao,
                                                    MAX(sf_tipo_documento.descricao) tdn,MAX(id_fornecedores_despesas) id_fornecedores_despesas,MAX(bol_nosso_numero) bol_nosso_numero, MAX(data_pagamento) data_pagamento, Max(data_cadastro) data_cadastro
                                                    from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento 
                                                    on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
                                                    inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
                                                    inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas                                                                        
                                                    left join sf_tipo_documento on sf_lancamento_movimento_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento                                                                        
                                                    inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_lancamento_movimento.status = 'Aprovado' and sf_contas_movimento.tipo = 'C' and sf_lancamento_movimento_parcelas.bol_nosso_numero = '" . $t_id_titulo_banco . "'
                                                    group by bol_nosso_numero having bol_nosso_numero is not null and count(id_parcela) > 1
                                                    union
                                                    select 'A-' +cast(sf_solicitacao_autorizacao_parcelas.id_parcela as VARCHAR) as pk ,data_parcela 'Vecto',pa ,sf_solicitacao_autorizacao.empresa empresa,razao_social, historico_baixa,'A' Origem, sf_contas_movimento.descricao 'cm',
                                                    valor_parcela,valor_pago,sf_grupo_contas.descricao gc,sf_solicitacao_autorizacao_parcelas.inativo inativo,sf_solicitacao_autorizacao_parcelas.sa_descricao documento,obs_baixa,isnull(bol_id_banco,0) bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,bol_nosso_numero,data_pagamento,data_cadastro from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
                                                    on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao                                                                        
                                                    inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
                                                    inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas
                                                    left join sf_tipo_documento on sf_solicitacao_autorizacao_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento                                                                                                                                                
                                                    inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'C' and status = 'Aprovado' and sf_solicitacao_autorizacao_parcelas.bol_nosso_numero = '" . $t_id_titulo_banco . "'
                                                    union    
                                                    select 'V-' +cast(sf_venda_parcelas.id_parcela as VARCHAR) as pk ,data_parcela 'Vecto',pa ,sf_vendas.empresa empresa,razao_social, sf_vendas.historico_venda historico_baixa,'V' Origem, 
                                                    'VENDAS' cm,valor_parcela,valor_pago,'VENDAS' gc,sf_venda_parcelas.inativo inativo,sf_venda_parcelas.sa_descricao documento,obs_baixa,bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,bol_nosso_numero,data_pagamento,data_cadastro   
                                                    from sf_venda_parcelas inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda left join sf_tipo_documento on sf_venda_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento                                                                                                                                                                                                    
                                                    inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas where sf_vendas.cov = 'V' and status = 'Aprovado' and sf_venda_parcelas.bol_nosso_numero = '" . $t_id_titulo_banco . "'
                                                    union    
                                                    select 'M-' +cast(id_mens as VARCHAR) as pk ,isnull(bol_data_parcela,dt_inicio_mens) 'Vecto','01/01' pa ,1 empresa,razao_social, 'PAG MENSALIDADE' historico_baixa,'M' Origem, 'MENSALIDADE' cm,bol_valor valor_parcela,isnull(valor_total,0) valor_pago,'MENSALIDADE' gc,sf_boleto.inativo inativo,'' documento,null obs_baixa,bol_id_banco,
                                                    sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,bol_nosso_numero,dt_pagamento_mens,bol_data_criacao   
                                                    from sf_boleto inner join sf_vendas_planos_mensalidade on sf_boleto.id_referencia = sf_vendas_planos_mensalidade.id_mens and tp_referencia = 'M' inner join sf_vendas_planos on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens left join sf_vendas_itens on sf_vendas_itens.id_item_venda = sf_vendas_planos_mensalidade.id_item_venda_mens 
                                                    left join sf_tipo_documento on sf_boleto.tipo_documento = sf_tipo_documento.id_tipo_documento
                                                    inner join sf_fornecedores_despesas on sf_vendas_planos.favorecido = sf_fornecedores_despesas.id_fornecedores_despesas 
                                                    where sf_boleto.inativo = 0 and mundi_id is null and carteira_id is not null and bol_nosso_numero = '" . $t_id_titulo_banco . "'";
                                                    $cur = odbc_exec($con, $query);
                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                        $pk = $pk . utf8_encode($RFP['pk']) . "|";
                                                        $NomeCliente = utf8_encode($RFP['razao_social']);
                                                        $DataVencimento = escreverData($RFP['Vecto']);
                                                        $Valor = $Valor + utf8_encode($RFP['valor_parcela']);
                                                        $Parcela = utf8_encode($RFP['pa']);
                                                        $Estado = $RFP['inativo'];
                                                        $ValorPago = $ValorPago + $RFP['valor_pago'];
                                                    }
                                                    $Valor = valoresNumericos2(escreverNumero($Valor, 0, 2));
                                                    if ($Estado == '1') {
                                                        $Estado = "inativo";
                                                    } else if ($ValorPago > 0) {
                                                        $Estado = "pago";
                                                    } else {
                                                        $Estado = "apagar";
                                                    }
                                                    $cor = "black";
                                                    if($Parcela == '00/00') {
                                                        $cor = "red";
                                                    } else if($u_v_pago == 0) {
                                                        $cor = "orange";
                                                    }                                                    
                                                    if (!($Estado == "inativo" && $bol_baixa_enc_ == 1)) { ?>   
                                                <tr class="<?php echo $Estado . ((($Valor == ($u_v_pago + $u_v_custo + $u_v_desconto)) || ($Valor == $u_v_pago) || ($Valor == ($u_v_pago + $u_v_desconto) 
                                                    && (strlen($t_id_titulo_banco) == 15 || strlen($t_id_titulo_banco) == 11)))                                                        
                                                    && ($t_tipo_historico == "06" || $t_tipo_historico == "08" || substr($linha, 0, 3) == "756") ? "" : ($Estado == "inativo" ? "" : "x")); ?>">                                                    
                                                <?php if ($u_v_pago > 0 && $Estado == "apagar" && ($t_tipo_historico == "06" || $t_tipo_historico == "08" || substr($linha, 0, 3) == "756")) { ?>
                                                    <td><center>
                                                        <input name="txtNn_<?php echo $pk; ?>" id="txtNn_<?php echo $pk; ?>" value="<?php echo $t_id_titulo_banco; ?>" type="hidden"/>
                                                        <input name="txtDt_<?php echo $pk; ?>" id="txtDt_<?php echo $pk; ?>" value="<?php echo $u_dt_ocorencia; ?>" type="hidden"/>   
                                                        <input name="txtVl_<?php echo $pk; ?>" id="txtVl_<?php echo $pk; ?>" value="<?php echo $u_v_pago; ?>" type="hidden"/>
                                                        <input name="txtCs_<?php echo $pk; ?>" id="txtCs_<?php echo $pk; ?>" value="<?php echo $u_v_custo; ?>" type="hidden"/>                                                        
                                                        <input id='check' type='checkbox' class='caixa' name='items[]' <?php echo ((($Valor == ($u_v_pago + $u_v_custo + $u_v_desconto)) || ($Valor == $u_v_pago) || ($Valor == ($u_v_pago + $u_v_desconto) && strlen($t_id_titulo_banco) == 15)) 
                                                        && ($t_tipo_historico == "06" || $t_tipo_historico == "08" || substr($linha, 0, 3) == "756") ? "checked" : ""); ?> value='<?php echo $pk; ?>'/>
                                                    </center></td>
                                                <?php } else { ?>
                                                    <td><center><div style="border: 1px solid #BEBEBE;width: 13px;height: 13px;"></div></center></td>
                                                <?php } ?>
                                                    <td style="color: <?php echo $cor;?>;"><center><b><?php echo $nome; ?></b></center></td>
                                                    <td style="color: <?php echo $cor;?>;"><center><b><?php echo $t_id_titulo_banco; ?></b></center></td>
                                                    <td style="color: <?php echo $cor;?>;"><center><span title="<?php echo $legenda[$t_tipo_historico]; ?>"><b><?php
                                                        if ($t_tipo_historico == "06" || $t_tipo_historico == "08" || substr($linha, 0, 3) == "756") {
                                                            echo "L";
                                                        } elseif ($t_tipo_historico == "09" || $t_tipo_historico == "82") {
                                                            echo "B";
                                                        } echo " (" . $t_tipo_historico . ")";
                                                        ?></b></span></center></td>                    
                                                    <td style="color: <?php echo $cor;?>;"><center><?php echo $DataVencimento; ?></center></td>
                                                    <td style="color: <?php echo $cor;?>;"><center><b><?php echo $u_dt_ocorencia; ?></b></center></td>
                                                    <td style="color: <?php echo $cor;?>;"><center><?php echo escreverNumero($Valor); ?></center></td>
                                                    <td style="color: <?php echo $cor;?>;"><center><b><?php echo escreverNumero($u_v_pago); ?></b></center></td>
                                                    <td style="color: <?php echo $cor;?>;"><center><b><?php echo escreverNumero($u_v_custo); ?></b></center></td>
                                                    <td style="color: <?php echo $cor;?>;"><center><?php echo escreverNumero($ValorPago); ?></center></td>
                                                    <td style="color: <?php echo $cor;?>;"><div id='formPQ' title='<?php echo $NomeCliente; ?>'><?php echo $NomeCliente; ?></div></td>
                                                    <td style="color: <?php echo $cor;?>;"><center><?php echo $Parcela; ?></center></td>
                                                    <td title="<?php echo $Estado; ?>"><center><img style="height: 16px;" src="../../img/<?php echo $Estado; ?>.PNG" value='Pago'></center></td>                            
                                                </tr>
                                                <?php
                                                }
                                                $t_id_titulo_banco = "";
                                                $u_v_pago = "";
                                                $u_dt_ocorencia = "";
                                            } elseif (is_numeric($clb_cpf) && is_numeric($clb_val) && $clb_val > 0 && is_numeric($clb_data)) {
                                                $j++;
                                                $clb_cpf = substr($linha, 58, 11);
                                                $clb_val = (substr($linha, 93, 6) / 100);
                                                $clb_data = substr($linha, 105, 4);
                                                $clb_nome = substr($linha, 28, 30); 
                                                $pk = "";
                                                $id_mens = "";
                                                $id_aluno = "";
                                                $dt_vencimento = "";
                                                $val_boleto = "";
                                                $val_pago = "";   
                                                $Estado = "inativo";
                                                $cor = "color: red;";                                                
                                                $query = "select id_fornecedores_despesas, id_mens, dt_inicio_mens,valor_mens, valor_total, conta_movimento, conta_produto 
                                                from sf_fornecedores_despesas left join sf_vendas_planos on sf_vendas_planos.favorecido = sf_fornecedores_despesas.id_fornecedores_despesas and dt_cancelamento is null
                                                left join sf_vendas_planos_mensalidade on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens
                                                and MONTH(dt_inicio_mens) = " . substr($clb_data, 0, 2) . " and YEAR(dt_inicio_mens) = " . "20" .substr($clb_data, 2, 2) . "
                                                left join sf_vendas_itens on sf_vendas_itens.id_item_venda = sf_vendas_planos_mensalidade.id_item_venda_mens
                                                left join sf_produtos on sf_produtos.conta_produto = sf_vendas_planos.id_prod_plano
                                                where replace(replace(cnpj, '.',''), '-','') = '" . $clb_cpf . "'";
                                                $cur = odbc_exec($con, $query);
                                                while ($RFP = odbc_fetch_array($cur)) {
                                                    $id_mens = utf8_encode($RFP['id_mens']);
                                                    $id_aluno = utf8_encode($RFP['id_fornecedores_despesas']);
                                                    $dt_vencimento = escreverData($RFP['dt_inicio_mens']);
                                                    $val_boleto = escreverNumero($RFP['valor_mens']);
                                                    $val_pago = escreverNumero($RFP['valor_total']);
                                                    if (!is_numeric($RFP['id_mens'])) {
                                                        $Estado = "inativo";
                                                    } else if ($RFP['valor_total'] > 0) {
                                                        $Estado = "pago";
                                                    } else {
                                                        if($clb_val == $RFP['valor_mens']) {
                                                            $pk = $RFP['id_mens'] . "|" . $clb_val . "|" . $id_aluno . "|" . $RFP['conta_movimento'] . "|" . $RFP['conta_produto'];
                                                        }
                                                        $Estado = "apagar";
                                                    }                                                    
                                                    $cor = "";
                                                }
                                                if (!($Estado == "inativo" && $bol_baixa_enc_ == 1)) { ?>
                                                <tr class="<?php echo $Estado . ((($Valor == ($u_v_pago + $u_v_custo + $u_v_desconto)) || 
                                                    ($Valor == $u_v_pago) || ($Valor == ($u_v_pago + $u_v_desconto) && strlen($t_id_titulo_banco) == 15)) && 
                                                    ($t_tipo_historico == "06" || $t_tipo_historico == "08" || substr($linha, 0, 3) == "756") ? "" : ($Estado == "inativo" ? "" : "x")); ?>">
                                                    <td><center><input <?php echo (strlen($pk) > 0 ? "checked" : "disabled"); ?> type='checkbox' class='caixa' name='itemsPapen[]' value='<?php echo $pk; ?>'/></center></td>
                                                    <td><center><?php echo $nome; ?></center></td>
                                                    <td><center><b><?php echo $clb_cpf; ?></b></center></td>
                                                    <td><center><?php echo $id_mens; ?></center></td>                                
                                                    <td><center><?php echo $dt_vencimento; ?></center></td>
                                                    <td><center><b><?php echo $clb_data; ?></b></center></td>
                                                    <td><center><?php echo $val_boleto; ?></center></td>
                                                    <td><center><?php echo $val_pago; ?></center></td>
                                                    <td><center><?php echo escreverNumero(0, 1); ?></center></td>
                                                    <td><center><b><?php echo escreverNumero($clb_val, 1); ?></b></center></td>
                                                    <td style="<?php echo $cor; ?>"><b><?php echo $clb_nome; ?></b></td>
                                                    <td><center><?php echo $id_aluno; ?></center></td>
                                                    <td><center><img style="height: 16px;" src="../../img/<?php echo $Estado; ?>.PNG" value='Pago'></center></td>
                                                </tr>
                                            <?php }
                                            }
                                        }
                                        fclose($lendo);
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="6"></th>
                                            <th style="text-align: center;">0,00</th>
                                            <th style="text-align: center;">0,00</th>
                                            <th style="text-align: center;">0,00</th>
                                            <th style="text-align: center;">0,00</th>
                                            <th>Total:</th>
                                            <th style="text-align: center;">0</th>                                            
                                            <th></th>                                            
                                        </tr>
                                    </tfoot>
                                </table>
                                <div style="clear:both"></div>
                                <input name="txtNome" id="txtNome" value="<?php echo $nome; ?>" type="hidden"/>
                                <strong><?php echo $msg; ?></strong>
                                <div class="widgets"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <script type="text/javascript" src="../../fancyapps/source/jquery.fancybox.js?v=2.1.4"></script>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='../../js/util.js'></script>        
        <script type="text/javascript">
            
            refreshTotal();
            
            function baixar() {
                if ($("#txtBanco").val() === "null") {
                    bootbox.alert("Selecione um Banco para baixa!");
                    return false;
                } else {
                    $("#loader").show();
                }
                return true;
            }
            
            $('#txtLayoutStatus').change(function () {
                $(".boxtext").html("Retorno de pagamentos - " + $("#txtLayoutStatus option:selected").text());
                $("#loader").show();
                setTimeout(function(){
                    refreshTotal();
                }, 500);                     
            });
            
            function refreshTotal() {
                let totRef = 0;
                let totMen = 0;
                let totCus = 0;
                let totPag = 0;
                if ($('#txtLayoutStatus').val() === "null") {
                    $("#tbRetorno tbody tr").show();
                } else {
                    $("#tbRetorno tbody tr").hide();
                    $("#tbRetorno tbody tr." + $('#txtLayoutStatus').val()).show();
                }
                $("#tbRetorno tbody tr:visible").each(function (PgIni) {
                    let selectedRow = $('#tbRetorno tbody tr:visible')[PgIni];
                    totRef = totRef + textToNumber($(selectedRow).closest("tr").find("td:eq(6)").text());
                    totMen = totMen + textToNumber($(selectedRow).closest("tr").find("td:eq(7)").text());
                    totCus = totCus + textToNumber($(selectedRow).closest("tr").find("td:eq(8)").text());
                    totPag = totPag + textToNumber($(selectedRow).closest("tr").find("td:eq(9)").text());
                });
                $("#tbRetorno tfoot th:eq(1)").text(numberFormat(totRef));
                $("#tbRetorno tfoot th:eq(2)").text(numberFormat(totMen));
                $("#tbRetorno tfoot th:eq(3)").text(numberFormat(totCus));
                $("#tbRetorno tfoot th:eq(4)").text(numberFormat(totPag));
                $("#tbRetorno tfoot th:eq(6)").text($("#tbRetorno tbody tr:visible").length);                
                $("#loader").hide();
            }
        </script>        
    </body>
<?php odbc_close($con); ?>
</html>
