<?php
include "../../Connections/configini.php";

if ($_GET['Del'] != '') {
    odbc_exec($con, "DELETE FROM sf_solicitacao_autorizacao_parcelas WHERE solicitacao_autorizacao =" . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_solicitacao_autorizacao_campos WHERE id_solicitacao_autorizacao =" . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_solicitacao_autorizacao WHERE id_solicitacao_autorizacao =" . $_GET['Del']);
    echo "<script>window.top.location.href = 'Solicitacao-de-Autorizacao.php?id=1'; </script>";
}

if (isset($_POST['btnAprov'])) {
    $items = $_POST['items'];
    $query = "0";
    for ($i = 0; $i < sizeof($items); $i++) {        
        if (is_numeric($items[$i])) {
            $query .= "," . $items[$i];
            odbc_exec($con, "update sf_solicitacao_autorizacao set destaprov = 1, data_aprov = getdate() where destinatario = '" . $_SESSION["login_usuario"] . "' and id_solicitacao_autorizacao = '" . $items[$i] . "'") or die(odbc_errormsg());
            odbc_exec($con, "update sf_solicitacao_autorizacao set destaprov2 = 1, data_aprov = getdate() where destinatario2 = '" . $_SESSION["login_usuario"] . "' and id_solicitacao_autorizacao = '" . $items[$i] . "'") or die(odbc_errormsg());
        }
    }
    odbc_exec($con, "update sf_solicitacao_autorizacao set status = 'Aprovado' where (destaprov = 1 and (destinatario2 = '' or destinatario2 is null or destaprov2 = 1)) and id_solicitacao_autorizacao in (" . $query . ")") or die(odbc_errormsg());
}

if (isset($_POST['btnCancel'])) {
    $items = $_POST['items'];
    for ($i = 0; $i < sizeof($items); $i++) {
        if (is_numeric($items[$i])) {
            odbc_exec($con, "update sf_solicitacao_autorizacao set destaprov = 2,status = 'Reprovado', data_aprov = getdate() where destinatario = '" . $_SESSION["login_usuario"] . "' and id_solicitacao_autorizacao = '" . $items[$i] . "'") or die(odbc_errormsg());
            odbc_exec($con, "update sf_solicitacao_autorizacao set destaprov2 = 2,status = 'Reprovado', data_aprov = getdate() where destinatario2 = '" . $_SESSION["login_usuario"] . "' and id_solicitacao_autorizacao = '" . $items[$i] . "'") or die(odbc_errormsg());
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>        
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Financeiro<small>Contas Variáveis</small></h1>
                    </div>
                    <form action="Solicitacao-de-Autorizacao.php" method="post" name="enviar" id="enviar">                        
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="boxfilter block">
                                    <div style="float:left;width: 40%;margin-top: 15px;">
                                        <button class="button button-green btn-primary" type="button" onClick="AbrirBox(0)" title="Novo"><span class="ico-file-4 icon-white"></span></button>
                                        <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir()" title="Imprimir"><span class="ico-print icon-white"></span></button>                                                                                
                                        <button id="btnAprov" name="btnAprov" class="button button-green btn-success" type="submit" onclick="return validar();" title="Aprovar" disabled><span class="ico-ok"></span></button>                                                                                
                                        <button id="btnCancel" name="btnCancel" class="button button-red btn-primary buttonX" type="submit" onClick="return confirm('Deseja cancelar esse registro?');" title="Reprovar" disabled><span class="ico-ban-circle"></span></button>
                                    </div>                                     
                                    <div style="float:right;width: 60%;"> 
                                        <div style="float:left;width: 22%;">
                                            <span>Solicitante</span>
                                            <select name="txtSolicitante" id="txtSolicitante" style="width:100%">
                                                <option value="null" >Selecione</option>
                                                <?php
                                                $query = "select login_user,nome from dbo.sf_usuarios where inativo = 0";
                                                $res = odbc_exec($con, $query);
                                                while ($RFC = odbc_fetch_array($res)) { ?>
                                                    <option value="<?php echo utf8_encode($RFC["login_user"]); ?>"><?php echo utf8_encode($RFC["nome"]); ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>                                        
                                        <div style="float:left;margin-left: 1%;width: 20%;">
                                            <div width="100%">Status:</div>
                                            <select style="width: 100%" name="status" id="status">
                                                <option value="null" >Selecione</option>
                                                <option value="1" selected>Aguardando</option>                                                
                                                <option value="2">Aprovadas</option>                                               
                                                <option value="3">Reprovadas</option>                                                
                                                <option value="4">Pendentes</option>                                                
                                                <option value="5">Todas</option>                                                
                                            </select>                                            
                                        </div>                                        
                                        <div style="float:left;margin-left: 1%;width: 17%;">
                                            <div width="100%">Tipo:</div>
                                            <select style="width: 100%" name="txtTipoB" id="txtTipoB" >
                                                <option value="null">Selecione</option>
                                                <option value="0">Crédito</option>
                                                <option value="1">Débito</option>
                                            </select>
                                        </div>                                        
                                        <div style="float:left;margin-left: 1%;width: 30%;">
                                            <div width="100%">Busca:</div>
                                            <input id="txtBusca" maxlength="100" type="text" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title=""/>
                                        </div>
                                        <div style="float:left;margin-left: 1%;width: 7%;margin-top: 15px;">
                                            <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="clear:both"></div>
                        <div class="boxhead">
                            <div class="boxtext">Contas Variáveis</div>
                        </div>
                        <div class="boxtable">
                            <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblLista">
                                <thead>
                                    <tr>
                                        <th width="4%" style="top: 0"><input id="checkAll" type="checkbox" class="checkall"/></th>
                                        <th width="5%"><center>Loja</center></th>
                                        <th width="7%"><center>Dt.Cad.</center></th>
                                        <th width="7%"><center>Solicitante</center></th>
                                        <th width="7%"><center>1º Venc</center></th>
                                        <th width="7%"><center>Destinatário</center></th>
                                        <th width="22%">Histórico</th>
                                        <th width="9%">Fornec./Clientes</th>
                                        <th width="2%"><center>D/C</center></th>
                                        <th width="11%">Contas Mov.</th>
                                        <th width="2%">Pa:</th>
                                        <th width="8%">Total C.I:</th>
                                        <th width="7%"><center>Status:</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <div style="clear:both"></div>
                        </div>                        
                    </form>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div> 
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script> 
        <script type="text/javascript" src="../../js/util.js"></script>                       
        <script type="text/javascript">
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }
            
            $(document).ready(function () {
                $("#btnfind").click(function () {
                    tbLista.fnReloadAjax(finalFind(0));
                });                                
            });
            
            function validar() {                
                if($.find('input[name="items[]"]:checked').length === 0){
                    bootbox.alert("Selecione uma ou mais linhas para envio!");
                    return false;                    
                }
                return true;
            }
            
            function AbrirBox(id){
                window.location = 'FormSolicitacao-de-Autorizacao.php';
            }
            
            function finalFind(imp) {
                var retPrint = (imp === 1 ? '&imp=1' : '?imp=0'); 
                if ($('#status').val() !== "") {
                    retPrint = retPrint + "&F1=" + $("#status").val();
                }
                if ($('#txtTipoB').val() !== "") {
                    retPrint = retPrint + "&F2=" + $("#txtTipoB").val();
                }
                if ($("#").val() !== "") {
                    retPrint = retPrint + '&F3=' + $("#txtSolicitante").val();
                }                
                if ($("#txtBusca").val() !== "") {
                    retPrint = retPrint + '&Search=' + $("#txtBusca").val();
                }
                retPrint = retPrint + "&filial=" + $("#txtLojaSel").val();                
                return "Solicitacao-de-Autorizacao_server_processing.php" + retPrint.replace(/\//g, "_");
            }

            function imprimir() {
                var pRel = "&NomeArq=" + "Contas Variáveis" +
                "&lbl=Loja|Dt.Cad.|Solicitante|1º Venc|Destinatário|Histórico|Fornec./Clientes|D/C|Contas Mov.|Pa:|Total C.I:|Status:" +
                "&siz=30|55|65|55|70|60|110|25|100|30|50|50" +
                "&pdf=0|13" + // Colunas do server processing que não irão aparecer no pdf
                "&filter=" + //Label que irá aparecer os parametros de filtro
                "&PathArqInclude=" + "../Modulos/Contas-a-pagar/" + finalFind(1); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }

            var tbLista = $('#tblLista').dataTable({
                "iDisplayLength": 20,
                "aLengthMenu": [20, 30, 40, 50, 100],
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "aoColumns": [
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false}],
                "sAjaxSource": finalFind(0),
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"},
                    "fnDrawCallback": function () {
                        refreshTotal();
                    },                
                "sPaginationType": "full_numbers"
            });
            
            function refreshTotal() {
                $("table :checkbox").click(function (event) {
                    if ($('input:checkbox:checked:not(".checkall")').length > 0) {
                        $("#btnAprov").attr("disabled", false);
                        $("#btnCancel").attr("disabled", false);
                    } else {
                        $("#btnAprov").attr("disabled", true);
                        $("#btnCancel").attr("disabled", true);
                    }
                });
            }
                                    
        </script>        
        <?php odbc_close($con); ?>
    </body>
</html>