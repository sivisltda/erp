<?php
include "../../Connections/configini.php";

$FinalUrl = '';
if ($_GET['idx'] != '') {
    $FinalUrl = "id=" . $_GET['idx'];
}
if ($_GET['tpx'] != '') {
    $FinalUrl = $FinalUrl . "&tp=" . $_GET['tpx'];
}
if ($_GET['tdx'] != '') {
    $FinalUrl = $FinalUrl . "&td=" . $_GET['tdx'];
}
if ($_GET['dti'] != '') {
    $FinalUrl = $FinalUrl . "&dti=" . $_GET['dti'];
}
if ($_GET['dtf'] != '') {
    $FinalUrl = $FinalUrl . "&dtf=" . $_GET['dtf'];
}
if ($_POST['txtFinalUrl']) {
    $FinalUrl = $_POST['txtFinalUrl'];
}

$id = '';
$msg = '';
$id_banco = '';

if (isset($_POST['txtVencimento'])) {
    $dataVenc = $_POST['txtVencimento'];
    $msg = '* Data Limite.';
} else {
    $dataVenc = getData("T");
}

if (isset($_POST['txtBanco'])) {
    $id_banco = $_POST['txtBanco'];
}

$ValorBaixa = '';
$Multa = 0;
$Juros = 0;
$WhereA = '';
$WhereD = '';
$WhereV = '';
$comentarios = '';
$disabled = 'disabled';
if ($_GET['id'] != '') {
    $count = explode("|", $_GET['id']);
    if (count($count) > 0) {
        if (count($count) == 2) {
            $disabled = '';
        }
        $i = 0;
        while ($i < count($count) - 1) {
            $item = explode("-", $count[$i]);
            if (count($item) == 2) {
                if ($item[0] == 'A') {
                    $WhereA = $WhereA . $item[1] . ',';
                } else if ($item[0] == 'D') {
                    $WhereD = $WhereD . $item[1] . ',';
                } else if ($item[0] == 'V') {
                    $WhereV = $WhereV . $item[1] . ',';
                }
            }
            $i++;
        }
    }
    $cur = odbc_exec($con, "select * from sf_solicitacao_autorizacao_parcelas sp
    inner join sf_solicitacao_autorizacao s on s.id_solicitacao_autorizacao = sp.solicitacao_autorizacao
    inner join sf_contas_movimento c on c.id_contas_movimento = s.conta_movimento where sp.id_parcela in (" . $WhereA . "0)") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['valor_pago'] == 0) {
            $dias = (int) floor((geraTimestamp($dataVenc) - geraTimestamp(escreverData($RFP['data_parcela']))) / (60 * 60 * 24));
            if ($dias > 0) {
                $Multa = $Multa + ($RFP['valor_parcela'] * $RFP['multa'] / 100);
                $Juros = $Juros + (($RFP['valor_parcela'] * $RFP['juros'] / 100) * $dias);
            }
            $ValorBaixa = $ValorBaixa + $RFP['valor_parcela'];
        } else {
            $id_banco = utf8_encode($RFP['id_banco']);
            $dataVenc = escreverData($RFP['data_pagamento']);
            $comentarios = utf8_encode($RFP['obs_baixa']);
            $Multa = escreverNumero($RFP['valor_multa']);
            $Juros = escreverNumero($RFP['valor_juros']);
            $ValorBaixa = $RFP['valor_pago'] - $RFP['valor_multa'] - $RFP['valor_juros'];
        }
    }
    $cur = odbc_exec($con, "select * from sf_venda_parcelas sp inner join sf_vendas s on s.id_venda = sp.venda where sp.id_parcela in (" . $WhereV . "0)") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['valor_pago'] == 0) {
            $dias = (int) floor((geraTimestamp($dataVenc) - geraTimestamp(escreverData($RFP['data_parcela']))) / (60 * 60 * 24));
            if ($dias > 0) {
                $Multa = $Multa + ($RFP['valor_parcela'] * $RFP['multa'] / 100);
                $Juros = $Juros + (($RFP['valor_parcela'] * $RFP['juros'] / 100) * $dias);
            }
            $ValorBaixa = $ValorBaixa + $RFP['valor_parcela'];
        } else {
            $id_banco = utf8_encode($RFP['id_banco']);
            $dataVenc = escreverData($RFP['data_pagamento']);
            $comentarios = utf8_encode($RFP['obs_baixa']);
            $Multa = escreverNumero($RFP['valor_multa']);
            $Juros = escreverNumero($RFP['valor_juros']);
            $ValorBaixa = $RFP['valor_pago'] - $RFP['valor_multa'] - $RFP['valor_juros'];
        }
    }

    $cur = odbc_exec($con, "select * from sf_lancamento_movimento_parcelas lp
    inner join sf_lancamento_movimento l on l.id_lancamento_movimento = lp.id_lancamento_movimento
    inner join sf_contas_movimento c on c.id_contas_movimento = l.conta_movimento where l.status = 'Aprovado' and lp.id_parcela in (" . $WhereD . "0)") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['valor_pago'] == 0) {
            $dias = (int) floor((geraTimestamp($dataVenc) - geraTimestamp(escreverData($RFP['data_vencimento']))) / (60 * 60 * 24));
            if ($dias > 0) {
                $Multa = $Multa + ($RFP['valor_parcela'] * $RFP['multa'] / 100);
                $Juros = $Juros + (($RFP['valor_parcela'] * $RFP['juros'] / 100) * $dias);
            }
            $ValorBaixa = $ValorBaixa + $RFP['valor_parcela'];
        } else {
            $id_banco = utf8_encode($RFP['id_banco']);
            $dataVenc = escreverData($RFP['data_pagamento']);
            $comentarios = utf8_encode($RFP['obs_baixa']);
            $Multa = escreverNumero($RFP['valor_multa']);
            $Juros = escreverNumero($RFP['valor_juros']);
            $ValorBaixa = $RFP['valor_pago'] - $RFP['valor_multa'] - $RFP['valor_juros'];
        }
    }
}

if (isset($_POST['bntSave'])) {
    if ($_POST['txtBanco'] == "null") {
        echo "<script>alert('Selecione um Banco, para efetuar esta operação!');</script>";
    } else {
        $dtVencimento = valoresData("txtVencimento");
        if ($_POST['txtDisabled'] == '') {
            
            $count = explode("|", $_GET['id']);
            $_POST['btnSendFileMultiple'] = "S";
            $_POST['type'] = "A_Pagar";
            $_POST['txtId'] = str_replace("-", "", $count[0]);
            include "../../Modulos/CRM/form/CRMDocumentos.php";
            
            $valorParcela = valoresNumericos2($_POST['txtValor']);
            $valorMulta = valoresNumericos2($_POST['txtMulta']);
            $valorJuros = valoresNumericos2($_POST['txtJuros']);
            if (is_numeric($valorParcela) && $dtVencimento != "null") {
                if ($valorParcela > 0) {
                    odbc_exec($con, "set dateformat dmy; update sf_solicitacao_autorizacao_parcelas set obs_baixa = '" . utf8_decode($_POST['txtComentarios']) . "', id_banco = " . $_POST['txtBanco'] . ", valor_pago = " . $valorParcela . ", data_pagamento = " . $dtVencimento . ", valor_multa = " . $valorMulta . ", valor_juros = " . $valorJuros . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela in (" . $_POST['txtWhereA'] . "0)") or die(odbc_errormsg());
                    odbc_exec($con, "set dateformat dmy; update sf_venda_parcelas set obs_baixa = '" . utf8_decode($_POST['txtComentarios']) . "', id_banco = " . $_POST['txtBanco'] . ", valor_pago = " . $valorParcela . ", data_pagamento = " . $dtVencimento . ", valor_multa = " . $valorMulta . ", valor_juros = " . $valorJuros . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela in (" . $_POST['txtWhereV'] . "0)") or die(odbc_errormsg());
                    odbc_exec($con, "set dateformat dmy; update sf_lancamento_movimento_parcelas set obs_baixa = '" . utf8_decode($_POST['txtComentarios']) . "', id_banco = " . $_POST['txtBanco'] . ", valor_pago = " . $valorParcela . ", data_pagamento = " . $dtVencimento . ", valor_multa = " . $valorMulta . ", valor_juros = " . $valorJuros . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela in (" . $_POST['txtWhereD'] . "0)") or die(odbc_errormsg());
                    if (isset($_POST['txtRecibo']) && $_POST['txtRecibo'] == "1") {
                        echo "<script>window.open('Relatorio-Recibo.php?idx=" . $_GET['id'] . "');</script>";
                    }
                    echo "<script>alert('Conta paga com sucesso!');parent.FecharBox(1);</script>";
                }
            }
        } else if ($dtVencimento != "null") {
            $cur = odbc_exec($con, "select * from sf_solicitacao_autorizacao_parcelas sp
                inner join sf_solicitacao_autorizacao s on s.id_solicitacao_autorizacao = sp.solicitacao_autorizacao
                inner join sf_contas_movimento c on c.id_contas_movimento = s.conta_movimento where sp.id_parcela in (" . $_POST['txtWhereA'] . "0)") or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                $MultaA = 0;
                $JurosA = 0;
                if ($RFP['valor_pago'] == 0) {
                    $dias = (int) floor((geraTimestamp($dataVenc) - geraTimestamp(escreverData($RFP['data_parcela']))) / (60 * 60 * 24));
                    if ($dias > 0) {
                        $MultaA = $MultaA + ($RFP['valor_parcela'] * $RFP['multa'] / 100);
                        $JurosA = $JurosA + (($RFP['valor_parcela'] * $RFP['juros'] / 100) * $dias);
                    }
                }
                odbc_exec($con, "set dateformat dmy; update sf_solicitacao_autorizacao_parcelas set obs_baixa = '" . utf8_decode($_POST['txtComentarios']) . "', id_banco = " . $_POST['txtBanco'] . ", valor_pago = " . ($RFP['valor_parcela'] + $MultaA + $JurosA) . ", data_pagamento = " . $dtVencimento . ", valor_multa = " . $MultaA . ", valor_juros = " . $JurosA . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela = " . $RFP['id_parcela']) or die(odbc_errormsg());
            }
            $cur = odbc_exec($con, "select * from sf_venda_parcelas sp inner join sf_vendas s on s.id_venda = sp.venda where sp.id_parcela in (" . $_POST['txtWhereV'] . "0)") or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                $MultaV = 0;
                $JurosV = 0;
                if ($RFP['valor_pago'] == 0) {
                    $dias = (int) floor((geraTimestamp($dataVenc) - geraTimestamp(escreverData($RFP['data_parcela']))) / (60 * 60 * 24));
                    if ($dias > 0) {
                        $MultaV = $MultaV + ($RFP['valor_parcela'] * $MultaIndice / 100);
                        $JurosV = $JurosV + (($RFP['valor_parcela'] * $JurosIndice / 100) * $dias);
                    }
                }
                odbc_exec($con, "set dateformat dmy; update sf_venda_parcelas set obs_baixa = '" . utf8_decode($_POST['txtComentarios']) . "', id_banco = " . $_POST['txtBanco'] . ", valor_pago = " . ($RFP['valor_parcela'] + $MultaV + $JurosV) . ", data_pagamento = " . $dtVencimento . ", valor_multa = " . $MultaV . ", valor_juros = " . $JurosV . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela = " . $RFP['id_parcela']) or die(odbc_errormsg());
            }
            $cur = odbc_exec($con, "select * from sf_lancamento_movimento_parcelas lp
                inner join sf_lancamento_movimento l on l.id_lancamento_movimento = lp.id_lancamento_movimento
                inner join sf_contas_movimento c on c.id_contas_movimento = l.conta_movimento where l.status = 'Aprovado' and lp.id_parcela in (" . $_POST['txtWhereD'] . "0)") or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                $MultaB = 0;
                $JurosB = 0;
                if ($RFP['valor_pago'] == 0) {
                    $dias = (int) floor((geraTimestamp($dataVenc) - geraTimestamp(escreverData($RFP['data_vencimento']))) / (60 * 60 * 24));
                    if ($dias > 0) {
                        $MultaB = $MultaB + ($RFP['valor_parcela'] * $RFP['multa'] / 100);
                        $JurosB = $JurosB + (($RFP['valor_parcela'] * $RFP['juros'] / 100) * $dias);
                    }
                }
                odbc_exec($con, "set dateformat dmy; update sf_lancamento_movimento_parcelas set obs_baixa = '" . utf8_decode($_POST['txtComentarios']) . "', id_banco = " . $_POST['txtBanco'] . ", valor_pago = " . ($RFP['valor_parcela'] + $MultaB + $JurosB) . ", data_pagamento = " . $dtVencimento . ", valor_multa = " . $MultaB . ", valor_juros = " . $JurosB . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela = " . $RFP['id_parcela']) or die(odbc_errormsg());
            }
            if (isset($_POST['txtRecibo']) && $_POST['txtRecibo'] == "1") {
                echo "<script>window.open('Relatorio-Recibo.php?idx=" . $_GET['id'] . "');</script>";
            }
            echo "<script>alert('Conta paga com sucesso!');parent.FecharBox(1);</script>";
        }
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Pagar Conta</title>
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
    </head>
    <link rel="icon" type="image/ico" href="../../favicon.ico"/>
    <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
    <link href="../../css/main.css" rel="stylesheet">
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script> 
        <body style="overflow:hidden">
            <div style="width:400px; margin:0; padding:0;">
                <form name="frmForm" action="FormContas-a-Pagar.php?id=<?php echo ($_GET["id"] != "" ? $_GET["id"] : ""); ?>" method="POST" enctype="multipart/form-data">
                    <div class="row-form">
                        <input name="txtWhereA" id="txtWhereA" value="<?php echo $WhereA; ?>" type="hidden"/>
                        <input name="txtWhereD" id="txtWhereD" value="<?php echo $WhereD; ?>" type="hidden"/>
                        <input name="txtWhereV" id="txtWhereV" value="<?php echo $WhereV; ?>" type="hidden"/>
                        <input name="txtValorLiquido" id="txtValorLiquido" value="<?php echo escreverNumero($ValorBaixa); ?>" type="hidden"/>
                        <input name="txtDisabled" id="txtDisabled" value="<?php echo $disabled; ?>" type="hidden"/>
                        <input name="txtFinalUrl" id="txtFinalUrl" value="<?php echo $FinalUrl; ?>" type="hidden"/>
                    </div>
                    <table>
                        <tr>
                            <td style="width:140px; text-align:right; padding-right:5px">Selecione o Banco:</td>
                            <td style="width:220px; padding-bottom:5px">
                                <select name="txtBanco" class="input-medium">
                                    <option value="null">--Selecione--</option>
                                    <?php $cur = odbc_exec($con, "select id_bancos,razao_social from sf_bancos where ativo = 0 order by razao_social") or die(odbc_errormsg());
                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                        <option value="<?php echo $RFP['id_bancos']; ?>" <?php
                                        if ($RFP['id_bancos'] == $id_banco) {
                                            echo "SELECTED";
                                        }
                                        ?>><?php echo utf8_encode($RFP['razao_social']) ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td><button style="border:none; float:right; width:25px; margin-right:10px; color:#FFFFFF; background-color:#333333;" onClick="parent.FecharBox(0)" id="bntOK"><b>x</b></button></td>
                        </tr>
                        <tr>
                            <td style="text-align:right; padding-right:5px">Data da Baixa:</td>
                            <td style="padding-bottom:5px">
                                <input style="width:100px" class="input-xlarge inputCenter datepicker" id="txtVencimento" name="txtVencimento" type="text" value="<?php echo $dataVenc; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:right; padding-right:5px">Valor da Multa:</td>
                            <td style="padding-bottom:5px">
                                <input onblur="somaGeral()" id="txtMulta" <?php echo $disabled; ?> style="width:100px" name="txtMulta" type="text" class="input-xlarge inputCenter" value="<?php echo escreverNumero($Multa); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:right; padding-right:5px">Valor da Juros:</td>
                            <td style="padding-bottom:5px">
                                <input onblur="somaGeral()" id="txtJuros" <?php echo $disabled; ?> style="width:100px" name="txtJuros" type="text" class="input-xlarge inputCenter" value="<?php echo escreverNumero($Juros); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:right; padding-right:5px">Valor da Parcela:</td>
                            <td style="padding-bottom:5px">
                                <input id="txtValor" <?php echo $disabled; ?> style="width:100px" name="txtValor" type="text" class="input-xlarge inputCenter" value="<?php echo escreverNumero(($ValorBaixa + $Multa + $Juros)); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:right; padding-right:5px">Observação:</td>
                            <td style="padding-bottom:5px">
                                <textarea name="txtComentarios" id="txtComentarios" style="width: 200px" cols="5" rows="3"><?php echo $comentarios; ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:right;padding-right:5px;" >Gerar Recibo:</td>
                            <td><input id="txtRecibo" <?php echo $disabled; ?> name="txtRecibo" type="checkbox" value="1"></input></td>
                        </tr>
                        <tr>
                            <td style="text-align:right;padding-right:5px; height: 28px;">Documento:</td>
                            <td colspan="2" style="padding-top:1px">
                                <input type="file" <?php echo $disabled; ?> style="font-size: 10px" name="file[]" id="file" multiple="multiple" />
                            </td>
                        </tr>                        
                    </table>
                    <div class="toolbar bottom tar" style="margin:10px 0; border-top:solid 1px #CCC; background:#F5F5F5">
                        <div class="data-fluid" style="overflow:hidden; padding:0 10px 10px">
                            <div style="float:left; text-align:left">
                                Sub Total: <b><?php echo escreverNumero($ValorBaixa, 1); ?></b><br/>
                                Total em Multa: <b><?php echo escreverNumero($Multa, 1); ?></b><br/>
                                Total em Juros: <b><?php echo escreverNumero($Juros, 1); ?></b><br/>
                                Total: <b><?php echo escreverNumero(($ValorBaixa + $Juros + $Multa), 1); ?></b>
                            </div>
                            <div class="btn-group">
                                <br/><br/><button class="btn btn-success" type="submit" name="bntSave" id="bntOK"><span class="ico-checkmark"></span>Baixar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <script type="text/javascript" src="../../fancyapps/source/jquery.fancybox.js?v=2.1.4"></script>
            <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
            <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
            <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
            <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
            <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
            <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
            <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
            <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
            <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
            <script type='text/javascript' src='../../js/plugins.js'></script>
            <script type='text/javascript' src='../../js/charts.js'></script>
            <script type='text/javascript' src='../../js/actions.js'></script>
            <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
            <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
            <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
            <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>        
            <script type='text/javascript' src='../../js/util.js'></script>        
            <script language="JavaScript">

                $("#txtVencimento").mask(lang["dateMask"]);
                $("#txtMulta, #txtJuros, #txtValor").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});

                function somaGeral() {
                    var inputs = 0;
                    inputs = inputs + textToNumber($("#txtValorLiquido").val());
                    inputs = inputs + textToNumber($("#txtMulta").val());
                    inputs = inputs + textToNumber($("#txtJuros").val());
                    $("#txtValor").val(numberFormat(inputs));
                    return;
                }
            </script>        
        </body>
        <?php odbc_close($con); ?>
</html>
