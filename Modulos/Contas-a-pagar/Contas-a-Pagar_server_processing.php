<?php

if (!isset($_GET['pdf'])) {
    require_once(__DIR__ . '/../../Connections/configini.php');
}

$aColumns = array('documento', 'razao_social', 'historico_baixa', 'gc', 'cm');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhere = "";
$sendURL = "";
$idFilial = "";
$tipoData = "0";
$sLimit = 20;
$imprimir = 0;
$grupoArray = [];

if ($_GET['id'] == '5') {
    $sOrder = " ORDER BY razao_social asc ";
} else {
    if ($_GET['ord'] == '7') {
        $sOrder = " ORDER BY razao_social asc ";
    } else {
        if ($_GET['id'] == '2') {
            $sOrder = " ORDER BY Vecto desc ";
        } else {
            $sOrder = " ORDER BY Vecto asc ";
        }
    }
}
if ($_GET['id'] != '') {
    $sendURL = $sendURL . "&idx=" . $_GET['id'];
}
if ($_GET['tp'] != '') {
    $sendURL = $sendURL . "&tpx=" . $_GET['tp'];
}
if ($_GET['td'] != '') {
    $sendURL = $sendURL . "&tdx=" . $_GET['td'];
}
if (is_numeric($_GET['tpd'])) {
    $sendURL = $sendURL . "&tpd=" . $_GET['tpd'];
}
if ($_GET['dti'] != '') {
    $sendURL = $sendURL . "&dti=" . $_GET['dti'];
}
if ($_GET['dtf'] != '') {
    $sendURL = $sendURL . "&dtf=" . $_GET['dtf'];
}
if ($_GET['filial'] != "") {
    $sendURL = $sendURL . "&filial=" . $_GET['filial'];
}
if ($_GET['imp'] != "") {
    $imprimir = $_GET['imp'];
}
if ($_GET['total'] != "") {
    $iTotal = $_GET['total'];
}
if ($_GET['id'] != "") {
    $F1 = $_GET['id'];
}
if ($_GET['tp'] != "") {
    $F2 = $_GET['tp'];
}
if ($_GET['td'] != "") {
    $F3 = $_GET['td'];
}
if ($_GET['grp'] != "") {
    $F3 = $_GET['grp'];
}
if (is_numeric($_GET['tpd'])) {
    $tipoData = $_GET['tpd'];
}
if ($_GET['dti'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
}
if ($_GET['dtf'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
}
if ($_GET['filial'] != "") {
    $idFilial = $_GET['filial'];
}
if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}
$PrintOrder = 0;
if (isset($_GET['iSortCol_0'])) {
    if ($_GET['id'] == '5') {
        $sOrder = " ORDER BY razao_social asc ";
    } else {
        if ($_GET['ord'] == '7') {
            $sOrder = " ORDER BY razao_social asc ";
        } else {
            if ($_GET['id'] == '2') {
                $sOrder = " ORDER BY Vecto desc, razao_social ";
            } else {
                $sOrder = " ORDER BY Vecto asc, razao_social ";
            }
        }
    }
}

if ($_GET['sSearch'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['sSearch']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

$comando = '';
$comandoX1 = '';
$comandoX2 = '';
$comandoX3 = '';

if ($F1 == "1") {
    $comando = " AND valor_pago = 0 ";
} elseif ($F1 == "2") {
    $comando = " AND valor_pago > 0 ";
}

if ($F2 == "0") {
    if ($F3 != "") {
        $comando = $comando . " AND sf_fornecedores_despesas.id_fornecedores_despesas = " . $F3;
    }
} elseif ($F2 == "1") {
    if ($F3 != "") {
        $comando = $comando . " AND sf_grupo_contas.id_grupo_contas = " . $F3;
    }
} elseif ($F2 == "2") {
    if ($F3 != "") {
        $comando = $comando . " AND sf_contas_movimento.id_contas_movimento = " . $F3;
    }
} elseif ($F2 == "3") {
    if ($F3 != "") {
        $comando .= " AND sf_tipo_documento.id_tipo_documento in (" . str_replace("\"", "'", str_replace(array("[", "]"), "", $F3)) . ")";
    }   
} elseif ($F2 == "6") {
    if ($F3 != "") {
        $comando = $comando . " AND sf_grupo_contas.centro_custo = " . $F3;
    }    
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $campo = "data_parcela";
    if ($tipoData == 1) {
        $campo = "data_pagamento";
    } elseif ($tipoData == 2) {
        $campo = "data_cadastro";
    }
    $comandoX1 = $comandoX1 . " AND " . ($tipoData == 0 ? "data_vencimento" : $campo) . " >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND " . ($tipoData == 0 ? "data_vencimento" : $campo) . " <= " . valoresDataHora2($DateEnd, "23:59:59");
    $comandoX2 = $comandoX2 . " AND " . $campo . " >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND " . $campo . " <= " . valoresDataHora2($DateEnd, "23:59:59");
    $comandoX3 = $comandoX3 . " AND " . $campo . " >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND " . $campo . " <= " . valoresDataHora2($DateEnd, "23:59:59");
}

if ($F1 == "3") {
    $comandoX1 = $comandoX1 . " AND sf_lancamento_movimento_parcelas.inativo = '1' ";
    $comandoX2 = $comandoX2 . " AND sf_solicitacao_autorizacao_parcelas.inativo = '1' ";
    $comandoX3 = $comandoX3 . " AND sf_venda_parcelas.inativo = '1' ";
} else {
    $comandoX1 = $comandoX1 . " AND sf_lancamento_movimento_parcelas.inativo = '0' ";
    $comandoX2 = $comandoX2 . " AND sf_solicitacao_autorizacao_parcelas.inativo = '0' ";
    $comandoX3 = $comandoX3 . " AND sf_venda_parcelas.inativo = '0' ";
}
if (is_numeric($idFilial)) {
    $comandoX1 = $comandoX1 . " AND sf_lancamento_movimento.empresa = " . $idFilial;
    $comandoX2 = $comandoX2 . " AND sf_solicitacao_autorizacao.empresa = " . $idFilial;
    $comandoX3 = $comandoX3 . " AND sf_vendas.empresa = " . $idFilial;
}

$sQuery1 = "set dateformat dmy; select *, ROW_NUMBER() OVER (" . $sOrder . ") as row from (select 'D-' +cast(sf_lancamento_movimento_parcelas.id_parcela as VARCHAR) as pk ,data_vencimento 'Vecto',pa ,sf_lancamento_movimento.empresa empresa,razao_social, historico_baixa,'' comentarios, 'D' Origem , sf_contas_movimento.descricao 'cm',
valor_parcela,valor_pago,sf_grupo_contas.descricao gc,sf_lancamento_movimento_parcelas.inativo inativo, sf_lancamento_movimento_parcelas.sa_descricao documento,obs_baixa,bol_id_banco,sf_tipo_documento.abreviacao,id_fornecedores_despesas,data_pagamento,data_cadastro,syslogin, sf_centro_custo.descricao centro_custo 
from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento 
on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
left join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas
left join sf_centro_custo on sf_grupo_contas.centro_custo = sf_centro_custo.id
left join sf_tipo_documento on sf_lancamento_movimento_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento                                                                                                                            
inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'D' and sf_lancamento_movimento.status = 'Aprovado' " . $comando . $comandoX1 . "
union
select 'A-' +cast(sf_solicitacao_autorizacao_parcelas.id_parcela as VARCHAR) as pk ,data_parcela 'Vecto',pa ,sf_solicitacao_autorizacao.empresa empresa,razao_social, historico_baixa,cast(comentarios as varchar),'A' Origem, sf_contas_movimento.descricao 'cm',
valor_parcela,valor_pago,sf_grupo_contas.descricao gc,sf_solicitacao_autorizacao_parcelas.inativo inativo,sf_solicitacao_autorizacao_parcelas.sa_descricao documento,obs_baixa,bol_id_banco,sf_tipo_documento.abreviacao,
id_fornecedores_despesas,data_pagamento,data_cadastro,syslogin, sf_centro_custo.descricao centro_custo
from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
left join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas        
left join sf_centro_custo on sf_grupo_contas.centro_custo = sf_centro_custo.id
left join sf_tipo_documento on sf_solicitacao_autorizacao_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento                                                                                                                                                                                                    
inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'D' and status = 'Aprovado' " . $comando . $comandoX2 . "
union    
select 'V-' +cast(sf_venda_parcelas.id_parcela as VARCHAR) as pk ,data_parcela 'Vecto',pa ,sf_vendas.empresa empresa,razao_social, historico_venda,cast(comentarios_venda as varchar), 'V' Origem, 
sf_contas_movimento.descricao cm,valor_parcela,valor_pago,sf_grupo_contas.descricao gc,sf_venda_parcelas.inativo inativo,sf_venda_parcelas.sa_descricao documento,
obs_baixa,bol_id_banco,sf_tipo_documento.abreviacao,id_fornecedores_despesas,data_pagamento,data_cadastro,syslogin collate SQL_Latin1_General_CP1_CI_AS, sf_centro_custo.descricao centro_custo   
from sf_venda_parcelas inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda 
inner join sf_contas_movimento on sf_vendas.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas 
left join sf_centro_custo on sf_grupo_contas.centro_custo = sf_centro_custo.id
left join sf_tipo_documento on sf_venda_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento                                                                                                                                                                                                    
inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas where sf_vendas.cov = 'C' and status = 'Aprovado' " . $comando . $comandoX3 . ") as x where pk is not null " . $sWhere;

//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$totparc = 0;
$totparcpg = 0;

while ($RFP = odbc_fetch_array($cur)) {
    $iFilteredTotal = $iFilteredTotal + 1;
    if ($RFP['row'] > $sLimit && $RFP['row'] <= ($sLimit + $sQtd) || $imprimir == 1) {
        $backColor = '';
        if ($RFP['valor_pago'] == 0) {
            if (escreverData($RFP['Vecto'], "Y-m-d") < date("Y-m-d")) {
                if ($F1 != "5" || $imprimir == 0) {
                    $backColor = " style='color:red' ";
                }
            }
        }
        $row = array();
        if ($imprimir == 0) {
            if ($iFilteredTotal == 1) {
                $ordem = "<input type='hidden' name='ordem' id='ordem' value='" . $PrintOrder . "'/>";
            } else {
                $ordem = "";
            }
            $row[] = "<center>" . $ordem . "<input id='check' type='checkbox' onClick=\"
            var total = contaCheckbox();
            var total2 = mostraCheckbox();
            if (total === 1){ 
                $('#bntBaixaLoad').attr('onclick','AbrirBox(1,0)');                
                if(document.getElementById('bntRenegociar') != undefined && " . $RFP['valor_pago'] . " == 0){
                    document.getElementById('bntRenegociar').disabled = false;
                }
                if(document.getElementById('bntInativo') != undefined ){
                    document.getElementById('bntInativo').disabled = false;   
                }
                if(document.getElementById('bntDesfazer') != undefined ){
                    document.getElementById('bntDesfazer').disabled = false;   
                }
                if(document.getElementById('btnDocs') != undefined ){
                    document.getElementById('btnDocs').disabled = false;   
                }
                if(document.getElementById('bntBaixaLoad') != undefined ){
                    document.getElementById('bntBaixaLoad').disabled = false; 
                }
                if(document.getElementById('bntExcluir') != undefined ){
                    document.getElementById('bntExcluir').disabled = false; 
                }           
            }else{                                                                            
                if (total > 1){
                    $('#bntBaixaLoad').attr('onclick','AbrirBox(1,0)');
                    if(document.getElementById('bntRenegociar') != undefined){
                        document.getElementById('bntRenegociar').disabled = true;    
                    }
                    if(document.getElementById('bntInativo') != undefined ){
                        document.getElementById('bntInativo').disabled = false;    
                    }
                    if(document.getElementById('bntDesfazer') != undefined ){
                        document.getElementById('bntDesfazer').disabled = false;    
                    }
                    if(document.getElementById('btnDocs') != undefined ){
                        document.getElementById('btnDocs').disabled = false;    
                    }
                    if(document.getElementById('bntBaixaLoad') != undefined ){
                        document.getElementById('bntBaixaLoad').disabled = false; 
                    }
                    if(document.getElementById('bntExcluir') != undefined ){
                        document.getElementById('bntExcluir').disabled = false; 
                    }
                }else{
                    $('#lblTotParc2').html('Total selecionado :<strong>' + numberFormat(0, 1) + '</strong>').show('slow');
                    if(document.getElementById('bntBaixaLoad') != undefined ){
                        document.getElementById('bntBaixaLoad').disabled = true;                                                                        
                    }
                    if(document.getElementById('bntExcluir') != undefined ){
                        document.getElementById('bntExcluir').disabled = true;                                                                        
                    }
                    if(document.getElementById('bntRenegociar') != undefined){
                        document.getElementById('bntRenegociar').disabled = true;
                    }
                    if(document.getElementById('bntInativo') != undefined ){
                        document.getElementById('bntInativo').disabled = true;
                    }
                    if(document.getElementById('bntDesfazer') != undefined ){
                        document.getElementById('bntDesfazer').disabled = true;
                    }
                    if(document.getElementById('btnDocs') != undefined ){
                        document.getElementById('btnDocs').disabled = true;
                    }
                }
            }\" class='caixa' name='items[]' value='" . utf8_encode($RFP['pk']) . "'/><input name='" . utf8_encode($RFP['pk']) . "' id='" . utf8_encode($RFP['pk']) . "' value='" . $RFP['valor_parcela'] . "' type='hidden'/></center>";
        }
        $row[] = "<center><div id='formPQ' " . $backColor . ">" . escreverData($RFP['data_cadastro']) . "</div></center>";
        if ($F1 != "5") {
            $row[] = "<center><div id='formPQ' title='" . utf8_encode($RFP['tdn']) . "' " . $backColor . ">" . utf8_encode($RFP['abreviacao']) . "</div></center>";
        }
        $row[] = "<center><div id='formPQ' " . $backColor . ">" . utf8_encode($RFP['documento']) . "</div></center>";
        $row[] = "<center><div id='formPQ' " . $backColor . ">" . escreverData($RFP['Vecto']) . "</div></center>";
        $row[] = "<center><div id='formPQ' " . $backColor . ">" . escreverData($RFP['data_pagamento']) . "</div></center>";
        $row[] = "<center><div id='formPQ' " . $backColor . ">" . utf8_encode($RFP['pa']) . "</div></center>";
        $row[] = "<div id='formPQ' title='" . utf8_encode($RFP['razao_social']) . "' " . $backColor . "><a href='#' onClick='AbrirBox(2," . $RFP['id_fornecedores_despesas'] . ")'>" . utf8_encode($RFP['razao_social']) . "</a></div><input name='" . utf8_encode($RFP['pk']) . "' id='" . utf8_encode($RFP['pk']) . "' value='" . $RFP['valor_parcela'] . "' type='hidden'/>";
        if ($F1 != "5") {
            $row[] = "<div id='formPQ' " . $backColor . " title='" . utf8_encode($RFP['historico_baixa']) . "\n" . utf8_encode($RFP['comentarios']) . "'>" . utf8_encode($RFP['historico_baixa']) . "</div>";
            if ($RFP['Origem'] == 'A') {
                $tipo = 'SOLICITAÇÃO DE AUTORIZAÇÃO';
            } elseif ($RFP['Origem'] == 'D') {
                $tipo = 'CONTAS FIXAS';
            } elseif ($RFP['Origem'] == 'V') {
                $tipo = 'VENDAS';
            } else {
                $tipo = '-';
            }
            if ($RFP['Origem'] == 'A') {
                $tipo2 = 'SA';
            } elseif ($RFP['Origem'] == 'D') {
                $tipo2 = 'CF';
            } elseif ($RFP['Origem'] == 'V') {
                $tipo2 = 'VD';
            } else {
                $tipo2 = '-';
            }
            $row[] = "<center><div id='formPQ' " . $backColor . " title='" . $tipo . "'>" . $tipo2 . "</div></center>";     
            $row[] = "<div id='formPQ' " . $backColor . " title='" . utf8_encode($RFP["gc"]) . "'>" . utf8_encode($RFP["gc"]) . "</div>";
            $row[] = "<div id='formPQ' " . $backColor . " title='" . utf8_encode($RFP['cm']) . "'>" . utf8_encode($RFP['cm']) . "</div>";
        }
        if (isset($_GET['pdf'])) {                
            $grp = array(utf8_encode($RFP['gc']), ($RFP['gc'] == "" ? "SEM GRUPO" : utf8_encode($RFP['gc'])), 9);
            if (!in_array($grp, $grupoArray)) {
                $grupoArray[] = $grp;
            }
        }          
        if ($backColor == "") {
            $aling = "style='text-align:right'";
        } else {
            $aling = "style='color:red; text-align:right'";
        }
        $row[] = "<div id='formPQ' " . $aling . ">" . escreverNumero($RFP['valor_parcela']) . "</div>";
        if ($F1 != "5") {
            $TotalPago = $TotalPago + $RFP['valor_pago'];
            $row[] = "<div id='formPQ' " . $aling . ">" . escreverNumero($RFP['valor_pago']) . "</div>";
        }
        if ($imprimir == 0) {
            $detalhe = "";
            if (($RFP['valor_pago'] > 0) and ($RFP['inativo'] != '1')) {
                $detalhe = "<li><a href=\"javascript:void(0)\" onClick=\"AbrirBox(3,'" . $RFP["pk"] . "')\")\"><input type='image' src=\"../../img/MS_Office_Upload_Center.png\" width='16' height='16'> Anexo de Arquivo</a></li>";
            }
            if (($RFP['valor_pago'] > 0) and ($RFP['inativo'] != '1')) {
                $detalhe .= "<li class=\"divider\"></li><li><a href=\"Relatorio-Recibo.php?idx=" . utf8_encode($RFP['pk']) . "\" target='_blank'><input type='image' src=\"../../img/1365123843_onebit_323 copy.PNG\" width='16' height='16'> Recibo</a></li>";
            }
            if ($detalhe != "") {
                $row[] = "<center><div style=\"text-align:left;\" class=\"btn-group\">
                        <button style=\"height: 13px;background-color: gray;\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\"><span style=\"margin-top: 1px;\" class=\"caret\"></span></button>
                        <ul style=\"left:-130px;\" class=\"dropdown-menu\">" . $detalhe . "</ul>
                     </div></center>";
            } else {
                $row[] = "";
            }
            //imagem
            if ($RFP['inativo'] == '1') {
                $row[] = "<center><img src='../../img/inativo.PNG' title='" . utf8_encode($RFP['obs_baixa']) . "'></center>";
            } else {
                $data2 = ($RFP['data_cadastro'] != "" ? "Data de Criação :" . escreverData($RFP['data_cadastro']) : "");
                if ($RFP['valor_pago'] > 0) {
                    $data2 .= "\nUsuário Baixa: " . utf8_encode($RFP['syslogin']);
                    $row[] = "<center><img src='../../img/pago.PNG' title='" . $data2 . "" . utf8_encode($RFP['obs_baixa']) . "'></center>";
                } else {
                    $row[] = "<center><img src='../../img/apagar.PNG' title='" . $data2 . "'></center>";
                }
            }
        } else {
            $row[] = "NÂO";                
        }
        if (isset($_GET['tpImp']) && $_GET['tpImp'] == "E") {
            $row[] = utf8_encode($RFP['centro_custo']);
        }
        $output['aaData'][] = $row;
    }
    $totparc = $totparc + $RFP['valor_parcela'];
    $totparcpg = $totparcpg + $RFP['valor_pago'];
}
if ($iFilteredTotal > 0) {
    $row = $output['aaData'][0];
    $row[1] = "<input type='hidden' name='ordem' id='ordem' value='" . $PrintOrder . "'/>" .
            "<input type='hidden' name='totparc' id='totparc' value='" . $totparc . "'/>" .
            "<input type='hidden' name='totparcpg' id='totparcpg' value='" . $totparcpg . "'/>" . $row[1];
    $output['aaData'][0] = $row;
}
$output['iTotalDisplayRecords'] = $iFilteredTotal;
$output['iTotalRecords'] = $iFilteredTotal;
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
} else {
    $output['aaTotal'][] = array(11, 11, "Sub Total", "Total", 0, 0);    
    $output['aaTotal'][] = array(12, 12, "", "", 0, 0);
    $output['aaGrupo'] = $grupoArray;
}
odbc_close($con);
