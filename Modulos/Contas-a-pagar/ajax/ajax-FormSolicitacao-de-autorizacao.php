<?php

require_once('../../../Connections/configini.php');
require_once('../../../util/util.php');
$id = $_GET['id'];
$listParcelas = array();
$TotalParcelas = 0;
if (is_numeric($id)) {
    $sql = "select * from sf_solicitacao_autorizacao_parcelas where inativo = 0 and solicitacao_autorizacao = " . $id;
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $listParcelas[] = array(
            "id_parcela" => tableFormato($RFP['id_parcela'], 'T', '', ''),
            "sa_descricao" => tableFormato($RFP['sa_descricao'], 'T', '', ''),
            "data_parcela" => escreverData($RFP['data_parcela']),
            "valor_parcela" => escreverNumero($RFP['valor_parcela']),
            "Valor_pago" => escreverNumero($RFP['valor_pago']),
            "tipo_documento" => tableFormato($RFP['tipo_documento'], 'T', '', '')
        );
    }
}

echo(json_encode($listParcelas));
odbc_close($con);
