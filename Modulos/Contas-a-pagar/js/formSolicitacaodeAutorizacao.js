
$("#txtData, #txtParcelaMakeD").mask(lang["dateMask"]);
$("#txtParcelaMakeDC").mask("9999999999", {placeholder: ""});
$("#txtParcelaMakeT, #txtParcelaMakeV").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});

refreshMask();

$(function () {
    $('#txtTipo').change(function () {
        if ($(this).val()) {
            $('#tblTable').hide();
            $.getJSON('campos.ajax.php?search=', {txtCampos: $(this).val(), ajax: 'true'}, function (j) {
                var options = '<thead><tr><th width="29%">Descrição:</th><th width="71%">Conteúdo:</th></tr></thead><tbody>';
                for (var i = 0; i < j.length; i++) {
                    options += '<tr><td>' + j[i].campo + '</td><td><input name="txtCampo' + j[i].id_tipo_documento_campos + '" id="txtCampo' + j[i].id_tipo_documento_campos + '" type="text" class="input-xlarge" value=""/></td></tr>';
                }
                options += '</tbody>';
                $('#tblTable').html(options).show("slow");
            });
        } else {
            $('#txtTipo').html('<thead><tr><th width="29%">Descrição:</th><th width="71%">Conteúdo:</th></tr></thead><tbody><tr><td></tr></td></tbody>');
        }
        gerarBoleto();
    });
    $('#txtTipoGrupo').change(function () {
        zeraTxtGrupo();
        zeraTxtConta();
        zeraTxtTipo();
        gerarBoleto();        
        if ($(this).val()) {
            $('#txtGrupo').hide();
            $('#txtConta').html('<option value="" >--Selecione--</option>');
            $.getJSON('grupo.ajax.php?search=', {txtTipo: $(this).val(), ajax: 'true'}, function (j) {
                var options = '<option value="" >--Selecione--</option>';
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].id_grupo_contas + '">' + j[i].descricao + '</option>';
                }
                $('#txtGrupo').html(options).show();
                $('#txtGrupo').css("display", "none");
            });
            $.getJSON('banco.ajax.php?search=', {txtTipo: $(this).val(), ajax: 'true'}, function (j) {
                var options = '<option value="null" >--Selecione--</option>';
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].id_grupo_contas + '">' + j[i].descricao + '</option>';
                }
                $('#txtBanco').html(options).show();
                $('#txtBanco').css("display", "none");
            });
            $.getJSON('tipo.ajax.php?search=', {txtTipo: $(this).val(), ajax: 'true'}, function (j) {
                var options = '<option value="" >--Selecione--</option>';
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].id_grupo_contas + '">' + j[i].descricao + '</option>';
                }
                $('#txtTipo').html(options).show();
                $('#txtTipo').css("display", "none");
                for (var j = 0; j < 60; j++) {
                    $("#txtTipo_" + (j + 1)).html(options).show();
                }
            });
        } else {
            $('#txtGrupo').html('<option value="" >--Selecione--</option>');
            $('#txtConta').html('<option value="" >--Selecione--</option>');
            $('#txtBanco').html('<option value="null" >--Selecione--</option>');
            $('#txtTipo').html('<option value="" >--Selecione--</option>');
            for (var j = 0; j < 60; j++) {
                $("#txtTipo_" + (j + 1)).html('<option value="">--Selecione--</option>');
            }
            $('#txtGrupo').css("display", "none");
            $('#txtConta').css("display", "none");
            $('#txtBanco').css("display", "none");
            $('#txtTipo').css("display", "none");
        }
    });
    $('#txtGrupo').change(function () {
        if ($(this).val()) {
            $('#txtConta').hide();
            zeraTxtConta();
            $.getJSON('conta.ajax.php?search=', {txtGrupo: $(this).val(), ajax: 'true'}, function (j) {
                var options = '<option value="">--Selecione--</option>';
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].id_contas_movimento + '">' + j[i].descricao + '</option>';
                }
                $('#txtConta').html(options).show();
                $('#txtConta').css("display", "none");
            });
        } else {
            $('#txtConta').html('<option value="">--Selecione--</option>');
            $('#txtConta').css("display", "none");
        }
    });
    $('#txtBanco').change(function () {
        if ($(this).val()) {
            $('#txtBancoCarteira').hide();
            $.getJSON('./../Financeiro/bancoCarteira.ajax.php?search=', {txtBanco: $(this).val(), ajax: 'true'}, function (j) {
                options = '';
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].sf_carteiras_id + '">' + j[i].sf_carteira_descricao + '</option>';
                }
                $('#txtBancoCarteira').html(options).show();
                $('#txtBancoCarteira').css("display", "none");
            });
        } else {
            $('#txtBancoCarteira').html('<option value="null">--Selecione--</option>');
            $('#txtBancoCarteira').css("display", "none");
        }
    });
    $("#txtFonecedorNome").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "../../Modulos/CRM/ajax.php",
                dataType: "json",
                data: {q: request.term, r: "S", t: "C','F','E','T"},
                success: function (data) {
                    response(data);
                }
            });
        }, minLength: 3,
        select: function (event, ui) {
            $("#txtFonecedor").val(ui.item.id);
            $("#txtFonecedorNome").val(ui.item.value);
        }
    });
    carregaParcelas();
    gerarBoleto();
});

function gerarBoleto() {
    if ($("#txtTipoGrupo").val() === 'C') {
        $(".bancos").show();
    } else {
        $(".bancos").hide();
    }    
    if ($("#txtTipoGrupo").val() === "C" && $("#txtTipo").val() === "10") {
        $("#txtBanco, #txtBancoCarteira").select2('enable');
    } else {        
        $("#txtBanco, #txtBancoCarteira").val(null).trigger('change');        
        $("#txtBanco, #txtBancoCarteira").select2('disable');
    }
}

function refreshMask() {
    $(".mdate").mask(lang["dateMask"]);
    $(".parce").mask("9999999999", {placeholder: ""});
    $(".price").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
}

function preencheZeros(valor, valor2, tamanho) {
    var qtd = parseInt(valor) + parseInt(valor2);
    if (qtd.toString().length < tamanho.length) {
        var limite = tamanho.length - qtd.toString().length;
        for (i = 0; i < limite; i++) {
            qtd = '0' + qtd;
        }
    }
    return qtd;
}

function calculaParcela() {
    if ($("#txtParcelaMakeT").val() !== '') {
        var qtde = parseInt($("#spinner").val());
        var valTotal = textToNumber($("#txtParcelaMakeT").val());
        if (qtde > 0 && valTotal > 0) {
            $("#txtParcelaMakeV").val(numberFormat(valTotal / qtde));
        } else {
            $("#txtParcelaMakeV").val("");
        }
    }
}

function Preencher() {
    var error = "";
    if ($("#txtTipo").val() === "") {
        error = "Selecione o Tipo e o Tipo Doc.";
    } else if ($("#txtBanco").val() !== "null" && $("#txtParcelaMakeDC").val() === "") {
        error = "Digite o Doc. inicial.";
    } else if ($("#txtParcelaMakeD").val() === "") {
        error = "Selecione a data de Vencimento.";
    } else if ($("#spinner").val() < 1) {
        error = "A quantidade de parcelas tem que ser maior do que zero.";
    } else if ((textToNumber($("#txtParcelaMakeT").val()) > 0) === false) {
        error = "O valor total tem que ser maior que zero.";
    }
    if (error === "") {
        var val = textToNumber($("#txtParcelaMakeV").val());
        var totDescParc = 0;
        var idLinha = 1;
        var qtde = parseInt($("#spinner").val());
        var valorx = textToNumber($("#txtParcelaMakeDC").val());

        if (qtde >= 0 && val >= 0 && moment($("#txtParcelaMakeD").val(), lang["dmy"], true).isValid()) {
            for (var i = $('#tbParcelas >tbody >tr').length; i > qtde; i--) {
                if ($("#txtPG_" + (i)).val() !== 'S') {
                    $("#linhaParcela_" + i).remove();
                } else {
                    bootbox.alert("Número de parcelas inválido!");
                    return false;
                }
            }
            for (var i = 0; i < qtde; i++) {
                var aDate = moment($("#txtParcelaMakeD").val(), lang["dmy"], true);
                var dataParcela = aDate.add(i, 'M').format(lang["dmy"]);
                var docParcela = "";
                if ($("#txtParcelaMakeDC").val() !== '') {
                    if ($("#txtFixo").is(':checked')) {
                        docParcela = $("#txtParcelaMakeDC").val();
                    } else {
                        docParcela = preencheZeros(valorx, i, $("#txtParcelaMakeDC").val());
                    }
                }
                if ($("#txtPG_" + (i + 1)).val() !== undefined) {
                    if ($("#txtPG_" + (i + 1)).val() !== 'S') {
                        $("#txtParcelaDC_" + (i + 1)).val(docParcela);
                        $("#txtParcelaD_" + (i + 1)).val(dataParcela);
                        $("#txtParcelaV_" + (i + 1)).val($("#txtParcelaMakeV").val());
                        $("#txtTipo_" + (i + 1)).val($("#txtTipo").val());
                    } else {
                        idLinha = idLinha + 1;
                        totDescParc += textToNumber($("#txtParcelaV_" + (i + 1)).val());
                    }
                } else {
                    addLinhaParcela(i + 1, "", "", "", "N", docParcela, dataParcela, $("#txtParcelaMakeV").val());
                    $("#txtTipo_" + (i + 1)).val($("#txtTipo").val());
                }
            }
            recalculaParc(textToNumber($("#txtParcelaMakeT").val()), totDescParc, idLinha);
        }
    } else {
        bootbox.alert(error);
    }
}

function changeParc(obj) {
    var totAntes = 0;
    for (var i = 1; i <= obj; i++) {
        totAntes = totAntes + textToNumber($("#txtParcelaV_" + (i)).val());
    }
    recalculaParc(textToNumber($("#txtParcelaMakeT").val()), totAntes, obj + 1);
}

function addLinhaParcela(nParc, backcolor, disabled, id_parcela, pago, sa_descricao, data_parcela, valor_parcela) {
    var tbody = "";
    tbody += "<tr id='linhaParcela_" + nParc + "'><td valign='middle'>" +
            "<center><input name='txtID_" + nParc + "' id='txtID_" + nParc + "' value='" + id_parcela + "' type='hidden'/>" +
            "<input name='txtPG_" + nParc + "' id='txtPG_" + nParc + "' value='" + pago + "' type='hidden'/>" +
            "<span style='margin-top:8px; display:block;'>" + nParc + "</span></center></td>";
    tbody += "<td><input style='text-align: center;background-color:" + backcolor + "' class='input-xxlarge inputDireito parce' name='txtParcelaDC_" + nParc + "' id='txtParcelaDC_" + nParc + "' " + disabled + " type='text' value='" + sa_descricao + "' /></td>";
    tbody += "<td><input style='text-align: center;background-color:" + backcolor + "' class='input-xxlarge inputDireito mdate' name='txtParcelaD_" + nParc + "' id='txtParcelaD_" + nParc + "' " + disabled + " type='text' value='" + data_parcela + "' /></td>";
    tbody += "<td><input style='text-align: center;background-color:" + backcolor + "' class='input-xxlarge inputDireito price' name='txtParcelaV_" + nParc + "' id='txtParcelaV_" + nParc + "' " + disabled + " onBlur='changeParc(" + nParc + ")' type='text' value='" + valor_parcela + "'/></td>";
    tbody += "<td><select name='txtTipo_" + nParc + "' id='txtTipo_" + nParc + "' style='width:100%;background-color:" + backcolor + "' " + disabled + "> \n\
             " + $('#txtTipo').html() + "</select></td>";
    tbody += "</tr>";
    $("#tbParcelas tbody").append(tbody);
    refreshMask();
}

function recalculaParc(TotalVenda, TotalPago, idLinha) {
    var qtd = parseInt($("#spinner").val()) - (idLinha - 1);
    var valParc = ((TotalVenda - TotalPago) / qtd);
    if ($('#tbParcelas >tbody >tr').length < idLinha) {
        valParc = textToNumber($("#txtParcelaV_" + (idLinha - 1)).val());
    }
    var total = TotalPago;
    var totParc = 0;
    for (var i = idLinha; i <= $('#tbParcelas >tbody >tr').length; i++) {
        $("#txtParcelaV_" + (i)).val(numberFormat(valParc));
        total = total + valParc;
        totParc = totParc + textToNumber($("#txtParcelaV_" + (i)).val());
    }
    var difTotal = TotalVenda - (totParc + TotalPago);
    if (difTotal > 0) {
        if ($('#tbParcelas >tbody >tr').length < idLinha) {
            $("#txtParcelaV_" + (idLinha - 1)).val(numberFormat(valParc + difTotal));
        } else {
            $("#txtParcelaV_" + (idLinha)).val(numberFormat(valParc + difTotal));
        }
    } else if (difTotal < 0) {
        $("#txtParcelaV_" + ($('#tbParcelas >tbody >tr').length)).val(numberFormat(valParc + difTotal));
    }
    $('#lblTotParc').hide();
    $('#lblTotParc').html('<tr><td>Total em parcelas: <strong>' + numberFormat(total, 1) + '</strong></td></tr>').show("slow");
}

function validar() {
    var error = '';
    if ($("#txtTipoGrupo").val() === "--Selecione--") {
        error = "Selecione um tipo.";
    } else if ($("#txtGrupo").val() === '') {
        error = "Selecione um grupo de contas.";
    } else if ($("#txtConta").val() === '') {
        error = "Selecione um Subgrupo de contas.";
    } else if ($("#txtFonecedor").val() === '') {
        error = "Selecione um Fornecedor/Funcionário/Transportadora.";
    } else if ($("#txtTipo").val() === '') {
        error = "Selecione um Tipo de Doc.";
    } else if ($("#txtDestinatario").val() === '') {
        error = "Selecione um destinatário.";
    } else if ($("#txtHistorico").val() === '') {
        error = "Digite um histórico.";
    } else if ($('#tbParcelas >tbody >tr').length === 0) {
        error = "Insira o valor da conta variavel.";
    } else if ($("#txtDestinatario").val() === $("#txtDestinatario2").val()) {
        error = "Os campos Destinatários não podem ser iguais!";
    }
    if (error !== '') {
        bootbox.alert(error);
        return false;
    } else {
        return true;
    }
}

var add = 0;
function AbrirBox(opc) {
    if (opc === 1) {
        add = 1;
        abrirTelaBox("./../Financeiro/FormGrupo-de-contas.php?idx=0&add=0", 245, 350);
    } else if (opc === 2) {
        add = 2;
        abrirTelaBox("./../Financeiro/FormContas-de-Movimentos.php?idx=0", 315, 500);
    } else if (opc === 3) {
        add = 3;
        if ($("#txtTipoGrupo").val() === 'C') {
            abrirTelaBox("./../Comercial/FormGerenciador-Clientes.php?idx=0", 525, 715);
        } else {
            abrirTelaBox("./../Financeiro/FormFornecedores-de-despesas.php?idx=0", 525, 715);
        }
    } else if (opc === 4) {
        add = 4;
        abrirTelaBox("./../Financeiro/FormTipos-de-Documentos.php?idx=0", 484, 650);
    }
}

function FecharBox() {
    $("#newbox").remove();
    if (add === 1) {
        zeraTxtGrupo();
    } else if (add === 2) {
        zeraTxtConta();
    } else if (add === 3) {
        zeraTxtFonecedor();
    } else if (add === 4) {
        zeraTipo();
        zeraTxtTipo();
    }
}

function zeraTipo() {
    $("#txtTipoGrupo").val('');
    $("#txtTipoGrupo").trigger("change");
}

function zeraTxtGrupo() {
    $("#txtGrupo").trigger("change");
    $("#txtGrupo").val('');
    $("#s2id_txtGrupo > .select2-choice > span").html("--Selecione--");
}

function zeraTxtConta() {
    $("#txtConta").trigger("change");
    $("#txtConta").val('');
    $("#s2id_txtConta > .select2-choice > span").html("--Selecione--");
}

function zeraTxtFonecedor() {
    $("#txtFonecedor").val('');
}

function zeraTxtTipo() {
    $("#txtTipo").val('');
    $("#s2id_txtTipo > .select2-choice > span").html("--Selecione--");
    $("#txtTipo").trigger("change");
}

function carregaParcelas() {
    $.getJSON("ajax/ajax-FormSolicitacao-de-autorizacao.php?id=" + $("#txtId").val(), function (data) {
        var total = 0;
        if (data !== null) {
            var nParc = 0;
            for (var i = 0; i < data.length; i++) {
                nParc = i + 1;
                var pago = "N";
                var backcolor = "";
                var disabled = "";
                if (data[i].Valor_pago !== numberFormat(0)) {
                    pago = "S";
                    backcolor = "#4899ba";
                    disabled = "disabled";
                } else {
                    if ($("#txtGrupo").prop("disabled") === false) {
                        disabled = "";
                    } else {
                        disabled = "disabled";
                    }
                    pago = "N";
                    backcolor = "";
                }
                addLinhaParcela(nParc, backcolor, disabled, data[i].id_parcela, pago, data[i].sa_descricao, data[i].data_parcela, data[i].valor_parcela);
                $("#txtTipo_" + nParc).val(data[i].tipo_documento);
                total += textToNumber(data[i].valor_parcela);
            }
            $('#lblTotParc').html('<tr><td>Total em parcelas: <strong>' + numberFormat(total, 1) + '</strong></td></tr>').show("slow");
        }
    });
}

function resetParcel() {
    if ($("#txtValReajustar").val() !== "0" && $.isNumeric(textToNumber($("#txtValReajustar").val()))) {
        $('#txtValReajustarInvalido').hide();
        $("#txtValReajustar").parent().hide();
        $("#source").dialog('close');
        var total = 0;
        var percent = textToNumber($("#txtValReajustar").val());
        var qtde = parseInt($("#spinner").val());
        for (var i = 0; i < qtde; i++) {
            if ($("#txtPG_" + (i + 1)).val() !== 'S') {
                var subtotal = textToNumber($("#txtParcelaV_" + (i + 1)).val());
                subtotal = subtotal + ((subtotal * percent) / 100);
                $("#txtParcelaV_" + (i + 1)).val(numberFormat(subtotal));
                total += subtotal;
            } else {
                total += textToNumber($("#txtParcelaV_" + (i + 1)).val());
            }
        }
        $('#txtParcelaMakeT').val(numberFormat(total));
        $('#lblTotParc').hide();
        $('#lblTotParc').html('<tr><td>Total em parcelas: <strong>' + numberFormat(total, 1) + '</strong></td></tr>').show("slow");
    } else {
        $('#txtValReajustarInvalido').show();
    }
}

$('#tbDocumentos').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bSort": false,
    "bLengthChange": false,
    "bFilter": false,  
    "sAjaxSource": "../../Modulos/CRM/ajax/Documentos_server_processing.php?id=" + $("#txtId").val() + "&type=ContasVariaveis",
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

function refreshDoc() {
    let tblCRM = $('#tbDocumentos').dataTable();
    tblCRM.fnReloadAjax("../../Modulos/CRM/ajax/Documentos_server_processing.php?id=" + $("#txtId").val() + "&type=ContasVariaveis");
}

function AdicionarDoc(e) {
    $("#loader").show();
    var formData = new FormData();
    formData.append("btnSendFile", "S");
    formData.append("txtId", $("#txtId").val());
    formData.append("type", "ContasVariaveis");
    formData.append("file", $("#file")[0].files[0]); 
    $.ajax({
        type: 'POST',
        url: "../../Modulos/CRM/form/CRMDocumentos.php",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    }).done(function (data) {
        $("#loader").hide();
        if (data === "YES") {
            $('#file').val('');
            refreshDoc();
        } else {
            bootbox.alert(data);
        }
    });
}

function RemoverDoc(filename) {
    var nomedoc = encodeURIComponent(filename.toString());
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/CRM/form/CRMDocumentos.php", "btnDelFile=S&txtId=" + $("#txtId").val() + "&type=ContasVariaveis&txtFileName=" + nomedoc).done(function (data) {
                if (data === "YES") {
                    refreshDoc();
                } else {
                    bootbox.alert(data + "!");
                }
            });
        } else {
            return;
        }
    });
}