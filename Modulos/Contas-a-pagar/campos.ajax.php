<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include "../../Connections/configini.php";

$id_area = $_REQUEST['txtCampos'];
$local = array();
$sql = "select id_tipo_documento_campos,campo from sf_tipo_documento_campos where inativo = 0 AND tipo_documento = " . $id_area . " ORDER BY 1";
$cur = odbc_exec($con, $sql) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $local[] = array('id_tipo_documento_campos' => $RFP['id_tipo_documento_campos'], 'campo' => utf8_encode($RFP['campo']));
}
echo( json_encode($local) );
odbc_close($con);
