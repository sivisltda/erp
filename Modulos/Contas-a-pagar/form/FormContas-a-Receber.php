<?php

require_once('../../../Connections/configini.php');

if (isset($_POST['bntInativo'])) {
    if (isset($_POST['items'])) {
        for ($i = 0; $i < count($_POST['items']); $i++) {
            $item = explode("-", $_POST['items'][$i]);
            if (count($item) == 2) {
                if ($item[0] == 'A') {
                    odbc_exec($con, "UPDATE sf_solicitacao_autorizacao_parcelas SET inativo = '1' WHERE id_parcela = " . $item[1]);
                } else if ($item[0] == 'D') {
                    odbc_exec($con, "UPDATE sf_lancamento_movimento_parcelas SET inativo = '1' WHERE id_parcela = " . $item[1]);
                } else if ($item[0] == 'Y') {
                    odbc_exec($con, "UPDATE sf_lancamento_movimento_parcelas SET inativo = '1' 
                    WHERE id_parcela in (select id_parcela from sf_lancamento_movimento_parcelas where bol_nosso_numero in (
                    select bol_nosso_numero from sf_lancamento_movimento_parcelas where id_parcela = " . $item[1] . "))");
                } else if ($item[0] == 'V') {
                    odbc_exec($con, "UPDATE sf_venda_parcelas SET inativo = '1' WHERE id_parcela = " . $item[1]);
                } else if ($item[0] == 'M') {
                    odbc_exec($con, "DELETE FROM sf_vendas_planos_mensalidade WHERE id_item_venda_mens is null and id_mens = " . $item[1]);
                }
            }
        }
        echo "YES";
    }
}

if (isset($_POST['bntDesfazer'])) {
    if (isset($_POST['items'])) {
        for ($i = 0; $i < count($_POST['items']); $i++) {
            $item = explode("-", $_POST['items'][$i]);
            if (count($item) == 2) {
                if ($item[0] == 'A') {
                    $query = "UPDATE sf_solicitacao_autorizacao_parcelas set valor_pago = 0, data_pagamento = null, id_banco = null, obs_baixa = null, syslogin = null where id_parcela = " . $item[1];
                    $tabela = "sf_solicitacao_autorizacao_par";
                } else if ($item[0] == 'D') {
                    $query = "UPDATE sf_lancamento_movimento_parcelas set valor_pago = 0, data_pagamento = null, id_banco = null, obs_baixa = null, syslogin = null where  id_parcela = " . $item[1];
                    $tabela = "sf_lancamento_movimento_parc";
                } else if ($item[0] == 'Y') {
                    $query = "UPDATE sf_lancamento_movimento_parcelas set valor_pago = 0, data_pagamento = null, id_banco = null, obs_baixa = null, syslogin = null WHERE id_parcela in (select id_parcela from sf_lancamento_movimento_parcelas where bol_nosso_numero in (
                              select bol_nosso_numero from sf_lancamento_movimento_parcelas where id_parcela = " . $item[1] . "))";
                    $tabela = "sf_lancamento_movimento_parc";
                } else if ($item[0] == 'V') {
                    $query = "UPDATE sf_venda_parcelas set valor_pago = 0, data_pagamento = null, obs_baixa = null where id_parcela =  " . $item[1];
                    $tabela = "sf_venda_parcelas";
                }
                $query = "set dateformat dmy;
                    BEGIN TRANSACTION " . $query . "
                    insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
                    ('" . $tabela . "', " . $item[1] . ", '" . $_SESSION["login_usuario"] . "', 'A', 'DESFAZER BAIXA', GETDATE(), null);
                    IF @@ERROR = 0
                    COMMIT
                    ELSE
                    ROLLBACK;";
                odbc_exec($con, $query);
            }
        }
        echo "YES";
    }
}

if (isset($_POST['bntExcluir'])) {
    if (isset($_POST['items'])) {
        for ($i = 0; $i < count($_POST['items']); $i++) {
            $item = explode("-", $_POST['items'][$i]);
            if (count($item) == 2) {
                if ($item[0] == 'A') {
                    $query = "delete from sf_solicitacao_autorizacao_parcelas WHERE id_parcela = " . $item[1];
                    $queryx = "select top 1 'PARCELA EXCLUIDA: ' + pa + ' - ' + CONVERT(VARCHAR(10), data_parcela, 103) + ' - ' + FORMAT(valor_parcela, 'C', 'pt-BR') from sf_solicitacao_autorizacao_parcelas WHERE id_parcela = " . $item[1];
                    $queryy = "select top 1 fornecedor_despesa from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao on sf_solicitacao_autorizacao.id_solicitacao_autorizacao = sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao WHERE id_parcela = " . $item[1];
                    $tabela = "sf_solicitacao_autorizacao_parcelas";
                } else if ($item[0] == 'D') {
                    $query = "delete from sf_lancamento_movimento_parcelas WHERE id_parcela = " . $item[1];
                    $queryx = "select top 1 'PARCELA EXCLUIDA: ' + pa + ' - ' + CONVERT(VARCHAR(10), data_vencimento, 103) + ' - ' + FORMAT(valor_parcela, 'C', 'pt-BR') from sf_lancamento_movimento_parcelas WHERE id_parcela = " . $item[1];
                    $queryy = "select top 1 fornecedor_despesa from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento on sf_lancamento_movimento.id_lancamento_movimento = sf_lancamento_movimento_parcelas.id_lancamento_movimento WHERE id_parcela = " . $item[1];
                    $tabela = "sf_lancamento_movimento_parcelas";
                } else if ($item[0] == 'Y') {
                    $query = "delete from sf_lancamento_movimento_parcelas WHERE id_parcela in (select id_parcela from sf_lancamento_movimento_parcelas where bol_nosso_numero in (
                    select bol_nosso_numero from sf_lancamento_movimento_parcelas where id_parcela = " . $item[1] . "))";
                    $queryx = "select top 1 'PARCELA EXCLUIDA: ' + pa + ' - ' + CONVERT(VARCHAR(10), data_vencimento, 103) + ' - ' + FORMAT(valor_parcela, 'C', 'pt-BR') from sf_lancamento_movimento_parcelas WHERE id_parcela = " . $item[1];                    
                    $queryy = "select top 1 fornecedor_despesa from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento on sf_lancamento_movimento.id_lancamento_movimento = sf_lancamento_movimento_parcelas.id_lancamento_movimento WHERE id_parcela = " . $item[1];                    
                    $tabela = "sf_lancamento_movimento_parcelas";
                } else if ($item[0] == 'V') {
                    $query = "delete from sf_venda_parcelas WHERE id_parcela = " . $item[1];
                    $queryx = "select top 1 'PARCELA EXCLUIDA: ' + pa + ' - ' + CONVERT(VARCHAR(10), data_parcela, 103) + ' - ' + FORMAT(valor_parcela, 'C', 'pt-BR') from sf_venda_parcelas WHERE id_parcela = " . $item[1];
                    $queryy = "select top 1 cliente_venda from sf_venda_parcelas inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda WHERE id_parcela = " . $item[1];                          
                    $tabela = "sf_venda_parcelas";
                }                
                odbc_exec($con, "set dateformat dmy;
                BEGIN TRANSACTION
                insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
                ('" . $tabela . "', " . $item[1] . ", '" . $_SESSION["login_usuario"] . "', 'X', (" . $queryx . "), GETDATE(), (" . $queryy . "));" . $query . ";
                IF @@ERROR = 0
                COMMIT
                ELSE
                ROLLBACK;");
            }
        }
        echo "YES";
    }
}

odbc_close($con);