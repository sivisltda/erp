<?php

$FinalUrl = '';
if ($_GET['idx'] != '') {
    $FinalUrl = "&idx=" . $_GET['idx'];
}
if ($_GET['idv'] != '') {
    $FinalUrl = "&idv=" . $_GET['idv'];
}
if ($_GET['tpx'] != '') {
    $FinalUrl = $FinalUrl . "&tpx=" . $_GET['tpx'];
}
if ($_GET['tdx'] != '') {
    $FinalUrl = $FinalUrl . "&tdx=" . $_GET['tdx'];
}
if ($_GET['dti'] != '') {
    $FinalUrl = $FinalUrl . "&dti=" . $_GET['dti'];
}
if ($_GET['dtf'] != '') {
    $FinalUrl = $FinalUrl . "&dtf=" . $_GET['dtf'];
}
if ($_GET['id'] != '') {
    $FinalUrl = "id=" . $_GET['id'] . $FinalUrl;
}

$disabled = 'disabled';
$r_documento = '';
$r_ven_ini = '';
$r_valor = '';
$r_valor_tot = '0';
$r_quantidade = '1';

if (isset($_POST['bntSave'])) {
    if (empty($_POST['txtConta']) || is_numeric($_POST['txtFonecedor']) == false || empty($_POST['txtTipo']) || empty($_POST['txtDestinatario']) || ($_POST['txtDestinatario'] == $_POST['txtDestinatario2'])) {
        if (empty($_POST['txtConta'])) {
            $campo .= "Conta,";
        }
        if (is_numeric($_POST['txtFonecedor']) == false) {
            $campo .= "Fornecedor,";
        }
        if (empty($_POST['txtTipo'])) {
            $campo .= "Tipo,";
        }
        if (empty($_POST['txtDestinatario'])) {
            $campo .= "Destinatário";
        }
        if ($_POST['txtDestinatario'] == $_POST['txtDestinatario2']) {
            $campo .= "2° Destinatário";
        }
    } else {
        if ($_POST['txtId'] != '') {
            odbc_exec($con, "set dateformat dmy");
            $query = "update sf_solicitacao_autorizacao set " .
            "empresa = " . valoresTexto("txtEmpresa") . "," .
            "grupo_conta = " . $_POST['txtGrupo'] . "," .
            "conta_movimento = " . $_POST['txtConta'] . "," .
            "fornecedor_despesa = " . $_POST['txtFonecedor'] . "," .
            "tipo_documento = " . $_POST['txtTipo'] . "," .
            "historico = " . valoresTexto("txtHistorico") . "," .
            "destinatario = " .valoresTexto("txtDestinatario") . "," .
            "destinatario2 = " . valoresTexto("txtDestinatario2") . "," .
            "comentarios = " . valoresTexto("txtComentarios") . "," .
            "data_lanc = " . valoresData("txtData") . "," .
            "destaprov = 0, destaprov2 = 0, data_aprov = null where id_solicitacao_autorizacao = " . $_POST['txtId'];
            //echo $query;exit();
            odbc_exec($con, $query) or die(odbc_errormsg());
            //-------------------------------Campos-de-Preenchimento-para-Documentos------------------------
            odbc_exec($con, "delete from sf_solicitacao_autorizacao_campos where id_solicitacao_autorizacao = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
            $sql = "select id_tipo_documento_campos,campo from sf_tipo_documento_campos where inativo = 0 AND tipo_documento = " . $_POST['txtTipo'] . " ORDER BY 1";
            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                $sss = "insert into sf_solicitacao_autorizacao_campos(id_solicitacao_autorizacao,tipo_documento_campos,conteudo) values (" .
                $_POST['txtId'] . "," . $RFP['id_tipo_documento_campos'] . "," . valoresTexto("txtCampo" . $RFP['id_tipo_documento_campos']) . ")";
                odbc_exec($con, $sss);
            }
            //----------------------------Calcula--Número--de--Parcelas-------------------------------------
            $totaldeParcelas = 0;
            for ($i = 1; $i <= 100; $i++) {
                $dataParcela = valoresData('txtParcelaD_' . $i);
                $valorParcela = valoresNumericos('txtParcelaV_' . $i);                
                if (is_numeric($valorParcela) && $dataParcela != "null") {
                    if ($valorParcela > 0) {
                        $totaldeParcelas = $totaldeParcelas + 1;
                    }
                }
            }
            for ($i = 1; $i <= 100; $i++) {
                if (is_numeric($_POST['txtID_' . $i])) {
                    if ($_POST['txtPG_' . $i] == "S") {
                        $totaldeParcelas = $totaldeParcelas + 1;
                    }
                }
            }
            //----------------------------Altera--as--Parcelas----------------------------------------------
            $toDelete = "0";
            for ($i = 1; $i <= 100; $i++) {
                $dataParcela = valoresData('txtParcelaD_' . $i);
                $valorParcela = valoresNumericos('txtParcelaV_' . $i);
                if (is_numeric($valorParcela) && $dataParcela != "null") {
                    if ($valorParcela > 0) {
                        if ($_POST['txtPG_' . $i] != "S") {
                            if (is_numeric($_POST['txtID_' . $i])) {
                                odbc_exec($con, "update sf_solicitacao_autorizacao_parcelas set
                                solicitacao_autorizacao = " . $_POST['txtId'] . "," .
                                "numero_parcela = " . $i . "," .
                                "data_parcela = " . $dataParcela . "," .
                                "sa_descricao = " . valoresTexto('txtParcelaDC_' . $i) . "," .
                                "tipo_documento = " . $_POST['txtTipo_' . $i] . "," .
                                "valor_parcela = " . $valorParcela . "," .
                                "historico_baixa = " . valoresTexto("txtHistorico") . "," .
                                "pa = '" . str_pad($i, 2, "0", STR_PAD_LEFT) . "/" . str_pad($totaldeParcelas, 2, "0", STR_PAD_LEFT) . "' " .
                                "where id_parcela = " . $_POST['txtID_' . $i]) or die(odbc_errormsg());
                                $toDelete = $toDelete . "," . $_POST['txtID_' . $i];
                            }
                        }
                    }
                }
            }
            //----------------------------Exclui--as--Parcelas--Removidas-----------------------------------
            odbc_exec($con, "DELETE FROM sf_solicitacao_autorizacao_parcelas where id_parcela not in (" . $toDelete . ") and data_pagamento is null and inativo = 0 and solicitacao_autorizacao = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
            //----------------------------Insere--as--Parcelas-Novas----------------------------------------
            for ($i = 1; $i <= 100; $i++) {
                $dataParcela = valoresData('txtParcelaD_' . $i);
                $valorParcela = valoresNumericos('txtParcelaV_' . $i);                
                if (is_numeric($valorParcela) && $dataParcela != "null") {
                    if ($valorParcela > 0) {
                        if ($_POST['txtPG_' . $i] != "S") {
                            if ($_POST['txtID_' . $i] == "") {
                                odbc_exec($con, "insert into sf_solicitacao_autorizacao_parcelas(solicitacao_autorizacao,numero_parcela,data_parcela,sa_descricao,tipo_documento,valor_parcela,historico_baixa,pa,data_cadastro) values (" .
                                $_POST['txtId'] . "," . $i . "," . $dataParcela . "," . valoresTexto('txtParcelaDC_' . $i) . "," . valoresTexto('txtTipo_' . $i) . "," . $valorParcela . "," . valoresTexto('txtHistorico') . ",'" . str_pad($i, 2, "0", STR_PAD_LEFT) . "/" . str_pad($totaldeParcelas, 2, "0", STR_PAD_LEFT) . "'," . valoresData("txtData") . ")") or die(odbc_errormsg());
                                $toInsert = $toInsert . "," . $i;
                            }
                        }
                    }
                }
            }
        } else {
            $nossoNumero = "null";
            $bol_id_banco = "null";
            $bol_data_criacao = "null";
            $bol_valor = "null";
            $bol_juros = "null";
            $bol_multa = "null";
            $carteira_id = "null";
            if (isset($_POST['txtBanco'])) {
                if ($_POST['txtBanco'] != "null") {
                    $cur = odbc_exec($con, "select isnull(MAX(x.bol_nosso_numero),0) vm, isnull(nosso_numero,0) nn from (
                    select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_boleto p on p.bol_id_banco = b.id_bancos
                    where id_bancos = '" . $_POST['txtBanco'] . "' union all                        
                    select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_solicitacao_autorizacao_parcelas p on p.bol_id_banco = b.id_bancos
                    where id_bancos = '" . $_POST['txtBanco'] . "' union all
                    select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_venda_parcelas p on p.bol_id_banco = b.id_bancos
                    where id_bancos = '" . $_POST['txtBanco'] . "' union all
                    select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_lancamento_movimento_parcelas l on l.bol_id_banco = b.id_bancos
                    where id_bancos = '" . $_POST['txtBanco'] . "') as x group by x.nosso_numero") or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) {
                        if ($RFP['nn'] > $RFP['vm']) {
                            $nossoNumero = $RFP['nn'];
                        } else {
                            $nossoNumero = bcadd($RFP['vm'], 1);
                        }
                    }
                    $bol_id_banco = $_POST['txtBanco'];
                    $bol_data_criacao = "getdate()";
                    $bol_juros = "0.00";
                    $bol_multa = "0.00";
                    if (is_numeric($_POST['txtBancoCarteira'])) {
                        $carteira_id = $_POST['txtBancoCarteira'];
                    } else {
                        $carteira_id = "null";
                    }
                }
            }
            odbc_exec($con, "set dateformat dmy");
            $query = "insert into sf_solicitacao_autorizacao(empresa,grupo_conta,conta_movimento,fornecedor_despesa,tipo_documento,historico,destinatario,destinatario2,comentarios,sys_login,data_lanc,status)values(" .
            valoresTexto('txtEmpresa') . "," . $_POST['txtGrupo'] . "," . $_POST['txtConta'] . "," . $_POST['txtFonecedor'] . "," . $_POST['txtTipo'] . "," .
            valoresTexto('txtHistorico') . "," . valoresTexto('txtDestinatario') . "," . valoresTexto('txtDestinatario2') . "," .
            valoresTexto('txtComentarios') . ",'" . $_SESSION['login_usuario'] . "'," . valoresData("txtData") . ",'Aguarda')";
            //echo $query;exit();
            odbc_exec($con, $query) or die(odbc_errormsg());
            $res = odbc_exec($con, "select top 1 id_solicitacao_autorizacao from sf_solicitacao_autorizacao order by id_solicitacao_autorizacao desc") or die(odbc_errormsg());
            $nn = odbc_result($res, 1);
            $_POST['txtId'] = $nn;
            $sql = "select id_tipo_documento_campos,campo from sf_tipo_documento_campos where inativo = 0 AND tipo_documento = " . $_POST['txtTipo'] . " ORDER BY 1";
            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                $query = "insert into sf_solicitacao_autorizacao_campos(id_solicitacao_autorizacao,tipo_documento_campos,conteudo) values (" .
                $_POST['txtId'] . "," . $RFP['id_tipo_documento_campos'] . "," . valoresTexto('txtCampo' . $RFP['id_tipo_documento_campos']) . ")";
                odbc_exec($con, $query);
            }
            $totaldeParcelas = 0;
            for ($i = 1; $i <= 100; $i++) {
                $dataParcela = valoresData('txtParcelaD_' . $i);
                $valorParcela = valoresNumericos('txtParcelaV_' . $i);
                if (is_numeric($valorParcela) && $dataParcela != "null") {
                    if ($valorParcela > 0) {
                        $totaldeParcelas = $totaldeParcelas + 1;
                    }
                }
            }
            for ($i = 1; $i <= 100; $i++) {
                $dataParcela = valoresData('txtParcelaD_' . $i);
                $valorParcela = valoresNumericos('txtParcelaV_' . $i);
                if (is_numeric($valorParcela) && $dataParcela != "null") {
                    if ($valorParcela > 0) {
                        if ($bol_id_banco != "null") {
                            $bol_valor = $valorParcela;
                        }
                        odbc_exec($con, "set dateformat dmy; insert into sf_solicitacao_autorizacao_parcelas(solicitacao_autorizacao,numero_parcela,data_parcela,sa_descricao,tipo_documento,valor_parcela,historico_baixa,pa,bol_id_banco,bol_data_criacao,bol_valor,bol_juros,bol_multa,bol_nosso_numero,carteira_id,data_cadastro) values (" .
                        $_POST['txtId'] . "," . $i . "," . $dataParcela . "," . valoresTexto('txtParcelaDC_' . $i) . "," . valoresTexto('txtTipo_' . $i) . "," . $valorParcela . "," . valoresTexto('txtHistorico') . ",'" . str_pad($i, 2, "0", STR_PAD_LEFT) . "/" . str_pad($totaldeParcelas, 2, "0", STR_PAD_LEFT) . "'," .
                        $bol_id_banco . "," . $bol_data_criacao . "," .
                        $bol_valor . "," . $bol_juros . "," .
                        $bol_multa . "," . $nossoNumero . "," . $carteira_id . "," . valoresData("txtData") . ")") or die(odbc_errormsg());
                        if ($nossoNumero != "null") {
                            $nossoNumero = bcadd($nossoNumero, 1);
                        }
                    }
                }
            }
            echo "<script>window.top.location.href = 'FormSolicitacao-de-Autorizacao.php?id=" . $nn . "';</script>";
        }
    }
}

if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_creditos where id_solicitacao_autorizacao = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
        odbc_exec($con, "DELETE FROM sf_solicitacao_autorizacao_parcelas where solicitacao_autorizacao = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
        odbc_exec($con, "DELETE FROM sf_solicitacao_autorizacao_campos where id_solicitacao_autorizacao = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
        odbc_exec($con, "DELETE FROM sf_solicitacao_autorizacao WHERE id_solicitacao_autorizacao = " . $_POST['txtId']) or die(odbc_errormsg());
        echo "<script>alert('Registro excluido com sucesso');window.top.location.href = 'Solicitacao-de-Autorizacao.php?id=1';</script>";
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
    echo "<script>window.top.location.href = 'FormSolicitacao-de-Autorizacao.php';</script>";
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_solicitacao_autorizacao 
    left join sf_contas_movimento on id_contas_movimento = conta_movimento 
    where id_solicitacao_autorizacao = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_solicitacao_autorizacao'];
        $empresa = $RFP['empresa'];
        $grupo = $RFP['grupo_conta'];
        $conta = $RFP['conta_movimento'];
        $fornecedor = $RFP['fornecedor_despesa'];
        $tipo = $RFP['tipo_documento'];
        $historico = utf8_encode($RFP['historico']);
        $destinatario = utf8_encode($RFP['destinatario']);
        $destaprov = utf8_encode($RFP['destaprov']);
        $destinatario2 = utf8_encode($RFP['destinatario2']);
        $destaprov2 = utf8_encode($RFP['destaprov2']);
        $comentarios = utf8_encode($RFP['comentarios']);
        $TipoGrupo = utf8_encode($RFP['tipo']);
        $status = utf8_encode($RFP['status']);
        $solicitante = utf8_encode($RFP['sys_login']);
        $data = escreverData($RFP['data_lanc']);
    }

    if (is_numeric($fornecedor)) {
        $cur = odbc_exec($con, "SELECT * FROM sf_fornecedores_despesas WHERE id_fornecedores_despesas = " . $fornecedor);
        while ($RFP = odbc_fetch_array($cur)) {
            $fornecedorNome = utf8_encode($RFP['razao_social']);
        }
    }
    
    $i = 0;
    $r_quantidade = '1';
    $cur = odbc_exec($con, "select sa_descricao,data_parcela,valor_parcela from dbo.sf_solicitacao_autorizacao_parcelas
                                    where inativo = 0 and solicitacao_autorizacao = " . $PegaURL . " order by id_parcela");
    while ($RFP = odbc_fetch_array($cur)) {
        if ($i == 0) {
            $r_documento = $RFP['sa_descricao'];
            $r_ven_ini = escreverData($RFP['data_parcela']);
        }
        $r_valor = escreverNumero($RFP['valor_parcela']);
        $r_valor_tot = $r_valor_tot + $RFP['valor_parcela'];
        $i++;
    }
    $r_valor_tot = escreverNumero($r_valor_tot);
    if ($i > 0) {
        $r_quantidade = $i;
    }
} else {
    $disabled = '';
    $id = '';
    $empresa = '';
    $grupo = '';
    $conta = '';
    $fornecedor = '';
    $fornecedorNome = '';
    $tipo = '';
    $historico = '';
    $destinatario = '';
    $destaprov = '';
    $destinatario2 = '';
    $destaprov2 = '';
    $comentarios = '';
    $TipoGrupo = '';
    $status = '';
    $solicitante = $_SESSION['login_usuario'];
    $data = getData("T");
}

if (isset($_POST['bntAprov']) || isset($_POST['bntReprov'])) {
    if ($destaprov < 2 && $destaprov2 < 2) {
        if (isset($_POST['bntAprov'])) {
            if ($destinatario == $_SESSION['login_usuario']) {
                if ($destaprov2 == 1 || empty($destinatario2)) {
                    $query = "status = 'Aprovado',";
                    $status = 'Aprovado';
                }
                odbc_exec($con, "update sf_solicitacao_autorizacao set " . $query . "destaprov = 1, data_aprov = getdate() where id_solicitacao_autorizacao = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
                $destaprov = 1;
            } elseif ($destinatario2 == $_SESSION['login_usuario']) {
                if ($destaprov == 1) {
                    $query = "status = 'Aprovado',";
                    $status = 'Aprovado';
                }
                odbc_exec($con, "update sf_solicitacao_autorizacao set " . $query . "destaprov2 = 1, data_aprov = getdate() where id_solicitacao_autorizacao = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
                $destaprov2 = 1;
            }
            $returnTo = "";
            $cur = odbc_exec($con, "select COUNT(id_parcela) qtd, max(id_parcela) id_parcela from sf_solicitacao_autorizacao_parcelas where
            bol_nosso_numero is not null and solicitacao_autorizacao = '" . $_POST['txtId'] . "' group by bol_nosso_numero") or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                if ($RFP['qtd'] == 1) {
                    $returnTo = $returnTo . "A-" . $RFP['id_parcela'] . "|";
                }
            }
            if ($returnTo != "") {
                echo "<script>window.open('/Boletos/Boleto.php?id=" . substr($returnTo, 0, -1) . "&crt=" . $_SESSION["contrato"] . "','_blank');</script>";
            }
        } else {
            if ($destinatario == $_SESSION['login_usuario']) {
                odbc_exec($con, "update sf_solicitacao_autorizacao set status = 'Reprovado', destaprov = 2, data_aprov = getdate() where id_solicitacao_autorizacao = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
                $destaprov = 2;
            } elseif ($destinatario2 == $_SESSION['login_usuario']) {
                odbc_exec($con, "update sf_solicitacao_autorizacao set status = 'Reprovado', destaprov2 = 2, data_aprov = getdate() where id_solicitacao_autorizacao = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
                $destaprov2 = 2;
            }
            $status = 'Reprovado';
        }
    }
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}

if (isset($_POST['bntVoltar'])) {
    if ($_GET['idx'] != '') {
        $FinalUrl = "?id=" . $_GET['idx'];
    }
    if ($_GET['tpx'] != '') {
        $FinalUrl = $FinalUrl . "&tp=" . $_GET['tpx'];
    }
    if ($_GET['tdx'] != '') {
        $FinalUrl = $FinalUrl . "&td=" . $_GET['tdx'];
    }
    if ($_GET['dti'] != '') {
        $FinalUrl = $FinalUrl . "&dti=" . $_GET['dti'];
    }
    if ($_GET['dtf'] != '') {
        $FinalUrl = $FinalUrl . "&dtf=" . $_GET['dtf'];
    }
    if ($_GET['idv'] != '') {
        $FinalUrl = "?id=" . $_GET['idv'];
        echo "<script>window.top.location.href = './../Estoque/FormVendas.php" . $FinalUrl . "';</script>";
    } else {
        if ($TipoGrupo == "D") {
            echo "<script>window.top.location.href = 'Contas-a-Pagar.php" . $FinalUrl . "';</script>";
        } elseif ($TipoGrupo == "C") {
            echo "<script>window.top.location.href = 'Contas-a-Receber.php" . $FinalUrl . "';</script>";
        }
    }
}