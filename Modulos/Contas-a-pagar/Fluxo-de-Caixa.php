<?php
include '../../Connections/configini.php';

$DescPagina = "Fluxo de Caixa";
$PageName = "Fluxo-de-Caixa2";
$Module = "Financeiro";
$aColumnsX = array('Dt.Baixa', 'Loja', 'Banco', 'Tipo Documento', 'Fornec./Clientes', 'Histórico', 'Referência', 'Crédito', 'Débito', 'Saldo');
$aColumnsP = array('6', '4', '10', '12', '20', '17', '12', '6', '6', '6');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>                       
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1><?php echo $Module; ?><small><?php echo $DescPagina; ?></small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="width: 12%;float:left;margin-top: 15px;">
                                    <div style="float:left">
                                        <form method="POST">
                                            <?php if ($ckb_fcaixa_read_ == 0) { ?>
                                            <button  class="button button-green btn-primary" type="button" title="Transferência" onClick="AbrirBox('')"><span class="ico-reply icon-white"></span></button>
                                            <?php } ?>
                                            <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir('I')" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                            <button id="btnExcel" class="button button-blue btn-primary" type="button" onclick="imprimir('E')" title="Exportar Excel"><span class="ico-download-3"></span></button>
                                        </form>
                                    </div>
                                </div>
                                <div style="float: right;width: 88%;">
                                    <div style="width:14%;float:left;">
                                        <span>Banco:</span>                                        
                                        <select id="txtBanco">                                            
                                            <?php $cur = odbc_exec($con, "select id_bancos,razao_social from sf_bancos where ativo = 0 order by id_bancos") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo utf8_encode($RFP['id_bancos']); ?>"><?php echo utf8_encode($RFP['razao_social']); ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div style="width:17%;float:left;margin-left:1%">
                                        <span>Forma de Pagamento:</span>
                                        <select name="txtTipoDocumento[]" id="txtTipoDocumento" multiple="multiple" class="select" style="width:100%" class="input-medium">                                        
                                            <?php $cur = odbc_exec($con, "select id_tipo_documento,descricao from sf_tipo_documento where inativo = 0 order by descricao") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo utf8_encode($RFP['id_tipo_documento']); ?>"><?php echo utf8_encode($RFP['descricao']); ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div style="width:14%; float:left;margin-left:1%">
                                        <span>Operador:</span>
                                        <select id="txtOperador">
                                            <option value="">TODOS</option>                                            
                                            <?php $cur = odbc_exec($con, "select id_usuario,UPPER(nome) nome,login_user from sf_usuarios where " . ($ckb_out_todos_operadores_ != 1 ? " id_usuario = " . $_SESSION["id_usuario"] . " AND " : "") . " inativo = 0 order by nome") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo utf8_encode($RFP['login_user']); ?>"><?php echo utf8_encode($RFP['nome']); ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div style="width:14%; float:left;margin-left:1%">
                                        <span>Grupo de Clientes:</span>
                                        <select id="txtContaMov">
                                            <option value="">TODOS</option>                                            
                                            <?php $cur = odbc_exec($con, "select id_grupo,descricao_grupo from dbo.sf_grupo_cliente where tipo_grupo = 'C' and inativo_grupo = 0 order by descricao_grupo") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo utf8_encode($RFP['id_grupo']); ?>"><?php echo utf8_encode($RFP['descricao_grupo']); ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div style="width:11%; float:left;margin-left:1%">
                                        <span>Tipo:</span>
                                        <select id="txtTpData">
                                            <option value="1">RECEBIMENTO</option>                                            
                                            <option value="2">VENDA</option>
                                        </select>
                                    </div>
                                    <div style="width:9%; float:left;margin-left:1%">
                                        <span>Data Inicial:</span>
                                        <input type="text" style="width:100%" id="txt_dt_begin" class="datepicker data-mask inputCenter" value="<?php echo getData("T"); ?>" placeholder="Data inicial">
                                    </div>
                                    <div style="width:9%; float:left;margin-left:1%">
                                        <span>Data Final:</span>
                                        <input type="text" style="width:90%" class="datepicker data-mask inputCenter" id="txt_dt_end" value="<?php echo getData("T"); ?>" placeholder="Data Final">
                                    </div>
                                    <div style="margin-top: 15px;float:left;margin-left:0.3%;">
                                        <div style="float:left">
                                            <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext"><?php echo $DescPagina; ?></div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="example">
                            <thead>
                                <tr>
                                    <?php for ($i = 0; $i < count($aColumnsX); $i++) { ?>
                                        <th width="<?php echo $aColumnsP[$i]; ?>%"><?php echo $aColumnsX[$i]; ?></th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Resumo de Caixa</div>
                    </div>
                    <div class="boxtable">
                        <table class="tableResumo" cellpadding="0" cellspacing="0" width="100%" id="example">
                            <tbody>
                                <tr>                                    
                                    <td width="70%">Total Geral</td>
                                    <td width="30%"><span id="lblTotal"><b>0,00</b></span></td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>                    
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/maskedinput/jquery.maskedinput-1.3.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type="text/javascript" src="../../js/util.js"></script>           
        <script type="text/javascript">

            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }

            function finalFind(imp) {
                var retPrint = "";
                retPrint = '?imp=0';
                if (imp === 1) {
                    retPrint = '?imp=1';
                }
                if ($("#txtBanco").val() !== "") {
                    retPrint = retPrint + '&id=' + $("#txtBanco").val();
                }
                if ($("#txtTipoDocumento").val() !== "") {
                    retPrint = retPrint + '&td=' + $("#txtTipoDocumento").val();
                }
                if ($("#txtOperador").val() !== "") {
                    retPrint = retPrint + '&id_operador=' + $("#txtOperador").val();
                }
                if ($("#txtContaMov").val() !== "") {
                    retPrint = retPrint + '&id_grupo=' + $("#txtContaMov").val();
                }
                if ($("#txtTpData").val() !== "") {
                    retPrint = retPrint + '&tpData=' + $("#txtTpData").val();
                }
                if ($('#txt_dt_begin').val() !== "") {
                    retPrint = retPrint + "&dtb=" + $('#txt_dt_begin').val();
                }
                if ($('#txt_dt_end').val() !== "") {
                    retPrint = retPrint + "&dte=" + $('#txt_dt_end').val();
                }                
                return "Fluxo-de-Caixa_server_processing.php" + retPrint.replace(/\//g, "_");
            }
                                    
            function AbrirCli(id) {
                window.open("./../Academia/ClientesForm.php?id=" + id, '_blank');         
            }

            function AbrirBox(id) {
                abrirTelaBox("Transferencia-entre-Bancos.php?id=" + id, 380, 430);                   
            }                        

            function FecharBox() {
                var oTable = $('#example').dataTable();
                oTable.fnDraw(false);
                $("#newbox").remove();
            }

            function imprimir(tipo) {
                var pRel = "&NomeArq=" + "Fluxo de Caixa" +
                        "&lbl=Dt.Baixa|Loja|Banco|Tipo Documento" + (tipo === "E" ? "|Documento Cliente" : "") + "|Fornec.Clientes|Histórico|Referência|Crédito|Débito|Saldo" +
                        "&siz=50|40|50|90" + (tipo === "E" ? "|90" : "") + "|130|85|75|60|60|60" +
                        "&pdf=12" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/Contas-a-pagar/" + finalFind(1).replace("?", "&"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tipo + "&PathArq=GenericModelPDF.php", '_blank');
            }
            
            function listaTable() {
                $('#example').dataTable({
                    "iDisplayLength": -1,
                    "ordering": false,
                    "bFilter": false,
                    "bInfo": false,
                    "sAjaxSource": finalFind(0),
                    "bLengthChange": false,
                    "bPaginate": false,                        
                    "bProcessing": true,
                    "bServerSide": true,
                    "aoColumns": [
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false}],
                    "fnDrawCallback": function (data) {
                        let info = data["aoData"][data["aoData"].length - 2];
                        $("#lblTotal").html(info["_aData"][9]); 
                    }, 'oLanguage': {
                        'sEmptyTable': "Não foi encontrado nenhum resultado",
                        'sLoadingRecords': "Carregando...",
                        'sProcessing': "Processando...",
                        'sZeroRecords': "Não foi encontrado nenhum resultado"}, 
                    "sPaginationType": "full_numbers"
                });
            }

            $(document).ready(function () {
                $('.data-mask').mask('99/99/9999');
                listaTable();
                $("#btnfind").click(function () {
                    $("#example").dataTable().fnDestroy();
                    listaTable();
                });                
            });
        </script>         
        <?php odbc_close($con); ?>
    </body>
</html>