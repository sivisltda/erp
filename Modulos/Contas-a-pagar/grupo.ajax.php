<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include "../../Connections/configini.php";

$id_area = $_REQUEST['txtTipo'];
$local = array();
$sql = "select id_grupo_contas,descricao, (select descricao from sf_centro_custo where id = centro_custo) cc from sf_grupo_contas where inativo = 0 and deb_cre = '" . $id_area . "' ORDER BY descricao";
$cur = odbc_exec($con, $sql) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $local[] = array('id_grupo_contas' => $RFP['id_grupo_contas'], 'descricao' => utf8_encode($RFP['descricao']) . (strlen(utf8_encode($RFP["cc"])) > 0 ? " [" . utf8_encode($RFP["cc"]) . "]" : ""));
}
echo( json_encode($local) );
odbc_close($con);
