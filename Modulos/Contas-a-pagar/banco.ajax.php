<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include "../../Connections/configini.php";

$local = array();
if ($_REQUEST['txtTipo'] == "C") {
    $sql = "select id_bancos,razao_social from sf_bancos where ativo = 0 order by razao_social";
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('id_grupo_contas' => $RFP['id_bancos'], 'descricao' => utf8_encode($RFP['razao_social']));
    }
}
echo( json_encode($local) );
odbc_close($con);
