<?php

include "../../Connections/configini.php";

$FinalUrl = '';
if ($_GET['idx'] != '') {
    $FinalUrl = "id=" . $_GET['idx'];
}
if ($_GET['tpx'] != '') {
    $FinalUrl = $FinalUrl . "&tp=" . $_GET['tpx'];
}
if ($_GET['tdx'] != '') {
    $FinalUrl = $FinalUrl . "&td=" . $_GET['tdx'];
}
if ($_GET['dti'] != '') {
    $FinalUrl = $FinalUrl . "&dti=" . $_GET['dti'];
}
if ($_GET['dtf'] != '') {
    $FinalUrl = $FinalUrl . "&dtf=" . $_GET['dtf'];
}
if ($_POST['txtFinalUrl']) {
    $FinalUrl = $_POST['txtFinalUrl'];
}

$msg = '';
$ValorBaixa = 0;
$Multa = 0;
$Juros = 0;
$TaxaAnt = 0;
$taxa = 0;
$MultaIndice = 0;
$JurosIndice = 0;
$WhereA = '';
$WhereD = '';
$WhereV = '';
$comentarios = '';
$disabled = 'disabled';
$disabledX = 'disabled';
$totChq = 0;
$banco = "";

if (isset($_POST['txtVencimento'])) {
    if (strlen($_POST['txtVencimento']) > 0 && (geraTimestamp($_POST['txtVencimento']) - geraTimestamp(getData("T"))) > 0) {
        $dataVenc = getData("T");
        $msg = '* Data Limite.';
    } else {
        $dataVenc = $_POST['txtVencimento'];
        $msg = '';
    }
} else {
    $dataVenc = getData("T");
    $msg = '';
}

if (isset($_POST['txtBanco'])) {
    $banco = $_POST['txtBanco'];
}

if (isset($_POST['bntSave'])) {
    if ($_POST['txtBanco'] == "null") {
        echo "<script>alert('Selecione um Banco, para efetuar esta operação!');</script>";
    } else {
        $dtVencimento = valoresData("txtVencimento");
        if ($_POST['txtDisabled'] == '') { // Unitário            
            $valorParcela = valoresNumericos('txtValor');
            $valorMulta = valoresNumericos('txtMulta');
            $valorJuros = valoresNumericos('txtJuros');
            $valorTaxaA = (($valorParcela * valoresNumericos('txtTaxaAnt')) / 100);
            if (is_numeric($valorParcela) && $dtVencimento != "null") {
                if ($valorParcela > 0) {
                    $count = explode("|", $_GET['id']);
                    if (count($count) > 0) {
                        $item = explode("-", $count[0]);
                        if (count($item) == 2) {
                            if ($item[0] == 'Y') {
                                $indice = 0;
                                $MultaIndice = 0;
                                $JurosIndice = 0;
                                $TaxaAB = 0;
                                if (valoresNumericos('txtValorBruto') > 0) {
                                    $indice = (($valorParcela * 100) / valoresNumericos('txtValorBruto')) / 100;
                                }
                                $cur = odbc_exec($con, "select multa,juros from sf_bancos where id_bancos = '" . $_POST['txtBanco'] . "'") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) {
                                    $MultaIndice = $RFP['multa'];
                                    $JurosIndice = $RFP['juros'];
                                }
                                $cur = odbc_exec($con, "select * from sf_lancamento_movimento_parcelas lp
                                inner join sf_lancamento_movimento l on l.id_lancamento_movimento = lp.id_lancamento_movimento
                                inner join sf_contas_movimento c on c.id_contas_movimento = l.conta_movimento where l.status = 'Aprovado' and lp.id_parcela in (" . $_POST['txtWhereD'] . "0)") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) {
                                    if ($RFP['valor_pago'] == 0) {
                                        $dias = (int) floor((geraTimestamp($dataVenc) - geraTimestamp(escreverData($RFP['data_vencimento']))) / (60 * 60 * 24));
                                        if ($dias > 0) {
                                            $MultaB = $MultaB + ($RFP['valor_parcela'] * $MultaIndice / 100);
                                            $JurosB = $JurosB + (($RFP['valor_parcela'] * $JurosIndice / 100) * $dias);
                                        }
                                        $TaxaAB = (($RFP['valor_parcela'] * valoresNumericos("txtTaxaAnt")) / 100); 
                                    }                                    
                                    $MultaB = $MultaB * $indice;
                                    $JurosB = $JurosB * $indice;
                                    $TaxaAB = $TaxaAB * $indice;
                                    $ValorP = (($RFP['valor_parcela'] * $indice) + $MultaB + $JurosB - $TaxaAB);
                                    odbc_exec($con, "update sf_cheques set chq_recebido = 1, chq_repasse = " . valoresTexto("txtProprietarioChq") . " where codigo_relacao in (" . $RFP['id_parcela'] . ") and tipo = 'C'");
                                    odbc_exec($con, "set dateformat dmy; update sf_lancamento_movimento_parcelas set obs_baixa = '" . utf8_decode($_POST['txtComentarios']) . "', id_banco = " . $_POST['txtBanco'] . ", valor_pago = " . $ValorP . ", data_pagamento = " . $dtVencimento . ", valor_multa = " . $MultaB . ", valor_juros = " . $JurosB . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela = " . $RFP['id_parcela']) or die(odbc_errormsg());
                                }      
                                if (isset($_POST['txtRecibo']) && $_POST['txtRecibo'] == "1") {
                                    echo "<script>window.open('Relatorio-Recibo.php?idx=" . $_GET['id'] . "&tp=1');</script>";
                                }
                                echo "<script>alert('Conta recebida com sucesso!');parent.FecharBox(1);</script>";
                            } else {                              
                                if ($_POST['txtWhereA'] != "") {
                                    odbc_exec($con, "update sf_cheques set chq_recebido = 1, chq_repasse = " . valoresTexto("txtProprietarioChq") . " where codigo_relacao in (" . $_POST['txtWhereA'] . "0) and tipo = 'S'");
                                    odbc_exec($con, "set dateformat dmy; update sf_solicitacao_autorizacao_parcelas set obs_baixa = '" . utf8_decode($_POST['txtComentarios']) . "', id_banco = " . $_POST['txtBanco'] . ", valor_pago = " . ($valorParcela - $valorTaxaA) . ", data_pagamento = " . $dtVencimento . ", valor_multa = " . $valorMulta . ", valor_juros = " . $valorJuros . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela in (" . $_POST['txtWhereA'] . "0)") or die(odbc_errormsg());
                                } else if ($_POST['txtWhereV'] != "") {
                                    odbc_exec($con, "update sf_cheques set chq_recebido = 1, chq_repasse = " . valoresTexto("txtProprietarioChq") . " where codigo_relacao in (" . $_POST['txtWhereV'] . "0) and tipo = 'V'");
                                    odbc_exec($con, "set dateformat dmy; update sf_venda_parcelas set obs_baixa = '" . utf8_decode($_POST['txtComentarios']) . "', id_banco = " . $_POST['txtBanco'] . ", valor_pago = " . ($valorParcela - $valorTaxaA) . ", data_pagamento = " . $dtVencimento . ", valor_multa = " . $valorMulta . ", valor_juros = " . $valorJuros . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela in (" . $_POST['txtWhereV'] . "0)") or die(odbc_errormsg());                                    
                                } else if ($_POST['txtWhereD'] != "") {
                                    odbc_exec($con, "update sf_cheques set chq_recebido = 1, chq_repasse = " . valoresTexto("txtProprietarioChq") . " where codigo_relacao in (" . $_POST['txtWhereD'] . "0) and tipo = 'C'");
                                    odbc_exec($con, "set dateformat dmy; update sf_lancamento_movimento_parcelas set obs_baixa = '" . utf8_decode($_POST['txtComentarios']) . "', id_banco = " . $_POST['txtBanco'] . ", valor_pago = " . ($valorParcela - $valorTaxaA) . ", data_pagamento = " . $dtVencimento . ", valor_multa = " . $valorMulta . ", valor_juros = " . $valorJuros . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela in (" . $_POST['txtWhereD'] . "0)") or die(odbc_errormsg());                                    
                                }
                                if (isset($_POST['txtRecibo']) && $_POST['txtRecibo'] == "1") {
                                    echo "<script>window.open('Relatorio-Recibo.php?idx=" . $_GET['id'] . "&tp=1');</script>";
                                }
                                echo "<script>alert('Conta recebida com sucesso!');parent.FecharBox(1);</script>";
                            }
                        }
                    }
                }
            }
        } else if ($dtVencimento != "null") {
            $cur = odbc_exec($con, "select *, 
            case when isnull(taxa, 0) > 0 then valor_parcela - ((valor_parcela * isnull(taxa, 0))/100) else valor_parcela end valor
            from sf_solicitacao_autorizacao_parcelas sp 
            inner join sf_solicitacao_autorizacao s on s.id_solicitacao_autorizacao = sp.solicitacao_autorizacao
            inner join sf_contas_movimento c on c.id_contas_movimento = s.conta_movimento 
            left join sf_tipo_documento td on td.id_tipo_documento = sp.tipo_documento
            where sp.id_parcela in (" . $_POST['txtWhereA'] . "0)") or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                $MultaA = 0;
                $JurosA = 0;
                $TaxaAA = 0;
                if ($RFP['valor_pago'] == 0) {
                    $dias = (int) floor((geraTimestamp($dataVenc) - geraTimestamp(escreverData($RFP['data_parcela']))) / (60 * 60 * 24));
                    if ($dias > 0) {
                        $MultaA = $MultaA + ($RFP['valor_parcela'] * $MultaIndice / 100);
                        $JurosA = $JurosA + (($RFP['valor_parcela'] * $JurosIndice / 100) * $dias);
                    }
                    $TaxaAA = (($RFP['valor_parcela'] * valoresNumericos("txtTaxaAnt")) / 100);                    
                }                
                odbc_exec($con, "set dateformat dmy; update sf_solicitacao_autorizacao_parcelas set obs_baixa = '" . utf8_decode($_POST['txtComentarios']) . "', id_banco = " . $_POST['txtBanco'] . ", valor_pago = " . ($RFP['valor'] + $MultaA + $JurosA - $TaxaAA) . ", data_pagamento = " . $dtVencimento . ", valor_multa = " . $MultaA . ", valor_juros = " . $JurosA . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela = " . $RFP['id_parcela']) or die(odbc_errormsg());
            }
            $cur = odbc_exec($con, "select * from sf_venda_parcelas sp
            inner join sf_vendas s on s.id_venda = sp.venda where sp.id_parcela in (" . $_POST['txtWhereV'] . "0)") or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                $MultaV = 0;
                $JurosV = 0;
                $TaxaAV = 0;
                if ($RFP['valor_pago'] == 0) {
                    $dias = (int) floor((geraTimestamp($dataVenc) - geraTimestamp(escreverData($RFP['data_parcela']))) / (60 * 60 * 24));
                    if ($dias > 0) {
                        $MultaV = $MultaV + ($RFP['valor_parcela'] * $MultaIndice / 100);
                        $JurosV = $JurosV + (($RFP['valor_parcela'] * $JurosIndice / 100) * $dias);
                    }
                    $TaxaAV = (($RFP['valor_parcela'] * valoresNumericos("txtTaxaAnt")) / 100);
                }
                odbc_exec($con, "set dateformat dmy; update sf_venda_parcelas set obs_baixa = '" . utf8_decode($_POST['txtComentarios']) . "', id_banco = " . $_POST['txtBanco'] . ",
                valor_pago = ([dbo].[FU_TAXA_FORMA_PAGAMENTO](sf_venda_parcelas.tipo_documento,sf_venda_parcelas.venda, 0, valor_parcela) + " . ($MultaV + $JurosV - $TaxaAV) . "),
                data_pagamento = " . $dtVencimento . ", valor_multa = " . $MultaV . ", valor_juros = " . $JurosV . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela = " . $RFP['id_parcela']) or die(odbc_errormsg());
            }            
            $cur = odbc_exec($con, "select *, 
            case when isnull(taxa, 0) > 0 then valor_parcela - ((valor_parcela * isnull(taxa, 0))/100) else valor_parcela end valor
            from sf_lancamento_movimento_parcelas lp
            inner join sf_lancamento_movimento l on l.id_lancamento_movimento = lp.id_lancamento_movimento
            inner join sf_contas_movimento c on c.id_contas_movimento = l.conta_movimento 
            left join sf_tipo_documento td on td.id_tipo_documento = lp.tipo_documento
            where l.status = 'Aprovado' and lp.id_parcela in (" . $_POST['txtWhereD'] . "0)") or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                $MultaB = 0;
                $JurosB = 0;
                $TaxaAB = 0;
                if ($RFP['valor_pago'] == 0) {
                    $dias = (int) floor((geraTimestamp($dataVenc) - geraTimestamp(escreverData($RFP['data_vencimento']))) / (60 * 60 * 24));
                    if ($dias > 0) {
                        $MultaB = $MultaB + ($RFP['valor_parcela'] * $MultaIndice / 100);
                        $JurosB = $JurosB + (($RFP['valor_parcela'] * $JurosIndice / 100) * $dias);
                    }
                    $TaxaAB = (($RFP['valor_parcela'] * valoresNumericos("txtTaxaAnt")) / 100);
                }
                odbc_exec($con, "set dateformat dmy; update sf_lancamento_movimento_parcelas set obs_baixa = '" . utf8_decode($_POST['txtComentarios']) . "', id_banco = " . $_POST['txtBanco'] . ", valor_pago = " . ($RFP['valor'] + $MultaB + $JurosB - $TaxaAB) . ", data_pagamento = " . $dtVencimento . ", valor_multa = " . $MultaB . ", valor_juros = " . $JurosB . ", syslogin = '" . $_SESSION["login_usuario"] . "' where id_parcela = " . $RFP['id_parcela']) or die(odbc_errormsg());
            }
            odbc_exec($con, "update sf_cheques set chq_recebido = 1, chq_repasse = " . valoresTexto("txtProprietarioChq") . " where codigo_relacao in (" . $_POST['txtWhereA'] . "0) and tipo = 'S'");
            odbc_exec($con, "update sf_cheques set chq_recebido = 1, chq_repasse = " . valoresTexto("txtProprietarioChq") . " where codigo_relacao in (" . $_POST['txtWhereV'] . "0) and tipo = 'V'");
            odbc_exec($con, "update sf_cheques set chq_recebido = 1, chq_repasse = " . valoresTexto("txtProprietarioChq") . " where codigo_relacao in (" . $_POST['txtWhereD'] . "0) and tipo = 'C'");
            if (isset($_POST['txtRecibo']) && $_POST['txtRecibo'] == "1") {
                echo "<script>window.open('Relatorio-Recibo.php?idx=" . $_GET['id'] . "&tp=1');</script>";
            }
            echo "<script>alert('Conta recebida com sucesso!');parent.FecharBox(1);</script>";
        }
    }
}

if ($_GET['id'] != '') {
    if (isset($_POST['txtBanco']) && $_POST['txtBanco'] != "null") {
        $cur = odbc_exec($con, "select multa,juros from sf_bancos where id_bancos = '" . $_POST['txtBanco'] . "'") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $MultaIndice = $RFP['multa'];
            $JurosIndice = $RFP['juros'];
        }
    }
    $count = explode("|", $_GET['id']);
    if (count($count) > 0) {
        if (count($count) == 2) {
            $disabled = '';
            $disabledX = '';
        }
        $i = 0;
        while ($i < count($count) - 1) {
            $item = explode("-", $count[$i]);
            if (count($item) == 2) {
                if ($item[0] == 'A') {
                    $WhereA = $WhereA . $item[1] . ',';
                } else if ($item[0] == 'D') {
                    $WhereD = $WhereD . $item[1] . ',';
                } else if ($item[0] == 'V') {
                    $WhereV = $WhereV . $item[1] . ',';
                } else if ($item[0] == 'Y') {
                    $cur = odbc_exec($con, "select id_parcela from sf_lancamento_movimento_parcelas where id_parcela in
                    (select id_parcela from sf_lancamento_movimento_parcelas where bol_nosso_numero in (
                    select bol_nosso_numero from sf_lancamento_movimento_parcelas where id_parcela = " . $item[1] . "))") or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) {
                        $WhereD = $WhereD . $RFP['id_parcela'] . ',';
                    }
                    $disabledX = 'disabled';
                } else if ($item[0] == 'M') {
                    echo "<script>alert('Não é possível efetuar esta operação!'); parent.FecharBox(0);</script>";                    
                }
            }
            $i++;
        }
    }
    $cur = odbc_exec($con, "select sp.tipo_documento,valor_pago,valor_parcela,data_parcela,bol_id_banco,bol_data_parcela,bol_juros,bol_multa,
    case when isnull(taxa, 0) > 0 then valor_parcela - ((valor_parcela * isnull(taxa, 0))/100) else valor_parcela end valor,
    case when isnull(taxa, 0) > 0 then ((valor_parcela * isnull(taxa, 0))/100) else 0 end taxa        
    from sf_solicitacao_autorizacao_parcelas sp inner join sf_solicitacao_autorizacao s on s.id_solicitacao_autorizacao = sp.solicitacao_autorizacao
    inner join sf_contas_movimento c on c.id_contas_movimento = s.conta_movimento
    left join sf_tipo_documento td on td.id_tipo_documento = sp.tipo_documento
    where sp.id_parcela in (" . $WhereA . "0)") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $data_parcela = $RFP['data_parcela'];
        if (is_numeric($RFP['bol_id_banco']) && !isset($_POST['txtBanco'])) {
            $banco = $RFP['bol_id_banco'];
            $data_parcela = $RFP['bol_data_parcela'];
            $dataVenc = escreverData($RFP['bol_data_parcela']);
            $Multa = $Multa + $RFP['bol_multa'];
            $Juros = $Juros + $RFP['bol_juros'];
            $ValorBaixa = $ValorBaixa + $RFP['valor'];
        } else {
            if ($RFP['valor_pago'] == 0) {
                $dias = (int) floor((geraTimestamp($dataVenc) - geraTimestamp(escreverData($data_parcela))) / (60 * 60 * 24));
                if ($dias > 0) {
                    $Multa = $Multa + ($RFP['valor_parcela'] * $MultaIndice / 100);
                    $Juros = $Juros + (($RFP['valor_parcela'] * $JurosIndice / 100) * $dias);
                }
            }
            $ValorBaixa = $ValorBaixa + $RFP['valor'];
            if ($RFP['tipo_documento'] == 5) {
                $totChq = $totChq + $RFP['valor_parcela'] + $Multa + $Juros;
            }
        }
        $taxa = $taxa + $RFP['taxa'];        
    }
    $cur = odbc_exec($con, "select valor_pago,data_parcela,valor_parcela,sp.tipo_documento,bol_id_banco,bol_data_parcela,bol_juros,bol_multa,
    case when valor_parcela_liquido > 0 then valor_parcela_liquido else valor_parcela end valor,
    case when valor_parcela_liquido > 0 then valor_parcela - valor_parcela_liquido else 0 end taxa
    from sf_venda_parcelas sp inner join sf_vendas s on s.id_venda = sp.venda 
    where sp.id_parcela in (" . $WhereV . "0)") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        if (is_numeric($RFP['bol_id_banco']) && !isset($_POST['txtBanco'])) {
            $banco = $RFP['bol_id_banco'];
            $data_parcela = $RFP['bol_data_parcela'];
            $dataVenc = escreverData($RFP['bol_data_parcela']);
            $Multa = $Multa + $RFP['bol_multa'];
            $Juros = $Juros + $RFP['bol_juros'];
            $ValorBaixa = $ValorBaixa + $RFP['valor'];
        } else {           
            if ($RFP['valor_pago'] == 0 && strlen($dataVenc) > 0) {
                $dias = (int) floor((geraTimestamp($dataVenc) - geraTimestamp(escreverData($RFP['data_parcela']))) / (60 * 60 * 24));
                if ($dias > 0) {
                    $Multa = $Multa + ($RFP['valor_parcela'] * $MultaIndice / 100);
                    $Juros = $Juros + (($RFP['valor_parcela'] * $JurosIndice / 100) * $dias);
                }
            }
            $ValorBaixa = $ValorBaixa + $RFP['valor'];
            if ($RFP['tipo_documento'] == 5) {
                $totChq = $totChq + $RFP['valor_parcela'] + $Multa + $Juros;
            }
        }
        $taxa = $taxa + $RFP['taxa'];
    }    
    $cur = odbc_exec($con, "select lp.tipo_documento,valor_parcela,data_vencimento,valor_pago,bol_id_banco,bol_data_parcela,bol_juros,bol_multa,
    case when isnull(taxa, 0) > 0 then valor_parcela - ((valor_parcela * isnull(taxa, 0))/100) else valor_parcela end valor,
    case when isnull(taxa, 0) > 0 then ((valor_parcela * isnull(taxa, 0))/100) else 0 end taxa        
    from sf_lancamento_movimento_parcelas lp inner join sf_lancamento_movimento l on l.id_lancamento_movimento = lp.id_lancamento_movimento
    inner join sf_contas_movimento c on c.id_contas_movimento = l.conta_movimento
    left join sf_tipo_documento td on td.id_tipo_documento = lp.tipo_documento
    where l.status = 'Aprovado' and lp.id_parcela in (" . $WhereD . "0)") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        if (is_numeric($RFP['bol_id_banco']) && !isset($_POST['txtBanco'])) {
            $banco = $RFP['bol_id_banco'];
            $data_parcela = $RFP['bol_data_parcela'];
            $dataVenc = escreverData($RFP['bol_data_parcela']);
            $Multa = $Multa + $RFP['bol_multa'];
            $Juros = $Juros + $RFP['bol_juros'];
            $ValorBaixa = $ValorBaixa + $RFP['valor'];
        } else {
            if ($RFP['valor_pago'] == 0 && strlen($dataVenc)) {
                $dias = (int) floor((geraTimestamp($dataVenc) - geraTimestamp(escreverData($RFP['data_vencimento']))) / (60 * 60 * 24));
                if ($dias > 0) {
                    $Multa = $Multa + ($RFP['valor_parcela'] * $MultaIndice / 100);
                    $Juros = $Juros + (($RFP['valor_parcela'] * $JurosIndice / 100) * $dias);
                }
            }
            $ValorBaixa = $ValorBaixa + $RFP['valor'];
            if ($RFP['tipo_documento'] == 5) {
                $totChq = $totChq + $RFP['valor_parcela'] + $Multa + $Juros;
            }
        }
        $taxa = $taxa + $RFP['taxa'];
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Pagar Receber</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>        
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>        
    </head>    
    <body>
        <div style="width:400px; margin:0; padding:0">
            <form name="frmForm" action="FormContas-a-Receber.php?id=<?php if ($_GET['id'] != '') { echo $_GET['id'];} echo $FinalUrl;?>" method="POST">
                <div class="data-fluid tabbable" style="margin-top:5px; overflow:hidden">
                    <ul class="nav nav-tabs">
                        <li class="active" style="margin-left:10px"><a href="#tab1" data-toggle="tab">Dados Principais</a></li>
                        <?php if ($totChq > 0) { ?>
                            <li><a href="#tab2" data-toggle="tab">Repassar Cheque</a></li>
                        <?php } ?>
                        <button style="float:right; width:23px; height:23px; margin:5px 10px; padding:0; border:0; background:#333" onClick="parent.FecharBox(0)" id="bntOK">
                            <span class="icon-remove icon-white"></span>
                        </button>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1" style="height:260px">
                            <input name="txtWhereA" id="txtWhereA" value="<?php echo $WhereA; ?>" type="hidden"/>
                            <input name="txtWhereD" id="txtWhereD" value="<?php echo $WhereD; ?>" type="hidden"/>
                            <input name="txtWhereV" id="txtWhereD" value="<?php echo $WhereV; ?>" type="hidden"/>
                            <input name="txtValorLiquido" id="txtValorLiquido" value="<?php echo escreverNumero($ValorBaixa); ?>" type="hidden"/>                            
                            <input name="txtValorBruto" id="txtValorBruto" value="<?php echo escreverNumero($ValorBaixa + $Juros + $Multa); ?>" type="hidden"/>                            
                            <input name="txtDisabled" id="txtDisabled" value="<?php echo $disabled; ?>" type="hidden"/>
                            <input name="txtFinalUrl" id="txtFinalUrl" value="<?php echo $FinalUrl; ?>" type="hidden"/>                            
                            <table width="380" border="0" cellpadding="3" class="textosComuns">
                                <tr>
                                    <td width="120">Selecione o Banco:</td>
                                    <td colspan="2">
                                        <select onchange="document.frmForm.submit();" name="txtBanco">
                                        <option value="null">--Selecione--</option>
                                        <?php $cur = odbc_exec($con, "select id_bancos,razao_social from sf_bancos where ativo = 0 order by razao_social") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['id_bancos'] ?>"<?php
                                            if ($RFP['id_bancos'] == $banco) {
                                                echo "SELECTED";
                                            } ?>><?php echo utf8_encode($RFP['razao_social']) ?></option>
                                        <?php } ?> 
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Informe a Data de Baixa:</td>
                                    <td>
                                        <input style="width:120px" class="inputCenter input-xlarge" id="txtVencimento" name="txtVencimento" type="text" value="<?php echo strlen($dataVenc) > 0 ? $dataVenc : getData("T"); ?>"/>
                                    </td>
                                    <td><font style="color: red"><?php echo $msg; ?></font></td>
                                </tr>
                                <tr>
                                    <td>Valor da Multa/Juros:</td>
                                    <td><input id="txtMulta" style="width:120px" name="txtMulta" type="text" class="input-xlarge inputCenter" onblur="somaGeral()" value="<?php echo escreverNumero($Multa); ?>"/></td>
                                    <td><input id="txtJuros" style="width:120px" name="txtJuros" type="text" class="input-xlarge inputCenter" onblur="somaGeral()" value="<?php echo escreverNumero($Juros); ?>"/></td>
                                </tr>
                                <tr>
                                    <td>Taxa de Antecipação(%):</td>
                                    <td><input id="txtTaxaAnt" style="width:120px" name="txtTaxaAnt" type="text" class="input-xlarge inputCenter" onblur="somaGeral()" value="<?php echo escreverNumero($TaxaAnt); ?>"/></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Valor da Parcela:</td>
                                    <td><input id="txtValor" <?php echo $disabled; ?> style="width:120px" name="txtValor" type="text" class="input-xlarge inputCenter" value="<?php echo escreverNumero(($ValorBaixa + $Multa + $Juros)); ?>"/></td>
                                    <td>
                                        <span style="margin-left: 10px;<?php echo ($taxa == 0 ? "display: none;" : ""); ?>">Taxa: <?php echo escreverNumero($taxa); ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Observação:</td>
                                    <td colspan="2"><textarea name="txtComentarios" id="txtComentarios" style="width:100%" cols="5" rows="3"><?php echo $comentarios; ?></textarea></td>
                                </tr>          
                                <tr>
                                    <td>Gerar Recibo:</td>
                                    <td><input id="txtRecibo" <?php echo $disabled; ?> name="txtRecibo" type="checkbox" value="1"></input></td>
                                    <td></td>
                                </tr>                                 
                            </table>
                        </div>
                        <?php if ($totChq > 0) { ?>
                            <div class="tab-pane" id="tab2" style="height:260px; overflow:hidden">
                                <table width="380" border="0" cellpadding="3" class="textosComuns">                                    
                                    <tr>
                                        <td align="right">Proprietário:</td>
                                        <td colspan="2"><input name="txtProprietarioChq" id="txtProprietarioChq" maxlength="128" type="text" class="input-medium" value=""/></td>                                        
                                    </tr>                                                                   
                                </table>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="toolbar bottom tar" style="margin:10px; margin-top:0px; border-top:solid 1px #CCC; overflow:hidden;">
                    <div class="data-fluid" style='overflow:hidden; padding-bottom:10px;'>
                        <div style="float: left;text-align: left">   
                            &nbsp;Sub Total: <b><?php echo escreverNumero($ValorBaixa, 1); ?></b><br/>    
                            &nbsp;Total em Multa(s): <b><?php echo escreverNumero($Multa, 1); ?></b><br/>
                            &nbsp;Total em Juros(s): <b><?php echo escreverNumero($Juros, 1); ?></b><br/> 
                            &nbsp;Total: <b><?php echo escreverNumero(($ValorBaixa + $Juros + $Multa), 1); ?></b></div>                                                                
                        <div class="btn-group">
                            <br>
                            <button class="btn btn-success" style="margin-bottom:5px" type="submit" name="bntSave" id="bntOK"><span class="ico-checkmark"> &nbsp;</span>Baixar</button><br />
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <script type="text/javascript" src="../../fancyapps/source/jquery.fancybox.js?v=2.1.4"></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type='text/javascript' src='../../js/util.js'></script>        
        <script language="JavaScript">

            $("#txtVencimento").mask(lang["dateMask"]);
            $("#txtMulta, #txtJuros, #txtValor, #txtTaxaAnt, #txtValorChq").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});

            function somaGeral() {
                var inputs = 0;                
                $("#txtValor").attr('readonly', false);                
                inputs = inputs + textToNumber($("#txtValorLiquido").val());
                if(textToNumber($("#txtTaxaAnt").val()) >= 100){
                    $("#txtTaxaAnt").val(numberFormat(0));
                }else if(textToNumber($("#txtTaxaAnt").val()) > 0) {
                    $("#txtValor").attr('readonly', true);
                    inputs = inputs - ((textToNumber($("#txtValorLiquido").val()) * textToNumber($("#txtTaxaAnt").val())) / 100);
                }
                inputs = inputs + textToNumber($("#txtMulta").val());
                inputs = inputs + textToNumber($("#txtJuros").val());                
                $("#txtValor").val(numberFormat(inputs));                
                return;
            }
            
            $("#txtVencimento").datepicker({dateFormat: lang["dPickerFormat"], onSelect: function (date, datepicker) { 
                if (date !== "") { 
                    document.frmForm.submit();
                }
            }});
            
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>