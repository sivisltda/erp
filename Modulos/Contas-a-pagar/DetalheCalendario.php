<?php
include "../../Connections/configini.php";

$toReturn = "";
if ($_GET['idx'] != "") {
    $toReturn = $toReturn . "?id=" . $_GET['idx'];
}
if ($_GET['tp'] != "") {
    $toReturn = $toReturn . "&tp=" . $_GET['tp'];
}
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<title>Sivis Business</title>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
<link rel="stylesheet" type="text/css" href="../../css/main.css"/>
<script type="text/javascript" src="../../js/graficos.js"></script>
<div class="row-fluid">
    <div class="block title">
        <div class="head">
            <div id="topBG" style="margin-bottom:5px;">
                <h2><font color="#FFFFFF">Detalhamento de parcelamento</font>
                    <button style="border: none;float: right;width: 25px;margin-right: 20px;color:#FFFFFF;background-color: #333333;" onClick="parent.FecharBox()" id="bntOK"><b>x</b></button></h2>                                        
            </div>
        </div>
        <div style="clear:both"></div>
        <div style="margin:0 1%">
            <div class="data-fluid" style="height:365px; overflow:hidden; overflow-y:scroll">
                <table width="100%" bordercolor="black" border="1" cellpadding="5" cellspacing="0">           
                    <thead>
                        <tr class="txtPq">
                            <th bgcolor="#e9e9e9">Data</th> 
                            <th bgcolor="#e9e9e9">PA</th>
                            <th bgcolor="#e9e9e9">Nº Doc</th>                        
                            <th bgcolor="#e9e9e9">Nosso Nº</th>                        
                            <th bgcolor="#e9e9e9">Vencimento</th>
                            <th bgcolor="#e9e9e9">Valor Parcela</th>
                            <th bgcolor="#e9e9e9">Valor Pago</th>
                            <th bgcolor="#e9e9e9">Pago Em</th>
                            <th bgcolor="#e9e9e9">Diferença</th>
                            <th bgcolor="#e9e9e9">Em %</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="txtPq">
                            <?php
                            if ($_GET['id'] != '') {
                                $cur = odbc_exec($con, "select * from sf_solicitacao_autorizacao_parcelas where inativo = 0 and solicitacao_autorizacao = " . $_GET['id']);
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                </tr>
                                <tr class="txtPq">
                                    <td><?php echo escreverData($RFP['data_cadastro']); ?></td>                                    
                                    <td><?php echo utf8_encode($RFP['pa']); ?></td>
                                    <td><?php echo utf8_encode($RFP['sa_descricao']); ?></td>
                                    <td><?php echo utf8_encode($RFP['bol_nosso_numero']); ?></td>
                                    <td><?php echo escreverData($RFP['data_parcela']); ?></td>
                                    <td><?php echo escreverNumero($RFP['valor_parcela'], 1); ?></td>
                                    <td><?php echo escreverNumero($RFP['valor_pago'], 1); ?></td>
                                    <td><?php echo escreverData($RFP['data_pagamento']); ?></td>
                                    <?php
                                    if ($RFP['valor_pago'] > 0) {
                                        $total = ($RFP['valor_pago'] - $RFP['valor_parcela']);
                                        echo "<td " . ($total < 0 ? "style='color: red'" : "") . ">" . escreverNumero($total);
                                    } else {
                                        echo "<td> " . escreverNumero(0) . " </td>";
                                    }
                                    if ($RFP['valor_pago'] > 0 && $RFP['valor_parcela'] > 0) {
                                        $total = ($RFP['valor_pago'] * 100 / $RFP['valor_parcela']) - 100;
                                        echo "<td " . ($total < 0 ? "style='color: red'" : "") . ">" . escreverNumero($total);
                                    } else {
                                        echo "<td> " . escreverNumero(0) . " </td>";
                                    }
                                }
                            } ?>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script type='text/javascript' src='../../js/util.js'></script>
<?php odbc_close($con); ?>
</div>