<?php
include "../../Connections/configini.php";

$FinalUrl = '';
if ($_GET['idx'] != '') {
    $FinalUrl = "id=" . $_GET['idx'];
}

if ($_GET['tpx'] != '') {
    $FinalUrl = $FinalUrl . "&tp=" . $_GET['tpx'];
}

if ($_GET['tdx'] != '') {
    $FinalUrl = $FinalUrl . "&td=" . $_GET['tdx'];
}

if ($_GET['dti'] != '') {
    $FinalUrl = $FinalUrl . "&dti=" . $_GET['dti'];
}

if ($_GET['dtf'] != '') {
    $FinalUrl = $FinalUrl . "&dtf=" . $_GET['dtf'];
}

if ($_POST['txtFinalUrl']) {
    $FinalUrl = $_POST['txtFinalUrl'];
}

$ValorBaixa = 0;
$Multa = 0;
$Juros = 0;
$MultaIndice = 0;
$JurosIndice = 0;
$desconto = 0;
$Documento = '';
$WhereA = '';
$WhereD = '';
$WhereV = '';
$comentarios = '';
$nossoNumero = '';
$banco = '';
$disabled = 'disabled';
$dataVenc = '';
$bancoCarteira = 'null';

if ($_GET['id'] != '') {
    if (isset($_POST['txtBanco'])) {
        if ($_POST['txtBanco'] != "null") {
            $cur = odbc_exec($con, "select multa,juros from sf_bancos where id_bancos = '" . $_POST['txtBanco'] . "'") or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                $MultaIndice = $RFP['multa'];
                $JurosIndice = $RFP['juros'];
            }
        }
    }
    $count = explode("|", $_GET['id']);
    if (count($count) > 0) {
        if (count($count) == 2) {
            $disabled = '';
        }
        $i = 0;
        while ($i < count($count) - 1) {
            $item = explode("-", $count[$i]);
            if (count($item) == 2) {
                if ($item[0] == 'A') {
                    $WhereA = $WhereA . $item[1] . ',';
                } else if ($item[0] == 'D') {
                    $WhereD = $WhereD . $item[1] . ',';
                } else if ($item[0] == 'V') {
                    $WhereV = $WhereV . $item[1] . ',';
                } else if ($item[0] == 'Y') {
                    echo "<script>alert('Não é possível gerar boleto de um valor agrupado!'); parent.FecharBox(0);</script>";
                } else if ($item[0] == 'M') {
                    echo "<script>alert('Não é possível efetuar esta operação!'); parent.FecharBox(0);</script>";
                }
            }
            $i++;
        }
    }
    if (isset($_POST['txtVencimento'])) {
        $dataVenc = $_POST['txtVencimento'];
    }
    $cur = odbc_exec($con, "select * from sf_solicitacao_autorizacao_parcelas sp
    inner join sf_solicitacao_autorizacao s on s.id_solicitacao_autorizacao = sp.solicitacao_autorizacao
    inner join sf_contas_movimento c on c.id_contas_movimento = s.conta_movimento
    where sp.id_parcela in (" . $WhereA . "0)") or die(odbc_errormsg());        
    while ($RFP = odbc_fetch_array($cur)) {
        $Documento = $RFP['sa_descricao'];
        $banco = $RFP['bol_id_banco'];
        $nossoNumero = $RFP['bol_nosso_numero'];
        $bancoCarteira = $RFP['carteira_id'];
        $desconto = escreverNumero($RFP['bol_desconto']);
        if ($dataVenc == '') {
            $dataVenc = escreverData($RFP['data_parcela']);
        }
        if ($RFP['valor_pago'] == 0) {
            $dias = (int) floor((geraTimestamp($dataVenc) - geraTimestamp(escreverData($RFP['data_parcela']))) / (60 * 60 * 24));
            if ($dias > 0) {
                $Multa = $Multa + ($RFP['valor_parcela'] * $MultaIndice / 100);
                $Juros = $Juros + (($RFP['valor_parcela'] * $JurosIndice / 100) * $dias);
            }
        }
        $ValorBaixa = $ValorBaixa + $RFP['valor_parcela'];
    }
    $cur = odbc_exec($con, "select * from sf_venda_parcelas sp inner join sf_vendas s on s.id_venda = sp.venda
    where sp.id_parcela in (" . $WhereV . "0)") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $Documento = $RFP['sa_descricao'];
        $banco = $RFP['bol_id_banco'];
        $nossoNumero = $RFP['bol_nosso_numero'];
        $bancoCarteira = $RFP['carteira_id'];
        $desconto = escreverNumero($RFP['bol_desconto']);
        if ($dataVenc == '') {
            $dataVenc = escreverData($RFP['data_parcela']);
        }
        if ($RFP['valor_pago'] == 0) {
            $dias = (int) floor((geraTimestamp($dataVenc) - geraTimestamp(escreverData($RFP['data_parcela']))) / (60 * 60 * 24));
            if ($dias > 0) {
                $Multa = $Multa + ($RFP['valor_parcela'] * $MultaIndice / 100);
                $Juros = $Juros + (($RFP['valor_parcela'] * $JurosIndice / 100) * $dias);
            }
        }
        $ValorBaixa = $ValorBaixa + $RFP['valor_parcela'];
    }
    $cur = odbc_exec($con, "select * from sf_lancamento_movimento_parcelas lp
    inner join sf_lancamento_movimento l on l.id_lancamento_movimento = lp.id_lancamento_movimento
    inner join sf_contas_movimento c on c.id_contas_movimento = l.conta_movimento
    where l.status = 'Aprovado' and lp.id_parcela in (" . $WhereD . "0)") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $Documento = $RFP['sa_descricao'];
        $banco = $RFP['bol_id_banco'];
        $nossoNumero = $RFP['bol_nosso_numero'];
        $bancoCarteira = $RFP['carteira_id'];
        $desconto = escreverNumero($RFP['bol_desconto']);
        if ($dataVenc == '') {
            $dataVenc = escreverData($RFP['data_vencimento']);
        }
        if ($RFP['valor_pago'] == 0) {
            $dias = (int) floor((geraTimestamp($dataVenc) - geraTimestamp(escreverData($RFP['data_vencimento']))) / (60 * 60 * 24));
            if ($dias > 0) {
                $Multa = $Multa + ($RFP['valor_parcela'] * $MultaIndice / 100);
                $Juros = $Juros + (($RFP['valor_parcela'] * $JurosIndice / 100) * $dias);
            }
        }
        $ValorBaixa = $ValorBaixa + $RFP['valor_parcela'];
    }
}

if (isset($_POST['bntSave'])) {
    if ($_POST['txtBanco'] == "null") {
        echo "<script>alert('Selecione um Banco, para efetuar esta operação!');</script>";
    } else {
        $dtVencimento = valoresData("txtVencimento");
        if ($dtVencimento != "null") {
            $desconto = "null";
            $valorParcela = valoresNumericos2($_POST['txtValor']);
            $valorMulta = valoresNumericos2($_POST['txtMulta']);
            $valorJuros = valoresNumericos2($_POST['txtJuros']);            
            if (valoresNumericos2($_POST['txtDesconto']) > 0) {
                $desconto = valoresNumericos2($_POST['txtDesconto']);            
            }
            $nossoNumero = $_POST['txtNossoNumero'];
            $Documento = $_POST['txtDocumento'];
            $bancoCarteira = 'null';
            if (is_numeric($_POST['txtCarteira'])) {
                $bancoCarteira = $_POST['txtCarteira'];
            }
            //echo $_POST['txtComentarios'];
            if (is_numeric($valorParcela)) {
                if ($valorParcela > 0) {
                    odbc_exec($con, "set dateformat dmy; update sf_solicitacao_autorizacao_parcelas set bol_data_parcela = " . $dtVencimento . ", bol_descricao = '" . utf8_decode($_POST['txtComentarios']) . "', bol_id_banco = " . $_POST['txtBanco'] . ", bol_valor = " . $valorParcela . ", bol_data_criacao = getdate(),sa_descricao = '" . $Documento . "', bol_multa = " . $valorMulta . ", bol_juros = " . $valorJuros . ",bol_nosso_numero = " . $nossoNumero . ", carteira_id = " . $bancoCarteira . ", bol_desconto = " . $desconto . " where id_parcela in (" . $_POST['txtWhereA'] . "0)") or die(odbc_errormsg());
                    odbc_exec($con, "set dateformat dmy; update sf_venda_parcelas set bol_data_parcela = " . $dtVencimento . ", bol_descricao = '" . utf8_decode($_POST['txtComentarios']) . "', bol_id_banco = " . $_POST['txtBanco'] . ", bol_valor = " . $valorParcela . ", bol_data_criacao = getdate(),sa_descricao = '" . $Documento . "', bol_multa = " . $valorMulta . ", bol_juros = " . $valorJuros . ",bol_nosso_numero = " . $nossoNumero . ", carteira_id = " . $bancoCarteira . ", bol_desconto = " . $desconto . " where id_parcela in (" . $_POST['txtWhereV'] . "0)") or die(odbc_errormsg());
                    odbc_exec($con, "set dateformat dmy; update sf_lancamento_movimento_parcelas set  bol_data_parcela = " . $dtVencimento . ", bol_descricao = '" . utf8_decode($_POST['txtComentarios']) . "', bol_id_banco = " . $_POST['txtBanco'] . ", bol_valor = " . $valorParcela . ", valor_parcela = " . $valorParcela . ", bol_data_criacao = getdate(),sa_descricao = '" . $Documento . "', bol_multa = " . $valorMulta . ", bol_juros = " . $valorJuros . ",bol_nosso_numero = " . $nossoNumero . ", carteira_id = " . $bancoCarteira . ", bol_desconto = " . $desconto . " where id_parcela in (" . $_POST['txtWhereD'] . "0)") or die(odbc_errormsg());
                    echo "<script>window.top.location.href = 'Contas-a-Receber.php?" . $FinalUrl . "';</script>";
                }
            }
        }
    }
}

if (isset($_POST['txtBanco'])) {
    $banco = $_POST['txtBanco'];
}

if (is_numeric($banco)) {
    $cur = odbc_exec($con, "select isnull(MAX(x.bol_nosso_numero),0) vm, isnull(nosso_numero,0) nn from (
    select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_boleto p on p.bol_id_banco = b.id_bancos
    where id_bancos = '" . $banco . "' union all        
    select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_solicitacao_autorizacao_parcelas p on p.bol_id_banco = b.id_bancos
    where id_bancos = '" . $banco . "' union all
    select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_venda_parcelas p on p.bol_id_banco = b.id_bancos
    where id_bancos = '" . $banco . "' union all
    select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_lancamento_movimento_parcelas l on l.bol_id_banco = b.id_bancos
    where id_bancos = '" . $banco . "') as x group by x.nosso_numero") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        if ($RFP['nn'] > $RFP['vm']) {
            $nossoNumero = $RFP['nn'];
        } else {
            $nossoNumero = bcadd($RFP['vm'], 1);
        }
    }
}

if ($desconto == "null") {
    $desconto = 0;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Pagar Receber</title>
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
    </head>
    <link rel="icon" type="image/ico" href="../../favicon.ico"/>
    <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
    <link href="../../css/main.css" rel="stylesheet"/>   
    <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>    
    <body>
        <div style="width:400px; margin:0; padding:0;">
            <form name="frmForm" action="FormContas-a-Receber-boleta.php?id=<?php echo ($_GET['id'] != '' ? $_GET['id'] : "") . $FinalUrl; ?>" method="POST">
                <div class="row-form">
                    <input name="txtWhereA" id="txtWhereA" value="<?php echo $WhereA; ?>" type="hidden"/>
                    <input name="txtWhereD" id="txtWhereD" value="<?php echo $WhereD; ?>" type="hidden"/>
                    <input name="txtWhereV" id="txtWhereD" value="<?php echo $WhereV; ?>" type="hidden"/>
                    <input name="txtValorLiquido" id="txtValorLiquido" value="<?php echo $ValorBaixa; ?>" type="hidden"/>
                    <input name="txtDisabled" id="txtDisabled" value="<?php echo $disabled; ?>" type="hidden"/>
                    <input name="txtFinalUrl" id="txtFinalUrl" value="<?php echo $FinalUrl; ?>" type="hidden"/>
                    <input name="txtMulta" id="txtMulta" value="<?php echo escreverNumero($Multa); ?>" type="hidden"/>
                    <input name="txtJuros" id="txtJuros" value="<?php echo escreverNumero($Juros); ?>" type="hidden"/>
                </div>
                <div style="padding-left:20px; margin-top:5px">
                    <button style="border:none; float:right; width:25px; margin-right:10px; color:#FFFFFF; background-color:#333333;" onClick="parent.FecharBox(0)" id="bntOK"><b>x</b></button>
                    <div style="float:left; width:100px; height:30px; line-height:30px; margin-right:3px; text-align:right">Selecione o Banco:</div>
                    <div><select onchange="document.frmForm.submit();" name="txtBanco" style="width:221px;">
                            <option value="null">--Selecione--</option>
                            <?php
                            $cur = odbc_exec($con, "select id_bancos,razao_social from sf_bancos where ativo = 0 order by razao_social") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP['id_bancos'] ?>"<?php
                                if ($RFP['id_bancos'] == $_POST['txtBanco']) {
                                    echo "SELECTED";
                                } else {
                                    if ($banco == $RFP['id_bancos']) {
                                        echo "SELECTED";
                                    }
                                }
                                ?>><?php echo utf8_encode($RFP['razao_social']) ?></option>
                        <?php } ?>
                        </select></div>
                </div>
                <div style="padding-left:20px; margin-top:5px">
                    <div style="float:left; width:100px; height:30px; line-height:30px; margin-right:3px; text-align:right">Data do Boleto:</div>
                    <div>
                        <input onchange="document.frmForm.submit();" style="width:100px;" class="input-medium inputCenter datepicker" id="txtVencimento" name="txtVencimento" type="text" value="<?php echo $dataVenc; ?>"/>
                        <select id="txtCarteira" name="txtCarteira" style="width:118px;">
                            <?php
                            if (is_numeric($banco)) {
                                $cur = odbc_exec($con, "select sf_carteiras_id,sf_carteira,sf_carteira_descricao,sf_desconto from sf_bancos_carteiras where sf_carteiras_bancos = " . $banco . " order by sf_carteira") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option dsc="<?php echo $RFP['sf_desconto']; ?>" value="<?php echo $RFP['sf_carteiras_id']; ?>"<?php
                                    if ($bancoCarteira == $RFP['sf_carteiras_id']) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP['sf_carteira_descricao']) ?></option>
                                            <?php
                                        }
                                    } else { ?>
                                <option dsc="0" value="null">--Selecione--</option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div style="padding-left:20px; margin-top:5px">
                    <div style="float:left; width:100px; height:30px; line-height:30px; margin-right:3px; text-align:right">Nº Documento:</div>
                    <div><input id="txtDocumento" <?php echo $disabled; ?> style="width:220px;" name="txtDocumento" type="text" class="input-medium inputCenter" value="<?php echo $Documento; ?>"/></div>
                </div>
                <div style="padding-left:20px; margin-top:5px">
                    <div style="float:left; width:100px; height:30px; line-height:30px; margin-right:3px; text-align:right">Nosso Número:</div>
                    <div><input id="txtNossoNumero" style="width:220px;" name="txtNossoNumero" type="text" class="input-medium inputCenter" value="<?php echo $nossoNumero; ?>"/></div>
                </div>
                <div style="padding-left:20px; margin-top:5px">
                    <div style="float:left; width:100px; height:30px; line-height:30px; margin-right:3px; text-align:right">Valor do Boleto:</div>
                    <div style="float:left;"><input id="txtValor" <?php echo $disabled; ?> style="width:90px;" name="txtValor" type="text" class="input-medium inputCenter" value="<?php echo escreverNumero($ValorBaixa + $Multa + $Juros); ?>"/></div>
                    <div style="float:left; width:57px; height:30px; line-height:30px; margin-right:3px; text-align:right">Desconto:</div>
                    <div><input id="txtDesconto" <?php echo $disabled; ?> style="width:70px;" name="txtDesconto" type="text" class="input-medium inputCenter" value="<?php echo escreverNumero($desconto); ?>"/></div>
                </div>
                <div style="padding-left:20px; margin-top:5px">
                    <div style="float:left; width:100px; height:30px; line-height:30px; margin-right:3px; text-align:right">Histórico:</div>
                    <div><input id="txtHistorico" style="width:220px;" name="txtHistorico" type="text" class="input-medium" value="<?php echo $Historico; ?>"/></div>
                </div>
                <div style="padding-left:20px; margin-top:5px">
                    <div style="float:left; width:100px; height:30px; line-height:30px; margin-right:3px; text-align:right">Observação:</div>
                    <div><textarea name="txtComentarios" id="txtComentarios" style="width:220px;" cols="5" rows="3"><?php echo $comentarios; ?></textarea></div>
                </div>
                <div class="toolbar bottom tar" style="margin:10px; border-top:solid 1px #CCC; overflow:hidden;">
                    <div class="data-fluid" style='overflow:hidden; padding-bottom:10px;'>
                        <div style="float: left;text-align: left">
                            &nbsp;Sub Total: <b><?php echo escreverNumero($ValorBaixa, 1); ?></b><br/>
                            &nbsp;Total em Multa(s): <b><?php echo escreverNumero($Multa, 1); ?></b><br/>
                            &nbsp;Total em Juros(s): <b><?php echo escreverNumero($Juros, 1); ?></b><br/>
                            &nbsp;Total: <b><?php echo escreverNumero(($ValorBaixa + $Juros + $Multa), 1); ?></b></div>
                        <div class="btn-group">
                            <br />
                            <br />
                            <button class="btn btn-success" type="submit" onclick="return validar();" name="bntSave" id="bntOK"  ><span class="ico-checkmark"></span>Boleto</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <script type="text/javascript" src="../../fancyapps/source/jquery.fancybox.js?v=2.1.4"></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>        
        <script type='text/javascript' src='../../js/util.js'></script>        
        <script language="JavaScript">            

            $("#txtVencimento").mask(lang["dateMask"]);
            $("#txtValor, #txtDesconto").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});

            function somaGeral() {
                var inputs = 0;
                inputs = inputs + textToNumber($("#txtValorLiquido").val());
                $("#txtValorLiquido").val(numberFormat(inputs));
                return;
            }

            function descontoGeral() {
                var des = textToNumber($("#txtCarteira option:selected").attr("dsc"));
                if (des > 0) {
                    var total = (textToNumber($("#txtValor").val()) * des) / 100;
                    $("#txtDesconto").val(numberFormat(total));
                } else {
                    $("#txtDesconto").val(numberFormat(0));
                }
            }

            $(document).ready(function () {
                $("#txtValor, #txtCarteira").change(function () {
                    descontoGeral();
                });
            });

            function validar() {
                if ($("#txtVencimento").val() === "") {
                    bootbox.alert("Preencha o campo Venc. Inicial corretamente!");
                    return false;
                }
                if ($.isNumeric($("#txtCarteira").val()) && $("#txtDocumento").val() === "") {
                    bootbox.alert("Preencha o campo N° Documento corretamente!");
                    return false;
                }
                if ($.isNumeric($("#txtCarteira").val()) && $("#txtNossoNumero").val() === "") {
                    bootbox.alert("Preencha o campo Nosso Número corretamente!");
                    return false;
                }
                if ($("#txtValor").val() === "") {
                    bootbox.alert("Preencha o campo Valor da Parcela corretamente!");
                    return false;
                }
                return true;
            }
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>
