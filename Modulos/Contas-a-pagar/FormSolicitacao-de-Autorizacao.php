<?php
include "../../Connections/configini.php";
include 'form/FormSolicitacao-de-Autorizacao-server.php';
$some = "block";
if ($TipoGrupo == "D") {
    $some = "none";
} else {
    $some = "block";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Solicitação de Autorização</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>        
        <style>
            .fancybox-custom .fancybox-skin {
                box-shadow: 0 0 50px #069;
            }
            .inputDireito {
                text-align:right;
            }
            #formulario {
                float: left;
                width: 100%;
            }
            .linha {
                float: left;
                width: 100%;
                display: block;
                border-bottom-width: 1px;
                border-bottom-style: solid;
                border-bottom-color: #f7f7f7;
                background-color: #f7f7f7;
                border-top-width: 1px;
                border-top-style: solid;
                border-top-color: #F7f7f7;
                padding-top: 10px;
                padding-bottom: 10px;
            }
        </style>
        <link href="../../../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1>Financeiro<small>Contas Variáveis</small></h1>
                    </div>
                    <div class="row-fluid">
                        <form action="FormSolicitacao-de-Autorizacao.php?<?php echo $FinalUrl; ?>" method="POST" id="frmEnviaDados">
                            <div class="span12">
                                <div class="block">
                                    <div id="formulario">
                                        <span style="background: none">
                                            <span style="width:10%; display:block; float:left;">
                                                <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                                                <span style="width:100%; display:block;">Loja:<br>
                                                    <select class="select" style="width: 100%;" name="txtEmpresa" id="txtEmpresa" <?php echo $disabled; ?>  >
                                                        <?php $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                                            <option value="<?php echo utf8_encode($RFP['id_filial']); ?>" <?php
                                                            if ($empresa == $RFP['id_filial']) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>><?php echo utf8_encode($RFP['numero_filial']); ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </span>
                                            </span>
                                            <span style="width:13.7%; display:block; float:left; margin-left:1%;">
                                                <span style="width:100%; display:block;">Tipo:<br>
                                                    <select class="select" style="width: 100%;" name="txtTipoGrupo" id="txtTipoGrupo" <?php echo $disabled; ?>  >
                                                        <option >--Selecione--</option>
                                                        <option value="D" <?php
                                                        if ($TipoGrupo == "D") {
                                                            echo "SELECTED";
                                                        }
                                                        ?>>DÉBITO</option>
                                                        <option value="C" <?php
                                                        if ($TipoGrupo == "C") {
                                                            echo "SELECTED";
                                                        }
                                                        ?>>CRÉDITO</option>
                                                    </select>
                                                </span>
                                            </span>
                                            <span style="width:32%; display:block; float:left; margin-left:1%; ">
                                                <span style="width:100%; display:block;">Grupos de Contas:<br>
                                                    <select <?php echo $disabled; ?> name="txtGrupo" class="select" style="width:88%" id="txtGrupo" >
                                                        <option value="">--Selecione--</option>
                                                        <?php
                                                        if ($TipoGrupo == "D" || $TipoGrupo == "C") {
                                                            $where_bloq = '';
                                                            if ($disabled == "disabled") {
                                                                $where_bloq = " and id_grupo_contas  = '" . $grupo . "'";
                                                            }
                                                            $cur = odbc_exec($con, "select id_grupo_contas,descricao, (select descricao from sf_centro_custo where id = centro_custo) cc
                                                            from sf_grupo_contas where inativo = 0 and deb_cre = '" . $TipoGrupo . "' " . $where_bloq . " order by descricao") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                            <option value="<?php echo $RFP['id_grupo_contas'] ?>"<?php
                                                            if (!(strcmp($RFP['id_grupo_contas'], $grupo))) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>><?php echo utf8_encode($RFP['descricao']) . (strlen(utf8_encode($RFP["cc"])) > 0 ? " [" . utf8_encode($RFP["cc"]) . "]" : ""); ?></option>
                                                                    <?php
                                                                }
                                                            } ?>
                                                    </select>
                                                    <button class="button button-blue btn-primary" style="padding: 3px 7px 3px 7px;" type="button" onClick="AbrirBox(1)" <?php
                                                    if (($disabled == "disabled") || ($ckb_fin_con_ == 0)) {
                                                        echo "disabled";
                                                    }
                                                    ?>><span class="icon-plus icon-white"></span></button>
                                                </span>
                                            </span>
                                            <span style="width:32%; display:block; float:left; margin-left:1%;">
                                                <span style="width:100%; display:block;">Subgrupos de Contas:<br>
                                                    <select name="txtConta" class="select" style="width:88%" id="txtConta"  <?php echo $disabled; ?> >
                                                        <option value="">--Selecione--</option>
                                                        <?php
                                                        if ($conta != '') {
                                                            $where_bloq = '';
                                                            if ($disabled == "disabled") {
                                                                $where_bloq = " and id_contas_movimento  = '" . $conta . "'";
                                                            }
                                                            $sql = "select id_contas_movimento,descricao from sf_contas_movimento where inativa = 0 and id_contas_movimento > 0 AND grupo_conta = " . $grupo . " " . $where_bloq . " ORDER BY descricao";
                                                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                            <option value="<?php echo $RFP['id_contas_movimento'] ?>"<?php
                                                            if (!(strcmp($RFP['id_contas_movimento'], $conta))) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                                    <?php
                                                                }
                                                            } ?>
                                                    </select>
                                                    <button class="button button-blue btn-primary" style="padding: 3px 7px 3px 7px;" type="button" onClick="AbrirBox(2)" <?php
                                                    if (($disabled == "disabled") || ($ckb_fin_mov_ == 0)) {
                                                        echo "disabled";
                                                    }
                                                    ?>><span class="icon-plus icon-white"></span></button>
                                                </span>
                                            </span>
                                            <span style="width:8.3%; display:block; float:left; margin-left:1%;">
                                                <span style="width:100%; display:block;">Dt. Cadastro:<br>
                                                    <input id="txtData" name="txtData" type="text" value="<?php echo $data; ?>" class="datepicker input-xlarge inputCenter" <?php echo $disabled; ?>/>
                                                </span>
                                            </span>
                                        </span>
                                        <span style="background: none;">
                                            <span style="width:42%; display:block; float:left; margin-top:5px">
                                                <span style="width:100%; display:block;">
                                                    <span>Cliente/Fornecedor/Funcionário/Transportadora:</span>
                                                    <br>
                                                    <input type="hidden" id="txtFonecedor" name="txtFonecedor" value="<?php echo $fornecedor;?>">                                     
                                                    <input type="text" id="txtFonecedorNome" style="width:90%" value="<?php echo $fornecedorNome;?>" <?php  echo $disabled; ?>/>                                                                                                                                                                  
                                                    <button type="button" class="button button-blue btn-primary" style="padding: 3px 7px 3px 7px;" onClick="AbrirBox(3)" <?php
                                                    if (($disabled == "disabled") || ($ckb_adm_for_ == 0)) {
                                                        echo "disabled";
                                                    }
                                                    ?>><span class="icon-plus icon-white"></span></button>
                                                </span>
                                            </span>
                                            <span style="width:25%; display:block; float:left; margin-left:1%; margin-top:5px">
                                                <span style="width:100%; display:block; ">Tipo Doc.: <br>
                                                    <select class="select" style="width:85.5%;" name="txtTipo" id="txtTipo" <?php echo $disabled; ?>>
                                                        <option value="">--Selecione--</option>
                                                        <?php
                                                        if ($TipoGrupo != '') {
                                                            $where_bloq = '';
                                                            $cur = odbc_exec($con, "select id_tipo_documento,descricao from sf_tipo_documento where (inativo = 0 or id_tipo_documento = '" . $tipo . "') and s_tipo in ('A','" . $TipoGrupo . "') " . $where_bloq . " order by descricao") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                            <option value="<?php echo $RFP['id_tipo_documento'] ?>"<?php
                                                            if (!(strcmp($RFP['id_tipo_documento'], $tipo))) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                                    <?php
                                                                }
                                                            } ?>
                                                    </select>
                                                    <button class="button button-blue btn-primary" style="padding: 3px 7px 3px 7px;" type="button" onClick="AbrirBox(4)" <?php
                                                    if (($disabled == "disabled") || ($ckb_fin_doc_ == 0)) {
                                                        echo "disabled";
                                                    }
                                                    ?>><span class="icon-plus icon-white"></span></button>
                                                </span>
                                            </span>
                                            <span style="width:15%; display:block; float:left; margin-left:1%; margin-top:5px">
                                                <span style="width:100%; display:block;">Destinatário: <span style="color:<?php
                                                    if ($destaprov == 0) {
                                                        echo "#bc9f00;";
                                                    } elseif ($destaprov == 1) {
                                                        echo "#0066cc;";
                                                    } elseif ($destaprov == 2) {
                                                        echo "#f00;";
                                                    }
                                                    ?>"><?php
                                                    if ($destaprov == 0 && empty($destinatario) == false) {
                                                        echo "Aguarda";
                                                    } elseif ($destaprov == 1 && empty($destinatario) == false) {
                                                        echo "Aprovado";
                                                    } elseif ($destaprov == 2 && empty($destinatario) == false) {
                                                        echo "Reprovado";
                                                    }
                                                    ?></span><br>
                                                    <span id="sprytextfield3">
                                                        <select class="input-medium" style="width:100%;" name="txtDestinatario" id="txtDestinatario" <?php echo $disabled; ?>>
                                                            <option value="">--Selecione--</option>
                                                            <?php
                                                            $where_bloq = '';
                                                            if ($disabled == "disabled") {
                                                                $where_bloq = " and login_user  = '" . $destinatario . "'";
                                                            }
                                                            $cur = odbc_exec($con, "select login_user,nome from sf_usuarios where inativo = 0 and login_user != 'Admin' and (master = 0 or master in (select id_permissoes from sf_usuarios_permissoes where fin_asol = 1)) " . $where_bloq . " order by nome") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['login_user'] ?>"<?php
                                                                if (!(strcmp($RFP['login_user'], $destinatario))) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>><?php echo (utf8_encode($RFP['nome']) == "" ? utf8_encode($RFP['login_user']) : formatNameCompact(utf8_encode($RFP['nome']))); ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </span>
                                                </span>
                                            </span>
                                            <span style="width:15%; display:block; float:left; margin-left:1%; margin-top:5px">
                                                <span style="width:100%; display:block;">Destinatário: <span style="color:<?php
                                                    if ($destaprov2 == 0) {
                                                        echo "#bc9f00;";
                                                    } elseif ($destaprov2 == 1) {
                                                        echo "#0066cc;";
                                                    } elseif ($destaprov2 == 2) {
                                                        echo "#f00;";
                                                    }
                                                    ?>"><?php
                                                    if ($destaprov2 == 0 && empty($destinatario2) == false) {
                                                        echo "Aguarda";
                                                    } elseif ($destaprov2 == 1 && empty($destinatario2) == false) {
                                                        echo "Aprovado";
                                                    } elseif ($destaprov2 == 2 && empty($destinatario2) == false) {
                                                        echo "Reprovado";
                                                    }
                                                    ?></span><br>
                                                    <span id="sprytextfield3">
                                                        <select class="input-medium" style="width:100%;" name="txtDestinatario2" id="txtDestinatario2" <?php echo $disabled; ?>>
                                                            <option value="">--Selecione--</option>
                                                            <?php
                                                            $where_bloq = '';
                                                            if ($disabled == "disabled") {
                                                                $where_bloq = " and login_user  = '" . $destinatario2 . "'";
                                                            }
                                                            $cur = odbc_exec($con, "select login_user,nome from sf_usuarios where inativo = 0 and login_user != 'Admin' and (master = 0 or master in (select id_permissoes from sf_usuarios_permissoes where fin_asol = 1)) " . $where_bloq . " order by nome") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['login_user'] ?>"<?php
                                                                if (!(strcmp($RFP['login_user'], $destinatario2))) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>><?php echo (utf8_encode($RFP['nome']) == "" ? utf8_encode($RFP['login_user']) : formatNameCompact(utf8_encode($RFP['nome']))); ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </span>
                                                </span>
                                            </span>
                                        </span>
                                        <span class="linha" style="background:none; border:0px; margin-bottom:5px;">
                                            <span style="width:58%; display:block; float:left;">
                                                <span style="width:100%; display:block;"> Histórico: </span>
                                                <span style="width:100%; display:block;">
                                                    <span id="sprytextfield1">
                                                        <input name="txtHistorico" id="txtHistorico" <?php echo $disabled; ?> type="text" class="input-xlarge" value="<?php echo $historico; ?>"/>
                                                    </span>
                                                </span>
                                            </span>
                                            <span style="width:20%; display:<?php echo $some; ?>; float:left; margin-left:1%;" class="bancos" <?php echo $some; ?>>
                                                <span style="width:100%; display:block;"> Banco: </span>
                                                <span style="width:100%; display:block;">
                                                    <select name="txtBanco" class="select" style="width: 100%;<?php echo ($PegaURL != '' ? "display:none" : "");?>" id="txtBanco" <?php echo $disabled; ?>>
                                                        <option value="null">--Selecione--</option>
                                                        <?php
                                                        if ($TipoGrupo == "C") {
                                                            $cur = odbc_exec($con, "select id_bancos,razao_social from sf_bancos where ativo = 0 order by razao_social") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['id_bancos'] ?>"><?php echo utf8_encode($RFP['razao_social']) ?></option>
                                                                <?php
                                                            }
                                                        } ?>
                                                    </select>
                                                </span>
                                            </span>
                                            <span style="width:20%; display:<?php echo $some; ?>; float:left; margin-left:1%;" class="bancos">
                                                <span style="width:100%; display:block;">Carteiras de Banco: </span>
                                                <span style="width:100%; display:block;">
                                                    <select name="txtBancoCarteira" class="select" style="width: 100%;<?php echo ($PegaURL != '' ? "display:none" : "");?>" id="txtBancoCarteira" <?php echo $disabled; ?>>
                                                        <option value="null">--Selecione--</option>
                                                        <?php
                                                        if ($TipoGrupo == "C") {
                                                            $cur = odbc_exec($con, "select id_bancos,razao_social from sf_bancos where ativo = 0 order by razao_social") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['id_bancos'] ?>"><?php echo utf8_encode($RFP['razao_social']) ?></option>
                                                                <?php
                                                            }
                                                        } ?>
                                                    </select>
                                                </span>
                                            </span>
                                        </span>
                                        <span style="background:none; border:0px; margin-bottom:5px;" id="block3" class="linha">
                                            <span style="width:42%; display:block; float:left;">
                                                <span style="width:100%; display:block;"> Doc. inicial: </span>
                                                <span style="width:100%; display:block;">
                                                    <input class="input-xlarge inputCenter" name="txtParcelaMakeDC" id="txtParcelaMakeDC" <?php echo $disabled; ?> type="text" value="<?php echo $r_documento; ?>"/>
                                                </span>
                                            </span>
                                            <span style="width:10%; display:block; float:left; margin-left:1%;">
                                                <span style="width:100%; display:block;"> Vencimento Inicial: </span>
                                                <span style="width:100%; display:block;" id="sprytextfield2">
                                                    <input class="datepicker input-xlarge inputCenter" name="txtParcelaMakeD" id="txtParcelaMakeD" <?php echo $disabled; ?> type="text" value="<?php echo $r_ven_ini; ?>"/>
                                                </span>
                                            </span>
                                            <span style="width:10%; display:block; float:left; margin-left:1%;">
                                                <span style="width:100%; display:block;"> Qtd. de Parcelas: </span>
                                                <span style="width:100%; display:block;">
                                                    <input name="txtParcelaMakeQ" onBlur="calculaParcela()" type="text" class="input-xlarge" id="spinner" value="<?php echo $r_quantidade; ?>" maxlength="2" <?php echo $disabled; ?>/>
                                                </span>
                                            </span>
                                            <span style="width:15%; display:block; float:left; margin-left:1%;">
                                                <span style="width:100%; display:block;"> Valor Total: </span>
                                                <span style="width:100%; display:block;">
                                                    <input name="txtParcelaMakeT" onBlur="calculaParcela()" id="txtParcelaMakeT" <?php echo $disabled; ?> type="text" class="input-xlarge inputCenter" value="<?php echo $r_valor_tot; ?>"/>
                                                </span>
                                            </span>
                                            <span style="width:15%; display:block; float:left; margin-left:1%;">
                                                <span style="width:100%; display:block;"> Valor da Parcela: </span>
                                                <span style="width:100%; display:block;">
                                                    <input name="txtParcelaMakeV" id="txtParcelaMakeV" <?php echo $disabled; ?> type="text" class="input-xlarge inputCenter" value="<?php echo $r_valor; ?>"/>
                                                </span>
                                            </span>
                                            <span style="width:3%; display:block; float:left; margin-left:1%;margin-top: 15px;">
                                                <span style="width:100%; display:block; text-align:right;">
                                                    <button class="button button-green btn-primary" style="padding: 3px 7px 3px 7px;" <?php echo $disabled; ?> name="bntMkParcelas" title="Gerar" id="bntMkParcelas" type="button" onClick="Preencher()"><span class="icon-plus icon-white"></span></button>
                                                </span>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span5" style="width:48%">
                                    <div class="block">
                                        <div class="boxhead">
                                            <div class="boxtext">Documentos</div>                                                                                        
                                        </div>
                                        <div class="data-fluid">
                                            <table id="tblTable" class="table" cellpadding="0" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th width="29%">Campo:</th>
                                                        <th width="71%">Conteúdo:</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if ($tipo != '') {
                                                        $sql = "select id_tipo_documento_campos,campo,isnull((select conteudo from sf_solicitacao_autorizacao_campos where id_solicitacao_autorizacao = '" . $id . "' and tipo_documento_campos = id_tipo_documento_campos),'') 'conteudo' from sf_tipo_documento_campos where inativo = 0 AND tipo_documento = " . $tipo . " ORDER BY 1";
                                                        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                                            <tr>
                                                                <td><?php echo utf8_encode($RFP['campo']); ?></td>
                                                                <td><input name="<?php echo 'txtCampo' . $RFP['id_tipo_documento_campos']; ?>" id="<?php echo 'txtCampo' . $RFP['id_tipo_documento_campos']; ?>" <?php echo $disabled; ?> type="text" class="input-xlarge" value="<?php echo $RFP['conteudo']; ?>"/></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="block">
                                        <?php if (is_numeric($id)) {
                                            include "../CRM/include/Documentos.php";
                                        } ?>
                                        <div class="boxhead">
                                            <div class="boxtext">Comentários</div>                                            
                                        </div>
                                        <div class="data-fluid">
                                            <table class="table" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td width="100%">
                                                            <label><textarea name="txtComentarios" id="txtComentarios" <?php echo $disabled; ?> cols="45" rows="10"><?php echo $comentarios; ?></textarea></label>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <span style="width:100%;">
                                                Solicitado por:
                                                <input class="input-xlarge" disabled type="text" value="<?php echo $solicitante; ?>"/>
                                            </span>                                            
                                        </div>
                                        <div class="spanBNT2" style="margin-left: 22px;">
                                            <div class="toolbar bottom tar">
                                                <div class="data-fluid">
                                                    <?php if (($status == 'Aguarda') && ($destaprov < 2 && $destaprov2 < 2) && (($_SESSION['login_usuario'] == $destinatario && $destaprov == 0) || ($_SESSION['login_usuario'] == $destinatario2 && $destaprov2 == 0))) { ?>
                                                        <div style="float:left">
                                                            <button class="btn btn-success" type="submit" name="bntAprov" id="bntAprov" ><span class="ico-checkmark"></span> Aprovar</button>
                                                            <button class="btn red" type="submit" name="bntReprov" id="bntReprov"><span class=" ico-remove"></span> Reprovar</button>
                                                        </div>
                                                    <?php } ?>
                                                    <div class="btn-group">
                                                        <?php if ($_GET['idx'] != '' || $_GET['idv'] != '') { ?>
                                                            <button class="btn  btn-warning" type="submit" title="Voltar" name="bntVoltar" id="bntVoltar" value="Voltar"><span class="ico-backward"> </span> Voltar</button>
                                                        <?php } if ($disabled == '') { ?>
                                                            <button class="btn btn-success" type="submit" title="Gravar" onclick="return validar();" name="bntSave" id="bntSave" ><span class="ico-checkmark"></span> Gravar</button>
                                                            <?php if ($_POST['txtId'] == '') { ?>
                                                                <button class="btn btn-success" onClick="history.go(-1)" title="Cancelar" id="bntCancelar"><span class="ico-reply"></span> Cancelar</button>
                                                            <?php } else { ?>
                                                                <button class="btn btn-success" type="submit" name="bntAlterar" title="Cancelar" id="bntAlterar" ><span class="ico-reply"></span> Cancelar</button>
                                                                <?php
                                                            }
                                                        } else { ?>
                                                            <button class="btn  btn-success" type="submit" title="Novo" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                                                            <button class="btn  btn-success" type="submit" name="bntEdit" title="Alterar" id="bntEdit" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                                                            <button class="btn red" type="submit" name="bntDelete" title="Excluir" id="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="span6" style="width:50%; margin-left:2%">
                                    <div class="block">
                                        <div class="boxhead">
                                            <div class="boxtext">Parcelamentos</div>
                                        </div>
                                        <div class="data-fluid">
                                            <div style="overflow:auto;">
                                                <table id="tbParcelas" class="table" cellpadding="0" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%" align="center">Nr:</th>
                                                            <th width="20%"  align="center">Nr Doc:</th>
                                                            <th width="15%"  align="center">Vencimento:</th>
                                                            <th width="20%"  align="right">Valor:
                                                                <button class="button button-green btn-primary" style="padding: unset;width: 25px;height: 25px;float: right; margin-top: 2px;" <?php echo $disabled; ?> title="Reajustar Parcelas em Aberto" type="button" onClick="$('#txtValReajustar').parent().show();$('#source').dialog({modal: true});"><span class="icon-plus icon-white"></span></button>
                                                            </th>
                                                            <th width="38%"  align="right">Forma Pg:</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span4" style="width:100%; background-color:#f2f2f2; margin:0; padding:8px;">
                                        <table id="lblTotParc" name="lblTotParc" class="table" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    Total em parcelas: <strong><?php echo escreverNumero($TotalParcelas, 1); ?></strong>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> 
        <div id="source" class="dialog" title="Reajustar Parcelas em Aberto">            
            <div style="margin-top: 5px;display: none;">
                <label for="txtValReajustar">Valor em %:</label>
                <input id="txtValReajustar" name="txtValReajustar" style="width: 60%;" type="text" value="0"/>
                <button id="bntReajustar" name="bntReajustar" class="btn btn-success" type="button" title="Reajustar" onclick="return resetParcel();"><span class="ico-checkmark"></span> Reajustar</button>
                <span id="txtValReajustarInvalido" style="color: Red;display: none;">Valor Inválido</span>
            </div>            
        </div>        
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>        
        <script type="text/javascript" src="../../js/actions.js"></script>                
        <script type="text/javascript" src="../../js/moment.min.js"></script>        
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script> 
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>        
        <script type='text/javascript' src='../../js/util.js'></script>        
        <script type="text/javascript" src="js/formSolicitacaodeAutorizacao.js"></script>
    </body>
    <?php odbc_close($con); ?>
</html>
