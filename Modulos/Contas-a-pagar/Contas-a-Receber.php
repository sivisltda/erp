<?php
include "../../Connections/configini.php";

$F1 = '';
$F2 = '';
$F3 = '';
$F4 = '';
$DateBegin = '';
$DateEnd = '';
$descricao = "";
$tipo_pag = 0;

$sql = "select adm_mens_acess_soma from sf_configuracao";   
$cur = odbc_exec($con, $sql) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {    
    $tipo_pag = $RFP['adm_mens_acess_soma'];
}

if ($tipo_pag > 0) {
    $DateBegin = getData("B");
    $DateEnd = getData("E");
}

if (is_numeric($_GET['id'])) {
    $F1 = $_GET['id'];
}
if (is_numeric($_GET['tp'])) {
    $F2 = $_GET['tp'];
}
if (is_numeric($_GET['td'])) {
    $F3 = $_GET['td'];
    $sql = "select razao_social,nome_fantasia from sf_fornecedores_despesas where id_fornecedores_despesas > 0 and tipo in ('C') and id_fornecedores_despesas = " . $F3 . " ORDER BY razao_social";
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $descricao = utf8_encode($RFP['razao_social']);
        if ($RFP['nome_fantasia'] != "") {
            $descricao = $descricao . " (" . utf8_encode($RFP['nome_fantasia']) . ")";
        }
    }
}
if (is_numeric($_GET['tpd'])) {
    $F4 = $_GET['tpd'];
}
if (is_numeric(str_replace("_", "", $_GET['dti']))) {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
}
if (is_numeric(str_replace("_", "", $_GET['dtf']))) {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <style>
            #formulario {
                float: left;
                width: 100%;
            }
            #formulario form .linha {
                float: left;
                height: 26px;
                width: 100%;
                display: block;
                border-bottom-width: 1px;
                border-bottom-style: solid;
                border-bottom-color: #f7f7f7;
                background-color: #f7f7f7;
                border-top-width: 1px;
                border-top-style: solid;
                border-top-color: #FFF;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .buttonX {
                padding: 5px 10px 4px 10px;
                margin: 0 0px;
            }
            .mynewtxt {
                float: left;
                margin-left: 5px;
                font-size: 11px;
                margin-top: -4px;
            }
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1>Financeiro<small>Contas a Receber</small></h1>
                    </div>
                    <div class="row-fluid">
                        <form action="Contas-a-Receber.php" method="POST">
                            <div>
                                <div class="block">
                                    <div id="formulario">
                                        <div class="boxfilter">
                                            <?php if ($ckb_creceber_read_ == 0) { ?>
                                            <div style="float: left;">
                                                <button type="button" name="bntBaixaLoad" id="bntBaixaLoad" onclick="AbrirBox(1,0);" title="Baixar" class="button button-green btn-primary buttonX" disabled><span class="icon-ok icon-white"></span></button>
                                                <button type="button" name="bntRenegociar" id="bntRenegociar" onclick="renegociar();" title="Renegociar" class="button button-red btn-primary buttonX" disabled><span class="icon-resize-small icon-white"></span></button>
                                                <?php if ($ckb_exc_creceber_ > 0) { ?>
                                                <button type="button" name="bntExcluir" id="bntExcluir" onClick="postItens('bntExcluir');" style="display: none;" title="Excluir" class="button button-red btn-primary buttonX" disabled value="Excluir"><span class="ico-ban-circle">Excluir</span></button>
                                                <?php } ?>
                                                <button type="button" name="bntInativo" id="bntInativo" onClick="postItens('bntInativo');" title="Cancelar" class="button button-red btn-primary buttonX" disabled value="Cancelar"><span class="ico-ban-circle"> </span></button>
                                                <button type="button" name="bntDesfazer" id="bntDesfazer" onClick="postItens('bntDesfazer');" style="display: none;" title="Desfazer Baixa" class="button button-orange btn-primary buttonX" disabled value="Desfazer"><span class="ico-undo"> </span></button>
                                                <button type="button" name="bntImprimir" id="bntImprimir" onclick="AbrirBox(2,0);" title="Gerar Boleto" class="button button-blue2 btn-primary buttonX" disabled><span class="ico-barcode-3"></span></button>
                                                <button type="button" name="bntImprimirBoleto" id="bntImprimirBoleto" onclick="AbrirBox(3,0);" title="Imprimir Boletos" class="button button-blue2 btn-primary buttonX" disabled><span class="ico-barcode-2"></span></button>                                                        
                                            </div>
                                            <?php } ?>
                                            <div style="float: right;">
                                                <select style="width: 103px;vertical-align: top;" name="jumpMenu" id="jumpMenu">
                                                    <option value="" >Selecione</option>
                                                    <option value="1" <?php
                                                    if ($F1 == 1) {
                                                        echo "selected";
                                                    }
                                                    ?>>A Receber</option>
                                                    <option value="2" <?php
                                                    if ($F1 == 2) {
                                                        echo "selected";
                                                    }
                                                    ?>>Recebidas</option>
                                                    <option value="7" <?php
                                                    if ($F1 == 7) {
                                                        echo "selected";
                                                    }
                                                    ?>>Renegociadas</option>
                                                    <option value="3" <?php
                                                    if ($F1 == 3) {
                                                        echo "selected";
                                                    }
                                                    ?>>Canceladas</option>
                                                </select>
                                                <?php if ($F1 != 5 && $F1 != 6) { ?>
                                                    <select style="width: 120px;vertical-align: top;" name="txtTipoBusca" id="txtTipoBusca">
                                                        <option value="">--Selecione--</option>
                                                        <option value="0" <?php
                                                        if ($F2 == 0) {
                                                            echo "selected";
                                                        }
                                                        ?>>Clientes/Fornecedores</option>
                                                        <option value="6" <?php
                                                        if ($F2 == 6) {
                                                            echo "SELECTED";
                                                        }
                                                        ?>>Centro de Custos</option>                                                        
                                                        <option value="1" <?php
                                                        if ($F2 == 1) {
                                                            echo "selected";
                                                        }
                                                        ?>>Grupo de Conta</option>
                                                        <option value="2" <?php
                                                        if ($F2 == 2) {
                                                            echo "selected";
                                                        }
                                                        ?>>Subgrupo de Conta</option>
                                                        <option value="3" <?php
                                                        if ($F2 == 3) {
                                                            echo "selected";
                                                        }
                                                        ?>>Forma de Pagamento</option>
                                                        <option value="4" <?php
                                                        if ($F2 == 4) {
                                                            echo "selected";
                                                        }
                                                        ?>>Grupo de Pessoas</option>
                                                    </select>
                                                    <input id="txtValue" name="txtValue" type="hidden" value="<?php echo $F3; ?>"/>
                                                    <input style="width: 150px;vertical-align: top;color:#000 !important;" type="text" name="txtDesc" id="txtDesc" class="inputbox" value="<?php echo $descricao; ?>" size="10" />
                                                    <span style="display: none;">
                                                        <select name="itemsGrupo[]" id="mscGrupo" multiple="multiple" class="select" style="min-width:150px; min-height: 29px;" class="input-medium"></select>
                                                    </span>
                                                    <select placeholder="Status do Cliente" name="itemsStatus[]" id="mscStatus" multiple="multiple" class="select" style="min-width:150px; min-height: 29px;" class="input-medium">
                                                        <option value="1">Ativo</option>
                                                        <option value="4">Ativo Credencial</option>
                                                        <option value="14">Ativo Abono</option>
                                                        <?php if ($mdl_seg_ == 0) { ?>
                                                        <option value="5">Ativo Ausente 1</option>
                                                        <option value="6">Ativo Ausente 2</option>
                                                        <option value="7">Ativo Ausente 3</option>
                                                        <option value="8">Ativo Ausente 4</option>
                                                        <option value="9">Ativo Ausente 5</option>
                                                        <?php } ?>
                                                        <option value="12">Ativo Em Aberto</option>
                                                        <option value="2">Suspenso</option>
                                                        <option value="16">Cancelado</option>
                                                        <option value="3">Inativo</option>
                                                        <?php if ($mdl_srs_ == 1) { ?>
                                                        <option value="10">Serasa</option>
                                                        <?php } if ($mdl_clb_ == 1 && $mdl_seg_ == 0) { ?>
                                                        <option value="11">Dependente</option>
                                                        <option value="13">Desligado</option>
                                                        <option value="15">Desligado Em Aberto</option>                                                        
                                                        <?php } ?>                                                            
                                                    </select>
                                                <?php } ?>
                                                <select id="txtTipoData" style="width: 80px;vertical-align: top;">
                                                    <option value="0" <?php
                                                    if ($F4 == "0") {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Dt.Venc.</option>
                                                    <option value="1" <?php
                                                    if ($F4 == "1") {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Dt.Pag.</option>
                                                    <option value="2" <?php
                                                    if ($F4 == "2") {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Dt.Cad.</option>
                                                </select>
                                                <input type="text" id="txt_dt_begin" name="txt_dt_begin" style="width: 75px;vertical-align: top;" class="datepicker" value="<?php echo $DateBegin; ?>" placeholder="Data inicial">
                                                <input type="text" id="txt_dt_end" name="txt_dt_end" style="width: 75px;vertical-align: top;" class="datepicker" value="<?php echo $DateEnd; ?>" placeholder="Data Final">
                                                <button type="button" name="btnfind" class="button button-turquoise btn-primary buttonX" onclick="refreshFind();" title="Buscar"><span class="ico-search icon-white"></span></button>
                                                <button type="button" name="btnPrint" onclick="imprimir('I')" class="button button-blue btn-primary buttonX" title="Imprimir"><span class="ico-print"></span></button>
                                                <button type="button" class="button button-blue btn-primary" onclick="imprimir('E')" title="Exportar Excel"><span class="ico-download-3"></span></button>             
                                                <?php if ($ckb_creceber_read_ == 0) { ?>                                                
                                                <button type="button" id="btnEmail" class="button button-turquoise btn-primary buttonX" title="Enviar E-mail" onclick="AbrirBox(4, 0)" disabled><span class="ico-envelope-3 icon-white"></span></button>
                                                <button type="button" id="btnSms" class="button button-blue btn-primary buttonX" title="Enviar SMS" onclick="AbrirBox(5, 0);" disabled><span class="ico-comment icon-white"></span></button>
                                                <button type="button" id="btnWhatsapp" class="button button-blue btn-primary buttonX" title="Enviar Whatsapp" onclick="AbrirBox(6, 0);" disabled><span class="ico-phone-4 icon-white"></span></button>
                                                <?php } ?>                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="block">
                                        <div class="boxhead">
                                            <div class="boxtext">
                                                <div class="mynewtxt"><input id="ckb_grupoFin" value="1" type="checkbox" class="input-medium"><span style="vertical-align: sub;">Boletos Gerados</span></div>
                                                Contas a Receber
                                            </div>
                                        </div>
                                        <div class="data-fluid">
                                            <table class="table" cellpadding="0" cellspacing="0" width="100%" id="example">
                                                <thead>
                                                    <tr>
                                                        <th id="del_check" width="2%">
                                                            <input id="checkAll" type="checkbox" class="checkall" onchange="selectCheck();"/>
                                                        </th>
                                                        <th width="4%"><div id="formPQ">Dt.Cd</div></th>
                                                        <?php if (($F1 != "5" && $F1 != "6")) { ?>
                                                            <th width="4%"><div id="formPQ">F.Pg</div></th>
                                                        <?php } ?>
                                                        <th width="6%"><div id="formPQ">Nr.Doc</div></th>
                                                        <th width="6%"><div id="formPQ">Ns.Num</div></th>
                                                        <th width="5%"><div id="formPQ">Vecto</div></th>
                                                        <th width="5%"><div id="formPQ">Pagto</div></th>
                                                        <th width="4%"><div id="formPQ">Parc</div></th>
                                                        <th width="13%"><div id="formPQ">Cliente</div></th>
                                                        <?php if (($F1 != "5" && $F1 != "6")) { ?>
                                                            <th width="11%"><div id="formPQ">Histórico:</div></th>
                                                            <th width="4%"><div id="formPQ">Org</div></th>
                                                            <th width="8%"><div id="formPQ">Grupo:</div></th>
                                                            <th width="9%"><div id="formPQ">Contas:</div></th>
                                                        <?php } ?>
                                                        <th style="text-align: left " width="8%"><div id="formPQ">Parc:</div></th>
                                                        <?php if (($F1 != "5" && $F1 != "6")) { ?>
                                                            <th style="text-align: left " width="8%"><div id="formPQ">Baixa:</div></th>
                                                            <th id="del_boleto" width="4%"><div id="formPQ">Doc</div></th>
                                                            <th id="del_aviso" width="4%"><div id="formPQ">Aviso</div></th>
                                                        <?php } ?>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr><td colspan="5" class="dataTables_empty">Carregando dados do Cliente</td></tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <table id="lblTotParc" name="lblTotParc" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td style="text-align: right"><div id="lblTotParc2" name="lblTotParc2">
                                                        Total selecionado :<strong><?php echo escreverNumero(0, 1); ?></strong>
                                                    </div></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right"><div id="lblTotParc3" name="lblTotParc3">
                                                        Total em Parcelas :<strong><?php echo escreverNumero(0, 1); ?></strong></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right"><div id="lblTotParc4" name="lblTotParc4">
                                                        Total Recebido :<strong><?php echo escreverNumero(0, 1); ?></strong></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="widgets"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/highlight/jquery.highlight-4.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>         
        <script type='text/javascript' src='../../js/util.js'></script>
        <script type='text/javascript'>

            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
            formaPagamento();

            function trocarCheque(id) {
                abrirTelaBox("FormCheques.php?tp=0&id=" + id, 325, 460);
            }

            function repassarCheque(id) {
                abrirTelaBox("FormCheques.php?tp=1&id=" + id, 325, 460);
            }

            function detalheCheque(id) {
                abrirTelaBox("FormCheques.php?tp=2&id=" + id, 325, 460);
            }

            function formaPagamento() {
                $('#mscGrupo').empty();
                $("#mscGrupo").select2('data', null);
                $.getJSON("./../CRM/ajax.php", {q: "", t: "FPC"}, function (j) {
                    var options = "";
                    for (var i = 0; i < j.length; i++) {
                        options += "<option value='" + j[i].id + "'>" + j[i].value + "</option>";
                    }
                    $("#mscGrupo").append(options);
                    $('#mscGrupo').trigger('change');
                });
            }
            
            $("#ckb_grupoFin").change(function() {
                refreshFind();
            });

            $('#txtTipoBusca').change(function () {
                $("#s2id_mscGrupo").select2("val", "");
                $('#txtDesc').val("");
                $('#txtValue').val("");
                if ($(this).val() === "3") {
                    $("#s2id_mscGrupo").parent().show();
                    $('#txtDesc').hide();
                } else {
                    $("#s2id_mscGrupo").parent().hide();
                    $('#txtDesc').show();
                }
            });            

            $("#txtDesc").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "./../CRM/ajax.php",
                        dataType: "json",
                        data: {
                            q: request.term,
                            t: typeSelect()
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    $("#txtValue").val(ui.item.id);
                    refreshFind();
                }
            });

            function typeSelect() {
                if ($("#txtTipoBusca").val() === "1") {
                    return "GCC";
                } else if ($("#txtTipoBusca").val() === "2") {
                    return "CMC";
                } else if ($("#txtTipoBusca").val() === "3") {
                    return "FPC";
                } else if ($("#txtTipoBusca").val() === "4") {
                    return "GP";
                } else if ($("#txtTipoBusca").val() === "5") {
                    return "SP";
                } else if ($("#txtTipoBusca").val() === "6") {
                    return "CCU";                    
                } else {
                    return "C','F";
                }
            }

            function refreshFind() {
                $('#bntDesfazer').hide();
                if ($("#jumpMenu").val() === "3") {
                    $('#bntBaixaLoad, #bntInativo, #bntImprimir, #bntImprimirBoleto, #bntRenegociar').hide();
                    $('#bntExcluir').show();
                } else {
                    $('#bntBaixaLoad').show();
                    if ($("#jumpMenu").val() === "1") {
                        $('#bntRenegociar').show();
                    } else {
                        $('#bntRenegociar').hide();
                    }
                    if ($("#jumpMenu").val() === "2") {
                        $('#bntDesfazer').show();
                    }
                    $('#bntExcluir').hide();
                    $('#bntInativo, #bntImprimir, #bntImprimirBoleto').show();
                }
                tbLista.fnReloadAjax(finalFind(0,0));                
            }
            
            var tbLista = $('#example').dataTable({
                "iDisplayLength": 20,
                "aLengthMenu": [20, 50, 500],
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": finalFind(0,0),
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"},
                    'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"},
                 "fnDrawCallback": function (data) {
                    refreshTotal();
                },
                "sPaginationType": "full_numbers"});
            
            function refreshTotal() {
                $("#del_check, #del_boleto, #del_aviso").attr("class", "");
                $('#lblTotParc2').html('Total selecionado :<strong>' + numberFormat(0, 1) + '</strong>').show('slow');
                $('#lblTotParc3').html('Total em Parcelas :<strong>' + numberFormat($('#totparc').val(), 1) + '</strong>').show('slow');
                $('#lblTotParc4').html('Total Recebido :<strong>' + numberFormat($('#totparcpg').val(), 1) + '</strong>').show('slow');
            }

            function finalFind(tp, imp) {
                var retorno = "";
                if ($('#jumpMenu').val() !== "") {
                    retorno = retorno + "&id" + (tp === 1 ? "x" : "") + "=" + $('#jumpMenu').val();
                }
                if ($('#txtTipoBusca').val() !== "") {
                    retorno = retorno + "&tp" + (tp === 1 ? "x" : "") + "=" + $('#txtTipoBusca').val();
                }
                if ($('#txtDesc').val() !== "") {
                    retorno = retorno + "&td" + (tp === 1 ? "x" : "") + "=" + $('#txtValue').val();
                }
                if ($("#mscGrupo").val() !== null) {
                    var grupos = JSON.stringify($("#mscGrupo").val());
                    retorno = retorno + "&grp=" + grupos;
                }
                if ($("#mscStatus").val() !== null) {
                    var grupos = JSON.stringify($("#mscStatus").val());
                    retorno = retorno + "&sts=" + grupos;
                }
                if ($('#txtTipoData').val() !== "") {
                    retorno = retorno + "&tpd=" + $('#txtTipoData').val();
                }
                if ($('#txt_dt_begin').val() !== "") {
                    retorno = retorno + "&dti=" + $('#txt_dt_begin').val();
                }
                if ($('#txt_dt_end').val() !== "") {
                    retorno = retorno + "&dtf=" + $('#txt_dt_end').val();
                }
                if ($('#ckb_grupoFin').is(':checked')) {
                    retorno = retorno + "&ibl=1";
                }                
                if (tp === 0) {
                    return "Contas-a-Receber_server_processing.php?filial=" + $("#txtLojaSel").val() + "&imp="  + imp + retorno.replace(/\//g, "_");
                } else {
                    return retorno.replace(/\//g, "_");
                }
            }  
            
            function renegociar() {
                $.getJSON("renegociar.ajax.php", {txtId: $(".caixa:checked").attr("id"), ajax: "true"}, function (j) {
                    window.location = "" + j[0].plugue + finalFind(1,0);
                });
            }

            function imprimir(tp) {
                var dtParam = $("#example").dataTable().fnSettings().aaSorting[0]; 
                var base = ($('#jumpMenu').val() === "5" || $('#jumpMenu').val() === "6") ? 0 : 1;                
                var pRel = "&NomeArq=" + "Contas a Receber" +
                        "&lbl=" + "Dt.Cd" + (base === 0 ? "" : "|F.Pg") + "|Nr.Doc|Ns.Num|Vecto|Pagto|Parc" + (tp === 'E' ? "|Matricula" : "") + "|Cliente" + (base === 0 ? "" : "|Histórico|Org|Grupo|Contas") + "|Parc|Baixa" + (tp === 'E' ? "|Consultor|Celular|E-mail" : "") +
                        "&pOri=" + (base === 0 ? "P" : "L") + //parametro de paisagem
                        "&siz=" + "50" + (base === 0 ? "" : "|30") + "|50|50|50|50|30" + (tp === 'E' ? "|40" : "") + "|" + (base === 0 ? "300" : "190|110|40|110|130") + "|60|60" + (tp === 'E' ? "|100|100|100" : "") +
                        "&pdf=" + "18" + // Colunas do server processing que não irão aparecer no pdf 9
                        "&filter=" + "Contas a Receber " + //Label que irá aparecer os parametros de filtro
                        "&iSortCol_0=" + dtParam[2] + "&iSortingCols=16&sSortDir_" + dtParam[0] + "=" + dtParam[1] + "&iSortCol_" + dtParam[0] + "=" + dtParam[0] + "&bSortable_" + dtParam[0] + "=true" +
                        "&PathArqInclude=../Modulos/Contas-a-pagar/" + finalFind(0,1).replace("?", "&"); // server processing            
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tp + "&PathArq=GenericModelPDF.php", '_blank');
            }

            function listaEmails() {
                var contatos = [];
                $.each($(".caixa:checked"), function(index, value) {
                    if ($(value).attr("email").length > 0) {
                        contatos.push($(value).attr("email") + "|" + $(value).attr("cliente") + "||" + $(value).attr("id"));
                    }
                });
                return contatos;
            }

            function listaSMS() {
                var contatos = [];
                $.each($(".caixa:checked"), function(index, value) {
                    if ($(value).attr("celular").length > 0) {
                        contatos.push($(value).attr("celular") + "|" + $(value).attr("cliente") + "||" + $(value).attr("id"));
                    }
                });
                return contatos;
            }
            
            function AbrirBox(opc, id) {
                if (id > 0 && (opc === "C" || opc === "P")) {
                    var myId = "?id=" + id;
                } else {
                    var myId = "";
                }
                if (opc === 1) {
                    $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:400px; height:390px; position:absolute; top:50%; left:50%; margin-left:-200px; margin-top:-205px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:390px' src='FormContas-a-Receber.php?id=" + getSelectCheck() + finalFind(1,0) + "'></iframe></div></div></div></div></div>");
                }
                if (opc === 2) {
                    $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:400px; height:350px; position:absolute; top:50%; left:50%; margin-left:-200px; margin-top:-175px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:350px' src='FormContas-a-Receber-boleta.php?id=" + getSelectCheck() + finalFind(1,0) + "'></iframe></div></div></div></div></div>");
                }
                if (opc === 3) {
                    window.open("./../../Boletos/Boleto.php?id=" + getSelectCheck() + "&crt=" + $("#txtMnContrato").val(), "_blank");
                }
                if (opc === 4) {
                    abrirTelaBox("../CRM/EmailsEnvioForm.php?bol=S", 650, 720);
                }
                if (opc === 5) {
                    abrirTelaBox("../CRM/SMSEnvioForm.php?bol=S", 400, 400);
                }
                if (opc === 6) {
                    abrirTelaBox("../CRM/SMSEnvioForm.php?bol=S&zap=s", 400, 400);
                }                
                if (opc === "C") { 
                    window.open("<?php echo ($mdl_aca_ == 0 ? "./../CRM/CRMForm.php?id=" : "./../Academia/ClientesForm.php?id=");?>" + id, '_blank');         
                }
                if (opc === "P") {
                    $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:717px; height:493px; position:absolute; top:50%; left:50%; margin-left:-358px; margin-top:-282px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:493px' src='./../CRM/FormGerenciador-Prospects.php" + myId + "'></iframe></div></div></div></div></div>");
                }
            }
            
            function FecharBox(opc) {
                $("#newbox").remove();
                if (opc === 0) {
                    refreshFind();
                } else if (opc === 1) {
                    var oTable = $('#example').dataTable();
                    oTable.fnDraw(false);
                    $("#bntRenegociar, #bntInativo, #bntDesfazer, #bntImprimir, #bntBaixaLoad, #bntImprimirBoleto, #bntExcluir, #btnEmail, #btnSms, #btnWhatsapp").prop("disabled", true);
                }
            }
            
            function selectCheck() {
                let totSel = 0;
                $.each($(".caixa:checked"), function(index, value) {
                    totSel += textToNumber($(value).val());
                });
                $('#lblTotParc2').html('Total selecionado: <strong>' + numberFormat(totSel) + '</strong>');
                $("#bntRenegociar, #bntInativo, #bntDesfazer, #bntImprimir, #bntBaixaLoad, #bntImprimirBoleto, #bntExcluir, #btnEmail, #btnSms, #btnWhatsapp").prop("disabled", true);
                if ($(".caixa:checked").length > 0) {
                    $("#bntInativo, #bntDesfazer, #bntImprimir, #bntBaixaLoad, #bntImprimirBoleto, #bntExcluir, #btnEmail, #btnSms, #btnWhatsapp").prop("disabled", false);
                }
                if ($(".caixa:checked").length === 1) {
                    $("#bntRenegociar").prop("disabled", false);
                }
            }      
            
            function getSelectCheck() {
                let totSel = "";
                $.each($(".caixa:checked"), function(index, value) {
                    totSel += $(value).attr("id") + "|";
                });
                return totSel;
            }
            
            function postItens(tipo) {
                bootbox.confirm('Confirma a ação de ' + $("#" + tipo).attr("title") + ' ?', function (result) {
                    if (result === true) {
                        let send = tipo + "=S&";
                        $.each($(".caixa:checked"), function(index, value) {
                            send += "items[]=" + $(value).attr("id") + "&";
                        });
                        $.post("./form/FormContas-a-Receber.php", send.slice(0,-1)).done(function (data) {
                            if (data.trim() === 'YES') {
                                refreshFind();
                            }
                        });
                    }
                });
            }
            
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>
