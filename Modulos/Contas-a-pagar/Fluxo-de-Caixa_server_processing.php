<?php

require_once(__DIR__ . '/../../Connections/configini.php');

$sWhere = "";
$sQtd = 0;
$sLimit = 20;
$imprimir = 0;
$id_banco = "0";
$DateBegin = getData("T");
$DateEnd = getData("T");
$tpData = "2";
$id_operador = "";
$comando = '0';
$comando2 = '0';
$queryOperadorL = "";
$queryOperadorS = "";
$queryOperadorV = "";
$filial = '';

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}
if (is_numeric($_GET['imp']) && $_GET['imp'] == 1) {
    $imprimir = 1;
}
if (is_numeric($_GET['id'])) {
    $id_banco = $_GET['id'];
}
if (is_numeric($_GET['tpData'])) {
    $tpData = $_GET['tpData'];
}
if ($_GET['dtb'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dtb']);
}
if ($_GET['dte'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dte']);
}
if ($_GET['id_operador'] != '') {
    $id_operador = str_replace("_", "/", $_GET['id_operador']);
}

$queryDataL = ($tpData == "1") ? "data_pagamento" : "data_pagamento";
$queryDataV = ($tpData == "1") ? "data_pagamento" : "data_venda";
$queryDataS = ($tpData == "1") ? "data_pagamento" : "data_lanc";
if ($id_operador !== "") {
    $queryOperadorL .= " and UPPER(syslogin) = UPPER('" . $id_operador . "')";
    $queryOperadorS .= " and UPPER(sys_login) = UPPER('" . $id_operador . "')";
    $queryOperadorV .= " and UPPER(vendedor) = UPPER('" . $id_operador . "')";
}
if ($tpData == "1") {
    $queryOperadorL .= " and td.id_tipo_documento not in (12)";
    $queryOperadorS .= " and td.id_tipo_documento not in (12)";
    $queryOperadorV .= " and td.id_tipo_documento not in (12)";
} else if ($tpData == "2") {
    $queryOperadorL .= " and somar_rel = 1";
    $queryOperadorS .= " and somar_rel = 1";
    $queryOperadorV .= " and somar_rel = 1";
}
if ($_GET['td'] != 'null') {
    $queryOperadorL .= " and td.id_tipo_documento in (" . $_GET['td'] . ")";
    $queryOperadorS .= " and td.id_tipo_documento in (" . $_GET['td'] . ")";
    $queryOperadorV .= " and td.id_tipo_documento in (" . $_GET['td'] . ")";   
}
if (is_numeric($_GET['id_grupo'])) {
    $queryOperadorL .= " and sf_fornecedores_despesas.grupo_pessoa in (" . $_GET['id_grupo'] . ")";
    $queryOperadorS .= " and sf_fornecedores_despesas.grupo_pessoa in (" . $_GET['id_grupo'] . ")";
    $queryOperadorV .= " and sf_fornecedores_despesas.grupo_pessoa in (" . $_GET['id_grupo'] . ")";   
}

if (is_numeric($id_banco)) {
    if ($id_banco > 0) {
        $comando = $id_banco;
    } else {
        $cur = odbc_exec($con, "select id_bancos from sf_bancos") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $comando .= "," . $RFP['id_bancos'];
        }
    }
}
$comandoVenda = " and id_banco in(" . $comando . ") ";
if ($filial == '') {
    $cur = odbc_exec($con, "select TOP 1 id_filial_f from sf_usuarios_filiais A 
    inner join sf_usuarios B on (A.id_usuario_f = B.id_usuario and A.id_filial_f = B.filial)        
    where id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
    while ($RFP = odbc_fetch_array($cur)) {
        $filial = $RFP['id_filial_f'];
    }
}
if (is_numeric($filial)) {
    if ($filial > 0) {
        $comando2 = $filial;
    } else {
        $query = "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'";
        $cur = odbc_exec($con, $query) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $comando2 .= "," . $RFP['id_filial'];
        }
    }
}
//------------------------------------------------------------------------------
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => 0,
    "iTotalDisplayRecords" => 0,
    "aaData" => array()
);
//------------------------------------------------------------------------------

$queryTransfD = "set dateformat dmy; select isnull(sum(valor_trans),0) total from sf_transferencia_banco
where data_trans < " . valoresDataHora2($DateBegin, "00:00:00") . " and banco_origem_trans in (" . $comando . ") and sf_transferencia_banco.empresa in (" . $comando2 . ")";

$queryTransfC = "set dateformat dmy; select isnull(sum(valor_trans),0) total from sf_transferencia_banco 
where data_trans < " . valoresDataHora2($DateBegin, "00:00:00") . " and banco_destino_trans in (" . $comando . ") and sf_transferencia_banco.empresa in (" . $comando2 . ")";

$queryLancamentos = "set dateformat dmy; select isnull(sum(ROUND(case when 1 = " . $tpData . " then valor_pago else valor_parcela end, 2)),0) total from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento
on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas
inner join sf_tipo_documento td on sf_lancamento_movimento_parcelas.tipo_documento = td.id_tipo_documento
where sf_lancamento_movimento.status = 'Aprovado' and " . $queryDataL . " is not null AND sf_lancamento_movimento.empresa in (" . $comando2 . ") " . $queryOperadorL .
        ($tpData == "2" ? " and sf_lancamento_movimento_parcelas.somar_rel = 1" : "") . " AND sf_lancamento_movimento_parcelas.inativo = '0'
and " . $queryDataL . " < " . valoresDataHora2($DateBegin, "00:00:00") . " and id_banco in (" . $comando . ") and sf_contas_movimento.tipo = '";

$querySolicitacao = "select isnull(sum(ROUND(case when 1 = " . $tpData . " then valor_pago else valor_parcela end, 2)),0) total from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas
inner join sf_tipo_documento td on sf_solicitacao_autorizacao_parcelas.tipo_documento = td.id_tipo_documento
where sf_solicitacao_autorizacao.status = 'Aprovado' AND sf_solicitacao_autorizacao.empresa in (" . $comando2 . ") and " . $queryDataS . " is not null " . $queryOperadorS . " AND sf_solicitacao_autorizacao_parcelas.inativo = '0'
and " . $queryDataS . " < " . valoresDataHora2($DateBegin, "00:00:00") . " and (id_banco in (" . $comando . ") or id_banco is null) " . ($tpData == "2" ? " and sf_solicitacao_autorizacao_parcelas.somar_rel = 1" : "") . " and sf_contas_movimento.tipo = '";

$queryVendas = "select isnull(sum(ROUND(case when 1 = " . $tpData . " then valor_pago else valor_parcela end, 2)),0) total from sf_venda_parcelas inner join sf_vendas
on sf_venda_parcelas.venda = sf_vendas.id_venda
inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas
inner join sf_tipo_documento td on sf_venda_parcelas.tipo_documento = td.id_tipo_documento
where sf_vendas.status = 'Aprovado' AND sf_vendas.empresa in (" . $comando2 . ") and " . $queryDataV . " is not null " . $queryOperadorV . " AND sf_venda_parcelas.inativo in (0,2)
and " . $queryDataV . " < " . valoresDataHora2($DateBegin, "00:00:00") . " " . $comandoVenda . ($tpData == "2" ? " and sf_venda_parcelas.somar_rel = 1" : "") . " and sf_vendas.cov = '";

$totalCredito = 0;
$totalDebito = 0;
$cur = odbc_exec($con, $queryLancamentos . "C'");
while ($RFP = odbc_fetch_array($cur)) {
    $totalCredito = $RFP['total'];
}
$cur = odbc_exec($con, $querySolicitacao . "C'");
while ($RFP = odbc_fetch_array($cur)) {
    $totalCredito = $totalCredito + $RFP['total'];
}
$cur = odbc_exec($con, $queryVendas . "V'");
while ($RFP = odbc_fetch_array($cur)) {
    $totalCredito = $totalCredito + $RFP['total'];
}
$cur = odbc_exec($con, $queryTransfC);
while ($RFP = odbc_fetch_array($cur)) {
    $totalCredito = $totalCredito + $RFP['total'];
}
$cur = odbc_exec($con, $queryLancamentos . "D'");
while ($RFP = odbc_fetch_array($cur)) {
    $totalDebito = $RFP['total'];
}
$cur = odbc_exec($con, $querySolicitacao . "D'");
while ($RFP = odbc_fetch_array($cur)) {
    $totalDebito = $totalDebito + $RFP['total'];
}
$cur = odbc_exec($con, $queryVendas . "C'");
while ($RFP = odbc_fetch_array($cur)) {
    $totalDebito = $totalDebito + $RFP['total'];
}
$cur = odbc_exec($con, $queryTransfD);
while ($RFP = odbc_fetch_array($cur)) {
    $totalDebito = $totalDebito + $RFP['total'];
}
//------------------------------------------------------------------------------
$SubTotal = $totalCredito - $totalDebito;
$SubTotalC = 0;
$SubTotalD = 0;
$SubCredito = 0;
$row = array();
$row[] = "";
$row[] = "";
$row[] = "";
$row[] = "";
if (isset($_GET['tpImp']) && $_GET['tpImp'] == "E") { 
    $row[] = "";    
}
$row[] = "";
$row[] = "<span style=\"color:#0066cc\"> => SALDO ANTERIOR</span>";
$row[] = "";
$row[] = "";
$row[] = "";
$row[] = "<span style=\"color:#0066cc\">" . escreverNumero($SubTotal) . "</span>";
$output['aaData'][] = $row;
//------------------------------------------------------------------------------
$sQuery1 = "set dateformat dmy;                                                
select id_trans id_parcela,data_trans data_pagamento,sf_transferencia_banco.empresa,sf_bancos2.razao_social banco,'TRANSFERENCIA' tipo_documento,0 id_fornecedores_despesas, 
'' cnpj, sf_bancos.razao_social,descricao_trans historico_baixa,
valor_trans valor_pago,'C' tipo, id_trans, '1' somar_rel,'TRANSFERENCIA ENTRE BANCOS' descricao 
from sf_transferencia_banco inner join dbo.sf_bancos on banco_origem_trans = id_bancos
inner join sf_bancos sf_bancos2 on banco_destino_trans = sf_bancos2.id_bancos 
where data_trans between " . valoresDataHora2($DateBegin, "00:00:00") . " and " . valoresDataHora2($DateEnd, "23:59:59") . " and banco_destino_trans in (" . $comando . ") and sf_transferencia_banco.empresa in (" . $comando2 . ") 
union all
select id_trans id_parcela,data_trans data_pagamento,sf_transferencia_banco.empresa,sf_bancos2.razao_social banco,'TRANSFERENCIA' tipo_documento,0 id_fornecedores_despesas, 
'' cnpj, sf_bancos.razao_social,descricao_trans historico_baixa,
valor_trans valor_pago,'D' tipo, id_trans, '1' somar_rel,'TRANSFERENCIA ENTRE BANCOS' descricao 
from sf_transferencia_banco inner join dbo.sf_bancos on banco_destino_trans = id_bancos
inner join sf_bancos sf_bancos2 on banco_origem_trans = sf_bancos2.id_bancos 
where data_trans between " . valoresDataHora2($DateBegin, "00:00:00") . " and " . valoresDataHora2($DateEnd, "23:59:59") . " and banco_origem_trans in (" . $comando . ") and sf_transferencia_banco.empresa in (" . $comando2 . ") 
union all                                                                
select id_parcela," . $queryDataL . ",numero_filial empresa,sf_bancos.razao_social banco,TD.descricao,sf_fornecedores_despesas.id_fornecedores_despesas id_fornecedores_despesas, 
sf_fornecedores_despesas.cnpj, sf_fornecedores_despesas.razao_social,sf_contas_movimento.descricao,
case when 1 = " . $tpData . " then valor_pago else valor_parcela end valor_pago,
sf_contas_movimento.tipo tipo, 0 id_trans, sf_lancamento_movimento_parcelas.somar_rel,historico_baixa 
from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento
on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas
inner join sf_bancos on sf_bancos.id_bancos = sf_lancamento_movimento_parcelas.id_banco
left join sf_filiais on sf_filiais.id_filial = sf_lancamento_movimento.empresa
inner join sf_tipo_documento TD on sf_lancamento_movimento_parcelas.tipo_documento = TD.id_tipo_documento
where sf_lancamento_movimento.status = 'Aprovado' and " . $queryDataL . " is not null AND sf_lancamento_movimento.empresa in (" . $comando2 . ") AND sf_lancamento_movimento_parcelas.inativo = '0'
and " . $queryDataL . " between " . valoresDataHora2($DateBegin, "00:00:00") . " and " . valoresDataHora2($DateEnd, "23:59:59") . " and id_banco in (" . $comando . ") " . $queryOperadorL . "
union all
select id_parcela," . $queryDataV . ",numero_filial empresa,sf_bancos.razao_social banco,TD.descricao,sf_fornecedores_despesas.id_fornecedores_despesas id_fornecedores_despesas,
sf_fornecedores_despesas.cnpj, sf_fornecedores_despesas.razao_social,historico_baixa,                                                           
case when 1 = " . $tpData . " then valor_pago else valor_parcela end valor_pago,
CASE sf_vendas.cov WHEN 'V' THEN ('C')WHEN 'C' THEN ('D') end tipo, 0 id_trans, sf_venda_parcelas.somar_rel,                                                            
(STUFF((SELECT distinct ',' + descricao FROM sf_vendas_itens vi inner join sf_produtos p on p.conta_produto = vi.produto WHERE vi.id_venda = sf_vendas.id_venda FOR XML PATH('')), 1, 1, '')) descricao2
from sf_venda_parcelas inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda
inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas 
left join sf_bancos on sf_bancos.id_bancos = sf_venda_parcelas.id_banco
left join sf_filiais on sf_filiais.id_filial = sf_vendas.empresa
inner join sf_tipo_documento TD on sf_venda_parcelas.tipo_documento = TD.id_tipo_documento
where sf_vendas.cov  in('V','C') and sf_vendas.status = 'Aprovado' AND sf_vendas.empresa in (" . $comando2 . ") and " . $queryDataV . " is not null AND sf_venda_parcelas.inativo in (0,2)
and " . $queryDataV . " between " . valoresDataHora2($DateBegin, "00:00:00") . " and " . valoresDataHora2($DateEnd, "23:59:59") . " " . $comandoVenda . $queryOperadorV . "
union all
select id_parcela," . $queryDataS . ",numero_filial empresa,sf_bancos.razao_social banco,TD.descricao,sf_fornecedores_despesas.id_fornecedores_despesas id_fornecedores_despesas,
sf_fornecedores_despesas.cnpj, sf_fornecedores_despesas.razao_social,cast(historico as varchar(200)) historico_baixa,
case when 1 = " . $tpData . " then valor_pago else valor_parcela end valor_pago,
sf_contas_movimento.tipo tipo, 0 id_trans, sf_solicitacao_autorizacao_parcelas.somar_rel,sf_contas_movimento.descricao 
from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas 
left join sf_bancos on sf_bancos.id_bancos = sf_solicitacao_autorizacao_parcelas.id_banco
left join sf_filiais on sf_filiais.id_filial = sf_solicitacao_autorizacao.empresa
inner join sf_tipo_documento TD on sf_solicitacao_autorizacao.tipo_documento = TD.id_tipo_documento
where sf_solicitacao_autorizacao.status = 'Aprovado' AND sf_solicitacao_autorizacao.empresa in (" . $comando2 . ") and " . $queryDataS . " is not null AND sf_solicitacao_autorizacao_parcelas.inativo = '0' " . $queryOperadorS . "
and " . $queryDataS . " between '" . $DateBegin . " 00:00:00 ' and " . valoresDataHora2($DateEnd, "23:59:59") . " and (id_banco in (" . $comando . ") or id_banco is null) order by 2,id_parcela";
$cur = odbc_exec($con, $sQuery1);
//echo $sQuery1; exit;
while ($RFP = odbc_fetch_array($cur)) {
    if ($RFP['tipo'] == "C") {
        if ($tpData == "1") {
            $SubTotalC += valoresNumericos2(escreverNumero($RFP['valor_pago']));
        } else {
            $SubTotalC += $RFP['somar_rel'] === "1" ? valoresNumericos2(escreverNumero(($RFP['valor_pago']))) : 0;
        }
    } else if ($RFP['tipo'] == "D") {
        if ($tpData == "1") {
            $SubTotalD += valoresNumericos2(escreverNumero($RFP['valor_pago']));
        } else {
            $SubTotalD += $RFP['somar_rel'] === "1" ? valoresNumericos2(escreverNumero(($RFP['valor_pago']))) : 0;
        }
    }
    if ($tpData == "1" || $RFP['somar_rel'] == "1") {
        if ($RFP['tipo'] == "C") {
            $SubTotal = $SubTotal + valoresNumericos2(escreverNumero($RFP['valor_pago']));
        } else {
            $SubTotal = $SubTotal - valoresNumericos2(escreverNumero($RFP['valor_pago']));
        }
    }
    $row = array();
    $row[] = escreverData($RFP['data_pagamento']);
    $row[] = str_pad($RFP['empresa'], 3, "0", STR_PAD_LEFT);
    $row[] = utf8_encode($RFP['banco']);
    $row[] = utf8_encode($RFP['tipo_documento']);
    if (isset($_GET['tpImp']) && $_GET['tpImp'] == "E") {
        $row[] = utf8_encode($RFP['cnpj']);
    }
    if ($RFP['id_fornecedores_despesas'] > 0) {
        $row[] = "<a href='javascript:void(0)' onClick='AbrirCli(" . utf8_encode($RFP['id_fornecedores_despesas']) . ")'><div id='formPQ' title='" . utf8_encode($RFP['razao_social']) . "'>" . utf8_encode($RFP['razao_social']) . "</div></a>";        
    } else {    
        $row[] = utf8_encode($RFP['razao_social']);    
    }
    $row[] = utf8_encode($RFP['historico_baixa']);
    if ($RFP['id_trans'] > 0) {
        $row[] = "<a onclick=\"AbrirBox(" . $RFP['id_trans'] . ");\">" . utf8_encode($RFP['descricao']) . "</a>";
    } else {
        $row[] = utf8_encode($RFP['descricao']);
    }
    $row[] = escreverNumero(($RFP['tipo'] == "C" ? $RFP['valor_pago'] : 0));
    $row[] = escreverNumero(($RFP['tipo'] == "D" ? $RFP['valor_pago'] : 0));
    $row[] = escreverNumero($SubTotal);
    $output['aaData'][] = $row;
}
//------------------------------------------------------------------------------
$row = array();
$row[] = "";
$row[] = "";
$row[] = "";
$row[] = "";
if (isset($_GET['tpImp']) && $_GET['tpImp'] == "E") { 
    $row[] = "";    
}
$row[] = "";
$row[] = "<span style=\"color:#333;font-weight: bold;\"> => Sub total do período</span>";
$row[] = "";
$row[] = "<span style=\"color:#333;font-weight: bold;\">" . escreverNumero($SubTotalC) . "</span>";
$row[] = "<span style=\"color:#333;font-weight: bold;\">" . escreverNumero($SubTotalD) . "</span>";
$row[] = "<span style=\"color:#333;font-weight: bold;\">" . str_replace("-0,00", "0,00", escreverNumero($SubTotalC - $SubTotalD)) . "</span>";
$output['aaData'][] = $row;
//------------------------------------------------------------------------------
$row = array();
$row[] = "";
$row[] = "";
$row[] = "";
$row[] = "";
if (isset($_GET['tpImp']) && $_GET['tpImp'] == "E") { 
    $row[] = "";    
}
$row[] = "";
$row[] = "<span style=\"color:darkgray;font-weight: bold;\"> => Saldo acumulado</span>";
$row[] = "";
$row[] = "<span style=\"color:darkgray;font-weight: bold;\"></span>";
$row[] = "<span style=\"color:darkgray;font-weight: bold;\"></span>";
$row[] = "<span style=\"color:darkgray;font-weight: bold;\">" . escreverNumero($SubTotal) . "</span>";
$output['aaData'][] = $row;
//------------------------------------------------------------------------------

$queryTransfD = "set dateformat dmy; select isnull(sum(valor_trans),0) total from sf_transferencia_banco
where banco_origem_trans in (" . $comando . ") and sf_transferencia_banco.empresa in (" . $comando2 . ")";

$queryTransfC = "set dateformat dmy; select isnull(sum(valor_trans),0) total from sf_transferencia_banco 
where banco_destino_trans in (" . $comando . ") and sf_transferencia_banco.empresa in (" . $comando2 . ")";

$queryLancamentos = "set dateformat dmy; select isnull(sum(ROUND(case when 1 = " . $tpData . " then valor_pago else valor_parcela end, 2)),0) total from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento
on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas
inner join sf_tipo_documento td on sf_lancamento_movimento_parcelas.tipo_documento = td.id_tipo_documento
where sf_lancamento_movimento.status = 'Aprovado' and " . $queryDataL . " is not null AND sf_lancamento_movimento.empresa in (" . $comando2 . ") " . $queryOperadorL . " 
" . ($tpData == "2" ? " and sf_lancamento_movimento_parcelas.somar_rel = 1 " : "") . " AND sf_lancamento_movimento_parcelas.inativo = '0'
and id_banco in (" . $comando . ") and sf_contas_movimento.tipo = '";

$querySolicitacao = "select isnull(sum(ROUND(case when 1 = " . $tpData . " then valor_pago else valor_parcela end, 2)),0) total from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas
inner join sf_tipo_documento td on sf_solicitacao_autorizacao_parcelas.tipo_documento = td.id_tipo_documento
where sf_solicitacao_autorizacao.status = 'Aprovado' AND sf_solicitacao_autorizacao.empresa in (" . $comando2 . ") and " . $queryDataS . " is not null " . $queryOperadorS . " AND sf_solicitacao_autorizacao_parcelas.inativo = '0'
and (id_banco in (" . $comando . ") or id_banco is null) " . ($tpData == "2" ? " and sf_solicitacao_autorizacao_parcelas.somar_rel = 1 " : "") . " and sf_contas_movimento.tipo = '";

$queryVendas = "select isnull(sum(ROUND(case when 1 = " . $tpData . " then valor_pago else valor_parcela end, 2)),0) total from sf_venda_parcelas inner join sf_vendas
on sf_venda_parcelas.venda = sf_vendas.id_venda
inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas
inner join sf_tipo_documento td on sf_venda_parcelas.tipo_documento = td.id_tipo_documento
where sf_vendas.status = 'Aprovado' AND sf_vendas.empresa in (" . $comando2 . ") and " . $queryDataV . " is not null " . $queryOperadorV . " AND sf_venda_parcelas.inativo in (0,2)
" . $comandoVenda . ($tpData == "2" ? " and sf_venda_parcelas.somar_rel = 1" : "") . " and sf_vendas.cov = '";

$totalCredito = 0;
$totalDebito = 0;
$cur = odbc_exec($con, $queryLancamentos . "C'");
while ($RFP = odbc_fetch_array($cur)) {
    $totalCredito = $RFP['total'];
}
$cur = odbc_exec($con, $querySolicitacao . "C'");
while ($RFP = odbc_fetch_array($cur)) {
    $totalCredito = $totalCredito + $RFP['total'];
}
$cur = odbc_exec($con, $queryVendas . "V'");
while ($RFP = odbc_fetch_array($cur)) {
    $totalCredito = $totalCredito + $RFP['total'];
}
$cur = odbc_exec($con, $queryTransfC);
while ($RFP = odbc_fetch_array($cur)) {
    $totalCredito = $totalCredito + $RFP['total'];
}
$cur = odbc_exec($con, $queryLancamentos . "D'");
while ($RFP = odbc_fetch_array($cur)) {
    $totalDebito = $RFP['total'];
}
$cur = odbc_exec($con, $querySolicitacao . "D'");
while ($RFP = odbc_fetch_array($cur)) {
    $totalDebito = $totalDebito + $RFP['total'];
}
$cur = odbc_exec($con, $queryVendas . "C'");
while ($RFP = odbc_fetch_array($cur)) {
    $totalDebito = $totalDebito + $RFP['total'];
}
$cur = odbc_exec($con, $queryTransfD);
while ($RFP = odbc_fetch_array($cur)) {
    $totalDebito = $totalDebito + $RFP['total'];
}
$row = array();
$row[] = "";
$row[] = "";
$row[] = "";
$row[] = "";
if (isset($_GET['tpImp']) && $_GET['tpImp'] == "E") { 
    $row[] = "";    
}
$row[] = "";
$row[] = "<span style=\"color:#0066cc\"> => SALDO ATUAL</span>";
$row[] = "";
$row[] = "";
$row[] = "";
$row[] = "<span style=\"color:#0066cc\">" . escreverNumero(($totalCredito - $totalDebito)) . "</span>";
$output['aaData'][] = $row;

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);