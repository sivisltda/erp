<?php

header('Cache-Control: no-cache');
include "../../Connections/configini.php";

$local = array();
$item = explode("-", $_REQUEST['txtId']);

if ($item[0] == 'A') {
    $cur = odbc_exec($con, "select solicitacao_autorizacao from sf_solicitacao_autorizacao_parcelas where id_parcela = " . $item[1]);
    while ($RFP = odbc_fetch_array($cur)) {
        $plugue = "FormSolicitacao-de-Autorizacao.php?id=" . $RFP['solicitacao_autorizacao'];
    }
} else if ($item[0] == 'D' || $item[0] == 'Y') {
    $cur = odbc_exec($con, "select id_lancamento_movimento from sf_lancamento_movimento_parcelas where id_parcela = " . $item[1]);
    while ($RFP = odbc_fetch_array($cur)) {
        $plugue = "./../Financeiro/Lancamento-de-Renegociacao.php?id=" . $RFP['id_lancamento_movimento'];
    }
} else if ($item[0] == 'V') {
    $cur = odbc_exec($con, "select venda from sf_venda_parcelas where id_parcela = " . $item[1]);
    while ($RFP = odbc_fetch_array($cur)) {
        $plugue = "./../Estoque/" . ($_REQUEST['txtFr'] == "P" ? "FormCompras.php" : "FormVendas.php") . "?id=" . $RFP['venda'];
    }
} else if ($item[0] == 'M') {
    $cur = odbc_exec($con, "select favorecido from sf_vendas_planos_mensalidade inner join sf_vendas_planos on id_plano_mens = id_plano where id_mens = " . $item[1]);
    while ($RFP = odbc_fetch_array($cur)) {
        $plugue = "./../Academia/ClientesForm.php?id=" . $RFP['favorecido'];
    }    
}

$local[] = array('plugue' => $plugue);
echo(json_encode($local));
odbc_close($con);
