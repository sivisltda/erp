<?php

$ValorBaixa = 0;
$DataBaixa = "";

if ($_GET['idx'] != '') {
    $count = explode("|", $_GET['idx']);
    if (count($count) > 0) {
        $i = 0;
        while ($i < count($count)) {
            $item = explode("-", $count[$i]);
            if (count($item) == 2) {
                if ($item[0] == 'A') {
                    $WhereA = $WhereA . $item[1] . ',';
                } else if ($item[0] == 'D') {
                    $WhereD = $WhereD . $item[1] . ',';
                } else if ($item[0] == 'V') {
                    $WhereV = $WhereV . $item[1] . ',';
                } else if ($item[0] == 'Y') {
                    $cur = odbc_exec($con, "select id_parcela from sf_lancamento_movimento_parcelas where id_parcela in
                                (select id_parcela from sf_lancamento_movimento_parcelas where bol_nosso_numero in (
                                select bol_nosso_numero from sf_lancamento_movimento_parcelas where id_parcela = " . $item[1] . "))") or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) {
                        $WhereD = $WhereD . $RFP['id_parcela'] . ',';
                    }
                    $disabledX = 'disabled';
                }
            }
            $i++;
        }
    }
    $cur = odbc_exec($con, "select razao_social,valor_pago,data_pagamento,historico
                            from sf_solicitacao_autorizacao_parcelas sp 
                            inner join sf_solicitacao_autorizacao s on s.id_solicitacao_autorizacao = sp.solicitacao_autorizacao
                            inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = s.fornecedor_despesa                            
                            inner join sf_contas_movimento c on c.id_contas_movimento = s.conta_movimento where sp.id_parcela in (" . $WhereA . "0)") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $nome = utf8_encode($RFP['razao_social']);
        $ValorBaixa = $ValorBaixa + $RFP['valor_pago'];
        $DataBaixa = escreverData($RFP['data_pagamento'], 'm/d/Y');
        $referencia = utf8_encode($RFP['historico']);
    }
    $cur = odbc_exec($con, "select razao_social,valor_pago,data_pagamento,historico_venda
                            from sf_venda_parcelas sp inner join sf_vendas s on s.id_venda = sp.venda 
                            inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = s.cliente_venda
                            where sp.id_parcela in (" . $WhereV . "0)") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $nome = utf8_encode($RFP['razao_social']);
        $ValorBaixa = $ValorBaixa + $RFP['valor_pago'];
        $DataBaixa = escreverData($RFP['data_pagamento'], 'm/d/Y');
        $referencia = utf8_encode($RFP['historico_venda']);
    }
    $cur = odbc_exec($con, "select razao_social,valor_pago,data_pagamento,historico
                            from sf_lancamento_movimento_parcelas lp inner join sf_lancamento_movimento l on l.id_lancamento_movimento = lp.id_lancamento_movimento
                            inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = l.fornecedor_despesa
                            inner join sf_contas_movimento c on c.id_contas_movimento = l.conta_movimento 
                            where l.status = 'Aprovado' and lp.id_parcela in (" . $WhereD . "0)") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $nome = utf8_encode($RFP['razao_social']);
        $ValorBaixa = $ValorBaixa + $RFP['valor_pago'];
        $DataBaixa = escreverData($RFP['data_pagamento'], 'm/d/Y');
        $referencia = utf8_encode($RFP['historico']);
    }
}

$mesI = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$mesP = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
$diaI = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
$diaP = array("Domingo", "Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado");

$valor = escreverNumero($ValorBaixa, 1);
$valor_texto = valorPorExtenso($ValorBaixa, true, false);
$data = str_replace($diaI, $diaP, str_replace($mesI, $mesP, strftime("%A, %d de %B de %Y", strtotime($DataBaixa))));

if ($_GET['tp'] == '1') {
    $para = $nome;
    $nome = $_SESSION["razao_social_contrato"];
} else {
    $para = $_SESSION["razao_social_contrato"];
}
?>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
            body {
                margin:0px;
                padding:0px;
            }
            table {
                margin-bottom: 10px;
                font-size: 12px;
            }
            .titulo {
                font-weight: Bold;
                text-align: center;
            }          
        </style>
    </head>
    <body>
        <div id="main" style="width: 700px; overflow:hidden;">
            <table width="100%" border="1" cellpadding="2" cellspacing="0">
                <tr>
                    <td width="500px" rowspan="2">
                        <table width="100%" cellpadding="0" border="0" cellspacing="0" style="border:0px solid white;padding-bottom: 15px !important;">
                            <tr>
                                <td width="200px">
                                    <?php if (file_exists("./../Pessoas/" . $_SESSION["contrato"] . "/Empresa/logo_" . $filial_orc . ".jpg")) { ?>
                                        <div style="line-height: 5px;">&nbsp;</div><img src ="./../Pessoas/<?php echo $_SESSION["contrato"]; ?>/Empresa/logo_<?php echo $filial_orc; ?>.jpg" width="150" height="50" align="middle" />
                                    <?php } else { ?>
                                        <div style="line-height: 5px;">&nbsp;</div>
                                    <?php } ?>
                                </td>
                                <td width="265px">
                                    <div style="text-align:right;font-size: 8px;"><?php echo $_SESSION["cabecalho"]; ?></div>
                                    <br>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="200px" colspan="2" align="center" style="line-height: 60px;font-size:18px;">
                        <b><?php echo "RECIBO"; ?></b>
                    </td>
                </tr>                
                <tr style="background-color: gray;color: white;">
                    <td style="padding-left:5px;width: 50px;text-align: right;font-size: 14px;font-weight: bold;">Valor:</td>
                    <td style="width: 150px;text-align: right;font-size: 14px;font-weight: bold;"><?php echo $valor; ?></td>
                </tr>
                <tr>         
                    <td width="700px" height="400px" colspan="2">
                        <br><br>
                        <table width="680px">            
                            <tr>
                                <td width="90px"></td>
                                <td width="500px">Num. <b><?php echo $_GET['idx']; ?></b></td>
                            </tr> 
                        </table>
                        <br><br>
                        <table width="680px">            
                            <tr>
                                <td width="90px"></td>
                                <td width="500px">Eu, <?php echo $nome; ?> recebi de <?php echo $para; ?></td>
                                <td width="90px"></td>                    
                            </tr>                                               
                            <tr>
                                <td></td>
                                <td>a importância de <?php echo $valor; ?> (<?php echo $valor_texto; ?>)</td>
                                <td></td>                    
                            </tr>                                               
                            <tr>
                                <td></td>
                                <td>referente à <?php echo $referencia; ?></td>
                                <td></td>                    
                            </tr>                                               
                            <tr><td colspan="3"></td></tr>
                            <tr>
                                <td></td>
                                <td>Para maior clareza, firmamos o presente, </td>
                                <td></td>                    
                            </tr>                                               
                            <tr><td colspan="3"></td></tr>
                            <tr><td colspan="3"></td></tr>
                            <tr><td colspan="3"></td></tr>                
                            <tr>
                                <td></td>
                                <td style="text-align: right"><?php echo $data; ?></td>
                                <td></td>                    
                            </tr>
                            <tr><td colspan="3"></td></tr>                        
                            <tr><td colspan="3"></td></tr>                
                            <tr>
                                <td></td>
                                <td style="text-align: right">__________________________________</td>
                                <td></td>                    
                            </tr>                                               
                            <tr>
                                <td></td>
                                <td style="text-align: right;font-size: 9px;"><?php echo $nome; ?></td>
                                <td></td>                    
                            </tr>                                               
                        </table>  
                    </td>
                </tr>
            </table>                   
        </div>
    </body>
    <?php odbc_close($con); ?>
</html>            