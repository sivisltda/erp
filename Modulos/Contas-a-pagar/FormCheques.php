<?php
include './../../Connections/configini.php';

$disabled = '';
$id = '';
$tp = '0';
$data = getData("T");
$banco = '';
$agencia = '';
$conta = '';
$numero = '';
$proprietario = '';
$valor = escreverNumero(0);
$recebido = '0';
$repassar = '';
$troca = '';
$tipo = ''; 
$codigo_relacao = '';

if (is_numeric($_GET['tp'])) {
    $tp = $_GET['tp'];
}

if (isset($_POST['bntSaveTroca'])) {
    $cur = odbc_exec($con, "select * from sf_cheques where chq_id = " . valoresNumericos('txtId'));
    while ($RFP = odbc_fetch_array($cur)) {
        $tipo = $RFP['tipo'];
        $codigo_relacao = $RFP['codigo_relacao'];
    }
    $query = "set dateformat dmy;insert into sf_cheques(chq_data,chq_agencia,chq_banco,chq_conta,chq_proprietario,chq_numero,chq_valor,chq_recebido,chq_troca, tipo, codigo_relacao)values(" .
    valoresData('txtData') . "," . valoresTexto('txtAgencia') . "," . valoresTexto('txtBanco') . "," . valoresTexto('txtConta') . "," .
    valoresTexto('txtProprietario') . "," . valoresTexto('txtNumero') . "," . valoresNumericos('txtValor') . ",0," . valoresNumericos('txtId') . "," . valoresTexto2($tipo) . "," . valoresNumericos2($codigo_relacao) . ")";
    odbc_exec($con, $query) or die(odbc_errormsg());
    odbc_exec($con, "update sf_cheques set chq_recebido = 2 where chq_id = " . valoresNumericos('txtId')) or die(odbc_errormsg());
    echo "<script>alert('Cheque Trocado com sucesso!');parent.FecharBox(0);</script>";
}

if (isset($_POST['bntSaveRepasse'])) {
    odbc_exec($con, "update sf_cheques set chq_repasse = " . valoresTexto('txtRepassar') . " where chq_id = " . $_POST['txtId']) or die(odbc_errormsg());
    echo "<script>alert('Cheque Repassado com sucesso!');parent.FecharBox(0);</script>";
}

if ($_GET['id'] != '') {
    $cur = odbc_exec($con, "select * from sf_cheques where chq_id = " . $_GET['id']);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['chq_id']);
        $data = escreverData($RFP['chq_data']);
        $banco = utf8_encode($RFP['chq_banco']);
        $agencia = utf8_encode($RFP['chq_agencia']);
        $conta = utf8_encode($RFP['chq_conta']);
        $numero = utf8_encode($RFP['chq_numero']);
        $proprietario = utf8_encode($RFP['chq_proprietario']);
        $valor = escreverNumero($RFP['chq_valor']);
        $recebido = utf8_encode($RFP['chq_recebido']);
        $repassar = utf8_encode($RFP['chq_repasse']);
        $troca = utf8_encode($RFP['chq_troca']);
    }
    $dataAnt = $data;
    $bancoAnt = $banco;
    $agenciaAnt = $agencia;
    $contaAnt = $conta;
    $numeroAnt = $numero;
}

if ($tp == "2") {
    $disabled = 'disabled';
    $dataAnt = '';
    $bancoAnt = '';
    $agenciaAnt = '';
    $contaAnt = '';
    $numeroAnt = '';
    if (is_numeric($troca)) {
        $cur = odbc_exec($con, "select * from sf_cheques where chq_id = " . $troca);
        while ($RFP = odbc_fetch_array($cur)) {
            $dataAnt = escreverData($RFP['chq_data']);
            $bancoAnt = utf8_encode($RFP['chq_banco']);
            $agenciaAnt = utf8_encode($RFP['chq_agencia']);
            $contaAnt = utf8_encode($RFP['chq_conta']);
            $numeroAnt = utf8_encode($RFP['chq_numero']);
        }
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
<link href="../../css/main.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../../css/styleLeo.css"/>
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <form action="FormCheques.php?tp=<?php echo $tp; ?>&id=<?php echo $id; ?>" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Cheques</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div style="padding: 10px; height: 195px;">
            <div class="data-fluid tabbable" style="overflow:hidden;">
                <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                <div style="float:left;width: 22%">
                    <label>Data:</label>
                    <input maxlength="10" disabled type="text" class="input-medium datepicker" value="<?php echo $data; ?>"/>
                </div>
                <div style="float:left;width: 26%; margin-left: 1%;">
                    <label>Banco:</label>
                    <input maxlength="128" disabled type="text" class="input-medium" value="<?php echo $banco; ?>"/>
                </div>
                <div style="float:left;width: 20%; margin-left: 1%;">
                    <label>Agencia:</label>
                    <input maxlength="128" disabled type="text" class="input-medium" value="<?php echo $agencia; ?>"/>
                </div>
                <div style="float:left;width: 28%; margin-left: 1%;">
                    <label>Conta:</label>
                    <input maxlength="128" disabled type="text" class="input-medium" value="<?php echo $conta; ?>"/>                    
                </div>     
                <div style="clear:both"></div>
                <div style="float:left;width: 27%;">
                    <label>Número:</label>
                    <input maxlength="128" disabled type="text" class="input-medium" value="<?php echo $numero; ?>"/>                    
                </div>
                <div style="float:left;width: 50%; margin-left: 1%;">
                    <label>Proprietário:</label>
                    <input maxlength="128" disabled type="text" class="input-medium" value="<?php echo $proprietario; ?>"/>
                </div>
                <div style="float:left;width: 20%; margin-left: 1%;">
                    <label>Valor:</label>
                    <input maxlength="12" disabled type="text" class="input-medium" value="<?php echo $valor; ?>"/>
                </div>                
                <div style="clear:both"></div>                
                <div style="margin-top: <?php echo ($tp == "2" ? "1" : "15"); ?>px; font-size:14px;"><?php echo ($tp == "0" ? "Trocar" : ($tp == "1" ? "Repassar" : "Detalhe do")); ?> Cheque</div>
                <hr style="margin:2px 0px <?php echo ($tp == "2" ? "1" : "15"); ?>px; border-top:1px solid #ddd">
                <?php if ($tp == "1" || $tp == "2") { ?>
                    <div style="float:left;width: 99%;">
                        <label for="txtRepassar">Repassar para:</label>
                        <input name="txtRepassar" id="txtRepassar" <?php echo $disabled; ?> maxlength="256" type="text" class="input-medium" value="<?php echo $repassar; ?>"/>                    
                    </div>                   
                <?php } if ($tp == "0" || $tp == "2") { ?>
                    <div style="float:left;width: 18%">
                        <label for="txtData">Data:<?php echo ($tp == "2" ? " (Anterior)" : ""); ?></label>
                        <input name="txtData" id="txtData" <?php echo $disabled; ?> maxlength="10" type="text" class="input-medium datepicker" value="<?php echo $dataAnt; ?>"/>
                    </div>
                    <div style="float:left;width: 22%; margin-left: 1%;">
                        <label for="txtBanco">Banco:</label>
                        <input name="txtBanco" id="txtBanco" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $bancoAnt; ?>"/>
                    </div>
                    <div style="float:left;width: 15%; margin-left: 1%;">
                        <label for="txtAgencia">Agencia:</label>
                        <input name="txtAgencia" id="txtAgencia" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $agenciaAnt; ?>"/>
                    </div>
                    <div style="float:left;width: 20%; margin-left: 1%;">
                        <label for="txtConta">Conta:</label>
                        <input name="txtConta" id="txtConta" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $contaAnt; ?>"/>                    
                    </div>      
                    <div style="float:left;width: 20%; margin-left: 1%;">
                        <label for="txtNumero">Número:</label>
                        <input name="txtNumero" id="txtNumero" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $numeroAnt; ?>"/>                    
                    </div>       
                    <input name="txtProprietario" id="txtProprietario" value="<?php echo $proprietario; ?>" type="hidden"/>
                    <input name="txtValor" id="txtValor" value="<?php echo $valor; ?>" type="hidden"/>                                              
                <?php } ?>                
            </div>
        </div>            
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($tp == "0" || $tp == "1") { ?>
                    <button class="btn green" type="submit" name="bntSave<?php echo ($tp == "0" ? "Troca" : "Repasse"); ?>" id="bntOK"><span class="ico-checkmark"></span> Gravar</button>
                <?php } ?>                
                <button class="btn yellow" style="<?php echo ($tp == "2" ? "margin-top: 15px;" : ""); ?>" onClick="parent.FecharBox()"><span class="ico-reply"></span> Cancelar</button>
            </div>
        </div>                    
    </form>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>                
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>     
    <script type="text/javascript" src="../../js/util.js"></script>       
    <?php odbc_close($con); ?>    
</body>