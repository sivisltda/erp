<?php

if (!isset($_GET['pdf'])) {
    require_once(__DIR__ . '/../../Connections/configini.php');
}

$aColumns = array('documento', 'bol_nosso_numero', 'razao_social', 'historico_baixa', 'gc', 'cm');
$iTotal = 0;
$totparc = 0;
$totparcpg = 0;
$sWhere = "";
$sendURL = "";
$tipoData = "0";
$F4 = "0";
$sLimit = 20;
$imprimir = 0;
$tipo_pag = 0;
$grupoArray = [];

$sql = "select adm_mens_acess_soma from sf_configuracao";   
$cur = odbc_exec($con, $sql) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {    
    $tipo_pag = $RFP['adm_mens_acess_soma'];
}

if ($tipo_pag > 0) {
    $sql = "update sf_vendas_planos_mensalidade set valor_acessorios = dbo.FU_VALOR_ACESSORIOS(id_plano_mens), valor_real = dbo.VALOR_REAL_MENSALIDADE(id_mens) 
    where id_mens in (select top 1000 id_mens from sf_vendas_planos_mensalidade where valor_real is null);";
    odbc_exec($con, $sql) or die(odbc_errormsg());
}

if ($_GET['id'] == '5' || $_GET['id'] == '6') {
    $sOrder = " ORDER BY razao_social asc ";
} else {
    if ($_GET['ord'] == '8') {
        $sOrder = " ORDER BY razao_social asc ";
    } else {
        if ($_GET['id'] == '2') {
            $sOrder = " ORDER BY Vecto desc ";
        } else {
            $sOrder = " ORDER BY Vecto asc ";
        }
    }
}
if ($_GET['id'] != '') {
    $sendURL = $sendURL . "&idx=" . $_GET ['id'];
}
if ($_GET['tp'] != '') {
    $sendURL = $sendURL . "&tpx=" . $_GET ['tp'];
}
if ($_GET['td'] != '') {
    $sendURL = $sendURL . "&tdx=" . $_GET ['td'];
}
if (is_numeric($_GET['tpd'])) {
    $sendURL = $sendURL . "&tpd=" . $_GET['tpd'];
}
if ($_GET['dti'] != '') {
    $sendURL = $sendURL . "&dti=" . $_GET ['dti'];
}
if ($_GET['dtf'] != '') {
    $sendURL = $sendURL . "&dtf=" . $_GET ['dtf'];
}
if ($_GET['filial'] != '') {
    $sendURL = $sendURL . "&filial=" . $_GET ['filial'];
}
if (is_numeric($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}
if ($_GET['total'] != "") {
    $iTotal = $_GET['total'];
}
if ($_GET['id'] != "") {
    $F1 = $_GET['id'];
}
if ($_GET['tp'] != "") {
    $F2 = $_GET['tp'];
}
if ($_GET['td'] != "") {
    $F3 = $_GET['td'];
}
if ($_GET['grp'] != "") {
    $F3 = $_GET['grp'];
}
if ($_GET['rel'] != "") {
    $F4 = $_GET['rel'];
}
if ($_GET['filial'] != "") {
    $idFilial = $_GET['filial'];
}
if (is_numeric($_GET['tpd'])) {
    $tipoData = $_GET['tpd'];
}
if ($_GET['dti'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
}
if ($_GET['dtf'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
}
if (isset($_GET['iDisplayStart']) && $_GET ['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) == 8) {
                $sOrder .= "razao_social " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        if ($_GET['id'] == '5' || $_GET['id'] == '6') {
            $sOrder = " ORDER BY razao_social asc ";
        } else {
            if ($_GET['ord'] == '8') {
                $sOrder = " ORDER BY razao_social asc ";
            } else {
                if ($_GET['id'] == '2') {
                    $sOrder = " ORDER BY Vecto desc ";
                } else {
                    $sOrder = " ORDER BY Vecto asc ";
                }
            }
        }
    }
}

if ($_GET['sSearch'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns [$i] . " LIKE '%" . utf8_decode($_GET['sSearch']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

$comando = '';
$comandoX1 = '';
$comandoX2 = '';
$comandoX3 = '';
$comandoX4 = '';
$comandoX5 = '';

if ($F1 == "1") {
    $comando = " AND valor_pago = 0 ";
} elseif ($F1 == "2") {
    $comando = " AND valor_pago > 0 and sf_tipo_documento.id_tipo_documento not in (12) ";
} elseif ($F1 == "5" || $F1 == "6") {
    $comando = " AND bol_id_banco is not null ";
}

if ($F2 == "0") {
    if ($F3 != "") {
        $comando .= " AND sf_fornecedores_despesas.id_fornecedores_despesas = " . $F3;
        $comandoX4 .= " AND sf_fornecedores_despesas.id_fornecedores_despesas = " . $F3;
        $comandoX5 .= " AND sf_fornecedores_despesas.id_fornecedores_despesas = " . $F3;
    }
} elseif ($F2 == "1") {
    if ($F3 != "") {
        $comando .= " AND sf_grupo_contas.id_grupo_contas = " . $F3;
        $comandoX4 .= " AND sf_grupo_contas.id_grupo_contas = " . $F3;
        $comandoX5 .= " AND sf_grupo_contas.id_grupo_contas = " . $F3;
    }
} elseif ($F2 == "2") {
    if ($F3 != "") {
        $comando .= " AND sf_contas_movimento.id_contas_movimento = " . $F3;
        $comandoX4 .= " AND sf_contas_movimento.id_contas_movimento = " . $F3;
        $comandoX5 .= " AND sf_contas_movimento.id_contas_movimento = " . $F3;
    }
} elseif ($F2 == "3") {
    if ($F3 != "") {
        $comando .= " AND sf_tipo_documento.id_tipo_documento in (" . str_replace("\"", "'", str_replace(array("[", "]"), "", $F3)) . ")";
        $comandoX4 .= " AND dt_cancelamento is null AND dt_cancelamento is not null";
        $comandoX5 .= " AND id_venda is null AND id_venda is not null";
    }
} elseif ($F2 == "4") {
    if ($F3 != "") {
        $comando .= " AND sf_fornecedores_despesas.grupo_pessoa = " . $F3;
        $comandoX4 .= " AND sf_fornecedores_despesas.grupo_pessoa = " . $F3;
        $comandoX5 .= " AND sf_fornecedores_despesas.grupo_pessoa = " . $F3;
    }
} elseif ($F2 == "6") {
    if ($F3 != "") {
        $comando .= " AND sf_grupo_contas.centro_custo = " . $F3;
        $comandoX4 .= " AND sf_grupo_contas.centro_custo = " . $F3;
        $comandoX5 .= " AND sf_grupo_contas.centro_custo = " . $F3;
    }    
}

if (isset($_GET['sts']) && $_GET['sts'] != 'null') {
    $Fx = " and (";
    $dataAux = explode(",", str_replace(array("[", "]", "\""), "", $_GET['sts']));
    for ($i = 0; $i < count($dataAux); $i++) {
        $Fx .= ($i > 0 ? " or " : "") . " fornecedores_status = '" . getStatus($dataAux[$i]) . "'";
    }
    $Fx .= ")";
    $comando .= $Fx;
    $comandoX4 .= $Fx;
    $comandoX5 .= $Fx;
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $campo = "data_parcela";
    $campo4 = "dt_inicio_mens";
    $campo5 = "cast(bol_data_parcela as date)";
    if ($tipoData == 1) {
        $campo = "data_pagamento";
        $campo4 = "dt_pagamento_mens";
        $campo5 = "cast(bol_data_parcela as date)";
    } elseif ($tipoData == 2) {
        $campo = "data_cadastro";
        $campo4 = "m.dt_cadastro";
        $campo5 = "cast(bol_data_criacao as date)";
    }
    $comandoX1 .= " AND " . ($tipoData == 0 ? "data_vencimento" : $campo) . " >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND " . ($tipoData == 0 ? "data_vencimento" : $campo) . " <= " . valoresDataHora2($DateEnd, "23:59:59");
    $comandoX2 .= " AND " . $campo . " >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND " . $campo . " <= " . valoresDataHora2($DateEnd, "23:59:59");
    $comandoX3 .= " AND " . $campo . " >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND " . $campo . " <= " . valoresDataHora2($DateEnd, "23:59:59");
    $comandoX4 .= " AND " . $campo4 . " >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND " . $campo4 . " <= " . valoresDataHora2($DateEnd, "23:59:59");
    $comandoX5 .= " AND " . $campo5 . " >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND " . $campo5 . " <= " . valoresDataHora2($DateEnd, "23:59:59");
}

if ($F1 == "3") {
    $comandoX1 .= " AND sf_lancamento_movimento_parcelas.inativo = '1' ";
    $comandoX2 .= " AND sf_solicitacao_autorizacao_parcelas.inativo = '1' ";
    $comandoX3 .= " AND sf_venda_parcelas.inativo = '1' ";
} elseif ($F1 == "7") {
    $comandoX1 .= " AND sf_lancamento_movimento_parcelas.inativo = '2' ";
    $comandoX2 .= " AND sf_solicitacao_autorizacao_parcelas.inativo = '2' ";
    $comandoX3 .= " AND sf_venda_parcelas.inativo = '2' ";
} else {
    $comandoX1 .= " AND sf_lancamento_movimento_parcelas.inativo = '0' ";
    $comandoX2 .= " AND sf_solicitacao_autorizacao_parcelas.inativo = '0' ";
    $comandoX3 .= " AND sf_venda_parcelas.inativo = '0' ";
}

if (is_numeric($idFilial)) {
    $comandoX1 .= " AND sf_lancamento_movimento.empresa = " . $idFilial;
    $comandoX2 .= " AND sf_solicitacao_autorizacao.empresa = " . $idFilial;
    $comandoX3 .= " and sf_vendas.empresa = " . $idFilial;
}

if (isset($_GET['ibl']) && $_GET['ibl'] == '1') {
    $comandoX1 .= " and sf_lancamento_movimento_parcelas.bol_id_banco is not null ";
    $comandoX2 .= " and sf_solicitacao_autorizacao_parcelas.bol_id_banco is not null ";
    $comandoX3 .= " and sf_venda_parcelas.bol_id_banco is not null ";
    $comandoX4 .= " and b.bol_id_banco is not null ";
    $comandoX5 .= " and t.id_banco_baixa is not null ";
}

if ($F4 == "1" && $F2 == "3") {
    $whereDymanic = "";
    $cur = odbc_exec($con, "select id_tipo_documento_campos from sf_tipo_documento
    inner join sf_tipo_documento_campos on tipo_documento = id_tipo_documento
    where sf_tipo_documento.id_tipo_documento in (" . str_replace("\"", "'", str_replace(array("[", "]"), "", $F3)) . ")");
    while ($RFP = odbc_fetch_array($cur)) {
        if (isset($_GET["txtCd" . $RFP['id_tipo_documento_campos']]) && $_GET["txtCd" . $RFP['id_tipo_documento_campos']] != "") {
            $whereDymanic .= "(tipo_documento_campos = " . $RFP['id_tipo_documento_campos'] .
                    " and conteudo like '%" . utf8_decode(str_replace("'", "", $_GET["txtCd" . $RFP['id_tipo_documento_campos']])) . "%') or";
        }
    }
    $comandoX3 .= (strlen($whereDymanic) > 0 ? " and sf_vendas.id_venda in (select id_sf_vendas from sf_vendas_campos where (" . substr_replace($whereDymanic, "", -3) . "))" : "");
}

$sQuery = "set dateformat dmy; select bol_nosso_numero into #temp_parc_group from sf_lancamento_movimento_parcelas 
group by bol_id_banco, bol_nosso_numero having bol_nosso_numero is not null and count(id_parcela) > 1;";
//echo $sQuery; exit;
$cur = odbc_exec($con, $sQuery);
//--------------------------------------------------------------------------------------------------------------------------
$sQuery1 = "";
if ($F1 != "6") {
    $sQuery1 .= "select 'D-' +cast(sf_lancamento_movimento_parcelas.id_parcela as VARCHAR) as pk ,data_vencimento 'Vecto',pa ,sf_lancamento_movimento.empresa empresa,razao_social,historico historico_baixa, 'D' Origem , sf_contas_movimento.descricao 'cm',
    valor_parcela, valor_pago,sf_grupo_contas.descricao gc,sf_lancamento_movimento_parcelas.inativo inativo, sf_lancamento_movimento_parcelas.sa_descricao documento,obs_baixa,isnull(bol_id_banco,0) bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,bol_nosso_numero,data_pagamento,data_cadastro,bol_data_parcela,bol_valor,
    sf_fornecedores_despesas.tipo, sf_fornecedores_despesas.id_user_resp,
    (select max(chq_id) cheque from sf_cheques ci where ci.codigo_relacao = sf_lancamento_movimento_parcelas.id_parcela and ci.tipo = 'C') cheque, '' boleto_online
    from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento
    on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
    inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
    inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas
    left join sf_tipo_documento on sf_lancamento_movimento_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento
    inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'C' and sf_lancamento_movimento.status = 'Aprovado' " . $comando . $comandoX1 . "
    and (sf_lancamento_movimento_parcelas.bol_nosso_numero is null or sf_lancamento_movimento_parcelas.bol_nosso_numero not in (select bol_nosso_numero from #temp_parc_group)) union
    select 'Y-' +cast(max(sf_lancamento_movimento_parcelas.id_parcela) as VARCHAR) as pk,MAX(data_vencimento) 'Vecto',MAX(pa) pa,MAX(sf_lancamento_movimento.empresa) empresa
    ,MAX(razao_social) razao_social, MAX(historico) historico_baixa, 'D' Origem , MAX(sf_contas_movimento.descricao) 'cm',
    sum(valor_parcela) valor_parcela,(sum(valor_pago)-((sum(valor_pago)/100)*sf_tipo_documento.taxa)) valor_pago,MAX(sf_grupo_contas.descricao) gc,MAX(sf_lancamento_movimento_parcelas.inativo) inativo,
    MAX(sf_lancamento_movimento_parcelas.sa_descricao) documento,
    MAX(obs_baixa) obs_baixa,MAX(isnull(bol_id_banco,0)) bol_id_banco,MAX(sf_tipo_documento.abreviacao)abreviacao,
    MAX(sf_tipo_documento.descricao) tdn,MAX(id_fornecedores_despesas) id_fornecedores_despesas,MAX(bol_nosso_numero) bol_nosso_numero, MAX(data_pagamento) data_pagamento, Max(data_cadastro) data_cadastro, Max(bol_data_parcela) bol_data_parcela,MAX(bol_valor) bol_valor
    ,MAX(sf_fornecedores_despesas.tipo) tipo, MAX(sf_fornecedores_despesas.id_user_resp) id_user_resp,
    (select max(chq_id) cheque from sf_cheques ci where ci.codigo_relacao = max(sf_lancamento_movimento_parcelas.id_parcela) and ci.tipo = 'C') cheque, '' boleto_online
    from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento
    on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
    inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
    inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas
    left join sf_tipo_documento on sf_lancamento_movimento_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento
    inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'C' and sf_lancamento_movimento.status = 'Aprovado' " . $comando . $comandoX1 . "
    group by bol_id_banco, bol_nosso_numero,sf_tipo_documento.taxa having bol_nosso_numero is not null and count(id_parcela) > 1 ";
}
if ($F1 != "5" && $F1 != "6") {
    $sQuery1 .= "union";
}
if ($F1 != "5") {
    $sQuery1 .= " select 'A-' +cast(sf_solicitacao_autorizacao_parcelas.id_parcela as VARCHAR) as pk ,data_parcela 'Vecto',pa ,sf_solicitacao_autorizacao.empresa empresa,razao_social, historico_baixa,'A' Origem, sf_contas_movimento.descricao 'cm',
    valor_parcela, valor_pago,sf_grupo_contas.descricao gc,sf_solicitacao_autorizacao_parcelas.inativo inativo,sf_solicitacao_autorizacao_parcelas.sa_descricao documento,obs_baixa,isnull(bol_id_banco,0) bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,bol_nosso_numero,data_pagamento,data_cadastro,bol_data_parcela,bol_valor,
    sf_fornecedores_despesas.tipo, sf_fornecedores_despesas.id_user_resp,
    (select max(chq_id) cheque from sf_cheques ci where ci.codigo_relacao = sf_solicitacao_autorizacao_parcelas.id_parcela and ci.tipo = 'S') cheque, '' boleto_online
    from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
    on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
    inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
    inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas
    left join sf_tipo_documento on sf_solicitacao_autorizacao_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento
    inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'C' and status = 'Aprovado' " . $comando . $comandoX2 . "
    union
    select 'V-' +cast(sf_venda_parcelas.id_parcela as VARCHAR) as pk ,data_parcela 'Vecto',pa ,sf_vendas.empresa empresa,razao_social, sf_vendas.historico_venda collate SQL_Latin1_General_CP850_CI_AI historico_baixa,'V' Origem,
    sf_contas_movimento.descricao cm,valor_parcela,valor_pago,sf_grupo_contas.descricao gc,sf_venda_parcelas.inativo inativo,sf_venda_parcelas.sa_descricao documento,obs_baixa,bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,bol_nosso_numero,data_pagamento,data_cadastro,bol_data_parcela,bol_valor,
    sf_fornecedores_despesas.tipo, sf_fornecedores_despesas.id_user_resp,
    (select max(chq_id) cheque from sf_cheques ci where ci.codigo_relacao = sf_venda_parcelas.id_parcela and ci.tipo = 'V') cheque, '' boleto_online
    from sf_venda_parcelas inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda
    inner join sf_contas_movimento on sf_vendas.conta_movimento = sf_contas_movimento.id_contas_movimento
    inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas
    left join sf_tipo_documento on sf_venda_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento
    inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas where sf_vendas.cov = 'V' and status = 'Aprovado' " . $comando . $comandoX3;
}
if ($F1 == "1") {
    $sQuery1 .= "union select 'M-' + cast(id_mens as VARCHAR) as pk, dt_inicio_mens, '01/01' pa, 1 empresa, razao_social, p.descricao, 'M' origem,'MENSALIDADE' cm, " .
    ($tipo_pag > 0 ? "valor_real" : "dbo.VALOR_REAL_MENSALIDADE(id_mens)") . " valor_mens, 0.00 valor_pg, cm.descricao gc, 0 inativo, '' documento, '' obs_baixa, 
    bol_id_banco, case when parcela = 0 then 'DCC' when parcela = -1 then 'BOL' when parcela = -2 then 'PIX' else cast(parcela as varchar) end f_pag, '' tdn, 
    id_fornecedores_despesas, bol_nosso_numero,dt_pagamento_mens, m.dt_cadastro, bol_data_parcela, bol_valor, sf_fornecedores_despesas.tipo, sf_fornecedores_despesas.id_user_resp, null cheque, 
    (select max(bol_link) from sf_boleto_online bo inner join sf_boleto_online_itens boi on bo.id_boleto = boi.id_boleto where inativo = 0 and boi.id_mens = m.id_mens) boleto_online
    from sf_vendas_planos_mensalidade m
    inner join sf_vendas_planos on id_plano_mens = id_plano
    inner join sf_produtos p on id_prod_plano = conta_produto
    left join sf_contas_movimento cm on id_contas_movimento = conta_movimento
    left join sf_grupo_contas on cm.grupo_conta = sf_grupo_contas.id_grupo_contas
    inner join sf_produtos_parcelas on id_parcela = id_parc_prod_mens
    inner join sf_fornecedores_despesas on favorecido = id_fornecedores_despesas
    left join sf_boleto b on id_referencia = id_mens and tp_referencia = 'M' and b.inativo = 0
    where dt_pagamento_mens is null and sf_fornecedores_despesas.tipo = 'C' 
    and id_mens not in (select id_mens from sf_fornecedores_despesas_veiculo v
        inner join sf_vendas_planos p on p.id_veiculo = v.id
        inner join sf_vendas_planos_mensalidade m on p.id_plano = m.id_plano_mens
        where status in ('Aguarda', 'Pendente', 'Reprovado'))
    and id_mens not in (select id_mens from sf_boleto_online_itens bi1 
        inner join  sf_boleto_online b1 on b1.id_boleto = bi1.id_boleto
        where id_mens is not null and b1.inativo = 0)
    and sf_fornecedores_despesas.inativo = 0 and dt_cancelamento is null " . $comandoX4;
    //(STUFF((SELECT distinct ',' + descricao from sf_boleto_online_itens bi inner join sf_produtos on conta_produto = bi.id_produto where bi.id_boleto = b.id_boleto FOR XML PATH('')), 1, 1, '')) as descricao, 
    $sQuery1 .= "union select 'B-' + cast(id_boleto as VARCHAR) as pk, cast(bol_data_parcela as date) dt_inicio_mens, '01/01' pa, 1 empresa, razao_social, 
    '' as descricao, 'B' origem,'BOLETO' cm, (select sum(valor_bruto) from sf_boleto_online_itens bi where bi.id_boleto = b.id_boleto) bol_valor, 0.00 valor_pg,
    null gc, 0 inativo, '' documento, '' obs_baixa, t.id_banco_baixa, t.abreviacao, '' tdn, id_fornecedores_despesas, '' bol_nosso_numero, 
    null dt_pagamento_mens, bol_data_criacao, bol_data_parcela, bol_valor, sf_fornecedores_despesas.tipo, sf_fornecedores_despesas.id_user_resp, null cheque, bol_link
    from sf_boleto_online b
    inner join sf_fornecedores_despesas on id_pessoa = id_fornecedores_despesas
    inner join sf_tipo_documento t on t.id_tipo_documento = b.tipo_documento
    where b.inativo = 0 and id_venda is null and sf_fornecedores_despesas.tipo = 'C' " . $comandoX5;     
}

$sQuery2 = "SELECT count(pk) pk, sum(valor_parcela) valor_parcela, sum(valor_pago) valor_pago from (" . 
$sQuery1 . ") as x WHERE id_fornecedores_despesas > 0 " . $sWhere;
//echo $sQuery2; exit;
$cur = odbc_exec($con, $sQuery2);
while ($RFP = odbc_fetch_array($cur)) {
    $iTotal = utf8_encode($RFP['pk']);
    $totparc = utf8_encode($RFP['valor_parcela']);
    $totparcpg = utf8_encode($RFP['valor_pago']);
}
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal, 
    "iTotalDisplayRecords" => $iTotal,
    "aaData" => array()
);

$sQuery3 = "set dateformat dmy;SELECT *, 
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = id_fornecedores_despesas) 'celular',
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = id_fornecedores_despesas) 'email',
(select top 1 nome from sf_usuarios where id_usuario = id_user_resp) 'responsavel'
FROM(SELECT *, ROW_NUMBER() OVER (" . $sOrder . ") as row from (" . $sQuery1 . ") as x WHERE id_fornecedores_despesas > 0 " . $sWhere . 
") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder;
//echo $sQuery3; exit;
$cur = odbc_exec($con, $sQuery3);
while ($RFP = odbc_fetch_array($cur)) {
    $backColor = '';
    $detalhe = '';
    if ($RFP['valor_pago'] == 0) {
        if (escreverData($RFP['Vecto'], "Y-m-d") < date("Y-m-d")) {
            if (($F1 != "5" && $F1 != "6") || $imprimir == 0) {
                $backColor = " style='color:red' ";
            }
        } elseif ($RFP['Origem'] == 'M' || $RFP['Origem'] == 'B') {
            $backColor = " style='color:gray' ";
        }
    }
    $row = array();
    if ($imprimir == 0 && $F4 == "0") {
        $row[] = "<center><input id='" . utf8_encode($RFP['pk']) . "' value='" . escreverNumero($RFP['valor_parcela']) . "' cliente='" . $RFP['id_fornecedores_despesas'] . "' email='" . utf8_encode($RFP['email']) . "' celular='" . utf8_encode($RFP['celular']) . "' name='items[]' onclick='selectCheck();' type='checkbox' class='caixa'/></center>";
    } 
    $row[] = "<center><div id='formPQ' " . $backColor . ">" . escreverData($RFP['data_cadastro']) . "</div></center>";
    if (($F1 != "5" && $F1 != "6") || $imprimir == 0) {
        $row[] = "<center><div id='formPQ' title='" . utf8_encode($RFP['tdn']) . "' " . $backColor . ">" . utf8_encode($RFP['abreviacao']) . "</div></center>";
    }
    if ($F4 == "0") {
        $row[] = "<center><div id='formPQ' " . $backColor . ">" . utf8_encode($RFP['documento']) . "</div></center>";
        $row[] = "<center><div id='formPQ' " . $backColor . ">" . utf8_encode($RFP['bol_nosso_numero']) . "</div></center>";
    }
    $row[] = "<center><div id='formPQ' " . $backColor . ">" . escreverData($RFP['Vecto']) . "</div></center>";
    $row[] = "<center><div id='formPQ' " . $backColor . ">" . escreverData($RFP['data_pagamento']) . "</div></center>";
    $row[] = "<center><div id='formPQ' " . $backColor . ">" . utf8_encode($RFP['pa']) . "</div></center>";
    if (isset($_GET['tpImp']) && $_GET['tpImp'] == "E") { 
        $row[] = "<center><div id='formPQ' " . $backColor . ">" . utf8_encode($RFP['id_fornecedores_despesas']) . "</div></center>";    
    }
    $row[] = "<div id='formPQ' title='" . utf8_encode($RFP['razao_social']) . "' " . $backColor . "><a href='javascript:void(0)' onClick='AbrirBox(\"" . $RFP['tipo'] . "\"," . $RFP['id_fornecedores_despesas'] . ")'>" . utf8_encode($RFP['razao_social']) . "</a></div>";
    if (($F1 != "5" && $F1 != "6") || $imprimir == 0) {
        $row[] = "<div id='formPQ' " . $backColor . " title='" . utf8_encode($RFP['historico_baixa']) . "'>" . utf8_encode($RFP['historico_baixa']) . "</div>";
        if ($RFP['Origem'] == 'A') {
            $tipo = 'SOLICITAÇÃO DE AUTORIZAÇÃO';
        } elseif ($RFP['Origem'] == 'D') {
            $tipo = 'CONTAS FIXAS';
        } elseif ($RFP['Origem'] == 'V') {
            $tipo = 'VENDAS';
        } elseif ($RFP['Origem'] == 'M') {
            $tipo = 'MENSALIDADE';
        } elseif ($RFP['Origem'] == 'B') {
            $tipo = 'BOLETO';
        } else {
            $tipo = '-';
        }
        if ($RFP['Origem'] == 'A') {
            $tipo2 = 'SA';
        } elseif ($RFP['Origem'] == 'D') {
            $tipo2 = 'CF';
        } elseif ($RFP['Origem'] == 'V') {
            $tipo2 = 'VD';
        } elseif ($RFP['Origem'] == 'M') {
            $tipo2 = 'ME';
        } elseif ($RFP['Origem'] == 'B') {
            $tipo2 = 'BO';
        } else {
            $tipo2 = '-';
        }
        $row[] = "<center><div id='formPQ' " . $backColor . " title='" . $tipo . "'>" . $tipo2 . "</div></center>";
        $row[] = "<div id='formPQ' " . $backColor . " title='" . utf8_encode($RFP['gc']) . "'>" . utf8_encode($RFP['gc']) . "</div>";
        $row[] = "<div id='formPQ' " . $backColor . " title='" . utf8_encode($RFP['cm']) . "'>" . utf8_encode($RFP['cm']) . "</div>";
    }
    if (isset($_GET['pdf'])) {                
        $grp = array(utf8_encode($RFP['gc']), ($RFP['gc'] == "" ? "SEM GRUPO" : utf8_encode($RFP['gc'])), 10);
        if (!in_array($grp, $grupoArray)) {
            $grupoArray[] = $grp;
        }
    }          
    if ($backColor == "") {
        $aling = "style='text-align:right'";
    } else {
        $aling = substr($backColor, 0, -2) . ";text-align:right'";
    }
    $row[] = "<div id='formPQ' " . $aling . ">" . escreverNumero($RFP['valor_parcela']) . "</div>";
    $TotalPago = $TotalPago + $RFP['valor_pago'];
    $row[] = "<div id='formPQ' " . $aling . ">" . escreverNumero($RFP['valor_pago']) . "</div>";
    if ($imprimir == 0) {
        if ($F4 == "0" && (($RFP['valor_pago'] > 0 && $RFP['inativo'] != '1') || $RFP['cheque'] != "")) {
            if ($RFP['cheque'] != "") {
                $detalhe .= "<li><a onclick=\"detalheCheque(" . $RFP['cheque'] . ")\"><input type='image' src=\"../../img/doc.PNG\" width='16' height='16'> Detalhe do Cheque</a></li>";
                $detalhe .= "<li><a onclick=\"trocarCheque(" . $RFP['cheque'] . ")\"><input type='image' src=\"../../img/refresh.png\" width='16' height='16'> Trocar Cheque</a></li>";
                if ($RFP['valor_pago'] > 0) {
                    $detalhe .= "<li><a onclick=\"repassarCheque(" . $RFP['cheque'] . ")\"><input type='image' src=\"../../img/undo_256.png\" width='16' height='16'> Repassar Cheque</a></li>";
                }
            }
            if ($RFP['valor_pago'] > 0 && $RFP['inativo'] != '1') {
                $detalhe .= "<li><a href=\"Relatorio-Recibo.php?idx=" . utf8_encode($RFP['pk']) . "&tp=1\" target='_blank'><input type='image' src=\"../../img/1365123843_onebit_323 copy.PNG\" width='16' height='16'> Recibo</a></li>";
            }
            $row[] = "<center><div style=\"text-align:left;\" class=\"btn-group\">
            <button style=\"height: 13px;background-color: gray;\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\"><span style=\"margin-top: 1px;\" class=\"caret\"></span></button>
            <ul style=\"left:-130px;\" class=\"dropdown-menu\">" . $detalhe . "</ul>
            </div></center>";
        } else if ($F4 == "0" && strlen($RFP['boleto_online']) > 0) {
            $row[] = "<center><a href='" . $RFP['boleto_online'] . "' target='_blank'><img title='' src='../../img/boleto.png' value='Pago'></a><center>";
        } else if ($F4 == "0" && $RFP['bol_id_banco'] > 0) {
            $row[] = "<center><a href='./../../Boletos/Boleto.php?id=" . $RFP['pk'] . "&crt=" . $contrato . 
            "' target='_blank'><img title='Vencimento " . ($RFP['bol_data_parcela'] != "" ? escreverData($RFP['bol_data_parcela']) : escreverData($RFP['Vecto'])) . ",valor " . 
            escreverNumero($RFP['bol_valor'], 1) . "' src='../../img/" . ($RFP['abreviacao'] == "PIX" ? "pix" : ($RFP['bol_data_parcela'] == $RFP['Vecto'] ? "boleto" : "boleto2")) . ".png' value='Pago'></a><center>";
        } else if ($F4 == "0") {
            $row[] = "<center></center>";
        }
        $data2 = ($RFP['data_cadastro'] != "" ? "Data de Criação :" . escreverData($RFP['data_cadastro']) : "");
        if ($RFP['inativo'] == '1') {
            $row[] = "<center><img src='../../img/inativo.PNG' title='" . utf8_encode($RFP['obs_baixa']) . "'></center>";
        } else {
            if ($RFP['valor_pago'] > 0) {
                $row[] = "<center><img src='../../img/pago.PNG' title='" . $data2 . "" . utf8_encode($RFP['obs_baixa']) . "'></center>";
            } else {
                $row[] = "<center><img src='../../img/apagar.PNG' title='" . $data2 . "Aviso'></center>";
            }
        }
    } else {
        $row[] = "<center><div id='formPQ' " . $backColor . ">" . utf8_encode($RFP['responsavel']) . "</div></center>";        
        $row[] = "<center><div id='formPQ' " . $backColor . ">" . utf8_encode($RFP['celular']) . "</div></center>";        
        $row[] = "<center><div id='formPQ' " . $backColor . ">" . utf8_encode($RFP['email']) . "</div></center>";        
    }
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    if ($iTotal > 0) {
        $row = $output['aaData'][0];
        $row[1] = "<input type='hidden' name='totparc' id='totparc' value='" . $totparc . "'/><input type='hidden' name='totparcpg' id='totparcpg' value='" . $totparcpg . "'/>" . $row[1];
        $output['aaData'][0] = $row;
    }
    echo json_encode($output);
} else if ($F1 != "5" && $F1 != "6") {
    $output['aaTotal'][] = array(12, 12, "Sub Total", "Total", 0, 0);    
    $output['aaTotal'][] = array(13, 13, "", "", 0, 0);
    $output['aaGrupo'] = $grupoArray;        
}

odbc_close($con);