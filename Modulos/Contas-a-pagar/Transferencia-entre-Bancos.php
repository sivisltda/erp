<?php
include "../../Connections/configini.php";
$retorno = "";
$disabled = 'disabled';

if ($_GET['idx'] != '') {
    $retorno = "?idx=" . $_GET['idx'];
    if ($_GET['dtb'] != '') {
        $retorno = $retorno . "&dtb=" . $_GET['dtb'];
    }
    if ($_GET['dte'] != '') {
        $retorno = $retorno . "&dte=" . $_GET['dte'];
    }
}

if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "set dateformat dmy;update sf_transferencia_banco set " .
        "empresa = " . utf8_decode($_POST['txtEmpresa']) . "," .
        "banco_origem_trans = " . utf8_decode($_POST['txtBancoOrigem']) . "," .
        "banco_destino_trans = " . utf8_decode($_POST['txtBancoDestino']) . "," .
        "valor_trans = " . valoresNumericos("txtValor") . "," .
        "data_trans = " . valoresData("txtData") . "," .
        "descricao_trans = '" . utf8_decode($_POST['txtDescricao']) . "' where id_trans = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "set dateformat dmy;insert into sf_transferencia_banco(empresa,banco_origem_trans,banco_destino_trans,valor_trans,descricao_trans,data_trans,usuario_trans)values(" .
        $_POST['txtEmpresa'] . "," . $_POST['txtBancoOrigem'] . "," . utf8_decode($_POST['txtBancoDestino']) . "," . 
        valoresNumericos("txtValor") . ",'" . utf8_decode($_POST['txtDescricao']) . "'," . valoresData("txtData") . ",'" . $_SESSION["login_usuario"] . "')") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_trans from sf_transferencia_banco order by id_trans desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_transferencia_banco WHERE id_trans = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso');window.top.location.href = 'Fluxo-de-Caixa.php" . str_replace("idx", "id", $retorno) . "';</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_transferencia_banco where id_trans =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_trans'];
        $bancoOrigem = utf8_encode($RFP['banco_origem_trans']);
        $bancoDestino = utf8_encode($RFP['banco_destino_trans']);
        $valor = escreverNumero($RFP['valor_trans']);
        $descricao = utf8_encode($RFP['descricao_trans']);
        $empresa = utf8_encode($RFP['empresa']);
        $data = escreverData($RFP['data_trans']);
    }
} else {
    $disabled = '';
    $id = '';
    $bancoOrigem = '';
    $bancoDestino = '';
    $valor = escreverNumero(0);
    $descricao = '';
    $empresa = '';
    $data = getData("T");
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<!DOCTYPE html>
<head>
    <link rel="icon" type="image/ico" href="../../../favicon.ico"/>
    <link href="../../../css/stylesheets.css" rel="stylesheet" type="text/css" />
    <link href="../../../css/stylesheet.css" rel="stylesheet" type="text/css" />
    <link href="../../../css/main.css" rel="stylesheet">
    <link href="../../../css/bootbox.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    <style>
        .linha_transf {
            float: left;
            width: 90%;
            margin-left: 10%;
        }
    </style>
</head>
<body>
    <div class="row-fluid">
        <form id="frmTransferencia" name="frmTransferencia" action="Transferencia-entre-Bancos.php<?php echo $retorno; ?>" method="POST">
            <div class="frmhead">
                <div class="frmtext">Transferencia entre Bancos</div>
                <div class="frmicon" style="top:13px" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="data-fluid tabbable" style="height: 270px;overflow:hidden;">
                <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                <div class="linha_transf">
                    <label for="txtBancoOrigem">Banco de Origem:</label>
                    <select  name="txtBancoOrigem" <?php echo $disabled; ?> style="width: 90%;" class="input-xlarge select">
                        <option value="null">--Selecione--</option>
                        <?php
                        $cur = odbc_exec($con, "select id_bancos,razao_social from sf_bancos where ativo = 0 order by razao_social") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option value="<?php echo $RFP['id_bancos']; ?>" <?php
                            if ($RFP['id_bancos'] == $bancoOrigem) {
                                echo "SELECTED";
                            }
                            ?>><?php echo utf8_encode($RFP['razao_social']) ?></option>
                        <?php } ?> 
                    </select>                
                </div>
                <div class="linha_transf">
                    <label for="txtBancoDestino">Banco de Destino:</label>    
                    <select  name="txtBancoDestino" <?php echo $disabled; ?> style="width: 90%;" class="input-xlarge select">
                        <option value="null">--Selecione--</option>
                        <?php
                        $cur = odbc_exec($con, "select id_bancos,razao_social from sf_bancos where ativo = 0 order by razao_social") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option value="<?php echo $RFP['id_bancos']; ?>" <?php
                            if ($RFP['id_bancos'] == $bancoDestino) {
                                echo "SELECTED";
                            }
                            ?>><?php echo utf8_encode($RFP['razao_social']) ?></option>
                        <?php } ?> 
                    </select>                
                </div>
                <div class="linha_transf" style="width: 41%;">
                    <label for="txtEmpresa">Empresa:</label>
                    <select name="txtEmpresa" id="txtEmpresa" <?php echo $disabled; ?> class="select" >
                        <?php
                        $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option value="<?php echo utf8_encode($RFP['id_filial']); ?>" <?php
                            if ($empresa == $RFP['id_filial']) {
                                echo "SELECTED";
                            }
                            ?>><?php echo utf8_encode($RFP['numero_filial']); ?></option>
                        <?php } ?>
                    </select>
                </div>  
                <div class="linha_transf" style="width: 30%;">
                    <label for="txtData">Data:</label> 
                    <input name="txtData" id="txtData" <?php echo $disabled; ?> type="text" class="datepicker inputCenter" value="<?php echo $data; ?>">
                </div>                      
                <div class="linha_transf">
                    <label for="txtValor">Valor:</label> 
                    <input name="txtValor" style="width:120px;" id="txtValor" <?php echo $disabled; ?> maxlength="128" type="text" class="input-xxlarge" value="<?php echo $valor; ?>"/>
                </div>                
                <div class="linha_transf">
                    <label for="txtDescricao">Histórico:</label> 
                    <input name="txtDescricao"  id="txtDescricao" <?php echo $disabled; ?> style="width: 90%;" maxlength="128" type="text" class="input-xxlarge" value="<?php echo $descricao; ?>"/>
                </div>  
                <div style="clear:both"></div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == "") { ?>
                        <button class="btn green" type="submit" name="bntSave" id="bntSave"><span class="ico-checkmark"></span> Gravar</button>
                        <?php if ($_POST["txtId"] == "") { ?>
                            <button class="btn yellow" onClick="parent.FecharBox()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                            <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                            <?php
                        }
                    } else { ?>
                        <button class="btn green" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                        <button class="btn red" type="submit" name="bntDelete" id="bntDelete" value="Excluir"><span class=" ico-remove"></span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins.js"></script>
    <script type="text/javascript" src="../../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type="text/javascript" src="../../../js/plugins/datatables/fnReloadAjax.js"></script>
    <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../../js/jquery.priceformat.min.js'></script>
    <script type="text/javascript" src="../../../js/util.js"></script>                                              
    <script type="text/javascript">
        
        $("#txtValor").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
        $("#txtData").mask(lang["dateMask"]);
        
    </script>                                              
</body>
<?php odbc_close($con); ?>
</html>