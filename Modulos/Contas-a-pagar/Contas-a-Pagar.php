<?php
include "../../Connections/configini.php";

$F1 = '';
$F2 = '';
$F3 = '';
$F4 = '';
$DateBegin = '';
$DateEnd = '';
$sendURL = "";
$finalUrlServer = "";

if (is_numeric($_GET['id'])) {
    $F1 = $_GET['id'];
    $sendURL = $sendURL . "&idx=" . $F1;
    $finalUrlServer = $finalUrlServer . "&id=" . $F1;
}
if (is_numeric($_GET['tp'])) {
    $F2 = $_GET['tp'];
    $sendURL = $sendURL . "&tpx=" . $F2;
    $finalUrlServer = $finalUrlServer . "&tp=" . $F2;
}
if (is_numeric($_GET['td'])) {
    $F3 = $_GET['td'];
    $sendURL = $sendURL . "&tdx=" . $F3;
    $finalUrlServer = $finalUrlServer . "&td=" . $F3;
}
if (is_numeric($_GET['tpd'])) {
    $F4 = $_GET['tpd'];
    $sendURL = $sendURL . "&tpdx=" . $F4;
    $finalUrlServer = $finalUrlServer . "&tpd=" . $_GET['tpd'];
}
if (is_numeric(str_replace("_", "", $_GET['dti']))) {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
    $sendURL = $sendURL . "&dti=" . $DateBegin;
    $finalUrlServer = $finalUrlServer . "&dti=" . $DateBegin;
}
if (is_numeric(str_replace("_", "", $_GET['dtf']))) {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
    $sendURL = $sendURL . "&dtf=" . $DateEnd;
    $finalUrlServer = $finalUrlServer . "&dtf=" . $DateEnd;
}

if (isset($_POST['bntInativo'])) {
    if (isset($_POST['items'])) {
        for ($i = 0; $i < count($_POST['items']); $i++) {
            $item = explode("-", $_POST['items'][$i]);
            if ($item[0] == 'A') {
                odbc_exec($con, "UPDATE sf_solicitacao_autorizacao_parcelas SET inativo = '1' WHERE id_parcela = " . $item[1]);
            } else if ($item[0] == 'D') {
                odbc_exec($con, "UPDATE sf_lancamento_movimento_parcelas SET inativo = '1' WHERE id_parcela = " . $item[1]);
            } else if ($item[0] == 'V') {
                odbc_exec($con, "UPDATE sf_venda_parcelas SET inativo = '1' WHERE id_parcela = " . $item[1]);
            }
        }
    }
}

if (isset($_POST['bntDesfazer'])) {
    if (isset($_POST['items'])) {
        for ($i = 0; $i < count($_POST['items']); $i++) {
            $item = explode("-", $_POST['items'][$i]);
            if ($item[0] == 'A') {
                $query = "UPDATE sf_solicitacao_autorizacao_parcelas set valor_pago = 0, data_pagamento = null, id_banco = null, obs_baixa = null, syslogin = null where id_parcela = " . $item[1];
                $tabela = "sf_solicitacao_autorizacao_par";
            } else if ($item[0] == 'D') {
                $query = "UPDATE sf_lancamento_movimento_parcelas set valor_pago = 0, data_pagamento = null, id_banco = null, obs_baixa = null, syslogin = null where  id_parcela = " . $item[1];
                $tabela = "sf_lancamento_movimento_parc";
            } else if ($item[0] == 'V') {
                $query = "UPDATE sf_venda_parcelas set valor_pago = 0, data_pagamento = null, obs_baixa = null where id_parcela =  " . $item[1];
                $tabela = "sf_venda_parcelas";
            }
            $query = "set dateformat dmy;
                    BEGIN TRANSACTION
                        " . $query . "
                        insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
                        ('" . $tabela . "', " . $item[1] . ", '" . $_SESSION["login_usuario"] . "', 'A', 'DESFAZER BAIXA', GETDATE(), null);
                    IF @@ERROR = 0
                    COMMIT
                    ELSE
                    ROLLBACK;";
            odbc_exec($con, $query);
        }
    }
}

if (isset($_POST['bntExcluir'])) {
    if (isset($_POST['items'])) {
        for ($i = 0; $i < count($_POST['items']); $i++) {
            $item = explode("-", $_POST['items'][$i]);
            if ($item[0] == 'A') {
                $query = "delete from sf_solicitacao_autorizacao_parcelas WHERE id_parcela = " . $item[1];
                $queryx = "select top 1 'PARCELA EXCLUIDA: ' + pa + ' - ' + CONVERT(VARCHAR(10), data_parcela, 103) + ' - ' + FORMAT(valor_parcela, 'C', 'pt-BR') from sf_solicitacao_autorizacao_parcelas WHERE id_parcela = " . $item[1];
                $queryy = "select top 1 fornecedor_despesa from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao on sf_solicitacao_autorizacao.id_solicitacao_autorizacao = sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao WHERE id_parcela = " . $item[1];
                $tabela = "sf_solicitacao_autorizacao_parcelas";                
            } else if ($item[0] == 'D') {
                $query = "delete from sf_lancamento_movimento_parcelas WHERE id_parcela = " . $item[1];
                $queryx = "select top 1 'PARCELA EXCLUIDA: ' + pa + ' - ' + CONVERT(VARCHAR(10), data_vencimento, 103) + ' - ' + FORMAT(valor_parcela, 'C', 'pt-BR') from sf_lancamento_movimento_parcelas WHERE id_parcela = " . $item[1];
                $queryy = "select top 1 fornecedor_despesa from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento on sf_lancamento_movimento.id_lancamento_movimento = sf_lancamento_movimento_parcelas.id_lancamento_movimento WHERE id_parcela = " . $item[1];
                $tabela = "sf_lancamento_movimento_parcelas";                
            } else if ($item[0] == 'V') {
                $query = "delete from sf_venda_parcelas WHERE id_parcela = " . $item[1];
                $queryx = "select top 1 'PARCELA EXCLUIDA: ' + pa + ' - ' + CONVERT(VARCHAR(10), data_parcela, 103) + ' - ' + FORMAT(valor_parcela, 'C', 'pt-BR') from sf_venda_parcelas WHERE id_parcela = " . $item[1];
                $queryy = "select top 1 cliente_venda from sf_venda_parcelas inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda WHERE id_parcela = " . $item[1];                          
                $tabela = "sf_venda_parcelas";                
            }
            odbc_exec($con, "set dateformat dmy;
            BEGIN TRANSACTION
            insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) values
            ('" . $tabela . "', " . $item[1] . ", '" . $_SESSION["login_usuario"] . "', 'Y', (" . $queryx . "), GETDATE(), (" . $queryy . "));" . $query . ";
            IF @@ERROR = 0
            COMMIT
            ELSE
            ROLLBACK;");            
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <style>
            #formulario {
                float: left;
                width: 100%;
            }
            #formulario form .linha {
                float: left;
                height: 26px;
                width: 100%;
                display: block;
                border-bottom-width: 1px;
                border-bottom-style: solid;
                border-bottom-color: #f7f7f7;
                background-color: #f7f7f7;
                border-top-width: 1px;
                border-top-style: solid;
                border-top-color: #FFF;
                padding-top: 10px;
                padding-bottom: 10px;
            }            
            .buttonX {
                padding: 5px 12px 4px 12px;
                margin: 0 0px;
            }
        </style>        
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1>Financeiro<small>Contas a pagar</small></h1>
                    </div>
                    <div class="row-fluid">
                        <form action="Contas-a-Pagar.php?id=<?php echo $F1 . "&tp=" . $F2 . "&td=" . $F3 . "&tpd=" . $F4 . "&dti=" . str_replace("/", "_", $DateBegin) . "&dtf=" . str_replace("/", "_", $DateEnd); ?>" method="POST">
                            <div class="span12">
                                <div class="block">
                                    <div id="formulario">
                                        <div class="boxfilter">
                                            <?php if ($ckb_cpagar_read_ == 0) { ?>
                                            <div style="float: left;">
                                                <button type="button" name="bntBaixaLoad" id="bntBaixaLoad" title="Baixar" class="button button-green btn-primary buttonX" disabled="true"><span class="icon-ok icon-white"></span></button>
                                                <button type="button" name="bntRenegociar" id="bntRenegociar" title="Renegociar" class="button button-red btn-primary buttonX" onclick="renegociar()" disabled="true"><span class="icon-resize-small icon-white"></span></button>
                                                <?php if ($ckb_exc_cpagar_ > 0) { ?>
                                                <button type="submit" style="display: none;"  name="bntExcluir" id="bntExcluir" title="Excluir" class="button button-red btn-primary buttonX" disabled="true" value="Excluir" onClick="return confirm('Deseja excluir esse(s) registro(s)?')" ><span class="ico-ban-circle">&nbsp;Excluir</span></button>
                                                <?php } ?>
                                                <button type="submit" name="bntInativo" id="bntInativo" title="Cancelar" class="button button-red btn-primary buttonX" disabled="true" value="Cancelar" onClick="return confirm('Deseja cancelar esse registro?')" ><span class="ico-ban-circle"> </span></button>
                                                <button type="submit" style="display: none;" name="bntDesfazer" id="bntDesfazer" title="Desfazer" class="button button-orange btn-primary buttonX" disabled="true" value="Desfazer" onClick="return confirm('Deseja desfazer a baixa deste registro?')" ><span class="ico-undo"> </span></button>
                                                <button type="button" style="display: none;" name="btnDocs" id="btnDocs" class="button button-red btn-primary" onclick="documentos();" title="Imprimir Recibos"><span class="ico-print icon-white"></span></button>                                                
                                            </div>
                                            <?php } ?>                                            
                                            <div style="float: right;">
                                                <select style="width: 100px;vertical-align: top;" name="jumpMenu" id="jumpMenu">
                                                    <option value="" >Selecione</option>
                                                    <option value="1" <?php
                                                    if ($F1 == 1) {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>A Pagar</option>
                                                    <option value="2" <?php
                                                    if ($F1 == 2) {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Pagas</option>
                                                    <option value="3" <?php
                                                    if ($F1 == 3) {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Canceladas</option>
                                                    <option value="4" <?php
                                                    if ($F1 == 4) {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Todas</option>
                                                </select>
                                                <select style="width: 160px;vertical-align: top;" name="txtTipoBusca" id="txtTipoBusca">
                                                    <option value="Contas-a-Pagar.php?id=<?php echo $F1; ?>" >--Selecione--</option>
                                                    <option value="0" <?php
                                                    if ($F2 == "0") {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Cli/For/Func/Tra</option>
                                                    <option value="6" <?php
                                                    if ($F2 == "6") {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Centro de Custos</option>                                                    
                                                    <option value="1" <?php
                                                    if ($F2 == "1") {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Grupo de Conta</option>
                                                    <option value="2" <?php
                                                    if ($F2 == "2") {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Subgrupo de Conta</option>
                                                    <option value="3" <?php
                                                    if ($F2 == "3") {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Forma de Pagamento</option>
                                                </select>
                                                <input id="txtValue" name="txtValue" type="hidden" value="<?php echo $F3; ?>"/>
                                                <input style="width: 160px;vertical-align: top;color:#000 !important;" type="text" name="txtDesc" id="txtDesc" class="inputbox" value="" size="10" />
                                                <span style="display: none;">
                                                    <select name="itemsGrupo[]" id="mscGrupo" multiple="multiple" class="select" style="min-width:160px; min-height: 29px;" class="input-medium"></select>                                                            
                                                </span>                                                    
                                                <select id="txtTipoData" style="width: 80px;vertical-align: top;">
                                                    <option value="0" <?php
                                                    if ($F4 == "0") {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Dt.Venc.</option>
                                                    <option value="1" <?php
                                                    if ($F4 == "1") {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Dt.Pag.</option>
                                                    <option value="2" <?php
                                                    if ($F4 == "2") {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Dt.Cad.</option>
                                                </select>
                                                <input type="text" style="width: 79px;vertical-align: top;" id="txt_dt_begin" class="datepicker" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial"> até                                                    
                                                <input type="text" style="width: 79px;vertical-align: top;" class="datepicker" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final">                                                    
                                                <button name="btnfind" class="button button-turquoise btn-primary buttonX" type="button" onclick="refreshFind();" title="Buscar"><span class="ico-search icon-white"></span></button>
                                                <button type="button" name="btnPrint" onclick="imprimir('I')" class="button button-blue btn-primary buttonX" title="Imprimir"><span class="ico-print"></span></button>
                                                <button id="btnExcel" class="button button-blue btn-primary" type="button" onclick="imprimir('E')" title="Exportar Excel"><span class="ico-download-3"></span></button>                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="block">
                                            <div class="boxhead">
                                                <div class="boxtext">Contas a Pagar</div>
                                            </div>
                                            <div class="data-fluid">
                                                <table class="table" cellpadding="0" cellspacing="0" width="100%" id="example">
                                                    <thead>
                                                        <tr>
                                                            <th width="1%"><input id="checkAll" type="checkbox" class="checkall" onClick="
                                                                if (document.getElementById('checkAll').checked === true) {
                                                                    $('#bntBaixaLoad').attr('onclick', 'AbrirBox(1,0)');
                                                                    if (todosCheckbox() != '') {
                                                                        if (document.getElementById('bntBaixaLoad') != undefined) {
                                                                            document.getElementById('bntBaixaLoad').disabled = false;
                                                                        }
                                                                        if (document.getElementById('bntRenegociar') != undefined) {
                                                                            document.getElementById('bntRenegociar').disabled = true;
                                                                        }
                                                                        if (document.getElementById('bntInativo') != undefined) {
                                                                            document.getElementById('bntInativo').disabled = false;
                                                                        }
                                                                        if (document.getElementById('bntDesfazer') != undefined) {
                                                                            document.getElementById('bntDesfazer').disabled = false;
                                                                        }
                                                                        if (document.getElementById('btnDocs') != undefined) {
                                                                            document.getElementById('btnDocs').disabled = false;
                                                                        }
                                                                        if (document.getElementById('bntExcluir') != undefined) {
                                                                            document.getElementById('bntExcluir').disabled = false;
                                                                        }
                                                                    }
                                                                } else {
                                                                    $('#lblTotParc2').html('Total selecionado :<strong>' + numberFormat(0, 1) + '</strong>').show('slow');
                                                                    $('#bntBaixaLoad').attr('onclick', '');
                                                                    if (document.getElementById('bntRenegociar') != undefined) {
                                                                        document.getElementById('bntRenegociar').disabled = true;
                                                                    }
                                                                    if (document.getElementById('bntInativo') != undefined) {
                                                                        document.getElementById('bntInativo').disabled = true;
                                                                    }
                                                                    if (document.getElementById('bntDesfazer') != undefined) {
                                                                        document.getElementById('bntDesfazer').disabled = true;
                                                                    }
                                                                    if (document.getElementById('btnDocs') != undefined) {
                                                                        document.getElementById('btnDocs').disabled = true;
                                                                    }
                                                                    if (document.getElementById('bntBaixaLoad') != undefined) {
                                                                        document.getElementById('bntBaixaLoad').disabled = true;
                                                                    }
                                                                    if (document.getElementById('bntExcluir') != undefined) {
                                                                        document.getElementById('bntExcluir').disabled = true;
                                                                    }
                                                                }"/>
                                                            </th>
                                                            <th width="5%">Dt.Cad</th>
                                                            <th width="5%">F.Pg</th>
                                                            <th width="8%">Nr.Doc</th>
                                                            <th width="6%">Vecto</th>
                                                            <th width="6%">Pagto</th>
                                                            <th width="4%">Parc.</th>
                                                            <th width="12%">Fornecedor</th>
                                                            <th width="12%">Histórico</th>
                                                            <th width="3%">Org.</th>
                                                            <th width="8%">Grupo</th>
                                                            <th width="9%">Contas</th>
                                                            <th style="text-align:left" width="9%">Parc.</th>
                                                            <th style="text-align:left" width="9%">Baixa</th>
                                                            <th width="5%">Doc</th>
                                                            <th width="4%">Aviso</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="5" class="dataTables_empty">Carregando dados do Cliente</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <table id="lblTotParc" name="lblTotParc" class="table" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td style="text-align: right"><div id="lblTotParc2" name="lblTotParc2">
                                                            Total selecionado :<strong><?php echo escreverNumero(0, 1); ?></strong>
                                                        </div></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right"><div id="lblTotParc3" name="lblTotParc3">
                                                            Total em Parcelas :<strong><?php echo escreverNumero(0, 1); ?></strong></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right"><div id="lblTotParc4" name="lblTotParc4">
                                                            Total Recebido :<strong><?php echo escreverNumero(0, 1); ?></strong></div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="widgets"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
            <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
            <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
            <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
            <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
            <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
            <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
            <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
            <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
            <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
            <script type='text/javascript' src='../../js/plugins.js'></script>
            <script type='text/javascript' src='../../js/charts.js'></script>
            <script type='text/javascript' src='../../js/actions.js'></script>
            <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
            <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
            <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
            <script type='text/javascript' src='../../js/plugins/highlight/jquery.highlight-4.js'></script>
            <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
            <script type='text/javascript' src='../../js/util.js'></script>        
            <script type='text/javascript'>
                $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
                formaPagamento();
                
                function formaPagamento() {
                    $('#mscGrupo').empty();
                    $("#mscGrupo").select2('data', null);
                    $.getJSON("./../CRM/ajax.php", {q: "", t: "FPC"}, function (j) {
                        var options = "";
                        for (var i = 0; i < j.length; i++) {
                            options += "<option value='" + j[i].id + "'>" + j[i].value + "</option>";
                        }
                        $("#mscGrupo").append(options);
                        $('#mscGrupo').trigger('change');
                    });
                }
                            
                function documentos() {
                    let itens = $("input[name='items[]']:checked").map(function () { 
                        return this.value; 
                    }).get().join(',');                    
                    window.open("../Seguro/Taxas_Recibo.php?itens=" + itens, '_blank');
                }
            
                $('#txtTipoBusca').change(function () {
                    $("#s2id_mscGrupo").select2("val", "");
                    $('#txtDesc').val("");
                    $('#txtValue').val("");
                    if ($(this).val() === "3") {
                        $("#s2id_mscGrupo").parent().show();
                        $('#txtDesc').hide();
                    } else {
                        $("#s2id_mscGrupo").parent().hide();
                        $('#txtDesc').show();
                    }
                });
                
                function renegociar() {
                    var idFind = '';
                    var inputs, x;
                    inputs = document.getElementsByTagName('input');
                    for (x = 0; x < inputs.length; x++) {
                        if (inputs[x].type == 'checkbox') {
                            if (inputs[x].checked == true && inputs[x].id == 'check') {
                                idFind = inputs[x].value;
                            }
                        }
                    }
                    $.getJSON("renegociar.ajax.php", {txtId: idFind, txtFr: 'P', ajax: "true"}, function (j) {
                        window.location = "" + j[0].plugue + "<?php echo $sendURL; ?>";
                    });
                }
                
                function contaCheckbox() {
                    var inputs, x, selecionados = 0;
                    inputs = document.getElementsByTagName('input');
                    for (x = 0; x < inputs.length; x++) {
                        if (inputs[x].type == 'checkbox') {
                            if (inputs[x].checked == true && inputs[x].id == 'check') {
                                selecionados++;
                            }
                        }
                    }
                    return selecionados;
                }
                
                function mostraCheckbox() {
                    var inputs, x, toReturn, totSel;
                    toReturn = '';
                    totSel = 0;
                    inputs = document.getElementsByTagName('input');
                    for (x = 0; x < inputs.length; x++) {
                        if (inputs[x].type == 'checkbox') {
                            if (inputs[x].checked == true && inputs[x].id == 'check') {
                                totSel = totSel + parseFloat(document.getElementById(inputs[x].value).value);
                                toReturn = toReturn + inputs[x].value + '|';
                            }
                        }
                    }
                    $('#lblTotParc2').html('Total selecionado :<strong>' + numberFormat(totSel, 1) + '</strong>').show("slow");
                    return toReturn;
                }
                
                function todosCheckbox() {
                    var inputs, x, toReturn, totSel;
                    toReturn = '';
                    totSel = 0;
                    inputs = document.getElementsByTagName('input');
                    for (x = 0; x < inputs.length; x++) {
                        if (inputs[x].type == 'checkbox') {
                            if (inputs[x].id == 'check') {
                                totSel = totSel + parseFloat(document.getElementById(inputs[x].value).value);
                                toReturn = toReturn + inputs[x].value + '|';
                            }
                        }
                    }
                    $('#lblTotParc2').html('Total selecionado :<strong>' + numberFormat(totSel, 1) + '</strong>').show("slow");
                    return toReturn;
                }
                
                $(function () {
                    $("#txtDesc").autocomplete({
                        source: function (request, response) {
                            $.ajax({
                                url: "./../CRM/ajax.php",
                                dataType: "json",
                                data: {
                                    q: request.term,
                                    t: typeSelect()
                                },
                                success: function (data) {
                                    response(data);
                                }
                            });
                        },
                        minLength: 3,
                        select: function (event, ui) {
                            $("#txtValue").val(ui.item.id);
                            refreshFind();
                        }
                    });
                });
                
                function ajustarBotoes() {
                    $('#bntDesfazer, #btnDocs').hide();
                    if ($("#jumpMenu").val() === "3") {
                        $('#bntBaixaLoad').hide();
                        $('#bntRenegociar').hide();
                        $('#bntExcluir').show();
                        $('#bntInativo').hide();
                    } else {
                        $('#bntBaixaLoad').show();
                        if ($("#jumpMenu").val() === "1") {
                            $('#bntRenegociar').show();
                        } else {
                            $('#bntRenegociar').hide();
                        }
                        if ($("#jumpMenu").val() === "2") {
                            $('#bntDesfazer, #btnDocs').show();
                        }
                        $('#bntExcluir').hide();
                        $('#bntInativo').show();
                    }
                }
                
                function typeSelect() {
                    if ($("#txtTipoBusca").val() === "1") {
                        return "GCD";
                    } else if ($("#txtTipoBusca").val() === "2") {
                        return "CMD";
                    } else if ($("#txtTipoBusca").val() === "3") {
                        return "FPD";
                    } else if ($("#txtTipoBusca").val() === "6") {
                        return "CCU";
                    } else {
                        return "C','F','E','T";
                    }
                }
                
                function refreshFind() {
                    ajustarBotoes();
                    var oTable = $('#example').dataTable({
                        "bServerSide": true,
                        "iDisplayLength": 20,
                        "aLengthMenu": [20, 30, 40, 50, 100],
                        "bProcessing": true,
                        "sAjaxSource": finalFind(0),
                        'oLanguage': {
                            'oPaginate': {
                                'sFirst': "Primeiro",
                                'sLast': "Último",
                                'sNext': "Próximo",
                                'sPrevious': "Anterior"
                            }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                            'sLengthMenu': "Visualização de _MENU_ registros",
                            'sLoadingRecords': "Carregando...",
                            'sProcessing': "Processando...",
                            'sSearch': "Pesquisar:",
                            'sZeroRecords': "Não foi encontrado nenhum resultado"},
                        "fnInitComplete": function (oSettings, json) {
                            $("#del_check,#del_boleto,#del_aviso").attr("class", "");
                            refreshTotal();
                        },
                        "sPaginationType": "full_numbers",
                        "bDestroy": true
                    });
                }
                
                function refreshTotal() {
                    $('#lblTotParc2').html('Total selecionado :<strong>' + numberFormat(0, 1) + '</strong>').show('slow');
                    $('#lblTotParc3').html('Total em Parcelas :<strong>' + numberFormat($('#totparc').val(), 1) + '</strong>').show('slow');
                    $('#lblTotParc4').html('Total Recebido :<strong>' + numberFormat($('#totparcpg').val(), 1) + '</strong>').show('slow');
                    if (document.getElementById('bntRenegociar') != undefined) {
                        document.getElementById('bntRenegociar').disabled = true;
                    }
                    if (document.getElementById('bntInativo') != undefined) {
                        document.getElementById('bntInativo').disabled = true;
                    }
                    if (document.getElementById('bntDesfazer') != undefined) {
                        document.getElementById('bntDesfazer').disabled = true;
                    }
                    if (document.getElementById('btnDocs') != undefined) {
                        document.getElementById('btnDocs').disabled = true;
                    }
                    if (document.getElementById('bntBaixaLoad') != undefined) {
                        document.getElementById('bntBaixaLoad').disabled = true;
                    }
                    if (document.getElementById('bntExcluir') != undefined) {
                        document.getElementById('bntExcluir').disabled = true;
                    }
                }
                
                function finalFind(imp) {
                    var retorno = "";
                    if ($('#jumpMenu').val() !== "") {
                        retorno = retorno + "&id=" + $('#jumpMenu').val();
                    }
                    if ($('#txtTipoBusca').val() !== "") {
                        retorno = retorno + "&tp=" + $('#txtTipoBusca').val();
                    }
                    if ($('#txtDesc').val() !== "") {
                        retorno = retorno + "&td=" + $('#txtValue').val();
                    }
                    if ($("#mscGrupo").val() !== null) {
                        var grupos = JSON.stringify($("#mscGrupo").val());
                        retorno = retorno + "&grp=" + grupos;
                    }
                    if ($('#txtTipoData').val() !== "") {
                        retorno = retorno + "&tpd=" + $('#txtTipoData').val();
                    }
                    if ($('#txt_dt_begin').val() !== "") {
                        retorno = retorno + "&dti=" + $('#txt_dt_begin').val();
                    }
                    if ($('#txt_dt_end').val() !== "") {
                        retorno = retorno + "&dtf=" + $('#txt_dt_end').val();
                    }
                    return "Contas-a-Pagar_server_processing.php?filial=" + $("#txtLojaSel").val() + "&imp=" + imp + retorno.replace(/\//g, "_") + "&ord=<?php echo $_POST['ordem']; ?>";
                }
                
                function imprimir(tp) {
                    var pRel = "&NomeArq=" + "Contas a Pagar" +
                            "&lbl=" + "Dt.Cd|F.Pg|Nr.Doc|Vecto|Pagto|Parc|Fornecedor|Histórico|Org|Grupo|Contas|Parc|Baixa|Doc" + (tp === "E" ? "|Centro de Custos" : "") +
                            "&pOri=L" + //parametro de paisagem
                            "&siz=" + "50|30|50|50|50|30|190|110|40|110|130|60|60|50" + (tp === "E" ? "|100" : "") +
                            "&pdf=" + "16" + // Colunas do server processing que não irão aparecer no pdf 9
                            "&filter=" + "Contas a Pagar " + //Label que irá aparecer os parametros de filtro
                            "&PathArqInclude=../Modulos/Contas-a-pagar/" + finalFind(1).replace("?", "&"); // server processing            
                    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tp + "&PathArq=GenericModelPDF.php", '_blank');
                }                
                
                $(document).ready(function () {
                    $('#example').dataTable({
                        "iDisplayLength": 20,
                        "aLengthMenu": [20, 50, 500],
                        "bProcessing": true,
                        "bServerSide": true,
                        "sAjaxSource": finalFind(0),
                        'oLanguage': {
                            'oPaginate': {
                                'sFirst': "Primeiro",
                                'sLast': "Último",
                                'sNext': "Próximo",
                                'sPrevious': "Anterior"
                            }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                            'sLengthMenu': "Visualização de _MENU_ registros",
                            'sLoadingRecords': "Carregando...",
                            'sProcessing': "Processando...",
                            'sSearch': "Pesquisar:",
                            'sZeroRecords': "Não foi encontrado nenhum resultado"},
                        "fnInitComplete": function (oSettings, json) {
                            $("#del_check,#del_boleto,#del_aviso").attr("class", "");
                            refreshTotal();
                        },
                        "sPaginationType": "full_numbers"
                    });
                });
                
                function AbrirBox(opc, id) {
                    if (opc == 1) {
                        abrirTelaBox("FormContas-a-Pagar.php?id=" + mostraCheckbox(), 385, 400);
                    }
                    if (opc == 2) {
                        $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:717px; height:550px; position:absolute; top:50%; left:50%; margin-left:-358px; margin-top:-275px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:550px' src='./../Financeiro/FormFornecedores-de-despesas.php?id=" + id + "&idx=0'></iframe></div></div></div></div></div>");
                    }
                    if (opc == 3) {
                        abrirTelaBox("FormContas-a-Pagar-envio.php?id=" + id, 435, 500);
                    }
                    if (opc == 7) {
                        var retPrint = "";
                        if ($('#jumpMenu').val() != "") {
                            retPrint = retPrint + "&id=" + $('#jumpMenu').val();
                        }
                        if ($('#txtTipoBusca').val() != "") {
                            retPrint = retPrint + "&tp=" + $('#txtTipoBusca').val();
                        }
                        if ($('#txtDesc').val() != "") {
                            retPrint = retPrint + "&td=" + $('#txtValue').val();
                        }
                        if ($('#txtTipoData').val() != "") {
                            retPrint = retPrint + "&tpd=" + $('#txtTipoData').val();
                        }
                        if ($('#txt_dt_begin').val() != "") {
                            retPrint = retPrint + "&dti=" + $('#txt_dt_begin').val().replace(/\//g, "_");
                        }
                        if ($('#txt_dt_end').val() != "") {
                            retPrint = retPrint + "&dtf=" + $('#txt_dt_end').val().replace(/\//g, "_");
                        }
                        window.open("Contas-a-Pagar.php?imp=1" + retPrint, "_blank");
                    }
                }
                
                function FecharBox(opc) {
                    $("#newbox").remove();
                    if (opc == 1) {
                        var oTable = $('#example').dataTable();
                        oTable.fnDraw(false);
                    }
                }
            </script>            
            <?php odbc_close($con); ?>
    </body>
</html>
