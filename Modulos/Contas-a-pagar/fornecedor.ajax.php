<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include "../../Connections/configini.php";

$id_area = "'F','E','T'";
if ($_REQUEST['txtTipo'] == "C") {
    $id_area = "'C'";
} else {
    $id_area = "'F','E','T'";
}
if ($_REQUEST['txtBloq'] == "S") {
    $whereX = " and bloqueado = 0 ";
}
$local = array();
$sql = "select id_fornecedores_despesas,razao_social,nome_fantasia from sf_fornecedores_despesas where tipo in (" . $id_area . ") " . $whereX . " ORDER BY razao_social";
$cur = odbc_exec($con, $sql) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $local[] = array('id_grupo_contas' => $RFP['id_fornecedores_despesas']
        , 'descricao' => utf8_encode($RFP['razao_social'])
        , 'nome_fantasia' => utf8_encode($RFP['nome_fantasia']));
}
echo(json_encode($local));
odbc_close($con);
