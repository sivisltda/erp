<?php

require_once(__DIR__ . '/../../Connections/configini.php');
require_once(__DIR__ . '/../../util/util.php');

function formataNumeros($valor) {
    if ($_GET['ts'] > 0) {
        return escreverNumero($valor, 1);
    } else {
        return $valor;
    }
}

$F1 = ""; //Usuário
$F2 = 2; //0-Todos, 1-Produtos, 2-Serviços, 3-Planos
$F3 = 0; //1-Professor, 2-Usuário | 0-Vendedores, 1-Comissionado, 2-Indicadores, 3-Gerentes 
$F4 = 0; //1-Sintetico, 2-Analítico, 3-Comissão
$F5 = ""; //Não definido no Produtividade
$F6 = 0; //0-Venda, 2-Recebimento, 1-Provisão, 3-Previsão
$imprimir = 0;
$mdl_aca_ = returnPart($_SESSION["modulos"], 6);
$mdl_seg_ = returnPart($_SESSION["modulos"], 14);
$DateBegin = getData("B") . " 00:00:00";
$DateEnd = getData("E") . " 23:59:59";
$from = "";
$comando = "";
$comandoX = "";
$union = "";
$order = "data_venda";
$campoNm = "vendedor";
$grupoArray = [];
$tempTable = "";

if (is_numeric($_GET['imp']) && $_GET['imp'] == "1") {
    $imprimir = 1;
}
   
if ($_GET['id'] != "") {
    $F1 = $_GET['id'];
}

if (is_numeric($_GET['fm'])) {
    $F2 = $_GET['fm'];
    if ($F2 == 1) {
        $comando .= " and p.tipo='P' ";
    } elseif ($F2 == 2) {
        $comando .= " and p.tipo='S' ";
    } elseif ($F2 == 3) {
        $comando .= " and p.tipo='C' ";
    }
}

if (is_numeric($_GET['tp'])) {
    $F3 = $_GET['tp'];
}

if (is_numeric($_GET['ts'])) {
    $F4 = $_GET['ts'];
}

if (is_numeric($_GET['pg'])) {
    $F5 = $_GET['pg'];
    if ($F5 == 1) {
        $comando .= " AND sa.status = 'Aprovado' ";
    } elseif ($F5 == 2) {
        $comando .= " AND sa.status is null ";
    } elseif ($F5 == 3) {
        $comando .= " AND sa.status = 'Aguarda' ";
    } elseif ($F5 == 4) {
        $comando .= " AND sa.status = 'Reprovado' ";
    }
}

if (is_numeric($_GET['pj'])) {
    $F6 = $_GET['pj'];
    if ($F6 == 1) {
        $comandoX .= " AND (pp.parcela in (-1,0,1) or p.parcelar = 1)";
    }
}

if (is_numeric($_GET['ord']) && $_GET['ord'] == "1") {
    $order = "cliente_venda";
}

if (isset($_GET['plan'])) {
    $comando .= " and p.conta_produto in (" . str_replace("','", ",", $_GET['plan']) . ")";
}

if (isset($_GET['grp'])) {
    $comando .= " and p.conta_movimento in (" . str_replace("\"", "'", str_replace(array("[", "]"), "", $_GET["grp"])) . ")";
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']) . " 00:00:00";
    $DateEnd = str_replace("_", "/", $_GET['dtf']) . " 23:59:59";
    $comandoX .= " and " . ($F6 == 2 ? "data_pagamento" : ($F6 == 3 ? "dt_inicio_mens" : "data_venda")) . " between " . valoresDataHoraUnico2($DateBegin) . " and " . valoresDataHoraUnico2($DateEnd);
}

if (is_numeric($_GET['emp'])) {
    if ($F6 == 3) {
        $comando .= " AND f.empresa = " . $_GET['emp'];
    } else {
        $comando .= " AND sf_vendas.empresa = " . $_GET['emp'];   
    }
}

$groupBegin = "set dateformat dmy;";
if ($F6 == 3) { //Previsão
    $select = "select id_mens id_item_venda,id_plano id_venda,0 cod_pedido,dt_inicio_mens data_venda,'' vendedor,
    g.descricao_grupo,ts.descricao_tipo,f.razao_social,p.descricao,e.razao_social nome,valor_mens valor_total,valor_custo_medio,valor_comissao_venda,1 quantidade,
    e.tbcomiss_prod,e.tbcomiss_serv,e.tbcomiss_gere,e.tbcomiss_plano,e.comiss_prod,e.comiss_serv,e.comiss_gere,
    e.comiss_plano,pp.valor_comissao,e.recebe_comissao,p.tipo,0 total_pg,
    null comissao,null id_sa,favorecido cliente_venda,pp.parcela,p.conta_produto ";
} else {
    $select = "select vi.id_item_venda,sf_vendas.id_venda,cod_pedido," . ($F6 == 2 ? "data_pagamento" : "data_venda") . " data_venda,vendedor,g.descricao_grupo,ts.descricao_tipo,f.razao_social,p.descricao,e.razao_social nome," .
    ($F6 == 2 ? "dbo.CALC_PROPORCAO(valor_pago,valor_total,(select isnull(sum(valor_total),0) from sf_vendas_itens where id_venda = sf_vendas.id_venda))" : "valor_total") .
    " valor_total,valor_custo_medio,valor_comissao_venda,quantidade,e.tbcomiss_prod,e.tbcomiss_serv,e.tbcomiss_gere,e.tbcomiss_plano,e.comiss_prod,e.comiss_serv,e.comiss_gere,
    e.comiss_plano,pp.valor_comissao,e.recebe_comissao,p.tipo,isnull((select count(*) from sf_solicitacao_autorizacao_parcelas where data_pagamento is not null and solicitacao_autorizacao = sa.id_solicitacao_autorizacao),0) total_pg,
    sa.status comissao,sa.id_solicitacao_autorizacao id_sa,sf_vendas.cliente_venda,pp.parcela,p.conta_produto, vi.valor_multa, ((vi.valor_bruto + vi.valor_multa) - vi.valor_total) desconto, vi.valor_bruto, pm.dt_inicio_mens ";
}    
$groupEnd = "";
if ($F4 == 1) {
    $tempTable = " into #tempTable ";
} elseif ($F4 == 3) { //Relatório de Comissão
    if ($mdl_aca_ == 1 && $mdl_seg_ == 0 && $F3 == 1) {
        $campoNm = "e.razao_social";
    } else if ($mdl_aca_ == 1 && $mdl_seg_ == 0 && $F3 == 2) {
        $campoNm = "u.login_user";
    }
    $select = "select sf_vendas.id_venda,sf_vendas.cod_pedido,sf_vendas.data_venda," . $campoNm . " vendedor,g.descricao_grupo,ts.descricao_tipo,f.razao_social
    ,sum(case when p.tipo = 'P' then valor_total else 0 end) tproduto
    ,sum(case when p.tipo = 'S' then valor_total else 0 end) tservico
    ,sum(case when p.tipo = 'C' then valor_mens else 0 end) tplano
    ,sum(case when p.tipo = 'P' then valor_custo_medio * quantidade else 0 end) dproduto
    ,sum(case when p.tipo = 'S' then valor_custo_medio * quantidade else 0 end) dservico
    ,sum(case when p.tipo = 'C' then valor_custo_medio * quantidade else 0 end) dplan
    ,sum(case when p.tipo = 'P' and e.recebe_comissao = 1 then ((valor_total - (valor_custo_medio * quantidade)) * case when e.tbcomiss_prod = 1 then valor_comissao_venda when e.tbcomiss_prod = 2 then e.comiss_prod else 0 end)/100 else 0 end) cproduto
    ,sum(case when p.tipo = 'S' and e.recebe_comissao = 1 then ((valor_total - (valor_custo_medio * quantidade)) * case when e.tbcomiss_serv = 1 then valor_comissao_venda when e.tbcomiss_serv = 2 then e.comiss_serv else 0 end)/100 else 0 end) cservico
    ,sum(case when p.tipo = 'C' and e.recebe_comissao = 1 then ((valor_total - (valor_custo_medio * quantidade)) * case when e.tbcomiss_plano = 1 then valor_comissao_venda when e.tbcomiss_plano = 2 then e.comiss_plano else 0 end)/100 else 0 end) cplano
    ,isnull((select count(*) from sf_solicitacao_autorizacao_parcelas where data_pagamento is not null and solicitacao_autorizacao = sa.id_solicitacao_autorizacao),0) total_pg
    ,sa.status comissao,sa.id_solicitacao_autorizacao id_sa, sf_vendas.cliente_venda";
}
if ($F6 == 3) { //Previsão
    $from .= " from sf_vendas_planos vp 
    inner join sf_fornecedores_despesas f on vp.favorecido = f.id_fornecedores_despesas        
    inner join sf_vendas_planos_mensalidade pm on id_plano = id_plano_mens
    left join sf_produtos_parcelas pp on pm.id_parc_prod_mens = pp.id_parcela
    left join sf_grupo_cliente g on g.id_grupo = f.grupo_pessoa
    left join sf_tipo_socio ts on ts.id_tipo = f.tipo_socio
    inner join sf_produtos p on p.conta_produto = id_prod_plano";
} else {
    $from .= " from sf_vendas 
    inner join sf_fornecedores_despesas f on sf_vendas.cliente_venda = f.id_fornecedores_despesas
    inner join sf_vendas_itens vi on vi.id_venda = sf_vendas.id_venda and vi.valor_total > 0       
    left join sf_vendas_planos_mensalidade pm on vi.id_item_venda = pm.id_item_venda_mens
    left join sf_produtos_parcelas pp on pm.id_parc_prod_mens = pp.id_parcela
    left join sf_grupo_cliente g on g.id_grupo = f.grupo_pessoa    
    left join sf_tipo_socio ts on ts.id_tipo = f.tipo_socio    
    inner join sf_produtos p on p.conta_produto = vi.produto";
}
if ($mdl_aca_ == 1 && $mdl_seg_ == 0 && $F3 == 1) { //Referencia a Professor
    $from .= " left join sf_fornecedores_despesas e on f.prof_resp = e.id_fornecedores_despesas";
} else if ($mdl_aca_ == 1 && $mdl_seg_ == 0 && $F3 == 2) { //Referencia a Usuário
    $from .= " inner join sf_usuarios u on u.id_usuario = f.id_user_resp
    left join sf_fornecedores_despesas e on u.funcionario = e.id_fornecedores_despesas";
} else if ($F6 != 3) { //Outra Referencia com venda
    $from .= " left join sf_usuarios u on UPPER(u.login_user) = UPPER(sf_vendas.vendedor)
    left join sf_fornecedores_despesas e on vi.vendedor_comissao = e.id_fornecedores_despesas";
}
if ($F6 == 2) { //Recebimento
    $from .= " inner join sf_venda_parcelas vp on vp.venda = sf_vendas.id_venda and vp.tipo_documento not in (12) ";
}
if ($F6 == 3) {
    $from .= " where dt_cancelamento is null and f.inativo = 0 and dt_pagamento_mens is null ";
} else {
    $from .= " left join sf_comissao_historico ch on sf_vendas.id_venda = ch.hist_venda and ch.hist_vendedor = '" . str_replace("'", "", $F1) . "'
    left join sf_solicitacao_autorizacao sa on sa.id_solicitacao_autorizacao = ch.hist_solicitacao_autorizacao
    where sf_vendas.status = 'Aprovado' and cov = 'V' ";
}
//------------------------------------------------------------------------------
if ($F3 == 1 && $F1 != "0") {  //Referencia a Professor
    if ($mdl_aca_ == 1 && $mdl_seg_ == 0) {
        $query .= "and f.prof_resp = " . $F1 . $comando . "";
    } else if($F6 != 3) {
        $query .= "and vi.vendedor_comissao = '" . str_replace("'", "", $F1) . "' " . $comando . "";
    }
} else if ($F3 == 2 && $F1 != "0") { //Referencia a Usuário
    if ($mdl_aca_ == 1 && $mdl_seg_ == 0) {
        $query .= "and f.id_user_resp = " . $F1 . $comando . "";
    } else if ($F6 != 3) {
        $query .= "and sf_vendas.indicador = '" . str_replace("'", "", $F1) . "' " . $comando . "";
    }
} else if ($F1 != "0") {
    if ($mdl_aca_ == 0 && $F6 != 3) {
        $query .= "and UPPER(sf_vendas.vendedor) = UPPER('" . str_replace("'", "", $F1) . "') " . $comando . "";
    }
} else {
    $query .= $comando . "";
}
//------------------------------------------------------------------------------
if ($F4 == 1) { //Sintetico
    $order = " order by valor_pago desc";
} elseif ($F4 == 2) { //Analítico
    $order = " order by " . $order;
} elseif ($F4 == 3 && $F6 != 3) { //Comissão
    $order = " group by sf_vendas.id_venda,sf_vendas.cod_pedido,sf_vendas.data_venda," . $campoNm . ",g.descricao_grupo,ts.descricao_tipo,f.razao_social,sa.status,sa.id_solicitacao_autorizacao,sf_vendas.cliente_venda order by data_venda";
}
if ($F4 != 3 && $F6 == 1) { //Provisionar Planos
    $contractDateBegin = geraTimestamp($DateBegin);
    $contractDateEnd = geraTimestamp($DateEnd);    
    $queryX = "set dateformat dmy;" . str_replace("valor_total valor_total", "(valor_total/pp.parcela) valor_total", $select) . " into #tempProdutividade " . $from . $query . " and (" . valoresDataHoraUnico2($DateBegin) . " <= DATEADD(month,pp.parcela,data_venda)) and (data_venda <= " . valoresDataHoraUnico2($DateEnd) . ") and pp.parcela > 1 and p.parcelar = 0 ";
    odbc_exec($con, $queryX) or die(odbc_errormsg());
    $queryX = "set dateformat dmy;insert into #tempProdutividade ";
    $cur = odbc_exec($con, "select * from #tempProdutividade");
    while ($RFP = odbc_fetch_array($cur)) {
        for ($i = 1; $i < $RFP['parcela']; $i++) {   
            $paymentDate = geraTimestamp(escreverDataSoma(escreverData($RFP['data_venda']), "+" . $i . " month"));
            if (($paymentDate >= $contractDateBegin) && ($paymentDate <= $contractDateEnd)) {
                $queryX .= " select id_item_venda,id_venda, cod_pedido, dateadd(month," . $i . ",data_venda) data_venda, vendedor,descricao_grupo,descricao_tipo, razao_social, descricao, nome, valor_total, valor_custo_medio,
                valor_comissao_venda, quantidade, tbcomiss_prod, tbcomiss_serv, tbcomiss_gere, tbcomiss_plano, comiss_prod, comiss_Serv,
                comiss_gere, comiss_plano, valor_comissao, recebe_comissao, tipo, total_pg, comissao, id_sa, cliente_venda, parcela, conta_produto
                from #tempProdutividade where id_venda = " . $RFP['id_venda'] . " and cast(data_venda as date) = " . valoresData2(escreverData($RFP['data_venda'])) . " union all";             
            }
        }
    }
    odbc_exec($con, substr_replace($queryX, "", -9));
    $union .= " union all select distinct * from #tempProdutividade where data_venda between " . valoresDataHoraUnico2($DateBegin) . " and " . valoresDataHoraUnico2($DateEnd);
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => 0,
    "iTotalDisplayRecords" => 0,
    "aaData" => array()
);

//echo $groupBegin . $select . $from . $query . $comandoX . $union . $groupEnd . $order; exit;
if ($F4 == 1) { //Sintético
    $cur = odbc_exec($con, $groupBegin . $select . $tempTable . $from . $query . $comandoX . $groupEnd);
    $groupBegin = "set dateformat dmy;select x.conta_produto,x.descricao descricao,sum(x.valor_total) valor_pago,x.tipo from (";
    $groupEnd = " ) as x group by x.conta_produto,x.descricao,x.tipo";
    $cur = odbc_exec($con, $groupBegin . " select * from #tempTable " . $union . $groupEnd . $order);
} else { //Analítico
    $cur = odbc_exec($con, $groupBegin . $select . $from . $query . $comandoX . $union . $groupEnd . $order);
}

$i = 0;
$totalValProd = 0;
$totalValServ = 0;
$totalValPlano = 0;
$totalDescProd = 0;
$totalDescServ = 0;
$totalComServ = 0;
$totalComProd = 0;
$totalComPlano = 0;
$totalComissao = 0;
$totalResumo = 0;

while ($RFP = odbc_fetch_array($cur)) {
    $row = array();
    if ($F4 == 2) { //Analítico
        $comissao = 0;
        $totalValPlano += $RFP['valor_total'];
        $totalDescServ += $custo;
        $totalComissao += $comissaoPagar;
        if ($RFP['recebe_comissao'] == 1) {
            if ($RFP['tipo'] == "P") {
                if ($RFP['tbcomiss_prod'] == 1) {
                    $comissao = $RFP['valor_comissao_venda'];
                } elseif ($RFP['tbcomiss_prod'] == 2) {
                    $comissao = $RFP['comiss_prod'];
                }
            } elseif ($RFP['tipo'] == "S") {
                if ($RFP['tbcomiss_serv'] == 1) {
                    $comissao = $RFP['valor_comissao_venda'];
                } elseif ($RFP['tbcomiss_serv'] == 2) {
                    $comissao = $RFP['comiss_serv'];
                }
            } elseif ($RFP['tipo'] == "C") {
                if ($RFP['tbcomiss_plano'] == 1) {
                    $comissao = $RFP['valor_comissao_venda'];
                } elseif ($RFP['tbcomiss_plano'] == 2) {
                    $comissao = $RFP['comiss_plano'];
                }
            }
        }
        $custo = $RFP['valor_custo_medio'] * $RFP['quantidade'];
        $comissaoPagar = (($RFP['valor_total'] - $custo) * $comissao) / 100;
        $row[] = utf8_encode($RFP['cliente_venda']);
        $row[] = escreverData($RFP['dt_inicio_mens']);
        $row[] = escreverData($RFP['data_venda']);
        $row[] = utf8_encode($RFP['vendedor']);
        $row[] = utf8_encode($RFP['descricao_grupo']);      
        $row[] = strtoupper(utf8_encode($RFP['razao_social']));        
        $row[] = utf8_encode($RFP['descricao']);       
        $row[] = utf8_encode($RFP['nome']);        
        $row[] = escreverNumero($RFP['quantidade'], 0);        
        $row[] = formataNumeros($RFP['valor_bruto']);        
        $row[] = formataNumeros($RFP['valor_multa']);
        $row[] = formataNumeros($RFP['desconto']);                
        $row[] = formataNumeros($RFP['valor_total']);        
        $row[] = formataNumeros($custo);
        $row[] = formataNumeros($comissaoPagar);
        if (isset($_GET['pdf'])) {
            $row[] = $RFP['conta_produto'];
            $grp = array($RFP['conta_produto'], utf8_encode($RFP['descricao']), 15);
            if (!in_array($grp, $grupoArray)) {
                $grupoArray[] = $grp;
            }
        }
        $row[] = utf8_encode($RFP['id_venda']);
        $row[] = utf8_encode($RFP['descricao_tipo']);        
    } elseif ($F4 == 3) { //Comissão
        if ($imprimir == 0) {
            if ($RFP['comissao'] == "") {
                $row[] = "<center><input type=\"checkbox\" onclick=\"total();\" class=\"caixa\" name=\"items[]\" value=\"" . $RFP['id_venda'] . "\"/><input id=\"" . $RFP['id_venda'] . "\" value=\"" . ($RFP['cproduto'] + $RFP['cservico'] + $RFP['cplano']) . "\" type=\"hidden\"/></center>";
            } else {
                $row[] = "<center><div style=\"border: 1px solid #BEBEBE;width: 10px;height: 10px;\"></div></center>";
            }
        }
        $totalValProd += $RFP['tproduto'];
        $totalValServ += $RFP['tservico'];
        $totalValPlano += $RFP['tplano'];
        $totalDescProd += $RFP['dproduto'];
        $totalDescServ += $RFP['dservico'];
        $totalDescServ += $RFP['dplan'];
        $totalComProd += $RFP['cproduto'];
        $totalComServ += $RFP['cservico'];
        $totalComPlano += $RFP['cplano'];
        $totalComissao += $RFP['cproduto'] + $RFP['cservico'] + $RFP['cplano'];
        if ($mdl_aca_ == 1 && $mdl_seg_ == 0) {
            $row[] = utf8_encode($RFP['cliente_venda']);
        } else {
            $row[] = utf8_encode($RFP['id_venda'] . ($RFP['cod_pedido'] != "" ? "/" . $RFP['cod_pedido'] : ""));
        }
        $row[] = escreverDataHora($RFP['data_venda']);
        $row[] = utf8_encode($RFP['vendedor']);
        $row[] = strtoupper(utf8_encode($RFP['razao_social']));
        $row[] = formataNumeros($RFP['tproduto']);
        $row[] = formataNumeros($RFP['tservico']);
        if ($mdl_aca_ == 1) {
            $row[] = formataNumeros($RFP['tplano']);
        }
        $row[] = formataNumeros($RFP['dproduto']);
        $descPlan = 0;
        if ($RFP['dplan'] > 0) {
            $row[] = formataNumeros($RFP['dplan']);
        } else {
            $row[] = formataNumeros($RFP['dservico']);
        }
        $row[] = formataNumeros($RFP['cproduto']);
        $row[] = formataNumeros($RFP['cservico']);
        if ($mdl_aca_ == 1) {
            $row[] = formataNumeros($RFP['cplano']);
        }
        $row[] = formataNumeros($RFP['cproduto'] + $RFP['cservico'] + $RFP['cplano']);
        $Solicitacao = "<center>";
        if (is_numeric($RFP['id_sa'])) {
            $Solicitacao .= "<a title=\"" . $RFP['comissao'] . "\" href=\"./../Contas-a-pagar/FormSolicitacao-de-Autorizacao.php?id=" . $RFP['id_sa'] . "\"><img src=\"../../img/";
            if ($RFP['comissao'] == 'Reprovado') {
                $Solicitacao .= "inativo";
            } else {
                if ($RFP['comissao'] == 'Aprovado') {
                    $Solicitacao .= "pago";
                } elseif ($RFP['comissao'] == 'Aguarda') {
                    $Solicitacao .= "apagar";
                } else {
                    $Solicitacao .= "back";
                }
            }
            $Solicitacao .= ".PNG\" value=\"\"></a>";
        }
        if ($RFP['total_pg'] == 0) {
            $Solicitacao .= "<img src=\"../../img/dollar2.png\" value=\"\">";
        } else {
            $Solicitacao .= "<img src=\"../../img/dollar.png\" value=\"\">";
        }
        $row[] = $Solicitacao . "</center>";
    } else { //Sintético
        $row[] = utf8_encode($RFP['tipo']);
        $row[] = utf8_encode($RFP['descricao']);
        $row[] = formataNumeros($RFP['valor_pago']);
        $totalResumo += $RFP['valor_pago'];
    }
    $output['aaData'][] = $row;
    $i++;
}

if (isset($_GET['pdf'])) {
    if ($F4 == 2) { //Analítico
        $output['aaTotal'][] = array(8, 8, "Sub Total", "Total", 0, 0, 0);
        $output['aaTotal'][] = array(9, 9, "", "", 0, 0);
        $output['aaTotal'][] = array(10, 10, "", "", 0, 0);
        $output['aaTotal'][] = array(11, 11, "", "", 0, 0);
        $output['aaTotal'][] = array(12, 12, "", "", 0, 0);
        $output['aaTotal'][] = array(13, 13, "", "", 0, 0);
        $output['aaTotal'][] = array(14, 14, "", "", 0, 0);        
        $output['aaGrupo'] = $grupoArray;        
    } else if ($F4 == 3) { //Comissão
        $row = array();
        $row[] = "";
        $row[] = "";
        $row[] = "";
        $row[] = "";
        $row[] = "";
        $row[] = formataNumeros($totalValProd);
        $row[] = formataNumeros($totalValServ);
        $row[] = formataNumeros($totalValPlano);
        $row[] = formataNumeros($totalDescProd);
        $row[] = formataNumeros($totalDescServ);
        $row[] = formataNumeros($totalComProd);
        $row[] = formataNumeros($totalComServ);
        $row[] = formataNumeros($totalComPlano);
        $row[] = formataNumeros($totalComissao);
        $output['aaData'][] = $row;        
    } else { //Sintético
        $row = array();
        $row[] = "Total";
        $row[] = "";
        $row[] = formataNumeros($totalResumo);
        $output['aaData'][] = $row;
    }
}

$output['iTotalRecords'] = $i;
$output['iTotalDisplayRecords'] = $i;

if (!isset($_GET['isPdf']) && !isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);