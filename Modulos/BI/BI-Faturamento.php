<?php
include "../../Connections/configini.php";
if ($_GET["tab"] != "") {
    $MyTab = $_GET["tab"];
} else {
    $MyTab = "1";
}
if (isset($_POST["btnimprimir"])) {
    $imprimir = 1;
} else {
    $imprimir = 0;
}

$fat_tur_data = getData("T", "-1 days");
$fat_semana_data = getData("T", "-1 days");
$fat_mes_data = date("m/Y", strtotime("-1 month"));
$fat_ult_mes = date("m/Y", strtotime("-1 month"));
$fat_tri_data = date("m/Y");
$fat_anu_data = date("Y");

$cur = odbc_exec($con, "select A.id_filial_f from sf_usuarios_filiais A
inner join sf_usuarios B on (A.id_usuario_f = B.id_usuario and A.id_filial_f = B.filial)
where A.id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
while ($RFP = odbc_fetch_array($cur)) {
    $filial = $RFP['id_filial_f'];
}

$filial = $filial == "" ? 1 : $filial;
$manha = 0.00;
$tarde = 0.00;
$noite = 0.00;
$zero_dez = 0.00;
$onze_vinte = 0.00;
$vinte_trinta = 0.00;
$jan_mar = 0.00;
$abr_jun = 0.00;
$jul_set = 0.00;
$out_dez = 0.00;
$ano_um = 0.00;
$ano_dois = 0.00;
$ano_tres = 0.00;
$ano_quatro = 0.00;
$pjan_mar = 0;
$pabr_jun = 1;
$pjul_set = 2;
$pout_dez = 3;
$se01 = 0.00;
$se02 = 0.00;
$se03 = 0.00;
$se04 = 0.00;
$se05 = 0.00;
$se06 = 0.00;
$se07 = 0.00;
$ult_mes01 = 0.00;
$ult_mes02 = 0.00;
$ult_mes03 = 0.00;
$ult_mes04 = 0.00;

$query = "select id_parcela,data_pagamento,numero_filial empresa,sf_bancos.razao_social banco,TD.descricao tipo_documento,sf_fornecedores_despesas.razao_social,historico_baixa,
valor_pago,sf_contas_movimento.tipo tipo, 0 id_trans, sf_lancamento_movimento_parcelas.somar_rel,sf_contas_movimento.descricao
from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento
on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas
inner join sf_bancos on sf_bancos.id_bancos = sf_lancamento_movimento_parcelas.id_banco
left join sf_filiais on sf_filiais.id_filial = sf_lancamento_movimento.empresa
inner join sf_tipo_documento TD on sf_lancamento_movimento_parcelas.tipo_documento = TD.id_tipo_documento
where sf_contas_movimento.tipo = 'C' and sf_lancamento_movimento.status = 'Aprovado' and data_pagamento is not null AND sf_lancamento_movimento.empresa in (" . $filial . ") AND sf_lancamento_movimento_parcelas.inativo = '0'
and data_pagamento between @dt_ini@ and @dt_fim@ and somar_rel = 1
union all
select id_parcela,data_venda,numero_filial empresa,sf_bancos.razao_social banco,TD.descricao,sf_fornecedores_despesas.razao_social,historico_baixa,valor_parcela,
CASE sf_vendas.cov WHEN 'V' THEN ('C') WHEN 'C' THEN ('D') end tipo, 0 id_trans, sf_venda_parcelas.somar_rel,                                                            
(STUFF((SELECT distinct ',' + descricao FROM sf_vendas_itens vi inner join sf_produtos p on p.conta_produto = vi.produto WHERE vi.id_venda = sf_vendas.id_venda FOR XML PATH('')), 1, 1, '')) descricao2
from sf_venda_parcelas inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda
inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas 
left join sf_bancos on sf_bancos.id_bancos = sf_venda_parcelas.id_banco
left join sf_filiais on sf_filiais.id_filial = sf_vendas.empresa
inner join sf_tipo_documento TD on sf_venda_parcelas.tipo_documento = TD.id_tipo_documento
where sf_vendas.cov in('V') and sf_vendas.status = 'Aprovado' AND sf_vendas.empresa in (" . $filial . ") and data_venda is not null AND sf_venda_parcelas.inativo in (0,2)
and data_venda between @dt_ini@ and @dt_fim@ and somar_rel = 1
union all
select id_parcela,data_lanc,numero_filial empresa,sf_bancos.razao_social banco,TD.descricao,sf_fornecedores_despesas.razao_social,cast(comentarios as varchar(200)) historico_baixa, valor_parcela, 
sf_contas_movimento.tipo tipo, 0 id_trans, sf_solicitacao_autorizacao_parcelas.somar_rel,sf_contas_movimento.descricao 
from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas 
left join sf_bancos on sf_bancos.id_bancos = sf_solicitacao_autorizacao_parcelas.id_banco
left join sf_filiais on sf_filiais.id_filial = sf_solicitacao_autorizacao.empresa
inner join sf_tipo_documento TD on sf_solicitacao_autorizacao.tipo_documento = TD.id_tipo_documento
where sf_contas_movimento.tipo = 'C' and sf_solicitacao_autorizacao.status = 'Aprovado' AND sf_solicitacao_autorizacao.empresa in (" . $filial . ") and data_lanc is not null AND sf_solicitacao_autorizacao_parcelas.inativo = '0'  and somar_rel = 1
and data_lanc between @dt_ini@ and @dt_fim@";

if (isset($_POST["btnbuscaTurno"]) || isset($_POST["btnbuscaMes"]) || isset($_POST["btnbuscaTri"]) || isset($_POST["btnbuscaCres"]) || isset($_POST["btnbuscaSemana"]) || isset($_POST["btnbuscaUltMes"])) {
    $fat_tur_data = $_POST["txt_dt_ini_turno"];
    $fat_semana_data = $_POST["txt_dt_ini_semana"];
    $fat_mes_data = $_POST["txt_dt_ini_mes"];
    $fat_ult_mes = $_POST["txt_dt_ult_mes"];
    $fat_tri_data = $_POST["txt_dt_ini_tri"];
    $fat_anu_data = $_POST["txt_dt_ini_cres"];
}
$queryX = "set dateformat dmy; select sum(cmm) as manha,sum(cmt) as tarde,sum(cmn) as noite from(select 
case when isnull(DATEPART(HOUR,data_pagamento),0) between 0 and 12 then valor_pago else 0 end cmm,
case when isnull(DATEPART(HOUR,data_pagamento),0) between 13 and 18 then valor_pago else 0 end cmt,
case when isnull(DATEPART(HOUR,data_pagamento),0) between 19 and 23 then valor_pago else 0 end cmn from (" .
str_replace(array("@dt_ini@", "@dt_fim@"), array(valoresDataHora2($fat_tur_data, "00:00:00"), valoresDataHora2($fat_tur_data, "23:59:59")), $query) . ") as x ) as y";
$cur = odbc_exec($con, $queryX);
while ($RFP = odbc_fetch_array($cur)) {
    $manha = $RFP["manha"];
    $tarde = $RFP["tarde"];
    $noite = $RFP["noite"];
}

$queryX = "set dateformat dmy;
select sum(a00) a00,sum(a01) a01,sum(a02) a02,sum(a03) a03,sum(a04) as a04,sum(a05) a05,sum(a06) a06 from( select 
case when dateadd(day,-6," . valoresData2($fat_semana_data) . ") = cast(data_pagamento as date) then valor_pago else 0 end a06,
case when dateadd(day,-5," . valoresData2($fat_semana_data) . ") = cast(data_pagamento as date) then valor_pago else 0 end a05,
case when dateadd(day,-4," . valoresData2($fat_semana_data) . ") = cast(data_pagamento as date) then valor_pago else 0 end a04,
case when dateadd(day,-3," . valoresData2($fat_semana_data) . ") = cast(data_pagamento as date) then valor_pago else 0 end a03,
case when dateadd(day,-2," . valoresData2($fat_semana_data) . ") = cast(data_pagamento as date) then valor_pago else 0 end a02,
case when dateadd(day,-1," . valoresData2($fat_semana_data) . ") = cast(data_pagamento as date) then valor_pago else 0 end a01,
case when dateadd(day,-0," . valoresData2($fat_semana_data) . ") = cast(data_pagamento as date) then valor_pago else 0 end a00 from (" .
str_replace(array("@dt_ini@", "@dt_fim@"), array("dateadd(day,-6," . valoresDataHora2($fat_semana_data, "00:00:00") . ")", valoresDataHora2($fat_semana_data, "23:59:59")), $query) . ") as y) as x";
$cur = odbc_exec($con, $queryX);
while ($RFP = odbc_fetch_array($cur)) {
    $se01 = $RFP["a00"];
    $se02 = $RFP["a01"];
    $se03 = $RFP["a02"];
    $se04 = $RFP["a03"];
    $se05 = $RFP["a04"];
    $se06 = $RFP["a05"];
    $se07 = $RFP["a06"];
}

$queryX = "set dateformat dmy;
select sum(a00) a00,sum(a01) a01,sum(a02) a02,sum(a03) a03 from( select 
case when month(dateadd(month,-3," . valoresData2("01/" . $fat_ult_mes) . ")) = month(data_pagamento) then valor_pago else 0 end a03,
case when month(dateadd(month,-2," . valoresData2("01/" . $fat_ult_mes) . ")) = month(data_pagamento) then valor_pago else 0 end a02,
case when month(dateadd(month,-1," . valoresData2("01/" . $fat_ult_mes) . ")) = month(data_pagamento) then valor_pago else 0 end a01,
case when month(" . valoresData2("01/" . $fat_ult_mes) . ") = month(data_pagamento) then valor_pago else 0 end a00 from (" .
str_replace(array("@dt_ini@", "@dt_fim@"), array("dateadd(month,-3," . valoresDataHora2("01/" . $fat_ult_mes, "00:00:00") . ")", "dateadd(day,-1,dateadd(month,1," . valoresDataHora2("01/" . $fat_ult_mes, "23:59:59") . "))"), $query) . ") as y) as x";
$cur = odbc_exec($con, $queryX);
while ($RFP = odbc_fetch_array($cur)) {
    $ult_mes01 = $RFP["a00"];
    $ult_mes02 = $RFP["a01"];
    $ult_mes03 = $RFP["a02"];
    $ult_mes04 = $RFP["a03"];
}

$queryX = "set dateformat dmy;
select sum(primeira) primeira,sum(segunda) segunda,sum(terceira) terceira from(select 
case when day(data_pagamento) between 1 and 10 then valor_pago else 0 end primeira,
case when day(data_pagamento) between 11 and 20 then valor_pago else 0 end segunda,
case when day(data_pagamento) between 21 and 31 then valor_pago else 0 end terceira from (" .
str_replace(array("@dt_ini@", "@dt_fim@"), array(valoresDataHora2("01/" . $fat_mes_data, "00:00:00"), "dateadd(day,-1,dateadd(month,1," . valoresDataHora2("01/" . $fat_mes_data, "23:59:59") . "))"), $query) . ") as y) as x";
$cur = odbc_exec($con, $queryX);
while ($RFP = odbc_fetch_array($cur)) {
    $zero_dez = $RFP["primeira"];
    $onze_vinte = $RFP["segunda"];
    $vinte_trinta = $RFP["terceira"];
}

if (strlen($fat_tri_data) == 7) {
    if (substr($fat_tri_data, 0, 2) == "01" || substr($fat_tri_data, 0, 2) == "02" || substr($fat_tri_data, 0, 2) == "03") {
        $dt_begin3 = "01/04/" . (substr($fat_tri_data, 3, 4) - 1);
        $dt_end3 = "31/03/" . substr($fat_tri_data, 3, 4);
        $pabr_jun = 0;
        $pjul_set = 1;
        $pout_dez = 2;
        $pjan_mar = 3;
    } elseif (substr($fat_tri_data, 0, 2) == "04" || substr($fat_tri_data, 0, 2) == "05" || substr($fat_tri_data, 0, 2) == "06") {
        $dt_begin3 = "01/07/" . (substr($fat_tri_data, 3, 4) - 1);
        $dt_end3 = "30/06/" . substr($fat_tri_data, 3, 4);
        $pjul_set = 0;
        $pout_dez = 1;
        $pjan_mar = 2;
        $pabr_jun = 3;
    } elseif (substr($fat_tri_data, 0, 2) == "07" || substr($fat_tri_data, 0, 2) == "08" || substr($fat_tri_data, 0, 2) == "09") {
        $dt_begin3 = "01/10/" . (substr($fat_tri_data, 3, 4) - 1);
        $dt_end3 = "30/09/" . substr($fat_tri_data, 3, 4);
        $pout_dez = 0;
        $pjan_mar = 1;
        $pabr_jun = 2;
        $pjul_set = 3;
    } else {
        $dt_begin3 = "01/01/" . substr($fat_tri_data, 3, 4);
        $dt_end3 = "31/12/" . substr($fat_tri_data, 3, 4);
    }    
    $queryX = "set dateformat dmy;
    select sum(jan_mar) jan_mar,sum(abr_jun) abr_jun,sum(jul_set) jul_set,sum(out_dez) out_dez from(select 
    case when MONTH(data_pagamento) between 1 and 3 then valor_pago else 0 end jan_mar,
    case when MONTH(data_pagamento) between 4 and 6 then valor_pago else 0 end abr_jun,
    case when MONTH(data_pagamento) between 7 and 9 then valor_pago else 0 end jul_set,
    case when MONTH(data_pagamento) between 10 and 12 then valor_pago else 0 end out_dez from (" .
    str_replace(array("@dt_ini@", "@dt_fim@"), array(valoresDataHora2($dt_begin3, "00:00:00"), valoresDataHora2($dt_end3, "23:59:59")), $query) . ") as y) as x";
    $cur = odbc_exec($con, $queryX);
    while ($RFP = odbc_fetch_array($cur)) {
        $jan_mar = $RFP["jan_mar"];
        $abr_jun = $RFP["abr_jun"];
        $jul_set = $RFP["jul_set"];
        $out_dez = $RFP["out_dez"];
    }
}

$queryX = "set dateformat dmy;
select sum(ano_um) ano_um,sum(ano_dois) ano_dois,sum(ano_tres) ano_tres,sum(ano_quatro) ano_quatro from(select 
case when year(dateadd(year,-3," . valoresData2("01/01/" . $fat_anu_data) . ")) = year(data_pagamento) then valor_pago else 0 end ano_um,
case when year(dateadd(year,-2," . valoresData2("01/01/" . $fat_anu_data) . ")) = year(data_pagamento) then valor_pago else 0 end ano_dois,
case when year(dateadd(year,-1," . valoresData2("01/01/" . $fat_anu_data) . ")) = year(data_pagamento) then valor_pago else 0 end ano_tres,
case when year(" . valoresData2("01/01/" . $fat_anu_data) . ") = year(data_pagamento) then valor_pago else 0 end ano_quatro from (" .
str_replace(array("@dt_ini@", "@dt_fim@"), array("dateadd(year,-3," . valoresDataHora2("01/01/" . $fat_anu_data, "00:00:00") . ")", valoresDataHora2("31/12/" . $fat_anu_data, "23:59:59")), $query) . ") as y) as x";
$cur = odbc_exec($con, $queryX);
while ($RFP = odbc_fetch_array($cur)) {
    $ano_um = $RFP["ano_um"];
    $ano_dois = $RFP["ano_dois"];
    $ano_tres = $RFP["ano_tres"];
    $ano_quatro = $RFP["ano_quatro"];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Gestão de resultados:</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <style type="text/css">
            .boxchart {
                min-height: 350px;
                padding: 15px 15px 0;
                width: calc(100% - 32px);
            }
            .data .head { float:left; width:calc(46.25% - 2px); border:1px solid #CCC; border-radius:5px; padding:1%; }            
        </style>
    </head>
    <body>
        <?php if ($imprimir == 0) { ?>
            <div id="loader"><img src="../../img/loader.gif"/></div>
        <?php } ?>
        <div class="wrapper">
            <?php
            if ($imprimir == 0) {
                include("../../menuLateral.php");
            }
            ?>
            <div class="body">
                <?php
                if ($imprimir == 0) {
                    include("../../top.php");
                    include "../../querys_finance.php";
                }
                ?>
                <div class="content" >
                    <?php if ($imprimir == 0) { ?>
                        <div class="page-header">
                            <div class="icon"> <span class="ico-arrow-right"></span> </div>
                            <h1>SIVIS B.I. <small>RESUMOS DOS MODULOS</small></h1>
                        </div>
                        <form action="BI-Faturamento.php<?php
                        if ($_GET["id"] != "") {
                            echo "?id=" . $_GET["id"];
                        }
                        if ($_GET["tab"] != "") {
                            if ($_GET["id"] != "") {
                                echo "&tab=" . $_GET["tab"];
                            } else {
                                echo "?tab=" . $_GET["tab"];
                            }
                            if ($_GET["tp"] != "") {
                                echo "&tp=" . $_GET["tp"];
                            }
                        }
                        ?>" method="POST"> 
                        <?php } ?>
                        <div class="box50">
                            <div class="boxhead">
                                <div class="boxtext">Faturamento por Turno</div>
                            </div>
                            <div class="boxchart">
                                <b>Faturamento por Turno</b> de 
                                <input type="text" style="width:85px; text-align:center" class="datepicker" id="txt_dt_ini_turno" name="txt_dt_ini_turno" value="<?php echo $fat_tur_data; ?>" placeholder="Data"/> 
                                Total: <b><?php echo escreverNumero(($manha + $tarde + $noite), 1); ?></b>
                                <input type="submit" class="button button-turquoise btn-primary" style="line-height:17px" name="btnbuscaTurno" id="btnbuscaTurno" value="Buscar"/>
                                <div style="height:300px" id="chart-10"></div>
                            </div>
                        </div>
                        <div class="box50" style="margin-left:2.5%">
                            <div class="boxhead">
                                <div class="boxtext">Faturamento por Semana</div>
                            </div>
                            <div class="boxchart">
                                <b>Faturamento por Semana</b> de 
                                <input type="text" style="width:85px; text-align:center" class="datepicker" id="txt_dt_ini_semana" name="txt_dt_ini_semana" value="<?php echo $fat_semana_data; ?>" placeholder="Data"/> 
                                Total: <b><?php echo escreverNumero(($se01 + $se02 + $se03 + $se04 + $se05 + $se06 + $se07), 1); ?></b>
                                <input type="submit" class="button button-turquoise btn-primary" style="line-height:17px" name="btnbuscaSemana" id="btnbuscaSemana" value="Buscar"/>
                                <div style="height:300px" id="chart-50"></div>
                            </div>
                        </div>
                        <div style="clear:both"></div>
                        <div class="box50">
                            <div class="boxhead">
                                <div class="boxtext">Faturamento no Mês</div>
                            </div>
                            <div class="boxchart">
                                <b>Faturamento no Mês</b> de 
                                <input type="text" style="width:85px; text-align:center" id="txt_dt_ini_mes" name="txt_dt_ini_mes" value="<?php echo $fat_mes_data; ?>" placeholder="Mês/Ano"/> 
                                Total: <b><?php echo escreverNumero(($zero_dez + $onze_vinte + $vinte_trinta), 1); ?></b>
                                <input type="submit" class="button button-turquoise btn-primary" style="line-height:17px" name="btnbuscaMes" id="btnbuscaMes" value="Buscar"/>
                                <div style="height:300px" id="chart-20"></div>
                            </div>
                        </div>
                        <div class="box50" style="margin-left:2.5%">
                            <div class="boxhead">
                                <div class="boxtext">Faturamento nos Últimos Meses</div>
                            </div>
                            <div class="boxchart">
                                <b>Faturamento nos Últimos Meses</b> de 
                                <input type="text" style="width:85px; text-align:center" id="txt_dt_ult_mes" name="txt_dt_ult_mes" value="<?php echo $fat_ult_mes; ?>" placeholder="Mês/Ano"/> 
                                Total: <b><?php echo escreverNumero(($ult_mes01 + $ult_mes02 + $ult_mes03 + $ult_mes04), 1); ?></b>
                                <input type="submit" class="button button-turquoise btn-primary" style="line-height:17px" name="btnbuscaUltMes" id="btnbuscaUltMes" value="Buscar"/>
                                <div style="height:300px" id="chart-60"></div>
                            </div>
                        </div>
                        <div style="clear:both"></div>
                        <div class="box50">
                            <div class="boxhead">
                                <div class="boxtext">Faturamento por Trimestre</div>
                            </div>
                            <div class="boxchart">
                                <b>Faturamento por Trimestre</b> de 
                                <input type="text" style="width:85px; text-align:center" id="txt_dt_ini_tri" name="txt_dt_ini_tri" value="<?php echo $fat_tri_data; ?>" placeholder="Mês/Ano"/> 
                                Total: <b><?php echo escreverNumero(($jan_mar + $abr_jun + $jul_set + $out_dez), 1); ?></b>
                                <input type="submit" class="button button-turquoise btn-primary" style="line-height:17px" name="btnbuscaTri" id="btnbuscaTri" value="Buscar"/>
                                <div style="height:300px" id="chart-30"></div>
                            </div>
                        </div>
                        <div class="box50" style="margin-left:2.5%">
                            <div class="boxhead">
                                <div class="boxtext">Crescimento Anual</div>
                            </div>
                            <div class="boxchart">
                                <b>Crescimento Anual</b> de 
                                <input type="text" style="width:85px; text-align:center" id="txt_dt_ini_cres" name="txt_dt_ini_cres" value="<?php echo $fat_anu_data; ?>" placeholder="Ano"/> 
                                <input type="submit" class="button button-turquoise btn-primary" style="line-height:17px" name="btnbuscaCres" id="btnbuscaCres" value="Buscar"/>
                                <div style="height:300px" id="chart-40"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" style="display:none" title="Source"></div>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/jquery.mousewheel.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/multiselect/jquery.multi-select.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>        
        <script type="text/javascript" src="../../js/plugins/epiechart/jquery.easy-pie-chart.js"></script>    
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>   
        <link rel="stylesheet" type="text/css" href="../../SpryAssets/SpryValidationTextField.css">
        <script type="text/javascript" src="../../SpryAssets/SpryValidationTextField.js"></script>        
        <script src="../../js/calendario.js" type="text/javascript"></script>
        <script type='text/javascript' src='../../js/app.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jflot/jquery.flot.js"></script>
        <script type="text/javascript" src="../../js/plugins/jflot/jquery.flot.stack.js"></script>
        <script type="text/javascript" src="../../js/plugins/jflot/jquery.flot.pie.js"></script>
        <script type="text/javascript" src="../../js/plugins/jflot/jquery.flot.resize.js"></script>        
        <script type="text/javascript" src="amcharts/amcharts.js"></script>
        <script type="text/javascript" src="amcharts/serial.js"></script>
        <script type="text/javascript" src="amcharts/themes/light.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type='text/javascript' src='../../js/util.js'></script>                    
        <script type="text/javascript">

            $("#txt_dt_ini_turno, #txt_dt_ini_semana").mask(lang["dateMask"]);
            $("#txt_dt_ini_mes, #txt_dt_ult_mes, #txt_dt_ini_tri").mask("99/9999");
            $("#txt_dt_ini_cres").mask("9999");

            function abbaMk(abba) {
                document.getElementById("jumpMenu").options[0].value = "BI-Faturamento.php?id=0#tab" + abba;
                document.getElementById("jumpMenu").options[1].value = "BI-Faturamento.php?id=1#tab" + abba;
                document.getElementById("jumpMenu").options[2].value = "BI-Faturamento.php?id=2#tab" + abba;
                document.getElementById("jumpMenu").options[3].value = "BI-Faturamento.php?id=3#tab" + abba;
                document.getElementById("jumpMenu").options[4].value = "BI-Faturamento.php?id=4#tab" + abba;
            }

            function showTooltip(x, y, contents) {
                $('<div class="ct">' + contents + '</div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y,
                    left: x + 10,
                    border: '1px solid #000',
                    padding: '3px',
                    opacity: '0.7',
                    'background-color': '#000',
                    color: '#fff'
                }).appendTo("body").fadeIn(200);
            }
            $(window).load(function () {
                if ($("#chart-10").length > 0) {
                    var d1 = [];
                    d1.push([0, <?php echo $manha; ?>]);
                    d1.push([1, <?php echo $tarde; ?>]);
                    d1.push([2, <?php echo $noite; ?>]);
                    var stack = 0, bars = true, lines = false, steps = false;
                    $.plot($("#chart-10"), [{data: d1, label: ""}], {
                        series: {bars: {show: true, barWidth: 0.6}},
                        grid: {hoverable: true, clickable: true},
                        xaxis: {ticks: [[0, "Manhã"], [1, "Tarde"], [2, "Noite"]]}
                    });
                }
                if ($("#chart-20").length > 0) {
                    var d2 = [];
                    d2.push([0,<?php echo $zero_dez; ?>]);
                    d2.push([1,<?php echo $onze_vinte; ?>]);
                    d2.push([2,<?php echo $vinte_trinta; ?>]);
                    var stack = 0, bars = true, lines = false, steps = false;
                    $.plot($("#chart-20"), [{data: "", label: ""}, {data: d2, label: ""}], {
                        series: {bars: {show: true, barWidth: 0.6}},
                        grid: {hoverable: true, clickable: true},
                        xaxis: {ticks: [[0, "Dia 01 a 10"], [1, "Dia 11 a 20"], [2, "Dia 21 a 31"]]}
                    });
                }
                if ($("#chart-30").length > 0) {
                    var d3 = [];
                    d3.push([<?php echo $pjan_mar; ?>,<?php echo $jan_mar; ?>]);
                    d3.push([<?php echo $pabr_jun; ?>,<?php echo $abr_jun; ?>]);
                    d3.push([<?php echo $pjul_set; ?>,<?php echo $jul_set; ?>]);
                    d3.push([<?php echo $pout_dez; ?>,<?php echo $out_dez; ?>]);
                    var stack = 0, bars = true, lines = false, steps = false;
                    $.plot($("#chart-30"), [{data: "", label: ""}, {data: "", label: ""}, {data: d3, label: ""}], {
                        series: {bars: {show: true, barWidth: 0.6}},
                        grid: {hoverable: true, clickable: true},
                        xaxis: {ticks: [[<?php echo $pjan_mar; ?>, "Jan-Mar"], [<?php echo $pabr_jun; ?>, "Abr-Jun"], [<?php echo $pjul_set; ?>, "Jul-Set"], [<?php echo $pout_dez; ?>, "Out-Dez"]]}
                    });
                }
                if ($("#chart-40").length > 0) {
                    var d4 = [];
                    d4.push([0,<?php echo $ano_um; ?>]);
                    d4.push([1,<?php echo $ano_dois; ?>]);
                    d4.push([2,<?php echo $ano_tres; ?>]);
                    d4.push([3,<?php echo $ano_quatro; ?>]);
                    var stack = 0, bars = false, lines = true, steps = false;
                    $.plot($("#chart-40"), [{data: "", label: ""}, {data: "", label: ""}, {data: "", label: ""}, {data: d4, label: ""}], {
                        series: {lines: {show: true, fill: true}, points: {show: true}},
                        grid: {hoverable: true, clickable: true},
                        xaxis: {labelWidth: 30, ticks: [[0, <?php echo ($fat_anu_data - 3); ?>], [1, <?php echo ($fat_anu_data - 2); ?>], [2, <?php echo ($fat_anu_data - 1); ?>], [3, <?php echo $fat_anu_data; ?>]]}
                    });
                }
                if ($("#chart-50").length > 0) {
                    var d4 = [];
                    d4.push([6,<?php echo $se01; ?>]);
                    d4.push([5,<?php echo $se02; ?>]);
                    d4.push([4,<?php echo $se03; ?>]);
                    d4.push([3,<?php echo $se04; ?>]);
                    d4.push([2,<?php echo $se05; ?>]);
                    d4.push([1,<?php echo $se06; ?>]);
                    d4.push([0,<?php echo $se07; ?>]);
                    var stack = 0, bars = true, lines = false, steps = false;
                    $.plot($("#chart-50"), [{data: "", label: ""}, {data: "", label: ""}, {data: "", label: ""}, {data: d4, label: ""}], {
                        series: {bars: {show: true, barWidth: 0.6}},
                        grid: {hoverable: true, clickable: true},
                        xaxis: {ticks: [[6, "<?php echo date("d/m/y", strtotime("-0 days", strtotime(str_replace('/', '-', $fat_semana_data)))); ?>"], [5, "<?php echo date("d/m/y", strtotime("-1 days", strtotime(str_replace('/', '-', $fat_semana_data)))); ?>"], [4, "<?php echo date("d/m/y", strtotime("-2 days", strtotime(str_replace('/', '-', $fat_semana_data)))); ?>"], [3, "<?php echo date("d/m/y", strtotime("-3 days", strtotime(str_replace('/', '-', $fat_semana_data)))); ?>"], [2, "<?php echo date("d/m/y", strtotime("-4 days", strtotime(str_replace('/', '-', $fat_semana_data)))); ?>"], [1, "<?php echo date("d/m/y", strtotime("-5 days", strtotime(str_replace('/', '-', $fat_semana_data)))); ?>"], [0, "<?php echo date("d/m/y", strtotime("-6 days", strtotime(str_replace('/', '-', $fat_semana_data)))); ?>"]]}
                    });
                }
                if ($("#chart-60").length > 0) {
                    var d4 = [];
                    d4.push([3,<?php echo $ult_mes01; ?>]);
                    d4.push([2,<?php echo $ult_mes02; ?>]);
                    d4.push([1,<?php echo $ult_mes03; ?>]);
                    d4.push([0,<?php echo $ult_mes04; ?>]);
                    var stack = 0, bars = false, lines = true, steps = false;
                    $.plot($("#chart-60"), [{data: "", label: ""}, {data: "", label: ""}, {data: "", label: ""}, {data: "", label: ""}, {data: "", label: ""}, {data: d4, label: ""}], {
                        series: {lines: {show: true, fill: true}, points: {show: true}},
                        grid: {hoverable: true, clickable: true},
                        xaxis: {labelWidth: 30, ticks: [[0, "<?php echo date("m/y", strtotime("-3 month", strtotime(str_replace('/', '-', "01/" . $fat_ult_mes)))); ?>"], [1, "<?php echo date("m/y", strtotime("-2 month", strtotime(str_replace('/', '-', "01/" . $fat_ult_mes)))); ?>"], [2, "<?php echo date("m/y", strtotime("-1 month", strtotime(str_replace('/', '-', "01/" . $fat_ult_mes)))); ?>"], [3, "<?php echo date("m/y", strtotime("-0 month", strtotime(str_replace('/', '-', "01/" . $fat_ult_mes)))); ?>"]]}
                    });
                }
                var previousPoint = null;
                $("#chart-10, #chart-20, #chart-30, #chart-40, #chart-50, #chart-60").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint !== item.dataIndex) {
                            previousPoint = item.dataIndex;
                            $(".ct").remove();
                            var x = item.datapoint[0].toFixed(2),
                                    y = item.datapoint[1].toFixed(2);
                            showTooltip(item.pageX, item.pageY, lang["prefix"] + " " + numberFormat(y));
                        }
                    } else {
                        $(".ct").remove();
                        previousPoint = null;
                    }
                });
            });

            jQuery(document).ready(function () {
                var Index = function () {
                    return {
                        init: function () {
                            App.addResponsiveHandler(function () {
                                Index.initCalendar();
                                jQuery('.vmaps').each(function () {
                                    var map = jQuery(this);
                                    map.width(map.parent().width());
                                });
                            });
                        },
                        initCalendar: function () {
                            if (!jQuery().fullCalendar) {
                                return;
                            }
                            var date = new Date();
                            var d = date.getDate();
                            var m = date.getMonth();
                            var y = date.getFullYear();
                            var h = {};
                            if ($('#calendar').width() <= 400) {
                                $('#calendar').addClass("mobile");
                                h = {
                                    left: 'title, prev, next',
                                    center: '',
                                    right: 'today,month,agendaWeek,agendaDay'
                                };
                            } else {
                                $('#calendar').removeClass("mobile");
                                if (App.isRTL()) {
                                    h = {
                                        right: 'title',
                                        center: '',
                                        left: 'prev,next,today,month,agendaWeek,agendaDay'
                                    };
                                } else {
                                    h = {
                                        left: 'title',
                                        center: '',
                                        right: 'prev,next,today,month,agendaWeek,agendaDay'
                                    };
                                }
                            }
                            $('#calendar').fullCalendar('destroy');
                            $('#calendar').fullCalendar({
                                disableDragging: false,
                                header: h,
                                editable: true,
                                events: [{
                                        title: 'Long Event 22',
                                        start: new Date(y, m, d - 5),
                                        end: new Date(y, m, d - 5),
                                        backgroundColor: App.getLayoutColorCode('green')
                                    }]
                            });
                        }
                    };
                }();
                App.init();
                Index.init();
                Index.initCalendar();
            });

<?php if ($imprimir == 1) { ?>
                $(window).load(function () {
                    window.print();
                    $(".body").css("margin-left", 0);
                    $("#example_length").remove();
                    $("#example_filter").remove();
                    $("#example_paginate").remove();
                    $("#formPQ > th").css("background-image", "none");
                    window.history.back();
                });
<?php } ?>
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>
