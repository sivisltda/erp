<?php
include "../../Connections/configini.php";

$F1 = 1;
$F2 = 0;
$DateBegin = getData("B");
$DateEnd = getData("E");
if ($_GET["tpData"] != "") {
    $F1 = $_GET["tpData"];
}
if (is_numeric($_GET["tp"]) && $_GET["tp"] == 1) {
    $F2 = $_GET["tp"];
}
if ($_GET["dti"] != "") {
    $DateBegin = str_replace("_", "/", $_GET["dti"]);
}
if ($_GET["dtf"] != "") {
    $DateEnd = str_replace("_", "/", $_GET["dtf"]);
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../SpryAssets/SpryValidationTextField.css"/>
        <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>         
        <style>
            #chartdiv { width: calc(100% - 32px); padding: 15px; }
            #tabela_1 { width: 100%; border-spacing: 5px; border-collapse: separate }
            #tabela_1 td { padding: 15px 5px; text-align: center; font-size: 12px; background: #EEE }
            .despesas { width: calc(100% - 17px) !important; position: absolute; top:86px; }
            .receitas { width: calc(100% - 17px) !important; position: absolute; top:56px; }
            #script { width:100%; position:relative; font-family:"Arial"; font-weight:bold; color:#7b8a90; }
            #scroll { width:100%; height:550px; overflow-y:scroll; }
            .cat-1, .cat-2	{ overflow:auto; width:100%; font-size:12px; }
            .col-1, .col-2	{ float:left; width:85px; height:30px; line-height:30px; padding-right:5px; text-align:right; }
            .cat-1 .col-e	{ width:calc(100% - 758px); padding-left:20px; border-right:10px solid #d4d4d4; }
            .cat-1 .col-d	{ width:78px; padding-right:10px; border-left:10px solid #d4d4d4; text-align:right; }
            .cat-1 .col-e, .cat-1 .col-d	{ float:left; overflow:hidden; height:30px; line-height:30px; background:#e8e8e8; }
            .cat-1 .col-1	{ background:#e0e0e0; }
            .cat-1 .col-2	{ background:#ececec; }
            .cat-2 .col-e	{ width:calc(100% - 758px); padding-left:20px; border-right:10px solid #d0d0d0; }
            .cat-2 .col-d	{ width:78px; padding-right:10px; border-left:10px solid #d0d0d0; text-align:right; }
            .cat-2 .col-e, .cat-2 .col-d	{ float:left; overflow:hidden; height:30px; line-height:30px; background:#ececec; }
            .cat-2 .col-1	{ background:#dcdcdc; }
            .cat-2 .col-2	{ background:#e8e8e8; }
            .cat-t		{ overflow:auto; width:100%; font-size:12px; }
            .cat-t .col-e	{ width:calc(100% - 745px); text-align:right; }
            .cat-t .col-d	{ width: 115px; }
            .cat-t .col-e, .cat-t .col-d	{ float:left; overflow:hidden; height:19px; padding-top:6px; }
            .cat-t .col-c	{ float:left; width:90px; height:25px; line-height:25px; text-align:center; }
            .cat-s, .cat-m	{ overflow:auto; width:100%; font-size:14px; border-top:1px solid #afafaf; border-bottom:1px solid #afafaf; }
            .cat-s .col-e	{ width:calc(100% - 765px); padding-left:10px; border-right:10px solid #bdbdbd; }
            .cat-s .col-d	{ width:78px; padding-right:27px; border-left:10px solid #bdbdbd; text-align:right; }
            .cat-s .col-e, .cat-s .col-d	{ float:left; overflow:hidden; height:30px; line-height:30px; background:#d2d2d2; }
            .cat-s .col-1	{ background:#c7c7c7; }
            .cat-s .col-2	{ background:#d2d2d2; }
            .cat-m .col-e	{ width:calc(100% - 748px); padding-left:10px; border-right:10px solid #bdbdbd; }
            .cat-m .col-d	{ width:78px; padding-right:10px; border-left:10px solid #bdbdbd; text-align:right; }
            .cat-m .col-e, .cat-m .col-d	{ float:left; overflow:hidden; height:30px; line-height:30px; background:#f0f0f0; }
            .cat-m .col-1	{ background:#d2d2d2; }
            .cat-m .col-2	{ background:#e5e5e5; }
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">      
                    <div class="page-header">
                        <div class="icon"><span class="ico-arrow-right"></span></div>
                        <h1>SIVIS B.I.<small>Fluxo de Caixa</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="block" style="font-size:13px">
                                <select style="width:160px; height:31px" name="txtTipoB" id="txtTipoB">
                                    <option value="0" <?php
                                    if ($F2 == "0") {
                                        echo "SELECTED";
                                    }
                                    ?>>SINTÉTICO</option>
                                    <option value="1" <?php
                                    if ($F2 == "1") {
                                        echo "SELECTED";
                                    }
                                    ?>>ANALÍTICO</option>
                                </select>
                                <select style="width:160px; height:31px" id="txtTpData">
                                    <option value="2" <?php
                                    if ($F1 == "2") {
                                        echo "SELECTED";
                                    }
                                    ?>>VENDA</option>
                                    <option value="1" <?php
                                    if ($F1 == "1") {
                                        echo "SELECTED";
                                    }
                                    ?>>RECEBIMENTO</option>
                                </select>
                                de <input type="text" class="datepicker" style="width:80px; height:31px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial"/>
                                até <input type="text" class="datepicker" style="width:80px; height:31px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final"/>
                                <button class="button button-turquoise btn-primary" type="button" onclick="AbrirBox(0)" id="btnfind"><span class="ico-search icon-white"></span></button>
                                <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="Imprimir()" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="ExportarExcel()" title="Exportar Excel"><span class="ico-download-3 icon-white"></span></button>
                            </div>
                        </div>
                        <div style="clear:both"></div>
                        <div class="boxhead">
                            <div class="boxtext">Fluxo de Caixa de <?php echo $DateBegin; ?> à <?php echo $DateEnd; ?></div>
                        </div>
                        <div class="boxchart" id="chartdiv"></div>
                        <div class="boxhead">
                            <div class="boxtext">Fluxo de Caixa de <?php echo $DateBegin; ?> à <?php echo $DateEnd; ?></div>
                        </div>
                        <div class="boxtable">
                            <?php if ($F2 == 1) { ?>
                                <table class="table" style="float:none" id="tbReceita">
                                    <thead>
                                        <tr>
                                            <th><center>DATA</center></th>
                                            <th><center>RECEITAS</center></th>
                                            <th><center>DESPESAS</center></th>
                                            <th><center>SALDO</center></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="4" class="dataTables_empty">Carregando dados ...</td>
                                        </tr>
                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <input id="receitaRow" type="hidden" value="0"/>
                                <input id="receitaCol" type="hidden" value="0"/>
                                <div class="row-fluid" style="float:none">
                                    <div class="span12">
                                        <div id="script">
                                            <div id="datas" class="cat-t"></div>
                                            <div id="saldoInicial" class="cat-s" style="border-top:0"></div>
                                            <div id="scroll">
                                                <div id="receitas" class="cat-m" style="border-top:0"></div>
                                                <div id="despesas" class="cat-m"></div>
                                            </div>
                                            <div id="saldoFinal" class="cat-s"></div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/charts.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="amcharts/amcharts.js"></script>
        <script type="text/javascript" src="amcharts/serial.js"></script>
        <script type="text/javascript" src="amcharts/themes/light.js"></script>
        <script type='text/javascript' src="../../js/numeral.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type='text/javascript' src='../../js/util.js'></script>        
        <script type="text/javascript">
            
            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
            
            function AbrirBox(imp) {
                var retPrint = "BI-Fluxo-de-Caixa.php?imp=" + imp;
                if ($("#txtTipoB").val() !== "") {
                    retPrint = retPrint + "&tp=" + $("#txtTipoB").val();
                }
                if ($("#txtTpData").val() !== "") {
                    retPrint = retPrint + "&tpData=" + $("#txtTpData").val();
                }
                if ($("#txt_dt_begin").val() !== "") {
                    retPrint = retPrint + "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "_");
                }
                if ($("#txt_dt_end").val() !== "") {
                    retPrint = retPrint + "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "_");
                }
                if (imp === 0) {
                    window.location = retPrint;
                } else {
                    return retPrint;
                }
            }

            function Imprimir() {
                var pRel = "&NomeArq=" + "Fluxo de Caixa" +
                "&lbl=DATA|RECEITAS|DESPESAS|SALDO" +
                "&siz=175|175|175|175" +
                "&pdf=5" + // Colunas do server processing que não irão aparecer no pdf
                "&filter=" + $("#txt_dt_begin").val() + " até " + $("#txt_dt_end").val() + //Label que irá aparecer os parametros de filtro
                "&PathArqInclude=" + "../Modulos/BI/" + finalFind(1, 1).replace("?", "&"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }

            function ExportarExcel() {
                window.open(finalFind(2, 1), '_blank');
            }

            function finalFind(imp, ts) {
                var retPrint = "&imp=" + imp;
                if ($("#txtTipoB").val() !== "") {
                    retPrint = retPrint + "&tp=" + $("#txtTipoB").val();
                }
                if ($("#txtTpData").val() !== "") {
                    retPrint = retPrint + "&tpData=" + $("#txtTpData").val();
                }                
                if ($("#txt_dt_begin").val() !== "") {
                    retPrint = retPrint + "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "_");
                }
                if ($("#txt_dt_end").val() !== "") {
                    retPrint = retPrint + "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "_");
                }
                return "BI-Fluxo-de-Caixa_server_processing.php?filial=" + $("#txtMnFilial").val() + "&ts=" + ts + retPrint;
            }
            
            $(document).ready(function () {
                $("#tbReceita").dataTable({
                    bPaginate: false, bFilter: false, bInfo: false, bSort: false,
                    "bProcessing": false,
                    "bServerSide": true,
                    "sAjaxSource": finalFind(0, 1)
                });
            });

            var chartData = [];
            var i = 0;
            $.getJSON(finalFind(0, 0), function (data) {
                $.each(data["aaData"], function (key, val) {
                    chartData[i] = {dia: val[0].substring(0, 5), receita: val[1], despesa: val[2]};
                    i = i + 1;
                });
            }).done(function () {
                var chart = AmCharts.makeChart("chartdiv", {
                    "type": "serial",
                    "addClassNames": true,
                    "theme": "light",
                    "pathToImages": "http://www.amcharts.com/lib/3/images/",
                    "autoMargins": false,
                    "marginBottom": 25,
                    "marginLeft": 50,
                    "marginRight": 0,
                    "marginTop": 5,
                    "numberFormatter": {"precision": 2, "decimalSeparator": lang["centsSeparator"], "thousandsSeparator": lang["thousandsSeparator"]},
                    "dataProvider": chartData,
                    "valueAxes": [{
                            "axisAlpha": 0,
                            "position": "left"
                        }],
                    "startDuration": 1,
                    "graphs": [{
                            "alphaField": "alpha",
                            "balloonText": "[[title]]: " + lang["prefix"] + " [[value]]",
                            "fillAlphas": 0.8,
                            "lineAlpha": 0.2,
                            "title": "Receitas",
                            "type": "column",
                            "valueField": "receita",
                            "fillColors": "#009900",
                            "dashLengthField": "dashLengthColumn"
                        }, {
                            "id": "graph2",
                            "balloonText": "[[title]]: " + lang["prefix"] + " [[value]]",
                            "bullet": "round",
                            "lineThickness": 3,
                            "bulletSize": 7,
                            "bulletBorderAlpha": 1,
                            "bulletColor": "#FFFFFF",
                            "useLineColorForBulletBorder": true,
                            "bulletBorderThickness": 3,
                            "fillAlphas": 0.0,
                            "lineAlpha": 0.8,
                            "lineColor": "#990000",
                            "title": "Despesas",
                            "valueField": "despesa"
                        }],
                    "categoryField": "dia",
                    "categoryAxis": {
                        "gridPosition": "start",
                        "axisAlpha": 0,
                        "tickLength": 0
                    }
                });
                $("svg[version='1.1']").next().remove();
            });
            
            function finalFindConta(ts) {
                var retPrint = "";
                if ($("#txt_dt_begin").val() !== "") {
                    retPrint = retPrint + "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "_");
                }
                if ($("#txt_dt_end").val() !== "") {
                    retPrint = retPrint + "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "_");
                }
                if ($("#txtTpData").val() !== "") {
                    retPrint = retPrint + "&tpData=" + $("#txtTpData").val();
                }                
                return "BI-Fluxo-de-Caixa_Conta_server_processing.php?filial=" + $("#txtMnFilial").val() + "&ts=" + ts + retPrint;
            }
            
            var conteudo = "";
            var totCred = [];
            var totDebt = [];
            $.getJSON(finalFindConta(0), function (data) {
                var i = 0;
                var totLinha = 0.00;
                var tipo = "";
                conteudo = "<div class=\"col-e\"><span class=\"ico-chevron-left\" style=\"cursor:pointer\" onClick=\"prev()\"></span></div>";
                $.each(data["aCalendar"], function (key, val) {
                    conteudo = conteudo + "<div class=\"col-c " + (i + 1) + "\" ";
                    if (i > 6) {
                        conteudo = conteudo + "style=\"display:none\"";
                    }
                    conteudo = conteudo + "> " + val[0] + "</div>";
                    totCred[i] = 0.00;
                    totDebt[i] = 0.00;
                    i = i + 1;
                });
                conteudo = conteudo + "<div class=\"col-d\"><span class=\"ico-chevron-right\" style=\"cursor:pointer\" onClick=\"next()\"></span></div>";
                $("#datas").html(conteudo);
                if (i > 6) {
                    $("#receitaCol").val(i - 6);
                }
                i = 0;
                var receitaRow = 0;
                $.each(data["aaData"], function (key, val) {
                    conteudo = "";
                    totLinha = 0.00;
                    tipo = val[val.length - 1];
                    if (i % 2 === 0) {
                        conteudo = conteudo + "<div class=\"cat-2\">";
                    } else {
                        conteudo = conteudo + "<div class=\"cat-1\">";
                    }
                    conteudo = conteudo + "<div class=\"col-e\">" + val[0].substring(0, 27) + "</div>";
                    for (j = 1; j < val.length - 1; j++) {
                        if (j % 2 === 0) {
                            conteudo = conteudo + "<div class=\"col-2 " + j + "\"";
                        } else {
                            conteudo = conteudo + "<div class=\"col-1 " + j + "\"";
                        }
                        if (j > 7) {
                            conteudo = conteudo + " style=\"display:none\"";
                        }
                        conteudo = conteudo + ">" + numberFormat(val[j]) + "</div>";
                        totLinha = totLinha + parseFloat(val[j]);
                        if (tipo === "C") {
                            totCred[j - 1] = totCred[j - 1] + parseFloat(val[j]);
                        } else if (tipo === "D") {
                            totDebt[j - 1] = totDebt[j - 1] + parseFloat(val[j]);
                        }
                    }
                    conteudo = conteudo + "<div class=\"col-d\">" + numberFormat(totLinha) + "</div>";
                    conteudo = conteudo + "</div>";
                    if (tipo === "C") {
                        $(conteudo).insertAfter("#receitas");
                        receitaRow = receitaRow + 1;
                    } else if (tipo === "D") {
                        $(conteudo).insertAfter("#despesas");
                    }
                    i = i + 1;
                });
                if (receitaRow > 0) {
                    $("#receitaRow").val(receitaRow - 1);
                }
            }).done(function () {
                
                var totalRow = 0.00;
                conteudo = "<div class=\"col-e\">Receita</div>";
                for (j = 0; j < totCred.length; j++) {
                    totalRow = totalRow + totCred[j];
                    conteudo = conteudo + "<div class=\"col-";
                    if (j % 2 === 0) {
                        conteudo = conteudo + "1 " + (j + 1) + "\"";
                    } else {
                        conteudo = conteudo + "2 " + (j + 1) + "\"";
                    }
                    if (j > 6) {
                        conteudo = conteudo + " style=\"display:none\"";
                    }
                    conteudo = conteudo + "> " + numberFormat(totCred[j]) + "</div>";
                }
                conteudo = conteudo + "<div class=\"col-d\">" + numberFormat(totalRow) + "</div>";
                $("#receitas").html(conteudo);

                totalRow = 0.00;
                conteudo = "<div class=\"col-e\">Despesa</div>";
                for (j = 0; j < totDebt.length; j++) {
                    totalRow = totalRow + totDebt[j];
                    conteudo = conteudo + "<div class=\"col-";
                    if (j % 2 === 0) {
                        conteudo = conteudo + "1 " + (j + 1) + "\"";
                    } else {
                        conteudo = conteudo + "2 " + (j + 1) + "\"";
                    }
                    if (j > 6) {
                        conteudo = conteudo + " style=\"display:none\"";
                    }
                    conteudo = conteudo + "> " + numberFormat(totDebt[j]) + "</div>";
                }
                conteudo = conteudo + "<div class=\"col-d\">" + numberFormat(totalRow) + "</div>";
                $("#despesas").html(conteudo);

                totalRow = 0.00;
                conteudo = "<div class=\"col-e\">Saldo Final</div>";
                for (j = 0; j < totDebt.length; j++) {
                    totalRow = totalRow + (totCred[j] - totDebt[j]);
                    conteudo = conteudo + "<div class=\"col-";
                    if (j % 2 === 0) {
                        conteudo = conteudo + "1 " + (j + 1) + "\"";
                    } else {
                        conteudo = conteudo + "2 " + (j + 1) + "\"";
                    }
                    if (j > 6) {
                        conteudo = conteudo + " style=\"display:none\"";
                    }
                    conteudo = conteudo + "> " + numberFormat((totCred[j] - totDebt[j])) + "</div>";
                }
                conteudo = conteudo + "<div class=\"col-d\">" + numberFormat(totalRow) + "</div>";
                $("#saldoFinal").html(conteudo);

                totalRow = 0.00;
                conteudo = "<div class=\"col-e\">Saldo Inicial</div>";
                for (j = 0; j < totDebt.length; j++) {
                    conteudo = conteudo + "<div class=\"col-";
                    if (j % 2 === 0) {
                        conteudo = conteudo + "1 " + (j + 1) + "\"";
                    } else {
                        conteudo = conteudo + "2 " + (j + 1) + "\"";
                    }
                    if (j > 6) {
                        conteudo = conteudo + " style=\"display:none\"";
                    }
                    conteudo = conteudo + "> " + numberFormat(totalRow) + "</div>";
                    totalRow = totalRow + (totCred[j] - totDebt[j]);
                }
                conteudo = conteudo + "<div class=\"col-d\">" + numberFormat(totalRow) + "</div>";
                $("#saldoInicial").html(conteudo);
            });
            
            $(document).ready(function () {
                $("#scroll").scroll(function () {
                    var loop = $("#receitaRow").val() * 30;
                    if ($(this).scrollTop() > 0) {
                        $("#receitas").addClass("receitas");
                    } else {
                        $("#receitas").removeClass("receitas");
                    }
                    if ($(this).scrollTop() > loop) {
                        $("#despesas").addClass("despesas");
                    } else {
                        $("#despesas").removeClass("despesas");
                    }
                });
            });
            
            var coluna = 1;
            function next() {
                if (coluna < $("#receitaCol").val()) {
                    $("." + (coluna + 0)).each(function () {
                        $("." + (coluna + 0)).hide();
                    });
                    $("." + (coluna + 7)).each(function () {
                        $("." + (coluna + 7)).show();
                    });
                    coluna = coluna + 1;
                }
            }
            function prev() {
                if (coluna > 1) {
                    $("." + (coluna + 6)).each(function () {
                        $("." + (coluna + 6)).hide();
                    });
                    $("." + (coluna - 1)).each(function () {
                        $("." + (coluna - 1)).show();
                    });
                    coluna = coluna - 1;
                }
            }
        </script>                   
    </body>
    <?php odbc_close($con); ?>
</html>