<?php

include './../../Connections/configini.php';
$mdl_aca_ = returnPart($_SESSION["modulos"], 6);
$lit = array("Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez");
$num = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");

$data1 = '01_' . $_GET['dtini'];
$dataAux = explode("_", $_GET['dtfim']);
$data2 = date("t", mktime(0, 0, 0, $dataAux[0], '01', $dataAux[1])) . "_" . $_GET['dtfim'];
$mesIni = date('Y-m-01', strtotime(str_replace('_', '-', $data1)));
$mesFim = date("Y-m-01", strtotime(str_replace('_', '-', $data2)));
$aux[] = "";

while ($mesIni <= $mesFim) {
    $meses[] = str_replace($num, $lit, date("m", strtotime($mesIni))) . "/" . date("Y", strtotime($mesIni));
    $mesIni = date('Y-m-d', strtotime("+1 months", strtotime($mesIni)));
    $aux[] = date("m", strtotime($mesIni)) . date("Y", strtotime($mesIni));
}

$dtInicio = $data1;
$dtFim = $data2;
$groupNm = "vendedor_comissao";
$inner = "  left join sf_fornecedores_despesas r on vi.vendedor_comissao = r.id_fornecedores_despesas
            inner join sf_usuarios u on UPPER(u.login_user) = UPPER(sf_vendas.vendedor)";
if ($mdl_aca_ == 1) {
    $groupNm = "r.id_fornecedores_despesas";
    if ($_GET['txtTpGrupo'] == 1) {
        $inner = "left join sf_fornecedores_despesas f on (f.id_fornecedores_despesas = vp.favorecido or sf_vendas.cliente_venda = f.id_fornecedores_despesas)
                  left join sf_fornecedores_despesas r on r.id_fornecedores_despesas = f.prof_resp";
    } else {
        $inner = "left join sf_fornecedores_despesas f on vp.favorecido = f.id_fornecedores_despesas 
                  left join sf_fornecedores_despesas f2 on sf_vendas.cliente_venda = f2.id_fornecedores_despesas 
                  left join sf_usuarios u on (f.id_user_resp = u.id_usuario or f2.id_user_resp = u.id_usuario ) 
                  left join sf_fornecedores_despesas r on u.funcionario = r.id_fornecedores_despesas ";
    }
}

$sQuery = "set dateformat dmy;
            select YEAR(data_venda) ano,MONTH(data_venda) mes, 
            ISNULL(" . $groupNm . ",0) codigo, ISNULL(r.razao_social,'NAO DEFINIDO') nome,
            ISNULL(descricao_grupo,'NAO DEFINIDO') grupo,
            sum(CASE 
                WHEN p.tipo = 'P' THEN 
                    (CASE 
                     WHEN r.tbcomiss_prod = 2 THEN cast(((vi.valor_total - (p.valor_custo_medio * vi.quantidade)) * r.comiss_prod / 100) AS NUMERIC(15,2))  
                     WHEN r.tbcomiss_prod = 1 THEN cast(((vi.valor_total - (p.valor_custo_medio * vi.quantidade)) * p.valor_comissao_venda / 100) AS NUMERIC(15,2)) ELSE 0 END)
                WHEN p.tipo = 'S' THEN 
                    CASE 
                    WHEN r.tbcomiss_serv = 2 THEN cast(((vi.valor_total - (p.valor_custo_medio * vi.quantidade)) * r.comiss_serv / 100) AS NUMERIC(15,2)) 
                    WHEN r.tbcomiss_serv = 1 THEN cast(((vi.valor_total - (p.valor_custo_medio * vi.quantidade)) * p.valor_comissao_venda / 100) AS NUMERIC(15,2)) ELSE 0 END
                ELSE 
                    CASE 
                    WHEN r.tbcomiss_plano = 2 THEN cast((pm.valor_mens * r.comiss_plano / 100) AS NUMERIC(15,2)) 
                    WHEN r.tbcomiss_plano = 1 THEN cast((pm.valor_mens * pp.valor_comissao / 100) AS NUMERIC(15,2)) ELSE 0 END
                END ) valor
            from sf_vendas
            inner join sf_vendas_itens vi on vi.id_venda = sf_vendas.id_venda
            left join sf_vendas_planos_mensalidade pm on vi.id_item_venda = pm.id_item_venda_mens
            left join sf_produtos_parcelas pp on pm.id_parc_prod_mens = pp.id_parcela
            left join sf_vendas_planos vp on pm.id_plano_mens = vp.id_plano
            " . $inner . "
            left join sf_grupo_cliente gc on r.grupo_pessoa = gc.id_grupo
            inner join sf_produtos p on p.conta_produto = vi.produto
            where 
            sf_vendas.status = 'Aprovado' and cov = 'V'
            and cast(data_venda as date) between REPLACE('" . $dtInicio . "','_','/') and REPLACE('" . $dtFim . "','_','/')
            and valor_total > 0
            and valor_total > 0 and sf_vendas.empresa = " . $_GET['empresa'] . "
            and r.dt_demissao is null    
            group by YEAR(data_venda), MONTH(data_venda), " . $groupNm . ", r.razao_social, gc.descricao_grupo
            order by gc.descricao_grupo,  codigo, ano desc, mes desc";
//echo $sQuery;
$cur = odbc_exec($con, $sQuery);
$idComAnt = -1;

while ($RFP = odbc_fetch_array($cur)) {
    if ($idComAnt !== -1 && $idComAnt <> $RFP['codigo']) {
        $myArray[0][] = $row;
    }
    if ($idComAnt == -1 || $idComAnt <> $RFP['codigo']) {
        $key = array_search($RFP['mes'] . $RFP['ano'], $aux);
        $row = array();
        $row["grupo"] = utf8_encode($RFP['grupo']);
        $row["nome"] = utf8_encode($RFP['nome']);
        foreach ($meses as $value) {
            $row[$value] = escreverNumero(0, 1);
        }
        $row[$meses[$key]] = escreverNumero($RFP['valor'], 1);
    } else {
        $key = array_search($RFP['mes'] . $RFP['ano'], $aux);
        $row[$meses[$key]] = escreverNumero($RFP['valor'], 1);
    }
    $idComAnt = $RFP['codigo'];
}
$myArray[0][] = $row;
$myArray[1] = $meses;
echo json_encode($myArray);
odbc_close($con);