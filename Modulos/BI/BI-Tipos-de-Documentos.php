<?php
include "../../Connections/configini.php";

$F1 = "0";
$F2 = "0";
$F3 = array();
$DateBegin = getData("B");
$DateEnd = getData("E");
$F4 = "4";
$tipo = "C";

if (is_numeric($_GET["fr"])) {
    $F1 = $_GET["fr"];
}
if (is_numeric($_GET["tp"])) {
    $F2 = $_GET["tp"];
}
if (isset($_GET["grp"])) {
    $F3 = explode(",", str_replace(array("[", "]", "\""), "", $_GET["grp"]));
}
if ($_GET["dti"] != "") {
    $DateBegin = str_replace("_", "/", $_GET["dti"]);
}
if ($_GET["dtf"] != "") {
    $DateEnd = str_replace("_", "/", $_GET["dtf"]);
}
if (is_numeric($_GET["gp"])) {
    $F4 = $_GET["gp"];
}
if (is_numeric($_GET["rl"])) {
    $F5 = $_GET["rl"];
}
switch ($F4) {
    case "4":
        $tipo = "C";
        break;
    case "3":
        $tipo = "L";
        break;
    case "2":
        $tipo = "S";
        break;
    case "1":
        $tipo = "P";
        break;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../SpryAssets/SpryValidationTextField.css"/>
        <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>        
        <style>
            #tbReceita td:last-child, #tbReceita th:last-child { text-align:right }
            #tbCredito td, #tbDebito td { padding:5px 10px }
            #tbReceita td { padding:5px 10px }
            #tbDebito { margin-top:20px }
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"><span class="ico-arrow-right"></span></div>
                        <h1>SIVIS B.I.<small>Relatório de Consulta por Tipos de Documentos</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="block" style="font-size:13px">
                                <div style="float: left;margin-top: 15px;">
                                    <button type="button" title="Imprimir" class="button button-blue btn-primary" style="margin:0;" onclick="printTipoDocumento()"><span class="ico-print icon-white"></span></button>
                                </div>                                
                                <div style="float:left;margin-left: 0.5%;">
                                    <label for="txtTpData">Tipo Data:</label>                                 
                                    <select style="width:125px; height:31px" name="txtTpData" id="txtTpData">
                                        <option value="0" <?php echo ($F1 == "0" ? "SELECTED" : ""); ?>>RECEBIMENTO</option>
                                        <option value="1" <?php echo ($F1 == "1" ? "SELECTED" : ""); ?>>VENDA</option>                                    
                                    </select>                                
                                </div>                                    
                                <div style="float:left;margin-left: 0.5%;">
                                    <label for="txtTipoB">Tipo:</label>                                                                 
                                    <select style="width:100px; height:31px" name="txtTipoB" id="txtTipoB">
                                        <option value="0" <?php echo ($F2 == "0" ? "SELECTED" : ""); ?>>RECEITAS</option>
                                        <option value="1" <?php echo ($F2 == "1" ? "SELECTED" : ""); ?>>DESPESAS</option>
                                    </select>
                                </div>
                                <div style="float: left;margin-left: 0.5%;">
                                    <label for="txtTipoC">Tipo</label>
                                    <select name="txtTipoC" id="txtTipoC" style="width:90px; height:31px">
                                        <option value="0" <?php echo ($F5 == "0" ? "SELECTED" : ""); ?>>Sintético</option>
                                        <option value="1" <?php echo ($F5 == "1" ? "SELECTED" : ""); ?>>Analítico</option>
                                    </select>
                                </div>                                
                                <div style="float: left;margin-left: 0.5%;">
                                    <label for="txtTpGrupos">Agrupamento</label>
                                    <select id="txtTpGrupos" style="width:90px; height:31px">
                                        <option value="4" <?php echo ($F4 == "4" ? "SELECTED" : ""); ?>>Clientes</option>
                                        <option value="1" <?php echo ($F4 == "1" ? "SELECTED" : ""); ?>>Produtos</option>
                                        <option value="2" <?php echo ($F4 == "2" ? "SELECTED" : ""); ?>>Serviço</option>
                                        <option value="3" <?php echo ($F4 == "3" ? "SELECTED" : ""); ?>>Planos</option>
                                    </select>
                                </div>                                
                                <div style="float: left;margin-left: 0.5%;">
                                    <label for="mscGrupo">Grupo de Cliente:</label>                                                                        
                                    <select style="min-width:140px;min-height:31px!important;" name="itemsGrupo[]" id="mscGrupo" multiple="multiple" class="select" class="input-medium">                                    
                                        <?php
                                        if ($tipo == "C") {
                                            $query = "select id_grupo id_contas_movimento,descricao_grupo descricao from sf_grupo_cliente where tipo_grupo = 'C' and inativo_grupo = 0 order by descricao_grupo";
                                        } else {
                                            $query = "select id_contas_movimento,descricao from dbo.sf_contas_movimento where tipo = '" . $tipo . "' order by descricao";
                                        }
                                        $cur = odbc_exec($con, $query) or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['id_contas_movimento']; ?>" <?php echo (in_array($RFP['id_contas_movimento'], $F3) ? "selected" : ""); ?>><?php echo utf8_encode($RFP['descricao']); ?></option>
                                        <?php } ?>
                                    </select>                                
                                </div> 
                                <div style="float: left;margin-left: 0.5%;">
                                    <label for="txt_dt_begin">De</label>                                
                                    <input type="text" class="datepicker" style="width:80px; height:31px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial"/>
                                </div>
                                <div style="float: left;margin-left: 0.5%;">
                                    <label for="txt_dt_end">até</label>                                    
                                    <input type="text" class="datepicker" style="width:80px; height:31px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final"/>
                                </div>                                                                                               
                                <div style="float: left;margin-top: 15px;margin-left: 0.5%;">
                                    <button class="button button-turquoise btn-primary" type="button" onclick="AbrirBox(0)" id="btnfind"><span class="ico-search icon-white"></span></button>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div class="box50">
                        <div class="boxhead">
                            <div class="boxtext">Tipos de Documentos</div>
                        </div>
                        <div class="boxtable" style="height: 380px;">
                            <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbReceita">
                                <thead>
                                    <tr>
                                        <th width="60%">DOCUMENTO</th>
                                        <th width="20%"><center>Manual</center></th>
                                        <th width="20%"><center>Automático</center></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <th>TOTAL</th>
                                        <th><div id="txtTotal"><?php echo escreverNumero(0); ?></div></th>
                                        <th><div id="txtAutom"><?php echo escreverNumero(0); ?></div></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="box50" style="margin-left:2.5%">
                        <div class="boxhead">
                            <div class="boxtext">Tipos de Documentos</div>
                        </div>
                        <div class="boxchart" id="chartdiv"></div>
                    </div>                    
                    <div style="clear:both"></div>
                    <span id="divAnalitico" style="display:none">
                        <div class="boxhead">
                            <div class="boxtext" style="position:relative">
                                <div id="lbltitulo" class="boxtext"></div>
                                <div style="top:4px; right:2px; line-height:1; position:absolute">
                                    <button type="button" title="Imprimir" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printTipoDocumentoAnalitico()">
                                        <span class="ico-print icon-white"></span>
                                    </button>
                                    <button type="button" title="Exportar Excel" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="exportarTipoDocumentoAnalitico();">
                                        <span class="ico-download-3 icon-white"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="boxtable">
                            <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbReceitaAnalitico">
                                <thead>
                                    <tr>
                                        <th width="7%">Cód.</th>
                                        <th width="10%">Data</th>
                                        <th width="6%">Matrícula</th>                                        
                                        <th width="23%">Cliente</th>
                                        <th width="10%">Grupo</th>
                                        <th width="20%">Documento</th>
                                        <th width="6%" style="text-align:right">Valor</th>
                                        <th width="6%" style="text-align:right">Juros</th>
                                        <th width="6%" style="text-align:right">Desconto</th>
                                        <th width="6%" style="text-align:right">Total</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>                                
                            </table>
                        </div>
                    </span>                    
                </div>
            </div>
        </div>       
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>        
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/charts.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>  
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/moment.min.js"></script>        
        <script type="text/javascript" src="amcharts/amcharts.js"></script>
        <script type="text/javascript" src="amcharts/serial.js"></script>
        <script type="text/javascript" src="amcharts/pie.js"></script>
        <script type="text/javascript" src="amcharts/themes/light.js"></script>
        <script type='text/javascript' src="../../js/numeral.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script> 
        <script type="text/javascript">

            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);

            $('#txtTpGrupos').change(function () {
                $("#s2id_mscGrupo").parent().show();
                $('#mscGrupo').empty();
                $("#mscGrupo").select2('data', null);
                $.getJSON("grupos.ajax.php", {txtTipo: $(this).val(), ajax: "true"}, function (j) {
                    var options = "";
                    for (var i = 0; i < j.length; i++) {
                        options += "<option value='" + j[i].id + "'>" + j[i].descricao + "</option>";
                    }
                    $("#mscGrupo").append(options);
                    $('#mscGrupo').trigger('change');
                });
            });

            function printTipoDocumento() {
                var pRel = "&NomeArq=" + "Tipos de Documentos" +
                "&lbl=GRUPO|DOCUMENTO|Manual|Automático" +
                "&siz=150|350|100|100" +
                "&pdf=4" + // Colunas do server processing que não irão aparecer no pdf
                "&filter=" + //Label que irá aparecer os parametros de filtro
                "&PathArqInclude=" + "../Modulos/BI/" + finalFind(1, 1).replace("?", "&"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }
            
            function printTipoDocumentoAnalitico() {
                var pRel = "&NomeArq=" + "Tipos de Documentos" + 
                "&pOri=L" +                        
                "&lbl=" + "Cód.|Data|Matrícula|Cliente|Grupo|Documento|Valor|Juros|Desconto|Total" +
                "&siz=" + "60|65|55|280|100|150|75|75|75|75" +
                "&pdf=" + "15" + "&filter=" + 
                "&PathArqInclude=" + "../Modulos/BI/" + finalFind(2, 1).replace("?", "&");
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }

            function exportarTipoDocumentoAnalitico() {
                window.open(finalFind(2, 1) + "&ex=1");
            }

            function AbrirBox(id) {
                var retPrint = "";
                if ($("#txtTpData").val() !== "") {
                    retPrint = retPrint + "&fr=" + $("#txtTpData").val();
                }
                if ($("#txtTipoB").val() !== "") {
                    retPrint = retPrint + "&tp=" + $("#txtTipoB").val();
                }
                if ($("#txt_dt_begin").val() !== "") {
                    retPrint = retPrint + "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "_");
                }
                if ($("#txt_dt_end").val() !== "") {
                    retPrint = retPrint + "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "_");
                }
                if ($('#txtTpGrupos').val() !== "") {
                    retPrint = retPrint + "&gp=" + $('#txtTpGrupos').val();
                }
                if ($('#txtTipoC').val() !== "") {
                    retPrint = retPrint + "&rl=" + $('#txtTipoC').val();
                }
                if ($("#mscGrupo").val() !== null) {
                    var grupos = JSON.stringify($("#mscGrupo").val());
                    retPrint = retPrint + "&grp=" + grupos;
                }
                if (id === 0) {
                    window.location = "BI-Tipos-de-Documentos.php?imp=0" + retPrint;
                } else if (id === 1) {
                    window.location = "BI-Tipos-de-Documentos.php?imp=1" + retPrint;
                }
            }

            function finalFind(ts, imp) {
                var retPrint = "&ts=" + ts + "&imp=" + imp;
                if ($('#txtTpData').val() !== "") {
                    retPrint = retPrint + "&fr=" + $('#txtTpData').val();
                }
                if ($('#txtTipoB').val() !== "") {
                    retPrint = retPrint + "&tp=" + $('#txtTipoB').val();
                }
                if ($('#txt_dt_begin').val() !== "") {
                    retPrint = retPrint + "&dti=" + $('#txt_dt_begin').val().replace(/\//g, "_");
                }
                if ($('#txt_dt_end').val() !== "") {
                    retPrint = retPrint + "&dtf=" + $('#txt_dt_end').val().replace(/\//g, "_");
                }
                if ($('#txtTpGrupos').val() !== "") {
                    retPrint = retPrint + "&gp=" + $('#txtTpGrupos').val();
                }
                if ($('#txtTipoC').val() !== "") {
                    retPrint = retPrint + "&rl=" + $('#txtTipoC').val();
                }
                if ($("#mscGrupo").val() !== null) {
                    var grupos = JSON.stringify($("#mscGrupo").val());
                    retPrint = retPrint + "&grp=" + grupos;
                }
                return "BI-Tipos-de-Documentos_server_processing.php?filial=" + $("#txtMnFilial").val() + retPrint;
            }

            $(document).ready(function () {
                $('#tbReceita').dataTable({
                    bPaginate: false, bFilter: false, bInfo: false, bSort: false,
                    "bProcessing": false,
                    "bServerSide": true,
                    "sAjaxSource": finalFind(1, 0),
                    "fnDrawCallback": function (data) {
                        if (data["aoData"].length > 0) {
                            criaGrafico(data["aoData"]);
                        }
                        var total = parseFloat(0);
                        var totalAutom = parseFloat(0);
                        for (var x = 0; x < data.aoData.length; x++) {
                            total = total + textToNumber(data.aoData[x]._aData[1]);
                            totalAutom = totalAutom + textToNumber(data.aoData[x]._aData[2]);
                        }
                        $("#txtTotal").html(lang["prefix"] + " " + numberFormat(total));
                        $("#txtAutom").html(lang["prefix"] + " " + numberFormat(totalAutom));
                    }
                });
                
                if ($("#txtTipoC").val() === "0") {
                    $("#divAnalitico").hide();
                } else {
                    $('#tbReceitaAnalitico').dataTable({
                        bPaginate: false, bFilter: false, bInfo: false, bSort: false,
                        "bProcessing": false,
                        "bServerSide": true,
                        "sAjaxSource": finalFind(2, 0),
                        "fnInitComplete": function (oSettings, json) {
                        }
                    });
                    $("#divAnalitico").show();
                }
                $("#lbltitulo").text("Detalhamento entre " + $("#txt_dt_begin").val() + " até " + $("#txt_dt_end").val());
            });

            function criaGrafico(data) {
                var chartData = [];
                var i = 0;
                $.each(data, function (key, val) {
                    chartData[i] = {desc: val["_aData"][0], valor: (textToNumber(val["_aData"][1]) + textToNumber(val["_aData"][2]))};
                    i = i + 1;
                });
                var chart = AmCharts.makeChart("chartdiv", {
                    "type": "pie",
                    "theme": "light",
                    "dataProvider": chartData,
                    "valueField": "valor",
                    "titleField": "desc",
                    "numberFormatter": {"precision": 2, "decimalSeparator": lang["centsSeparator"], "thousandsSeparator": lang["thousandsSeparator"]},
                    "pullOutRadius": 30,
                    "autoMargins": false,
                    "labelText": "[[percents]]%",
                    "balloonText": "[[title]]<br/>" + lang["prefix"] + " [[value]]",
                    "export": {
                        "enabled": true,
                        "libs": {
                            "path": "http://www.amcharts.com/lib/3/plugins/export/libs/"
                        }
                    }
                });
            }
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>