<?php
include "./../../Connections/configini.php";
    $imprimir = 0;    
    $DateBegin = date("m/Y", strtotime("-12 month"));
    $DateEnd = date("m/Y");
    
    if ($_GET["dti"] != "") {
        $DateBegin = str_replace("_", "/", $_GET["dti"]);
    }
    if ($_GET["dtf"] != "") {
        $DateEnd = str_replace("_", "/", $_GET["dtf"]);
    }
    if (is_numeric($_GET["imp"]) && $_GET["imp"] > 0) {
        $imprimir = 1;
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../SpryAssets/SpryValidationTextField.css"/>        
        <style> #tbComissao td { padding:5px } </style>
    </head>
    <body>
        <?php if ($imprimir == 0) { ?>
            <div id="loader"><img src="../../img/loader.gif"/></div>
        <?php } ?>
        <div class="wrapper">
            <?php if ($imprimir == 0) {
                include("../../menuLateral.php");
            } else {
                $cur = odbc_exec($con, "select filial from sf_usuarios where id_usuario = '" . $_SESSION["id_usuario"] . "'");
                while ($RFP = odbc_fetch_array($cur)) {
                    $filial = $RFP['filial'];
                    $id_filial = $RFP['filial'];
                }
                $empresaComissao = $filial; ?>
                <input id="txtMnContrato" type="hidden" value="<?php echo $contrato; ?>" />        
                <input id="txtMnFilial" type="hidden" value="<?php echo $filial; ?>" />
                <input id="txtMnLanguage" type="hidden" value="<?php echo $languages; ?>" />            
            <?php } ?>
            <div class="body">
                <?php if ($imprimir == 0) {
                    include("../../top.php");
                    $empresaComissao = $filial;
                } ?>
                <div class="content">
                    <?php if ($imprimir !== 0) { $visible = "hidden"; } 
                    if ($imprimir == 0) { ?>
                    <div class="page-header" <?php echo $visible; ?>>
                        <div class="icon"><span class="ico-arrow-right"></span></div>
                        <h1>SIVIS B.I.<small>Comissionados</small></h1>
                    </div>
                    <?php } ?>
                    <div class="row-fluid" <?php echo $visible; ?>>
                        <div class="span12">
                            <div class="block" style="font-size:13px">
                                <div style="float:left;">
                                    <div style="width: 100%;">Periodo:</div>
                                    <input name="txtDtIni" id="txtDtIni" maxlength="7" type="text" style="width:85px; text-align:center;float: left;" value="<?php echo $DateBegin;?>" placeholder="Mês/Ano"/>
                                    <div style="float: left;padding: 5px;">até</div>
                                    <input name="txtDtFim" id="txtDtFim" maxlength="7" type="text" style="width:85px; text-align:center" value="<?php echo $DateEnd;?>" placeholder="Mês/Ano"/>
                                </div>
                                <?php if ($mdl_aca_ == 1 ) { ?>
                                <div style="float:left;margin-left: 1%">
                                    <div width="100%">Grupo:</div>
                                    <select id="txtTipo" style="width:166px;">
                                        <option value="1" <?php if($F3 == 1){ echo "SELECTED";}?>>Professor Responsável</option>
                                        <option value="2" <?php if($F3 == 2){ echo "SELECTED";}?>>Usuario Responsável</option>
                                    </select>
                                </div>
                                <?php } ?>
                                <button style="margin-top: 13px;margin-left: 1%;" class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind"><span class="ico-search icon-white"></span></button>
                                <span style="float:right">
                                    <button name="btnPrint" class="button button-blue btn-primary" type="button" onclick="AbrirBox()"><span class="ico-print"></span></button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead" <?php echo $visible; ?>>
                        <div class="boxtext">Comissões</div>
                    </div>
                    <div <?php if ($imprimir == 0) { echo "class=\"boxtable\""; } ?>>
                        <?php if ($imprimir == 1) {
                            $titulo_pagina = "RELATÓRIO DE COMISSIONADOS";
                            include "../Financeiro/Cabecalho-Impressao.php";
                        } ?>
                        <table <?php if ($imprimir == 0) { echo "class=\"table\""; } else { echo "border=\"1\""; } ?> style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbComissao">
                            <thead>
                                <tr>
                                    <th style="width:140px"></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>        
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>        
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script> 
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            
            $("#txtDtIni, #txtDtFim").mask("99/9999");
            
            function AbrirBox() {   
                if(validaDt()){
                    var retPrint = "";
                    retPrint = retPrint +"&dti="+ $("#txtDtIni").val().replace(/\//g, "_");
                    retPrint = retPrint +"&dtf="+ $("#txtDtFim").val().replace(/\//g, "_");
                    retPrint = retPrint +"&empresa="+ $("#txtMnFilial").val();
                    window.open("BI-Comissionado-Comparativo.php?imp=1"+ retPrint, "_blank");
                }
            }
            
            function validaDt(){
                var result = true;
                if($("#txtDtIni").val() === "" ||$("#txtDtFim").val() === "" ){
                    result =  false;
                }                                
                if(result === false){
                    bootbox.alert("Período inválido");
                } 
                return result;
            }
            
            $(document).ready(function () {
                preencheTabela();                
                $("#btnfind").click(function(){
                    if (validaDt()) {
                        preencheTabela();
                    }
                });
            });
            
            function preencheTabela(){
                $("#tbComissao thead tr th").remove();
                $("#tbComissao tbody tr").remove();
                $("#tbComissao thead tr").append("<th style=\"width:80px\"></th>");
                $("#tbComissao thead tr").append("<th style=\"width:170px\"></th>");
                var tpGrupo = "";
                if($("#txtTipo").length){
                    tpGrupo = "&txtTpGrupo=" +$("#txtTipo").val();
                }
                $.getJSON("BI-Comissionado-Comparativo_server_processing.php?dtini="+ $("#txtDtIni").val().replace("/","_") +"&dtfim="+ $("#txtDtFim").val().replace("/","_") +tpGrupo+"&empresa=" + $("#txtMnFilial").val(), function (data) {
                    $.each(data[1], function (key, val) {
                        $("#tbComissao thead tr").append("<th style=\"text-align:right\">"+ val +"</th>");
                    });
                    var tbody = "";
                    $.each(data[0],function(index, value){
                        $.each(value,function(k,v){
                            if (k === "grupo") {
                                if (tbody === "") {
                                    tbody += "<tr><td style=\"text-align:left\">"+ v +"</td>";
                                } else {
                                    tbody += "</tr><tr><td style=\"text-align:left\">"+ v +"</td>";
                                }
                            } else if (k === "nome") {
                                tbody += "<td style=\"text-align:left\">"+ v +"</td>";
                            } else {
                                tbody += "<td style=\"text-align:right\">"+ v +"</td>";
                            }
                        });
                    });
                    $("#tbComissao tbody").append(tbody);
                });
            }
        </script>
    </body>
    <?php if ($imprimir == 1) { ?>
        <script type="text/javascript">
            var intervalo = window.setTimeout(geraRel, 1000);
            function geraRel() {
                $(".body").css("margin-left", 0);
                window.print();
                window.close();
            }
        </script>
        <?php }
        odbc_close($con);
    ?>
</html>