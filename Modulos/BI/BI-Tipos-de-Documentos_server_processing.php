<?php

$url = dirname(__FILE__) . "./../../";
require_once($url . 'Connections/configini.php');
if (!isset($_GET['isPdf'])) {
    require_once($url . 'util/util.php');
}
require_once($url . 'util/excel/exportaExcel.php');

$gp = "C";
$tp1 = "D";
$tp2 = "C";
$DateBegin = getData("B");
$DateEnd = getData("E");
$campoValor = "valor_pago";
$whereLanMov = "";
$whereSolAut = "";
$whereVenda = "";
$comando = "";
$grupoArray = [];
$groupBy = array("", "", "");

function formataNumeros($valor) {
    if (is_numeric($_GET['ts']) && $_GET['ts'] > 0) {
        return escreverNumero($valor, 1);
    } else {
        return $valor;
    }
}

if ($_GET['dti'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
}
if ($_GET['dtf'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
}

if (is_numeric($_GET["fr"])) {
    if ($_GET["fr"] == 1) {
        $campoValor = "valor_parcela";
        $whereLanMov .= " AND " . $campoValor . " > 0 AND data_cadastro >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND data_cadastro <= " . valoresDataHora2($DateEnd, "23:59:59");
        $whereSolAut .= " AND " . $campoValor . " > 0 AND data_lanc >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND data_lanc <= " . valoresDataHora2($DateEnd, "23:59:59");
        $whereVenda .= " AND " . $campoValor . " > 0 AND data_venda >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND data_venda <= " . valoresDataHora2($DateEnd, "23:59:59");
    } else {
        $campoValor = "valor_pago";
        $whereLanMov .= " AND " . $campoValor . " > 0 AND data_pagamento >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND data_pagamento <= " . valoresDataHora2($DateEnd, "23:59:59");
        $whereSolAut .= " AND " . $campoValor . " > 0 AND data_pagamento >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND data_pagamento <= " . valoresDataHora2($DateEnd, "23:59:59");
        $whereVenda .= " AND " . $campoValor . " > 0 AND data_pagamento >= " . valoresDataHora2($DateBegin, "00:00:00") . " AND data_pagamento <= " . valoresDataHora2($DateEnd, "23:59:59");
    }
}

if (is_numeric($_GET['tp'])) {
    if ($_GET['tp'] == 0) {
        $tp1 = "C";
        $tp2 = "V";
    }
}

if (isset($_GET['gp'])) {
    switch ($_GET['gp']) {
        case "4":
            $gp = "C";
            break;
        case "3":
            $gp = "L";
            break;
        case "2":
            $gp = "S";
            break;
        case "1":
            $gp = "P";
            break;
    }
}

if (isset($_GET['grp'])) {
    if ($gp == "C") {
        $comando .= " and sf_fornecedores_despesas.grupo_pessoa in (" . str_replace(array("[", "]", "\"", "'"), "", $_GET["grp"]) . ")";
        $whereLanMov .= $comando;
        $whereSolAut .= $comando;
        $whereVenda .= $comando;
    } else {
        $comando .= " and sf_produtos.conta_movimento in (" . str_replace(array("[", "]", "\"", "'"), "", $_GET["grp"]) . ")";
        $whereVenda .= $comando;
    }
}

if (is_numeric($_GET['filial'])) {
    $whereLanMov .= " and sf_lancamento_movimento.empresa = " . $_GET['filial'];
    $whereSolAut .= " and sf_solicitacao_autorizacao.empresa = " . $_GET['filial'];
    $whereVenda .= " and sf_vendas.empresa = " . $_GET['filial'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => 0,
    "iTotalDisplayRecords" => 0,
    "aaData" => array()
);

if ($_GET['ts'] == 0) {
    $select = "select x.descricao,sum(x.valor_pago) valor_pago";
    $groupBy = array(" group by sf_tipo_documento.descricao ", " group by sf_tipo_documento.descricao ", " group by sf_tipo_documento.descricao ");
    $select2 = array("sf_tipo_documento.descricao,sum(" . $campoValor . ") valor_pago", " sf_tipo_documento.descricao,sum(" . $campoValor . ") valor_pago ", " sf_tipo_documento.descricao,sum(" . $campoValor . ") valor_pago ");
} else if ($_GET['ts'] == 1) {
    $select = "select x.descricao,sum(x.valor_pago_manual) valor_pago_manual, sum(x.valor_pago_robo) valor_pago_robo " . ($_GET['imp'] == 1 ? ",descricao_grupo" : "");
    $select2 = array(
        " sf_tipo_documento.descricao,valor_pago valor_pago_manual, 0 valor_pago_robo " . ($_GET['imp'] == 1 ? ($gp == "C" ? ",descricao_grupo" : ",'SEM GRUPO' descricao_grupo") : ""),
        " sf_tipo_documento.descricao,valor_pago valor_pago_manual, 0 valor_pago_robo " . ($_GET['imp'] == 1 ? ($gp == "C" ? ",descricao_grupo" : ",'SEM GRUPO' descricao_grupo") : ""),
        " sf_tipo_documento.descricao,case when syslogin <> 'Sistema' then " .
        "dbo.CALC_PROPORCAO(" . $campoValor . ",valor_total,(select isnull(sum(valor_total),0) from sf_vendas_itens where id_venda = sf_vendas.id_venda)) else 0 end valor_pago_manual, case when syslogin = 'Sistema' then " .
        "dbo.CALC_PROPORCAO(" . $campoValor . ",valor_total,(select isnull(sum(valor_total),0) from sf_vendas_itens where id_venda = sf_vendas.id_venda)) else 0 end valor_pago_robo " . ($_GET['imp'] == 1 ? ($gp == "C" ? ",descricao_grupo" : ",sf_contas_movimento.descricao descricao_grupo") : ""));
} else if ($_GET['ts'] == 2) {
    $select = "select * ";
    $groupBy = array("", "", "");
    $select2 = array("sf_lancamento_movimento.id_lancamento_movimento, data_lanc, id_fornecedores_despesas, razao_social, sf_tipo_documento.descricao,descricao_grupo,0 valor_multa, 0 desconto, 0 valor_bruto, valor_pago ",
    "id_solicitacao_autorizacao, data_lanc, id_fornecedores_despesas, razao_social, sf_tipo_documento.descricao,descricao_grupo,0 valor_multa, 0 desconto, 0 valor_bruto, valor_pago ",
    "sf_vendas.id_venda, data_venda, id_fornecedores_despesas, razao_social, sf_tipo_documento.descricao,descricao_grupo,
    sf_vendas_itens.valor_multa, ((sf_vendas_itens.valor_bruto + sf_vendas_itens.valor_multa) - sf_vendas_itens.valor_total) desconto, sf_vendas_itens.valor_bruto, " . 
    "dbo.CALC_PROPORCAO(" . $campoValor . ",valor_total,(select isnull(sum(valor_total),0) from sf_vendas_itens where id_venda = sf_vendas.id_venda)) valor_pago ");
}

$sQuery = "set dateformat dmy; ";
$sQuery .= $select . " into #tempVenda from (select " . $select2[2] . " from sf_venda_parcelas inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda
left join sf_tipo_documento on sf_venda_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento
inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas
left join sf_grupo_cliente on id_grupo = grupo_pessoa
inner join sf_vendas_itens on sf_vendas_itens.id_venda = sf_vendas.id_venda " .
        ($gp == "C" ? "" : "	
inner join sf_produtos on sf_produtos.conta_produto = sf_vendas_itens.produto
left join sf_contas_movimento on sf_contas_movimento.id_contas_movimento = sf_produtos.conta_movimento") .
        " where status = 'Aprovado' and sf_vendas.cov = '" . $tp2 . "'" . $whereVenda . " AND sf_venda_parcelas.inativo = '0' " . $groupBy[2] .
        ($_GET['ts'] == 1 && $_GET['imp'] == 1 ? ") as x group by x.descricao_grupo,x.descricao order by descricao_grupo,x.descricao;" :
        ($_GET['ts'] != 2 ? ") as x group by x.descricao order by x.descricao;" : ") as x; "));
$sQuery .= $select . " into #tempOutro from (select " . $select2[0] . " from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento
on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas
left join sf_tipo_documento on sf_lancamento_movimento_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento
inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas 
left join sf_grupo_cliente on id_grupo = grupo_pessoa " .
        " where sf_lancamento_movimento.status = 'Aprovado' and sf_contas_movimento.tipo = '" . $tp1 . "'" . $whereLanMov . " AND sf_lancamento_movimento_parcelas.inativo = '0' " . $groupBy[0];
$sQuery .= "union all select " . $select2[1] . " from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas
left join sf_tipo_documento on sf_solicitacao_autorizacao_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento
inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas 
left join sf_grupo_cliente on id_grupo = grupo_pessoa " .
        " where status = 'Aprovado' and sf_contas_movimento.tipo = '" . $tp1 . "'" . $whereSolAut . " AND sf_solicitacao_autorizacao_parcelas.inativo = '0' " . $groupBy[1] .
        ($_GET['ts'] == 1 && $_GET['imp'] == 1 ? ") as x group by x.descricao_grupo,x.descricao order by descricao_grupo,x.descricao;" :
        ($_GET['ts'] != 2 ? ") as x group by x.descricao order by x.descricao;" : ") as x; "));
//echo $sQuery; exit;
odbc_exec($con, $sQuery);
$sQuery = $select . " from (select " . ($_GET['ts'] == 1 && $_GET['imp'] == 1 ? "descricao,valor_pago_manual,valor_pago_robo,descricao_grupo collate SQL_Latin1_General_CP1_CI_AS descricao_grupo" : "*") . " from #tempVenda " .
        "union all select " . ($_GET['ts'] == 1 && $_GET['imp'] == 1 ? "descricao,valor_pago_manual,valor_pago_robo,descricao_grupo collate SQL_Latin1_General_CP1_CI_AS descricao_grupo" : "*") . " from #tempOutro " .
        ($_GET['ts'] == 1 && $_GET['imp'] == 1 ? ") as x group by x.descricao_grupo,x.descricao order by descricao_grupo,x.descricao;" :
        ($_GET['ts'] != 2 ? ") as x group by x.descricao order by x.descricao;" : ") as x; "));
//echo $sQuery; exit;
$cur = odbc_exec($con, $sQuery);
$i = 0;
while ($RFP = odbc_fetch_array($cur)) {
    $row = array();
    if ($_GET['ts'] == 2) {
        $row[] = utf8_encode($RFP['id_venda']);
        $row[] = escreverData($RFP['data_venda']);
        $row[] = utf8_encode($RFP['id_fornecedores_despesas']);
        $row[] = utf8_encode($RFP['razao_social']);
    }    
    if ($_GET['ts'] != 1 || isset($_GET['pdf'])) {
        $row[] =  utf8_encode($RFP['descricao_grupo']);    
    }
    $row[] = utf8_encode($RFP['descricao']);
    if (isset($_GET['pdf'])) {        
        $grp = array($RFP['descricao_grupo'], utf8_encode(($RFP['descricao_grupo'] == "" ? "SEM GRUPO" : $RFP['descricao_grupo'])), 0);
        if (!in_array($grp, $grupoArray)) {
            $grupoArray[] = $grp;
        }
    }
    if ($_GET['ts'] == 1) {
        $row[] = formataNumeros($RFP['valor_pago_manual']);
        $row[] = formataNumeros($RFP['valor_pago_robo']);
    } else {
        $indice = (($RFP['valor_bruto'] - $RFP['desconto']) > 0 ? $RFP['valor_pago'] * 100 / ($RFP['valor_bruto'] - $RFP['desconto']) : 0);
        $multa = $RFP['valor_multa'] * $indice/100;
        $desconto = $RFP['desconto'] * $indice/100;
        $row[] = formataNumeros($RFP['valor_pago'] + $desconto - $multa);
        $row[] = formataNumeros($multa);
        $row[] = formataNumeros($desconto);
        $row[] = formataNumeros($RFP['valor_pago']);
    }
    $output['aaData'][] = $row;
    $i++;
}
if ($_GET['ts'] != 2) {
    $output['aaTotal'][] = array(2, 2, "", "", 0, 0);
    $output['aaTotal'][] = array(3, 3, "", "", 0, 0);
    $output['aaGrupo'] = $grupoArray;
}
$output['iTotalRecords'] = $i;
$output['iTotalDisplayRecords'] = $i;

if (isset($_GET['ex']) && $_GET['ex'] == 1) {
    $topo = '<tr><td><b>Cód. </b></td><td><b>Data </b></td><td><b>Matrícula </b></td><td><b>Cliente </b><td><b>Documento </b></td><td><b>Valor </b></td></tr>';
    exportaExcel($output['aaData'], $topo);
}
//echo json_encode($output); exit;
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
