<?php
include "./../../Connections/configini.php";
$imprimir = 0;
$F1 = "";
$F2 = 0;
$DateBegin = getData("T");
$DateEnd = getData("T");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../SpryAssets/SpryValidationTextField.css"/>
        <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <style>
            #tbReceita td:last-child, #tbReceita th:last-child { text-align:right }
            #tbReceita td:first-child, #tbReceita th:first-child { text-align:center }
            #tbReceita td { padding:5px 10px }
            #tbReceitaAnalitico td { padding:5px 10px }
            #tbReceitaAnalitico td:nth-child(9), #tbReceitaAnalitico th:nth-child(9) { text-align:right }
            #tbReceitaAnalitico td:nth-child(10), #tbReceitaAnalitico th:nth-child(10) { text-align:right }
            #tbReceitaAnalitico td:last-child, #tbReceitaAnalitico th:last-child { text-align:right }
            #tbCredito td, #tbDebito td { padding:5px 10px }
            #tbDebito { margin-top:20px }
            #s2id_mscPlano,#s2id_mscGrupo{
                min-width: 150px;
            }
            #s2id_mscPlano .select2-choices,#s2id_mscGrupo .select2-choices{
                min-height: 29px!important;
            }
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"><span class="ico-arrow-right"></span></div>
                        <h1>SIVIS B.I.<small>Produtividade</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="block" style="font-size:13px">
                                <div style="float: left;margin-right: 0.5%;">
                                    <label for="txtTipo">Responsável</label>
                                    <select id="txtTipo" style="width:100px; height:31px;">
                                        <?php if ($mdl_aca_ == 1) { ?>
                                            <option value="1" <?php
                                            if ($F3 == 1) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo ($mdl_seg_ > 0 ? "Indicador" : "Funcionário");?></option>
                                            <option value="2" <?php
                                            if ($F3 == 2) {
                                                echo "SELECTED";
                                            }
                                            ?>>Usuário</option>
                                            <?php } else { ?>
                                            <option value="0" <?php
                                            if ($F3 == 0) {
                                                echo "SELECTED";
                                            }
                                            ?>>Vendedores</option>
                                            <option value="1" <?php
                                            if ($F3 == 1) {
                                                echo "SELECTED";
                                            }
                                            ?>>Comissionado</option>
                                            <option value="2" <?php
                                            if ($F3 == 2) {
                                                echo "SELECTED";
                                            }
                                            ?>>Indicadores</option>
                                            <option value="3" <?php
                                            if ($F3 == 3) {
                                                echo "SELECTED";
                                            }
                                            ?>>Gerentes</option>
                                                <?php } ?>
                                    </select>
                                </div>
                                <div style="float: left;margin-right: 0.5%;">
                                    <label for="txtUsuario">Usuário</label>
                                    <span id="carregando" name="carregando" style="color:#666; display:none">Aguarde, carregando...</span>
                                    <select id="txtUsuario" style="width:170px; height:31px">
                                        <option style="color: red;" value="0" >Selecione</option>
                                    </select>
                                </div>
                                <div style="float: left;margin-right: 0.5%;">
                                    <label for="txtTipoC">Tipo</label>
                                    <select name="txtTipoC" id="txtTipoC" style="width:90px; height:31px">
                                        <option value="0" <?php
                                        if ($F4 == "0") {
                                            echo "SELECTED";
                                        }
                                        ?>>Sintético</option>
                                        <option value="1" <?php
                                        if ($F4 == "1") {
                                            echo "SELECTED";
                                        }
                                        ?>>Analítico</option>
                                    </select>
                                </div>
                                <div style="float: left;margin-right: 0.5%;">
                                    <label for="txtTpGrupos">Agrupamento</label>
                                    <select id="txtTpGrupos" style="width:90px; height:31px">
                                        <option value="0">Todos</option>
                                        <option value="1">Produtos</option>
                                        <option value="2">Serviço</option>
                                        <option value="3">Planos</option>
                                    </select>
                                </div>
                                <div style="float: left;margin-right: 0.5%;display: none;">
                                    <label for="mscGrupo">Grupo</label>
                                    <select name="itemsGrupo[]" id="mscGrupo" multiple="multiple" class="select" style="min-width:160px;min-height:31px!important;" class="input-medium">
                                    </select>
                                </div>
                                <div style="float: left;margin-right: 0.5%;">
                                    <label for="txtForma">Categoria</label>
                                    <select id="txtForma" style="width:90px; height:31px">
                                        <option value="0" <?php
                                        if ($F2 == 0) {
                                            echo "SELECTED";
                                        }
                                        ?>>Todos</option>
                                        <option value="1" <?php
                                        if ($F2 == 1) {
                                            echo "SELECTED";
                                        }
                                        ?>>Produtos</option>
                                        <option value="2" <?php
                                        if ($F2 == 2) {
                                            echo "SELECTED";
                                        }
                                        ?>>Serviços</option>
                                        <?php if ($mdl_aca_ == 1) { ?>
                                            <option value="3" <?php
                                            if ($F2 == 3) {
                                                echo "SELECTED";
                                            }
                                            ?>>Planos</option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div style="float: left;margin-right: 0.5%;display: none;">
                                    <label for="mscPlano">Item</label>
                                    <select name="itemsPlano[]" id="mscPlano" multiple="multiple" class="select" style="min-width:160px;min-height:31px!important;" class="input-medium">
                                    </select>
                                </div>
                                <div style="float: left;margin-right: 0.5%;">
                                    <label for="txt_dt_begin">De</label>
                                    <input type="text" class="datepicker" style="width:80px; height:31px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial"/>
                                </div>
                                <div style="float: left;margin-right: 0.5%;">
                                    <label for="txt_dt_end">até</label>
                                    <input type="text" class="datepicker" style="width:80px; height:31px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final"/>
                                </div>
                                <div style="float: left;margin-right: 0.5%;">
                                    <label for="txtProvisao">Modo</label>
                                    <select name="txtProvisao" title="Teste" id="txtProvisao" style="width:110px; height:31px">
                                        <option value="0">Venda</option>
                                        <option value="2">Recebimento</option>
                                        <option value="3">Previsão</option>
                                    </select>
                                </div>
                                <div style="float: left;margin-right: 0.5%;">
                                    <label for="txtOrdenar">Ordem</label>
                                    <select name="txtOrdenar" id="txtOrdenar" style="width:65px; height:31px">
                                        <option value="0">Data</option>
                                        <option value="1">Matrí.</option>
                                    </select>
                                </div>
                                <div style="float: left;margin-top: 15px;">
                                    <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind"><span class="ico-search icon-white"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box50">
                        <div class="boxhead" style="position: relative;">
                            <div class="boxtext" style="position:relative">
                            Produtividade</div>
                            <div style="top:4px; right:2px; line-height:1; position:absolute">
                                <button type="button" title="Imprimir" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printRenovacoes('I');">
                                    <span class="ico-print icon-white"></span>
                                </button>
                                <button type="button" title="Exportar Excel" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printRenovacoes('E');">
                                    <span class="ico-download-3 icon-white"></span>
                                </button>
                            </div>                            
                        </div>
                        <div class="boxtable" style="height: 380px;overflow: scroll;">
                            <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbReceita">
                                <thead>
                                    <tr>
                                        <th width="10%">TIPO</th>
                                        <th width="70%">DESCRIÇÃO</th>
                                        <th width="20%"><center>VALOR</center></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <th>TOTAL</th>
                                        <th colspan="2"><div id="txtTotal"><?php echo escreverNumero(0); ?></div></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="box50" style="margin-left:2.5%">
                        <div class="boxhead">
                            <div class="boxtext">Gráfico</div>
                        </div>
                        <div class="boxchart" id="chartdiv"></div>
                    </div>
                    <div style="clear:both"></div>
                    <span id="divAnalitico" style="display:none">
                        <div class="boxhead">
                            <div class="boxtext" style="position:relative">
                                <div id="lbltitulo" class="boxtext"></div>
                                <div style="top:4px; right:2px; line-height:1; position:absolute">
                                    <button type="button" title="Imprimir" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printProdutividade('I')">
                                        <span class="ico-print icon-white"></span>
                                    </button>
                                    <button type="button" title="Exportar Excel" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printProdutividade('E');">
                                        <span class="ico-download-3 icon-white"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="boxtable">
                            <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbReceitaAnalitico">
                                <thead>
                                    <tr>
                                        <th width="4%">Mat.</th>
                                        <th width="5%">Dt.Venc.</th>
                                        <th width="5%">Dt.Pag.</th>
                                        <th width="6%">Vendedor</th>
                                        <th width="8%">Grupo</th>
                                        <th width="14%">Cliente</th>
                                        <th width="13%">Serviço</th>
                                        <th width="8%">Comissionado</th>
                                        <th width="5%" style="text-align:right">Qtd.</th>                                        
                                        <th width="5%" style="text-align:right">Val.Plano</th>
                                        <th width="5%" style="text-align:right">Juros</th>
                                        <th width="5%" style="text-align:right">Desc.</th>
                                        <th width="5%" style="text-align:right">V.Parc.</th>
                                        <th width="6%" style="text-align:right">D.Com.</th>
                                        <th width="6%" style="text-align:right">Valor</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="4"></th>
                                        <th>Clientes</th>
                                        <th style="text-align:right"><div id="txtClientes">0</div></th>
                                        <th colspan="2">TOTAL</th>
                                        <th style="text-align:right"><div id="txtQtd"><?php echo escreverNumero(0, 0);?></div></th>
                                        <th style="text-align:right"><div id="txtValorPlano"><?php echo escreverNumero(0);?></div></th>
                                        <th style="text-align:right"><div id="txtJuros"><?php echo escreverNumero(0);?></div></th>
                                        <th style="text-align:right"><div id="txtDesconto"><?php echo escreverNumero(0);?></div></th>
                                        <th style="text-align:right"><div id="txtParcelas"><?php echo escreverNumero(0);?></div></th>
                                        <th style="text-align:right"><div id="txtDesComis"><?php echo escreverNumero(0);?></div></th>
                                        <th style="text-align:right"><div id="txtValor"><?php echo escreverNumero(0);?></div></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </span>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/charts.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="amcharts/amcharts.js"></script>
        <script type="text/javascript" src="amcharts/pie.js"></script>
        <script type="text/javascript" src="amcharts/themes/light.js"></script>
        <script type='text/javascript' src="../../js/numeral.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>
        <script type="text/javascript">

            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
            $("#s2id_mscPlano, #txtOrdenar, #s2id_mscGrupo").parent().hide();
            carregaUsuarios();

            $('#txtTipo').change(function () {
                if ($(this).val()) {
                    carregaUsuarios();
                } else {
                    $('#txtUsuario').html('<option value="null" >Selecione</option>');
                }
            });

            $('#txtTipoC').change(function () {
                if ($(this).val() === "1") {
                    $("#txtOrdenar").parent().show();
                } else {
                    $("#txtOrdenar").parent().hide();
                }
            });

            $('#txtForma').change(function () {
                if ($(this).val() === "3") {
                    $('#txtProvisao').append($('<option>', {value: 1,text: 'Provisão'}));
                } else {
                    $("#txtProvisao option[value='1']").remove();
                }
                if ($(this).val() === "0") {
                    $("#s2id_mscPlano").parent().hide();
                    $("#s2id_mscPlano").select2("val", "");
                } else {
                    $("#s2id_mscPlano").parent().show();
                    abrePlanosProd();
                }
            });

            $('#txtTpGrupos').change(function () {
                $("#txtForma").val($(this).val());
                if ($(this).val() === "3") {
                    $('#txtProvisao').append($('<option>', {value: 1,text: 'Provisão'}));
                } else {
                    $("#txtProvisao option[value='1']").remove();
                }
                if ($(this).val() !== "0") {
                    $("#txtForma").attr("disabled", true);
                    $("#s2id_mscPlano").parent().show();
                    $("#s2id_mscGrupo").parent().show();
                    $('#mscGrupo').empty();
                    $("#mscGrupo").select2('data', null);
                    abrePlanosProd();
                    $.getJSON("grupos.ajax.php", {txtTipo: $(this).val(), ajax: "true"}, function (j) {
                        var options = "";
                        for (var i = 0; i < j.length; i++) {
                            options += "<option value='" + j[i].id + "'>" + j[i].descricao + "</option>";
                        }
                        $("#mscGrupo").append(options);
                        $('#mscGrupo').trigger('change');
                    });
                }else{
                    $("#txtForma").attr("disabled", false);
                    $("#s2id_mscPlano").select2("val", "");
                    $("#s2id_mscGrupo").select2("val", "");
                    $("#s2id_mscPlano").parent().hide();
                    $("#s2id_mscGrupo").parent().hide();
                }
            });

            $("#mscGrupo").change(function () {
                if($("#mscGrupo").val() !== null){
                    $('#mscPlano').empty();
                    $("#mscPlano").select2('data', null);
                    $("#s2id_mscPlano").select2("val", "");
                    $.getJSON("produtos.ajax.php", {groups: JSON.stringify($("#mscGrupo").val()), PorGrupo: "Yes"}, function (j) {
                        var options = "";
                        for (var i = 0; i < j.length; i++) {
                            options += "<option value='" + j[i].conta_produto + "'>" + j[i].descricao + "</option>";
                        }
                        $("#mscPlano").append(options);
                        $('#mscPlano').trigger('change');
                    });
                }else{
                    abrePlanosProd();
                }
            });

            $("#btnfind").click(function () {
                $("#loader").show();
                tbReceita.fnReloadAjax(finalFind(1));
                if ($("#txtTipoC").val() === "0") {
                    $("#divAnalitico").hide();
                } else {
                    tbReceitaAnalitico.fnReloadAjax(finalFind(2));
                    $("#divAnalitico").show();
                }
                $("#lbltitulo").text("Detalhamento entre " + $("#txt_dt_begin").val() + " até " + $("#txt_dt_end").val());
            });

            function printProdutividade(tp) {
                var pRel = "&NomeArq=" + "Produtividade" +
                        "&pOri=L" +
                        "&lbl=" + "Mat.|Dt.Venc.|Dt.Pag.|Vendedor|Grupo|Cliente|Serviço|Comissionado|Qtd.|Val.Plano|Juros|Desconto|Tot.Parcel.|Desc/Comis.|Valor" + (tp === "E" ? "|Recibo|Tipo" : "") +
                        "&siz=" + "40|50|50|65|65|120|110|90|60|60|60|60|60|60|60" + (tp === "E" ? "|60|100" : "") +
                        "&pdf=" + (tp === "E" ? "15" : "21") +
                        "&filter=" + $("#lbltitulo").html() +
                        "&PathArqInclude=" + "../Modulos/BI/" + finalFind(2).replace("?", "&");
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tp + "&PathArq=GenericModelPDF.php", '_blank');
            }

            function carregaUsuarios() {
                $('#txtUsuario').hide();
                $('#carregando').show();
                $.getJSON('./../Estoque/comissao.ajax.php?search=', {txtTipo: $('#txtTipo').val(), ajax: 'true'}, function (j) {
                    var options = '<option value="0">Selecione</option>';
                    for (var i = 0; i < j.length; i++) {
                        options += '<option value="' + j[i].login_user + '">' + j[i].nome + '</option>';
                    }
                    $('#txtUsuario').html(options).show();
                    $('#carregando').hide();
                });
            }

            function abrePlanosProd(){
                $('#mscPlano').empty();
                $("#mscPlano").select2('data', null);
                $.getJSON("produtos.ajax.php", {txtTipo: $("#txtForma").val(), ajax: "true"}, function (j) {
                    var options = "";
                    for (var i = 0; i < j.length; i++) {
                        options += "<option value='" + j[i].conta_produto + "'>" + j[i].descricao + "</option>";
                    }
                    $("#mscPlano").append(options);
                    $('#mscPlano').trigger('change');
                });
            }

            function finalFind(ts) {
                var retPrint = "?ts=" + ts;
                if ($('#txtTipo').val() !== "") {
                    retPrint = retPrint + "&tp=" + $('#txtTipo').val();
                }
                if ($('#txtUsuario').val() !== "") {
                    retPrint = retPrint + "&id=" + $('#txtUsuario').val();
                }
                if ($('#txtForma').val() !== "") {
                    retPrint = retPrint + "&fm=" + $('#txtForma').val();
                }
                if ($('#mscPlano').val() !== null) {
                    retPrint = retPrint + "&plan=" + $('#mscPlano').val();
                }
                if ($('#txt_dt_begin').val() !== "") {
                    retPrint = retPrint + "&dti=" + $('#txt_dt_begin').val().replace(/\//g, "_");
                }
                if ($('#txt_dt_end').val() !== "") {
                    retPrint = retPrint + "&dtf=" + $('#txt_dt_end').val().replace(/\//g, "_");
                }
                if ($('#txtOrdenar').val() !== "") {
                    retPrint = retPrint + "&ord=" + $('#txtOrdenar').val();
                }
                if ($('#txtProvisao').val() !== "") {
                    retPrint = retPrint + "&pj=" + $('#txtProvisao').val();
                }
                if($("#mscGrupo").val() !== null){
                    var grupos = JSON.stringify($("#mscGrupo").val());
                    retPrint = retPrint + "&grp="+grupos;
                }
                retPrint = retPrint + "&emp=" + $("#txtLojaSel").val();
                return "BI-Produtividade-Funcionario_server_processing.php" + retPrint;
            }

            function criaGrafico(data) {
                var chartData = [];
                var i = 0;
                var total = parseFloat(0);
                var totalOutros = parseFloat(0);
                for (var i = 0; i < data.length; i++) {
                    if (i < 4) {
                        chartData[i] = {desc: data[i]["_aData"][1], valor: textToNumber(data[i]["_aData"][2])};
                    } else {
                        totalOutros = totalOutros + textToNumber(data[i]["_aData"][2]);
                        chartData[4] = {desc: "Outros", valor: totalOutros};
                    }
                    total = total + textToNumber(data[i]["_aData"][2]);
                }
                $("#txtTotal").html(lang["prefix"] + " " + numberFormat(total));
                $("#lblChart").hide();
                if (chartData.length === 0) {
                    $("#lblChart").show();
                }
                var chart = AmCharts.makeChart("chartdiv", {
                    "type": "pie",
                    "theme": "light",
                    "dataProvider": chartData,
                    "valueField": "valor",
                    "titleField": "desc",
                    "numberFormatter": {"precision": 2, "decimalSeparator": lang["centsSeparator"], "thousandsSeparator": lang["thousandsSeparator"]},
                    pullOutRadius: 30,
                    autoMargins: false,
                    "labelText": "[[percents]]%",
                    "balloonText": "[[title]]<br/>" + lang["prefix"] + " [[value]]"
                });
            }

            var tbReceita = $('#tbReceita').dataTable({
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                bSort: false,
                "fnDrawCallback": function (data) {
                    $("#loader").hide();
                    if (data["aoData"].length > 0) {
                        criaGrafico(data["aoData"]);
                    }
                },
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"
                }
            });
            tbReceita.fnReloadAjax(finalFind(1));
            
            function printRenovacoes(tp) {
                var pRel = "&NomeArq=" + "Produtividade" +
                        "&lbl=" + "TIPO|DESCRIÇÃO|VALOR" +
                        "&siz=" + "50|550|100" +
                        "&pdf=" + "4" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + "Produtividade " + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/BI/" + finalFind(1).replace("?", "&"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tp + "&PathArq=GenericModelPDF.php", '_blank');
            }

            var tbReceitaAnalitico = $('#tbReceitaAnalitico').dataTable({
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                bSort: false,
                "fnDrawCallback": function () {
                    var totcliente = 0;
                    var totValorQtd = 0.00;
                    var totValorPlano = 0.00;
                    var totJuros = 0.00;
                    var totDesconto = 0.00;
                    var totParcelas = 0.00;
                    var totDesComis = 0.00;
                    var totValor = 0.00;
                    $('#tbReceitaAnalitico > tbody  > tr').each(function () {
                        if ($('>td:nth-child(9)', $(this)).html() !== undefined) {
                            totcliente = totcliente + 1;
                            totValorQtd += textToNumber($('>td:nth-child(9)', $(this)).html());
                            totValorPlano += textToNumber($('>td:nth-child(10)', $(this)).html());
                            totJuros += textToNumber($('>td:nth-child(11)', $(this)).html());
                            totDesconto += textToNumber($('>td:nth-child(12)', $(this)).html());
                            totParcelas += textToNumber($('>td:nth-child(13)', $(this)).html());
                            totDesComis += textToNumber($('>td:nth-child(14)', $(this)).html());
                            totValor += textToNumber($('>td:nth-child(15)', $(this)).html());
                        }
                    });
                    $('#txtClientes').html(totcliente);
                    $('#txtQtd').html(numberFormat(totValorQtd));
                    $('#txtValorPlano').html(lang["prefix"] + " " + numberFormat(totValorPlano));
                    $('#txtJuros').html(lang["prefix"] + " " + numberFormat(totJuros));
                    $('#txtDesconto').html(lang["prefix"] + " " + numberFormat(totDesconto));
                    $('#txtParcelas').html(lang["prefix"] + " " + numberFormat(totParcelas));
                    $('#txtDesComis').html(lang["prefix"] + " " + numberFormat(totDesComis));
                    $('#txtValor').html(lang["prefix"] + " " + numberFormat(totValor));
                },
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"
                }
            });
                        
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>
