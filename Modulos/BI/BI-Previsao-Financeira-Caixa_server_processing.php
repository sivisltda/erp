<?php

include './../../Connections/configini.php';

$lit = array("Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez");
$num = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
$ano = date("Y");
$j = 0;

if (is_numeric($_GET['ano'])) {
    $ano = $_GET['ano'];
}

$dtInicio = date('d/m/Y', strtotime(date($ano . "-01-01")));
$dtFim = date('d/m/Y', strtotime(date($ano . "-12-31")));

if (is_numeric($_GET['filial'])) {
    $whereVenda = " and sf_vendas.empresa = " . $_GET['filial'];
    $whereLanMov = " and sf_lancamento_movimento.empresa = " . $_GET['filial'];
    $whereSolAut = " and sf_solicitacao_autorizacao.empresa = " . $_GET['filial'];
}

if (is_numeric($_GET["fr"]) && $_GET["fr"] == "0") {
    $dtLancamento = "data_pagamento";
    $whereL = " and td.id_tipo_documento not in (12)";
    $tpValorL = "valor_pago";        
    $dtSolicitacao = "data_pagamento";
    $whereS = " and td.id_tipo_documento not in (12)";
    $tpValorS = "valor_pago";        
    $dtVenda = "data_pagamento";    
    $whereV = " and td.id_tipo_documento not in (12)";    
    $tpValorV = "valor_pago";
} else {   
    $dtLancamento = "data_pagamento";
    $whereL = " and somar_rel = 1";
    $tpValorL = "valor_pago";       
    $dtSolicitacao = "data_lanc";
    $whereS = " and somar_rel = 1";
    $tpValorS = "valor_parcela";           
    $dtVenda = "data_venda";
    $whereV = " and somar_rel = 1";
    $tpValorV = "valor_parcela";    
}

$sQueryLP = "from sf_lancamento_movimento_parcelas
inner join sf_lancamento_movimento on sf_lancamento_movimento.id_lancamento_movimento = sf_lancamento_movimento_parcelas.id_lancamento_movimento
inner join sf_contas_movimento on sf_contas_movimento.id_contas_movimento = sf_lancamento_movimento.conta_movimento
inner join sf_grupo_contas gc on gc.id_grupo_contas = sf_contas_movimento.grupo_conta
inner join sf_tipo_documento td on sf_lancamento_movimento_parcelas.tipo_documento = td.id_tipo_documento
where sf_lancamento_movimento.status = 'Aprovado' and " . $dtLancamento . " is not null
AND sf_lancamento_movimento_parcelas.inativo = '0' " . $whereL . $whereLanMov;

$sQuerySA = "from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao on sf_solicitacao_autorizacao.id_solicitacao_autorizacao = sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao
inner join sf_contas_movimento on sf_contas_movimento.id_contas_movimento = sf_solicitacao_autorizacao.conta_movimento
inner join sf_grupo_contas gc on gc.id_grupo_contas = sf_contas_movimento.grupo_conta
inner join sf_tipo_documento td on sf_solicitacao_autorizacao_parcelas.tipo_documento = td.id_tipo_documento
where status = 'Aprovado' and " . $dtSolicitacao . " is not null and
sf_solicitacao_autorizacao_parcelas.inativo = '0' " . $whereS . $whereSolAut;

$sQueryVD = "from sf_venda_parcelas inner join sf_vendas on sf_vendas.id_venda = sf_venda_parcelas.venda
inner join sf_contas_movimento on sf_contas_movimento.id_contas_movimento = sf_vendas.conta_movimento
inner join sf_grupo_contas gc on gc.id_grupo_contas = sf_contas_movimento.grupo_conta    
inner join sf_tipo_documento td on sf_venda_parcelas.tipo_documento = td.id_tipo_documento
where cov in ('C','V') and sf_vendas.status = 'Aprovado' and " . $tpValorV . " is not null 
and sf_venda_parcelas.inativo in (0,2) " . $whereV . $whereVenda;

//-----------------------------------------------------------------------------------------------------------

if (is_numeric($_GET["tp"]) && $_GET["tp"] == "1") {       
    $sQuery = "set dateformat dmy;
    select x.tipo, x.grupo, x.conta, SUM(x.jan) jan, SUM(x.fev) fev, SUM(x.mar) mar, SUM(x.abr) abr, SUM(x.mai) mai,
    SUM(x.jun) jun, SUM(x.jul) jul, SUM(x.ago) ago, SUM(x.[set]) [set], SUM(x.[out]) [out], SUM(x.nov) nov, SUM(x.dez) dez, SUM(x.total) total from (
    select tipo, gc.descricao grupo, sf_contas_movimento.descricao conta, 
    sum(case when month(" . $dtLancamento . ") = 1 then " . $tpValorL . " else 0 end) jan,
    sum(case when month(" . $dtLancamento . ") = 2 then " . $tpValorL . " else 0 end) fev,
    sum(case when month(" . $dtLancamento . ") = 3 then " . $tpValorL . " else 0 end) mar,
    sum(case when month(" . $dtLancamento . ") = 4 then " . $tpValorL . " else 0 end) abr,
    sum(case when month(" . $dtLancamento . ") = 5 then " . $tpValorL . " else 0 end) mai,
    sum(case when month(" . $dtLancamento . ") = 6 then " . $tpValorL . " else 0 end) jun,
    sum(case when month(" . $dtLancamento . ") = 7 then " . $tpValorL . " else 0 end) jul,
    sum(case when month(" . $dtLancamento . ") = 8 then " . $tpValorL . " else 0 end) ago,
    sum(case when month(" . $dtLancamento . ") = 9 then " . $tpValorL . " else 0 end) [set],
    sum(case when month(" . $dtLancamento . ") = 10 then " . $tpValorL . " else 0 end) [out],
    sum(case when month(" . $dtLancamento . ") = 11 then " . $tpValorL . " else 0 end) nov,
    sum(case when month(" . $dtLancamento . ") = 12 then " . $tpValorL . " else 0 end) dez,
    sum(" . $tpValorL . ") total " . $sQueryLP . " and " . $dtLancamento . " between " . valoresDataHora2($dtInicio, "00:00:00") . " and " . valoresDataHora2($dtFim, "23:59:59") . "
    group by tipo, gc.descricao, sf_contas_movimento.descricao  union all 
    select tipo, gc.descricao grupo, sf_contas_movimento.descricao conta, 
    sum(case when month(" . $dtSolicitacao . ") = 1 then " . $tpValorS . " else 0 end) jan,
    sum(case when month(" . $dtSolicitacao . ") = 2 then " . $tpValorS . " else 0 end) fev,
    sum(case when month(" . $dtSolicitacao . ") = 3 then " . $tpValorS . " else 0 end) mar,
    sum(case when month(" . $dtSolicitacao . ") = 4 then " . $tpValorS . " else 0 end) abr,
    sum(case when month(" . $dtSolicitacao . ") = 5 then " . $tpValorS . " else 0 end) mai,
    sum(case when month(" . $dtSolicitacao . ") = 6 then " . $tpValorS . " else 0 end) jun,
    sum(case when month(" . $dtSolicitacao . ") = 7 then " . $tpValorS . " else 0 end) jul,
    sum(case when month(" . $dtSolicitacao . ") = 8 then " . $tpValorS . " else 0 end) ago,
    sum(case when month(" . $dtSolicitacao . ") = 9 then " . $tpValorS . " else 0 end) [set],
    sum(case when month(" . $dtSolicitacao . ") = 10 then " . $tpValorS . " else 0 end) [out],
    sum(case when month(" . $dtSolicitacao . ") = 11 then " . $tpValorS . " else 0 end) nov,
    sum(case when month(" . $dtSolicitacao . ") = 12 then " . $tpValorS . " else 0 end) dez,
    sum(" . $tpValorS . ") total " . $sQuerySA . " and " . $dtSolicitacao . " between " . valoresDataHora2($dtInicio, "00:00:00") . " and " . valoresDataHora2($dtFim, "23:59:59") . "
    group by tipo, gc.descricao, sf_contas_movimento.descricao  union all 
    select case when cov = 'C' then 'D' else 'C' end tipo, gc.descricao grupo, sf_contas_movimento.descricao conta, 
    sum(case when month(" . $dtVenda . ") = 1 then " . $tpValorV . " else 0 end) jan,
    sum(case when month(" . $dtVenda . ") = 2 then " . $tpValorV . " else 0 end) fev,
    sum(case when month(" . $dtVenda . ") = 3 then " . $tpValorV . " else 0 end) mar,
    sum(case when month(" . $dtVenda . ") = 4 then " . $tpValorV . " else 0 end) abr,
    sum(case when month(" . $dtVenda . ") = 5 then " . $tpValorV . " else 0 end) mai,
    sum(case when month(" . $dtVenda . ") = 6 then " . $tpValorV . " else 0 end) jun,
    sum(case when month(" . $dtVenda . ") = 7 then " . $tpValorV . " else 0 end) jul,
    sum(case when month(" . $dtVenda . ") = 8 then " . $tpValorV . " else 0 end) ago,
    sum(case when month(" . $dtVenda . ") = 9 then " . $tpValorV . " else 0 end) [set],
    sum(case when month(" . $dtVenda . ") = 10 then " . $tpValorV . " else 0 end) [out],
    sum(case when month(" . $dtVenda . ") = 11 then " . $tpValorV . " else 0 end) nov,
    sum(case when month(" . $dtVenda . ") = 12 then " . $tpValorV . " else 0 end) dez,
    sum(" . $tpValorV . ") total " . $sQueryVD . " and " . $dtVenda . " between " . valoresDataHora2($dtInicio, "00:00:00") . " and " . valoresDataHora2($dtFim, "23:59:59") . "
    group by case when cov = 'C' then 'D' else 'C' end, gc.descricao, sf_contas_movimento.descricao 
    ) as x group by x.tipo, x.grupo, x.conta order by x.tipo, x.grupo, 16 desc";    
    //echo $sQuery; exit;
    $cur = odbc_exec($con, $sQuery);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = [];
        $row["tipo"] = $RFP['tipo'];
        $row["grupo"] = utf8_encode($RFP['grupo']);
        $row["conta"] = utf8_encode($RFP['conta']);
        $row["jan"] = escreverNumero($RFP['jan']);
        $row["fev"] = escreverNumero($RFP['fev']);
        $row["mar"] = escreverNumero($RFP['mar']);
        $row["abr"] = escreverNumero($RFP['abr']);
        $row["mai"] = escreverNumero($RFP['mai']);
        $row["jun"] = escreverNumero($RFP['jun']);
        $row["jul"] = escreverNumero($RFP['jul']);
        $row["ago"] = escreverNumero($RFP['ago']);
        $row["set"] = escreverNumero($RFP['set']);
        $row["out"] = escreverNumero($RFP['out']);
        $row["nov"] = escreverNumero($RFP['nov']);
        $row["dez"] = escreverNumero($RFP['dez']);
        $row["total"] = escreverNumero($RFP['total']);
        $myArray[] = $row;
    }
} else {    
    for ($i = 0; $i < 12; $i++) {
        $myArray[$j]['mes'] = str_replace($num, $lit, str_pad($i + 1, 2, "0", STR_PAD_LEFT)) . "/" . $ano;
        $myArray[$j]['receitas'] = 0.00;
        $myArray[$j]['despesas'] = 0.00;
        $j++;
    }
    $sQuery = "set dateformat dmy;select ano,mes,SUM(x.despesas) despesas,SUM(x.receitas) receitas from (
    select YEAR(" . $dtLancamento . ") ano,MONTH(" . $dtLancamento . ") mes,
    isnull(SUM(case when tipo = 'D' then " . $tpValorL . " end),0) despesas,isnull(SUM(case when tipo = 'C' then " . $tpValorL . " end),0) receitas " .
    $sQueryLP . " and " . $dtLancamento . " between " . valoresDataHora2($dtInicio, "00:00:00") . " and " . valoresDataHora2($dtFim, "23:59:59") . "
    group by YEAR(" . $dtLancamento . "),MONTH(" . $dtLancamento . ")    
    union all
    select YEAR(" . $dtSolicitacao . "),MONTH(" . $dtSolicitacao . "),
    isnull(SUM(case when tipo = 'D' then " . $tpValorS . " end),0) despesas,isnull(SUM(case when tipo = 'C' then " . $tpValorS . " end),0) receitas " .
    $sQuerySA . " and " . $dtSolicitacao . " between " . valoresDataHora2($dtInicio, "00:00:00") . " and " . valoresDataHora2($dtFim, "23:59:59") . "
    group by YEAR(" . $dtSolicitacao . "),MONTH(" . $dtSolicitacao . ")    
    union all
    select YEAR(" . $dtVenda . "),MONTH(" . $dtVenda . "),
    isnull(SUM(case when cov = 'C' then " . $tpValorV . " end),0) despesas,isnull(SUM(case when cov = 'V' then " . $tpValorV . " end),0) receitas " .
    $sQueryVD . " and " . $dtVenda . " between " . valoresDataHora2($dtInicio, "00:00:00") . " and " . valoresDataHora2($dtFim, "23:59:59") . "
    group by YEAR(" . $dtVenda . "),MONTH(" . $dtVenda . ")
    ) as x group by x.ano, x.mes order by x.ano, x.mes";
    //echo $sQuery; exit;
    $cur = odbc_exec($con, $sQuery);
    while ($RFP = odbc_fetch_array($cur)) {
        $myArray[($RFP["mes"] - 1)]["receitas"] = $RFP['receitas'];
        $myArray[($RFP["mes"] - 1)]["despesas"] = $RFP['despesas'];
    }
    //-----------------------------------------------------------------------------------------------------------
    $sQuery = "set dateformat dmy; select SUM(x.despesas) despesas,SUM(x.receitas) receitas from (
    select isnull(SUM(case when tipo = 'D' then " . $tpValorL . " end),0) despesas,
    isnull(SUM(case when tipo = 'C' then " . $tpValorL . " end),0) receitas " .
    $sQueryLP . " and " . $dtLancamento . " < " . valoresDataHora2($dtInicio, "00:00:00") . " union all
    select isnull(SUM(case when tipo = 'D' then " . $tpValorS . " end),0) despesas,
    isnull(SUM(case when tipo = 'C' then " . $tpValorS . " end),0) receitas " .
    $sQuerySA . " and " . $dtSolicitacao . " < " . valoresDataHora2($dtInicio, "00:00:00") . " union all
    select isnull(SUM(case when cov = 'C' then " . $tpValorV . " end),0) despesas,
    isnull(SUM(case when cov = 'V' then " . $tpValorV . " end),0) receitas " .
    $sQueryVD . " and " . $dtVenda . " < " . valoresDataHora2($dtInicio, "00:00:00") . ") as x";
    //echo $sQuery; exit;
    $cur = odbc_exec($con, $sQuery);
    while ($RFP = odbc_fetch_array($cur)) {
        $myArray[12]['mes'] = "acumulado";
        $myArray[12]["receitas"] = $RFP['receitas'];
        $myArray[12]["despesas"] = $RFP['despesas'];
    }    
}

echo json_encode($myArray);
odbc_close($con);