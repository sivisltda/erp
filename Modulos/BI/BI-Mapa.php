<?php include "./../../Connections/configini.php"; 

$totEquipe = 0;
$cur = odbc_exec($con, "select count(sf_usuarios_dependentes.id_usuario) total from sf_usuarios_dependentes 
inner join sf_usuarios on id_fornecedor_despesas = funcionario
where sf_usuarios.id_usuario = " . $_SESSION["id_usuario"]) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $totEquipe = $RFP['total'];
}

function getInfo($con, $id_usuario, $id_responsavel, $toReturn) {
    $id = 0;
    $sQuery = "select f.id_fornecedores_despesas, f.razao_social, f.profissao, u.imagem,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = f.id_fornecedores_despesas) 'celular'
    from sf_usuarios u
    inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = u.funcionario
    where u.inativo = 0 and tipo = 'E' and u.id_usuario = " . $id_usuario;
    $cur = odbc_exec($con, $sQuery);
    while ($RFP = odbc_fetch_array($cur)) {            
        $row = array();
        $id = $RFP['id_fornecedores_despesas'];
        $row[] = $id;
        $row[] = utf8_encode($RFP['razao_social']);
        $row[] = utf8_encode($RFP['profissao']);
        $row[] = $id_responsavel;
        $row[] = utf8_encode($RFP['imagem']);
        $row[] = utf8_encode($RFP['celular']);
        $toReturn[] = $row;
    }
    $sQuery = "select d.id_usuario from sf_usuarios_dependentes d
    inner join sf_usuarios u on u.id_usuario = d.id_usuario
    inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = u.funcionario
    where id_fornecedor_despesas = " . $id . " and u.inativo = 0 and f.dt_demissao is null";
    $cur = odbc_exec($con, $sQuery);
    while ($RFP = odbc_fetch_array($cur)) {
        $toReturn = getInfo($con, $RFP['id_usuario'], $id, $toReturn);
    }
    return $toReturn;
}
$id_user = (isset($_GET["id"]) && is_numeric($_GET["id"]) ? $_GET["id"] : $_SESSION["id_usuario"]);
$lista = getInfo($con, $id_user, 0, []);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>                
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <link rel="stylesheet" href="../../css/treant/Treant.css">
        <link rel="stylesheet" href="../../css/treant/custom-color-plus-scrollbar.css">    
        <link rel="stylesheet" href="../../css/treant/perfect-scrollbar.css">
        <style type="text/css">
            img {
                width: 60px;
                height: 60px;
            }
        </style>        
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">      
                    <div class="page-header" style="margin-bottom: 10px;">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1>SIVIS B.I.<small>Mapa</small></h1>                    
                    </div>
                    <div class="page-header">
                        <div style="width:20%; float:left;margin-left:0.5%; margin-bottom: 15px;">
                            <span>Usuário Resp.:</span>
                            <select class="input-medium" style="width:100%" name="txtUserResp" id="txtUserResp" <?php echo ($crm_pro_ure_ > 0 && $totEquipe == 0 ? "disabled" : ""); ?>>
                                <option value="null">Selecione</option>
                                <?php $cur = odbc_exec($con, "select distinct id_usuario, nome from sf_usuarios 
                                inner join sf_usuarios_filiais on id_usuario_f = id_usuario 
                                where sf_usuarios.inativo = 0 and id_filial_f in (select id_filial_f from sf_usuarios_filiais where id_usuario_f = '" . $_SESSION["id_usuario"] . "') order by nome") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP['id_usuario']; ?>" <?php echo ($RFP['id_usuario'] == $id_user ? "selected" : ""); ?>><?php echo formatNameCompact(strtoupper(utf8_encode($RFP['nome']))); ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div style="float:left; width: 3%; margin-left: 0.5%; margin-top: 12px; margin-bottom: 15px;">
                            <button class="button button-turquoise btn-primary" type="button" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                        </div>                        
                    </div>
                    <div class="row-fluid">
                        <div class="chart" id="OrganiseChart1"></div>
                    </div>
                </div>
            </div>
        </div>              
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>        
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>        
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script> 
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>  
        <script type="text/javascript" src="../../js/treant/raphael.js"></script>
        <script type="text/javascript" src="../../js/treant/Treant.js"></script>    
        <script type="text/javascript" src="../../js/treant/jquery.mousewheel.js"></script>
        <script type="text/javascript" src="../../js/treant/perfect-scrollbar.js"></script>    
        <script>
            
            $('#btnfind').click(function() {
                window.location='BI-Mapa.php?id=' + $("#txtUserResp").val();
            });
            
            var config = {
                container: "#OrganiseChart1",
                rootOrientation: 'WEST', // NORTH || EAST || WEST || SOUTH
                // levelSeparation: 30,
                siblingSeparation: 20,
                subTeeSeparation: 60,
                scrollbar: "fancy",
                connectors: {
                    type: 'step'
                },
                node: {
                    HTMLclass: 'nodeExample1'
                }
            }
            <?php
                $lista_itens = "";
                for ($i = 0; $i < count($lista); $i++) {
                    $lista_itens .= "m" . $lista[$i][0] . ",";
                    echo ",m" . $lista[$i][0] . " = {" .
                        ($lista[$i][3] > 0 ? "parent: m" . $lista[$i][3] . "," : "") .
                        "text: {
                            name: \"" . $lista[$i][1] . "\",
                            title: \"" . $lista[$i][2] . "\",
                            contact: \"Tel: " . $lista[$i][5] . "\",
                        },
                        image: \"" . ($lista[$i][4] != '' ? "./../../Pessoas/" . $contrato . "/" . $lista[$i][4] : "./../../img/dmitry_m.gif") . "\",
                        HTMLid: \"m" . $lista[$i][0] . "\"
                    }";           
                }
            ?>
            ,ALTERNATIVE = [
                config,
                <?php echo $lista_itens; ?>
            ];
            new Treant(ALTERNATIVE);
        </script>        
    </body>
    <?php odbc_close($con); ?>
</html>