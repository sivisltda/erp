<?php
include "./../../Connections/configini.php";
$DateBegin = date("m/Y", strtotime("-5 month"));
$DateEnd = date("m/Y");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>         
        <link href="../../js/jBox/jBox.css" rel="stylesheet" type="text/css"/> 
        <link href="../../js/jBox/themes/TooltipBorder.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../SpryAssets/SpryValidationTextField.css"/>
        <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <style>
            .informacao { float: right; margin-right: 10px; }
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"><span class="ico-arrow-right"></span></div>
                        <h1>SIVIS B.I.<small>Matrículas x Renovações</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="block" style="font-size:13px">
                                <input id="txt_filter_data" type="hidden" value=""/>
                                <input id="txt_filter_type" type="hidden" value=""/>
                                <div style="float:left; width: 16%;">
                                    <label>Selecione a Filial: </label>
                                    <select id="txtFilial" name="txtFilial[]" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                        <?php
                                        $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo utf8_encode($RFP['id_filial']); ?>" selected><?php echo utf8_encode($RFP['numero_filial']) . " - " . utf8_encode($RFP['Descricao']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>                                
                                <div style="float:left; width:15%; margin-left: 0.5%;">
                                    <label for="txtConsulResp">Usuário Responsável:</label> 
                                    <select name="txtConsulResp" id="txtConsulResp" class="select" style="width:100%; height: 31px;" class="input-medium">
                                        <option value="null">Selecione</option>
                                        <?php $cur = odbc_exec($con, "select distinct id_usuario, nome from sf_usuarios 
                                        inner join sf_usuarios_filiais on id_usuario_f = id_usuario 
                                        where sf_usuarios.inativo = 0 and id_filial_f in (select id_filial_f from sf_usuarios_filiais where id_usuario_f = '" . $_SESSION["id_usuario"] . "') order by nome") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['id_usuario'] ?>"><?php echo formatNameCompact(($RFP['nome'] != ''? utf8_encode($RFP['nome']) : utf8_encode($RFP['login_user']))); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div style="float:left; width:15%; margin-left: 0.5%;">
                                    <label for="txtStatus">Status:</label>
                                    <select name="txtStatus[]" id="txtStatus" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                        <option value="Ativo">Ativo</option>
                                        <option value="AtivoEmAberto">Ativo Em Aberto</option>
                                        <option value="Suspenso">Suspenso</option>
                                        <option value="Inativo">Inativo</option>
                                        <?php if ($mdl_clb_ === "1") { ?>
                                            <option value="Dependente"  <?php
                                            if ($txtStatus === "Dependente") {
                                                echo "SELECTED";
                                            }
                                            ?>>Dependente</option>
                                            <option value="Desligado"  <?php
                                            if ($txtStatus === "Desligado") {
                                                echo "SELECTED";
                                            }
                                            ?>>Desligado</option>
                                            <option value="DesligadoEmAberto"  <?php
                                            if ($txtStatus === "DesligadoEmAberto") {
                                                echo "SELECTED";
                                            }
                                            ?>>Desligado Em Aberto</option>
                                        <?php } ?>                                                                                                                              
                                    </select>
                                </div>
                                <div style="float:left; width: 14%; margin-left: 0.5%;">
                                    <label for="txtGrupo">Grupo de Cliente:</label>
                                    <select name="itemsGrupo[]" id="txtGrupo" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                        <?php $cur = odbc_exec($con, "select id_grupo,descricao_grupo from dbo.sf_grupo_cliente where tipo_grupo = 'C' and inativo_grupo = 0 order by descricao_grupo") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['id_grupo'] ?>"><?php echo utf8_encode($RFP['descricao_grupo']) ?></option>
                                        <?php } ?>
                                        <option value="-1">Sem Grupo</option>
                                    </select>  
                                </div>                                
                                <div style="float: left; margin-left: 0.5%;">
                                    <label for="txt_dt_begin">De</label>                                
                                    <input type="text" maxlength="7" style="width:80px; height: 31px;" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial"/>
                                </div>
                                <div style="float: left; margin-left: 0.5%;">
                                    <label for="txt_dt_end">até</label>                                    
                                    <input type="text" maxlength="7" style="width:80px; height: 31px;" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final"/>                             
                                </div>
                                <div style="float:left; width:15%; margin-left: 0.5%;">
                                    <label for="txtTipo">Tipo:</label> 
                                    <select name="txtTipo" id="txtTipo" class="input-medium" style="width:100%; height: 31px;">
                                        <?php if ($mdl_seg_ === "0") { ?>
                                        <option value="0">Novos e Desligados</option>                                        
                                        <?php } if ($mdl_seg_ === "1") { ?>                                        
                                        <option value="1">Novos e Fim Contratos</option>
                                        <?php } ?>                                        
                                    </select>
                                </div>                                
                                <div style="float: left; margin-top: 15px; margin-left: 0.5%;">
                                    <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind"><span class="ico-search icon-white"></span></button> 
                                </div>
                            </div>                            
                        </div>
                    </div>
                    <div class="box50">
                        <div class="boxhead">
                            <div class="boxtext">
                            <span class="ico-question-sign informacao" title="
                            Comparativo da data de <b>pagamento</b> da mensalidade no período selecionado<br>
                            com a data de pagamento da mensalidade <b>anterior</b>, sua classificação<br> 
                            é levada em consideração pela configuração de <b>Dias para Renovação</b>.<br><br>
                            <b>Novos Planos</b>: Quando é o primeiro pagamento da mensalidade.<br>
                            <b>Retornos</b>: Diferença entre dias de pagamento é maior que o valor da configuração.<br>
                            <b>Renovações</b>: Diferença entre dias de pagamento é menor ou igual que o valor da configuração.<br>
                            <?php if ($mdl_seg_ === "0") { ?>
                            <b>Desligados</b>: Leva em consideração o dia de trancamento.<br>
                            <?php } if ($mdl_seg_ === "1") { ?>                                        
                            <b>Desligados</b>: Leva em consideração a data final do plano somando os <b>Dias para Suspenso</b><br>
                            <?php } ?>                            
                            "></span>                                
                            Novos Planos, Retornos, Renovações e Desligados</div>
                        </div>
                        <div class="boxchart" id="chartBar" style="min-height: 450px;"></div>
                    </div>
                    <div class="box50" style="margin-left:2.5%">
                        <div class="boxhead">
                            <div id="lbltituloConsultor" class="boxtext"></div>
                        </div>
                        <div class="boxchart" id="chartLine" style="min-height: 450px;"></div>
                    </div> 
                    <div style="clear:both"></div>
                    <span id="tbRen">
                        <div class="boxhead">
                            <div class="boxtext" style="position:relative">                                
                                <div id="lbltitulo" class="boxtext">Lista de Novos Planos, Retornos, Renovações e Desligados</div>
                                <div style="top:4px; right:2px; line-height:1; position:absolute">
                                    <button type="button" title="Imprimir" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printProdutividade('I');">
                                        <span class="ico-print icon-white"></span>
                                    </button>
                                    <button type="button" title="Exportar Excel" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printProdutividade('E');">
                                        <span class="ico-download-3 icon-white"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="boxtable">
                            <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbRenovacao">
                                <thead>
                                    <tr>
                                        <th width="20%"><center>Nome</center></th>
                                        <th width="14%">Período.</th>
                                        <th width="5%">Valor</th>
                                        <th width="5%">Desc.</th>
                                        <th width="4%">Tp.Des.</th>
                                        <th width="6%">Até Venc.</th>
                                        <th width="6%">Status.</th>
                                        <th width="11%"><center>Plano</center></th>                            
                                        <th width="9%">Consultor</th>
                                        <th width="8%"><center>Celular</center></th>
                                        <th width="12%"><center>Email</center></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th colspan="10" style="text-align:right">Total:</th>
                                        <th>0</th>
                                    </tr>
                                </tfoot> 
                            </table>
                        </div>
                    </span>                    
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>        
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/charts.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>  
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/moment.min.js"></script>        
        <script type="text/javascript" src="amcharts/amcharts.js"></script>
        <script type="text/javascript" src="amcharts/serial.js"></script>
        <script type="text/javascript" src="amcharts/themes/light.js"></script>
        <script type="text/javascript" src="../../js/jBox/jBox.min.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            
            $("#txt_dt_begin, #txt_dt_end").mask("99/9999");            
            criaGrafico();         
            
            $("#btnfind").click(function () {
                $("#txt_filter_data").val("");
                $("#txt_filter_type").val("");                
                var dt_bgn = $("#txt_dt_begin").val().substring(3, 7) + "-" + $("#txt_dt_begin").val().substring(0, 2) + "-01";
                var dt_end = $("#txt_dt_end").val().substring(3, 7) + "-" + $("#txt_dt_end").val().substring(0, 2) + "-01";
                if(!moment(dt_bgn).isValid() || !moment(dt_end).isValid()) {
                    bootbox.alert("Preencha os valores de período corretamente!");
                } else if (moment(dt_end).diff(moment(dt_bgn), 'months', true) < 1 || moment(dt_end).diff(moment(dt_bgn), 'months', true) > 12) {
                    bootbox.alert("Intervalo entre valores inválido!");
                } else {
                    $("#lbltitulo").text("Detalhamento entre " + $("#txt_dt_begin").val() + " até " + $("#txt_dt_end").val());
                    criaGrafico();
                    detailsChart($("#txt_dt_end").val(), "Novos");
                }
            });            
                        
            var procedencia = AmCharts.makeChart("chartLine", {
                "theme": "light",
                "type": "serial",
                "graphs": [{"balloonText": "[[category]]: [[value]]", "fillAlphas": 1, "lineAlpha": 0.2, "title": "Porcentagem", "valueField": "value", "type": "column"}],
                "rotate": true,
                "categoryField": "title",
                "categoryAxis": {"gridPosition": "start", "fillAlpha": 0.05, "position": "left"}
            });
            
            var tbRenovacao = $('#tbRenovacao').dataTable({
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                bSort: false,
                sAjaxSource: finalFind(0, 1),
                "fnDrawCallback": function (data) {
                    $("#loader").hide();   
                    $("#tbRenovacao tfoot tr th")[1].innerText = data["aoData"].length; 
                    renovacaoChart(data);
                },
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"
                }
            });                    
            
            function renovacaoChart(data) {
                var chartData2 = [];                
                let chartData8 = [];                
                for (var i = 0; i < data["aoData"].length; i++) {
                    let val = data["aoData"][i]["_aData"];
                    chartData8[val[8]] = (!isNaN(chartData8[val[8]]) ? chartData8[val[8]] : 0)  + 1;
                }
                for (const property in chartData8) {
                    chartData2.push({title: property, value: chartData8[property]});
                }
                chartData2.sort(function(a, b){return b.value - a.value;});
                procedencia.dataProvider = chartData2;
                procedencia.validateData();                
            }
            
            function AbrirBox(opc, id) {
                $("#txtId").val("");
                if (opc === 1) {
                    let url = "./../CRM/CRMForm.php?id=" + id;
                    window.open(url,'_blank');
                }
                if (opc === 2) {
                    let url = "./../Academia/ClientesForm.php?id=" + id;
                    window.open(url,'_blank');
                }
                if (opc === 5) {                    
                    abrirTelaBox("../CRM/EmailsEnvioForm.php", 650, 720);
                }
                if (opc === 6) {
                    abrirTelaBox("../CRM/SMSEnvioForm.php", 400, 400);
                }
                if (opc === 7) {
                    abrirTelaBox("../CRM/SMSEnvioForm.php?zap=s", 400, 400);
                }                
            }
            
            function printProdutividade(tp) {
                var pRel = "&NomeArq=" + ($("#txt_filter_type").val() === "Novos" ? "Novos Planos" : 
                ($("#txt_filter_type").val() === "Retornos" ? "Retornos" : $("#txt_filter_type").val() === "Desligados" ? "Desligados" : "Renovações")) + "&pOri=L" +                
                "&lbl=" + "Nome|Período|Valor|Desc.|Tp.Des.|Até Venc.|Status|Plano|Consultor|Celular|Email" +
                "&siz=" + "200|110|60|40|40|40|80|120|100|80|140" +
                "&pdf=" + "13" + "&filter=" + $("#lbltitulo").html() +
                "&PathArqInclude=" + "../Modulos/BI/" + finalFind(1, 1).replace("?", "&");
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tp + "&PathArq=GenericModelPDF.php", '_blank');
            }
            
            function finalFind(imp, ts) {
                var retPrint = "?imp=" + imp + "&ts=" + ts;
                if ($('#txt_dt_begin').val() !== "") {
                    retPrint += "&dti=" + $('#txt_dt_begin').val().replace(/\//g, "_");
                }
                if ($('#txt_dt_end').val() !== "") {
                    retPrint += "&dtf=" + $('#txt_dt_end').val().replace(/\//g, "_");
                }
                if ($('#txt_filter_data').val() !== "") {
                    retPrint += "&fdt=" + $('#txt_filter_data').val().replace(/\//g, "_");
                }
                if ($('#txt_filter_type').val() !== "") {
                    retPrint += "&ftp=" + $('#txt_filter_type').val().replace(/\//g, "_");
                }
                if ($("#txtTipo").val() !== "") {
                    retPrint += '&txtTipo=' + $("#txtTipo").val();
                }                
                if ($('#txtConsulResp').val() !== "null") {
                    retPrint += "&rep=" + $('#txtConsulResp').val().replace(/\//g, "_");
                }
                if ($("#txtStatus").val() !== null) {
                    retPrint += '&txtStatus=' + $("#txtStatus").val();
                }                
                if ($("#txtGrupo").val() !== null) {
                    retPrint += "&gr=" + $("#txtGrupo").val();
                }
                if ($("#txtFilial").val() !== null) {
                    retPrint += "&fil=" + $("#txtFilial").val();
                }                
                return "BI-Matriculas-Renovacoes_server_processing.php" + retPrint;
            }                                              
                        
            function detailsChart(data, tipo){
                $("#loader").show();
                $("#txt_filter_data").val(data);
                $("#txt_filter_type").val(tipo);
                $("#lbltitulo, #lbltituloConsultor").text(($("#txt_filter_type").val() === "Novos" ? "Novos Planos" : 
                ($("#txt_filter_type").val() === "Retornos" ? "Retornos" : $("#txt_filter_type").val() === "Desligados" ? "Desligados" : "Renovações")
                ) + " de " + $("#txt_filter_data").val());                
                tbRenovacao.fnReloadAjax(finalFind(0, 1));
            }

            function criaGrafico() {
                $("#loader").show();
                var chartData = [];
                var i = 0;
                $.getJSON(finalFind(0, 0), function (data) {
                    $.each(data["aaData"], function (key, val) {
                        chartData[i] = {mes: val[0], Novos: val[1] , Retornos: val[2], Renovacoes: val[3], Desligados: val[4]};
                        i++;
                    });
                }).done(function () {
                    $("#loader").hide();
                    var chartBar = AmCharts.makeChart("chartBar", {
                        "type": "serial",
                        "theme": "light",
                        "dataProvider": chartData,
                        "categoryField": "mes",
                        "startDuration": 1,
                        "numberFormatter": {"precision": -1, "decimalSeparator": lang["centsSeparator"], "thousandsSeparator": lang["thousandsSeparator"]},
                        "graphs": [{
                                "title": "Novos Planos",
                                "balloonText": "[[title]]: [[value]]",
                                "labelText": "[[value]]",                                    
                                "fillAlphas": 0.8, "lineAlpha": 0.2, "type": "column",
                                "valueField": "Novos"
                            }, {
                                "title": "Retornos",
                                "balloonText": "[[title]]: [[value]]",
                                "labelText": "[[value]]",                                    
                                "fillAlphas": 0.8, "lineAlpha": 0.2, "type": "column",
                                "valueField": "Retornos"
                            }, {
                                "title": "Renovações",
                                "balloonText": "[[title]]: [[value]]",
                                "labelText": "[[value]]",                                    
                                "fillAlphas": 0.8, "lineAlpha": 0.2, "type": "column",
                                "valueField": "Renovacoes"
                            }, {
                                "title": "Desligados",
                                "balloonText": "[[title]]: [[value]]",
                                "labelText": "[[value]]",
                                "fillAlphas": 0.8, "lineAlpha": 0.2, "type": "column",
                                "valueField": "Desligados"
                            }
                        ],"listeners": [{
                            "event": "clickGraphItem",
                            "method": function(event) {
                                detailsChart(event.item.category, event.item.graph.valueField);
                            }
                        }]                        
                    });                    
                });
            }      
            
            detailsChart(moment().format('MM/YYYY'), "Novos");
            
            new jBox('Tooltip', {
                attach: '.informacao',
                animation: 'pulse',
                theme: 'TooltipBorder',
                addClass: 'tooltipMU',
                content: ''
            });
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>
