<?php
include "../../Connections/configini.php";

$mdl_seg_ = returnPart($_SESSION["modulos"], 14);
$ano = date("Y");

if (is_numeric($_GET["ano"])) {
    $ano = $_GET["ano"];
}

for ($i = 1; $i <= 12; $i++) {
    $id[$i] = "";
    $txtMeta[$i] = escreverNumero(0);
    $txtAtivo[$i] = "0";
    $txtPlano[$i] = "0";
}

if (isset($_POST['bntSave'])) {
    if (is_numeric($_POST['txtAno'])) {
        for ($i = 1; $i <= 12; $i++) {
            if (is_numeric($_POST['txtId' . $i])) {
                odbc_exec($con, "set dateformat dmy;update sf_metas set faturamento_meta = " . valoresNumericos('txtMeta' . $i) .
                                ",faturamento_ativos = " . valoresNumericos('txtAtivo' . $i) .
                                ",faturamento_planos = " . valoresNumericos('txtPlano' . $i) .
                                " where id_meta = " . $_POST['txtId' . $i]) or die(odbc_errormsg());
            } else {
                odbc_exec($con, "set dateformat dmy;insert into sf_metas(data_meta,faturamento_meta,faturamento_ativos, faturamento_planos) values " .
                                "('01/" . $i . "/" . $_POST['txtAno'] . "'," . valoresNumericos('txtMeta' . $i) . "," . valoresNumericos('txtAtivo' . $i) . "," . valoresNumericos('txtPlano' . $i) . ")") or die(odbc_errormsg());
            }
        }
    }
}

if ($ano > 1990 && $ano < 2100) {
    $sQuery = "set dateformat dmy;select id_meta,data_meta,faturamento_meta,faturamento_ativos, faturamento_planos from sf_metas
               where data_meta between '01/01/" . $ano . "' and '31/12/" . $ano . "' order by data_meta";
    $cur = odbc_exec($con, $sQuery);
    while ($RFP = odbc_fetch_array($cur)) {
        $id[escreverData($RFP['data_meta'], "n")] = $RFP['id_meta'];
        $txtMeta[escreverData($RFP['data_meta'], "n")] = escreverNumero($RFP['faturamento_meta']);
        $txtAtivo[escreverData($RFP['data_meta'], "n")] = $RFP['faturamento_ativos'];
        $txtPlano[escreverData($RFP['data_meta'], "n")] = $RFP['faturamento_planos'];
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link href="../../favicon.ico" rel="icon" type="image/ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css"/>
        <link href="../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css"/>
        <link href="../../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css"/>        
        <style>
            body { font-family: sans-serif; }
            .tabScroll {
                float: left;
                width: 300px;
                padding: 3px 0;
                border: 1px solid #e9e9e9;
                border-top: none;
            }
            .tabHead {
                float: left;
                padding: 5px 10px;
                line-height: 18px;
                font-weight: bold;
                background: #E9E9E9;
            }
            .tabBody {
                padding: 2px 5px;
                line-height: 18px;
            }
        </style>        
    </head>
    <body>
        <form action="FormMetas.php?ano=<?php echo $ano; ?>" method="POST" name="frmEnviaDados" id="frmEnviaDados">
            <div class="frmhead">
                <div class="frmtext">Ponto de Equilibrio</div>
                <div class="frmicon" onClick="parent.FecharBox(1);">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <?php for ($i = 1; $i <= 12; $i++) { ?>
                    <input name="txtId<?php echo $i; ?>" id="txtId<?php echo $i; ?>" value="<?php echo $id[$i]; ?>" type="hidden"/>
                <?php } ?>
                <div style="height:42px">
                    <span>Ano:</span>
                    <div style="clear:both"></div>
                    <select name="txtAno" id="txtAno" style="width:220px">
                        <option value="null">Selecione o Ano:</option>
                        <?php for ($i = -1; $i <= 3; $i++) { ?>
                            <option value="<?php echo date("Y", strtotime("+" . $i . " year")); ?>" <?php
                            if (date("Y", strtotime("+" . $i . " year")) == $ano) {
                                echo "selected";
                            }
                            ?>><?php echo date("Y", strtotime("+" . $i . " year")); ?></option>
                                <?php } ?>
                    </select>
                </div>
                <div style="margin-top:10px">
                    <div class="tabHead" style="width:45px">Mês</div>
                    <div class="tabHead" style="width:65px; margin-left:1px">Meta</div>
                    <div class="tabHead" style="width:54px; margin-left:1px">Ativo</div>                    
                    <div class="tabHead" style="width:55px; margin-left:1px"><?php echo ($mdl_seg_ > 0 ? "Veículo" : "Plano");?></div>                    
                    <div class="tabHead" style="width:45px; margin-left:4px">Mês</div>
                    <div class="tabHead" style="width:65px; margin-left:1px">Meta</div>
                    <div class="tabHead" style="width:54px; margin-left:1px">Ativo</div>
                    <div class="tabHead" style="width:55px; margin-left:1px"><?php echo ($mdl_seg_ > 0 ? "Veículo" : "Plano");?></div>
                    <div style="clear:both"></div>
                </div>
                <div class="tabScroll">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="textosComuns">
                        <tr>
                            <td class="tabBody" style="width:50px">Janeiro</td>
                            <td class="tabBody" style="width:80px">
                                <input id="txtMeta1" name="txtMeta1" type="text" maxlength="10" value="<?php echo $txtMeta[1]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtAtivo1" name="txtAtivo1" type="text" maxlength="5" value="<?php echo $txtAtivo[1]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtPlano1" name="txtPlano1" type="text" maxlength="5" value="<?php echo $txtPlano[1]; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="tabBody">Fevereiro</td>
                            <td class="tabBody">
                                <input id="txtMeta2" name="txtMeta2" type="text" maxlength="10" value="<?php echo $txtMeta[2]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtAtivo2" name="txtAtivo2" type="text" maxlength="5" value="<?php echo $txtAtivo[2]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtPlano2" name="txtPlano2" type="text" maxlength="5" value="<?php echo $txtPlano[2]; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="tabBody">Março</td>
                            <td class="tabBody">
                                <input id="txtMeta3" name="txtMeta3" type="text" maxlength="10" value="<?php echo $txtMeta[3]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtAtivo3" name="txtAtivo3" type="text" maxlength="5" value="<?php echo $txtAtivo[3]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtPlano3" name="txtPlano3" type="text" maxlength="5" value="<?php echo $txtPlano[3]; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="tabBody">Abril</td>
                            <td class="tabBody">
                                <input id="txtMeta4" name="txtMeta4" type="text" maxlength="10" value="<?php echo $txtMeta[4]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtAtivo4" name="txtAtivo4" type="text" maxlength="5" value="<?php echo $txtAtivo[4]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtPlano4" name="txtPlano4" type="text" maxlength="5" value="<?php echo $txtPlano[4]; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="tabBody">Maio</td>
                            <td class="tabBody">
                                <input id="txtMeta5" name="txtMeta5" type="text" maxlength="10" value="<?php echo $txtMeta[5]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtAtivo5" name="txtAtivo5" type="text" maxlength="5" value="<?php echo $txtAtivo[5]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtPlano5" name="txtPlano5" type="text" maxlength="5" value="<?php echo $txtPlano[5]; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="tabBody">Junho</td>
                            <td class="tabBody">
                                <input id="txtMeta6" name="txtMeta6" type="text" maxlength="10" value="<?php echo $txtMeta[6]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtAtivo6" name="txtAtivo6" type="text" maxlength="5" value="<?php echo $txtAtivo[6]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtPlano6" name="txtPlano6" type="text" maxlength="5" value="<?php echo $txtPlano[6]; ?>"/>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="tabScroll" style="margin-left:4px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="textosComuns">
                        <tr>
                            <td class="tabBody" style="width:50px">Julho</td>
                            <td class="tabBody" style="width:80px">
                                <input id="txtMeta7" name="txtMeta7" type="text" maxlength="10" value="<?php echo $txtMeta[7]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtAtivo7" name="txtAtivo7" type="text" maxlength="5" value="<?php echo $txtAtivo[7]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtPlano7" name="txtPlano7" type="text" maxlength="5" value="<?php echo $txtPlano[7]; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="tabBody">Agosto</td>
                            <td class="tabBody">
                                <input id="txtMeta8" name="txtMeta8" type="text" maxlength="10" value="<?php echo $txtMeta[8]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtAtivo8" name="txtAtivo8" type="text" maxlength="5" value="<?php echo $txtAtivo[8]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtPlano8" name="txtPlano8" type="text" maxlength="5" value="<?php echo $txtPlano[8]; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="tabBody">Setembro</td>
                            <td class="tabBody">
                                <input id="txtMeta9" name="txtMeta9" type="text" maxlength="10" value="<?php echo $txtMeta[9]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtAtivo9" name="txtAtivo9" type="text" maxlength="5" value="<?php echo $txtAtivo[9]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtPlano9" name="txtPlano9" type="text" maxlength="5" value="<?php echo $txtPlano[9]; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="tabBody">Outubro</td>
                            <td class="tabBody">
                                <input id="txtMeta10" name="txtMeta10" type="text" maxlength="10" value="<?php echo $txtMeta[10]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtAtivo10" name="txtAtivo10" type="text" maxlength="5" value="<?php echo $txtAtivo[10]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtPlano10" name="txtPlano10" type="text" maxlength="5" value="<?php echo $txtPlano[10]; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="tabBody">Novembro</td>
                            <td class="tabBody">
                                <input id="txtMeta11" name="txtMeta11" type="text" maxlength="10" value="<?php echo $txtMeta[11]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtAtivo11" name="txtAtivo11" type="text" maxlength="5" value="<?php echo $txtAtivo[11]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtPlano11" name="txtPlano11" type="text" maxlength="5" value="<?php echo $txtPlano[11]; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="tabBody">Dezembro</td>
                            <td class="tabBody">
                                <input id="txtMeta12" name="txtMeta12" type="text" maxlength="10" value="<?php echo $txtMeta[12]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtAtivo12" name="txtAtivo12" type="text" maxlength="5" value="<?php echo $txtAtivo[12]; ?>"/>
                            </td>
                            <td class="tabBody">
                                <input id="txtPlano12" name="txtPlano12" type="text" maxlength="5" value="<?php echo $txtPlano[12]; ?>"/>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <button class="btn btn-success" type="submit" name="bntSave" title="Gravar"><span class="ico-checkmark"></span> Gravar</button>
                    <button class="btn btn-success" title="Cancelar" onClick="parent.FecharBox(1)"><span class="ico-reply"></span> Cancelar</button>
                </div>
            </div>
        </form>
        <script src="../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
        <script src="../../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/jquery.mousewheel.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/animatedprogressbar/animated_progressbar.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/charts.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/jquery.priceformat.min.js"></script> 
        <script type='text/javascript' src="../../js/util.js"></script>          
        <script type="text/javascript">
            $("#txtMeta1, #txtMeta2, #txtMeta3, #txtMeta4, #txtMeta5, #txtMeta6, #txtMeta7, #txtMeta8, #txtMeta9, #txtMeta10, #txtMeta11, #txtMeta12").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});

            $('#txtAno').change(function () {
                if ($("#txtAno").val() !== "null") {
                    window.location = "FormMetas.php?ano=" + $("#txtAno").val();
                }
            });
        </script>        
        <?php odbc_close($con); ?>
    </body>
</html>