<?php include "./../../Connections/configini.php"; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>         
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../SpryAssets/SpryValidationTextField.css"/>
        <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>        
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"><span class="ico-arrow-right"></span></div>
                        <h1>SIVIS B.I.<small>Leads</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="block" style="font-size:13px">
                                <div style="float:left; width: 13%;">
                                    <div width="100%">Selecione a Filial:</div>
                                    <select id="txtFilial" name="txtFilial[]" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                        <?php
                                        $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo utf8_encode($RFP['id_filial']); ?>" <?php echo ($RFP['id_filial'] == $filial ? "selected" : ""); ?>><?php echo utf8_encode($RFP['numero_filial']) . " - " . utf8_encode($RFP['Descricao']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>                                
                                <div style="float:left; width: 16%;margin-left:1%">
                                    <div width="100%">Procedência:</div>                                        
                                    <select class="select input-medium" style="width:100%" name="txtProcedencia" id="txtProcedencia">
                                        <option value="null">Selecione</option>
                                        <?php $cur = odbc_exec($con, "select id_procedencia,nome_procedencia from sf_procedencia where tipo_procedencia = 0 order by nome_procedencia") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['id_procedencia'] ?>"><?php echo utf8_encode($RFP['nome_procedencia']) ?></option>
                                        <?php } ?>
                                    </select>                                    
                                </div>
                                <div style="float: left; margin-top: 15px; margin-left: 0.5%;">
                                    <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind"><span class="ico-search icon-white"></span></button> 
                                </div>
                            </div>                            
                        </div>
                    </div>
                    <div class="box50">
                        <div class="boxhead">
                            <div class="boxtext">Leads</div>
                        </div>
                        <div class="boxchart" id="chartBar"></div>
                    </div>
                    <div class="box50" style="margin-left:2.5%">
                        <div class="boxhead">
                            <div class="boxtext">Top 10 Indicadores</div>
                        </div>
                        <div class="boxchart" id="chartLine"></div>
                    </div> 
                    <div style="clear:both"></div>
                    <span id="tbRen">
                        <div class="boxhead">
                            <div class="boxtext" style="position:relative">                                
                                <div id="lbltitulo" class="boxtext">Leads Status</div>
                                <div style="top:4px; right:2px; line-height:1; position:absolute">
                                    <button type="button" title="Imprimir" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="imprimir('I');">
                                        <span class="ico-print icon-white"></span>
                                    </button>
                                    <button type="button" title="Exportar Excel" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="imprimir('E');">
                                        <span class="ico-download-3 icon-white"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="boxtable">
                            <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbRenovacao">
                                <thead>
                                    <tr>
                                        <th width="8%"><center>Data Cad.:</center></th>
                                        <th width="15%">Nome/Razão Social</th>
                                        <th width="8%">Tel.:</th>
                                        <th width="15%">E-mail:</th>
                                        <th width="15%">Procedência:</th>
                                        <th width="10%">Status</th>                                    
                                        <th width="12%">Lead Indicador:</th>
                                        <th width="12%">Usuário Resp.:</th>
                                        <th width="5%"><center>Ação:</center></th>
                                    </tr>
                                </thead>
                                <tbody></tbody> 
                            </table>
                            <div style="clear: both;"></div>
                        </div>
                    </span>                    
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>        
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/charts.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>  
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/moment.min.js"></script>        
        <script type="text/javascript" src="amcharts/amcharts.js"></script>
        <script type="text/javascript" src="amcharts/serial.js"></script>
        <script type="text/javascript" src="amcharts/pie.js"></script>        
        <script type="text/javascript" src="amcharts/themes/light.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">                           
            
            var tbRenovacao = $('#tbRenovacao').dataTable({
                "iDisplayLength": 20,
                "aLengthMenu": [20, 30, 40, 50, 100],
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "aoColumns": [{"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false}],
                "sAjaxSource": finalFind(0, 1),
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"},
                "sPaginationType": "full_numbers"
            }); 
            
            criaGrafico();            
            $("#btnfind").click(function () {
                criaGrafico();
                tbRenovacao.fnReloadAjax(finalFind(0, 1));
            });  
            
            function finalFind(imp, tp) {
                var retPrint = "../" + (imp === 1 ? "Modulos/" : "") + "CRM/Lead_server_processing.php?tp=" + tp + "&imp=" + imp;
                if ($("#txtProcedencia").val() !== "null") {
                    retPrint += '&txtProcedencia=' + $("#txtProcedencia").val();
                }
                if ($("#txtFilial").val() !== null) {
                    retPrint += "&fil=" + $("#txtFilial").val();
                }                
                return retPrint;
            }                                     
            
            function imprimir(tipo) {
                var pRel = "&NomeArq=" + "Leads" +
                        "&lbl=Data Cad.|Nome/Razão Social|Tel.|E-mail|Procedência|Status" +
                        "&siz=80|200|80|120|120|100" +
                        "&pdf=8" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + finalFind(1, 1).replace("?", "&"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tipo + "&PathArq=GenericModelPDF.php", '_blank');
            }                                                                
            
            function criaGrafico() {
                $("#loader").show();
                var chartData = [];                
                var chartBar = [];                
                $.getJSON(finalFind(0, 0), function (data) {                                              
                    $.each(data["Status"], function (key, val) {
                        chartBar[key] = val;
                    });
                    $.each(data["Indicadores"], function (key, val) {
                        chartData[key] = val;
                    });
                }).done(function () {
                    $("#loader").hide();
                    var chart = AmCharts.makeChart("chartBar", {
                        "type": "pie",
                        "theme": "light",
                        "dataProvider": chartBar,
                        "valueField": "valor",
                        "titleField": "desc",
                        "legend": {
                            "align": "center",
                            "position": "bottom",
                            "markerType": "circle",
                            "labelText": "[[title]]",
                            "valueText": "",
                            "fontSize": 12
                        },
                        "marginTop": 0,
                        "marginBottom": 0,
                        "pullOutRadius": 10,
                        "autoMargins": false,
                        "labelText": "",
                        "balloonText": "[[title]]: [[value]] ([[percents]]%)"
                    });                                        
                    var procedencia = AmCharts.makeChart("chartLine", {
                        "theme": "light",
                        "type": "serial",
                        "dataProvider": chartData,
                        "graphs": [{"balloonText": "[[category]]: [[value]]", "fillAlphas": 1, "lineAlpha": 0.2, "title": "Porcentagem", "valueField": "value", "type": "column"}],
                        "rotate": true,
                        "categoryField": "title",
                        "categoryAxis": {"gridPosition": "start", "fillAlpha": 0.05, "position": "left"}
                    });
                });
            }
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>
