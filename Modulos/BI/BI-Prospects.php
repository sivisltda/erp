<?php
include "../../Connections/configini.php";
$DateBegin = getData("B");
$DateEnd = getData("E");
$tp = "0";
$tipoChamado = "";
$tipo2 = "null";
$forma = "0";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <style>
            #script { width:100%; position:relative; font-family:"Arial"; font-weight:bold; color:#7b8a90; }
            #scroll { width:100%; overflow-y:scroll; }
            .txt_head { margin-left: 10px; font-size: 17px; font-weight: bold; }
            .txt_text { margin: 0 0 5px 10px; }
            .txt_text b { color: #FF0000; }
            .despesas { width: calc(100% - 17px) !important; position: absolute; top:86px; }
            .receitas { width: calc(100% - 17px) !important; position: absolute; top:56px; }
            .cat-t { width:100%; font-size:12px; text-align:center; }
            .cat-s { width:100%; font-size:12px; text-align:center; border-bottom:1px solid #afafaf; background:#bdbdbd; }
            .cat-1 { width:100%; font-size:12px; text-align:center; border-bottom:1px solid #d4d4d4; }
            .cat-t .col-e { float:left; width:calc(37% - 94.5px); height:19px; padding-top:6px; text-align:right; }
            .cat-s .col-e { float:left; width:calc(37% - 104.5px); line-height:40px; background:#d2d2d2; border-right:10px solid #bdbdbd; }
            .cat-1 .col-e { float:left; width:calc(35.5% - 98px); line-height:30px; background:#e8e8e8; border-right:10px solid #d4d4d4; text-align:left; padding-left:1.5%; cursor:pointer; }
            .cat-t .col-d { float:left; width:95px; height:19px; padding-top:6px; text-align:left; }
            .cat-s .col-d { float:left; width:34px; height:36px; background:#d2d2d2; border-left:10px solid #bdbdbd; padding-top:4px; font-size:10px; }
            .cat-1 .col-d { float:left; width:34px; line-height:30px; background:#e8e8e8; border-left:10px solid #d4d4d4; }
            .cat-t .col-1 { float:left; width:calc(4.5% - 0.75px); height:25px; line-height:25px; }
            .cat-s .col-1 { float:left; width:calc(4.5% - 1.75px); line-height:40px; background:#c7c7c7; border-right:1px solid #bdbdbd; }
            .cat-1 .col-1 { float:left; width:calc(4.5% - 1px); line-height:30px; border-right:1px solid #d4d4d4; cursor:pointer; }
            .dia_normal { background-color: white; color: #7b8a90;}
            .dia_zerado { background-color: #D53F26; color: white;}
            .dia_fds { background-color: #009AD7; color: white;}            
            tspan { cursor: pointer; }
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1>SIVIS B.I.<small>Gestão de Clientes</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="boxhead">
                            <div class="boxtext">Gestão de Clientes</div>
                        </div>
                        <div class="boxtable">
                            <input id="txtFiltro" name="txtFiltro" type="hidden" value="" />
                            <select id="txtFilial" name="txtFilial[]" multiple="multiple" class="select" style="width:200px" class="input-medium">
                                <?php
                                $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo utf8_encode($RFP['id_filial']); ?>" <?php echo ($RFP['id_filial'] == $filial ? "selected" : ""); ?>><?php echo utf8_encode($RFP['numero_filial']) . " - " . utf8_encode($RFP['Descricao']); ?></option>
                                <?php } ?>
                            </select>
                            <input id="txtResponsavel" name="txtResponsavel" type="hidden" value="0" />
                            <select name="mscGrupo" id="mscGrupo" placeholder="Selecione o Usuário" <?php echo ($crm_pro_ure_ > 0 && $totEquipe == 0 ? "disabled" : "");?> style="width: 200px;" class="select" multiple>
                                <?php $cur = odbc_exec($con, "select distinct id_usuario, nome from sf_usuarios 
                                inner join sf_usuarios_filiais on id_usuario_f = id_usuario 
                                where sf_usuarios.inativo = 0 and id_filial_f in (select id_filial_f from sf_usuarios_filiais where id_usuario_f = '" . $_SESSION["id_usuario"] . "') order by nome") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP['id_usuario'] ?>"><?php echo formatNameCompact(($RFP['nome'] != ''? utf8_encode($RFP['nome']) : utf8_encode($RFP['login_user']))); ?></option>
                                <?php } ?>
                            </select>                            
                            De <span id="sprytextfield1"><input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial"></span>
                            até <span id="sprytextfield2"><input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final"></span>
                            <input type="button" title="Buscar" class="button button-turquoise btn-primary" style="line-height:17px" name="btnfind" id="btnfind" value="Buscar">
                            <div style="clear:both; margin-top: 15px;"></div>                            
                            <div class="box50">
                                <div class="boxhead">
                                    <div class="boxtext">Funil de Vendas</div>
                                </div>
                                <div class="boxchart lite" id="funilVendas"></div>
                            </div>
                            <div class="box50" style="margin-left:2.5%">
                                <div class="boxhead">
                                    <div class="boxtext">Contratos</div>
                                </div>
                                <div class="boxchart lite" id="procedencia"></div>
                            </div>
                            <div style="clear:both"></div>
                            <div class="boxhead">
                                <div class="boxtext" style="position:relative">                                    
                                    <span class="title">Leads / Prospects / Clientes</span>
                                    <div style="top:4px; right:2px; line-height:1; position:absolute">
                                        <button type="button" title="Imprimir" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printPDF('I');">
                                            <span class="ico-print icon-white"></span>
                                        </button>
                                        <button type="button" title="Exportar Excel" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printPDF('E');">
                                            <span class="ico-download-3 icon-white"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="boxtable">
                                <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbClientes">
                                    <thead>
                                        <tr>
                                            <th width="7%"><center>Código</center></th>
                                            <th width="30%"><center>Nome</center></th>
                                            <th width="8%">Tipo</th>
                                            <th width="17%">Responsável</th>
                                            <th width="11%"><center>Data</center></th>
                                            <th width="10%"><center>Celular</center></th>
                                            <th width="17%"><center>Email</center></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                                <div style="clear:both"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/amcharts.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/pie.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/funnel.js"></script>        
        <script type="text/javascript" src="../../js/plugins/amcharts_new/serial.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/light.js"></script>       
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/uniform/jquery.uniform.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>
        <script type='text/javascript' src='js/BI-prospects.js'></script>
    </body>
    <?php odbc_close($con); ?>
</html>
