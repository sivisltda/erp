<?php
include "./../../Connections/configini.php";
$imprimir = 0;
$F1 = "";
$F2 = 0;
$DateBegin = getData("B");
$DateEnd = getData("T");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>
        <link href="../../js/jBox/jBox.css" rel="stylesheet" type="text/css"/> 
        <link href="../../js/jBox/themes/TooltipBorder.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../SpryAssets/SpryValidationTextField.css"/>
        <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <style>
            #tbReceita td:last-child, #tbReceita th:last-child { text-align:right }
            #tbReceita td { padding:5px 10px }
            #tbReceitaAnalitico td { padding:5px 10px }
            #tbReceitaAnalitico td:nth-child(7), #tbReceitaAnalitico th:nth-child(7) { text-align:right }
            #tbReceitaAnalitico td:nth-child(8), #tbReceitaAnalitico th:nth-child(8) { text-align:right }
            #tbReceitaAnalitico td:last-child, #tbReceitaAnalitico th:last-child { text-align:right }
            .informacao { float: right; margin-right: 10px; }            
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"><span class="ico-arrow-right"></span></div>
                        <h1>SIVIS B.I.<small>Inadimplência</small></h1>
                        <input name="txtId" id="txtId" type="hidden"/>
                        <input name="txtSendSms" id="txtSendSms" type="hidden"/>
                        <input name="txtSendEmail" id="txtSendEmail" type="hidden"/>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="block" style="font-size:13px">
                                <div style="float:left; width: 16%;">
                                    <label>Selecione a Filial: </label>
                                    <select id="txtFilial" name="txtFilial[]" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                        <?php
                                        $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo utf8_encode($RFP['id_filial']); ?>" selected><?php echo utf8_encode($RFP['numero_filial']) . " - " . utf8_encode($RFP['Descricao']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>                                 
                                <div style="float:left; width:20%; margin-left: 0.5%;">
                                    <label for="txtConsulResp">Usúario Responsável:</label> 
                                    <select name="txtConsulResp" id="txtConsulResp" class="select" style="width:100%; height: 31px;" class="input-medium">
                                        <option value="null">Selecione</option>
                                        <?php $cur = odbc_exec($con, "select distinct id_usuario, nome from sf_usuarios 
                                        inner join sf_usuarios_filiais on id_usuario_f = id_usuario 
                                        where sf_usuarios.inativo = 0 and id_filial_f in (select id_filial_f from sf_usuarios_filiais where id_usuario_f = '" . $_SESSION["id_usuario"] . "') order by nome") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['id_usuario'] ?>"><?php echo formatNameCompact(($RFP['nome'] != ''? utf8_encode($RFP['nome']) : utf8_encode($RFP['login_user']))); ?></option>
                                        <?php } ?>
                                    </select>
                                </div> 
                                <div style="float: left; width:20%; margin-left: 0.5%;">
                                    <label for="mscGrupo">Grupo</label>                                                                        
                                    <select name="itemsGrupo[]" id="mscGrupo" multiple="multiple" class="select" style="min-width:100%;min-height:31px!important;" class="input-medium">                                    
                                        <?php $query = "select id_contas_movimento,descricao from dbo.sf_contas_movimento where tipo = 'L' and inativa = 0";
                                        $cur = odbc_exec($con, $query) or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['id_contas_movimento']; ?>"><?php echo utf8_encode($RFP['descricao']); ?></option>
                                        <?php } ?>
                                    </select>                                
                                </div>                                
                                <div style="float: left;margin-left: 0.5%;">
                                    <label for="txt_dt_begin">De</label>                                
                                    <input type="text" maxlength="7" style="width:80px; height: 31px;" class="datepicker" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial"/>
                                </div>
                                <div style="float: left;margin-left: 0.5%;">
                                    <label for="txt_dt_end">até</label>                                    
                                    <input type="text" maxlength="7" style="width:80px; height: 31px;" class="datepicker" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final"/>                             
                                </div>                                                                                               
                                <div style="float: left;margin-top: 15px;margin-left: 0.5%;">
                                    <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind"><span class="ico-search icon-white"></span></button> 
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div class="box50">
                        <div class="boxhead">                          
                            <div class="boxtext">
                            <span class="ico-question-sign informacao" title="
                            Comparativo entre mensalidades no período,<br>
                            Desconsiderando planos cancelados e sem o primeiro pagamento.<br>
                            Contabiliza as mensalidades pagas e em aberto.<br><br>
                            <b>Número de planos Vencendo</b>: Somatório entre planos Renovados e planos Não-Renovados.<br>
                            <b>Número de planos Renovados</b>: Mensalidades pagas no período.<br>
                            <b>Número de planos Não-Renovados</b>: Mensalidades em aberto no período.<br><br>
                            Baseado em valores pagos / valores de mensalidade.
                            "></span>                                
                            Inadimplência</div>
                        </div>
                        <div class="boxtable" style="height: 380px;">
                            <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbReceita">
                                <thead>
                                    <tr>
                                        <th width="80%">DESCRIÇÃO</th>
                                        <th width="20%"><center>VALOR</center></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <th>Percentual de Evasão</th>
                                        <th><div id="txtTotal">0%</div></th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="box50" style="margin-top: 2%;">
                                <div class="boxhead">
                                    <div class="boxtext">Inadimplentes</div>
                                </div>
                                <div class="boxtable" style="height: 175px;display: flex;align-items: center;justify-content: center;">
                                    <div id="lblNaoRenovados" style="color: #ed1c24;font-size: 80px;font-family: fantasy;">0%</div>
                                </div>                            
                            </div>                                                   
                            <div class="box50" style="margin-top: 2%; margin-left: 2%;">
                                <div class="boxhead">
                                    <div class="boxtext">Adimplentes</div>
                                </div>
                                <div class="boxtable" style="height: 175px;display: flex;align-items: center;justify-content: center;">
                                    <div id="lblRenovados" style="color: gray;font-size: 50px;font-family: fantasy;">0%</div>
                                </div>                            
                            </div>                                                        
                        </div>
                    </div>
                    <div class="box50" style="margin-left:2.5%">
                        <div class="boxhead">
                            <div class="boxtext">Gráfico Inadimplentes x Adimplentes</div>
                        </div>
                        <div class="boxchart" id="chartdiv"></div>
                    </div>
                    <div style="clear:both"></div>
                    <span id="divAnalitico">
                        <div class="boxhead">
                            <div class="boxtext" style="position:relative">
                                <div id="lbltitulo" class="boxtext">Inadimplentes (Retenção)</div>
                                <div style="top:4px; right:2px; line-height:1; position:absolute">
                                    <button type="button" title="Imprimir" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printProdutividade(1)">
                                        <span class="ico-print icon-white"></span>
                                    </button>
                                    <button type="button" title="Exportar Excel" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printProdutividade(2);">
                                        <span class="ico-download-3 icon-white"></span>
                                    </button>
                                    <button id="btnEmail" title="Enviar E-mail" class="button button-turquoise btn-primary" type="button" onclick="AbrirBox(5, 0)">
                                        <span class="ico-envelope-3 icon-white"></span>
                                    </button>
                                    <button type="button" title="Enviar SMS" class="button button-blue btn-primary buttonX" onclick="AbrirBox(6, 0);">
                                        <span class="ico-comment icon-white"></span>
                                    </button>
                                    <?php if ($mdl_wha_ > 0) { ?>
                                    <button type="button" title="Enviar Whatsapp" class="button button-blue btn-primary buttonX" onclick="AbrirBox(7, 0);">
                                        <span class="ico-phone-4 icon-white"></span>
                                    </button>
                                    <?php } ?>
                                </div>
                            </div>                            
                        </div>
                        <div class="boxtable">
                            <input id="txtAnalitico" type="hidden" value="nrenv"/>
                            <table class="table" style="float:none;" cellpadding="0" cellspacing="0" width="100%" id="tbReceitaAnalitico">
                                <thead>
                                    <tr>                           
                                        <th width="5%"><center>Mat.</center></th>
                                        <th width="20%"><center>Nome</center></th>
                                        <th width="13%">Período</th>
                                        <th width="6%">Val.Mens.</th>
                                        <th width="6%">Status</th>
                                        <th width="15%"><center>Plano</center></th>
                                        <th width="9%">Consultor</th>
                                        <th width="8%"><center>Telefone</center></th>
                                        <th width="9%"><center>Celular</center></th>
                                        <th width="9%"><center>Email</center></th>                            
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="9" style="text-align:right">Total:</th>
                                        <th><div id="txtTotalAnalitico"></div></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </span>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/charts.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="amcharts/amcharts.js"></script>
        <script type="text/javascript" src="amcharts/pie.js"></script>
        <script type="text/javascript" src="amcharts/themes/light.js"></script>
        <script type='text/javascript' src="../../js/numeral.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/jBox/jBox.min.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">            

            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);               
            $("#lbltitulo").text(titleAnalitico() + " entre " + $("#txt_dt_begin").val() + " até " + $("#txt_dt_end").val());
            
            function finalFind(ts) {
                var retPrint = "?ts=" + ts;                
                if ($('#txt_dt_begin').val() !== "") {
                    retPrint = retPrint + "&dti=" + $('#txt_dt_begin').val().replace(/\//g, "_");
                }
                if ($('#txt_dt_end').val() !== "") {
                    retPrint = retPrint + "&dtf=" + $('#txt_dt_end').val().replace(/\//g, "_");
                }    
                if ($('#txtConsulResp').val() !== "") {
                    retPrint = retPrint + "&rep=" + $('#txtConsulResp').val().replace(/\//g, "_");
                }                
                if ($('#txtAnalitico').val() !== "") {
                    retPrint = retPrint + "&tipo=" + $('#txtAnalitico').val().replace(/\//g, "_");
                }        
                if ($("#mscGrupo").val() !== null) {
                    var grupos = JSON.stringify($("#mscGrupo").val());
                    retPrint = retPrint + "&grp=" + grupos;
                }                
                if ($("#txtFilial").val() !== null) {
                    retPrint += "&fil=" + $("#txtFilial").val();
                }
                return "BI-Inadimplencia_server_processing.php" + retPrint;
            }   
            
            function printProdutividade(imp) {
                var pRel = "&NomeArq=" + "Inadimplentes" + "&pOri=L" +
                        "&lbl=" + "Mat.|Nome|Período|Val.Mens.|Status|Plano|Consultor|Telefone|Celular|Email" +                        
                        "&siz=" + "40|200|130|70|70|100|100|80|80|140" +
                        "&pdf=" + "11" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + $("#lbltitulo").text() + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/BI/" + finalFind(1).replace("?", "&");
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&imp=" + imp + "&PathArq=GenericModelPDF.php", '_blank');
            }           
            
            var tbReceita = $('#tbReceita').dataTable({
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                bSort: false,
                sAjaxSource: finalFind(0),
                "fnDrawCallback": function (data) {
                    $("#loader").hide();
                    if (data["aoData"].length > 0) {
                        criaGrafico(data["aoData"]);
                    }
                },
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"
                }
            });
            
            var tbReceitaAnalitico = $('#tbReceitaAnalitico').dataTable({
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                bSort: false,
                sAjaxSource: finalFind(1), 
                "fnDrawCallback": function (data) {
                    $("#loader").hide();
                    $("#txtTotalAnalitico").html(data["aoData"].length);
                },                
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"
                }           
            });              
            
            $("#btnfind").click(function () {                
                $("#loader").show();
                $("#lbltitulo").text(titleAnalitico() + " entre " + $("#txt_dt_begin").val() + " até " + $("#txt_dt_end").val());
                $("#tbReceitaAnalitico tbody").html(`<td valign="top" colspan="9" class="dataTables_empty">Carregando...</td>`);
                $("#txtTotalAnalitico").html("0");
                tbReceita.fnReloadAjax(finalFind(0));
                tbReceitaAnalitico.fnReloadAjax(finalFind(1));
            });
            
            function filterStatusChurn(estado) {
                $("#loader").show();
                $("#txtAnalitico").val(estado);
                $("#lbltitulo").text(titleAnalitico() + " entre " + $("#txt_dt_begin").val() + " até " + $("#txt_dt_end").val());
                tbReceitaAnalitico.fnReloadAjax(finalFind(1));                
            }
            
            function titleAnalitico(){
                if($("#txtAnalitico").val() === "todas") {
                    return "Planos";
                } else if($("#txtAnalitico").val() === "renov") {    
                    return "Adimplentes";                    
                } else if($("#txtAnalitico").val() === "nrenv") {    
                    return "Inadimplentes";
                }
            }
            
            function criaGrafico(data) {
                var chartData = [];                
                chartData[0] = {desc: "Planos Renovados", valor: textToNumber(data[1]["_aData"][1])};
                chartData[1] = {desc: "Planos não renovados", valor: textToNumber(data[2]["_aData"][1])};                 
                $("#lblChart").hide();
                if (chartData.length === 0) {
                    $("#lblChart").show();
                }                                              
                var chart = AmCharts.makeChart("chartdiv", {
                    "type": "pie",
                    "theme": "light",
                    "dataProvider": chartData,
                    "valueField": "valor",
                    "titleField": "desc",
                    "legend": {
                        "align": "center",
                        "position": "bottom",
                        "markerType": "circle",
                        "labelText": "[[title]]",
                        "valueText": "",
                        "fontSize": 12
                    },
                    "marginTop": 0,
                    "marginBottom": 0,
                    "pullOutRadius": 10,
                    "autoMargins": false,
                    "innerRadius": "50%",
                    "numberFormatter": {"precision": 2, "decimalSeparator": lang["centsSeparator"], "thousandsSeparator": lang["thousandsSeparator"]},
                    "labelText": lang["prefix"] + " [[value]] ([[percents]]%)",
                    "balloonText": "[[title]]: " + lang["prefix"] + " [[value]] ([[percents]]%)",
                    "colors": ["#5b9bd5", "#ed7d31"]
                });
                var totR = 0;
                var totNr = 0;
                if (textToNumber(data[0]["_aData"][1]) > 0) {
                    var totNr = ((textToNumber(data[2]["_aData"][1]) * 100) / textToNumber(data[0]["_aData"][1]));
                    var totR = ((textToNumber(data[1]["_aData"][1]) * 100) / textToNumber(data[0]["_aData"][1]));
                }
                $("#txtTotal").html(totNr.toFixed(2) + " %");    
                $("#lblNaoRenovados").html(totNr.toFixed(2) + "%");
                $("#lblRenovados").html(totR.toFixed(2) + "%");
            }                                                          
            
            function AbrirBox(opc, id) {
                $("#txtId").val("");
                if (opc === 1) {
                    let url = "./../CRM/CRMForm.php?id=" + id;
                    window.open(url,'_blank');
                }
                if (opc === 2) {
                    let url = "./../Academia/ClientesForm.php?id=" + id;
                    window.open(url,'_blank');
                }
                if (opc === 5) {                    
                    abrirTelaBox("../CRM/EmailsEnvioForm.php", 650, 720);
                }
                if (opc === 6) {
                    abrirTelaBox("../CRM/SMSEnvioForm.php", 400, 400);
                }
                if (opc === 7) {
                    abrirTelaBox("../CRM/SMSEnvioForm.php?zap=s", 400, 400);
                }                
            }
            
            function abrirTelaSMS(sms, zap, id) {                               
                $("#txtId").val(id);                
                var link = "../CRM/SMSEnvioForm.php" + (zap === true ? "?zap=s" : "");
                abrirTelaBox(link, 265, 400);
                $("#txtSendSms").val(sms);
            }
            
            function abrirTelaEmail(email, id) {  
                $("#txtId").val(id);                
                abrirTelaBox("../CRM/EmailsEnvioForm.php", 650, 720);
                $("#txtSendEmail").val(email);
            }
            
            function FecharBox(opc) {
                $("#newbox").remove();
            }
            
            function listaSMS() {
                var sms = [];
                if ($("#txtId").val() === "") {
                    $.ajax({                    
                        url: finalFind(1),
                        async: false,
                        dataType: "json",
                        success: function (j) {
                            for (var x in j.aaData) {
                                if (j.aaData[x][8].trim() !== ""){               
                                    sms.push($(j.aaData[x][8].replace(/ /g, "")).text() + "|" + j.aaData[x][0]);
                                }
                            }
                        }
                    });                    
                } else {
                    sms.push($("#txtSendSms").val() + "|" + $("#txtId").val());    
                }
                return sms;
            }
            
            function listaEmails() {                               
                var emails = [];
                if ($("#txtId").val() === "") {
                    $.ajax({
                        url: finalFind(1),
                        async: false,
                        dataType: "json",
                        success: function (j) {
                            for (var x in j.aaData) {
                                if (j.aaData[x][9].trim() !== "") {
                                    emails.push($(j.aaData[x][9].replace(/ /g, "")).text() + "|" + j.aaData[x][0]);
                                }
                            }
                        }
                    });
                } else {
                    emails.push($("#txtSendEmail").val() + "|" + $("#txtId").val());
                }
                return emails;
            }
                    
            new jBox('Tooltip', {
                attach: '.informacao',
                animation: 'pulse',
                theme: 'TooltipBorder',
                addClass: 'tooltipMU',
                content: ''
            });            
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>