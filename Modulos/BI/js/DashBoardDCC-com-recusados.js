$("#dccAno").change(function () {
    var ano = $("#dccAno").val();
    carregaVolTransiction(ano);
});

$(document).ready(function () {
    var ano = new Date().getFullYear();
    carregaVolTransiction(ano);
});

function carregaVolTransiction(ano) {
    $.get("/Modulos/BI/ajax/BI-DashBoard-DCC_server_processing.php?year=" + ano,
            function (data, status) {
                var VolTransaction = JSON.parse(data);
                DashbordNegadasMes(VolTransaction.TransacaoNegMes, ano);
                DashBordVolTransacao(VolTransaction.VolumeTransacao, ano);
            });
}

function VolTransaction(data) {
    AmCharts.makeChart("chartdiv2", {
        "type": "serial",
        "theme": "light",
        "legend": {
            "maxColumns": 3,
            "position": "bottom",
            "useGraphSettings": true,
            "markerSize": 20
        },
        "numberFormatter": {"precision": -1, "decimalSeparator": lang["centsSeparator"], "thousandsSeparator": lang["thousandsSeparator"]},
        "pathToImages": "http://www.amcharts.com/lib/3/images/",
        "categoryField": "mes",
        "startDuration": 1,
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "position": "left"
        },
        "graphs": [{
                "balloonText": "[[title]]: [[value]]",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Aprovados",
                "type": "column",
                "fillColors": "#538234",
                "valueField": "aprovados"
            }, {
                "balloonText": "[[title]]: [[value]]",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Negados",
                "type": "column",
                "fillColors": "#c55b11",
                "valueField": "negados"
            }, {
                "balloonText": "[[title]]: [[value]]",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Estornados",
                "type": "column",
                "fillColors": "#c00000",
                "valueField": "estornados"
            }],
        "dataProvider": data,
        "export": {
            "enabled": true,
            "libs": {
                "path": "http://www.amcharts.com/lib/3/plugins/export/libs/"
            }
        }
    });

}

function DashBordVolTransacao(data, ano) {
    var DataComp = [{"negados": 0, "mes": "Jan/" + ano, "aprovados": 0, "estornados": 0}, {"negados": 0, "mes": "Fev/" + ano, "aprovados": 0, "estornados": 0}, {"negados": 0, "mes": "Mar/" + ano, "aprovados": 0, "estornados": 0},
        {"negados": 0, "mes": "Abri/" + ano, "aprovados": 0, "estornados": 0}, {"negados": 0, "mes": "Mai/" + ano, "aprovados": 0, "estornados": 0}, {"negados": 0, "mes": "Jun/" + ano, "aprovados": 0, "estornados": 0},
        {"negados": 0, "mes": "Jul/" + ano, "aprovados": 0, "estornados": 0}, {"negados": 0, "mes": "Ago/" + ano, "aprovados": 0, "estornados": 0}, {"negados": 0, "mes": "Set/" + ano, "aprovados": 0, "estornados": 0},
        {"negados": 0, "mes": "Out/" + ano, "aprovados": 0, "estornados": 0}, {"negados": 0, "mes": "Nov/" + ano, "aprovados": 0, "estornados": 0}, {"negados": 0, "mes": "Dez/" + ano, "aprovados": 0, "estornados": 0}
    ];

    for (var x = 0; x < data.length; x++) {
        for (var i = 0; i < data[x].length; i++) {
            if (data[x][i].Status === 'E') {
                DataComp[x].estornados++;
            } else if (data[x][i].Status === 'D') {
                DataComp[x].negados++;
            } else if (data[x][i].Status === 'A' && data[x][i].Mensagem === 'APPROVED') {
                DataComp[x].aprovados++;
            }
        }
    }

    VolTransaction(DataComp);
}

/*---------------------------------------fim de dashboard Volume de transação-----------------------------*/
/*---------------------------------------inicio dashboard dcc negados Mesa-----------------------------*/
var dataGraficConfig = [{
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Cartão Vencido",
        "type": "column",
        "color": "#000000",
        "valueField": "CCVencido",
        "fillColors": "#f80001"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Saldo Insuficiente",
        "type": "column",
        "color": "#000000",
        "valueField": "SaldoInsufic",
        "fillColors": "#fec401"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Não Autorizada",
        "type": "column",
        "color": "#000000",
        "valueField": "naoAutorizado",
        "fillColors": "#f37a1d"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Cartão Restrição",
        "type": "column",
        "color": "#000000",
        "valueField": "CCRestricao",
        "fillColors": "#64318d"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Cod. Seg. Ausente",
        "type": "column",
        "color": "#000000",
        "valueField": "codSegAusent",
        "fillColors": "#f7802e"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Cod. Seg. Invalido",
        "type": "column",
        "color": "#000000",
        "valueField": "codSegInvalid",
        "fillColors": "#8ccc5a"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Cartão Bloqueado",
        "type": "column",
        "color": "#000000",
        "valueField": "CCbloqueado",
        "fillColors": "#00abf8"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Outros",
        "type": "column",
        "color": "#000000",
        "valueField": "outros",
        "fillColors": "#34571d"
    }];

function dccReprovadosPilha() {
    AmCharts.makeChart("GraficoReturnDcc", {
        "type": "serial",
        "theme": "light",
        "legend": {
            "horizontalGap": 10,
            "maxColumns": 1,
            "position": "right",
            "useGraphSettings": true,
            "markerSize": 10
        },
        "dataProvider": dadosDccNegMes,
        "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.3,
                "gridAlpha": 0
            }],
        "graphs": dataGraficConfig,
        "categoryField": "month",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "position": "left"
        },
        "export": {
            "enabled": true
        }

    });

}
//dccReprovadosPilha();
//1 - sem saldo / inadimplente
//2 - cartao invalido
//3 - Restrito uso domestico / restrito



function DashbordNegadasMes(data, ano) {
    var DataComp = [
        {"month": "Jan/" + ano, "semsaldo": 0, "ccInvalido": 0, "restrito": 0, "outros": 0}
    ];
}


var dadosDccNegMes = [{
        "year": "Jan/2017",
        "CCVencido": 2.3,
        "SaldoInsufic": 2.5,
        "naoAutorizado": 2.1,
        "CCRestricao": 0.3,
        "codSegAusent": 0.2,
        "codSegInvalid": 0.1,
        "CCbloqueado": 0.2,
        "outros": 1
    }, {
        "year": "Fev/2017",
        "CCVencido": 2.4,
        "SaldoInsufic": 2.7,
        "naoAutorizado": 2.2,
        "CCRestricao": 0.3,
        "codSegAusent": 0.3,
        "codSegInvalid": 0.1,
        "CCbloqueado": 0.2,
        "outros": 1
    }];


var status = [

];























// generate data
// Transações Negadas - Respostas
//Llink para outro grafico ser feito
//http://jsfiddle.net/api/post/library/pure/
var dataDCCNegados = [
    {
        color: "#64318d",
        desc: "<div class='dccRestricao'></div><span style='cursor:pointer;'> &nbsp; Cartão Restrição</span>",
        valor: "20"
    },
    {
        color: "#f7802e",
        desc: "<div class='dccSegAusente'></div><span style='cursor:pointer;'> &nbsp; Cod. Seg. Ausente</span>",
        valor: "20"
    },
    {
        color: "#fec401",
        desc: "<div class='dccSaldoInsufi'></div><span style='cursor:pointer;'> &nbsp; Saldo Insuficiente</span>",
        valor: "60"
    }
];


function DccNegados() {
    var i = 0;
    var chartData = [];
    $.getJSON(final_url_Status(0, ''), function (data) {
        $.each(data["aaData"], function (key, val) {
            chartData[i] = {desc: val[0], valor: val[1], color: val[3]};
            i++;
        });
    }).done(function () {

        AmCharts.makeChart("pizzaReturnDcc", {
            "type": "pie",
            "theme": "light",
            "dataProvider": dataDCCNegados,
            "valueField": "valor",
            "titleField": "desc",
            "colorField": "color",
            "marginTop": 0,
            "marginBottom": 0,
            "pullOutRadius": 10,
            "autoMargins": false,
            "labelText": "",
            "balloonText": "[[title]]: [[percents]]%", "export": {
                "enabled": true,
                "libs": {"path": "http://www.amcharts.com/lib/3/plugins/export/libs/"}
            }
        });

    });
}


function final_url_Status(forma, filtro) {
    return "Modulos/Academia/ajax/InformacoesGerenciaisStatus_server_processing.php?fr=" + forma + "&fil=" + filtro;
}


//DccNegados();


// this initializes the dialog (and uses some common options that I do)
$("#dialog").dialog({
    autoOpen: false,
    modal: true,
    show: "blind",
    hide: "blind",
    width: 600
});

// next add the onclick handler
function openData() {
    $("#dialog").dialog("open");
    return false;
}
;
