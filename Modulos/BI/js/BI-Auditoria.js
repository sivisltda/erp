var chartData;
var retPrint = "";
var i = 0;

$("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);

$(document).ready(function () {
    $("#btnfind").click(function () {
        tbLista.fnReloadAjax(finalFind(0));
    });
});

var tbLista = $("#example").dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bSort": false,
    "sAjaxSource": finalFind(0),
    "bFilter": false,
    "oLanguage": {
        "oPaginate": {
            "sFirst": "Primeiro",
            "sLast": "Último",
            "sNext": "Próximo",
            "sPrevious": "Anterior"
        }, "sEmptyTable": "Não foi encontrado nenhum resultado",
        "sInfo": "Visualização do registro _START_ ao _END_ de _TOTAL_",
        "sInfoEmpty": "Visualização do registro 0 ao 0 de 0",
        "sInfoFiltered": "(filtrado de _MAX_ registros totais)",
        "sLengthMenu": "Visualização de _MENU_ registros",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sSearch": "Pesquisar:",
        "sZeroRecords": "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers",
    "fnInitComplete": function (oSettings, json) {
    }
});

function trocaTipo() {
    tbLista.fnReloadAjax(finalFind(0));
}

$(function () {
    $("#txtDesc").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "./ajax/BI-Auditoria_server_processing.php",
                dataType: "json",
                data: {
                    q: request.term,
                    tp: 'F'
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            $("#txtValue").val(ui.item.id);
            trocaTipo();
        }
    });
});

function finalFind(op) {
    var retPrint = '';
    if ($("#txt_dt_begin").val() !== "") {
        retPrint = retPrint + "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "/");
    }
    if ($("#txt_dt_end").val() !== "") {
        retPrint = retPrint + "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "/");
    }
    if ($("#txtValue").val() !== "" && $("#txtDesc").val().length > 0) {
        retPrint = retPrint + "&idF=" + $("#txtValue").val();
    }
    if ($("#mscPlano").val() !== null) {
        retPrint = retPrint + "&idUsers=" + $("#mscPlano").val();
    }
    return "ajax/BI-Auditoria_server_processing.php?imp=" + op + "&tp=" + $('#txt_tipo').val() + retPrint;
}

function printPDF(tipo) {
    var retPrint = '';
    if ($("#txt_dt_begin").val() !== "") {
        retPrint = retPrint + "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "/");
    }
    if ($("#txt_dt_end").val() !== "") {
        retPrint = retPrint + "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "/");
    }
    var pRel = "&NomeArq=" + "BI / Auditoria" +
            "&lbl=" + "Cod.|Cliente|Data|Descrição|Usuario" +
            "&siz=" + "30|175|85|330|80" +
            "&pdf=" + "5" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + "Auditoria " + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + "../Modulos/BI/" + finalFind(1).replace("?", "&"); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tipo + "&PathArq=GenericModelPDF.php", '_blank');
}