$(document).ready(function () {    
    tbturmaSintetico.fnReloadAjax(finalFind(0));
    montaGrafico();
    montaTurmas();
    montaProf();
});

var inicial = 0;
var tbturmasAnalitico;

$("#txtcomissao").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
$("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);

function finalFind() {
    var retPrint = "&tp=" + $("#txtTpRela").val();
    if ($('#txt_dt_begin').val() !== "") {
        retPrint = retPrint + "&dti=" + $('#txt_dt_begin').val();
    }
    if ($('#txt_dt_end').val() !== "") {
        retPrint = retPrint + "&dtf=" + $('#txt_dt_end').val();
    }
    if ($('#mscProf').val() !== null) {
        retPrint = retPrint + "&prof=" + $('#mscProf').val();
    }
    if ($('#mscStat').val() !== null) {
        retPrint = retPrint + "&turms=" + $('#mscStat').val();
    }
    if ($('#txtcomissao').val() !== "") {
        var tipo = ($("#tp_sinal_1").html() === "%" ? true : false);
        retPrint = retPrint + "&com=" + $('#txtcomissao').val() + "&tpcom=" + tipo;
    }
    return "ajax/BI-Turmas.php?filial=" + $("#txtMnFilial").val() + retPrint;
}

function montaGrafico() {
    var chartData = [];
    var i = 0;
    var total = parseFloat(0);
    var totalCom = parseFloat(0);
    $.getJSON(finalFind(0), function (data) {
        $.each(data["aaData"], function (key, val) {
            chartData[i] = {desc: val[0], valor: textToNumber(val[1])};
            total = total + textToNumber(val[1]);
            totalCom = totalCom + textToNumber(val[2]);
            i = i + 1;            
        });
    }).done(function () {
        $("#txtTotal").html(numberFormat(total));
        $("#txtTotCom").html(numberFormat(totalCom));
        var chart = AmCharts.makeChart("chartdiv", {
            "type": "pie",
            "theme": "light",
            "dataProvider": chartData,
            "valueField": "valor",
            "titleField": "desc",
            "numberFormatter": {
                "precision": -1,
                "decimalSeparator": lang["centsSeparator"],
                "thousandsSeparator": lang["thousandsSeparator"]
            },
            pullOutRadius: 30,
            autoMargins: false,
            "labelText": "[[percents]]%",
            "balloonText": "[[title]]<br/>" + lang["prefix"] + " [[value]]"
        });
    });
}

var tbturmaSintetico = $('#tbReceita').dataTable({
    bPaginate: false, bFilter: false, bInfo: false, bSort: false,
    "bProcessing": false,
    "bServerSide": true,
    "sAjaxSource": finalFind(1)
});

function montaAnalitico() {
    tbturmasAnalitico = $('#example').dataTable({
        bPaginate: false, bFilter: false, bInfo: false, bSort: false,
        "bProcessing": false,
        "bServerSide": true,
        "dataSrc": "data",
        "sAjaxSource": finalFind(1),
        "bDestroy": true,
        "fnInitComplete": function (oSettings, json) {
            var total = parseFloat(0);
            var totalPorc = parseFloat(0);
            for (var x = 0; x < oSettings.aoData.length; x++) {
                var quebra = oSettings.aoData[x]._aData[5].split("$")[1];
                var quebraagain = quebra.split("</center>")[0];
                total = total + textToNumber(quebraagain);                
                var quebra2 = oSettings.aoData[x]._aData[6].split("$")[1];
                var quebraagain2 = quebra2.split("</center>")[0];
                totalPorc = totalPorc + textToNumber(quebraagain2);
            }
            $("#txtTotalNumeric").html(oSettings.aoData.length);
            $("#txtTotalAnalitic").html("<center> " + lang["prefix"] + ": " + numberFormat(total) + "</center>");
            $("#txtTotComAnalitic").html("<center> " + lang["prefix"] + ": " + numberFormat(totalPorc) + "</center>");
        }
    });
}

$('#mscProf').change(function () {
    if ($(this).val() !== null) {
        montaTurmas($(this).val());
    } else {
        $('#mscStat').empty();
        montaTurmas();
    }
});

function montaTurmas(e) {
    var data;
    if (e !== undefined) {
        $('#mscStat').empty();
        data = {listTurm: "true", profs: JSON.stringify(e)};
    } else {
        data = {listTurm: "true"};
    }
    $.getJSON("ajax/BI-Turmas.php", data, function (j) {
        j = j.aaData;
        if (j !== null) {
            var options = "";
            for (var i = 0; i < j.length; i++) {
                options += "<option value='" + j[i].id + "'>" + j[i].desc + "</option>";
            }
            $("#mscStat").append(options);
            $('#mscStat').trigger('change');
        }
    });
}

function montaProf() {
    $.getJSON("ajax/BI-Turmas.php", {listProf: "true"}, function (j) {
        j = j.aaData;
        var options = "";
        for (var i = 0; i < j.length; i++) {
            options += "<option value='" + j[i].id + "'>" + j[i].desc + "</option>";
        }
        $("#mscProf").append(options);
        $('#mscProf').trigger('change');
    });
}

function AbrirBox() {
    if ($("#txtTpRela").val() === "0") {
        $("#example").hide();
        $(".box50").show();
        $(".topExemple").hide();
        tbturmaSintetico.fnReloadAjax(finalFind(0));
    }else if ($("#txtTpRela").val() === "1") {
        inicial++;
        $(".box50").hide();
        $("#example").show();
        $(".topExemple").show();
        if (inicial > 1) {
            $('#example').dataTable().fnDestroy();
            montaAnalitico();
        } else {
            montaAnalitico();
        }
    }
    montaGrafico();
}

function trocaSinal(id) {
    if ($("#tp_sinal_" + id).html() === "%") {
        $("#tp_sinal_" + id).html("$");
        $("#txtSinal_" + id).val("D");
    } else {
        $("#tp_sinal_" + id).html("%");
        $("#txtSinal_" + id).val("P");
    }
}

function pdfSintetic(tp) {
    var pRel = "&NomeArq=" + "Relatório Financeiro de Turmas" +
            "&lbl=" + "Turma|Valor|Comissão" +
            "&siz=" + "300|200|200" +
            "&pdf=" + "3" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + getLabel() + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + "../Modulos/BI/" + finalFind().replace('?', '&'); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tp + "&PathArq=GenericModelPDF.php", '_blank');
}

function pdfAnalitic(tp) {
    var pRel = "&NomeArq=" + "Relatório Financeiro de Turmas" +
            "&lbl=" + "Código|Nome|Turma|Período|Data PGTO|Valor|Comissão" +
            "&siz=" + "50|180|130|110|70|80|80" +
            "&pdf=" + "8" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + getLabel() + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + "../Modulos/BI/" + finalFind().replace('?', '&') + "&pdf=true"; // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tp + "&PathArq=GenericModelPDF.php", '_blank');
}

function getLabel() {
    let toReturn = "De " + $("#txt_dt_begin").val() + " até " + $("#txt_dt_end").val() + " ";
    toReturn += "Prof.: " + $("#mscProf option:selected").map(function() { return $(this).text(); }).get().join(', ') + " ";
    toReturn += "Turmas: " + $("#mscStat option:selected").map(function() { return $(this).text(); }).get().join(', ') + " ";
    toReturn += "Comissão: " + $("#txtcomissao").val();
    return toReturn;
}