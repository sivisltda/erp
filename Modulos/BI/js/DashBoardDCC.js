var ano = new Date().getFullYear();
$(document).ready(function () {
    carregaPainel();
    carregaVolTransiction(ano);
    generateChartData();
});

/*---------------------------------------Carrega painel programado-----------------------------------------*/
function carregaPainel() {
    $.get("/Modulos/BI/ajax/BI-DashBoard-DCC_server_processing.php?cobrancas=S",
            function (data, status) {
                var dados = JSON.parse(data);
                $(".dentroPrazo")[0].innerHTML = dados.noPrazo;
                $(".foraPrazo")[0].innerHTML = dados.foraPrazo;
                $(".dccnegados")[0].innerHTML = dados.totNeg;
            });

    $.get("/Modulos/BI/ajax/BI-DashBoard-DCC_server_processing.php?recebido=S",
            function (data, status) {
                var dados = JSON.parse(data);
                if (parseFloat(dados.diferenca) > 0) {
                    $('.recebidoPassadoColor').css("color", "#44aa55");
                    $('.pagtrecb .ic_sinal').addClass("ico-upload-3");
                    $('.pagtrecb .ico-upload-3').css("color", "#44aa55");
                } else {
                    $('.pagtrecb .ic_sinal').addClass("ico-download-4");
                    $('.ic_sinal').css("color", "#ee4433");
                    $('.recebidoPassadoColor').css("color", "#ee4433");
                }

                $(".recebidoAtual")[0].innerHTML = dados.mesAtual;
                $(".recebidoPassado")[0].innerHTML = dados.diferenca;
                $(".diferencPercnt")[0].innerHTML = '(' + dados.porcent + '%)';

            });

    $.get("/Modulos/BI/ajax/BI-DashBoard-DCC_server_processing.php?countAtivos=S",
            function (data, status) {
                var dados = JSON.parse(data);
                var sinal = '';
                var diferenca = 0;

                if ((dados.mesAtual - dados.mesPassado) > 0) {
                    $('.ic_sinalAsss').addClass("ico-upload-3");
                    $('.cnttum p').css("color", "#44aa55");
                } else {
                    $('.ic_sinalAsss').addClass("ico-download-4");
                    $('.cnttum p').css("color", "#ee4433");
                    $('.ic_sinalAsss').css("color", "#ee4433");
                }

                $('.cnttmidle h3')[0].innerHTML = dados.mesAtual;
                $(".ativosAtual")[0].innerHTML = (dados.mesAtual - dados.mesPassado);
                $(".ativosEvolution")[0].innerHTML = '(' + dados.porcent + '%)';
            });

    $.get("/Modulos/BI/ajax/BI-DashBoard-DCC_server_processing.php?ultimosPagmts=S",
            function (data, status) {
                var VolTransaction = JSON.parse(data);
                DashBordUltimosPgmts(VolTransaction, ano);
            });
}


/*------------------------------------------Inicio painel de dcc resumo-------------------------------------*/
var chartData = [];

function generateChartData() {
    var firstDate = new Date();
    firstDate.setTime(firstDate.getTime() - 10 * 24 * 60 * 60 * 1000);

    for (var i = firstDate.getTime(); i < (firstDate.getTime() + 10 * 24 * 60 * 60 * 1000); i += 60 * 60 * 1000) {
        var newDate = new Date(i);
        if (i === firstDate.getTime()) {
            var value1 = Math.round(Math.random() * 10) + 1;
        } else {
            var value1 = Math.round(chartData[chartData.length - 1].value1 / 100 * (90 + Math.round(Math.random() * 20)) * 100) / 100;
        }
        if (newDate.getHours() === 12) {
            // we set daily data on 12th hour only
            var value2 = Math.round(Math.random() * 12) + 1;
            chartData.push({
                date: newDate,
                value1: value1,
                value2: value2
            });
        } else {
            chartData.push({
                date: newDate,
                value1: value1
            });
        }
    }
}

/*---------------------------------------Inicio de dashboard Volume de transação-----------------------------*/

$("#dccAno").change(function () {
    var ano = $("#dccAno").val();
    carregaVolTransiction(ano);
});

function carregaVolTransiction(ano) {
    $.get("/Modulos/BI/ajax/BI-DashBoard-DCC_server_processing.php?year=" + ano,
            function (data, status) {
                var VolTransaction = JSON.parse(data);
                DashBordVolTransacao(VolTransaction.VolumeTransacao, ano);
            });
}

function VolTransaction(data) {

    AmCharts.makeChart("chartdiv2", {
        "type": "serial",
        "theme": "light",
        "legend": {
            "useGraphSettings": true,
            "enabled": false
        },
        "numberFormatter": {
            "precision": -1,
            "decimalSeparator": ",",
            "thousandsSeparator": "."
        },
        "pathToImages": "http://www.amcharts.com/lib/3/images/",
        "categoryField": "mes",
        "startDuration": 1,
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "position": "left"
        },
        "graphs": [{
                "balloonText": "[[title]]: [[value]]",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Aprovados",
                "type": "column",
                "fillColors": "#538234",
                "valueField": "aprovados"
            }, {
                "balloonText": "[[title]]: [[value]]",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Negados",
                "type": "column",
                "fillColors": "#c55b11",
                "valueField": "negados"
            }, {
                "balloonText": "[[title]]: [[value]]",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Estornados",
                "type": "column",
                "fillColors": "#c00000",
                "valueField": "estornados"
            }],
        "dataProvider": data,
        "export": {
            "enabled": true,
            "libs": {
                "path": "http://www.amcharts.com/lib/3/plugins/export/libs/"
            }
        }
    });

}

function DashBordVolTransacao(data, ano) {

    var DataComp = [{"negados": 0, "mes": "Jan/" + ano, "aprovados": 0, "estornados": 0}, {"negados": 0, "mes": "Fev/" + ano, "aprovados": 0, "estornados": 0}, {"negados": 0, "mes": "Mar/" + ano, "aprovados": 0, "estornados": 0},
        {"negados": 0, "mes": "Abri/" + ano, "aprovados": 0, "estornados": 0}, {"negados": 0, "mes": "Mai/" + ano, "aprovados": 0, "estornados": 0}, {"negados": 0, "mes": "Jun/" + ano, "aprovados": 0, "estornados": 0},
        {"negados": 0, "mes": "Jul/" + ano, "aprovados": 0, "estornados": 0}, {"negados": 0, "mes": "Ago/" + ano, "aprovados": 0, "estornados": 0}, {"negados": 0, "mes": "Set/" + ano, "aprovados": 0, "estornados": 0},
        {"negados": 0, "mes": "Out/" + ano, "aprovados": 0, "estornados": 0}, {"negados": 0, "mes": "Nov/" + ano, "aprovados": 0, "estornados": 0}, {"negados": 0, "mes": "Dez/" + ano, "aprovados": 0, "estornados": 0}
    ];

    for (var x = 0; x < data.length; x++) {
        for (var i = 0; i < data[x].length; i++) {
            if (data[x][i].Status === 'E') {
                DataComp[x].estornados++;
            } else if (data[x][i].Status === 'D') {
                DataComp[x].negados++;
            } else if (data[x][i].Status === 'A' && data[x][i].Mensagem === 'APPROVED') {
                DataComp[x].aprovados++;
            }
        }
    }

    VolTransaction(DataComp);
}

function DashBordUltimosPgmts(data, ano) {
    var dataInicial = moment().subtract(30, 'days');
    var DataComp = [];
    for (var x = 0; x < 31; x++) {
        var createDate = moment(dataInicial).add(x, 'days');
        DataComp.push({"mes": createDate.format('DD/MM'), "val": 0});
    }
    for (var x = 0; x < DataComp.length; x++) {
        for (var i = 0; i < data.length; i++) {
            if (data[i].data === DataComp[x].mes) {
                DataComp[x].val = data[i].valor;
            }
        }
    }
    ultimosPagmts(DataComp);
}

function ultimosPagmts(data) {

    AmCharts.makeChart("chartdiv", {
        "type": "serial",
        "theme": "light",
        "legend": {
            "useGraphSettings": true,
            "enabled": false
        },
        "pathToImages": "http://www.amcharts.com/lib/3/images/",
        "categoryField": "mes",
        "startDuration": 1,
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "position": "left"
        },
        "graphs": [{
                "balloonText": "[[title]]: [[value]]",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": lang["prefix"],
                "type": "column",
                "fillColors": "#538234",
                "valueField": lang["prefix"]
            }],
        "dataProvider": data,
        "export": {
            "enabled": true,
            "libs": {
                "path": "http://www.amcharts.com/lib/3/plugins/export/libs/"
            }
        }
    });

}

// this initializes the dialog (and uses some common options that I do)
$("#dialog").dialog({
    autoOpen: false,
    modal: true,
    show: "blind",
    hide: "blind",
    width: 600
});

// next add the onclick handler
function openData() {
    $("#dialog").dialog("open");
    return false;
}
