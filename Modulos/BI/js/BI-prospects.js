
$("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);

var chartCores = ["#7f7f7f", "#67b7dc", "#fdd400", "#84b761"];
var funilVendas = AmCharts.makeChart("funilVendas", {
    "type": "funnel",
    "theme": "light",
    "valueField": "value",
    "titleField": "title",
    "marginTop": 20,
    "marginLeft": 50,
    "marginRight": 200,
    "marginBottom": 20,
    "neckWidth": "20%",
    "labelText": "[[value]]",
    "labelPosition": "center",
    "balloonText": "[[title]]: [[value]]",
    "colors": chartCores
});
funilVendas.addListener("clickSlice", handleClick);

var procedencia = AmCharts.makeChart("procedencia", {
    "theme": "light",
    "type": "serial",
    "graphs": [{"balloonText": "[[category]]: [[share]]% ([[value]])", "fillAlphas": 1, "lineAlpha": 0.2, "title": "Porcentagem", "valueField": "value", "type": "column"}],
    "rotate": true,
    "categoryField": "title",
    "categoryAxis": {"gridPosition": "start", "fillAlpha": 0.05, "position": "left"},
    "export": {"enabled": true}
});

function finalFind(tp, imp, filter) {
    var retPrint = "&imp=" + imp;       
    if ($("#txtResponsavel").val() !== "") {
        retPrint = retPrint + "&tpr=" + $("#txtResponsavel").val();
    }    
    if ($("#mscGrupo").val() !== null) {
        retPrint = retPrint + "&tpg=" + $("#mscGrupo").val();
    } 
    if ($("#txt_dt_begin").val() !== "") {
        retPrint = retPrint + "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "_");
    }
    if ($("#txt_dt_end").val() !== "") {
        retPrint = retPrint + "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "_");
    }
    if ($("#txtFilial").val() !== null) {
        retPrint = retPrint + "&fil=" + $("#txtFilial").val();
    }     
    return "ajax/BI-Gestao-de-Vendas_server_processing.php?tp=" + tp + filter + retPrint;
}

var tbClientes = $('#tbClientes').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bFilter": false,
    "aoColumns": [
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false}
    ],
    "sAjaxSource": finalFind(1, 1, ''),
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

function refreshGraficosPeriodos() {
    $.getJSON(finalFind(1, 0, ''), function (data) {
        let dataProvider = [];
        dataProvider[0] = data["aaData"][0].lead;
        dataProvider[1] = data["aaData"][0].prospect;
        dataProvider[2] = data["aaData"][0].cliente;
        dataProvider[3] = data["aaData"][0].faturamento;
        funilVendas.dataProvider = dataProvider;
        funilVendas.validateData();
    });
    $.getJSON(finalFind(1, 2, ''), function (data) {
        var sum = 0;
        for (var x in data["aaData"]) {
            sum += parseInt(data["aaData"][x].value);
        }
        for (var x in data["aaData"]) {
            data["aaData"][x].share = Math.round(parseInt(data["aaData"][x].value) / sum * 100);
        }
        procedencia.dataProvider = data["aaData"];
        procedencia.validateData();
    });    
}

function handleClick(event) {
    $("#txtFiltro").val(event.dataItem.title);
    tbClientes.fnReloadAjax(finalFind(1, 1, "&tipo=" + event.dataItem.title));
}

function printPDF(tipo) {
    var pRel = "&NomeArq=" + $("#txtFiltro").val() +
            "&lbl=Código|Nome|Tipo|Data|Telefone|E-mail"+
            "&siz=60|220|50|100|100|170" +
            "&pdf=11" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=../Modulos/BI/" + finalFind(1, 1, '').replace("?", "&") + "&tipo=" + $("#txtFiltro").val(); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tipo + "&PathArq=GenericModelPDF.php", '_blank');    
}

refreshGraficosPeriodos();

$("#btnfind").click(function () {
    handleClick({dataItem : {title : ""}});
    refreshGraficosPeriodos();
});