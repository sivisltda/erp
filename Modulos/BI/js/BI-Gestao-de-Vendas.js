
$("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);

var chartCores = ["#7f7f7f", "#67b7dc", "#fdd400", "#84b761"];
var funilVendas = AmCharts.makeChart("funilVendas", {
    "type": "funnel",
    "theme": "light",
    "valueField": "value",
    "titleField": "title",
    "marginTop": 20,
    "marginLeft": 50,
    "marginRight": 200,
    "marginBottom": 20,
    "neckWidth": "20%",
    "labelText": "[[value]]",
    "labelPosition": "center",
    "balloonText": "[[title]]: [[value]]",
    "colors": chartCores
});
funilVendas.addListener("clickSlice", handleClick);

var procedencia = AmCharts.makeChart("procedencia", {
    "theme": "light",
    "type": "serial",
    "graphs": [{"balloonText": "[[category]]: [[share]]% ([[value]])", "fillAlphas": 1, "lineAlpha": 0.2, "title": "Porcentagem", "valueField": "value", "type": "column"}],
    "rotate": true,
    "categoryField": "title",
    "categoryAxis": {"gridPosition": "start", "fillAlpha": 0.05, "position": "left"},
    "export": {"enabled": true}
});

var metaVendas = AmCharts.makeChart("metaVendas", {
    "type": "gauge",
    "theme": "light",
    "axes": [{
            "axisColor": "#777",
            "tickColor": "#777",
            "axisThickness": 2,
            "tickThickness": 2,
            "labelOffset": 0,
            "valueInterval": 20,
            "bands": [{"color": "#CC4748", "endValue": 33, "startValue": 0, "innerRadius": "75%"
                }, {"color": "#FDD400", "endValue": 67, "startValue": 33, "innerRadius": "75%"
                }, {"color": "#84B761", "endValue": 100, "startValue": 67, "innerRadius": "75%"}],
            "bottomText": "META DE VENDAS CLIENTES",
            "bottomTextFontSize": 13,
            "bottomTextYOffset": 20,
            "endValue": 100
        }],
    "arrows": [{"innerRadius": 1, "value": 0}],
    "export": {"enabled": true}
});

var pontoEquilibrio = AmCharts.makeChart("pontoEquilibrio", {
    "type": "gauge",
    "theme": "light",
    "axes": [{
            "axisColor": "#777",
            "tickColor": "#777",
            "axisThickness": 2,
            "tickThickness": 2,
            "labelOffset": 0,
            "valueInterval": 20,
            "bands": [{"color": "#CC4748", "endValue": 33, "startValue": 0, "innerRadius": "75%"
                }, {"color": "#FDD400", "endValue": 67, "startValue": 33, "innerRadius": "75%"
                }, {"color": "#84B761", "endValue": 100, "startValue": 67, "innerRadius": "75%"}],
            "bottomText": "META DE VENDAS FATURAMENTO",
            "bottomTextFontSize": 13,
            "bottomTextYOffset": 20,
            "endValue": 100
        }],
    "arrows": [{"innerRadius": 1, "value": 0}],
    "export": {"enabled": true}
});

var tbRanking = $('#tbRanking').dataTable({
    "iDisplayLength": 10,
    "bProcessing": true,
    "bServerSide": true,
    "bFilter": false,
    "bInfo": false,
    "bLengthChange": false,    
    "aoColumns": [
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false}
    ],
    "sAjaxSource": finalFind(0),
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

//---------------------------------------------------------------------------------

var chart = AmCharts.makeChart("chartBar", {
    "theme": "light",    
    "type": "pie",
    "valueField": "valor",
    "titleField": "desc",
    "legend": {
        "align": "center",
        "position": "bottom",
        "markerType": "circle",
        "labelText": "[[title]]",
        "valueText": "",
        "fontSize": 12
    },
    "marginTop": 0,
    "marginBottom": 0,
    "pullOutRadius": 10,
    "autoMargins": false,
    "labelText": "",
    "balloonText": "[[title]]: [[value]] ([[percents]]%)"
});

var procedenciaLead = AmCharts.makeChart("procedenciaLead", {
    "theme": "light",
    "type": "serial",
    "graphs": [{"balloonText": "[[category]]: [[value]]", "fillAlphas": 1, "lineAlpha": 0.2, "title": "Porcentagem", "valueField": "value", "type": "column"}],
    "rotate": true,
    "categoryField": "title",
    "categoryAxis": {"gridPosition": "start", "fillAlpha": 0.05, "position": "left"}
}); 

//---------------------------------------------------------------------------------

function refreshGraficosPeriodos() {
    tbRanking.fnReloadAjax(finalFind(0));
    $.getJSON(finalFind(1), function (data) {
        let dataProvider = [];
        dataProvider[0] = data["aaData"][0].lead;
        dataProvider[1] = data["aaData"][0].prospect;
        dataProvider[2] = data["aaData"][0].cliente;
        dataProvider[3] = data["aaData"][0].faturamento;
        funilVendas.dataProvider = dataProvider;
        funilVendas.validateData();
    });
    filtroProcedencia();
    $.getJSON(finalFind(3), function (data) {
        $("#tbAnalitico").empty();
        for (var i = 0; i < data["aaData"].length; i++) {
            $("#tbAnalitico").append("<tr><td>" + data["aaData"][i].qtd + "</td><td>" + data["aaData"][i].desc + "</td><td>" + data["aaData"][i].valor + "</td></tr>");
        }
    });
    $.getJSON(finalFind(4), function (data) {
        setTimeout(function() {
            metaVendas.arrows[0].setValue(data["aaData"][0].venda);
            pontoEquilibrio.arrows[0].setValue(data["aaData"][0].equilibrio);
        }, 3000);
    });     
    $.getJSON(finalFindLead(0), function (data) {                                              
        var chartBar = [];
        var chartData = [];
        $.each(data["Status"], function (key, val) {
            chartBar[key] = val;
        });
        chart.dataProvider = chartBar;
        chart.validateData();
        $.each(data["Indicadores"], function (key, val) {
            chartData[key] = val;
        });
        procedenciaLead.dataProvider = chartData;
        procedenciaLead.validateData();
    });   
}

function filtroProcedencia() {
    $.getJSON(finalFind(2), function (data) {
        var sum = 0;
        for (var x in data["aaData"]) {
            sum += parseInt(data["aaData"][x].value);
        }
        for (var x in data["aaData"]) {
            data["aaData"][x].share = Math.round(parseInt(data["aaData"][x].value) / sum * 100);
        }
        procedencia.dataProvider = data["aaData"];
        procedencia.validateData();
    });    
}
    
function refresh() {
    refreshGraficosPeriodos();
}

function finalFind(tp) {
    var retPrint = "";    
    if ($("#txtVenda").val() !== "") {
        retPrint += "&tpv=" + $("#txtVenda").val();
    }
    if ($("#tpProcedencia").val() !== "") {
        retPrint += "&tpp=" + $("#tpProcedencia").val();
    }
    if ($("#txtResponsavel").val() !== "") {
        retPrint += "&tpr=" + $("#txtResponsavel").val();
    }
    if ($("#mscGrupo").val() !== null) {
        retPrint += "&tpg=" + $("#mscGrupo").val();
    }    
    if ($("#txt_dt_begin").val() !== "") {
        retPrint += "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "_");
    }
    if ($("#txt_dt_end").val() !== "") {
        retPrint += "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "_");
    }
    if ($("#txtFilial").val() !== null) {
        retPrint += "&fil=" + $("#txtFilial").val();
    }
    return "ajax/BI-Gestao-de-Vendas_server_processing.php?tp=" + tp + retPrint;
}

function finalFindLead(tp) {
    var retPrint = "../CRM/Lead_server_processing.php?tp=" + tp + "&imp=0&tpData=1";
    if ($("#txtResponsavel").val() !== "") {
        retPrint += "&tpr=" + $("#txtResponsavel").val();
    }
    if ($("#mscGrupo").val() !== null) {
        retPrint += "&tpg=" + $("#mscGrupo").val();
    }
    if ($("#txt_dt_begin").val() !== "") {
        retPrint += "&dtb=" + $("#txt_dt_begin").val().replace(/\//g, "_");
    }
    if ($("#txt_dt_end").val() !== "") {
        retPrint += "&dte=" + $("#txt_dt_end").val().replace(/\//g, "_");
    }    
    if ($("#txtFilial").val() !== null) {
        retPrint += "&fil=" + $("#txtFilial").val();
    }                
    return retPrint;
}

function AbrirBox() {
    abrirTelaBox("FormMetas.php", 425, 650);
}

function FecharBox(opc) {
    $("#newbox").remove();
    if (opc === 1) {
        refreshGraficosPeriodos();
    }
}

function filtroUsuario() {
    $("#s2id_mscGrupo").parent().show();
    $('#mscGrupo').empty();
    $("#mscGrupo").select2('data', null);                
    $.getJSON('./../Seguro/comissao.ajax.php', {txtTipo: $('#txtResponsavel').val()}, function (j) {
        var options = '';
        for (var i = 0; i < j.length; i++) {
            options += '<option value="' + j[i].login_user + '">' + j[i].nome + '</option>';
        }
        $("#mscGrupo").append(options);
        $('#mscGrupo').trigger('change');
    });             
}

function valorRecorrentes() {
    $.getJSON('./../Seguro/Comissoes_clientes_all_server_processing.php', {imp: 0, txtForma: 2, txtTipo: 5, txtUsuario: $("#txtIdUsu").val(), 
        dti: moment().startOf('month').format('DD/MM/YYYY'), dtf: moment().endOf('month').format('DD/MM/YYYY')}, function (j) {
        let total = 0;
        for (var i = 0; i < j.aaData.length; i++) {
            total += textToNumber(j.aaData[i][9]);
        }    
        $("#totRecorrentes").html(numberFormat(total));
    });        
}

$('#txtResponsavel').change(function () {
    filtroUsuario();
});

$('#tpProcedencia').change(function () {
    filtroProcedencia();
});

function handleClick(event) {
    var pRel = "&NomeArq=" + event.dataItem.title + "&imp=1" +
            "&lbl=Código|Nome|Tipo|Data|Telefone|E-mail"+
            "&siz=60|220|50|100|100|170" +
            "&pdf=7" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=../Modulos/BI/" + finalFind(1).replace("?", "&") + "&tipo=" + event.dataItem.title; // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');    
}

refreshGraficosPeriodos();
valorRecorrentes();
filtroUsuario();