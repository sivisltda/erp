
$("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);

var lista = $('#example').dataTable({
    "iDisplayLength": 100,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,        
    "sAjaxSource": finalFind(0,'',''),
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers"
});

function finalFindConta() {
    var retPrint = "";
    if ($('#txt_tipo').val() !== "") {
        retPrint = retPrint + "&tp=" + $('#txt_tipo').val();
    }
    if ($('#txtTipo').val() !== "null") {
        retPrint = retPrint + "&pd=" + $('#txtTipo').val();
    }
    if ($('#txtTipoChamado').val() !== "null") {
        retPrint = retPrint + "&frm=" + $('#txtTipoChamado').val();
    }
    if ($('#txtForma').val() !== "null") {
        retPrint = retPrint + "&for=" + $('#txtForma').val();
    }      
    if ($('#txtTipoData').val() !== "") {
        retPrint = retPrint + "&tpd=" + $('#txtTipoData').val();
    }    
    if ($('#txt_id').val() !== "") {
        retPrint = retPrint + "&id=" + $('#txt_id').val();
    }
    if ($('#txt_dt_begin').val() !== "") {
        retPrint = retPrint + "&dti=" + $('#txt_dt_begin').val().replace(/\//g, "_");
    }
    if ($('#txt_dt_end').val() !== "") {
        retPrint = retPrint + "&dtf=" + $('#txt_dt_end').val().replace(/\//g, "_");
    }
    return "./ajax/Controle-de-Atendimentos_server_processing.php?imp=0" + retPrint;
}

function refreshTable(id,dt_ini,dt_fim){  
    lista.fnReloadAjax(finalFind(id,dt_ini,dt_fim));
}

function finalFind(id,dt_ini,dt_fim){
    var retorno = "";   
    if ($('#txt_tipo').val() !== "") {
        retorno = retorno + "&tp=" + $('#txt_tipo').val();
    }    
    if ($('#txtTipo').val() !== "null") {
        retorno = retorno + "&pd=" + $('#txtTipo').val();
    }    
    if ($('#txtTipoChamado').val() !== "null") {
        retorno = retorno + "&frm=" + $('#txtTipoChamado').val();
    }    
    if ($('#txtForma').val() !== "null") {
        retorno = retorno + "&for=" + $('#txtForma').val();
    }    
    if ($('#txtTipoData').val() !== "") {
        retorno = retorno + "&tpd=" + $('#txtTipoData').val();
    }     
    retorno = retorno + "&id=" + id;
    retorno = retorno + "&dti=" + dt_ini.replace(/\//g, "_");
    retorno = retorno + "&dtf=" + dt_fim.replace(/\//g, "_");    
    return "./ajax/Controle-de-Atendimentos_server_processing.php?imp=1" + retorno;
}

function refresh() {
    $.getJSON(finalFindConta(), function (data) {
        var i = 0;
        var dia_semana = 0;
        var conteudo = "<div class=\"col-e\"><span class=\"ico-chevron-left\" style=\"cursor:pointer\" onClick=\"prev()\"></span></div>";
        var conteudo2 = "<div class=\"col-e\">COLABORADOR</div>";
        $.each(data["aCalendar"], function (key, val) {
            conteudo = conteudo + "<div class=\"col-1 " + (i + 1) + "\" ";
            conteudo2 = conteudo2 + "<div class=\"col-1 " + (i + 1) + "\" ";
            if (i === 0) {
                dia_semana = val[1];
            }
            if (i > 13) {
                conteudo = conteudo + "style=\"display:none\"";
                conteudo2 = conteudo2 + "style=\"display:none\"";
            }
            conteudo = conteudo + ">" + val[0].substring(0, 5) + "</div>";
            conteudo2 = conteudo2 + ">" + converteDia(val[1]) + "</div>";
            i = i + 1;
        });
        if (i > 13) {
            $("#receitaCol").val(i - 13);
        }
        conteudo = conteudo + "<div class=\"col-d\"><span class=\"ico-chevron-right\" style=\"cursor:pointer\" onClick=\"next()\"></span></div>";
        conteudo = conteudo + "<div style=\"clear:both\"></div>";
        conteudo2 = conteudo2 + "<div class=\"col-d\">DIAS<br>TRAB</div>";
        conteudo2 = conteudo2 + "<div class=\"col-d\">TOTAL</div>";
        conteudo2 = conteudo2 + "<div style=\"clear:both\"></div>";
        $("#datas").html(conteudo);
        $("#cabecalho").html(conteudo2);
        var conteudo3 = "";
        $.each(data["aaData"], function (key, val) {
            conteudo3 = conteudo3 + "<div class=\"cat-1\">";
            conteudo3 = conteudo3 + "<div onclick=\"refreshTable(" + val[0] + ",'"+$('#txt_dt_begin').val()+"','"+$('#txt_dt_end').val()+"');\" class=\"col-e\">" + val[1] + "</div>";
            var ds = parseInt(dia_semana);
            var dias_trab = 0;
            for (i = 3; i < val.length; i++) {
                if (ds > 7) {
                    ds = 1;
                }
                if (ds < 6 && val[i] > 0) {
                    dias_trab = dias_trab + 1;
                }
                var dataFiltro = data["aCalendar"][i-3][0];
                conteudo3 = conteudo3 + "<div onclick=\"refreshTable(" + val[0] + ",'"+dataFiltro+"','"+dataFiltro+"');\" class=\"col-1 " + (i - 2) + " " + retornaClassCor(ds, val[i]) + "\" ";
                ds = ds + 1;
                if (i > 16) {
                    conteudo3 = conteudo3 + "style=\"display:none\"";
                }
                conteudo3 = conteudo3 + ">" + val[i] + "</div>";
            }
            conteudo3 = conteudo3 + "<div class=\"col-d\">" + dias_trab + "</div>";
            conteudo3 = conteudo3 + "<div class=\"col-d\">" + val[2] + "</div>";
            conteudo3 = conteudo3 + "<div style=\"clear:both\"></div>";
            conteudo3 = conteudo3 + "</div>";
        });
        $("#scroll").html(conteudo3);
        refreshTable(0,'','');
    });
}

function retornaClassCor(dia, qtd) {
    if (dia === 6 || dia === 7) {
        return "dia_fds";
    } else if (qtd > 0) {
        return "dia_normal";
    } else {
        return "dia_zerado";
    }
}

function converteDia(dia) {
    if (dia === "1") {
        return "SEG";
    } else if (dia === "2") {
        return "TER";
    } else if (dia === "3") {
        return "QUA";
    } else if (dia === "4") {
        return "QUI";
    } else if (dia === "5") {
        return "SEX";
    } else if (dia === "6") {
        return "SAB";
    } else if (dia === "7") {
        return "DOM";
    }
    return "XXX";
}

var coluna = 1;
function next() {
    if (coluna < $("#receitaCol").val()) {
        $("." + (coluna + 0)).each(function () {
            $("." + (coluna + 0)).hide();
        });
        $("." + (coluna + 14)).each(function () {
            $("." + (coluna + 14)).show();
        });
        coluna = coluna + 1;
    }
}
function prev() {
    if (coluna > 1) {
        $("." + (coluna + 13)).each(function () {
            $("." + (coluna + 13)).hide();
        });
        $("." + (coluna - 1)).each(function () {
            $("." + (coluna - 1)).show();
        });
        coluna = coluna - 1;
    }
}

$(document).ready(function () {
    refresh();        
});