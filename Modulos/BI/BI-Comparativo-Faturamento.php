<?php
include "./../../Connections/configini.php";
$DateBegin = date("Y", strtotime("-2 year"));
$DateEnd = date("Y");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../SpryAssets/SpryValidationTextField.css"/>
        <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <style>
            #tbReceitaAnalitico th { text-align:center }
            #tbReceitaAnalitico td { padding:5px 10px; text-align:right; }
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"><span class="ico-arrow-right"></span></div>
                        <h1>SIVIS B.I.<small>Comparativo de Faturamento</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="block" style="font-size:13px">
                                <div style="float: left;margin-right: 0.5%;">
                                    <label for="txt_dt_begin">De</label>
                                    <input type="text" maxlength="4" style="width:80px; height:31px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Ano inicial"/>
                                </div>
                                <div style="float: left;margin-right: 0.5%;">
                                    <label for="txt_dt_end">até</label>
                                    <input type="text" maxlength="4" style="width:80px; height:31px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Ano Final"/>
                                </div>
                                <div style="float: left;margin-top: 15px;">
                                    <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind"><span class="ico-search icon-white"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box50">
                        <div class="boxhead">
                            <div class="boxtext">Comparativo</div>
                        </div>
                        <div class="boxchart" id="chartLine"></div>
                    </div>
                    <div class="box50" style="margin-left:2.5%">
                        <div class="boxhead">
                            <div class="boxtext">Faturamento Médio</div>
                        </div>
                        <div class="boxchart" id="chartBar"></div>
                    </div>
                    <div style="clear:both"></div>
                    <span id="divAnalitico">
                        <div class="boxhead">
                            <div class="boxtext" style="position:relative">
                                <div id="lbltitulo" class="boxtext">Comparativo de Faturamento</div>
                                <div style="top:4px; right:2px; line-height:1; position:absolute">
                                    <button type="button" title="Imprimir" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printProdutividade()">
                                        <span class="ico-print icon-white"></span>
                                    </button>
                                    <button type="button" title="Exportar Excel" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="exportarProdutividade();">
                                        <span class="ico-download-3 icon-white"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="boxtable">
                            <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbReceitaAnalitico">
                                <thead>
                                    <tr>
                                        <th width="4%">Ano</th></th>
                                        <th width="8%">Janeiro</th>
                                        <th width="8%">Fevereiro</th>
                                        <th width="8%">Março</th>
                                        <th width="8%">Abril</th>
                                        <th width="8%">Maio</th>
                                        <th width="8%">Junho</th>
                                        <th width="8%">Julho</th>
                                        <th width="8%">Agosto</th>
                                        <th width="8%">Setembro</th>
                                        <th width="8%">Outubro</th>
                                        <th width="8%">Novembro</th>
                                        <th width="8%">Dezembro</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <th>Média</th>
                                        <th style="text-align:right"><?php echo escreverNumero(0); ?></th>
                                        <th style="text-align:right"><?php echo escreverNumero(0); ?></th>
                                        <th style="text-align:right"><?php echo escreverNumero(0); ?></th>
                                        <th style="text-align:right"><?php echo escreverNumero(0); ?></th>
                                        <th style="text-align:right"><?php echo escreverNumero(0); ?></th>
                                        <th style="text-align:right"><?php echo escreverNumero(0); ?></th>
                                        <th style="text-align:right"><?php echo escreverNumero(0); ?></th>
                                        <th style="text-align:right"><?php echo escreverNumero(0); ?></th>
                                        <th style="text-align:right"><?php echo escreverNumero(0); ?></th>
                                        <th style="text-align:right"><?php echo escreverNumero(0); ?></th>
                                        <th style="text-align:right"><?php echo escreverNumero(0); ?></th>
                                        <th style="text-align:right"><?php echo escreverNumero(0); ?></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </span>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/charts.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="amcharts/amcharts.js"></script>
        <script type="text/javascript" src="amcharts/serial.js"></script>
        <script type="text/javascript" src="amcharts/themes/light.js"></script>
        <script type='text/javascript' src='../../js/util.js'></script>
        <script type="text/javascript">

            $("#txt_dt_begin, #txt_dt_end").mask("9999");

            function printProdutividade() {
                var pRel = "&NomeArq=" + "Comp. Faturamento" +
                        "&lbl=" + "Ano|JAN|FEV|MAR|ABR|MAI|JUN|JUL|AGO|SET|OUT|NOV|DEZ" +
                        "&siz=" + "28|56|56|56|56|56|56|56|56|56|56|56|56" +
                        "&pdf=" + "14" +
                        "&filter=" + $("#lbltitulo").html() +
                        "&PathArqInclude=" + "../Modulos/BI/" + finalFind(1).replace("?", "&");
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }

            function exportarProdutividade() {
                window.open(finalFind(2) + "&ex=1");
            }

            function finalFind(imp) {
                var retPrint = "?imp=" + imp;
                if ($('#txt_dt_begin').val() !== "") {
                    retPrint = retPrint + "&dti=" + $('#txt_dt_begin').val().replace(/\//g, "_");
                }
                if ($('#txt_dt_end').val() !== "") {
                    retPrint = retPrint + "&dtf=" + $('#txt_dt_end').val().replace(/\//g, "_");
                }
                retPrint = retPrint + "&emp=" + $("#txtLojaSel").val();
                return "BI-Comparativo-Faturamento_server_processing.php" + retPrint;
            }

            $(document).ready(function () {
                var tbReceitaAnalitico = $('#tbReceitaAnalitico').dataTable({
                    bPaginate: false,
                    bFilter: false,
                    bInfo: false,
                    bSort: false,
                    sAjaxSource: finalFind(0),
                    "fnDrawCallback": function (data) {
                        $("#loader").hide();
                        if (data["aoData"].length > 0) {
                            criaGrafico(data["aoData"]);
                        }
                    },
                    'oLanguage': {
                        'oPaginate': {
                            'sFirst': "Primeiro",
                            'sLast': "Último",
                            'sNext': "Próximo",
                            'sPrevious': "Anterior"
                        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                        'sLengthMenu': "Visualização de _MENU_ registros",
                        'sLoadingRecords': "Carregando...",
                        'sProcessing': "Processando...",
                        'sSearch': "Pesquisar:",
                        'sZeroRecords': "Não foi encontrado nenhum resultado"
                    }
                });

                $("#btnfind").click(function () {
                    if (!$.isNumeric($("#txt_dt_begin").val()) || !$.isNumeric($("#txt_dt_end").val())) {
                        bootbox.alert("Preencha os valores de período corretamente!");
                    } else if ((parseInt($("#txt_dt_end").val()) - parseInt($("#txt_dt_begin").val())) < 1 || (parseInt($("#txt_dt_end").val()) - parseInt($("#txt_dt_begin").val())) > 5) {
                        bootbox.alert("Intervalo entre valores inválido!");
                    } else {
                        $("#loader").show();
                        $("#lbltitulo").text("Comparativo de Faturamento entre " + $("#txt_dt_begin").val() + " até " + $("#txt_dt_end").val());
                        tbReceitaAnalitico.fnReloadAjax(finalFind(0));
                    }
                });

                function criaGrafico(data) {
                    var chartData = [];
                    var chartGraphs = [];
                    var meses = ["JAN", "FEV", "MAR", "ABR", "MAI", "JUN", "JUL", "AGO", "SET", "OUT", "NOV", "DEZ"];
                    for (var i = 0; i < data.length; i++) {
                        chartGraphs.push({"title": data[i]["_aData"][0],
                            "balloonText": "[[title]]: [[value]]",
                            "bullet": "round", "bulletSize": 10, "bulletBorderAlpha": 1, "bulletBorderThickness": 2,
                            "valueField": data[i]["_aData"][0]});
                    }
                    for (var i = 0; i < meses.length; i++) {
                        var val = 0;
                        var media = 0;
                        var command = "chartData.push({mes: '" + meses[i] + "',";
                        for (var j = 0; j < data.length; j++) {
                            val += textToNumber(data[j]["_aData"][i + 1]);
                            command += data[j]["_aData"][0] + ": " + textToNumber(data[j]["_aData"][i + 1]) + ",";
                        }
                        media = (val / data.length).toFixed(2);
                        command += "media: " + media + "});";
                        eval(command);
                        $("#tbReceitaAnalitico tfoot tr th")[i + 1].innerHTML = numberFormat(media);
                    }
                    var chartBar = AmCharts.makeChart("chartBar", {
                        "type": "serial",
                        "theme": "light",
                        "dataProvider": chartData,
                        "categoryField": "mes",
                        "startDuration": 1,
                        "numberFormatter": {"precision": 2, "decimalSeparator": lang["centsSeparator"], "thousandsSeparator": lang["thousandsSeparator"]},
                        "graphs": [{
                                "title": "Faturamento",
                                "balloonText": "[[title]]: [[value]]",
                                "labelText": "[[value]]",
                                "fillAlphas": 0.8, "lineAlpha": 0.2, "type": "column", "valueField": "media"
                            }]
                    });
                    var chartLine = AmCharts.makeChart("chartLine", {
                        "type": "serial",
                        "theme": "light",
                        "dataProvider": chartData,
                        "categoryField": "mes",
                        "startDuration": 1,
                        "numberFormatter": {"precision": 2, "decimalSeparator": lang["centsSeparator"], "thousandsSeparator": lang["thousandsSeparator"]},
                        "graphs": chartGraphs,
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        }
                    });
                }
            });
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>
