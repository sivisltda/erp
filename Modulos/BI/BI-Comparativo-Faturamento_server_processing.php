<?php

$url = dirname(__FILE__) . "./../../";
require_once($url . 'Connections/configini.php');
if (!isset($_GET['isPdf'])) {
    require_once($url . 'util/util.php');
}
require_once($url . 'util/excel/exportaExcel.php');

$imprimir = 0;
$DateBegin = '01/01/' . date("Y") . " 00:00:00";
$DateEnd = '31/12/' . date("Y") . " 23:59:59";
$comando = "";
$i = 0;

if (is_numeric($_GET['imp']) && $_GET['imp'] == "1") {
    $imprimir = 1;
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $DateBegin = '01/01/' . str_replace("_", "/", $_GET['dti']) . " 00:00:00";
    $DateEnd = '31/12/' . str_replace("_", "/", $_GET['dtf']) . " 23:59:59";
}

if (is_numeric($_GET['emp'])) {
    $comando .= " AND empresa = " . $_GET['emp'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => 0,
    "iTotalDisplayRecords" => 0,
    "aaData" => array()
);

$sQuery1 = "set dateformat dmy; select x.ano, 
SUM(case when x.mes = 1 then x.total else 0 end) jan, SUM(case when x.mes = 2 then x.total else 0 end) fev, 
SUM(case when x.mes = 3 then x.total else 0 end) mar, SUM(case when x.mes = 4 then x.total else 0 end) abr, 
SUM(case when x.mes = 5 then x.total else 0 end) mai, SUM(case when x.mes = 6 then x.total else 0 end) jun, 
SUM(case when x.mes = 7 then x.total else 0 end) jul, SUM(case when x.mes = 8 then x.total else 0 end) ago, 
SUM(case when x.mes = 9 then x.total else 0 end) 'set', SUM(case when x.mes = 10 then x.total else 0 end) 'out', 
SUM(case when x.mes = 11 then x.total else 0 end) nov, SUM(case when x.mes = 12 then x.total else 0 end) dez 
from(select year(data_vencimento) ano,month(data_vencimento) mes,sum(lp.valor_parcela) total
from sf_lancamento_movimento l inner join sf_grupo_contas gc on gc.id_grupo_contas = l.grupo_conta
inner join sf_lancamento_movimento_parcelas lp on lp.id_lancamento_movimento = l.id_lancamento_movimento
inner join sf_contas_movimento cm on l.conta_movimento = cm.id_contas_movimento
inner join sf_tipo_documento td  on td.id_tipo_documento = l.tipo_documento
where lp.inativo in (0,2) and l.status = 'Aprovado' and lp.somar_rel = 1 and lp.data_pagamento is not null and lp.data_vencimento between '" . $DateBegin . "' and '" . $DateEnd . "'
and cm.tipo = 'C' " . $comando . " group by year(data_vencimento),month(data_vencimento) union all 
select year(data_parcela) ano,month(data_parcela) mes,sum(sp.valor_parcela) total
from sf_solicitacao_autorizacao p inner join sf_grupo_contas gc on gc.id_grupo_contas = p.grupo_conta
inner join sf_solicitacao_autorizacao_parcelas sp on sp.solicitacao_autorizacao = p.id_solicitacao_autorizacao
inner join sf_contas_movimento cm on p.conta_movimento = cm.id_contas_movimento
inner join sf_tipo_documento td  on td.id_tipo_documento = sp.tipo_documento
where sp.inativo in (0,2) and sp.somar_rel = 1 and status = 'Aprovado' and sp.data_pagamento is not null and sp.data_parcela between '" . $DateBegin . "' and '" . $DateEnd . "'
and cm.tipo = 'C' " . $comando . " group by year(data_parcela),month(data_parcela) union all 
select year(data_venda) ano,month(data_venda) mes, sum(sp.valor_parcela) total
from sf_vendas p inner join sf_venda_parcelas sp on sp.venda = p.id_venda
inner join sf_tipo_documento td  on td.id_tipo_documento = sp.tipo_documento
where sp.inativo in (0,2) and sp.somar_rel = 1 and status = 'Aprovado' and data_venda between '" . $DateBegin . "' and '" . $DateEnd . "'
and p.cov = 'V' " . $comando . " group by year(data_venda),month(data_venda)
) as x group by x.ano";

//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($RFP = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = $RFP['ano'];
    $row[] = escreverNumero($RFP['jan']);
    $row[] = escreverNumero($RFP['fev']);
    $row[] = escreverNumero($RFP['mar']);
    $row[] = escreverNumero($RFP['abr']);
    $row[] = escreverNumero($RFP['mai']);
    $row[] = escreverNumero($RFP['jun']);
    $row[] = escreverNumero($RFP['jul']);
    $row[] = escreverNumero($RFP['ago']);
    $row[] = escreverNumero($RFP['set']);
    $row[] = escreverNumero($RFP['out']);
    $row[] = escreverNumero($RFP['nov']);
    $row[] = escreverNumero($RFP['dez']);
    $output['aaData'][] = $row;
    $i++;
}

if (isset($_GET['pdf'])) {
    /* $output['aaTotal'][] = array(6, 6, "Sub Total", "Total", 0, 0);
      $output['aaTotal'][] = array(7, 7, "", "", 0, 0);
      $output['aaTotal'][] = array(8, 8, "", "", 0, 0);
      $output['aaGrupo'] = $grupoArray; */
}
$output['iTotalRecords'] = $i;
$output['iTotalDisplayRecords'] = $i;

if (isset($_GET['ex']) && $_GET['ex'] == 1) {
    $topo = '<tr><td><b>Ano</b></td><td><b>JAN</b></td><td><b>FEV</b></td><td><b>MAR</b></td><td><b>ABR</b></td><td><b>MAI</b></td><td><b>JUN</b></td><td><b>JUL</b></td><td><b>AGO</b></td><td><b>SET</b></td><td><b>OUT</b></td><td><b>NOV</b></td><td><b>DEZ</b></td></tr>';
    exportaExcel($output['aaData'], $topo);
}

if (!isset($_GET['isPdf']) && !isset($_GET['pdf'])) {
    echo json_encode($output);
}

odbc_close($con);
