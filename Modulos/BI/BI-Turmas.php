<?php
include "../../Connections/configini.php";
$F1 = "0";
$F2 = "0";
$DateBegin = getData("B");
$DateEnd = getData("E");
if (is_numeric($_GET["fr"])) {
    $F1 = $_GET["fr"];
}
if (is_numeric($_GET["tp"])) {
    $F2 = $_GET["tp"];
}
if ($_GET["dti"] != "") {
    $DateBegin = str_replace("_", "/", $_GET["dti"]);
}
if ($_GET["dtf"] != "") {
    $DateEnd = str_replace("_", "/", $_GET["dtf"]);
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../SpryAssets/SpryValidationTextField.css"/>
        <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
        <script type='text/javascript' src='../../js/plugins/multiselect/jquery.multi-select.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <style>
            #tbReceita td:last-child, #tbReceita th:last-child { text-align:right }
            #tbCredito td, #tbDebito td { padding:5px 10px }
            #tbReceita td { padding:5px 10px }
            #tbDebito { margin-top:20px }
            .select2-container-multi .select2-choices{ padding-top: 3px;}
            .select2-container{margin-top:3px;}
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"><span class="ico-arrow-right"></span></div>
                        <h1>SIVIS B.I.<small>Relatório de financeiro Turmas</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="block" style="font-size:13px">
                                <select style="width:125px; height:31px" name="txtTpRela" id="txtTpRela">
                                    <option value="0" selected>SINTÉTICO</option>        
                                    <option value="1">ANALÍTICO</option>                            
                                </select>                                                                
                                <select name="itemsProf[]" id="mscProf" placeholder="Professor" multiple="multiple" class="select input-medium" style="width:250px; height:31px;margin-top: 2px;">                                 
                                </select>
                                <select name="itemsStat[]" id="mscStat" placeholder="Turmas" multiple="multiple" class="select input-medium" style="width:250px; height:31px;margin-top: 2px;">
                                </select>
                                de <input type="text" class="datepicker" style="width:80px; height:31px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial"/>
                                até <input type="text" class="datepicker" style="width:80px; height:31px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final"/>
                                <input type="text" id="txtcomissao" name="txtcomissao"  style="width:100px;height:31px;" value="<?php echo escreverNumero(0);?>" placeholder="Comissão">
                                <button onclick="trocaSinal(1)" class="btn btn-primary" type="button" style="padding:2px 7px;background-color: #308698 !important;width:30px;height: 30px;margin-top: 3px;margin-left: -3px;margin-left:5px;"><span id="tp_sinal_1">$</span></button>
                                <button class="button button-turquoise btn-primary" type="button" onclick="AbrirBox(0)" id="btnfind"><span class="ico-search icon-white"></span></button>   
                            </div>
                        </div>
                    </div>
                    <div class="box50">
                        <div class="boxhead">
                            <div class="boxtext">Turmas 
                                <div style="float: right;margin-right: 5px;">
                                    <button type="button" title="Imprimir" class="button button-blue btn-primary" style="margin:0; line-height:17px;margin-top: -3px;" onclick="pdfSintetic('I');">
                                        <span class="ico-print icon-white"></span>
                                    </button>
                                    <button type="button" title="Exportar Excel" class="button button-blue btn-primary" style="margin:0; line-height:17px;margin-top: -3px;" onclick="pdfSintetic('E');">
                                        <span class="ico-download-3 icon-white"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="boxtable" style="height: 380px;">
                            <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbReceita">
                                <thead>
                                    <tr>
                                        <th width="70%">TURMA</th>
                                        <th width="15%"><center>VALOR</center></th>
                                        <th width="15%"><center>COMISSÃO</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>TOTAL</th>
                                        <th><div id="txtTotal"><?php echo escreverNumero(0);?></div></th>
                                         <th><div id="txtTotCom"><?php echo escreverNumero(0);?></div></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="box50" style="margin-left:2.5%">
                        <div class="boxhead">
                            <div class="boxtext">Turmas</div>
                        </div>
                        <div class="boxchart" id="chartdiv"></div>
                    </div>
                    <div class="boxhead topExemple" style="float:none;clear:both;display:none;">
                        <div class="boxtext" style="position: relative;">
                            Lista de Alunos Turmas
                            <div style="top:4px; right:2px; line-height:1; position:absolute">
                                <button type="button" title="Imprimir" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="pdfAnalitic('I');">
                                    <span class="ico-print icon-white"></span>
                                </button>
                                <button type="button" title="Exportar Excel" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="pdfAnalitic('E');">
                                    <span class="ico-download-3 icon-white"></span>
                                </button>
                            </div>
                        </div>
                    </div>              
                    <table class="table" style="float:none;display:none;" cellpadding="0" cellspacing="0" width="100%" id="example">
                        <thead>
                            <tr>
                                <th width="5%"><center>Código</center></th>
                                <th width="22%"><center>Nome</center></th>
                                <th width="20%">Turma</th>
                                <th width="20%">Período</th>
                                <th width="13%"><center>Data PGTO.</center></th>
                                <th width="10%"><center>Valor</center></th>
                                <th width="10%"><center>Comissão</center></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                            <tr>
                                <th>TOTAL</th>
                                <th><div id="txtTotalNumeric" style="text-align: left"><?php echo escreverNumero(0);?></div></th>
                                <th><div></div></th>
                                <th><div></div></th>
                                <th><div></div></th>
                                <th><div id="txtTotalAnalitic"><?php echo escreverNumero(0);?></div></th>
                                <th><div id="txtTotComAnalitic"><?php echo escreverNumero(0);?></div></th>
                            </tr>
                        </tfoot>
                    </table>
                    <div style="clear:both"></div>  
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>  
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/charts.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="amcharts/amcharts.js"></script>
        <script type="text/javascript" src="amcharts/pie.js"></script>
        <script type="text/javascript" src="amcharts/themes/light.js"></script>
        <script type='text/javascript' src="../../js/numeral.min.js"></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>  
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type='text/javascript' src="./js/BI-Turmas.js"></script>
    </body>
<?php odbc_close($con); ?>
</html>