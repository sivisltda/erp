<?php

$url = dirname(__FILE__) . "./../../";
require_once($url . 'Connections/configini.php');
if (!isset($_GET['isPdf'])) {
    require_once($url . 'util/util.php');
}

$imprimir = 0;
$F1 = "";
$WhereX = "";
$DateBegin = date("m/Y");
$DateEnd = date("m/Y");
$sQuery = "";
$comando = "";

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => 0,
    "iTotalDisplayRecords" => 0,
    "aaData" => array()
);

if (is_numeric($_GET['ts'])) {
    $F1 = $_GET['ts'];
}

if (is_numeric($_GET['imp']) && $_GET['imp'] == "1") {
    $imprimir = 1;
}

if ($_GET['fdt'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['fdt']);
    $DateEnd = str_replace("_", "/", $_GET['fdt']);
} elseif ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
}

if (is_numeric($_GET['rep'])) {
    $comando .= " AND sf_fornecedores_despesas.id_user_resp = " . $_GET['rep'];
}

if ($_GET['txtStatus'] != "null" && $_GET['txtStatus'] != "") {    
    if (strpos($_GET['txtStatus'], "Ativo") !== false && $_GET['txtStatus'] != "AtivoEmAberto") {
        $_GET['txtStatus'] = $_GET['txtStatus'] . ",AtivoCred,AtivoAbono,Ativo Ausente 1,Ativo Ausente 2,Ativo Ausente 3,Ativo Ausente 4,Ativo Ausente 5";
    }
    $comando .= " and sf_fornecedores_despesas.fornecedores_status in ('" . str_replace(",", "','", $_GET['txtStatus']) . "') ";
}

if (isset($_GET['gr']) && $_GET['gr'] != 'null') {
    $comando .= " and (sf_fornecedores_despesas.grupo_pessoa in (" . $_GET['gr'] . ")" . (strpos($_GET['gr'], "-1") !== false ? " or sf_fornecedores_despesas.grupo_pessoa is null" : "") . ")";
}

if (isset($_GET['fil'])) {
    $comando .= " and sf_fornecedores_despesas.empresa in (" . str_replace(array("[", "]", "\"", "'"), "", $_GET["fil"]) . ")";
} else {
    $comando .= " and sf_fornecedores_despesas.empresa in (select id_filial_f from sf_usuarios_filiais where id_usuario_f = " . $_SESSION["id_usuario"] . ")";    
}

if ($_GET['ftp'] != '') {
    if ($_GET['ftp'] == 'Novos') {
        $WhereX .= " and (select max(dt_pagamento_mens) from sf_vendas_planos_mensalidade B where B.id_plano_mens = A.id_plano_mens and B.id_mens < A.id_mens) is null";
    } elseif ($_GET['ftp'] == 'Retornos') {
        $WhereX .= " and (select max(dt_pagamento_mens) from sf_vendas_planos_mensalidade B where B.id_plano_mens = A.id_plano_mens and B.id_mens < A.id_mens) is not null
        and datediff(day,(select max(dt_pagamento_mens) from sf_vendas_planos_mensalidade B where B.id_plano_mens = A.id_plano_mens and B.id_mens < A.id_mens),dt_pagamento_mens) > (select ACA_PLN_DIAS_RENOVACAO from sf_configuracao)";
    } elseif ($_GET['ftp'] == 'Renovacoes') {
        $WhereX .= " and (select max(dt_pagamento_mens) from sf_vendas_planos_mensalidade B where B.id_plano_mens = A.id_plano_mens and B.id_mens < A.id_mens) is not null
        and datediff(day,(select max(dt_pagamento_mens) from sf_vendas_planos_mensalidade B where B.id_plano_mens = A.id_plano_mens and B.id_mens < A.id_mens),dt_pagamento_mens) <= (select ACA_PLN_DIAS_RENOVACAO from sf_configuracao)";
    }
}

if ($_GET['ftp'] == '' || $_GET['ftp'] == 'Novos' || $_GET['ftp'] == 'Retornos' || $_GET['ftp'] == 'Renovacoes') {
    $sQuery .= "select (select max(dt_pagamento_mens) from sf_vendas_planos_mensalidade B where B.id_plano_mens = A.id_plano_mens and B.id_mens < A.id_mens) anterior, 
    dt_pagamento_mens atual, 0 desligado, (select ACA_PLN_DIAS_RENOVACAO from sf_configuracao) dias, year(dt_pagamento_mens) year, month(dt_pagamento_mens) month,
    id_fornecedores_despesas, razao_social, data_nascimento, fornecedores_status, id_user_resp, grupo_pessoa,
    dt_inicio_mens, dt_fim_mens, planos_status, cnpj, sf_produtos.descricao, id_prod_plano, id_plano
    from sf_fornecedores_despesas inner join sf_vendas_planos on id_fornecedores_despesas = favorecido  
    inner join sf_vendas_planos_mensalidade A on A.id_plano_mens = sf_vendas_planos.id_plano
    inner join sf_produtos on sf_produtos.conta_produto = id_prod_plano    
    where sf_fornecedores_despesas.tipo = 'C' " . $comando . " and inativo = 0
    and id_item_venda_mens is not null and dt_pagamento_mens between '01/" . $DateBegin . " 00:00:00'
    and dateadd(day,-1,dateadd(month,1,'01/" . $DateEnd . " 23:59:59')) " . $WhereX . "";
}
if ($_GET['ftp'] == '') {
    $sQuery .= " union all ";
}
if ($_GET['txtTipo'] == "1" && ($_GET['ftp'] == '' || $_GET['ftp'] == 'Desligados')) {
    $sQuery .= "select null anterior, null atual, 1 desligados, 0 dias, year(maior_vencimento) year, month(maior_vencimento) month,
    id_fornecedores_despesas,razao_social,data_nascimento, fornecedores_status, id_user_resp, grupo_pessoa,
    null dt_inicio_mens,null dt_fim_mens,null planos_status,cnpj,null descricao,null id_prod_plano,null id_plano
    from (select distinct favorecido, dateadd(day, (select ACA_DIAS_SUSPENSO from sf_configuracao), max(dt_fim)) maior_vencimento 
    from sf_vendas_planos inner join sf_fornecedores_despesas on favorecido = id_fornecedores_despesas
    where sf_fornecedores_despesas.tipo = 'C' " . $comando . " and inativo = 0
    group by favorecido) as y inner join sf_fornecedores_despesas on y.favorecido = id_fornecedores_despesas
    where y.maior_vencimento < cast(getdate() as date) and y.maior_vencimento between '01/" . $DateBegin . " 00:00:00' 
    and dateadd(day,-1,dateadd(month,1,'01/" . $DateEnd . " 23:59:59'))";
} else if ($_GET['ftp'] == '' || $_GET['ftp'] == 'Desligados') {
    $sQuery .= "select null anterior, null atual, 1 desligados, 0 dias, year(data_trancamento) year, month(data_trancamento) month,
    id_fornecedores_despesas,razao_social,data_nascimento, fornecedores_status, id_user_resp, grupo_pessoa,
    null dt_inicio_mens,null dt_fim_mens,null planos_status,cnpj,null descricao,null id_prod_plano,null id_plano
    from sf_fornecedores_despesas inner join sf_trancamento on id_fornecedores_despesas = fornecedores_despesas
    where sf_fornecedores_despesas.tipo = 'C' " . $comando . " and inativo = 0 and sf_trancamento.tipo = 1
    and data_trancamento between '01/" . $DateBegin . " 00:00:00' and dateadd(day,-1,dateadd(month,1,'01/" . $DateEnd . " 23:59:59'))";
}

$sQuery1 = "";
if ($F1 == 0) {
    $sQuery1 = "set dateformat dmy; select sum(case when x.desligado = 0 and x.anterior is null then 1 else 0 end) 'Novos', 
    sum(case when x.desligado = 0 and x.anterior is not null and datediff(day,x.anterior,x.atual) > x.dias then 1 else 0 end) 'Retornos', 
    sum(case when x.desligado = 0 and x.anterior is not null and datediff(day,x.anterior,x.atual) <= x.dias then 1 else 0 end) 'Renovacoes',
    sum(case when x.desligado > 0 then 1 else 0 end) 'Desligados', x.year, x.month from(" . $sQuery . ") as x group by x.year, x.month order by x.year, x.month";
} elseif ($F1 == 1 && ($WhereX != "" || $_GET['ftp'] == 'Desligados')) {
    $sQuery1 = "set dateformat dmy; select id_fornecedores_despesas,razao_social,data_nascimento, dt_inicio_mens dt_inicio, dt_fim_mens dt_fim, descricao, planos_status status, 
    (select top 1 case when nome = '' then login_user else nome end login_user from sf_usuarios c where c.inativo = 0 and c.id_usuario = sf_fornecedores_despesas.id_user_resp) consultor,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'celular',     
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'email', cnpj,
    (select max(cast(valor as varchar) + '-' + case when tp_valor = 'D' then '$' else '%' end + '-' + case when ate_vencimento = 1 then 'SIM' else 'NÃO'end) 
        from sf_fornecedores_despesas_convenios fc 
        inner join sf_convenios on convenio = id_convenio
        inner join sf_convenios_planos on id_convenio = id_convenio_planos
        inner join sf_convenios_regras on sf_convenios_regras.convenio = id_convenio
        where id_fornecedor = id_fornecedores_despesas and id_prod_convenio = id_prod_plano and id_conv_plano = id_plano
        --and vp.dt_inicio between fc.dt_inicio and fc.dt_fim
    ) desconto, (select dbo.VALOR_REAL_MENSALIDADE(min(id_mens)) from sf_vendas_planos_mensalidade where id_plano_mens = id_plano) valor_mens 
    from (" . $sQuery . ") as sf_fornecedores_despesas order by dt_inicio";
}
//echo $sQuery1; exit;
if (strlen($sQuery1) > 0) {
    $cur = odbc_exec($con, $sQuery1);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        if ($F1 == 0) {
            $row[] = str_pad(utf8_encode($RFP['month']), 2, "0", STR_PAD_LEFT) . "/" . utf8_encode($RFP['year']);
            $row[] = utf8_encode($RFP['Novos']);        
            $row[] = utf8_encode($RFP['Retornos']);
            $row[] = utf8_encode($RFP['Renovacoes']);
            $row[] = utf8_encode($RFP['Desligados']);
        } elseif ($F1 == 1) {
            $consultor = (strlen($RFP['consultor']) > 0 ? formatNameCompact(utf8_encode($RFP['consultor'])) : "Sem Consultor");
            $row[] = "<a href=\"javascript:void(0)\" onclick=\"AbrirBox(2," . $RFP['id_fornecedores_despesas'] . ")\">" . utf8_encode($RFP['razao_social']) . "</a>";        
            $row[] = escreverData($RFP['dt_inicio']) . " até " . escreverData($RFP['dt_fim']);
            $row[] = escreverNumero($RFP['valor_mens'], 1);
                $desconto = explode("-", $RFP['desconto']);
                $row[] = "<center>" . (count($desconto) == 3 ? $desconto[0] : "") . "</center>";    
                $row[] = "<center>" . (count($desconto) == 3 ? $desconto[1] : "") . "</center>";    
                $row[] = "<center>" . (count($desconto) == 3 ? $desconto[2] : "") . "</center>";
            $row[] = utf8_encode($RFP['status']);
            $row[] = utf8_encode($RFP['descricao']);
            $row[] = $consultor;
            $row[] = utf8_encode($RFP['celular']);
            $row[] = utf8_encode($RFP['email']);
            $row[] = utf8_encode($RFP['cnpj']);
            if (isset($_GET['pdf'])) {
                $row[] = $consultor;
                $grp = array($consultor, $consultor, 12);
                if (!in_array($grp, $grupoArray)) {
                    $grupoArray[] = $grp;
                }
            }            
        }        
        $output['aaData'][] = $row;
    }
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
} else {
    $output['aaGrupo'] = $grupoArray;
}

odbc_close($con);
