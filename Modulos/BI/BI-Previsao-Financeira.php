<?php include "../../Connections/configini.php"; 
$F1 = "0";
$DateBegin = date("Y");
if (is_numeric($_GET["fr"])) {
    $F1 = $_GET["fr"];
}
if (is_numeric($_GET["ano"])) {
    $DateBegin = $_GET["ano"];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <style>
            #tabela_1, #tabela_2 { width: 100%; border-spacing: 5px; border-collapse: separate; }
            #tabela_1 td, #tabela_2 td { padding: 15px 5px; text-align: center; font-size: 12px; background: #EEE; }
            #tabela_3 td {text-align: right;}
            #tabela_3 td:nth-child(1) {text-align: left;}
            .txt_head { margin-left: 10px; font-size: 17px; font-weight: bold; }
            .txt_text { margin: 0 0 5px 10px; }
            .txt_text b { color: #FF0000; }
            
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1>SIVIS B.I.<small>Previsão Financeira</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="block" style="font-size:13px">
                                <div style="float: left;margin-right: 0.5%;">
                                    <input type="text" maxlength="4" style="width:80px; height:31px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Ano inicial"/>
                                </div>                                
                                <select style="width:125px; height:31px" name="txtTpData" id="txtTpData">
                                    <option value="0" <?php echo ($F1 == "0" ? "SELECTED" : "");?>>RECEBIMENTO</option>
                                    <option value="1" <?php echo ($F1 == "1" ? "SELECTED" : "");?>>VENDA</option>                                    
                                </select>                                
                                <button class="button button-turquoise btn-primary" type="button" onclick="AbrirBox(0)" id="btnfind"><span class="ico-search icon-white"></span></button>
                            </div>
                        </div>
                    </div>                    
                    <div class="row-fluid">
                        <div class="boxhead">
                            <div class="boxtext">Receitas x Despesas Retroativo</div>
                        </div>
                        <div class="boxchart" id="chartdiv"></div>
                        <div class="boxhead">
                            <div class="boxtext">DRE - Demonstrativo de Resultados do Exercício - Sintético</div>
                        </div>
                        <div class="boxtable">
                            <table style="float:none" id="tabela_1">
                                <thead>
                                    <tr>
                                        <th style="width:130px"></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="boxhead">
                            <div class="boxtext">DRE - Demonstrativo de Resultados do Exercício - Analítico</div>
                        </div>
                        <div class="boxtable">
                            <table class="table" style="float:none" id="tabela_3">
                                <thead>
                                    <tr>
                                        <th style="width:15.5%"></th>
                                        <th style="width:6.5%">JANEIRO</th>
                                        <th style="width:6.5%">FEVEREIRO</th>
                                        <th style="width:6.5%">MARÇO</th>
                                        <th style="width:6.5%">ABRIL</th>
                                        <th style="width:6.5%">MAIO</th>
                                        <th style="width:6.5%">JUNHO</th>
                                        <th style="width:6.5%">JULHO</th>
                                        <th style="width:6.5%">AGOSTO</th>
                                        <th style="width:6.5%">SETEMBRO</th>
                                        <th style="width:6.5%">OUTUBRO</th>
                                        <th style="width:6.5%">NOVEMBRO</th>
                                        <th style="width:6.5%">DEZEMBRO</th>
                                        <th style="width:6.5%">TOTAL</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <div class="boxhead">
                            <div class="boxtext">Fluxo de Caixa Projetado</div>
                        </div>
                        <div class="boxtable">
                            <div class="txt_head">Faça uma análise das contas a pagar / receber e da sua necessidade de caixa para cada mês:</div>
                            <div class="txt_text"><b>Importante:</b> Aqui você terá uma visão consolidada do seu contas a pagar / receber. Obeserve que a Necessidade de Caixa é o valor que você precisa ter no caixa para conseguir honrar seus compromissos financeiros.</div>
                            <table style="float:none" id="tabela_2">
                                <thead>
                                    <tr>
                                        <th style="width:130px"></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
        <script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script>
        <script type="text/javascript" src="amcharts/themes/light.js"></script>
        <script type='text/javascript' src="../../js/numeral.min.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>                
        <script type="text/javascript">

            var chartData = [];
            
            function AbrirBox(id) {
                let retPrint = "?filial=" + $("#txtMnFilial").val();
                if ($("#txtTpData").val() !== "") {
                    retPrint = retPrint + "&fr=" + $("#txtTpData").val();
                }
                if ($("#txt_dt_begin").val() !== "") {
                    retPrint = retPrint + "&ano=" + $("#txt_dt_begin").val();
                }
                window.location = "BI-Previsao-Financeira.php" + retPrint;
            }                                 
            
            $.getJSON("BI-Previsao-Financeira-Caixa_server_processing.php?filial=" + $("#txtMnFilial").val() + 
                "&fr=" + $("#txtTpData").val() + "&ano=" + $("#txt_dt_begin").val(), function (data) {
                $.each(data, function (key, val) {
                    chartData[key] = {mes: val.mes, receitas: val.receitas, despesas: val.despesas};
                });
            }).done(function () {
                $.each(chartData, function (idx) {
                    if (idx < 12) {
                        $("#tabela_1 thead tr").append("<th>" + chartData[idx]["mes"] + "</th>");
                    }
                });
                var lucro = [0];
                var saldo = [0];
                for (var i = 0; i < 12; i++) {
                    lucro[i] = chartData[i]["receitas"] - chartData[i]["despesas"];                    
                }
                saldo[0] = chartData[12]["receitas"] - chartData[12]["despesas"];
                for (var i = 0; i < 12; i++) {
                    saldo[i + 1] = saldo[i] + lucro[i];                    
                }
                var dados_1 = [
                    ["Saldo Inicial", numberFormat(saldo[0], 1), numberFormat(saldo[1], 1), numberFormat(saldo[2], 1), numberFormat(saldo[3], 1),
                        numberFormat(saldo[4], 1), numberFormat(saldo[5], 1), numberFormat(saldo[6], 1), numberFormat(saldo[7], 1), numberFormat(saldo[8], 1), numberFormat(saldo[9], 1), numberFormat(saldo[10], 1), numberFormat(saldo[11], 1)],
                    ["Receitas", numberFormat(chartData[0]["receitas"], 1), numberFormat(chartData[1]["receitas"], 1), numberFormat(chartData[2]["receitas"], 1), numberFormat(chartData[3]["receitas"], 1), numberFormat(chartData[4]["receitas"], 1),
                        numberFormat(chartData[5]["receitas"], 1), numberFormat(chartData[6]["receitas"], 1), numberFormat(chartData[7]["receitas"], 1), numberFormat(chartData[8]["receitas"], 1), numberFormat(chartData[9]["receitas"], 1), numberFormat(chartData[10]["receitas"], 1), numberFormat(chartData[11]["receitas"], 1)],
                    ["Despesas", numberFormat(chartData[0]["despesas"], 1), numberFormat(chartData[1]["despesas"], 1), numberFormat(chartData[2]["despesas"], 1), numberFormat(chartData[3]["despesas"], 1), numberFormat(chartData[4]["despesas"], 1),
                        numberFormat(chartData[5]["despesas"], 1), numberFormat(chartData[6]["despesas"], 1), numberFormat(chartData[7]["despesas"], 1), numberFormat(chartData[8]["despesas"], 1), numberFormat(chartData[9]["despesas"], 1), numberFormat(chartData[10]["despesas"], 1), numberFormat(chartData[11]["despesas"], 1)],
                    ["Lucro / Prejuízo", numberFormat(lucro[0], 1), numberFormat(lucro[1], 1), numberFormat(lucro[2], 1), numberFormat(lucro[3], 1), numberFormat(lucro[4], 1), numberFormat(lucro[5], 1), numberFormat(lucro[6], 1), numberFormat(lucro[7], 1), numberFormat(lucro[8], 1), numberFormat(lucro[9], 1), numberFormat(lucro[10], 1), numberFormat(lucro[11], 1)],
                    ["Acumulado", numberFormat(saldo[1], 1), numberFormat(saldo[2], 1), numberFormat(saldo[3], 1), numberFormat(saldo[4], 1), numberFormat(saldo[5], 1), numberFormat(saldo[6], 1), numberFormat(saldo[7], 1), numberFormat(saldo[8], 1), numberFormat(saldo[9], 1), numberFormat(saldo[10], 1), numberFormat(saldo[11], 1), numberFormat(saldo[12], 1)],
                    ["Lucratividade", calcLucratividade(chartData[0]["despesas"], chartData[0]["receitas"]),
                        calcLucratividade(chartData[1]["despesas"], chartData[1]["receitas"]),
                        calcLucratividade(chartData[2]["despesas"], chartData[2]["receitas"]),
                        calcLucratividade(chartData[3]["despesas"], chartData[3]["receitas"]),
                        calcLucratividade(chartData[4]["despesas"], chartData[4]["receitas"]),
                        calcLucratividade(chartData[5]["despesas"], chartData[5]["receitas"]),
                        calcLucratividade(chartData[6]["despesas"], chartData[6]["receitas"]),
                        calcLucratividade(chartData[7]["despesas"], chartData[7]["receitas"]),
                        calcLucratividade(chartData[8]["despesas"], chartData[8]["receitas"]),
                        calcLucratividade(chartData[9]["despesas"], chartData[9]["receitas"]),
                        calcLucratividade(chartData[10]["despesas"], chartData[10]["receitas"]),
                        calcLucratividade(chartData[11]["despesas"], chartData[11]["receitas"])]
                ];       
                chartData.pop();
                var chart = AmCharts.makeChart("chartdiv", {
                    "type": "serial",
                    "theme": "light",
                    "legend": {
                        "maxColumns": 2,
                        "position": "bottom",
                        "useGraphSettings": true,
                        "markerSize": 20
                    },
                    "numberFormatter": {"precision": 2, "decimalSeparator": lang["centsSeparator"], "thousandsSeparator": lang["thousandsSeparator"]},
                    "pathToImages": "http://www.amcharts.com/lib/3/images/",
                    "categoryField": "mes",
                    "startDuration": 1,
                    "categoryAxis": {
                        "gridPosition": "start",
                        "axisAlpha": 0,
                        "gridAlpha": 0,
                        "position": "left"
                    },
                    "graphs": [{
                            "balloonText": "[[title]]: [[value]]",
                            "fillAlphas": 0.8,
                            "lineAlpha": 0.2,
                            "title": "Receitas",
                            "type": "column",
                            "fillColors": "#009900",
                            "valueField": "receitas"
                        }, {
                            "balloonText": "[[title]]: [[value]]",
                            "fillAlphas": 0.8,
                            "lineAlpha": 0.2,
                            "title": "Despesas",
                            "type": "column",
                            "fillColors": "#990000",
                            "valueField": "despesas"
                        }],
                    "dataProvider": chartData                    
                });
                $("#tabela_1").DataTable({
                    aaData: dados_1, bPaginate: false, bFilter: false, bInfo: false, bSort: false,
                    aoColumnDefs: [{
                        aTargets: [0], fnCreatedCell: function (nTd) {
                            $(nTd).css({"background-color": "#6699CC", "color": "#FFFFFF"});
                        }
                    }, {
                        aTargets: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], fnCreatedCell: function (nTd, sData, iRow, iCol) {
                            if (iCol === 3 || iCol === 4) {
                                var myData = textToNumber(sData);
                                if (myData < 0.00) {
                                    $(nTd).css({"background-color": "#FFC7CE", "color": "#9C0006"});
                                } else if (myData > 0.00) {
                                    $(nTd).css({"background-color": "#C6EFCE", "color": "#006100"});
                                }
                            }
                        }
                    }]
                });
                $("svg[version='1.1']").next().hide();
            });
            
            var chartData2 = [];
            $.getJSON("BI-Previsao-Financeira-Conta_server_processing.php?sr=0" + 
                "&filial=" + $("#txtMnFilial").val() + "&ano=" + $("#txt_dt_begin").val(), function (data) {
                $.each(data, function (key, val) {
                    chartData2[key] = {mes: val.mes, receber: val.receber, preceber: val.preceber, pagar: val.pagar};
                });
            }).done(function () {
                $.each(chartData2, function (idx) {
                    $("#tabela_2 thead tr").append("<th>" + chartData2[idx]["mes"] + "</th>");
                });
                var conta = [];
                for (var i = 0; i < 12; i++) {
                    if ((parseFloat(chartData2[i]["pagar"]) - (parseFloat(chartData2[i]["receber"]) + parseFloat(chartData2[i]["preceber"]))) > 0) {
                        conta[i] = (parseFloat(chartData2[i]["pagar"]) - (parseFloat(chartData2[i]["receber"]) + parseFloat(chartData2[i]["preceber"])));
                    } else {
                        conta[i] = 0.00;
                    }
                }
                var dados_2 = [
                    ["Contas a Receber", numberFormat(chartData2[0]["receber"], 1), numberFormat(chartData2[1]["receber"], 1), numberFormat(chartData2[2]["receber"], 1), numberFormat(chartData2[3]["receber"], 1), numberFormat(chartData2[4]["receber"], 1), numberFormat(chartData2[5]["receber"], 1), numberFormat(chartData2[6]["receber"], 1), numberFormat(chartData2[7]["receber"], 1), numberFormat(chartData2[8]["receber"], 1), numberFormat(chartData2[9]["receber"], 1), numberFormat(chartData2[10]["receber"], 1), numberFormat(chartData2[11]["receber"], 1)],
                    ["Previsão a Receber", numberFormat(chartData2[0]["preceber"], 1), numberFormat(chartData2[1]["preceber"], 1), numberFormat(chartData2[2]["preceber"], 1), numberFormat(chartData2[3]["preceber"], 1), numberFormat(chartData2[4]["preceber"], 1), numberFormat(chartData2[5]["preceber"], 1), numberFormat(chartData2[6]["preceber"], 1), numberFormat(chartData2[7]["preceber"], 1), numberFormat(chartData2[8]["preceber"], 1), numberFormat(chartData2[9]["preceber"], 1), numberFormat(chartData2[10]["preceber"], 1), numberFormat(chartData2[11]["preceber"], 1)],
                    ["Contas a Pagar", numberFormat(chartData2[0]["pagar"], 1), numberFormat(chartData2[1]["pagar"], 1), numberFormat(chartData2[2]["pagar"], 1), numberFormat(chartData2[3]["pagar"], 1), numberFormat(chartData2[4]["pagar"], 1), numberFormat(chartData2[5]["pagar"], 1), numberFormat(chartData2[6]["pagar"], 1), numberFormat(chartData2[7]["pagar"], 1), numberFormat(chartData2[8]["pagar"], 1), numberFormat(chartData2[9]["pagar"], 1), numberFormat(chartData2[10]["pagar"], 1), numberFormat(chartData2[11]["pagar"], 1)],
                    ["Necessidade de Caixa", numberFormat(conta[0], 1), numberFormat(conta[1], 1), numberFormat(conta[2], 1), numberFormat(conta[3], 1), numberFormat(conta[4], 1), numberFormat(conta[5], 1), numberFormat(conta[6], 1), numberFormat(conta[7], 1), numberFormat(conta[8], 1), numberFormat(conta[9], 1), numberFormat(conta[10], 1), numberFormat(conta[11], 1)]
                ];
                $("#tabela_2").DataTable({
                    aaData: dados_2, bPaginate: false, bFilter: false, bInfo: false, bSort: false,
                    aoColumnDefs: [{
                        aTargets: [0], fnCreatedCell: function (nTd, sData, iRow, iCol) {
                            if (iCol === 0) {
                                $(nTd).css({"background-color": "#00B050", "color": "#FFFFFF"});
                            }
                            if (iCol === 1) {
                                $(nTd).css({"background-color": "#61b020", "color": "#FFFFFF"});
                            }
                            if (iCol === 2) {
                                $(nTd).css({"background-color": "#CC3333", "color": "#FFFFFF"});
                            }
                            if (iCol === 3) {
                                $(nTd).css({"background-color": "#6699CC", "color": "#FFFFFF"});
                            }
                        }
                    }, {
                        aTargets: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], fnCreatedCell: function (nTd, sData, iRow, iCol) {
                            if (iCol === 3) {
                                var myData = textToNumber(sData);
                                if (myData > 0.00) {
                                    $(nTd).css({"background-color": "#FFC7CE", "color": "#9C0006"});
                                } else if (myData === 0.00) {
                                    $(nTd).css({"background-color": "#C6EFCE", "color": "#006100"});
                                }
                            }
                        }
                    }]
                });
            });

            function calcLucratividade(valor1, valor2) {
                if ($.isNumeric(valor1) && $.isNumeric(valor2) && valor2 > 0) {
                    return numberFormat((1 - (valor1 / valor2)) * 100, 0) + " %";
                } else {
                    return numberFormat(0, 0) + " %";
                }
            }
            
            function addGroup(descricao, tipo, data) {
                let jan = 0.00, fev = 0.00, mar = 0.00, abr = 0.00, mai = 0.00, jun = 0.00, jul = 0.00, ago = 0.00, set = 0.00, out = 0.00, nov = 0.00, dez = 0.00, tot = 0.00;
                let cor = (tipo === 'D' ? `background-color: rgb(255, 199, 206); color: rgb(156, 0, 6)` : `background-color: rgb(198, 239, 206); color: rgb(0, 97, 0)`);
                let toReturn = `<tr><td style="${cor};" colspan="13">${descricao}</td><td style="${cor};"></td></tr>`;   
                $.each(data, function (key, val) {                     
                    if (descricao === val.grupo && tipo === val.tipo) {
                        toReturn += addSubGroup(val);
                        jan += textToNumber(val.jan); fev += textToNumber(val.fev); mar += textToNumber(val.mar); 
                        abr += textToNumber(val.abr); mai += textToNumber(val.mai); jun += textToNumber(val.jun); 
                        jul += textToNumber(val.jul); ago += textToNumber(val.ago); set += textToNumber(val.set); 
                        out += textToNumber(val.out); nov += textToNumber(val.nov); dez += textToNumber(val.dez); 
                        tot += textToNumber(val.total);
                    }
                });
                toReturn += `<tr>
                    <td style="${cor};">TOTAL</td>
                    <td style="${cor};">${numberFormat(jan)}</td><td style="${cor};">${numberFormat(fev)}</td>
                    <td style="${cor};">${numberFormat(mar)}</td><td style="${cor};">${numberFormat(abr)}</td>
                    <td style="${cor};">${numberFormat(mai)}</td><td style="${cor};">${numberFormat(jun)}</td>
                    <td style="${cor};">${numberFormat(jul)}</td><td style="${cor};">${numberFormat(ago)}</td>
                    <td style="${cor};">${numberFormat(set)}</td><td style="${cor};">${numberFormat(out)}</td>
                    <td style="${cor};">${numberFormat(nov)}</td><td style="${cor};">${numberFormat(dez)}</td>
                    <td style="${cor};">${numberFormat(tot)}</td>
                </tr>`;
                toReturn += `<tr><td colspan="14"></td></tr>`;
                return toReturn;
            }
            
            function addSubGroup(data) {
                let toReturn = `<tr>
                    <td>${data.conta}</td><td>${data.jan}</td><td>${data.fev}</td>
                    <td>${data.mar}</td><td>${data.abr}</td><td>${data.mai}</td>
                    <td>${data.jun}</td><td>${data.jul}</td><td>${data.ago}</td>
                    <td>${data.set}</td><td>${data.out}</td><td>${data.nov}</td>
                    <td>${data.dez}</td><td>${data.total}</td>
                </tr>`;
                return toReturn;
            }
            
            $.getJSON("BI-Previsao-Financeira-Caixa_server_processing.php?tp=1&filial=" + $("#txtMnFilial").val() + 
                "&fr=" + $("#txtTpData").val() + "&ano=" + $("#txt_dt_begin").val(), function (data) {
                let itensC = [];    
                let itensD = [];    
                $.each(data, function (key, val) {
                    if (val.tipo === 'C' && !itensC.includes(val.grupo)) {
                        itensC.push(val.grupo);
                        $("#tabela_3 tbody").append(addGroup(val.grupo, val.tipo, data));
                    } else if (val.tipo === 'D' && !itensD.includes(val.grupo)) {
                        itensD.push(val.grupo);
                        $("#tabela_3 tbody").append(addGroup(val.grupo, val.tipo, data));
                    }
                });
            });
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>
