<?php

require_once(__DIR__ . '/../../Connections/configini.php');

$sWhere = "";
$sQtd = 0;
$sLimit = 20;
$imprimir = 0;
$id_banco = "0";
$DateBegin = getData("T");
$DateEnd = getData("T");
$tpData = "2";
$id_operador = "";
$comando = '0';
$comando2 = '0';
$queryOperadorL = "";
$queryOperadorS = "";
$queryOperadorV = "";
$filial = '';

function formataNumeros($valor) {
    if (is_numeric($_GET['ts']) && $_GET['ts'] > 0) {
        return escreverNumero($valor, 1);
    } else {
        return $valor;
    }
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}
if (is_numeric($_GET['imp']) && $_GET['imp'] == 1) {
    $imprimir = 1;
}
if (is_numeric($_GET['id'])) {
    $id_banco = $_GET['id'];
}
if (is_numeric($_GET['tpData'])) {
    $tpData = $_GET['tpData'];
}
if ($_GET['dti'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
}
if ($_GET['dtf'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
}
if ($_GET['id_operador'] != '') {
    $id_operador = str_replace("_", "/", $_GET['id_operador']);
}
$queryDataL = ($tpData == "1") ? "data_pagamento" : "data_pagamento";
$queryDataV = ($tpData == "1") ? "data_pagamento" : "data_venda";
$queryDataS = ($tpData == "1") ? "data_pagamento" : "data_lanc";
if ($id_operador !== "") {
    $queryOperadorL .= " and UPPER(syslogin) = UPPER('" . $id_operador . "')";
    $queryOperadorS .= " and UPPER(sys_login) = UPPER('" . $id_operador . "')";
    $queryOperadorV .= " and UPPER(vendedor) = UPPER('" . $id_operador . "')";
}
if ($tpData == "1") {
    $queryOperadorL .= " and td.id_tipo_documento not in (12)";
    $queryOperadorS .= " and td.id_tipo_documento not in (12)";
    $queryOperadorV .= " and td.id_tipo_documento not in (12)";
} else if ($tpData == "2") {
    $queryOperadorL .= " and somar_rel = 1";
    $queryOperadorS .= " and somar_rel = 1";
    $queryOperadorV .= " and somar_rel = 1";
}
if (is_numeric($id_banco)) {
    if ($id_banco > 0) {
        $comando = $id_banco;
    } else {
        $cur = odbc_exec($con, "select id_bancos from sf_bancos") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $comando .= "," . $RFP['id_bancos'];
        }
    }
}
$comandoVenda = " and id_banco in(" . $comando . ") ";
if ($filial == '') {
    $cur = odbc_exec($con, "select TOP 1 id_filial_f from sf_usuarios_filiais A 
    inner join sf_usuarios B on (A.id_usuario_f = B.id_usuario and A.id_filial_f = B.filial)        
    where id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
    while ($RFP = odbc_fetch_array($cur)) {
        $filial = $RFP['id_filial_f'];
    }
}
if (is_numeric($filial)) {
    if ($filial > 0) {
        $comando2 = $filial;
    } else {
        $query = "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'";
        $cur = odbc_exec($con, $query) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $comando2 .= "," . $RFP['id_filial'];
        }
    }
}

$dias = (int) floor((geraTimestamp($DateEnd) - geraTimestamp($DateBegin)) / (60 * 60 * 24));
if ($dias > 0) {
    for ($i = 0; $i <= $dias; $i++) {
        $data = escreverDataSoma($DateBegin, " + " . $i . " days");
        $aux[$i] = $data;
        $myArray[$i]['dia'] = $data;
        $myArray[$i]['receitas'] = 0.00;
        $myArray[$i]['despesas'] = 0.00;
    }
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $dias,
    "iTotalDisplayRecords" => $dias,
    "aaData" => array()
);

$sQuery1 = "set dateformat dmy; 
select cast(data_pagamento as date) data_vencimento, sum (case when tipo = 'C' then valor_pago else 0 end) receitas, sum (case when tipo = 'D' then valor_pago else 0 end) despesas from (     
select id_parcela," . $queryDataL . ",numero_filial empresa,sf_bancos.razao_social banco,TD.descricao tipo_documento,sf_fornecedores_despesas.razao_social,historico_baixa,
case when 1 = " . $tpData . " then valor_pago else valor_parcela end valor_pago,
sf_contas_movimento.tipo tipo, 0 id_trans, sf_lancamento_movimento_parcelas.somar_rel,sf_contas_movimento.descricao
from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento
on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas
inner join sf_bancos on sf_bancos.id_bancos = sf_lancamento_movimento_parcelas.id_banco
left join sf_filiais on sf_filiais.id_filial = sf_lancamento_movimento.empresa
inner join sf_tipo_documento TD on sf_lancamento_movimento_parcelas.tipo_documento = TD.id_tipo_documento
where sf_lancamento_movimento.status = 'Aprovado' and " . $queryDataL . " is not null AND sf_lancamento_movimento.empresa in (" . $comando2 . ") AND sf_lancamento_movimento_parcelas.inativo = '0'
and " . $queryDataL . " between " . valoresDataHora2($DateBegin, "00:00:00") . " and " . valoresDataHora2($DateEnd, "23:59:59") . " and id_banco in (" . $comando . ") " . $queryOperadorL . "
union all
select id_parcela," . $queryDataV . ",numero_filial empresa,sf_bancos.razao_social banco,TD.descricao,sf_fornecedores_despesas.razao_social,historico_baixa,                                                           
case when 1 = " . $tpData . " then valor_pago else valor_parcela end valor_pago,
CASE sf_vendas.cov WHEN 'V' THEN ('C')WHEN 'C' THEN ('D') end tipo, 0 id_trans, sf_venda_parcelas.somar_rel,                                                            
(STUFF((SELECT distinct ',' + descricao FROM sf_vendas_itens vi inner join sf_produtos p on p.conta_produto = vi.produto WHERE vi.id_venda = sf_vendas.id_venda FOR XML PATH('')), 1, 1, '')) descricao2
from sf_venda_parcelas inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda
inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas 
left join sf_bancos on sf_bancos.id_bancos = sf_venda_parcelas.id_banco
left join sf_filiais on sf_filiais.id_filial = sf_vendas.empresa
inner join sf_tipo_documento TD on sf_venda_parcelas.tipo_documento = TD.id_tipo_documento
where sf_vendas.cov  in('V','C') and sf_vendas.status = 'Aprovado' AND sf_vendas.empresa in (" . $comando2 . ") and " . $queryDataV . " is not null AND sf_venda_parcelas.inativo in (0,2)
and " . $queryDataV . " between " . valoresDataHora2($DateBegin, "00:00:00") . " and " . valoresDataHora2($DateEnd, "23:59:59") . " " . $comandoVenda . $queryOperadorV . "
union all
select id_parcela," . $queryDataS . ",numero_filial empresa,sf_bancos.razao_social banco,TD.descricao,sf_fornecedores_despesas.razao_social,cast(comentarios as varchar(200)) historico_baixa,
case when 1 = " . $tpData . " then valor_pago else valor_parcela end valor_pago,
sf_contas_movimento.tipo tipo, 0 id_trans, sf_solicitacao_autorizacao_parcelas.somar_rel,sf_contas_movimento.descricao 
from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas 
left join sf_bancos on sf_bancos.id_bancos = sf_solicitacao_autorizacao_parcelas.id_banco
left join sf_filiais on sf_filiais.id_filial = sf_solicitacao_autorizacao.empresa
inner join sf_tipo_documento TD on sf_solicitacao_autorizacao.tipo_documento = TD.id_tipo_documento
where sf_solicitacao_autorizacao.status = 'Aprovado' AND sf_solicitacao_autorizacao.empresa in (" . $comando2 . ") and " . $queryDataS . " is not null AND sf_solicitacao_autorizacao_parcelas.inativo = '0' " . $queryOperadorS . "
and " . $queryDataS . " between '" . $DateBegin . " 00:00:00 ' and " . valoresDataHora2($DateEnd, "23:59:59") . " and (id_banco in (" . $comando . ") or id_banco is null) 
) as x group by cast(data_pagamento as date);";
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($RFP = odbc_fetch_array($cur)) {
    $key = array_search(escreverData($RFP['data_vencimento']), $aux);
    if (is_numeric($key)) {
        $myArray[$key]['receitas'] = $RFP['receitas'];
        $myArray[$key]['despesas'] = $RFP['despesas'];
    }
}
$count = count($myArray);
for ($i = 0; $i < $count; $i++) {
    $row = array();
    $row[] = $myArray[$i]['dia'];
    $row[] = formataNumeros($myArray[$i]['receitas']);
    $row[] = formataNumeros($myArray[$i]['despesas']);
    $row[] = formataNumeros($myArray[$i]['receitas'] - $myArray[$i]['despesas']);
    $output['aaData'][] = $row;
}

if (isset($_GET['imp']) && $_GET['imp'] == 2) {
    $_GET['pdf'] = "S";
    $_GET['ex'] = 1;
    require_once(__DIR__ . '/../../util/excel/exportaExcel.php');
    $topo = '<tr><td><b>DATA</b></td><td><b>RECEITAS</b></td><td><b>DESPESAS</b></td><td><b>SALDO</b></td></tr>';
    exportaExcel($output['aaData'], $topo);
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
