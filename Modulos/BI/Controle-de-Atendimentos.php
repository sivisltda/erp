<?php
include "../../Connections/configini.php";
$DateBegin = getData("B");
$DateEnd = getData("E");
$tp = "0";
$tipoChamado = "";
$tipo2 = "null";
$forma = "0";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <style>
            #script { width:100%; position:relative; font-family:"Arial"; font-weight:bold; color:#7b8a90; }
            #scroll { width:100%; overflow-y:scroll; }			
            .txt_head { margin-left: 10px; font-size: 17px; font-weight: bold; }
            .txt_text { margin: 0 0 5px 10px; }
            .txt_text b { color: #FF0000; }			
            .despesas { width: calc(100% - 17px) !important; position: absolute; top:86px; }
            .receitas { width: calc(100% - 17px) !important; position: absolute; top:56px; }			
            .cat-t { width:100%; font-size:12px; text-align:center; }
            .cat-s { width:100%; font-size:12px; text-align:center; border-bottom:1px solid #afafaf; background:#bdbdbd; }
            .cat-1 { width:100%; font-size:12px; text-align:center; border-bottom:1px solid #d4d4d4; }			
            .cat-t .col-e { float:left; width:calc(37% - 94.5px); height:19px; padding-top:6px; text-align:right; }
            .cat-s .col-e { float:left; width:calc(37% - 104.5px); line-height:40px; background:#d2d2d2; border-right:10px solid #bdbdbd; }
            .cat-1 .col-e { float:left; width:calc(35.5% - 98px); line-height:30px; background:#e8e8e8; border-right:10px solid #d4d4d4; text-align:left; padding-left:1.5%; cursor:pointer; }			
            .cat-t .col-d { float:left; width:95px; height:19px; padding-top:6px; text-align:left; }
            .cat-s .col-d { float:left; width:34px; height:36px; background:#d2d2d2; border-left:10px solid #bdbdbd; padding-top:4px; font-size:10px; }
            .cat-1 .col-d { float:left; width:34px; line-height:30px; background:#e8e8e8; border-left:10px solid #d4d4d4; }			
            .cat-t .col-1 { float:left; width:calc(4.5% - 0.75px); height:25px; line-height:25px; }
            .cat-s .col-1 { float:left; width:calc(4.5% - 1.75px); line-height:40px; background:#c7c7c7; border-right:1px solid #bdbdbd; }
            .cat-1 .col-1 { float:left; width:calc(4.5% - 1px); line-height:30px; border-right:1px solid #d4d4d4; cursor:pointer; } 
            .dia_normal { background-color: white; color: #7b8a90;}
            .dia_zerado { background-color: #D53F26; color: white;}                                    
            .dia_fds { background-color: #009AD7; color: white;}
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">      
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1>SIVIS B.I.<small>Controle de Atendimentos</small></h1>                    
                    </div>
                    <div class="row-fluid">
                        <div class="boxhead">
                            <div class="boxtext">Controle de Atendimentos</div>
                        </div>
                        <div class="boxtable">
                            Tipo: <select id="txt_tipo" name="txt_tipo" style="width: 160px;">
                                <option value="" <?php
                                if ($tp == "") {
                                    echo "selected";
                                }
                                ?>>Todos</option>
                                <option value="0" <?php
                                if ($tp == "0") {
                                    echo "selected";
                                }
                                ?>>Atendimentos</option>
                                <option value="1" <?php
                                if ($tp == "1") {
                                    echo "selected";
                                }
                                ?>>Serviços</option>
                            </select>
                            <select id="txtTipo" name="txtTipo" style="width: 113px;">
                                <option value="null">Todos</option>
                                <option value="0" <?php
                                if ($tipo2 == '0') {
                                    echo "SELECTED";
                                }
                                ?>>Fechados</option>
                                <option value="1" <?php
                                if ($tipo2 == '1') {
                                    echo "SELECTED";
                                }
                                ?>>Abertos</option>
                            </select>                            
                            <select id="txtTipoChamado" style="width: 120px;">
                                <option value="null">Todos</option>
                                <option value="T" <?php
                                if ($tipoChamado == "T") {
                                    echo "SELECTED";
                                }
                                ?>>Telefone</option>
                                <option value="E" <?php
                                if ($tipoChamado == "E") {
                                    echo "SELECTED";
                                }
                                ?>>Email</option>
                                <option value="V" <?php
                                if ($tipoChamado == "V") {
                                    echo "SELECTED";
                                }
                                ?>>Visita</option>
                                <option value="S" <?php
                                if ($tipoChamado == "S") {
                                    echo "SELECTED";
                                }
                                ?>>SMS</option>
                                <option value="P" <?php
                                if ($tipoChamado == "P") {
                                    echo "SELECTED";
                                }
                                ?>>Presencial</option>                                                                    
                                <option value="W" <?php
                                if ($tipoChamado == "W") {
                                    echo "SELECTED";
                                }
                                ?>>Whatsapp</option>
                                <option value="O" <?php
                                if ($tipoChamado == "O") {
                                    echo "SELECTED";
                                }
                                ?>>Outro</option>
                                <option value="R" <?php
                                if ($tipoChamado == "R") {
                                    echo "SELECTED";
                                }
                                ?>>Portal</option>
                            </select>
                            <select id="txtForma" name="txtForma" style="width: 100px;">
                                <option value="null" <?php
                                if ($forma == "null") {
                                    echo "SELECTED";
                                }
                                ?>>Todos</option>
                                <option value="1" <?php
                                if ($forma == "1") {
                                    echo "SELECTED";
                                }
                                ?>>Agrupados</option>
                            </select>                            
                            <select id="txtTipoData" style="width: 110px;">
                                <option value="0" <?php
                                if ($tipoData == "0") {
                                    echo "SELECTED";
                                }
                                ?>>Dt.Chamado</option>
                                <option value="1" <?php
                                if ($tipoData == "1") {
                                    echo "SELECTED";
                                }
                                ?>>Dt.Prazo</option>
                            </select> De <span id="sprytextfield1"><input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial"></span> 
                            até <span id="sprytextfield2"><input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final"></span>
                            <input type="button" title="Buscar" class="button button-turquoise btn-primary" style="line-height:17px" name="btnfind" id="btnfind" onclick="refresh()" value="Buscar">
                            <hr style="margin: 10px 0">
                            <div class="row-fluid" style="float:none">
                                <div class="span12">
                                    <div id="script">
                                        <div id="datas" class="cat-t"></div>
                                        <div id="cabecalho" class="cat-s"></div>
                                        <div id="scroll"></div>
                                    </div>
                                    <hr style="margin: 10px 0">
                                    <table class="table" cellpadding="0" cellspacing="0" width="100%" id="example">
                                        <thead>
                                            <tr>       
                                                <th width="10%"><center>Data</center></th>
                                                <th width="28%">Cliente</th>
                                                <th width="12%">Tipo</th>
                                                <th width="10%">Atendente</th>
                                                <th width="5%"><center>Pendente</center></th>
                                                <th width="10%">Encaminhado</th>
                                                <th width="25%"><center>Assunto</center></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="8" class="dataTables_empty"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <input id="receitaCol" type="hidden" value="0"/> 
                        </div>                 
                    </div>
                </div>
            </div>
        </div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/uniform/jquery.uniform.min.js'></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script> 
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type='text/javascript' src='js/BI-Controle-de-Atendimentos.js'></script>
    </body>
    <?php odbc_close($con); ?>
</html>
