<?php
include "../../Connections/configini.php";
$DateBegin = getData("B");
$DateEnd = getData("E");

$total_meta = 0;
$Meta_alunos = 0;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/plugins/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css"/>
        <link href="../../js/jBox/jBox.css" rel="stylesheet" type="text/css"/> 
        <link href="../../js/jBox/themes/TooltipBorder.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/plugins/epiechart/jquery.easy-pie-chart.js"></script>
        <style>
            #tbPrincipaisPedidos td:last-child, #tbRanking td:last-child,#tbRanking td:nth-child(4),#tbRanking td:nth-child(5) , #tbAnalitico td:last-child { text-align: right; }
            #tbPrincipaisPedidos td, #tbRanking td{ padding: 5px 10px; }
            #tbAnalitico { width: 100%; border-spacing: 5px; border-collapse: separate;}
            #tbAnalitico td { padding: 15px 5px; text-align: center; font-size: 12px; background: #EEE; }
            #tbAnalitico td { padding-top: 7.5px; padding-bottom: 7.5px; text-align: left; }
            #tbAnalitico td:first-child { font-weight: bold; text-align: center; }
            #tbRanking td:nth-child(3) {text-align: center; }
            .txt_head { margin-left: 10px; font-size: 17px; font-weight: bold; }
            .txt_text { margin: 0 0 5px 10px; }
            .txt_text b { color: #FF0000; }
            .informacao { float: right; margin-right: 10px; }
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php 
                    include("../../top.php"); 
                    include("../../querys_finance_dashboard.php");
                    if (!empty($Status_alunos)) {
                        foreach ($Status_alunos as $status) {
                            if (substr($status["name"], 0, 5) == "Ativo") {
                                $total_meta = $total_meta + $status["value"];
                            }
                        }
                    }
                ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1>SIVIS B.I.<small>Gestão de Vendas</small></h1>
                    </div>
                    <div class="topcont" style="clear: both;display:flex; margin-bottom: 10px;">
                        <div class="tophead" style="width:calc(40% - 1px); border:0;">
                            <div class="topname">Meta de <?php echo $Meta_alunos; ?> Clientes Ativos</div>
                            <div class="topresp">
                                <div class="topnumb" style="width: 100%;">
                                    <div class="indfoot" style="float:left; width:100%; height:40px; padding:5px 0" id="chartdiv4"></div>
                                    <div style="width:70px; height:50px; position:relative; top:-38px; left:50%; margin-left:-35px; color:#FFF; font-size:19px; font-weight:bold; text-align:center">
                                        <?php echo ($Meta_alunos > 0 ? escreverNumero(($total_meta * 100) / $Meta_alunos) : 0)."%"; ?>
                                    </div>
                                </div>
                                <div style="clear:both"></div>
                            </div>
                        </div>
                        <div class="tophead" style="width:calc(15% - 1px)">
                            <div class="topname">Clientes Ativos</div>
                            <div class="topresp">
                                <div class="topnumb"><?php echo $total_meta; ?></div>
                                <div style="clear:both"></div>
                            </div>
                        </div>
                        <?php if ($mdl_seg_ > 0) { ?>
                        <div class="tophead" style="width:calc(15% - 1px)">
                            <div class="topname">Veículos Protegidos</div>
                            <div class="topresp">
                                <div class="topnumb"><?php echo $veiculos; ?></div>
                                <div style="clear:both"></div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="tophead" style="width:calc(15% - 1px)">
                            <div class="topname">Recorrentes</div>
                            <div class="topresp">
                                <div class="topnumb"><span id="totRecorrentes">0,00</span></div>
                                <div style="clear:both"></div>
                            </div>
                        </div>
                        <div class="tophead" style="width:calc(15% - 1px)">
                            <div class="topname">Não Renovados</div>
                            <div class="topresp">
                                <div class="topnumb" style="color: #ff7f50"><?php echo escreverNumero(($renovados + $nrenovados) > 0 ? (($nrenovados * 100)/($renovados + $nrenovados)) : 0); ?>%</div>
                                <div style="clear:both"></div>
                            </div>
                        </div>
                        <div style="clear:both"></div>
                    </div>                    
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="block" style="font-size:13px">
                                <div style="float:left; width: 16%;">
                                    <label>Selecione a Filial: </label>
                                    <select id="txtFilial" name="txtFilial[]" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                        <?php
                                        $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo utf8_encode($RFP['id_filial']); ?>" selected><?php echo utf8_encode($RFP['numero_filial']) . " - " . utf8_encode($RFP['Descricao']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div style="float: left;margin-left: 0.5%;">
                                    <label for="txtVenda">Vendas:</label>
                                    <select id="txtVenda" style="width:90px; height:31px">
                                        <option value="0">Todas</option>
                                        <option value="1" selected>1ª Venda</option>
                                    </select>
                                </div>                                                                                                                                    
                                <div style="float: left;margin-left: 0.5%;">
                                    <label for="txtResponsavel">Tipo:</label>
                                    <select id="txtResponsavel" style="width:100px; height:31px">
                                        <option value="0">Usuários</option>
                                        <option value="1">Indicadores</option>
                                    </select>
                                </div>                                
                                <div style="float: left;margin-left: 0.5%;">
                                    <label for="mscGrupo">Responsáveis:</label>                                                                        
                                    <select style="min-width:240px;min-height:31px!important;" name="itemsGrupo[]" id="mscGrupo" multiple="multiple" class="select" class="input-medium"></select>                                
                                </div>
                                <div style="float: left;margin-left: 0.5%;">
                                    <label for="txt_dt_begin">de</label>
                                    <input type="text" class="datepicker" style="width:80px; height:31px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial"/>
                                </div>                                                                    
                                <div style="float: left;margin-left: 0.5%;">
                                    <label for="txt_dt_end">até</label>
                                    <input type="text" class="datepicker" style="width:80px; height:31px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final"/>                                    
                                </div>                                
                                <button class="button button-turquoise btn-primary" type="button" style="margin-top: 15px;" onclick="refresh()" id="btnfind"><span class="ico-search icon-white"></span></button>
                                <?php if ($ckb_bi_gve_sfin_ == 0) { ?>
                                <span style="float:right; margin-top: 15px;">
                                    <button class="button button-blue btn-primary" type="button" onclick="AbrirBox()"><span class="ico-cog-3 icon-white"></span></button>
                                </span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>                    
                    <div class="box50">
                        <div class="boxhead">
                            <div class="boxtext">
                            <span class="ico-question-sign informacao" title="
                            Quantificação de Pessoas:<br><br>
                            <b>Leads</b>: cadastrados no período<br>
                            <b>Prospects</b>: cadastrados no período<br>
                            <b>Clientes</b>: cadastrados no período ou convertidos de prospects<br>
                            <b>Faturados</b>: Clientes que fizeram algum pagamento<br>
                            "></span>
                            Funil de Vendas                                
                            </div>
                        </div>
                        <div class="boxchart lite" id="funilVendas"></div>
                    </div>
                    <div class="box50" style="margin-left:2.5%">
                        <div class="boxhead">
                            <div class="boxtext">
                            <select id="tpProcedencia" style="float: left; width: 140px; margin-left: 5px; margin-top: 5px;">
                                <option value="L">Leads</option>
                                <option value="P">Prospects</option>
                                <option value="C">Clientes</option>
                                <option value="F" selected>Clientes Faturados</option>
                            </select>
                            <span class="ico-question-sign informacao" title="
                            Procedência dos clientes do<br>
                            <b>Funil de Vendas</b>: <br>
                            De acordo com o filtro.</b>
                            "></span>                                
                            Procedência</div>
                        </div>
                        <div class="boxchart lite" id="procedencia"></div>
                    </div>
                    <div class="box50">
                        <div class="boxhead">
                            <div class="boxtext">
                            <span class="ico-question-sign informacao" title="
                            Valores de Mensalidades p/ filtros:<br><br>
                            <b>Orçamentos</b>: todas as mensalidades de prospects.<br>
                            <b>Faturados</b>: são as mensalidades pagas de clientes.<br>
                            <b>A faturar</b>: são as mensalidades em aberto de clientes.<br>
                            <b>Cancelados</b>: mensalidades com planos cancelados.<br><br>
                            <b>META DE VENDAS CLIENTES</b> – comparativo em %<br>
                            do número de vendas com o índice (Ativo).<br>
                            <b>META DE VENDAS FATURAMENTO</b> - comparativo em %<br>
                            do faturamento no período com o índice (Meta).
                            "></span>        
                            Analítico Total</div>
                        </div>
                        <div class="boxchart lite">
                            <div style="width:calc(100% - 10px); padding:5px 5px 0">
                                <table style="float:none" id="tbAnalitico"></table>
                            </div>
                            <div class="boxed50" id="metaVendas"></div>
                            <div class="boxed50" id="pontoEquilibrio"></div>
                        </div>
                    </div>
                    <div class="box50" style="margin-left:2.5%">
                        <div class="boxhead">
                            <div class="boxtext">
                            <span class="ico-question-sign informacao" title="
                            Valores de Vendas p/ filtros:<br><br>
                            Todas as vendas agrupadas por <b>usuários</b> responsáveis<br>
                            Contabilizando seus totais<br>
                            "></span>
                            Ranking de Vendas Total</div>
                        </div>
                        <div class="boxtable" style="height: 380px;">
                            <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbRanking">
                                <thead>
                                    <tr>
                                        <th width="5%">N°</th>
                                        <th><center>Responsáveis</center></th>
                                        <th width="10%">Vendas</th>
                                        <th width="15%">Maior Venda</th>
                                        <th width="15%">Total Vendido</th>
                                        <th width="15%">Ticket Médio</th>   
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>                    
                    <div style="clear:both"></div>
                    <div class="box50">
                        <div class="boxhead">
                            <div class="boxtext">
                            <span class="ico-question-sign informacao" title="
                            Cadastros de Leads p/ filtros:<br><br>
                            Considera todos os cadastros que <b>iniciaram</b> a interação pelo cadastro de Leads,<br>
                            demonstrando quantos foram <b>convertidos</b> e quantos estão <b>aguardando</b>.<br>
                            "></span>                                
                            Leads</div>
                        </div>
                        <div class="boxchart lite" id="chartBar"></div>
                    </div>
                    <div class="box50" style="margin-left:2.5%">
                        <div class="boxhead">
                            <div class="boxtext">
                            <span class="ico-question-sign informacao" title="
                            Cadastros de Leads p/ filtros:<br><br>
                            Considera todos os cadastros que <b>iniciaram</b> a interação pelo cadastro de Leads,<br>
                            demonstrando todos <b>convertidos</b> e <b>aguardando</b>.<br>
                            Agrupados pelos usuários responsáveis.<br>
                            "></span>                                
                            Top 10 Indicadores (Leads)</div>
                        </div>
                        <div class="boxchart lite" id="procedenciaLead"></div>
                    </div> 
                    <div style="clear:both"></div>                    
                </div>
            </div>
        </div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/moment.min.js"></script>        
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/amcharts.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/serial.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/pie.js"></script>        
        <script type="text/javascript" src="../../js/plugins/amcharts_new/funnel.js"></script>
        <script type="text/javascript" src="../../js/plugins/amcharts_new/gauge.js"></script>
        <script type='text/javascript' src="../../js/numeral.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/jBox/jBox.min.js"></script>      
        <script type='text/javascript' src='../../js/util.js'></script>        
        <script type="text/javascript" src="../../js/plugins/amcharts/themes/light.js"></script>        
        <script type='text/javascript' src='./js/BI-Gestao-de-Vendas.js'></script>
        <script type="text/javascript">
            var chart4 = AmCharts.makeChart("chartdiv4", {
                "type": "serial",
                "theme": "light",
                "dataProvider": [{"year": "2003", "europe": <?php echo $total_meta; ?>, "namerica": <?php echo ($Meta_alunos - $total_meta > 0 ? $Meta_alunos - $total_meta : 0); ?>, "asia": <?php echo ($Meta_alunos > 0 ? ($Meta_alunos * 1.2 - $Meta_alunos) : 0); ?>}],
                "valueAxes": [{
                    "stackType": "100%",
                    "axisAlpha": 0,
                    "gridAlpha": 0,
                    "labelsEnabled": false,
                    "position": "left"
                }],
                "graphs": [{
                    "showBalloon": false,
                    "fillAlphas": 1,
                    "title": "Europe",
                    "type": "column",
                    "fillColors": ["#0C5D4B"],
                    "lineColor": ["#0C5D4B"],
                    "valueField": "europe"
                }, {
                    "showBalloon": false,
                    "fillAlphas": 1,
                    "title": "North America",
                    "type": "column",
                    "fillColors": ["#149B7E"],
                    "lineColor": ["#149B7E"],
                    "valueField": "namerica"
                }, {
                    "showBalloon": false,
                    "fillAlphas": 1,
                    "title": "Asia-Pacific",
                    "type": "column",
                    "fillColors": ["#72C3B1"],
                    "lineColor": ["#72C3B1"],
                    "valueField": "asia"
                }],                                        
                "rotate": true,
                "showBalloon": false,
                "marginTop": 5,
                "marginLeft": 10,
                "marginRight": 10,
                "marginBottom": 5,
                "autoMargins": false,
                "categoryField": "year",                                                              
                "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 0,
                    "gridAlpha": 0
                }
            });
            
            new jBox('Tooltip', {
                attach: '.informacao',
                animation: 'pulse',
                theme: 'TooltipBorder',
                addClass: 'tooltipMU',
                content: ''
            });
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>