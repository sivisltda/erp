<?php

require_once(__DIR__ . '/../../Connections/configini.php');
require_once(__DIR__ . '/../../util/util.php');

$DateBegin = getData("B");
$DateEnd = getData("E");
$excel = "";
$comando = "";
$comandoX = "";
$i = 0;

if ($_GET['dti'] != "" && $_GET['dtf'] != "") {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
    $comando .= " and dt_inicio_mens between " . valoresData2($DateBegin) . " and " . valoresData2($DateEnd);
}

if ($_GET['tipo'] == "renov" && $_GET['ts'] == 1) {
    $comando .= " and dt_pagamento_mens is not null";
} else if ($_GET['tipo'] == "nrenv" && $_GET['ts'] == 1) {    
    $comando .= " and dt_pagamento_mens is null";
}

if (isset($_GET['grp'])) {
    $comandoX .= " and sf_produtos.conta_movimento in (" . str_replace("\"", "'", str_replace(array("[", "]"), "", $_GET["grp"])) . ")";
}

if (is_numeric($_GET['rep'])) {
    $comandoX .= " AND sf_fornecedores_despesas.id_user_resp = " . $_GET['rep'];
}

if (isset($_GET['fil'])) {
    $comandoX .= " and sf_fornecedores_despesas.empresa in (" . str_replace(array("[", "]", "\"", "'"), "", $_GET["fil"]) . ")";
} else {
    $comandoX .= " and sf_fornecedores_despesas.empresa in (select id_filial_f from sf_usuarios_filiais where id_usuario_f = " . $_SESSION["id_usuario"] . ")";        
}

$query .= "set dateformat dmy;";
if ($_GET['ts'] == 0) {
    $query .= "select sum(case when dt_pagamento_mens is not null and quantidade > 0 then (valor_total/quantidade) else 0 end) renovados,
    sum(case when dt_pagamento_mens is null then valor_mens else 0 end) nrenovados
    from sf_vendas_planos_mensalidade m inner join sf_vendas_planos on id_plano = id_plano_mens
    left join sf_vendas_itens on id_item_venda = id_item_venda_mens
    inner join sf_fornecedores_despesas on id_fornecedores_despesas = favorecido
    inner join sf_produtos on sf_produtos.conta_produto = id_prod_plano	
    where id_fornecedores_despesas > 0 and sf_fornecedores_despesas.tipo = 'C' and inativo = 0 and dt_cancelamento is null and planos_status not in ('Novo') " . $comandoX . $comando;
} else if ($_GET['ts'] == 1) {
    $query .= "select sf_fornecedores_despesas.id_fornecedores_despesas,razao_social,data_nascimento,dt_inicio_mens dt_inicio,dt_fim_mens dt_fim, valor_mens, sf_produtos.descricao,planos_status status, placa,
    (select top 1 case when nome = '' then login_user else nome end login_user from sf_usuarios c where c.inativo = 0 and c.id_usuario = sf_fornecedores_despesas.id_user_resp) estado,            
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'telefone',    
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'celular',
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'email'
    from sf_vendas_planos_mensalidade m inner join sf_vendas_planos on id_plano = id_plano_mens
    inner join sf_fornecedores_despesas on id_fornecedores_despesas = favorecido 
    inner join sf_produtos on sf_produtos.conta_produto = id_prod_plano
    left join sf_fornecedores_despesas_veiculo on id = id_veiculo
    where sf_fornecedores_despesas.id_fornecedores_despesas > 0 and sf_fornecedores_despesas.tipo = 'C' and inativo = 0 and dt_cancelamento is null and planos_status not in ('Novo') " . $comandoX . $comando ." order by 2";
}
//echo $query; exit;
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => 0,
    "iTotalDisplayRecords" => 0,
    "aaData" => array()
);

$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $row = array();    
    if ($_GET['ts'] == 0) {
        $row[] = "<span onclick=\"filterStatusChurn('todas')\">Valor de planos Vencendo</span>";
        $row[] = escreverNumero($RFP['renovados'] + $RFP['nrenovados']);
        $output['aaData'][] = $row;
        $row = array();
        $row[] = "<span onclick=\"filterStatusChurn('renov')\">Valor de planos Renovados</span>";
        $row[] = escreverNumero($RFP['renovados']);
        $output['aaData'][] = $row;
        $row = array();
        $row[] = "<span onclick=\"filterStatusChurn('nrenv')\">Valor de planos Não-Renovados</span>";
        $row[] = escreverNumero($RFP['nrenovados']);
    } else if ($_GET['ts'] == 1) {
        $row[] = utf8_encode($RFP['id_fornecedores_despesas']);
        $row[] = "<a href=\"javascript:void(0)\" onclick=\"AbrirBox(2," . $RFP['id_fornecedores_despesas'] . ")\">" . utf8_encode($RFP['razao_social']) . "</a>";
        $row[] = escreverData($RFP['dt_inicio']) . " até " . escreverData($RFP['dt_fim']);
        $row[] = escreverNumero($RFP['valor_mens']);
        $row[] = utf8_encode($RFP['status']);
        $row[] = utf8_encode($RFP['descricao']) . (strlen($RFP['placa']) > 0 ? " [" . $RFP['placa'] . "]" : "");
        $row[] = formatNameCompact(utf8_encode($RFP['estado']));
        $row[] = utf8_encode($RFP['telefone']);        
        $row[] = ($RFP['celular'] != "" ? "<a href=\"javascript:void(0)\" onclick=\"abrirTelaSMS('" . utf8_encode($RFP['celular']) . "',true," . $RFP['id_fornecedores_despesas'] . ")\">" . utf8_encode($RFP['celular']) . "</a>" : "");                
        $row[] = ($RFP['email'] != "" ? "<a href=\"javascript:void(0)\" onclick=\"abrirTelaEmail('" . utf8_encode($RFP['email']) . "'," . $RFP['id_fornecedores_despesas'] . ")\">" . utf8_encode($RFP['email']) . "</a>" : "");
    }
    $output['aaData'][] = $row;    
    $i++;
}

$output['iTotalRecords'] = $i;
$output['iTotalDisplayRecords'] = $i;

if (!isset($_GET['isPdf']) && !isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);