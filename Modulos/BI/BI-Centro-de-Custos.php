<?php
include "../../Connections/configini.php";

$DateBegin = getData("B");
$DateEnd = getData("E");
if ($_GET["dti"] != "") {
    $DateBegin = str_replace("_", "/", $_GET["dti"]);
}
if ($_GET["dtf"] != "") {
    $DateEnd = str_replace("_", "/", $_GET["dtf"]);
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../SpryAssets/SpryValidationTextField.css"/>
        <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
        <style>
            #tbReceita td:last-child, #tbDespesas td:last-child { text-align: right; }
            #tbReceita td, #tbDespesas td { padding: 5px 10px; }
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"><span class="ico-arrow-right"></span></div>
                        <h1>SIVIS B.I.<small>Centro de Custos</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="block" style="font-size:13px">
                                de <input type="text" class="datepicker data-mask" style="width:80px; height:31px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial"/>
                                até <input type="text" class="datepicker data-mask" style="width:80px; height:31px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final"/>
                                <button class="button button-turquoise btn-primary" type="button" onclick="AbrirBox(0)" id="btnfind"><span class="ico-search icon-white"></span></button>
                            </div>
                        </div>
                    </div>
                    <div class="box50">
                        <div class="boxhead">
                            <div class="boxtext">Receitas - Top 15</div>
                        </div>
                        <div class="boxchart" id="chartdiv"></div>
                    </div>
                    <div class="box50" style="margin-left:2.5%">
                        <div class="boxhead">
                            <div class="boxtext">Despesas - Top 15</div>
                        </div>
                        <div class="boxchart" id="chartdiv2"></div>
                    </div>
                    <div class="box50">
                        <div class="boxhead">
                            <div class="boxtext">Receitas (Contas Recebidas) - Top 15</div>
                        </div>
                        <div class="boxtable">
                            <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbReceita">
                                <thead>
                                    <tr>
                                        <th width="80%"><center>Descrição</center></th>
                                        <th width="20%">Valor Pago</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>TOTAL</th>
                                        <th><div id="txtchartdiv">R$ 0,00</div></th>
                                    </tr>
                                </tfoot>                                
                            </table>
                        </div>
                    </div>
                    <div class="box50" style="margin-left:2.5%">
                        <div class="boxhead">
                            <div class="boxtext">Despesas (Contas Pagas) - Top 15</div>
                        </div>
                        <div class="boxtable">
                            <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="tbDespesas">
                                <thead>
                                    <tr>
                                        <th width="80%"><center>Descrição</center></th>
                                        <th width="20%">Valor Pago</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>TOTAL</th>
                                        <th><div id="txtchartdiv2">R$ 0,00</div></th>
                                    </tr>
                                </tfoot>                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type='text/javascript' src='../../js/plugins/maskedinput/jquery.maskedinput-1.3.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/charts.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="amcharts/amcharts.js"></script>
        <script type="text/javascript" src="amcharts/pie.js"></script>
        <script type="text/javascript" src="amcharts/themes/light.js"></script>
        <script type='text/javascript' src="../../js/numeral.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type='text/javascript' src='../../js/util.js'></script>          
        <script type="text/javascript">
            $('.data-mask').mask(lang["dateMask"]);
            function AbrirBox() {
                var retPrint = "";
                if ($("#txt_dt_begin").val() !== "") {
                    retPrint = retPrint + "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "_");
                }
                if ($("#txt_dt_end").val() !== "") {
                    retPrint = retPrint + "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "_");
                }
                window.location = "BI-Centro-de-Custos.php?imp=0" + retPrint;
            }

            function finalFind(tp) {
                var retPrint = "";
                if ($("#txt_dt_begin").val() !== "") {
                    retPrint = retPrint + "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "_");
                }
                if ($("#txt_dt_end").val() !== "") {
                    retPrint = retPrint + "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "_");
                }
                return "BI-Centro-de-Custos_server_processing.php?tp=" + tp + "&filial=" + $("#txtLojaSel").val() + retPrint;
            }

            $(document).ready(function () {
                $("#tbReceita").dataTable({
                    bPaginate: false, bFilter: false, bInfo: false, bSort: false,
                    "bProcessing": false,
                    "bServerSide": true,
                    "sAjaxSource": finalFind(0),
                    "fnDrawCallback": function (data) {
                        if (data["aoData"].length > 0) {
                            criaGrafico(data["aoData"], "chartdiv");
                        }
                    }
                });
                $("#tbDespesas").dataTable({
                    bPaginate: false, bFilter: false, bInfo: false, bSort: false,
                    "bProcessing": false,
                    "bServerSide": true,
                    "sAjaxSource": finalFind(1),
                    "fnDrawCallback": function (data) {
                        if (data["aoData"].length > 0) {
                            criaGrafico(data["aoData"], "chartdiv2");
                        }
                    }
                });
            });

            function criaGrafico(data, grafico) {
                var chartData = [];
                var i = 0;
                var total = parseFloat(0);
                var totalOutros = parseFloat(0);
                for (var i = 0; i < data.length; i++) {
                    if (i < 4) {
                        chartData[i] = {desc: data[i]["_aData"][0], valor: textToNumber(data[i]["_aData"][1])};
                    } else {
                        totalOutros = totalOutros + textToNumber(data[i]["_aData"][1]);
                        chartData[4] = {desc: "Demais Despesas", valor: totalOutros};
                    }
                    total = total + textToNumber(data[i]["_aData"][1]);
                }
                $("#txt" + grafico).html(lang["prefix"] + " " + numberFormat(total));
                console.log(chartData);
                var chart = AmCharts.makeChart(grafico, {
                    "type": "pie",
                    "theme": "light",
                    "dataProvider": chartData,
                    "valueField": "valor",
                    "titleField": "desc",
                    "numberFormatter": {"precision": 2, "decimalSeparator": lang["centsSeparator"], "thousandsSeparator": lang["thousandsSeparator"]},
                    pullOutRadius: 30,
                    autoMargins: false,
                    "labelText": "[[percents]]%",
                    "balloonText": "[[title]]<br/>" + lang["prefix"] + " [[value]]"
                });
            }
        </script>
    </body>
    <?php odbc_close($con); ?>
</html>