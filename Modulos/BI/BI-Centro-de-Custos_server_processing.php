<?php

include './../../Connections/configini.php';
$count = 0;
$totalOutros = 0;
$empresa = "1";
$query = "set dateformat dmy;";

if ($_GET['dti'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
} else {
    $DateBegin = getData("B");
}

if ($_GET['dtf'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
} else {
    $DateEnd = getData("E");
}

if (is_numeric($_GET['filial'])) {
    $empresa = $_GET['filial'];
}

if (is_numeric($_GET['tp'])) {
    if ($_GET['tp'] == "0") {
        $query .= "select y.cm descricao, sum(y.valor_pago) valor from (
        select *, ROW_NUMBER() OVER ( ORDER BY Vecto desc ) as row from (select 'D-' +cast(sf_lancamento_movimento_parcelas.id_parcela as VARCHAR) as pk ,
        data_vencimento 'Vecto',pa ,sf_lancamento_movimento.empresa empresa,razao_social,historico historico_baixa, 'D' Origem , sf_contas_movimento.descricao 'cm', 
        valor_parcela, valor_pago,sf_grupo_contas.descricao gc,sf_lancamento_movimento_parcelas.inativo inativo, 
        sf_lancamento_movimento_parcelas.sa_descricao documento,obs_baixa,isnull(bol_id_banco,0) bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,
        id_fornecedores_despesas,bol_nosso_numero,data_pagamento,data_cadastro,bol_data_parcela,bol_valor,sf_fornecedores_despesas.tipo, 
        (select max(chq_id) cheque from sf_cheques ci where ci.codigo_relacao = sf_lancamento_movimento_parcelas.id_parcela and ci.tipo = 'C') cheque 
        from sf_lancamento_movimento_parcelas 
        inner join sf_lancamento_movimento on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento 
        inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento 
        inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas 
        left join sf_tipo_documento on sf_lancamento_movimento_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento 
        inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas 
        where sf_contas_movimento.tipo = 'C' and sf_lancamento_movimento.status = 'Aprovado' AND valor_pago > 0 and sf_tipo_documento.id_tipo_documento not in (12) 
        AND data_pagamento >= '" . $DateBegin . " 00:00:00' AND data_pagamento <= '" . $DateEnd . " 23:59:59' AND sf_lancamento_movimento_parcelas.inativo = '0' 
        AND sf_lancamento_movimento.empresa = " . $empresa . "  
        union 
        select 'A-' +cast(sf_solicitacao_autorizacao_parcelas.id_parcela as VARCHAR) as pk ,data_parcela 'Vecto',pa ,sf_solicitacao_autorizacao.empresa empresa,razao_social, 
        historico_baixa,'A' Origem, sf_contas_movimento.descricao 'cm', valor_parcela, valor_pago,
        sf_grupo_contas.descricao gc,sf_solicitacao_autorizacao_parcelas.inativo inativo,sf_solicitacao_autorizacao_parcelas.sa_descricao documento,obs_baixa,
        isnull(bol_id_banco,0) bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,bol_nosso_numero,data_pagamento,data_cadastro,
        bol_data_parcela,bol_valor,sf_fornecedores_despesas.tipo, (select max(chq_id) cheque from sf_cheques ci where ci.codigo_relacao = sf_solicitacao_autorizacao_parcelas.id_parcela and ci.tipo = 'S') cheque 
        from sf_solicitacao_autorizacao_parcelas 
        inner join sf_solicitacao_autorizacao on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao 
        inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento 
        inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas 
        left join sf_tipo_documento on sf_solicitacao_autorizacao_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento 
        inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas 
        where sf_contas_movimento.tipo = 'C' and status = 'Aprovado' AND valor_pago > 0 and sf_tipo_documento.id_tipo_documento not in (12) 
        AND data_pagamento >= '" . $DateBegin . " 00:00:00' AND data_pagamento <= '" . $DateEnd . " 23:59:59' AND sf_solicitacao_autorizacao_parcelas.inativo = '0' 
        AND sf_solicitacao_autorizacao.empresa = " . $empresa . " 
        union 
        select 'V-' +cast(sf_venda_parcelas.id_parcela as VARCHAR) as pk ,data_parcela 'Vecto',pa ,
        sf_vendas.empresa empresa,razao_social, sf_vendas.historico_venda historico_baixa,'V' Origem, sf_contas_movimento.descricao cm,valor_parcela,
        valor_pago,sf_grupo_contas.descricao gc,sf_venda_parcelas.inativo inativo,sf_venda_parcelas.sa_descricao documento,obs_baixa,bol_id_banco,
        sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,bol_nosso_numero,data_pagamento,data_cadastro,bol_data_parcela,
        bol_valor,sf_fornecedores_despesas.tipo, 
        (select max(chq_id) cheque from sf_cheques ci where ci.codigo_relacao = sf_venda_parcelas.id_parcela and ci.tipo = 'V') cheque 
        from sf_venda_parcelas 
        inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda 
        inner join sf_contas_movimento on sf_vendas.conta_movimento = sf_contas_movimento.id_contas_movimento 
        inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas 
        left join sf_tipo_documento on sf_venda_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento 
        inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas 
        where sf_vendas.cov = 'V' and status = 'Aprovado' AND valor_pago > 0 and sf_tipo_documento.id_tipo_documento not in (12) 
        AND data_pagamento >= '" . $DateBegin . " 00:00:00' AND data_pagamento <= '" . $DateEnd . " 23:59:59' AND sf_venda_parcelas.inativo = '0' and sf_vendas.empresa = " . $empresa . "
        ) as x where pk is not null) as y group by y.cm order by valor desc";
    } elseif ($_GET['tp'] == "1") {
        $query .= "select y.cm descricao, sum(y.valor_pago) valor from (
        select *, ROW_NUMBER() OVER ( ORDER BY Vecto desc ) as row from (
        select 'D-' +cast(sf_lancamento_movimento_parcelas.id_parcela as VARCHAR) as pk ,data_vencimento 'Vecto',pa ,sf_lancamento_movimento.empresa empresa,razao_social, 
        historico_baixa, 'D' Origem , sf_contas_movimento.descricao 'cm', valor_parcela,valor_pago,sf_grupo_contas.descricao gc,sf_lancamento_movimento_parcelas.inativo inativo,
        sf_lancamento_movimento_parcelas.sa_descricao documento,obs_baixa,bol_id_banco,sf_tipo_documento.abreviacao,id_fornecedores_despesas,data_pagamento,data_cadastro 
        from sf_lancamento_movimento_parcelas 
        inner join sf_lancamento_movimento on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento 
        inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento 
        left join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas 
        left join sf_tipo_documento on sf_lancamento_movimento_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento 
        inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas 
        where sf_contas_movimento.tipo = 'D' and sf_lancamento_movimento.status = 'Aprovado' AND valor_pago > 0 AND data_pagamento >= '" . $DateBegin . " 00:00:00' 
        AND data_pagamento <= '" . $DateEnd . " 23:59:59' AND sf_lancamento_movimento_parcelas.inativo = '0' AND sf_lancamento_movimento.empresa = " . $empresa . " 
        union 
        select 'A-' +cast(sf_solicitacao_autorizacao_parcelas.id_parcela as VARCHAR) as pk ,data_parcela 'Vecto',pa ,sf_solicitacao_autorizacao.empresa empresa,razao_social, 
        historico_baixa,'A' Origem, sf_contas_movimento.descricao 'cm', valor_parcela,valor_pago,sf_grupo_contas.descricao gc,sf_solicitacao_autorizacao_parcelas.inativo inativo,
        sf_solicitacao_autorizacao_parcelas.sa_descricao documento,obs_baixa,bol_id_banco,sf_tipo_documento.abreviacao,id_fornecedores_despesas,data_pagamento,data_cadastro 
        from sf_solicitacao_autorizacao_parcelas 
        inner join sf_solicitacao_autorizacao on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao 
        inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento 
        left join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas 
        left join sf_tipo_documento on sf_solicitacao_autorizacao_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento 
        inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas 
        where sf_contas_movimento.tipo = 'D' and status = 'Aprovado' AND valor_pago > 0 AND data_pagamento >= '" . $DateBegin . " 00:00:00' 
        AND data_pagamento <= '" . $DateEnd . " 23:59:59' AND sf_solicitacao_autorizacao_parcelas.inativo = '0' AND sf_solicitacao_autorizacao.empresa = " . $empresa . "
        union 
        select 'V-' +cast(sf_venda_parcelas.id_parcela as VARCHAR) as pk ,data_parcela 'Vecto',pa ,sf_vendas.empresa empresa,razao_social, historico_venda,'V' Origem, 
        sf_contas_movimento.descricao cm,valor_parcela,valor_pago,sf_grupo_contas.descricao gc,sf_venda_parcelas.inativo inativo,sf_venda_parcelas.sa_descricao documento,
        obs_baixa,bol_id_banco,sf_tipo_documento.abreviacao,id_fornecedores_despesas,data_pagamento,data_cadastro 
        from sf_venda_parcelas 
        inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda 
        inner join sf_contas_movimento on sf_vendas.conta_movimento = sf_contas_movimento.id_contas_movimento 
        inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas 
        left join sf_tipo_documento on sf_venda_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento 
        inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas 
        where sf_vendas.cov = 'C' and status = 'Aprovado' AND valor_pago > 0 AND data_pagamento >= '" . $DateBegin . " 00:00:00' 
        AND data_pagamento <= '" . $DateEnd . " 23:59:59' AND sf_venda_parcelas.inativo = '0' AND sf_vendas.empresa = " . $empresa . "
        ) as x where pk is not null) as y group by y.cm order by valor desc";
    }
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => 0,
    "iTotalDisplayRecords" => 0,
    "aaData" => array()
);

$cur2 = odbc_exec($con, $query);
while ($ASR = odbc_fetch_array($cur2)) {
    if ($count < 14) {
        $row = array();
        $row[] = utf8_encode($ASR["descricao"]);
        $row[] = escreverNumero($ASR["valor"], 1);
        $output['aaData'][] = $row;
        $count++;
    } else {
        $totalOutros += $ASR["valor"];
    }
}
if ($totalOutros > 0) {
    $row = array();
    $row[] = $_GET['tp'] == "0" ? "DEMAIS RECEITAS" : "DEMAIS DESPESAS";
    $row[] = escreverNumero($totalOutros, 1);
    $output['aaData'][] = $row;
    $count++;
}
$output["iTotalRecords"] = $count;
$output["iTotalDisplayRecords"] = $count;

echo json_encode($output);
odbc_close($con);
