<?php

include '../../Connections/configini.php';
$local = array();
$tipo = "";

if (isset($_GET["PorGrupo"])) {
    $trocar = array("[", "]");
    $trocar2 = array('"');
    $grupos = str_replace($trocar, "", $_GET["groups"]);
    $grupos2 = str_replace($trocar2, "'", $grupos);
    $query = "select conta_produto, descricao from dbo.sf_produtos where conta_produto > 0 and conta_movimento in (" . $grupos2 . ")";
    $cur = odbc_exec($con, $query) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('conta_produto' => $RFP['conta_produto'], 'descricao' => utf8_encode($RFP['descricao']));
    }
} else {
    switch ($_REQUEST['txtTipo']) {
        case "1":
            $tipo = "P";
            break;
        case "2":
            $tipo = "S";
            break;
        case "3":
            $tipo = "C";
            break;
    }
    if ($tipo != "") {
        $query = "select conta_produto, descricao from dbo.sf_produtos where conta_produto > 0 and tipo = '" . $tipo . "' and inativa = 0 order by descricao";
        $cur = odbc_exec($con, $query) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $local[] = array('conta_produto' => $RFP['conta_produto'], 'descricao' => utf8_encode($RFP['descricao']));
        }
    }
}

echo(json_encode($local));
odbc_close($con);
