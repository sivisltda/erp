<?php

if (!isset($_GET['pdf'])) {
    require_once(__DIR__ . '/../../../Connections/configini.php');
    require_once(__DIR__ . '/../../../util/util.php');
}
require_once(__DIR__ . '/../../../util/excel/exportaExcel.php');


if ($_GET['tp'] == 'F') {
    $query .= "select id_fornecedores_despesas,razao_social from dbo.sf_fornecedores_despesas
    where razao_social like '%" . $_GET['q'] . "%'";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row['id'] = $RFP['id_fornecedores_despesas'];
        $row['value'] = utf8_encode($RFP['razao_social']);
        $todos[] = $row;
    }
    echo json_encode($todos);
    exit();
}

$sQtd = 0;
$sLimit = 20;
$iTotal = 0;
$imprimir = 0;
$sOrder = " ORDER BY data desc ";
$query = "set dateformat dmy;";
$where = "";

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['tp'])) {
    if ($_GET['tp'] == "0") {
        $where .= " and acao = 'D' and tabela = 'sf_vendas' ";
    } elseif ($_GET['tp'] == "1") {
        $where .= " and acao = 'E' and tabela = 'sf_vendas' ";
    } elseif ($_GET['tp'] == "2") {
        $where .= " and acao = 'R' and tabela = 'sf_vendas_planos_mensalidade' ";
    } elseif ($_GET['tp'] == "3") {
        $where .= " and acao = 'A' and tabela = 'sf_vendas_planos_mensalidade' ";
    } elseif ($_GET['tp'] == "4") {
        $where .= " and acao = 'V' and tabela = 'sf_vendas' and descricao like 'VENDA  DTPAG:%' ";
    } elseif ($_GET['tp'] == "19") {
        $where .= " and acao = 'V' and tabela = 'sf_vendas' and descricao like 'VENDA RETROATIVA%' ";
    } elseif ($_GET['tp'] == "5") {
        $where .= " and acao in ('I','R') and tabela = 'sf_fornecedores_despesas_convenios' ";
    } elseif ($_GET['tp'] == "6") {
        $where .= " and acao = 'B' and tabela = 'sf_vendas' ";
    } elseif ($_GET['tp'] == "7") {
        $where .= " and acao = 'C' and tabela = 'sf_fornecedores_despesas_credenciais' ";
    } elseif ($_GET['tp'] == "8") {
        $where .= " and acao in ('I','A','R') and tabela = 'sf_fornecedores_despesas_cartao' ";
    } elseif ($_GET['tp'] == "9") {        
        $where .= " and acao = 'M' and tabela = 'sf_vendas' ";
    } elseif ($_GET['tp'] == "10") {        
        $where .= " and acao = 'X' and tabela in ('sf_solicitacao_autorizacao_parcelas', 'sf_lancamento_movimento_parcelas', 'sf_venda_parcelas') ";
    } elseif ($_GET['tp'] == "11") {        
        $where .= " and acao = 'Y' and tabela in ('sf_solicitacao_autorizacao_parcelas', 'sf_lancamento_movimento_parcelas', 'sf_venda_parcelas') ";
    } elseif ($_GET['tp'] == "12") {           
        $where .= " and acao in ('I','A','E') and tabela = 'sf_agendamento' ";        
    } elseif ($_GET['tp'] == "13") {        
        $where .= " and acao = 'T' and tabela = 'sf_fornecedores_despesas' ";
    } elseif ($_GET['tp'] == "14") {        
        $where .= " and acao = 'P' and tabela = 'sf_fornecedores_despesas' ";
    } elseif ($_GET['tp'] == "15") { 
        $where .= " and acao in ('I','R') and tabela = 'sf_alunos_turmas' ";        
    } elseif ($_GET['tp'] == "16") {     
        $where .= " and acao in ('C') and tabela = 'sf_vendas_planos' ";        
    } elseif ($_GET['tp'] == "17") { 
        $where .= " and acao in ('I','E') and tabela = 'sf_fornecedores_despesas_veiculo' ";                
    } elseif ($_GET['tp'] == "18") { 
        $where .= " and acao in ('I','A','C') and tabela = 'sf_boleto' ";                
    } elseif ($_GET['tp'] == "20") { 
        $where .= " and acao in ('E') and tabela = 'sf_fornecedores_despesas_dependentes' ";                
    } elseif ($_GET['tp'] == "21") { 
        $where .= " and acao in ('E') and tabela = 'sf_vistorias' ";                
    } elseif ($_GET['tp'] == "22") { 
        $where .= " and acao in ('R') and tabela = 'sf_fornecedores_despesas_veiculo' ";                
    } elseif ($_GET['tp'] == "23") { 
        $where .= " and acao in ('E') and tabela = 'sf_clientes' and descricao like 'EXCLUIR%' ";  
    } elseif ($_GET['tp'] == "24") {        
        $where .= " and acao = 'B' and tabela = 'sf_fornecedores_despesas' ";        
    } elseif ($_GET['tp'] == "25") {        
        $where .= " and acao = 'G' and tabela = 'sf_fornecedores_despesas' "; 
    } elseif ($_GET['tp'] == "26") { 
        $where .= " and acao in ('I','E') and tabela = 'sf_rastreador' ";          
    } elseif ($_GET['tp'] == "27") { 
        $where .= " and acao in ('E') and tabela = 'sf_requisicao' ";
    } elseif ($_GET['tp'] == "28") { 
        $where .= " and acao in ('D') and tabela = 'sf_fornecedores_despesas_veiculo' ";        
    }
}

if (isset($_GET['idUsers'])) {
    $where .= " and usuario in('" . str_replace(",", "','", $_GET['idUsers']) . "')";
}

if (isset($_GET['idF'])) {
    $where .= " and l.id_fornecedores_despesas = " . $_GET['idF'];
}

$query1 = "from sf_logs l left join sf_fornecedores_despesas f on l.id_fornecedores_despesas = f.id_fornecedores_despesas
where data BETWEEN " . valoresDataHora2($_GET['dti'], "00:00:00") . " and " . valoresDataHora2($_GET['dtf'], "23:59:59") . $where;

$cur = odbc_exec($con, $query . "SELECT COUNT(*) total " . $query1);
while ($RFP = odbc_fetch_array($cur)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iTotal,
    "aaData" => array()
);

$query .= "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,
id_item,l.id_fornecedores_despesas,razao_social,data,descricao,usuario " . $query1 . "
) as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir;
//echo $query;exit();
$cur = odbc_exec($con, $query);

while ($RFP = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<center>" . utf8_encode($RFP['id_item']) . "</center>";
    $row[] = utf8_encode($RFP['id_fornecedores_despesas']) . " - " . utf8_encode($RFP['razao_social']);
    $row[] = escreverDataHora($RFP['data']);
    $row[] = utf8_encode($RFP['descricao']);
    $row[] = utf8_encode($RFP['usuario']);
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
