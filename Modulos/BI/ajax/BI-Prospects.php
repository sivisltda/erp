<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');
require_once(__DIR__ . '/../../../util/excel/exportaExcel.php');

$total = array();
$iTotal = 0;
$sLimit = 20;
$imprimir = 0;
$sWhereX = "";
$grupoArray = [];

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['ur']) && $_GET['ur'] != "null" && $_GET['ur'] != "") {
    $sWhereX .= " and id_user_resp in (" . $_GET['ur'] . ")";
} 

if ($crm_pro_ure_ > 0) {
    $sWhereX .= " and (id_user_resp = " . $_SESSION["id_usuario"] . " 
    or id_user_resp in (select sf_usuarios_dependentes.id_usuario from sf_usuarios_dependentes 
    inner join sf_usuarios on id_fornecedor_despesas = funcionario where sf_usuarios.id_usuario = " . $_SESSION["id_usuario"] . "))";
}

if (isset($_GET['fil'])) {
    $sWhereX .= " and sf_fornecedores_despesas.empresa in (" . str_replace(array("[", "]", "\"", "'"), "", $_GET["fil"]) . ")";
} else {
    $sWhereX .= " and sf_fornecedores_despesas.empresa in (select id_filial from sf_filiais
    inner join sf_usuarios_filiais on id_filial_f = id_filial 
    inner join sf_usuarios on id_usuario_f = id_usuario 
    where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "')";
}

if (isset($_GET['pln'])) {
    $sWhereX .= " and id_fornecedores_despesas in (select favorecido from sf_vendas_planos A 
    inner join sf_produtos B on A.id_prod_plano = B.conta_produto 
    where B.descricao = '" . $_GET['pln'] . "')";
}

if(!isset($_GET["tpCli"])){
    $tpCli = "'P', 'C'";
    $tpCli2 =  " and ((dt_cadastro BETWEEN " . valoresDataHora2($_GET['dti'], "00:00:00") . " and " . valoresDataHora2($_GET['dtf'], "23:59:59") . " and tipo = 'P')
    or (dt_convert_cliente BETWEEN " . valoresDataHora2($_GET['dti'], "00:00:00") . " and " . valoresDataHora2($_GET['dtf'], "23:59:59") . " and tipo = 'C'))";
    $tpCli3 = " sum(case when tipo = 'P' and dt_cadastro BETWEEN " . valoresDataHora2($_GET['dti'], "00:00:00") . " and " . valoresDataHora2($_GET['dtf'], "23:59:59") . " then 1 else 0 end) P,
    sum(case when tipo = 'C' and dt_convert_cliente BETWEEN " . valoresDataHora2($_GET['dti'], "00:00:00") . " and " . valoresDataHora2($_GET['dtf'], "23:59:59") . " then 1 else 0 end) C ";
    $tpCli4 = " and ((sf_fornecedores_despesas.tipo = 'C' and dt_convert_cliente BETWEEN " . valoresDataHora2($_GET['dti'], "00:00:00") . " and " . valoresDataHora2($_GET['dtf'], "23:59:59") . ")
    or (sf_fornecedores_despesas.tipo = 'P' and sf_fornecedores_despesas.dt_cadastro BETWEEN " . valoresDataHora2($_GET['dti'], "00:00:00") . " and " . valoresDataHora2($_GET['dtf'], "23:59:59") . "))";
}elseif ($_GET["tpCli"] == 2) {
    $tpCli = "'P'";
    $tpCli2 = " and (dt_cadastro BETWEEN " . valoresDataHora2($_GET['dti'], "00:00:00") . " and " . valoresDataHora2($_GET['dtf'], "23:59:59") . " and tipo = 'P')";
    $tpCli3 = " sum(case when tipo = 'P' and dt_cadastro BETWEEN " . valoresDataHora2($_GET['dti'], "00:00:00") . " and " . valoresDataHora2($_GET['dtf'], "23:59:59") . " then 1 else 0 end) P ";
    $tpCli4 = " and (sf_fornecedores_despesas.tipo = 'P' and sf_fornecedores_despesas.dt_cadastro BETWEEN " . valoresDataHora2($_GET['dti'], "00:00:00") . " and " . valoresDataHora2($_GET['dtf'], "23:59:59") . ")";
}elseif ($_GET["tpCli"] == 1) {
    $tpCli = "'C'";
    $tpCli2 = " and (dt_convert_cliente BETWEEN " . valoresDataHora2($_GET['dti'], "00:00:00") . " and " . valoresDataHora2($_GET['dtf'], "23:59:59") . " and tipo = 'C')";
    $tpCli3 = " sum(case when tipo = 'C' and dt_convert_cliente BETWEEN " . valoresDataHora2($_GET['dti'], "00:00:00") . " and " . valoresDataHora2($_GET['dtf'], "23:59:59") . " then 1 else 0 end) C ";
    $tpCli4 = " and (sf_fornecedores_despesas.tipo = 'C' and dt_convert_cliente BETWEEN " . valoresDataHora2($_GET['dti'], "00:00:00") . " and " . valoresDataHora2($_GET['dtf'], "23:59:59") . ")";  
}

if (isset($_GET['contratos'])) {
    $query = "set dateformat dmy;select count(distinct favorecido) id, sf_produtos.descricao from sf_fornecedores_despesas 
    inner join sf_vendas_planos on favorecido = id_fornecedores_despesas
    inner join sf_produtos on id_prod_plano = conta_produto
    where id_fornecedores_despesas > 0 and dt_cancelamento is null " . $sWhereX . $tpCli4 . "    
    group by sf_produtos.descricao order by descricao";
    //echo $query; exit();
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $total[] = array(utf8_encode($RFP["id"]), utf8_encode($RFP["descricao"]));
    }    
}

if (isset($_GET['dashboard'])) {
    $query = "set dateformat dmy;select ".$tpCli3." from sf_fornecedores_despesas where id_fornecedores_despesas > 0 " . $sWhereX;
    //echo $query;exit();
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $total[] = $RFP["P"];
        $total[] = $RFP["C"];
    }
}

if (isset($_GET['listProsp'])) {
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    $where = " where tipo in (".$tpCli.") " . $tpCli2 . $sWhereX;
    //echo $where;exit();
    if (!isset($_GET['ex']) && !isset($_GET['pdf'])) {
        $query = "set dateformat dmy;SELECT id_fornecedores_despesas,razao_social,dt_cadastro,dt_convert_cliente,tipo,tel,cel,email,valor,nome 
        FROM(SELECT ROW_NUMBER() OVER (ORDER BY id_fornecedores_despesas asc) as row,sf_fornecedores_despesas.*,nome,
        (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where fornecedores_despesas = id_fornecedores_despesas and tipo_contato = 1) cel,
        (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where fornecedores_despesas = id_fornecedores_despesas and tipo_contato = 0) tel,
        (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where fornecedores_despesas = id_fornecedores_despesas and tipo_contato = 2) email,
        (select sum(x.valor) from (select top 1 isnull(vi.valor_total, 0) valor 
        from sf_vendas_planos A inner join sf_vendas_planos_mensalidade vpm on vpm.id_plano_mens = A.id_plano 
        inner join sf_produtos B on A.id_prod_plano = B.conta_produto 
        left join sf_vendas_itens vi on vi.id_item_venda = vpm.id_item_venda_mens        
        left join sf_vendas_planos_dcc_agendamento C on A.id_plano = C.id_plano_agendamento
        where favorecido = id_fornecedores_despesas and ((DATEADD(day,(select ACA_PLN_DIAS_RENOVACAO from sf_configuracao),A.dt_fim) > GETDATE() and B.parcelar = 0) or
        (A.dt_fim >= GETDATE() and B.parcelar = 1) or ((select COUNT(*) from sf_vendas_planos_mensalidade C where C.id_plano_mens = A.id_plano and dt_pagamento_mens is null) > 0)) order by dt_inicio_mens asc) as x) valor
        from sf_fornecedores_despesas left join sf_usuarios on id_user_resp = id_usuario " . $where . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder2;
    } else {
        $query = "set dateformat dmy;SELECT id_fornecedores_despesas,razao_social,dt_cadastro,dt_convert_cliente,tipo,
        (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where fornecedores_despesas = id_fornecedores_despesas and tipo_contato = 1) cel, 
        (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where fornecedores_despesas = id_fornecedores_despesas and tipo_contato = 0) tel,
        (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where fornecedores_despesas = id_fornecedores_despesas and tipo_contato = 2) email,
        (select sum(x.valor) from (select top 1 isnull(vi.valor_total, 0) valor 
        from sf_vendas_planos A inner join sf_vendas_planos_mensalidade vpm on vpm.id_plano_mens = A.id_plano 
        inner join sf_produtos B on A.id_prod_plano = B.conta_produto
        left join sf_vendas_itens vi on vi.id_item_venda = vpm.id_item_venda_mens                
        left join sf_vendas_planos_dcc_agendamento C on A.id_plano = C.id_plano_agendamento
        where favorecido = id_fornecedores_despesas and ((DATEADD(day,(select ACA_PLN_DIAS_RENOVACAO from sf_configuracao),A.dt_fim) > GETDATE() and B.parcelar = 0) or
        (A.dt_fim >= GETDATE() and B.parcelar = 1) or ((select COUNT(*) from sf_vendas_planos_mensalidade C where C.id_plano_mens = A.id_plano and dt_pagamento_mens is null) > 0)) order by dt_inicio_mens asc) as x) valor, nome gc
        from sf_fornecedores_despesas left join sf_usuarios on id_user_resp = id_usuario " . $where;
    }
    //echo $query; exit();
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        if (isset($_GET['pdf'])) {
            $row[] = "";
        }
        $row[] = utf8_encode($RFP['id_fornecedores_despesas']);
        $row[] = "<a href=\"javascript:void(0)\" onclick=\"AbrirBox(2," . $RFP['id_fornecedores_despesas'] . ")\">" . utf8_encode($RFP['razao_social']) . "</a>";        
        $row[] = escreverData($RFP['dt_cadastro']);
        $row[] = escreverData($RFP['dt_convert_cliente']);
        $row[] = utf8_encode(($RFP['tipo'] == "C" ? "Convertido" : "Prospect"));
        if (!isset($_GET['ex']) && !isset($_GET['pdf'])) {
            $row[] = formatNameCompact(utf8_encode($RFP['nome']));
        } else {
            $row[] = utf8_encode($RFP['tel']);    
        }
        $row[] = utf8_encode($RFP['cel']);
        $row[] = utf8_encode($RFP['email']);
        $row[] = escreverNumero($RFP['valor']);
        if (isset($_GET['pdf'])) {      
            $row[] = utf8_encode($RFP['gc']);
            $grp = array(utf8_encode($RFP['gc']), ($RFP['gc'] == "" ? "SEM GRUPO" : utf8_encode($RFP['gc'])), 9);
            if (!in_array($grp, $grupoArray)) {
                $grupoArray[] = $grp;
            }
        }           
        $output['aaData'][] = $row;
    }
    $query2 = "set dateformat dmy;SELECT COUNT(*) total from sf_fornecedores_despesas " . $where;
    $cur = odbc_exec($con, $query2);
    while ($RFP = odbc_fetch_array($cur)) {
        $output['iTotalRecords'] = $RFP['total'];
        $output['iTotalDisplayRecords'] = $RFP['total'];
    }
    $total = $output;
}

if (isset($_GET['ex']) && $_GET['ex'] == 1) {
    $topo = '<tr><td><b>Código </b></td><td><b>Nome </b></td><td><b>Dt.Cad. </b></td><td><b>Dt. Conv. </b><td><b>Tipo </b></td><td><b>Telefone </b></td><td><b>Celular </b></td><td><b>E-mail </b></td><td><b>Valor </b></td></tr>';
    exportaExcel($total['aaData'], $topo);
} else {
    if (!isset($_GET['pdf'])) {
        echo json_encode($total);
    } else {
        $output['aaTotal'][] = array(8, 8, "Sub Total", "Total", 0, 0); 
        $output['aaGrupo'] = $grupoArray;        
    }
}
odbc_close($con);
