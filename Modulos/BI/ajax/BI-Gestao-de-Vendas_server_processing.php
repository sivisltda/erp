<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');

$where = "";
$whereX = "";
$whereL = "";
$grupoArray = [];
$sLimit = 0;
$sQtd = 10;
$imprimir = 0;

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (is_numeric($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if ($_GET['tpv'] == "1") {
    $where .= " and id_venda in (select min(id_venda) from sf_vendas where sf_vendas.status = 'Aprovado' and cov = 'V' group by cliente_venda)";
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $where .= "and data_venda between '" . str_replace("_", "/", $_GET['dti']) . " 00:00:00' and '" . str_replace("_", "/", $_GET['dtf']) . " 23:59:59'";
}

if (is_numeric($_GET['tpr']) && isset($_GET['tpg'])) {
    $whereX .= " and " . ($_GET['tpr'] == "1" ? "c.prof_resp" : "c.id_user_resp") . " in (" . str_replace(array("[", "]", "\"", "'"), "", $_GET["tpg"]) . ")";
    $whereL .= " and indicador in (select funcionario from sf_usuarios where id_usuario in (" . str_replace(array("[", "]", "\"", "'"), "", $_GET["tpg"]) . "))";    
}

if (isset($_GET['fil'])) {
    $whereX .= " and c.empresa in (" . str_replace(array("[", "]", "\"", "'"), "", $_GET["fil"]) . ")";
    $whereL .= " and indicador in (select id_fornecedores_despesas from sf_fornecedores_despesas where tipo = 'E' and empresa in (" . str_replace(array("[", "]", "\"", "'"), "", $_GET["fil"]) . "))";
} else {
    $whereX .= " and c.empresa in (select id_filial_f from sf_usuarios_filiais where id_usuario_f = " . $_SESSION["id_usuario"] . ")";    
    $whereL .= " and indicador in (select id_fornecedores_despesas from sf_fornecedores_despesas where tipo = 'E' and empresa in (select id_filial_f from sf_usuarios_filiais where id_usuario_f = " . $_SESSION["id_usuario"] . "))";
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => 0,
    "iTotalDisplayRecords" => 0,
    "aaData" => array()
);

$query = "select id_venda, data_venda, sf_vendas.status, cov, 
isnull(" . ($_GET['tpr'] == "1" ? "ind.razao_social" : "login_user") . ", '" . utf8_decode("Não Informado") . "') login_user,
isnull((select sum(valor_total) from sf_vendas_itens vi where vi.id_venda = sf_vendas.id_venda), 0) total 
from sf_vendas inner join sf_fornecedores_despesas c on cliente_venda = c.id_fornecedores_despesas
left join sf_fornecedores_despesas ind on ind.id_fornecedores_despesas = c.prof_resp and ind.id_fornecedores_despesas > 0
left join sf_usuarios on id_usuario = c.id_user_resp
where sf_vendas.status = 'Aprovado' and cov = 'V' " . $where . $whereX;

if ($_GET['tp'] == 0) {
    $cur = odbc_exec($con, "set dateformat dmy;
    select count(*) total from (select count(login_user) total from (" . $query . ") as x where x.total > 0 group by login_user) as y");
    while ($RFP = odbc_fetch_array($cur)) {
        $output['iTotalRecords'] = $RFP['total'];
        $output['iTotalDisplayRecords'] = $RFP['total'];
    }
    $query = "SELECT * FROM(SELECT ROW_NUMBER() OVER (order by total desc) as row, * from (select login_user, count(id_venda) qtd, max(total) maior, sum(total) total, sum(total)/count(id_venda) media 
    from (" . $query . ") as x group by login_user) as y) as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " order by total desc";
} else if ($_GET['tp'] == 4) {
    $query = "select count(id_venda) disponivel, sum(total) value,
    (select faturamento_ativos from sf_metas where data_meta between '" . str_replace("_", "/", $_GET['dti']) . " 00:00:00' and '" . str_replace("_", "/", $_GET['dtf']) . " 23:59:59') m_ativos,
    (select faturamento_meta from sf_metas where data_meta between '" . str_replace("_", "/", $_GET['dti']) . " 00:00:00' and '" . str_replace("_", "/", $_GET['dtf']) . " 23:59:59') m_valor 
    from (" . $query . ") as x";    
} else if ($_GET['tp'] == 1 || $_GET['tp'] == 2) {    
    $query = "select id_lead, razao_social, 'L' tipo, dt_cadastro, procedencia, telefone_contato, email_contato, 0 pago, 
    (select max(id_usuario) from sf_usuarios where funcionario = indicador) id_user_resp
    from sf_lead where dt_cadastro between '" . str_replace("_", "/", $_GET['dti']) . " 00:00:00' and '" . str_replace("_", "/", $_GET['dtf']) . " 23:59:59' 
    and inativo = 0 and prospect is null " . $whereL . " union select id_fornecedores_despesas, razao_social, tipo, dt_cadastro, procedencia, '' telefone_contato,'' email_contato, 0 pago, id_user_resp 
    from sf_fornecedores_despesas c where tipo = 'P' " . $whereX . " 
    and dt_cadastro between '" . str_replace("_", "/", $_GET['dti']) . " 00:00:00' and '" . str_replace("_", "/", $_GET['dtf']) . " 23:59:59' and inativo = 0
    union select id_fornecedores_despesas, razao_social, tipo, dt_cadastro, procedencia, '' telefone_contato,'' email_contato, 
    (select count(id_venda) from sf_vendas where cov = 'V' and status = 'Aprovado' and cliente_venda = id_fornecedores_despesas) pago, id_user_resp 
    from sf_fornecedores_despesas c where tipo = 'C' " . $whereX . " and dt_convert_cliente is null 
    and dt_cadastro between '" . str_replace("_", "/", $_GET['dti']) . " 00:00:00' and '" . str_replace("_", "/", $_GET['dtf']) . " 23:59:59' and inativo = 0
    union select id_fornecedores_despesas, razao_social, tipo, dt_convert_cliente, procedencia, '' telefone_contato,'' email_contato, 
    (select count(id_venda) from sf_vendas where cov = 'V' and status = 'Aprovado' and cliente_venda = id_fornecedores_despesas) pago, id_user_resp 
    from sf_fornecedores_despesas c where tipo = 'C' " . $whereX . " and dt_convert_cliente is not null 
    and dt_convert_cliente between '" . str_replace("_", "/", $_GET['dti']) . " 00:00:00' and '" . str_replace("_", "/", $_GET['dtf']) . " 23:59:59' and inativo = 0";        
    if ($_GET['tp'] == 1 && $imprimir == 0) {
        $query = "select count(x.id_lead) lead, 
        sum(case when x.tipo in ('P', 'C') then 1 else 0 end) prospect, sum(case when x.tipo = 'C' then 1 else 0 end) cliente, 
        sum(case when x.tipo = 'C' and x.pago > 0 then 1 else 0 end) faturamento from (" . $query . ") as x";
    } else if ($_GET['tp'] == 1 && $imprimir == 2) {
        $query = "select plano, count(id_lead) total from (select id_lead, max(isnull(descricao, 'SEM PLANO')) plano 
        from (" . $query . ") as x left join sf_vendas_planos on favorecido = x.id_lead and x.tipo in ('P', 'C') and dt_cancelamento is null
        left join sf_produtos on conta_produto = id_prod_plano group by id_lead) y group by y.plano order by total desc";        
    } else if ($_GET['tp'] == 1) {
        $query = "select id_lead, razao_social, tipo, dt_cadastro, u.nome,
        case when tipo = 'L' then telefone_contato collate SQL_Latin1_General_CP1_CI_AS else (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = id_lead) end telefone_contato, 
        case when tipo = 'L' then email_contato collate SQL_Latin1_General_CP1_CI_AS else (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = id_lead) end email_contato 
        from (" . $query . ") as x left join sf_usuarios u on u.id_usuario = x.id_user_resp " . 
        ($_GET['tipo'] == "Clientes" ? " where x.tipo = 'C'" : 
        ($_GET['tipo'] == "Clientes Faturados" ? " where x.tipo = 'C' and pago > 0" : 
        ($_GET['tipo'] == "Prospects" ? " where x.tipo in ('P', 'C')" : "")));        
        if (!isset($_GET['isPdf']) && !isset($_GET['pdf'])) {
            $cur = odbc_exec($con, "set dateformat dmy; select count(*) total from (" . $query . ") as y");
            while ($RFP = odbc_fetch_array($cur)) {
                $output['iTotalRecords'] = $RFP['total'];
                $output['iTotalDisplayRecords'] = $RFP['total'];
            }
            $query = "SELECT * FROM(SELECT ROW_NUMBER() OVER (order by dt_cadastro desc) as row, * from (" . $query . ") as y) as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd);            
        }
        $query .= " order by dt_cadastro desc";
    } else if ($_GET['tp'] == 2) {
        $query = "select count(id_lead) total, isnull(nome_procedencia, '" . utf8_decode("Não Informado") . "') nome_procedencia
        from (" . $query . ") as x left join sf_procedencia on sf_procedencia.id_procedencia = x.procedencia and tipo_procedencia = 0 " .
        ($_GET['tpp'] == "C" ? " where x.tipo = 'C'" : 
        ($_GET['tpp'] == "F" ? " where x.tipo = 'C' and pago > 0" : 
        ($_GET['tpp'] == "P" ? " where x.tipo in ('P', 'C')" : ""))) .
        " group by nome_procedencia order by 1 desc";
    }         
} else if ($_GET['tp'] == 3) {
    $query = "select sum(v_orcamento) v_orcamento, sum(q_orcamento) q_orcamento, 
    sum(v_apagar) v_apagar, sum(q_apagar) q_apagar, 
    sum(v_pago) v_pago, sum(q_pago) q_pago,
    sum(v_cancelado) v_cancelado, sum(q_cancelado) q_cancelado
    from (select case when tipo = 'P' and dt_cancelamento is null then valor_mens else 0 end v_orcamento,
    case when tipo = 'P' and dt_cancelamento is null then 1 else 0 end q_orcamento,
    case when tipo = 'C' and dt_cancelamento is null and dt_pagamento_mens is null then valor_mens else 0 end v_apagar,
    case when tipo = 'C' and dt_cancelamento is null and dt_pagamento_mens is null then 1 else 0 end q_apagar,
    case when tipo = 'C' and dt_cancelamento is null and dt_pagamento_mens is not null then valor_mens else 0 end v_pago,
    case when tipo = 'C' and dt_cancelamento is null and dt_pagamento_mens is not null then 1 else 0 end q_pago,
    case when tipo = 'C' and dt_cancelamento is not null then valor_mens else 0 end v_cancelado,
    case when tipo = 'C' and dt_cancelamento is not null then 1 else 0 end q_cancelado
    from sf_vendas_planos inner join sf_fornecedores_despesas c on id_fornecedores_despesas = favorecido
    inner join sf_vendas_planos_mensalidade on id_plano = id_plano_mens
    where dt_inicio_mens between '" . str_replace("_", "/", $_GET['dti']) . " 00:00:00' and '" . str_replace("_", "/", $_GET['dtf']) . " 23:59:59' " . $whereX . ") as x";
}
//echo $query; exit;
$lit = array("Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez");
$cur = odbc_exec($con, "set dateformat dmy;" . $query);
while ($RFP = odbc_fetch_array($cur)) {
    $row = array();
    if ($_GET['tp'] == 0) {
        $row[] = $RFP['row'];
        $row[] = utf8_encode($RFP['login_user']);
        $row[] = $RFP['qtd'];
        $row[] = escreverNumero($RFP['maior'], 1);
        $row[] = escreverNumero($RFP['total'], 1);
        $row[] = escreverNumero($RFP['media'], 1);
    } else if ($_GET['tp'] == 1 && $imprimir == 0) {        
        $row["lead"] = ["title" => utf8_encode("Leads"), "value" => $RFP['lead']];
        $row["prospect"] = ["title" => utf8_encode("Prospects"), "value" => $RFP['prospect']];
        $row["cliente"] = ["title" => utf8_encode("Clientes"), "value" => $RFP['cliente']];
        $row["faturamento"] = ["title" => utf8_encode("Clientes Faturados"), "value" => $RFP['faturamento']];                
    } else if ($_GET['tp'] == 1 && $imprimir == 1) {
        $row[] = $RFP['id_lead'];
        $row[] = utf8_encode($RFP['razao_social']);        
        $row[] = utf8_encode($RFP['tipo']); 
        if (!isset($_GET['pdf'])) {
            $row[] = formatNameCompact(utf8_encode($RFP['nome']));            
        }
        $row[] = escreverDataHora($RFP['dt_cadastro']);        
        $row[] = utf8_encode($RFP['telefone_contato']);        
        $row[] = utf8_encode($RFP['email_contato']);
        if (isset($_GET['pdf'])) {
            $row[] = utf8_encode($RFP['nome']);
            $grp = array(utf8_encode($RFP['nome']), ($RFP['nome'] == "" ? "SEM GRUPO" : utf8_encode($RFP['nome'])), 6);
            if (!in_array($grp, $grupoArray)) {
                $grupoArray[] = $grp;
            }
        }        
    } else if ($_GET['tp'] == 1 && $imprimir == 2) {
        $row = ["title" => utf8_encode($RFP['plano']), "value" => utf8_encode($RFP['total'])];
    } else if ($_GET['tp'] == 2) {        
        $row = ["title" => utf8_encode($RFP['nome_procedencia']), "value" => utf8_encode($RFP['total'])];
    } else if ($_GET['tp'] == 3) {               
        $output['aaData'][] = ["desc" => "Orçamentos Enviados", "qtd" => utf8_encode($RFP['q_orcamento']), "valor" => escreverNumero($RFP['v_orcamento'], 1)];
        $output['aaData'][] = ["desc" => "Planos Faturados", "qtd" => utf8_encode($RFP['q_pago']), "valor" => escreverNumero($RFP['v_pago'], 1)];        
        $output['aaData'][] = ["desc" => "Planos à Faturar", "qtd" => utf8_encode($RFP['q_apagar']), "valor" => escreverNumero($RFP['v_apagar'], 1)];        
        $row = ["desc" => "Planos Cancelados", "qtd" => utf8_encode($RFP['q_cancelado']), "valor" => escreverNumero($RFP['v_cancelado'], 1)];
    } else if ($_GET['tp'] == 4) {
        $row = ["venda" => ($RFP['m_ativos'] > 0 ? ($RFP['disponivel'] * 100)/$RFP['m_ativos'] : 0),
        "equilibrio" => ($RFP['m_valor'] > 0 ? ($RFP['value'] * 100)/$RFP['m_valor'] : 0)];
    }
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
} else {
    $output['aaGrupo'] = $grupoArray;        
}
odbc_close($con);