<?php

$sQueryAtivos = "set dateformat dmy;select count(id_fornecedores_despesas) ativos,
sum(case when fornecedores_status not in('AtivoCred') then 1 else 0 end) pagantes  
from sf_fornecedores_despesas 
where id_fornecedores_despesas > 0 and tipo = 'C' and inativo = 0
and fornecedores_status like 'ativo%';";

$sQueryRenovacoes = "set dateformat dmy;select sum(case when x.anterior is null then 1 else 0 end) 'Novos', 
sum(case when x.anterior is not null and datediff(day,x.anterior,x.atual) > x.dias then 1 else 0 end) 'Retornos', 
sum(case when x.anterior is not null and datediff(day,x.anterior,x.atual) <= x.dias then 1 else 0 end) 'Renovacoes'
,x.year, x.month from(select (select max(dt_pagamento_mens) from sf_vendas_planos_mensalidade B 
where B.id_plano_mens = A.id_plano_mens and B.id_mens < A.id_mens) anterior, 
dt_pagamento_mens atual,(select ACA_PLN_DIAS_RENOVACAO from sf_configuracao) dias
,year(dt_pagamento_mens) year,month(dt_pagamento_mens) month
from sf_fornecedores_despesas inner join sf_vendas_planos on id_fornecedores_despesas = favorecido inner join sf_produtos on sf_produtos.conta_produto = id_prod_plano 
inner join sf_vendas_planos_mensalidade A on A.id_plano_mens = sf_vendas_planos.id_plano
where sf_fornecedores_despesas.tipo = 'C' and inativo = 0
and id_item_venda_mens is not null and dt_pagamento_mens between '01/" . date("m/Y") . " 00:00:00'
and dateadd(day,-1,dateadd(month,1,'01/" . date("m/Y") . " 23:59:59'))) as x group by x.year, x.month order by x.year, x.month";

$sQueryEvasao = "set dateformat dmy;select sum(case when dt_inicio_mens >= cast(GETDATE() as date) and id_item_venda_mens is null then 1 else 0 end) 'renovar',
sum(case when dt_inicio_mens < cast(GETDATE() as date) and id_item_venda_mens is null then 1 else 0 end) 'nrenovar',
sum(case when id_item_venda_mens is not null then 1 else 0 end) 'renovado'
from sf_vendas_planos_mensalidade A inner join sf_vendas_planos P on P.id_plano = A.id_plano_mens
inner join sf_fornecedores_despesas F on P.favorecido = F.id_fornecedores_despesas
where P.dt_cancelamento is null and dt_inicio_mens between " . valoresDataHoraUnico2(getData("B") . " 00:00:00") . " and " . valoresDataHoraUnico2(getData("E") . " 23:59:59");

$sQueryTM = "set dateformat dmy;select sum(valor_parcela) total_pago,
(select COUNT(id_fornecedores_despesas) from sf_fornecedores_despesas where tipo = 'C' and fornecedores_status like 'Ativo%') alunos
from sf_venda_parcelas inner join sf_vendas on sf_vendas.id_venda = sf_venda_parcelas.venda
inner join sf_tipo_documento on sf_tipo_documento.id_tipo_documento = sf_venda_parcelas.tipo_documento
where data_parcela between " . valoresDataHoraUnico2(getData("B") . " 00:00:00") . " and " . valoresDataHoraUnico2(getData("E") . " 23:59:59") . "
and status = 'Aprovado' and cov = 'V'";

$sQueryCM = "set dateformat dmy; select x.descricao,sum(x.valor_pago_manual) valor_pago_manual, sum(x.valor_pago_robo) valor_pago_robo from ( select sf_tipo_documento.descricao,valor_pago valor_pago_manual, 0 valor_pago_robo from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas left join sf_tipo_documento on sf_lancamento_movimento_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_lancamento_movimento.status = 'Aprovado' and sf_contas_movimento.tipo = 'D' AND valor_pago > 0 
AND data_pagamento >= " . valoresDataHoraUnico2(getData("B") . " 00:00:00") . " AND data_pagamento <= " . valoresDataHoraUnico2(getData("E") . " 23:59:59") . " 
and sf_lancamento_movimento.empresa = 1 AND sf_lancamento_movimento_parcelas.inativo = '0'union all select sf_tipo_documento.descricao,valor_pago valor_pago_manual, 0 valor_pago_robo from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas left join sf_tipo_documento on sf_solicitacao_autorizacao_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where status = 'Aprovado' and sf_contas_movimento.tipo = 'D' AND valor_pago > 0 
AND data_pagamento >= " . valoresDataHoraUnico2(getData("B") . " 00:00:00") . " AND data_pagamento <= " . valoresDataHoraUnico2(getData("E") . " 23:59:59") . " and sf_solicitacao_autorizacao.empresa = 1 AND sf_solicitacao_autorizacao_parcelas.inativo = '0' union all select sf_tipo_documento.descricao,case when syslogin <> 'Sistema' then valor_pago else 0 end valor_pago_manual, case when syslogin = 'Sistema' then valor_pago else 0 end valor_pago_robo from sf_venda_parcelas inner join sf_vendas on sf_venda_parcelas.venda = sf_vendas.id_venda left join sf_tipo_documento on sf_venda_parcelas.tipo_documento = sf_tipo_documento.id_tipo_documento inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas where status = 'Aprovado' and sf_vendas.cov = 'C' AND valor_pago > 0 
AND data_pagamento >= " . valoresDataHoraUnico2(getData("B") . " 00:00:00") . " AND data_pagamento <= " . valoresDataHoraUnico2(getData("E") . " 23:59:59") . " and sf_vendas.empresa = 1 AND sf_venda_parcelas.inativo = '0' ) as x group by x.descricao order by x.descricao";

$sQueryCancelamento = "set dateformat dmy;select count(isnull(obs_cancelamento,'')) total from sf_vendas_planos
where dt_cancelamento is not null and cast(dt_cancelamento as date) BETWEEN " . 
valoresData2(getData("B")) . " and " . valoresData2(getData("E"));  

$sQueryFaturamento = "set dateformat dmy;select sum(case when sf_produtos.tipo = 'C' then valor_total else 0 end) valor_contrato,
sum(case when sf_produtos.tipo = 'S' then valor_total else 0 end) valor_servico
from sf_vendas inner join sf_vendas_itens on sf_vendas.id_venda = sf_vendas_itens.id_venda
inner join sf_produtos on sf_produtos.conta_produto = sf_vendas_itens.produto
where status = 'Aprovado' and cov = 'V' AND dt_estorno is null
and sf_produtos.tipo in ('C','S') 
and data_venda between " . valoresDataHoraUnico2(getData("B") . " 00:00:00") . " and " . valoresDataHoraUnico2(getData("E") . " 23:59:59");

$sQueryParcelas = "set dateformat dmy;select parcela,count(distinct favorecido) total from sf_vendas_planos_mensalidade
inner join sf_vendas_planos on sf_vendas_planos_mensalidade.id_plano_mens = sf_vendas_planos.id_plano
inner join sf_fornecedores_despesas on sf_fornecedores_despesas.id_fornecedores_despesas = sf_vendas_planos.favorecido
inner join sf_produtos_parcelas on sf_vendas_planos_mensalidade.id_parc_prod_mens = sf_produtos_parcelas.id_parcela
inner join sf_produtos on sf_produtos_parcelas.id_produto = sf_produtos.conta_produto
where dt_cancelamento is null and sf_produtos.tipo = 'C' and inativa = 0 and sf_fornecedores_despesas.inativo = 0 
and sf_fornecedores_despesas.empresa = 1 group by parcela order by 1";

$sQueryVeiculo = "set dateformat dmy;SELECT count(v.id) total from sf_fornecedores_despesas_veiculo v 
inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = v.id_fornecedores_despesas
left join sf_vendas_planos on id_veiculo = v.id left join sf_produtos on id_prod_plano = conta_produto
left join sf_usuarios on id_user_resp = id_usuario    
WHERE id > 0 AND f.tipo = 'C' AND v.id in(select id_veiculo from sf_vistorias where data_aprov is not null) 
and sf_vendas_planos.planos_status in ('Ativo')";