<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');

$where = '';
$porc = "0";
$totalTurmas = 0;
$totalComiss = 0;
$grupoArray = [];

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => 0,
    "iTotalDisplayRecords" => 0,
    "aaData" => array()
);

if (isset($_GET['prof'])) {
    $where = " and t.professor in ('" . str_replace(",", "','", $_GET['prof']) . "')";
}

if (isset($_GET['turms'])) {
    $where = " and t.id_turma in ('" . str_replace(",", "','", $_GET['turms']) . "')";
}

if (isset($_GET['com'])) {
    if ($_GET['tpcom'] == 'true') {
        if ($_GET['tp'] == 0) {
            $porc = ' (SUM(valor_mens)/100)*' . valoresNumericos2($_GET['com']);
        } else {
            $porc = ' (valor_mens/100)*' . valoresNumericos2($_GET['com']);
        }
    } else {
        $porc = valoresNumericos2($_GET['com']);
    }
}

if ($_GET['tp'] == 0) {
    $query = "set dateformat dmy;select f.id_turma, t.descricao, COUNT(*) qntdd, SUM(valor_mens) total," . $porc . " comisao 
    from sf_fornecedores_despesas_turmas as f
    inner join sf_turmas as t on f.id_turma = t.id_turma
    inner join sf_vendas_planos as vp on f.id_plano = vp. id_plano
    inner join sf_vendas_planos_mensalidade as vpm on vp.id_plano = vpm.id_plano_mens
    where vpm.dt_pagamento_mens is not null
    and vpm.dt_pagamento_mens between " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']) . $where . " group by f.id_turma,t.descricao";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        if (isset($_GET['pdf']) || isset($_GET['ex'])) {
            $row = array();
            $row[] = utf8_encode($RFP['descricao']);
            $row[] = escreverNumero($RFP['total'], 1);
            $row[] = escreverNumero($RFP['comisao'], 1);
            $totalTurmas = $totalTurmas + $RFP['total'];
            $totalComiss = $totalComiss + $RFP['comisao'];
        } else {
            $row = array(utf8_encode($RFP['descricao']), escreverNumero($RFP['total']), escreverNumero($RFP['comisao']));
        }
        $output['aaData'][] = $row;
    }

    if (isset($_GET['pdf']) || isset($_GET['ex'])) {
        $row = array();
        $row[] = "TOTAL";
        $row[] = escreverNumero($totalTurmas, 1);
        $row[] = escreverNumero($totalComiss, 1);
        $output['aaData'][] = $row;
    }
}

if ($_GET['tp'] == 1) {
    $query = "set dateformat dmy;
            select f.id_fornecedores_despesas,fd.razao_social,t.descricao, t.descricao,vpm.dt_pagamento_mens, valor_mens, 
            dt_inicio_mens, dt_fim_mens, E.razao_social nome_prof, " . $porc . " comisao 
            from sf_fornecedores_despesas_turmas as f
            inner join sf_fornecedores_despesas as fd on f.id_fornecedores_despesas = fd.id_fornecedores_despesas 
            inner join sf_turmas as t on f.id_turma = t.id_turma                         
            inner join sf_vendas_planos as vp on f.id_plano = vp. id_plano 
            inner join sf_vendas_planos_mensalidade as vpm on vp.id_plano = vpm.id_plano_mens 
            left join sf_fornecedores_despesas E on t.professor = E.id_fornecedores_despesas            
            where vpm.dt_pagamento_mens is not null 
            and vpm.dt_pagamento_mens between " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']) . $where . "";
    //echo $query; exit;
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        if (isset($_GET['pdf']) || isset($_GET['ex'])) {
            $row = array();
            if ($_GET['pdf']) {
                $row[] = "";
            }
            $row[] = $RFP['id_fornecedores_despesas'];
            $row[] = utf8_encode($RFP['razao_social']);
            $row[] = utf8_encode($RFP['descricao']);
            $row[] = "<center>" . escreverData($RFP['dt_inicio_mens']) . " a " . escreverData($RFP['dt_fim_mens']) . "</center>";
            $row[] = escreverData($RFP['dt_pagamento_mens']);
            $row[] = escreverNumero($RFP['valor_mens'], 1);
            $row[] = escreverNumero($RFP['comisao'], 1);
            $row[] = "<div id='formPQ' title='" . utf8_encode($RFP["nome_prof"]) . "'>" . utf8_encode($RFP["nome_prof"]) . "</div>";
            if (isset($_GET['pdf'])) {
                $grp = array(utf8_encode($RFP['nome_prof']), utf8_encode(($RFP['nome_prof'] == "" ? "SEM GRUPO" : $RFP['nome_prof'])), 7);
                if (!in_array($grp, $grupoArray)) {
                    $grupoArray[] = $grp;
                }
            }
            $output['aaData'][] = $row;
            $totalTurmas = $totalTurmas + $RFP['valor_mens'];
            $totalComiss = $totalComiss + $RFP['comisao'];
        } else {
            $row = array();
            $row[] = "<center>" . utf8_encode($RFP['id_fornecedores_despesas']) . "</center>";
            $row[] = utf8_encode($RFP['razao_social']);
            $row[] = utf8_encode($RFP['descricao']);
            $row[] = "<center>" . escreverData($RFP['dt_inicio_mens']) . " a " . escreverData($RFP['dt_fim_mens']) . "</center>";
            $row[] = "<center>" . escreverData($RFP['dt_pagamento_mens']) . "</center>";
            $row[] = "<center>" . escreverNumero($RFP['valor_mens'], 1) . "</center>";
            $row[] = "<center>" . escreverNumero($RFP['comisao'], 1) . "</center>";
            $output['aaData'][] = $row;
        }
    }

    if (isset($_GET['pdf']) || isset($_GET['ex'])) {
        $output['aaTotal'][] = array(5, 5, "", "", 0, 0, 1);
        $output['aaTotal'][] = array(6, 6, "", "", 0, 0, 1);
        $output['aaGrupo'] = $grupoArray;
    }
}

if (isset($_GET["listTurm"])) {
    $where = "";
    if (isset($_GET["profs"])) {
        $repla = array("[", "]");
        $dados = str_replace($repla, "", $_GET["profs"]);
        $dados2 = str_replace('"', "", $dados);
        $where = " where professor in('" . str_replace(",", "','", $dados2) . "')";
    }
    $query = "select * from sf_turmas " . $where;
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["id"] = $RFP['id_turma'];
        $row["desc"] = utf8_encode($RFP['descricao']);
        $rowTot[] = $row;
    }
    $output['aaData'] = $rowTot;
}

if (isset($_GET["listProf"])) {
    $query = "select * from dbo.sf_fornecedores_despesas where tipo = 'E'";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["id"] = $RFP['id_fornecedores_despesas'];
        $row["desc"] = utf8_encode($RFP['razao_social']);
        $rowTot[] = $row;
    }
    $output['aaData'] = $rowTot;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);


