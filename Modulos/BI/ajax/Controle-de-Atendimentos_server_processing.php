<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');
require_once(__DIR__ . '/../../../util/excel/exportaExcel.php');

$DateBegin = getData("B");
$DateEnd = getData("E");
$data = "0";
$linhaX = "";
$total = 0;
$where = "";
$imprimir = 0;
$tpd = "0";
$sLimit = 20;
$for = "";

if (is_numeric($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}
if (is_numeric($_GET['tpd'])) {
    $tpd = $_GET['tpd'];
}
if (is_numeric($_GET['pd'])) {
    $where .= " and pendente = " . $_GET['pd'];
}
if ($_GET['frm'] != '') {
    $where .= " and especificacao = '" . $_GET['frm'] . "'";
}
if (is_numeric($_GET['for'])) {
    $for = $_GET['for'];
}
if (is_numeric($_GET['id'])) {
    $where .= " and u1.id_usuario = " . $_GET['id'];
}
if (is_numeric($_GET['tp'])) {
    $where .= " and atendimento_servico = " . $_GET['tp'];
}
if ($_GET['dti'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
}
if ($_GET['dtf'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
}
if (is_numeric($_GET['filial'])) {
    $where .= " and empresa = " . $_GET['filial'];
}

function formataNumeros($valor) {
    if (is_numeric($_GET['ts']) && $_GET['ts'] > 0) {
        return escreverNumero($valor, 1);
    } else {
        return $valor;
    }
}

function legenda($i) {
    switch ($i) {
        case "T":
            return "Telefone";
        case "E":
            return "Email";
        case "V":
            return "Visita";
        case "V":
            return "Presencial";
        case "S":
            return "SMS";
        case "W":
            return "Whatsapp";            
        case "R":
            return "Portal";            
    }
    return "Outro";
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => 0,
    "iTotalDisplayRecords" => 0,
    "aaData" => array(),
    "aCalendar" => array()
);

if ($imprimir == 0) {
    $dias = (int) floor((geraTimestamp($DateEnd) - geraTimestamp($DateBegin)) / (60 * 60 * 24));
    if ($dias > 0) {
        for ($i = 0; $i <= $dias; $i++) {
            $row = array();
            $data = escreverDataSoma($DateBegin, "+" . $i . " days");
            $row[] = $data;
            $row[] = escreverDataSoma($DateBegin, "+" . $i . " days", "N");
            $output['aCalendar'][] = $row;
            $linhaX .= ",isnull(SUM(case when cast(x.data_tele as date) = " .  valoresData2($data) . " then 1 else 0 end),0) dd" . $i . " ";
        }
    }
}

$sQuery = "set dateformat dmy;";
if ($imprimir == 0) {
    $u1 = "u1";
    $u2 = "u2";
    $sQuery .= "SELECT x.id_usuario,x.nome, count(" . ($for == "1" ? "pessoa" : "cast(x.data_tele as date)") . ") total " . $linhaX . " from ( ";
    if ($for == "1") {
        $sQuery .= "SELECT x.id_usuario,x.nome,x.pessoa,cast(x.data_tele as date) data_tele from ( ";
    }
} else {
    $u1 = "u2";
    $u2 = "u1";
    if ($for == "1") {
        $sQuery .= "SELECT max(id_tele)id_tele,x.id_usuario,max(nome) nome,max(x.data_tele) data_tele,
        max(x.razao_social) razao_social, max(x.relato) relato,min(x.pendente) pendente, 
        max(x.especificacao) especificacao, min(x.proximo_contato) proximo_contato, max(x.id_usuario_p) id_usuario_p,
        max(x.nome_p) nome_p,x.pessoa ";
    } else {
        $sQuery .= "SELECT * ";
    }
    $sQuery .= "FROM(select ROW_NUMBER() OVER (order by data_tele asc) as row,* from ( ";
}
//0 - Dt.Chamado | 1 - Dt.Prazo
//Quem abriu o chamado
$sQuery .= "select id_tele,u1.id_usuario,u1.nome," . ($tpd == "0" ? "data_tele" : "proximo_contato") . " data_tele,razao_social,relato collate SQL_Latin1_General_CP1_CI_AS relato,pendente,especificacao,proximo_contato,u2.id_usuario id_usuario_p,u2.nome nome_p,t.pessoa
from sf_telemarketing t inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = t.pessoa
inner join sf_usuarios u1 on u1.id_usuario = t.de_pessoa 
left join sf_usuarios u2 on u2.id_usuario = t.para_pessoa
where " . ($tpd == "0" ? "data_tele" : "proximo_contato") . " between " . valoresDataHora2($DateBegin, "00:00:00") . " and " . valoresDataHora2($DateEnd, "23:59:59") . $where;
//Quem para quem foi encaminhado o chamado    
$sQuery .= "union select id_tele," . $u1 . ".id_usuario," . $u1 . ".nome," . ($tpd == "0" ? "data_tele" : "proximo_contato") . " data_tele,razao_social,relato collate SQL_Latin1_General_CP1_CI_AS relato,pendente,especificacao,proximo_contato," . $u2 . ".id_usuario id_usuario_p," . $u2 . ".nome nome_p,t.pessoa
from sf_telemarketing t inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = t.pessoa
inner join sf_usuarios u2 on u2.id_usuario = t.de_pessoa 
inner join sf_usuarios u1 on u1.id_usuario = t.para_pessoa
where " . ($tpd == "0" ? "data_tele" : "proximo_contato") . " between " . valoresDataHora2($DateBegin, "00:00:00") . " and " . valoresDataHora2($DateEnd, "23:59:59") . $where;
//Quem abriu uma descrição de chamado
if ($tpd == "0") {
    $sQuery .= "union select tm.id_tele,u1.id_usuario,u1.nome,data data_tele,razao_social,mensagem collate SQL_Latin1_General_CP1_CI_AS relato,pendente,especificacao,proximo_contato,u2.id_usuario id_usuario_p,u2.nome nome_p,t.pessoa  
    from sf_mensagens_telemarketing tm inner join sf_telemarketing t on t.id_tele = tm.id_tele
    inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = t.pessoa
    inner join sf_usuarios u1 on u1.id_usuario = tm.id_usuario_de 
    left join sf_usuarios u2 on u2.id_usuario = tm.id_usuario_para
    where data between " . valoresDataHora2($DateBegin, "00:00:00") . " and " . valoresDataHora2($DateEnd, "23:59:59") . $where;
    //Quem para quem foi encaminhado o detalhe do chamado
    $sQuery .= "union select tm.id_tele," . $u1 . ".id_usuario," . $u1 . ".nome,data data_tele,razao_social,mensagem collate SQL_Latin1_General_CP1_CI_AS relato,pendente,especificacao,proximo_contato," . $u2 . ".id_usuario id_usuario_p," . $u2 . ".nome nome_p,t.pessoa
    from sf_mensagens_telemarketing tm inner join sf_telemarketing t on t.id_tele = tm.id_tele
    inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = t.pessoa
    inner join sf_usuarios u2 on u2.id_usuario = tm.id_usuario_de 
    inner join sf_usuarios u1 on u1.id_usuario = tm.id_usuario_para
    where data between " . valoresDataHora2($DateBegin, "00:00:00") . " and " . valoresDataHora2($DateEnd, "23:59:59") . $where;
}
if ($imprimir == 0) {
    if ($for == "1") {
        $sQuery .= ") as x group by x.id_usuario,x.nome,x.pessoa,cast(x.data_tele as date)";
    }
    $sQuery .= ") as x group by x.id_usuario,x.nome order by 3 desc";
} else {
    $sQuery .= ") as x) as x WHERE x.row > " . $sLimit . " and x.row <= " . ($sLimit + $sQtd) . " ";
    if ($for == "1") {
        $sQuery .= "group by x.id_usuario,cast(x.data_tele as DATE),x.pessoa";
    }
}
//echo $sQuery;exit();

$cur = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur)) {
    $row = array();
    if ($imprimir == 0) {
        $row[] = utf8_encode($RFP['id_usuario']);
        $row[] = utf8_encode($RFP['nome']);
        $row[] = utf8_encode($RFP['total']);
        if ($dias > 0) {
            for ($i = 0; $i <= $dias; $i++) {
                $row[] = formataNumeros($RFP["dd" . $i . ""]);
            }
        }
    } elseif ($imprimir == 1) {
        $row[] = "<a title=\"Atendimentos do Cliente\" href=\"./../CRM/CRMForm.php?id=" . $RFP['pessoa'] . "\"><div id=\"formPQ\" style=\"color:#08c;\" title=\"" . escreverDataHora($RFP['data_tele']) . "\">" . escreverDataHora($RFP['data_tele']) . "</div></a>";
        $row[] = utf8_encode($RFP['razao_social']);
        $row[] = legenda($RFP['especificacao']);
        $row[] = ($RFP['ord'] == 0 ? utf8_encode($RFP['nome']) : utf8_encode($RFP['nome_p']));
        $row[] = "<center>" . ($RFP['pendente'] == 1 ? utf8_encode("SIM") : utf8_encode("NÃO")) . "</center>";
        $row[] = ($RFP['ord'] == 0 ? utf8_encode($RFP['nome_p']) : utf8_encode($RFP['nome']));
        $row[] = utf8_encode($RFP['relato']);
    }
    $output['aaData'][] = $row;
    $total ++;
}
$output['iTotalRecords'] = $total;
$output['iTotalDisplayRecords'] = $total;
echo json_encode($output);
odbc_close($con);
