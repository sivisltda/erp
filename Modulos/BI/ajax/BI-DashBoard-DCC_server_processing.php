<?php

require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../util/util.php');
require_once(__DIR__ . '/../../../util/excel/exportaExcel.php');

$ano = $_GET["year"];
$output = '';

function verificaData($data) {
    $mesCurrent = date('m');
    $now = new DateTime($data);
    $month = $now->format('m');
    if ($month == $mesCurrent) {
        return date("Y/m/d", strtotime("last day of previous month"));
    } else {
        return $data;
    }
}

//ultimo dia do mÊs current
$ultimoDia = new DateTime('last day of this month');
$ultimoDia = date_add($ultimoDia, date_interval_create_from_date_string('0 month'));
$ultimoDia = $ultimoDia->format('d/m/Y');

//dia atual menos 1
$diaAtualmenoum = new DateTime('');
$diaAtualmenoum = date_add($diaAtualmenoum, date_interval_create_from_date_string('-1 day'));
$diaAtualmenoum = $diaAtualmenoum->format('d/m/Y');

//primeiro dia esse mês
$diaprimeiro = new DateTime('first day of this month');
$diaprimeiro = date_add($diaprimeiro, date_interval_create_from_date_string('0 month'));
$diaprimeiro = $diaprimeiro->format('d/m/Y');

//primeiro dia do mÊs passado
$diaprimeiroPass = new DateTime('first day of this month');
$diaprimeiroPass = date_add($diaprimeiroPass, date_interval_create_from_date_string('-1 month'));
$diaprimeiroPass = $diaprimeiroPass->format('d/m/Y');


//dia atual porem mês passado // melhorar isso
$diaPassado = new DateTime();
$diaPassado = date_add($diaPassado, date_interval_create_from_date_string('-30 day'));
$diaPassado = $diaPassado->format('Y/m/d');
$diaPassado = verificaData($diaPassado);
$diaPassado = date("d/m/Y", strtotime($diaPassado));
/* $diaPassado = new DateTime();
  $diaPassado = date_add($diaPassado, date_interval_create_from_date_string('-30 day'));
  $diaPassado = $diaPassado->format('d/m/Y'); */

//dia atual
$diaHoje = new DateTime();
$diaHoje = date_add($diaHoje, date_interval_create_from_date_string('0 day'));
$diaHoje = $diaHoje->format('d/m/Y');



if (isset($_GET["year"])) {
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "TotalTransacao" => 0,
        "iTotalDisplayRecords" => 0,
        "VolumeTransacao" => array(),
        "TransacaoNegMes" => array(),
        "TransacaoNegAno" => array()
    );

    $query = "set dateformat dmy;select * from dbo.sf_vendas_itens_dcc where cast(transactionTimestamp as date) between  '01/01/" . $ano . "' and '31/12/" . $ano . "' and status_transacao in ('A', 'E', 'D')";
    $cur = odbc_exec($con, $query);
    $total = 0;
    $meses = array();
    $mesesNegados = array();

    while ($RFP = odbc_fetch_array($cur)) {

        $dataExplod = escreverData($RFP["transactionTimestamp"], "d");
        if ($dataExplod == '01') {
            $meses[0][] = array("Status" => utf8_decode($RFP['status_transacao']), "Mensagem" => utf8_decode($RFP['processorMessage']));
        } else if ($dataExplod == '02') {
            $meses[1][] = array("Status" => utf8_decode($RFP['status_transacao']), "Mensagem" => utf8_decode($RFP['processorMessage']));
        } else if ($dataExplod == '03') {
            $meses[2][] = array("Status" => utf8_decode($RFP['status_transacao']), "Mensagem" => utf8_decode($RFP['processorMessage']));
        } else if ($dataExplod == '04') {
            $meses[3][] = array("Status" => utf8_decode($RFP['status_transacao']), "Mensagem" => utf8_decode($RFP['processorMessage']));
        } else if ($dataExplod == '05') {
            $meses[4][] = array("Status" => utf8_decode($RFP['status_transacao']), "Mensagem" => utf8_decode($RFP['processorMessage']));
        } else if ($dataExplod == '06') {
            $meses[5][] = array("Status" => utf8_decode($RFP['status_transacao']), "Mensagem" => utf8_decode($RFP['processorMessage']));
        } else if ($dataExplod == '07') {
            $meses[6][] = array("Status" => utf8_decode($RFP['status_transacao']), "Mensagem" => utf8_decode($RFP['processorMessage']));
        } else if ($dataExplod == '08') {
            $meses[7][] = array("Status" => utf8_decode($RFP['status_transacao']), "Mensagem" => utf8_decode($RFP['processorMessage']));
        } else if ($dataExplod == '09') {
            $meses[8][] = array("Status" => utf8_decode($RFP['status_transacao']), "Mensagem" => utf8_decode($RFP['processorMessage']));
        } else if ($dataExplod == '10') {
            $meses[9][] = array("Status" => utf8_decode($RFP['status_transacao']), "Mensagem" => utf8_decode($RFP['processorMessage']));
        } else if ($dataExplod == '11') {
            $meses[10][] = array("Status" => utf8_decode($RFP['status_transacao']), "Mensagem" => utf8_decode($RFP['processorMessage']));
        } else if ($dataExplod == '12') {
            $meses[11][] = array("Status" => utf8_decode($RFP['status_transacao']), "Mensagem" => utf8_decode($RFP['processorMessage']));
        }

        if ($RFP['status_transacao'] == 'D') {
            if ($dataExplod == '01') {
                $mesesNegados[0][] = array("Status" => utf8_decode($RFP['status_transacao']), "Code" => utf8_decode($RFP['processorCode']));
            } else if ($dataExplod == '02') {
                $mesesNegados[1][] = array("Status" => utf8_decode($RFP['status_transacao']), "Code" => utf8_decode($RFP['processorCode']));
            } else if ($dataExplod == '03') {
                $mesesNegados[2][] = array("Status" => utf8_decode($RFP['status_transacao']), "Code" => utf8_decode($RFP['processorCode']));
            } else if ($dataExplod == '04') {
                $mesesNegados[3][] = array("Status" => utf8_decode($RFP['status_transacao']), "Code" => utf8_decode($RFP['processorCode']));
            } else if ($dataExplod == '05') {
                $mesesNegados[4][] = array("Status" => utf8_decode($RFP['status_transacao']), "Code" => utf8_decode($RFP['processorCode']));
            } else if ($dataExplod == '06') {
                $mesesNegados[5][] = array("Status" => utf8_decode($RFP['status_transacao']), "Code" => utf8_decode($RFP['processorCode']));
            } else if ($dataExplod == '07') {
                $mesesNegados[6][] = array("Status" => utf8_decode($RFP['status_transacao']), "Code" => utf8_decode($RFP['processorCode']));
            } else if ($dataExplod == '08') {
                $mesesNegados[7][] = array("Status" => utf8_decode($RFP['status_transacao']), "Code" => utf8_decode($RFP['processorCode']));
            } else if ($dataExplod == '09') {
                $mesesNegados[8][] = array("Status" => utf8_decode($RFP['status_transacao']), "Code" => utf8_decode($RFP['processorCode']));
            } else if ($dataExplod == '10') {
                $mesesNegados[9][] = array("Status" => utf8_decode($RFP['status_transacao']), "Code" => utf8_decode($RFP['processorCode']));
            } else if ($dataExplod == '11') {
                $mesesNegados[10][] = array("Status" => utf8_decode($RFP['status_transacao']), "Code" => utf8_decode($RFP['processorCode']));
            } else if ($dataExplod == '12') {
                $mesesNegados[11][] = array("Status" => utf8_decode($RFP['status_transacao']), "Code" => utf8_decode($RFP['processorCode']));
            }
        }

        $total = $total + 1;
    }
    $output["VolumeTransacao"] = $meses;
    $output["TransacaoNegMes"] = $mesesNegados;
    $output["TotalTransacao"] = $total;
}

if (isset($_GET['cobrancas'])) {
    $noPrazo = 0;
    $foraPrazo = 0;
    $totNeg = 0;
    $query = "set dateformat dmy;select sum(case when dt_inicio_mens BETWEEN '" . $diaHoje . "' and '" . $ultimoDia . "' then 1 else 0 end) totPrazo, sum(case when dt_inicio_mens BETWEEN '" . $diaprimeiro . "' and '" . $diaAtualmenoum . "' then 1 else 0 end) totAberto,
    (select COUNT(*) from dbo.sf_vendas_itens_dcc where status_transacao = 'D'
      and transactionTimestamp BETWEEN '" . $diaprimeiro . "' and '" . $ultimoDia . "') totNeg
     from sf_vendas_planos_mensalidade as vpm inner join sf_produtos_parcelas as pp on pp.id_parcela = vpm.id_parc_prod_mens where dt_pagamento_mens is null and parcela = 0";
    //echo $query;exit();
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $noPrazo = $RFP["totPrazo"];
        $foraPrazo = $RFP["totAberto"];
        $totNeg = $RFP["totNeg"];
    }

    $output = array(
        "noPrazo" => $noPrazo,
        "foraPrazo" => $foraPrazo,
        "totNeg" => $totNeg
    );
}

if (isset($_GET['recebido'])) {
    $passado = '';
    $atual = '';
    $query = "set dateformat dmy;select sum(case when cast(transactionTimestamp as date) BETWEEN cast('" . $diaprimeiro . "' as date) and cast('" . $diaHoje . "' as date) then valor_transacao else 0 end) mesAtual, sum(case when cast(transactionTimestamp as date) BETWEEN cast('" . $diaprimeiroPass . "' as date) and cast('" . $diaPassado . "' as date) then valor_transacao else 0 end) mesPassado from dbo.sf_vendas_itens_dcc where processorMessage = 'APPROVED' and status_transacao = 'A'";
    //echo $query;exit();
    $diferença = '';
    $porcentagem = '';
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $passado = escreverNumero($RFP["mesPassado"]);
        $atual = escreverNumero($RFP["mesAtual"]);
        $diferença = escreverNumero($RFP["mesAtual"] - $RFP["mesPassado"]);
        $porcentagem = escreverNumero(($RFP["mesAtual"] / $RFP["mesPassado"]));
    }

    $output = array(
        "mesPassado" => $passado,
        "mesAtual" => $atual,
        "diferenca" => $diferença,
        "porcent" => $porcentagem
    );
}

if (isset($_GET['countAtivos'])) {
    $passado = '';
    $atual = '';
    $porcentagem = '';
    $query = "set dateformat dmy;select sum(case when cast(transactionTimestamp as date) BETWEEN cast('" . $diaprimeiro . "' as date) and cast('" . $diaHoje . "' as date) then 1 else 0 end) mesAtual,
  sum(case when cast(transactionTimestamp as date) BETWEEN cast('" . $diaprimeiroPass . "' as date) and cast('" . $diaPassado . "' as date) then 1 else 0 end) mesPassado
  from dbo.sf_vendas_itens_dcc where processorMessage = 'APPROVED' and status_transacao = 'A'";
    //echo $query;exit();
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $passado = $RFP["mesPassado"];
        $atual = $RFP["mesAtual"];
        $porcentagem = escreverNumero(($RFP["mesAtual"] / $RFP["mesPassado"]));
    }

    $output = array(
        "mesPassado" => $passado,
        "mesAtual" => $atual,
        "porcent" => $porcentagem
    );
}

//ultimos pagamentos feito nos ultimos 30 dias atrás
if (isset($_GET['ultimosPagmts'])) {
    $total = array();
    $valor = '';
    $query = "set dateformat dmy;select SUM(valor_transacao) as total,cast(transactionTimestamp as date) as dataPgto from sf_vendas_itens_dcc group by cast(transactionTimestamp as date) having cast(transactionTimestamp as date) BETWEEN '" . $diaPassado . "' and '" . $diaHoje . "'";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $array = array();
        $array["valor"] = intval($RFP["total"]);
        $array["data"] = escreverData($RFP["dataPgto"], "d/m");
        $total[] = $array;
    }

    $output = $total;
}

echo json_encode($output);
odbc_close($con);
