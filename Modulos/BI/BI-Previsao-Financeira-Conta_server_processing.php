<?php

include './../../Connections/configini.php';

$dtHoje = date("Y-m-d");
$dtInicio = getData("B");
$dtFim = date("t/m/Y", strtotime("+1 year"));
$tipo_pag = 0;

$sql = "select adm_mens_acess_soma from sf_configuracao";   
$cur = odbc_exec($con, $sql) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {    
    $tipo_pag = $RFP['adm_mens_acess_soma'];
}

if ($tipo_pag > 0) {
    $sql = "update sf_vendas_planos_mensalidade set valor_acessorios = dbo.FU_VALOR_ACESSORIOS(id_plano_mens), valor_real = dbo.VALOR_REAL_MENSALIDADE(id_mens) 
    where id_mens in (select top 1000 id_mens from sf_vendas_planos_mensalidade where valor_real is null)";
    odbc_exec($con, $sql) or die(odbc_errormsg());
}

if (is_numeric($_GET['ano'])) {
    $dtHoje = date($_GET['ano'] . "-m-d");    
    $dtInicio = date("01/m/" . $_GET['ano']);
    $dtFim = date('d/m/Y', strtotime("+1 year", strtotime($dtHoje)));
}

if (is_numeric($_GET['filial'])) {
    $where = "and empresa = " . $_GET['filial'];
}

for ($i = 0; $i < 12; $i++) {
    $aux[$i] = date("m", strtotime("+" . $i . " month", strtotime($dtHoje))) . date("Y", strtotime("+" . $i . " month", strtotime($dtHoje)));
    $lit = array("Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez");
    $num = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
    $myArray[$i]['mes'] = str_replace($num, $lit, date("m", strtotime("+" . $i . " month", strtotime($dtHoje)))) . "/" . date("Y", strtotime("+" . $i . " month", strtotime($dtHoje)));
    $myArray[$i]['receber'] = 0.00;
    $myArray[$i]['preceber'] = 0.00;
    $myArray[$i]['pagar'] = 0.00;
}

$sQuery = "set dateformat dmy;
select YEAR(dt_inicio_mens) ano,MONTH(dt_inicio_mens) mes, sum(" . ($tipo_pag > 0 ? "valor_real" : "dbo.VALOR_REAL_MENSALIDADE(id_mens)") . ") total 
from sf_fornecedores_despesas inner join sf_vendas_planos on id_fornecedores_despesas = favorecido 
inner join sf_vendas_planos_mensalidade on id_plano_mens = id_plano
where id_fornecedores_despesas > 0 and sf_fornecedores_despesas.tipo = 'C' 
and inativo = 0 and dt_cancelamento is null " . $where . " and dt_pagamento_mens is null 
and dt_inicio_mens between '" . $dtInicio . "' and '" . $dtFim . "'
group by YEAR(dt_inicio_mens),MONTH(dt_inicio_mens)";
$cur = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur)) {
    $key = array_search($RFP['mes'] . $RFP['ano'], $aux);
    if (is_numeric($key)) {
        $myArray[$key]['preceber'] = $RFP['total'];
    }
}

$sQuery = "set dateformat dmy;
select ano,mes,SUM(x.pagar) pagar,SUM(x.receber) receber from (
select YEAR(data_vencimento) ano,MONTH(data_vencimento) mes,
isnull(SUM(case when tipo = 'D' then valor_parcela end),0) pagar,isnull(SUM(case when tipo = 'C' then valor_parcela end),0) receber 
from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento on sf_lancamento_movimento.id_lancamento_movimento = sf_lancamento_movimento_parcelas.id_lancamento_movimento
inner join sf_contas_movimento on sf_contas_movimento.id_contas_movimento = sf_lancamento_movimento.conta_movimento
inner join sf_tipo_documento td on sf_lancamento_movimento_parcelas.tipo_documento = td.id_tipo_documento
where sf_lancamento_movimento_parcelas.inativo = 0 and sf_lancamento_movimento.status = 'Aprovado' 
and data_pagamento is null and data_vencimento between '" . $dtInicio . "' and '" . $dtFim . "' " . $where . "
group by YEAR(data_vencimento),MONTH(data_vencimento)
union all
select YEAR(data_parcela),MONTH(data_parcela),
isnull(SUM(case when tipo = 'D' then valor_parcela end),0) pagar,isnull(SUM(case when tipo = 'C' then valor_parcela end),0) receber 
from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao on sf_solicitacao_autorizacao.id_solicitacao_autorizacao = sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao
inner join sf_contas_movimento on sf_contas_movimento.id_contas_movimento = sf_solicitacao_autorizacao.conta_movimento
inner join sf_tipo_documento td on sf_solicitacao_autorizacao_parcelas.tipo_documento = td.id_tipo_documento
where sf_solicitacao_autorizacao_parcelas.inativo = 0 and status = 'Aprovado' 
and data_pagamento is null and data_parcela between '" . $dtInicio . "' and '" . $dtFim . "' " . $where . "
group by YEAR(data_parcela),MONTH(data_parcela)
union all
select YEAR(data_parcela),MONTH(data_parcela),
isnull(SUM(case when cov = 'C' then valor_parcela end),0) pagar,isnull(SUM(case when cov = 'V' then valor_parcela end),0) receber 
from sf_venda_parcelas inner join sf_vendas on sf_vendas.id_venda = sf_venda_parcelas.venda
inner join sf_tipo_documento td on sf_venda_parcelas.tipo_documento = td.id_tipo_documento
where sf_venda_parcelas.inativo = 0 and sf_vendas.status = 'Aprovado' 
and data_pagamento is null and data_parcela between '" . $dtInicio . "' and '" . $dtFim . "' " . $where . "
group by YEAR(data_parcela),MONTH(data_parcela)) as x
group by x.ano, x.mes order by x.ano, x.mes";
$cur = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur)) {
    $key = array_search($RFP['mes'] . $RFP['ano'], $aux);
    if (is_numeric($key)) {
        $row = array();
        $row["mes"] = $myArray[$key]['mes'];
        $row["receber"] = $RFP['receber'];
        $row["preceber"] = $myArray[$key]['preceber'];
        $row["pagar"] = $RFP['pagar'];
        $myArray[$key] = $row;
    }
}
echo json_encode($myArray);
odbc_close($con);