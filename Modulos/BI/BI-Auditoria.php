<?php include "../../Connections/configini.php";
$DateBegin = getData("B");
$DateEnd = getData("E");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>                
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">      
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1>SIVIS B.I.<small>Auditoria</small></h1>                    
                    </div>
                    <div class="row-fluid">
                        <input id="txtValue" name="txtValue" type="hidden" value="">
                        Tipo: 
                        <select id="txt_tipo" name="txt_tipo" style="width: 225px;">
                            <option value="null">Selecione</option>
                            <option value="6">Abonos</option>                            
                            <option value="12">Agendamento</option>                            
                            <option value="25">Alteração de Grupo de Clientes</option>                                                        
                            <option value="14">Alteração de Responsável</option>                            
                            <option value="3">Alteração de Valores de Mensalidades</option>
                            <?php if ($mdl_seg_ > 0) { ?>
                            <option value="17">Assistência 24h</option>
                            <?php } ?>                   
                            <option value="24">Bloqueio de Clientes</option>                            
                            <option value="18">Boletos</option>                                         
                            <option value="16">Cancelamentos</option>                            
                            <option value="9">Cancelamento de Multa</option>
                            <option value="8">Cartão DCC</option>                            
                            <option value="5">Convênios</option>                            
                            <option value="7">Credenciais</option>                            
                            <option value="10">C.Receber Parcelas Excluídas</option>                            
                            <option value="11">C.Pagar Parcelas Excluídas</option>                            
                            <?php if ($mdl_clb_ > 0) { ?>
                            <option value="13">Desligar Titularidade</option>                        
                            <?php } ?>                            
                            <option value="0">Descontos</option>                            
                            <option value="1">Estorno</option>
                            <option value="23">Exclusão de Clientes</option>                            
                            <option value="20">Exclusão de Dependentes</option>
                            <?php if ($mdl_seg_ > 0) { ?>
                            <option value="28">Exclusão de Veículos</option>                            
                            <option value="21">Exclusão de Vistoria</option>                                                        
                            <?php } ?>
                            <option value="27">Exclusão de Requisição</option>
                            <option value="2">Mensalidades Excluídas</option>
                            <option value="26">Rastreamento</option>                            
                            <?php if ($mdl_tur_ > 0) { ?>
                            <option value="15">Turmas</option>
                            <?php } ?>                                                                                                                
                            <?php if ($mdl_seg_ > 0) { ?>                            
                            <option value="22">Veículos Reprovados</option>
                            <?php } ?>                                                                                                                
                            <option value="4">Vendas</option>
                            <option value="19">Vendas Retroativas</option>                            
                        </select>
                        De <span id="sprytextfield1"><input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial"></span> 
                        até <span id="sprytextfield2"><input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final"></span>
                        <span>Cliente:</span>
                        <input style="width: 160px;vertical-align: top;color:#000 !important;" type="text" name="txtDesc" id="txtDesc" class="inputbox" value="" size="10" />
                        <span>Usuário:</span>
                        <select name="itemsPlano[]" id="mscPlano" multiple="multiple" class="select" style="min-width:200px" class="input-medium">
                            <?php $cur = odbc_exec($con, "select * from sf_usuarios where inativo = 0") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP['login_user'] ?>"><?php echo utf8_encode($RFP['login_user']) ?></option>
                            <?php } ?>
                        </select>
                        <input type="button" title="Buscar" class="button button-turquoise btn-primary" style="line-height:17px" name="btnfind" id="btnfind" value="Buscar">
                        <hr style="margin: 10px 0">
                        <div class="boxhead">
                            <div class="boxtext" style="position:relative">
                                Auditoria
                                <div style="top:4px; right:2px; line-height:1; position:absolute">
                                    <button type="button" title="Imprimir" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printPDF('I');">
                                        <span class="ico-print icon-white"></span>
                                    </button>
                                    <button type="button" title="Exportar Excel" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printPDF('E');">
                                        <span class="ico-download-3 icon-white"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="boxtable">
                            <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="example">
                                <thead>
                                    <tr>
                                        <th width="7%"><center>Cod.</center></th>
                                        <th width="28%"><center>Cliente</center></th>
                                        <th width="12%">Data</th>
                                        <th width="43%">Descrição</th>                           
                                        <th width="10%"><center>Usuario</center></th>                                            
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            <div style="clear:both"></div>                                
                        </div>  
                    </div>
                </div>
            </div>
        </div>              
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>        
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>        
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script> 
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>   
        <script type='text/javascript' src='js/BI-Auditoria.js'></script>
    </body>
    <?php odbc_close($con); ?>
</html>