<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include "../../Connections/configini.php";

$id_area = str_replace("'", "", utf8_decode($_REQUEST['txtGrupo']));
if ($_REQUEST['txtMetodo'] != "") {
    $id_tipo = str_replace("'", "", utf8_decode($_REQUEST['txtMetodo']));
} else {
    $id_tipo = "P";
}
if ($_REQUEST['txtTabelaPreco'] == "S") {
    $comando = " AND (tabela_preco_nao is null or tabela_preco_nao = 0)";
} else {
    $comando = "";
}
$query = "select ISNULL(filial, (SELECT id_filial FROM dbo.sf_filiais where id_filial = 1)) filial from dbo.sf_usuarios where login_user = '" . $_SESSION["login_usuario"] . "'";
$cur = odbc_exec($con, $query) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $idFilial = $RFP['filial'];
}

$local = array();
if (is_numeric($id_area)) {
    if ($id_area > 0) {
        if ($id_area < 999999) {
            $sql = "select dbo.ESTOQUE_FILIAL(sf_produtos.conta_produto,0) as filiais, conta_produto,conta_movimento,descricao,preco_venda, nao_vender_sem_estoque, REPLICATE('0',14-LEN(codigo_barra)) + codigo_barra codigo_barra,
                            (select SUM(quantidade) from sf_vendas_itens vi inner join sf_vendas v on vi.id_venda = v.id_venda where cov = 'V' and status = 'Aprovado' and vi.produto = sf_produtos.conta_produto and v.empresa = " . $idFilial . ") saidas,
                            (select SUM(quantidade) from sf_vendas_itens vi inner join sf_vendas v on vi.id_venda = v.id_venda where cov = 'C' and status = 'Aprovado' and vi.produto = sf_produtos.conta_produto and v.empresa = " . $idFilial . ") entradas,estoque_minimo,sem_entrada,
                            (select COUNT(id_materiaprima) from sf_produtos_materiaprima where id_produtomp = conta_produto) pacote
                            from sf_produtos where conta_produto > 0 " . $comando . " AND conta_movimento = " . $id_area . " AND INATIVA = 0 ORDER BY conta_produto";
        } else {
            $sql = "select dbo.ESTOQUE_FILIAL(sf_produtos.conta_produto,0) as filiais, conta_produto,conta_movimento,descricao,preco_venda, nao_vender_sem_estoque, REPLICATE('0',14-LEN(codigo_barra)) + codigo_barra codigo_barra,
                            (select SUM(quantidade) from sf_vendas_itens vi inner join sf_vendas v on vi.id_venda = v.id_venda where cov = 'V' and status = 'Aprovado' and vi.produto = sf_produtos.conta_produto and v.empresa = " . $idFilial . ") saidas,
                            (select SUM(quantidade) from sf_vendas_itens vi inner join sf_vendas v on vi.id_venda = v.id_venda where cov = 'C' and status = 'Aprovado' and vi.produto = sf_produtos.conta_produto and v.empresa = " . $idFilial . ") entradas,estoque_minimo,sem_entrada,
                            (select COUNT(id_materiaprima) from sf_produtos_materiaprima where id_produtomp = conta_produto) pacote
                            from sf_produtos where conta_produto > 0 " . $comando . " AND tipo = '" . $id_tipo . "' AND INATIVA = 0 AND codigo_barra = '" . $id_area . "' ORDER BY conta_produto";
        }
    } else {
        $sql = "select dbo.ESTOQUE_FILIAL(sf_produtos.conta_produto,0) as filiais, conta_produto,conta_movimento,descricao,preco_venda, nao_vender_sem_estoque, REPLICATE('0',14-LEN(codigo_barra)) + codigo_barra codigo_barra,
                        (select SUM(quantidade) from sf_vendas_itens vi inner join sf_vendas v on vi.id_venda = v.id_venda where cov = 'V' and status = 'Aprovado' and vi.produto = sf_produtos.conta_produto and v.empresa = " . $idFilial . ") saidas,
                        (select SUM(quantidade) from sf_vendas_itens vi inner join sf_vendas v on vi.id_venda = v.id_venda where cov = 'C' and status = 'Aprovado' and vi.produto = sf_produtos.conta_produto and v.empresa = " . $idFilial . ") entradas,estoque_minimo,sem_entrada,
                        (select COUNT(id_materiaprima) from sf_produtos_materiaprima where id_produtomp = conta_produto) pacote
                        from sf_produtos where conta_produto > 0 " . $comando . " AND tipo = '" . $id_tipo . "' AND INATIVA = 0 AND favorito = 1 ORDER BY conta_produto";
    }
} else {
    $sql = "select dbo.ESTOQUE_FILIAL(sf_produtos.conta_produto,0) as filiais, conta_produto,conta_movimento,descricao,preco_venda, nao_vender_sem_estoque, REPLICATE('0',14-LEN(codigo_barra)) + codigo_barra codigo_barra,
                    (select SUM(quantidade) from sf_vendas_itens vi inner join sf_vendas v on vi.id_venda = v.id_venda where cov = 'V' and status = 'Aprovado' and vi.produto = sf_produtos.conta_produto and v.empresa = " . $idFilial . ") saidas,
                    (select SUM(quantidade) from sf_vendas_itens vi inner join sf_vendas v on vi.id_venda = v.id_venda where cov = 'C' and status = 'Aprovado' and vi.produto = sf_produtos.conta_produto and v.empresa = " . $idFilial . ") entradas,estoque_minimo,sem_entrada, 
                    (select COUNT(id_materiaprima) from sf_produtos_materiaprima where id_produtomp = conta_produto) pacote
                    from sf_produtos where conta_produto > 0 " . $comando . " AND tipo = '" . $id_tipo . "' AND INATIVA = 0 AND descricao LIKE '%" . $id_area . "%' ORDER BY conta_produto";
}
$cur = odbc_exec($con, $sql) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $img = "";
    $qtdDisponivel = "-";
    if (file_exists("./../../Produtos/" . $contrato . "/" . $RFP['conta_produto'] . ".png")) {
        $img = "./../../Produtos/" . $contrato . "/" . $RFP['conta_produto'] . ".png";
    } else {
        $img = "./../../img/img.png";
    }
    if ($aRow['sem_entrada'] == 0) {
        $qtdDisponivel = $RFP['entradas'] - $RFP['saidas'];
    }
    $local[] = array('id_contas_movimento' => $RFP['conta_produto'], 'grupo' => $RFP['conta_movimento'], 'descricao' => utf8_encode($RFP['descricao']), 'preco_venda' => utf8_encode($RFP['preco_venda']), 'img' => $img, 'qtdDisponivel' => $qtdDisponivel, 'naoVenderSemEstoque' => $RFP['nao_vender_sem_estoque'], 'filiais' => $RFP['filiais'], 'pacote' => $RFP['pacote'], 'codigo_barra' => $RFP['codigo_barra']);
}
echo(json_encode($local));
odbc_close($con);
