<?php

require_once(__DIR__ . '/../../Connections/configini.php');
$aColumns = array('id_requisicao', 'empresa', 'nome_departamento', 'historico_venda', 'responsavel', 'data_venda', 'destinatario_cotacao');
$table = "sf_requisicao r left join sf_departamentos d on r.id_departamento = d.id_departamento";
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = " AND cov = 'R' AND status = 'Aprovado' ";
$sWhere = "";
$sOrder = " ORDER BY " . $aColumns[0] . " asc ";
$sOrder2 = " ORDER BY " . $aColumns[0] . " asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (is_numeric($_GET['filial'])) {
    $sWhereX .= " AND empresa = " . $_GET['filial'];
}

if (isset($_GET['id']) && $_GET['id'] != 'null') {
    $sWhereX .= " and (";
    $dataAux = explode(",", $_GET['id']);
    for ($i = 0; $i < count($dataAux); $i++) {
        $sWhereX .= ($i > 0 ? " or " : "");
        if ($dataAux[$i] == "1") {
            $sWhereX .= "(status_cotacao = 'Aguarda')";
        } elseif ($dataAux[$i] == "2") {
            $sWhereX .= "(status_cotacao = 'Aprovado')";
        } elseif ($dataAux[$i] == "3") {
            $sWhereX .= "(status_cotacao = 'Reprovado')";
        } elseif ($dataAux[$i] == "4") {
            $sWhereX .= "(status_cotacao is null)";        
        } elseif ($dataAux[$i] == "5") {
            $sWhereX .= "(inativo = 1)";        
        }
    }
    $sWhereX .= ")";
}

if (is_numeric($_GET['tp']) && isset($_GET['td'])) {
    if ($_GET['tp'] == "0") {
        $sWhere .= " and responsavel = " . valoresTexto2($_GET['td']);
    } elseif ($_GET['tp'] == "1") {
        $sWhere .= " and destinatario = " . valoresTexto2($_GET['td']);
    } elseif ($_GET['tp'] == "2") {
        $sWhere .= " and r.id_departamento = " . valoresNumericos2($_GET['td']);
    }
}

if (is_numeric($_GET['cp']) && isset($_GET['cp'])) {
    if ($_GET['cp'] == "0") {
        $sWhere .= " and id_requisicao in (select id_requisicao from sf_requisicao_cotacao where id_venda is not null) ";
    } elseif ($_GET['cp'] == "1") {
        $sWhere .= " and id_requisicao not in (select id_requisicao from sf_requisicao_cotacao where id_venda is not null) ";
    }
}

if ($_GET['dti'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
    $sWhere .= " AND data_venda >= " . valoresDataHora2($DateBegin, "00:00:00");
}

if ($_GET['dtf'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
    $sWhere .= " AND data_venda <= " . valoresDataHora2($DateEnd, "23:59:59");
}

if ($_GET['txtBusca'] != "") {
    $sWhereX .= " and (historico_venda like '%" . $_GET['txtBusca'] . "%')";
}

$sQuery = "set dateformat dmy;
SELECT COUNT(*) total from " . $table . " where " . $aColumns[0] . " > 0 $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "set dateformat dmy;
SELECT COUNT(*) total from " . $table . " where " . $aColumns[0] . " > 0 $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

for ($i = 0; $i < count($aColumns); $i++) {
    if ($i == 0) {
        $colunas = $aColumns[$i];
    } else {
        $colunas = $colunas . "," . $aColumns[$i];
    }
}

$sQuery1 = "set dateformat dmy;
SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row," . $colunas . ", isnull(status_cotacao, 'Novo') status_cotacao
from " . $table . " where " . $aColumns[0] . " > 0 " . $sWhereX . $sWhere . ") as a 
WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder2;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<center><a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow["id_requisicao"]) . ");'><div id='formPQ'>" . escreverDataHora($aRow["data_venda"]) . "</div></a></center>";
    $row[] = "<center>" . str_pad($aRow["empresa"], 3, 0, STR_PAD_LEFT) . "</center>";      
    $row[] = utf8_encode($aRow["responsavel"]);      
    $row[] = utf8_encode($aRow["nome_departamento"]);      
    $row[] = utf8_encode($aRow["historico_venda"]);      
    $row[] = utf8_encode($aRow["destinatario_cotacao"]);
    $color = "style=\"color:";
    if (utf8_encode($aRow['status_cotacao']) == 'Aguarda') {
        $color = $color . "#bc9f00\"";
    } elseif (utf8_encode($aRow['status_cotacao']) == 'Aprovado') {
        $color = $color . "#0066cc\"";
    } elseif (utf8_encode($aRow['status_cotacao']) == 'Reprovado') {
        $color = $color . "#f00\"";
    } elseif (utf8_encode($aRow['status_cotacao']) == 'Excluido') {
        $color = $color . "#f00\"";
    } else {
        $color = $color . "black\"";
    }    
    $row[] = "<div id='formPQ' " . $color . "><center>" . utf8_encode($aRow['status_cotacao']) . "</center></div>";
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
