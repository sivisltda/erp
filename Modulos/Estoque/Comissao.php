<?php
include '../../Connections/configini.php';
$mdl_aca_ = returnPart($_SESSION["modulos"], 6);
$mdl_seg_ = returnPart($_SESSION["modulos"], 14);
$F1 = '';
$F5 = 2;
$DateBegin = getData("B");
$DateEnd = getData("E");
$sendURL = "?imp=0";
if (is_numeric($_GET['imp']) && $_GET['imp'] == "1") {
    $sendURL = "?imp=1";
}
if ($_GET['id'] != "") {
    $F1 = $_GET['id'];
    $sendURL = $sendURL . "&id=" . $_GET['id'];
}
$txtUsuario = '';
if ($_GET['nmUser'] != "") {
    $txtUsuario = $_GET['nmUser'];
}

if (is_numeric($_GET['tp'])) {
    $F3 = $_GET['tp'];
    $sendURL = $sendURL . "&tp=" . $_GET['tp'];
}
if (is_numeric($_GET['pg'])) {
    $F5 = $_GET['pg'];
    $sendURL = $sendURL . "&pg=" . $_GET['pg'];
}
if ($_GET['dti'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
    $sendURL = $sendURL . "&dti=" . $_GET['dti'];
}
if ($_GET['dtf'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
    $sendURL = $sendURL . "&dtf=" . $_GET['dtf'];
}
$sendURL = $sendURL . "&fm=0&ts=3";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link rel="stylesheet" type="text/css" href="../../../SpryAssets/SpryValidationTextField.css"/>
        <link href="./../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <style>
            #s2id_mscPlano .select2-choices{
                min-height: 29px!important;
            }
            #formulario {
                float: left;
                width: 100%;
            }
            #formulario form .linha {
                float: left;
                height: 26px;
                width: 100%;
                display: block;
                border-bottom-width: 1px;
                border-bottom-style: solid;
                border-bottom-color: #f7f7f7;
                background-color: #f7f7f7;
                border-top-width: 1px;
                border-top-style: solid;
                border-top-color: #FFF;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            div.checker {
                top: 0px;
            }
            #tbReceitaAnalitico td { padding:5px 5px }
            #tbReceitaAnalitico td:nth-child(6), #tbReceita th:nth-child(6) { text-align:right }
            #tbReceitaAnalitico td:nth-child(7), #tbReceita th:nth-child(7) { text-align:right }
            #tbReceitaAnalitico td:nth-child(8), #tbReceita th:nth-child(8) { text-align:right }
            #tbReceitaAnalitico td:nth-child(9), #tbReceita th:nth-child(9) { text-align:right }
            #tbReceitaAnalitico td:nth-child(10), #tbReceita th:nth-child(10) { text-align:right }
            #tbReceitaAnalitico td:nth-child(11), #tbReceita th:nth-child(11) { text-align:right }
            <?php if ($mdl_aca_ == 1) { ?>
                #tbReceitaAnalitico td:nth-child(12), #tbReceita th:nth-child(12) { text-align:right }
                #tbReceitaAnalitico td:nth-child(13), #tbReceita th:nth-child(13) { text-align:right }
                #tbReceitaAnalitico td:nth-child(14), #tbReceita th:nth-child(14) { text-align:right;background-color: LightPink;font-weight: bold }
            <?php } else { ?>
                #tbReceitaAnalitico td:nth-child(12), #tbReceita th:nth-child(12) { text-align:right;background-color: LightPink;font-weight: bold }
            <?php } ?>

        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1>Financeiro<small>Comissões sobre Vendas</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="width:20%;margin-top: 15px;float:left;">
                                    <button name="bntBaixaLoad" title="Gerar Comissão" id="bntBaixaLoad" onclick="AbrirBox();" disabled="true" class="button button-green btn-primary" type="button" ><span class="icon-refresh icon-white"></span>Gerar Comissão</button>
                                    <button name="btnPrint" class="button button-blue btn-primary" type="button" onclick="AbrirBox(2);" title="Imprimir"><span class="ico-print"></span></button>
                                </div>
                                <div style="float:right;width:80%;display: flex;justify-content: flex-end;">
                                    <div style="float:left;width:22%;">
                                        <div width="100%">Grupo:</div>
                                        <select id="txtTipo">
                                            <?php if ($mdl_aca_ == 1 && $mdl_seg_ == 0) { ?>
                                                <option value="1" <?php
                                                if ($F3 == 1) {
                                                    echo "SELECTED";
                                                }
                                                ?>>Professor Responsável</option>
                                                <option value="2" <?php
                                                if ($F3 == 2) {
                                                    echo "SELECTED";
                                                }
                                                ?>>Usuario Responsável</option>
                                            <?php } else { ?>
                                                <option value="0" <?php
                                                if ($F3 == 0) {
                                                    echo "SELECTED";
                                                }
                                                ?>>Vendedores</option>
                                                <option value="1" <?php
                                                if ($F3 == 1) {
                                                    echo "SELECTED";
                                                }
                                                ?>>Comissionado</option>
                                                <option value="2" <?php
                                                if ($F3 == 2) {
                                                    echo "SELECTED";
                                                }
                                                ?>>Indicadores</option>
                                                <option value="3" <?php
                                                if ($F3 == 3) {
                                                    echo "SELECTED";
                                                }
                                                ?>>Gerentes</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div style="float:left;margin-left: 1%;width:22%;">
                                        <div width="100%">Nome:</div>
                                        <span id="carregando" name="carregando" style="color:#666;display:none">Aguarde, carregando...</span>
                                        <select id="txtUsuario">
                                            <option style="color: red;" value="0" >Selecione</option>
                                        </select>
                                    </div>
                                    <div style="float:left;margin-left: 1%;width:15%;">
                                        <div width="100%">Situação:</div>
                                        <select id="txtForma">
                                            <option value="0" <?php
                                            if ($F5 == 0) {
                                                echo "SELECTED";
                                            }
                                            ?>>Todas</option>
                                            <option value="1" <?php
                                            if ($F5 == 1) {
                                                echo "SELECTED";
                                            }
                                            ?>>Geradas</option>
                                            <option value="2" <?php
                                            if ($F5 == 2) {
                                                echo "SELECTED";
                                            }
                                            ?>>A Gerar</option>
                                            <option value="3" <?php
                                            if ($F5 == 3) {
                                                echo "SELECTED";
                                            }
                                            ?>>Aguardando</option>
                                            <option value="4" <?php
                                            if ($F5 == 4) {
                                                echo "SELECTED";
                                            }
                                            ?>>Reprovadas</option>
                                        </select>
                                    </div>
                                    <div style="float:left;margin-left: 1%;width:22%;">
                                        <div width="100%">Planos:</div>
                                        <select name="itemsPlano[]" id="mscPlano" multiple="multiple" class="select" style="width:100%;" class="input-medium">
                                            <?php
                                            $query = "select conta_produto, descricao from dbo.sf_produtos where tipo = 'C' and inativa = 0 and conta_produto > 0";
                                            $cur = odbc_exec($con, $query);
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['conta_produto']; ?>"><?php echo utf8_encode($RFP['descricao']); ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div style="float:left;margin-left: 1%;width:29%;">
                                        <div style="width: 100%;">Periodo.</div>
                                        <input name="txt_dt_begin" id="txt_dt_begin" maxlength="128" class="datepicker" type="text" style="width:40%;text-align:center;float: left;" value="<?php echo $DateBegin; ?>"/>
                                        <div style="float: left;padding: 5px;">até</div>
                                        <input name="txt_dt_end" id="txt_dt_end" maxlength="128" class="datepicker" type="text" style="width:40%; text-align:center" value="<?php echo $DateEnd; ?>" />
                                    </div>
                                    <div style="margin-top: 15px;float:left;margin-left:1%;">
                                        <div style="float:left">
                                            <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar" onclick="refresh();"><span class="ico-search icon-white"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead" <?php echo $visible; ?>>
                        <div class="boxtext">Comissões</div>
                    </div>
                    <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tbReceitaAnalitico">
                        <thead>
                            <tr>
                                <th width="2%" align="center"><input id="checkAll" type="checkbox" class="checkall" onchange="total();"/></th>
                                <th width="3%"><center>Código</center></th>
                                <th width="11%"><center>Dt.Venda</center></th>
                                <th width="9%">Comissionado</th>
                                <th width="20%">Cliente</th>
                                <th width="8%" style="text-align:right">Val.Prod.</th>
                                <th width="8%" style="text-align:right">Val.Serv.</th>
                                <?php if ($mdl_aca_ == 1) { ?>
                                    <th width="8%" style="text-align:right">Val.Plano.</th>
                                <?php } ?>
                                <th width="8%" style="text-align:right">Dsc.Prod.</th>
                                <th width="8%" style="text-align:right">Dsc.Serv.</th>
                                <th width="8%" style="text-align:right">C.Prod.</th>
                                <th width="8%" style="text-align:right">C.Serv.</th>
                                <?php if ($mdl_aca_ == 1) { ?>
                                    <th width="8%" style="text-align:right">C.Plano</th>
                                <?php } ?>
                                <th width="10%" style="padding:0px 10px; text-align:right">C.Total</th>
                                <th width="5%"><center>Aviso</center></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                            <tr>
                                <th colspan="5">TOTAL</th>
                                <th style="text-align:right;"><div id="txtValorP"><?php echo escreverNumero(0); ?></div></th>
                                <th style="text-align:right;"><div id="txtValorS"><?php echo escreverNumero(0); ?></div></th>
                                <?php if ($mdl_aca_ == 1) { ?>
                                    <th style="text-align:right;"><div id="txtValorPl"><?php echo escreverNumero(0); ?></div></th>
                                <?php } ?>
                                <th style="text-align:right;"><div id="txtDescontoP"><?php echo escreverNumero(0); ?></div></th>
                                <th style="text-align:right;"><div id="txtDescontoS"><?php echo escreverNumero(0); ?></div></th>
                                <th style="text-align:right;"><div id="txtTotalAcumuladoP"><?php echo escreverNumero(0); ?></div></th>
                                <th style="text-align:right;"><div id="txtTotalAcumuladoS"><?php echo escreverNumero(0); ?></div></th>
                                <?php if ($mdl_aca_ == 1) { ?>
                                    <th style="text-align:right;"><div id="txtTotalAcumuladoPl"><?php echo escreverNumero(0); ?></div></th>
                                <?php } ?>
                                <th style="text-align:right;"><div id="txtTotal"><?php echo escreverNumero(0); ?></div></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                    <table id="lblTotParc" name="lblTotParc" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="text-align: right">
                                <div id="lblTotParc2" name="lblTotParc2">
                                    Total selecionado :<strong><?php echo escreverNumero(0, 1); ?></strong>
                                </div>
                                <input id="totalSelecionado" type="hidden" value="0"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            
            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);

            function refresh() {
                if ($("#txtUsuario").val() === 0) {
                    bootbox.alert("Escolha um Comissionado!");
                    return;
                }
                $('#txtValorP').html(numberFormat(0));
            <?php if ($mdl_aca_ == 1) { ?>
                $('#txtValorPl').html(numberFormat(0));
                $('#txtTotalAcumuladoPl').html(numberFormat(0));
            <?php } ?>
                $('#txtValorS').html(numberFormat(0));
                $('#txtDescontoP').html(numberFormat(0));
                $('#txtDescontoS').html(numberFormat(0));
                $('#txtTotalAcumuladoP').html(numberFormat(0));
                $('#txtTotalAcumuladoS').html(numberFormat(0));
                $('#txtTotal').html(numberFormat(0));
                ListComissoes();
            }

            function validaForm() {
                if ($('#txtUsuario').val() === 0) {
                    bootbox.alert("Selecione um usuário para esta operação!");
                    return false;
                }
                return true;
            }
            
            function listaFiltro() {
                var filtros = "";
                filtros = "Comissões por: " + $("#txtTipo option:selected").text();
                if ($("#txtUsuario").val() !== "0") {
                    filtros += " - Usuario: " + $("#txtUsuario option:selected").text();
                }
                if ($("#txt_dt_begin").val() !== "") {
                    filtros += " - Periodo de " + $("#txt_dt_begin").val();
                }
                if ($("#txt_dt_end").val() !== "") {
                    filtros += " até " + $("#txt_dt_end").val();
                }
                return filtros;
            }
            
            function AbrirBox(opc) {
                <?php
                $columns = "";
                if ($mdl_aca_ == 1) {
                    $dcolumns = "Cod.|Dt.Venda|Comissionado|Cliente|Val.Prod|Val.Serv|Val.Plano|Dsc.Prod|Dsc.Serv|C.Prod|C.Serv|C.Plano|C.Total";
                    $tcolumns = "50|70|140|210|60|60|60|60|60|60|60|60";
                } else {
                    $dcolumns = "Cod.|Dt.Venda|Comissionado|Cliente|Val.Prod|Val.Serv|Dsc.Prod|Dsc.Serv|C.Prod|C.Serv|C.Total";
                    $tcolumns = "60|70|160|210|80|80|70|70|70|70";
                }
                ?>
                if (opc === 2) {
                    var pRel = "&NomeArq=" + "Comissoes" +
                            "&lbl=<?php echo $dcolumns; ?>" +
                            "&siz=<?php echo $tcolumns; ?>" +
                            "&pdf='" + "0" +
                            "&filter=" + listaFiltro() +
                            "&PathArqInclude=" + "../Modulos/Financeiro/" + finalFind(3).replace("?", "&"); // server processing
                    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&pOri=L&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
                } else {
                    if (validaForm()) {
                        var id = '';
                        $("input[name='items[]']:checked").each(function () {
                            id = id + $(this).val() + '|';
                        });
                        abrirTelaBox("FormComissao.php?id=" + id + "&us=" + $('#txtUsuario').val() + "&tp=" + $('#txtTipo').val() + "&vl=" + $('#totalSelecionado').val(), 280, 400);
                    }
                }
            }
            function FecharBox() {
                $("#newbox").remove();
                refresh();
            }
            function total() {
                var total = 0.00;
                $("input[name='items[]']:checked").each(function () {
                    total = total + parseFloat($("#" + $(this).val() + "").val());
                });
                if (total > 0.00) {
                    $('#bntBaixaLoad').prop('disabled', false);
                } else {
                    $('#bntBaixaLoad').prop('disabled', true);
                }
                $('#totalSelecionado').val(total);
                $('#lblTotParc2').html('Total selecionado :<strong>' + numberFormat(total, 1) + '</strong>').show('slow');
            }
            $(function () {
                $('#txtTipo').change(function () {
                    if ($(this).val()) {
                        $('#txtUsuario').hide();
                        $('#carregando').show();
                        $.getJSON('comissao.ajax.php?search=', {txtTipo: $(this).val(), ajax: 'true'}, function (j) {
                            var options = '<option value="0">Selecione</option>';
                            for (var i = 0; i < j.length; i++) {
                                options += '<option value="' + j[i].login_user + '">' + j[i].nome + '</option>';
                            }
                            $('#txtUsuario').html(options).show();
                            $('#carregando').hide();
                        });
                    } else {
                        $('#txtUsuario').html('<option value="null" >Selecione</option>');
                    }
                });
            });

            var creatTable = 0;
            function ListComissoes() {
                if (creatTable === 0) {
                    $('#tbReceitaAnalitico').dataTable({
                        bPaginate: false,
                        bFilter: false,
                        bInfo: false,
                        bSort: false,
                        "bProcessing": true,
                        "bServerSide": true,
                        "fnDrawCallback": function (oSettings) {
                            if (oSettings.aiDisplay.length === 0)
                            {
                                return;
                            }
                            var txtValorP = 0.00;
                            var txtValorS = 0.00;
                            var txtValorPl = 0.00;
                            var txtDescontoP = 0.00;
                            var txtDescontoS = 0.00;
                            var txtTotalAcumuladoP = 0.00;
                            var txtTotalAcumuladoS = 0.00;
                            var txtTotalAcumuladoPl = 0.00;
                            var txtTotal = 0.00;
                            $('#tbReceitaAnalitico > tbody  > tr').each(function () {
                    <?php $coluna = 6; ?>
                        txtValorP = txtValorP + textToNumber($('>td:nth-child(6)', $(this)).html());
                        txtValorS = txtValorS + textToNumber($('>td:nth-child(7)', $(this)).html());
                    <?php if ($mdl_aca_ == 1) { $coluna = 7; ?>
                        txtValorPl = txtValorPl + textToNumber($('>td:nth-child(<?php echo $coluna + 1; ?>)', $(this)).html());
                    <?php } ?>
                        txtDescontoP = txtDescontoP + textToNumber($('>td:nth-child(<?php echo $coluna + 2; ?>)', $(this)).html());
                        txtDescontoS = txtDescontoS + textToNumber($('>td:nth-child(<?php echo $coluna + 3; ?>)', $(this)).html());
                        txtTotalAcumuladoP = txtTotalAcumuladoP + textToNumber($('>td:nth-child(<?php echo $coluna + 4; ?>)', $(this)).html());
                        txtTotalAcumuladoS = txtTotalAcumuladoS + textToNumber($('>td:nth-child(<?php echo $coluna + 5; ?>)', $(this)).html());
                    <?php if ($mdl_aca_ == 1) { ?>
                        txtTotalAcumuladoPl = txtTotalAcumuladoPl + textToNumber($('>td:nth-child(<?php echo $coluna + 6; ?>)', $(this)).html());
                    <?php } else { $coluna = 5; } ?>
                        txtTotal = txtTotal + textToNumber($('>td:nth-child(<?php echo $coluna + 7; ?>)', $(this)).html());
                    });
                    $('#txtValorP').html(numberFormat(txtValorP));
                        <?php if ($mdl_aca_ == 1) { ?>
                            $('#txtValorPl').html(numberFormat(txtValorPl));
                            $('#txtTotalAcumuladoPl').html(numberFormat(txtTotalAcumuladoPl));
                        <?php } ?>
                            $('#txtValorS').html(numberFormat(txtValorS));
                            $('#txtDescontoP').html(numberFormat(txtDescontoP));
                            $('#txtDescontoS').html(numberFormat(txtDescontoS));
                            $('#txtTotalAcumuladoP').html(numberFormat(txtTotalAcumuladoP));
                            $('#txtTotalAcumuladoS').html(numberFormat(txtTotalAcumuladoS));
                            $('#txtTotal').html(numberFormat(txtTotal));
                        },
                        "sAjaxSource": finalFind(3)
                    });
                    creatTable = 1;
                } else {
                    var oTable = $('#tbReceitaAnalitico').dataTable();
                    oTable.fnReloadAjax(finalFind(3));
                }
            }
            $(document).ready(function () {
                $("#txtTipo").trigger("change");
            });
            function finalFind(ts) {
                var retPrint = "?ts=" + ts;
                if ($('#txtTipo').val() !== "") {
                    retPrint = retPrint + "&tp=" + $('#txtTipo').val();
                }
                if ($('#txtUsuario').val() !== "") {
                    retPrint = retPrint + "&id=" + $('#txtUsuario').val();
                }
                if ($('#txtForma').val() !== "") {
                    retPrint = retPrint + "&pg=" + $('#txtForma').val();
                }
                if ($('#mscPlano').val() !== null) {
                    retPrint = retPrint + "&plan=" + $('#mscPlano').val();
                }
                retPrint = retPrint + "&fm=0";
                if ($('#txt_dt_begin').val() !== "") {
                    retPrint = retPrint + "&dti=" + $('#txt_dt_begin').val().replace(/\//g, "_");
                }
                if ($('#txt_dt_end').val() !== "") {
                    retPrint = retPrint + "&dtf=" + $('#txt_dt_end').val().replace(/\//g, "_");
                }
                retPrint = retPrint + "&emp=" + $("#txtMnFilial").val();
                return "./../BI/BI-Produtividade-Funcionario_server_processing.php" + retPrint;
            }
        </script>
        <?php odbc_close($con); ?>
    </body>
</html>
