<?php

include "../../../Connections/configini.php";

if (isset($_POST['btnSave'])) {
    $where = "";
    $mes = date("m");
    $ano = date("Y");
    $tipo = "1";
    
    if (is_numeric($_POST['mes'])) {
        $mes = $_POST['mes'];
    }
    
    if (is_numeric($_POST['ano'])) {
        $ano = $_POST['ano'];
    }
    
    if (is_numeric($_POST['tipo'])) {
        $tipo = $_POST['tipo'];
    }
        
    if ($_POST['sta'] != 'null') {
        $where .= " and (";
        $dataAux = explode(",", $_POST['sta']);
        for ($i = 0; $i < count($dataAux); $i++) {
            $where .= ($i > 0 ? " or " : "") . " F.fornecedores_status = '" . getStatus($dataAux[$i]) . "'";
        }
        $where .= ")";
    }
    
    if (is_numeric($_POST['cliente'])) {
        $where .= " and A.favorecido = " . $_POST['cliente'];
    }
    
    if ($_POST['plano'] != 'null') {
        $where .= " and A.id_prod_plano in (" . $_POST['plano'] . ")";
    }

    $query = "set dateformat dmy;
    insert into sf_vendas_planos_mensalidade (id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens)
    select id_plano, case when ISDATE(x.dia + '/" . $mes . "/" . $ano . "') > 0 then x.dia + '/" . $mes . "/" . $ano . "' else dateadd(day, -1, dateadd(month, 1, '01/" . $mes . "/" . $ano . "')) end data_ini,
    dateadd(day, -1, dateadd(month, 1, case when ISDATE(x.dia + '/" . $mes . "/" . $ano . "') > 0 then x.dia + '/" . $mes . "/" . $ano . "' else dateadd(day, -1, dateadd(month, 1, '01/" . $mes . "/" . $ano . "')) end)) data_fim,
    id_parcela, getdate(), valor_unico from (select id_plano, id_parcela, valor_unico, favorecido, 
    (select top 1 cast(day(dateadd(day, 1, dt_fim_mens)) as varchar) from sf_vendas_planos_mensalidade where id_plano_mens = id_plano order by id_mens desc) dia
    from sf_vendas_planos A inner join sf_fornecedores_despesas F on F.id_fornecedores_despesas = A.favorecido
    inner join sf_produtos B on A.id_prod_plano = B.conta_produto
    inner join sf_produtos_parcelas on id_produto = id_prod_plano and parcela = " . $tipo . "
    where dt_cancelamento is null " . $where . " and (select count(id_mens) from sf_vendas_planos_mensalidade where id_plano_mens = id_plano and month(dt_inicio_mens) = '" . $mes . "' and year(dt_inicio_mens) = '" . $ano . "') = 0
    and ((DATEADD(day,(select ACA_PLN_DIAS_RENOVACAO from sf_configuracao),A.dt_fim) > GETDATE() and B.parcelar = 0) or (A.dt_fim >= GETDATE() and B.parcelar = 1) or ((select COUNT(*) from sf_vendas_planos_mensalidade C where C.id_plano_mens = A.id_plano and dt_pagamento_mens is null) > 0))
    and (select top 1 parcela from sf_vendas_planos_mensalidade PM inner join sf_produtos_parcelas PP on PM.id_parc_prod_mens = PP.id_parcela where PM.id_plano_mens = A.id_plano order by id_mens desc) = " . $tipo . ") as x";
    //echo $query; exit;
    odbc_exec($con, $query);
    echo "YES";
}

odbc_close($con);
