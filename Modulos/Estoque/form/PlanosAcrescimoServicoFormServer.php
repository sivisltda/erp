<?php

include "../../../Connections/configini.php";

if (isset($_POST['btnSave'])) {
    if (is_numeric($_POST['txtId'])) {
        if (is_numeric($_POST['txtIdAcrSer'])) {
            $query = "UPDATE sf_produtos_acrescimo_servicos SET " .
                    "id_produto = " . valoresSelect("txtId") . "," .
                    "id_servico = " . valoresSelect("txtServicoAcrSer") . "," .
                    "qtd_min = " . valoresNumericos("txtQtdMinAcrSer") . "," .
                    "qtd_max = " . valoresNumericos("txtQtdMaxAcrSer") . " " .
                    "WHERE id = " . $_POST['txtIdAcrSer'];
        } else {
            $query = "INSERT INTO sf_produtos_acrescimo_servicos(id_produto,id_servico,qtd_min,qtd_max) VALUES(" .
                    valoresSelect("txtId") . "," .
                    valoresSelect("txtServicoAcrSer") . "," .
                    valoresNumericos("txtQtdMinAcrSer") . "," .
                    valoresNumericos("txtQtdMaxAcrSer") . ")";
        }
        odbc_exec($con, $query);
        echo "YES";
    }
}

if (isset($_POST['btnDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        $query = "delete from sf_produtos_acrescimo_servicos where id = " . $_POST['txtId'];
        odbc_exec($con, $query);
        echo "YES";
    }
}

if (isset($_POST['getAcrescimo'])) {
    $local = array();    
    $query = "select * from sf_produtos_acrescimo_servicos where id = " . $_POST['getAcrescimo'];
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array(
            'id' => $RFP['id'], 
            'id_produto' => $RFP['id_produto'], 
            'id_servico' => $RFP['id_servico'], 
            'qtd_min' => $RFP['qtd_min'], 
            'qtd_max' => $RFP['qtd_max']
        );
    }
    echo(json_encode($local));
}

if (isset($_GET['listAcrescimo'])) {
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    if (is_numeric($_GET['listAcrescimo'])) {    
        $query = "select * from sf_produtos_acrescimo_servicos 
        inner join sf_produtos on sf_produtos.conta_produto = sf_produtos_acrescimo_servicos.id_servico        
        where id_produto = " . $_GET['listAcrescimo'];
        $cur = odbc_exec($con, $query);
        while ($RFP = odbc_fetch_array($cur)) {
            $row = array();
            $row[] = "<a href=\"javascript:void(0)\" onClick=\"BuscarAcrescimoSer(" . $RFP["id"] . ")\">" . utf8_encode($RFP["descricao"]) . "</a>";
            $row[] = utf8_encode($RFP["qtd_min"]);
            $row[] = utf8_encode($RFP["qtd_max"]);
            $row[] = "<center><img src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' onClick=\"RemoverAcrescimoSer(" . $RFP["id"] . ")\"></center>";
            $output['aaData'][] = $row;
        }
    }
    echo json_encode($output);
}

odbc_close($con);
