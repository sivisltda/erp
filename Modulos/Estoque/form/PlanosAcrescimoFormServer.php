<?php

include "../../../Connections/configini.php";

if (isset($_POST['btnSave'])) {
    if (is_numeric($_POST['txtId'])) {
        if (is_numeric($_POST['txtIdAcr'])) {
            $query = "UPDATE sf_produtos_acrescimo SET " .
                    "id_produto = " . valoresSelect("txtId") . "," .
                    "valor = " . valoresNumericos("txtValorAcr") . "," .
                    "id_parentesco = " . valoresSelect("txtParentescoAcr") . "," .
                    "idade_min = " . valoresNumericos("txtIdMinAcr") . "," .
                    "idade_max_m = " . valoresNumericos("txtLimiteMascAcr") . "," .
                    "idade_max_f = " . valoresNumericos("txtLimiteFemAcr") . "," .
                    "qtd_min = " . valoresNumericos("txtQtdMinAcr") . "," .
                    "qtd_max = " . valoresNumericos("txtQtdMaxAcr") . " " .
                    "WHERE id = " . $_POST['txtIdAcr'];
        } else {
            $query = "INSERT INTO sf_produtos_acrescimo(id_produto,valor,id_parentesco
            ,idade_min,idade_max_m,idade_max_f,qtd_min,qtd_max) VALUES(" .
                    valoresSelect("txtId") . "," .
                    valoresNumericos("txtValorAcr") . "," .
                    valoresSelect("txtParentescoAcr") . "," .
                    valoresNumericos("txtIdMinAcr") . "," .
                    valoresNumericos("txtLimiteMascAcr") . "," .
                    valoresNumericos("txtLimiteFemAcr") . "," .
                    valoresNumericos("txtQtdMinAcr") . "," .
                    valoresNumericos("txtQtdMaxAcr") . ")";
        }
        odbc_exec($con, $query);
        echo "YES";
    }
}

if (isset($_POST['btnDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        $query = "delete from sf_produtos_acrescimo where id = " . $_POST['txtId'];
        odbc_exec($con, $query);
        echo "YES";
    }
}

if (isset($_POST['getAcrescimo'])) {
    $local = array();    
    if (is_numeric($_GET['getAcrescimo'])) {
        $query = "select * from sf_produtos_acrescimo where id = " . $_POST['getAcrescimo'];
        $cur = odbc_exec($con, $query);
        while ($RFP = odbc_fetch_array($cur)) {
            $local[] = array(
                'id' => $RFP['id'], 
                'id_produto' => $RFP['id_produto'], 
                'valor' => escreverNumero($RFP['valor'], 0, 4), 
                'id_parentesco' => $RFP['id_parentesco'], 
                'idade_min' => $RFP['idade_min'], 
                'idade_max_m' => $RFP['idade_max_m'], 
                'idade_max_f' => $RFP['idade_max_f'], 
                'qtd_min' => $RFP['qtd_min'], 
                'qtd_max' => $RFP['qtd_max']
            );
        }
    }
    echo(json_encode($local));
}

if (isset($_GET['listAcrescimo'])) {
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => 0,
        "iTotalDisplayRecords" => 0,
        "aaData" => array()
    );
    if (is_numeric($_GET['listAcrescimo'])) {
        $query = "select * from sf_produtos_acrescimo 
        left join sf_parentesco on sf_produtos_acrescimo.id_parentesco = sf_parentesco.id_parentesco        
        where id_produto = " . $_GET['listAcrescimo'];
        $cur = odbc_exec($con, $query);
        while ($RFP = odbc_fetch_array($cur)) {
            $row = array();
            $row[] = "<a href=\"javascript:void(0)\" onClick=\"BuscarAcrescimo(" . $RFP["id"] . ")\">" . escreverNumero($RFP["valor"], 0, 4) . "</a>";
            $row[] = utf8_encode($RFP["nome_parentesco"]);
            $row[] = utf8_encode($RFP["idade_min"]);
            $row[] = utf8_encode($RFP["idade_max_m"]);
            $row[] = utf8_encode($RFP["idade_max_f"]);
            $row[] = utf8_encode($RFP["qtd_min"]);
            $row[] = utf8_encode($RFP["qtd_max"]);
            $row[] = "<center><img src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' onClick=\"RemoverAcrescimo(" . $RFP["id"] . ")\"></center>";
            $output['aaData'][] = $row;
        }
    }
    echo json_encode($output);
}

odbc_close($con);
