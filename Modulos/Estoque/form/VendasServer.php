<?php

include "../../../Connections/configini.php";
require_once('../../../util/util.php');

if (isset($_POST['btnAprov'])) {
    $items = $_POST['items'];
    for ($i = 0; $i < sizeof($items); $i++) {
        $item = explode("-", $items[$i]);
        if ($item[0] == "V" && is_numeric($item[1])) {
            odbc_exec($con, "update sf_vendas set status = 'Aprovado', data_aprov = getdate() where id_venda = '" . $item[1] . "'") or die(odbc_errormsg());
        }
    }
    echo "YES";
}

if (isset($_POST['btnCancel'])) {
    $items = $_POST['items'];
    for ($i = 0; $i < sizeof($items); $i++) {
        $item = explode("-", $items[$i]);
        if ($item[0] == "V" && is_numeric($item[1])) {        
            odbc_exec($con, "update sf_vendas set status = 'Reprovado', data_aprov = getdate() where id_venda = '" . $item[1] . "'") or die(odbc_errormsg());
        }
    }
    echo "YES";
}

if (isset($_POST['btnNfs'])) {
    $items = $_POST['items'];
    $cur = odbc_exec($con, "select id_fornecedores_despesas,estado,cidade,
    (select max(nfs_ambiente) from sf_configuracao) ambiente, 
    (select isnull(max(nnf),0) + 1 from sf_nfe_notas where mod = 0) codigo,
    (select top 1 id_enq from sf_nfs_enquadramento) enquadramento
    from sf_fornecedores_despesas where tipo = 'M'");
    while ($RFP = odbc_fetch_array($cur)) {
        $numero = $RFP['codigo'];
        $uf = $RFP['estado'];
        $cidade = $RFP['cidade'];
        $ambiente = $RFP['ambiente'];
        $emitente = $RFP['id_fornecedores_despesas'];
        $enquadramento = $RFP['enquadramento'];
    }
    for ($i = 0; $i < sizeof($items); $i++) {
        $item = explode("-", $items[$i]);
        if (is_numeric($item[1]) && is_numeric($uf) && is_numeric($cidade) && is_numeric($ambiente) && is_numeric($emitente)) {
            $query = "DECLARE @newID INT;BEGIN TRANSACTION
            INSERT INTO [sf_nfe_notas]([status_nfe],[code],[versao_xml],[mod],[serie],[nnf],[demis],[cnf],[cdv],[tpnf],[tpemis],[finNFe],
            [tpImp],[natop],[uf],[cmunfg],[contingencia_motivo],[tpAmb],[procEmi],[verProc],[codigo_emitente],[codigo_destinatario],
            [impresso],[exportado],[mod_transp],[placa_veiculo],[reboque_veiculo],[reboque_veiculo_placa],[tipo_volume],
            [volume_transp],[peso_liq_transp],[peso_bruto_transp],[info_add_fisco],[info_add_complementar],[destinatario],
            [status],[descontop],[descontotp],[conFinal],[destOper],[tipoAten],[id_venda_nfe],hSaiEnt,hSaiEmi)";
            if ($item[0] == "R") {
                $query .= "SELECT '" . utf8_decode("Em Digitação") . "','','1.0',0,1," . ($numero + $i) . ",[data_parcela],0,0," . $enquadramento . ",0,0,0,'" . utf8_decode("SERVIÇO") . "'," . $uf . "," . $cidade . ",''," . $ambiente . ",
                3,'1.0.0'," . $emitente . ",[cliente_venda],0,0,0,'','','','',0,0,0,'" . utf8_decode("SERVIÇO PRESTADO AO CLIENTE") . "','',[destinatario],[status],[descontos],[descontots],0,0,0,[id_venda],
                '00:00:00','00:00:00' FROM [sf_vendas] inner join sf_venda_parcelas vp on vp.venda = sf_vendas.id_venda
                where id_parcela = '" . $item[1] . "'";
            } elseif ($item[0] == "V") {
                $query .= "SELECT '" . utf8_decode("Em Digitação") . "','','1.0',0,1," . ($numero + $i) . ",[data_venda],0,0," . $enquadramento . ",0,0,0,'" . utf8_decode("SERVIÇO") . "'," . $uf . "," . $cidade . ",''," . $ambiente . ",
                3,'1.0.0'," . $emitente . ",[cliente_venda],0,0,0,'','','','',0,0,0,'" . utf8_decode("SERVIÇO PRESTADO AO CLIENTE") . "','',[destinatario],[status],[descontos],[descontots],0,0,0,[id_venda],
                '00:00:00','00:00:00' FROM [sf_vendas] where id_venda = '" . $item[1] . "'";                
            } 
            $query .= "SELECT @newID = SCOPE_IDENTITY();                
            INSERT INTO [sf_nfe_notas_itens] ([cod_nota],[cod_prod],[cfop],[qtd_comercial],[qtd_trib],[tot_seguro],[tot_desconto]
            ,[tot_frete],[tot_outras_despesas],[tot_val_bruto],[icms_sit_trib]
            ,[icms_origem],[pis_sit_trib],[pis_quantidade],[cofins_sit_trib],[cofins_quantidade],[info_add])";
            if ($item[0] == "R") {            
                $query .= "select top 1 @newID,produto,'',quantidade,quantidade,0,0,0,0,valor_parcela,
                0,0,0,0,0,0,'' from sf_vendas_itens inner join sf_venda_parcelas vp on vp.venda = sf_vendas_itens.id_venda
                where id_parcela = " . $item[1];
            } elseif ($item[0] == "V") {
                $query .= "select @newID,produto,'',quantidade,quantidade,0,0,0,0,valor_total,
                0,0,0,0,0,0,'' from sf_vendas_itens where id_venda = " . $item[1];                
            }                 
            $query .= "IF @@ERROR = 0
                COMMIT
            ELSE
                ROLLBACK;";
            odbc_exec($con, $query) or die(odbc_errormsg());
        }
    }
    echo "YES";
}
odbc_close($con);
