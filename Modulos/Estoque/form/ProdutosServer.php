<?php

include "../../../Connections/configini.php";
require_once('../../../util/util.php');

$idFilial = 0;
if (!is_numeric($_REQUEST['txtFilial'])) {
    $query = "select ISNULL(filial, (SELECT id_filial FROM dbo.sf_filiais where id_filial = 1)) filial from dbo.sf_usuarios where id_usuario = '" . $_SESSION["id_usuario"] . "'";
    $cur = odbc_exec($con, $query) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $idFilial = $RFP['filial'];
    }
} else {
    $idFilial = $_REQUEST['txtFilial'];
}

if (isset($_POST['VerEstoqueListaProdutos'])) {
    $itens = '';
    $query = "select conta_produto, descricao from sf_produtos
              where conta_produto in(" . $_POST['Itens'] . ") and convert(decimal(8,4),dbo.ESTOQUE_FILIAL(conta_produto," . $idFilial . ")) <= 0 and nao_vender_sem_estoque = 1";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $itens .= $itens !== "" ? ", " + utf8_decode($RFP['descricao']) : utf8_decode($RFP['descricao']);
    }
    echo $itens;
}
odbc_close($con);
