<?php

if (isset($_POST['isAjax']) || isset($_GET['isAjax'])) {
    include "../../../Connections/configini.php";
}

if (isset($_GET['listPlanos'])) {
    $query = "select conta_produto, descricao from sf_produtos where tipo = 'C' and inativa = 0 and conta_produto > 0";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["conta_produto"] = utf8_encode($RFP["conta_produto"]);
        $row["descricao"] = utf8_encode($RFP["descricao"]);
        $records[] = $row;
    }
    echo json_encode($records);
}

if (isset($_GET['refreshEmail']) && is_numeric($_GET['refreshEmail'])) { //    sf_usuarios.email = sf_departamentos.email,
    $query = "update sf_usuarios set
    sf_usuarios.email_desc = sf_departamentos.email_desc,
    sf_usuarios.email_host = sf_departamentos.email_host,
    sf_usuarios.email_porta = sf_departamentos.email_porta,
    sf_usuarios.email_ssl = sf_departamentos.email_ssl,
    sf_usuarios.email_login = sf_departamentos.email_login,
    sf_usuarios.email_senha = sf_departamentos.email_senha,
    sf_usuarios.email_ckb = sf_departamentos.email_ckb,
    sf_usuarios.email_smtp = sf_departamentos.email_smtp,
    sf_usuarios.copyMe = sf_departamentos.copyMe,
    sf_usuarios.assinatura = sf_departamentos.assinatura
    from sf_usuarios inner join sf_departamentos on sf_departamentos.id_departamento = sf_usuarios.departamento
    where id_departamento = " . $_GET['refreshEmail'];
    odbc_exec($con, $query);
    echo "YES";
}

if (isset($_GET['refreshMensalidades']) && is_numeric($_GET['refreshMensalidades'])) {
    $tpAtualizacao = "";
    $cur = odbc_exec($con, "select adm_atualizar_mens from sf_configuracao where id = 1");
    while ($RFP = odbc_fetch_array($cur)) {
        $tpAtualizacao = ($RFP['adm_atualizar_mens'] > 0 ? " and dt_inicio_mens > getdate() " : "");
    }    
    $query = "update sf_vendas_planos_mensalidade set valor_mens = case when parcelar = 1 and parcela > 0 then valor_unico/parcela else valor_unico end 
    from sf_vendas_planos_mensalidade inner join sf_produtos_parcelas on sf_produtos_parcelas.id_parcela = id_parc_prod_mens
    inner join sf_produtos on sf_produtos.conta_produto = sf_produtos_parcelas.id_produto
    inner join sf_vendas_planos on id_plano_mens = id_plano 
    where id_item_venda_mens is null " . $tpAtualizacao . " and usu_alt is null
    and sf_vendas_planos.id_prod_plano = " . $_GET['refreshMensalidades'];
    odbc_exec($con, $query);
    echo "YES";
}

odbc_close($con);
