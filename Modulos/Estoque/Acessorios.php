<?php
include '../../Connections/configini.php';

if (is_numeric($_GET["Del"])) {
    odbc_exec($con, "DELETE FROM sf_produtos WHERE conta_produto = " . $_GET["Del"]);
    if (odbc_error()) {
        echo "<script>alert('Não é possível excluir um serviço que esteja atrelado à uma venda.');</script>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />        
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>                     
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"><span class="ico-arrow-right"></span></div>
                        <h1>Proteção<small>Acessórios</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="margin-top: 15px;float:left;">
                                    <div style="float:left">
                                        <form method="POST">
                                            <button  class="button button-green btn-primary" type="button" onClick="AbrirBox(1, 0)"><span class="ico-file-4 icon-white"></span></button>
                                            <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir('I')" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                            <button id="btnExcel" class="button button-blue btn-primary" type="button" onclick="imprimir('E')" title="Exportar Excel"><span class="ico-download-3"></span></button>
                                        </form>
                                    </div>
                                </div>
                                <div style="float: right;width: 80%; display: flex;align-items: center;justify-content: flex-end;">
                                    <div style="float:left;margin-left: 1%;">                                    
                                        <div width="100%">Grupos:</div>
                                        <select id="txtGrupo">
                                            <option value="null">Selecione</option>
                                            <?php $sql = "select id_contas_movimento,descricao from sf_contas_movimento where tipo = 'A' and inativa = 0 ORDER BY descricao";
                                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id_contas_movimento'] ?>"<?php
                                                if (!(strcmp($RFP["id_contas_movimento"], $grupo))) {
                                                    echo "SELECTED";
                                                }
                                            ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                            <?php } ?>
                                        </select>                                           
                                    </div>                                                                       
                                    <div style="float:left;margin-left: 1%;">                                    
                                        <div width="100%">Tipo:</div>
                                        <select id="txtTipoVeiculo">
                                            <option value="null">Selecione</option>
                                            <?php $sql = "select id, descricao from sf_veiculo_tipos where v_inativo = 0 ORDER BY id";
                                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id'] ?>"><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                            <?php } ?>
                                        </select>                                           
                                    </div>                                                                       
                                    <div style="float:left;margin-left: 1%;">                                    
                                        <div width="100%">Inativo:</div>
                                        <select id="txtInativo">
                                            <option value="0">NÃO</option>
                                            <option value="1">SIM</option>
                                        </select>                                           
                                    </div>                                    
                                    <div style="float:left;margin-left: 1%;">
                                        <div width="100%">Busca:</div>
                                        <input name="txtBusca"  id="txtBusca" maxlength="100" type="text" style="width:175px;" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title="Nome / CPF/ CNPJ / Endereço / Bairro / Cidade / Contato"/>
                                    </div>
                                    <div style="margin-top: 15px;float:left;margin-left:0.3%;">
                                        <div style="float:left">
                                            <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Acessórios</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="example">
                            <thead>
                                <tr>
                                    <th width="10%">Cod.</th>
                                    <th width="20%">Descrição do Grupo</th>
                                    <th width="3%"><center>Nível</center></th>
                                    <th width="42%">Descrição do Serviço</th>
                                    <th width="10%">Tipo</th>
                                    <th width="10%">Valor</th>
                                    <th width="5%"><center>Ação:</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                    <div class="widgets"></div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>
        <script type="text/javascript" src="../../fancyapps/source/jquery.fancybox.js?v=2.1.4"></script>               
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }

            $(document).ready(function () {
                listaTable();
                $("#btnfind").click(function () {
                    $("#example").dataTable().fnDestroy();
                    listaTable();
                });

                function listaTable() {
                    $('#example').dataTable({
                        "iDisplayLength": 20,
                        "aLengthMenu": [20, 30, 40, 50, 100],
                        "bProcessing": true,
                        "bServerSide": true,
                        "sAjaxSource": "Acessorios_server_processing.php" + finalFind(0),
                        "bFilter": false,
                        "ordering": false,
                        'oLanguage': {
                            'oPaginate': {
                                'sFirst': "Primeiro",
                                'sLast': "Último",
                                'sNext': "Próximo",
                                'sPrevious': "Anterior"
                            }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                            'sLengthMenu': "Visualização de _MENU_ registros",
                            'sLoadingRecords': "Carregando...",
                            'sProcessing': "Processando...",
                            'sSearch': "Pesquisar:",
                            'sZeroRecords': "Não foi encontrado nenhum resultado"},
                        "sPaginationType": "full_numbers"
                    });
                }
            });
            
            function imprimir(tp) {              
                var pRel = "&NomeArq=" + "Acessórios" +
                "&lbl=Cod.|Descrição do Grupo|Nível|Descrição do Serviço|Tipo|Valor" +
                "&siz=50|250|50|200|100|50" +
                "&pdf=6" + // Colunas do server processing que não irão aparecer no pdf
                "&filter=" + //Label que irá aparecer os parametros de filtro
                "&PathArqInclude=" + "../Modulos/Estoque/Acessorios_server_processing.php" + finalFind(1).replace("?", "&"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tp + "&PathArq=GenericModelPDF.php", '_blank');
            }
            
            function finalFind(imp) {
                var retPrint = "";
                retPrint = '?imp=0';
                if (imp === 1) {
                    retPrint = '?imp=1';
                }
                if ($("#txtBusca").val() !== "") {
                    retPrint = retPrint + '&sSearch=' + $("#txtBusca").val();
                }
                if ($("#txtGrupo").val() !== "null") {
                    retPrint = retPrint + '&gp=' + $("#txtGrupo").val();
                }                
                if ($("#txtTipoVeiculo").val() !== "null") {
                    retPrint = retPrint + '&tp=' + $("#txtTipoVeiculo").val();
                }                
                if ($("#txtInativo").val() !== "null") {
                    retPrint = retPrint + '&at=' + $("#txtInativo").val();
                }                
                return retPrint.replace(/\//g, "_");
            }

            function AbrirBox(id, tp) {
                abrirTelaBox("FormAcessorios.php" + (tp === 2 ? "?id=" + id : ""), 400, 550);
            }
            
            function FecharBox(opc) {
                var oTable = $('#example').dataTable();
                oTable.fnDraw(false);
                $("#newbox").remove();
            }            
        </script>             
        <?php odbc_close($con); ?>
    </body>
</html>
