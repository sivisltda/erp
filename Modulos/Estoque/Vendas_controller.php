<?php

include '../../Connections/configini.php';

if (isset($_GET['existeComanda'])) {
    $where = '';
    if ($_GET['comandaUnica'] == 0) {
        $where = " and status = 'Aguarda' ";
    }
    $id_vendaComanda = '';
    $cur = odbc_exec($con, "select id_venda from dbo.sf_vendas where cod_pedido = '" . $_GET['existeComanda'] . "'" . $where);
    while ($RFP = odbc_fetch_array($cur)) {
        $id_vendaComanda = $RFP['id_venda'];
    }

    if ($id_vendaComanda !== "") {
        if (isset($_GET['idVenda'])) {
            if ($id_vendaComanda !== $_GET['idVenda']) {
                echo 'YES';
            } else {
                echo 'NO';
            }
        } else {
            echo 'NO';
        }
    } else {
        echo 'NO';
    }
}

if (isset($_POST['insereItem'])) {
    $id_vd = $_POST['id_vd'];
    $prod_cod = $_POST['prod_cod'];
    $prod_grp = $_POST['prod_grp'];
    $prod_com = "null";
    if (isset($_POST['prod_com'])) {
        if (is_numeric($_POST['prod_com'])) {
            $prod_com = $_POST['prod_com'];
        }
    }
    $valorSubTotal = (valoresNumericos('prod_prc') * valoresNumericos('prod_qnt'));
    $sQuery = " DECLARE @newID INT;
                BEGIN TRANSACTION
                    insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total,vendedor_comissao) 
                    values(" . $id_vd . "," . $prod_grp . "," . $prod_cod . "," . valoresNumericos('prod_qnt') . "," . $valorSubTotal . ", " . $prod_com . ");
                    SELECT @newID = SCOPE_IDENTITY();
                    insert into sf_vendas_itens_pacotes(produto_usar,venda_item,venda_aplicada)
                    select id_prodmp, @newID venda,null venda_aplicada from sf_produtos_materiaprima where id_produtomp in (" . $prod_cod . ");
                IF @@ERROR = 0
                COMMIT
                ELSE
                ROLLBACK;
                SELECT 'ID' = @newID;";
    $result = odbc_exec($con, $sQuery) or die(odbc_errormsg());
    odbc_next_result($result);
    odbc_next_result($result);
    odbc_next_result($result);
    $codVenda = odbc_result($result, 1);
    echo $codVenda;
}

if (isset($_GET['listaParcelas'])) {

    $cur = odbc_exec($con, "select *, (select top 1 id_credito from sf_creditos where sf_creditos.inativo = 0 and id_venda = venda and id_venda_quitacao is null and valor = valor_parcela and dt_vencimento = data_parcela and tipo_documento = 12) carteira
    from sf_venda_parcelas where inativo in (0,2) and venda = " . $_GET['listaParcelas']);
    while ($aRow = odbc_fetch_array($cur)) {
        $local[] = array('id_parcela' => utf8_encode($aRow['id_parcela']), 'numero_parcela' => utf8_encode($aRow['numero_parcela']), 'sa_descricao' => utf8_encode($aRow['sa_descricao']),
        'data_parcela' => escreverData($aRow['data_parcela']), 'valor_parcela' => escreverNumero($aRow['valor_parcela']),
        'valor_pago' => escreverNumero($aRow['valor_pago']), 'tipo_documento' => utf8_encode($aRow['tipo_documento']), 'carteira' => utf8_encode($aRow['carteira']));
    }
    echo(json_encode($local));
}
odbc_close($con);