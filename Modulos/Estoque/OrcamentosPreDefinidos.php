<?php
include "../../Connections/configini.php";

$imprimir = 0;
$F1 = "";
$F2 = "";
$F3 = "";
$DateBegin = getData("B");
$DateEnd = getData("E");
$sendURL = "";

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = $_GET['imp'];
}

if (is_numeric($_GET['id'])) {
    $F1 = $_GET['id'];
}

if (is_numeric($_GET['tp'])) {
    $F2 = $_GET['tp'];
}

if ($_GET['td'] != '') {
    $F3 = $_GET['td'];
}

if ($_GET['dti'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
}

if ($_GET['dtf'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
}

if (isset($_POST['btnAprov'])) {
    $items = $_POST['items'];
    for ($i = 0; $i < sizeof($items); $i++) {
        if (is_numeric($items[$i])) {
            odbc_exec($con, "update sf_vendas set status = 'Aprovado', data_aprov = getdate() where id_venda = '" . $items[$i] . "'") or die(odbc_errormsg());
        }
    }
}

if (isset($_POST['btnCance'])) {
    $items = $_POST['items'];
    for ($i = 0; $i < sizeof($items); $i++) {
        if (is_numeric($items[$i])) {
            odbc_exec($con, "update sf_vendas set status = 'Reprovado', data_aprov = getdate() where id_venda = '" . $items[$i] . "'") or die(odbc_errormsg());
        }
    }
}

$sendURL = "?imp=" . $imprimir . "&id=" . $F1 . "&tp=" . $F2 . "&td=" . $F3 . "&dti=" . str_replace("/", "_", $DateBegin) . "&dtf=" . str_replace("/", "_", $DateEnd);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../SpryAssets/SpryValidationTextField.css"/>
        <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
        <style>
            .fancybox-wrap, .fancybox-skin, .fancybox-outer, .fancybox-inner, .fancybox-image,  .fancybox-wrap object, .fancybox-nav, .fancybox-nav span, .fancybox-tmp {
                padding: 0;
                margin: 0;
                border: 0;
                outline: none;
                vertical-align: top;
                max-height:319px;
                background:url(../../img/fundosForms/formBancos.PNG);
                height: 320px;
            }                  
            #formulario {
                float: left;
                width: 100%;
            }
            #formulario form .linha {
                float: left;
                height: 26px;
                width: 100%;
                display: block;
                border-bottom-width: 1px;
                border-bottom-style: solid;
                border-bottom-color: #f7f7f7;
                background-color: #f7f7f7;
                border-top-width: 1px;
                border-top-style: solid;
                border-top-color: #FFF;
                padding-top: 10px;
                padding-bottom: 10px;
            }
        </style>                    
    </head>
    <body>
        <?php if ($imprimir == 0) { ?>
            <div id="loader"><img src="../../img/loader.gif"/></div><?php } ?>
        <div class="wrapper">
            <?php
            if ($imprimir == 0) {
                include("../../menuLateral.php");
            }
            ?>
            <div <?php if ($imprimir == 0) { ?>class="body"<?php } ?>>
                <?php
                if ($imprimir == 0) {
                    include("../../top.php");
                }
                ?>
                <div class="content">
                    <form method="POST" action="Orcamentos.php<?php echo $sendURL; ?>">
                        <div class="page-header">
                            <?php if ($imprimir == 0) { ?>
                                <div class="icon"> <span class="ico-arrow-right"></span> </div>
                                <h1>CRM<small>Orçamentos Pré Definidos</small></h1>
                            <?php } ?>
                        </div>
                        <?php if ($imprimir == 0) { ?>                            
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="block">
                                        <div id="formulario">       
                                            <span style="padding-left:0px">
                                                <button class="btn btn-success" type="button" onClick="javascript:location.href = 'FormOrcamentosPreDefinidos.php'"><span class="icon-plus-sign icon-white"></span> Cadastrar Novo</button>
                                                <button name="btnPrint" class="btn btn-primary" type="button" onclick="redirectFind(1);" title="Imprimir"><span class="icon-print icon-white"></span></button>
                                            </span>                                        
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="block">
                                        <?php
                                        if ($imprimir == 1) {
                                            $titulo_pagina = "RELATÓRIO DE CONSULTA<br/>ORÇAMENTOS PRÉ DEFINIDOS";
                                            include "../Financeiro/Cabecalho-Impressao.php";
                                        } else {
                                            ?>
                                            <div class="head dblue">
                                                <div class="icon"><span class="ico-money"></span></div>
                                                <h2>Orçamentos Pré-Definidos</h2>
                                                <ul class="buttons">
                                                    <li><div class="icon"><span class="ico-info"></span></div></li>
                                                </ul>
                                            </div>
                                        <?php } ?>
                                        <div class="data-fluid">
                                            <table <?php
                                            if ($imprimir == 0) {
                                                echo "class=\"table\"";
                                            } else {
                                                echo "border=\"1\"";
                                            }
                                            ?> cellpadding="0" cellspacing="0" width="100%" id="example">
                                                <thead>
                                                    <tr>
                                                        <th width="50%"><center>Descrição</center></th>
                                                        <th width="15%" style="text-align:right">Tot. Produtos</th>
                                                        <th width="15%" style="text-align:right">Tot. Serviços</th>
                                                        <th width="15%" style="text-align:right">Total Geral</th>
                                                        <th width="5%"><center>Status:</center></th>
                                                    </tr>
                                                </thead>
                                                <tbody><tr><td colspan="5" class="dataTables_empty">Carregando dados do Cliente</td></tr></tbody>
                                            </table>
                                            <table id="lblTotParc" name="lblTotParc" class="table" cellpadding="0" cellspacing="0" width="100%">                                            
                                                <tr><td style="text-align:right"><div id="lblTotParc2" name="lblTotParc2">Número de Orçamentos : <strong>0</strong></div></td></tr>
                                                <tr><td style="text-align:right"><div id="lblTotParc3" name="lblTotParc3">Total em Orçamentos : <strong><?php echo escreverNumero(0,1); ?></strong></div></td></tr>
                                                <tr><td style="text-align:right"><div id="lblTotParc4" name="lblTotParc4">Ticket Médio : <strong><?php echo escreverNumero(0,1); ?></strong></div></td></tr>
                                            </table>                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="span3"></div>
                                <div class="span4"></div>
                            </div>
                        </div>                                                            
                    </form>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen"/>   
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>    
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script language="JavaScript">
            
            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
            
            function redirectFind(tp) {
                var finalURL;
                if (tp === 1) {
                    finalURL = '?imp=1';
                } else {
                    finalURL = '?imp=0';
                }
                if ($("#txtEstado").val() !== "") {
                    finalURL = finalURL + '&id=' + jQuery("#txtEstado").val();
                }
                if ($("#txtTipoBusca").val() !== "") {
                    finalURL = finalURL + '&tp=' + jQuery("#txtTipoBusca").val();
                }
                if ($("#txtUsuario").val() !== "") {
                    finalURL = finalURL + '&td=' + jQuery("#txtUsuario").val();
                }
                finalURL = finalURL + '&dti=' + jQuery("#txt_dt_begin").val();
                finalURL = finalURL + '&dtf=' + jQuery("#txt_dt_end").val();
                window.location = 'Orcamentos.php' + finalURL.replace(/\//g, "_");
            }            
            function contaCheckboxSelecionada() {
                var inputs, x, selecionados = 0;
                inputs = document.getElementsByTagName('input');
                for (x = 0; x < inputs.length; x++) {
                    if (inputs[x].type === 'checkbox') {
                        if (inputs[x].checked === true && inputs[x].id === 'check') {
                            selecionados++;
                        }
                    }
                }
                return selecionados;
            }
            function contaCheckbox() {
                if (document.getElementById('checkAll').checked === true) {
                    var inputs, x, selecionados = 0;
                    inputs = document.getElementsByTagName('input');
                    for (x = 0; x < inputs.length; x++) {
                        if (inputs[x].type === 'checkbox') {
                            if (inputs[x].id === 'check') {
                                selecionados++;
                            }
                        }
                    }
                    if (document.getElementById('btnAprov') !== undefined) {
                        if (selecionados > 0) {
                            document.getElementById('btnAprov').disabled = false;
                        } else {
                            document.getElementById('btnAprov').disabled = true;
                        }
                    }
                    if (document.getElementById('btnCance') !== undefined) {
                        if (selecionados > 0) {
                            document.getElementById('btnCance').disabled = false;
                        } else {
                            document.getElementById('btnCance').disabled = true;
                        }
                    }
                } else {
                    if (document.getElementById('btnAprov') !== undefined) {
                        document.getElementById('btnAprov').disabled = true;
                    }
                    if (document.getElementById('btnCance') !== undefined) {
                        document.getElementById('btnCance').disabled = true;
                    }
                }
                return 0;
            }
            function refreshTotal() {
                if ($("#qtdparc").val() !== undefined) {
                    $("#lblTotParc2").html("Número de Orçamentos : <strong>" + ($('#qtdparc').val() !== undefined ? $('#qtdparc').val() : "0") + "</strong>").show("slow");
                    $("#lblTotParc3").html("Total em Orçamentos : <strong>" + numberFormat($("#totparc").val(), 1) + "</strong>").show("slow");
                    $("#lblTotParc4").html("Ticket Médio : <strong>" + numberFormat(($("#totparc").val() / $("#qtdparc").val()), 1) + "</strong>").show("slow");
                }
            }

            $(document).ready(function () {
                $('#example').dataTable({
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": "OrcamentosPreDefinidos_server_processing.php<?php echo $sendURL; ?>",
                    'oLanguage': {
                        'oPaginate': {
                            'sFirst': "Primeiro",
                            'sLast': "Último",
                            'sNext': "Próximo",
                            'sPrevious': "Anterior"
                        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                        'sLengthMenu': "Visualização de _MENU_ registros",
                        'sLoadingRecords': "Carregando...",
                        'sProcessing': "Processando...",
                        'sSearch': "Pesquisar:",
                        'sZeroRecords': "Não foi encontrado nenhum resultado"},
                    'fnInitComplete': function (oSettings, json) {
                    <?php
                    if ($imprimir == 1) {
                        echo "window.print(); window.history.back();";
                    }
                    ?>refreshTotal();
                    },
                    "sPaginationType": "full_numbers"
                });
            });
        </script>
        <?php if ($imprimir == 1) { ?>
            <script type="text/javascript">
                $(window).load(function () {
                    $(".body").css("margin-left", 0);
                    $("#example_length").remove();
                    $("#example_filter").remove();
                    $("#example_paginate").remove();
                    $("#formPQ > th").css("background-image", "none");
                    <?php if ($F1 == 5) { ?>
                        $("#example_info").remove();
                    <?php } ?>
                });
            </script>
            <?php
        }
        odbc_close($con);
        ?>
    </body>
</html>