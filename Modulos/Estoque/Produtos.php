<?php
include '../../Connections/configini.php';

$estoque_minimo = '';
$fabricante = '';
$grupo = '';
$prod_sem_entrada = '';
$ativo = '';
$finalURL = '?imp=0';
$empresa = '';

if (is_numeric($_GET['Del'])) {
    odbc_exec($con, "DELETE FROM sf_produtos_historico_preco where id_produto = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_produtos_materiaprima where id_produtomp = " . $_GET['Del'] . " or id_materiaprima = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_produtos WHERE conta_produto =" . $_GET['Del']);
    echo "<script>window.top.location.href = 'Produtos.php'; </script>";
}
if (is_numeric($_GET['fb'])) {
    $fabricante = $_GET['fb'];
    $finalURL .= '&fb=' . $_GET['fb'];
}
if (is_numeric($_GET['at'])) {
    $ativo = $_GET['at'];
    $finalURL .= '&at=' . $_GET['at'];
}
if (is_numeric($_GET['gp'])) {
    $grupo = $_GET['gp'];
    $finalURL .= '&gp=' . $_GET['gp'];
}
if (is_numeric($_GET['se'])) {
    $prod_sem_entrada = $_GET['se'];
    $finalURL .= '&se=' . $_GET['se'];
}
if (is_numeric($_GET['em'])) {
    $estoque_minimo = $_GET['em'];
    $finalURL .= '&em=' . $_GET['em'];
}
if (is_numeric($_GET['empresa'])) {
    $empresa = $_GET['empresa'];
    $finalURL .= '&empresa=' . $_GET['empresa'];
}
if (isset($_GET['txtBusca'])) {
    $txtBusca = $_GET['txtBusca'];
    $finalURL .= '&txtBusca=' . $_GET['txtBusca'];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <style>
            .fancybox-custom .fancybox-skin {
                box-shadow: 0 0 50px #069;
            }            
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">                    
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Estoque<small>Produtos</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="margin-top: 15px;float:left;">
                                    <div style="float:left">
                                        <form method="POST">
                                            <button class="button button-green btn-primary" type="button" onclick="AbrirBox(0)"><span class="ico-file-4 icon-white"></span></button>
                                            <button id="btnPrint" class="button button-blue btn-primary" type="button" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                            <button id="btnExcel" class="button button-blue btn-primary" type="button" title="Exportar Excel"><span class="ico-download-3"></span></button>                                            
                                        </form>
                                    </div>
                                </div>
                                <div style="float: right;width: 873px;">
                                    <div style="float: left;width: 115px;">
                                        <span>Filial:</span>
                                        <select name="txtEmpresa" id="txtEmpresa" class="select input-medium" style="width: 100%;">
                                            <option value="-1">Est. Atual</option>
                                            <option value="-2">Nenhum</option>
                                            <?php
                                            $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo utf8_encode($RFP['id_filial']); ?>" <?php
                                                if ($filial == $RFP['id_filial']) {
                                                    echo "SELECTED";
                                                }
                                                ?>><?php echo utf8_encode($RFP['numero_filial']); ?></option>
                                                    <?php } ?>
                                            <option value="0" <?php
                                            if ($empresa == "0") {
                                                echo "SELECTED";
                                            }
                                            ?> >Todos</option>
                                        </select>
                                    </div>
                                    <div style="float: left;width: 115px;margin-left: 1%;">
                                        <span>Situação:</span>
                                        <select name="txtAtivo" id="txtAtivo" class="select input-medium" style="width: 100%;">
                                            <option value="-1">Selecione</option>
                                            <option value="0" <?php
                                            if ($ativo == "0") {
                                                echo "SELECTED";
                                            }
                                            ?>>Ativos</option>
                                            <option value="1" <?php
                                            if ($ativo == "1") {
                                                echo "SELECTED";
                                            }
                                            ?>>Inativos</option>
                                        </select>
                                    </div>
                                    <div style="float: left;width: 115px;margin-left: 1%;">
                                        <span>Grupo:</span>
                                        <select name="txtGrupo" id="txtGrupo" class="select input-medium" style="width:100%">
                                            <option value="-1">Selecione:</option>
                                            <?php
                                            $sql = "select id_contas_movimento,descricao from sf_contas_movimento where tipo = 'P' ORDER BY descricao";
                                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) {
                                                ?>
                                                <option value="<?php echo $RFP['id_contas_movimento'] ?>"<?php
                                                if (!(strcmp($RFP['id_contas_movimento'], $grupo))) {
                                                    echo "SELECTED";
                                                }
                                                ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                    <?php } ?>
                                        </select>
                                    </div>
                                    <div style="float: left;width: 115px;margin-left: 1%;">
                                        <span>Tipo:</span>
                                        <select name="txtSemEntrada" id="txtSemEntrada" class="select input-medium" style="width: 100%;">
                                            <option value="-1">Selecione:</option>
                                            <option value="0" <?php
                                            if ($prod_sem_entrada == "0") {
                                                echo "SELECTED";
                                            }
                                            ?>>MERCADORIA</option>
                                            <option value="1" <?php
                                            if ($prod_sem_entrada == "1") {
                                                echo "SELECTED";
                                            }
                                            ?>>PRODUTO</option>
                                        </select>
                                    </div>
                                    <div style="float:left;margin-left: 1%;">
                                        <div width="100%">Busca:</div>
                                        <input name="txtBusca"  id="txtBusca" maxlength="100" type="text" style="width:175px;" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title="Nome / CPF/ CNPJ / Endereço / Bairro / Cidade / Contato"/>
                                    </div>
                                    <div style="float: left;width: 139px;margin-left: 1%;margin-top: 25px;">
                                        <input name="txtEstMinimo" id="txtEstMinimo" type="checkbox" <?php
                                        if ($estoque_minimo == "1") {
                                            echo "CHECKED";
                                        }
                                        ?> value="1" class="input-medium"/> Abaixo do Est. Mínimo
                                    </div>
                                    <div style="margin-top: 15px;float:left;margin-left:0.3%;">
                                        <div style="float:left">
                                            <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar" onclick="AtualizaLista()"><span class="ico-search icon-white"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="block">                               
                                <div class="boxhead">
                                    <div class="boxtext">Produtos</div>
                                </div>
                                <style>#example td { vertical-align: middle }</style>
                                <div class="data-fluid">
                                    <table class="table" cellpadding="0" cellspacing="0" width="100%" id="example">
                                        <thead>
                                            <tr>
                                                <th width="1%">Foto</th>
                                                <th width="3%">Cod.</th>
                                                <th width="35%">Nome do Produto</th>
                                                <th width="12%">Grupo</th>
                                                <th width="5%">Uni.</th>
                                                <th width="8%">Preço</th>
                                                <th width="9%">Est. Atual</th>
                                                <th width="10%">Ult. Compra</th>
                                                <th width="10%">Ult. Venda</th>
                                                <th width="8%">Fabricante</th>
                                                <th width="2%"><center>Ação:</center></th>                                        
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="12" class="dataTables_empty">Carregando dados ...</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table id="lblTotParc" name="lblTotParc" class="table" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td style="text-align: right"><div id="lblTotParc2" name="lblTotParc2">
                                                    Total Inventário (Preço de custo em estoque) :<strong><?php echo escreverNumero(0,1); ?></strong>
                                                </div></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="widgets"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />        
        <script type="text/javascript" src="../../fancyapps/source/jquery.fancybox.js?v=2.1.4"></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>        
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }

            $(window).keydown(function (event) {
                if (event.ctrlKey && event.keyCode === 74) {
                    event.preventDefault();
                }
            });

            function refreshTotal() {
                $.getJSON("Produtos_controller.php", {getInventario: 'S',
                    fb: $('#txtFabricante').val() > 0 ? $('#txtFabricante').val() : '',
                    gp: $('#txtGrupo').val() > 0 ? $('#txtGrupo').val() : '',
                    sentrada: $('#txtSemEntrada').val() > 0 ? $('#txtSemEntrada').val() : '',
                    ativo: $('#txtAtivo').val() > 0 ? $('#txtAtivo').val() : '',
                    empresa: $('#txtEmpresa').val() > 0 ? $('#txtEmpresa').val() : ''}, function (data) {
                    $('#lblTotParc2').html('Total Inventário (Preço de custo em estoque) :<strong>' + numberFormat(data[0].valor, 1) + '</strong>').show('slow');
                }).done(function () {});
            }

            function AtualizaLista() {
                $("#example").dataTable().fnDestroy();
                listaTable();
            }
            
            function listaTable() {
                var column = [{"bSortable": false},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": true},
                    {"bSortable": false}];
                $('#example').dataTable({
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "sAjaxSource": finalFind(0),
                    "fnDrawCallback": function () {
                        if ($('#txtEmpresa').val() >= 0) {
                            $('td:nth-child(7)').show();
                            $('#example  tr:eq(0) th:eq(6)').show();
                        } else {
                            $('td:nth-child(7)').hide();
                            $('#example  tr:eq(0) th:eq(6)').hide();
                        }
                    },
                    "aoColumns": column,
                    'oLanguage': {
                        'oPaginate': {
                            'sFirst': "Primeiro",
                            'sLast': "Último",
                            'sNext': "Próximo",
                            'sPrevious': "Anterior"
                        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                        'sLengthMenu': "Visualização de _MENU_ registros",
                        'sLoadingRecords': "Carregando...",
                        'sProcessing': "Processando...",
                        'sSearch': "Pesquisar:",
                        'sZeroRecords': "Não foi encontrado nenhum resultado"},
                    "sPaginationType": "full_numbers",
                    "fnInitComplete": function (oSettings, json) {
                    refreshTotal();
                    }
                });
            }

            $(document).ready(function () {
                listaTable();
            });

            function finalFind(imp) {
                var retPrint = "";
                retPrint = '?imp=0';
                if (imp === 1) {
                    retPrint = '?imp=1';
                }
                if ($('#txtEmpresa').val() >= 0) {
                    retPrint = retPrint + "&empresa=" + $('#txtEmpresa').val();
                }
                if ($('#txtFabricante').val() >= 0) {
                    retPrint = retPrint + "&fb=" + $('#txtFabricante').val();
                }
                if ($('#txtAtivo').val() >= 0) {
                    retPrint = retPrint + "&at=" + $('#txtAtivo').val();
                }
                if ($('#txtGrupo').val() >= 0) {
                    retPrint = retPrint + "&gp=" + $('#txtGrupo').val();
                }
                if ($('#txtSemEntrada').val() >= 0) {
                    retPrint = retPrint + "&se=" + $('#txtSemEntrada').val();
                }
                if ($('#txtEstMinimo').is(':checked')) {
                    retPrint = retPrint + "&em=1";
                }
                if ($("#txtBusca").val() !== "") {
                    retPrint = retPrint + '&txtBusca=' + $("#txtBusca").val();
                }
                return "Produtos_server_processing.php" + retPrint.replace(/\//g, "_");
            }
            
            $("#btnPrint").click(function () {
                var pRel = "&NomeArq=" + "Produtos" +
                        "&siz=40|235|30|70|60|65|60|70|70" +
                        "&lbl=Cod|Nome do Produto|Uni.|Preço|Est. Atual|Ult. Compra|Ult. Venda|Fabricante|Cod. Barra" +
                        "&pdf=11" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/Estoque/" + finalFind(1).replace("?", "&"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            });
            
            $("#btnExcel").click(function () {
                window.open(finalFind(1) + '&ex=1');
            });            

            function AbrirBox(id) {
                if (id === -1) {
                    window.location = "Produtos.php" + finalFind(0);
                } else if (id === -2) {
                    window.location = "Produtos.php" + finalFind(1);
                } else {
                    if (id > 0) {
                        var myId = "?id=" + id;
                    } else {
                        var myId = "";
                    }
                    $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:717px; height:556px; position:absolute; top:50%; left:50%; margin-left:-358px; margin-top:-278px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:556px' src='FormProdutos.php" + myId + "'></iframe></div></div></div></div></div>");
                }
            }
            function FecharBox() {
                var oTable = $('#example').dataTable();
                oTable.fnDraw(false);
                $("#newbox").remove();
            }
        </script>                
        <?php odbc_close($con); ?>
    </body>
</html>
