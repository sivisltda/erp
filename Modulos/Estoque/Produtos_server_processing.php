<?php
set_time_limit(1800);

require_once(__DIR__ . '/../../Connections/configini.php');
require_once(__DIR__ . '/../../util/util.php');
require_once(__DIR__ . '/../../util/excel/exportaExcel.php');

if (is_numeric($_GET['empresa'])) {
    $estoque = $_GET['empresa'] > 0 ? 'dbo.ESTOQUE_FILIAL(p.conta_produto,' . $_GET['empresa'] . ')' : 'dbo.ESTOQUE_ATUAL_TOTAL(p.conta_produto)';
    $aColumns = array('conta_produto', 'codigo_interno', 'p.descricao', 'sf_contas_movimento.descricao', 'unidade_comercial', 'preco_venda',
        'estoque_minimo',
        $estoque,
        'nome_fabricante',
        '(select max(data_venda) data from sf_vendas v inner join sf_vendas_itens i on v.id_venda = i.id_venda where cov = \'C\' and status = \'Aprovado\' and produto = p.conta_produto)',
        '(select max(data_venda) data from sf_vendas v inner join sf_vendas_itens i on v.id_venda = i.id_venda where cov = \'V\' and status = \'Aprovado\' and produto = p.conta_produto)',
        'codigo_barra');
    $aColumnsA = array('', '', 'descricao', 'descr', '', '', '', 'estoqueatual', '', 'ultCompra', 'ultVenda', '');
} else {
    $aColumns = array('conta_produto', 'codigo_interno', 'p.descricao', 'sf_contas_movimento.descricao', 'unidade_comercial', 'preco_venda',
        'estoque_minimo',
        'nome_fabricante',
        '(select max(data_venda) data from sf_vendas v inner join sf_vendas_itens i on v.id_venda = i.id_venda where cov = \'C\' and status = \'Aprovado\' and produto = p.conta_produto)',
        '(select max(data_venda) data from sf_vendas v inner join sf_vendas_itens i on v.id_venda = i.id_venda where cov = \'V\' and status = \'Aprovado\' and produto = p.conta_produto)',
        'codigo_barra');
    $aColumnsA = array('', '', 'descricao', 'descr', '', '', '', '', 'ultCompra', 'ultVenda', '');
}

$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " order by " . getNameCollum(0, $aColumns[2], $aColumnsA[2]) . "," . getNameCollum(0, $aColumns[3], $aColumnsA[3]);
$sOrder2 = " order by " . getNameCollum(1, $aColumns[2], $aColumnsA[2]) . "," . getNameCollum(1, $aColumns[3], $aColumnsA[3]);
$sLimit = 20;
$imprimir = 0;

function getNameCollum($pos, $tipo, $tipoA) {
    if ($pos == 1 && strlen($tipoA) > 0) {
        return $tipoA;
    } else {
        return $tipo;
    }
}

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}
if (is_numeric($_GET['gp'])) {
    $sWhereX .= " and p.conta_movimento = " . $_GET['gp'];
}
if (is_numeric($_GET['se'])) {
    $sWhereX .= " and sem_entrada = " . $_GET['se'];
}
if (is_numeric($_GET['at'])) {
    $sWhereX .= " and p.inativa = " . $_GET['at'];
}
if (is_numeric($_GET['em']) && $_GET['em'] == 1 && is_numeric($_GET['empresa'])) {
    $sWhereX .= " and estoque_minimo > " . getNameCollum(0, $aColumns[7], $aColumnsA[7]);
}

if ($_GET['txtBusca'] != "") {
    $sWhereX = $sWhereX . " and (p.descricao like '%" . $_GET['txtBusca'] . "%'  or sf_contas_movimento.descricao like '%" . $_GET['txtBusca'] .
            "%' or unidade_comercial like '%" . $_GET['txtBusca'] . "%' or preco_venda like '%" . $_GET['txtBusca'] . "%' or nome_fabricante like '%" . $_GET['txtBusca'] . "%')";
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    if ($_GET['iSortCol_0'] == 8) {
        $sOrder = " order by nome_fabricante " . $_GET['sSortDir_0'] . ", ";
    } else {
        for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
            if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                $sOrder = " order by " . $aColumns[intval($_GET['iSortCol_0'])] . " " . $_GET['sSortDir_0'] . ", ";
                $sOrder2 = " order by " . $aColumnsA[intval($_GET['iSortCol_0'])] . " " . $_GET['sSortDir_0'] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        if ($imprimir == 0) {
            $sOrder = " order by conta_produto ";
            $sOrder2 = " order by conta_produto ";
        } else {
            $sOrder = " order by p.descricao ";
            $sOrder2 = " order by descricao ";
        }
    }
}

for ($i = 0; $i < count($aColumns); $i++) {
    $colunas .= "," . getNameCollum(0, $aColumns[$i], $aColumnsA[$i]) . " " . getNameCollum(1, $aColumns[$i], $aColumnsA[$i]);
}

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row" . $colunas . ",sem_entrada
            from sf_produtos p left join sf_contas_movimento on p.conta_movimento = sf_contas_movimento.id_contas_movimento left join sf_fabricantes on id_fabricante = cod_fabricante
            where p.tipo = 'P' " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder2;
$cur = odbc_exec($con, $sQuery1);
//echo $sQuery1;exit;

$sQuery = "SELECT COUNT(*) total from sf_produtos p left join sf_contas_movimento on p.conta_movimento = sf_contas_movimento.id_contas_movimento left join sf_fabricantes on id_fabricante = cod_fabricante where p.tipo = 'P' $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($aRow = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $aRow['total'];
}

$sQuery = "SELECT COUNT(*) total from sf_produtos p left join sf_contas_movimento on p.conta_movimento = sf_contas_movimento.id_contas_movimento left join sf_fabricantes on id_fabricante = cod_fabricante where p.tipo = 'P' $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($aRow = odbc_fetch_array($cur2)) {
    $iTotal = $aRow['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    if (file_exists("./../../Produtos/" . $contrato . "/" . $aRow["conta_produto"] . ".png")) {
        $source = file_get_contents("./../../Produtos/" . $contrato . "/" . $aRow["conta_produto"] . ".png");
        $url = "data:image/png;base64," . base64_encode($source);
    } else {
        $url = "./../../img/img.png";
    }
    if ($imprimir == 0) {
        $row[] = "<center><img src=\"" . $url . "\" style=\"width:60px; height:60px; border:1px solid #CCC\"/></center>";
    }
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow["codigo_interno"]) . "'>" . utf8_encode($aRow["codigo_interno"]) . "</div></center>";
    if ($_GET['ex'] == 1) {
        $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["descricao"]) . "'>" . utf8_encode($aRow["descricao"]) . "</div>";
    } else {
        $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . $aRow["conta_produto"] . ")'><div id='formPQ' title='" . utf8_encode($aRow["descricao"]) . "'>" . utf8_encode($aRow["descricao"]) . "</div></a>";
    }
    if ($imprimir == 0 || $_GET['ex'] == 1) {
        $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["descr"]) . "'>" . utf8_encode($aRow["descr"]) . "</div>";
    }
    $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow["unidade_comercial"]) . "'>" . utf8_encode($aRow["unidade_comercial"]) . "</div></center>";
    $row[] = "<div id='formPQ' style=\"text-align: right;\" title='" . escreverNumero($aRow['preco_venda'], 1) . "'>" . escreverNumero($aRow['preco_venda'], 1) . "</div>";
    $row[] = ($aRow['sem_entrada'] == 1 ? "" : "<center><div id='formPQ' title='" . escreverNumero($aRow['estoqueatual'], 0, 4) . "'>" . escreverNumero($aRow['estoqueatual'], 0, 4) . "</div></center>");
    $row[] = "<div><center>" . escreverData($aRow["ultCompra"]) . "</center></div>";
    $row[] = "<div><center>" . escreverData($aRow["ultVenda"]) . "</center></div>";
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["nome_fabricante"]) . "'>" . utf8_encode($aRow["nome_fabricante"]) . "</div>";
    if ($imprimir == 1) {
        $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["codigo_barra"]) . "'>" . utf8_encode($aRow["codigo_barra"]) . "</div>";
    }
    if ($imprimir == 0) {
        $row[] = "<center><a title='Excluir' href='Produtos.php?Del=" . $aRow['conta_produto'] . "'><input name='' type='image' onClick='return confirm('Deseja deletar esse registro?')' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
    }
    $output['aaData'][] = $row;
}
if ($iTotal > 0) {
    $row = $output['aaData'][0];
    $output['aaData'][0] = $row;
}
if (isset($_GET['ex']) && $_GET['ex'] == 1) {   
    $topo = "<td><b>Cód.</b></td><td><b>Desc.Produto</b></td><td><b>Desc.Grupo</b></td>
    <td><b>Uni.</b></td><td><b>Preço</b></td><td><b>Est.Atual</b></td><td><b>Ult.Compra</b></td>
    <td><b>Ult.Venda</b></td><td><b>Fabricante</b></td><td><b>Cod.Barras</b></td>";
    exportaExcel($output['aaData'], $topo);
} else {
    if (!isset($_GET['pdf'])) {
        echo json_encode($output);
    }
}
odbc_close($con);