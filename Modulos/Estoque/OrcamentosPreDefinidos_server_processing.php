<?php

include "../../Connections/configini.php";
$aColumns = array('id_venda', 'data_venda', 'razao_social', 'vendedor', 'historico_venda', 'pa', 'tp', 'ts', 'ts', 'status');
$iTotal = 0;
$iFilteredTotal = 0;
$totparc = 0;
$sWhere = "";
$sOrder = " ORDER BY data_venda desc ";

$imprimir = 0;
$F1 = "";
$F2 = "";
$F3 = "";
$F4 = "";
$DateBegin = "";
$DateEnd = "";

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = $_GET['imp'];
}

if (is_numeric($_GET['id'])) {
    $F1 = $_GET['id'];
}

if (is_numeric($_GET['tp'])) {
    $F2 = $_GET['tp'];
}

if ($_GET['td'] != '') {
    $F3 = $_GET['td'];
}

if (is_numeric($_GET['Cli'])) {
    $F4 = $_GET['Cli'];
}

$comando = "";
if ($F1 == "1") {
    $comando .= " and status = 'Aguarda' ";
} elseif ($F1 == "2") {
    $comando .= " and status = 'Aprovado' ";
} elseif ($F1 == "3") {
    $comando .= " and status = 'Reprovado' ";
}
if ($F2 == "0") {
    if ($F3 != "") {
        $comando .= " AND destinatario = '" . $F3 . "'";
    }
} elseif ($F2 == "1") {
    if ($F3 != "") {
        $comando .= " AND UPPER(vendedor) = UPPER('" . $F3 . "')";
    }
}

if (is_numeric($F4)) {
    $comando .= " and sf_vendas.cliente_venda = " . $F4;
}

if ($_GET['dti'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
    $comando .= " AND data_venda >= " . valoresDataHora2($DateBegin, "00:00:00");
}

if ($_GET['dtf'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
    $comando .= " AND data_venda <= " . valoresDataHora2($DateEnd, "23:59:59");
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . "
                                    " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY data_venda desc";
    }
}

if ($_GET['sSearch'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        if ($i >= 2 & $i <= 7) {
            $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['sSearch']) . "%' OR ";
        }
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

$sQuery1 = "set dateformat dmy; select *, ROW_NUMBER() OVER (" . $sOrder . ") as row from (select id_venda,data_venda,razao_social,nome_fantasia,UPPER(vendedor) vendedor,historico_venda,status,ISNULL((select SUM(valor_total) from sf_vendas_itens v inner join sf_produtos c on v.produto = c.conta_produto 
            where tipo = 'P' and id_venda = sf_vendas.id_venda),0) as tp, ISNULL((select SUM(valor_total) from sf_vendas_itens v inner join sf_produtos c on v.produto = c.conta_produto 
            where tipo = 'S' and id_venda = sf_vendas.id_venda),0) as ts, ISNULL((select COUNT(id_parcela) from sf_venda_parcelas where venda = sf_vendas.id_venda and inativo = 0),0) as pa,destinatario 
            ,data_aprov from sf_vendas inner join sf_fornecedores_despesas f on sf_vendas.cliente_venda = f.id_fornecedores_despesas where cov = 'O' " . $comando . ") as x where id_venda is not null " . $sWhere;

$cur = odbc_exec($con, $sQuery1);
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ($RFP = odbc_fetch_array($cur)) {
    $iFilteredTotal = $iFilteredTotal + 1;
    if (($RFP['row'] > $sLimit && $RFP['row'] <= ($sLimit + $sQtd)) or $imprimir == 1) {
        $row = array();
        if ($F1 == "1" && $imprimir == 0) {
            if (utf8_encode($RFP['destinatario']) == $_SESSION["login_usuario"]) {
                $row[] = "<center><input id=\"check\" type=\"checkbox\" onClick=\"
                    var total = contaCheckboxSelecionada();
                    if (total === 1){
                        if(document.getElementById('btnAprov') != undefined ){
                            document.getElementById('btnAprov').disabled = false;
                        }
                        if(document.getElementById('btnCance') != undefined ){
                            document.getElementById('btnCance').disabled = false;
                        }                                                                        
                    }else{
                        if (total > 1){
                            if(document.getElementById('btnAprov') != undefined ){
                                document.getElementById('btnAprov').disabled = false;
                            }
                            if(document.getElementById('btnCance') != undefined ){
                                document.getElementById('btnCance').disabled = false;
                            }                                                                            
                        }else{
                            if(document.getElementById('btnAprov') != undefined ){
                                document.getElementById('btnAprov').disabled = true;
                            }
                            if(document.getElementById('btnCance') != undefined ){
                                document.getElementById('btnCance').disabled = true;
                            }
                        }
                    }
                    \" class=\"caixa\" name=\"items[]\" value=\"" . $RFP['id_venda'] . "\"/></center>";
            } else {
                $row[] = "<div id='formPQ'><center><div style=\"border: 1px solid #BEBEBE;width: 10px;height: 10px;\"></div></center></div>";
            }
        }
        if (is_numeric($F4)) {
            $row[] = "<div id='formPQ'><center>" . utf8_encode($RFP['id_venda']) . "</center></div>";
        }
        $row[] = "<div id='formPQ'><center><a href='./../Estoque/FormOrcamentosPreDefinidos.php?id=" . $RFP['id_venda'] . "' >" . escreverDataHora($RFP['data_venda']) . "</a></center></div>";
        $nome_fantasia = "";
        if (utf8_encode($RFP["nome_fantasia"]) != "") {
            $nome_fantasia = " (" . utf8_encode($RFP["nome_fantasia"]) . ")";
        }
        $row[] = "<div id=\"formPQ\">" . utf8_encode($RFP["razao_social"]) . $nome_fantasia . "</div>";
        $row[] = "<div id=\"formPQ\"><center>" . utf8_encode($RFP["vendedor"]) . "</center></div>";
        $row[] = "<div id=\"formPQ\">" . utf8_encode($RFP["historico_venda"]) . "</div>";
        $row[] = "<div id=\"formPQ\"><center>" . utf8_encode($RFP["pa"]) . "</center></div>";
        $row[] = "<div id=\"formPQ\" style=\"text-align:right\">" . escreverNumero($RFP["tp"], 1) . "</div>";
        $row[] = "<div id=\"formPQ\" style=\"text-align:right\">" . escreverNumero($RFP["ts"], 1) . "</div>";
        if ($F4 == "") {
            $row[] = "<div id=\"formPQ\" style=\"text-align:right\">" . escreverNumero(($RFP["tp"] + $RFP["ts"]), 1) . "</div>";
        }
        $color = "style=\"color:";
        if (utf8_encode($RFP['status']) == 'Aguarda') {
            $color = $color . "#bc9f00\"";
        } elseif (utf8_encode($RFP['status']) == 'Aprovado') {
            $color = $color . "#0066cc\"";
        } elseif (utf8_encode($RFP['status']) == 'Reprovado') {
            $color = $color . "#f00\"";
        } else {
            $color = $color . "black\"";
        }
        if ($RFP['data_aprov'] != "") {
            $titleStatus = utf8_encode($RFP['status'] . " por " . $RFP['destinatario'] . ": " . escreverDataHora($RFP['data_aprov']));
        } else {
            $titleStatus = utf8_encode($RFP['status']);
        }
        $row[] = "<div id='formPQ' " . $color . " title=\"" . $titleStatus . "\"><center>" . utf8_encode($RFP['status']) . "</center></div>";
        $output['aaData'][] = $row;
    }
    $totparc = $totparc + $RFP['tp'] + $RFP['ts'];
}
if ($iFilteredTotal > 0) {
    $row = $output['aaData'][0];
    $row[1] = "<input type='hidden' name='totparc' id='totparc' value='" . $totparc . "'/>" .
            "<input type='hidden' name='qtdparc' id='qtdparc' value='" . $iFilteredTotal . "'/>" . $row[1];
    $output['aaData'][0] = $row;
}
$output['iTotalRecords'] = $iFilteredTotal;
$output['iTotalDisplayRecords'] = $iFilteredTotal;
echo json_encode($output);
odbc_close($con);
