<?php
include '../../Connections/configini.php';
if (is_numeric($_GET['Del'])) {
    odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
    values ('sf_vendas', " . $_GET['Del'] . ", '" . $_SESSION["login_usuario"] . "', 'E', 'EXCLUSAO - Entradas/Saidas', GETDATE(), null)");        
    odbc_exec($con, "DELETE FROM sf_vendas_itens WHERE id_venda = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_vendas WHERE id_venda = " . $_GET['Del']);
    echo "<script>window.top.location.href = 'Operadoras.php'; </script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>                      
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">      
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Financeiro<small>Entradas/Saídas</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="float:left;">
                                    <button  class="button button-green btn-primary" type="button" onClick="AbrirBox(0)"><span class="ico-file-4 icon-white"></span></button>
                                    <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir('I')" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                    <button id="btnExcel" class="button button-blue btn-primary" type="button" onclick="imprimir('E')" title="Exportar Excel"><span class="ico-download-3"></span></button>                                            
                                </div>
                                <div style="float:right;">   
                                    <input id="txtValue" name="txtValue" type="hidden" value=""/>
                                    <select class="select" style="width:130px" name="txtTipo" id="txtTipo">
                                        <option value="null">Selecione</option>
                                        <option value="C">ENTRADA</option>
                                        <option value="V">SAÍDA</option>
                                    </select>                                    
                                    <select class="select" style="width:130px" name="txtEstado" id="txtEstado">
                                        <option value="null">Selecione</option>
                                        <option value="1" selected>Aguardando</option>
                                        <option value="2">Aprovadas</option>
                                        <option value="3">Reprovadas</option>
                                        <option value="4">Todas</option>
                                    </select>
                                    <select class="select" style="width:175px" name="txtTipoBusca" id="txtTipoBusca">
                                        <option value="null">Selecione</option>
                                        <option value="0">Responsável</option>
                                        <option value="1">Autorização</option>
                                    </select>                                            
                                    <input style="width: 150px;vertical-align: top;color:#000 !important;" type="text" name="txtDesc" id="txtDesc" class="inputbox" value="" size="10" />                                            
                                    De <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin" name="txt_dt_begin" value="" placeholder="Data inicial">
                                    até <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end" name="txt_dt_end" value="" placeholder="Data Final">
                                    <button id="btnfind" name="btnfind" class="button button-turquoise btn-primary buttonX" type="button" onclick="refresh();" title="Buscar"><span class="ico-search icon-white"></span></button>
                                </div>                                
                            </div>                            
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Entradas/Saídas</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblOrcamento">
                            <thead>
                                <tr>
                                    <th width="10%">Data</th>                                    
                                    <th width="5%">Loja</th>                                                                       
                                    <th width="10%">Responsável</th>                                     
                                    <th width="20%">Tipo</th>                                     
                                    <th width="30%">Histórico</th>                                     
                                    <th width="10%">Autorização</th>                                     
                                    <th width="10%">Status</th>                                     
                                    <th width="5%"><center>Ação</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />                        
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>    
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>  
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            
            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
            
            $("#txtDesc").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "./../CRM/ajax.php",
                        dataType: "json",
                        data: {
                            q: request.term,
                            t: typeSelect()
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    $("#txtValue").val(ui.item.id);
                }
            });
            
            function typeSelect() {
                if ($("#txtTipoBusca").val() === "0") {
                    return "UR";
                } else if ($("#txtTipoBusca").val() === "1") {
                    return "UR";
                } else if ($("#txtTipoBusca").val() === "2") {
                    return "D";
                }
            }

            function AbrirBox(id) {
                location.href = "FormEntradas-Saidas.php" + (id > 0 ? "?id=" + id : "");
            }

            function imprimir(tipo) {
                var pRel = "&NomeArq=" + "Entradas e Saídas" +
                        "&lbl=Data|Loja|Responsável|Tipo|Histórico|Autorização|Status" +
                        "&siz=70|50|80|150|180|90|80" +
                        "&pdf=8" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=../Modulos/Estoque/" + finalFind(1).replace("?", "&"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tipo + "&PathArq=GenericModelPDF.php", '_blank');
            }
            
            $('#tblOrcamento').dataTable({
                "iDisplayLength": 20,
                "aLengthMenu": [20, 30, 40, 50, 100],
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": finalFind(0),
                "bFilter": false,
                "bSort" : false,
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"},
                "sPaginationType": "full_numbers"
            });
            
            function finalFind(imp) {
                var retorno = "?filial=" + $("#txtLojaSel").val() + "&imp=" + imp;                
                if ($("#txtEstado").val() !== "null") {
                    retorno = retorno + '&id=' + $("#txtEstado").val();
                }
                if ($("#txtTipo").val() !== "null") {
                    retorno = retorno + '&cv=' + $("#txtTipo").val();
                }
                if ($("#txtTipoBusca").val() !== "null") {
                    retorno = retorno + '&tp=' + $("#txtTipoBusca").val();
                }
                if ($("#txtValue").val() !== "") {
                    retorno = retorno + '&td=' + $("#txtValue").val();
                }                
                if ($("#txt_dt_begin").val() !== "") {
                    retorno = retorno + "&dti=" + $("#txt_dt_begin").val();
                }
                if ($("#txt_dt_end").val() !== "") {
                    retorno = retorno + "&dtf=" + $("#txt_dt_end").val();
                }                
                return "Entradas-Saidas_server_processing.php" + retorno;
            }
            
            function refresh() {
                var oTable = $('#tblOrcamento').dataTable();
                oTable.fnReloadAjax(finalFind(0));
            }
            
        </script>
        <?php odbc_close($con); ?>
    </body>
</html>
