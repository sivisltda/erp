<?php
include "../../Connections/configini.php";
$disabled = "disabled";
$active1 = "active";

if (isset($_POST["bntSave"])) {
    if ($_POST["txtId"] != "") {
        $query = "set dateformat dmy;UPDATE sf_produtos SET " .
                "descricao = " . valoresTexto("txtDescricao") .
                ",tipo = 'S' " .
                ",inativa = " . valoresCheck("txtInativa") .
                ",ativar_convite = " . valoresCheck("txtAtivarConvite") .
                ",alterar_valor = " . valoresCheck("txtAlterarVal") .
                ",favorito = " . valoresCheck("txtFavorito") .
                ",conta_movimento = " . utf8_decode($_POST["txtGrupo"]) .
                ",preco_venda = " . valoresNumericos("txtPreco_venda") .
                ",valor_comissao_venda = " . valoresNumericos("txtComissao_venda") .
                ",valor_custo_medio = " . valoresNumericos("txtCusto") .
                ",codigo_interno = " . valoresTexto("txtCodigo_interno") .
                ",desc_prod = ltrim(" . valoresTexto("ckeditor") . ")" .
                ",unidade_comercial = " . valoresTexto("txtUnidade_comercial") .
                " WHERE conta_produto = " . $_POST["txtId"];
        odbc_exec($con, $query) or die(odbc_errormsg());
        if (is_numeric($_POST["txtId"])) {
            $prod_id = $_POST["prod_id"];
            $prod_cod = $_POST["prod_cod"];
            $notIn = 0;
            if (is_array($prod_id)) {
                for ($i = 0; $i < sizeof($prod_id); $i++) {
                    if (is_numeric($prod_id[$i])) {
                        $notIn = $notIn . "," . $prod_id[$i];
                    }
                }
            }
            odbc_exec($con, "DELETE FROM sf_produtos_materiaprima WHERE id_produtomp = " . $_POST["txtId"] . " and id_prodmp not in (" . $notIn . ")");
            if (is_array($prod_cod)) {
                for ($i = 0; $i < sizeof($prod_cod); $i++) {
                    if (is_numeric($prod_id[$i])) {
                        odbc_exec($con, "UPDATE sf_produtos_materiaprima set id_materiaprima = " . $prod_cod[$i] . ",quantidademp = 0 WHERE id_prodmp = " . $prod_id[$i]);
                    } else {
                        odbc_exec($con, "INSERT INTO sf_produtos_materiaprima(id_produtomp,quantidademp,id_materiaprima) VALUES (" . $_POST["txtId"] . ",0," . $prod_cod[$i] . ")");
                    }
                }
            }
        }
    } else {
        $query = "INSERT INTO sf_produtos(descricao,tipo,quantidade_comercial,inativa,ativar_convite,alterar_valor,conta_movimento,
        favorito,preco_venda,valor_comissao_venda,codigo_interno,desc_prod,unidade_comercial,valor_custo_medio,editavel) VALUES(" .
                valoresTexto("txtDescricao") . "," .
                "'S',1.00," .
                valoresCheck("txtInativa") . "," .
                valoresCheck("txtAtivarConvite") . "," .
                valoresCheck("txtAlterarVal") . "," .           
                utf8_decode($_POST["txtGrupo"]) . "," .
                valoresCheck("txtFavorito") . "," .
                valoresNumericos("txtPreco_venda") . "," .
                valoresNumericos("txtComissao_venda") . "," .
                valoresTexto("txtCodigo_interno") . "," .
                "LTRIM(" . valoresTexto("ckeditor") . ")," .
                valoresTexto("txtUnidade_comercial") . "," .
                valoresNumericos("txtCusto") . ",0)";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 conta_produto from sf_produtos where conta_produto not in (466) order by conta_produto desc") or die(odbc_errormsg());
        $id = odbc_result($res, 1);
        $_POST["txtId"] = $id;
        if (is_numeric($id)) {
            $prod_cod = $_POST["prod_cod"];
            for ($i = 0; $i < sizeof($prod_cod); $i++) {
                odbc_exec($con, "INSERT INTO sf_produtos_materiaprima(id_produtomp,quantidademp,id_materiaprima) VALUES (" . $id . ",0," . $prod_cod[$i] . ")");
            }
        }
    }
}
if (isset($_POST["bntDelete"])) {
    if ($_POST["txtId"] != "") {
        odbc_exec($con, "DELETE FROM sf_produtos WHERE conta_produto = " . $_POST["txtId"]);
        echo "<script>alert('Registro excluido com sucesso');window.top.location.href = 'Servicos.php';</script>";
    }
}
if (isset($_POST["bntNew"])) {
    $_GET["id"] = "";
    $_POST["txtId"] = "";
}
if ($_GET["id"] != "" || $_POST["txtId"] != "") {
    $PegaURL = $_GET["id"];
    if ($_POST["txtId"] != "") {
        $disabled = "disabled";
        $PegaURL = $_POST["txtId"];
    }
    $cur = odbc_exec($con, "select * from sf_produtos where conta_produto =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP["conta_produto"];
        $descricao = utf8_encode($RFP["descricao"]);
        $grupo = $RFP["conta_movimento"];
        $preco_venda = escreverNumero($RFP["preco_venda"]);
        $comissao_venda = escreverNumero($RFP["valor_comissao_venda"]);
        $custo = escreverNumero($RFP["valor_custo_medio"]);
        $prod_favorito = $RFP["favorito"];
        $prod_inativo = $RFP["inativa"];
        $ativar_convite = $RFP["ativar_convite"];
        $txtAlterarVal = $RFP["alterar_valor"];
        $codigo_interno = utf8_encode($RFP["codigo_interno"]);
        $texto_desc = utf8_encode($RFP["desc_prod"]);
        $unidade_comercial = utf8_encode($RFP["unidade_comercial"]);
    }
} else {
    $disabled = "";
    $id = "";
    $descricao = "";
    $grupo = "";
    $preco_venda = escreverNumero(0);
    $comissao_venda = escreverNumero(0);
    $custo = escreverNumero(0);
    $prod_favorito = "";
    $prod_inativo = "";
    $ativar_convite = "";
    $txtAlterarVal = "";
    $codigo_interno = "";
    $texto_desc = "";
    $unidade_comercial = "";
}
if (isset($_POST["bntEdit"])) {
    if ($_POST["txtId"] != "") {
        $disabled = "";
    }
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<link href="../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<link href="../../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../js/jcrop/jquery.Jcrop.min.css" type="text/css"/>
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
<body>
    <div class="row-fluid">
        <form action="FormServicos.php" method="POST" name="frmEnviaDados" id="frmEnviaDados" enctype="multipart/form-data">
            <div class="frmhead">
                <div class="frmtext">Serviços</div>
                <div class="frmicon" onClick="parent.FecharBox(1)">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="tabbable frmtabs">
                <ul class="nav nav-tabs" style="margin:0">
                    <li class="active"><a href="#tab1" data-toggle="tab">Geral</a></li>
                    <li><a href="#tab2" data-toggle="tab">Descrição</a></li>
                    <li><a href="#tab3" data-toggle="tab">Pacote</a></li>
                </ul>
                <div class="tab-content frmcont" style="height:200px;">
                    <div class="tab-pane active" id="tab1">
                        <table width="100%" border="0" cellpadding="3" class="textosComuns">
                            <tr>
                                <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                                <td width="102" align="right">Nome do Serviço:</td>
                                <td colspan="3"><input name="txtDescricao" id="txtDescricao" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $descricao; ?>"/></td>
                            </tr>
                            <tr>
                                <td align="right">Grupo:</td>
                                <td><select name="txtGrupo" id="txtGrupo" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                                    <option value="null" >Selecione o grupo:</option>
                                    <?php
                                    $sql = "select id_contas_movimento,descricao from sf_contas_movimento where tipo = 'S' ORDER BY descricao";
                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                        <option value="<?php echo $RFP["id_contas_movimento"] ?>"<?php
                                        if (!(strcmp($RFP["id_contas_movimento"], $grupo))) {
                                            echo "SELECTED";
                                        }
                                        ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                    <?php } ?>
                                    </select>
                                </td>
                                <td align="right">Valor de Venda:</td>
                                <td><input style="text-align:right" name="txtPreco_venda" id="txtPreco_venda" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $preco_venda; ?>"/></td>
                            </tr>
                            <tr>
                                <td align="right">Custo do Serviço:</td>
                                <td><input style="text-align:right" name="txtCusto" id="txtCusto" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $custo; ?>"/></td>
                                <td align="right">Unidade:</td>
                                <td>
                                    <select name="txtUnidade_comercial" id="txtUnidade_comercial" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                                        <option value="UN" <?php
                                        if ($unidade_comercial == "UN") {
                                            echo "SELECTED";
                                        }
                                        ?>>Unitários</option>
                                        <option value="MQ" <?php
                                        if ($unidade_comercial == "MQ") {
                                            echo "SELECTED";
                                        }
                                        ?>>Metros Quadrados</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">Comissão Venda (%):</td>
                                <td><input style="text-align:right" name="txtComissao_venda" id="txtComissao_venda" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $comissao_venda; ?>"/></td>
                                <td align="right">Ativar Convite:</td>
                                <td><input name="txtAtivarConvite" id="txtAtivarConvite" <?php echo $disabled; ?> type="checkbox" <?php
                                    if ($ativar_convite == "1") {
                                        echo "CHECKED";
                                    }
                                    ?> value="1" class="input-medium"/></td>
                            </tr>
                            <tr>
                                <td align="right">Código:</td>
                                <td><input name="txtCodigo_interno" id="txtCodigo_interno" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $codigo_interno; ?>"/></td>
                                <td align="right">Alterar Valor no Pag.</td>
                                <td><input name="txtAlterarVal" id="txtAlterarVal" <?php echo $disabled; ?> type="checkbox" <?php
                                    if ($txtAlterarVal == "1") {
                                        echo "CHECKED";
                                    }
                                    ?> value="1" class="input-medium"/></td>                                
                            </tr>
                            <tr>
                                <td align="right">Inativo:</td>
                                <td><input name="txtInativa" id="txtInativa" <?php echo $disabled; ?> type="checkbox" <?php
                                    if ($prod_inativo == "1") {
                                        echo "CHECKED";
                                    }
                                    ?> value="1" class="input-medium"/></td>
                                <td align="right">Favorito:</td>
                                <td><input name="txtFavorito" id="txtFavorito" <?php echo $disabled; ?> type="checkbox" <?php
                                    if ($prod_favorito == "1") {
                                        echo "CHECKED";
                                    }
                                    ?> value="1" class="input-medium"/></td>                                
                            </tr>
                        </table>
                    </div>
                    <div class="tab-pane" id="tab2">
                        <label for="ckeditor"></label>
                        <textarea id="ckeditor" name="ckeditor" <?php echo $disabled; ?>>
                            <?php echo $texto_desc; ?>
                        </textarea>
                    </div>
                    <div class="tab-pane" id="tab3">
                        <table width="100%" border="0" cellpadding="3" class="textosComuns">
                            <tr>
                                <td width="50" align="right">Serviço:</td>
                                <td width="300">
                                    <select name="txtPrdMP" id="txtPrdMP" <?php echo $disabled; ?> style="width:100%">
                                        <option value="null" >Selecione o Serviço:</option>
                                        <?php
                                        $sql = "select conta_produto,descricao from dbo.sf_produtos where tipo = 'S' and inativa = 0 and conta_produto not in(select distinct(id_produtomp) from dbo.sf_produtos_materiaprima) ORDER BY descricao";
                                        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) {
                                            ?>
                                            <option value="<?php echo $RFP["conta_produto"] ?>"><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td width="75" align="right">Quantidade:</td>
                                <td width="50"><input name="txtQuant" id="txtQuant" <?php echo $disabled; ?> maxlength="5" type="text" class="input-medium" value="1"/></td>
                                <td width="100" height="37" align="right">
                                    <button class="btn btn-success" <?php echo $disabled; ?> type="button" name="bntSaveMP" onClick="novaLinha()"><span class="ico-checkmark"></span> Adicionar</button>
                                <td>
                            </tr>
                        </table>
                        <table width="100%" cellpadding="8" border="1" bordercolor="#FFF" style="background:#E9E9E9; font-size:12px">
                            <tr>
                                <td style="width:30px; line-height:18px"><b>#</b></td>
                                <td style="line-height:18px"><b>Produto</b></td>
                                <td style="line-height:18px; width:53px"><b>Ação</b></td>
                            </tr>
                        </table>
                        <div style="overflow-y:scroll; height:127px; border-left:1px solid #FFF; border-right:1px solid #FFF">
                            <table id="listMatPrim" width="100%" cellpadding="3" border="1" bordercolor="#FFF" style="font-size:12px">
                                <?php
                                if ($id != "") {
                                    $conta = 1000;
                                    $cur = odbc_exec($con, "select id_prodmp,id_materiaprima,descricao from sf_produtos_materiaprima inner join sf_produtos on id_materiaprima = conta_produto where id_produtomp = " . $id . " order by descricao");
                                    while ($RFP = odbc_fetch_array($cur)) {
                                        ?>
                                        <tr id="list_<?php echo $conta; ?>">
                                        <input type="hidden" name="prod_id[]" value="<?php echo $RFP["id_prodmp"]; ?>">
                                        <input type="hidden" name="prod_cod[]" value="<?php echo $RFP["id_materiaprima"]; ?>">
                                        <td style="line-height:23px"><?php echo utf8_encode($RFP["descricao"]); ?></td>
                                        <td style="width:46px; text-align:center"><a onClick="removeLinha(<?php echo $conta; ?>)" href="javascript:void(0)"><img src="../../img/1365123843_onebit_33 copy.PNG" width="16" height="16"/></a></td>
                                        </tr>
                                        <?php
                                        $conta = $conta + 1;
                                    }
                                }
                                ?>
                            </table>
                        </div>                        
                    </div>
                </div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == "") { ?>
                        <button class="btn green" type="submit" name="bntSave"><span class="ico-checkmark"></span> Gravar</button>
                        <?php if ($_POST["txtId"] == "") { ?>
                            <button class="btn yellow" onClick="parent.FecharBox(1)"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                            <button class="btn yellow" type="submit" name="bntAlterar"><span class="ico-reply"></span> Cancelar</button>
                        <?php
                        }
                    } else { ?>
                        <button class="btn green" type="submit" name="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" type="submit" name="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                        <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
    <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
    <script type="text/javascript" src="../../js/plugins/other/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/animatedprogressbar/animated_progressbar.js"></script>
    <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../js/plugins.js"></script>
    <script type="text/javascript" src="../../js/charts.js"></script>
    <script type="text/javascript" src="../../js/actions.js"></script>
    <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../js/jquery.priceformat.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type="text/javascript">
        
        $(document).ready(function () {
            CKEDITOR.replace("ckeditor", {removePlugins: "elementspath", height: "128px"});
            $("#txtPreco_venda, #txtCusto, #txtComissao_venda").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"], suffix: ""});
        });
        
        jQuery.validator.addMethod("notEqual", function (value, element, param) {
            return this.optional(element) || value !== param;
        }, "Please specify a different (non-default) value");
        $("#frmEnviaDados").validate({
            errorPlacement: function (error, element) {
                return true;
            },
            rules: {
                txtDescricao: {required: true},
                txtPreco_venda: {notEqual: numberFormat(0)}
            }
        });                            
        var conta = 1;
        function novaLinha() {
            if ($("#txtPrdMP").val() !== "" && $("#txtQuant").val() > 0 && $("#txtQuantidadeEnt").val() !== "") {
                $.getJSON("conta_a.ajax.php", {txtConta: $("#txtPrdMP").val(), txtQtd: $("#txtQuantidadeEnt").val(), ajax: "true"}, function (j) {
                    for (var i = 0; i < $("#txtQuant").val(); i++) {
                        var listMatPrim = "<tr id=\"list_" + conta + "\">";
                        listMatPrim = listMatPrim + "	<input type=\"hidden\" name=\"prod_id[]\" value=\"\">";
                        listMatPrim = listMatPrim + "	<input type=\"hidden\" name=\"prod_cod[]\" value=\"" + $("#txtPrdMP").val() + "\">";
                        listMatPrim = listMatPrim + "	<td style=\"width:40px; line-height:23px\">" + conta + "</td>";
                        listMatPrim = listMatPrim + "	<td style=\"line-height:23px\">" + $("#txtPrdMP option:selected").text() + "</td>";
                        listMatPrim = listMatPrim + "	<td style=\"width:46px; text-align:center\">";
                        listMatPrim = listMatPrim + "		<a onClick=\"removeLinha(" + conta + ")\" href=\"javascript:void(0)\">";
                        listMatPrim = listMatPrim + "			<img src=\"../../img/1365123843_onebit_33 copy.PNG\" width=\"16\" height=\"16\"/>";
                        listMatPrim = listMatPrim + "		</a>";
                        listMatPrim = listMatPrim + "	</td>";
                        listMatPrim = listMatPrim + "</tr>";
                        $("#listMatPrim").append(listMatPrim);
                        conta = conta + 1;
                    }
                });
            }
        }
        function removeLinha(id) {
            $("#list_" + id).remove();
        }
    </script>    
    <?php odbc_close($con); ?>
</body>
