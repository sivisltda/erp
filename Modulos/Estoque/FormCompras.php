<?php
include "../../Connections/configini.php";
$FinalUrl = '';
$TotalGeral = 0;
$pasta = "./../../Pessoas/" . $contrato . "/NFE_COMPRAS";

if ($_GET['idx'] != '') {
    $FinalUrl = "&idx=" . $_GET['idx'];
}
if ($_GET['tpx'] != '') {
    $FinalUrl = $FinalUrl . "&tpx=" . $_GET['tpx'];
}
if ($_GET['tdx'] != '') {
    $FinalUrl = $FinalUrl . "&tdx=" . $_GET['tdx'];
}
if ($_GET['dti'] != '') {
    $FinalUrl = $FinalUrl . "&dti=" . $_GET['dti'];
}
if ($_GET['dtf'] != '') {
    $FinalUrl = $FinalUrl . "&dtf=" . $_GET['dtf'];
}
if ($_GET['id'] != '') {
    $FinalUrl = "id=" . $_GET['id'] . $FinalUrl;
}

$cur = odbc_exec($con, "select * from dbo.sf_configuracao");
while ($RFP = odbc_fetch_array($cur)) {
    $CheckCodigoProduto = $RFP['FIN_ORC_CODIGO_PRODUTO'];
}

$disabled = 'disabled';
$PegaURL = '';
$r_documento = '';
$r_ven_ini = '';
$r_valor = '';
$r_valor_tot = '';
$r_quantidade = '1';

if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        $query = "set dateformat dmy;update sf_vendas set " .
                "cliente_venda = " . $_POST['txtFonecedor'] . "," .
                "historico_venda = '" . utf8_decode($_POST['txtHistorico']) . "'," .
                "vendedor = '" . strtoupper(utf8_decode($_POST['txtDestinatario'])) . "'," .
                "destinatario = '" . utf8_decode($_POST['txtAutorizacao']) . "'," .
                "tipo_documento = " . $_POST['txtTipo'] . "," .
                "cov = 'C'," .
                "garantia_venda = '" . utf8_decode($_POST['txtGarantiaProd']) . "'," .
                "garantia_servico = '" . utf8_decode($_POST['txtGarantiaServ']) . "'," .
                "descontop = " . valoresNumericos('txtProdDesconto' . $i) . "," .
                "descontos = " . valoresNumericos('txtServDesconto' . $i) . "," .
                "entrega = '" . utf8_decode($_POST['txtDtEntrega']) . "'," .
                "validade = '" . utf8_decode($_POST['txtDtProposta']) . "'," .
                "descontots = " . utf8_decode($_POST['txtServSinal']) . "," .
                "descontotp = " . utf8_decode($_POST['txtProdSinal']) . "," .
                "grupo_conta = " . $_POST['txtGrupoConta'] . "," .
                "conta_movimento = " . $_POST['txtContaConta'] . "," .
                "empresa = " . $_POST['txtEmpresa'] . "," .
                "cod_pedido = " . valoresTexto("txtComanda") . "," .
                "n_produtos = " . valoresCheck('ckbNValores_Produtos') . "," .
                "comentarios_venda = '" . utf8_decode($_POST['txtComentarios']) . "' where id_venda = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        //-------------------------------Campos-de-Preenchimento-para-Documentos------------------------
        odbc_exec($con, "delete from sf_vendas_campos where id_sf_vendas = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
        $sql = "select id_tipo_documento_campos,campo from sf_tipo_documento_campos where inativo = 0 AND tipo_documento = " . $_POST['txtTipo'] . " ORDER BY 1";
        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $sss = "insert into sf_vendas_campos(id_sf_vendas,tipo_documento_campos,conteudo) values (" .
                    $_POST['txtId'] . "," .
                    $RFP['id_tipo_documento_campos'] . ",'" .
                    utf8_decode($_POST['txtCampo' . $RFP['id_tipo_documento_campos']]) . "')";
            odbc_exec($con, $sss);
        }
        //-----------------------------------------------------------------------------------------------
        $notIn = '0';
        if (isset($_POST['txtIdTP']) || isset($_POST['txtIdTS'])) {
            $maxP = $_POST['txtIdTP'];
            for ($i = 0; $i <= $maxP; $i++) {
                if (isset($_POST['txtIP_' . $i])) {
                    if (is_numeric($_POST['txtIP_' . $i])) {
                        $notIn = $notIn . "," . $_POST['txtIP_' . $i];
                    }
                }
            }
            $maxS = $_POST['txtIdTS'];
            for ($i = 0; $i <= $maxS; $i++) {
                if (isset($_POST['txtIPS_' . $i])) {
                    if (is_numeric($_POST['txtIPS_' . $i])) {
                        $notIn = $notIn . "," . $_POST['txtIPS_' . $i];
                    }
                }
            }
            odbc_exec($con, "update sf_vendas_itens_pacotes set venda_aplicada = null where venda_aplicada in(
                                        select id_item_venda from sf_vendas_itens where id_venda = " . $_POST['txtId'] . " and id_item_venda not in (" . $notIn . "))");
            odbc_exec($con, "delete from dbo.sf_vendas_itens_pacotes where venda_item in(
                                        select id_item_venda from sf_vendas_itens where id_venda = " . $_POST['txtId'] . " and id_item_venda not in (" . $notIn . "))");
            odbc_exec($con, "delete from sf_vendas_itens where id_venda = " . $_POST['txtId'] . " and id_item_venda not in (" . $notIn . ")");
        }
        if (isset($_POST['txtIdTP'])) {
            $maxP = $_POST['txtIdTP'];
            for ($i = 0; $i <= $maxP; $i++) {
                if (isset($_POST['txtIP_' . $i])) {
                    if (is_numeric($_POST['txtIP_' . $i])) {
                        odbc_exec($con, "update sf_vendas_itens set grupo = " . valoresSelect('txtGR_' . $i) . ",produto = " . $_POST['txtPD_' . $i] . ",quantidade = " . valoresNumericos('txtVQ_' . $i) . " ,valor_total = " . valoresNumericos('txtVP_' . $i) . " where id_item_venda = " . $_POST['txtIP_' . $i]) or die(odbc_errormsg());
                    } else {
                        odbc_exec($con, "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total) values(" . $_POST['txtId'] . "," . valoresSelect('txtGR_' . $i) . "," . $_POST['txtPD_' . $i] . "," . valoresNumericos('txtVQ_' . $i) . "," . valoresNumericos('txtVP_' . $i) . ")") or die(odbc_errormsg());
                    }
                }
            }
        }
        if (isset($_POST['txtIdTS'])) {
            $maxS = $_POST['txtIdTS'];
            for ($i = 0; $i <= $maxS; $i++) {
                if (isset($_POST['txtIPS_' . $i])) {
                    if (is_numeric($_POST['txtIPS_' . $i])) {
                        odbc_exec($con, "update sf_vendas_itens set grupo = " . valoresSelect('txtGRS_' . $i) . ",produto = " . $_POST['txtPDS_' . $i] . ",quantidade = " . valoresNumericos('txtVQS_' . $i) . " ,valor_total = " . valoresNumericos('txtVPS_' . $i) . " where id_item_venda = " . $_POST['txtIPS_' . $i]) or die(odbc_errormsg());
                    } else {
                        odbc_exec($con, "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total) values(" . $_POST['txtId'] . "," . valoresSelect('txtGRS_' . $i) . "," . $_POST['txtPDS_' . $i] . "," . valoresNumericos('txtVQS_' . $i) . "," . valoresNumericos('txtVPS_' . $i) . ")") or die(odbc_errormsg());
                    }
                }
            }
        }
        //----------------------------Calcula--Número--de--Parcelas-------------------------------------
        $totaldeParcelas = 0;
        for ($i = 1; $i <= 60; $i++) {
            $explode = valoresData('txtParcelaD_' . $i);
            $valorParcela = valoresNumericos('txtParcelaV_' . $i);           
            if (is_numeric($valorParcela) && $explode != "null") {
                if ($valorParcela > 0) {
                    $totaldeParcelas = $totaldeParcelas + 1;
                }
            }
        }
        for ($i = 1; $i <= 60; $i++) {
            if (is_numeric($_POST['txtID_' . $i])) {
                if ($_POST['txtPG_' . $i] == "S") {
                    $totaldeParcelas = $totaldeParcelas + 1;
                }
            }
        }
        //----------------------------Altera--as--Parcelas----------------------------------------------
        $toDelete = "0";
        for ($i = 1; $i <= 60; $i++) {
            $explode = valoresData('txtParcelaD_' . $i);
            $valorParcela = valoresNumericos('txtParcelaV_' . $i);           
            if (is_numeric($valorParcela) && $explode != "null") {
                if ($valorParcela > 0) {
                    if ($_POST['txtPG_' . $i] != "S") {
                        if (is_numeric($_POST['txtID_' . $i])) {
                            odbc_exec($con, "update sf_venda_parcelas set
                                            venda = " . $_POST['txtId'] . "," .
                                            "numero_parcela = " . $i . "," .
                                            "data_parcela = '" . $_POST['txtParcelaD_' . $i] . "'," .
                                            "sa_descricao = '" . $_POST['txtParcelaDC_' . $i] . "'," .
                                            "valor_parcela = " . $valorParcela . "," .
                                            "tipo_documento = " . $_POST['txtTipo_' . $i] . "," .
                                            "pa = '" . str_pad($i, 2, "0", STR_PAD_LEFT) . "/" . str_pad($totaldeParcelas, 2, "0", STR_PAD_LEFT) . "' " .
                                            "where id_parcela = " . $_POST['txtID_' . $i]) or die(odbc_errormsg());
                            $toDelete = $toDelete . "," . $_POST['txtID_' . $i];
                        }
                    }
                }
            }
        }
        //----------------------------Exclui--as--Parcelas--Removidas-----------------------------------
        odbc_exec($con, "DELETE FROM sf_venda_parcelas where id_parcela not in (" . $toDelete . ") and data_pagamento is null and inativo = 0 and venda = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
        //----------------------------Insere--as--Parcelas-Novas----------------------------------------
        for ($i = 1; $i <= 60; $i++) {
            $explode = valoresData('txtParcelaD_' . $i);
            $valorParcela = valoresNumericos('txtParcelaV_' . $i);           
            if (is_numeric($valorParcela) && $explode != "null") {
                if ($valorParcela > 0) {
                    if ($_POST['txtPG_' . $i] != "S") {
                        if ($_POST['txtID_' . $i] == "") {
                            odbc_exec($con, "insert into sf_venda_parcelas(venda,numero_parcela,data_parcela,sa_descricao,valor_parcela,tipo_documento,historico_baixa,pa,data_cadastro) values (" .
                                            $_POST['txtId'] . "," . $i . ",'" . $_POST['txtParcelaD_' . $i] . "','" . $_POST['txtParcelaDC_' . $i] . "'," . $valorParcela . "," . $_POST['txtTipo_' . $i] . ",'" . utf8_decode($_POST['txtHistorico']) . "','" . str_pad($i, 2, "0", STR_PAD_LEFT) . "/" . str_pad($totaldeParcelas, 2, "0", STR_PAD_LEFT) . "',getdate())") or die(odbc_errormsg());
                            $toInsert = $toInsert . "," . $i;
                        }
                    }
                }
            }
        }
        //----------------------------(editar)-----Geração--de--Solicitação--de--Autorização----------
        if (isset($_POST['txtTransportadora'])) {
            $explode = valoresData('txtDtVenda');
            $valorParcela = valoresNumericos('txtValorFrete');            
            if ($_POST['txtTransportadora'] != "null" && is_numeric($valorParcela) && $explode != "null") {
                $gc = 0;
                $cm = 0;
                $doc = 0;
                $cur = odbc_exec($con, "select id_contas_movimento,id_grupo_contas from sf_contas_movimento cm inner join sf_grupo_contas gc on cm.grupo_conta = gc.id_grupo_contas where cm.descricao = '" . utf8_decode("FRETE") . "' and gc.descricao in ('TRANSPORTE', 'DESPESAS EVENTUAIS')");
                while ($RFP = odbc_fetch_array($cur)) {
                    $cm = $RFP['id_contas_movimento'];
                    $gc = $RFP['id_grupo_contas'];
                }
                $cur = odbc_exec($con, "select id_tipo_documento from dbo.sf_tipo_documento where descricao = 'DINHEIRO'");
                while ($RFP = odbc_fetch_array($cur)) {
                    $doc = $RFP['id_tipo_documento'];
                }
                if ($valorParcela > 0 && $gc > 0 && $cm > 0 && $doc > 0) {
                    $sa = '';
                    $query = "set dateformat dmy;insert into sf_solicitacao_autorizacao(empresa,grupo_conta,conta_movimento,fornecedor_despesa,tipo_documento,historico,destinatario,comentarios,sys_login,data_lanc,status)values('001'," .
                            $gc . "," .
                            $cm . "," .
                            $_POST['txtTransportadora'] . "," .
                            $doc . ",'" .
                            utf8_decode($_POST['txtHistorico']) . "','" .
                            utf8_decode($_POST['txtAutorizacao']) . "','" .
                            utf8_decode($_POST['txtComentarios']) . "','SYSADM',GetDate(),'Aguarda')";
                    odbc_exec($con, $query) or die(odbc_errormsg());
                    $res = odbc_exec($con, "select top 1 id_solicitacao_autorizacao from sf_solicitacao_autorizacao order by id_solicitacao_autorizacao desc") or die(odbc_errormsg());
                    $sa = odbc_result($res, 1);
                    //----------------------------------------------Parcelas-----------------------------------------
                    odbc_exec($con, "set dateformat dmy; insert into sf_solicitacao_autorizacao_parcelas(solicitacao_autorizacao,numero_parcela,data_parcela,sa_descricao,valor_parcela,historico_baixa,pa,bol_id_banco,bol_data_criacao,bol_valor,bol_juros,bol_multa,bol_nosso_numero,data_cadastro) values (" .
                                    $sa . ",1,'" . $_POST['txtDtVenda'] . "',''," . $valorParcela . ",'" . utf8_decode($_POST['txtHistorico']) . "','01/01',null,null,null,null,null,null,getdate())") or die(odbc_errormsg());
                    odbc_exec($con, "INSERT INTO sf_frete_historico(frete_venda,frete_transportadora,frete_solicitacao_autorizacao) VALUES (" . $_POST['txtId'] . "," . $_POST['txtTransportadora'] . "," . $sa . ")") or die(odbc_errormsg());
                } else {
                    echo "<script>alert('Verifique os campos necessários para geração de conta do tipo FRETE!');</script>";
                }
            }
        }
        //--------------------------------------------------------------------------------------------
    } else {
        $nossoNumero = "null";
        $bol_id_banco = "null";
        $bol_data_criacao = "null";
        $bol_valor = "null";
        $bol_juros = "null";
        $bol_multa = "null";
        if (isset($_POST['txtBanco'])) {
            if ($_POST['txtBanco'] != "null") {
                $cur = odbc_exec($con, "select isnull(MAX(x.bol_nosso_numero),0) vm, isnull(nosso_numero,0) nn from (
                select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_boleto p on p.bol_id_banco = b.id_bancos
                where id_bancos = '" . $_POST['txtBanco'] . "' union all
                select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_solicitacao_autorizacao_parcelas p on p.bol_id_banco = b.id_bancos
                where id_bancos = '" . $_POST['txtBanco'] . "' union all
                select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_venda_parcelas p on p.bol_id_banco = b.id_bancos
                where id_bancos = '" . $_POST['txtBanco'] . "' union all
                select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_lancamento_movimento_parcelas l on l.bol_id_banco = b.id_bancos
                where id_bancos = '" . $_POST['txtBanco'] . "') as x group by x.nosso_numero") or die(odbc_errormsg());
                while ($RFP = odbc_fetch_array($cur)) {
                    if ($RFP['nn'] > $RFP['vm']) {
                        $nossoNumero = $RFP['nn'];
                    } else {
                        $nossoNumero = bcadd($RFP['vm'], 1);
                    }
                }
                $bol_id_banco = $_POST['txtBanco'];
                $bol_data_criacao = "getdate()";
                $bol_juros = "0.00";
                $bol_multa = "0.00";
            }
        }
        $query = "set dateformat dmy;insert into sf_vendas(vendedor,cliente_venda,historico_venda,data_venda,destinatario,
        status,tipo_documento,descontop,descontos,entrega,validade,descontots,descontotp,comentarios_venda,sys_login,cov,
        garantia_venda,garantia_servico,grupo_conta,conta_movimento,empresa, n_produtos, cod_pedido)values('" .
                strtoupper(utf8_decode($_POST['txtDestinatario'])) . "'," .
                $_POST['txtFonecedor'] . ",'" .
                utf8_decode($_POST['txtHistorico']) . "','" .
                $_POST['txtDtVenda'] . " " . date("H:i:s") . "','" .
                utf8_decode($_POST['txtAutorizacao']) . "','Aguarda'," .
                $_POST['txtTipo'] . "," .
                valoresNumericos('txtProdDesconto') . "," .
                valoresNumericos('txtServDesconto') . ",'" .
                utf8_decode($_POST['txtDtEntrega']) . "','" .
                utf8_decode($_POST['txtDtProposta']) . "'," .
                utf8_decode($_POST['txtServSinal']) . "," .
                utf8_decode($_POST['txtProdSinal']) . ",'" .
                utf8_decode($_POST['txtComentarios']) . "','" . $_SESSION["login_usuario"] . "','C','" . 
                utf8_decode($_POST['txtGarantiaProd']) . "','" . utf8_decode($_POST['txtGarantiaServ']) . "'," . 
                $_POST['txtGrupoConta'] . "," . 
                $_POST['txtContaConta'] . "," . 
                $_POST['txtEmpresa'] . "," . 
                valoresCheck('ckbNValores_Produtos') . "," . 
                valoresTexto("txtComanda") . ")";
        //echo $query;
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_venda from sf_vendas order by id_venda desc") or die(odbc_errormsg());
        $nn = odbc_result($res, 1);
        //-----------------------------------------Campos--de--descricao------------------------------
        $sql = "select id_tipo_documento_campos,campo from sf_tipo_documento_campos where inativo = 0 AND tipo_documento = " . $_POST['txtTipo'] . " ORDER BY 1";
        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $sss = "insert into sf_vendas_campos(id_sf_vendas,tipo_documento_campos,conteudo) values (" .
                    $nn . "," .
                    $RFP['id_tipo_documento_campos'] . ",'" .
                    utf8_decode($_POST['txtCampo' . $RFP['id_tipo_documento_campos']]) . "')";
            //echo $sss;
            odbc_exec($con, $sss);
        }
        //--------------------------------------------------------------------------------------------
        if (isset($_POST['txtIdTP'])) {
            $maxP = $_POST['txtIdTP'];
            for ($i = 0; $i <= $maxP; $i++) {
                if (isset($_POST['txtIP_' . $i])) {
                    odbc_exec($con, "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total) values(" . $nn . "," . valoresSelect('txtGR_' . $i) . "," . $_POST['txtPD_' . $i] . "," . valoresNumericos('txtVQ_' . $i) . "," . valoresNumericos('txtVP_' . $i) . ")") or die(odbc_errormsg());
                }
            }
        }
        if (isset($_POST['txtIdTS'])) {
            $maxS = $_POST['txtIdTS'];
            for ($i = 0; $i <= $maxS; $i++) {
                if (isset($_POST['txtIPS_' . $i])) {
                    odbc_exec($con, "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total) values(" . $nn . "," . valoresSelect('txtGRS_' . $i) . "," . $_POST['txtPDS_' . $i] . "," . valoresNumericos('txtVQS_' . $i) . "," . valoresNumericos('txtVPS_' . $i) . ")") or die(odbc_errormsg());
                }
            }
        }
        $_POST['txtId'] = $nn;
        $totaldeParcelas = 0;
        for ($i = 1; $i <= 60; $i++) {
            $explode = valoresData('txtParcelaD_' . $i);
            $valorParcela = valoresNumericos('txtParcelaV_' . $i);           
            if (is_numeric($valorParcela) && $explode != "null") {
                if ($valorParcela > 0) {
                    $totaldeParcelas = $totaldeParcelas + 1;
                }
            }
        }
        for ($i = 1; $i <= 60; $i++) {
            $explode = valoresData('txtParcelaD_' . $i);
            $valorParcela = valoresNumericos('txtParcelaV_' . $i);                       
            if (is_numeric($valorParcela) && $explode != "null") {
                if ($valorParcela > 0) {
                    if ($bol_id_banco != "null") {
                        $bol_valor = $valorParcela;
                    }
                    odbc_exec($con, "set dateformat dmy; insert into sf_venda_parcelas(venda,numero_parcela,data_parcela,sa_descricao,valor_parcela,tipo_documento,historico_baixa,pa,bol_id_banco,bol_data_criacao,bol_valor,bol_juros,bol_multa,bol_nosso_numero,data_cadastro) values (" .
                                    $_POST['txtId'] . "," . $i . ",'" . $_POST['txtParcelaD_' . $i] . "','" . $_POST['txtParcelaDC_' . $i] . "'," . $valorParcela . "," . $_POST['txtTipo_' . $i] . ",'" . utf8_decode($_POST['txtHistorico']) . "','" . str_pad($i, 2, "0", STR_PAD_LEFT) . "/" . str_pad($totaldeParcelas, 2, "0", STR_PAD_LEFT) . "'," .
                                    $bol_id_banco . "," .
                                    $bol_data_criacao . "," .
                                    $bol_valor . "," .
                                    $bol_juros . "," .
                                    $bol_multa . "," .
                                    $nossoNumero . ",getdate())") or die(odbc_errormsg());
                    if ($nossoNumero != "null") {
                        $nossoNumero = bcadd($nossoNumero, 1);
                    }
                }
            }
        }
        //-----------------------------------------Geração--de--Solicitação--de--Autorização----------
        if (isset($_POST['txtTransportadora'])) {
            $explode = valoresData('txtDtVenda');
            $valorParcela = valoresNumericos('txtValorFrete');           
            if ($_POST['txtTransportadora'] != "null" && is_numeric($valorParcela) && $explode != "null") {
                $gc = 0;
                $cm = 0;
                $doc = 0;
                $cur = odbc_exec($con, "select id_contas_movimento,id_grupo_contas from sf_contas_movimento cm inner join sf_grupo_contas gc on cm.grupo_conta = gc.id_grupo_contas where cm.descricao = '" . utf8_decode("FRETE") . "' and gc.descricao in ('TRANSPORTE', 'DESPESAS EVENTUAIS')");
                while ($RFP = odbc_fetch_array($cur)) {
                    $cm = $RFP['id_contas_movimento'];
                    $gc = $RFP['id_grupo_contas'];
                }
                $cur = odbc_exec($con, "select id_tipo_documento from dbo.sf_tipo_documento where descricao = 'DINHEIRO'");
                while ($RFP = odbc_fetch_array($cur)) {
                    $doc = $RFP['id_tipo_documento'];
                }
                if ($valorParcela > 0 && $gc > 0 && $cm > 0 && $doc > 0) {
                    $sa = '';
                    $query = "set dateformat dmy;insert into sf_solicitacao_autorizacao(empresa,grupo_conta,conta_movimento,fornecedor_despesa,tipo_documento,historico,destinatario,comentarios,sys_login,data_lanc,status)values('001'," .
                            $gc . "," .
                            $cm . "," .
                            $_POST['txtTransportadora'] . "," .
                            $doc . ",'" .
                            utf8_decode($_POST['txtHistorico']) . "','" .
                            utf8_decode($_POST['txtAutorizacao']) . "','" .
                            utf8_decode($_POST['txtComentarios']) . "','SYSADM',GetDate(),'Aguarda')";
                    odbc_exec($con, $query) or die(odbc_errormsg());
                    $res = odbc_exec($con, "select top 1 id_solicitacao_autorizacao from sf_solicitacao_autorizacao order by id_solicitacao_autorizacao desc") or die(odbc_errormsg());
                    $sa = odbc_result($res, 1);
                    //----------------------------------------------Parcelas-----------------------------------------
                    odbc_exec($con, "set dateformat dmy; insert into sf_solicitacao_autorizacao_parcelas(solicitacao_autorizacao,numero_parcela,data_parcela,sa_descricao,valor_parcela,historico_baixa,pa,bol_id_banco,bol_data_criacao,bol_valor,bol_juros,bol_multa,bol_nosso_numero,data_cadastro) values (" .
                                    $sa . ",1,'" . $_POST['txtDtVenda'] . "',''," . $valorParcela . ",'" . utf8_decode($_POST['txtHistorico']) . "','01/01',null,null,null,null,null,null,getdate())") or die(odbc_errormsg());
                    odbc_exec($con, "INSERT INTO sf_frete_historico(frete_venda,frete_transportadora,frete_solicitacao_autorizacao) VALUES (" . $nn . "," . $_POST['txtTransportadora'] . "," . $sa . ")") or die(odbc_errormsg());
                } else {
                    echo "<script>alert('Verifique os campos necessários para geração de conta do tipo FRETE!');</script>";
                }
            }
        }
        //--------------------------------------------------------------------------------------------
        //header('Localização: FormCompras.php?id='.$nn);
        $FinalUrl = "id=" . $nn;
        echo "<script>window.top.location.href = 'FormCompras.php?id=" . $nn . "';</script>";
    }
    if (substr($_POST['pdfName'], -3) == "pdf" && is_numeric($_POST['txtId'])) {
        if (file_exists($_POST['pdfName'])) {
            copy($_POST['pdfName'], $pasta . "/" . $_POST['txtId'] . ".pdf");
            unlink($_POST['pdfName']);
        }
    }
    if ($_POST['xmlName'] != "" && is_numeric($_POST['txtId'])) {
        if (file_exists($_POST['xmlName'])) {
            copy($_POST['xmlName'], $pasta . "/" . $_POST['txtId'] . ".xml");
            unlink($_POST['xmlName']);
        }
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        if (file_exists($pasta . "/" . $_POST['txtId'] . ".pdf")) {
            unlink($pasta . "/" . $_POST['txtId'] . ".pdf");
        }
        if (file_exists($pasta . "/" . $_POST['txtId'] . ".xml")) {
            unlink($pasta . "/" . $_POST['txtId'] . ".xml");
        }         
        odbc_exec($con, "UPDATE sf_requisicao_cotacao SET id_venda = null WHERE id_venda = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
        odbc_exec($con, "DELETE FROM sf_vendas_campos where id_sf_vendas = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
        odbc_exec($con, "DELETE FROM sf_vendas_itens where id_venda = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
        odbc_exec($con, "DELETE FROM sf_venda_parcelas where venda = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
        odbc_exec($con, "DELETE FROM sf_vendas WHERE id_venda = " . $_POST['txtId']) or die(odbc_errormsg());
        echo "<script>alert('Registro excluido com sucesso');window.top.location.href = 'Compras.php?id=1';</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
    echo "<script>window.top.location.href = 'FormCompras.php';</script>";
}
if (isset($_POST['bntAprov']) || isset($_POST['bntReprov']) || isset($_POST['bntDesfazer'])) {
    if (isset($_POST['bntAprov'])) {
        odbc_exec($con, "insert into sf_produtos_historico_preco(id_produto,data_atual,preco_atual,margem_lucro)
        select conta_produto,GETDATE(),preco_venda,valor_margem_lucro from sf_vendas
        inner join sf_vendas_itens on sf_vendas_itens.id_venda = sf_vendas.id_venda
        inner join sf_produtos on sf_produtos.conta_produto = sf_vendas_itens.produto
        where cov = 'C' and tp_preco = 1 and sf_vendas.id_venda = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
        odbc_exec($con, "update sf_produtos set preco_venda = (valor_total/quantidade) * ((valor_margem_lucro + 100) / 100)
        from sf_vendas inner join sf_vendas_itens on sf_vendas_itens.id_venda = sf_vendas.id_venda
        inner join sf_produtos on sf_produtos.conta_produto = sf_vendas_itens.produto
        where cov = 'C' and tp_preco = 1 and sf_vendas.id_venda = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
        odbc_exec($con, "update sf_vendas set status = 'Aprovado', data_aprov = getdate() where id_venda = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
    } else if (isset($_POST['bntDesfazer'])) {
        odbc_exec($con, "update sf_vendas set status = 'Aguarda', data_aprov = null where id_venda = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
    } else {
        odbc_exec($con, "update sf_vendas set status = 'Reprovado', data_aprov = getdate() where id_venda = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
    }
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select *,sf_vendas.empresa empresax,
                            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato,
                            (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = id_fornecedores_despesas) email_contato
                            from sf_vendas inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = sf_vendas.cliente_venda
                            left join tb_estados e on f.estado = e.estado_codigo left join tb_cidades c on f.cidade = c.cidade_codigo where id_venda = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $dados1 = '';
        $dados2 = '';
        $id = $RFP['id_venda'];
        $cliente_venda = $RFP['cliente_venda'];
        $vendedor = strtoupper(utf8_encode($RFP['vendedor']));
        $data_venda = escreverData($RFP['data_venda']);
        $historico_venda = utf8_encode($RFP['historico_venda']);
        $comentarios_venda = utf8_encode($RFP['comentarios_venda']);
        $sys_login = utf8_encode($RFP['sys_login']);
        $destinatario = utf8_encode($RFP['destinatario']);
        $status = utf8_encode($RFP['status']);
        $tipo = utf8_encode($RFP['tipo_documento']);
        $garantia_prod = utf8_encode($RFP['garantia_venda']);
        $garantia_serv = utf8_encode($RFP['garantia_servico']);
        $descontop = $RFP['descontop'];
        $descontos = $RFP['descontos'];
        $entrega = utf8_encode($RFP['entrega']);
        $validade = utf8_encode($RFP['validade']);
        $descontots = utf8_encode($RFP['descontots']);
        $descontotp = utf8_encode($RFP['descontotp']);
        $empresa = utf8_encode($RFP['empresax']);
        $comanda = utf8_encode($RFP['cod_pedido']);        
        $n_produtos = utf8_encode($RFP['n_produtos']);
        $grupo = utf8_encode($RFP['grupo_conta']);
        $conta = utf8_encode($RFP['conta_movimento']);        
        $TipoGrupo = '';
        if ($RFP['endereco'] != '') {
            $dados1 = utf8_encode($RFP['endereco']);
        }
        if ($RFP['numero'] != '') {
            $dados1 = $dados1 . ' N° ' . utf8_encode($RFP['numero']);
        }
        if ($RFP['complemento'] != '') {
            $dados1 = $dados1 . ' ' . utf8_encode($RFP['complemento']);
        }
        if ($RFP['bairro'] != '') {
            $dados1 = $dados1 . ',' . utf8_encode($RFP['bairro']);
        }
        if ($RFP['cidade_nome'] != '') {
            $dados1 = $dados1 . "\n" . utf8_encode($RFP['cidade_nome']);
        }
        if ($RFP['estado_sigla'] != '') {
            $dados1 = $dados1 . '-' . utf8_encode($RFP['estado_sigla']);
        }
        if ($RFP['cep'] != '') {
            $dados1 = $dados1 . ', Cep: ' . utf8_encode($RFP['cep']);
        }
        if ($RFP['cnpj'] != '') {
            $dados2 = $dados2 . 'CNPJ/CPF:' . utf8_encode($RFP['cnpj']);
        }
        if ($RFP['telefone_contato'] != '') {
            $dados2 = $dados2 . "\n" . 'Tel:' . utf8_encode($RFP['telefone_contato']);
        }
        if ($RFP['email_contato'] != '') {
            $dados2 = $dados2 . ' E-mail:' . utf8_encode($RFP['email_contato']);
        }
    }
    $i = 0;
    $r_quantidade = '1';
    $r_valor_tot = 0;
    $cur = odbc_exec($con, "select sa_descricao,data_parcela,valor_parcela from dbo.sf_venda_parcelas
    where inativo = 0 and venda = " . $PegaURL . " order by id_parcela");
    while ($RFP = odbc_fetch_array($cur)) {
        if ($i == 0) {
            $r_documento = $RFP['sa_descricao'];
            $r_ven_ini = escreverData($RFP['data_parcela']);
        }
        $r_valor = escreverNumero($RFP['valor_parcela']);
        $r_valor_tot = $r_valor_tot + $RFP['valor_parcela'];
        $i++;
    }
    $r_valor_tot = escreverNumero($r_valor_tot);
    if ($i > 0) {
        $r_quantidade = $i;
    }
    $valor_frete = escreverNumero(0);
    $cur = odbc_exec($con, "select * , (select sum(valor_parcela) from sf_solicitacao_autorizacao_parcelas where solicitacao_autorizacao = frete_solicitacao_autorizacao) parcelas, (select top 1 status from sf_solicitacao_autorizacao where id_solicitacao_autorizacao = frete_solicitacao_autorizacao) estado
    from sf_frete_historico where frete_venda = " . $PegaURL . "");
    while ($RFP = odbc_fetch_array($cur)) {
        $transportadora = $RFP['frete_transportadora'];
        $transportadora_estado = $RFP['estado'];
        $transportadora_sa = $RFP['frete_solicitacao_autorizacao'];
        $valor_frete = escreverNumero($RFP['parcelas']);
    }
} else {
    $disabled = '';
    $id = '';
    $cliente_venda = '';
    $vendedor = '';
    $data_venda = getData("T");
    $historico_venda = 'COMPRA';
    $comentarios_venda = '';
    $sys_login = '';
    $dados1 = '';
    $dados2 = '';
    $tipo = '';
    $status = '';
    $garantia_prod = '';
    $garantia_serv = '';
    $descontop = '0';
    $descontos = '0';
    $entrega = '';
    $comanda = (isset($_GET['com']) ? $_GET['com'] : "");
    $validade = '';
    $descontots = '0';
    $descontotp = '0';
    $TipoGrupo = 'C';
    $transportadora = 'null';
    $transportadora_estado = '';
    $transportadora_sa = '';
    $valor_frete = escreverNumero(0);
    $empresa = '';
    $n_produtos = '0';
    $grupo = 15; 
    $conta = 5;
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
if (isset($_POST['bntVoltar'])) {
    if ($_GET['idx'] != '') {
        $FinalUrl = "?id=" . $_GET['idx'];
    }
    if ($_GET['tpx'] != '') {
        $FinalUrl = $FinalUrl . "&tp=" . $_GET['tpx'];
    }
    if ($_GET['tdx'] != '') {
        $FinalUrl = $FinalUrl . "&td=" . $_GET['tdx'];
    }
    if ($_GET['dti'] != '') {
        $FinalUrl = $FinalUrl . "&dti=" . $_GET['dti'];
    }
    if ($_GET['dtf'] != '') {
        $FinalUrl = $FinalUrl . "&dtf=" . $_GET['dtf'];
    }
    if ($TipoGrupo == "D") {
        echo "<script>window.top.location.href = 'Contas-a-Pagar.php" . $FinalUrl . "';</script>";
    } elseif ($TipoGrupo == "C") {
        echo "<script>window.top.location.href = 'Contas-a-Receber.php" . $FinalUrl . "';</script>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Compras</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">
        <link href="../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
        <link href="./../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script src="../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
        <script src="../../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <style>
            .inputDireito {
                text-align:right;
            }
            .fancybox-wrap, .fancybox-skin, .fancybox-outer, .fancybox-inner, .fancybox-image, .fancybox-wrap object, .fancybox-nav, .fancybox-nav span, .fancybox-tmp {
                padding: 0;
                margin: 0;
                border: 0;
                outline: none;
                vertical-align: top;
                background:url(../../img/fundosForms/formBancos.PNG);
                border:solid 1 #cfcfcf;
            }
            #formulario {
                float: left;
                width: 100%;
            }
            .linha {
                float: left;
                width: 100%;
                display: block;
                padding-top: 10px;
                padding-bottom: 10px;
            }            
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header" style="display: inherit;">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1 style="margin-top:8px">Compras</h1>
                        <button onclick="AbrirBox(4)" type="button" style="float: right;width:28px; height:28px; padding:0px;margin-top:18px" class="btn btn-primary"><span class="ico-upload-alt icon-white"></span></button>
                        <?php if (file_exists($pasta . "/" . $id . ".xml")) { ?>
                            <a href="<?php echo $pasta . "/" . $id . ".xml"; ?>" target="_blank">
                                <img style="float: right;width:28px; height:28px; padding:0px;margin-top:18px" src="./../../img/xml.png"/>
                            </a>
                        <?php } if (file_exists($pasta . "/" . $id . ".pdf")) { ?>
                            <a href="<?php echo $pasta . "/" . $id . ".pdf"; ?>" target="_blank">
                                <img style="float: right;width:28px; height:28px; padding:0px;margin-top:18px" src="./../../img/pdf.png"/>
                            </a>
                        <?php } ?>
                    </div>
                    <div class="row-fluid">
                        <form action="FormCompras.php?<?php echo $FinalUrl; ?>" method="POST" name="frmEnviaDados" id="frmEnviaDados">
                            <div class="span12">
                                <div class="block">
                                    <div id="formulario">       
                                        <div>
                                            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                                            <div style="width:9%; float:left;">
                                                <div>Loja:</div>
                                                <div>
                                                    <select name="txtEmpresa" id="txtEmpresa" <?php echo $disabled; ?> class="select" >
                                                        <?php
                                                        $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                                        while ($RFP = odbc_fetch_array($cur)) {
                                                            ?>
                                                            <option value="<?php echo utf8_encode($RFP['id_filial']); ?>" <?php
                                                            if ($empresa == $RFP['id_filial']) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>><?php echo utf8_encode($RFP['numero_filial']); ?></option>
                                                                <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div style="width:39.8%; float:left">
                                                <div>Fornecedor: <span id="carregando3" name="carregando3" style="color:#666; display:none">Aguarde, carregando...</span></div>
                                                <div id="spryselect4" style="width:92.5%; float:left">
                                                    <select name="txtFonecedor" id="txtFonecedor" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                        <option value="">--Selecione--</option>
                                                        <?php
                                                        $where_bloq = '';
                                                        if ($disabled == "disabled") {
                                                            $where_bloq = " and id_fornecedores_despesas  = '" . $cliente_venda . "'";
                                                        }
                                                        $cur = odbc_exec($con, "select id_fornecedores_despesas,razao_social from sf_fornecedores_despesas where tipo = 'F' " . $where_bloq . " order by razao_social") or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) {
                                                            ?>
                                                            <option value="<?php echo $RFP['id_fornecedores_despesas'] ?>"<?php
                                                            if (!(strcmp($RFP['id_fornecedores_despesas'], $cliente_venda))) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>><?php echo utf8_encode($RFP['razao_social']) ?></option>
                                                                <?php } ?>
                                                    </select>
                                                    <div class="selectRequiredMsg">Selecione um item.</div>
                                                </div>
                                                <div style="width:7.5%; float:left"><button type="button" class="btn btn-primary" style="width:100%; height:28px; padding:0px" onClick="AbrirBox(3)" <?php
                                                    if (($disabled == "disabled") || ($ckb_adm_cli_ == 0)) {
                                                        echo "disabled";
                                                    }
                                                    ?>><span class="icon-plus-sign icon-white"></span></button></div>
                                            </div>
                                            <div style="width:36%; float:left; margin-left:1%; padding-left:1%; border-left:dashed 1px #CCC">
                                                <div>Responsável pela Compra:</div>
                                                <div>
                                                    <select name="txtDestinatario" id="txtDestinatario" class="select" style="width:100%" <?php echo $disabled; ?>>
                                                        <option>--Selecione--</option>
                                                        <?php
                                                        $where_bloq = '';
                                                        if ($disabled == "disabled") {
                                                            $where_bloq = " and UPPER(login_user)  = '" . $vendedor . "'";
                                                        }
                                                        $cur = odbc_exec($con, "select UPPER(login_user) login_user,nome from sf_usuarios where inativo = 0 " . $where_bloq . " order by nome") or die(odbc_errormsg());
                                                        $query = "select UPPER(login_user) login_user,nome from sf_usuarios where inativo = 0 " . $where_bloq . " order by nome";
                                                        echo $query;
                                                        while ($RFP = odbc_fetch_array($cur)) {
                                                            echo $RFP['login_user'];
                                                            echo strtoupper($vendedor);
                                                            ?>
                                                            <option value="<?php echo $RFP['login_user'] ?>"<?php
                                                            if ($id == '') {
                                                                if (!(strcmp($RFP['login_user'], strtoupper($_SESSION["login_usuario"])))) {
                                                                    echo "SELECTED";
                                                                }
                                                            } else {
                                                                if (!(strcmp($RFP['login_user'], strtoupper($vendedor)))) {
                                                                    echo "SELECTED";
                                                                }
                                                            }
                                                            ?>><?php echo utf8_encode($RFP['nome']) ?></option>
                                                                <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div style="width:10.6%; float:left; margin-left:1%; padding-left:1%; border-left:dashed 1px #CCC;">
                                                <div>Data:</div>
                                                <div><input type="text" name="txtDtVenda" id="txtDtVenda" class="datepicker inputCenter" value="<?php echo $data_venda; ?>" <?php echo $disabled; ?>></div>
                                            </div>
                                        </div>
                                        <div style="clear:both"></div>
                                        <div>
                                            <div style="width:48.9%; float:left; margin-top:5px">
                                                <div>Dados do Cliente: <span id="carregando4" name="carregando4" style="color:#666;display:none">Aguarde, carregando...</span></div>
                                                <div><textarea disabled="true" name="txtDados" id="txtDados" style="width:100%" rows="10"><?php echo $dados2; ?></textarea></div>
                                            </div>
                                            <div style="width:48.9%; float:left; margin-left:1%; padding-left:1%; margin-top:5px; border-left:dashed 1px #CCC">
                                                <div>Endereço de Entrega: <span id="carregando5" name="carregando5" style="color:#666;display:none">Aguarde, carregando...</span></div>
                                                <div><textarea disabled="true" name="txtEntrega" id="txtEntrega" style="width:100%" rows="10"><?php echo $dados1; ?></textarea></div>
                                            </div>
                                        </div>
                                        <span style="width:100%; display:block; float:left; margin-top:4px;">       
                                            <div style="margin-top: 5px;" class="head dblue">
                                                <h2><?php echo 'Produtos'; ?></h2>
                                                <ul class="buttons"><li><div><a href="javascript:void(0)" onclick="AbrirBox(1)" title="Adicionar Produto"><div class="icon"><span class="ico-plus"></span></div></a></div></li></ul>
                                            </div>
                                            <table class="table" cellpadding="0" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th width="18%">Grupos de Produtos:</th>
                                                        <th width="42%">Produtos:</th>
                                                        <th width="5%">Und:</th>
                                                        <th width="5%">Qtd:</th>
                                                        <th width="12.5%">Valor Unitário:</th>
                                                        <th width="12.5%">Valor Total:</th>
                                                        <th width="5%">Ação:</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <select  name="txtGrupo" class="select" style="width: 100%;" id="txtGrupo" <?php echo $disabled; ?> >
                                                                <option value="null">--Selecione--</option>
                                                                <?php
                                                                if ($disabled == "") {
                                                                    $cur = odbc_exec($con, "select id_contas_movimento,descricao from sf_contas_movimento where tipo = 'P' order by descricao") or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                                        ?>
                                                                        <option value="<?php echo $RFP['id_contas_movimento'] ?>"><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <span id="carregando" name="carregando" style="color:#666;display:none">Aguarde, carregando...</span>
                                                            <select name="txtConta" class="select" style="width:100%" id="txtConta"  <?php echo $disabled; ?> >
                                                                <option value="null">--Selecione--</option>
                                                                <?php
                                                                if ($disabled == "") {
                                                                    $sql = "select conta_produto,descricao,codigo_interno, REPLICATE('0',14-LEN(codigo_barra)) + codigo_barra codigo_barra from sf_produtos where inativa = 0 and conta_produto > 0 AND tipo = 'P' ORDER BY conta_produto";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                                        ?>
                                                                        <option value="<?php echo $RFP['conta_produto'] ?>"> <?php
                                                                            if ($CheckCodigoProduto == '1') {
                                                                                echo $RFP['codigo_barra'] . " - ";
                                                                            }
                                                                            ?> <?php
                                                                            echo utf8_encode($RFP['descricao']);
                                                                            if (strlen($RFP['codigo_interno']) > 0) {
                                                                                echo " [" . $RFP['codigo_interno'] . "]";
                                                                            }
                                                                            ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input name="txtUnidade" id="txtUnidade" type="text" class="input-xlarge inputCenter" value="" disabled/>
                                                        </td>
                                                        <td>
                                                            <input name="txtQtdProduto" id="txtQtdProduto" <?php echo $disabled; ?> type="text" class="input-xlarge inputCenter" value="1" onBlur="prodValor()"/>
                                                        </td>
                                                        <td>
                                                            <input name="hidValorProduto" id="hidValorProduto" <?php echo $disabled; ?> type="text" class="input-xlarge inputCenter" value="<?php echo escreverNumero(0); ?>" onBlur="prodValor()"/>
                                                        </td>
                                                        <td>
                                                            <input name="txtValorProduto" id="txtValorProduto" type="text" class="input-xlarge inputCenter" value="<?php echo escreverNumero(0); ?>" disabled />
                                                        </td>
                                                        <td>
                                                <center><button class="btn btn-primary" id="for-block1" <?php echo $disabled; ?> type="button" onClick="adicionaItem('P')">+</button></center>
                                                </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <table class="table" style="margin-top: -7px;" cellpadding="0" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th width="18%"></th>
                                                        <th width="5%"></th>
                                                        <th width="37%"></th>
                                                        <th width="5%"></th>
                                                        <th width="5%"></th>
                                                        <th width="12.5%"></th>
                                                        <th width="12.5%"></th>
                                                        <th width="5%"></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tabela_produto">
                                                    <?php
                                                    $i = 0;
                                                    $totalp = 0;
                                                    $idTP = 0;
                                                    if ($PegaURL != "") {
                                                        $cur = odbc_exec($con, "select id_item_venda,conta_movimento grupo,gc.descricao grupodesc,conta_produto produto,cm.descricao produtodesc,
                                                                                quantidade,valor_total,unidade_comercial,codigo_interno from sf_vendas_itens vi
                                                                                left join sf_contas_movimento gc on vi.grupo = gc.id_contas_movimento inner join sf_produtos cm
                                                                                on cm.conta_produto = vi.produto where cm.tipo = 'P' and id_venda = " . $PegaURL);
                                                        while ($RFP = odbc_fetch_array($cur)) {
                                                            ?>
                                                            <tr id='tabela_linha_<?php echo $i; ?>'>
                                                                <td><input name='txtIP_<?php echo $i; ?>' id='txtIP_<?php echo $i; ?>' type='hidden' value='<?php echo $RFP['id_item_venda']; ?>'/>
                                                                    <input name='txtGR_<?php echo $i; ?>' id='txtGR_<?php echo $i; ?>' type='hidden' value='<?php echo $RFP['grupo']; ?>'/>
                                                                    <?php echo utf8_encode($RFP['grupodesc']); ?>
                                                                </td>
                                                                <td>
                                                        <center><?php echo utf8_encode($RFP['codigo_interno']); ?></center>
                                                        </td>
                                                        <?php if ($disabled !== "") { ?>
                                                            <td>
                                                                <input name='txtPD_<?php echo $i; ?>' id='txtPD_<?php echo $i; ?>' type='hidden' value='<?php echo $RFP['produto']; ?>'/>
                                                                <?php echo utf8_encode($RFP['produtodesc']); ?>
                                                            </td>
                                                        <?php } else { ?>
                                                            <td>
                                                                <input name='txtPD_<?php echo $i; ?>' id='txtPD_<?php echo $i; ?>' type='hidden' value='<?php echo $RFP['produto']; ?>'/>
                                                                <a style='cursor: pointer;' onclick="carregaItemP(<?php echo $i; ?>)"><?php echo utf8_encode($RFP['produtodesc']); ?></a>
                                                            </td>
                                                        <?php } ?>
                                                        <td>
                                                        <input name='txtUn_<?php echo $i; ?>' id='txtUn_<?php echo $i; ?>' type='hidden' value='<?php echo $RFP['unidade_comercial']; ?>'/><center><?php echo $RFP['unidade_comercial']; ?></center>
                                                        </td>
                                                        <td style='text-align: right;' id='myVQ_<?php echo $i; ?>'>
                                                            <input name='txtVQ_<?php echo $i; ?>' id='txtVQ_<?php echo $i; ?>' type='hidden' value='<?php echo escreverNumero($RFP['quantidade'], 0, 4); ?>'/><?php echo escreverNumero($RFP['quantidade'], 0, 4); ?>
                                                        </td>
                                                        <td style='text-align: right;'>
                                                            <input name='txtVU_<?php echo $i; ?>' id='txtVU_<?php echo $i; ?>' type='hidden' value='<?php
                                                            if ($RFP['quantidade'] > 0) {
                                                                $valor_unitario = $RFP['valor_total'] / $RFP['quantidade'];
                                                            } else {
                                                                $valor_unitario = 0;
                                                            }
                                                            echo escreverNumero($valor_unitario);
                                                            ?>'/><?php echo escreverNumero($valor_unitario); ?>
                                                        </td>
                                                        <td id='myVP_<?php echo $i; ?>' style='text-align: right;'>
                                                            <input name='txtVP_<?php echo $i; ?>' id='txtVP_<?php echo $i; ?>' type='hidden' value='<?php
                                                            $totalp = $totalp + $RFP['valor_total'];
                                                            echo escreverNumero($RFP['valor_total']);
                                                            ?>'/><?php echo escreverNumero($RFP['valor_total']); ?>
                                                        </td>
                                                        <td><center><input <?php echo $disabled; ?> class='btn red' type='button' value='x' onClick="removeLinha('tabela_linha_<?php echo $i; ?>')" style="margin:0px; padding:2px 4px 3px 4px; line-height:10px;"></input></center></td>
                                                        </tr>
                                                        <?php
                                                        $i++;
                                                    }
                                                    $idTP = $i;
                                                }?>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="height: 20px;">
                                                        <td colspan="8" style="width:100%; background-color:#f2f2f2; margin:0; padding:8px;">
                                                            <div id="txtTotalProd" style="width:200px; float:right; text-align:right; line-height:20px">
                                                                Total sem Desconto: <?php echo escreverNumero($totalp, 1); ?><br>
                                                                Valor do Desconto: 
                                                                <?php
                                                                if ($descontotp == 1) {
                                                                    $valDesc = $descontop;
                                                                } else {
                                                                    $valDesc = $totalp * ($descontop / 100);
                                                                }
                                                                echo escreverNumero(($valDesc), 1);
                                                                ?><br>
                                                                <b>Total com Desconto: <?php
                                                                    $TotalProduto = $TotalProduto + ($totalp - $valDesc);
                                                                    echo escreverNumero(($totalp - $valDesc), 1);
                                                                    ?></b>
                                                            </div>
                                                            <div style="float:right;margin-top: 12px;">
                                                                <span style="width:100%; display:block;"></span>
                                                                <span style="width:100%; display:block;">
                                                                    <button class="btn btn-primary" id="bntProdDesconto" <?php echo $disabled; ?> type="button" style="width:35px; padding-bottom:3px; text-align:center"><span id="prod_sinal">%</span></button>
                                                                    <button onclick="aplicaDesconto('P')" class="btn green" type="button" <?php echo $disabled; ?> style="width:35px;margin-left: -3px;height: 27px;"><span class="ico-arrow-right icon-white"></span></button>
                                                                </span>
                                                            </div>
                                                            <div style="width:300px; float:left;vertical-align: bottom;">
                                                                <input id="ckbNValores_Produtos" name="ckbNValores_Produtos" type="checkbox" <?php echo $disabled; ?> onClick="somaTotal()" value="1" <?php
                                                                if ($n_produtos == 1) {
                                                                    echo "CHECKED";
                                                                }
                                                                ?>>
                                                                Não contabilizar valores de Produto
                                                            </div>
                                                            <div style="width:100px; float:right">
                                                                <span style="width:100%; display:block;"> Desconto: </span>
                                                                <span style="width:100%; display:block;">
                                                                    <input id="txtProdDescontoVal" <?php echo $disabled; ?> type="text" class="input-xlarge" value="<?php echo escreverNumero($descontop); ?>"/>
                                                                    <input name="txtProdSinal" id="txtProdSinal" value="<?php echo $descontotp; ?>" type="hidden"/>
                                                                    <input name="txtProdDesconto" id="txtProdDesconto" value="<?php echo escreverNumero($descontop); ?>" type="hidden" />
                                                                </span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </span>
                                        <span style="width:100%; display:block; float:left; margin-top:4px;">
                                            <div style="margin-top: 10px;" class="head dblue">
                                                <h2><?php echo 'Serviços'; ?></h2>
                                                <ul class="buttons"><li><div><a href="javascript:void(0)" onclick="AbrirBox(2)" title="Adicionar Produto"><div class="icon"><span class="ico-plus"></span></div></a></div></li></ul>
                                            </div>
                                            <table class="table" cellpadding="0" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th width="20%">Grupos de Serviços:</th>
                                                        <th width="45%">Serviços:</th>
                                                        <th width="5%">Qtd:</th>
                                                        <th width="12.5%">Valor Unitário:</th>
                                                        <th width="12.5%">Valor Total:</th>
                                                        <th width="5%">Ação:</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <select name="txtGrupoS" class="select" style="width: 100%;" id="txtGrupoS" <?php echo $disabled; ?> >
                                                                <option value="null">--Selecione--</option>
                                                                <?php
                                                                if ($disabled == "") {
                                                                    $cur = odbc_exec($con, "select id_contas_movimento,descricao from sf_contas_movimento where tipo = 'S' order by descricao") or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                                        ?>
                                                                        <option value="<?php echo $RFP['id_contas_movimento'] ?>"><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <span id="carregandoS" name="carregandoS" style="color:#666;display:none">Aguarde, carregando...</span>
                                                            <select name="txtContaS" class="select" style="width:100%" id="txtContaS"  <?php echo $disabled; ?> >
                                                                <option value="null">--Selecione--</option>
                                                                <?php
                                                                if ($disabled == "") {
                                                                    $sql = "select conta_produto,descricao from sf_produtos where inativa = 0 and conta_produto > 0 AND tipo = 'S' ORDER BY conta_produto";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                                        ?>
                                                                        <option value="<?php echo $RFP['conta_produto'] ?>"><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input name="txtQtdProdutoS" id="txtQtdProdutoS" <?php echo $disabled; ?> type="text" class="input-xlarge inputCenter" value="1" onBlur="servValor()"/>
                                                        </td>
                                                        <td>
                                                            <input name="hidValorProdutoS" id="hidValorProdutoS" <?php echo $disabled; ?> type="text" class="input-xlarge inputCenter" value="<?php echo escreverNumero(0); ?>" onBlur="servValor()"/>
                                                        </td>
                                                        <td>
                                                            <input name="txtValorProdutoS" id="txtValorProdutoS" type="text" class="input-xlarge inputCenter" value="<?php echo escreverNumero(0); ?>" disabled />
                                                        </td>
                                                        <td>
                                                <center><button class="btn btn-primary" id="for-block2" <?php echo $disabled; ?> type="button" onClick="adicionaItem('S')">+</button></center>
                                                </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <table class="table" style="margin-top: -7px;" cellpadding="0" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th width="20%"></th>
                                                        <th width="45%"></th>
                                                        <th width="5%"></th>
                                                        <th width="12.5%"></th>
                                                        <th width="12.5%"></th>
                                                        <th width="5%"></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tabela_produtoS">
                                                    <?php
                                                    $j = 0;
                                                    $totalp = 0;
                                                    $idTS = 0;
                                                    if ($PegaURL != "") {
                                                        $cur = odbc_exec($con, "select id_item_venda,conta_movimento grupo,gc.descricao grupodesc,conta_produto produto,cm.descricao produtodesc,
                                                                                quantidade,valor_total from sf_vendas_itens vi
                                                                                left join sf_contas_movimento gc on vi.grupo = gc.id_contas_movimento inner join sf_produtos cm
                                                                                on cm.conta_produto = vi.produto where cm.tipo = 'S' and id_venda = " . $PegaURL);
                                                        while ($RFP = odbc_fetch_array($cur)) {
                                                            ?>
                                                            <tr id='tabela_linhaS_<?php echo $j; ?>'>
                                                                <td><input name='txtIPS_<?php echo $j; ?>' id='txtIPS_<?php echo $j; ?>' type='hidden' value='<?php echo $RFP['id_item_venda']; ?>'/>
                                                                    <input name='txtGRS_<?php echo $j; ?>' id='txtGRS_<?php echo $j; ?>' type='hidden' value='<?php echo $RFP['grupo']; ?>'/>
                                                                    <?php echo utf8_encode($RFP['grupodesc']); ?>
                                                                </td>
                                                                <?php if ($disabled !== "") { ?>
                                                                    <td>
                                                                        <input name='txtPDS_<?php echo $j; ?>' id='txtPDS_<?php echo $j; ?>' type='hidden' value='<?php echo $RFP['produto']; ?>'/>
                                                                        <?php echo utf8_encode($RFP['produtodesc']); ?>
                                                                    </td>
                                                                <?php } else { ?>
                                                                    <td>
                                                                        <input name='txtPDS_<?php echo $j; ?>' id='txtPDS_<?php echo $j; ?>' type='hidden' value='<?php echo $RFP['produto']; ?>'/>
                                                                        <a style='cursor: pointer;' onclick="carregaItemS(<?php echo $j; ?>)"><?php echo utf8_encode($RFP['produtodesc']); ?></a>
                                                                    </td>
                                                                <?php } ?>
                                                                <td id='myVQS_<?php echo $j; ?>'>
                                                        <input name='txtVQS_<?php echo $j; ?>' id='txtVQS_<?php echo $j; ?>' type='hidden' value='<?php echo escreverNumero($RFP['quantidade'], 0, 4); ?>'/><center><?php echo escreverNumero($RFP['quantidade'], 0, 4); ?></center>
                                                        </td>
                                                        <td style='text-align: right;'>
                                                            <input name='txtVUS_<?php echo $j; ?>' id='txtVUS_<?php echo $j; ?>' type='hidden' value='<?php
                                                            if ($RFP['quantidade'] > 0) {
                                                                $valor_unitario = $RFP['valor_total'] / $RFP['quantidade'];
                                                            } else {
                                                                $valor_unitario = 0;
                                                            }
                                                            echo escreverNumero($valor_unitario);
                                                            ?>'/><?php echo escreverNumero($valor_unitario); ?>
                                                        </td>
                                                        <td id='myVPS_<?php echo $j; ?>' style='text-align: right;'>
                                                            <input name='txtVPS_<?php echo $j; ?>' id='txtVPS_<?php echo $j; ?>' type='hidden' value='<?php
                                                            $totalp = $totalp + $RFP['valor_total'];
                                                            echo escreverNumero($RFP['valor_total']);
                                                            ?>'/><?php echo escreverNumero($RFP['valor_total']); ?>
                                                        </td>
                                                        <td><center><input <?php echo $disabled; ?> class='btn red' type='button' value='x' onClick="javascript:removeLinha('tabela_linhaS_<?php echo $j; ?>')" style="margin:0px; padding:2px 4px 3px 4px; line-height:10px;"></input></center></td>
                                                        </tr>
                                                        <?php
                                                        $j++;
                                                    }
                                                    $idTS = $j;
                                                }
                                                ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="height: 20px;">
                                                        <td colspan="6" style="width:100%; background-color:#f2f2f2; margin:0; padding:8px;">
                                                            <div id="txtTotalServ" style="width:200px; float:right; text-align:right; line-height:20px">
                                                                Total sem Desconto: <?php echo escreverNumero($totalp, 1); ?><br>
                                                                Valor do Desconto: <?php
                                                                if ($descontotp == 1) {
                                                                    $valDesc = $descontos;
                                                                } else {
                                                                    $valDesc = $totalp * ($descontos / 100);
                                                                }
                                                                echo escreverNumero(($valDesc), 1);
                                                                ?><br>
                                                                <b>Total com Desconto: <?php
                                                                    $TotalServico = $TotalServico + ($totalp - $valDesc);
                                                                    echo escreverNumero(($totalp - $valDesc), 1);
                                                                    ?></b>
                                                            </div>
                                                            <div style="float:right;margin-top: 12px;">
                                                                <span style="width:100%; display:block;"></span>
                                                                <span style="width:100%; display:block;">
                                                                    <button class="btn btn-primary" id="bntServDesconto" <?php echo $disabled; ?> type="button" style="width:35px; padding-bottom:3px; text-align:center"><span id="serv_sinal">%</span></button>
                                                                    <button onclick="aplicaDesconto('S')" class="btn green" type="button" <?php echo $disabled; ?> style="width:35px;margin-left: -3px;height: 27px;"><span class="ico-arrow-right icon-white"></span></button>
                                                                </span>
                                                            </div>
                                                            <div style="width:100px; float:right">
                                                                <span style="width:100%; display:block;"> Desconto: </span>
                                                                <span style="width:100%; display:block;">
                                                                    <input id="txtServDescontoVal" <?php echo $disabled; ?> type="text" class="input-xlarge" value="<?php echo escreverNumero($descontos); ?>"/>
                                                                    <input name="txtServSinal" id="txtServSinal" value="<?php echo $descontots; ?>" type="hidden"/>
                                                                    <input name="txtServDesconto" id="txtServDesconto" value="<?php echo escreverNumero($descontos); ?>" type="hidden" />
                                                                </span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>        
                                            <div style="margin-top: 10px;" class="head dblue">
                                                <h2><?php echo 'Total Geral'; ?></h2>
                                                <?php
                                                $TotalGeral += $TotalServico;
                                                if ($n_produtos == 0) {
                                                    $TotalGeral += $TotalProduto;
                                                }
                                                ?>
                                                <ul class="buttons"><h2><div id="totGeral"><?php echo escreverNumero($TotalGeral, 1); ?></div></h2></ul>
                                            </div>
                                        </span>
                                        <span class="linha" style="background:none; border:0px; margin-bottom:5px;">
                                            <span style="width:35%; display:block; float:left;">
                                                <span style="width:100%; display:block;"> Histórico: </span>
                                                <span style="width:100%; display:block;">
                                                    <span id="sprytextfield1">
                                                        <input name="txtHistorico" id="txtHistorico" <?php echo $disabled; ?> type="text" class="input-xlarge" value="<?php echo $historico_venda; ?>"/>
                                                        <span class="textfieldRequiredMsg">Um valor é necessário. </span>
                                                    </span>
                                                </span>
                                            </span>
                                            <span style="width:15%; display:block; float:left; margin-left:1%;">
                                                <span style="width:100%; display:block;"> Previsão de Entrega: </span>
                                                <span style="width:100%; display:block;">
                                                    <input name="txtDtEntrega" id="txtDtEntrega" <?php echo $disabled; ?> type="text" class="datepicker input-xlarge" value="<?php echo $entrega; ?>"/>
                                                </span>
                                            </span>
                                            <span style="width:15%; display:block; float:left; margin-left:1%;">
                                                <span style="width:100%; display:block;"> Validade da Proposta: </span>
                                                <span style="width:100%; display:block;">
                                                    <input name="txtDtProposta" id="txtDtProposta" <?php echo $disabled; ?> type="text" class="datepicker input-xlarge" value="<?php echo $validade; ?>"/>
                                                </span>
                                            </span>
                                            <span style="width:21%; display:block; float:left; margin-left:1%;">
                                                <span style="width:100%; display:block;"> Banco: </span>
                                                <span style="width:100%; display:block;">
                                                    <span id="carregando4" name="carregando4" style="color:#666;display:none">Aguarde, carregando...</span>
                                                    <select name="txtBanco" class="select" style="width: 100%;<?php
                                                    if ($PegaURL != '') {
                                                        echo "display:none";
                                                    }
                                                    ?>" id="txtBanco" disabled>
                                                        <option value="null">--Selecione--</option>
                                                        <?php
                                                        if ($TipoGrupo == "C" && $disabled == "") {
                                                            $cur = odbc_exec($con, "select id_bancos,razao_social from sf_bancos where ativo = 0 order by razao_social") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) {
                                                                ?>
                                                                <option value="<?php echo $RFP['id_bancos'] ?>"><?php echo utf8_encode($RFP['razao_social']) ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </span>
                                            </span>
                                            <span style="width:10%; display:block; float:left; margin-left:1%;">
                                                <span style="width:100%; display:block;"></span>
                                                <span style="width:100%; display:block; text-align:right;margin-top: 14px;">
                                                    <button class="btn btn-primary" id="for-block3" style="width:100%;" type="button" onClick="clearT1();">Parcelas</button>
                                                </span>
                                            </span>
                                        </span>
                                        <span style="display:none; background:none; border:0px; margin-bottom:5px;" id="block3" class="linha">
                                            <span style="width:20%; display:block; float:left; margin-right:1%">
                                                <span style="width:100%; display:block; ">Forma de Pagto: <br>
                                                    <span id="spryselect5">
                                                        <select class="select" style="width: 100%;" name="txtTipo" id="txtTipo" <?php echo $disabled; ?>>
                                                            <option value="null" >--Selecione--</option>
                                                            <?php
                                                            $where_bloq = '';
                                                            $cur = odbc_exec($con, "select id_tipo_documento,descricao from sf_tipo_documento where (inativo = 0 or id_tipo_documento = '" . $tipo . "') and s_tipo in ('A','D') " . $where_bloq . " order by descricao") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) {
                                                                ?>
                                                                <option value="<?php echo $RFP['id_tipo_documento'] ?>"<?php
                                                                if (!(strcmp($RFP['id_tipo_documento'], $tipo))) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                                    <?php } ?>
                                                        </select>
                                                        <span class="selectRequiredMsg">Selecione um item.</span></span>
                                                </span>
                                            </span>
                                            <span style="width:17%; display:block; float:left;">
                                                <span style="width:100%; display:block;"> Documento Inicial: </span>
                                                <span style="width:100%; display:block;">
                                                    <input class="input-xlarge inputCenter" name="txtParcelaMakeDC" id="txtParcelaMakeDC" <?php echo $disabled; ?> type="text" value="<?php echo $r_documento; ?>"/>
                                                </span>
                                            </span>
                                            <span style="width:10%; display:block; float:left; margin-left:1%;">
                                                <span style="width:100%; display:block;"> Vencimento Inicial: </span>
                                                <span style="width:100%; display:block;">
                                                    <input class="datepicker input-xlarge inputCenter" name="txtParcelaMakeD" id="txtParcelaMakeD" <?php echo $disabled; ?> type="text" value="<?php echo $r_ven_ini; ?>"/>
                                                    <span class="textfieldInvalidFormatMsg">Formato inválido.</span>
                                                </span>
                                            </span>
                                            <span style="width:10%; display:block; float:left; margin-left:1%;">
                                                <span style="width:100%; display:block;"> Qtd. de Parcelas: </span>
                                                <span style="width:100%; display:block;">
                                                    <input name="txtParcelaMakeQ" onblur="calculaParcela()" type="text" class="input-xlarge" id="spinner" value="<?php echo $r_quantidade; ?>" maxlength="2" <?php echo $disabled; ?>/>
                                                </span>
                                            </span>
                                            <span style="width:15%; display:block; float:left; margin-left:1%;">
                                                <span style="width:100%; display:block;"> Valor Total: </span>
                                                <span style="width:100%; display:block;">
                                                    <input name="txtParcelaMakeT" onblur="checaValor();calculaParcela();" id="txtParcelaMakeT" <?php echo $disabled; ?> type="text" class="input-xlarge inputCenter" value="<?php echo $r_valor_tot; ?>"/>
                                                </span>
                                            </span>
                                            <span style="width:15%; display:block; float:left; margin-left:1%;">
                                                <span style="width:100%; display:block;"> Valor da Parcela: </span>
                                                <span style="width:100%; display:block;">
                                                    <input name="txtParcelaMakeV" id="txtParcelaMakeV" <?php echo $disabled; ?> type="text" class="input-xlarge inputCenter" value="<?php echo $r_valor; ?>"/>
                                                </span>
                                            </span>
                                            <span style="width:7%; display:block; float:left; margin-left:1%;">
                                                <span style="width:100%; display:block;"></span>
                                                <span style="width:100%; display:block; text-align:right; margin-top: 14px;">
                                                    <button class="btn btn-success" <?php echo $disabled; ?> name="bntMkParcelas" id="bntMkParcelas" type="button" onClick="Preencher()" style="width:100%;">Gerar</button>
                                                </span>
                                            </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span5" style="width:48%;">
                                    <div class="block">
                                        <div class="head dblue">
                                            <div class="icon"><span class="ico-money"></span></div>
                                            <h2><?php echo 'Documentos'; ?></h2>
                                        </div>
                                        <div class="data-fluid">
                                            <table id="tblTable" class="table" cellpadding="0" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th width="29%">Campo:</th>
                                                        <th width="71%">Conteúdo:</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if ($tipo != '') {
                                                        $sql = "select id_tipo_documento_campos,campo,isnull((select conteudo from sf_vendas_campos where id_sf_vendas = '" . $id . "' and tipo_documento_campos = id_tipo_documento_campos),'') 'conteudo' from sf_tipo_documento_campos where inativo = 0 AND tipo_documento = " . $tipo . " ORDER BY 1";
                                                        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) {
                                                            ?>
                                                            <tr>
                                                                <td><?php echo utf8_encode($RFP['campo']); ?></td>
                                                                <td><input name="<?php echo 'txtCampo' . $RFP['id_tipo_documento_campos']; ?>" id="<?php echo 'txtCampo' . $RFP['id_tipo_documento_campos']; ?>" <?php echo $disabled; ?> type="text" class="input-xlarge" value="<?php echo $RFP['conteudo']; ?>"/></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="block">
                                        <div class="head dblue">
                                            <div class="icon"><span class="ico-money"></span></div>
                                            <h2><?php echo 'Garantia'; ?></h2>
                                        </div>
                                        <div class="data-fluid">
                                            <span style="width:100%;">
                                                <span style="width:100%; display:block; margin-top:5px"> Garantia do Produto: </span>
                                                <span style="width:100%; display:block;">
                                                    <input name="txtGarantiaProd" id="txtGarantiaProd" <?php echo $disabled; ?> type="text" class="input-xlarge" value="<?php echo $garantia_prod; ?>"/>
                                                </span>
                                            </span>
                                            <span style="width:100%;">
                                                <span style="width:100%; display:block; margin-top:5px"> Garantia do Serviço: </span>
                                                <span style="width:100%; display:block;">
                                                    <input name="txtGarantiaServ" id="txtGarantiaServ" <?php echo $disabled; ?> type="text" class="input-xlarge" value="<?php echo $garantia_serv; ?>"/>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="block">
                                        <div class="head dblue">
                                            <div class="icon"><span class="ico-money"></span></div>
                                            <h2><?php echo 'Grupo de Contas'; ?></h2>
                                        </div>
                                        <div class="data-fluid">
                                            <span style="width:50%;float: left;">
                                                <span style="width:100%; display:block; float:left"> Grupo: </span>
                                                <span style="width:100%; display:block; float:left">
                                                    <select class="select" style="width: 100%;" name="txtGrupoConta" id="txtGrupoConta" <?php echo $disabled; ?>>
                                                        <option value="null">--Selecione--</option>
                                                        <?php 
                                                            $cur = odbc_exec($con, "select id_grupo_contas,descricao, (select descricao from sf_centro_custo where id = centro_custo) cc
                                                            from sf_grupo_contas where deb_cre = 'D' and inativo = 0 order by descricao") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                            <option value="<?php echo $RFP["id_grupo_contas"] ?>"<?php
                                                            if (!(strcmp($RFP["id_grupo_contas"], $grupo))) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>><?php echo utf8_encode($RFP["descricao"]) . (strlen(utf8_encode($RFP["cc"])) > 0 ? " [" . utf8_encode($RFP["cc"]) . "]" : ""); ?></option>
                                                                <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </span>
                                            </span>                                            
                                            <span style="width:49%;float: left; margin-left: 1%;">
                                                <span style="width:100%; display:block; float:left"> Conta: </span>
                                                <span style="width:100%; display:block; float:left">
                                                    <select class="select" style="width: 100%;" name="txtContaConta" id="txtContaConta" <?php echo $disabled; ?>>
                                                        <option value="null">--Selecione--</option>
                                                        <?php
                                                        if (is_numeric($grupo)) {
                                                            $sql = "select id_contas_movimento,descricao from sf_contas_movimento where grupo_conta = " . $grupo . " and inativa = 0 ORDER BY descricao";
                                                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                            <option value="<?php echo $RFP["id_contas_movimento"] ?>"<?php
                                                            if (!(strcmp($RFP["id_contas_movimento"], $conta))) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                                                    <?php
                                                            }
                                                        } ?>
                                                    </select>
                                                </span>
                                            </span>
                                        </div>                                        
                                    </div>                                        
                                    <div class="block">
                                        <div class="head green">
                                            <div class="icon"><span class="ico-money"></span></div>
                                            <h2><?php echo 'Frete a Pagar'; ?>
                                                <?php if (is_numeric($transportadora_sa)) { ?>
                                                    <a title="<?php echo $transportadora_estado; ?>" href='./../Contas-a-pagar/FormSolicitacao-de-Autorizacao.php?id=<?php echo $transportadora_sa; ?>' ><img src="../../img/<?php
                                                        if ($transportadora_estado == 'Reprovado') {
                                                            echo "inativo";
                                                        } else {
                                                            if ($transportadora_estado == 'Aprovado') {
                                                                echo "pago";
                                                            } elseif ($transportadora_estado == 'Aguarda') {
                                                                echo "apagar";
                                                            } else {
                                                                echo "back";
                                                            }
                                                        }
                                                        ?>.PNG" value=''>
                                                    </a><?php } ?>
                                            </h2>
                                        </div>
                                        <div class="data-fluid">
                                            <span style="width:60%;float: left;">
                                                <span style="width:100%; display:block; float:left"> Transportadora: </span>
                                                <span style="width:100%; display:block; float:left">
                                                    <select style="width: 100%;" name="txtTransportadora" id="txtTransportadora" <?php
                                                    if (is_numeric($transportadora_sa)) {
                                                        echo "disabled";
                                                    } else {
                                                        echo $disabled;
                                                    }
                                                    ?>>
                                                        <option value="null">--Sem Frete--</option>
                                                        <?php
                                                        $where_bloq = '';
                                                        if ($disabled == "disabled") {
                                                            $where_bloq = " and id_fornecedores_despesas  = '" . $transportadora . "'";
                                                        }
                                                        $cur = odbc_exec($con, "select id_fornecedores_despesas,razao_social from sf_fornecedores_despesas where tipo = 'T' " . $where_bloq . " order by razao_social") or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) {
                                                            ?>
                                                            <option value="<?php echo $RFP['id_fornecedores_despesas'] ?>"<?php
                                                            if (!(strcmp($RFP['id_fornecedores_despesas'], $transportadora))) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>><?php echo utf8_encode($RFP['razao_social']) ?></option><?php } ?>
                                                    </select>
                                                </span>
                                            </span>
                                            <span style="width:39%;float: right">
                                                <span style="width:100%; display:block; float:left; margin-left:1%;"> Valor do custo de transporte: </span>
                                                <span style="width:100%; display:block; float:left; margin-right: 1%;">
                                                    <input name="txtValorFrete" id="txtValorFrete" style="text-align: right;" value="<?php echo $valor_frete; ?>" <?php
                                                    if (is_numeric($transportadora_sa)) {
                                                        echo "disabled";
                                                    } else {
                                                        echo $disabled;
                                                    }
                                                    ?> type="text" class="input-medium"/>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="block">
                                        <?php if (is_numeric($id)) {
                                            include "../CRM/include/Documentos.php";
                                        } ?>                                        
                                        <div class="head yellow">
                                            <div class="icon"><span class="ico-money"></span></div>
                                            <h2><?php echo 'Comentários'; ?></h2>
                                        </div>
                                        <div class="data-fluid">
                                            <table class="table" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td width="100%">
                                                            <textarea name="txtComentarios" id="txtComentarios" <?php echo $disabled; ?> cols="45" rows="10"><?php echo $comentarios_venda; ?></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="100%" style="background:none">
                                                            <span style="width:45%; display:block; float:left"> Responsável pela Autorização da Compra: </span>
                                                            <span style="width:39%; display:block; float:left; margin-left:1%"> Status da Compra: </span>
                                                            <span style="width:14%; display:block; float:left; margin-left:1%"> OS: </span>
                                                            <span style="width:45%; display:block; float:left">
                                                                <select style="width: 100%;" name="txtAutorizacao" id="txtAutorizacao" <?php echo $disabled; ?>>
                                                                    <option value="">--Selecione--</option>
                                                                    <?php
                                                                    $where_bloq = '';
                                                                    if ($disabled == "disabled") {
                                                                        $where_bloq = " and login_user  = '" . $destinatario . "'";
                                                                    }
                                                                    $cur = odbc_exec($con, "select login_user,nome from sf_usuarios where inativo = 0 and login_user != 'Admin' and (master = 0 or master in (select id_permissoes from sf_usuarios_permissoes where est_acom = 1)) " . $where_bloq . " order by nome") or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                                        ?>
                                                                        <option value="<?php echo $RFP['login_user'] ?>"<?php
                                                                        if (!(strcmp($RFP['login_user'], $destinatario))) {
                                                                            echo "SELECTED";
                                                                        }
                                                                        ?>><?php echo utf8_encode($RFP['nome']) ?></option><?php } ?>
                                                                </select>
                                                            </span>
                                                            <span style="width:39%; display:block; float:left; margin-left:1%">
                                                                <input name="txtStatusVenda" id="txtStatusVenda" value="<?php echo $status; ?>" disabled type="text" class="input-medium"/>
                                                                </select>
                                                            </span>
                                                            <span style="width:14%; display:block; float:left; margin-left:1%">
                                                                <input name="txtComanda" id="txtComanda" value="<?php echo $comanda; ?>" type="text" class="input-medium" <?php echo $disabled; ?> style="text-transform: uppercase;" <?php echo (is_numeric($comanda) ? "readonly" : ""); ?>/>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="spanBNT3" style="margin-left:10px">
                                            <div class="toolbar bottom tar">
                                                <div class="data-fluid">                                                    
                                                    <div style="float:left">
                                                        <?php if ($disabled == 'disabled' && $status == 'Aguarda' && $_SESSION['login_usuario'] == $destinatario) { ?>
                                                            <button class="btn btn-success" type="submit" name="bntAprov" id="bntAprovar" ><span class="icon-ok icon-white"></span> Aprovar</button>
                                                            <button class="btn red" type="submit" name="bntReprov" id="bntReprovar"><span class="icon-remove icon-white"></span> Reprovar</button>
                                                        <?php } else if ($disabled == 'disabled' && ($status == 'Aprovado' || $status == 'Reprovado') && $_SESSION['login_usuario'] == $destinatario) { ?>                                                            
                                                            <button class="btn orange" type="submit" name="bntDesfazer" id="bntDesfazer"><span class="ico-undo icon-white"></span> Desfazer Autorização</button>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="btn-group">
                                                        <?php if ($_GET['idx'] != '') { ?>
                                                            <button class="btn btn-warning" type="submit" name="bntVoltar" id="bntVoltar" value="Voltar"><span class="icon-backward icon-white"> </span> Voltar</button>
                                                        <?php } if ($disabled == '') { ?>
                                                            <button class="btn btn-success" type="submit" name="bntSave" id="bntSave" onclick="return validaForm();"><span class="icon-ok icon-white"></span> Gravar</button>
                                                            <?php if ($_POST['txtId'] == '') { ?>
                                                                <button class="btn btn-success" onClick="history.go(-1)" id="bntCancelar"><span class="icon-backward icon-white"></span> Cancelar</button>
                                                            <?php } else { ?>
                                                                <button class="btn btn-success" type="submit" name="bntAlterar" id="bntAlterar" ><span class="icon-backward icon-white"></span> Cancelar</button>
                                                                <?php
                                                            }
                                                        } else {
                                                            ?>
                                                            <a href="Impressao.php?id=<?php echo $id . "&crt=" . $_SESSION['contrato']; ?>" target="_blank">
                                                                <button class="btn btn-primary" type="button" name="bntBol" id="bntBol" value="Imprimir" title="Imprimir"><span class="icon-print icon-white"> </span></button>
                                                            </a>
                                                            <button class="btn btn-success" type="submit" name="bntNew" id="bntNew" value="Novo" title="Novo"><span class="icon-plus icon-white"> </span></button>
                                                            
                                                            <button class="btn btn-success" type="submit" name="bntEdit" id="bntEdit" value="Alterar"> <span class="icon-edit icon-white"> </span> Alterar</button>
                                                            <button class="btn red" type="submit" name="bntDelete" id="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class="icon-remove icon-white"> </span> Excluir</button>
                                                            
                                                        <?php }; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="span6" style="width:50%; margin-left:2%;">
                                    <div class="block">
                                        <div class="head dblue">
                                            <div class="icon"><span class="ico-money"></span></div>
                                            <h2><?php echo 'Parcelamentos'; ?></h2>
                                            <ul class="buttons">
                                            </ul>
                                        </div>
                                        <div class="data-fluid">
                                            <div style="overflow:auto; min-height:523px; margin-bottom:0px;  border-bottom:solid 6px #099999;">
                                                <table class="table " cellpadding="0" cellspacing="0" width="100%" id="tbParcelas">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%" align="center">Nr:</th>
                                                            <th width="20%"  align="center">Nr Doc:</th>
                                                            <th width="15%"  align="center">Vencimento:</th>
                                                            <th width="20%"  align="right">Valor:</th>
                                                            <th width="38%"  align="right">Forma Pg:</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span4" style="width:100%; background-color:#f2f2f2; margin:0; padding:8px;">
                                        <table id="lblTotParc" name="lblTotParc" class="table" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    Total em parcelas: <strong><?php echo escreverNumero($TotalParcelas, 1); ?></strong>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <input name="txtIdTP" id="txtIdTP" value="<?php echo $idTP; ?>" type="hidden"/>
                            <input name="txtIdTS" id="txtIdTS" value="<?php echo $idTS; ?>" type="hidden"/>
                            <input name="pdfName" id="pdfName" value="<?php echo $pdfName; ?>" type="hidden"/>
                            <input name="xmlName" id="xmlName" value="<?php echo $xmlName; ?>" type="hidden"/>
                            <input name="chkCodigoProduto" id="chkCodigoProduto" value="<?php echo $CheckCodigoProduto; ?>" type="hidden"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="dialog" id="source" title="Source"></div>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>    
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/moment.min.js"></script>    
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>     
    <script language="javascript">
        
        $("#txtDtVenda, #txtParcelaMakeD").mask(lang["dateMask"]);        
        $("#txtParcelaMakeDC").mask("9999999999", {placeholder: ""});        
        $("#txtValorProduto, #txtProdDescontoVal, #txtValorProdutoS, #txtServDescontoVal, #txtParcelaMakeV, #txtParcelaMakeT, #txtValorFrete").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
        $("#hidValorProduto, #hidValorProdutoS").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"], centsLimit: 3});
        
        function refreshMask() {
            $(".mdate").mask(lang["dateMask"]);
            $(".parce").mask("9999999999", {placeholder: ""});
            $(".price").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
        }        
        
        $(document).on('keypress', 'input', function (e) {
            if (e.keyCode === 13 && e.target.type !== 'submit') {
                e.preventDefault();
                var inputs = $(this).parents("form").eq(0).find(":input:visible:not(disabled):not([readonly])"),
                        idx = inputs.index(this);
                if (idx === inputs.length - 1) {
                    inputs[0].select();
                } else {
                    inputs[idx + 1].focus();
                }
                return false;
            }
        });   

        function reLoad(id) {
            $('#txtFonecedor').hide();
            $('#carregando3').show();
            $.getJSON('./../Contas-a-pagar/fornecedor.ajax.php?search=', {txtTipo: 'F', ajax: 'true'}, function (j) {
                var options = '<option value="" >--Selecione--</option>';
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].id_grupo_contas + '">' + j[i].descricao + '</option>';
                }
                $('#txtFonecedor').html(options).show();
                $('#txtFonecedor').css("display", "none");
                $('#carregando3').hide();
            }).done(function () {
                $("#txtFonecedor").select2("val", id);
                reLoadEndereco(id);
            });
        }

        function clearT1() {
            $("#block3").toggle("slow");
        }

        function preencheZeros(valor, valor2, tamanho) {
            var qtd = parseInt(valor) + parseInt(valor2);
            if (qtd.toString().length < tamanho.length) {
                var limite = tamanho.length - qtd.toString().length;
                for (i = 0; i < limite; i++) {
                    qtd = '0' + qtd;
                }
            }
            return qtd;
        }

        function calculaParcela() {
            if ($("#txtParcelaMakeT").val() !== '') {
                var qtde = parseInt($("#spinner").val());
                var valTotal = textToNumber($("#txtParcelaMakeT").val());
                if (qtde > 0 && valTotal > 0) {
                    $("#txtParcelaMakeV").val(numberFormat(valTotal / qtde));
                } else {
                    $("#txtParcelaMakeV").val(numberFormat(0));
                }
            }
            if (textToNumber($("#txtParcelaV_1").val()) > 0) {
                Preencher();
            }            
        }                    

        function changeParc(obj) {
            var totAntes = 0;
            for (var i = 1; i <= obj; i++) {
                totAntes = totAntes + textToNumber($("#txtParcelaV_" + (i)).val());
            }
            recalculaParc(textToNumber($("#txtParcelaMakeT").val()), totAntes, obj + 1);
        }        
        
        function recalculaParc(TotalVenda, TotalPago, idLinha) {
            var qtd = parseInt($("#spinner").val()) - (idLinha - 1);
            var valParc = ((TotalVenda - TotalPago) / qtd);
            if ($('#tbParcelas >tbody >tr').length < idLinha) {
                valParc = textToNumber($("#txtParcelaV_" + (idLinha - 1)).val());
            }
            var total = TotalPago;
            var totParc = 0;
            for (var i = idLinha; i <= $('#tbParcelas >tbody >tr').length; i++) {
                $("#txtParcelaV_" + (i)).val(numberFormat(valParc));
                total = total + valParc;
                totParc = totParc + textToNumber($("#txtParcelaV_" + (i)).val());
            }
            var difTotal = TotalVenda - (totParc + TotalPago);
            if (difTotal > 0) {
                if ($('#tbParcelas >tbody >tr').length < idLinha) {
                    $("#txtParcelaV_" + (idLinha - 1)).val(numberFormat(valParc + difTotal));
                } else {
                    $("#txtParcelaV_" + (idLinha)).val(numberFormat(valParc + difTotal));
                }
            } else if (difTotal < 0) {
                $("#txtParcelaV_" + ($('#tbParcelas >tbody >tr').length)).val(numberFormat(valParc + difTotal));
            }
            $('#lblTotParc').hide();
            $('#lblTotParc').html('<tr><td>Total em parcelas: <strong>' + numberFormat(total, 1) + '</strong></td></tr>').show("slow");
        }
        
        $("#frmEnviaDados").validate({
            errorPlacement: function (error, element) {
                return true;
            },
            ignore: 'input[type=hidden]',
            rules: {
                txtHistorico: {required: true},
                txtDtVenda: {required: true},
                txtAutorizacao: {required: true},
                txtFonecedor: {required: true},
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                bootbox.alert("Existem campos obrigatórios não preenchidos!");
            },
            highlight: function (element, errorClass, validClass) {
                var elem = $(element);
                if (elem.hasClass("select")) {
                    $("#s2id_" + elem.attr("id") + " a").css("background-color", "#FF9F9F !important;");
                } else {
                    elem.addClass(errorClass);
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                var elem = $(element);
                if (elem.hasClass("select")) {
                    $("#s2id_" + elem.attr("id") + " a").css("background-color", "white");
                } else {
                    elem.removeClass(errorClass);
                }
            }
        });
        
        function validaForm() {
            var totGeral = textToNumber($("#totGeral").text());
            var totParc = 0;
            for (var i = 1; i <= $('#tbParcelas >tbody >tr').length; i++) {
                totParc = totParc + textToNumber($("#txtParcelaV_" + (i)).val());
                if ($("#txtTipo_" + (i)).val() === "null") {
                    bootbox.alert("Todas as parcelas devem possuir uma forma de pagamento!");
                    return false;
                }
                if ($("#txtParcelaD_" + (i)).val() === "") {
                    bootbox.alert("Todas as parcelas devem possuir uma data de pagamento!");
                    return false;
                }
            }
            if (totParc.toFixed(2) !== totGeral.toFixed(2)) {
                bootbox.alert("Não foi possivel salvar pois os valores de parcelas não conferem com o total!");
                return false;
            }
            return true;
        }
            
        function Preencher() {            
            var val = textToNumber($("#txtParcelaMakeV").val());
            var qtde = textToNumber($("#spinner").val());
            var valorx = textToNumber($("#txtParcelaMakeDC").val());
            var totDescParc = 0;
            var idLinha = 1;        
            if (qtde >= 0 && val >= 0 && moment($("#txtParcelaMakeD").val(), lang["dmy"], true).isValid()) {                                     
                for (var i = $('#tbParcelas >tbody >tr').length; i > qtde; i--) {
                    if ($("#txtPG_" + (i)).val() !== 'S') {
                        $("#linhaParcela_" + i).remove();
                    } else {
                        bootbox.alert("Número de parcelas inválido!");
                        return false;
                    }
                }            
                for (var i = 0; i < qtde; i++) {
                    var docParcela = "";                    
                    var now = moment($("#txtParcelaMakeD").val(), lang["dmy"], true);                    
                    var dataParcela = now.add(i, 'M').format(lang["dmy"]);
                    if ($("#txtParcelaMakeDC").val() !== '') {
                        docParcela = preencheZeros(valorx, i, $("#txtParcelaMakeDC").val());
                    }
                    if ($("#txtPG_" + (i + 1)).val() !== undefined) {
                        if ($("#txtPG_" + (i + 1)).val() !== 'S') {
                            $("#txtParcelaDC_" + (i + 1)).val(docParcela);
                            $("#txtParcelaD_" + (i + 1)).val(dataParcela);
                            $("#txtParcelaV_" + (i + 1)).val($("#txtParcelaMakeV").val());
                            $("#txtTipo_" + (i + 1)).val($("#txtTipo").val());
                        } else {
                            idLinha = idLinha + 1;
                            totDescParc += textToNumber($("#txtParcelaV_" + (i + 1)).val());
                        }
                    } else {
                        addLinhaParcela(i + 1, "", "", "", "N", docParcela, dataParcela, $("#txtParcelaMakeV").val());
                        $("#txtTipo_" + (i + 1)).val($("#txtTipo").val());
                    }
                }
                recalculaParc(textToNumber($("#txtParcelaMakeT").val()), totDescParc, idLinha);
            }
        }     
        
        var conta = $("#txtIdTP").val();
        var contaS = $("#txtIdTS").val();       
        
        function novaLinha() {
            var qtpX = textToNumber($("#txtQtdProduto").val());            
            if ($("#txtConta").val() !== "" && (qtpX > 0) && $("#txtValorProduto").val() !== numberFormat(0)) {
                var valTotal = textToNumber($("#txtValorProduto").val());
                if (valTotal > 0.00) {                                
                    var grp = $("#txtGrupo option:selected").text();
                    var prp = $("#txtConta option:selected").text();
                    var grc = $("#txtGrupo").val();
                    var qtp = $("#txtQtdProduto").val();
                    var prc = $("#txtConta").val();
                    var und = $("#txtUnidade").val();
                    var vlu = $("#hidValorProduto").val();
                    var vlp = $("#txtValorProduto").val();
                    var chg = 0;                                        
                    for (var i = 0; i < conta; i++) {
                        if ($("#txtPD_" + i).val() !== "null") {
                            var prv = $("#txtPD_" + i).val();
                            var qtv = $("#txtVQ_" + i).val();
                            //var vlv = textToNumber($("#txtVP_" + i).val());
                            if (prc === prv) {
                                qtv = parseInt(qtp);
                                $("#myVQ_" + i).html("<input name='txtVQ_" + i + "' id='txtVQ_" + i + "' type='hidden' value='" + qtv + "'/><center>" + qtv + "</center>");
                                $("#myVU_" + i).html("<input name='txtVU_" + i + "' id='txtVU_" + i + "' type='hidden' value='" + vlu + "'/><center>" + vlu + "</center>");
                                $("#myVP_" + i).html("<input name='txtVP_" + i + "' id='txtVP_" + i + "' type='hidden' value='" + vlp + "'/>" + vlp + "");
                                chg = 1;
                            }
                        }
                    }                    
                    if (chg === 0) {                    
                        var prpx = prp.split('[');
                        var linha = "<td><input name='txtIP_" + conta + "' id='txtIP_" + conta + "' type='hidden' value=''/><input name='txtGR_" + conta + "' id='txtGR_" + conta + "' type='hidden' value='" + grc + "'/>" + grp + "</td>" +
                        "<td>" + (typeof prpx[1] !== "undefined" ? "<center>" + prpx[1].replace("]", "") + "</center>" : "") + "</td>" +
                        "<td><input name='txtPD_" + conta + "' id='txtPD_" + conta + "' type='hidden' value='" + prc + "'/><a style='cursor: pointer;' onclick='carregaItemP(" + conta + ")'>" + prpx[0] + "</a></td>" +
                        "<td id='myUn_" + conta + "'><input name='txtUn_" + conta + "' id='txtUn_" + conta + "' type='hidden' value='" + und + "'/><center>" + und + "</center></td>" +
                        "<td id='myVQ_" + conta + "'><input name='txtVQ_" + conta + "' id='txtVQ_" + conta + "' type='hidden' value='" + qtp + "'/><center>" + qtp + "</center></td>" +
                        "<td id='myVU_" + conta + "' style='text-align: right;'><input name='txtVU_" + conta + "' id='txtVU_" + conta + "' type='hidden' value='" + vlu + "'/>" + vlu + "</td>" +
                        "<td id='myVP_" + conta + "' style='text-align: right;'><input name='txtVP_" + conta + "' id='txtVP_" + conta + "' type='hidden' value='" + vlp + "'/>" + vlp + "</td>" +
                        "<td><center><input class='btn red' type='button' value='x' onClick=\"javascript:removeLinha('tabela_linha_" + conta + "')\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\"></input></center></td>";
                        $("#tabela_produto").append("<tr id='tabela_linha_" + conta + "'>" + linha + "</tr>");
                        conta++;
                    }
                    somaTotal();
                }
            } else {
                bootbox.alert("Todos os campos devem ser preenchidos.");
            }
        }
        
        function novaLinhaS() {
            if ($("#txtContaS").val() !== "" && $("#txtValorProdutoS").val() !== numberFormat(0) && $("#txtQtdProdutoS").val() > 0) {
                var valTotal = textToNumber($("#txtValorProdutoS").val());
                if (valTotal > 0.00) {
                    var grp = $("#txtGrupoS option:selected").text();
                    var prp = $("#txtContaS option:selected").text();
                    var grc = $("#txtGrupoS").val();
                    var prc = $("#txtContaS").val();                    
                    var qtp = $("#txtQtdProdutoS").val();                    
                    var vlu = $("#hidValorProdutoS").val();                    
                    var vlp = $("#txtValorProdutoS").val();                    
                    var chg = 0;   
                    for (var i = 0; i < contaS; i++) {
                        if ($("#txtPDS_" + i).val() !== "null") {
                            var prv = $("#txtPDS_" + i).val();
                            var qtv = $("#txtVQS_" + i).val();
                            //var vlv = textToNumber($("#txtVPS_" + i).val());
                            if (prc === prv) {
                                qtv = parseInt(qtp);
                                $("#myVQS_" + i).html("<input name='txtVQS_" + i + "' id='txtVQS_" + i + "' type='hidden' value='" + qtv + "'/><center>" + qtv + "</center>");
                                $("#myVUS_" + i).html("<input name='txtVUS_" + i + "' id='txtVUS_" + i + "' type='hidden' value='" + vlu + "'/><center>" + vlu + "</center>");                                
                                $("#myVPS_" + i).html("<input name='txtVPS_" + i + "' id='txtVPS_" + i + "' type='hidden' value='" + vlp + "'/>" + vlp + "");
                                chg = 1;
                            }
                        }
                    }                                     
                    if (chg === 0) {
                        var linha = "<td><input name='txtIPS_" + contaS + "' id='txtIPS_" + contaS + "' type='hidden' value=''/><input name='txtGRS_" + contaS + "' id='txtGRS_" + contaS + "' type='hidden' value='" + grc + "'/>" + grp + "</td>" +
                        "<td><input name='txtPDS_" + contaS + "' id='txtPDS_" + contaS + "' type='hidden' value='" + prc + "' <a style='cursor: pointer;' onclick='carregaItemS(" + contaS + ")'>" + prp + "</a></td>" +
                        "<td id='myVQS_" + contaS + "'><input name='txtVQS_" + contaS + "' id='txtVQS_" + contaS + "' type='hidden' value='" + qtp + "'/><center>" + qtp + "</center></td>" +
                        "<td id='myVUS_" + contaS + "' style='text-align: right;'><input name='txtVUS_" + contaS + "' id='txtVUS_" + contaS + "' type='hidden' value='" + vlu + "'/>" + vlu + "</td>" +
                        "<td id='myVPS_" + contaS + "' style='text-align: right;'><input name='txtVPS_" + contaS + "' id='txtVPS_" + contaS + "' type='hidden' value='" + vlp + "'/>" + vlp + "</td>" +
                        "<td><center><input class='btn red' type='button' value='x' onClick=\"javascript:removeLinha('tabela_linhaS_" + contaS + "')\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\"></input></center></td>";                        
                        $("#tabela_produtoS").append("<tr id='tabela_linhaS_" + contaS + "'>" + linha + "</tr>");
                        contaS++;
                    }
                    somaTotal();
                }
            } else {
                bootbox.alert("Todos os campos devem ser preenchidos.");
            }
        }
        
        function removeLinha(id) {
            $("#" + id).remove();
            somaTotal();
        }
            
        function aplicaDesconto(tipo) {
            if (tipo === "P") {
                $("#txtProdDesconto").val($("#txtProdDescontoVal").val());
                var totalVendas = 0;                
                for (var i = 0; i < conta; i++) {
                    if ($("#txtPD_" + i).val() !== "null") {
                        totalVendas = totalVendas + textToNumber($("#txtVP_" + i).val());
                    }
                }                
                var prod_desc = textToNumber($("#txtProdDesconto").val());
                if ($("#prod_sinal").html() === "%") {
                    prod_desc = totalVendas * (prod_desc / 100);
                }
                $("#txtTotalProd").html("Total sem Desconto: " + numberFormat(totalVendas, 1) + "<br>Valor do Desconto: " + numberFormat(prod_desc, 1) + "<br><b>Total com Desconto: " + numberFormat((totalVendas - prod_desc), 1) + "</b>");
            } else {
                $("#txtServDesconto").val($("#txtServDescontoVal").val());
                var totalServicos = 0;
                for (var i = 0; i < contaS; i++) {
                    if ($("#txtPDS_" + i).val() !== "null") {
                        totalServicos = totalServicos + textToNumber($("#txtVPS_" + i).val());
                    }
                }                                 
                var serv_desc = textToNumber($("#txtServDesconto").val());
                if ($("#serv_sinal").html() === "%") {
                    serv_desc = totalServicos * (serv_desc / 100);
                }
                $("#txtTotalServ").html("Total sem Desconto: " + numberFormat(totalServicos, 1) + "<br>Valor do Desconto: " + numberFormat(serv_desc, 1) + "<br><b>Total com Desconto: " + numberFormat((totalServicos - serv_desc), 1) + "</b>");
            }
            somaTotal();
        }

        function somaTotal() {
            var totalVendas = 0;
            var totalServicos = 0;
            var totalGeral = 0;            
            for (var i = 0; i < conta; i++) {
                if ($("#txtPD_" + i).val() !== "null") {
                    totalVendas = totalVendas + textToNumber($("#txtVP_" + i).val());
                }
            }
            for (var i = 0; i < contaS; i++) {
                if ($("#txtPDS_" + i).val() !== "null") {
                    totalServicos = totalServicos + textToNumber($("#txtVPS_" + i).val());
                }
            }           
            var prod_desc = textToNumber($("#txtProdDesconto").val());
            if ($("#prod_sinal").html() === "%") {
                prod_desc = totalVendas * (prod_desc / 100);
            }
            $("#txtTotalProd").html("Total sem Desconto: " + numberFormat(totalVendas, 1) + "<br>Valor do Desconto: " + numberFormat(prod_desc, 1) + "<br><b>Total com Desconto: " + numberFormat((totalVendas - prod_desc), 1) + "</b>");            
            var serv_desc = textToNumber($("#txtServDesconto").val());
            if ($("#serv_sinal").html() === "%") {
                serv_desc = totalServicos * (serv_desc / 100);
            }
            $("#txtTotalServ").html("Total sem Desconto: " + numberFormat(totalServicos, 1) + "<br>Valor do Desconto: " + numberFormat(serv_desc, 1) + "<br><b>Total com Desconto: " + numberFormat((totalServicos - serv_desc), 1) + "</b>");            
            totalGeral = totalGeral + (totalServicos - serv_desc);
            if ($("#ckbNValores_Produtos").is(":checked") === false) {
                totalGeral = totalGeral + (totalVendas - prod_desc);
            }
            $("#txtParcelaMakeT").val(numberFormat(totalGeral));
            $("#totGeral").html(numberFormat(totalGeral), 1);
            calculaParcela();
            $("#txtIdTP").val(conta);
            $("#txtIdTS").val(contaS);
        }           
        
        function checaValor() {
            var totalVendas = 0;
            var totalServicos = 0;
            for (var i = 0; i < conta; i++) {
                if ($("#txtPD_" + i).val() !== "null") {
                    totalVendas = totalVendas + textToNumber($("#txtVP_" + i).val());
                }
            }
            for (var i = 0; i < contaS; i++) {
                if ($("#txtPDS_" + i).val() !== "null") {
                    totalServicos = totalServicos + textToNumber($("#txtVPS_" + i).val());
                }
            }         
            if ((totalVendas + totalServicos) > textToNumber($("#txtParcelaMakeT").val())) {
                somaTotal();
            }
        }
        
        function MakeDateFormPG(forma) {
            $.getJSON('../Estoque/tipo.documento.ajax.php?search=', {txtCampos: forma, ajax: 'true'}, function (j) {
                for (var i = 0; i < j.length; i++) {
                    if (j[i].dias > 0) {
                        var dias = j[i].dias;
                        var aDate = moment($("#txtDtVenda").val(), lang["dmy"], true);                
                        $("#txtParcelaMakeD").val(aDate.add(dias, 'days').format(lang["dmy"]));                                                
                    } else {
                        $("#txtParcelaMakeD").val($("#txtDtVenda").val());
                    }
                }
            });
        }
        
        function carregaParcelas() {
            $.getJSON("Vendas_controller.php?listaParcelas=" + $("#txtId").val(), function (data) {
                var total = 0;
                if (data !== null) {
                    var nParc = 0;
                    for (var i = 0; i < data.length; i++) {
                        nParc = i + 1;
                        var pago = "N";
                        var backcolor = "";
                        var disabled = $("#txtDtVenda").attr("disabled") === "disabled" ? "disabled" : "";
                        if (data[i].valor_pago !== numberFormat(0)) {
                            pago = "S";
                            backcolor = "#4899ba";
                            disabled = "disabled";
                        }
                        addLinhaParcela(nParc, backcolor, disabled, data[i].id_parcela, pago, data[i].sa_descricao, data[i].data_parcela, data[i].valor_parcela);
                        $("#txtTipo_" + nParc).val(data[i].tipo_documento);
                        total += textToNumber(data[i].valor_parcela);
                    }
                    $('#lblTotParc').html('<tr><td>Total em parcelas: <strong>' + numberFormat(total, 1) + '</strong></td></tr>').show("slow");
                }
            });
        }
        
        function prodValor() {            
            var UpdQtd = textToNumber($("#txtQtdProduto").val());
            var UpdVal = textToNumber($("#hidValorProduto").val());
            $("#txtValorProduto").val(numberFormat((UpdVal * UpdQtd)));
        }
        
        function servValor() {            
            var UpdQtd = textToNumber($("#txtQtdProdutoS").val());
            var UpdVal = textToNumber($("#hidValorProdutoS").val());
            $("#txtValorProdutoS").val(numberFormat((UpdVal * UpdQtd)));
        }
        
        function AbrirBox(opc) {
            if (opc === 1) {
                $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:717px; height:563px; position:absolute; top:50%; left:50%; margin-left:-358px; margin-top:-282px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:563px' src='./../Estoque/FormProdutos.php?idx=0'></iframe></div></div></div></div></div>");
            }
            if (opc === 2) {
                $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:667px; height:390px; position:absolute; top:50%; left:50%; margin-left:-332px; margin-top:-114px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:390px' src='./../Estoque/FormServicos.php?idx=0'></iframe></div></div></div></div></div>");
            }
            if (opc === 3) {
                $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:717px; height:550px; position:absolute; top:50%; left:50%; margin-left:-358px; margin-top:-275px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:550px' src='./../Financeiro/FormFornecedores-de-despesas.php?idx=0'></iframe></div></div></div></div></div>");
            }
            if (opc === 4) {
                $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:917px; height:550px; position:absolute; top:50%; left:50%; margin-left:-458px; margin-top:-275px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:550px' src='./../Estoque/FormImportarXML.php?idx=0'></iframe></div></div></div></div></div>");
            }
        }
        
        function FecharBox(opc) {
            $("#newbox").remove();
            if (opc === 1) {
                $("#txtGrupo").trigger("change");
                $("#s2id_txtConta > .select2-choice > span").html("--Selecione--");
            }
            if (opc === 2) {
                $("#txtGrupoS").trigger("change");
                $("#s2id_txtContaS > .select2-choice > span").html("--Selecione--");
            }
            if (opc === 3) {
                reLoad(0);
            }
        }
        
        function carregaItemP(item) {
            $("#txtGrupo").select2("val", $("#txtGR_" + item).val());
            $("#txtGrupo").trigger('change');
            setTimeout(function () {
                $("#txtConta").select2("val", $("#txtPD_" + item).val());
                $("#txtUnidade").val($("#txtUn_" + item).val());
                $("#txtQtdProduto").val($("#txtVQ_" + item).val());
                $("#hidValorProduto").val($("#txtVU_" + item).val());
                $("#txtValorProduto").val($("#txtVP_" + item).val());
                $("#txtComProduto").select2("val", $("#txtCO_" + item).val());
            }, 1500);
        }
        
        function carregaItemS(item) {
            $("#txtGrupoS").select2("val", $("#txtGRS_" + item).val());
            $("#txtGrupoS").trigger('change');
            setTimeout(function () {
                $("#txtContaS").select2("val", $("#txtPDS_" + item).val());
                $("#txtQtdProdutoS").val($("#txtVQS_" + item).val());
                $("#hidValorProdutoS").val($("#txtVUS_" + item).val());
                $("#txtValorProdutoS").val($("#txtVPS_" + item).val());
                $("#txtComProdutoS").select2("val", $("#txtCOS_" + item).val());
            }, 1500);
        }
        
        function addLinhaParcela(nParc, backcolor, disabled, id_parcela, pago, sa_descricao, data_parcela, valor_parcela) {
            var tbody = "";
            tbody += "<tr id='linhaParcela_" + nParc + "'><td valign='middle'>" +
                    "<center><input name='txtID_" + nParc + "' id='txtID_" + nParc + "' value='" + id_parcela + "' type='hidden'/>" +
                    "<input name='txtPG_" + nParc + "' id='txtPG_" + nParc + "' value='" + pago + "' type='hidden'/>" +
                    "<span style='margin-top:8px; display:block;'>" + nParc + "</span></center></td>";
            tbody += "<td><input style='text-align: center;background-color:" + backcolor + "' class='input-xxlarge inputDireito parce' name='txtParcelaDC_" + nParc + "' id='txtParcelaDC_" + nParc + "' " + disabled + " type='text' value='" + sa_descricao + "' /></td>";
            tbody += "<td><input style='text-align: center;background-color:" + backcolor + "' class='input-xxlarge inputDireito mdate' name='txtParcelaD_" + nParc + "' id='txtParcelaD_" + nParc + "' " + disabled + " type='text' value='" + data_parcela + "' /></td>";
            tbody += "<td><input style='text-align: center;background-color:" + backcolor + "' class='input-xxlarge inputDireito price' name='txtParcelaV_" + nParc + "' id='txtParcelaV_" + nParc + "' " + disabled + " onBlur='changeParc(" + nParc + ")' type='text' value='" + valor_parcela + "'/></td>";
            tbody += "<td><select name='txtTipo_" + nParc + "' id='txtTipo_" + nParc + "' style='width:100%;background-color:" + backcolor + "' " + disabled + "> \n\
                     " + $('#txtTipo').html() + "</select></td>";
            tbody += "</tr>";
            $("#tbParcelas tbody").append(tbody);
            refreshMask();
        }
        
        $(document).ready(function () {
            
            carregaParcelas();            
            
            if ($("#txtProdSinal").val() === 1) {
                $("#prod_sinal").html("$");
            }
            
            if ($("#txtServSinal").val() === 1) {
                $("#serv_sinal").html("$");
            }
            
            if($("#txtDtVenda").attr("disabled") !== "disabled") {
                $('.ui-spinner-button').click(function () { 
                    calculaParcela();
                });                                 
            }       
            
            $('#txtFonecedor').change(function () {
                if ($(this).val()) {
                    reLoadEndereco($(this).val());
                } else {
                    reLoadEndereco(0);
                }
            });
            
            $('#txtConta').change(function () {
                if ($(this).val()) {
                    $.getJSON('conta_a.ajax.php?search=', {txtConta: $(this).val(), ajax: 'true'}, function (j) {
                        var grupo = 'null';
                        var preco = numberFormat(0);
                        var qtdcom = '1';
                        var unidade = '';
                        for (var i = 0; i < j.length; i++) {
                            if (j[i].grupo_conta !== '') {
                                grupo = j[i].grupo_conta;
                            }
                            if (j[i].preco_venda !== '') {
                                preco = j[i].preco_venda;
                            }
                            if (j[i].unidade_comercial !== '') {
                                unidade = j[i].unidade_comercial;
                            }
                            if (j[i].quantidade_comercial !== '') {
                                if (parseFloat(j[i].quantidade_comercial) > 0) {
                                    qtdcom = parseInt(j[i].quantidade_comercial);
                                } else {
                                    qtdcom = 1;
                                }
                            }
                        }
                        $('#txtGrupo').find('option[value="' + grupo + '"]').attr('selected', true);
                        $('#txtUnidade').val(unidade).show();
                        $('#txtQtdProduto').val(qtdcom).show();
                        $('#txtValorProduto').val(numberFormat(preco)).show();
                        $('#hidValorProduto').val(numberFormat(preco) + "0").show();
                    });
                } else {
                    $('#txtUnidade').html('');
                    $('#txtQtdProduto').html('1');
                    $('#txtValorProduto').html(numberFormat(0));
                    $('#hidValorProduto').html(numberFormat(0));
                }
            });

            $('#txtContaS').change(function () {
                if ($(this).val()) {
                    $.getJSON('conta_a.ajax.php?search=', {txtContaS: $(this).val(), ajax: 'true'}, function (j) {
                        var grupo = 'null';
                        var preco = numberFormat(0);
                        var qtdcom = '1';
                        for (var i = 0; i < j.length; i++) {
                            if (j[i].grupo_conta !== '') {
                                grupo = j[i].grupo_conta;
                            }
                            if (j[i].preco_venda !== '') {
                                preco = j[i].preco_venda;
                            }
                            if (j[i].quantidade_comercial !== '') {
                                if (parseFloat(j[i].quantidade_comercial) > 0) {
                                    qtdcom = parseInt(j[i].quantidade_comercial);
                                } else {
                                    qtdcom = 1;
                                }
                            }
                        }
                        $('#txtGrupoS').find('option[value="' + grupo + '"]').attr('selected', true);
                        $('#txtQtdProdutoS').val(qtdcom).show();
                        $('#txtValorProdutoS').val(numberFormat(preco)).show();
                        $('#hidValorProdutoS').val(numberFormat(preco) + "0").show();
                    });
                } else {
                    $('#txtQtdProdutoS').html('1');
                    $('#txtValorProdutoS').html(numberFormat(0));
                    $('#hidValorProdutoS').html(numberFormat(0));
                }
            }); 
            
            $('#txtTipo').change(function () {
                if ($(this).val()) {
                    $('#tblTable').hide();
                    $.getJSON('campos.ajax.php?search=', {txtCampos: $(this).val(), ajax: 'true'}, function (j) {
                        var options = '<thead><tr><th width="29%">Descrição:</th><th width="71%">Conteúdo:</th></tr></thead><tbody>';
                        for (var i = 0; i < j.length; i++) {
                            options += '<tr><td>' + j[i].campo + '</td><td><input name="txtCampo' + j[i].id_tipo_documento_campos + '" id="txtCampo' + j[i].id_tipo_documento_campos + '" ' + 
                            ($("#txtDtVenda").attr("disabled") === "disabled" ? "disabled" : "") + ' type="text" class="input-xlarge" value=""/></td></tr>';
                        }
                        options += '</tbody>';
                        MakeDateFormPG($('#txtTipo').val());
                        $('#tblTable').html(options).show("slow");
                    });
                } else {
                    $('#txtTipo').html('<thead><tr><th width="29%">Descrição:</th><th width="71%">Conteúdo:</th></tr></thead><tbody><tr><td></tr></td></tbody>');
                }
            });
            
            $('#txtGrupo').change(function () {
                if ($(this).val()) {
                    reLoadProdutos($(this).val());
                } else {
                    reLoadProdutos(0);
                }
            });

            $('#txtGrupoS').change(function () {
                if ($(this).val()) {
                    $('#txtContaS').hide();
                    $('#carregandoS').show();
                    $.getJSON('conta_s.ajax.php?search=', {txtGrupo: $(this).val(), ajax: 'true'}, function (j) {
                        var options = '<option value="">--Selecione--</option>';
                        for (var i = 0; i < j.length; i++) {
                            options += '<option value="' + j[i].id_contas_movimento + '">' + j[i].descricao + '</option>';
                        }
                        $('#txtContaS').html(options).show();
                        $('#txtContaS').css("display", "none");
                        $('#carregandoS').hide();
                    });
                } else {
                    $('#txtContaS').html('<option value="">--Selecione--</option>');
                }
            });

            $("#bntProdDesconto").click(function () {
                if ($("#prod_sinal").html() === "%") {
                    $("#prod_sinal").html("$");
                    $("#txtProdSinal").val("1");
                } else {
                    $("#prod_sinal").html("%");
                    $("#txtProdSinal").val("0");
                }
            });

            $("#bntServDesconto").click(function () {
                if ($("#serv_sinal").html() === "%") {
                    $("#serv_sinal").html("$");
                    $("#txtServSinal").val("1");
                } else {
                    $("#serv_sinal").html("%");
                    $("#txtServSinal").val("0");
                }
            });
            
            $("#txtGrupoConta").change(function () {
                $("#txtContaConta").select2("val", "");
                if ($(this).val()) {
                    $.getJSON("conta.ajax.php?search=", {txtGrupo: $(this).val(), ajax: "true"}, function (j) {
                        var options = "<option value=''>--Selecione--</option>";
                        for (var i = 0; i < j.length; i++) {
                            options += "<option value='" + j[i].id_contas_movimento + "'>" + j[i].descricao + "</option>";
                        }
                        $("#txtContaConta").html(options);
                    });
                } else {
                    $("#txtContaConta").select2("val", "");
                }
            });            
        });
        
        function adicionaItem(tipo) {
            if ($("#txtId").val() === "") {
                if (tipo === 'P') {
                    novaLinha();
                } else {
                    novaLinhaS();
                }
            } else {
                var prod_cod, prod_grp, prod_qnt, prod_prc = "";
                if (tipo === 'P') {
                    prod_cod = $("#txtConta").val();
                    prod_grp = $("#txtGrupo").val();
                    prod_qnt = $("#txtQtdProduto").val();
                    prod_prc = $("#hidValorProduto").val();
                } else {
                    prod_cod = $("#txtContaS").val();
                    prod_grp = $("#txtGrupoS").val();
                    prod_qnt = $("#txtQtdProdutoS").val();
                    prod_prc = $("#hidValorProdutoS").val();
                }
                salvaItem(prod_cod, prod_grp, prod_qnt, prod_prc, tipo);
            }
        }        

        function salvaItem(prod_cod, prod_grp, prod_qnt, prod_prc, tipo) {
            $.ajax({
                type: "POST",
                url: "Vendas_controller.php",
                data: {insereItem: 'S',
                    id_vd: $("#txtId").val(),
                    prod_cod: prod_cod,
                    prod_grp: prod_grp,
                    prod_qnt: prod_qnt,
                    prod_prc: prod_prc},
                success: function (data) {
                    if ($.isNumeric(data)) {
                        if (tipo === 'P') {
                            novaLinha();
                        } else {
                            novaLinhaS();
                        }
                    }
                }
            });
        }

        function reLoadEndereco(id) {
            if (id > 0) {
                $('#carregando4').show();
                $('#carregando5').show();
                $.getJSON('endereco.ajax.php?search=', {txtFonecedor: id, ajax: 'true'}, function (j) {
                    var optionsDados = '';
                    var options = '';
                    for (var i = 0; i < j.length; i++) {
                        if (j[i].endereco !== '') {
                            options += j[i].endereco;
                        }
                        if (j[i].numero !== '') {
                            options += ' N° ' + j[i].numero;
                        }
                        if (j[i].complemento !== '') {
                            options += ' ' + j[i].complemento;
                        }
                        if (j[i].bairro !== '') {
                            options += ',' + j[i].bairro;
                        }
                        if (j[i].cidade_nome !== '') {
                            options += '\n' + j[i].cidade_nome;
                        }
                        if (j[i].estado_sigla !== '') {
                            options += '-' + j[i].estado_sigla;
                        }
                        if (j[i].cep !== '') {
                            options += ', Cep: ' + j[i].cep;
                        }
                        if (j[i].cnpj !== '') {
                            optionsDados += 'CNPJ/CPF:' + j[i].cnpj;
                        }
                        if (j[i].telefone !== '') {
                            optionsDados += '\nTel:' + j[i].telefone;
                        }
                        if (j[i].email !== '') {
                            optionsDados += ' E-mail:' + j[i].email;
                        }
                    }
                    $('#txtDados').html(optionsDados).show();
                    $('#txtEntrega').html(options).show();
                    $('#carregando4').hide();
                    $('#carregando5').hide();
                });
            } else {
                $('#txtDados').html('');
                $('#txtEntrega').html('');
            }
        }
            
        function reLoadProdutos(id) {
            if (id > 0 || id === null) {
                $('#txtConta').hide();
                $('#carregando').show();
                $.getJSON('conta_p.ajax.php?search=', {txtGrupo: id, ajax: 'true'}, function (j) {
                    var options = '<option value="null">--Selecione--</option>';
                    for (var i = 0; i < j.length; i++) {
                        options += '<option value="' + j[i].id_contas_movimento + '">' + ($("#chkCodigoProduto").val() === "1" ? j[i].codigo_barra + " - " :  "") + j[i].descricao + '</option>';
                    }
                    $('#txtConta').html(options).show();
                    $('#txtConta').css("display", "none");
                    $('#carregando').hide();
                });
            } else {
                $('#txtConta').html('<option value="">--Selecione--</option>');
            }
        }

        function EnviaXML(id, produtos, quantidades, valores, grupos, unidades, pdfName, xmlName) {            
            $("#pdfName").val(pdfName);
            $("#xmlName").val(xmlName);
            $("#newbox").remove();
            reLoad(id);
            $("#txtGrupo").select2("val", null);
            $('#txtConta').hide();
            $('#carregando').show();
            $.getJSON('conta_p.ajax.php?search=', {txtGrupo: null, ajax: 'true'}, function (j) {
                var options = '<option value="null">--Selecione--</option>';
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].id_contas_movimento + '">' + j[i].descricao + '</option>';
                }
                $('#txtConta').html(options).show();
                $('#txtConta').css("display", "none");
                $('#carregando').hide();
            }).done(function () {
                for (i = 1; i < produtos.length; i++) {
                    $("#txtGrupo").select2("val", grupos[i]);
                    $("#txtConta").select2("val", produtos[i]);
                    $('#txtUnidade').val(unidades[i]).show();
                    $("#txtQtdProduto").val(numberFormat(quantidades[i]));
                    $("#hidValorProduto").val(numberFormat(valores[i]));
                    prodValor();
                    adicionaItem('P');
                }
                clearT1();
                $("#txtTipo").select2("val", 6);
                $("#txtParcelaMakeD").val($("#txtDtVenda").val());
                Preencher();
                $("#txtAutorizacao").val($("#txtDestinatario").val());
            });
        }
        
        $(window).keydown(function (event) {
            if (event.ctrlKey && event.keyCode === 74) {
                event.preventDefault();
            }
        });
        
        $('#tbDocumentos').dataTable({
            "iDisplayLength": 20,
            "aLengthMenu": [20, 30, 40, 50, 100],
            "bProcessing": true,
            "bServerSide": true,
            "bSort": false,
            "bLengthChange": false,
            "bFilter": false,  
            "sAjaxSource": "../../Modulos/CRM/ajax/Documentos_server_processing.php?id=" + $("#txtId").val() + "&type=Compras",
            'oLanguage': {
                'oPaginate': {
                    'sFirst': "Primeiro",
                    'sLast': "Último",
                    'sNext': "Próximo",
                    'sPrevious': "Anterior"
                }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                'sLengthMenu': "Visualização de _MENU_ registros",
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sSearch': "Pesquisar:",
                'sZeroRecords': "Não foi encontrado nenhum resultado"},
            "sPaginationType": "full_numbers"
        });

        function refreshDoc() {
            let tblCRM = $('#tbDocumentos').dataTable();
            tblCRM.fnReloadAjax("../../Modulos/CRM/ajax/Documentos_server_processing.php?id=" + $("#txtId").val() + "&type=Compras");
        }

        function AdicionarDoc(e) {
            $("#loader").show();
            var formData = new FormData();
            formData.append("btnSendFile", "S");
            formData.append("txtId", $("#txtId").val());
            formData.append("type", "Compras");
            formData.append("file", $("#file")[0].files[0]); 
            $.ajax({
                type: 'POST',
                url: "../../Modulos/CRM/form/CRMDocumentos.php",
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            }).done(function (data) {
                $("#loader").hide();
                if (data === "YES") {
                    $('#file').val('');
                    refreshDoc();
                } else {
                    bootbox.alert(data);
                }
            });
        }

        function RemoverDoc(filename) {
            var nomedoc = encodeURIComponent(filename.toString());
            bootbox.confirm('Confirma a exclusão do registro?', function (result) {
                if (result === true) {
                    $.post("../../Modulos/CRM/form/CRMDocumentos.php", "btnDelFile=S&txtId=" + $("#txtId").val() + "&type=Compras&txtFileName=" + nomedoc).done(function (data) {
                        if (data === "YES") {
                            refreshDoc();
                        } else {
                            bootbox.alert(data + "!");
                        }
                    });
                } else {
                    return;
                }
            });
        }        
        
    </script>
</body>
<?php odbc_close($con); ?>
</html>
