<?php

require_once(__DIR__ . '/../../../Connections/configini.php');

if (isset($_POST['salvaCompras'])) {
    $query = "insert into sf_vendas (vendedor, cliente_venda, historico_venda, comentarios_venda, data_venda, sys_login, empresa, destinatario, status,
    tipo_documento, cov, garantia_venda, garantia_servico, descontop, descontos, entrega, validade, descontotp, descontots, n_servicos, data_aprov, cod_pedido,
    grupo_conta, conta_movimento, favorito, reservar_estoque, ent_sai)
    select responsavel, id_fornecedor, historico_venda, comentarios_venda, GETDATE(), 'admin', empresa, destinatario_cotacao, 'Aguarda',
    id_tipo_documento, 'C', '', '', 0.00, 0.00, FORMAT(dateadd(day, dias_frete, GETDATE()), 'dd/MM/yyyy'), '', 0, 0, 0, null, '" . str_pad($_POST['cotacao'], 5, '0', STR_PAD_LEFT) .  "', 15, 5, 0, 0, 0
    from sf_requisicao_cotacao c inner join sf_requisicao r on c.id_requisicao = r.id_requisicao
    where id_cota = " . valoresNumericos('cotacao');
    odbc_exec($con, $query) or die(odbc_errormsg());
    $res = odbc_exec($con, "select top 1 id_venda from sf_vendas order by id_venda desc") or die(odbc_errormsg());
    $nn = odbc_result($res, 1);
    
    $query = "insert into sf_vendas_itens (id_venda, grupo, produto, quantidade, valor_total, valor_bruto, valor_multa)
    select " . $nn . ", (select conta_movimento from sf_produtos where conta_produto = produto),
    produto, quantidade, (valor * quantidade), (valor * quantidade), 0.00 from sf_requisicao_itens ri
    inner join sf_requisicao_cotacao_itens ci on ri.id_item = ci.id_item
    where id_cota = " . valoresNumericos('cotacao') . " and ci.id_item in (" . $_POST['itens'] . ")";    
    odbc_exec($con, $query) or die(odbc_errormsg());
    
    $query = "insert into sf_venda_parcelas (venda, numero_parcela, data_parcela, valor_parcela, valor_pago, pa, sa_descricao, inativo, historico_baixa, valor_multa, valor_juros, tipo_documento, valor_parcela_liquido, data_cadastro, exp_remessa, somar_rel)
    select " . $nn . ", '1', cast(GETDATE() as date), (select sum(valor * quantidade) from sf_requisicao_itens ri
    inner join sf_requisicao_cotacao_itens ci on ri.id_item = ci.id_item where ci.id_cota = c.id_cota and ci.id_item in (" . $_POST['itens'] . ")), 
    0.00, '01/01', '', 0, '', 0.00, 0.00, id_tipo_documento, 0.00, GETDATE(), 0, 1
    from sf_requisicao_cotacao c
    where id_cota = " . valoresNumericos('cotacao');
    odbc_exec($con, $query) or die(odbc_errormsg());
    
    $query = "update sf_requisicao_cotacao set id_venda = " . $nn . " where id_cota = " . valoresNumericos('cotacao');
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo $nn;
}

if (isset($_GET['listCotacoes'])) {
    $query = "select id_cota, id_fornecedor, ordem, id_tipo_documento, frete, dias_frete, razao_social, tipo, id_venda,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato
    from sf_requisicao_cotacao inner join sf_fornecedores_despesas on id_fornecedores_despesas = id_fornecedor
    where id_requisicao = " . valoresNumericos2($_GET['id']) . " order by ordem";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();        
        $descricao = utf8_encode($RFP['razao_social']);
        if ($RFP['nome_fantasia'] != "") {
            $descricao = $descricao . " (" . utf8_encode($RFP['nome_fantasia']) . ")";
        }
        if ($RFP['tipo'] != "") {
            $descricao = $descricao . " [" . getTipoFornecedor($RFP['tipo']) . "]";
        }
        $row['razao_social'] = $descricao;                
        $row["telefone_contato"] = utf8_encode($RFP["telefone_contato"]);
        $row["id_cota"] = utf8_encode($RFP["id_cota"]);
        $row["id_fornecedor"] = utf8_encode($RFP["id_fornecedor"]);
        $row["ordem"] = utf8_encode($RFP["ordem"]);
        $row["id_tipo_documento"] = utf8_encode($RFP["id_tipo_documento"]);
        $row["frete"] = escreverNumero($RFP["frete"]);
        $row["dias_frete"] = utf8_encode($RFP["dias_frete"]);
        $row["id_venda"] = utf8_encode($RFP["id_venda"]);
        $row["itens"] = listCotacoesItens($con, $RFP["id_cota"]);
        $records[] = $row;
    }
    echo json_encode($records);
}

function listCotacoesItens($con, $id) {
    $records = [];
    $query = "select id_item, valor, ativo from sf_requisicao_cotacao_itens where id_cota = " . $id;
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();  
        $row["id_item"] = utf8_encode($RFP["id_item"]);
        $row["valor"] = escreverNumero($RFP["valor"]);
        $row["ativo"] = utf8_encode($RFP["ativo"]);
        $records[] = $row;
    }
    return $records;
}

function getTipoFornecedor($tipo) {
    if ($tipo == "P") {
        return "Prospect";
    } elseif ($tipo == "C") {    
        return "Cliente";
    } elseif ($tipo == "E") {    
        return "Funcionário";
    } elseif ($tipo == "F") {    
        return "Fornecedor";
    } elseif ($tipo == "T") {    
        return "Transportadora";
    }
}