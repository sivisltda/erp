<?php
include ('../../Connections/configini.php');
$dataVenc = getData("T");
$gc = 0;
$cm = 0;
$doc = 0;
$TotalParcelas = 0;
$funcionario = 0;
$nome_funcionario = '';
$mdl_aca_ = returnPart($_SESSION["modulos"], 6);
$mdl_seg_ = returnPart($_SESSION["modulos"], 14);
if ($_GET['us'] != "") {
    $F1 = $_GET['us'];
}
if (is_numeric($_GET['tp'])) {
    $F3 = $_GET['tp'];
}
if (is_numeric($_GET['vl'])) {
    $TotalParcelas = $_GET['vl'];
}
if ($_GET['id'] != '') {
    $cur = odbc_exec($con, "select id_contas_movimento,id_grupo_contas from sf_contas_movimento cm inner join sf_grupo_contas gc on cm.grupo_conta = gc.id_grupo_contas where cm.descricao = '" . utf8_decode("COMISSÕES") . "' and gc.descricao = '" . utf8_decode("OBRIGAÇÕES TRABALHISTAS") . "'");
    while ($RFP = odbc_fetch_array($cur)) {
        $cm = $RFP['id_contas_movimento'];
        $gc = $RFP['id_grupo_contas'];
    }
    $cur = odbc_exec($con, "select id_tipo_documento from dbo.sf_tipo_documento where descricao = 'DINHEIRO'");
    while ($RFP = odbc_fetch_array($cur)) {
        $doc = $RFP['id_tipo_documento'];
    }
    if ($F3 == 1) {
        $query = "select id_fornecedores_despesas,razao_social from sf_fornecedores_despesas f where f.tipo = 'E' and f.id_fornecedores_despesas = '" . str_replace("'", "", $F1) . "'";
    } elseif ($F3 == 2) {
        if ($mdl_aca_ == 1 && $mdl_seg_ = 0) {
            $query = "select id_fornecedores_despesas,razao_social from sf_fornecedores_despesas f inner join sf_usuarios u on f.id_fornecedores_despesas = u.funcionario where f.tipo = 'E' and u.id_usuario = '" . str_replace("'", "", $F1) . "'";
        } else {
            $query = "select id_fornecedores_despesas,razao_social from sf_fornecedores_despesas f where f.tipo = 'I' and f.id_fornecedores_despesas = '" . str_replace("'", "", $F1) . "'";
        }
    } else {
        if ($mdl_aca_ == 1 && $mdl_seg_ = 0) {
            $query = "select id_fornecedores_despesas,razao_social from sf_fornecedores_despesas f inner join sf_usuarios u on f.id_fornecedores_despesas = u.funcionario where u.login_user = '" . str_replace("'", "", $F1) . "'";
        }
    }
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $funcionario = utf8_encode($RFP['id_fornecedores_despesas']);
        $nome_funcionario = utf8_encode($RFP['razao_social']);
    }
}

if (isset($_POST['bntSave'])) {
    if ($_POST['txtDestinatario'] == "null") {
        echo "<script>alert('Selecione um responsável para aprovação da comissão!');</script>";
    } else {
        $dtVencimento = valoresData("txtVencimento");
        $valorParcela = valoresNumericos("txtValor");
        if (is_numeric($valorParcela) && $valorParcela > 0 && $dtVencimento != "null" && $gc > 0 && $cm > 0 && $funcionario > 0 && $doc > 0) {
            $query = "set dateformat dmy;insert into sf_solicitacao_autorizacao(empresa,grupo_conta,conta_movimento,fornecedor_despesa,tipo_documento,historico,destinatario,comentarios,sys_login,data_lanc,status)values('001'," .
                    $gc . "," .
                    $cm . "," .
                    $funcionario . "," .
                    $doc . "," .
                    valoresTexto("txtHistorico") . "," .
                    valoresTexto("txtDestinatario") . "," .
                    valoresTexto("txtComentarios") . ",'SYSADM',GetDate(),'Aguarda')";
            odbc_exec($con, $query) or die(odbc_errormsg());
            $res = odbc_exec($con, "select top 1 id_solicitacao_autorizacao from sf_solicitacao_autorizacao order by id_solicitacao_autorizacao desc") or die(odbc_errormsg());
            $nn = odbc_result($res, 1);
            //----------------------------------------------Parcelas-----------------------------------------                                
            odbc_exec($con, "set dateformat dmy; insert into sf_solicitacao_autorizacao_parcelas(solicitacao_autorizacao,numero_parcela,data_parcela,sa_descricao,valor_parcela,historico_baixa,pa,bol_id_banco,bol_data_criacao,bol_valor,bol_juros,bol_multa,bol_nosso_numero,data_cadastro,tipo_documento) values (" .
                            $nn . ",1," . $dtVencimento . ",''," . $valorParcela . "," . valoresTexto("txtHistorico") . ",'01/01',null,null,null,null,null,null,GETDATE()," . $doc . ")") or die(odbc_errormsg());
            //----------------------------------------------Historico----------------------------------------
            $count = explode("|", $_GET['id']);
            if (count($count) > 0) {
                $i = 0;
                while ($i < count($count) - 1) {
                    if (is_numeric($count[$i])) {
                        odbc_exec($con, "INSERT sf_comissao_historico (hist_venda, hist_vendedor, hist_solicitacao_autorizacao, hist_parcela)
                        SELECT DISTINCT " . $count[$i] . ", '" . str_replace("'", "", $F1) . "', " . $nn . ", 0 FROM sf_comissao_historico cr WHERE NOT EXISTS (SELECT * FROM sf_comissao_historico c
                        WHERE hist_venda = " . $count[$i] . " and hist_vendedor = '" . str_replace("'", "", $F1) . "' and hist_solicitacao_autorizacao = " . $nn . " and hist_parcela = 0)") or die(odbc_errormsg());
                    }
                    $i++;
                }
            }
            echo "<script>parent.FecharBox();</script>";
        }
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Comissões</title>
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
    </head>
    <link rel="icon" type="image/ico" href="../../favicon.ico"/>
    <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
    <link href="../../css/main.css" rel="stylesheet"/>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>    
    <script type="text/javascript" src="../../js/graficos.js"></script>
    <script type="text/javascript" src="http://photonui.orangehilldev.com/js/plugins/justgage.1.0.1.min.js"></script>    
    <body style="overflow:hidden">
        <div style="width:400px; margin:0; padding:0;">
            <form name="frmForm" action="FormComissao.php<?php echo "?tp=" . $_GET['tp'] . "&id=" . $_GET['id'] . "&us=" . $_GET['us'];?>" method="POST">
                <div style="float: left; width: 100%; margin-top: 10px;">
                    <div style="float: left;width: 29%; text-align: right;margin-top: 5px;">
                        Destinatário:
                    </div>
                    <div style="float: left;width: 60%; margin-left: 1%;">
                        <select name="txtDestinatario" id="txtDestinatario" style="width: 220px;" class="input-medium inputCenter">
                            <option value="null">--Selecione--</option>
                            <?php
                            $cur = odbc_exec($con, "select login_user,nome from sf_usuarios where inativo = 0 and login_user != 'Admin' order by nome") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP['login_user'] ?>"><?php echo utf8_encode($RFP['nome']) ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div style="float: left;width: 10%">
                        <button style="border:none; float:right; width:25px; margin-right:10px; color:#FFFFFF; background-color:#333333" onClick="parent.FecharBox();" id="bntOK"><b>x</b></button>
                    </div>
                </div>
                <div style="float: left; width: 100%; margin-top: 5px;">
                    <div style="float: left;width: 29%; text-align: right; margin-top: 5px;">
                        Vencimento:
                    </div>
                    <div style="float: left;width: 60%; margin-left: 1%;">
                        <input type="text" name="txtVencimento" id="txtVencimento" class="datepicker input-medium inputCenter" style="width:100px" value="<?php echo $dataVenc; ?>"/>
                    </div>
                </div>
                <div style="float: left; width: 100%; margin-top: 5px;">
                    <div style="float: left;width: 29%; text-align: right; margin-top: 5px;">
                        Valor:
                    </div>
                    <div style="float: left;width: 60%; margin-left: 1%;">
                        <input type="text" name="txtValor" id="txtValor" class="input-medium inputCenter" style="width:100px" value="<?php echo escreverNumero($TotalParcelas); ?>"/>
                    </div>
                </div>
                <div style="float: left; width: 100%; margin-top: 5px;">
                    <div style="float: left;width: 29%; text-align: right; margin-top: 5px;">
                        Histórico:
                    </div>
                    <div style="float: left;width: 60%; margin-left: 1%;">
                        <input type="text" name="txtHistorico" id="txtHistorico" class="input-medium" style="width:220px" value="Comissão"/>
                    </div>
                </div>
                <div style="float: left; width: 100%; margin-top: 5px; margin-bottom: 10px;">
                    <div style="float: left;width: 29%; text-align: right; margin-top: 5px;">
                        Observação:
                    </div>
                    <div style="float: left;width: 60%; margin-left: 1%;">
                        <textarea name="txtComentarios" id="txtComentarios" style="width:220px" cols="5" rows="3"></textarea>
                    </div>
                </div>                            
                <div class="toolbar bottom tar" style="margin:10px; border-top:solid 1px #CCC; overflow:hidden">
                    <div class="data-fluid" style="overflow:hidden; padding-bottom:10px">
                        <div style="float: left;text-align: left">
                            Grupo:<strong><?php
                                if ($gc > 0) {
                                    echo " OBRIGAÇÕES TRABALHISTAS";
                                } else {
                                    echo " Não encontrado";
                                }
                                ?></strong><br/>
                            Conta:<strong><?php
                                if ($cm > 0) {
                                    echo " COMISSÕES";
                                } else {
                                    echo " Não encontrada";
                                }
                                ?></strong><br/>
                            Documento:<strong><?php
                                if ($doc > 0) {
                                    echo " DINHEIRO";
                                } else {
                                    echo " Não encontrado";
                                }
                                ?></strong><br/>
                            Funcionário:<strong><?php
                                if ($funcionario > 0) {
                                    echo $nome_funcionario;
                                } else {
                                    echo " Não encontrado";
                                }
                                ?></strong><br/>
                        </div>
                        <div class="btn-group">
                            <button type="submit" name="bntSave" id="bntOK" class="btn btn-success" style="margin-top:20px"><span class="ico-refresh"> </span> Gerar</button>                                                       
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <script type="text/javascript" src="../../fancyapps/source/jquery.fancybox.js?v=2.1.4"></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            
            $("#txtVencimento").mask(lang["dateMask"]);            
            $("#txtValor").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
            
        </script>
        <?php odbc_close($con); ?>                
    </body>
</html>