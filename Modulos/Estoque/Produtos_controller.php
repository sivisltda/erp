<?php

include "../../Connections/configini.php";

if (isset($_GET['getInventario'])) {
    $iTotalInventario = 0;
    $sWhereX = "";
    if (is_numeric($_GET['fb'])) {
        $sWhereX .= " and cod_fabricante = " . $_GET['fb'];
    }
    if (is_numeric($_GET['gp'])) {
        $sWhereX .= " and p.conta_movimento = " . $_GET['gp'];
    }
    if (is_numeric($_GET['sentrada'])) {
        $sWhereX .= " and sem_entrada = " . $_GET['sentrada'];
    }
    if (is_numeric($_GET['ativo'])) {
        $sWhereX .= " and p.inativa = " . $_GET['ativo'];
    }
    if (is_numeric($_GET['empresa'])) {
        $sEmpresa .= " and v.empresa = " . $_GET['empresa'];
    }

    $sQuery = "select sum(isnull(case when x.preco_venda > 0 and x.sem_entrada = 0 and x.total > 0 then x.total * x.preco_venda end,0)) total
                from (select sem_entrada,preco_venda,case when sem_entrada = 1 then 0 else (select isnull(SUM(case when cov = 'V' then case when quantidademp is null then quantidade else quantidade * quantidademp end*-1 else quantidade end ),0) 
                from sf_vendas_itens vi inner join sf_vendas v on vi.id_venda = v.id_venda left join sf_produtos_materiaprima on id_produtomp = produto where cov in ('V','C') and status = 'Aprovado' " . $sEmpresa . " and (vi.produto = p.conta_produto or id_materiaprima = p.conta_produto))end total
                from sf_produtos p left join sf_contas_movimento on p.conta_movimento = sf_contas_movimento.id_contas_movimento 
                left join sf_fabricantes on id_fabricante = cod_fabricante where p.tipo = 'P' $sWhereX) as x";
    $local[] = array('valor' => $iTotalInventario);
    echo(json_encode($local));
}

if (isset($_GET['buscaCodigoBarra'])) {
    $sQuery = "select codigo_barra, descricao from dbo.sf_produtos where codigo_barra = '" . $_GET['buscaCodigoBarra'] . "'";
    $cur = odbc_exec($con, $sQuery);

    while ($row = odbc_fetch_array($cur)) {
        $local[] = array('descricao' => $row['descricao']);
    }
    echo(json_encode($local));
}
odbc_close($con);
