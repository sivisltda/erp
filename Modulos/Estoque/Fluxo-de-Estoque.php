<?php
include '../../Connections/configini.php';

$imprimir = 0;
$empresa = "";
$conta_produto_item = "";
$FinalUrl = "";
$tipo = "";

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = $_GET['imp'];
}

if (is_numeric($_GET['id'])) {
    $conta_produto = $_GET['id'];
}

if (is_numeric($_GET['pd'])) {
    $conta_produto_item = $_GET['pd'];
}

if (is_numeric($_GET['empresa'])) {
    $empresa = $_GET['empresa'];
}

if (is_numeric($_GET['tipo'])) {
    if ($_GET['tipo'] == 1) {
        $tipo = "C";
    } else if ($_GET['tipo'] == 2) {
        $tipo = "V";
    }
}

if ($_GET['dtIni'] != '') {
    $dt_begin = str_replace("_", "/", $_GET['dtIni']);
} else {
    $dt_begin = getData("B");
}

if ($_GET['dtFim'] != '') {
    $dt_end = str_replace("_", "/", $_GET['dtFim']);
} else {
    $dt_end = getData("T");
}

if ($_GET['descr']) {
    $descrProd = $_GET['descr'];
}

$idCliente = "";
$FinalUrl = "?imp=" . $imprimir . "&id=" . $conta_produto . "&pd=" . $conta_produto_item . "&dtIni=" . $dt_begin . "&dtFim=" . $dt_end . "&filial=" . $idFilial . "&tipo=" . $tipo . "&descr=" . $descrProd;
if (is_numeric($_GET['idCli'])) {
    $FinalUrl .= "&idCli=" . $_GET['idCli'];
    $cur = odbc_exec($con, "select id_fornecedores_despesas,tipo,razao_social,nome_fantasia from dbo.sf_fornecedores_despesas where id_fornecedores_despesas = " . $_GET['idCli']) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $nmCliente = $RFP['razao_social'];
        $idCliente = $_GET['idCli'];
        if ($RFP['nome_fantasia'] != "") {
            $nmCliente = $nmCliente . " (" . $RFP['nome_fantasia'] . ")";
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../SpryAssets/SpryValidationTextField.css"/>
        <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
        <style>
            .fancybox-wrap, .fancybox-skin, .fancybox-outer, .fancybox-inner, .fancybox-image,  .fancybox-wrap object, .fancybox-nav, .fancybox-nav span, .fancybox-tmp {
                padding: 0;
                margin: 0;
                border: 0;
                outline: none;
                vertical-align: top;

                background:url(../../img/fundosForms/formBancos.PNG);
            }
            #formulario {
                float: left;
                width: 100%;
            }
            #formulario form .linha {
                float: left;
                height: 26px;
                width: 100%;
                display: block;
                border-bottom-width: 1px;
                border-bottom-style: solid;
                border-bottom-color: #f7f7f7;
                background-color: #f7f7f7;
                border-top-width: 1px;
                border-top-style: solid;
                border-top-color: #FFF;
                padding-top: 10px;
                padding-bottom: 10px;
            }
        </style>
    </head>
    <body>
        <form action="Fluxo-de-Estoque.php<?php echo $FinalUrl; ?>" method="POST">
            <div id="loader"><img src="../../img/loader.gif"/></div>
            <div class="wrapper">
                <?php
                if ($imprimir == 0) {
                    include('../../menuLateral.php');
                }
                ?>
                <div class="body">
                    <?php
                    if ($imprimir == 0) {
                        include("../../top.php");
                    }
                    ?>
                    <div class="content">
                        <?php if ($imprimir == 0) { ?>
                            <div class="page-header">
                                <div class="icon"> <span class="ico-arrow-right"></span> </div>
                                <h1>Estoque<small>Fluxo de Estoque</small></h1>
                            </div>
                        <?php } ?>
                        <div class="row-fluid">
                            <?php if ($imprimir == 0) { ?>
                                <div class="span12">
                                    <div class="boxfilter block">
                                        <div style="width:6%; float:left">
                                            <span>Loja:</span>
                                            <select name="txtEmpresa" id="txtEmpresa">
                                                <?php
                                                $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                                while ($RFP = odbc_fetch_array($cur)) {
                                                    ?>
                                                    <option value="<?php echo utf8_encode($RFP['id_filial']); ?>" <?php
                                                    if ($empresa == "") {
                                                        $empresa = $RFP['id_filial'];
                                                    }
                                                    if ($empresa == $RFP['id_filial']) {
                                                        echo "SELECTED";
                                                    }
                                                    ?>><?php echo utf8_encode($RFP['numero_filial']); ?></option>
                                                        <?php } ?>
                                            </select>
                                        </div>
                                        <div style="width:8%; float:left;margin-left: 1%">
                                            <span> Tipo: </span>
                                            <select name="txtTipo" id="txtTipo">
                                                <option value="0" <?php
                                                if ($tipo == "") {
                                                    echo "SELECTED";
                                                }
                                                ?>  >Todos</option>
                                                <option value="1" <?php
                                                if ($tipo == "C") {
                                                    echo "SELECTED";
                                                }
                                                ?> >Compra</option>
                                                <option value="2" <?php
                                                if ($tipo == "V") {
                                                    echo "SELECTED";
                                                }
                                                ?> >Venda</option>
                                            </select>
                                        </div>
                                        <div style="width:16%; float:left;margin-left: 1%">
                                            <span> Grupo: </span>
                                            <select  name="txtGrupo" id="txtGrupo" class="select" style="height: 31px;width: 100%">
                                                <option value="0" <?php
                                                if ($conta_produto == 0) {
                                                    echo "SELECTED";
                                                }
                                                ?>>--Todos--</option>
                                                        <?php
                                                        $cur = odbc_exec($con, "select id_contas_movimento,descricao from sf_contas_movimento where tipo = 'P' order by descricao") or die(odbc_errormsg());
                                                        while ($RFP = odbc_fetch_array($cur)) {
                                                            ?>
                                                    <option value="<?php echo $RFP['id_contas_movimento'] ?>"
                                                    <?php
                                                    if (!(strcmp($RFP['id_contas_movimento'], $conta_produto))) {
                                                        echo "SELECTED";
                                                    }
                                                    ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                        <?php } ?>
                                            </select>
                                        </div>
                                        <div style="width:18%; float:left;margin-left: 1%">
                                            <span>Produto: </span>
                                            <input type="hidden" id="txtCodProd" value="<?php echo $conta_produto_item; ?>"/>
                                            <span id="carregando" name="carregando" style="color:#666;display:none;line-height: 26px;">Aguarde, carregando...</span>
                                            <span id="spryselect1">
                                                <select name="txtProduto" id="txtProduto" style="width: 100%" class="select">
                                                    <option value="0">--Selecione--</option>
                                                </select>
                                            </span>
                                        </div>
                                        <div style="width:18%; float:left;margin-left: 1%">
                                            <span>Cliente: </span>
                                            <input id="txtIdCliente" name="txtIdCliente" type="hidden" value="<?php echo $idCliente; ?>"/>
                                            <input type="text" name="txtCliente" id="txtCliente" class="inputbox" value="<?php echo $nmCliente; ?>" size="10" style="vertical-align:top;width:100%; color:#000 !important"/>                                        
                                        </div>
                                        <div style="width:8%; float:left;margin-left: 1%">
                                            <span> Data Inicial: </span>
                                            <input type="text" style="width:100%" id="txt_dt_begin" class="datepicker inputCenter" name="txt_dt_begin" value="<?php echo $dt_begin; ?>" placeholder="Data inicial">
                                        </div>
                                        <div style="width:8%; float:left;margin-left: 1%">
                                            <span> Data Final: </span>
                                            <input type="text" style="width:100%" class="datepicker inputCenter" id="txt_dt_end" name="txt_dt_end" value="<?php echo $dt_end; ?>" placeholder="Data Final">
                                        </div>
                                        <div style="margin-top: 15px;float:left;margin-left: 0.5%;">
                                            <div style="float:left">
                                                <button name="btnfind" class="button button-turquoise btn-primary" type="button" title="Buscar" onclick="MontaUrl(0);"><span class="ico-search icon-white"></span></button>
                                                <button name="btnPrint" class="button button-blue btn-primary" type="button" title="Imprimir" onclick="MontaUrl(1);"><span class="ico-print"></span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="block">
                                        <?php
                                        if ($imprimir == 1) {
                                            $titulo_pagina = "FLUXO DE ESTOQUE<br/>" . utf8_encode($descrProd);
                                            include "../Financeiro/Cabecalho-Impressao.php";
                                        } else {
                                            ?>
                                            <div class="boxhead">
                                                <div class="boxtext">Fluxo de Estoque</div>
                                            </div>
                                        <?php } ?>
                                        <div class="data-fluid"> 
                                            <table <?php
                                            if ($imprimir == 0) {
                                                echo "class=\"table\"";
                                            } else {
                                                echo "border=\"1\"";
                                            }
                                            ?> cellpadding="0" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th width="5%" style="text-align: center;">Data</th>
                                                        <th width="5%" style="text-align: center;">Loja</th>
                                                        <th width="26%">Cliente/Fornecedor</th>
                                                        <th width="16%">Produto</th>
                                                        <th width="10%">Histórico</th>
                                                        <th width="5%">Quantidade</th>
                                                        <?php if ($conta_produto_item !== "0" && $conta_produto_item !== "") { ?>
                                                            <th width="7%">Saldo</th>
                                                        <?php } ?>
                                                        <th width="8%" style="text-align: right;">Crédito</th>
                                                        <th width="8%" style="text-align: right;">Débito</th>
                                                        <?php if ($conta_produto_item !== "0" && $conta_produto_item !== "") { ?>
                                                            <th width="10%">Saldo</th>
                                                        <?php } ?>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $comando = '0';
                                                    if (is_numeric($conta_produto_item)) {
                                                        $comando = $conta_produto_item;
                                                    }

                                                    $totalCredito = 0;
                                                    $totalDebito = 0;
                                                    $qtdCredito = 0;
                                                    $qtdDebito = 0;
                                                    $cur = odbc_exec($con, "set dateformat dmy; select sum(total) total, sum(qtd) qtd from (
                                                                            select isnull(sum(case when n_produtos = 1 then 0 else valor_total end),0) total, isnull(sum(quantidade),0) qtd
                                                                                from sf_vendas_itens vi inner join sf_vendas v on vi.id_venda = v.id_venda
                                                                                inner join sf_produtos p on vi.produto = p.conta_produto
                                                                                inner join sf_contas_movimento cm on cm.id_contas_movimento = p.conta_movimento
                                                                                where ((cov = 'V' and status = 'Aprovado')  or
                                                                                (cov in('O','P') and validade >= cast(GETDATE() as date) and reservar_estoque = 1)) and
                                                                                data_venda < " . valoresDataHora2($dt_begin, "00:00:00") . " and v.empresa = " . $empresa . " and status = 'Aprovado' and p.tipo = 'P' and produto = " . $comando . "
                                                                             union all
                                                                             select 0 total, isnull(SUM(quantidade),0) qtd from sf_lancamento_movimento_itens li
                                                                                inner join sf_lancamento_movimento lm on lm.id_lancamento_movimento = li.id_lancamento_movimento
                                                                                where lm.status = 'Aprovado' and dt_inclusao < " . valoresDataHora2($dt_begin, "00:00:00") . " and lm.empresa = " . $empresa . ") as result");

                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                        $totalCredito = $RFP['total'];
                                                        $qtdCredito = $RFP['qtd'];
                                                    }

                                                    $cur = odbc_exec($con, "set dateformat dmy; select sum(total) total, sum(qtd) qtd from (
                                                                            select sum(valor_total) total, sum(quantidade) qtd
                                                                                from sf_vendas_itens vi inner join sf_vendas v on vi.id_venda = v.id_venda
                                                                                inner join sf_produtos p on vi.produto = p.conta_produto
                                                                                inner join sf_contas_movimento cm on cm.id_contas_movimento = p.conta_movimento
                                                                                where cov = 'C' and data_venda < " . valoresDataHora2($dt_begin, "00:00:00") . " and v.empresa = " . $empresa . " and status = 'Aprovado' and p.tipo = 'P' and produto = " . $comando . "
                                                                            union all
                                                                            select 0 total, isnull(SUM(quantidade),0) qtd from sf_lancamento_movimento_itens li
                                                                                inner join sf_lancamento_movimento lm on lm.id_lancamento_movimento = li.id_lancamento_movimento
                                                                                where lm.status = 'Aprovado' and dt_devolucao < " . valoresDataHora2($dt_begin, "00:00:00") . " and lm.empresa = " . $empresa . ") as result");
                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                        $totalDebito = $RFP['total'];
                                                        $qtdDebito = $RFP['qtd'];
                                                    }
                                                    ?>               
                                                    <tr>
                                                        <td></td>                                                        
                                                        <td></td>                                                        
                                                        <td></td>
                                                        <td></td>
                                                        <td style="color:#0066cc" colspan="2">=> SALDO ANTERIOR</td>
                                                        <?php if ($conta_produto_item !== "0" && $conta_produto_item !== "") { ?>
                                                            <td style="color:#0066cc; text-align:center"><div id="formPQ"><?php echo escreverNumero($qtdDebito - $qtdCredito, 0, 4); ?></td>
                                                        <?php } ?>
                                                        <td style="color:#0066cc; text-align:right"><div id="formPQ"><?php echo escreverNumero($totalCredito, 1); ?></div></td>
                                                        <td style="color:#0066cc; text-align:right"><div id="formPQ"><?php echo escreverNumero($totalDebito, 1); ?></div></td>
                                                        <?php if ($conta_produto_item !== "0" && $conta_produto_item !== "") { ?>
                                                            <td style="color:#0066cc; text-align:right; padding-top:5px"><div id="formPQ"><?php echo escreverNumero($totalCredito - $totalDebito, 1); ?></div></td>
                                                        <?php } ?>
                                                    </tr>                        
                                                    <?php
                                                    $whereTp = "'C','V'";
                                                    $whereTp2 = "'O','P'";
                                                    if ($tipo !== "") {
                                                        $whereTp = "'" . $tipo . "'";
                                                        $whereTp2 = ($tipo === "C") ? "'C'" : "'O','P'";
                                                    }
                                                    $cliLM = "";
                                                    $cliVd = "";
                                                    $Qproduto = "";
                                                    if ($idCliente !== "") {
                                                        $cliLM = " and lm.fornecedor_despesa = " . $idCliente;
                                                        $cliVd = " and v.cliente_venda = " . $idCliente;
                                                    }

                                                    if ($comando !== '0') {
                                                        $Qproduto = " and produto = " . $comando;
                                                    }
                                                    $QueryLM = "union all
                                                                select dt_inclusao, sf_filiais.numero_filial, razao_social, u.nome, cm.descricao dc, p.descricao dp, quantidade, valor_total, 'CONTAS FIXAS', 'LI'
                                                                from sf_lancamento_movimento_itens li
                                                                inner join sf_lancamento_movimento lm on lm.id_lancamento_movimento = li.id_lancamento_movimento
                                                                inner join sf_produtos p on li.produto = p.conta_produto
                                                                inner join sf_contas_movimento cm on cm.id_contas_movimento = p.conta_movimento
                                                                inner join sf_fornecedores_despesas fd on lm.fornecedor_despesa = fd.id_fornecedores_despesas
                                                                left join sf_filiais on sf_filiais.id_filial = lm.empresa
                                                                left join sf_usuarios u on li.id_usuario_inclusao = u.id_usuario
                                                                where lm.status = 'Aprovado' and dt_inclusao between " . valoresDataHora2($dt_begin, "00:00:00") . " and " . valoresDataHora2($dt_end, "23:59:59") . " and lm.empresa =  " . $empresa . $Qproduto . $cliLM . "
                                                                union all
                                                                select dt_devolucao, sf_filiais.numero_filial, razao_social, u.nome, cm.descricao dc, p.descricao dp, quantidade, valor_total, 'CONTAS FIXAS', 'LD'
                                                                from sf_lancamento_movimento_itens li
                                                                inner join sf_lancamento_movimento lm on lm.id_lancamento_movimento = li.id_lancamento_movimento
                                                                inner join sf_produtos p on li.produto = p.conta_produto
                                                                inner join sf_contas_movimento cm on cm.id_contas_movimento = p.conta_movimento
                                                                inner join sf_fornecedores_despesas fd on lm.fornecedor_despesa = fd.id_fornecedores_despesas
                                                                left join sf_filiais on sf_filiais.id_filial = lm.empresa
                                                                left join sf_usuarios u on li.id_usuario_inclusao = u.id_usuario
                                                                where lm.status = 'Aprovado' and dt_devolucao between " . valoresDataHora2($dt_begin, "00:00:00") . " and " . valoresDataHora2($dt_end, "23:59:59") . " and lm.empresa =  " . $empresa . $Qproduto . $cliLM;

                                                    if ($tipo !== "") {
                                                        if ($tipo == "V") {
                                                            $QueryLM = "union all
                                                                    select dt_inclusao, sf_filiais.numero_filial, razao_social, u.nome, cm.descricao dc, p.descricao dp, quantidade, valor_total, 'CONTAS FIXAS', 'LI'
                                                                    from sf_lancamento_movimento_itens li
                                                                    inner join sf_lancamento_movimento lm on lm.id_lancamento_movimento = li.id_lancamento_movimento
                                                                    inner join sf_produtos p on li.produto = p.conta_produto
                                                                    inner join sf_contas_movimento cm on cm.id_contas_movimento = p.conta_movimento
                                                                    inner join sf_fornecedores_despesas fd on lm.fornecedor_despesa = fd.id_fornecedores_despesas
                                                                    left join sf_filiais on sf_filiais.id_filial = lm.empresa
                                                                    left join sf_usuarios u on li.id_usuario_inclusao = u.id_usuario
                                                                    where lm.status = 'Aprovado' and dt_inclusao between " . valoresDataHora2($dt_begin, "00:00:00") . " and " . valoresDataHora2($dt_end, "23:59:59") . " and lm.empresa =  " . $empresa . $Qproduto . $cliLM;
                                                        } else {
                                                            $QueryLM = "union all
                                                                    select dt_devolucao, sf_filiais.numero_filial, razao_social, u.nome, cm.descricao dc, p.descricao dp, quantidade, valor_total, 'CONTAS FIXAS', 'LD'
                                                                    from sf_lancamento_movimento_itens li
                                                                    inner join sf_lancamento_movimento lm on lm.id_lancamento_movimento = li.id_lancamento_movimento
                                                                    inner join sf_produtos p on li.produto = p.conta_produto
                                                                    inner join sf_contas_movimento cm on cm.id_contas_movimento = p.conta_movimento
                                                                    inner join sf_fornecedores_despesas fd on lm.fornecedor_despesa = fd.id_fornecedores_despesas
                                                                    left join sf_filiais on sf_filiais.id_filial = lm.empresa
                                                                    left join sf_usuarios u on li.id_usuario_inclusao = u.id_usuario
                                                                    where lm.status = 'Aprovado' and dt_devolucao between " . valoresDataHora2($dt_begin, "00:00:00") . " and " . valoresDataHora2($dt_end, "23:59:59") . " and lm.empresa =  " . $empresa . $Qproduto . $cliLM;
                                                        }
                                                    }

                                                    $query = "set dateformat dmy; select * from (
                                                                select data_venda,numero_filial empresa,UPPER(sf.razao_social) razao_social, vendedor,cm.descricao dc,p.descricao dp,quantidade,valor_total, historico_venda + ' - ' + CAST(V.id_venda AS varchar(50)) historico_venda, cov
                                                                from sf_vendas_itens vi inner join sf_vendas v on vi.id_venda = v.id_venda
                                                                inner join sf_produtos p on vi.produto = p.conta_produto
                                                                inner join sf_contas_movimento cm on cm.id_contas_movimento = p.conta_movimento
                                                                left join sf_filiais on sf_filiais.id_filial = v.empresa
                                                                inner join sf_fornecedores_despesas sf on v.cliente_venda = sf.id_fornecedores_despesas
                                                                where ((cov in(" . $whereTp . ") and status = 'Aprovado') or
                                                                       (cov in(" . $whereTp2 . ") and validade >= cast(GETDATE() as date) and reservar_estoque = 1)) and p.tipo = 'P' and data_venda
                                                                    between " . valoresDataHora2($dt_begin, "00:00:00") . " and " . valoresDataHora2($dt_end, "23:59:59") . " and v.empresa = " . $empresa . $Qproduto . $cliVd . $QueryLM . ") as x
                                                                order by x.data_venda desc";
                                                    //echo $query; exit;
                                                    $cur = odbc_exec($con, $query);
                                                    $SubTotal = $totalCredito - $totalDebito;
                                                    $sub_qtd = $qtdDebito - $qtdCredito;
                                                    $totalCredito = 0;
                                                    $totalDebito = 0;
                                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                                        <tr>
                                                            <td style="text-align:center"><div id="formPQ"><?php echo escreverData($RFP['data_venda']); ?></div></td>                                                        
                                                            <td style="text-align:center"><div id="formPQ"><?php echo utf8_encode($RFP['empresa']); ?></div></td>                                                        
                                                            <td title="<?php echo utf8_encode($RFP['razao_social']); ?>"><div id="formPQ"><?php echo utf8_encode($RFP['razao_social']); ?></div></td>
                                                            <td><div id="formPQ"><?php echo utf8_encode($RFP['dp']); ?></div> </td>
                                                            <td title="<?php
                                                            if (utf8_encode($RFP['cov']) == "LI") {
                                                                echo "CONTAS FIXAS - SAIDA";
                                                            } else if (utf8_encode($RFP['cov']) == "LD") {
                                                                echo "CONTAS FIXAS - ENTRADA";
                                                            } else {
                                                                echo utf8_encode($RFP['historico_venda']);
                                                            }
                                                            ?>"><div id="formPQ"><?php
                                                                if (utf8_encode($RFP['cov']) == "LI") {
                                                                    echo "CONTAS FIXAS - SAIDA";
                                                                } else if (utf8_encode($RFP['cov']) == "LD") {
                                                                    echo "CONTAS FIXAS - ENTRADA";
                                                                } else {
                                                                    echo utf8_encode($RFP['historico_venda']);
                                                                }
                                                                ?></div></td>                                                        
                                                            <td><div id="formPQ"><center><?php
                                                                if ($RFP['cov'] === "C" || utf8_encode($RFP['cov']) === "LD") {
                                                                    echo "+ ";
                                                                } else {
                                                                    echo "- ";
                                                                } echo escreverNumero($RFP['quantidade'], 0, 4);
                                                                ?></center></div></td>
                                                            <?php if ($conta_produto_item !== "0" && $conta_produto_item !== "") { ?>
                                                                <td><div id="formPQ"><center><?php
                                                                    if ($RFP['cov'] === "C" || utf8_encode($RFP['cov']) === "LD") {
                                                                        $sub_qtd = $sub_qtd + utf8_encode($RFP['quantidade']);
                                                                    } else {
                                                                        $sub_qtd = $sub_qtd - utf8_encode($RFP['quantidade']);
                                                                    }
                                                                    echo escreverNumero($sub_qtd, 0, 4);
                                                                    ?></center></div></td>
                                                            <?php } ?>
                                                            <td style="text-align:right"><div id="formPQ"><?php
                                                                if ($RFP['cov'] === "C" || utf8_encode($RFP['cov']) === "LI") {
                                                                    echo escreverNumero(0, 1);
                                                                } else {
                                                                    echo escreverNumero($RFP['valor_total'], 1);
                                                                    $totalCredito += $RFP['valor_total'];
                                                                }
                                                                ?></div></td>
                                                            <td style="text-align:right"><div id="formPQ"><?php
                                                                if ($RFP['cov'] === "C" || utf8_encode($RFP['cov']) === "LI") {
                                                                    echo escreverNumero($RFP['valor_total'], 1);
                                                                    $totalDebito += $RFP['valor_total'];
                                                                } else {
                                                                    echo escreverNumero(0, 1);
                                                                }
                                                                ?></div></td>
                                                            <?php if ($conta_produto_item !== "0" && $conta_produto_item !== "") { ?>
                                                                <td style="text-align:right; padding-top:5px"><div id="formPQ"><?php
                                                                    if ($RFP['cov'] === "V") {
                                                                        $SubTotal = $SubTotal + $RFP['valor_total'];
                                                                    } else {
                                                                        $SubTotal = $SubTotal - $RFP['valor_total'];
                                                                    }echo escreverNumero($SubTotal, 1);
                                                                    ?></div></td>
                                                            <?php } ?>
                                                        </tr>         
                                                    <?php } ?>
                                                    <tr>
                                                        <td></td>                                                        
                                                        <td></td>                                                        
                                                        <td></td>
                                                        <td></td>
                                                        <td style="color:#0066cc" colspan="2">=> SALDO ATUAL</td>
                                                        <?php if ($conta_produto_item !== "0" && $conta_produto_item !== "") { ?>
                                                            <td style="color:#0066cc; text-align:center"><b><div id="formPQ"><?php echo escreverNumero($sub_qtd, 0, 4); ?></div></b></td>
                                                        <?php } ?>
                                                        <td style="color:#0066cc; text-align:right"><b><div id="formPQ"><?php echo escreverNumero($totalCredito, 1); ?></div></b></td>
                                                        <td style="color:#0066cc; text-align:right"><b><div id="formPQ"><?php echo escreverNumero($totalDebito, 1); ?></div></b></td>
                                                        <?php if ($conta_produto_item !== "0" && $conta_produto_item !== "") { ?>
                                                            <td style="color:#0066cc; text-align:right; padding-top:5px"><b><div id="formPQ"><?php echo escreverNumero($totalCredito - $totalDebito, 1); ?></div></b></td>
                                                        <?php } ?>
                                                    </tr>                        
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/ckeditor/ckeditor.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/jcrop/jquery.Jcrop.min.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>         
        <script type="text/javascript">
            
            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
            
            function MontaUrl(tp) {
                var finalURL = tp === 1 ? '?imp=1' : '?imp=0';
                if ($('#txtCliente').val() !== "" && $('#txtIdCliente').val() !== "") {
                    finalURL = finalURL + "&idCli=" + $('#txtIdCliente').val();
                }
                if ($('#txtGrupo').val() >= 0) {
                    finalURL = finalURL + "&id=" + $('#txtGrupo').val();
                }
                if ($('#txtProduto').val() >= 0) {
                    finalURL = finalURL + "&pd=" + $('#txtProduto').val();
                }
                if ($('#txtEmpresa').val() >= 0) {
                    finalURL = finalURL + "&empresa=" + $('#txtEmpresa').val();
                }
                if ($('#txtTipo').val() >= 0) {
                    finalURL = finalURL + "&tipo=" + $('#txtTipo').val();
                }
                if ($('#txt_dt_begin').val() !== 0) {
                    finalURL = finalURL + "&dtIni=" + $("#txt_dt_begin").val();
                }
                if ($('#txt_dt_end').val() !== 0) {
                    finalURL = finalURL + "&dtFim=" + $("#txt_dt_end").val();
                }
                if ($('#txtProduto').select2('data').text !== "") {
                    finalURL = finalURL + "&descr=" + $("#txtProduto").select2('data').text;
                }
                window.location = 'Fluxo-de-Estoque.php' + finalURL.replace(/\//g, "_");
            }

            $('#txtGrupo').change(function () {
                carregaProdutos($('#txtGrupo').val());
            });

            function carregaProdutos(grupo) {
                $("#txtProduto").html('<option value="">--Selecione--</option>');
                if (grupo) {
                    $('#txtProduto').select2("close").parent().hide();
                    $('#carregando').show();
                    $.getJSON('conta_p.ajax.php?search=', {txtGrupo: grupo, ajax: 'true'}, function (j) {
                        var options = '<option value="null">--Selecione--</option>';
                        for (var i = 0; i < j.length; i++) {
                            options += '<option value="' + j[i].id_contas_movimento + '">' + j[i].descricao + '</option>';
                        }
                        $('#txtProduto').html(options);
                        $('#carregando').hide();
                        $('#txtProduto').select2("close").parent().show();
                        if ($("#txtCodProd").val() !== "") {
                            $("#txtProduto").select2("val", $("#txtCodProd").val());
                        }
                    });
                } else {
                    $('#txtProduto').html('<option value="">--Selecione--</option>');
                }
                $("#txtProduto").select2("val", "");
            }
            $(function () {
                $("#txtCliente").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "./../CRM/ajax.php",
                            dataType: "json",
                            data: {
                                q: request.term,
                                t: 'C'
                            },
                            success: function (data) {
                                response(data);
                            }
                        });
                    },
                    minLength: 3,
                    select: function (event, ui) {
                        $("#txtIdCliente").val(ui.item.id);
                    }
                });
            });

            $(window).load(function () {
                if ($('#txtGrupo').val() !== "0") {
                    carregaProdutos($('#txtGrupo').val());
                }
<?php if ($imprimir == 1) { ?>
                $(".body").css("margin-left", 0);
                window.print();
                window.history.back();
<?php } ?>
        });
        </script>
        <style type="text/css">
            .fancybox-custom .fancybox-skin {
                box-shadow: 0 0 50px #069;
            }
        </style>        
        <?php odbc_close($con); ?>
    </body>
</html>