<?php
include "../../Connections/configini.php";
$imprimir = 0;
$ativo = 0;
if (is_numeric($_GET["Del"])) {
    odbc_exec($con, "DELETE FROM sf_produtos WHERE conta_produto = " . $_GET["Del"]);
    if (odbc_error()) {
        echo "<script>alert('Não é possível excluir um serviço que esteja atrelado à uma venda.');</script>";
    }
}
if (is_numeric($_GET["imp"]) && $_GET["imp"] > 0) {
    $imprimir = 1;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <style>            
            #example td {
                padding: 5px;
                line-height: 16px;
            }
            .select2-choice { height: 29px !important; }
            .select2-choice span { font-size:13px; line-height: 29px !important; }
        </style>
    </head>
    <body>
        <?php if ($imprimir == 0) { ?>
            <div id="loader"><img src="../../img/loader.gif"/></div>
        <?php } ?>
        <div class="wrapper">
            <?php
            if ($imprimir == 0) {
                include("../../menuLateral.php");
            }
            ?>
            <div class="body">
                <?php
                if ($imprimir == 0) {
                    include("../../top.php");
                }
                ?>
                <div class="content">
                    <?php
                    if ($imprimir !== 0) {
                        $visible = "hidden";
                    }
                    ?>
                    <div class="page-header" <?php echo $visible; ?>>
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Tabela de Veículos<small>Serviços</small></h1>
                    </div>
                    <div class="row-fluid" style="display:none">
                        <div class="span12">
                            <div class="block">
                                <button id="btnNovo" class="button button-green btn-primary" type="button" onclick="AbrirBox(0, 0)"><span class="ico-file-4 icon-white"></span></button>
                                <button id="btnPrint" class="button button-blue btn-primary" type="button" onClick="AbrirBox(1, 1)"><span class="ico-print icon-white"></span></button>
                                <input id="imprimir" name="imprimir" type="hidden" value="<?php echo $_GET["imp"]; ?>"/>
                                <select name="txtAtivo" id="txtAtivo" class="select input-medium" style="width:150px; height:31px">
                                    <option value="0" <?php
                                    if ($ativo == "0") {
                                        echo "SELECTED";
                                    }
                                    ?>>Ativos</option>
                                    <option value="1" <?php
                                    if ($ativo == "1") {
                                        echo "SELECTED";
                                    }
                                    ?>>Inativos</option>
                                </select>
                                <select name="txtGrupo" id="txtGrupo" class="select input-medium" style="width:250px; height:31px">
                                    <option value="null">Grupos</option>
                                    <?php
                                    $sql = "select id_contas_movimento,descricao from sf_contas_movimento where tipo = 'S' ORDER BY descricao";
                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                    while ($RFP = odbc_fetch_array($cur)) {
                                        ?>
                                        <option value="<?php echo $RFP['id_contas_movimento'] ?>"<?php
                                        if (!(strcmp($RFP["id_contas_movimento"], $grupo))) {
                                            echo "SELECTED";
                                        }
                                        ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                            <?php } ?>
                                </select>
                                <button name="btnfind" id="btnfind" class="button button-turquoise btn-primary" type="button" onclick="refresh()"><span class="ico-search icon-white"></span></button>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead" <?php echo $visible; ?>>
                        <div class="boxtext">Veículos</div>
                    </div>
                    <div <?php
                    if ($imprimir == 0) {
                        echo "class=\"boxtable\"";
                    }
                    ?>>
                            <?php
                            if ($imprimir == 1) {
                                $titulo_pagina = "RELATÓRIO DE CONSULTA<br/>SERVIÇOS";
                                include "../Financeiro/Cabecalho-Impressao.php";
                            }
                            ?>
                        <table <?php
                        if ($imprimir == 0) {
                            echo "class=\"table\"";
                        } else {
                            echo "border=\"1\"";
                        }
                        ?> style="float:none" cellpadding="0" cellspacing="0" width="100%" id="example">
                            <thead>
                                <tr>
                                    <th width="20%">Montadora</th>
                                    <!-- <th width="45%">Descrição do Serviço</th>
                                    <th width="10%">Custo</th>
                                    <th width="10%">Valor Venda</th> -->
                                    <?php if ($imprimir == 0) { ?>
                                        <!-- <th width="5%"><center>Ação:</center></th> -->
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="5" class="dataTables_empty">Carregando dados ...</td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/jquery.mousewheel.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/charts.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>
        
        <script type="text/javascript">
            
            $.json

            function finalURL(op) {
                var retPrint = "";
                if (op === 0) {
                    retPrint = "TabelaVeiculos_server_processing.php?imp=<?php
                    echo $imprimir;
                    if ($imprimir == 1) {
                        echo "&at=" . $ativo . "&gp=" . $_GET["gp"];
                    }
                    ?>";
                }
            <?php if ($imprimir == 0) { ?>
                retPrint = retPrint + "&at=" + $("#txtAtivo").val();
                retPrint = retPrint + "&gp=" + $("#txtGrupo").val();
            <?php } ?>
                return retPrint;
            }
            $(document).ready(function () {
                $("#example").dataTable({
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": finalURL(0),
                    "oLanguage": {
                        "oPaginate": {
                            "sFirst": "Primeiro",
                            "sLast": "Último",
                            "sNext": "Próximo",
                            "sPrevious": "Anterior"
                        }, "sEmptyTable": "Não foi encontrado nenhum resultado",
                        "sInfo": "Visualização do registro _START_ ao _END_ de _TOTAL_",
                        "sInfoEmpty": "Visualização do registro 0 ao 0 de 0",
                        "sInfoFiltered": "(filtrado de _MAX_ registros totais)",
                        "sLengthMenu": "Visualização de _MENU_ registros",
                        "sLoadingRecords": "Carregando...",
                        "sProcessing": "Processando...",
                        "sSearch": "Pesquisar:",
                        "sZeroRecords": "Não foi encontrado nenhum resultado"},
                    "sPaginationType": "full_numbers",
            <?php if ($imprimir == 1) { ?>
                    "fnInitComplete": function (oSettings, json) {
                        window.print();
                        window.history.back();
                    }
            <?php } ?>
                });
            });
            function AbrirBox(id, tp) {
                if (tp === 1) {
                    window.location = "TabelaVeiculos.php?imp=" + id + finalURL(1);
                } else {
                    if (id > 0) {
                        var myId = "?id=" + id;
                    } else {
                        var myId = "";
                    }
                    abrirTelaBox("FormTabelaVeiculos.php" + myId, 388, 600);
                }
            }
            function refresh() {
                var oTable = $("#example").dataTable();
                oTable.fnReloadAjax(finalURL(0));
            }
            function FecharBox() {
                refresh();
                $("#newbox").remove();
            }
        <?php if ($imprimir == 1) { ?>  
            $(window).load(function () {
                $(".body").css("margin-left", 0);
                $("#example_length").remove();
                $("#example_filter").remove();
                $("#example_paginate").remove();
                $("#formPQ > th").css("background-image", "none");
            });
        <?php } ?>
        </script>

        <style type="text/css">
            .fancybox-custom .fancybox-skin {
                box-shadow: 0 0 50px #069;
            }
        </style>        
        <?php odbc_close($con); ?>
    </body>
</html>
