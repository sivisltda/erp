<?php
include '../../Connections/configini.php';
$disabled = 'disabled';
$active1 = 'active';
$active7 = '';

if (isset($_POST['bntSave'])) {
    $totalCodX = 0;
    if (strlen($_POST['txtCodigo_interno']) > 0) {
        $cur = odbc_exec($con, "select count(*) total from sf_produtos where codigo_interno = " . valoresTexto('txtCodigo_interno') . " and conta_produto <> " . valoresNumericos('txtId'));
        while ($RFP = odbc_fetch_array($cur)) {
            $totalCodX = $RFP['total'];
        }
    }
    if ($_POST['txtDescricao'] != "" && $_POST['txtGrupo'] != "" && $totalCodX == 0) {
        if ($_POST['txtId'] != '') {
            $image = explode(",", $_POST["src"]);
            $types = array("data:image/jpeg;base64", "data:image/png;base64", "data:image/gif;base64");
            if (in_array($image[0], $types)) {
                file_put_contents("./../../Produtos/" . $contrato . "/" . $_POST["txtId"] . ".png", base64_decode($image[1]));
                $size_tmp = getimagesize("./../../Produtos/" . $contrato . "/" . $_POST["txtId"] . ".png");
                $size_new = 265;
                if ($size_tmp[0] > $size_tmp[1]) {
                    $ratio = ($size_tmp[0] / $size_new);
                } else {
                    $ratio = ($size_tmp[1] / $size_new);
                }
                if ($image[0] == $types[0]) {
                    $source = imagecreatefromjpeg("./../../Produtos/" . $contrato . "/" . $_POST["txtId"] . ".png");
                } else if ($image[0] == $types[1]) {
                    $source = imagecreatefrompng("./../../Produtos/" . $contrato . "/" . $_POST["txtId"] . ".png");
                } else if ($image[0] == $types[2]) {
                    $source = imagecreatefromgif("./../../Produtos/" . $contrato . "/" . $_POST["txtId"] . ".png");
                }
                $output = imagecreatetruecolor($size_new, $size_new);
                imagecopyresampled($output, $source, 0, 0, ($_POST["x"] * $ratio), ($_POST["y"] * $ratio), $size_new, $size_new, ($_POST["w"] * $ratio), ($_POST["h"] * $ratio));
                imagejpeg($output, "./../../Produtos/" . $contrato . "/" . $_POST["txtId"] . ".png", 100);
            } else if ($_POST["src"] != $_POST["txtId"] . ".png") {
                if (file_exists("./../../Produtos/" . $contrato . "/" . $_POST["txtId"] . ".png")) {
                    unlink("./../../Produtos/" . $contrato . "/" . $_POST["txtId"] . ".png");
                }
            }
            $query = "set dateformat dmy;UPDATE sf_produtos SET " .
                    "descricao = " . valoresTexto('txtDescricao') .
                    ",tipo = 'P' " .
                    ",inativa = " . valoresCheck('txtInativo') .
                    ",sem_entrada = " . valoresSelect('txtSemEntrada') .
                    ",favorito = " . valoresCheck('txtFavorito') .
                    ",alterar_valor = " . valoresSelect("txtAlterarVal") .                    
                    ",conta_movimento = " . valoresSelect('txtGrupo') .
                    ",codigo_interno = " . valoresTexto('txtCodigo_interno') .
                    ",codigo_barra = " . valoresTexto('txtCodigo_barra') .
                    ",unidade_comercial = " . valoresTexto('txtUnidade_comercial') .
                    ",quantidade_comercial = " . valoresNumericos('txtQuantidade_comercial') .
                    ",cod_fabricante = " . valoresSelect('txtFabricante') .
                    ",tabela_preco_nao = " . valoresCheck('txtTabela_preco_nao') .
                    ",data_inativo = " . valoresData('txtData_inativo') .
                    ",preco_venda = " . valoresNumericos('txtPreco_venda') .
                    ",localizacao = " . valoresSelect('txtLocalizacao') .
                    ",estoque_minimo = " . valoresNumericos('txtEstoque_minimo') .
                    ",estoque_maximo = " . valoresNumericos('txtEstoque_maximo') .
                    ",previsao_chegada = " . valoresData('txtPrevisao_chegada') .
                    ",quantidade_chegada = " . valoresNumericos('txtQuantidade_chegada') .
                    ",dim_pro_largura = " . valoresNumericos('txtDim_pro_largura') .
                    ",dim_pro_altura = " . valoresNumericos('txtDim_pro_altura') .
                    ",dim_pro_comprimento = " . valoresNumericos('txtDim_pro_comprimento') .
                    ",dim_emb_largura = " . valoresNumericos('txtDim_emb_largura') .
                    ",dim_emb_altura = " . valoresNumericos('txtDim_emb_altura') .
                    ",dim_emb_comprimento = " . valoresNumericos('txtDim_emb_comprimento') .
                    ",peso_bruto = " . valoresNumericos('txtPeso_bruto') .
                    ",peso_liquido = " . valoresNumericos('txtPeso_liquido') .
                    ",origem = " . valoresTexto('txtOrigem') .
                    ",ncm = " . valoresTexto('txtNcm') .
                    ",cfop = " . valoresTexto('txtCfop') .
                    ",cest = " . valoresTexto('txtCest') .
                    ",substituicao_trib = " . valoresTexto('txtSubstituicao_trib') .
                    ",unidade_trib = " . valoresTexto('txtUnidade_trib') .
                    ",quantidade_trib = " . valoresNumericos('txtQuantidade_trib') .
                    ",valor_unitario_trib = " . valoresNumericos('txtValor_unitario_trib') .
                    ",valor_impostos = " . valoresNumericos('txtImpostos') .
                    ",valor_frete = " . valoresNumericos('txtFrete') .
                    ",valor_comissoes = " . valoresNumericos('txtComissoes') .
                    ",unidade_dimensao = " . valoresTexto('txtUnidade_dimensao') .
                    ",p_cor = " . valoresSelect('txtP_Cor') .
                    ",p_tamanho = " . valoresSelect('txtP_Tamanho') .
                    ",valor_desconto = " . valoresNumericos('txtDesc_Max') .
                    ",desc_prod = ltrim(" . valoresTexto('ckeditor') .
                    "),producao = " . valoresCheck('txtProducao') .
                    ",cenario = " . valoresSelect('txtCenario') .
                    ",tempo_producao = " . valoresNumericos('txtTime') .
                    ",valor_comissao_venda = " . valoresNumericos('txtComissao_venda') .
                    ",nao_vender_sem_estoque = " . valoresCheck('txtNaoVendeSemEstoque') .
                    ",tp_preco = " . valoresSelect('txtForma_Preco') .
                    ",valor_margem_lucro = " . valoresNumericos('txtMargem_Lucro_Val') .
                    " WHERE conta_produto = " . $_POST['txtId'];
            //echo $query;
            odbc_exec($con, $query) or die(odbc_errormsg());
            if (is_numeric($_POST['txtId'])) {
                $prod_id = $_POST['prod_id'];
                $prod_cod = $_POST['prod_cod'];
                $prod_qnt = $_POST['prod_qnt'];
                $notIn = 0;
                for ($i = 0; $i < sizeof($prod_id); $i++) {
                    if (is_numeric($prod_id[$i])) {
                        $notIn = $notIn . "," . $prod_id[$i];
                    }
                }
                odbc_exec($con, "DELETE FROM sf_produtos_materiaprima WHERE id_produtomp = " . $_POST['txtId'] . " and id_prodmp not in (" . $notIn . ")");
                for ($i = 0; $i < sizeof($prod_cod); $i++) {
                    if (is_numeric($prod_id[$i])) {
                        odbc_exec($con, "UPDATE sf_produtos_materiaprima set id_materiaprima = " . $prod_cod[$i] . ",quantidademp = " . valoresNumericos2($prod_qnt[$i]) . " WHERE id_prodmp = " . $prod_id[$i]);
                    } else {
                        odbc_exec($con, "INSERT INTO sf_produtos_materiaprima(id_produtomp,quantidademp,id_materiaprima) VALUES (" . $_POST['txtId'] . "," . valoresNumericos2($prod_qnt[$i]) . "," . $prod_cod[$i] . ")");
                    }
                }
            }
            if ((valoresNumericos('txtPrecoAtual') != valoresNumericos('txtPreco_venda')) && is_numeric($_POST['txtId'])) {
                odbc_exec($con, "INSERT INTO sf_produtos_historico_preco(id_produto,data_atual,preco_atual,margem_lucro) VALUES (" . $_POST['txtId'] . ",getdate()," . valoresNumericos('txtPrecoAtual') . "," . valoresNumericos('txtMargemLucroAtual') . ")");
            }
        } else {
            $query = "INSERT INTO sf_produtos(descricao,tipo,inativa,conta_movimento,codigo_interno,codigo_barra,unidade_comercial,quantidade_comercial,cod_fabricante
            ,tabela_preco_nao,data_inativo,preco_venda,localizacao,estoque_minimo
            ,estoque_maximo,previsao_chegada,quantidade_chegada,dim_pro_largura,dim_pro_altura,dim_pro_comprimento
            ,dim_emb_largura,dim_emb_altura,dim_emb_comprimento,peso_bruto,peso_liquido,origem
            ,ncm,cfop,cest,substituicao_trib,unidade_trib,quantidade_trib,unidade_dimensao,valor_unitario_trib,valor_impostos,valor_frete,valor_comissoes,p_cor,p_tamanho,sem_entrada
            ,valor_desconto,desc_prod,alterar_valor,favorito,editavel,producao,cenario,tempo_producao,valor_comissao_venda,nao_vender_sem_estoque,tp_preco,valor_margem_lucro) VALUES(" .
                    valoresTexto('txtDescricao') . ",'P'," .
                    valoresCheck('txtInativo') . "," .
                    valoresSelect('txtGrupo') . "," .
                    valoresTexto('txtCodigo_interno') . "," .
                    valoresTexto('txtCodigo_barra') . "," .
                    valoresTexto('txtUnidade_comercial') . "," .
                    valoresNumericos('txtQuantidade_comercial') . "," .
                    valoresSelect('txtFabricante') . "," .
                    valoresCheck('txtTabela_preco_nao') . "," .
                    valoresData('txtData_inativo') . "," .
                    valoresNumericos('txtPreco_venda') . "," .
                    valoresSelect('txtLocalizacao') . "," .
                    valoresNumericos('txtEstoque_minimo') . "," .
                    valoresNumericos('txtEstoque_maximo') . "," .
                    valoresData('txtPrevisao_chegada') . "," .
                    valoresNumericos('txtQuantidade_chegada') . "," .
                    valoresNumericos('txtDim_pro_largura') . "," .
                    valoresNumericos('txtDim_pro_altura') . "," .
                    valoresNumericos('txtDim_pro_comprimento') . "," .
                    valoresNumericos('txtDim_emb_largura') . "," .
                    valoresNumericos('txtDim_emb_altura') . "," .
                    valoresNumericos('txtDim_emb_comprimento') . "," .
                    valoresNumericos('txtPeso_bruto') . "," .
                    valoresNumericos('txtPeso_liquido') . "," .
                    valoresTexto('txtOrigem') . "," .
                    valoresTexto('txtNcm') . "," .
                    valoresTexto('txtCfop') . "," .
                    valoresTexto('txtCest') . "," .
                    valoresTexto('txtSubstituicao_trib') . "," .
                    valoresTexto('txtUnidade_trib') . "," .
                    valoresNumericos('txtQuantidade_trib') . "," .
                    valoresTexto('txtUnidade_dimensao') . "," .
                    valoresNumericos('txtValor_unitario_trib') . "," .
                    valoresNumericos('txtImpostos') . "," .
                    valoresNumericos('txtFrete') . "," .
                    valoresNumericos('txtComissoes') . "," .
                    valoresSelect('txtP_Cor') . "," .
                    valoresSelect('txtP_Tamanho') . "," .
                    valoresSelect('txtSemEntrada') . "," .
                    valoresNumericos('txtDesc_Max') . ",LTRIM(" .
                    valoresTexto('ckeditor') . ")," .
                    valoresSelect("txtAlterarVal") . "," .                      
                    valoresCheck('txtFavorito') . ",0," .
                    valoresCheck('txtProducao') . "," .
                    valoresSelect('txtCenario') . "," .
                    valoresNumericos('txtTime') . "," .
                    valoresNumericos('txtComissao_venda') . "," .
                    valoresCheck('txtNaoVendeSemEstoque') . "," .
                    valoresSelect('txtForma_Preco') . "," .
                    valoresNumericos('txtMargem_Lucro_Val') . ")";
            odbc_exec($con, $query) or die(odbc_errormsg());
            $res = odbc_exec($con, "select top 1 conta_produto from sf_produtos where conta_produto not in (466) order by conta_produto desc") or die(odbc_errormsg());
            $id = odbc_result($res, 1);
            $_POST['txtId'] = $id;
            if (is_numeric($id)) {
                $prod_cod = $_POST['prod_cod'];
                $prod_qnt = $_POST['prod_qnt'];
                for ($i = 0; $i < sizeof($prod_cod); $i++) {
                    odbc_exec($con, "INSERT INTO sf_produtos_materiaprima(id_produtomp,quantidademp,id_materiaprima) VALUES (" . $id . "," . valoresNumericos2($prod_qnt[$i]) . "," . $prod_cod[$i] . ")");
                }
            }
        }
    } else {
        if ($totalCodX > 0) {
            echo "<script>alert('Código de produto já cadastrado, não é possível registrar o mesmo código!');</script>";
        } else {
            echo "<script>alert('Preencha os campos obrigatórios corretamente!');</script>";
        }
    }
}

if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        odbc_exec($con, "DELETE FROM sf_produtos_historico_preco where id_produto = " . $_POST['txtId']);
        odbc_exec($con, "DELETE FROM sf_produtos_materiaprima where id_produtomp = " . $_POST['txtId'] . " or id_materiaprima = " . $_POST['txtId']);
        odbc_exec($con, "DELETE FROM sf_produtos WHERE conta_produto = " . $_POST['txtId']);
        if (file_exists("./../../Produtos/" . $contrato . "/" . $_POST['txtId'] . ".png")) {
            unlink("./../../Produtos/" . $contrato . "/" . $_POST['txtId'] . ".png");
        }
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox(0);</script>";
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_produtos where conta_produto =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['conta_produto'];
        $descricao = utf8_encode($RFP['descricao']);
        $codigo_interno = utf8_encode($RFP['codigo_interno']);
        $grupo = $RFP['conta_movimento'];
        $codigo_barra = utf8_encode($RFP['codigo_barra']);
        $unidade_comercial = utf8_encode($RFP['unidade_comercial']);
        $quantidade_comercial = escreverNumero($RFP['quantidade_comercial']);
        $fabricante = utf8_encode($RFP['cod_fabricante']);
        $prod_inativo = $RFP['inativa'];
        $prod_sem_entrada = $RFP['sem_entrada'];
        $prod_favorito = $RFP['favorito'];
        $tabela_preco_nao = $RFP['tabela_preco_nao'];
        $data_inativo = escreverData($RFP['data_inativo']);
        $localizacao = utf8_encode($RFP['localizacao']);
        $estoque_minimo = escreverNumero($RFP['estoque_minimo']);
        $estoque_maximo = escreverNumero($RFP['estoque_maximo']);
        $previsao_chegada = escreverData($RFP['previsao_chegada']);
        $quantidade_chegada = escreverNumero($RFP['quantidade_chegada']);
        $dim_pro_largura = escreverNumero($RFP['dim_pro_largura']);
        $dim_pro_altura = escreverNumero($RFP['dim_pro_altura']);
        $dim_pro_comprimento = escreverNumero($RFP['dim_pro_comprimento']);
        $dim_emb_largura = escreverNumero($RFP['dim_emb_largura']);
        $dim_emb_altura = escreverNumero($RFP['dim_emb_altura']);
        $dim_emb_comprimento = escreverNumero($RFP['dim_emb_comprimento']);
        $peso_bruto = escreverNumero($RFP['peso_bruto'], 0, 3);
        $peso_liquido = escreverNumero($RFP['peso_liquido'], 0, 3);
        $origem = utf8_encode($RFP['origem']);
        $ncm = utf8_encode($RFP['ncm']);
        $cfop = utf8_encode($RFP['cfop']);
        $cest = utf8_encode($RFP['cest']);
        $substituicao_trib = utf8_encode($RFP['substituicao_trib']);
        $unidade_trib = utf8_encode($RFP['unidade_trib']);
        $quantidade_trib = escreverNumero($RFP['quantidade_trib']);
        $valor_unitario_trib = escreverNumero($RFP['valor_unitario_trib']);
        $ultimo_reajuste = '';
        $preco_custo = escreverNumero(0);
        $preco_venda = $RFP['preco_venda'];
        $custo_medio = escreverNumero(0);
        $margem_lucro = escreverNumero(0);
        $Desc_Max = escreverNumero($RFP['valor_desconto']);
        $texto_desc = utf8_encode($RFP['desc_prod']);
        $valor_lucro = escreverNumero(0);
        $estoque_atual = escreverNumero(0);
        $imagemx = $RFP['conta_produto'] . ".png";
        $unidade_dimensao = utf8_encode($RFP['unidade_dimensao']);
        $impostos = escreverNumero($RFP['valor_impostos']);
        $frete = escreverNumero($RFP['valor_frete']);
        $comissoes = escreverNumero($RFP['valor_comissoes']);
        $p_cor = $RFP['p_cor'];
        $p_tamanho = $RFP['p_tamanho'];
        $producao = $RFP['producao'];
        $time = $RFP['tempo_producao'];
        $cenario = $RFP['cenario'];
        $comissao_venda = escreverNumero($RFP['valor_comissao_venda']);
        $nao_vender_sem_estoque = $RFP['nao_vender_sem_estoque'];
        $margem_lucro_val = escreverNumero($RFP['valor_margem_lucro']);
        $formaPreco = $RFP['tp_preco'];
        $txtAlterarVal = $RFP["alterar_valor"];
    }
    $cur = odbc_exec($con, "select top 1 quantidade qtd_total,valor_total valor_total , data_venda ult_reaj
                            from dbo.sf_vendas v inner join sf_vendas_itens i on v.id_venda = i.id_venda
                            where cov = 'C' and status = 'Aprovado' and produto = " . $PegaURL . "  order by data_venda desc ");
    while ($RFP = odbc_fetch_array($cur)) {
        $ultimo_reajuste = escreverData($RFP['ult_reaj']);
        if ($RFP['qtd_total'] > 0) {
            $preco_custo = ($RFP['valor_total'] / $RFP['qtd_total']);
            $valor_lucro = $preco_venda - ($preco_custo + $impostos + $frete + $comissoes);
            $margem_lucro = ($valor_lucro / $preco_custo) * 100;
            $preco_custo = escreverNumero($preco_custo);
        }
    }
    $preco_venda = escreverNumero($preco_venda);
    $cur = odbc_exec($con, "select sum(valor_total)/sum(quantidade) total from sf_vendas v inner join sf_vendas_itens i on v.id_venda = i.id_venda
                            where cov = 'C' and status = 'Aprovado' and produto = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $custo_medio = escreverNumero($RFP['total']);
    }
    $margem_lucro = escreverNumero($margem_lucro);
    $valor_lucro = escreverNumero($valor_lucro);
    if ($prod_sem_entrada == 1) {
        $estoque_atual = "-";
    } else {
        $cur = odbc_exec($con, "select dbo.ESTOQUE_ATUAL_TOTAL(" . $PegaURL . ") total from sf_produtos where conta_produto = " . $PegaURL);
        while ($RFP = odbc_fetch_array($cur)) {
            $estoque_atual = escreverNumero($RFP['total'], 0, 4);
        }
    }
    $cur = odbc_exec($con, "select max(data_venda) data from sf_vendas v inner join sf_vendas_itens i on v.id_venda = i.id_venda
                            where cov = 'C' and status = 'Aprovado' and produto = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $ultima_compra = escreverData($RFP['data']);
    }
    $cur = odbc_exec($con, "select max(data_venda) data from sf_vendas v inner join sf_vendas_itens i on v.id_venda = i.id_venda
                            where cov = 'V' and status = 'Aprovado' and produto = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $ultima_venda = escreverData($RFP['data']);
    }
} else {
    $disabled = '';
    $id = '';
    $descricao = '';
    $codigo_interno = '';
    $grupo = '';
    $codigo_barra = '';
    $unidade_comercial = '';
    $quantidade_comercial = escreverNumero(1);
    $fabricante = '';
    $prod_inativo = '';
    $prod_sem_entrada = '';
    $prod_favorito = '';
    $tabela_preco_nao = '';
    $data_inativo = '';
    $preco_venda = escreverNumero(0);
    $localizacao = '';
    $estoque_minimo = escreverNumero(1);
    $estoque_maximo = escreverNumero(100);
    $previsao_chegada = '';
    $quantidade_chegada = '0';
    $dim_pro_largura = escreverNumero(0);
    $dim_pro_altura = escreverNumero(0);
    $dim_pro_comprimento = escreverNumero(0);
    $dim_emb_largura = escreverNumero(0);
    $dim_emb_altura = escreverNumero(0);
    $dim_emb_comprimento = escreverNumero(0);
    $peso_bruto = escreverNumero(0 ,0 ,3);
    $peso_liquido = escreverNumero(0 ,0 ,3);
    $origem = '';
    $ncm = '';
    $cfop = '';
    $cest = '';
    $substituicao_trib = '';
    $unidade_trib = '';
    $quantidade_trib = escreverNumero(0);
    $valor_unitario_trib = escreverNumero(0);
    $preco_custo = escreverNumero(0);
    $margem_lucro = escreverNumero(0);
    $ultimo_reajuste = '';
    $estoque_atual = escreverNumero(0);
    $custo_medio = escreverNumero(0);
    $imagemx = '';
    $unidade_dimensao = '';
    $impostos = escreverNumero(0);
    $frete = escreverNumero(0);
    $comissoes = escreverNumero(0);
    $p_cor = 'null';
    $p_tamanho = 'null';
    $Desc_Max = escreverNumero(0);
    $texto_desc = '';
    $producao = '';
    $time = '0';
    $cenario = 'null';
    $comissao_venda = escreverNumero(0);
    $nao_vender_sem_estoque = '';
    $ultima_compra = '';
    $ultima_venda = '';
    $cur = odbc_exec($con, "select top 1 comiss_gere from sf_fornecedores_despesas where tipo in ('M') order by razao_social") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $impostos = escreverNumero($RFP['comiss_gere']);
    }
    $margem_lucro_val = escreverNumero(0);
    $formaPreco = '0';
    $txtAlterarVal = "";    
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
$FinalUrl = "";
if (isset($_GET["idx"])) {
    $FinalUrl = $_GET["idx"];
}
if (isset($_POST["txtFinalUrl"])) {
    $FinalUrl = $_POST["txtFinalUrl"];
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<link href="../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<link href="../../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<script src="../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="../../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<link rel="stylesheet" href="../../js/jcrop/jquery.Jcrop.min.css" type="text/css"/>
<body>
    <form action="FormProdutos.php<?php
    if ($FinalUrl != "") {
        echo "?idx=" . $FinalUrl;
    }
    ?>" method="POST" name="frmEnviaDados" id="frmEnviaDados" enctype="multipart/form-data">
        <div class="frmhead">
            <div class="frmtext">Produtos</div>
            <div class="frmicon" onClick="parent.FecharBox(1)">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="tabbable frmtabs">
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <input name="txtPrecoAtual" id="txtPrecoAtual" value="<?php echo $preco_venda; ?>" type="hidden"/>
            <input name="txtMargemLucroAtual" id="txtMargemLucroAtual" value="<?php echo $valor_lucro; ?>" type="hidden"/>
            <ul class="nav nav-tabs" style="margin:0">
                <li class="<?php echo $active1; ?>"><a href="#tab1" data-toggle="tab">Geral</a></li>
                <li><a href="#tab8" data-toggle="tab">Descrição</a></li>
                <li><a href="#tab2" data-toggle="tab">Financeiro</a></li>
                <li><a href="#tab3" data-toggle="tab">Estoque</a></li>
                <li><a href="#tab4" data-toggle="tab">Dimensões</a></li>
                <li><a href="#tab5" data-toggle="tab">Dados Fiscais</a></li>
                <li><a href="#tab6" data-toggle="tab">Entradas</a></li>
                <li id="matpri" class="<?php echo $active7; ?>"><a href="#tab7" data-toggle="tab">Materias Primas</a></li>
            </ul>
            <div class="tab-content frmcont" style="height:370px">
                <div class="tab-pane active" <?php echo $active1; ?> id="tab1" style="overflow:hidden; height:370px">
                    <div style="float: left;width: 50%">
                        <div style="width: 100%;float:left;">
                            <span style="width: 50%">Nome do Produto:</span>
                            <input name="txtDescricao" id="txtDescricao" type="text" <?php echo $disabled; ?> class="input-medium" value="<?php echo $descricao; ?>"/>
                        </div>
                    </div>
                    <div style="float: left;width: 18%;margin-left: 4%;margin-top: 18px;">
                        <div style="width: 88%;float:left;">
                            <input name="txtInativo" id="txtInativo" <?php echo $disabled; ?> type="checkbox" <?php
                            if ($prod_inativo == "1") {
                                echo "CHECKED";
                            }
                            ?> value="1" class="input-medium"/>
                            <span style="width: 50%">Produto Inativo</span>
                        </div>
                    </div>
                    <div style="float: left;width: 15%;display:none" id="data_hide">
                        <div style="float:left;">
                            <span style="width: 50%">Data:</span>
                            <input style="width:100px; text-align:center" name="txtData_inativo" id="txtData_inativo" type="text" <?php echo $disabled; ?> class="datepicker" maxlength="100" value="<?php echo $data_inativo; ?>"/>
                        </div>
                    </div>
                    <div style="clear:both;height: 5px;"></div>
                    <div style="float: left;width: 50%">
                        <div style="width: 100%;float:left;">
                            <span style="width: 50%">Fabricante:</span>
                            <select name="txtFabricante" id="txtFabricante" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                                <option value="null">Selecione o Fabricante:</option>
                                <?php
                                $sql = "select id_fabricante,nome_fabricante from sf_fabricantes ORDER BY nome_fabricante";
                                $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) {
                                    ?>
                                    <option value="<?php echo $RFP['id_fabricante'] ?>"<?php
                                    if (!(strcmp($RFP['id_fabricante'], $fabricante))) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP['nome_fabricante']) ?></option>
                                        <?php } ?>
                            </select>
                        </div>
                        <div style="clear:both;height: 4px;"></div>
                        <div style="width: 100%;float:left;">
                            <span style="width: 50%">Grupo:</span>
                            <select name="txtGrupo" id="txtGrupo" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                                <option value="">Selecione o grupo:</option>
                                <?php
                                $sql = "select id_contas_movimento,descricao from sf_contas_movimento where tipo = 'P' ORDER BY descricao";
                                $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) {
                                    ?>
                                    <option value="<?php echo $RFP['id_contas_movimento'] ?>"<?php
                                    if (!(strcmp($RFP['id_contas_movimento'], $grupo))) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                        <?php } ?>
                            </select>
                        </div>
                        <div style="clear:both;height: 4px;"></div>
                        <div style="width: 100%;float:left;">
                            <span style="width: 50%">Código:</span>
                            <input name="txtCodigo_interno" id="txtCodigo_interno" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $codigo_interno; ?>"/>
                        </div>
                        <div style="clear:both;height: 4px;"></div>
                        <div style="width: 100%;float:left;">
                            <span style="width: 50%">EAN13:</span>
                            <input name="txtCodigo_barra" id="txtCodigo_barra" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $codigo_barra; ?>"/>
                        </div>
                        <div style="clear:both;height: 4px;"></div>
                        <div style="width: 50%;float:left;">
                            <span style="width: 50%">Quant. Comercial:</span>
                            <input style="text-align:right" name="txtQuantidade_comercial" id="txtQuantidade_comercial" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $quantidade_comercial; ?>"/>
                        </div>
                        <div style="width: 49%;float:left;margin-left: 1%">
                            <span style="width: 50%">Unidade Comercial:</span>
                            <select name="txtUnidade_comercial" id="txtUnidade_comercial" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                                <option value="UN" <?php
                                if ($unidade_comercial == "UN") {
                                    echo "SELECTED";
                                }
                                ?>>Unitários</option>
                                <option value="GR" <?php
                                if ($unidade_comercial == "GR") {
                                    echo "SELECTED";
                                }
                                ?>>Gramas</option>
                                <option value="KG" <?php
                                if ($unidade_comercial == "KG") {
                                    echo "SELECTED";
                                }
                                ?>>Quilos</option>
                                <option value="ML" <?php
                                if ($unidade_comercial == "ML") {
                                    echo "SELECTED";
                                }
                                ?>>Mililitros</option>
                                <option value="LT" <?php
                                if ($unidade_comercial == "LT") {
                                    echo "SELECTED";
                                }
                                ?>>Litros</option>
                                <option value="CM" <?php
                                if ($unidade_comercial == "CM") {
                                    echo "SELECTED";
                                }
                                ?>>Centímetros</option>
                                <option value="MT" <?php
                                if ($unidade_comercial == "MT") {
                                    echo "SELECTED";
                                }
                                ?>>Metros</option>
                                <option value="MQ" <?php
                                if ($unidade_comercial == "MQ") {
                                    echo "SELECTED";
                                }
                                ?>>Metros Quadrados</option>
                                <option value="CX" <?php
                                if ($unidade_comercial == "CX") {
                                    echo "SELECTED";
                                }
                                ?>>Caixas</option>
                                <option value="PC" <?php
                                if ($unidade_comercial == "PC") {
                                    echo "SELECTED";
                                }
                                ?>>Peças</option>
                            </select>
                        </div>
                        <div style="clear:both;height: 4px;"></div>
                        <div style="width: 100%;float:left;">
                            <span style="width: 50%">Tipo:</span>
                            <select name="txtSemEntrada" id="txtSemEntrada" <?php echo $disabled; ?> class="input-medium" style="width:100%">
                                <option value="0" <?php
                                if ($prod_sem_entrada == "0") {
                                    echo "SELECTED";
                                }
                                ?>>MERCADORIA</option>
                                <option value="1" <?php
                                if ($prod_sem_entrada == "1") {
                                    echo "SELECTED";
                                }
                                ?>>PRODUTO</option>
                            </select>
                        </div>
                        <div style="clear:both;height: 4px;"></div>
                        <div style="width:100%;float: left;">
                            <input name="txtTabela_preco_nao" id="txtTabela_preco_nao" <?php echo $disabled; ?> value="1" type="checkbox" <?php
                            if ($tabela_preco_nao == 1) {
                                echo "CHECKED";
                            }
                            ?> class="input-medium"/>
                            <span style="width: 50%">Não incluir na tabela de preços</span>
                        </div>
                        <div style="width:100%;float: left;">
                            <input name="txtFavorito" id="txtFavorito" <?php echo $disabled; ?> type="checkbox" <?php
                            if ($prod_favorito == "1") {
                                echo "CHECKED";
                            }
                            ?> value="1" class="input-medium"/>
                            <span style="width: 50%">Favorito (Produto em destaque)</span>
                        </div>
                    </div>
                    <div style="float: left;width: 50%">
                        <div style="width: 88%;float:left;">
                            <div style="float:right; width:265px; height:265px; margin-top:10px; border:1px solid #CCC; background-color:#EEE">
                                <img name="picImag" id="picImag" src="<?php
                                if ($imagemx != "" and file_exists("./../../Produtos/" . $contrato . "/" . $imagemx)) {
                                    $source = file_get_contents("./../../Produtos/" . $contrato . "/" . $imagemx);
                                    echo "data:image/png;base64," . base64_encode($source);
                                } else {
                                    echo "./../../img/img.png";
                                }
                                ?>" style="width:auto; height:auto; max-width:100%; max-height:100%;<?php
                                     if ($disabled == "") {
                                         echo " cursor:pointer";
                                     }
                                     ?>"/>
                                <div style="float:right; margin-right:2px">
                                    <img id="delImag" src="./../../img/inativo.png" width="24" height="24" style="<?php
                                    if ($id == "" || $disabled != "" || $imagemx == "" || !file_exists("./../../Produtos/" . $contrato . "/" . $imagemx)) {
                                        echo "display:none; ";
                                    }
                                    ?>cursor:pointer"/>
                                </div>
                                <input type="file" name="txtImag" id="txtImag" style="display:none"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab8" style="overflow:hidden; height:370px">
                    <div style="width:100%;float: left;">
                        <textarea id="ckeditor" name="ckeditor" style="height: 365px;" <?php echo $disabled; ?>><?php echo $texto_desc; ?></textarea>
                    </div>
                </div>
                <div class="tab-pane" id="tab2" style="overflow:hidden; height:370px">
                    <div style="width: 25%;float:left;">
                        <span style="width: 50%">Controle de Preço:</span>
                        <select name="txtForma_Preco" id="txtForma_Preco" <?php echo $disabled; ?>>
                            <option value="0" <?php
                            if ($formaPreco == 0) {
                                echo "SELECTED";
                            }
                            ?>>Por valor Fixo</option>
                            <option value="1" <?php
                            if ($formaPreco == 1) {
                                echo "SELECTED";
                            }
                            ?>>Por margem de Lucro</option>
                        </select>
                    </div>
                    <div style="width: 24%;float:left;margin-left: 1%">
                        <span style="width: 50%">Último Reajuste:</span>
                        <input style="text-align:center" name="txtUltimo_reajuste" id="txtUltimo_reajuste" type="text" disabled class="datepicker" maxlength="100" value="<?php echo $ultimo_reajuste; ?>"/>
                    </div>
                    <div style="width: 24%;float:left;margin-left: 1%">
                        <span style="width: 50%">Última Compra:</span>
                        <input style="text-align:center" name="txtUltima_compra" id="txtUltima_compra" type="text" disabled class="datepicker" maxlength="100" value="<?php echo $ultima_compra; ?>"/>
                    </div>
                    <div style="width: 24%;float:left;margin-left: 1%">
                        <span style="width: 50%">Última Venda:</span>
                        <input style="text-align:center" name="txtUltima_venda" id="txtUltima_venda" type="text" disabled class="datepicker" maxlength="100" value="<?php echo $ultima_venda; ?>"/>
                    </div>
                    <div style="clear:both;height:10px;"></div>
                    <div style="width: 25%;float:left;">
                        <span style="width: 50%">Preço de Custo:</span>
                        <input style="text-align:right" name="txtPreco_custo" id="txtPreco_custo" type="text" disabled class="input-medium" maxlength="100" value="<?php echo $preco_custo; ?>"/>
                    </div>
                    <div style="float: left;width: 24%;margin-left: 1%">
                        <span style="width: 50%">Custo Médio:</span>
                        <input style="text-align:right" name="txtCusto_medio" id="txtCusto_medio" type="text" disabled class="input-medium" maxlength="100" value="<?php echo $custo_medio; ?>"/>
                    </div>
                    <div style="float: left;width: 24%;margin-left: 1%">
                        <span style="width: 50%">Desconto Máximo (%):</span>
                        <input style="text-align:center" name="txtDesc_Max" id="txtDesc_Max" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="6" value="<?php echo $Desc_Max; ?>"/>
                    </div>
                    <div style="float: left;width: 24%;margin-left: 1%">
                        <span style="width: 50%">Comissão Venda (%):</span>
                        <input style="text-align:center" name="txtComissao_venda" id="txtComissao_venda" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="6" value="<?php echo $comissao_venda; ?>"/>
                    </div>
                    <div style="clear:both;height: 5px;"></div>
                    <div style="float: left;width: 50%">
                        <div style="width: 50%;float:left;">
                            <span style="width: 50%">Margem de Lucro (%):</span>
                            <input style="<?php
                            if ($formaPreco == 1) {
                                echo "display: none;";
                            }
                            ?>text-align:right" name="txtMargem_lucro" id="txtMargem_lucro" type="text" disabled class="input-medium" maxlength="100" value="<?php echo $margem_lucro; ?>"/>
                            <input style="<?php
                            if ($formaPreco == 0) {
                                echo "display: none;";
                            }
                            ?>text-align:right; font-size:14px; font-weight:bold" name="txtMargem_Lucro_Val" id="txtMargem_Lucro_Val" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $margem_lucro_val; ?>"/>
                        </div>
                        <div style="width: 48%;float:left;margin-left: 1%;">
                            <span>Margem de Lucro <?php echo $lang['prefix'];?>:</span>
                            <input style="margin-left:2%; text-align:right" name="txtValor_lucro" id="txtValor_lucro" type="text" disabled class="input-medium" maxlength="100" value="<?php echo $valor_lucro; ?>"/>
                        </div>
                    </div>
                    <div style="float: left;width: 49%;margin-left: 1%">
                        <div style="width: 50%;float:left;">
                            <span style="width: 50%">Impostos (%):</span>
                            <input style="text-align:right" id="porcImpostos" type="text" class="input-medium" maxlength="6" <?php echo $disabled; ?> />
                        </div>
                        <div style="width: 48%;float:left;margin-left: 1%;">
                            <span>Impostos <?php echo $lang['prefix'];?>:</span>
                            <input style="margin-left:2%; text-align:right" name="txtImpostos" id="txtImpostos" type="text" readonly class="input-medium" maxlength="100" value="<?php echo $impostos ?>"/>
                        </div>
                    </div>
                    <div style="clear:both;height: 5px;"></div>
                    <div style="float: left;width: 50%">
                        <div style="width: 50%;float:left;">
                            <span style="width: 50%">Frete (%):</span>
                            <input style="text-align:right" id="porcFrete" type="text" class="input-medium" maxlength="6" <?php echo $disabled; ?> />
                        </div>
                        <div style="width: 48%;float:left;margin-left: 1%;">
                            <span>Frete <?php echo $lang['prefix'];?>:</span>
                            <input style="margin-left:2%; text-align:right" name="txtFrete" id="txtFrete" type="text" readonly class="input-medium" maxlength="100" value="<?php echo $frete ?>"/>
                        </div>
                    </div>
                    <div style="float: left;width: 49%;margin-left: 1%">
                        <div style="width: 50%;float:left;">
                            <span style="width: 50%">Comissões (%):</span>
                            <input style="text-align:right" id="porcComissoes" type="text" class="input-medium" maxlength="6" <?php echo $disabled; ?> />
                        </div>
                        <div style="width: 48%;float:left;margin-left: 1%;">
                            <span>Comissões <?php echo $lang['prefix'];?>:</span>
                            <input style="margin-left:2%; text-align:right" name="txtComissoes" id="txtComissoes" type="text" readonly class="input-medium" maxlength="100" value="<?php echo $comissoes ?>"/>
                        </div>
                    </div>
                    <div style="clear:both;height: 5px;"></div>
                    <div style="float: left;width: 25%">
                        <span style="width: 50%">Valor Unit. Comercial:</span>
                        <input style="text-align:right; font-size:14px; font-weight:bold" name="txtPreco_venda" id="txtPreco_venda" type="text" <?php echo $disabled; ?> <?php
                        if ($formaPreco == 1) {
                            echo "readonly";
                        }
                        ?> class="input-medium" maxlength="100" value="<?php echo $preco_venda; ?>"/>
                    </div>
                    <div style="float: left;width: 25%; margin-left: 1%;">
                        <span>Alterar Valor no Pagamento:</span>
                        <select name="txtAlterarVal" id="txtAlterarVal" <?php echo $disabled; ?> style="width: 96%;">
                            <option value="0" <?php echo ($txtAlterarVal == "0" ? "SELECTED" : ""); ?>>NÂO</option>
                            <option value="1" <?php echo ($txtAlterarVal == "1" ? "SELECTED" : ""); ?>>SIM</option>
                        </select>
                    </div>
                    <div style="clear:both;height:10px;"></div>
                    <div>
                        <table width="99%">
                            <div style="height:35px; width:36%; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Último Reajuste:</b></div>
                            <div style="height:35px; width:30%; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Valor Unit. Comercial:</b></div>
                            <div style="height:35px; width:30%; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Margem de Lucro $:</b></div>
                        </table>
                        <div style="overflow:scroll; height:84px;">
                            <table width="99%">
                                <?php
                                if ($id != '') {
                                    $cur = odbc_exec($con, "select * from sf_produtos_historico_preco where id_produto = " . $id . " order by data_atual desc");
                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                        <div style="height:30px; width:38%; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left">
                                            <?php echo escreverDataHora($RFP['data_atual']); ?>
                                        </div>
                                        <div style="height:30px; width:31%; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo escreverNumero($RFP['preco_atual']); ?></div>
                                        <div style="height:30px; width:29%; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo escreverNumero($RFP['margem_lucro']); ?></div>
                                        <?php
                                    }
                                } ?>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab3" style="overflow:hidden; height:370px">
                    <div style="width: 75%;float:left;">
                        <span style="width: 50%">Localização:</span>
                        <select name="txtLocalizacao" id="txtLocalizacao" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                            <option value="null" >Selecione o local:</option>
                            <?php
                            $sql = "select id_local,nome_local from sf_produtos_locais ORDER BY nome_local";
                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP['id_local'] ?>"<?php
                                if (!(strcmp($RFP['id_local'], $localizacao))) {
                                    echo "SELECTED";
                                }
                                ?>><?php echo utf8_encode($RFP['nome_local']) ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div style="width:24%;float: left;margin-left: 1%;margin-top: 18px;">
                        <input name="txtNaoVendeSemEstoque" id="txtNaoVendeSemEstoque" <?php
                        echo $disabled;
                        if ($nao_vender_sem_estoque == "1") {
                            echo " CHECKED";
                        }
                        ?> value="1" type="checkbox" class="input-medium"/>
                        <span style="width: 50%">Não Vender Sem Estoque:</span>
                    </div>
                    <div style="clear:both;height:5px;"></div>
                    <div style="width: 34%;float:left;">
                        <span style="width: 50%">Estoque Mínimo:</span>
                        <input style="text-align:right" name="txtEstoque_minimo" id="txtEstoque_minimo" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $estoque_minimo; ?>"/>
                    </div>
                    <div style="width: 32%;float:left;margin-left: 1%">
                        <span style="width: 50%">Estoque Máximo:</span>
                        <input style="text-align:right" name="txtEstoque_maximo" id="txtEstoque_maximo" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $estoque_maximo; ?>"/>
                    </div>
                    <div style="width: 32%;float:left;margin-left: 1%">
                        <span style="width: 50%">Estoque Atual:</span>
                        <input style="text-align:right" name="txtEstoque_atual" id="txtEstoque_atual" type="text" disabled class="input-medium" maxlength="100" value="<?php echo $estoque_atual; ?>"/>
                    </div>
                    <div style="clear:both;height:5px;"></div>
                    <div style="width:100%; margin:10px 0">
                        <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px;">Previsão de Chegada</span>
                        <hr style="margin:2px 0; border-top:1px solid #ddd;width: 50%;">
                    </div>
                    <div style="width: 34%;float:left;">
                        <span style="width: 50%">Data:</span>
                        <input style="text-align:center" name="txtPrevisao_chegada" id="txtPrevisao_chegada" type="text" <?php echo $disabled; ?> class="datepicker" maxlength="100" value="<?php echo $previsao_chegada; ?>"/>
                    </div>
                    <div style="width: 32%;float:left;margin-left: 1%">
                        <span style="width: 50%">Quantidade:</span>
                        <input style="text-align:right" name="txtQuantidade_chegada" id="txtQuantidade_chegada" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $quantidade_chegada; ?>"/>
                    </div>
                </div>
                <div class="tab-pane" id="tab4" style="overflow:hidden; height:370px">
                    <div style="width: 34%;float:left;">
                        <span style="width: 50%">Unidade Padrão:</span>
                        <select name="txtUnidade_dimensao" id="txtUnidade_dimensao" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                            <option value="MT" <?php
                            if ($unidade_dimensao == "MT") {
                                echo "SELECTED";
                            }
                            ?>>Metros</option>
                            <option value="CM" <?php
                            if ($unidade_dimensao == "CM") {
                                echo "SELECTED";
                            }
                            ?>>Centímetros</option>
                            <option value="ML" <?php
                            if ($unidade_dimensao == "ML") {
                                echo "SELECTED";
                            }
                            ?>>Milímetros</option>
                        </select>
                    </div>
                    <div style="clear:both;height:5px;"></div>
                    <div style="width: 34%;float:left">
                        <span style="width: 50%">Peso Bruto (Kg):</span>
                        <input style="text-align:right" name="txtPeso_bruto" id="txtPeso_bruto" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $peso_bruto; ?>"/>
                    </div>
                    <div style="width: 32%;float:left;margin-left: 1%">
                        <span style="width: 50%">Peso Líquido (Kg):</span>
                        <input style="text-align:right" name="txtPeso_liquido" id="txtPeso_liquido" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $peso_liquido; ?>"/>
                    </div>
                    <div style="clear:both;height:5px;"></div>
                    <div style="width: 34%;float:left;">
                        <span style="width: 50%">Cor Padrão:</span>
                        <select name="txtP_Cor" id="txtP_Cor" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                            <option value="null" >Selecione a Cor:</option>
                            <?php
                            $sql = "select id_produto_cor,nome_produto_cor from sf_produtos_cor order by nome_produto_cor";
                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) {
                                ?>
                                <option value="<?php echo $RFP['id_produto_cor'] ?>"<?php
                                if (!(strcmp($RFP['id_produto_cor'], $p_cor))) {
                                    echo "SELECTED";
                                }
                                ?>><?php echo utf8_encode($RFP['nome_produto_cor']) ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div style="width: 32%;float:left;margin-left: 1%">
                        <span style="width: 50%">Tamanho Padrão:</span>
                        <select name="txtP_Tamanho" id="txtP_Tamanho" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                            <option value="null" >Selecione o Tamanho:</option>
                            <?php
                            $sql = "select id_produto_tamanho,nome_produto_tamanho from sf_produtos_tamanho order by nome_produto_tamanho";
                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) {
                                ?>
                                <option value="<?php echo $RFP['id_produto_tamanho'] ?>"<?php
                                if (!(strcmp($RFP['id_produto_tamanho'], $p_tamanho))) {
                                    echo "SELECTED";
                                }
                                ?>><?php echo utf8_encode($RFP['nome_produto_tamanho']) ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div style="clear:both;height:5px;"></div>
                    <div style="width:100%; margin:10px 0">
                        <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px;">Produto</span>
                        <hr style="margin:2px 0; border-top:1px solid #ddd;width: 50%;">
                    </div>
                    <div style="width: 34%;float:left">
                        <span style="width: 50%">Largura:</span>
                        <input style="text-align:right" name="txtDim_pro_largura" id="txtDim_pro_largura" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $dim_pro_largura; ?>"/>
                    </div>
                    <div style="width: 32%;float:left;margin-left: 1%">
                        <span style="width: 50%">Altura:</span>
                        <input style="text-align:right" name="txtDim_pro_altura" id="txtDim_pro_altura" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $dim_pro_altura; ?>"/>
                    </div>
                    <div style="width: 32%;float:left;margin-left: 1%">
                        <span style="width: 50%">Comprimento:</span>
                        <input style="text-align:right" name="txtDim_pro_comprimento" id="txtDim_pro_comprimento" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $dim_pro_comprimento; ?>"/>
                    </div>
                    <div style="clear:both;height:5px;"></div>
                    <div style="width:100%; margin:10px 0">
                        <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px;">Embalagem</span>
                        <hr style="margin:2px 0; border-top:1px solid #ddd;width: 50%;">
                    </div>
                    <div style="width: 34%;float:left">
                        <span style="width: 50%">Largura:</span>
                        <input style="text-align:right" name="txtDim_emb_largura" id="txtDim_emb_largura" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $dim_emb_largura; ?>"/>
                    </div>
                    <div style="width: 32%;float:left;margin-left: 1%">
                        <span style="width: 50%">Altura:</span>
                        <input style="text-align:right" name="txtDim_emb_altura" id="txtDim_emb_altura" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $dim_emb_altura; ?>"/>
                    </div>
                    <div style="width: 32%;float:left;margin-left: 1%">
                        <span style="width: 50%">Comprimento:</span>
                        <input style="text-align:right" name="txtDim_emb_comprimento" id="txtDim_emb_comprimento" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $dim_emb_comprimento; ?>"/>
                    </div>
                </div>
                <div class="tab-pane" id="tab5" style="overflow:hidden; height:370px">
                    <div style="width: 50%;float:left;">
                        <span style="width: 50%">EAN Unid. Tributável:</span>
                        <input name="txtOrigem" id="txtOrigem" <?php echo $disabled; ?> type="text" class="input-medium"  maxlength="170" value="<?php echo $origem; ?>"/>
                    </div>
                    <div style="clear:both;height:5px;"></div>
                    <div style="width: 50%;float:left;">
                        <span style="width: 50%">NCM:</span>
                        <input name="txtNcm" id="txtNcm" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="100" value="<?php echo $ncm; ?>"/>
                    </div>
                    <div style="width: 49%;float:left;margin-left: 1%">
                        <span style="width: 50%">Ex TIPI:</span>
                        <input name="txtSubstituicao_trib" id="txtSubstituicao_trib" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="100" value="<?php echo $substituicao_trib; ?>"/>
                    </div>
                    <div style="clear:both;height:5px;"></div>
                    <div style="width: 50%;float:left;">
                        <span style="width: 50%">CEST:</span>
                        <input name="txtCest" id="txtCest" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="100" value="<?php echo $cest; ?>"/>
                    </div>
                    <div style="width: 49%;float:left;margin-left: 1%">
                        <span style="width: 50%">CFOP:</span>
                        <select name='txtCfop' id='txtCfop' class="input-medium" <?php echo $disabled; ?> style="width:100%" maxlength="100">
                            <option value="">Selecione</option>
                            <?php
                            $cur2 = odbc_exec($con, "select id_nfe_cfop from sf_nfe_cfop") or die(odbc_errormsg());
                            while ($RFP2 = odbc_fetch_array($cur2)) {
                                ?>
                                <option value="<?php echo $RFP2['id_nfe_cfop'] ?>" <?php
                                if (!(strcmp($RFP2['id_nfe_cfop'], $cfop))) {
                                    echo "SELECTED";
                                }
                                ?>><?php echo utf8_encode($RFP2['id_nfe_cfop']) ?></option>
                                    <?php } ?>
                        </select>
                    </div>
                    <div style="clear:both;height:5px;"></div>
                    <div style="width: 50%;float:left;">
                        <span style="width: 50%">Unidade Tributável:</span>
                        <select name="txtUnidade_trib" id="txtUnidade_trib" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                            <option value="UN" <?php
                            if ($unidade_trib == "UN") {
                                echo "SELECTED";
                            }
                            ?>>Unitários</option>
                            <option value="GR" <?php
                            if ($unidade_trib == "GR") {
                                echo "SELECTED";
                            }
                            ?>>Gramas</option>
                            <option value="KG" <?php
                            if ($unidade_trib == "KG") {
                                echo "SELECTED";
                            }
                            ?>>Quilos</option>
                            <option value="ML" <?php
                            if ($unidade_trib == "ML") {
                                echo "SELECTED";
                            }
                            ?>>Mililitros</option>
                            <option value="LT" <?php
                            if ($unidade_trib == "LT") {
                                echo "SELECTED";
                            }
                            ?>>Litros</option>
                            <option value="CM" <?php
                            if ($unidade_trib == "CM") {
                                echo "SELECTED";
                            }
                            ?>>Centímetros</option>
                            <option value="MT" <?php
                            if ($unidade_trib == "MT") {
                                echo "SELECTED";
                            }
                            ?>>Metros</option>
                            <option value="MQ" <?php
                            if ($unidade_trib == "MQ") {
                                echo "SELECTED";
                            }
                            ?>>Metros Quadrados</option>
                            <option value="CX" <?php
                            if ($unidade_trib == "CX") {
                                echo "SELECTED";
                            }
                            ?>>Caixas</option>
                            <option value="PC" <?php
                            if ($unidade_comercial == "PC") {
                                echo "SELECTED";
                            }
                            ?>>Peças</option>
                        </select>
                    </div>
                    <div style="width: 49%;float:left;margin-left: 1%">
                        <span style="width: 50%">Quantidade Tributável:</span>
                        <input style="text-align:right" name="txtQuantidade_trib" id="txtQuantidade_trib" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="100" value="<?php echo $quantidade_trib; ?>"/>
                    </div>
                    <div style="clear:both;height:5px;"></div>
                    <div style="width: 50%;float:left;">
                        <span style="width: 50%">Valor Unitário Tributável:</span>
                        <input style="text-align:right" name="txtValor_unitario_trib" id="txtValor_unitario_trib" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="100" value="<?php echo $valor_unitario_trib; ?>"/>
                    </div>
                    <div style="width: 49%;float:left;margin-left: 1%">
                        <span style="width: 50%">Cenário</span><br>
                        <select name="txtCenario" id="txtCenario" class="input-medium" <?php echo $disabled; ?> style="width:87%">
                            <option value="null" >Selecione o Cenário:</option>
                            <?php
                            $sql = "select id_cenario,nome_cenario, id_produto from sf_nfe_cenarios where id_produto is null" . ($id !== "" ? " or id_produto = " . $id : "");
                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) {
                                ?>
                                <option value="<?php echo $RFP['id_cenario'] ?>"<?php
                                if (!(strcmp($RFP['id_cenario'], $cenario))) {
                                    echo "SELECTED";
                                }
                                ?> vIdProd="<?php echo $RFP['id_produto']; ?>"><?php echo utf8_encode($RFP['nome_cenario']) ?></option>
                            <?php } ?>
                        </select>
                        <button class="btn btn-info" type="button" onclick="AbrirBox(0)" <?php echo $disabled; ?>><span class=" ico-pencil-2 icon-white" style="font-size: 19px;padding-bottom: 5px;"></span></button>
                    </div>
                </div>
                <div class="tab-pane" id="tab6" style="overflow:hidden; height:370px">
                    <table width="100%">
                        <div style="height:35px; width:91px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Data</b></div>
                        <div style="height:35px; width:154px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Fornecedor</b></div>
                        <div style="height:35px; width:123px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Valor unitário</b></div>
                        <div style="height:35px; width:120px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Quantidade</b></div>
                        <div style="height:35px; width:124px; background:#E9E9E9; line-height:35px; margin-left:1px; padding-left:8px; float:left"><b>Valor Total</b></div>
                    </table>
                    <div style="overflow:scroll; height:332px;">
                        <table width="100%">
                            <?php
                            if ($id != '') {
                                $cur = odbc_exec($con, "select * from sf_vendas_itens vi inner join sf_vendas v on vi.id_venda = v.id_venda inner join sf_fornecedores_despesas f on v.cliente_venda = f.id_fornecedores_despesas where cov = 'C' and status = 'Aprovado' and vi.produto = " . $id . " order by data_venda desc");
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <div style="height:30px; width:100px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left">
                                        <?php echo escreverDataHora($RFP['data_venda']); ?>
                                    </div>
                                    <div style="height:30px; width:156px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo utf8_encode($RFP['razao_social']); ?></div>
                                    <div style="height:30px; width:131px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php
                                        if ($RFP['quantidade'] > 0) {
                                            echo escreverNumero(($RFP['valor_total'] / $RFP['quantidade']), 1);
                                        }
                                        ?></div>
                                    <div style="height:30px; width:122px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo escreverNumero($RFP['quantidade'], 0, 4); ?></div>
                                    <div style="height:30px; width:110px; background:#FFF; line-height:30px; margin-left:1px; padding-left:3px; float:left"><?php echo escreverNumero($RFP['valor_total'], 1); ?></div>
                                    <div style="clear:both"></div>
                                    <?php
                                }
                            } ?>
                        </table>
                    </div>
                </div>
                <div class="tab-pane <?php echo $active7; ?>" id="tab7" style="overflow:hidden; height:370px">
                    <div style="width: 67%;float:left;">
                        <span style="width: 50%">Produto:</span>
                        <select name="txtPrdMP" id="txtPrdMP" class="select" <?php echo $disabled; ?> style="width:100%">
                            <option value="null" >Selecione o Produto:</option>
                            <?php
                            $sql = "select conta_produto,descricao from dbo.sf_produtos where tipo = 'P' and inativa = 0 and conta_produto <> '" . $id . "' ORDER BY descricao";
                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) {
                                ?>
                                <option value="<?php echo $RFP['conta_produto'] ?>"><?php echo utf8_encode($RFP['descricao']) ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div style="width: 20%;float:left;margin-left: 1%">
                        <span style="width: 50%">Quantidade:</span>
                        <input style="text-align:right" name="txtQuantMt" id="txtQuantidadeEnt" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="100" value="1,0000"/>
                    </div>
                    <div style="width: 11%;float:left;margin-left: 1%;margin-top: 15px;">
                        <button class="btn btn-success" <?php echo $disabled; ?> type="button" name="bntSaveMP" onClick="novaLinha()"><span class="ico-checkmark"></span> Gravar</button>
                    </div>
                    <div style="clear:both;height:5px;"></div>
                    <table width="100%" cellpadding="8" border="1" bordercolor="#FFF" style="background:#E9E9E9; font-size:12px">
                        <tr>
                            <td style="line-height:18px"><b>Produto</b></td>
                            <td style="line-height:18px; width:100px"><b>Quantidade</b></td>
                            <td style="line-height:18px; width:100px"><b>Custo</b></td>
                            <td style="line-height:18px; width:53px"><b>Ação</b></td>
                        </tr>
                    </table>
                    <div style="overflow-y:scroll; height:232px">
                        <table id="listMatPrim" width="100%" cellpadding="3" border="1" bordercolor="#FFF" style="font-size:12px">
                            <?php
                            if ($id != "") {
                                $conta = 1000;
                                $cur = odbc_exec($con, "select id_prodmp,id_materiaprima,descricao,quantidademp,(select top 1 valor_total/quantidade from dbo.sf_vendas v inner join sf_vendas_itens i on v.id_venda = i.id_venda where cov = 'C' and status = 'Aprovado' and produto = id_materiaprima order by data_venda desc) custo from sf_produtos_materiaprima inner join sf_produtos on id_materiaprima = conta_produto where id_produtomp = " . $id . " order by descricao");
                                while ($RFP = odbc_fetch_array($cur)) {
                                    ?>
                                    <tr id="list_<?php echo $conta; ?>">
                                    <input type="hidden" name="prod_id[]" value="<?php echo $RFP["id_prodmp"]; ?>">
                                    <input type="hidden" name="prod_cod[]" value="<?php echo $RFP["id_materiaprima"]; ?>">
                                    <input type="hidden" name="prod_qnt[]" value="<?php echo escreverNumero($RFP["quantidademp"], 0, 4); ?>">
                                    <td style="line-height:23px"><?php echo utf8_encode($RFP["descricao"]); ?></td>
                                    <td style="line-height:23px; width:100px"><?php echo escreverNumero($RFP["quantidademp"], 0, 4); ?></td>
                                    <td style="line-height:23px; width:100px"><?php echo escreverNumero(($RFP["quantidademp"] * $RFP["custo"])); ?></td>
                                    <td style="width:46px; text-align:center"><a onClick="removeLinha(<?php echo $conta; ?>)" href="javascript:void(0)"><img src="../../img/1365123843_onebit_33 copy.PNG" width="16" height="16"/></a></td>
                                    </tr>
                                    <?php
                                    $conta = $conta + 1;
                                }
                            }
                            ?>
                        </table>
                    </div>
                    <div style="clear:both;height:5px;"></div>
                    <div style="width:13%;float: left;margin-top: 18px;">
                        <input name="txtProducao" id="txtProducao" <?php echo $disabled; ?> type="checkbox" <?php
                        if ($producao == "1") {
                            echo "CHECKED";
                        }
                        ?> value="1" class="input-medium"/>
                        <span style="width: 50%">Produção:</span>
                    </div>
                    <div style="width: 20%;float:left;margin-left: 1%">
                        <span style="width: 50%">Tempo de Produção:</span>
                        <input style="text-align:right" name="txtTime" id="txtTime" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="4" value="<?php echo $time; ?>"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="frmfoot">
            <input type="hidden" id="x" name="x"/>
            <input type="hidden" id="y" name="y"/>
            <input type="hidden" id="w" name="w"/>
            <input type="hidden" id="h" name="h"/>
            <input type="hidden" id="src" name="src"<?php
            if ($imagemx != "" && file_exists("./../../Produtos/" . $contrato . "/" . $imagemx)) {
                echo " value=\"" . $imagemx . "\"";
            } ?>/>
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" onclick="return validar();" name="bntSave" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox(1)" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK" ><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else { ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/charts.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/ckeditor/ckeditor.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/jcrop/jquery.Jcrop.min.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type="text/javascript">

        $("#txtData_inativo, #txtPrevisao_chegada, #txtDataEnt").mask(lang["dateMask"]);        
        $("#txtQuantidade_comercial, #txtEstoque_minimo, #txtEstoque_maximo, #txtQuantidade_chegada, #txtDim_pro_comprimento, " +
        "#txtDim_pro_altura, #txtDim_pro_largura, #txtDim_emb_comprimento, #txtDim_emb_altura, #txtDim_emb_largura, #txtQuantidade_trib, " + 
        "#txtPreco_venda, #txtValor_unitario_trib, #txtValorEnt").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});                
        $("#txtPeso_bruto, #txtPeso_liquido").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"], centsLimit: 3});
        $("#txtQuantidadeEnt").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"], centsLimit: 4});        
        $("#txtMargem_lucro, #porcImpostos, #porcFrete, #porcComissoes, #txtDesc_Max, #txtComissao_venda, #txtMargem_Lucro_Val").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], suffix: "%"});

        var conta = 1;
        function novaLinha() {
            if ($("#txtPrdMP").val() !== "" && $("#txtQuantidadeEnt").val() !== "") {
                $.getJSON("conta_a.ajax.php", {txtConta: $("#txtPrdMP").val(), txtQtd: $("#txtQuantidadeEnt").val(), ajax: "true"}, function (j) {
                    conta = conta + 1;
                    $("#listMatPrim").append("<tr id=\"list_" + conta + "\"><input type=\"hidden\" name=\"prod_id[]\" value=\"\"><input type=\"hidden\" name=\"prod_cod[]\" value=\"" + $("#txtPrdMP").val() + "\"><input type=\"hidden\" name=\"prod_qnt[]\" value=\"" + $("#txtQuantidadeEnt").val() + "\"><td style=\"line-height:23px\">" + $("#txtPrdMP option:selected").text() + "</td><td style=\"line-height:23px; width:100px\">" + $("#txtQuantidadeEnt").val() + "</td><td style=\"line-height:23px; width:100px\">" + j[0].custo + "</td><td style=\"width:46px; text-align:center\"><a onClick=\"removeLinha(" + conta + ")\" href=\"javascript:void(0)\"><img src=\"../../img/1365123843_onebit_33 copy.PNG\" width=\"16\" height=\"16\"/></a></td></tr>");
                });
            }
        }
        
        function removeLinha(id) {
            $("#list_" + id).remove();
        }

        $("#txtSemEntrada").change(function () {
            if ($("#txtSemEntrada").val() === 0) {
                $("#matpri").hide();
            } else {
                $("#matpri").show();
            }
        });
        $("#txtSemEntrada").change();
        
        $(document).ready(function () {
            var total = textToNumber($("#txtPreco_venda").val());
            var impos = textToNumber($("#txtImpostos").val());
            var frete = textToNumber($("#txtFrete").val());            
            var comis = textToNumber($("#txtComissoes").val());             
            var subTot = (impos / total) * 100;
            $("#porcImpostos").val(numberFormat(subTot > 0 ? subTot : 0) + "%");                        
            subTot = (frete / total) * 100;            
            $("#porcFrete").val(numberFormat(subTot > 0 ? subTot : 0) + "%");                                
            subTot = (comis / total) * 100;            
            $("#porcComissoes").val(numberFormat(subTot > 0 ? subTot : 0) + "%");
        });
        
        function porcImpostos() {
            var valor = textToNumber($("#porcImpostos").val());
            var lucro = textToNumber($("#txtValor_lucro").val());
            var venda = textToNumber($("#txtPreco_venda").val());
            $("#txtImpostos").val(numberFormat(venda * (valor / 100)));
        }
        function porcFrete() {
            var valor = textToNumber($("#porcFrete").val());
            var lucro = textToNumber($("#txtValor_lucro").val());
            var venda = textToNumber($("#txtPreco_venda").val());
            $("#txtFrete").val(numberFormat(venda * (valor / 100)));
        }
        function porcComissoes() {
            var valor = textToNumber($("#porcComissoes").val());
            var lucro = textToNumber($("#txtValor_lucro").val());
            var venda = textToNumber($("#txtPreco_venda").val());
            $("#txtComissoes").val(numberFormat(venda * (valor / 100)));
        }
        function porcMargemLucro() {
            var venda = textToNumber($("#txtPreco_venda").val());
            var custo = textToNumber($("#txtPreco_custo").val());
            var impos = textToNumber($("#txtImpostos").val());
            var frete = textToNumber($("#txtFrete").val());
            var comis = textToNumber($("#txtComissoes").val());
            $("#txtValor_lucro").val(numberFormat(venda - (custo + impos + frete + comis)));
            if (custo > 0) {
                $("#txtMargem_lucro").val(numberFormat(((venda - (custo + impos + frete + comis)) / custo) * 100) + "%");
            } else {
                $("#txtMargem_lucro").val(numberFormat(100) + "%");
            }
        }
        $("#txtPreco_venda").blur(function () {
            porcImpostos();
            porcFrete();
            porcComissoes();
            porcMargemLucro();
        });
        $("#porcImpostos").blur(function () {
            porcImpostos();
            porcMargemLucro();
        });
        $("#porcFrete").blur(function () {
            porcFrete();
            porcMargemLucro();
        });
        $("#porcComissoes").blur(function () {
            porcComissoes();
            porcMargemLucro();
        });
        var jcrop_api = "";
        $(document).ready(function () {
            $("#txtMargem_Lucro_Val").change(function () {
                $("#txtPreco_venda").val(numberFormat(((100 + textToNumber($("#txtMargem_Lucro_Val").val())) / 100) * textToNumber($("#txtPreco_custo").val())));
            });
            var ckeditor = CKEDITOR.replace('ckeditor', {removePlugins: 'elementspath'});

            $("#txtImag").change(function () {
                var file = this.files[0];
                var imagefile = file.type;
                var imagesize = file.size;
                var match = ["image/jpeg", "image/png", "image/jpg"];
                if (!((imagefile === match[0]) || (imagefile === match[1]) || (imagefile === match[2]))) {
                    bootbox.alert("Este arquivo não é uma imagem válida!");
                    $("#picImag").attr("src", "./../../img/img.png");
                    return false;
                } else if (imagesize > 2048000) {
                    bootbox.alert("Tamanho máximo de imagem de 2M!");
                    $("#picImag").attr("src", "./../../img/img.png");
                    return false;                    
                } else {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded;
                    reader.readAsDataURL(this.files[0]);
                }
            });
            $("#delImag").click(function () {
                $("#picImag").attr("src", "./../../img/img.png");
                $("#picImag").height("auto").width("auto");
                $("#txtImag,#src,#x,#y,#w,#h").val("");
                $("#delImag").hide();
                if (jcrop_api !== "") {
                    jcrop_api.destroy();
                }
            });
<?php if ($disabled == "") { ?>
                $("#picImag").click(function () {
                    $("#txtImag").click();
                }).show();
<?php } ?>
            });
        function imageIsLoaded(e) {
            $("#delImag").show();
            $("#src").val(e.target.result);
            $("#picImag").attr("src", e.target.result);
            $("#picImag").Jcrop({aspectRatio: 1, setSelect: [20, 20, 100, 100], onSelect: updateCoords}, function () {
                jcrop_api = this;
            });
        }
        
        function updateCoords(c) {
            $("#x").val(c.x);
            $("#y").val(c.y);
            $("#w").val(c.w);
            $("#h").val(c.h);
        }

        $(window).keydown(function (event) {
            if (event.ctrlKey && event.keyCode === 74) {
                event.preventDefault();
            }
        });
        
        $("#frmEnviaDados").validate({
            errorPlacement: function (error, element) {
                return true;
            }, rules: {
                txtDescricao: {required: true},
                txtGrupo: {required: true},
                txtCodigo_barra: {number: true}
            }
        });
        
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $("#picImag").attr("src", e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        
        $(document).ready(function () {
            if ($("#txtInativo").is(":checked")) {
                $("#data_hide").show();
            }
        });
        
        $("#txtInativo").click(function () {
            if ($(this).is(":checked")) {
                $("#data_hide").show();
            } else {
                $("#data_hide").hide();
                $("#txtData_inativo").val("");
            }
        });

        $("#txtCodigo_barra").change(function () {
            if ($("#txtCodigo_barra").val() !== "") {
                verificaCodigoBarras();
            }
        });

        $("#txtForma_Preco").change(function () {
            if ($("#txtForma_Preco").val() === "1") {
                $("#txtMargem_lucro").hide();
                $("#txtMargem_Lucro_Val").show();
                $("#txtPreco_venda").attr("readonly", true);
            } else {
                $("#txtMargem_lucro").show();
                $("#txtMargem_Lucro_Val").hide();
                $("#txtPreco_venda").attr("readonly", false);
            }
        });

        function validar() {
            if ($('#txtSemEntrada').val() === "1" && $('#listMatPrim tbody tr').length === 0) {
                bootbox.alert("Para cadastrar o tipo Produto, deve ser informada pelo menos uma matéria prima, caso não seja composto por matéria(s) prima(s) altere o tipo para Mercadoria!");
                return false;
            } else if (CKEDITOR.instances.ckeditor.getData().length > 1024) {
                bootbox.alert("O cadastro de descrição não pode conter mais de 128 caracteres.");
                return false;
            } else if ($("#txtCodigo_interno").val().length > 10) {
                bootbox.alert("O código não pode passar de 10 caracteres.");
                return false;
            } else if ($("#txtEstoque_minimo").val().length > 24) {
                bootbox.alert("O valor de estoque minimo não pode passar de 24 caracteres.");
                return false;
            } else if ($("#txtEstoque_maximo").val().length > 24) {
                bootbox.alert("O valor de estoque minimo não pode passar de 24 caracteres.");
                return false;
            } else if ($("#txtCest").val().length > 10) {
                bootbox.alert("O valor de CEST não pode passar de 10 caracteres.");
                return false;
            }
            return true;
        }
        
        function verificaCodigoBarras() {
            $.getJSON("Produtos_controller.php", {buscaCodigoBarra: $("#txtCodigo_barra").val()}, function (j) {
                if (j !== null) {
                    if (j.length > 0) {
                        bootbox.alert("Codigo de Barras já cadastrado para o produto: " + j[0].descricao);
                        $(".bootbox").css("top", "200px");
                        $("#txtCodigo_barra").val("");
                    }
                }
            });
        }
        
        function AbrirBox(id) {
            if ($("#txtId").val() !== "") {
                var cenario = "";
                if ($("#txtCenario").val() !== "null" && $("#txtCenario").val() !== "") {
                    cenario = "&id=" + $("#txtCenario").val();
                    cenario += $("#txtCenario option:selected").attr('vidprod') !== "" ? "&tpCenario=A" : "&tpCenario=N";
                }
                $("body").append("<div id='frmCenario' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:677px; height:525px; position:absolute; top:50%; left:50%; margin-left:-338px; margin-top:-282px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:525px' src='./../NFE/FormCenarios.php?IdProduto=" + $("#txtId").val() + cenario + "'></iframe></div></div></div></div></div>");
            } else {
                bootbox.alert("Salve o produto antes de editar o cenário.");
            }
        }
        
        function FecharBox(tela) {
            $("#frmCenario").remove();
            if (tela === 3) {
                location.reload();
            }
        }
                
    </script>
    <?php odbc_close($con); ?>
</body>
