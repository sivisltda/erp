<?php
include '../../Connections/configini.php';
include '../../util/util.php';
$disabled = 'disabled';
$active1 = 'active';
$mdl_clb_ = returnPart($_SESSION["modulos"], 12);
$mdl_seg_ = returnPart($_SESSION["modulos"], 14);

if (isset($_POST['bntSave'])) {

    $qtd_dias_acesso = "0";
    $qtd_intervalo_acesso = "0";
    $qtd_limite_acesso = "0";
    $qtd_limite_acesso_min = "0";
    $horario = '';
    $horarioExterno = '';

    if ($_POST['txtsys'] == 'null') {
        $horario = 'null';
        $horarioExterno = 'null';
    } else {
        $horario = utf8_decode($_POST['txtHorario']);
        $horarioExterno = utf8_decode($_POST['txtHorarioExterno']);
    }

    if ($_POST['txtTipoAcesso'] == "E") {
        $qtd_dias_acesso = valoresNumericos('txtQtdDiasAcesso');
        $qtd_intervalo_acesso = valoresNumericos('txtQtdIntervaloAcesso');
    } elseif ($_POST['txtTipoAcesso'] == "C") {
        $qtd_dias_acesso = valoresNumericos('txtQtdDiasAcesso');
    }

    if (valoresCheck('txtLimitar_Acesso') == "1") {
        $qtd_limite_acesso = valoresNumericos('txtQtdLimiteAcesso');
        $qtd_limite_acesso_min = valoresNumericos('txtQtdLimiteAcessoMin');
    }

    if (is_numeric($_POST['txtId'])) {
        $query = "set dateformat dmy;UPDATE sf_produtos SET " .
                "descricao = " . valoresTexto('txtDescricao') .
                ",tipo = 'C' " .
                ",inativa = " . valoresCheck('txtBloqueado') .
                ",conta_movimento = " . utf8_decode($_POST['txtGrupo']) .
                ",preco_venda = " . valoresNumericos('txtPreco_venda') .
                ",favorito = " . valoresCheck('txtFavorito') .
                ",id_turno = " . $horario .
                ",id_turno_externo = " . $horarioExterno .
                ",desc_bloqueado = " . valoresCheck('txtDesc_Bloqueado') .
                ",parcelar = " . valoresCheck('txtParcelar') .
                ",tp_valid_acesso = '" . utf8_decode($_POST['txtTipoAcesso']) .
                "',limitar_acesso = " . valoresCheck('txtLimitar_Acesso') .
                ",necessita_turma = " . valoresCheck('txtNecessita_turma') .
                ",qtd_limit_acesso = " . $qtd_limite_acesso .
                ",qtd_limit_acesso_min = " . $qtd_limite_acesso_min .
                ",qtd_dias_acesso = " . $qtd_dias_acesso .
                ",qtd_intervalo_acesso = " . $qtd_intervalo_acesso .
                ",id_ambiente = " . valoresSelect('txtAmbiente') .
                ",zerar_domingo = " . valoresCheck('txtZerarDomingo') .
                ",mensalidade_vencida = " . valoresCheck('txtMensalidade_vencida') .
                ",mostrar_site = " . valoresCheck('txtMostrar_site') .
                ",bloq_congelamento = " . valoresCheck('txtBloqCongelamento') .
                ",mensagem_site = " . valoresTexto('ck_msg') .
                ",descricao_site = " . valoresTexto('ck_desc') .
                ",conta_produto_adesao = " . valoresSelect('txtServico') .
                ",convenios_site = " . valoresSelect('txtConvenios') .
                ",convenios_meses_site = " . valoresNumericos('txtConveniosMes') .
                ",valor_comissao_venda = " . valoresNumericos2(str_replace("%", "", $_POST['txtComissaoPlan'])) .
                ",valor_custo_medio = " . valoresNumericos('txtCustoPlan') .
                ",valor_limite = " . valoresNumericos('txtValorLimite') .
                ",segMin = " . valoresNumericos('segMin') .
                ",segMax = " . valoresNumericos('segMax') .
                ",inativar_renovacao = " . valoresCheck('txtNRenovarAuto') .
                ",adesao_tipo = " . valoresSelect('txtAdesaoTipo') .
                ",adesao_intervalo = " . valoresSelect('txtAdesaoIntervalo') .                
                ",adesao_soma = " . valoresCheck('txtAdesaoSoma') .               
                ",grupo_cidades = " . valoresSelect('txtGrupoCidades') .                
                ",grupo_fabricantes = " . valoresSelect('txtGrupoFabricantes') .                
                ",parcelar_site = " . valoresCheck('txtParcelarSite') .                
                ",ano_piso = " . valoresNumericos('txtLimiteAnos') .                 
                ",cota_defaut = " . valoresNumericos('txtCotaDefaut') .                 
                ",cota_defaut_tp = " . valoresNumericos('txtProdSinal') .
                ",tempo_producao = " . valoresNumericos("txtTempoProducao") .
                " WHERE conta_produto = " . $_POST['txtId'];
        //echo $query;exit();
        odbc_exec($con, $query) or die(odbc_errormsg());
        if (is_numeric($_POST['txtId'])) {
            $prod_id = $_POST['prod_id'];
            $prod_cod = $_POST['prod_cod'];
            $notIn = 0;
            if (is_array($prod_id)) {
                for ($i = 0; $i < sizeof($prod_id); $i++) {
                    if (is_numeric($prod_id[$i])) {
                        $notIn = $notIn . "," . $prod_id[$i];
                    }
                }
                odbc_exec($con, "DELETE FROM sf_produtos_materiaprima WHERE id_produtomp = " . $_POST['txtId'] . " and id_prodmp not in (" . $notIn . ")");
                for ($i = 0; $i < sizeof($prod_cod); $i++) {
                    if (is_numeric($prod_id[$i])) {
                        odbc_exec($con, "UPDATE sf_produtos_materiaprima set id_materiaprima = " . $prod_cod[$i] . ",quantidademp = 1 WHERE id_prodmp = " . $prod_id[$i]);
                    } else {
                        odbc_exec($con, "INSERT INTO sf_produtos_materiaprima(id_produtomp,quantidademp,id_materiaprima) VALUES (" . $_POST['txtId'] . ",1," . $prod_cod[$i] . ")");
                    }
                }
            }
        }        
        $plan_id = $_POST['plan_id'];
        $plan_num = $_POST['plan_num'];
        $plan_val = $_POST['plan_val'];
        $plan_des = $_POST['plan_des'];
        $plan_com = $_POST['plan_com'];
        $notIn = '0';
        for ($i = 0; $i < sizeof($plan_id); $i++) {
            if (is_numeric($plan_id[$i])) {
                $notIn .= "," . $plan_id[$i];
            }
        }
        odbc_exec($con, "delete from sf_produtos_parcelas where id_produto = " . $_POST['txtId'] . " and id_parcela not in (" . $notIn . ")");
        for ($i = 0; $i < sizeof($plan_id); $i++) {
            if (is_numeric($plan_id[$i])) {
                $q1 = "update sf_produtos_parcelas set " .
                        "parcela = " . valoresNumericos2($plan_num[$i]) .
                        ",valor_unico = " . valoresNumericos2($plan_val[$i]) .
                        ",valor_desconto = " . valoresNumericos2($plan_des[$i]) .
                        ",valor_comissao = " . valoresNumericos2($plan_com[$i]) .
                        " where id_parcela = " . $plan_id[$i];
                odbc_exec($con, $q1);
            } else {
                $q1 = "insert into sf_produtos_parcelas(id_produto,parcela,valor_unico,valor_desconto,valor_comissao) values(" . $_POST['txtId'] . "," .
                        valoresNumericos2($plan_num[$i]) .
                        "," . valoresNumericos2($plan_val[$i]) .
                        "," . valoresNumericos2($plan_des[$i]) .
                        "," . valoresNumericos2($plan_com[$i]) . ")";
                odbc_exec($con, $q1);
            }
        }
    } else {
        $query = "INSERT INTO sf_produtos(descricao,tipo,unidade_comercial,quantidade_comercial,inativa,conta_movimento,favorito,preco_venda,editavel,id_turno,id_turno_externo,desc_bloqueado,parcelar,
                tp_valid_acesso,limitar_acesso,necessita_turma,qtd_limit_acesso,qtd_limit_acesso_min,qtd_dias_acesso,qtd_intervalo_acesso, id_ambiente, zerar_domingo, mensalidade_vencida,mostrar_site,bloq_congelamento,mensagem_site,
                descricao_site, conta_produto_adesao, convenios_site, convenios_meses_site, valor_custo_medio, valor_limite, segMin, segMax, valor_comissao_venda, inativar_renovacao, adesao_tipo, adesao_intervalo, adesao_soma, 
                grupo_cidades,grupo_fabricantes,parcelar_site,ano_piso,cota_defaut, cota_defaut_tp, tempo_producao) VALUES(" .
                valoresTexto('txtDescricao') . ",'C','UN',1.00," . valoresCheck('txtBloqueado') . "," . utf8_decode($_POST['txtGrupo']) . "," . valoresCheck('txtFavorito') . "," .
                valoresNumericos('txtPreco_venda') . ",0," . $horario . "," . $horarioExterno . "," . valoresCheck('txtDesc_Bloqueado') . "," . valoresCheck('txtParcelar') . ",'" .
                utf8_decode($_POST['txtTipoAcesso']) . "', " . valoresCheck('txtLimitar_Acesso') . "," . valoresCheck('txtNecessita_turma') . "," . valoresNumericos('txtQtdLimiteAcesso') . "," . valoresNumericos('txtQtdLimiteAcessoMin') . "," . $qtd_dias_acesso . "," . $qtd_intervalo_acesso . "," . 
                valoresSelect('txtAmbiente') . "," . valoresCheck('txtZerarDomingo') . "," . valoresCheck('txtMensalidade_vencida') . "," . valoresCheck('txtMostrar_site') . "," . valoresCheck('txtBloqCongelamento') . "," . valoresTexto('ck_msg') . "," . valoresTexto('ck_desc') . "," .
                valoresSelect('txtServico') . "," . valoresSelect('txtConvenios') . "," . valoresNumericos('txtConveniosMes') . "," . valoresNumericos('txtCustoPlan') . "," . valoresNumericos('txtValorLimite') . "," . valoresNumericos('segMin') . "," . valoresNumericos('segMax') . "," .
                valoresNumericos2(str_replace("%", "", $_POST['txtComissaoPlan'])) . "," . valoresCheck('txtNRenovarAuto') . "," . valoresSelect('txtAdesaoTipo') . "," . valoresSelect('txtAdesaoIntervalo') . "," . valoresCheck('txtAdesaoSoma') . "," . 
                valoresSelect('txtGrupoCidades') . "," . valoresSelect('txtGrupoFabricantes') . "," . valoresCheck('txtParcelarSite') . "," . valoresNumericos('txtLimiteAnos') . "," . valoresNumericos('txtCotaDefaut') . "," . valoresNumericos('txtProdSinal') . "," . 
                valoresNumericos("txtTempoProducao") . ")";
        //echo $query; exit;
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 conta_produto from sf_produtos where conta_produto not in (466) order by conta_produto desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
        if (is_numeric($_POST['txtId'])) {
            $prod_cod = $_POST['prod_cod'];
            for ($i = 0; $i < sizeof($prod_cod); $i++) {
                odbc_exec($con, "INSERT INTO sf_produtos_materiaprima(id_produtomp,quantidademp,id_materiaprima) VALUES (" . $_POST['txtId'] . ",1," . $prod_cod[$i] . ")");
            }
        }        
        $plan_id = $_POST['plan_id'];
        $plan_num = $_POST['plan_num'];
        $plan_val = $_POST['plan_val'];
        $plan_des = $_POST['plan_des'];
        $plan_com = $_POST['plan_com'];
        for ($i = 0; $i < sizeof($plan_id); $i++) {
            $q1 = "insert into sf_produtos_parcelas(id_produto,parcela,valor_unico,valor_desconto,valor_comissao) values(" . $_POST['txtId'] . "," .
                    valoresNumericos2($plan_num[$i]) . "," . valoresNumericos2($plan_val[$i]) . "," . valoresNumericos2($plan_des[$i]) . "," . valoresNumericos2($plan_com[$i]) . ")";
            odbc_exec($con, $q1);
        }
    }    
    if (isset($_POST['txtTipoVeiculo'])) {
        $inValue = "0";
        for ($i = 0; $i < count($_POST['txtTipoVeiculo']); $i++) {
            if ($_POST['txtTipoVeiculo'][$i] != '') {
                $inValue .= "," . $_POST['txtTipoVeiculo'][$i];
            }
        }
        odbc_exec($con, "delete from sf_produtos_veiculo_tipos where id_produto = " . $_POST['txtId'] . " and id_tipo not in (" . $inValue . ")") or die(odbc_errormsg());        
        for ($i = 0; $i < count($_POST['txtTipoVeiculo']); $i++) {        
            odbc_exec($con, "IF NOT EXISTS (SELECT id_produto, id_tipo FROM sf_produtos_veiculo_tipos WHERE id_produto = " . $_POST['txtId'] . " AND id_tipo = " . $_POST['txtTipoVeiculo'][$i] . ")
            BEGIN INSERT INTO sf_produtos_veiculo_tipos(id_produto, id_tipo) VALUES (" . $_POST['txtId'] . "," . $_POST['txtTipoVeiculo'][$i] . "); END");
        }
    }    
}

if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_produtos_parcelas where id_produto =  " . $_POST['txtId'] . ";
                         DELETE FROM sf_produtos WHERE conta_produto = " . $_POST['txtId']);
        $cur = odbc_exec($con, "select conta_produto from sf_produtos where conta_produto =" . $_POST['txtId']);
        $idProd = 0;
        while ($RFP = odbc_fetch_array($cur)) {
            $idProd = $RFP['conta_produto'];
        }
        if ($idProd != 0) {
            echo "<script>alert('Impossivel excluir, Registro esta sendo usado!');</script>";
        } else {
            echo "<script>alert('Registro excluido com sucesso!');window.top.location.href = 'Planos.php';</script>";
        }
    }
}

if (isset($_POST['bntReplicar']) && is_numeric($_POST['txtId']) && is_numeric($_POST['txtGrupo'])) {
    $query = "insert into sf_produtos_materiaprima(id_produtomp, quantidademp, id_materiaprima)
    select conta_produto, quantidademp, id_materiaprima 
    from sf_produtos_materiaprima
    inner join sf_produtos on id_produtomp <> 0 
    where id_produtomp = " . $_POST['txtId'] . " and conta_movimento = " . $_POST['txtGrupo'] . " 
    and conta_produto not in (" . $_POST['txtId'] . ")";
    odbc_exec($con, $query) or die(odbc_errormsg());
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

$tpAtualizacao = "";
$pix_chave = "";
$cur = odbc_exec($con, "select adm_atualizar_mens, seg_ano_veiculo_piso, seg_cota_defaut, PIX_CHAVE from sf_configuracao where id = 1");
while ($RFP = odbc_fetch_array($cur)) {
    $tpAtualizacao = ($RFP['adm_atualizar_mens'] > 0 ? "(Exceto as Vencidas)" : "");
    $cotaDefaut = $RFP['seg_cota_defaut'];  
    $descontotp = "0";
    $limiteAnos = escreverNumero($RFP['seg_ano_veiculo_piso']);
    $pix_chave = $RFP['PIX_CHAVE'];
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $classEnable = "disablePlan";
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_produtos where conta_produto =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['conta_produto'];
        $descricao = utf8_encode($RFP['descricao']);
        $grupo = $RFP['conta_movimento'];
        $preco_venda = escreverNumero($RFP['preco_venda']);
        $prod_favorito = $RFP['favorito'];
        $horario = $RFP['id_turno'];
        $horarioExterno = $RFP['id_turno_externo'];
        $inativa = $RFP['inativa'];
        $desc_bloqueado = $RFP['desc_bloqueado'];
        $parcelar = $RFP['parcelar'];
        $tp_valid_acesso = $RFP['tp_valid_acesso'];
        $qtd_dias_acesso = $RFP['qtd_dias_acesso'];
        $qtd_intervalo_acesso = $RFP['qtd_intervalo_acesso'];
        $limitar_acesso = $RFP['limitar_acesso'];
        $zerar_domingo = $RFP['zerar_domingo'];
        $necessita_turma = $RFP['necessita_turma'];
        $mostrar_site = $RFP['mostrar_site'];
        $texto_desc = $RFP['descricao_site'];
        $texto_mensagem = $RFP['mensagem_site'];
        $mensalidade_vencida = $RFP['mensalidade_vencida'];
        $ambiente = $RFP['id_ambiente'];
        $qtd_limite_acesso = $RFP['qtd_limit_acesso'];
        $qtd_limite_acesso_min = $RFP['qtd_limit_acesso_min'];
        $comissao_venda = escreverNumero($RFP["valor_comissao_venda"]);
        $custo = escreverNumero($RFP["valor_custo_medio"]);
        $segMin = escreverNumero($RFP["segMin"]);
        $segMax = escreverNumero($RFP["segMax"]);
        $idServico = $RFP['conta_produto_adesao'];
        $convenios_site = $RFP['convenios_site'];
        $conveniosMes_site = escreverNumero($RFP['convenios_meses_site'], 0, 0);
        $bloquear_congelamento = $RFP['bloq_congelamento'];
        $n_renovar_auto = $RFP['inativar_renovacao'];
        $adesaoTipo = $RFP['adesao_tipo'];
        $adesaoIntervalo = $RFP['adesao_intervalo'];
        $ValorLimite = escreverNumero($RFP["valor_limite"]);        
        $adesaoSoma = $RFP['adesao_soma'];
        $grupoCidades = $RFP['grupo_cidades'];
        $grupoFabricantes = $RFP['grupo_fabricantes'];
        $parcelar_site = $RFP['parcelar_site'];
        $cotaDefaut = escreverNumero($RFP['cota_defaut']);
        $descontotp = $RFP['cota_defaut_tp'];
        $limiteAnos = $RFP['ano_piso'];
        $tempo_producao = utf8_encode($RFP["tempo_producao"]); 
    }
} else {
    $disabled = '';
    $id = '';
    $descricao = '';
    $grupo = '';
    $horario = '';
    $horarioExterno = '';
    $preco_venda = escreverNumero(0);
    $prod_favorito = '';
    $inativa = '0';
    $desc_bloqueado = '0';
    $parcelar = '0';
    $tp_valid_acesso = "L";
    $qtd_dias_acesso = "0";
    $qtd_intervalo_acesso = "0";
    $limitar_acesso = "0";
    $zerar_domingo = "0";
    $necessita_turma = "0";
    $texto_desc = '';
    $texto_mensagem = '';
    $mostrar_site = "0";
    $mensalidade_vencida = "0";
    $ambiente = "";
    $qtd_limite_acesso = "0";
    $qtd_limite_acesso_min = "0";
    $idServico = "";
    $convenios_site = "";
    $conveniosMes_site = "0";
    $bloquear_congelamento = "0";
    $n_renovar_auto = "0";
    $adesaoTipo = '';
    $adesaoIntervalo = '';    
    $ValorLimite = "0";
    $adesaoSoma = '';
    $grupoCidades = '';
    $grupoFabricantes = '';
    $parcelar_site = '';
    $tempo_producao = "";
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
        $classEnable = "";
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<link href="../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<link href="../../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<style type="text/css">
    .disablePlan{
        pointer-events: none;
        cursor: default;
    }
    .rtable {
        display: block;
        height: 270px;
        overflow-y: scroll;
    }
</style>
<body>
    <form action="FormPlanos.php" method="POST" name="frmEnviaDados" id="frmEnviaDados" enctype="multipart/form-data">
        <div class="frmhead">
            <div class="frmtext">Planos</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="tabbable frmtabs">
            <ul class="nav nav-tabs" style="margin:0">
                <li class="active"><a href="#tab1" data-toggle="tab">Geral</a></li>
                <?php if ($_SESSION["mod_emp"] != 1) { ?>                    
                    <?php if ($mdl_seg_ != 1) { ?>
                        <li><a href="#tab2" data-toggle="tab">Outros</a></li>
                    <?php } ?>
                    <li><a href="#tab3" data-toggle="tab">Site</a></li>
                <?php } ?>
                <li><a href="#tab4" data-toggle="tab">Acréscimo</a></li>                
                <?php if ($servico_adesao == 1) { ?>
                <li><a href="#tab5" data-toggle="tab">Serviço de adesão</a></li>                
                <?php } ?>
            </ul>
            <div class="tab-content frmcont" style="height:322px; overflow:hidden;">
                <div class="tab-pane active" id="tab1">
                    <input type="hidden" name="txtsys" id="txtsys" value="<?php echo($_SESSION["mod_emp"] == 1 ? 'null' : ''); ?>">
                    <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                    <div style="width: 34%;float:left;">
                        <span>Plano:</span>
                        <input name="txtDescricao" id="txtDescricao" <?php echo $disabled; ?> maxlength="1024" type="text" class="input-medium" value="<?php echo $descricao; ?>"/>
                    </div>
                    <div style="width: 32%;float:left; margin-left:1%;">                        
                        <span>Grupo:</span>
                        <select name="txtGrupo" id="txtGrupo" class="select" <?php echo $disabled; ?> style="width:100%">
                            <option value="null" >Selecione o grupo:</option>
                            <?php
                            $sql = "select id_contas_movimento,descricao from sf_contas_movimento where tipo = 'L' and inativa = 0 ORDER BY descricao";
                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) {
                                ?>
                                <option value="<?php echo $RFP['id_contas_movimento'] ?>"<?php
                                if (!(strcmp($RFP['id_contas_movimento'], $grupo))) {
                                    echo "SELECTED";
                                }
                                ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                    <?php } ?>
                        </select>
                    </div>
                    <?php if ($_SESSION["mod_emp"] != 1) { ?>
                        <div style="width: 32%;float:left; margin-left:1%; <?php
                        if ($mdl_seg_ == 1) {
                            echo 'display:none';
                        }
                        ?>">
                            <span>Horário Fixo:</span>
                            <select name="txtHorario" id="txtHorario" class="select" <?php echo $disabled; ?> style="width:100%">
                                <option value="null" >Selecione o Horário:</option>
                                <?php
                                $sql = "select cod_turno, nome_turno from dbo.sf_turnos where cod_turno > 0 order by nome_turno";
                                $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) {
                                    ?>
                                    <option value="<?php echo $RFP['cod_turno'] ?>"<?php
                                    if (!(strcmp($RFP['cod_turno'], $horario))) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP['nome_turno']) ?></option>
                                        <?php } ?>
                            </select>
                        </div>
                    <?php } ?>
                    <div style="clear:both;height: 5px;"></div>
                    <div style="width:100%;">
                        <?php if ($_SESSION["mod_emp"] != 1) { ?>
                            <div style="width: 11%;float:left;">
                                <span>Valor:</span>
                                <input style="text-align:right" name="txtPreco_venda" id="txtPreco_venda" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $preco_venda; ?>"/>
                            </div>
                            <div style="width: 10%;float:left; margin-left:1%">
                                <span style="float: left;">Máximo:</span>
                                <input name="txtMaximo" id="txtMaximo" type="text" maxlength="2" class="input-medium" value="<?php echo $maximo; ?>" <?php echo $disabled; ?> style="float:left;width:48%;"/>
                                <button type="button" name="bntAdiciona" <?php echo $disabled; ?> class="btn btn-success" style="height:27px; margin:0; padding-top:6px;float: left;" onclick="novaLinha()"><span class="ico-plus"></span></button>
                            </div>
                        <?php } ?>
                        <div style="width: 14%;margin-left:1%;float:left;">
                            <span style="float: left;width: 100%;">DCC</span>
                            <input name="txtDCC" id="txtDCC" type="text" class="input-medium" value="<?php echo $maximo; ?>" <?php echo $disabled; ?> style="float:left;width: 65%;"/>
                            <button type="button" <?php echo $disabled; ?> class="btn btn-success" style="height:27px; margin:0; padding-top:6px;float: left;" onclick="addDCC()"><span class="ico-plus"></span></button>
                        </div>
                        <div style="width: 14%;margin-left:1%;float:left;">
                            <span style="float: left;width: 100%;">BOL</span>
                            <input name="txtBOL" id="txtBOL" type="text" class="input-medium" value="<?php echo $maximo; ?>" <?php echo $disabled; ?> style="float:left;width: 65%;"/>
                            <button type="button" <?php echo $disabled; ?> class="btn btn-success" style="height:27px; margin:0; padding-top:6px;float: left;" onclick="addBOL()"><span class="ico-plus"></span></button>
                        </div>
                        <div style="width: 14%;margin-left:1%;float:left;">
                            <span style="float: left;width: 100%;">PIX</span>
                            <input name="txtPIX" id="txtPIX" type="text" class="input-medium" value="<?php echo $maximo; ?>" <?php echo $disabled; ?> style="float:left;width: 65%;"/>
                            <button type="button" <?php echo $disabled; ?> class="btn btn-success" style="height:27px; margin:0; padding-top:6px;float: left;" onclick="addPIX()"><span class="ico-plus"></span></button>
                        </div>
                        <div style="width:32%;float:right;margin-left:1%;">
                            <div style="width:100%; float:left;">
                                <div style="float:left; margin-top:0">
                                    <input type="checkbox" class="input-medium" id="txtBloqueado" name="txtBloqueado" <?php echo $disabled; ?> <?php
                                    if (substr($inativa, 0, 1) == 1) {
                                        echo "checked";
                                    }
                                    ?> value="1"/>
                                </div>
                                <div style="float:left; margin:2px 5px">Bloqueado</div>
                            </div>
                            <div style="width:100%; float:left;">
                                <div style="float:left; margin-top:0">
                                    <input type="checkbox" class="input-medium" id="txtDesc_Bloqueado" name="txtDesc_Bloqueado" <?php echo $disabled; ?> <?php
                                    if (substr($desc_bloqueado, 0, 1) == 1) {
                                        echo "checked";
                                    }
                                    ?> value="1"/>
                                </div>
                                <div style="float:left; margin:2px 5px">Não permitir desconto na Venda</div>
                            </div>
                            <div style="width:100%; float:right;">
                                <div style="float:left; margin-top:0">
                                    <input type="checkbox" class="input-medium" id="txtParcelar" name="txtParcelar" <?php echo $disabled; ?> <?php
                                    if (substr($parcelar, 0, 1) == 1) {
                                        echo "checked";
                                    }
                                    ?> value="1"/>
                                </div>
                                <div style="float:left; margin:2px 5px">Gerar Parcela Automática</div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both;height: 5px;"></div>
                    <table width="100%" cellpadding="8" border="1" bordercolor="#FFF" style="background:#E9E9E9; font-size:12px; margin:0 0px">
                        <tr>
                            <td style="line-height:18px;"><b>Nº</b></td>
                            <td style="line-height:18px; width:145px"><b>Valor</b></td>
                            <td style="line-height:18px; width:90px"><b>Desconto %</b></td>
                            <td style="line-height:18px; width:90px"><b>Desconto $</b></td>
                            <td style="line-height:18px; width:140px"><b>Valor Final</b></td>
                            <td style="line-height:18px; width:140px"><b>Pagamento</b></td>
                            <td style="line-height:18px; width:53px"><b>Ação</b></td>
                        </tr>
                    </table>
                    <div style="width:671px; height:172px; margin:0 0px; overflow-y:scroll">
                        <table id="listParcPlano" width="100%" cellpadding="3" border="1" bordercolor="#FFF" style="font-size:12px">
                            <?php
                            if ($_GET['id'] != '' || $_POST['txtId'] != '') {
                                $cur = odbc_exec($con, "select *,(valor_desconto * valor_unico) / 100 descontoD from sf_produtos_parcelas where id_produto = " . $PegaURL . " order by parcela ");
                                while ($RFP = odbc_fetch_array($cur)) {
                                    ?>
                                    <tr id="list_<?php echo utf8_encode($RFP['parcela']); ?>">
                                        <?php
                                        if ($RFP['parcela'] === "-2" || $RFP['parcela'] === "-1" || $RFP['parcela'] === "0") {
                                            $parcela = 1;
                                        } else {
                                            $parcela = $RFP['parcela'];
                                        }
                                        ?>
                                        <td style="line-height:23px; text-align:center; font-weight:bold;width: 26px;"><input name="plan_id[]" value="<?php echo utf8_encode($RFP['id_parcela']); ?>" type="hidden"/><input name="plan_num[]" value="<?php echo utf8_encode($RFP['parcela']); ?>" type="hidden"/><?php
                                            if ($RFP['parcela'] === "-2") {
                                                echo "PIX";
                                            } else if ($RFP['parcela'] === "-1") {
                                                echo "BOL";
                                            } else if ($RFP['parcela'] === "0") {
                                                echo "DCC";
                                            } else {
                                                echo $RFP['parcela'];
                                            }
                                            ?></td>
                                        <td style="line-height:23px; width:120px"><input type="text" id="val_<?php echo utf8_encode($RFP['parcela']); ?>" name="plan_val[]" value="<?php echo escreverNumero($RFP['valor_unico']); ?>" onchange="updateLinha(<?php echo utf8_encode($RFP['parcela']); ?>, 'V')" <?php echo $disabled; ?>/></td>
                                        <td style="line-height:23px; width:102px"><input type="text" id="des_<?php echo utf8_encode($RFP['parcela']); ?>" name="plan_des[]" value="<?php echo escreverNumero($RFP['valor_desconto']); ?>" onchange="updateLinha(<?php echo utf8_encode($RFP['parcela']); ?>, 'P')" <?php echo $disabled; ?>/></td>
                                        <td style="line-height:23px; width:102px"><input type="text" id="desD_<?php echo utf8_encode($RFP['parcela']); ?>" name="plan_desD[]" value="<?php echo escreverNumero($RFP['descontoD']); ?>" onchange="updateLinha(<?php echo utf8_encode($RFP['parcela']); ?>, 'D')" <?php echo $disabled; ?>/></td>
                                        <td style="line-height:23px; width:120px"><input type="text" id="fin_<?php echo utf8_encode($RFP['parcela']); ?>" value="<?php echo escreverNumero($RFP['valor_unico'] - (($RFP['valor_unico'] * $RFP['valor_desconto']) / 100)); ?>" readonly /></td>
                                        <td style="line-height:23px; width:140px"><input type="text" id="pag_<?php echo utf8_encode($RFP['parcela']); ?>" value="<?php
                                            echo $parcela . " x ";
                                            if ($parcela > 0) {
                                                echo escreverNumero(($RFP['valor_unico'] - (($RFP['valor_unico'] * $RFP['valor_desconto']) / 100)) / $parcela);
                                            } else {
                                                echo escreverNumero(0);
                                            }
                                            ?>" readonly /></td>
                                        <td style="width:41px; text-align:center"><a class="<?php echo $classEnable; ?>" onclick="removeLinha(<?php echo utf8_encode($RFP['parcela']) . "," . utf8_encode($RFP['id_parcela']); ?>)" href="javascript:void(0)"><img src="../../img/1365123843_onebit_33 copy.PNG" width="16" height="16"></a></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="tab2">
                    <div style="float: left; width: 50%;">
                        <div>
                            <span>Tipo de Validação:</span>
                            <select name="txtTipoAcesso" id="txtTipoAcesso" class="select" <?php echo $disabled; ?> style="width:100%">
                                <option value="L" <?php
                                if (!(strcmp("L", $tp_valid_acesso))) {
                                    echo "SELECTED";
                                }
                                ?>>DIAS LIVRES</option>
                                <option value="E" <?php
                                if (!(strcmp("E", $tp_valid_acesso))) {
                                    echo "SELECTED";
                                }
                                ?>>DIAS ESPECÍFICOS</option>
                                <option value="C" <?php
                                if (!(strcmp("C", $tp_valid_acesso))) {
                                    echo "SELECTED";
                                }
                                ?>>DIAS CORRIDOS</option>
                            </select>
                        </div>
                        <div style="clear:both;height: 5px;"></div>                        
                        <div style="float:left;">
                            <input type="checkbox" class="input-medium" id="txtLimitar_Acesso" name="txtLimitar_Acesso" <?php echo $disabled; ?> <?php
                            if (substr($limitar_acesso, 0, 1) == 1) {
                                echo "checked";
                            }
                            ?> value="1"/>
                            Limitar
                            <input style="width: 16%;" name="txtQtdLimiteAcesso" id="txtQtdLimiteAcesso" maxlength="2" type="text" class="input-medium" value="<?php echo $qtd_limite_acesso; ?>" <?php echo $disabled; ?>>
                            Acesso(s) a cada
                            <input style="width: 16%;" name="txtQtdLimiteAcessoMin" id="txtQtdLimiteAcessoMin" maxlength="4" type="text" class="input-medium" value="<?php echo $qtd_limite_acesso_min; ?>" <?php echo $disabled; ?>>
                            Min
                        </div>
                        <div style="float:left;">
                            <input type="checkbox" class="input-medium" id="txtZerarDomingo" name="txtZerarDomingo" <?php echo $disabled; ?> <?php
                            if (substr($zerar_domingo, 0, 1) == 1) {
                                echo "checked";
                            }
                            ?> value="1"/>Zerar aos Domingos(Dias específicos)
                        </div>                                                
                        <?php if ($mdl_clb_ == 1) { ?>
                            <div style="float:left;">
                                <input type="checkbox" class="input-medium" id="txtNRenovarAuto" name="txtNRenovarAuto" <?php echo $disabled; ?> <?php
                                if (substr($n_renovar_auto, 0, 1) == 1) {
                                    echo "checked";
                                }
                                ?> value="1"/>Não Renovar (Automatico)
                            </div>
                        <?php } ?>                        
                        <div style="float:left;">
                            <input type="checkbox" class="input-medium" id="txtMensalidade_vencida" name="txtMensalidade_vencida" <?php echo $disabled; ?> <?php
                            if (substr($mensalidade_vencida, 0, 1) == 1) {
                                echo "checked";
                            }
                            ?> value="1"/>Bloquear Acesso por Mensalidades Vencidas
                        </div>                        
                        <div style="float:left;">
                            <input type="checkbox" class="input-medium" id="txtBloqCongelamento" name="txtBloqCongelamento" <?php echo $disabled; ?> <?php
                            if (substr($bloquear_congelamento, 0, 1) == 1) {
                                echo "checked";
                            }
                            ?> value="1"/>Bloquear Congelamento
                        </div>                        
                        <div style="float:left; width: 100%;">
                            <span>Ambiente:</span>
                            <select id="txtAmbiente" name="txtAmbiente" <?php echo $disabled; ?> class="select" style="width:100%">
                                <option value="null">Selecione</option>
                                <?php
                                $cur = odbc_exec($con, "select id_ambiente,nome_ambiente from sf_ambientes order by nome_ambiente ") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) {
                                    ?>
                                    <option value="<?php echo $RFP["id_ambiente"] ?>"<?php
                                    if (!(strcmp($RFP["id_ambiente"], $ambiente))) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP["nome_ambiente"]) ?></option>
                                        <?php } ?>
                            </select>
                        </div>                      
                        <div style="float:left;">
                            <span>Horário Fixo Ambiente Externo:</span>
                            <select name="txtHorarioExterno" id="txtHorarioExterno" class="select" <?php echo $disabled; ?> style="width:100%">
                                <option value="null" >Selecione o Horário:</option>
                                <?php
                                $sql = "select cod_turno, nome_turno from dbo.sf_turnos order by nome_turno";
                                $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) {
                                    ?>
                                    <option value="<?php echo $RFP['cod_turno'] ?>"<?php
                                    if (!(strcmp($RFP['cod_turno'], $horarioExterno))) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP['nome_turno']) ?></option>
                                        <?php } ?>
                            </select>
                        </div>                        
                        <div style="width:33%;float:left;">
                            <span>Custo do Plano:</span>
                            <input name="txtCustoPlan" id="txtCustoPlan" type="text" value="<?php echo ($custo == 0 ? escreverNumero(0) : $custo); ?>" class="input-medium" <?php echo $disabled; ?> />
                        </div>                        
                        <div style="width:33%;float:left;margin-left:1%">
                            <span>Comissão(%):</span>
                            <input name="txtComissaoPlan" id="txtComissaoPlan" type="text" value="<?php echo ($comissao_venda == 0 ? escreverNumero(0) . '%' : $comissao_venda); ?>" class="input-medium" <?php echo $disabled; ?> />
                        </div>  
                        <div style="width:32%;float:left;margin-left:1%">
                            <span>Valor Limite:</span>                            
                            <input name="txtValorLimite" id="txtValorLimite" type="text" value="<?php echo escreverNumero($ValorLimite); ?>" class="input-medium" <?php echo $disabled; ?> />
                        </div>                        
                        <?php if ($servico_adesao == 1) { ?>                        
                            <div style="width:49%;float:left;">
                                <span>Tipo Adesão:</span>
                                <select name="txtAdesaoTipo" id="txtAdesaoTipo" style="width: 100%" class="select" <?php echo $disabled; ?>>
                                    <option value="null">Selecione</option>
                                    <option value="1" <?php echo ($adesaoTipo == 1 ? "selected" : "");?>>Mês fixo</option>
                                </select>
                            </div>                        
                            <div style="width:49%;float:left;margin-left:1%">
                                <span>Período recorrente de Adesão:</span>
                                <select name="txtAdesaoIntervalo" id="txtAdesaoIntervalo" style="width: 100%" class="select" <?php echo $disabled; ?>>
                                    <option value="null">Selecione</option>
                                    <option value="1" <?php echo ($adesaoIntervalo == 1 ? "selected" : "");?>>Janeiro</option>
                                    <option value="2" <?php echo ($adesaoIntervalo == 2 ? "selected" : "");?>>Fevereiro</option>
                                    <option value="3" <?php echo ($adesaoIntervalo == 3 ? "selected" : "");?>>Março</option>
                                    <option value="4" <?php echo ($adesaoIntervalo == 4 ? "selected" : "");?>>Abril</option>
                                    <option value="5" <?php echo ($adesaoIntervalo == 5 ? "selected" : "");?>>Maio</option>
                                    <option value="6" <?php echo ($adesaoIntervalo == 6 ? "selected" : "");?>>Junho</option>
                                    <option value="7" <?php echo ($adesaoIntervalo == 7 ? "selected" : "");?>>Julho</option>
                                    <option value="8" <?php echo ($adesaoIntervalo == 8 ? "selected" : "");?>>Agosto</option>
                                    <option value="9" <?php echo ($adesaoIntervalo == 9 ? "selected" : "");?>>Setembro</option>
                                    <option value="10" <?php echo ($adesaoIntervalo == 10 ? "selected" : "");?>>Outubro</option>
                                    <option value="11" <?php echo ($adesaoIntervalo == 11 ? "selected" : "");?>>Novembro</option>
                                    <option value="12" <?php echo ($adesaoIntervalo == 12 ? "selected" : "");?>>Dezembro</option>
                                </select>
                            </div>                        
                        <?php } ?>
                    </div>
                    <div style="float: left; width: 49%; margin-left: 1%;">
                        <div id="divDiasEspecificos" hidden>
                            <div style="float: left; width: 30%;">
                                <span>Dias Permitido:</span>
                                <input name="txtQtdDiasAcesso" id="txtQtdDiasAcesso" maxlength="2" type="text" class="input-medium" value="<?php echo $qtd_dias_acesso; ?>" <?php echo $disabled; ?>/>
                            </div>
                            <div style="float: left; width: 30%; margin-left: 1%">
                                <span>A cada(dias):</span>
                                <input name="txtQtdIntervaloAcesso" id="txtQtdIntervaloAcesso" maxlength="2" type="text" class="input-medium" value="<?php echo $qtd_intervalo_acesso; ?>" <?php echo $disabled; ?>/>
                            </div>
                        </div>                                                                       
                    </div>
                    <div style="clear:both;height: 5px;"></div>                                       
                </div>
                <div class="tab-pane" id="tab3">
                    <ul class="nav nav-tabs">
                        <li class="active" style="margin-left:10px;"><a href="#subsite" data-toggle="tab">Geral</a></li>
                        <li><a href="#subsite2" data-toggle="tab">Descrição</a></li>
                        <li><a href="#subsite3" data-toggle="tab">Mensagem</a></li>
                        <?php if ($mdl_seg_ > 0 ) { ?>
                        <li><a href="#subsite4" data-toggle="tab">Acessórios</a></li>
                        <?php } ?>
                    </ul>
                    <div class="tab-content" style="height: 282px;">
                        <div class="tab-pane active" id="subsite">
                            <div style="float:left;margin-left:1%;">
                                <div style="width:100%; float:left; margin-top: 3px;">
                                    <input type="checkbox" class="input-medium" id="txtMostrar_site" name="txtMostrar_site" <?php echo $disabled; ?> <?php echo ($mostrar_site == 1 ? "checked" : ""); ?> value="1"/> Vender Plano no site
                                </div>
                                <div style="float:left;">
                                    <input type="checkbox" class="input-medium" id="txtFavorito" name="txtFavorito" <?php echo $disabled; ?> <?php echo ($prod_favorito == 1 ? "checked" : ""); ?> value="1"/> Favorito
                                </div>
                                <div style="float:left; margin-left: 1%;">
                                    <input type="checkbox" class="input-medium" id="txtNecessita_turma" name="txtNecessita_turma" <?php echo $disabled; ?> <?php
                                    if (substr($necessita_turma, 0, 1) == 1) {
                                        echo "checked";
                                    }
                                    ?> value="1"/><?php echo ($mdl_seg_ > 0 ? "Plano Ativo em Oficina" : "Necessita de Turma"); ?>
                                </div>                                
                                <div style="width:100%; float:left; margin-bottom: 7px;">
                                    <input type="checkbox" class="input-medium" id="txtParcelarSite" name="txtParcelarSite" <?php echo $disabled; ?> <?php echo ($parcelar_site == 1 ? "checked" : ""); ?> value="1"/> Parcelar de acordo com número de meses (Cartão)
                                </div>
                                <div style="width:50%; float: left;">
                                    <span>Serviço de Adesão:</span>
                                    <select name="txtServico" id="txtServico" class="select" <?php echo $disabled; ?> style="width:100%">
                                        <option value="null">Selecione</option>
                                        <?php
                                        $sql = "Select conta_produto, descricao from sf_produtos where inativa = 0 and tipo = 'S'";
                                        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) {
                                            ?>
                                            <option value="<?php echo $RFP['conta_produto'] ?>"<?php
                                            if ($RFP["conta_produto"] == $idServico) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                                <?php } ?>
                                    </select>
                                </div>
                                <div style="width:49%;float:left; margin-left: 1%; margin-top: 20px;">
                                    <input name="txtAdesaoSoma" id="txtAdesaoSoma" <?php echo ($adesaoSoma == 1 ? "checked" : ""); ?> type="checkbox" value="1" class="input-medium" <?php echo $disabled; ?> />
                                    <span>(Mensalidade + Taxa de Adesão)</span>
                                </div>                                
                                <div style="clear: both;"></div>
                                <div style="width:50%; float: left;">
                                    <span>Convênios de Adesão:</span>
                                    <select name="txtConvenios" id="txtConvenios" class="select" <?php echo $disabled; ?> style="width:100%">
                                        <option value="null">Selecione</option>
                                        <?php
                                        $sql = "Select id_convenio, descricao
                                        from sf_convenios where tipo = 1 and status = 0 and (convenio_especifico = 0 or id_convenio
                                        in(select id_convenio_planos from sf_convenios_planos where id_prod_convenio = " . valoresNumericos2($id) . "))";
                                        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) {
                                            ?>
                                            <option value="<?php echo $RFP['id_convenio'] ?>"<?php
                                            if ($RFP["id_convenio"] == $convenios_site) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                                <?php } ?>
                                    </select>
                                </div>
                                <div style="width:49%;float:left; margin-left: 1%;">
                                    <span>Validade Convênios (Meses):</span>
                                    <input name="txtConveniosMes" id="txtConveniosMes" maxlength="2" type="text" value="<?php echo $conveniosMes_site; ?>" class="input-medium" <?php echo $disabled; ?> />
                                </div>
                                <div style="clear: both;"></div>                                
                                <div style="width:50%; float: left;">
                                    <span>Grupo de Cidades:</span>
                                    <select id="txtGrupoCidades" name="txtGrupoCidades" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>                                
                                        <option value="null">Selecione</option>
                                        <?php $sql = "select id, descricao from sf_regioes where inativo = 0 ORDER BY id";
                                        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['id'] ?>"<?php
                                            if ($RFP["id"] == $grupoCidades) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                        <?php } ?>                                
                                    </select>
                                </div>                                
                                <div style="width:49%;float:left; margin-left: 1%;">
                                    <span>Grupo de Fabricantes:</span>
                                    <select id="txtGrupoFabricantes" name="txtGrupoFabricantes" class="select input-medium" style="width:100%" <?php echo $disabled; ?>>                                
                                        <option value="null">Selecione</option>
                                        <?php $sql = "select id, descricao from sf_veiculo_grupo where inativo = 0 ORDER BY id";
                                        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['id'] ?>"<?php
                                            if ($RFP["id"] == $grupoFabricantes) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                        <?php } ?>                                
                                    </select>
                                </div>                                                                
                                <div style="clear: both;"></div>                                
                                <div style="width:50%; float: left;">
                                    <span>Tipo Veículo:</span>
                                    <select id="txtTipoVeiculo" name="txtTipoVeiculo[]" multiple="multiple" class="select" style="width:100%" class="input-medium" <?php echo $disabled; ?>>
                                        <?php $cur = odbc_exec($con, "select t.*, id_produto from sf_veiculo_tipos t
                                        left join sf_produtos_veiculo_tipos pt on t.id = pt.id_tipo and pt.id_produto = " . valoresSelect2($id) . " where v_inativo = 0 order by t.descricao") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['id'] ?>"<?php
                                            if ($RFP["id_produto"] != null) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                        <?php } ?>
                                    </select>                                    
                                </div>
                                <div style="width:22%;float:left; margin-left: 1%;">
                                    <span>Cota default:</span>
                                    <input id="txtCotaDefaut" name="txtCotaDefaut" type="text" style="width: 72%;" class="input-medium" value="<?php echo $cotaDefaut; ?>" <?php echo $disabled; ?>>
                                    <button class="btn btn-primary" id="bntProdDesconto" type="button" style="width:35px; padding-bottom:3px; text-align:center" <?php echo $disabled; ?>><span id="prod_sinal">%</span></button>
                                    <input name="txtProdSinal" id="txtProdSinal" value="<?php echo $descontotp; ?>" type="hidden"/>
                                </div>                                
                                <div style="width: 12%;float:left; margin-left: 1%;">
                                    <span>Limite anos:</span>
                                    <input id="txtLimiteAnos" name="txtLimiteAnos" type="text" class="input-medium" maxlength="2" value="<?php echo $limiteAnos; ?>" <?php echo $disabled; ?>>
                                </div>
                                <div style="width: 13%;float: left; margin-left: 1%;">
                                    <span>Nível:</span>
                                    <input name="txtTempoProducao" id="txtTempoProducao" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $tempo_producao; ?>"/>
                                </div>                                  
                            </div>
                        </div>
                        <div class="tab-pane" id="subsite2">
                            <label for="ckeditor"></label>
                            <textarea id="ck_desc" name="ck_desc" <?php echo $disabled; ?>><?php echo $texto_desc; ?></textarea>
                        </div>
                        <div class="tab-pane" id="subsite3">
                            <label for="ckeditor"></label>
                            <textarea id="ck_msg" name="ck_msg" <?php echo $disabled; ?>><?php echo $texto_mensagem; ?></textarea>
                        </div>
                        <div class="tab-pane" id="subsite4">
                            <div style="width: 87%;float:left;">
                                <span style="width: 50%">Acessório:</span>
                                <select name="txtPrdMPA" id="txtPrdMPA" class="select" <?php echo $disabled; ?> style="width:100%">
                                    <option value="null" >Selecione o Produto:</option>
                                    <?php $sql = "select conta_produto,descricao,
                                    (select descricao from sf_contas_movimento where id_contas_movimento = conta_movimento) grupo
                                    from dbo.sf_produtos where tipo = 'A' and inativa = 0 
                                    and (veiculo_tipo is null or veiculo_tipo in (select id_tipo from sf_produtos_veiculo_tipos 
                                    where id_produto = " . valoresSelect2($id) . ")) ORDER BY descricao";
                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                        <option value="<?php echo $RFP['conta_produto'] ?>"><?php echo "[" . utf8_encode($RFP['grupo']) . "] ". utf8_encode($RFP['descricao']) ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div style="width: 12%;float:left;margin-left: 1%;margin-top: 15px;">
                                <button class="btn btn-success" <?php echo $disabled; ?> type="button" name="bntSaveMPA" onClick="novaLinhaSerA()"><span class="ico-checkmark"></span> Gravar</button>
                            </div>
                            <div style="clear:both;height:5px;"></div>
                            <table width="100%" cellpadding="8" border="1" bordercolor="#FFF" style="background:#E9E9E9; font-size:12px">
                                <tr>
                                    <td style="line-height:18px"><b>Acessório</b></td>
                                    <td style="line-height:18px; width:53px"><b>Ação</b></td>
                                </tr>
                            </table>
                            <div style="overflow-y:scroll; height:135px">
                                <table id="listMatPrimA" width="100%" cellpadding="3" border="1" bordercolor="#FFF" style="font-size:12px">
                                    <?php
                                    if ($id != "") {
                                        $conta = 1000;
                                        $cur = odbc_exec($con, "select id_prodmp,id_materiaprima,descricao,quantidademp from sf_produtos_materiaprima inner join sf_produtos on id_materiaprima = conta_produto where tipo = 'A' and id_produtomp = " . $id . " order by id_prodmp");
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <tr id="listSA_<?php echo $conta; ?>">
                                            <input type="hidden" name="prod_id[]" value="<?php echo $RFP["id_prodmp"]; ?>">
                                            <input type="hidden" name="prod_cod[]" value="<?php echo $RFP["id_materiaprima"]; ?>">
                                            <td style="line-height:23px"><?php echo utf8_encode($RFP["descricao"]); ?></td>
                                            <td style="width:46px; text-align:center"><a onClick="removeLinhaSerA(<?php echo $conta; ?>)" href="javascript:void(0)"><img src="../../img/1365123843_onebit_33 copy.PNG" width="16" height="16"/></a></td>
                                            </tr>
                                            <?php
                                            $conta = $conta + 1;
                                        }
                                    }
                                    ?>
                                </table>
                            </div>
                            <div style="clear:both;height:5px;"></div>
                            <div style="width:19%;float:left;margin-left:1%">
                                <span>Valor Minimo Veículo:</span>
                                <input name="segMin" id="segMin" type="text" value="<?php echo ($segMin == 0 ? escreverNumero(0) : $segMin); ?>" class="input-medium" <?php echo $disabled; ?> />
                            </div>
                            <div style="width:19%;float:left;margin-left:1%">
                                <span>Valor Máximo Veículo:</span>                            
                                <input name="segMax" id="segMax" type="text" value="<?php echo ($segMax == 0 ? escreverNumero(0) : $segMax); ?>" class="input-medium" <?php echo $disabled; ?> />
                            </div>    
                            <button class="btn green" type="submit" style="float: right;" name="bntReplicar" value="Novo" <?php echo $disabled; ?>><span class="ico-refresh"></span> Replicar p/ Grupo</button>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab4">
                    <input name="txtIdAcr" id="txtIdAcr" value="" type="hidden"/>
                    <div style="width: 15%;float:left;">
                        <span>Valor:</span>
                        <input id="txtValorAcr" name="txtValorAcr" maxlength="10" value="" <?php echo $disabled; ?> type="text" style="text-align: right;" class="input-medium"/>
                    </div>
                    <div style="width: 23%;float:left; margin-left: 1%;">
                        <span>Parentesco</span>
                        <select id="txtParentescoAcr" name="txtParentescoAcr" <?php echo $disabled; ?>>
                            <option value="null">Todos</option>
                            <?php $cur = odbc_exec($con, "select id_parentesco, nome_parentesco from sf_parentesco") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP['id_parentesco'] ?>"><?php echo utf8_encode($RFP["nome_parentesco"]) ?></option>
                            <?php } ?>                            
                        </select>
                    </div>
                    <div style="width: 10%;float:left; margin-left: 1%;">
                        <span>Idade Min.</span>
                        <input id="txtIdMinAcr" name="txtIdMinAcr" value="" <?php echo $disabled; ?> maxlength="3" type="text" class="input-medium"/>
                    </div>
                    <div style="width: 10%;float:left; margin-left: 1%;">
                        <span>Limite Masc.</span>
                        <input id="txtLimiteMascAcr" name="txtLimiteMascAcr" value="" <?php echo $disabled; ?> maxlength="3" type="text" class="input-medium"/>
                    </div>
                    <div style="width: 10%;float:left; margin-left: 1%;">
                        <span>Limite Fem.</span>
                        <input id="txtLimiteFemAcr" name="txtLimiteFemAcr" value="" <?php echo $disabled; ?> maxlength="3" type="text" class="input-medium"/>
                    </div>
                    <div style="width: 10%;float:left; margin-left: 1%;">
                        <span>Qtd. Min.</span>
                        <input id="txtQtdMinAcr" name="txtQtdMinAcr" value="" <?php echo $disabled; ?> maxlength="3" type="text" class="input-medium"/>
                    </div>
                    <div style="width: 10%;float:left; margin-left: 1%;">
                        <span>Qtd. Max.</span>
                        <input id="txtQtdMaxAcr" name="txtQtdMaxAcr" value="" <?php echo $disabled; ?> maxlength="3" type="text" class="input-medium"/>
                    </div>      
                    <div style="width: 5%;float:left; margin-left: 1%; margin-top: 16px;">                   
                        <button type="button" name="bntAdicionaAcr" onclick="addLinhaAcr()" <?php echo $disabled; ?> class="btn btn-success" style="height:27px; margin:0; padding-top:6px;float: left;"><span class="ico-plus"></span></button>
                    </div> 
                    <div style="clear:both;height: 15px;"></div>                    
                    <table id="tbPlanoAcrescimo" width="100%" cellpadding="8" class="rtable" style="font-size:12px; margin:0 0px;" border="1" bordercolor="#FFF">
                        <thead style="background:#E9E9E9;">
                            <td style="line-height:18px; width:60px"><b>Valor</b></td>
                            <td style="line-height:18px; width:90px"><b>Parentesco</b></td>
                            <td style="line-height:18px; width:80px"><b>Idade Min.</b></td>
                            <td style="line-height:18px; width:80px"><b>Lim. Masc.</b></td>
                            <td style="line-height:18px; width:80px"><b>Lim. Fem.</b></td>
                            <td style="line-height:18px; width:80px"><b>Qtd. Min.</b></td>
                            <td style="line-height:18px; width:80px"><b>Qtd. Max.</b></td>
                            <td style="line-height:18px; width:40px"><b>Ação</b></td>                            
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab5">
                    <input name="txtIdAcrSer" id="txtIdAcrSer" value="" type="hidden"/>
                    <div style="width: 40%;float:left;">
                        <span>Serviço</span>
                        <select id="txtServicoAcrSer" name="txtServicoAcrSer" <?php echo $disabled; ?>>
                            <option value="null">Todos</option>
                            <?php $cur = odbc_exec($con, "select conta_produto, descricao from sf_produtos B where tipo = 'S'") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP['conta_produto'] ?>"><?php echo utf8_encode($RFP["descricao"]) ?></option>
                            <?php } ?>                            
                        </select>
                    </div>
                    <div style="width: 10%;float:left; margin-left: 1%;">
                        <span>Qtd. Min.</span>
                        <input id="txtQtdMinAcrSer" name="txtQtdMinAcrSer" value="" <?php echo $disabled; ?> maxlength="2" type="text" class="input-medium"/>
                    </div>
                    <div style="width: 10%;float:left; margin-left: 1%;">
                        <span>Qtd. Max.</span>
                        <input id="txtQtdMaxAcrSer" name="txtQtdMaxAcrSer" value="" <?php echo $disabled; ?> maxlength="2" type="text" class="input-medium"/>
                    </div>      
                    <div style="width: 5%;float:left; margin-left: 1%; margin-top: 16px;">                   
                        <button type="button" name="bntAdicionaAcrSer" onclick="addLinhaAcrSer()" <?php echo $disabled; ?> class="btn btn-success" style="height:27px; margin:0; padding-top:6px;float: left;"><span class="ico-plus"></span></button>
                    </div> 
                    <div style="clear:both;height: 15px;"></div>                    
                    <table id="tbPlanoAcrescimoSer" width="100%" cellpadding="8" style="font-size:12px; margin:0 0px;" border="1" bordercolor="#FFF">
                        <thead style="background:#E9E9E9;">
                            <td style="line-height:18px; "><b>Serviço</b></td>
                            <td style="line-height:18px; width:80px"><b>Qtd. Min.</b></td>
                            <td style="line-height:18px; width:80px"><b>Qtd. Max.</b></td>
                            <td style="line-height:18px; width:40px"><b>Ação</b></td>                            
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == "") { ?>
                    <button class="btn green" type="submit" onclick="return validar();" name="bntSave"><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST["txtId"] == "") { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar"><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else { ?>
                    <button class="btn button-blue" style="float: left; margin-top: 14px;" type="button" onclick="return AtualizarMensalidades();" name="bntRefresh" value="Atualizar Mensalidades"><span class="ico-refresh"></span> Atualizar Mensalidades <?php echo $tpAtualizacao; ?></button>    
                    <button class="btn green" type="submit" name="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>    
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/charts.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/plugins/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>
    <script type="text/javascript" src="./js/FormPlanos.js"></script>
    <script type="text/javascript">

        function novaLinhaAcr() {
            $("#txtIdAcr").val("");
            $("#txtValorAcr").val("0,0000");
            $("#txtParentescoAcr").val("null");
            $("#txtIdMinAcr").val("0");
            $("#txtLimiteMascAcr").val("999");
            $("#txtLimiteFemAcr").val("999");
            $("#txtQtdMinAcr").val("1");
            $("#txtQtdMaxAcr").val("1");
        }
        
        $("#bntProdDesconto").click(function () {
            if ($("#prod_sinal").html() === "%") {
                $("#prod_sinal").html("$");
                $("#txtProdSinal").val("1");
            } else {
                $("#prod_sinal").html("%");
                $("#txtProdSinal").val("0");
            }
        });
        
        if ($("#txtProdSinal").val() === "1") {
            $("#prod_sinal").html("$");
        }
            
        function addLinhaAcr() {
            var dados = {
                btnSave: "S",
                txtId: $("#txtId").val(),                
                txtIdAcr: $("#txtIdAcr").val(),
                txtValorAcr: $("#txtValorAcr").val(),
                txtParentescoAcr: $("#txtParentescoAcr").val(),
                txtIdMinAcr: $("#txtIdMinAcr").val(),
                txtLimiteMascAcr: $("#txtLimiteMascAcr").val(),
                txtLimiteFemAcr: $("#txtLimiteFemAcr").val(),
                txtQtdMinAcr: $("#txtQtdMinAcr").val(),
                txtQtdMaxAcr: $("#txtQtdMaxAcr").val()
            };    
            $.post("form/PlanosAcrescimoFormServer.php", dados).done(function (data) {
                if (data.trim() === "YES") {
                    novaLinhaAcr();
                    refreshAcrescimo();
                }           
            });
        }
        
        function novaLinhaAcrSer() {
            $("#txtIdAcrSer").val("");
            $("#txtServicoAcrSer").val("null");
            $("#txtQtdMinAcrSer").val("1");
            $("#txtQtdMaxAcrSer").val("1");
        }
        
        function addLinhaAcrSer() {
            var dados = {
                btnSave: "S",
                txtId: $("#txtId").val(),                
                txtIdAcrSer: $("#txtIdAcrSer").val(),
                txtServicoAcrSer: $("#txtServicoAcrSer").val(),
                txtQtdMinAcrSer: $("#txtQtdMinAcrSer").val(),
                txtQtdMaxAcrSer: $("#txtQtdMaxAcrSer").val()
            };    
            $.post("form/PlanosAcrescimoServicoFormServer.php", dados).done(function (data) {
                if (data.trim() === "YES") {
                    novaLinhaAcrSer();
                    refreshAcrescimoSer();
                }           
            });
        }

        function finalFindAcrescimo() {
            return "form/PlanosAcrescimoFormServer.php?listAcrescimo=" + $("#txtId").val();
        }

        function finalFindAcrescimoSer() {
            return "form/PlanosAcrescimoServicoFormServer.php?listAcrescimo=" + $("#txtId").val();
        }
        
        function BuscarAcrescimo(id) {
            $.post("form/PlanosAcrescimoFormServer.php", {getAcrescimo: id}).done(function (json) {
                var data = JSON.parse(json)[0];
                $("#txtIdAcr").val(data.id);
                $("#txtValorAcr").val(data.valor);
                $("#txtParentescoAcr").val(data.id_parentesco);
                $("#txtIdMinAcr").val(data.idade_min);
                $("#txtLimiteMascAcr").val(data.idade_max_m);
                $("#txtLimiteFemAcr").val(data.idade_max_f);
                $("#txtQtdMinAcr").val(data.qtd_min);
                $("#txtQtdMaxAcr").val(data.qtd_max);
            });
        }
        
        function RemoverAcrescimo(id) {
            bootbox.confirm('Deseja realmente excluir esse acréscimo?', function (result) {
                if (result === true) {
                    $.post("form/PlanosAcrescimoFormServer.php", {btnDelete: "S", txtId: id}).done(function (data) {
                        if (data.trim() !== "YES") {
                            bootbox.alert("Erro ao excluir registro!");
                        } else {
                            refreshAcrescimo();
                        }
                    });
                }
            });
        }  
        
        function BuscarAcrescimoSer(id) {
            $.post("form/PlanosAcrescimoServicoFormServer.php", {getAcrescimo: id}).done(function (json) {
                var data = JSON.parse(json)[0];
                $("#txtIdAcrSer").val(data.id);
                $("#txtServicoAcrSer").val(data.id_servico);
                $("#txtQtdMinAcrSer").val(data.qtd_min);
                $("#txtQtdMaxAcrSer").val(data.qtd_max);
            });
        }
        
        function RemoverAcrescimoSer(id) {
            bootbox.confirm('Deseja realmente excluir esse acréscimo?', function (result) {
                if (result === true) {
                    $.post("form/PlanosAcrescimoServicoFormServer.php", {btnDelete: "S", txtId: id}).done(function (data) {
                        if (data.trim() !== "YES") {
                            bootbox.alert("Erro ao excluir registro!");
                        } else {
                            refreshAcrescimoSer();
                        }
                    });
                }
            });
        }        

        $('#tbPlanoAcrescimo').dataTable({
            "iDisplayLength": 20,
            "aLengthMenu": [20, 30, 40, 50, 100],
            "bProcessing": true,
            "bServerSide": true,
            bPaginate: false, bFilter: false, bInfo: false, bSort: false,
            "sAjaxSource": finalFindAcrescimo(),
            'oLanguage': {
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sZeroRecords': "Não foi encontrado nenhum resultado"
            }
        });
       
        function refreshAcrescimo() {
            var tblAcrescimo = $('#tbPlanoAcrescimo').dataTable();
            tblAcrescimo.fnReloadAjax(finalFindAcrescimo());
        }

        $('#tbPlanoAcrescimoSer').dataTable({
            "iDisplayLength": 20,
            "aLengthMenu": [20, 30, 40, 50, 100],
            "bProcessing": true,
            "bServerSide": true,
            bPaginate: false, bFilter: false, bInfo: false, bSort: false,
            "sAjaxSource": finalFindAcrescimoSer(),
            'oLanguage': {
                'sLoadingRecords': "Carregando...",
                'sProcessing': "Processando...",
                'sZeroRecords': "Não foi encontrado nenhum resultado"
            }
        });
       
        function refreshAcrescimoSer() {
            var tblAcrescimoSer = $('#tbPlanoAcrescimoSer').dataTable();
            tblAcrescimoSer.fnReloadAjax(finalFindAcrescimoSer());
        }
        
        novaLinhaAcr();
        novaLinhaAcrSer();
        
        var contaA = 1;
        function novaLinhaSerA() {
            if ($("#txtPrdMPA").val() !== "") {
                contaA = contaA + 1;
                $("#listMatPrimA").append("<tr id=\"listSA_" + contaA + "\"><input type=\"hidden\" name=\"prod_id[]\" value=\"\"><input type=\"hidden\" name=\"prod_cod[]\" value=\"" + $("#txtPrdMPA").val() + "\"><td style=\"line-height:23px\">" + $("#txtPrdMPA option:selected").text() + "</td><td style=\"width:46px; text-align:center\"><a onClick=\"removeLinhaSerA(" + contaA + ")\" href=\"javascript:void(0)\"><img src=\"../../img/1365123843_onebit_33 copy.PNG\" width=\"16\" height=\"16\"/></a></td></tr>");
            }
        }
        
        function removeLinhaSerA(id) {
            $("#listSA_" + id).remove();
        }
        
        $('#txtTipoVeiculo').change(function () {
            getAcessorios();
        });
                
        function getAcessorios() {
            $.getJSON("./../Seguro/form/FormVeiculosServer.php?getAcessorios=" + $("#txtTipoVeiculo").val(), {isAjax: "S"}, function (json) {
                let options = "<option value=\"null\">Selecione</option>";
                if (json !== null) {
                    for (var x = 0; x < json.length; x++) {
                        options += "<option value=\"" + json[x].conta_produto + "\" val=\"" + json[x].preco_venda + "\">" + json[x].descricao + "</option>";
                    }
                }
                $("#txtPrdMPA").html(options);
                $("#txtPrdMPA").select2("val", "null");
            }); 
        }

    </script>
    <?php odbc_close($con); ?>
</body>