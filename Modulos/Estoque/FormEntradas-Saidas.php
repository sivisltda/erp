<?php
include "../../Connections/configini.php";

$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "set dateformat dmy;
        update sf_vendas set " . 
        "empresa = " . valoresSelect('txtEmpresa') . "," .                
        "vendedor = " . valoresTexto('txtVendedor') . "," .  
        "historico_venda = " . valoresTexto('txtHistorico') . "," .
        "comentarios_venda = " . valoresTexto('txtComentarios') . "," .
        "data_venda = " . valoresDataHora2($_POST['txtDtVenda'], date("H:i:s")) . "," .
        "destinatario = " . valoresTexto('txtDestinatario') . "," .
        "cov = " . valoresSelectTexto('txtTipo') . "," .  
        "grupo_conta = " . valoresSelect('txtGrupoConta') . "," .  
        "conta_movimento = " . valoresSelect('txtContaConta') . "," .  
        "id_justificativa = " . valoresSelect('txtJustificativa') . "," .                 
        "status = 'Aguarda', data_aprov = null where id_venda = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {        
        odbc_exec($con, "set dateformat dmy;
        insert into sf_vendas (empresa, vendedor,historico_venda,comentarios_venda,data_venda,destinatario,cov, grupo_conta,conta_movimento,id_justificativa,
        sys_login,ent_sai,status,cliente_venda,descontop,descontos,descontotp,descontots,n_servicos,n_produtos,favorito,reservar_estoque) values (" .
        valoresSelect('txtEmpresa') . "," .                
        valoresTexto('txtVendedor') . "," .  
        valoresTexto('txtHistorico') . "," .  
        valoresTexto('txtComentarios') . "," .
        valoresDataHora2($_POST['txtDtVenda'], date("H:i:s")) . "," .                
        valoresTexto('txtDestinatario') . "," .                   
        valoresSelectTexto('txtTipo') . "," .                  
        valoresSelect('txtGrupoConta') . "," .  
        valoresSelect('txtContaConta') . "," .  
        valoresSelect('txtJustificativa') . "," .  
        "'" . $_SESSION["login_usuario"] . "',1, 'Aguarda', 0, 0.00,0.00,0,0, 1, 1, 0, 0)") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_venda from sf_vendas order by id_venda desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }    
    if (is_numeric($_POST['txtId'])) {         
        $notIn = 0;        
        $item_id = $_POST['item_id'];
        $prod_id = $_POST['prod_id'];
        $prod_qt = $_POST['prod_qt'];
        if (is_array($item_id)) {
            for ($i = 0; $i < sizeof($item_id); $i++) {
                if (is_numeric($item_id[$i])) {
                    $notIn = $notIn . "," . $item_id[$i];
                }
            }
        }
        odbc_exec($con, "DELETE FROM sf_vendas_itens WHERE id_venda = " . $_POST['txtId'] . " and id_item_venda not in (" . $notIn . ")");
        if (is_array($item_id)) {
            for ($i = 0; $i < sizeof($item_id); $i++) {
                if (is_numeric($item_id[$i])) {
                    odbc_exec($con, "UPDATE sf_vendas_itens set quantidade = " . valoresNumericos2($prod_qt[$i]) . " WHERE id_item_venda = " . $item_id[$i]);
                } else {
                    odbc_exec($con, "insert into sf_vendas_itens (id_venda, grupo, produto, quantidade, valor_total, valor_bruto, valor_multa)
                    values (" . $_POST['txtId'] . ", (select conta_movimento from sf_produtos where conta_produto = " . valoresSelect2($prod_id[$i]) . "), " . 
                    valoresSelect2($prod_id[$i]) . ", " . valoresNumericos2($prod_qt[$i]) . ", 0.00, 0.00, 0.00)");
                }
            }
        }   
    }   
}

if (isset($_POST['bntAprov']) && is_numeric($_POST['txtId'])) {
    odbc_exec($con, "update sf_vendas set status = 'Aprovado', data_aprov = getDate() where id_venda = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
}

if (isset($_POST['bntReprov']) && is_numeric($_POST['txtId'])) {
    odbc_exec($con, "update sf_vendas set status = 'Reprovado', data_aprov = getDate() where id_venda = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
}

if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
        values ('sf_vendas', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'E', 'EXCLUSAO - Entradas/Saidas', GETDATE(), null)");        
        odbc_exec($con, "DELETE FROM sf_vendas_itens WHERE id_venda = " . $_POST['txtId']);
        odbc_exec($con, "DELETE FROM sf_vendas WHERE id_venda = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); window.top.location.href = 'Entradas-Saidas.php';</script>";
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_vendas where id_venda = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_venda'];
        $tipo = utf8_encode($RFP['cov']);
        $empresa = utf8_encode($RFP['empresa']);        
        $vendedor = utf8_encode($RFP['vendedor']);        
        $justificativa = utf8_encode($RFP['id_justificativa']);        
        $historico_venda = utf8_encode($RFP['historico_venda']);        
        $data_venda = escreverData($RFP['data_venda']);        
        $grupo = utf8_encode($RFP['grupo_conta']);
        $conta = utf8_encode($RFP['conta_movimento']);       
        $comentarios_venda = utf8_encode($RFP['comentarios_venda']);        
        $destinatario = utf8_encode($RFP['destinatario']);        
        $status = utf8_encode($RFP['status']);
    }
} else {
    $disabled = '';
    $id = '';
    $tipo = 'V';
    $empresa = $_SESSION["filial"];
    $vendedor = $_SESSION["login_usuario"];
    $justificativa = '';
    $historico_venda = '';
    $data_venda = getData("T");
    $grupo = 'null';
    $conta = 'null';
    $comentarios_venda = '';
    $destinatario = $_SESSION["login_usuario"];
    $status = 'Aguarda';
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Requisição</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1 style="margin-top:8px;float:left">Entradas/Saídas</h1>
                    </div>
                    <div class="row-fluid">
                        <form action="FormEntradas-Saidas.php" method="POST" name="frmEnviaDados" id="frmEnviaDados">
                            <div class="block">                                
                                <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>                                                                                                                  
                                <div style="width: 10%;float: left">
                                    <span>Tipo:</span>
                                    <select name="txtTipo" id="txtTipo" style="width:100%;" <?php echo $disabled; ?> class="select" >
                                        <option value="C" <?php echo ($tipo == "C" ? "SELECTED" : ""); ?>>ENTRADA</option>
                                        <option value="V" <?php echo ($tipo == "V" ? "SELECTED" : ""); ?>>SAÍDA</option>
                                    </select>
                                </div>    
                                <div style="width: 8%;float: left; margin-left: 1%;">
                                    <span>Loja:</span>
                                    <select name="txtEmpresa" id="txtEmpresa" style="width:100%;" <?php echo $disabled; ?> class="select" >
                                        <?php $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo utf8_encode($RFP['id_filial']); ?>" <?php
                                            if ($empresa == $RFP['id_filial']) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP['numero_filial']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>    
                                <div style="width: 18%;float: left; margin-left: 1%;">
                                    <span>Responsável:</span>
                                    <select name="txtVendedor" id="txtVendedor" class="select" style="width:100%" <?php echo $disabled; ?>>
                                        <option value="null" >Selecione</option>
                                        <?php $sql = "select UPPER(login_user) login_user,nome from sf_usuarios where inativo = 0 order by nome";
                                        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP["login_user"] ?>"<?php
                                            if (!(strcmp($RFP["login_user"], $vendedor))) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP["nome"]) ?></option>
                                        <?php } ?>
                                    </select>                            
                                </div>
                                <div style="width: 24%;float: left; margin-left: 1%;">
                                    <span>Justificativa:</span>
                                    <select name="txtJustificativa" id="txtJustificativa" class="select" style="width:100%" <?php echo $disabled; ?>>
                                        <option value="null">Selecione</option>
                                        <?php $sql = "select id,descricao from sf_justificativas";
                                        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP["id"] ?>"<?php
                                            if (!(strcmp($RFP["id"], $justificativa))) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                        <?php } ?>
                                    </select>                            
                                </div>
                                <div style="width: 27%;float: left; margin-left: 1%;">
                                    <span>Histórico:</span>
                                    <input name="txtHistorico" id="txtHistorico" type="text" class="input-xlarge" value="<?php echo $historico_venda; ?>" <?php echo $disabled; ?>/>
                                </div>  
                                <div style="width: 8%;float: left; margin-left: 1%;">
                                    <span>Data:</span>
                                    <input type="text" name="txtDtVenda" id="txtDtVenda" class="datepicker inputCenter" value="<?php echo $data_venda; ?>" <?php echo $disabled; ?>>
                                </div>  
                            </div>
                            <div class="block">
                                <div class="head dblue">
                                    <div class="icon"><span class="ico-money"></span></div>
                                    <h2>Produtos:</h2>                              
                                </div>                
                                <div class="data-fluid">
                                    <table id="tbProdutos" cellpadding="0" cellspacing="0" width="100%" class="table">
                                        <thead>
                                            <tr>
                                                <th width="25%">Grupos de Produtos</th>
                                                <th width="60%">Descrição do Produto</th>
                                                <th width="5%">Und.</th>
                                                <th width="5%">Qtd.</th>
                                                <th width="5%">Ação</th>
                                            </tr>
                                            <tr>
                                                <th width="25%">
                                                    <select id="txtGrupoP" class="select" style="width: 100%;" <?php echo $disabled; ?>>
                                                        <option value="null">Selecione</option>
                                                        <?php $cur = odbc_exec($con, "select id_contas_movimento,descricao from sf_contas_movimento where tipo = 'P' order by descricao") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                                <option value="<?php echo $RFP['id_contas_movimento'] ?>"><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </th>
                                                <th width="65%">
                                                    <input id="txtContaP" value="" type="hidden"/>
                                                    <input id="txtGrupoNomeP" value="" type="hidden"/>
                                                    <input id="txtNomeP" type="text" style="width:100%" value="" <?php echo $disabled; ?>/>
                                                </th>
                                                <th width="5%">
                                                    <input id="txtUnidadeP" type="text" class="input-xlarge inputCenter" value="" disabled/>
                                                </th>
                                                <th width="5%">
                                                    <input id="txtQtdP" <?php echo $disabled; ?> type="text" class="input-xlarge inputCenter" value="1,00"/>
                                                </th>
                                                <th width="5%">
                                                    <button class="btn btn-primary" id="for-block3" <?php echo $disabled; ?> type="button" onClick="novaLinhaP()">+</button>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>                                                                                        
                                        <?php 
                                            $i = 0;
                                            if (is_numeric($id)) {
                                                $cur = odbc_exec($con, "select id_item_venda, produto, quantidade, p.descricao, c.descricao grupo, unidade_comercial 
                                                from sf_vendas_itens i inner join sf_produtos p on i.produto = p.conta_produto
                                                left join sf_contas_movimento c on c.id_contas_movimento = p.conta_movimento
                                                where i.id_venda = " . $id . " and p.tipo = 'P' order by id_item_venda") or die(odbc_errormsg());
                                                while ($RFP = odbc_fetch_array($cur)) { $i++; ?>
                                                    <tr>
                                                        <td style="text-align:center">
                                                            <input type="hidden" name="item_id[]" value="<?php echo utf8_encode($RFP['id_item_venda']); ?>">                
                                                            <input type="hidden" name="prod_id[]" value="<?php echo utf8_encode($RFP['produto']); ?>">
                                                            <input type="hidden" name="prod_qt[]" value="<?php echo escreverNumero($RFP['quantidade']); ?>">
                                                            <?php echo utf8_encode($RFP['grupo']); ?>
                                                        </td>
                                                        <td><?php echo utf8_encode($RFP['descricao']); ?></td>
                                                        <td style="text-align:center"><?php echo utf8_encode($RFP['unidade_comercial']); ?></td>                                                        
                                                        <td style="text-align:center"><?php echo escreverNumero($RFP['quantidade']); ?></td>
                                                        <td style="width:46px; text-align:center"><input class="btn red" type="button" value="x" style="margin:0px; padding:2px 4px 3px 4px; line-height:10px;" onclick="removeLinha(this, 'tbProdutos');" <?php echo $disabled; ?>></td>
                                                    </tr>
                                                <?php }
                                            }
                                        ?>                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="4">Total</th>
                                                <th style="text-align:center"><?php echo $i; ?></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>                
                            </div>                                            
                            <div class="block">
                                <!--<div style="width: 50%;float: left;">
                                    <div class="head dblue">
                                        <div class="icon"><span class="ico-money"></span></div>
                                        <h2>Grupo de Contas:</h2>                              
                                    </div>                
                                    <div class="data-fluid">
                                        <div style="width: 50%;float: left;">
                                            <span>Grupo:</span>
                                            <select name="txtGrupoConta" id="txtGrupoConta" class="select" valor="<?php echo $grupo; ?>" style="width:100%" <?php echo $disabled; ?>>
                                                <option value="null">Selecione</option>                                                
                                            </select>                            
                                        </div>
                                        <div style="width: 49%;float: left; margin-left: 1%;">
                                            <span>Conta:</span>
                                            <select name="txtContaConta" id="txtContaConta" class="select" valor="<?php echo $conta; ?>" style="width:100%" <?php echo $disabled; ?>>
                                                <option value="null">Selecione</option>                                               
                                            </select>                            
                                        </div>
                                    </div>
                                </div>!-->
                                <div style="width: 49%;float: left;">
                                    <div class="head yellow">
                                        <div class="icon"><span class="ico-money"></span></div>
                                        <h2>Comentários:</h2>                              
                                    </div>                
                                    <div class="data-fluid">
                                        <textarea name="txtComentarios" id="txtComentarios" <?php echo $disabled; ?> cols="45" rows="10"><?php echo $comentarios_venda; ?></textarea>
                                    </div>
                                </div>                                
                            </div>
                            <div class="block">
                                <hr style="margin: 0px 0;">
                            </div>
                            <div class="block">                                
                                <div style="width: 50%;float: left;">
                                    <div style="width: 60%;float: left">
                                        <span>Responsável:</span>
                                        <select name="txtDestinatario" id="txtDestinatario" class="select" style="width:100%" <?php echo $disabled; ?>>
                                            <option value="null" >Selecione</option>
                                            <?php $cur = odbc_exec($con, "select UPPER(login_user) login_user,nome from sf_usuarios 
                                            where inativo = 0 and login_user != 'Admin' and 
                                            (master = 0 or master in (select id_permissoes from sf_usuarios_permissoes where est_aent_sai = 1)) 
                                            order by nome") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP["login_user"] ?>"<?php
                                                if (!(strcmp($RFP["login_user"], $destinatario))) {
                                                    echo "SELECTED";
                                                }
                                                ?>><?php echo utf8_encode($RFP["nome"]) ?></option>
                                            <?php } ?>
                                        </select>                            
                                    </div>
                                    <div style="width: 39%;float: left; margin-left: 1%;">                                        
                                        <span>Status:</span>
                                        <input name="txtStatusVenda" id="txtStatusVenda" value="<?php echo $status; ?>" disabled type="text" class="input-medium"/>
                                    </div>
                                </div>
                            </div>        
                            <div style="clear:both;height: 5px;"></div>
                            <div class="frmfoot">
                                <div style="float:left">
                                <?php if (($disabled == 'disabled') && ($status == 'Aguarda') && (strtolower($_SESSION['login_usuario']) == strtolower($destinatario))) { ?>
                                    <button class="btn btn-success" type="submit" name="bntAprov" title="Aprovar" id="bntAprovar" ><span class="icon-ok icon-white"></span> Aprovar</button>
                                    <button class="btn red" type="button" title="Reprovar" id="bntReprov" onclick="reprovar();"><span class="icon-remove icon-white"></span> Reprovar</button>
                                <?php } ?>
                                </div>                                    
                                <div class="frmbtn">
                                    <?php if ($disabled == '') { ?>
                                        <button class="btn green" type="submit" name="bntSave" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                                        <?php if ($_POST['txtId'] == '') { ?>
                                        <button class="btn yellow" type="button" onClick="window.top.location.href = 'Entradas-Saidas.php';" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                                        <?php } else { ?>
                                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK" ><span class="ico-reply"></span> Cancelar</button>
                                            <?php
                                        }
                                    } else { ?>
                                        <button class="btn green" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                                        <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                                        <button class="btn red" type="submit" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                                    <?php } ?>
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="dialog" id="source" title="Source"></div>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/moment.min.js"></script>    
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>  
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script language="javascript">
        
        $("#txtDtVenda").mask(lang["dateMask"]); 
        $("#txtQtdP").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
        
        $("#txtTipo").change(function () {
            if ($(this).val()) {
                GrupoConta($(this).val());
            }
        });
        
        $("#txtGrupoConta").change(function () {
            if ($(this).val()) {
                ContaConta($(this).val());                
            }
        });
        
        GrupoConta($("#txtTipo").val());
        
        function GrupoConta(id) {
            let id_sel = $("#txtGrupoConta").attr("valor");
            $.getJSON("grupo.ajax.php", {txtTipo: id}, function (j) {
                var options = "<option value='null'>Selecione</option>";
                for (var i = 0; i < j.length; i++) {
                    options += "<option value='" + j[i].id_grupo_contas + "'>" + j[i].descricao + "</option>";
                }
                $("#txtGrupoConta").html(options);
                $("#txtGrupoConta").select2("val", id_sel);
                ContaConta($("#txtGrupoConta").val());                
            });
        }        
        
        function ContaConta(id) {
            let id_sel = $("#txtContaConta").attr("valor");
            $.getJSON("conta.ajax.php", {txtGrupo: id}, function (j) {
                var options = "<option value='null'>Selecione</option>";
                for (var i = 0; i < j.length; i++) {
                    options += "<option value='" + j[i].id_contas_movimento + "'>" + j[i].descricao + "</option>";
                }
                $("#txtContaConta").html(options);
                $("#txtContaConta").select2("val", id_sel);                
            });
        }

        $("#txtNomeP").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "./../Seguro/produto.ajax.php",
                    dataType: "json",
                    data: {t: request.term, v: "P", grupo: $("#txtGrupoP").val()},
                    success: function (data) {
                        response(data);
                    }
                });
            }, minLength: 3,
            select: function (event, ui) {
                $("#txtContaP").val(ui.item.id);
                $("#txtNomeP").val(ui.item.value);               
                $("#txtGrupoNomeP").val(ui.item.grupo);
                $('#txtUnidadeP').val(ui.item.unidade_comercial);
                $('#txtQtdP').val(numberFormat(ui.item.quantidade_comercial));
            }
        });        
        
        function novaLinhaP() {
            if ($("#txtContaP").val() !== "" && $("#txtQtdP").val() !== "") {
                $("#tbProdutos tbody").append(`<tr><td style="text-align:center">
                <input type="hidden" name="item_id[]" value="">
                <input type="hidden" name="prod_id[]" value="${$("#txtContaP").val()}">
                <input type="hidden" name="prod_qt[]" value="${$("#txtQtdP").val()}">
                ${$("#txtGrupoNomeP").val()}</td>
                <td>${$("#txtNomeP").val()}</td>
                <td style="text-align:center">${$("#txtUnidadeP").val()}</td>
                <td style="text-align:center">${$("#txtQtdP").val()}</td>
                <td style="width:46px; text-align:center">
                <input class="btn red" type="button" value="x" style="margin:0px; padding:2px 4px 3px 4px; line-height:10px;" 
                onclick="removeLinha(this, 'tbProdutos');"></td></tr>`);
                limparLinhaP();
                $("#tbProdutos tfoot tr > th:last").html($("#tbProdutos tbody tr").length);
            } else {
                bootbox.alert("Todos os campos devem ser preenchidos.");
            }
        }
        
        function limparLinhaP() {
            $("#txtGrupoNomeP").val("");            
            $("#txtContaP").val("");
            $("#txtNomeP").val("");               
            $('#txtUnidadeP').val("");
            $('#txtQtdP').val("1,00");            
        }
        
        $('#txtGrupoP').change(function () {
            limparLinhaP();
        });                                                      
        
        function removeLinha(ln, table) {
            bootbox.confirm('Deseja realmente excluir esse item?', function (result) {
                if (result === true) {
                    $(ln).closest("tr").remove();
                    $("#" + table + " tfoot tr > th:last").html($("#" + table + " tbody tr").length);
                }
            });
        }           
        
        function reprovar() {
            let bb = bootbox.prompt({title: "Confirma a reprovação desta Entradas/Saídas? <br>Preencha o motivo abaixo:", inputType: 'textarea', callback: function (result) {
                if (result === null) {
                    return;            
                } else if (result.length > 0) {
                    $('#frmEnviaDados').append('<input type="hidden" name="bntReprov" value="' + result + '"/>');
                    $('#frmEnviaDados').submit();
                } else {            
                    bb.find('.bootbox-input-textarea').css("border-color", "red");
                    return false;
                }
            }});        
        }
        
    </script>
</body>
<?php odbc_close($con); ?>
</html>