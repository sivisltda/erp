<?php

require_once(__DIR__ . '/../../Connections/configini.php');
$aColumns = array('conta_produto', 'sf_contas_movimento.descricao descr', 'p.descricao descricao', 'p.preco_venda preco_venda');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY conta_produto asc ";
$sOrder2 = " ORDER BY conta_produto asc ";
$sLimit = 20;
$imprimir = 0;
$mdl_seg_ = returnPart($_SESSION["modulos"], 14);

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (is_numeric($_GET['gp'])) {
    $sWhereX .= " and p.conta_movimento = " . $_GET['gp'];
}

if (is_numeric($_GET['at'])) {
    $sWhereX .= " and p.inativa = " . $_GET['at'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) < 3) {
                $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 0) . " " . $_GET['sSortDir_' . $i] . ", ";
                $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 1) . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY conta_produto asc";
        $sOrder2 = " ORDER BY conta_produto asc";
    }
}

if ($_GET['txtBusca'] != "") {
    $sWhereX = $sWhereX . " and (sf_contas_movimento.descricao like '%" . utf8_decode($_GET['txtBusca']) . "%' or p.descricao like '%" . utf8_decode($_GET['txtBusca']) . "%')";
}

$sQuery = "SELECT COUNT(*) total from sf_produtos p left join sf_contas_movimento on p.conta_movimento = sf_contas_movimento.id_contas_movimento where conta_produto > 0 and p.tipo = 'C' $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total from sf_produtos p left join sf_contas_movimento on p.conta_movimento = sf_contas_movimento.id_contas_movimento where conta_produto > 0 and p.tipo = 'C' $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,conta_produto,p.descricao, sf_contas_movimento.descricao descr, p.preco_venda, p.editavel, p.segMin, p.segMax,
(STUFF((SELECT '|' + case when parcela = -2 then 'PIX' when parcela = -1 then 'BOL' when parcela = 0 then 'DCC' else cast(parcela as varchar) end + ' - ' + FORMAT(valor_unico,'N2') from sf_produtos_parcelas where id_produto = conta_produto order by parcela FOR XML PATH('')), 1, 1, '')) list_valores,
(STUFF((SELECT '|' + pp.descricao from sf_produtos_materiaprima mp inner join sf_produtos pp on mp.id_materiaprima = pp.conta_produto where pp.tipo = 'A' and mp.id_produtomp = p.conta_produto order by mp.id_prodmp FOR XML PATH('')), 1, 1, '')) list_acessorios,
(STUFF((SELECT '|' + FORMAT(valor,'N2') + ' - ' + nome_parentesco from sf_produtos_acrescimo left join sf_parentesco on sf_produtos_acrescimo.id_parentesco = sf_parentesco.id_parentesco where id_produto = p.conta_produto order by nome_parentesco FOR XML PATH('')), 1, 1, '')) list_parentesco,
(STUFF((SELECT '|' + pp.descricao from sf_produtos_acrescimo_servicos mp inner join sf_produtos pp on mp.id_servico = pp.conta_produto where mp.id_produto = p.conta_produto order by mp.id_produto FOR XML PATH('')), 1, 1, '')) list_adesao,
(STUFF((SELECT '|' + pp.descricao from sf_produtos_veiculo_tipos mp inner join sf_veiculo_tipos pp on mp.id_tipo = pp.id where mp.id_produto = p.conta_produto order by mp.id_produto FOR XML PATH('')), 1, 1, '')) tipo_veiculo, 
(select pp.descricao from sf_produtos pp where pp.conta_produto = p.conta_produto_adesao) conta_produto_adesao,mostrar_site, favorito, necessita_turma, parcelar_site, convenios_meses_site, ano_piso,
gf.descricao grupo_fabricante, rg.descricao regioes, cs.descricao convenio_desc, adesao_soma, cota_defaut, cota_defaut_tp
from sf_produtos p 
left join sf_contas_movimento on p.conta_movimento = sf_contas_movimento.id_contas_movimento
left join sf_convenios cs on cs.id_convenio = p.convenios_site
left join sf_veiculo_grupo gf on gf.id = p.grupo_fabricantes
left join sf_regioes rg on rg.id = p.grupo_cidades
where conta_produto > 0 and p.tipo = 'C' " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder2;
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["descr"]) . "'>" . utf8_encode($aRow["descr"]) . "</div>";
    if ($aRow['editavel'] == 0) {
        $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[$aColumns[0]]) . ",2)'><div id='formPQ' title='" . utf8_encode($aRow["descricao"]) . "'>" . utf8_encode($aRow["descricao"]) . "</div></a>";
    } else {
        $row[] = "<strong><div id='formPQ' title='" . utf8_encode($aRow["descricao"]) . "'>" . utf8_encode($aRow["descricao"]) . "</div></strong>";
    }
    if ($mdl_seg_ > 0) {
        $row[] = "<div id='formPQ' style=\"text-align: right\" title='" . escreverNumero($aRow['segMin'], 1) . "'>" . escreverNumero($aRow['segMin'], 1) . "</div>";
        $row[] = "<div id='formPQ' style=\"text-align: right\" title='" . escreverNumero($aRow['segMax'], 1) . "'>" . escreverNumero($aRow['segMax'], 1) . "</div>";    
    }
    $row[] = "<div id='formPQ' style=\"text-align: right\" title='" . escreverNumero($aRow['preco_venda'], 1) . "'>" . escreverNumero($aRow['preco_venda'], 1) . "</div>";
    if ($imprimir == 0 && $aRow['editavel'] == 0) {
        $row[] = "<center><a title='Excluir' onClick=\"return confirm('Deseja deletar esse registro?');\" href='Planos.php?Del=" . $aRow[$aColumns[0]] . $toReturnAnd . "'><input name='' type='image' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
    } else if ($imprimir == 0 && $aRow['editavel'] == 1) {
        $row[] = "";
    } else {
        $row[] = utf8_encode($aRow['list_valores']);        
        $row[] = utf8_encode($aRow['list_acessorios']);
        $row[] = utf8_encode($aRow['list_parentesco']);
        $row[] = utf8_encode($aRow['conta_produto_adesao']);
        $row[] = ($aRow['mostrar_site'] == 1 ? "SIM" : "NÃO");
        $row[] = ($aRow['favorito'] == 1 ? "SIM" : "NÃO");
        $row[] = ($aRow['necessita_turma'] == 1 ? "SIM" : "NÃO");
        $row[] = ($aRow['parcelar_site'] == 1 ? "SIM" : "NÃO");
        $row[] = utf8_encode($aRow['convenio_desc']);
        $row[] = utf8_encode($aRow['convenios_meses_site']);
        $row[] = utf8_encode($aRow['regioes']);
        $row[] = utf8_encode($aRow['grupo_fabricante']);
        $row[] = utf8_encode($aRow['tipo_veiculo']);
        $row[] = utf8_encode($aRow['ano_piso']);
        $row[] = ($aRow['adesao_soma'] == 1 ? "SIM" : "NÃO");        
        $row[] = escreverNumero($aRow['cota_defaut']) . ($aRow['cota_defaut_tp'] == 1 ? "($)" : "(%)");
    }
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
