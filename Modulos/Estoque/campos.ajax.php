<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include '../../Connections/configini.php';

$id_area = $_REQUEST['txtCampos'];
$local = array();
$sql = "select id_tipo_documento_campos,campo, isnull(dias,0) dias from sf_tipo_documento_campos inner join sf_tipo_documento
                on sf_tipo_documento.id_tipo_documento = sf_tipo_documento_campos.tipo_documento where sf_tipo_documento_campos.inativo = 0 AND sf_tipo_documento_campos.tipo_documento = " . $id_area . " ORDER BY 1";
$cur = odbc_exec($con, $sql) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $local[] = array('id_tipo_documento_campos' => $RFP['id_tipo_documento_campos'], 'campo' => utf8_encode($RFP['campo']), 'dias' => utf8_encode($RFP['dias']));
}
echo( json_encode($local) );
odbc_close($con);