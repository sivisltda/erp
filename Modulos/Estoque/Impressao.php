<?php
include "../../Connections/configini.php";
$PegaURL = "";
$CheckOrcamentoImagem = '0';
$adm_associado = "Cliente";

if ($_GET["crt"] != "") {
    if (is_numeric($_GET["crt"])) {
        $contrato2 = $_GET["crt"];
    }
}

$cur = odbc_exec($con, "select * from dbo.sf_configuracao");
while ($RFP = odbc_fetch_array($cur)) {
    $CheckOrcamentoImagem = $RFP['FIN_ORC_FOTO'];
    $adm_associado = utf8_encode($RFP['adm_associado']);
}

$cur = odbc_exec($con, "select A.assinatura from sf_usuarios A where A.login_user = '" . $_SESSION["login_usuario"] . "'");
while ($RFP = odbc_fetch_array($cur)) {
    $Assinatura = $RFP['assinatura'];
}

if ($_GET["id"] != "") {
    $PegaURL = $_GET["id"];
    $cur = odbc_exec($con, "select * from sf_vendas inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = sf_vendas.cliente_venda 
                            left join tb_estados e on f.estado = e.estado_codigo left join tb_cidades c on f.cidade = c.cidade_codigo where id_venda = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $dados1 = "";
        $dados2 = "";
        $id = $RFP["id_venda"];
        $cliente_venda = $RFP["cliente_venda"];
        $vendedor = utf8_encode($RFP["vendedor"]);
        $data_venda = escreverData($RFP["data_venda"]);
        $historico_venda = utf8_encode($RFP["historico_venda"]);
        $comentarios_venda = utf8_encode($RFP["comentarios_venda"]);
        $sys_login = utf8_encode($RFP["sys_login"]);
        $destinatario = utf8_encode($RFP["destinatario"]);
        $status = utf8_encode($RFP["status"]);
        $tipo = utf8_encode($RFP["cov"]);
        $garantia_prod = utf8_encode($RFP["garantia_venda"]);
        $garantia_serv = utf8_encode($RFP["garantia_servico"]);
        $entrega = utf8_encode($RFP["entrega"]);
        $validade = utf8_encode($RFP["validade"]);
        $transportadora = utf8_encode($RFP['orc_frete_trans']);
        $valor_frete = escreverNumero($RFP['orc_frete_val']);
        $n_servicos = utf8_encode($RFP['n_servicos']);
        if ($RFP["endereco"] != "") {
            $dados1 = utf8_encode($RFP["endereco"]);
        }
        if ($RFP["numero"] != "") {
            $dados1 = $dados1 . " N° " . utf8_encode($RFP["numero"]);
        }
        if ($RFP["complemento"] != "") {
            $dados1 = $dados1 . " " . utf8_encode($RFP["complemento"]);
        }
        if ($RFP["bairro"] != "") {
            $dados1 = $dados1 . ", " . utf8_encode($RFP["bairro"]);
        }
        if ($RFP["cidade_nome"] != "") {
            $dados1 = $dados1 . "\n" . utf8_encode($RFP["cidade_nome"]);
        }
        if ($RFP["estado_sigla"] != "") {
            $dados1 = $dados1 . "-" . utf8_encode($RFP["estado_sigla"]);
        }
        if ($RFP["cep"] != "") {
            $dados1 = $dados1 . ", Cep: " . utf8_encode($RFP["cep"]);
        }
        if ($RFP["cnpj"] != "") {
            $dados2 = $dados2 . "CNPJ/CPF: " . utf8_encode($RFP["cnpj"]);
        }
        if ($RFP["telefone_contato"] != "") {
            $dados2 = $dados2 . "\n" . "Tel: " . utf8_encode($RFP["telefone_contato"]);
        }
        if ($RFP["email_contato"] != "") {
            $dados2 = $dados2 . " E-mail: " . utf8_encode($RFP["email_contato"]);
        }
    }
    $i = 0;
    $r_quantidade = "1";
    $r_valor_tot = 0;
    $cur = odbc_exec($con, "select sa_descricao,data_parcela,valor_parcela from dbo.sf_venda_parcelas 
              where inativo = 0 and venda = " . $PegaURL . " order by id_parcela");
    while ($RFP = odbc_fetch_array($cur)) {
        if ($i == 0) {
            $r_documento = $RFP["sa_descricao"];
            $r_ven_ini = escreverData($RFP["data_parcela"]);
        }
        $r_valor = escreverNumero($RFP["valor_parcela"]);
        $r_valor_tot = $r_valor_tot + $RFP["valor_parcela"];
        $i++;
    }
    $r_valor_tot = escreverNumero($r_valor_tot);
    if ($i > 0) {
        $r_quantidade = $i;
    }
} else {
    $id = "";
    $cliente_venda = "";
    $vendedor = "";
    $data_venda = getData("T");
    $historico_venda = "";
    $comentarios_venda = "";
    $sys_login = "";
    $dados1 = "";
    $dados2 = "";
    $tipo = "";
    $status = "";
    $garantia_prod = "";
    $garantia_serv = "";
    $entrega = "";
    $validade = "";
    $transportadora = "";
    $valor_frete = "";
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />        
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <style type="text/css">
            body {
                margin:0px;
                padding:0px;
            }
            table {
                font: 12px Arial;
                margin-bottom: 10px;
                border: 1px solid #999;
                border-collapse: collapse;
            }
            .modelo {
                height: 50px;
                font: 14px Arial;
                font-weight: Bold;
            }
            .titulo {
                font: 14px Arial;
                font-weight: Bold;
                padding: 2px;
                padding-left: 5px;
            }
            .tabela {
                height: 18px;
                padding-left: 5px;
                padding-right: 5px;
            }
            .assinatura {
                font: 12px Arial;
                position:absolute;
                bottom:0;
                width:100%;
            }
            .assinatura p{
                line-height: 1px;
            }
        </style>
    </head>
    <body>
        <div id="main" style="height:880px; overflow:hidden; margin-bottom:100px">
            <div id="cont">
                <table width="100%" border="1">
                    <tr>
                        <td width="540px" rowspan="3" style="padding-right:5px;background:url('./../../Pessoas/<?php echo $_SESSION["contrato"]; ?>/Empresa/logo_.jpg') no-repeat;background-position:20px 15px ;background-size: 150px 50px;"><div style="float:right; text-align:right;font-size: 9px;"><?php echo $_SESSION["cabecalho"]; ?></div></td>
                        <td width="200px" class="modelo" colspan="2" align="center"><?php
                            if ($tipo == "V") {
                                echo "VENDA";
                            } else if ($tipo == "C") {
                                echo "COMPRA";
                            } else if ($tipo == "O") {
                                echo "ORÇAMENTO";
                            } else {
                                echo "PEDIDO";
                            }
                            ?></td>
                    </tr>
                    <tr>
                        <td width="100px" style="padding-left:5px">Proposta Nº:</td>
                        <td width="100px" align="center"><?php echo $id . "/" . substr($data_venda, -4); ?></td>
                    </tr>
                    <tr>
                        <td style="padding-left:5px">Data:</td>
                        <td align="center"><?php echo $data_venda; ?></td>
                    </tr>
                </table>
                <table width="100%" border="1">
                    <tr style="background-color: lightgrey">
                        <td class="titulo" colspan="3">Cliente</td>
                    </tr>
                    <tr>
                        <?php
                        $cur = odbc_exec($con, "select razao_social from sf_fornecedores_despesas where id_fornecedores_despesas = '" . $cliente_venda . "'") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) {
                            ?>
                            <td width="540px" style="padding-left:5px"><b><?php echo utf8_encode($RFP["razao_social"]); ?></b><br><?php echo $dados1; ?></td>
                        <?php } ?>
                        <td width="215px" style="padding-left:5px"><b>Endereço de Entrega:</b><br><?php echo $dados2; ?></td>
                    </tr>
                    <tr>
                        <td width="540px;" style="padding-left:5px"><b>Responsável:</b></td>
                        <?php
                        $cur = odbc_exec($con, "select nome from sf_usuarios where UPPER(login_user) = UPPER('" . $vendedor . "')") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) {
                            ?>
                            <td width="215px" style="padding-left:5px"><?php echo utf8_encode($RFP["nome"]); ?></td>
                        <?php } ?>
                    </tr>
                </table>
                <table width="100%" border="1" style="margin-bottom: 0;">
                    <tr style="background-color: lightgrey">
                        <td class="titulo" colspan="<?php
                        if ($CheckOrcamentoImagem == 1) {
                            echo "7";
                        } else {
                            echo "6";
                        }
                        ?>">Produtos</td>
                    </tr>
                    <tr>
                        <td width="35px" align="center">Item</td>
                        <td width="55px" align="center">Qde.</td>
                        <td width="39px" align="center">Und.</td>
                        <td width="410px" <?php
                        if ($CheckOrcamentoImagem == "1") {
                            echo "colspan=\"2\"";
                        }
                        ?> align="center">Descrição</td>
                        <td width="96px" align="center">Valor Unitário</td>
                        <td width="96px" align="center">Valor Total</td>
                    </tr>
                    <?php
                    if ($PegaURL != "") {
                        $loop = 1;
                        $sub_total = 0;
                        $cur = odbc_exec($con, "select id_item_venda,v.conta_movimento grupo,gc.descricao grupodesc,conta_produto produto, cm.descricao,
                        case when len(isnull(cm.desc_prod,'')) > 0 then cm.desc_prod else cm.descricao collate SQL_Latin1_General_CP1_CI_AS end produtodesc,
                        quantidade,valor_total,descontop,descontos,descontotp,descontots, unidade_comercial  from sf_vendas_itens vi left join sf_contas_movimento gc on vi.grupo = gc.id_contas_movimento inner join sf_produtos cm
                        on cm.conta_produto = vi.produto inner join sf_vendas v on v.id_venda = vi.id_venda where cm.tipo = 'P' and vi.id_venda = " . $PegaURL);
                        while ($RFP = odbc_fetch_array($cur)) {
                            $valor_unitario = $RFP["valor_total"] / $RFP["quantidade"];
                            $sub_total = $sub_total + $RFP["valor_total"];
                            if ($RFP["descontotp"] == 0) {
                                $desc_valor = $sub_total * ($RFP["descontop"] / 100);
                                $desconto = "%";
                            } else {
                                $desc_valor = $RFP["descontop"];
                                $desconto = "$";
                            }
                            ?>
                            <tr>
                                <td class="tabela" align="center"><?php echo $loop; ?></td>
                                <td class="tabela" align="center"><?php echo $RFP["quantidade"]; ?></td>
                                <td class="tabela" align="center"><?php echo $RFP["unidade_comercial"]; ?></td>
                                <?php if ($CheckOrcamentoImagem == "1") { ?>
                                    <td class="tabela" width="7.5%">
                                        <img src="<?php
                                        if (file_exists("./../../Produtos/" . $contrato2 . "/" . $RFP["produto"] . ".png")) {
                                            echo "./../../Produtos/" . $contrato2 . "/" . $RFP["produto"] . ".png";
                                        } else {
                                            echo "./../../img/test.png";
                                        }
                                        ?>" style="width:64px; height:64px"/>
                                    </td>
                                <?php } ?>                                
                                <td class="tabela"><?php
                                    echo utf8_encode($RFP["descricao"]);
                                    if ($tipo == "O") {
                                        ?> <br><span style="font-size: 8px;"><?php echo utf8_encode($RFP["produtodesc"]); ?></span><?php } ?></td>
                                <td class="tabela"><div style="float:right"><?php echo escreverNumero($valor_unitario, 1); ?></div></td>
                                <td class="tabela"><div style="float:right"><?php echo escreverNumero($RFP["valor_total"], 1); ?></div></td>
                            </tr>
                            <?php
                            $loop++;
                        }
                    }
                    ?>
                    <tr>
                        <td class="tabela" colspan="<?php
                        if ($CheckOrcamentoImagem == "1") {
                            echo "5";
                        } else {
                            echo "4";
                        }
                        ?>"></td>
                        <td class="tabela">Subtotal</td>
                        <td class="tabela"><div style="float:right"><?php echo escreverNumero($sub_total, 1); ?></div></td>
                    </tr>
                    <tr>
                        <td class="tabela" colspan="<?php
                        if ($CheckOrcamentoImagem == "1") {
                            echo "4";
                        } else {
                            echo "3";
                        }
                        ?>">Garantia:</td>
                        <td class="tabela"><?php echo $garantia_prod; ?></td>
                        <td class="tabela">Desconto (<?php echo $desconto; ?>)</td>
                        <td class="tabela"><div style="float:right"><?php echo escreverNumero($desc_valor, 1); ?></div></td>
                    </tr>
                    <tr>
                        <td class="tabela" colspan="<?php
                        if ($CheckOrcamentoImagem == "1") {
                            echo "5";
                        } else {
                            echo "4";
                        }
                        ?>" style="border-bottom: none;"></td>
                        <td class="tabela" style="border-bottom: none;"><b>Total Produto</b></td>
                        <td class="tabela" style="border-bottom: none;"><div style="float:right"><b><?php echo escreverNumero(($sub_total - $desc_valor), 1); ?></b></div></td>
                    </tr>
                    <?php $TotalProduto = $sub_total - $desc_valor; ?>
                </table>
                <table width="100%" border="1" style="margin-top: -1px;">
                    <tr style="background-color: lightgrey">
                        <td class="titulo" colspan="6" >Serviços</td>
                    </tr>
                    <tr>
                        <td width="37px" align="center">Item</td>
                        <td width="57px" align="center">Qde.</td>
                        <td width="43px" align="center">Und.</td>
                        <td width="403px" align="center">Descrição</td>
                        <td width="105px" align="center">Valor Unitário</td>
                        <td width="105px" align="center">Valor Total</td>
                    </tr>
                    <?php
                    if ($PegaURL != "") {
                        $loop = 1;
                        $sub_total = 0;
                        $desc_valor = 0;
                        $cur = odbc_exec($con, "select id_item_venda,v.conta_movimento grupo,gc.descricao grupodesc,conta_produto produto,cm.descricao,
                                                case when len(isnull(cm.desc_prod,'')) > 0 then cm.desc_prod else cm.descricao collate SQL_Latin1_General_CP1_CI_AS end produtodesc,
                                                quantidade,valor_total,descontop,descontos,descontotp,descontots, unidade_comercial from sf_vendas_itens vi left join sf_contas_movimento gc on vi.grupo = gc.id_contas_movimento inner join sf_produtos cm
                                                on cm.conta_produto = vi.produto inner join sf_vendas v on v.id_venda = vi.id_venda where cm.tipo = 'S' and vi.id_venda = " . $PegaURL);
                        while ($RFP = odbc_fetch_array($cur)) {
                            $valor_unitario = $RFP["valor_total"] / $RFP["quantidade"];
                            $sub_total = $sub_total + $RFP["valor_total"];
                            if ($RFP["descontots"] == 0) {
                                $desc_valor = $sub_total * ($RFP["descontos"] / 100);
                                $desconto = "%";
                            } else {
                                $desc_valor = $RFP["descontos"];
                                $desconto = "$";
                            } ?>
                            <tr>
                                <td class="tabela" align="center"><?php echo $loop; ?></td>
                                <td class="tabela" align="center"><?php echo $RFP["quantidade"]; ?></td>
                                <td align="center"><?php echo $RFP["unidade_comercial"]; ?></td>
                                <td class="tabela"><?php
                                    echo utf8_encode($RFP["descricao"]);
                                    if ($tipo == "O") {
                                        ?> <br><span style="font-size: 8px;"><?php echo utf8_encode($RFP["produtodesc"]); ?></span><?php } ?></td>
                                <td class="tabela"><div style="float:right"><?php echo escreverNumero($valor_unitario, 1); ?></div></td>
                                <td class="tabela"><div style="float:right"><?php echo escreverNumero($RFP["valor_total"], 1); ?></div></td>
                            </tr>
                            <?php
                            $loop++;
                        }
                    } ?>
                    <tr>
                        <td class="tabela" colspan="4"></td>
                        <td class="tabela">Subtotal</td>
                        <td class="tabela"><div style="float:right"><?php echo escreverNumero($sub_total, 1); ?></div></td>
                    </tr>
                    <tr>
                        <td class="tabela" colspan="3">Garantia:</td>
                        <td class="tabela"><?php echo $garantia_serv; ?></td>
                        <td class="tabela">Desconto (<?php echo $desconto; ?>)</td>
                        <td class="tabela"><div style="float:right"><?php echo escreverNumero($desc_valor, 1); ?></div></td>
                    </tr>
                    <tr>
                        <td class="tabela" colspan="4"></td>
                        <td class="tabela"><b>Total Serviço</b></td>
                        <td class="tabela"><div style="float:right"><b><?php echo escreverNumero(($sub_total - $desc_valor), 1); ?></b></div></td>
                    </tr>
                    <?php $TotalServico = $sub_total - $desc_valor; ?>
                    <?php
                    if ($n_servicos == 1) {
                        $TotalGeral = $TotalProduto;
                    } else {
                        $TotalGeral = $TotalProduto + $TotalServico;
                    }
                    ?>
                    <tr>
                        <td style="background-color: lightgrey;" colspan="5"><b>Total Geral</b></td>
                        <td style="background-color: lightgrey;text-align: right;" ><b><?php echo escreverNumero($TotalGeral, 1); ?></b></td>
                    </tr>
                </table>
                <table width="100%" border="1">
                    <tr style="background-color: lightgrey">
                        <td width="100%" align="center"><b>Observações</b></td>
                    </tr>
                    <tr>
                        <td class="tabela"><?php echo nl2br($comentarios_venda); ?></td>
                    </tr>
                </table>
                <table width="100%" border="1">
                    <tr style="background-color: lightgrey">
                        <td class="titulo" colspan="2">Condições Comerciais</td>
                    </tr>
                    <tr>
                        <td class="tabela" width="140px">Prazo de Entrega:</td>
                        <td align="center" width="630px"><?php echo $entrega; ?></td>
                    </tr>
                    <tr>
                        <td class="tabela">Forma de Pagamento:</td>
                        <?php
                        $parcelas = "";
                        $sql = "select * from sf_venda_parcelas where inativo = 0 and venda = " . $PegaURL;
                        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) {
                            $parcelas = $parcelas . escreverNumero($RFP["valor_parcela"], 1) . " - " . escreverData($RFP["data_parcela"]) . ", ";
                        }
                        ?>
                        <td align="center" style="<?php
                        if ($parcelas != "") {
                            echo "font-size: 9px";
                        }
                        ?>"><?php
                                if ($parcelas == "") {
                                    echo "A COMBINAR";
                                } else {
                                    echo substr($parcelas, 0, -2);
                                }
                                ?></td>
                    </tr>
                    <tr>
                        <td class="tabela">Validade da Proposta:</td>
                        <td align="center"><?php echo $validade; ?></td>
                    </tr>
                    <tr>
                        <td class="tabela">Frete:</td>
                        <td align="center"><?php
                            if ($transportadora == "1") {
                                echo "A PAGAR " . $valor_frete;
                            } else {
                                echo escreverNumero(0);
                            }
                            ?></td>
                    </tr>                                        
                </table>
            </div>
        </div>
        <?php if ($tipo !== "O") { ?>
            <div id="foot_0" style="width:100%; position:absolute; top:888px;">
                <table width="100%" border="1">
                    <tr>
                        <td width="33.3%" style="height:50px"></td>
                        <td width="33.4%"></td>
                        <?php if ($tipo == "V") { ?>
                        <td width="33.3%"></td>
                        <?php } ?>                
                    </tr>
                    <tr>
                        <td align="center">Visto Responsável</td>
                        <td align="center">Visto Supervisor</td>
                        <?php if ($tipo == "V") { ?>
                        <td align="center">Visto <?php echo $adm_associado;?></td>
                        <?php } ?>                
                    </tr>
                    <tr>
                        <td colspan="<?php echo ($tipo == "V" ? "3" : "2"); ?>" style="font-size:8px"><div style="width:10%; float:left">&nbsp;</div><div style="width:80%; float:left; text-align:center">Gerado pelo sistema SIVIS Finance - www.sivis.com.br</div><div id="pag" style="width:10%; float:left; text-align:right">Página 1/1</div></td>
                    </tr>
                </table>
            </div>
        <?php } else { ?>
            <div id="foot_0" class="assinatura"> 
                <?php echo $Assinatura; ?>            
            </div>   
        <?php } ?>
    </body>
    <script type="text/javascript" src="../../js/util.js"></script>      
    <script type="text/javascript">
        $(document).ready(function () {
            if ($("#main").height() < $("#cont").height()) {
                var maxloop = Math.ceil($("#cont").height() / $("#main").height());
                for (i = 1; i < maxloop; i++) {
                    var copy = $("#cont").clone().attr("id", "copy").css({"margin-top": "-" + ($("#main").height() * i) + "px", "height": $("#main").height() * (i + 1) + "px"});
                    $("body").append("<div id=\"copy_" + i + "\" style=\"height:895px; overflow:hidden; page-break-before:always\"></div>");
                    $("#copy_" + i).append(copy);
                    var foot = $("#foot_0").clone().attr("id", "foot_" + i).css({"top": 888 + (980 * i) + "px"});
                    $("body").append(foot);
                    $("#foot_0 #pag").html("Página 1/" + maxloop);
                    $("#foot_" + i + " #pag").html("Página " + (i + 1) + "/" + maxloop);
                }
            }
        });        
        
        $(window).load(function () {
            window.print();
            setTimeout(function () {
                window.close();
            }, 500);
        });
    </script>
    <?php odbc_close($con); ?>
</html>