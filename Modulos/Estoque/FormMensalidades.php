<?php include '../../Connections/configini.php'; 
$mdl_srs_ = returnPart($_SESSION["modulos"], 9);
$mdl_clb_ = returnPart($_SESSION["modulos"],12);
?>
<link rel="icon" type="image/ico" href="../../../favicon.ico"/>
<link href="../../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../../css/main.css" rel="stylesheet">
<link href="../../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' src='../../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<style>
    body { font-family: sans-serif; }
    .selected { background: #acbad4 !important;}
</style>
<body>
    <form action="FormMensalidades.php" name="frmMensalidades" method="POST">
        <div class="frmhead">
            <div class="frmtext">Gerar Mensalidades</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont" style="height: 350px;">
            <div style="width:100%; float:left;">
                <div class="" style="display:flex;">
                    <div style="flex:1;">
                        <span style="display:block;">Status do Cliente:</span>
                        <select name="itemsStat[]" id="mscStat" multiple="multiple" class="select" style="width:100%" class="input-medium">
                            <option value="1">Ativo</option>
                            <option value="4">Ativo Credencial</option>
                            <option value="14">Ativo Abono</option>
                            <?php if ($mdl_seg_ == 0) { ?>
                            <option value="5">Ativo Ausente 1</option>
                            <option value="6">Ativo Ausente 2</option>
                            <option value="7">Ativo Ausente 3</option>
                            <option value="8">Ativo Ausente 4</option>
                            <option value="9">Ativo Ausente 5</option>
                            <?php } ?>
                            <option value="12">Ativo Em Aberto</option>
                            <option value="2">Suspenso</option>
                            <option value="16">Cancelado</option>
                            <option value="3">Inativo</option>
                            <?php if ($mdl_srs_ == 1) { ?>
                                <option value="10">Serasa</option>
                            <?php } if ($mdl_clb_ == 1 && $mdl_seg_ == 0) { ?>
                                <option value="11">Dependente</option>
                                <option value="13">Desligado</option>
                                <option value="15">Desligado Em Aberto</option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>            
            <div style="width:100%; float:left;">
                <span>Plano(s):</span>
                <select name="itemsPlano[]" id="mscPlano" multiple="multiple" class="select" style="width:100%" class="input-medium">
                    <?php $cur = odbc_exec($con, "SELECT p.conta_produto, p.descricao from sf_produtos p inner join sf_produtos_parcelas on id_produto = conta_produto where p.tipo = 'C' and p.conta_produto > 0 and parcela = 1") or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) { ?>
                        <option value="<?php echo $RFP['conta_produto'] ?>"><?php echo utf8_encode($RFP['descricao']) ?></option>
                    <?php } ?>
                </select>
            </div>           
            <div style="width:100%; float:left;">
                <span>Cliente / Prospect:</span>
                <input type="hidden" id="txtAluno" name="txtAluno" value="">
                <input type="text" id="txtAlunoNome" style="width:100%" value=""/>
            </div>            
            <div style="width: 30%;float: left">
                <span>Mês:</span>
                <select id="txtMes" style="width:100%">
                    <option value="01" <?php echo (date('m') == "1" ? "selected" : ""); ?>>Janeiro</option>
                    <option value="02" <?php echo (date('m') == "2" ? "selected" : ""); ?>>Fevereiro</option>
                    <option value="03" <?php echo (date('m') == "3" ? "selected" : ""); ?>>Março</option>
                    <option value="04" <?php echo (date('m') == "4" ? "selected" : ""); ?>>Abril</option>
                    <option value="05" <?php echo (date('m') == "5" ? "selected" : ""); ?>>Maio</option>
                    <option value="06" <?php echo (date('m') == "6" ? "selected" : ""); ?>>Junho</option>
                    <option value="07" <?php echo (date('m') == "7" ? "selected" : ""); ?>>Julho</option>
                    <option value="08" <?php echo (date('m') == "8" ? "selected" : ""); ?>>Agosto</option>
                    <option value="09" <?php echo (date('m') == "9" ? "selected" : ""); ?>>Setembro</option>
                    <option value="10" <?php echo (date('m') == "10" ? "selected" : ""); ?>>Outubro</option>
                    <option value="11" <?php echo (date('m') == "11" ? "selected" : ""); ?>>Novembro</option>
                    <option value="12" <?php echo (date('m') == "12" ? "selected" : ""); ?>>Dezembro</option>
                </select>
            </div>
            <div style="width: 20%;float: left; margin-left: 1%;">
                <span>Ano:</span>
                <input name="txtAno" id="txtAno" type="text" class="input-medium" maxlength="4" value="<?php echo date("Y"); ?>"/>
            </div>
            <div style="width: 20%;float: left; margin-left: 1%;">
                <span>Parcelas:</span>
                <input name="txtParcelas" id="txtParcelas" type="text" class="input-medium" maxlength="2" value="1"/>
            </div>
            <div style="width: 27%;float: left; margin-left: 1%;">
                <span>Tipo:</span>
                <select id="txtTipo" style="width:100%">
                    <option value="1">Mensal</option>
                    <option value="0">DCC</option>
                    <option value="-1">BOL</option>
                    <option value="-2">PIX</option>
                </select>
            </div>            
            <div style="clear:both;"></div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <button class="btn green" type="button" id="bntEnviar" ><span class="ico-checkmark"></span> Gravar</button>
                <button class="btn yellow" onClick="parent.FecharBox()"><span class="ico-reply"></span> Cancelar</button>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../../js/plugins.js'></script>
    <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type="text/javascript">

        $("#txtAno").mask("9999");
        $("#txtParcelas").mask("99");

        $("#txtAlunoNome").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "../../Modulos/CRM/ajax.php",
                    dataType: "json",
                    data: {q: request.term, t: "C','P"},
                    success: function (data) {
                        response(data);
                    }
                });
            }, minLength: 3,
            select: function (event, ui) {
                $("#txtAluno").val(ui.item.id);
                $("#txtAlunoNome").val(ui.item.value);
            }
        });

        $("#bntEnviar").click(function () {
            bootbox.confirm('Confirma o lançamento destas mensalidades?', function (result) {
                if (result === true) {
                    if (validar()) {
                        processar(0);
                    }
                }
            });
        });

        function processar(i) {
            if (i < textToNumber($("#txtParcelas").val())) {
                let data_lanc = moment("01/" + $("#txtMes").val() + "/" + $("#txtAno").val(), "DD-MM-YYYY").add(i, 'month');
                let info = data_lanc.format('MM') + "/" + data_lanc.format('YYYY');
                $.post("form/FormMensalidades.php", "btnSave=S&cliente=" + $("#txtAluno").val() +
                        "&plano=" + $("#mscPlano").val() + "&sta=" + $("#mscStat").val() +
                        "&mes=" + data_lanc.format('MM') + "&ano=" + data_lanc.format('YYYY') + 
                        "&tipo=" + $("#txtTipo").val()).done(function (data) {
                    if (data.trim() === "YES") {
                        console.log("inicio " + info);
                    }
                    processar(i + 1);
                });
            } else {
                bootbox.alert(i + " mês(es) gerado(s) com sucesso!");
            }
        }

        function validar() {
            if (!$.isNumeric($("#txtAno").val()) || textToNumber($("#txtAno").val()) < 2000 || textToNumber($("#txtAno").val()) > 2200) {
                bootbox.alert("Preencha um valor correto para o ano!");
                return false;
            } else if (!$.isNumeric($("#txtParcelas").val()) || textToNumber($("#txtParcelas").val()) < 1 || textToNumber($("#txtParcelas").val()) > 12) {
                bootbox.alert("Preencha um valor correto para a parcela de 1 a 12!");
                return false;
            } else {
                return true;
            }
        }

    </script>        
    <?php odbc_close($con); ?>
</body>
