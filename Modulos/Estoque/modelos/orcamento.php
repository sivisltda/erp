<?php
$protocolo = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http');
$host = $_SERVER['HTTP_HOST'];
$UrlAtual = $protocolo . '://' . $host;
$PegaURL = "";
$CheckOrcamentoImagem = '0';
$contrato2 = $_SESSION['contrato'];
$totProdutos = 0;
$totServicos = 0;
$adm_associado = "Cliente";

function quebralinha($texto, $max) {
    $retorno = "";
    if (strlen($texto) > 0) {
        for ($i = 0; $i <= (strlen($texto) / $max); $i++) {
            $retorno .= substr($texto, $i * $max, $max) . "<br>";
        }
    }
    return $retorno;
}

$cur = odbc_exec($con, "select * from dbo.sf_configuracao");
while ($RFP = odbc_fetch_array($cur)) {
    $CheckOrcamentoImagem = $RFP['FIN_ORC_FOTO'];
    $adm_associado = utf8_encode($RFP['adm_associado']);    
}

if ($_GET["id"] != "") {
    $cur = odbc_exec($con, "select *,(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = id_fornecedores_despesas) email_contato
    from sf_fornecedores_despesas f left join tb_estados e on f.estado = e.estado_codigo left join tb_cidades c on f.cidade = c.cidade_codigo 
    inner join sf_vendas on f.id_fornecedores_despesas = sf_vendas.cliente_venda
    where id_venda = " . $_GET['id']);
    while ($RFP = odbc_fetch_array($cur)) {
        $dados1 = "";
        $dados2 = "";
        $dados3 = "";
        $id = $RFP["id_venda"];
        $cliente_venda = $RFP["cliente_venda"];
        $vendedor = utf8_encode($RFP["vendedor"]);
        $id_veiculo = utf8_encode($RFP['id_veiculo']);        
        $data_venda = escreverData($RFP["data_venda"]);
        $historico_venda = utf8_encode($RFP["historico_venda"]);
        $comentarios_venda = utf8_encode($RFP["comentarios_venda"]);
        $sys_login = utf8_encode($RFP["sys_login"]);
        $destinatario = utf8_encode($RFP["destinatario"]);
        $status = utf8_encode($RFP["status"]);
        $tipo = utf8_encode($RFP["cov"]);
        $garantia_prod = utf8_encode($RFP["garantia_venda"]);
        $garantia_serv = utf8_encode($RFP["garantia_servico"]);
        $entrega = $RFP["entrega"];
        $validade = utf8_encode($RFP["validade"]);
        $transportadora = utf8_encode($RFP['orc_frete_trans']);
        $valor_frete = escreverNumero($RFP['orc_frete_val']);
        $n_servicos = utf8_encode($RFP['n_servicos']);
        $filial_orc = str_pad($RFP["empresa"], 3, "0", STR_PAD_LEFT);
        if ($RFP["endereco"] != "") {
            $dados1 = utf8_encode($RFP["endereco"]);
        }
        if ($RFP["numero"] != "") {
            $dados1 = $dados1 . " N° " . utf8_encode($RFP["numero"]);
        }
        if ($RFP["complemento"] != "") {
            $dados1 = $dados1 . " " . utf8_encode($RFP["complemento"]);
        }
        if ($RFP["bairro"] != "") {
            $dados1 = $dados1 . ", " . utf8_encode($RFP["bairro"]);
        }
        if ($RFP["cidade_nome"] != "") {
            $dados1 = $dados1 . "\n" . utf8_encode($RFP["cidade_nome"]);
        }
        if ($RFP["estado_sigla"] != "") {
            $dados1 = $dados1 . "-" . utf8_encode($RFP["estado_sigla"]);
        }
        if ($RFP["cep"] != "") {
            $dados1 = $dados1 . ", Cep: " . utf8_encode($RFP["cep"]);
        }
        if ($RFP["cnpj"] != "") {
            $dados2 = $dados2 . "CNPJ/CPF: " . utf8_encode($RFP["cnpj"]);
        }
        if ($RFP["telefone_contato"] != "") {
            $dados2 = $dados2 . "\n" . "Tel: " . utf8_encode($RFP["telefone_contato"]);
        }
        if ($RFP["email_contato"] != "") {
            $dados2 = $dados2 . " E-mail: " . utf8_encode($RFP["email_contato"]);
        }
    }
    $i = 0;
    $r_quantidade = "1";
    $r_valor_tot = 0;
    $cur = odbc_exec($con, "select sa_descricao,data_parcela,valor_parcela from sf_venda_parcelas
                            where inativo = 0 and venda = " . $_GET['id'] . " order by id_parcela");
    while ($RFP = odbc_fetch_array($cur)) {
        if ($i == 0) {
            $r_documento = $RFP["sa_descricao"];
            $r_ven_ini = escreverData($RFP["data_parcela"]);
        }
        $r_valor = escreverNumero($RFP["valor_parcela"]);
        $r_valor_tot = $r_valor_tot + $RFP["valor_parcela"];
        $i++;
    }
    $cur = odbc_exec($con, "select sum(case when cm.tipo = 'P' then 1 else 0 end) Produtos,
                            sum(case when cm.tipo = 'S' then 1 else 0 end) Servicos from sf_vendas_itens vi left join sf_contas_movimento gc on vi.grupo = gc.id_contas_movimento inner join sf_produtos cm
                            on cm.conta_produto = vi.produto inner join sf_vendas v on v.id_venda = vi.id_venda where --cm.tipo = 'S' and 
                            vi.id_venda = " . $_GET['id']);
    while ($RFP = odbc_fetch_array($cur)) {
        $totProdutos = $RFP["Produtos"];
        $totServicos = $RFP["Servicos"];
    }
    $r_valor_tot = escreverNumero($r_valor_tot);
    if ($i > 0) {
        $r_quantidade = $i;
    }
} else {
    $id = "";
    $cliente_venda = "";
    $vendedor = "";
    $id_veiculo = "";
    $data_venda = getData("T");
    $historico_venda = "";
    $comentarios_venda = "";
    $sys_login = "";
    $dados1 = "";
    $dados2 = "";
    $tipo = "";
    $status = "";
    $garantia_prod = "";
    $garantia_serv = "";
    $validade = "";
    $entrega = "";
    $transportadora = "";
    $valor_frete = "";
    $filial_orc = "0";
}

if (is_numeric($id_veiculo)) {
    $query = " SELECT sf_fornecedores_despesas_veiculo.*, md.id id_modelo 
    FROM sf_fornecedores_despesas_veiculo 
    left join sf_veiculo_marca m on marca = m.descricao
    left join sf_veiculo_modelo md on modelo = md.descricao and m.id = md.id_marca        
    WHERE sf_fornecedores_despesas_veiculo.id = " . $id_veiculo;
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) { 
        $dados3 = utf8_encode($RFP["placa"]) . " - " . utf8_encode($RFP["modelo"]) . " (" . utf8_encode($RFP["marca"]) . ") \n" . utf8_encode($RFP["combustivel"]) . " - " . utf8_encode($RFP["ano_modelo"]);
    }
}

$cur = odbc_exec($con, "select A.assinatura from sf_usuarios A where UPPER(A.login_user) = UPPER('" . $vendedor . "')");
while ($RFP = odbc_fetch_array($cur)) {
    $Assinatura = $RFP['assinatura'];
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
            body {
                margin:0px;
                padding:0px;
            }
            table {
                font: 10px Calibri;
                margin-bottom: 10px;
                border: 1px solid #999;
                border-collapse: collapse;
                vertical-align: middle;
            }
            .titulo {
                font: 14px Calibri;
                font-weight: Bold;
                padding: 2px;
                padding-left: 5px;
            }
            .tabela {
                height: 18px;
                padding-left: 5px;
                padding-right: 5px;
            }
            .assinatura {
                font: 12px Arial;
                bottom:0;
                width:100%;
            }
            .assinatura p{
                line-height: 1px;
            }
        </style>
    </head>
    <body>
        <div id="main" style="width: 700px; overflow:hidden;">
            <table width="100%" border="1" cellpadding="2" cellspacing="0">
                <tr>
                    <td width="500px" rowspan="3">
                        <table width="100%" cellpadding="0" border="0" cellspacing="0" style="border:0px solid white;padding-bottom: 15px !important;">
                            <tr>
                                <td width="200px">
                                    <?php if (file_exists("./../Pessoas/" . $_SESSION["contrato"] . "/Empresa/logo_" . $filial_orc . ".jpg")) { ?>
                                        <div style="line-height: 5px;">&nbsp;</div><img src ="./../Pessoas/<?php echo $_SESSION["contrato"]; ?>/Empresa/logo_<?php echo $filial_orc; ?>.jpg" width="150" height="50" align="middle" />
                                    <?php } else { ?>
                                        <div style="line-height: 5px;">&nbsp;</div>
                                    <?php } ?>
                                </td>
                                <td width="265px" >
                                    <div style="text-align:right;font-size: 8px;"><?php echo $_SESSION["cabecalho"]; ?></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="200px" colspan="2" align="center" style="line-height: 25px;font-size:16px;"><b><?php
                            if ($tipo == "V") {
                                echo "VENDA";
                            } else if ($tipo == "C") {
                                echo "COMPRA";
                            } else if ($tipo == "O") {
                                echo "ORÇAMENTO";
                            } else {
                                echo "PEDIDO";
                            }
                            ?></b></td>
                </tr>
                <tr>
                    <td width="100px" style="padding-left:5px">Proposta Nº:</td>
                    <td width="100px" align="center"><?php echo $id . "/" . substr($data_venda, -4); ?></td>
                </tr>
                <tr>
                    <td style="padding-left:5px">Data:</td>
                    <td align="center"><?php echo $data_venda; ?></td>
                </tr>
            </table>
            <br><br>
            <table width="700px" border="1">
                <tr style="background-color: lightgrey">
                    <td class="titulo">Cliente</td>
                </tr>
                <tr>
                    <?php
                    $cur = odbc_exec($con, "select razao_social from sf_fornecedores_despesas where id_fornecedores_despesas = '" . $cliente_venda . "'") or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) { ?>
                        <td width="700px" style="padding-left:5px;"><b><?php echo utf8_encode($RFP["razao_social"]); ?></b><br><?php echo $dados2; ?></td>
                    <?php } ?>
                </tr>
                <tr>                    
                    <td width="700px" style="padding-left:5px;"><b>Endereço:</b><br><?php echo $dados1; ?></td>    
                </tr>
                <?php if (is_numeric($id_veiculo)) { ?>
                <tr>                    
                    <td width="700px" style="padding-left:5px;"><b>Dados do Veículo:</b><br><?php echo $dados3; ?></td>    
                </tr>
                <?php } ?>                
                <tr>                    
                    <?php
                    $cur = odbc_exec($con, "select nome from sf_usuarios where UPPER(login_user) = UPPER('" . $vendedor . "')") or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) { ?>
                        <td width="700px;" style="padding-left:5px;"><b>Responsável:</b> <?php echo utf8_encode($RFP["nome"]); ?></td>
                    <?php } ?>
                </tr>
            </table>
            <br><br>
            <?php if ($totServicos > 0) { ?>
                <table width="700px" border="1">
                    <tr style="background-color: lightgrey">
                        <td class="titulo" colspan="6" >Serviços</td>
                    </tr>
                    <tr>
                        <td width="33px" align="center"><b>Item</b></td>
                        <td width="58px" align="center"><b>Qde.</b></td>
                        <td width="35px" align="center"><b>Und.</b></td>
                        <td width="374px" align="center"><b>Descrição</b></td>
                        <td width="100px" align="center"><b>Val.Unit.</b></td>
                        <td width="100px" align="center"><b>Val.Total</b></td>
                    </tr>
                    <?php
                    if ($_GET['id'] != "") {
                        $loop = 1;
                        $sub_total = 0;
                        $cur = odbc_exec($con, "select id_item_venda,v.conta_movimento grupo,gc.descricao grupodesc,conta_produto produto,cm.descricao, cm.desc_prod as produtodesc,
                                            quantidade,valor_total,descontop,descontos,descontotp,descontots, unidade_comercial from sf_vendas_itens vi left join sf_contas_movimento gc on vi.grupo = gc.id_contas_movimento inner join sf_produtos cm
                                            on cm.conta_produto = vi.produto inner join sf_vendas v on v.id_venda = vi.id_venda where cm.tipo = 'S' and vi.id_venda = " . $_GET['id']);
                        while ($RFP = odbc_fetch_array($cur)) {
                            $valor_unitario = $RFP["valor_total"] / $RFP["quantidade"];
                            $sub_total = $sub_total + $RFP["valor_total"];
                            if ($RFP["descontots"] == 0) {
                                $desc_valor = $sub_total * ($RFP["descontos"] / 100);
                                $desconto = "%";
                            } else {
                                $desc_valor = $RFP["descontos"];
                                $desconto = "$";
                            }
                            ?>
                            <tr>
                                <td class="tabela" align="center"><?php echo $loop; ?></td>
                                <td class="tabela" align="center"><?php echo escreverNumero($RFP["quantidade"]); ?></td>
                                <td align="center"><?php echo $RFP["unidade_comercial"]; ?></td>
                                <td class="tabela"><?php
                                    echo utf8_encode($RFP["descricao"]);
                                    if ($tipo == "O") {
                                        ?><span style="font-size: 8px;"><?php echo str_replace("<em>", "", utf8_encode($RFP["produtodesc"])); ?></span><?php } ?></td>
                                <td class="tabela"><div style="text-align:right"><?php echo escreverNumero($valor_unitario); ?></div></td>
                                <td class="tabela"><div style="text-align:right"><?php echo escreverNumero($RFP["valor_total"]); ?></div></td>
                            </tr>
                            <?php
                            $loop++;
                        }
                    }
                    ?>
                    <tr>
                        <td class="tabela" colspan="4"></td>
                        <td class="tabela">Subtotal</td>
                        <td class="tabela"><div style="text-align:right"><?php echo escreverNumero($sub_total, 1); ?></div></td>
                    </tr>
                    <tr>
                        <td class="tabela" colspan="3">Garantia:</td>
                        <td class="tabela"><?php echo $garantia_serv; ?></td>
                        <td class="tabela">Desconto (<?php echo $desconto; ?>)</td>
                        <td class="tabela"><div style="text-align:right"><?php echo escreverNumero($desc_valor, 1); ?></div></td>
                    </tr>
                    <tr>
                        <td class="tabela" colspan="4"></td>
                        <td class="tabela"><b>Total Serviço</b></td>
                        <td class="tabela"><div style="text-align:right"><b><?php echo escreverNumero(($sub_total - $desc_valor), 1); ?></b></div></td>
                    </tr>
                    <?php $TotalServico = $sub_total - $desc_valor; ?>
                </table>            
            <?php } if ($totProdutos > 0) { ?>
                <table width="700px" border="1">
                    <tr style="background-color: lightgrey">
                        <td class="titulo" colspan="<?php
                        if ($CheckOrcamentoImagem == 1) {
                            echo "7";
                        } else {
                            echo "6";
                        }
                        ?>">Produtos</td>
                    </tr>
                    <tr>
                        <td width="33px" align="center"><b>Item</b></td>
                        <td width="58px" align="center"><b>Qde.</b></td>
                        <td width="35px" align="center"><b>Und.</b></td>
                        <td width="374px" <?php
                        if ($CheckOrcamentoImagem == "1") {
                            echo "colspan=\"2\"";
                        }
                        ?> align="center"><b>Descrição</b></td>
                        <td width="100px" align="center"><b>Val.Unit.</b></td>
                        <td width="100px" align="center"><b>Val.Total</b></td>
                    </tr>
                    <?php
                    if ($_GET['id'] != "") {
                        $loop = 1;
                        $sub_total = 0;
                        $cur = odbc_exec($con, "select id_item_venda,v.conta_movimento grupo,gc.descricao grupodesc,conta_produto produto, cm.descricao, cm.desc_prod as produtodesc,
                                                quantidade,valor_total,descontop,descontos,descontotp,descontots, unidade_comercial  from sf_vendas_itens vi left join sf_contas_movimento gc on vi.grupo = gc.id_contas_movimento inner join sf_produtos cm
                                                on cm.conta_produto = vi.produto inner join sf_vendas v on v.id_venda = vi.id_venda where cm.tipo = 'P' and vi.id_venda = " . $_GET['id']);
                        while ($RFP = odbc_fetch_array($cur)) {
                            $valor_unitario = $RFP["valor_total"] / $RFP["quantidade"];
                            $sub_total = $sub_total + $RFP["valor_total"];
                            if ($RFP["descontotp"] == 0) {
                                $desc_valor = $sub_total * ($RFP["descontop"] / 100);
                                $desconto = "%";
                            } else {
                                $desc_valor = $RFP["descontop"];
                                $desconto = "$";
                            }
                            ?>
                            <tr>
                                <td class="tabela" align="center"><?php echo $loop; ?></td>
                                <td class="tabela" align="center"><?php echo escreverNumero($RFP["quantidade"]); ?></td>
                                <td class="tabela" align="center"><?php echo $RFP["unidade_comercial"]; ?></td>
                                <?php if ($CheckOrcamentoImagem == "1") { ?>
                                    <td class="tabela" width="75px">
                                        <img src="<?php
                                        if (file_exists("./../Produtos/" . $contrato2 . "/" . $RFP["produto"] . ".png")) {
                                            echo "./../Produtos/" . $contrato2 . "/" . $RFP["produto"] . ".png";
                                        } else {
                                            echo "./../img/test.png";
                                        }
                                        ?>" style="width:64px; height:64px"/>
                                    </td>
                                    <td class="tabela" width="299px"><?php
                                        echo utf8_encode($RFP["descricao"]);
                                        if ($tipo == "O") {
                                            ?><span style="font-size: 8px;"><?php echo str_replace("<em>", "", str_replace("</em>", "", utf8_encode($RFP["produtodesc"]))); ?></span><?php } ?></td>
                                    <?php } else { ?>
                                    <td class="tabela"><?php
                                        echo utf8_encode($RFP["descricao"]);
                                        if ($tipo == "O") {
                                            ?><span style="font-size: 8px;"><?php echo str_replace("<em>", "", str_replace("</em>", "", utf8_encode($RFP["produtodesc"]))); ?></span><?php } ?></td>
                                    <?php } ?>
                                <td class="tabela"><div style="text-align:right"><?php echo escreverNumero($valor_unitario); ?></div></td>
                                <td class="tabela"><div style="text-align:right"><?php echo escreverNumero($RFP["valor_total"]); ?></div></td>
                            </tr>
                            <?php
                            $loop++;
                        }
                    }
                    ?>
                    <tr>
                        <td class="tabela" colspan="<?php
                        if ($CheckOrcamentoImagem == "1") {
                            echo "5";
                        } else {
                            echo "4";
                        }
                        ?>"></td>
                        <td class="tabela">Subtotal</td>
                        <td class="tabela"><div style="text-align:right;"><?php echo escreverNumero($sub_total, 1); ?></div></td>
                    </tr>
                    <tr>
                        <td class="tabela" colspan="<?php
                        if ($CheckOrcamentoImagem == "1") {
                            echo "4";
                        } else {
                            echo "3";
                        }
                        ?>">Garantia:</td>
                        <td class="tabela"><?php echo $garantia_prod; ?></td>
                        <td class="tabela">Desconto (<?php echo $desconto; ?>)</td>
                        <td class="tabela"><div style="text-align:right;"><?php echo escreverNumero($desc_valor, 1); ?></div></td>
                    </tr>
                    <tr>
                        <td class="tabela" colspan="<?php
                        if ($CheckOrcamentoImagem == "1") {
                            echo "5";
                        } else {
                            echo "4";
                        }
                        ?>" style="border-bottom: none;"></td>
                        <td class="tabela" style="border-bottom: none;"><b>Total Produto</b></td>
                        <td class="tabela" style="border-bottom: none;"><div style="text-align:right;"><b><?php echo escreverNumero(($sub_total - $desc_valor), 1); ?></b></div></td>
                    </tr>
                    <?php $TotalProduto = $sub_total - $desc_valor; ?>
                    <?php
                    if ($n_servicos == 1) {
                        $TotalGeral = $TotalProduto;
                    } else {
                        $TotalGeral = $TotalProduto + $TotalServico;
                    }
                    ?>
                    <tr>
                        <td style="background-color: lightgrey;" colspan="5"><b>Total Geral</b></td>
                        <td style="background-color: lightgrey;text-align: right;"><b><?php echo escreverNumero($TotalGeral, 1); ?></b></td>
                    </tr>                    
                </table>            
            <?php } ?>
            <br><br>
            <table width="700px" border="1">
                <tr style="background-color: lightgrey">
                    <td width="100%" align="center"><b>Observações</b></td>
                </tr>
                <tr>
                    <td class="tabela"><?php echo nl2br($comentarios_venda); ?></td>
                </tr>
            </table>
            <br><br>
            <table width="700px" border="1">
                <tr style="background-color: lightgrey">
                    <td class="titulo" colspan="2">Condições Comerciais</td>
                </tr>
                <tr>
                    <td class="tabela" width="114px">Prazo de Entrega:</td>
                    <td align="center" width="586px"><?php echo $entrega; ?></td>
                </tr>
                <tr>
                    <td class="tabela">Forma de Pag.:</td>
                    <?php
                    $parcelas = "";
                    $sql = "select * from sf_venda_parcelas where inativo = 0 and venda = " . $_GET['id'];
                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) {
                        $parcelas = $parcelas . escreverNumero($RFP["valor_parcela"], 1) . " - " . escreverData($RFP["data_parcela"]) . ", ";
                    }
                    ?>
                    <td align="center" style="<?php
                    if ($parcelas != "") {
                        echo "font-size: 9px";
                    }
                    ?>"><?php
                            if ($parcelas == "") {
                                echo "A COMBINAR";
                            } else {
                                echo substr($parcelas, 0, -2);
                            }
                            ?></td>
                </tr>
                <tr>
                    <td class="tabela">Valid. da Proposta:</td>
                    <td align="center"><?php echo $validade; ?></td>
                </tr>
                <tr>
                    <td class="tabela">Frete:</td>
                    <td align="center"><?php
                        if ($transportadora == "1") {
                            echo "A PAGAR " . $valor_frete;
                        } else {
                            echo escreverNumero(0);
                        }
                        ?></td>
                </tr>
            </table>
        </div>
        <div id="foot_0" class="assinatura">
            <?php
            if ($tipo == "O") {
                echo $Assinatura;
            } else {
                ?>
                <table width="100%" border="1" cellspacing="0">
                    <tr>
                        <td style="height:50px; width:33%"></td>
                        <td style="height:50px; width:34%"></td>
                        <td style="height:50px; width:33%"></td>
                    </tr>
                    <tr>
                        <td style="text-align:center">Visto Atendente</td>
                        <td style="text-align:center">Visto Superior</td>
                        <td style="text-align:center">Visto <?php echo $adm_associado;?></td>
                    </tr>
                    <tr>
                        <td style="height:25px; text-align:center; vertical-align:bottom">____/____/________</td>
                        <td style="height:25px; text-align:center; vertical-align:bottom">____/____/________</td>
                        <td style="height:25px; text-align:center; vertical-align:bottom">____/____/________</td>
                    </tr>
                </table>
                <div style="font-size: 10px;margin-top: 15px;">&nbsp;&nbsp;Gerado pelo sistema SIVIS Finance - www.sivis.com.br</div></div>
            <?php } ?>
    </div>
</body>
</html>