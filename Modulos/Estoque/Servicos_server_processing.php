<?php
if (!isset($_GET['pdf'])) {
    include '../../Connections/configini.php';
}
$aColumns = array('conta_produto', 'sf_contas_movimento.descricao descr', 'p.descricao descricao', 'p.valor_custo_medio valor_custo_medio', 'p.preco_venda preco_venda');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY conta_produto asc ";
$sOrder2 = " ORDER BY conta_produto asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}
if (is_numeric($_GET['gp'])) {
    $sWhereX .= " and p.conta_movimento = " . $_GET['gp'];
}
if (is_numeric($_GET['at'])) {
    $sWhereX .= " and p.inativa = " . $_GET['at'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) < 3) {
                $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 0) . " " . $_GET['sSortDir_' . $i] . ", ";
                $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 1) . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY conta_produto asc";
        $sOrder2 = " ORDER BY conta_produto asc";
    }
}

if ($_GET['sSearch'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= prepareCollum($aColumns[$i], 0) . " LIKE '%" . utf8_decode($_GET['sSearch']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

$sQuery = "SELECT COUNT(*) total from sf_produtos p left join sf_contas_movimento on p.conta_movimento = sf_contas_movimento.id_contas_movimento where p.tipo = 'S' $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total from sf_produtos p left join sf_contas_movimento on p.conta_movimento = sf_contas_movimento.id_contas_movimento where p.tipo = 'S' $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,conta_produto,p.descricao, sf_contas_movimento.descricao descr, 
p.preco_venda,p.valor_custo_medio, p.editavel, p.codigo_interno, p.ativar_convite, p.alterar_valor, 
p.valor_comissao_venda, p.unidade_comercial, p.inativa, p.favorito, desc_prod
from sf_produtos p left join sf_contas_movimento on p.conta_movimento = sf_contas_movimento.id_contas_movimento 
where p.tipo = 'S' " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder2;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<center>" . utf8_encode($aRow["codigo_interno"]) . "</center>";    
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["descr"]) . "'>" . utf8_encode($aRow["descr"]) . "</div>";
    if ($aRow['editavel'] == 0) {
        $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[$aColumns[0]]) . ")'><div id='formPQ' title='" . utf8_encode($aRow["descricao"]) . "'>" . utf8_encode($aRow["descricao"]) . "</div></a>";
    } else {
        $row[] = "<strong><div id='formPQ' title='" . utf8_encode($aRow["descricao"]) . "'>" . utf8_encode($aRow["descricao"]) . "</div></strong>";
    }
    $row[] = "<center>" . ($aRow['ativar_convite'] == 1 ? "SIM" : "NÃO") . "</center>";
    $row[] = "<center>" . ($aRow['alterar_valor'] == 1 ? "SIM" : "NÃO") . "</center>";
    $row[] = "<div id='formPQ' style=\"text-align: right\" title='" . escreverNumero($aRow['valor_custo_medio'],1) . "'>" . escreverNumero($aRow['valor_custo_medio'],1) . "</div>";
    $row[] = "<div id='formPQ' style=\"text-align: right\" title='" . escreverNumero($aRow['preco_venda'],1) . "'>" . escreverNumero($aRow['preco_venda'],1) . "</div>";
    if ($imprimir == 0) {
        if ($aRow['editavel'] == 0) {
            $row[] = "<center><a title='Excluir' onClick=\"return confirm('Deseja deletar esse registro?');\" href='Servicos.php?Del=" . $aRow[$aColumns[0]] . $toReturnAnd . "'><input name='' type='image' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
        } else {
            $row[] = "";
        }
    } else {
        $row[] = escreverNumero($aRow['valor_comissao_venda'],1);    
        $row[] = utf8_encode($aRow["unidade_comercial"]);    
        $row[] = ($aRow['inativa'] == 1 ? "SIM" : "NÃO");    
        $row[] = ($aRow['favorito'] == 1 ? "SIM" : "NÃO");    
        $row[] = utf8_encode($aRow["desc_prod"]);            
    }
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}