<?php

include "../../Connections/configini.php";
$novaFoto = "0_t.png";
if (is_numeric($_GET['id'])) {
    $novaFoto = $_GET['id'] . "_t.png";
}

$imgUrl = "./../../Produtos/" . $contrato . "/" . $novaFoto;
$imgInitW = $_POST['imgInitW'];
$imgInitH = $_POST['imgInitH'];
$imgW = $_POST['imgW'];
$imgH = $_POST['imgH'];
$imgY1 = $_POST['imgY1'];
$imgX1 = $_POST['imgX1'];
$cropW = $_POST['cropW'];
$cropH = $_POST['cropH'];
$output_filename = str_replace("_t.png", "_c.png", $imgUrl);
$what = getimagesize($imgUrl);
switch (strtolower($what['mime'])) {
    case 'image/png':
        $source_image = imagecreatefrompng($imgUrl);
        break;
    case 'image/jpeg':
        $source_image = imagecreatefromjpeg($imgUrl);
        break;
    case 'image/gif':
        $source_image = imagecreatefromgif($imgUrl);
        break;
    default: die('image type not supported');
}
$resizedImage = imagecreatetruecolor($imgW, $imgH);
$whiteBackground = imagecolorallocate($resizedImage, 255, 255, 255);
imagefill($resizedImage, 0, 0, $whiteBackground);
imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
$dest_image = imagecreatetruecolor($cropW, $cropH);
imagecopyresampled($dest_image, $resizedImage, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
imagejpeg($dest_image, $output_filename, 100);
if (file_exists($imgUrl)) {
    unlink($imgUrl);
}
$response = array(
    "status" => 'success',
    "url" => $output_filename
);
print json_encode($response);
odbc_close($con);
