<?php
include "../../Connections/configini.php";

$disabled = 'disabled';
if (isset($_POST['bntSave']) && is_numeric($_POST['txtId'])) {
    $item_id = $_POST['txtIdItem'];    
    $cotacao_id = $_POST['txtIdCotacao'];
    $query = "update sf_requisicao set destinatario_cotacao = " . valoresTexto('txtDestinatario') . "," .
    "status_cotacao = 'Aguarda' where id_requisicao = " . valoresSelect("txtId");
    odbc_exec($con, $query);              
    if (is_array($cotacao_id)) {
        for ($i = 0; $i < sizeof($cotacao_id); $i++) {
            if (is_numeric($cotacao_id[$i])) {
                $query = "set dateformat dmy;
                update sf_requisicao_cotacao set " .
                "id_fornecedor = " . valoresSelect("txtFornecedorId_" . $item_id[$i]) . "," . 
                "ordem = " . valoresNumericos2($item_id[$i]) . "," . 
                "id_tipo_documento = " . valoresSelect("txt_" . $item_id[$i] . "_tipo") . "," . 
                "frete = " . valoresNumericos("txt_" . $item_id[$i] . "_frete") . "," . 
                "dias_frete = " . valoresNumericos("txt_" . $item_id[$i] . "_prazo") .
                " where id_cota = " . $cotacao_id[$i];
                odbc_exec($con, $query);  
                $id_requisicao_cotacao = $cotacao_id[$i];
            } else {
                $query = "set dateformat dmy;
                insert into sf_requisicao_cotacao (id_requisicao, id_fornecedor, ordem, id_tipo_documento, frete, dias_frete) values (" . 
                valoresSelect("txtId") . "," . 
                valoresSelect("txtFornecedorId_" . $item_id[$i]) . "," . 
                valoresNumericos2($item_id[$i]) . "," . 
                valoresSelect("txt_" . $item_id[$i] . "_tipo") . "," . 
                valoresNumericos("txt_" . $item_id[$i] . "_frete") . "," . 
                valoresNumericos("txt_" . $item_id[$i] . "_prazo") . ")";
                odbc_exec($con, $query);
                $res = odbc_exec($con, "select top 1 id_cota from sf_requisicao_cotacao order by id_cota desc") or die(odbc_errormsg());
                $id_requisicao_cotacao = odbc_result($res, 1);
            }
            $itens = [];
            odbc_exec($con, "delete from sf_requisicao_cotacao_itens where id_cota = " . $id_requisicao_cotacao);
            $cur = odbc_exec($con, "select id_item from sf_requisicao_itens where id_requisicao = " . $_POST['txtId']);
            while ($RFP = odbc_fetch_array($cur)) {
                $itens[] = $RFP['id_item'];
            }
            for ($j = 0; $j < count($itens); $j++) {
                if (isset($_POST["txt_" . $item_id[$i] . "_" . $itens[$j]]) && valoresNumericos2($_POST["txt_" . $item_id[$i] . "_" . $itens[$j]]) > 0) {
                    $query = "insert into sf_requisicao_cotacao_itens (id_cota, id_item, valor, ativo) values (" . 
                    $id_requisicao_cotacao . "," . $itens[$j] . "," . valoresNumericos2($_POST["txt_" . $item_id[$i] . "_" . $itens[$j]]) . ", 0)";
                    odbc_exec($con, $query);
                }
            }
        }
    }
}

if (isset($_POST['bntAprov']) && is_numeric($_POST['txtId'])) {
    odbc_exec($con, "update sf_requisicao set status_cotacao = 'Aprovado', data_aprov_cotacao = getDate() where id_requisicao = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
}

if (isset($_POST['bntReprov']) && is_numeric($_POST['txtId'])) {
    odbc_exec($con, "update sf_requisicao set status_cotacao = 'Reprovado', data_aprov_cotacao = getDate() where id_requisicao = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
}

if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_requisicao set inativo = 1, status_cotacao = 'Excluido' WHERE id_requisicao = " . $_POST['txtId']) or die(odbc_errormsg());
        echo "<script>alert('Registro excluido com sucesso');window.top.location.href = 'Cotacoes.php';</script>";
    }
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select *, isnull(status_cotacao, 'Novo') st
    from sf_requisicao where id_requisicao = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_requisicao'];
        $empresa = utf8_encode($RFP['empresa']);
        $vendedor = utf8_encode($RFP['responsavel']);
        $departamento = utf8_encode($RFP['id_departamento']);
        $historico_venda = utf8_encode($RFP['historico_venda']);
        $data_venda = escreverData($RFP['data_venda']);
        $comentarios_venda = utf8_encode($RFP['comentarios_venda']);
        $destinatario = utf8_encode($RFP['destinatario_cotacao']);
        $status = utf8_encode($RFP['st']); 
    }
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Requisição</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <style>
            .trx td {
                padding-top: 0px !important;
                padding-bottom: 0px !important;
            }
            .trx tbody tr {
                height: 27px;
            }
            .trx tfoot th {
                text-align: center;
            }
            .trx input {
                text-align: right;
            }
            .menor {
                background-color: greenyellow !important;
            }
            .maior {
                background-color: yellow !important;
            }
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header" style="display: inherit;">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1 style="margin-top:8px;float:left;">Cotação</h1>
                        <h1 style="margin-top:12px;float:right;font-size: 20px;">RQ: <?php echo str_pad($id, 5, '0', STR_PAD_LEFT); ?></h1>                        
                    </div>
                    <div class="row-fluid">
                        <form action="FormCotacoes.php" method="POST" name="frmEnviaDados" id="frmEnviaDados">                            
                            <div class="block">                                
                                <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>                                                                                                                  
                                <div style="width: 10%;float: left">
                                    <span>Loja:</span>
                                    <select name="txtEmpresa" id="txtEmpresa" style="width:100%;" class="select" disabled>
                                        <?php $cur = odbc_exec($con, "select * from sf_filiais inner join sf_usuarios_filiais on id_filial_f = id_filial inner join sf_usuarios on id_usuario_f = id_usuario where ativo = 0 and id_usuario_f = '" . $_SESSION["id_usuario"] . "'");
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo utf8_encode($RFP['id_filial']); ?>" <?php
                                            if ($empresa == $RFP['id_filial']) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP['numero_filial']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>    
                                <div style="width: 20%;float: left; margin-left: 1%;">
                                    <span>Responsável pela Requisição:</span>
                                    <select name="txtVendedor" id="txtVendedor" class="select" style="width:100%" disabled>
                                        <option value="null" >Selecione</option>
                                        <?php $sql = "select UPPER(login_user) login_user,nome from sf_usuarios where inativo = 0 order by nome";
                                        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP["login_user"] ?>"<?php
                                            if (!(strcmp($RFP["login_user"], $vendedor))) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP["nome"]) ?></option>
                                        <?php } ?>
                                    </select>                            
                                </div>
                                <div style="width: 26%;float: left; margin-left: 1%;">
                                    <span>Departamento:</span>
                                    <select name="txtDepartamento" id="txtDepartamento" class="select" style="width:100%" disabled>
                                        <option value="null">Selecione</option>
                                        <?php $sql = "select id_departamento,nome_departamento from sf_departamentos";
                                        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP["id_departamento"] ?>"<?php
                                            if (!(strcmp($RFP["id_departamento"], $departamento))) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP["nome_departamento"]) ?></option>
                                        <?php } ?>
                                    </select>                            
                                </div>
                                <div style="width: 30%;float: left; margin-left: 1%;">
                                    <span>Histórico:</span>
                                    <input name="txtHistorico" id="txtHistorico" type="text" class="input-xlarge" value="<?php echo $historico_venda; ?>" disabled/>
                                </div>  
                                <div style="width: 10%;float: left; margin-left: 1%;">
                                    <span>Data:</span>
                                    <input type="text" name="txtDtVenda" id="txtDtVenda" class="datepicker inputCenter" value="<?php echo $data_venda; ?>" disabled>
                                </div>  
                            </div>
                            <div class="block" style="margin-bottom: 0px;">
                                <div class="head dblue">
                                    <div class="icon"><span class="ico-money"></span></div>
                                    <h2>Fornecedores:</h2>
                                    <?php if ($disabled == "") { ?>
                                    <ul class="buttons">
                                        <li><div>
                                            <a href="javascript:void(0)" onclick="newFornecedor();" title="Adicionar Fornecedor">
                                            <div class="icon"><span class="ico-plus"></span></div>
                                            </a></div>
                                        </li>
                                    </ul>
                                    <?php } ?>
                                </div>
                            </div>                                                     
                            <div style="float: left; width: 25%;">
                                <div class="block" style="margin-bottom: 0px;">                
                                    <div class="data-fluid">
                                        <div style="width: 69%;float: left; margin-left: 1%;">
                                            <span>Menor Valor:</span>
                                            <input id="txtNameMinVal" type="text" style="color: white; background-color: #099999;" class="input-xlarge" value="" disabled/>
                                            <select id="txtFormaPagamento" style="display: none;">
                                                <option value="null">Selecione</option>
                                                <?php $sql = "select id_tipo_documento, descricao from sf_tipo_documento where inativo = 0";
                                                $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                                    <option value="<?php echo $RFP["id_tipo_documento"]; ?>"><?php echo utf8_encode($RFP["descricao"]); ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div style="width: 29%;float: left; margin-left: 1%;">
                                            <span>Valor:</span>
                                            <input id="txtValMinVal" type="text" style="color: white; background-color: #099999;" class="input-xlarge" value="R$ 0,00" disabled/>
                                        </div>
                                        <div style="width: 69%;float: left; margin-left: 1%; margin-bottom: 5px;">
                                            <span>Menor Prazo:</span>
                                            <input id="txtNameMinPrz" type="text" style="color: white; background-color: #099999;" class="input-xlarge" value="" disabled/>
                                        </div>                                        
                                        <div style="width: 29%;float: left; margin-left: 1%; margin-bottom: 5px;">
                                            <span>Dias:</span>
                                            <input id="txtValMinPrz" type="text" style="color: white; background-color: #099999;" class="input-xlarge" value="0" disabled/>
                                        </div>                                        
                                        <table id="tbProdutos" cellpadding="0" cellspacing="0" width="100%" class="table trx">
                                            <thead>
                                                <tr>
                                                    <th style="width: calc(100% - 50px);">Descrição do Produto</th>
                                                    <th style="width: 25px;">Und.</th>
                                                    <th style="width: 25px;">Qtd.</th>
                                                </tr>                                            
                                            </thead>
                                            <tbody>                                                                                        
                                            <?php 
                                                $i = 0;
                                                if (is_numeric($id)) {
                                                    $cur = odbc_exec($con, "select id_item, produto, quantidade, p.descricao, c.descricao grupo, unidade_comercial 
                                                    from sf_requisicao_itens i inner join sf_produtos p on i.produto = p.conta_produto
                                                    left join sf_contas_movimento c on c.id_contas_movimento = p.conta_movimento
                                                    where i.id_requisicao = " . $id . " and p.tipo = 'P' order by id_item") or die(odbc_errormsg());
                                                    while ($RFP = odbc_fetch_array($cur)) { $i++; ?>
                                                        <tr>
                                                            <td>
                                                                <input id="_<?php echo utf8_encode($RFP['id_item']); ?>" value="<?php echo escreverNumero($RFP['quantidade']); ?>" type="hidden"/>
                                                                <?php echo utf8_encode($RFP['descricao']); ?>
                                                            </td>
                                                            <td style="text-align:center"><?php echo utf8_encode($RFP['unidade_comercial']); ?></td>                                                        
                                                            <td style="text-align:center"><?php echo escreverNumero($RFP['quantidade']); ?></td>
                                                        </tr>
                                                    <?php }
                                                }
                                            ?>                                            
                                            </tbody>
                                        </table>
                                        <table id="tbServicos" cellpadding="0" cellspacing="0" width="100%" class="table trx">
                                            <thead>
                                                <tr>
                                                    <th style="width: calc(100% - 25px);">Descrição do Serviço</th>
                                                    <th style="width: 25px;">Qtd.</th>
                                                </tr>                                            
                                            </thead>
                                            <tbody>
                                            <?php 
                                                $i = 0;
                                                if (is_numeric($id)) {
                                                    $cur = odbc_exec($con, "select id_item, produto, quantidade, p.descricao, c.descricao grupo, unidade_comercial 
                                                    from sf_requisicao_itens i inner join sf_produtos p on i.produto = p.conta_produto
                                                    left join sf_contas_movimento c on c.id_contas_movimento = p.conta_movimento
                                                    where i.id_requisicao = " . $id . " and p.tipo = 'S' order by id_item") or die(odbc_errormsg());
                                                    while ($RFP = odbc_fetch_array($cur)) { $i++; ?>
                                                        <tr>
                                                            <td>
                                                                <input id="_<?php echo utf8_encode($RFP['id_item']); ?>" value="<?php echo escreverNumero($RFP['quantidade']); ?>" type="hidden"/>
                                                                <?php echo utf8_encode($RFP['descricao']); ?>
                                                            </td>
                                                            <td style="text-align:center"><?php echo escreverNumero($RFP['quantidade']); ?></td>
                                                        </tr>
                                                    <?php }
                                                }
                                            ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="3">VALOR COTAÇÃO</th>
                                                </tr>
                                                <tr>
                                                    <th colspan="3">FORMA DE PAGAMENTO</th>
                                                </tr>                                                
                                                <tr>
                                                    <th colspan="3">FRETE</th>
                                                </tr>                                                
                                                <tr>
                                                    <th colspan="3">PRAZO DE ENTREGA (DIAS)</th>
                                                </tr>
                                                <tr>
                                                    <th colspan="3">VALOR TOTAL BRUTO</th>
                                                </tr>
                                                <tr>
                                                    <th colspan="3">DISPONIBILIDADE DOS ITENS</th>
                                                </tr> 
                                            </tfoot>
                                        </table>
                                    </div>                
                                </div>                                                                            
                            </div>
                            <input id="txtIdMax" name="txtIdMax" value="0" type="hidden"/>
                            <div id="tbItens" style="display: flex; width: 75%; overflow-x: auto; overflow-y: hidden;"></div>
                            <div class="block">                                
                                <div style="width: 50%;float: left;">
                                    <div style="width: 60%;float: left">
                                        <span>Responsável pela Autorização da Cotação:</span>
                                        <select name="txtDestinatario" id="txtDestinatario" class="select" style="width:100%" <?php echo $disabled; ?>>
                                            <option value="null" >Selecione</option>
                                            <?php $cur = odbc_exec($con, "select UPPER(login_user) login_user,nome from sf_usuarios 
                                            where inativo = 0 and login_user != 'Admin' and 
                                            (master = 0 or master in (select id_permissoes from sf_usuarios_permissoes where est_aorc = 1)) 
                                            order by nome") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP["login_user"] ?>"<?php
                                                if (!(strcmp($RFP["login_user"], $destinatario))) {
                                                    echo "SELECTED";
                                                }
                                                ?>><?php echo utf8_encode($RFP["nome"]) ?></option>
                                            <?php } ?>
                                        </select>                            
                                    </div>
                                    <div style="width: 39%;float: left; margin-left: 1%;">                                        
                                        <span>Status da Cotação:</span>
                                        <input name="txtStatusVenda" id="txtStatusVenda" value="<?php echo $status; ?>" disabled type="text" class="input-medium"/>
                                    </div>
                                </div>
                            </div>        
                            <div style="clear:both;height: 5px;"></div>
                            <div class="frmfoot">
                                <?php if (($disabled == 'disabled') && ($status == 'Aguarda') && (strtoupper($_SESSION['login_usuario']) == $destinatario)) { ?>
                                    <div style="float:left">
                                        <button class="btn btn-success" type="submit" name="bntAprov" title="Aprovar" id="bntAprovar" ><span class="icon-ok icon-white"></span> Aprovar</button>
                                        <button class="btn red" type="button" title="Reprovar" id="bntReprov" onclick="reprovar();"><span class="icon-remove icon-white"></span> Reprovar</button>
                                    </div>
                                <?php } ?>
                                <div class="frmbtn">
                                    <?php if ($disabled == '') { ?>
                                        <button class="btn green" type="submit" name="bntSave" id="bntOK" onclick="validarForm();" ><span class="ico-checkmark"></span> Gravar</button>
                                        <?php if ($_POST['txtId'] == '') { ?>
                                        <button class="btn yellow" type="button" onClick="window.top.location.href = 'Requisicoes.php';" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                                        <?php } else { ?>
                                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK" ><span class="ico-reply"></span> Cancelar</button>
                                            <?php
                                        }
                                    } else { ?>
                                        <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                                        <button class="btn red" type="submit" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                                    <?php } ?>
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="dialog" id="source" title="Source"></div>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/moment.min.js"></script>    
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>  
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script language="javascript">
        
        $("#txtDtVenda").mask(lang["dateMask"]); 
        $("#txtQtdP, #txtQtdS").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
        
        function validarForm() {    
            let toReturn = false;
            if ($("#txtDestinatario").val() === "null") {
                bootbox.alert("Selecione um Responsável pela Autorização da Cotação!");
                toReturn = true;
            }
            for (var j = 0; j < $("input[name='txtIdItem[]']").length; j++) {
                let id = $($("input[name='txtIdItem[]']")[j]).val();
                if ($("#txtFornecedorId_" + id).val() === "" && toReturn === false) {
                    bootbox.alert("Selecione um Fornecedores!");                    
                    toReturn = true;
                } else if ($("#txt_" + id + "_tipo").val() === "null" && toReturn === false) {
                    bootbox.alert("Selecione uma forma de pagamento!");
                    toReturn = true;                    
                }
            }       
            if (toReturn) {
                event.preventDefault();                    
            }
        }
        
        function colorItens() {
            for (var i = 0; i < $("#tbProdutos input").length; i++) {
                let id_min = '';
                let val_min = 0;
                let id_item = $($("#tbProdutos input")[i]).attr("id");
                for (var j = 0; j < $("input[name='txtIdItem[]']").length; j++) {
                    let id = $($("input[name='txtIdItem[]']")[j]).val();
                    let valor = textToNumber($("#txt_" + id + id_item).val());
                    $("#lbl_" + id + id_item).removeClass("menor maior");
                    if (valor > 0) {
                        $("#lbl_" + id + id_item).addClass("maior");
                    }                    
                    if (valor > 0 && (valor < val_min || val_min === 0)) {
                        id_min = "#lbl_" + id + id_item;
                        val_min = valor;
                    }
                }    
                $(id_min).removeClass("maior").addClass("menor");
            }
            for (var i = 0; i < $("#tbServicos input").length; i++) {
                let id_min = '';
                let val_min = 0;
                let id_item = $($("#tbServicos input")[i]).attr("id");
                for (var j = 0; j < $("input[name='txtIdItem[]']").length; j++) {
                    let id = $($("input[name='txtIdItem[]']")[j]).val();
                    let valor = textToNumber($("#txt_" + id + id_item).val());
                    $("#lbl_" + id + id_item).removeClass("menor maior");
                    if (valor > 0) {
                        $("#lbl_" + id + id_item).addClass("maior");
                    }                    
                    if (valor > 0 && (valor < val_min || val_min === 0)) {
                        id_min = "#lbl_" + id + id_item;
                        val_min = valor;
                    }
                }    
                $(id_min).removeClass("maior").addClass("menor");                
            }
            for (var i = 0; i < $(".menor").length; i++) {
                $("#" + $($(".menor")[i]).attr("id").replace('lbl','ckb')).attr("checked", true);
            }
            for (var i = 0; i < $(".maior").length; i++) {
                $("#" + $($(".maior")[i]).attr("id").replace('lbl','ckb')).attr("checked", false);
            }
            $("#txtNameMinVal").val("");
            $("#txtValMinVal").val("R$ 0,00");
            $("#txtNameMinPrz").val(""); 
            $("#txtValMinPrz").val("0");            
            for (var j = 0; j < $("input[name='txtIdItem[]']").length; j++) {
                let id = $($("input[name='txtIdItem[]']")[j]).val();
                let valor = textToNumber($("#lbl_" + id + "_total_frete").html());
                let val_min = textToNumber($("#txtValMinVal").val());
                if (valor > 0 && (valor < val_min || val_min === 0)) {
                    $("#txtNameMinVal").val($("#txtFornecedor_" + id).val());
                    $("#txtValMinVal").val(numberFormat(valor,1));
                }        
                let prazo = textToNumber($("#txt_" + id + "_prazo").val());
                let prazo_min = textToNumber($("#txtValMinPrz").val());
                if (prazo > 0 && (prazo < prazo_min || prazo_min === 0)) {
                    $("#txtNameMinPrz").val($("#txtFornecedor_" + id).val());
                    $("#txtValMinPrz").val(prazo);
                }
            }
        }
        
        function calcTotalItem(item) {
            let id = $(item).attr("id").split("_");
            let valor = textToNumber($(item).val());
            let qtd = textToNumber($("#_" + id[2]).val());
            $("#lbl_" + id[1] + "_" + id[2]).html(numberFormat((valor * qtd), 1));
            calcTotal(id[1]);
        }
        
        function calcTotal(id) {
            let total = 0;
            let total_itens = 0;
            for (var i = 0; i < $("#tbProdutos input").length; i++) {
                let id_item = $($("#tbProdutos input")[i]).attr("id");
                let qtd_item = textToNumber($($("#tbProdutos input")[i]).val());
                let valor = textToNumber($("#txt_" + id + id_item).val());
                total_itens += (valor > 0 ? 1 : 0);
                total += (valor * qtd_item);
            }
            for (var i = 0; i < $("#tbServicos input").length; i++) {
                let id_item = $($("#tbServicos input")[i]).attr("id");
                let qtd_item = textToNumber($($("#tbServicos input")[i]).val());
                let valor = textToNumber($("#txt_" + id + id_item).val());
                total_itens += (valor > 0 ? 1 : 0);                
                total += (valor * qtd_item);
            }
            $("#lbl_" + id + "_total").html(numberFormat(total, 1));
            let frete = textToNumber($("#txt_" + id + "_frete").val());
            $("#lbl_" + id + "_total_frete").html(numberFormat((total + frete), 1));
            let total_itens_geral = ($("#tbProdutos input").length + $("#tbServicos input").length);
            $("#lbl_" + id + "_porcentagem").html(numberFormat((total_itens * 100/total_itens_geral), 0) + "%");
            colorItens();
        }
        
        function removeFornecedor(item) {
            bootbox.confirm('Deseja realmente excluir esse item?', function (result) {
                if (result === true) {
                    $(item).closest(".item").remove();
                    colorItens();
                }
            });
        }
        
        function newFornecedor() {
            let id = parseInt($("#txtIdMax").val()) + 1;
            addFornecedor(id, '', null);
        }
        
        function addFornecedor(id, item, data) {
            let fornecedor = `<div class="item" style="flex-shrink: 0; width: 20%; margin-left: 0.5%;">
            <div class="block" style="margin-bottom: 0px;">            
                <div class="data-fluid">
                    <input name="txtIdCotacao[]" value="${item}" type="hidden"/>
                    <input name="txtIdItem[]" value="${id}" type="hidden"/>
                    <div style="width: 99%;float: left; margin-left: 1%;">
                        <span id="txtIdLabel_${id}"></span><span>Fornecedores:</span><a href="javascript:void(0)" style="float: right; color: red;" onclick="removeFornecedor(this);"><b>x</b></a>
                        <input id="txtFornecedor_${id}" type="text" class="input-xlarge" value="" <?php echo $disabled; ?>/>
                        <input id="txtFornecedorId_${id}" name="txtFornecedorId_${id}" value="" type="hidden"/>
                    </div>
                    <div style="width: 99%;float: left; margin-left: 1%; margin-bottom: 5px;">
                        <span>Telefone:</span>
                        <input id="txtFornecedorCel_${id}" type="text" class="input-xlarge" value="" disabled/>
                    </div>                                        
                    <table id="tbProdutos_${id}" cellpadding="0" cellspacing="0" width="100%" class="table trx">
                        <thead>
                            <tr>
                                <th style="width: 10%;"></th>
                                <th style="width: 45%;">Val.Und.</th>
                                <th style="width: 45%;">Val.Total</th>
                            </tr>                                            
                        </thead>
                        <tbody>`;
                        for (var i = 0; i < $("#tbProdutos input").length; i++) {
                            fornecedor += `<tr>
                                <td style="text-align:center">
                                    <input id="ckb_${id}` + $($("#tbProdutos input")[i]).attr("id") + `" name="ckb_${id}` + $($("#tbProdutos input")[i]).attr("id") + `" type="checkbox" value="` + $($("#tbProdutos input")[i]).attr("id") + `"/>
                                </td>
                                <td style="text-align:center"><input id="txt_${id}` + $($("#tbProdutos input")[i]).attr("id") + `" 
                                name="txt_${id}` + $($("#tbProdutos input")[i]).attr("id") + `" onchange="calcTotalItem(this)" type="text" value="0,00" <?php echo $disabled; ?>/></td>                                                        
                                <td style="text-align:center" id="lbl_${id}` + $($("#tbProdutos input")[i]).attr("id") + `">R$ 0,00</td>
                            </tr>`;
                        }
                        fornecedor += `</tbody>
                    </table>
                    <table id="tbServicos_${id}" cellpadding="0" cellspacing="0" width="100%" class="table trx">
                        <thead>
                            <tr>
                                <th style="width: 10%;"></th>
                                <th style="width: 45%;">Val.Und.</th>
                                <th style="width: 45%;">Val.Total</th>
                            </tr>                                            
                        </thead>
                        <tbody>`;
                        for (var i = 0; i < $("#tbServicos input").length; i++) {
                            fornecedor += `<tr>
                                <td style="text-align:center">
                                    <input id="ckb_${id}` + $($("#tbServicos input")[i]).attr("id") + `" name="ckb_${id}` + $($("#tbServicos input")[i]).attr("id") + `" class="checkbox" type="checkbox" value="` + $($("#tbServicos input")[i]).attr("id") + `"/>
                                </td>
                                <td style="text-align:center"><input id="txt_${id}` + $($("#tbServicos input")[i]).attr("id") + `" 
                                name="txt_${id}` + $($("#tbServicos input")[i]).attr("id") + `" onchange="calcTotalItem(this)" type="text" value="0,00" <?php echo $disabled; ?>/></td>                                                        
                                <td style="text-align:center" id="lbl_${id}` + $($("#tbServicos input")[i]).attr("id") + `">R$ 0,00</td>
                            </tr>`;
                        }
                        fornecedor += `</tbody>
                        <tfoot>
                            <tr><th colspan="3" id="lbl_${id}_total">R$ 0,00</th></tr>
                            <tr>
                                <th colspan="3">
                                    <select id="txt_${id}_tipo" name="txt_${id}_tipo" <?php echo $disabled; ?>>` + $("#txtFormaPagamento").html() + `</select>
                                </th>
                            </tr>                                                
                            <tr><th colspan="3"><input id="txt_${id}_frete" name="txt_${id}_frete" onchange="calcTotal(${id})" type="text" value="0,00" <?php echo $disabled; ?>/></th></tr>                                               
                            <tr><th colspan="3"><input id="txt_${id}_prazo" name="txt_${id}_prazo" onchange="colorItens()" type="text" value="0" <?php echo $disabled; ?>/></th></tr>
                            <tr><th colspan="3" id="lbl_${id}_total_frete">R$ 0,00</th></tr>
                            <tr><th colspan="3" id="lbl_${id}_porcentagem">0,00%</th></tr> 
                            <tr><th colspan="3" id="lbl_${id}_compras" style="padding: 0px;"></th></tr>
                        </tfoot>
                    </table>
                </div>                
            </div>                                
        </div>`;
        $("#tbItens").append(fornecedor);
        $("#tbProdutos_" + id + " input[type=text], #tbServicos_" + id + " input[type=text]:not(#txt_" + id + "_prazo)").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
        $("#txt_" + id + "_prazo").mask("99", {placeholder: ""});
        $("#txtIdMax").val(id);        
        $("#txtFornecedor_" + id).autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "../../Modulos/CRM/ajax.php",
                    dataType: "json",
                    data: {q: request.term, r: "S", t: "PFE"},
                    success: function (data) {
                        response(data);
                    }
                });
            }, minLength: 3,
            select: function (event, ui) {
                $("#txtFornecedorId_" + id).val(ui.item.id);
                $("#txtFornecedor_" + id).val(ui.item.value);
                $("#txtFornecedorCel_" + id).val(ui.item.telefone);
            }
        });
        populateCotacao(id, data);
        }
        
        $.getJSON("../Estoque/ajax/FormCotacoes.php", {id: $("#txtId").val(), listCotacoes: "S"}, function (data) {
            if(data !== null && data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    addFornecedor(data[i].ordem, data[i].id_cota, data[i]);
                }
            } else {
                newFornecedor();
                newFornecedor();
            }
        });
        
        function populateCotacao(id, data) {
            if (data !== null) {
                $("#txtFornecedorId_" + id).val(data.id_fornecedor);
                $("#txtFornecedor_" + id).val(data.razao_social);
                $("#txtFornecedorCel_" + id).val(data.telefone_contato);            
                $("#txt_" + id + "_tipo").val(data.id_tipo_documento);
                $("#txt_" + id + "_frete").val(data.frete);
                $("#txt_" + id + "_prazo").val(data.dias_frete);  
                $("#txtIdLabel_" + id).html("Ct: <b>" + ("00000" + data.id_cota).slice(-5) + "</b> - ");
                if (data.id_venda.length > 0) {
                    $("#lbl_" + id + "_compras").html(`<b>COMPRA ID: ${data.id_venda}</b>`);
                } else if ($('#txtDestinatario').is(':disabled') && $("#txtStatusVenda").val() === 'Aprovado') {
                    $("#lbl_" + id + "_compras").html(`<button onclick="saveCompras(${id},${data.id_cota});" class="btn green" style="width: 100%;" type="button"><span class="ico-checkmark"></span> Gerar Pedido de Compra</button>`);
                }
                for (var i = 0; i < data.itens.length; i++) {
                    $("#txt_" + data.ordem +  "_" + data.itens[i].id_item).val(data.itens[i].valor); 
                    let valor = textToNumber($("#txt_" + data.ordem +  "_" + data.itens[i].id_item).val());
                    let qtd = textToNumber($("#_" + data.itens[i].id_item).val());
                    $("#lbl_" + data.ordem + "_" + data.itens[i].id_item).html(numberFormat((valor * qtd), 1));
                }
                calcTotal(id);
            }
        }
        
        function saveCompras(id, data) {        
            bootbox.confirm('Confirma a geração da Compra com os itens selecionados?', function (value) {
                if (value === true) {
                    $("#loader").show();                    
                    let itens = "0";
                    for (var i = 0; i < $("input[name^='ckb_" + id + "']").length; i++) {
                        if ($("input[name^='ckb_" + id + "']")[i].checked) {
                            itens += "," + $($("input[name^='ckb_" + id + "']")[i]).val().replace('_','');
                        }
                    }                      
                    $.post("../Estoque/ajax/FormCotacoes.php", "salvaCompras=S&cotacao=" + data + "&itens=" + itens).done(function (data) {
                        if (!isNaN(data.trim())) {
                            $("#lbl_" + id + "_compras").html('<b>COMPRA ID: ' + data.trim() + '</b>');
                            console.log(data.trim());                            
                        } else {
                            bootbox.alert("Erro ao gerar a Compra!<br/>" + data.trim());
                        }
                        $("#loader").hide();                                                
                    });
                } else {
                    return;
                }
            });        
        }
        
    </script>
</body>
<?php odbc_close($con); ?>
</html>