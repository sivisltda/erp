$(document).ready(function () {
    $("#txtTipoAcesso").trigger("change");
    $("#txtComissaoPlan").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], suffix: "%"});
    $("#txtCustoPlan, #txtPreco_venda, #txtDCC, #txtBOL, #txtPIX, #segMin, #segMax, #txtValorLimite, #txtCotaDefaut").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"], suffix: ""});
    $("#listParcPlano input[type=text]:not([readonly]").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"], suffix: ""});
    $("#txtValorAcr").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"], suffix: "", centsLimit: 4});
    CKEDITOR.replace("ck_desc", {height: "200px"});
    CKEDITOR.replace("ck_msg", {height: "200px"});    
});

function validar() {
    if ($('#txtTipoAcesso').val() === "C") {
        if (!$.isNumeric($("#txtQtdDiasAcesso").val()) || $("#txtQtdDiasAcesso").val() === "0") {
            bootbox.alert("Preencha o campo 'Dias Permitido' corretamente!");
            return false;
        }
        if ($("#txtParcelar").attr("checked")) {
            bootbox.alert("Não é possível Gerar Parcela Automática para um plano em Dias Corridos!");
            return false;
        }
        if (!$("#list_1").length) {
            bootbox.alert("É necessário gerar um valor referente ao número Maximo '1' para um plano em Dias Corridos!!");
            return false;
        }
        if ($("#listParcPlano tr").length !== 1) {
            bootbox.alert("Não é possível gerar lançamentos referentes a 'planos Contratuais'/'DCC' para um plano em Dias Corridos!");
            return false;
        }
    }
    return true;
}

function AtualizarMensalidades() {
    bootbox.confirm('Confirma a atualização das mensalidades em aberto?', function (result) {
        if (result === true) {
            $.get("../Estoque/form/PlanosFormServer.php?isAjax=S&refreshMensalidades=" + $("#txtId").val()).done(
            function (data) {
                if (data.trim() === "YES") {
                    bootbox.alert("Mensalidades em aberto atualizadas com Sucesso!");
                } else {
                    bootbox.alert("Erro ao atualizar mensalidades!");
                }
            });
        }
    });
}

$('#txtTipoAcesso').change(function () {
    $("#divDiasEspecificos").hide();
    if ($('#txtTipoAcesso').val() === "E") {
        $("#divDiasEspecificos").show();
        $("#txtQtdIntervaloAcesso").parent().show();
    }
    if ($('#txtTipoAcesso').val() === "C") {
        $("#divDiasEspecificos").show();
        $("#txtQtdIntervaloAcesso").parent().hide();
    }
});

$('[id="txtLimitar_Acesso"]').click(function (e) {
    if ($("#txtLimitar_Acesso:checked").val() === "0") {
        $("#txtQtdLimiteAcesso").val("0");
    }
});

jQuery.validator.addMethod("notEqual", function (value, element, param) {
    return this.optional(element) || value !== param;
}, "Please specify a different (non-default) value");
$("#frmEnviaDados").validate({
    errorPlacement: function (error, element) {
        return true;
    },
    rules: {
        txtDescricao: {required: true},
        txtPreco_venda: {notEqual: numberFormat(0)}
    }
});

function addDCC() {
    $("#list_0").remove();
    $("#listParcPlano").prepend(addlinha($("#txtDCC").val(), 0));
}

function addBOL() {
    $("#list_-1").remove();
    $("#listParcPlano").prepend(addlinha($("#txtBOL").val(), -1));
}

function addPIX() {
    $("#list_-2").remove();
    $("#listParcPlano").prepend(addlinha($("#txtPIX").val(), -2));
}

function novaLinha() {
    if ($("#txtDescricao").val() !== "" && $("#txtPreco_venda").val() !== "" && $("#txtMaximo").val() > 0 && $("#txtMaximo").val() < 61) {
        var conteudo = "";
        var numlinhas = $("#txtMaximo").val();
        if (parseInt($("#txtMaximo").val()) < parseInt($('#listParcPlano tr:last-child >td:first').text())) {
            numlinhas = parseInt($('#listParcPlano tr:last-child >td:first').text());
        }
        for (var conta = 1; conta <= numlinhas; conta++) {
            if ($("#list_" + conta).length > 0) {
                conteudo = conteudo + "<tr id=\"list_" + conta + "\">" + $("#list_" + conta).html() + "</tr>";
            } else {
                if ($('#listParcPlano').find('tr').length > 0 && conta === parseInt($("#txtMaximo").val())) {
                    conteudo = conteudo + addlinha($("#txtPreco_venda").val(), conta);
                } else if ($('#listParcPlano').find('tr').length === 0) {
                    conteudo = conteudo + addlinha($("#txtPreco_venda").val(), conta);
                }
            }
        }
        $('#listParcPlano').find('tr:not([id=list_-1]):not([id=list_0])').remove();
        $("#listParcPlano").append(conteudo);
    }
}

function addlinha(preco, conta) {
    var linha = "";
    linha = linha + "<tr id=\"list_" + conta + "\">";
    linha = linha + "<td style=\"line-height:23px; text-align:center; font-weight:bold;width:26px;\"><input name=\"plan_id[]\" value=\"\" type=\"hidden\"/><input name=\"plan_num[]\" value=\"" + conta + "\" type=\"hidden\"/>" + (conta === -2 ? 'PIX' : (conta === -1 ? 'BOL' : (conta === 0 ? 'DCC' : conta))) + "</td>";
    linha = linha + "<td style=\"line-height:23px; width:120px\"><input type=\"text\" id=\"val_" + conta + "\" name=\"plan_val[]\" value=\"" + numberFormat(textToNumber(preco) * (conta !== -2 && conta !== -1 && conta !== 0 ? conta : 1)) + "\" onchange=\"updateLinha(" + conta + ",'V')\"/></td>";
    linha = linha + "<td style=\"line-height:23px; width:102px\"><input type=\"text\" id=\"des_" + conta + "\" name=\"plan_des[]\" value=\"" + numberFormat(0) + "\" onchange=\"updateLinha(" + conta + ",'P')\"/></td>";
    linha = linha + "<td style=\"line-height:23px; width:102px\"><input type=\"text\" id=\"desD_" + conta + "\" name=\"plan_desD[]\" value=\"" + numberFormat(0) + "\" onchange=\"updateLinha(" + conta + ",'D')\"/></td>";
    linha = linha + "<td style=\"line-height:23px; width:120px\"><input type=\"text\" id=\"fin_" + conta + "\" value=\"" + numberFormat(textToNumber(preco)) + "\" readonly /></td>";
    linha = linha + "<td style=\"line-height:23px; width:140px\"><input type=\"text\" id=\"pag_" + conta + "\" value=\"" + (conta !== -2 && conta !== -1 && conta !== 0 ? conta : 1) + " x " + numberFormat(textToNumber(preco)) + "\" readonly /></td>";
    linha = linha + "<td style=\"width:41px; text-align:center\"><a onclick=\"removeLinha(" + conta + ")\" href=\"javascript:void(0)\"><img src=\"../../img/1365123843_onebit_33 copy.PNG\" width=\"16\" height=\"16\"></a></td>";
    linha = linha + "</tr>";
    return linha;
}

function removeLinha(id, idparc) {
    if (idparc === undefined) {
        $("#list_" + id).remove();
    } else {
        bootbox.confirm('Deseja realmente excluir essa parcela?', function (result) {
            if (result === true) {
                $.post("ajax/ajax_planos.php", {id_parc: idparc}).done(function (data) {
                    if (data.trim() !== "YES") {
                        bootbox.alert("Erro ao excluir registro, Mensalidade do plano vinculado a algum Cliente!");
                    } else {
                        bootbox.alert("Excluido com sucesso!");
                        $("#list_" + id).remove();
                    }
                });
            }
        });
    }
}

function updateLinha(id, tipo) {
    var plan_val = textToNumber($("#val_" + id).val());
    if (tipo === 'D') {
        var descP = textToNumber($("#desD_" + id).val()) * 100 / plan_val;
        $("#des_" + id).val(numberFormat(descP));
    } else if (tipo === 'P') {
        var descD = textToNumber($("#des_" + id).val()) * plan_val / 100;
        $("#desD_" + id).val(numberFormat(descD));
    }
    var plan_des = textToNumber($("#des_" + id).val());
    var calc_des = plan_val - (plan_val * (plan_des / 100));
    $("#fin_" + id).val(numberFormat(calc_des));
    $("#pag_" + id).val((id === -1 || id === 0 ? 1 : id) + " x " + numberFormat(calc_des / (id === -1 || id === 0 ? 1 : id)));
}