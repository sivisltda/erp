<?php

if (!isset($_GET['pdf'])) {
    include '../../Connections/configini.php';
}

$aColumns = array('id_venda', 'data_venda', 'numero_filial', 'razao_social', 'vendedor', 'historico_venda', 'pa', 'tp', 'ts', 'tc', 'tt', 'status');
$iTotal = 0;
$iFilteredTotal = 0;
$totparc = 0;
$sLimit = 0;
$query = "";
$sWhere = "";
$campos = "";
$campos2 = "";
$sOrder = " ORDER BY data_venda desc ";

$imprimir = 0;
$F1 = "";
$F2 = "";
$F3 = "";
$F4 = "";
$DateBegin = "";
$DateEnd = "";

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = $_GET['imp'];
}

if (is_numeric($_GET['id'])) {
    $F1 = $_GET['id'];
}

if (is_numeric($_GET['tp'])) {
    $F2 = $_GET['tp'];
}

if ($_GET['td'] != '') {
    $F3 = $_GET['td'];
}

if ($_GET['fd'] != '') {
    $F4 = $_GET['fd'];
}

if ($_GET['filial'] != '') {
    $idFilial = $_GET['filial'];
}

$comando = "";
if ($F1 == "1") {
    $comando .= " AND status = 'Aguarda' ";
} elseif ($F1 == "2") {
    $comando .= " AND status = 'Aprovado' ";
} elseif ($F1 == "3") {
    $comando .= " AND status = 'Reprovado' ";
}

if($F2 == "6"){
  $comando .= " AND f.tipo = 'E'";
}

if ($F3 != "") {
    if ($F2 == "0") {
        $comando .= " AND cliente_venda = " . $F3;
    } elseif ($F2 == "1") {
        $comando .= " AND grupo_conta = " . $F3;
    } elseif ($F2 == "2") {
        $comando .= " AND conta_movimento = " . $F3;
    } elseif ($F2 == "3") {
        if($F4 == "0") {
            $comando .= " AND id_venda in (select venda from sf_venda_parcelas where inativo = 0 and tipo_documento = " . $F3 . ") ";    
        } else {
            $comando .= " AND tipo_documento = " . $F3;
        }
    } elseif ($F2 == "4") {
        $comando .= " AND destinatario = '" . $F3 . "'";
    } elseif ($F2 == "5") {
        $comando .= " AND UPPER(vendedor) = UPPER('" . $F3 . "')";
    }elseif ($F2 == "7") {
        $comando .= " AND f.grupo_pessoa = ".$F3;
    }
}

if ($_GET['dti'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
    $comando .= " AND " . ($F4 == "0" ? "data_parcela" : "data_venda") . " >= " . valoresDataHora2($DateBegin, "00:00:00");
}

if ($_GET['dtf'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
    $comando .= " AND " . ($F4 == "0" ? "data_parcela" : "data_venda") . " <= " . valoresDataHora2($DateEnd, "23:59:59");
}

if ($_GET['com'] != '') {
    $comando .= " AND cod_pedido = " . valoresTexto2($_GET['com']) . "";
}

if ($_GET['usr'] != '' && $_GET['usr'] != 'null') {
    $comando .= " AND UPPER(vendedor) = UPPER(" . valoresTexto2($_GET['usr']) . ")";
}

if (isset($_GET['tpImp']) && $_GET['tpImp'] == 'E') {
    $campos = ",f.cnpj,f.cep,f.endereco,f.numero,complemento,f.bairro,tb_cidades.cidade_nome,tb_estados.estado_sigla,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = id_fornecedores_despesas) telefone_contato,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = id_fornecedores_despesas) email_contato ";
    $campos2 = ",STUFF((SELECT distinct '|' + abreviacao FROM sf_venda_parcelas vp inner join sf_tipo_documento td on vp.tipo_documento = td.id_tipo_documento WHERE vp.venda = a.id_venda FOR XML PATH('')), 1, 1, '') as abreviacao,    
    STUFF((SELECT distinct ', ' + descricao FROM sf_vendas_itens vi inner join sf_produtos p on p.conta_produto = vi.produto WHERE vi.id_venda = a.id_venda FOR XML PATH('')), 1, 1, '') as itens ";
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . "
                                    " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY data_venda desc";
    }
}

if ($_GET['sSearch'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        if ($i >= 2 & $i <= 7) {
            $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['sSearch']) . "%' OR ";
        }
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

if (is_numeric($idFilial)) {
    $comando .= " AND sf_vendas.empresa =" . $idFilial;
}

if ($F4 == "0") {  
    $query = "select id_venda,data_parcela data_venda,razao_social,nome_fantasia,UPPER(vendedor) vendedor,historico_venda,status,
    ISNULL((select SUM(valor_total) from sf_vendas_itens v inner join sf_produtos c on v.produto = c.conta_produto where tipo = 'P' and id_venda = sf_vendas.id_venda),0) as tp, 
    ISNULL((select SUM(valor_total) from sf_vendas_itens v inner join sf_produtos c on v.produto = c.conta_produto where tipo = 'S' and id_venda = sf_vendas.id_venda),0) as ts, 
    ISNULL((select SUM(valor_total) from sf_vendas_itens v inner join sf_produtos c on v.produto = c.conta_produto where tipo = 'C' and id_venda = sf_vendas.id_venda),0) as tc, " .
    (isset($_GET['tpImp']) && $_GET['tpImp'] == 'E' ? "ISNULL((select SUM(dbo.VALOR_REAL_MENSALIDADE(m.id_mens)) from sf_vendas_itens vi inner join sf_vendas_planos_mensalidade m on vi.id_item_venda = m.id_item_venda_mens where vi.id_venda = sf_vendas.id_venda),0) as tm, " : "0 tm, ") .    
    "pa,destinatario ,data_aprov,numero_filial, cod_pedido, nfe_iss,(select isnull(max(cod_nfe),0) from sf_nfe_notas where id_venda_nfe = id_venda and demis = data_parcela) nfs, valor_parcela $campos
    from sf_vendas left join sf_filiais on sf_filiais.id_filial = sf_vendas.empresa 
    inner join sf_fornecedores_despesas f on sf_vendas.cliente_venda = f.id_fornecedores_despesas
    left join tb_cidades on tb_cidades.cidade_codigo = cidade 
    left join tb_estados on tb_estados.estado_codigo = f.estado    
    inner join sf_venda_parcelas vp on vp.venda = sf_vendas.id_venda
    where cov = 'V' AND ent_sai = 0 " . $comando . ") as x WHERE id_venda is not null " . $sWhere;
} else {    
    $query = "select id_venda,data_venda,razao_social,nome_fantasia,UPPER(vendedor) vendedor,historico_venda,status,
    ISNULL((select SUM(valor_total) from sf_vendas_itens v inner join sf_produtos c on v.produto = c.conta_produto where tipo = 'P' and id_venda = sf_vendas.id_venda),0) as tp, 
    ISNULL((select SUM(valor_total) from sf_vendas_itens v inner join sf_produtos c on v.produto = c.conta_produto where tipo = 'S' and id_venda = sf_vendas.id_venda),0) as ts, 
    ISNULL((select SUM(valor_total) from sf_vendas_itens v inner join sf_produtos c on v.produto = c.conta_produto where tipo = 'C' and id_venda = sf_vendas.id_venda),0) as tc, " . 
    (isset($_GET['tpImp']) && $_GET['tpImp'] == 'E' ? "ISNULL((select SUM(dbo.VALOR_REAL_MENSALIDADE(m.id_mens)) from sf_vendas_itens vi inner join sf_vendas_planos_mensalidade m on vi.id_item_venda = m.id_item_venda_mens where vi.id_venda = sf_vendas.id_venda),0) as tm, " : "0 tm, ") .
    "ISNULL((select COUNT(id_parcela) from sf_venda_parcelas where venda = sf_vendas.id_venda and inativo = 0),0) as pa,
    destinatario,data_aprov,numero_filial, cod_pedido, nfe_iss,(select isnull(max(cod_nfe),0) from sf_nfe_notas where id_venda_nfe = id_venda) nfs $campos    
    from sf_vendas left join sf_filiais on sf_filiais.id_filial = sf_vendas.empresa 
    inner join sf_fornecedores_despesas f on sf_vendas.cliente_venda = f.id_fornecedores_despesas
    left join tb_cidades on tb_cidades.cidade_codigo = cidade 
    left join tb_estados on tb_estados.estado_codigo = f.estado    
    where cov = 'V' AND ent_sai = 0 " . $comando . ") as x WHERE id_venda is not null " . $sWhere;
}
$sQuery = "set dateformat dmy; " . 
"select count(id_venda) id_venda, sum(tp) tp, sum(ts) ts, sum(tc) tc, sum(tm) tm from (" . $query;
$cur = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur)) {
    $iFilteredTotal = $RFP['id_venda'];
    $totparc = $RFP['tp'] + $RFP['ts'] + $RFP['tc'];
}
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iFilteredTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);
$sQuery1 = "set dateformat dmy; " . 
"select *" . $campos2 . " from (select *, ROW_NUMBER() OVER (" . $sOrder . ") as row from (" .
$query . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1=" . $imprimir;
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($RFP = odbc_fetch_array($cur)) {
    $row = array();
    if ($_GET['comanda']) {
        $row[] = utf8_encode($RFP['id_venda']);
        $row[] = utf8_encode($RFP['cod_pedido']);
        $row[] = escreverData($RFP['data_venda']);
        $row[] = utf8_encode($RFP['razao_social']);
        $row[] = utf8_encode($RFP['vendedor']);
        $row[] = escreverNumero(($RFP['tp'] + $RFP['ts'] + $RFP['tc']), 1);
    } else {
        $nome_fantasia = "";            
        if (!isset($_GET['com']) && !isset($_GET['cli'])) {
            $row[] = "<center><input type=\"checkbox\" class=\"caixa\" name=\"items[]\" value=\"" . ($F4 == "0" ? "R-" : "V-") . $RFP['id_venda'] . "\" " .
            (($F1 == "1" && utf8_encode($RFP['destinatario']) == $_SESSION["login_usuario"]) || ($F1 == "2" && ($RFP['ts'] + $RFP['tc']) > 0 && $RFP['nfs'] == 0) ? "" : "disabled") . "/></center>";
        }
        $row[] = "<div id='formPQ'><center><a " . (isset($_GET['com']) ? "target='_blank'" : "") . " href='../../" . (isset($_GET['com']) ? "../" : "") . "Modulos/Estoque/FormVendas.php?id=" . $RFP['id_venda'] . "' >" . escreverDataHora($RFP['data_venda']) . "</a></center></div>";            
        if (!isset($_GET['com']) && !isset($_GET['cli'])) {        
            $row[] = "<div id='formPQ'><center>" . utf8_encode($RFP['numero_filial']) . "</center></div>";
        }
        if (utf8_encode($RFP['nome_fantasia']) != '') {
            $nome_fantasia = " (" . utf8_encode($RFP['nome_fantasia']) . ")";
        }
        $row[] = "<div id=\"formPQ\">" . utf8_encode($RFP['razao_social']) . $nome_fantasia . "</div>";
        if (isset($_GET['tpImp']) && $_GET['tpImp'] == 'E') {
            $row[] = utf8_encode($RFP['cnpj']);    
            $row[] = utf8_encode($RFP['cep']);    
            $row[] = utf8_encode($RFP['endereco']);    
            $row[] = utf8_encode($RFP['numero']);    
            $row[] = utf8_encode($RFP['complemento']);    
            $row[] = utf8_encode($RFP['bairro']);    
            $row[] = utf8_encode($RFP['cidade_nome']);    
            $row[] = utf8_encode($RFP['estado_sigla']);    
            $row[] = utf8_encode($RFP['telefone_contato']);    
            $row[] = utf8_encode($RFP['email_contato']);    
            $row[] = utf8_encode($RFP['abreviacao']);    
            $row[] = utf8_encode($RFP['itens']);
            $row[] = ($RFP['nfe_iss'] == 1 ? "SIM" : "NÃO");
            $row[] = escreverNumero($RFP['tm'], 1);
        } else if (!isset($_GET['com']) && !isset($_GET['cli'])) {        
            $row[] = "<div id='formPQ'><center>" . utf8_encode($RFP['vendedor']) . "</center></div>";
            $row[] = "<div id='formPQ'>" . utf8_encode($RFP['historico_venda']) . "</div>";
            $row[] = "<div id='formPQ'><center>" . utf8_encode($RFP['pa']) . "</center></div>";
        } else {            
            $row[] = "<div id='formPQ'>" . utf8_encode($RFP['historico_venda']) . "</div>";            
        }
        $row[] = "<div id='formPQ' style=\"text-align:right\">" . escreverNumero($RFP['tp'], 1) . "</div>";
        $row[] = "<div id='formPQ' style=\"text-align:right\">" . escreverNumero($RFP['ts'], 1) . "</div>";
        if (!isset($_GET['com']) && !isset($_GET['cli'])) {        
            $row[] = "<div id='formPQ' style=\"text-align:right\">" . escreverNumero($RFP['tc'], 1) . "</div>";
        }
        if ($F4 == "0") {
            $row[] = "<div id='formPQ' style=\"text-align:right\">" . escreverNumero(($RFP['valor_parcela']), 1) . "</div>";                
        } else {
            $row[] = "<div id='formPQ' style=\"text-align:right\">" . escreverNumero(($RFP['tp'] + $RFP['ts'] + $RFP['tc']), 1) . "</div>";
        }
        $color = "style=\"color:";
        if (utf8_encode($RFP['status']) == 'Aguarda') {
            $color = $color . "#bc9f00\"";
        } elseif (utf8_encode($RFP['status']) == 'Aprovado') {
            $color = $color . "#0066cc\"";
        } elseif (utf8_encode($RFP['status']) == 'Reprovado') {
            $color = $color . "#f00\"";
        } else {
            $color = $color . "black\"";
        }
        if ($RFP['data_aprov'] != "") {
            $titleStatus = utf8_encode($RFP['status'] . " por " . $RFP['destinatario'] . ": " . escreverDataHora($RFP['data_aprov']));
        } else {
            $titleStatus = utf8_encode($RFP['status']);
        }
        $row[] = "<div id='formPQ' " . $color . " title=\"" . $titleStatus . "\"><center>" . utf8_encode($RFP['status']) . "</center></div>";
        $row[] = ($RFP['ts'] > 0 ? "<center><span class=\"ico-money icon-white\" style=\"color:" . ($RFP['nfs'] == 0 ? "#e0e0e0" : "#08c") . "\"></span></center>" : "");
    }
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    if ($iFilteredTotal > 0) {
        $row = $output['aaData'][0];
        $row[1] = "<input type='hidden' name='totparc' id='totparc' value='" . $totparc . "'/>" .
        "<input type='hidden' name='qtdparc' id='qtdparc' value='" . $iFilteredTotal . "'/>" . $row[1];
        $output['aaData'][0] = $row;
    }
    echo json_encode($output);
}
odbc_close($con);