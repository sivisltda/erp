<?php
include '../../Connections/configini.php';
$disabled = 'disabled';

if (isset($_POST['bntAplicarReajuste'])) {
    if (is_numeric($_POST['txtId'])) {
        $indice = (100 + valoresNumericos('txtReajuste_venda')) / 100;
        odbc_exec($con, "insert into sf_produtos_historico_preco(id_produto,data_atual,preco_atual,margem_lucro)
        select conta_produto,GETDATE(),preco_venda,
        isnull((select top 1 ((sf_produtos.preco_venda - ((valor_total / quantidade) + isnull(sf_produtos.valor_impostos,0) 
        + isnull(sf_produtos.valor_frete,0) + isnull(sf_produtos.valor_comissoes,0))) / (valor_total / quantidade)) * 100
        from dbo.sf_vendas v inner join sf_vendas_itens i on v.id_venda = i.id_venda where cov = 'C' and status = 'Aprovado' and quantidade > 0 
        and produto = sf_produtos.conta_produto order by data_venda desc),0) 
        from sf_produtos where cod_fabricante = " . $_POST['txtId']) or die(odbc_errormsg());
        odbc_exec($con, "update sf_produtos set preco_venda = preco_venda * " . $indice . " where tipo = 'P' and cod_fabricante = " . $_POST['txtId']) or die(odbc_errormsg());
        echo "<script>alert('Reajuste de " . $_POST['txtReajuste_venda'] . "% aplicados com sucesso para os produtos deste Fabricante!');</script>";
    }
}

if (isset($_POST['bntAplicar'])) {
    if (is_numeric($_POST['txtId'])) {
        odbc_exec($con, "update sf_produtos set valor_comissao_venda = " . valoresNumericos('txtComissao') . " where tipo = 'P' and cod_fabricante = " . $_POST['txtId']) or die(odbc_errormsg());
        echo "<script>alert('Comissões aplicadas aos produtos com sucesso!');</script>";
    }
}

if (isset($_POST['bntSave'])) {
    if (is_numeric($_POST['txtId'])) {
        odbc_exec($con, "update sf_fabricantes set " .
                        "nome_fabricante = " . valoresTexto('txtdescricao_cor') .
                        ",valor_comissao_fabricante = " . valoresNumericos('txtComissao_venda') .
                        " where id_fabricante = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_fabricantes(nome_fabricante,valor_comissao_fabricante)values(" .
                        valoresTexto('txtdescricao_cor') . "," .
                        valoresNumericos('txtComissao_venda') . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_fabricante from sf_fabricantes order by id_fabricante desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}

if (isset($_POST['bntDelete'])) {
    if (is_numeric($_POST['txtId'])) {
        odbc_exec($con, "DELETE FROM sf_fabricantes WHERE id_fabricante = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_fabricantes where id_fabricante =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_fabricante'];
        $tipo = $RFP['tipo_grupo'];
        $nome_fabricante = $RFP['nome_fabricante'];
        $comissao_venda = escreverNumero($RFP['valor_comissao_fabricante']);
    }
} else {
    $disabled = '';
    $id = '';
    $tipo = '';
    $nome_fabricante = '';
    $comissao_venda = escreverNumero(0);
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script> 
<script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<body>
    <form action="FormFabricantes.php" name="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext">Fabricantes</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont">
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <input name="txtComissao" id="txtComissao" value="<?php echo $comissao_venda; ?>" type="hidden"/>
            <div style="width: 100%;float: left">
                <span>Nome do Fabricante:</span>
                <input name="txtdescricao_cor" id="txtdescricao_cor" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="128" value="<?php echo utf8_encode($nome_fabricante); ?>"/>
            </div>
            <div style="clear:both;height: 5px;"></div>
            <div style="width: 100%;float: left">
                <span>Comissão Venda (%):</span> <br>
                <input style="text-align:right;width: 40%" name="txtComissao_venda" id="txtComissao_venda" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="7" value="<?php echo $comissao_venda; ?>"/>
                <button onClick="return confirm('Deseja aplicar esta comissão aos produtos deste fabricante?');" style="width: 59%" class="btn btn-success" type="submit" title="Gravar" name="bntAplicar" id="bntAplicar" <?php
                if (!is_numeric($id) || $disabled == "") {
                    echo "disabled";
                }
                ?>><span class="ico-checkmark"></span>Aplicar Comissão para Produtos</button>
            </div>
            <div style="clear:both;height: 5px;"></div>
            <div style="width: 100%;float: left">
                <span>Reajustar produtos em (%):</span> <br>
                <input style="text-align:right;width: 40%;" name="txtReajuste_venda" id="txtReajuste_venda" type="text" <?php
                if (!is_numeric($id) || $disabled == "") {
                    echo "disabled";
                }
                ?> class="input-medium" maxlength="7" value="<?php echo escreverNumero(0);?>"/>
                <button onClick="return reajustarPreco();" style="width: 59%" class="btn btn-info" type="submit" title="Gravar" name="bntAplicarReajuste" id="bntAplicarReajuste" <?php
                if (!is_numeric($id) || $disabled == "") {
                    echo "disabled";
                }
                ?>><span class="ico-checkmark"></span>Aplicar Reajuste para Produtos</button>
            </div>
            <div style="clear:both;height: 5px;"></div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" name="bntSave" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK" ><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else { ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/animatedprogressbar/animated_progressbar.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/charts.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/ckeditor/ckeditor.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/jcrop/jquery.Jcrop.min.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>   
    <script type="text/javascript">
        
        $("#txtComissao_venda, #txtReajuste_venda").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], suffix: "%"});
        
        function reajustarPreco() {
            if (confirm('Deseja aplicar este Reajuste aos produtos deste fabricante?')) {
                if (textToNumber($("#txtReajuste_venda").val()) > 0) {
                    return true;
                } else {
                    alert('Preencha um valor numérico válido para esta operação!');
                    return false;
                }
            } else {
                return false;
            }
        }
    </script>
    <?php odbc_close($con); ?>
</body>
