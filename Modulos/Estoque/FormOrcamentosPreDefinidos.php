<?php
include "../../Connections/configini.php";
$FinalUrl = '';
$TotalGeral = 0;
if ($_GET['idx'] != '') {
    $FinalUrl = "&idx=" . $_GET['idx'];
}
if ($_GET['tpx'] != '') {
    $FinalUrl = $FinalUrl . "&tpx=" . $_GET['tpx'];
}
if ($_GET['tdx'] != '') {
    $FinalUrl = $FinalUrl . "&tdx=" . $_GET['tdx'];
}
if ($_GET['dti'] != '') {
    $FinalUrl = $FinalUrl . "&dti=" . $_GET['dti'];
}
if ($_GET['dtf'] != '') {
    $FinalUrl = $FinalUrl . "&dtf=" . $_GET['dtf'];
}
if ($_GET['gb'] != '') {
    $FinalUrl = $FinalUrl . "&gb=" . $_GET['gb'];
}
if ($_GET['jn'] != '') {
    $FinalUrl = $FinalUrl . "&jn=" . $_GET['jn'];
}
if ($_GET['id'] != '') {
    $FinalUrl = "id=" . $_GET['id'] . $FinalUrl;
}
if ($_GET['menu']) {
    $menu = $_GET['menu'];
} else {
    $menu = 1;
}

$disabled = 'disabled';
$PegaURL = '';
$r_documento = '';
$r_ven_ini = '';
$r_valor = '';
$r_valor_tot = '';
$r_quantidade = '1';
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        $query = "set dateformat dmy;update sf_vendas set " .
                "cliente_venda = " . $_POST['txtFonecedor'] . "," .
                "historico_venda = '" . utf8_decode($_POST['txtHistorico']) . "'," .
                "vendedor = '" . strtoupper(utf8_decode($_POST['txtDestinatario'])) . "'," .
                "destinatario = '" . utf8_decode($_POST['txtAutorizacao']) . "'," .
                "tipo_documento = " . $_POST['txtTipo'] . "," .
                "cov = 'O'," .
                "garantia_venda = '" . utf8_decode($_POST['txtGarantiaProd']) . "'," .
                "garantia_servico = '" . utf8_decode($_POST['txtGarantiaServ']) . "'," .
                "descontop = " . valoresNumericos('txtProdDesconto' . $i) . "," .
                "descontos = " . valoresNumericos('txtServDesconto' . $i) . "," .
                "entrega = '" . utf8_decode($_POST['txtDtEntrega']) . "'," .
                "validade = '" . utf8_decode($_POST['txtDtProposta']) . "'," .
                "descontots = " . utf8_decode($_POST['txtServSinal']) . "," .
                "descontotp = " . utf8_decode($_POST['txtProdSinal']) . "," .
                "n_servicos = " . valoresCheck('ckbNCV') . "," .
                "comentarios_venda = '" . utf8_decode($_POST['txtComentarios']) . "' where id_venda = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        //-------------------------------Campos-de-Preenchimento-para-Documentos------------------------
        odbc_exec($con, "delete from sf_vendas_campos where id_sf_vendas = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
        $sql = "select id_tipo_documento_campos,campo from sf_tipo_documento_campos where inativo = 0 AND tipo_documento = " . $_POST['txtTipo'] . " ORDER BY 1";
        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $sss = "insert into sf_vendas_campos(id_sf_vendas,tipo_documento_campos,conteudo) values (" .
                    $_POST['txtId'] . "," .
                    $RFP['id_tipo_documento_campos'] . ",'" .
                    utf8_decode($_POST['txtCampo' . $RFP['id_tipo_documento_campos']]) . "')";
            odbc_exec($con, $sss);
        }
        //-----------------------------------------------------------------------------------------------
        $notIn = '0';
        if (isset($_POST['txtIdTP']) || isset($_POST['txtIdTS'])) {
            $maxP = $_POST['txtIdTP'];
            for ($i = 0; $i <= $maxP; $i++) {
                if (isset($_POST['txtIP_' . $i])) {
                    if (is_numeric($_POST['txtIP_' . $i])) {
                        $notIn = $notIn . "," . $_POST['txtIP_' . $i];
                    }
                }
            }
            $maxS = $_POST['txtIdTS'];
            for ($i = 0; $i <= $maxS; $i++) {
                if (isset($_POST['txtIPS_' . $i])) {
                    if (is_numeric($_POST['txtIPS_' . $i])) {
                        $notIn = $notIn . "," . $_POST['txtIPS_' . $i];
                    }
                }
            }
            odbc_exec($con, "delete from sf_vendas_itens where id_venda = " . $_POST['txtId'] . " and id_item_venda not in (" . $notIn . ")");
        }
        if (isset($_POST['txtIdTP'])) {
            $maxP = $_POST['txtIdTP'];
            for ($i = 0; $i <= $maxP; $i++) {
                if (isset($_POST['txtIP_' . $i])) {
                    if (is_numeric($_POST['txtIP_' . $i])) {
                        odbc_exec($con, "update sf_vendas_itens set grupo = " . valoresSelect('txtGR_' . $i) . ",produto = " . $_POST['txtPD_' . $i] . ",quantidade = " . valoresNumericos('txtVQ_' . $i) . " ,valor_total = " . valoresNumericos('txtVP_' . $i) . " where id_item_venda = " . $_POST['txtIP_' . $i]) or die(odbc_errormsg());
                    } else {
                        odbc_exec($con, "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total) values(" . $_POST['txtId'] . "," . valoresSelect('txtGR_' . $i) . "," . $_POST['txtPD_' . $i] . "," . valoresNumericos('txtVQ_' . $i) . "," . valoresNumericos('txtVP_' . $i) . ")") or die(odbc_errormsg());
                    }
                }
            }
        }
        if (isset($_POST['txtIdTS'])) {
            $maxS = $_POST['txtIdTS'];
            for ($i = 0; $i <= $maxS; $i++) {
                if (isset($_POST['txtIPS_' . $i])) {
                    if (is_numeric($_POST['txtIPS_' . $i])) {
                        odbc_exec($con, "update sf_vendas_itens set grupo = " . valoresSelect('txtGRS_' . $i) . ",produto = " . $_POST['txtPDS_' . $i] . ",quantidade = " . valoresNumericos('txtVQS_' . $i) . " ,valor_total = " . valoresNumericos('txtVPS_' . $i) . " where id_item_venda = " . $_POST['txtIPS_' . $i]) or die(odbc_errormsg());
                    } else {
                        odbc_exec($con, "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total) values(" . $_POST['txtId'] . "," . valoresSelect('txtGRS_' . $i) . "," . $_POST['txtPDS_' . $i] . "," . valoresNumericos('txtVQS_' . $i) . "," . valoresNumericos('txtVPS_' . $i) . ")") or die(odbc_errormsg());
                    }
                }
            }
        }

        //----------------------------Calcula--Número--de--Parcelas-------------------------------------
        $totaldeParcelas = 0;
        for ($i = 1; $i <= 60; $i++) {
            $explode = valoresData('txtParcelaD_' . $i);
            $valorParcela = valoresNumericos('txtParcelaV_' . $i);           
            if (is_numeric($valorParcela) && $explode != "null") {
                if ($valorParcela > 0) {
                    $totaldeParcelas = $totaldeParcelas + 1;
                }
            }
        }
        for ($i = 1; $i <= 60; $i++) {
            if (is_numeric($_POST['txtID_' . $i])) {
                if ($_POST['txtPG_' . $i] == "S") {
                    $totaldeParcelas = $totaldeParcelas + 1;
                }
            }
        }
        //----------------------------Altera--as--Parcelas----------------------------------------------
        $toDelete = "0";
        for ($i = 1; $i <= 60; $i++) {
            $explode = valoresData('txtParcelaD_' . $i);
            $valorParcela = valoresNumericos('txtParcelaV_' . $i);           
            if (is_numeric($valorParcela) && $explode != "null") {
                if ($valorParcela > 0) {
                    if ($_POST['txtPG_' . $i] != "S") {
                        if (is_numeric($_POST['txtID_' . $i])) {
                            odbc_exec($con, "update sf_venda_parcelas set
                                                    venda = " . $_POST['txtId'] . "," .
                                            "numero_parcela = " . $i . "," .
                                            "data_parcela = '" . $_POST['txtParcelaD_' . $i] . "'," .
                                            "sa_descricao = '" . $_POST['txtParcelaDC_' . $i] . "'," .
                                            "valor_parcela = " . $valorParcela . "," .
                                            "tipo_documento = " . $_POST['txtTipo_' . $i] . "," .
                                            "pa = '" . str_pad($i, 2, "0", STR_PAD_LEFT) . "/" . str_pad($totaldeParcelas, 2, "0", STR_PAD_LEFT) . "' " .
                                            "where id_parcela = " . $_POST['txtID_' . $i]) or die(odbc_errormsg());
                            $toDelete = $toDelete . "," . $_POST['txtID_' . $i];
                        }
                    }
                }
            }
        }
        //----------------------------Exclui--as--Parcelas--Removidas-----------------------------------
        odbc_exec($con, "DELETE FROM sf_venda_parcelas where id_parcela not in (" . $toDelete . ") and data_pagamento is null and inativo = 0 and venda = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
        //----------------------------Insere--as--Parcelas-Novas----------------------------------------
        for ($i = 1; $i <= 60; $i++) {
            $explode = valoresData('txtParcelaD_' . $i);
            $valorParcela = valoresNumericos('txtParcelaV_' . $i);           
            if (is_numeric($valorParcela) && $explode != "null") {
                if ($valorParcela > 0) {
                    if ($_POST['txtPG_' . $i] != "S") {
                        if ($_POST['txtID_' . $i] == "") {
                            odbc_exec($con, "insert into sf_venda_parcelas(venda,numero_parcela,data_parcela,sa_descricao,valor_parcela,tipo_documento,historico_baixa,pa,data_cadastro) values (" .
                                            $_POST['txtId'] . "," . $i . ",'" . $_POST['txtParcelaD_' . $i] . "','" . $_POST['txtParcelaDC_' . $i] . "'," . $valorParcela . "," . $_POST['txtTipo_' . $i] . ",'" . utf8_decode($_POST['txtHistorico']) . "','" . str_pad($i, 2, "0", STR_PAD_LEFT) . "/" . str_pad($totaldeParcelas, 2, "0", STR_PAD_LEFT) . "',getdate())") or die(odbc_errormsg());
                            $toInsert = $toInsert . "," . $i;
                        }
                    }
                }
            }
        }
    } else {
        $nossoNumero = "null";
        $bol_id_banco = "null";
        $bol_data_criacao = "null";
        $bol_valor = "null";
        $bol_juros = "null";
        $bol_multa = "null";
        if (isset($_POST['txtBanco'])) {
            if ($_POST['txtBanco'] != "null") {
                $cur = odbc_exec($con, "select isnull(MAX(x.bol_nosso_numero),0) vm, isnull(nosso_numero,0) nn from (
                        select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_boleto p on p.bol_id_banco = b.id_bancos
                        where id_bancos = '" . $_POST['txtBanco'] . "' union all     
                        select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_solicitacao_autorizacao_parcelas p on p.bol_id_banco = b.id_bancos
                        where id_bancos = '" . $_POST['txtBanco'] . "' union all                            
                        select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_venda_parcelas p on p.bol_id_banco = b.id_bancos
                        where id_bancos = '" . $_POST['txtBanco'] . "' union all
                        select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_lancamento_movimento_parcelas l on l.bol_id_banco = b.id_bancos
                        where id_bancos = '" . $_POST['txtBanco'] . "') as x group by x.nosso_numero") or die(odbc_errormsg());
                while ($RFP = odbc_fetch_array($cur)) {
                    if ($RFP['nn'] > $RFP['vm']) {
                        $nossoNumero = $RFP['nn'];
                    } else {
                        $nossoNumero = bcadd($RFP['vm'], 1);
                    }
                }
                $bol_id_banco = $_POST['txtBanco'];
                $bol_data_criacao = "getdate()";
                $bol_juros = "0.00";
                $bol_multa = "0.00";
            }
        }
        $query = "set dateformat dmy;insert into sf_vendas(vendedor,cliente_venda,historico_venda,data_venda,destinatario,status,tipo_documento,descontop,descontos,entrega,validade,descontots,descontotp,comentarios_venda,sys_login,cov,garantia_venda,garantia_servico,n_servicos,grupo_conta,conta_movimento)values('" .
                strtoupper(utf8_decode($_POST['txtDestinatario'])) . "'," .
                $_POST['txtFonecedor'] . ",'" .
                utf8_decode($_POST['txtHistorico']) . "','" .
                $_POST['txtDtVenda'] . " " . date("H:i:s") . "','" .
                utf8_decode($_POST['txtAutorizacao']) . "','" .
                "Aguarda'," .
                $_POST['txtTipo'] . "," .
                valoresNumericos('txtProdDesconto') . "," .
                valoresNumericos('txtServDesconto') . ",'" .
                utf8_decode($_POST['txtDtEntrega']) . "','" .
                utf8_decode($_POST['txtDtProposta']) . "'," .
                utf8_decode($_POST['txtServSinal']) . "," .
                utf8_decode($_POST['txtProdSinal']) . ",'" .
                utf8_decode($_POST['txtComentarios']) . "','" . $_SESSION["login_usuario"] . "','O','" . utf8_decode($_POST['txtGarantiaProd']) . "','" . utf8_decode($_POST['txtGarantiaServ']) . "'," . valoresCheck('ckbNCV') . ",14,1)";
        //echo $query;
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_venda from sf_vendas order by id_venda desc") or die(odbc_errormsg());
        $nn = odbc_result($res, 1);
        //-----------------------------------------Campos--de--descricao------------------------------
        $sql = "select id_tipo_documento_campos,campo from sf_tipo_documento_campos where inativo = 0 AND tipo_documento = " . $_POST['txtTipo'] . " ORDER BY 1";
        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $sss = "insert into sf_vendas_campos(id_sf_vendas,tipo_documento_campos,conteudo) values (" .
                    $nn . "," .
                    $RFP['id_tipo_documento_campos'] . ",'" .
                    utf8_decode($_POST['txtCampo' . $RFP['id_tipo_documento_campos']]) . "')";
            //echo $sss;
            odbc_exec($con, $sss);
        }
        //--------------------------------------------------------------------------------------------
        if (isset($_POST['txtIdTP'])) {
            $maxP = $_POST['txtIdTP'];
            for ($i = 0; $i <= $maxP; $i++) {
                if (isset($_POST['txtIP_' . $i])) {
                    odbc_exec($con, "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total) values(" . $nn . "," . valoresSelect('txtGR_' . $i) . "," . $_POST['txtPD_' . $i] . "," . valoresNumericos('txtVQ_' . $i) . "," . valoresNumericos('txtVP_' . $i) . ")") or die(odbc_errormsg());
                }
            }
        }
        if (isset($_POST['txtIdTS'])) {
            $maxS = $_POST['txtIdTS'];
            for ($i = 0; $i <= $maxS; $i++) {
                if (isset($_POST['txtIPS_' . $i])) {
                    odbc_exec($con, "insert into sf_vendas_itens(id_venda,grupo,produto,quantidade,valor_total) values(" . $nn . "," . valoresSelect('txtGRS_' . $i) . "," . $_POST['txtPDS_' . $i] . "," . valoresNumericos('txtVQS_' . $i) . "," . valoresNumericos('txtVPS_' . $i) . ")") or die(odbc_errormsg());
                }
            }
        }
        $_POST['txtId'] = $nn;
        $totaldeParcelas = 0;
        for ($i = 1; $i <= 60; $i++) {
            $explode = valoresData('txtParcelaD_' . $i);
            $valorParcela = valoresNumericos('txtParcelaV_' . $i);           
            if (is_numeric($valorParcela) && $explode != "null") {
                if ($valorParcela > 0) {
                    $totaldeParcelas = $totaldeParcelas + 1;
                }
            }
        }
        for ($i = 1; $i <= 60; $i++) {
            $explode = valoresData('txtParcelaD_' . $i);
            $valorParcela = valoresNumericos('txtParcelaV_' . $i);            
            if (is_numeric($valorParcela) && $explode != "null") {
                if ($valorParcela > 0) {
                    if ($bol_id_banco != "null") {
                        $bol_valor = $valorParcela;
                    }
                    odbc_exec($con, "set dateformat dmy; insert into sf_venda_parcelas(venda,numero_parcela,data_parcela,sa_descricao,valor_parcela,tipo_documento,historico_baixa,pa,bol_id_banco,bol_data_criacao,bol_valor,bol_juros,bol_multa,bol_nosso_numero,data_cadastro) values (" .
                                    $_POST['txtId'] . "," . $i . ",'" . $_POST['txtParcelaD_' . $i] . "','" . $_POST['txtParcelaDC_' . $i] . "'," . $valorParcela . "," . $_POST['txtTipo_' . $i] . ",'" . utf8_decode($_POST['txtHistorico']) . "','" . str_pad($i, 2, "0", STR_PAD_LEFT) . "/" . str_pad($totaldeParcelas, 2, "0", STR_PAD_LEFT) . "'," .
                                    $bol_id_banco . "," .
                                    $bol_data_criacao . "," .
                                    $bol_valor . "," .
                                    $bol_juros . "," .
                                    $bol_multa . "," .
                                    $nossoNumero . ",getdate())") or die(odbc_errormsg());
                    if ($nossoNumero != "null") {
                        $nossoNumero = bcadd($nossoNumero, 1);
                    }
                }
            }            
        }
        //header('Localização: FormOrcamentos.php?id='.$nn);
        $FinalUrl = "id=" . $nn;
        if ($_GET['idx'] != '') {
            $FinalUrl = "&idx=" . $_GET['idx'];
        }
        if ($_GET['tpx'] != '') {
            $FinalUrl = $FinalUrl . "&tpx=" . $_GET['tpx'];
        }
        if ($_GET['tdx'] != '') {
            $FinalUrl = $FinalUrl . "&tdx=" . $_GET['tdx'];
        }
        if ($_GET['dti'] != '') {
            $FinalUrl = $FinalUrl . "&dti=" . $_GET['dti'];
        }
        if ($_GET['dtf'] != '') {
            $FinalUrl = $FinalUrl . "&dtf=" . $_GET['dtf'];
        }
        if ($_GET['gb'] != '') {
            $FinalUrl = $FinalUrl . "&gb=" . $_GET['gb'];
        }
        if ($_GET['jn'] != '') {
            $FinalUrl = $FinalUrl . "&jn=" . $_GET['jn'];
        }
        echo "<script>window.top.location.href = 'FormOrcamentos.php?menu=" . $menu . "&" . $FinalUrl . "';</script>";
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_vendas_campos where id_sf_vendas = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
        odbc_exec($con, "DELETE FROM sf_vendas_itens where id_venda = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
        odbc_exec($con, "DELETE FROM sf_venda_parcelas where venda = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
        odbc_exec($con, "DELETE FROM sf_vendas WHERE id_venda = " . $_POST['txtId']) or die(odbc_errormsg());
        if ($_GET['jn'] == 'c') {
            echo "<script>alert('Registro excluido com sucesso');window.top.location.href = './../CRM/Gerenciador-Prospects.php?Cli=" . $_GET['gb'] . "';</script>";
        } else {
            echo "<script>alert('Registro excluido com sucesso');window.top.location.href = 'Orcamentos.php?menu=" . $menu . "&id=1';</script>";
        }
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
    echo "<script>window.top.location.href = 'FormOrcamentos.php?menu=" . $menu . "';</script>";
}
if (isset($_POST['bntAprov']) || isset($_POST['bntReprov'])) {
    $canAprov = "true";
    if ($ckb_fin_dmp_ == 0) {
        $cur = odbc_exec($con, "select quantidade,valor_total,descontop,descontotp,preco_venda,valor_desconto
                from sf_vendas_itens vi inner join sf_produtos p on vi.produto = p.conta_produto
                inner join sf_vendas v on v.id_venda = vi.id_venda where tipo = 'P' and cov in ('V','O') and v.id_venda = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $MaxDesconto = ($RFP['quantidade'] * $RFP['preco_venda']) * ((100 - $RFP['valor_desconto']) / 100);
            if ($RFP['descontotp'] == 1) {
                $ValorFinal = $RFP['valor_total'] - $RFP['descontop'];
            } else {
                $ValorFinal = $RFP['valor_total'] - ((100 - $RFP['descontop']) / 100);
            }
            if ($MaxDesconto > $ValorFinal) {
                $canAprov = "false";
            }
        }
    }
    if ($canAprov == "true") {
        if (isset($_POST['bntAprov'])) {
            odbc_exec($con, "update sf_vendas set status = 'Aprovado', data_aprov = getdate() where id_venda = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
        } else {
            odbc_exec($con, "update sf_vendas set status = 'Reprovado', data_aprov = getdate() where id_venda = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
        }
    } else {
        echo "<script>alert('Preço final para os produtos lançados não são válidos!');</script>";
    }
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select *, (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = id_fornecedores_despesas) email_contato
    from sf_vendas inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = sf_vendas.cliente_venda 
    left join tb_estados e on f.estado = e.estado_codigo left join tb_cidades c on f.cidade = c.cidade_codigo where id_venda = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $dados1 = '';
        $dados2 = '';
        $id = $RFP['id_venda'];
        $cliente_venda = $RFP['cliente_venda'];
        $vendedor = strtoupper(utf8_encode($RFP['vendedor']));
        $data_venda = escreverData($RFP['data_venda']);
        $historico_venda = utf8_encode($RFP['historico_venda']);
        $comentarios_venda = utf8_encode($RFP['comentarios_venda']);
        $sys_login = utf8_encode($RFP['sys_login']);
        $destinatario = utf8_encode($RFP['destinatario']);
        $status = utf8_encode($RFP['status']);
        $tipo = utf8_encode($RFP['tipo_documento']);
        $garantia_prod = utf8_encode($RFP['garantia_venda']);
        $garantia_serv = utf8_encode($RFP['garantia_servico']);
        $descontop = escreverNumero($RFP['descontop']);
        $descontos = escreverNumero($RFP['descontos']);
        $entrega = utf8_encode($RFP['entrega']);
        $validade = utf8_encode($RFP['validade']);
        $descontots = utf8_encode($RFP['descontots']);
        $descontotp = utf8_encode($RFP['descontotp']);
        $n_servicos = utf8_encode($RFP['n_servicos']);
        $razao_social = utf8_encode($RFP['razao_social']);
        if ($RFP['endereco'] != '') {
            $dados1 = utf8_encode($RFP['endereco']);
        }
        if ($RFP['numero'] != '') {
            $dados1 = $dados1 . ' N° ' . utf8_encode($RFP['numero']);
        }
        if ($RFP['complemento'] != '') {
            $dados1 = $dados1 . ' ' . utf8_encode($RFP['complemento']);
        }
        if ($RFP['bairro'] != '') {
            $dados1 = $dados1 . ',' . utf8_encode($RFP['bairro']);
        }
        if ($RFP['cidade_nome'] != '') {
            $dados1 = $dados1 . "\n" . utf8_encode($RFP['cidade_nome']);
        }
        if ($RFP['estado_sigla'] != '') {
            $dados1 = $dados1 . '-' . utf8_encode($RFP['estado_sigla']);
        }
        if ($RFP['cep'] != '') {
            $dados1 = $dados1 . ', Cep: ' . utf8_encode($RFP['cep']);
        }
        if ($RFP['cnpj'] != '') {
            $dados2 = $dados2 . 'CNPJ/CPF:' . utf8_encode($RFP['cnpj']);
        }
        if ($RFP['telefone_contato'] != '') {
            $dados2 = $dados2 . "\n" . 'Tel:' . utf8_encode($RFP['telefone_contato']);
        }
        if ($RFP['email_contato'] != '') {
            $dados2 = $dados2 . ' E-mail:' . utf8_encode($RFP['email_contato']);
            $email_destinatario = utf8_encode($RFP['email_contato']);
        }
    }
    $i = 0;
    $r_quantidade = '1';
    $r_valor_tot = 0;
    $cur = odbc_exec($con, "select sa_descricao,data_parcela,valor_parcela from dbo.sf_venda_parcelas 
              where inativo = 0 and venda = " . $PegaURL . " order by id_parcela");
    while ($RFP = odbc_fetch_array($cur)) {
        if ($i == 0) {
            $r_documento = $RFP['sa_descricao'];
            $r_ven_ini = escreverData($RFP['data_parcela']);
        }
        $r_valor = escreverNumero($RFP['valor_parcela']);
        $r_valor_tot = $r_valor_tot + $RFP['valor_parcela'];
        $i++;
    }
    $r_valor_tot = escreverNumero($r_valor_tot);
    if ($i > 0) {
        $r_quantidade = $i;
    }
} else {
    $disabled = '';
    $id = '';
    if ($_GET['jn'] == 'c') {
        $cliente_venda = $_GET['gb'];
    } else {
        $cliente_venda = '';
    }
    $vendedor = '';
    $data_venda = getData("T");
    $historico_venda = 'ORÇAMENTO';
    $comentarios_venda = '';
    $sys_login = '';
    $dados1 = '';
    $dados2 = '';
    $tipo = '';
    $status = '';
    $garantia_prod = '';
    $garantia_serv = '';
    $descontop = '0';
    $descontos = '0';
    $entrega = '';
    $validade = '';
    $descontots = '0';
    $descontotp = '0';
    $n_servicos = '0';
    $razao_social = '';
    $email_destinatario = '';
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
if (isset($_POST['bntVoltar'])) {
    if ($_GET['idx'] != '') {
        $FinalUrl = "?id=" . $_GET['idx'];
    }
    if ($_GET['tpx'] != '') {
        $FinalUrl = $FinalUrl . "&tp=" . $_GET['tpx'];
    }
    if ($_GET['tdx'] != '') {
        $FinalUrl = $FinalUrl . "&td=" . $_GET['tdx'];
    }
    if ($_GET['dti'] != '') {
        $FinalUrl = $FinalUrl . "&dti=" . $_GET['dti'];
    }
    if ($_GET['dtf'] != '') {
        $FinalUrl = $FinalUrl . "&dtf=" . $_GET['dtf'];
    }
    if ($_GET['jn'] == 'c' && is_numeric($_GET['gb'])) {
        echo "<script>window.top.location.href = './../CRM/Gerenciador-Prospects.php?Cli=" . $_GET['gb'] . "';</script>";
    } else {
        if ($TipoGrupo == "D") {
            echo "<script>window.top.location.href = 'Contas-a-Pagar.php" . $FinalUrl . "';</script>";
        } elseif ($TipoGrupo == "C") {
            echo "<script>window.top.location.href = 'Contas-a-Receber.php" . $FinalUrl . "';</script>";
        }
    }
}
if (isset($_POST['bntConvert'])) {
    odbc_exec($con, "update sf_vendas set status = 'Aguarda', cov = 'V' where id_venda = '" . $_POST['txtId'] . "'") or die(odbc_errormsg());
    odbc_exec($con, "update sf_fornecedores_despesas set tipo = 'C' where id_fornecedores_despesas in (select cliente_venda from sf_vendas where id_venda = '" . $_POST['txtId'] . "') and tipo = 'P'") or die(odbc_errormsg());
    echo "<script>window.top.location.href = 'Orcamentos.php?menu=1&id=1';</script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Orçamentos</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="./../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">  
        <link href="../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">        
        <link href="./../../css/main.css" rel="stylesheet">        
        <script src="../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
        <script src="../../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <style>
            .inputDireito {
                text-align:right;
            }
            .fancybox-wrap, .fancybox-skin, .fancybox-outer, .fancybox-inner, .fancybox-image, .fancybox-wrap object, .fancybox-nav, .fancybox-nav span, .fancybox-tmp {
                padding: 0;
                margin: 0;
                border: 0;
                outline: none;
                vertical-align: top;
                background:url(../../img/fundosForms/formBancos.PNG);
                border:solid 1 #cfcfcf;
            }
            #formulario {
                float: left;
                width: 100%;
            }
            .linha {
                float: left;
                width: 100%;
                display: block;
                padding-top: 10px;
                padding-bottom: 10px;
            }                       
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1 style="margin-top:8px">Orçamentos Pré Definidos</h1>
                    </div>
                    <div class="row-fluid">
                        <form action="FormOrcamentos.php?menu=<?php echo $menu; ?>&<?php echo $FinalUrl; ?>" method="POST" name="frmEnviaDados" id="frmEnviaDados">
                            <div class="span12">
                                <div class="block">
                                    <div id="formulario">
                                        <div>
                                            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                                            <div style="width:49%; float:left"> 
                                                <div>Descrição:</div>
                                                <div><input type="text" name="txtDescricao" id="txtDescricao" value="" <?php echo $disabled; ?>></div> 
                                            </div>
                                        </div>
                                        <span style="width:100%; display:block; float:left; margin-top:4px">                                            
                                            <div style="margin-top:5px" class="head dblue">
                                                <h2>Produtos</h2>
                                                <ul class="buttons"><li><div><a href="javascript:void(0)" onclick="AbrirBox(1)" title="Adicionar Produto"><div class="icon"><span class="ico-plus"></span></div></a></div></li></ul>
                                            </div>                                                    
                                            <table class="table" cellpadding="0" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th width="18%">Grupos de Produtos:</th>
                                                        <th width="42%">Produtos:</th>
                                                        <th width="5%">Und:</th>
                                                        <th width="5%">Qtd:</th>
                                                        <th width="12.5%">Valor Unitário:</th>
                                                        <th width="12.5%">Valor Total:</th>
                                                        <th width="5%">Ação:</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <select name="txtGrupo" class="select" style="width:100%" id="txtGrupo" <?php echo $disabled; ?>>
                                                                <option value="null">--Selecione--</option>
                                                                <?php
                                                                if ($disabled == "") {
                                                                    $cur = odbc_exec($con, "select id_contas_movimento,descricao from sf_contas_movimento where tipo = 'P' order by descricao") or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                                        ?>
                                                                        <option value="<?php echo $RFP['id_contas_movimento'] ?>"><?php echo utf8_encode($RFP['descricao']); ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <span id="carregando" name="carregando" style="color:#666; display:none">Aguarde, carregando...</span>
                                                            <select name="txtConta" class="select" style="width:100%" id="txtConta"  <?php echo $disabled; ?>>
                                                                <option value="null">--Selecione--</option>
                                                                <?php
                                                                if ($disabled == "") {
                                                                    $sql = "select conta_produto,descricao,codigo_interno from sf_produtos where conta_produto > 0 AND tipo = 'P' ORDER BY descricao";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                                        ?>
                                                                        <option value="<?php echo $RFP['conta_produto'] ?>"><?php
                                                                            echo utf8_encode($RFP['descricao']);
                                                                            if (strlen($RFP['codigo_interno']) > 0) {
                                                                                echo " [" . $RFP['codigo_interno'] . "]";
                                                                            }
                                                                            ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>                                                            
                                                        </td>
                                                        <td><input name="txtUnidade" id="txtUnidade" type="text" class="input-xlarge inputCenter" value="" disabled/></td>
                                                        <td><input name="txtQtdProduto" id="txtQtdProduto" <?php echo $disabled; ?> type="text" class="input-xlarge inputCenter" value="1" onBlur="prodValor()"/></td>
                                                        <td><input name="hidValorProduto" id="hidValorProduto" <?php echo $disabled; ?> type="text" class="input-xlarge inputCenter" value="<?php echo escreverNumero(0); ?>" onBlur="prodValor()"/></td>
                                                        <td><input name="txtValorProduto" id="txtValorProduto" type="text" class="input-xlarge inputCenter" value="<?php echo escreverNumero(0); ?>" disabled /></td>
                                                        <td><center><button class="btn btn-primary" id="for-block3" <?php echo $disabled; ?> type="button" onClick="novaLinha()">+</button></center></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <table class="table" style="margin-top:-7px" cellpadding="0" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th width="18%"></th>
                                                        <th width="5%"></th>
                                                        <th width="37%"></th>
                                                        <th width="5%"></th>
                                                        <th width="5%"></th>
                                                        <th width="12.5%"></th>
                                                        <th width="12.5%"></th>
                                                        <th width="5%"></th>
                                                    </tr>
                                                </thead>                                                
                                                <tbody id="tabela_produto">
                                                    <?php
                                                    $i = 0;
                                                    $totalp = 0;
                                                    $idTP = 0;
                                                    if ($PegaURL != "") {
                                                        $cur = odbc_exec($con, "select id_item_venda,conta_movimento grupo,gc.descricao grupodesc,conta_produto produto,cm.descricao produtodesc,
                                                        quantidade,valor_total,unidade_comercial,codigo_interno from sf_vendas_itens vi left join sf_contas_movimento gc on vi.grupo = gc.id_contas_movimento inner join sf_produtos cm 
                                                        on cm.conta_produto = vi.produto where cm.tipo = 'P' and id_venda = " . $PegaURL);
                                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                                            <tr id='tabela_linha_<?php echo $i; ?>'>
                                                                <td><input name='txtIP_<?php echo $i; ?>' id='txtIP_<?php echo $i; ?>' type='hidden' value='<?php echo $RFP['id_item_venda']; ?>'/>
                                                                    <input name='txtGR_<?php echo $i; ?>' id='txtGR_<?php echo $i; ?>' type='hidden' value='<?php echo $RFP['grupo']; ?>'/>
                                                                    <?php echo utf8_encode($RFP['grupodesc']); ?>
                                                                </td>
                                                                <td><center><?php echo utf8_encode($RFP['codigo_interno']); ?></center></td>
                                                        <td><input name='txtPD_<?php echo $i; ?>' id='txtPD_<?php echo $i; ?>' type='hidden' value='<?php echo $RFP['produto']; ?>'/><?php echo utf8_encode($RFP['produtodesc']); ?></td>
                                                        <td><input name='txtUn_<?php echo $i; ?>' id='txtUn_<?php echo $i; ?>' type='hidden' value='<?php echo $RFP['unidade_comercial']; ?>'/><center><?php echo $RFP['unidade_comercial']; ?></center></td>                                                                
                                                        <td style='text-align:right'><input name='txtVQ_<?php echo $i; ?>' id='txtVQ_<?php echo $i; ?>' type='hidden' value='<?php echo escreverNumero($RFP['quantidade'], 0, 4); ?>'/><?php echo escreverNumero($RFP['quantidade'], 0, 4); ?></td>
                                                        <td style='text-align:right'>
                                                            <input name='txtVU_<?php echo $i; ?>' id='txtVU_<?php echo $i; ?>' type='hidden' value='<?php
                                                            if ($RFP['quantidade'] > 0) {
                                                                $valor_unitario = $RFP['valor_total'] / $RFP['quantidade'];
                                                            } else {
                                                                $valor_unitario = 0;
                                                            }
                                                            echo escreverNumero($valor_unitario);
                                                            ?>'/><?php echo escreverNumero($valor_unitario); ?>
                                                        </td>
                                                        <td style='text-align:right'>
                                                            <input name='txtVP_<?php echo $i; ?>' id='txtVP_<?php echo $i; ?>' type='hidden' value='<?php
                                                            $totalp = $totalp + $RFP['valor_total'];
                                                            echo escreverNumero($RFP['valor_total']);
                                                            ?>'/><?php echo escreverNumero($RFP['valor_total']); ?>
                                                        </td>
                                                        <td><center><input <?php echo $disabled; ?> class='btn red' type='button' value='x' onClick="removeLinha('tabela_linha_<?php echo $i; ?>')" style="margin:0px; padding:2px 4px 3px 4px; line-height:10px;"></input></center></td>
                                                        </tr>
                                                        <?php
                                                        $i++;
                                                    }
                                                    $idTP = $i;
                                                } ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="height:20px">
                                                        <td colspan="8" style="width:100%; background-color:#f2f2f2; margin:0; padding:8px">
                                                            <div id="txtTotalProd" style="width:200px; float:right; text-align:right; line-height:20px">
                                                                Total sem Desconto: <?php echo escreverNumero($totalp, 1); ?><br>
                                                                Valor do Desconto: <?php echo escreverNumero(($totalp * ($descontop / 100)), 1); ?><br>
                                                                <b>Total com Desconto: <?php
                                                                    $TotalGeral = $TotalGeral + ($totalp - ($totalp * ($descontop / 100)));
                                                                    echo escreverNumero($totalp - ($totalp * ($descontop / 100)), 1);
                                                                    ?></b>
                                                            </div>
                                                            <div style="float:right">
                                                                <span style="width:100%; display:block"></span>
                                                                <span style="width:100%; display:block">
                                                                    <button class="btn btn-primary" id="bntProdDesconto" <?php echo $disabled; ?> type="button" style="width:35px; padding-bottom:3px; text-align:center"><span id="prod_sinal">%</span></button>
                                                                </span>
                                                            </div>
                                                            <div style="width:100px; float:right">
                                                                <span style="width:100%; display:block"> Desconto: </span>
                                                                <span style="width:100%; display:block">
                                                                    <input name="txtProdDesconto" id="txtProdDesconto" <?php echo $disabled; ?> type="text" class="input-xlarge" value="<?php echo $descontop; ?>" onblur="somaTotal()"/>
                                                                    <input name="txtProdSinal" id="txtProdSinal" value="<?php echo $descontotp; ?>" type="hidden"/>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </span>                                                                            
                                        <span style="width:100%; display:block; float:left; margin-top:4px">
                                            <div style="margin-top:10px" class="head dblue">
                                                <h2>Serviços</h2>
                                                <ul class="buttons"><li><div><a href="javascript:void(0)" onclick="AbrirBox(2)" title="Adicionar Produto"><div class="icon"><span class="ico-plus"></span></div></a></div></li></ul>
                                            </div>                                                    
                                            <table class="table" cellpadding="0" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th width="20%">Grupos de Serviços:</th>
                                                        <th width="45%">Serviços:</th>
                                                        <th width="5%">Qtd:</th>
                                                        <th width="12.5%">Valor Unitário:</th>
                                                        <th width="12.5%">Valor Total:</th>
                                                        <th width="5%">Ação:</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <select name="txtGrupoS" class="select" style="width:100%" id="txtGrupoS" <?php echo $disabled; ?> >
                                                                <option value="null">--Selecione--</option>
                                                                <?php
                                                                if ($disabled == "") {
                                                                    $cur = odbc_exec($con, "select id_contas_movimento,descricao from sf_contas_movimento where tipo = 'S' order by descricao") or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                                        ?>
                                                                        <option value="<?php echo $RFP['id_contas_movimento'] ?>"><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <span id="carregandoS" name="carregandoS" style="color:#666; display:none">Aguarde, carregando...</span>
                                                            <select name="txtContaS" class="select" style="width:100%" id="txtContaS"  <?php echo $disabled; ?> >
                                                                <option value="null">--Selecione--</option>
                                                                <?php
                                                                if ($disabled == "") {
                                                                    $sql = "select conta_produto,descricao from sf_produtos where conta_produto > 0 AND tipo = 'S' ORDER BY descricao";
                                                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                                        ?>
                                                                        <option value="<?php echo $RFP['conta_produto'] ?>"><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </td>
                                                        <td><input name="txtQtdProdutoS" id="txtQtdProdutoS" <?php echo $disabled; ?> type="text" class="input-xlarge inputCenter" value="1" onBlur="servValor()"/></td>
                                                        <td><input name="hidValorProdutoS" id="hidValorProdutoS" <?php echo $disabled; ?> type="text" class="input-xlarge inputCenter" value="<?php echo escreverNumero(0); ?>" onBlur="servValor()"/></td>
                                                        <td><input name="txtValorProdutoS" id="txtValorProdutoS" type="text" class="input-xlarge inputCenter" value="<?php echo escreverNumero(0); ?>" disabled /></td>
                                                        <td><center><button class="btn btn-primary" id="for-block3" <?php echo $disabled; ?> type="button" onClick="novaLinhaS()">+</button></center></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <table class="table" style="margin-top:-7px" cellpadding="0" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th width="20%"></th>
                                                        <th width="45%"></th>
                                                        <th width="5%"></th>
                                                        <th width="12.5%"></th>
                                                        <th width="12.5%"></th>
                                                        <th width="5%"></th>
                                                    </tr>
                                                </thead>                                                
                                                <tbody id="tabela_produtoS"> 
                                                    <?php
                                                    $j = 0;
                                                    $totalp = 0;
                                                    $idTS = 0;
                                                    if ($PegaURL != "") {
                                                        $cur = odbc_exec($con, "select id_item_venda,conta_movimento grupo,gc.descricao grupodesc,conta_produto produto,cm.descricao produtodesc,
																					quantidade,valor_total from sf_vendas_itens vi left join sf_contas_movimento gc on vi.grupo = gc.id_contas_movimento inner join sf_produtos cm 
																					on cm.conta_produto = vi.produto where cm.tipo = 'S' and id_venda = " . $PegaURL);
                                                        while ($RFP = odbc_fetch_array($cur)) {
                                                            ?>
                                                            <tr id='tabela_linhaS_<?php echo $j; ?>'>
                                                                <td><input name='txtIPS_<?php echo $j; ?>' id='txtIPS_<?php echo $j; ?>' type='hidden' value='<?php echo $RFP['id_item_venda']; ?>'/>
                                                                    <input name='txtGRS_<?php echo $j; ?>' id='txtGRS_<?php echo $j; ?>' type='hidden' value='<?php echo $RFP['grupo']; ?>'/>
                                                                    <?php echo utf8_encode($RFP['grupodesc']); ?>
                                                                </td>
                                                                <td>
                                                                    <input name='txtPDS_<?php echo $j; ?>' id='txtPDS_<?php echo $j; ?>' type='hidden' value='<?php echo $RFP['produto']; ?>'/>
                                                                    <?php echo utf8_encode($RFP['produtodesc']); ?>
                                                                </td>
                                                                <td><input name='txtVQS_<?php echo $j; ?>' id='txtVQS_<?php echo $j; ?>' type='hidden' value='<?php echo escreverNumero($RFP['quantidade'], 0, 4); ?>'/><center><?php echo escreverNumero($RFP['quantidade'], 0, 4); ?></center></td>
                                                        <td style='text-align: right;'>
                                                            <input name='txtVUS_<?php echo $i; ?>' id='txtVUS_<?php echo $i; ?>' type='hidden' value='<?php
                                                            if ($RFP['quantidade'] > 0) {
                                                                $valor_unitario = $RFP['valor_total'] / $RFP['quantidade'];
                                                            } else {
                                                                $valor_unitario = 0;
                                                            }
                                                            echo escreverNumero($valor_unitario);
                                                            ?>'/><?php echo escreverNumero($valor_unitario); ?>
                                                        </td>
                                                        <td style='text-align: right;'>
                                                            <input name='txtVPS_<?php echo $j; ?>' id='txtVPS_<?php echo $j; ?>' type='hidden' value='<?php
                                                            $totalp = $totalp + $RFP['valor_total'];
                                                            echo escreverNumero($RFP['valor_total']);
                                                            ?>'/><?php echo escreverNumero($RFP['valor_total']); ?>
                                                        </td>
                                                        <td><center><input <?php echo $disabled; ?> class='btn red' type='button' value='x' onClick="javascript:removeLinha('tabela_linhaS_<?php echo $j; ?>')" style="margin:0px; padding:2px 4px 3px 4px; line-height:10px"></input></center></td>
                                                        </tr>
                                                        <?php
                                                        $j++;
                                                    }
                                                    $idTS = $j;
                                                }
                                                ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="height:20px">
                                                        <td colspan="6" style="width:100%; background-color:#f2f2f2; margin:0; padding:8px">
                                                            <div id="txtTotalServ" style="width:200px; float:right; text-align:right; line-height:20px">
                                                                Total sem Desconto: <?php echo escreverNumero($totalp, 1); ?><br>
                                                                Valor do Desconto: <?php echo escreverNumero(($totalp * ($descontos / 100)), 1); ?><br>
                                                                <b>Total com Desconto: <?php
                                                                    $TotalGeral = $TotalGeral + ($totalp - ($totalp * ($descontos / 100)));
                                                                    echo escreverNumero($totalp - ($totalp * ($descontos / 100)), 1);
                                                                    ?></b>
                                                            </div>
                                                            <div style="float:right">
                                                                <span style="width:100%; display:block"></span>
                                                                <span style="width:100%; display:block">
                                                                    <button class="btn btn-primary" id="bntServDesconto" <?php echo $disabled; ?> type="button" style="width:35px; padding-bottom:3px; text-align:center"><span id="serv_sinal">%</span></button>
                                                                </span>
                                                            </div>
                                                            <div style="width:300px; float:left; vertical-align:bottom">
                                                                <input id="ckbNCV" name="ckbNCV" type="checkbox" <?php echo $disabled; ?> onClick="somaTotal()" value="1" <?php
                                                                if ($n_servicos == 1) {
                                                                    echo "CHECKED";
                                                                }
                                                                ?>>
                                                                Não contabilizar valores de Serviços
                                                            </div>
                                                            <div style="width:100px; float:right">
                                                                <span style="width:100%; display:block"> Desconto: </span>
                                                                <span style="width:100%; display:block">
                                                                    <input name="txtServDesconto" id="txtServDesconto" <?php echo $disabled; ?> type="text" class="input-xlarge" value="<?php echo $descontos; ?>" onblur="somaTotal()"/>
                                                                    <input name="txtServSinal" id="txtServSinal" value="<?php echo $descontots; ?>" type="hidden"/>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>                                                                                     
                                            <div class="head dblue" style="margin-top:10px; border-radius:0">
                                                <h2>Total Geral</h2>
                                                <ul class="buttons"><h2><div id="totGeral"><?php echo escreverNumero($TotalGeral, 1); ?></div></h2></ul>
                                            </div>                                              
                                        </span>
                                        <span style="display:none; background:none; border:0px; margin-bottom:5px" id="block3" class="linha"> 
                                            <span style="width:19%; display:block; float:left; margin-right:1%"> 
                                                <span style="width:100%; display:block">Forma de Pagto: <br>
                                                    <span id="spryselect5">
                                                        <select class="select" style="width:100%" name="txtTipo" id="txtTipo" <?php echo $disabled; ?>>
                                                            <option value="null">--Selecione--</option>
                                                            <?php
                                                            $where_bloq = "";
                                                            if ($disabled == "disabled") {
                                                                $where_bloq = " and id_tipo_documento  = '" . $tipo . "'";
                                                            }
                                                            $cur = odbc_exec($con, "select id_tipo_documento,descricao from sf_tipo_documento where s_tipo in ('A','C') " . $where_bloq . " order by descricao") or die(odbc_errormsg());
                                                            while ($RFP = odbc_fetch_array($cur)) {
                                                                ?>
                                                                <option value="<?php echo $RFP['id_tipo_documento'] ?>"<?php
                                                                if (!(strcmp($RFP['id_tipo_documento'], $tipo))) {
                                                                    echo "SELECTED";
                                                                }
                                                                ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                                                    <?php } ?>
                                                        </select>
                                                        <span class="selectRequiredMsg">Selecione um item.</span>
                                                    </span>
                                                </span>
                                            </span>                                            
                                            <span style="width:18%; display:block; float:left"> 
                                                <span style="width:100%; display:block"> Documento Inicial: </span>
                                                <span style="width:100%; display:block">
                                                    <input class="input-xlarge inputCenter" name="txtParcelaMakeDC" id="txtParcelaMakeDC" <?php echo $disabled; ?> type="text" value="<?php echo $r_documento; ?>"/>
                                                </span> 
                                            </span> 
                                            <span style="width:10%; display:block; float:left; margin-left:1%"> 
                                                <span style="width:100%; display:block"> Vencimento Inicial: </span>
                                                <span style="width:100%; display:block">
                                                    <input class="datepicker input-xlarge inputCenter" name="txtParcelaMakeD" id="txtParcelaMakeD" <?php echo $disabled; ?> type="text" value="<?php echo $r_ven_ini; ?>"/>
                                                    <span class="textfieldInvalidFormatMsg">Formato inválido.</span>                                                
                                                </span>                                                
                                            </span>
                                            <span style="width:10%; display:block; float:left; margin-left:1%"> 
                                                <span style="width:100%; display:block"> Qtd. de Parcelas: </span>
                                                <span style="width:100%; display:block">
                                                    <input name="txtParcelaMakeQ" onblur="calculaParcela();" type="text" class="input-xlarge" id="spinner" value="<?php echo $r_quantidade; ?>" maxlength="2" <?php echo $disabled; ?>/>
                                                </span> 
                                            </span>                                            
                                            <span style="width:15%; display:block; float:left; margin-left:1%">
                                                <span style="width:100%; display:block"> Valor Total: </span>
                                                <span style="width:100%; display:block">
                                                    <input name="txtParcelaMakeT" onblur="checaValor(); calculaParcela();" id="txtParcelaMakeT" <?php echo $disabled; ?> type="text" class="input-xlarge inputCenter" value="<?php echo $r_valor_tot; ?>"/>
                                                </span> 
                                            </span> 
                                            <span style="width:15%; display:block; float:left; margin-left:1%">
                                                <span style="width:100%; display:block"> Valor da Parcela: </span>
                                                <span style="width:100%; display:block">
                                                    <input name="txtParcelaMakeV" id="txtParcelaMakeV" <?php echo $disabled; ?> type="text" class="input-xlarge inputCenter" value="<?php echo $r_valor; ?>"/>
                                                </span> 
                                            </span>                                            
                                            <span style="width:7%; display:block; float:left; margin-left:1%">
                                                <span style="width:100%; display:block"></span>
                                                <span style="width:100%; display:block; text-align:right">	
                                                    <button class="btn btn-success" <?php echo $disabled; ?> name="bntMkParcelas" id="bntMkParcelas" type="button" onClick="Preencher()" style="width:100%;">Gerar</button>
                                                </span> 
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span12" style="width:100%">
                                    <div class="block">
                                        <div class="spanBNT3" style="margin-left:10px">
                                            <div class="toolbar bottom tar">
                                                <div class="data-fluid">
                                                    <div class="btn-group">
                                                        <?php if ($id == "" || $disabled == "") { ?>
                                                            <button class="btn btn-success" type="submit" name="bntSave" id="bntOK"><span class="ico-checkmark icon-white"></span> Gravar</button>
                                                            <button class="btn btn-success" onClick="history.go(-1)" id="bntOK"><span class="ico-reply icon-white"></span> Cancelar</button>
                                                        <?php } else { ?>
                                                            <button class="btn btn-success" type="submit" name="bntEdit" id="bntOK" title="Alterar" value="Alterar"> <span class="icon-edit icon-white"></span> Editar</button>
                                                            <button class="btn red" type="submit" name="bntDelete" id="bntOK" title="Excluir" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="icon-remove icon-white"></span> Deletar</button>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input name="txtIdTP" id="txtIdTP" value="<?php echo $idTP; ?>" type="hidden"/>
                            <input name="txtIdTS" id="txtIdTS" value="<?php echo $idTS; ?>" type="hidden"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="dialog" id="source" title="Source"></div>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>                
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/moment.min.js"></script>    
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>    
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script language="javascript">
        
        $("#txtDtVenda, #txtParcelaMakeD").mask(lang["dateMask"]);        
        $("#txtParcelaMakeDC").mask("9999999999", {placeholder: ""});        
        $("#hidValorProduto, #txtValorProduto, #txtProdDescontoVal, #hidValorProdutoS,#txtValorProdutoS, #txtServDescontoVal, #txtParcelaMakeV, #txtParcelaMakeT, #txtValorFrete").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
        
        function refreshMask() {
            $(".mdate").mask(lang["dateMask"]);
            $(".parce").mask("9999999999", {placeholder: ""});
            $(".price").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});
        }        
        
        $(document).on('keypress', 'input', function (e) {
            if (e.keyCode === 13 && e.target.type !== 'submit') {
                e.preventDefault();
                var inputs = $(this).parents("form").eq(0).find(":input:visible:not(disabled):not([readonly])"),
                        idx = inputs.index(this);
                if (idx === inputs.length - 1) {
                    inputs[0].select();
                } else {
                    inputs[idx + 1].focus();
                }
                return false;
            }
        });  
        
        function reLoad() {
            $('#txtFonecedor').hide();
            $('#carregando3').show();
            $.getJSON('./../Contas-a-pagar/fornecedor.ajax.php?search=', {txtTipo: 'C', ajax: 'true'}, function (j) {
                var options = '<option value="" >--Selecione--</option>';
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].id_grupo_contas + '">' + j[i].descricao + '</option>';
                }
                $('#txtFonecedor').html(options).show();
                $('#txtFonecedor').css("display", "none");
                $('#carregando3').hide();
            });
        }
        
        function clearT1() {
            $("#block3").toggle("slow");
        }
        function preencheZeros(valor, valor2, tamanho) {
            var qtd = parseInt(valor) + parseInt(valor2);
            if (qtd.toString().length < tamanho.length) {
                var limite = tamanho.length - qtd.toString().length;
                for (i = 0; i < limite; i++) {
                    qtd = '0' + qtd;
                }
            }
            return qtd;
        }        
        
        function calculaParcela() {
            if ($("#txtParcelaMakeT").val() !== '') {
                var qtde = parseInt($("#spinner").val());
                var valTotal = textToNumber($("#txtParcelaMakeT").val());
                if (qtde > 0 && valTotal > 0) {
                    $("#txtParcelaMakeV").val(numberFormat(valTotal / qtde));
                } else {
                    $("#txtParcelaMakeV").val(numberFormat(0));
                }
            }       
        }
        
        $("#frmEnviaDados").validate({
            errorPlacement: function (error, element) {
                return true;
            },
            rules: {
                txtHistorico: {required: true},
                txtDtVenda: {required: true},
                txtAutorizacao: {required: true}
            }
        });
        
        function Preencher() {
            var val = textToNumber($("#txtParcelaMakeV").val());
            var qtde = textToNumber($("#spinner").val());
            var valorx = textToNumber($("#txtParcelaMakeDC").val());
            var total = 0;       
            if (qtde >= 0 && val >= 0 && moment($("#txtParcelaMakeD").val(), lang["dmy"], true).isValid()) {
                for (var i = $('#tbParcelas >tbody >tr').length; i > qtde; i--) {
                    if ($("#txtPG_" + (i)).val() !== 'S') {
                        $("#linhaParcela_" + i).remove();
                    } else {
                        bootbox.alert("Número de parcelas inválido!");
                        return false;
                    }
                }            
                for (var i = 0; i < 60; i++) {
                    if (i < qtde) {
                        if ($("#txtPG_" + (i + 1)).val() !== 'S') {
                            total += textToNumber($("#txtParcelaMakeV").val());
                            if ($("#txtParcelaMakeDC").val() !== '') {
                                $("#txtParcelaDC_" + (i + 1)).val(preencheZeros(valorx, i, $("#txtParcelaMakeDC").val())); 
                            } else {
                                $("#txtParcelaDC_" + (i + 1)).val(""); 
                            }
                            var now = moment($("#txtParcelaMakeD").val(), lang["dmy"], true);                                                                 
                            $("#txtParcelaD_" + (i + 1)).val(now.add(i, 'M').format(lang["dmy"]));
                            $("#txtParcelaV_" + (i + 1)).val($("#txtParcelaMakeV").val());
                            $("#txtTipo_" + (i + 1)).val($("#txtTipo").val());
                        } else {
                            total += textToNumber($("#txtParcelaV_" + (i + 1)).val());
                        }
                    } else if ($("#txtPG_" + (i + 1)).val() !== 'S') {
                        $("#txtParcelaDC_" + (i + 1)).val("");
                        $("#txtParcelaD_" + (i + 1)).val("");
                        $("#txtParcelaV_" + (i + 1)).val(numberFormat(0));
                    }
                }
                $('#lblTotParc').hide();
                $('#lblTotParc').html('<tr><td>Total em parcelas: <strong>' + numberFormat(total, 1) + '</strong></td></tr>').show("slow");
            }
        }
        
        var conta = $("#txtIdTP").val();
        var contaS = $("#txtIdTS").val();
        
        function novaLinha() {
            var qtpX = textToNumber($("#txtQtdProduto").val());            
            if ($("#txtConta").val() !== "" && (qtpX > 0) && $("#txtValorProduto").val() !== numberFormat(0)) {
                var valTotal = textToNumber($("#txtValorProduto").val());
                if (valTotal > 0.00) {        
                    var grp = $("#txtGrupo option:selected").text();
                    var prp = $("#txtConta option:selected").text();
                    var grc = $("#txtGrupo").val();
                    var qtp = $("#txtQtdProduto").val();
                    var prc = $("#txtConta").val();
                    var und = $("#txtUnidade").val();
                    var vlu = $("#hidValorProduto").val();
                    var vlp = $("#txtValorProduto").val();
                    var chg = 0;                                                          
                    for (var i = 0; i < conta; i++) {
                        if ($("#txtPD_" + i).val() !== "null") {
                            var prv = $("#txtPD_" + i).val();
                            var qtv = $("#txtVQ_" + i).val();
                            var vlv = textToNumber($("#txtVP_" + i).val());
                            if (prc === prv) {
                                qtv = parseInt(qtp);
                                $("#myVQ_" + i).html("<input name='txtVQ_" + i + "' id='txtVQ_" + i + "' type='hidden' value='" + qtv + "'/><center>" + qtv + "</center>");
                                $("#myVP_" + i).html("<input name='txtVP_" + i + "' id='txtVP_" + i + "' type='hidden' value='" + vlv + "'/>" + vlv + "");
                                chg = 1;
                            }
                        }
                    } 
                    if (chg === 0) {
                        var prpx = prp.split('[');
                        var linha = "<td><input name='txtIP_" + conta + "' id='txtIP_" + conta + "' type='hidden' value=''/><input name='txtGR_" + conta + "' id='txtGR_" + conta + "' type='hidden' value='" + grc + "'/>" + grp + "</td>" +
                        "<td>" + (typeof prpx[1] !== "undefined" ? "<center>" + prpx[1].replace("]", "") + "</center>" : "") + "</td>" +
                        "<td><input name='txtPD_" + conta + "' id='txtPD_" + conta + "' type='hidden' value='" + prc + "'/><a style='cursor: pointer;' onclick='carregaItemP(" + conta + ")'>" + prpx[0] + "</a></td>" +
                        "<td id='myUn_" + conta + "'><input name='txtUn_" + conta + "' id='txtUn_" + conta + "' type='hidden' value='" + und + "'/><center>" + und + "</center></td>" +
                        "<td id='myVQ_" + conta + "'><input name='txtVQ_" + conta + "' id='txtVQ_" + conta + "' type='hidden' value='" + qtp + "'/><center>" + qtp + "</center></td>" +
                        "<td id='myVU_" + conta + "' style='text-align: right;'><input name='txtVU_" + conta + "' id='txtVU_" + conta + "' type='hidden' value='" + vlu + "'/>" + vlu + "</td>" +
                        "<td id='myVP_" + conta + "' style='text-align: right;'><input name='txtVP_" + conta + "' id='txtVP_" + conta + "' type='hidden' value='" + vlp + "'/>" + vlp + "</td>" +
                        "<td><center><input class='btn red' type='button' value='x' onClick=\"javascript:removeLinha('tabela_linha_" + conta + "')\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\"></input></center></td>";
                        $("#tabela_produto").append("<tr id='tabela_linha_" + conta + "'>" + linha + "</tr>");                        
                        conta++;
                    }
                    somaTotal();
                }
            } else {
                bootbox.alert("Todos os campos devem ser preenchidos.");
            }
        }
        
        function novaLinhaS() {
            if ($("#txtContaS").val() !== "" && $("#txtValorProdutoS").val() !== numberFormat(0) && $("#txtQtdProdutoS").val() > 0) {
                var valTotal = textToNumber($("#txtValorProdutoS").val());
                if (valTotal > 0.00) {
                    var grp = $("#txtGrupoS option:selected").text();
                    var prp = $("#txtContaS option:selected").text();
                    var grc = $("#txtGrupoS").val();
                    var prc = $("#txtContaS").val();                    
                    var qtp = $("#txtQtdProdutoS").val();                    
                    var vlu = $("#hidValorProdutoS").val();                    
                    var vlp = $("#txtValorProdutoS").val();    
                    var chg = 0;
                    for (var i = 0; i < contaS; i++) {
                        if ($("#txtPDS_" + i).val() !== "null") {
                            var prv = $("#txtPDS_" + i).val();
                            var qtv = $("#txtVQS_" + i).val();
                            var vlv = textToNumber($("#txtVPS_" + i).val());
                            if (prc === prv) {
                                qtv = parseInt(qtp);
                                $("#myVQS_" + i).html("<input name='txtVQS_" + i + "' id='txtVQS_" + i + "' type='hidden' value='" + qtv + "'/><center>" + qtv + "</center>");
                                $("#myVPS_" + i).html("<input name='txtVPS_" + i + "' id='txtVPS_" + i + "' type='hidden' value='" + vlv + "'/>" + vlv + "");
                                chg = 1;
                            }
                        }
                    } 
                    if (chg === 0) {
                        var linha = "<td><input name='txtIPS_" + contaS + "' id='txtIPS_" + contaS + "' type='hidden' value=''/><input name='txtGRS_" + contaS + "' id='txtGRS_" + contaS + "' type='hidden' value='" + grc + "'/>" + grp + "</td>" +
                        "<td><input name='txtPDS_" + contaS + "' id='txtPDS_" + contaS + "' type='hidden' value='" + prc + "' <a style='cursor: pointer;' onclick='carregaItemS(" + contaS + ")'>" + prp + "</a></td>" +
                        "<td id='myVQS_" + contaS + "'><input name='txtVQS_" + contaS + "' id='txtVQS_" + contaS + "' type='hidden' value='" + qtp + "'/><center>" + qtp + "</center></td>" +
                        "<td id='myVUS_" + contaS + "' style='text-align: right;'><input name='txtVUS_" + contaS + "' id='txtVUS_" + contaS + "' type='hidden' value='" + vlu + "'/>" + vlu + "</td>" +
                        "<td id='myVPS_" + contaS + "' style='text-align: right;'><input name='txtVPS_" + contaS + "' id='txtVPS_" + contaS + "' type='hidden' value='" + vlp + "'/>" + vlp + "</td>" +
                        "<td><center><input class='btn red' type='button' value='x' onClick=\"javascript:removeLinha('tabela_linhaS_" + contaS + "')\" style=\"margin:0px; padding:2px 4px 3px 4px; line-height:10px;\"></input></center></td>";                        
                        $("#tabela_produtoS").append("<tr id='tabela_linhaS_" + contaS + "'>" + linha + "</tr>");
                        contaS++;
                    }
                    somaTotal();
                }
            } else {
                bootbox.alert("Todos os campos devem ser preenchidos.");
            }
        }
        
        function removeLinha(id) {
            $("#" + id).remove();
            somaTotal();
        }
        
        function somaTotal() {
            var totalVendas = 0;
            var totalServicos = 0;            
            for (var i = 0; i < conta; i++) {
                if ($("#txtPD_" + i).val() !== "null") {
                    totalVendas = totalVendas + textToNumber($("#txtVP_" + i).val());
                }
            }
            for (var i = 0; i < contaS; i++) {
                if ($("#txtPDS_" + i).val() !== "null") {
                    totalServicos = totalServicos + textToNumber($("#txtVPS_" + i).val());
                }
            }
            var prod_desc = textToNumber($("#txtProdDesconto").val());
            if ($("#prod_sinal").html() === "%") {
                prod_desc = totalVendas * (prod_desc / 100);
            }
            $("#txtTotalProd").html("Total sem Desconto: " + numberFormat(totalVendas, 1) + "<br>Valor do Desconto: " + numberFormat(prod_desc, 1) + "<br><b>Total com Desconto: " + numberFormat((totalVendas - prod_desc), 1) + "</b>");            
            var serv_desc = textToNumber($("#txtServDesconto").val());
            if ($("#serv_sinal").html() === "%") {
                serv_desc = totalServicos * (serv_desc / 100);
            }
            $("#txtTotalServ").html("Total sem Desconto: " + numberFormat(totalServicos, 1) + "<br>Valor do Desconto: " + numberFormat(serv_desc, 1) + "<br><b>Total com Desconto: " + numberFormat((totalServicos - serv_desc), 1) + "</b>");            
            if ($("#ckbNCV").is(":checked") === false) {
                $("#txtParcelaMakeT").val(numberFormat((totalVendas - prod_desc) + (totalServicos - serv_desc)));
            } else {
                $("#txtParcelaMakeT").val(numberFormat((totalVendas - prod_desc)));
            }
            $("#totGeral").html(numberFormat((totalVendas - prod_desc) + (totalServicos - serv_desc)), 1);
            $("#txtIdTP").val(conta);
            $("#txtIdTS").val(contaS);
        }
        
        function checaValor() {
            var totalVendas = 0;
            var totalServicos = 0;
            for (var i = 0; i < conta; i++) {
                if ($("#txtPD_" + i).val() !== "null") {
                    totalVendas = totalVendas + textToNumber($("#txtVP_" + i).val());
                }
            }
            if ($("#ckbNCV").is(":checked") === false) {            
                for (var i = 0; i < contaS; i++) {
                    if ($("#txtPDS_" + i).val() !== "null") {
                        totalServicos = totalServicos + textToNumber($("#txtVPS_" + i).val());
                    }
                }         
            }
            if ($("#ckbNCV").is(":checked") === false && (totalVendas + totalServicos) > textToNumber($("#txtParcelaMakeT").val())) {
                somaTotal();
            } else if (totalVendas > textToNumber($("#txtParcelaMakeT").val())) {
                somaTotal();
            }
        }
        
        function MakeDateFormPG(forma) {
            $.getJSON('../Estoque/tipo.documento.ajax.php?search=', {txtCampos: forma, ajax: 'true'}, function (j) {
                for (var i = 0; i < j.length; i++) {
                    if (j[i].dias > 0) {
                        var dias = j[i].dias;
                        var aDate = moment($("#txtDtVenda").val(), lang["dmy"], true);                
                        $("#txtParcelaMakeD").val(aDate.add(dias, 'days').format(lang["dmy"]));                                                
                    } else {
                        $("#txtParcelaMakeD").val($("#txtDtVenda").val());
                    }
                }
            });
        }
        
        function prodValor() {            
            var UpdQtd = textToNumber($("#txtQtdProduto").val());
            var UpdVal = textToNumber($("#hidValorProduto").val());
            $("#txtValorProduto").val(numberFormat((UpdVal * UpdQtd)));
        }
        
        function servValor() {            
            var UpdQtd = textToNumber($("#txtQtdProdutoS").val());
            var UpdVal = textToNumber($("#hidValorProdutoS").val());
            $("#txtValorProdutoS").val(numberFormat((UpdVal * UpdQtd)));
        }
        
        function AbrirBox(opc) {
            if (opc === 1) {
                $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:717px; height:563px; position:absolute; top:50%; left:50%; margin-left:-358px; margin-top:-282px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:563px' src='./../Estoque/FormProdutos.php?idx=0'></iframe></div></div></div></div></div>");
            }
            if (opc === 2) {
                $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:667px; height:390px; position:absolute; top:50%; left:50%; margin-left:-332px; margin-top:-114px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:390px' src='./../Estoque/FormServicos.php?idx=0'></iframe></div></div></div></div></div>");
            }
            if (opc === 3) {
                $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:717px; height:563px; position:absolute; top:50%; left:50%; margin-left:-358px; margin-top:-282px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:563px' src='./../Comercial/FormGerenciador-Clientes.php?idx=0'></iframe></div></div></div></div></div>");
            }
        }
        
        function FecharBox(opc) {
            $("#newbox").remove();
            if (opc === 1) {
                $("#txtGrupo").trigger("change");
                $("#s2id_txtConta > .select2-choice > span").html("--Selecione--");
            }
            if (opc === 2) {
                $("#txtGrupoS").trigger("change");
                $("#s2id_txtContaS > .select2-choice > span").html("--Selecione--");
            }
            if (opc === 3) {
                reLoad();
            }
        }
        
        $(document).ready(function () {
            
            somaTotal();           
            
            if ($("#txtProdSinal").val() === 1) {
                $("#prod_sinal").html("$");
            }
            
            if ($("#txtServSinal").val() === 1) {
                $("#serv_sinal").html("$");
            }
            
            $('#txtFonecedor').change(function () {
                if ($(this).val()) {
                    $('#carregando4').show();
                    $('#carregando5').show();
                    $.getJSON('endereco.ajax.php?search=', {txtFonecedor: $(this).val(), ajax: 'true'}, function (j) {
                        var optionsDados = '';
                        var options = '';
                        for (var i = 0; i < j.length; i++) {
                            if (j[i].endereco !== '') {
                                options += j[i].endereco;
                            }
                            if (j[i].numero !== '') {
                                options += ' N° ' + j[i].numero;
                            }
                            if (j[i].complemento !== '') {
                                options += ' ' + j[i].complemento;
                            }
                            if (j[i].bairro !== '') {
                                options += ',' + j[i].bairro;
                            }
                            if (j[i].cidade_nome !== '') {
                                options += '\n' + j[i].cidade_nome;
                            }
                            if (j[i].estado_sigla !== '') {
                                options += '-' + j[i].estado_sigla;
                            }
                            if (j[i].cep !== '') {
                                options += ', Cep: ' + j[i].cep;
                            }
                            if (j[i].cnpj !== '') {
                                optionsDados += 'CNPJ/CPF:' + j[i].cnpj;
                            }
                            if (j[i].telefone !== '') {
                                optionsDados += '\nTel:' + j[i].telefone;
                            }
                            if (j[i].email !== '') {
                                optionsDados += ' E-mail:' + j[i].email;
                            }
                        }
                        $('#txtDados').html(optionsDados).show();
                        $('#txtEntrega').html(options).show();
                        $('#carregando4').hide();
                        $('#carregando5').hide();
                    });
                } else {
                    $('#txtDados').html('');
                    $('#txtEntrega').html('');
                }
            });
            
            $('#txtConta').change(function () {
                if ($(this).val()) {
                    $.getJSON('conta_a.ajax.php?search=', {txtConta: $(this).val(), ajax: 'true'}, function (j) {
                        var grupo = 'null';
                        var preco = numberFormat(0);
                        var qtdcom = '1';
                        var unidade = '';
                        for (var i = 0; i < j.length; i++) {
                            if (j[i].grupo_conta !== '') {
                                grupo = j[i].grupo_conta;
                            }
                            if (j[i].preco_venda !== '') {
                                preco = j[i].preco_venda;
                            }
                            if (j[i].unidade_comercial !== '') {
                                unidade = j[i].unidade_comercial;
                            }
                            if (j[i].quantidade_comercial !== '') {
                                if (parseFloat(j[i].quantidade_comercial) > 0) {
                                    qtdcom = parseInt(j[i].quantidade_comercial);
                                } else {
                                    qtdcom = 1;
                                }
                            }
                        }
                        $('#txtGrupo').find('option[value="' + grupo + '"]').attr('selected', true);
                        $('#txtUnidade').val(unidade).show();
                        $('#txtQtdProduto').val(qtdcom).show();
                        $('#txtValorProduto').val(numberFormat(preco)).show();
                        $('#hidValorProduto').val(numberFormat(preco)).show();
                    });
                } else {
                    $('#txtUnidade').html('');
                    $('#txtQtdProduto').html('1');
                    $('#txtValorProduto').html(numberFormat(0));
                    $('#hidValorProduto').html(numberFormat(0));
                }
            });

            $('#txtContaS').change(function () {
                if ($(this).val()) {
                    $.getJSON('conta_a.ajax.php?search=', {txtContaS: $(this).val(), ajax: 'true'}, function (j) {
                        var grupo = 'null';
                        var preco = numberFormat(0);
                        var qtdcom = '1';
                        for (var i = 0; i < j.length; i++) {
                            if (j[i].grupo_conta !== '') {
                                grupo = j[i].grupo_conta;
                            }
                            if (j[i].preco_venda !== '') {
                                preco = j[i].preco_venda;
                            }
                            if (j[i].quantidade_comercial !== '') {
                                if (parseFloat(j[i].quantidade_comercial) > 0) {
                                    qtdcom = parseInt(j[i].quantidade_comercial);
                                } else {
                                    qtdcom = 1;
                                }
                            }
                        }
                        $('#txtGrupoS').find('option[value="' + grupo + '"]').attr('selected', true);
                        $('#txtQtdProdutoS').val(qtdcom).show();
                        $('#txtValorProdutoS').val(numberFormat(preco)).show();
                        $('#hidValorProdutoS').val(numberFormat(preco)).show();
                    });
                } else {
                    $('#txtQtdProdutoS').html('1');
                    $('#txtValorProdutoS').html(numberFormat(0));
                    $('#hidValorProdutoS').html(numberFormat(0));
                }
            });
            
            $('#txtTipo').change(function () {
                if ($(this).val()) {
                    $('#tblTable').hide();
                    $.getJSON('campos.ajax.php?search=', {txtCampos: $(this).val(), ajax: 'true'}, function (j) {
                        var options = '<thead><tr><th width="29%">Descrição:</th><th width="71%">Conteúdo:</th></tr></thead><tbody>';
                        for (var i = 0; i < j.length; i++) {
                            options += '<tr><td>' + j[i].campo + '</td><td><input name="txtCampo' + j[i].id_tipo_documento_campos + '" id="txtCampo' + j[i].id_tipo_documento_campos + '" ' + 
                            ($("#txtDtVenda").attr("disabled") === "disabled" ? "disabled" : "") + ' type="text" class="input-xlarge" value=""/></td></tr>';
                        }
                        options += '</tbody>';
                        MakeDateFormPG($('#txtTipo').val());
                        $('#tblTable').html(options).show("slow");
                    });
                } else {
                    $('#txtTipo').html('<thead><tr><th width="29%">Descrição:</th><th width="71%">Conteúdo:</th></tr></thead><tbody><tr><td></tr></td></tbody>');
                }
            });
            
            $('#txtGrupo').change(function () {
                if ($(this).val()) {
                    $('#txtConta').hide();
                    $('#carregando').show();
                    $.getJSON('conta_p.ajax.php?search=', {txtGrupo: $(this).val(), ajax: 'true'}, function (j) {
                        var options = '<option value="null">--Selecione--</option>';
                        for (var i = 0; i < j.length; i++) {
                            options += '<option value="' + j[i].id_contas_movimento + '">' + j[i].descricao + '</option>';
                        }
                        $('#txtConta').html(options).show();
                        $('#txtConta').css("display", "none");
                        $('#carregando').hide();
                    });
                } else {
                    $('#txtConta').html('<option value="">--Selecione--</option>');
                }
            });
            
            $('#txtGrupoS').change(function () {
                if ($(this).val()) {
                    $('#txtContaS').hide();
                    $('#carregandoS').show();
                    $.getJSON('conta_s.ajax.php?search=', {txtGrupo: $(this).val(), ajax: 'true'}, function (j) {
                        var options = '<option value="">--Selecione--</option>';
                        for (var i = 0; i < j.length; i++) {
                            options += '<option value="' + j[i].id_contas_movimento + '">' + j[i].descricao + '</option>';
                        }
                        $('#txtContaS').html(options).show();
                        $('#txtContaS').css("display", "none");
                        $('#carregandoS').hide();
                    });
                } else {
                    $('#txtContaS').html('<option value="">--Selecione--</option>');
                }
            });
            
            $("#bntProdDesconto").click(function () {
                if ($("#prod_sinal").html() === "%") {
                    $("#prod_sinal").html("$");
                    $("#txtProdSinal").val("1");
                } else {
                    $("#prod_sinal").html("%");
                    $("#txtProdSinal").val("0");
                }
                somaTotal();
            });
            
            $("#bntServDesconto").click(function () {
                if ($("#serv_sinal").html() === "%") {
                    $("#serv_sinal").html("$");
                    $("#txtServSinal").val("1");
                } else {
                    $("#serv_sinal").html("%");
                    $("#txtServSinal").val("0");
                }
                somaTotal();
            });                 
        });         
        
    </script>
</body>
<?php odbc_close($con); ?>
</html>