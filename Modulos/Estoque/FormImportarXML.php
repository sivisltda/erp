<?php
include "../../Connections/configini.php";
$filename = "";
$nome = "";
$pasta = "./../../Pessoas/" . $contrato . "/NFE_COMPRAS";
$fornecedor = 0;
$produtos = "'0'";
$quantidades = "'0'";
$valores = "'0'";
$grupos = "'0'";
$unidades = "'0'";
$total = 0;
$totalImpostos = 0;
$formaPreco = 0;
$margem_lucro = 0;

if (isset($_POST['txtNome'])) {
    $filename = $_POST['txtNome'];
}

if (isset($_POST['bntLer'])) {
    $nome = $_FILES['arquivo']['name'];
    $type = $_FILES['arquivo']['type'];
    $size = $_FILES['arquivo']['size'];
    $tmp = $_FILES['arquivo']['tmp_name'];
    if ($tmp) {
        move_uploaded_file($tmp, $pasta . "/" . $nome);
        $filename = $pasta . "/" . $nome;
    }
}

if (isset($_POST['bntSave'])) {
    if (file_exists($filename)) {
        $xml = simplexml_load_file($filename);       
        $where_bloq = " and cnpj = '" . substr($xml->NFe->infNFe->emit->CNPJ, 0, 2) . "." . substr($xml->NFe->infNFe->emit->CNPJ, 2, 3) . "." . substr($xml->NFe->infNFe->emit->CNPJ, 5, 3) . "/" . substr($xml->NFe->infNFe->emit->CNPJ, 8, 4) . "-" . substr($xml->NFe->infNFe->emit->CNPJ, 12, 2) . "' ";
        $cur = odbc_exec($con, "select id_fornecedores_despesas,razao_social from sf_fornecedores_despesas where tipo = 'F' " . $where_bloq . " order by razao_social") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $fornecedor = $RFP['id_fornecedores_despesas'];
        }
        //Verificar o Fornecedor 
        if ($fornecedor == 0) {
            $estado = "null";
            $cidade = "null";
            $cur = odbc_exec($con, "select * from tb_cidades inner join tb_estados on tb_cidades.cidade_codigoEstado = tb_estados.estado_codigo
                where estado_sigla = '" . $xml->NFe->infNFe->emit->enderEmit->UF . "' and cidade_nome = '" . $xml->NFe->infNFe->emit->enderEmit->xMun . "'") or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                $estado = $RFP['estado_codigo'];
                $cidade = $RFP['cidade_codigo'];
            }
            $query = "insert into sf_fornecedores_despesas(razao_social,cnpj,inscricao_estadual,
                endereco,numero,complemento,bairro,estado,cidade,cep,empresa,inativo,tipo,dt_cadastro,contato,nome_fantasia)values (" .
                    valoresTexto2($xml->NFe->infNFe->emit->xNome) . "," .
                    valoresTexto2(substr($xml->NFe->infNFe->emit->CNPJ, 0, 2) . "." . substr($xml->NFe->infNFe->emit->CNPJ, 2, 3) . "." . substr($xml->NFe->infNFe->emit->CNPJ, 5, 3) . "/" . substr($xml->NFe->infNFe->emit->CNPJ, 8, 4) . "-" . substr($xml->NFe->infNFe->emit->CNPJ, 12, 2)) . "," .
                    valoresTexto2($xml->NFe->infNFe->emit->IE) . "," .
                    valoresTexto2($xml->NFe->infNFe->emit->enderEmit->xLgr) . "," .
                    valoresTexto2($xml->NFe->infNFe->emit->enderEmit->nro) . "," .
                    valoresTexto2($xml->NFe->infNFe->emit->enderEmit->xCpl) . "," .
                    valoresTexto2($xml->NFe->infNFe->emit->enderEmit->xBairro) . "," .
                    $estado . "," .
                    $cidade . "," .
                    valoresTexto2($xml->NFe->infNFe->emit->enderEmit->CEP) . ",'001',0,'F',getdate(),'','')";
            odbc_exec($con, $query) or die(odbc_errormsg());
            $res = odbc_exec($con, "select top 1 id_fornecedores_despesas from sf_fornecedores_despesas order by id_fornecedores_despesas desc") or die(odbc_errormsg());
            $fornecedor = odbc_result($res, 1);
            if ($xml->NFe->infNFe->emit->enderEmit->fone != "") {
                odbc_exec($con, "insert into sf_fornecedores_despesas_contatos(fornecedores_despesas,tipo_contato,conteudo_contato) values(" .
                                $fornecedor . ",1," . valoresTexto2("(" . substr($xml->NFe->infNFe->emit->enderEmit->fone, 0, 2) . ") " . substr($xml->NFe->infNFe->emit->enderEmit->fone, 2, 5) . "-" . substr($xml->NFe->infNFe->emit->enderEmit->fone, 7, 4)) . ")") or die(odbc_errormsg());
            }
        }
        //Verificar os Produtos            
        $cur = odbc_exec($con, "select * from sf_configuracao") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $formaPreco = $RFP['CMP_FORMA'];
            $margem_lucro = $RFP['CMP_VALOR'];
        }
        for ($i = 0; $i <= count($xml->NFe->infNFe->det) - 1; $i++) {
            if ($_POST['txtProduto_' . $i] == 0) {
                $query = "INSERT INTO sf_produtos(descricao,codigo_interno,tipo,unidade_comercial,quantidade_comercial,preco_venda,inativa," .
                        "unidade_trib,quantidade_trib,valor_unitario_trib," .
                        "codigo_barra,origem,ncm,cfop,favorito,editavel,tp_preco,valor_margem_lucro) VALUES(" .
                        valoresTexto2($xml->NFe->infNFe->det[$i]->prod->xProd) . "," .
                        valoresTexto2($xml->NFe->infNFe->det[$i]->prod->cProd) . ",'P'," .
                        valoresTexto2($xml->NFe->infNFe->det[$i]->prod->uCom) . ",1," .
                        $xml->NFe->infNFe->det[$i]->prod->vUnCom . ",0," .
                        valoresTexto2($xml->NFe->infNFe->det[$i]->prod->uTrib) . ",1," .
                        $xml->NFe->infNFe->det[$i]->prod->vUnTrib . "," .                        
                        valoresTexto2($xml->NFe->infNFe->det[$i]->prod->cEAN) . "," .
                        valoresTexto2($xml->NFe->infNFe->det[$i]->prod->cEANTrib) . "," .                        
                        valoresTexto2($xml->NFe->infNFe->det[$i]->prod->NCM) . "," .
                        valoresTexto2($xml->NFe->infNFe->det[$i]->prod->CFOP) . ",0,0," .
                        $formaPreco . "," . $margem_lucro . ")";
                //echo $query;
                odbc_exec($con, $query) or die(odbc_errormsg());
                $res = odbc_exec($con, "select top 1 conta_produto from sf_produtos where conta_produto not in (466) order by conta_produto desc") or die(odbc_errormsg());
                $_POST['txtProduto_' . $i] = odbc_result($res, 1);
            }
            $res = odbc_exec($con, "select conta_movimento,unidade_comercial from sf_produtos where conta_produto = '" . $_POST['txtProduto_' . $i] . "'") or die(odbc_errormsg());
            $grupos .= ",'" . odbc_result($res, 1) . "'";
            $unidades .= ",'" . odbc_result($res, 2) . "'";
            $produtos .= ",'" . $_POST['txtProduto_' . $i] . "'";
            $quantidades .= ",'" . $xml->NFe->infNFe->det[$i]->prod->qCom . "'";
            $impostos = (((float) $xml->NFe->infNFe->det[$i]->imposto->IPI->IPITrib->vIPI) / ((float) $xml->NFe->infNFe->det[$i]->prod->qCom));
            $valores .= ",'" . ((float) $xml->NFe->infNFe->det[$i]->prod->vUnCom + $impostos) . "'";
        }
        $nomePdf = $_FILES['arquivoPdf']['name'];
        $tmp = $_FILES['arquivoPdf']['tmp_name'];
        if ($_FILES["arquivoPdf"]["type"] == "application/pdf") {
            if ($tmp) {
                move_uploaded_file($tmp, $pasta . "/" . $nomePdf);
            }
        }
        echo "<script>var x=[" . $produtos . "];var y=[" . $quantidades . "];var z=[" . $valores . "];var k=[" . $grupos . "];var l=[" . $unidades . "];parent.EnviaXML(" . $fornecedor . ",x,y,z,k,l,'" . $pasta . "/" . $nomePdf . "','" . $filename . "');</script>";
        exit(0);
    } else {
        exit('Falha ao abrir test.xml.');
    }
}
?> 
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />        
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>        
        <style>
            body { font-family: sans-serif; }
            .total {
                line-height: 30px;
                background: #E9E9E9;
                font-weight: bold;
                padding: 0 10px;
                font-size: 11px;
                margin: 0px;
                color: #666;
            }
            .frmcont {
                height: 400px;
                min-height: 400px;
                max-height: 400px;
            }
            img{
                padding-right: 5px;
            }
        </style>
    </head>
    <body>
        <form action="FormImportarXML.php" method="POST" enctype="multipart/form-data" name="frmEnviaDados" id="frmEnviaDados">
            <div class="frmhead">
                <div class="frmtext">Importar Dados de XML</div>
                <select id="txtProdutos" style="display: none;">
                    <?php
                    $sql = "select conta_produto,descricao,codigo_interno from sf_produtos where inativa = 0 and conta_produto > 0 AND tipo = 'P' ORDER BY descricao";
                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) { ?>
                        <option value="<?php echo $RFP['conta_produto']; ?>"><?php echo utf8_encode($RFP['descricao']); ?></option>
                    <?php } ?>                                    
                </select>
                <div class="frmicon" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                XML: <input name="txtNome" id="txtNome" value="<?php echo $filename; ?>" type="hidden"/>
                <?php
                if ($nome != "") {
                    echo "<strong>" . $nome . "</strong>";
                } else {
                    ?>
                    <input class="btn btn-primary" style="height:20px; line-height:21px" type="file" name="arquivo" id="arquivo"/>
                    <button class="btn btn-file" type="submit" name="bntLer" id="bntLer" value="Ler"><span class="ico-plus-sign"></span> Ler</button>
                <?php } ?>
                <div style="width:100%; margin:10px 0 0">
                    <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Fornecedor</span>
                    <hr style="margin:2px 0; border-top:1px solid #ddd">
                </div>
                <div class="body" style="margin-left:0; padding:0">
                    <div class="content" style="min-height:20px; margin-top:0; height: 75px;">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="18%">CNPJ:</th>
                                    <th width="10%">IE:</th>
                                    <th width="30%">Razão Social:</th>
                                    <th width="37%">Endereço:</th>
                                    <th width="5%">Telefone:</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (file_exists($filename)) {
                                    $xml = simplexml_load_file($filename);
                                    $where_bloq = " and cnpj = '" . substr($xml->NFe->infNFe->emit->CNPJ, 0, 2) . "." . substr($xml->NFe->infNFe->emit->CNPJ, 2, 3) . "." . substr($xml->NFe->infNFe->emit->CNPJ, 5, 3) . "/" . substr($xml->NFe->infNFe->emit->CNPJ, 8, 4) . "-" . substr($xml->NFe->infNFe->emit->CNPJ, 12, 2) . "' ";
                                    $cur = odbc_exec($con, "select id_fornecedores_despesas,razao_social from sf_fornecedores_despesas where tipo = 'F' " . $where_bloq . " order by razao_social") or die(odbc_errormsg());
                                    while ($RFP = odbc_fetch_array($cur)) {
                                        $fornecedor = $RFP['id_fornecedores_despesas'];
                                    }
                                    ?>
                                    <tr>
                                        <td>
                                            <?php
                                            if ($fornecedor > 0) {
                                                echo "<img src='../../img/pago.PNG'>";
                                            } else {
                                                echo "<img src='../../img/apagar.PNG'>";
                                            }
                                            echo substr($xml->NFe->infNFe->emit->CNPJ, 0, 2) . "." . substr($xml->NFe->infNFe->emit->CNPJ, 2, 3) . "." . substr($xml->NFe->infNFe->emit->CNPJ, 5, 3) . "/" . substr($xml->NFe->infNFe->emit->CNPJ, 8, 4) . "-" . substr($xml->NFe->infNFe->emit->CNPJ, 12, 2);
                                            ?></td>
                                        <td><?php echo $xml->NFe->infNFe->emit->IE; ?></td>
                                        <td><?php echo $xml->NFe->infNFe->emit->xNome; ?></td>
                                        <td><?php
                                            echo $xml->NFe->infNFe->emit->enderEmit->xLgr . " n° " .
                                            $xml->NFe->infNFe->emit->enderEmit->nro . "," .
                                            $xml->NFe->infNFe->emit->enderEmit->xCpl . " " .
                                            $xml->NFe->infNFe->emit->enderEmit->xBairro . "," .
                                            $xml->NFe->infNFe->emit->enderEmit->xMun . "-" .
                                            $xml->NFe->infNFe->emit->enderEmit->UF . ", cep:" .
                                            $xml->NFe->infNFe->emit->enderEmit->CEP;
                                            ?></td>
                                        <td><?php echo $xml->NFe->infNFe->emit->enderEmit->fone; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div style="width:100%; margin:10px 0 0">
                    <span style="color:black; font-size:14px; text-shadow:0 1px #fff; padding-left:5px">Produtos</span>
                    <hr style="margin:2px 0; border-top:1px solid #ddd">
                </div>
                <div class="body" style="margin-left:0; padding:0">
                    <div class="content" style="min-height:20px; margin-top:0; height: 195px;overflow-y: scroll;">
                        <table id="tbProdutos" class="table" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="13%">NCM:</th>
                                    <th width="42%">Descrição:</th>
                                    <th width="5%">Edt.</th>
                                    <th width="5%">Un:</th>
                                    <th width="5%">Qtd:</th>
                                    <th width="10%">Val.Unit:</th>
                                    <th width="10%">Val.Imp.Total:</th>
                                    <th width="10%">Val.Total:</th>
                                </tr>
                            </thead>
                            <tbody>   
                                <?php
                                for ($i = 0; $i <= count($xml->NFe->infNFe->det) - 1; $i++) {
                                    $produto[$i] = 0;
                                    $cur = odbc_exec($con, "select * from sf_produtos where descricao = " . valoresTexto2($xml->NFe->infNFe->det[$i]->prod->xProd) . "") or die(odbc_errormsg());
                                    while ($RFP = odbc_fetch_array($cur)) {
                                        $produto[$i] = $RFP['conta_produto'];
                                    }
                                    ?>
                                    <tr>
                                        <td>
                                            <input id="txtProduto_<?php echo $i; ?>" name="txtProduto_<?php echo $i; ?>" type="hidden" value="<?php echo $produto[$i]; ?>"/>
                                            <?php
                                            if ($produto[$i] > 0) {
                                                echo "<img id=\"ico_" . $i . "\" src=\"../../img/pago.PNG\">";
                                            } else {
                                                echo "<img id=\"ico_" . $i . "\" src=\"../../img/apagar.PNG\">";
                                            }
                                            echo $xml->NFe->infNFe->det[$i]->prod->NCM;
                                            ?>
                                        </td>
                                        <td><?php echo $xml->NFe->infNFe->det[$i]->prod->xProd; ?></td>
                                        <td>
                                <center>                                                
                                    <a id="btnAlt" href="javascript:;" data-id="<?php echo $i; ?>"><span class="ico-edit" title="Alterar" style="font-size:18px;"></span></a>
                                </center>
                                </td>
                                <td><?php echo $xml->NFe->infNFe->det[$i]->prod->uCom; ?></td>
                                <td><?php echo escreverNumero((float) $xml->NFe->infNFe->det[$i]->prod->qCom); ?></td>
                                <td><?php echo escreverNumero((float) $xml->NFe->infNFe->det[$i]->prod->vUnCom); ?></td>
                                <td><?php
                                    $impostos = ((float) $xml->NFe->infNFe->det[$i]->imposto->IPI->IPITrib->vIPI);
                                    $totalImpostos = $totalImpostos + $impostos;
                                    echo escreverNumero($impostos);
                                    ?></td>
                                <td><?php
                                    echo escreverNumero((float) $xml->NFe->infNFe->det[$i]->prod->vProd + $impostos);
                                    $total = $total + (float) $xml->NFe->infNFe->det[$i]->prod->vProd;
                                    ?></td>
                                </tr>                                            
                            <?php } ?>
                            </tbody>
                        </table>      
                    </div>
                </div>                          
                <div class="total">SubTotal: <?php echo escreverNumero($total, 1); ?>, Total Impostos: <?php echo escreverNumero($totalImpostos, 1); ?> Total: <?php echo escreverNumero($total + $totalImpostos, 1); ?></div>
            </div>
            <div class="frmfoot">
                <?php if ($nome != "") { ?>
                    <span style="float: left;">
                        <input class="btn btn-primary" style="height:20px; line-height:21px" type="file" name="arquivoPdf" id="arquivoPdf"/>                
                        Anexar arquivo PDF a esta compra:                        
                    </span>
                <?php } ?>
                <div class="frmbtn">
                    <button class="btn btn-success" <?php
                    if ($filename == "") {
                        echo "disabled";
                    }
                    ?> type="submit" id="btnGravarPlano" name="bntSave" title="Gravar"><span class="ico-checkmark"></span> Gravar</button>                                    
                    <button class="btn yellow" title="Cancelar" onclick="parent.FecharBox(1)"><span class="ico-reply"></span> Cancelar</button>                                                                        
                </div>
            </div>
            <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
            <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
            <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
            <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
            <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
            <script type='text/javascript' src='../../js/plugins.js'></script>
            <script type='text/javascript' src='../../js/actions.js'></script>        
            <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
            <script type="text/javascript" src="../../js/util.js"></script>            
            <script type="text/javascript">
                $(document).ready(function () {
                    var nEditing = null;
                    var idEditing = "";
                    var textEdit = "";

                    $("body").on("click", "#tbProdutos #btnAlt", function () {
                        var nRow = $(this).parents('tr')[0];
                        if (nEditing !== null) {
                            restoreRow(nEditing, idEditing);
                        } else {
                            var id = $(this).attr("data-id");
                            nEditing = nRow;
                            idEditing = id;
                            var jqTds = $('>td', nRow);
                            textEdit = jqTds[1].innerHTML;
                            jqTds[1].innerHTML = '<select id="txtConta" style="width:100%;" class="select">' + $("#txtProdutos").html() + '</select>';
                            jqTds[2].innerHTML = '<center><a href="javascript:;" data-id="' + id + '" class="btn green-haze save" title="Salvar" style=\"padding: 0px 4px;background:#5bb75b;\"><span class="ico-checkmark"></span></a><a href="javascript:;" data-id="' + id + '" class="btn red-haze cancel" title="Cancelar" style=\"padding: 0px 4px;background:red;\"><span class="ico-reply"></span></a> <center>';
                            $("#txtConta").select2();
                        }
                    });

                    function restoreRow(nRow, id) {
                        var jqTds = $('>td', nRow);
                        jqTds[1].innerHTML = textEdit;
                        jqTds[2].innerHTML = '<center><a href="javascript:;" data-id="' + id + '" id="btnAlt"><span class="ico-edit" title="Alterar" style="font-size:18px;"></span></a><center>';
                        nEditing = null;
                    }

                    $("#tbProdutos").on('click', '.save', function (e) {
                        var id = $(this).attr("data-id");
                        var nRow = $(this).parents('tr')[0];
                        $("#txtProduto_" + id).val($("#txtConta").val());
                        textEdit = $("#txtConta option:selected").text();
                        $("#ico_" + id).attr('src', '../../img/pago.PNG');
                        restoreRow(nRow, id);
                    });

                    $("#tbProdutos").on('click', '.cancel', function (e) {
                        var id = $(this).attr("data-id");
                        var nRow = $(this).parents('tr')[0];
                        restoreRow(nRow, id);
                    });
                });
            </script>            
        </form>
    </body>
    <?php odbc_close($con); ?>
</html>

