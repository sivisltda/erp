<?php

//header('Cache-Control: no-cache');
//header('Content-type: application/xml; charset="utf-8"', true);
include "../../Connections/configini.php";

$id_area = $_REQUEST['txtConta'];
if ($id_area == '') {
    $id_area = $_REQUEST['txtContaS'];
}
$idFilial = 0;
if (!is_numeric($_REQUEST['txtFilial'])) {
    $query = "select ISNULL(filial, (SELECT id_filial FROM dbo.sf_filiais where id_filial = 1)) filial from dbo.sf_usuarios where id_usuario = '" . $_SESSION["id_usuario"] . "'";
    $cur = odbc_exec($con, $query) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $idFilial = $RFP['filial'];
    }
} else {
    $idFilial = $_REQUEST['txtFilial'];
}
$local = array();
if (is_numeric($id_area)) {
    $sql = "select conta_movimento grupo_conta,preco_venda,quantidade_comercial,unidade_comercial,
    (select top 1 valor_total/quantidade from dbo.sf_vendas v inner join sf_vendas_itens i on v.id_venda = i.id_venda where cov = 'C' and status = 'Aprovado' and produto = conta_produto order by data_venda desc) custo,
    nao_vender_sem_estoque,  dbo.ESTOQUE_FILIAL(conta_produto," . $idFilial . ") estoque_atual
    from sf_produtos where inativa = 0 and conta_produto = " . $id_area . " ORDER BY 1";
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('grupo_conta' => $RFP['grupo_conta'], 'preco_venda' => utf8_encode($RFP['preco_venda']), 'quantidade_comercial' => utf8_encode($RFP['quantidade_comercial']),
            'unidade_comercial' => utf8_encode($RFP['unidade_comercial']), 'custo' => escreverNumero(($RFP['custo'] * valoresNumericos2($_REQUEST['txtQtd']))), 'nao_vender_sem_estoque' => $RFP['nao_vender_sem_estoque'], 'estoque_atual' => $RFP['estoque_atual']);
    }
}
echo(json_encode($local));
odbc_close($con);