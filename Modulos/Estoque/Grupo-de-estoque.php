<?php
include '../../Connections/configini.php';

$F2 = "";
$lbl_modulo = "";
$lbl_pagina = "";

if (is_numeric($_GET["tp"])) {
    $F2 = $_GET["tp"];
}

$mdl_clb_ = returnPart($_SESSION["modulos"],12);
$mdl_seg_ = returnPart($_SESSION["modulos"],14);

if ($F2 == "0") {
    $lbl_modulo = "Estoque";
    $lbl_pagina = "Grupo de Produtos";    
} elseif ($F2 == "1") {
    $lbl_modulo = ($mdl_clb_ == 1 || $mdl_seg_ == 1 ? "Configuração" : "Academia");
    $lbl_pagina = "Grupo de Serviços";
} elseif ($F2 == "2") {
    $lbl_modulo = "Proteção";
    $lbl_pagina = "Grupo de Acessórios";    
}

if (is_numeric($_GET['Del'])) {
    odbc_exec($con, "DELETE FROM sf_contas_movimento WHERE id_contas_movimento = " . $_GET['Del']);
    echo "<script>window.top.location.href = 'Grupo-de-estoque.php?tp=" . $F2 . "'; </script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />        
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>                     
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"><span class="ico-arrow-right"></span></div>
                        <h1><?php echo $lbl_modulo; ?><small><?php echo $lbl_pagina;?></small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="margin-top: 15px;float:left;">
                                    <div style="float:left">
                                        <form method="POST">
                                            <button  class="button button-green btn-primary" type="button" onClick="AbrirBox(1, 0)"><span class="ico-file-4 icon-white"></span></button>
                                            <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir()" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                        </form>
                                    </div>
                                </div>
                                <div style="float: right;width: 50%; display: flex;align-items: center;justify-content: flex-end;">
                                    <input id="txtTipo" name="txtTipo" type="hidden" value="<?php echo $F2; ?>"/>
                                    <?php if ($F2 == 2) { ?>
                                    <div style="float:left;margin-left: 1%;">                                    
                                        <div width="100%">Tipo:</div>
                                        <select id="txtTipoVeiculo">
                                            <option value="null">Selecione</option>
                                        <?php $sql = "select id, descricao from sf_veiculo_tipos where v_inativo = 0 ORDER BY id";
                                        $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="<?php echo $RFP['id'] ?>"><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                        <?php } ?>
                                        </select>                                           
                                    </div>                                    
                                    <?php } ?>                                    
                                    <div style="float:left;margin-left: 1%;">                                    
                                        <div width="100%">Inativo:</div>
                                        <select id="txtInativo">
                                            <option value="0">NÃO</option>
                                            <option value="1">SIM</option>
                                        </select>                                           
                                    </div>                                    
                                    <div style="float:left;margin-left: 1%;">
                                        <div width="100%">Busca:</div>
                                        <input name="txtBusca"  id="txtBusca" maxlength="100" type="text" style="width:175px;" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title="Nome / CPF/ CNPJ / Endereço / Bairro / Cidade / Contato"/>
                                    </div>
                                    <div style="margin-top: 15px;float:left;margin-left:0.3%;">
                                        <div style="float:left">
                                            <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead" <?php echo $visible; ?>>
                        <div class="boxtext"><?php echo $lbl_pagina;?></div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="example">
                            <thead>
                                <tr>
                                    <th width="3%"><center><?php echo ($F2 == 2 ? "Nível" : "P/S"); ?></center></th>
                                    <th width="19%">Descrição do grupo</th>
                                    <th width="<?php echo ($F2 == 2 ? "42" : "52") ?>%">Descrição do <?php echo ($mdl_seg_ == 1 ? "Grupo de Acessórios" : "Subgrupos de Contas");?></th>
                                    <?php if ($F2 == 2) { ?>
                                    <th width="10%">Tipo</th>
                                    <?php } ?>
                                    <th width="10%"><?php echo ($F2 == 2 ? "Sel.Múlt" : "Menu Ter."); ?></th>
                                    <th width="10%">Inativo</th>
                                    <th width="6%"><center>Ação:</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                    <div class="widgets"></div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>
        <script type="text/javascript" src="../../fancyapps/source/jquery.fancybox.js?v=2.1.4"></script>               
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }

            $(document).ready(function () {
                listaTable();
                $("#btnfind").click(function () {
                    $("#example").dataTable().fnDestroy();
                    listaTable();
                });

                function listaTable() {
                    $('#example').dataTable({
                        "iDisplayLength": 20,
                        "aLengthMenu": [20, 30, 40, 50, 100],
                        "bProcessing": true,
                        "bServerSide": true,
                        "sAjaxSource": "Grupo-de-estoque_server_processing.php" + finalFind(0),
                        "bFilter": false,
                        "ordering": false,
                        'oLanguage': {
                            'oPaginate': {
                                'sFirst': "Primeiro",
                                'sLast': "Último",
                                'sNext': "Próximo",
                                'sPrevious': "Anterior"
                            }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                            'sLengthMenu': "Visualização de _MENU_ registros",
                            'sLoadingRecords': "Carregando...",
                            'sProcessing': "Processando...",
                            'sSearch': "Pesquisar:",
                            'sZeroRecords': "Não foi encontrado nenhum resultado"},
                        "sPaginationType": "full_numbers"
                    });
                }
            });
            
            function imprimir() {
                var pRel = "&NomeArq=" + "<?php echo $lbl_pagina; ?>" +
                "&lbl=Nível|Descrição do Grupo|Descrição do <?php echo ($mdl_seg_ == 1 ? "Grupo de Acessórios" : "Subgrupos de Contas");?>|Tipo|<?php echo ($F2 == 2 ? "Sel.Múlt" : "Menu Ter."); ?>|Inativo" +
                "&siz=50|130|300|100|60|60" +
                "&pdf=6" + // Colunas do server processing que não irão aparecer no pdf
                "&filter=" + //Label que irá aparecer os parametros de filtro
                "&PathArqInclude=" + "../Modulos/Estoque/Grupo-de-estoque_server_processing.php" + finalFind(1).replace("?", "&"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }
            
            function finalFind(imp) {
                var retPrint = "";
                retPrint = '?imp=0';
                if (imp === 1) {
                    retPrint = '?imp=1';
                }
                retPrint = retPrint + '&tp=' + $("#txtTipo").val();
                if ($("#txtBusca").val() !== "") {
                    retPrint = retPrint + '&txtBusca=' + $("#txtBusca").val();
                }
                if ($("#txtTipoVeiculo").val() !== "null") {
                    retPrint = retPrint + '&txtTipo=' + $("#txtTipoVeiculo").val();
                }                
                if ($("#txtInativo").val() !== "null") {
                    retPrint = retPrint + '&txtInativo=' + $("#txtInativo").val();
                }                
                return retPrint.replace(/\//g, "_");
            }

            function AbrirBox(opc, id) {
                var tp = $("#txtTipo").val();
                if (opc === 1) {                    
                    abrirTelaBox("FormGrupo-de-estoque.php?tp=" + tp, 550, 750);
                }else if (opc === 2) {
                    abrirTelaBox("FormGrupo-de-estoque.php?tp=" + tp + "&id=" + id, 550, 750);
                }                
            }
            
            function FecharBox(opc) {
                var oTable = $('#example').dataTable();
                oTable.fnDraw(false);
                $("#newbox").remove();
            }            
        </script>             
        <?php odbc_close($con); ?>
    </body>
</html>
