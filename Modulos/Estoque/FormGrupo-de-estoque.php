<?php
include '../../Connections/configini.php';
$mdl_seg_ = returnPart($_SESSION["modulos"], 14);

if (is_numeric($_GET["tp"])) {
    $F2 = $_GET["tp"];
    if ($F2 == "0") {
        $grupo = "33";
    } elseif ($F2 == "0") {
        $grupo = "34";
    } elseif ($F2 == "0") {
        $grupo = "35";
    }
}

$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_contas_movimento set " .
                        "descricao = " . valoresTexto("txtDescricao") . "," .
                        "grupo_observacao = " . valoresTexto("txtObservacao") . "," .
                        "grupo_conta = " . valoresSelect("txtGrupo") . "," .
                        "veiculo_tipo = " . valoresSelect("txtTipo") . "," .
                        "terminal = " . valoresSelect("txtMenu") . "," .
                        "inativa = " . valoresSelect("txtInativo") . "," .
                        "nivel = " . valoresNumericos("txtNivel") . "," .
                        "direcao_texto = " . valoresSelect("txtDirecaoTexto") . "," .
                        "so_padrao = " . valoresSelect("txtPadrao") . " where id_contas_movimento = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_contas_movimento(tipo,descricao,grupo_observacao,grupo_conta,veiculo_tipo,terminal,nivel,direcao_texto,inativa, so_padrao)values(
        (select max(deb_cre) from sf_grupo_contas where id_grupo_contas = " . valoresSelect("txtGrupo") . ")," . valoresTexto("txtDescricao") . "," .
                        valoresTexto("txtObservacao") . "," . valoresSelect("txtGrupo") . "," . valoresSelect("txtTipo") . "," . valoresSelect("txtMenu") . "," .
                        valoresNumericos("txtNivel") . "," . valoresSelect("txtDirecaoTexto") . "," . valoresSelect("txtInativo") . "," . valoresSelect("txtPadrao") . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_contas_movimento from sf_contas_movimento order by id_contas_movimento desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
    $inValue = "0";
    for ($i = 0; $i < count($_POST['txtFilial']); $i++) {
        if ($_POST['txtFilial'][$i] != '') {
            $inValue .= "," . $_POST['txtFilial'][$i];
        }
    }
    odbc_exec($con, "delete from sf_contas_movimento_contas where id_conta = " . $_POST['txtId'] . " and id_conta_ref not in (" . $inValue . ")") or die(odbc_errormsg());
    for ($i = 0; $i < count($_POST['txtFilial']); $i++) {
        odbc_exec($con, "IF NOT EXISTS (SELECT id_conta, id_conta_ref FROM sf_contas_movimento_contas WHERE id_conta = " . $_POST['txtId'] . " AND id_conta_ref = " . $_POST['txtFilial'][$i] . ")
        BEGIN INSERT INTO sf_contas_movimento_contas(id_conta, id_conta_ref) VALUES (" . $_POST['txtId'] . "," . $_POST['txtFilial'][$i] . "); END");
    }
}

if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_contas_movimento WHERE id_contas_movimento = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_contas_movimento where id_contas_movimento =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_contas_movimento'];
        $descricao = utf8_encode($RFP['descricao']);
        $observacao = utf8_encode($RFP['grupo_observacao']);
        $grupo = $RFP['grupo_conta'];
        $tipoVeiculo = $RFP['veiculo_tipo'];
        $menu = $RFP['terminal'];
        $nivel = $RFP['nivel'];
        $direcaoTexto = $RFP['direcao_texto'];
        $inativo = $RFP['inativa'];
        $padrao = $RFP['so_padrao'];
    }
} else {
    $disabled = "";
    $id = "";
    $descricao = "";
    $observacao = "";
    $grupo = "";
    $tipoVeiculo = "";
    $menu = "";
    $nivel = "";
    $direcaoTexto = "";
    $inativo = "";
    $padrao = "";
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<style>
    .select2-choices {
        font-size: 8.5px;
    }
</style>
<script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <form action="FormGrupo-de-estoque.php?tp=<?php echo $F2; ?>" name="frmEnviaDados" id="frmEnviaDados" method="POST">
        <div class="frmhead">
            <div class="frmtext"><?php echo ($F2 == 1 ? "Grupo de Serviço" : ($F2 == 0 ? "Grupo de Produto" : ($mdl_seg_ == 1 ? "Grupo de Acessórios" : "Subgrupos de Contas"))); ?></div>
            <div class="frmicon" onClick="parent.FecharBox(1)">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="tabbable frmtabs">
            <ul class="nav nav-tabs" style="margin:0">
                <li class="active"><a href="#tab1" data-toggle="tab">Dados principais</a></li>
                <li><a href="#tab2" data-toggle="tab">Grupos</a></li>
            </ul>
            <div class="tab-content frmcont" style="height:360px">
                <div class="tab-pane active" id="tab1" style="overflow:hidden; height:360px">
                    <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>                        
                    <div style="height:42px;width: 70%;float: left">
                        <span>Descrição:</span>
                        <input name="txtDescricao" id="txtDescricao" <?php echo $disabled; ?> maxlength="128" type="text" class="input-medium" value="<?php echo $descricao; ?>"/>
                    </div>
                    <div style="width: 14%;float:left; margin-left: 1%;">
                        <span><?php echo ($F2 == "2" ? 'Sel.Múlt.' : 'Menu Ter.'); ?>:</span>
                        <select class="input-medium" <?php echo $disabled; ?> name="txtMenu" id="txtMenu" style="width: 100%">
                            <option value="0" <?php echo ($menu == "0" ? "SELECTED" : ""); ?>>NÃO</option>
                            <option value="1" <?php echo ($menu == "1" ? "SELECTED" : ""); ?>>SIM</option>
                        </select>
                    </div>            
                    <div style="width: 14%;float:left; margin-left: 1%;">
                        <span>Inativo:</span>
                        <select class="input-medium" <?php echo $disabled; ?> name="txtInativo" id="txtInativo" style="width: 100%">
                            <option value="0" <?php echo ($inativo == "0" ? "SELECTED" : ""); ?>>NÃO</option>
                            <option value="1" <?php echo ($inativo == "1" ? "SELECTED" : ""); ?>>SIM</option>
                        </select>
                    </div>            
                    <div style="height:42px;width: 85%;float: left">
                        <span>Observação:</span>
                        <input name="txtObservacao" id="txtObservacao" <?php echo $disabled; ?> maxlength="1024" type="text" class="input-medium" value="<?php echo $observacao; ?>"/>
                    </div>
                    <div style="width: 14%;float:left; margin-left: 1%;">
                        <span>Só Padrão:</span>
                        <select class="input-medium" <?php echo $disabled; ?> name="txtPadrao" id="txtPadrao" style="width: 100%">
                            <option value="0" <?php echo ($padrao == "0" ? "SELECTED" : ""); ?>>NÃO</option>
                            <option value="1" <?php echo ($padrao == "1" ? "SELECTED" : ""); ?>>SIM</option>
                        </select>
                    </div>                    
                    <div style="clear:both;height: 5px;"></div>
                    <div style="width: 40%;float:left;">
                        <span>Grupo:</span>
                        <select class="input-medium" <?php echo $disabled; ?> name="txtGrupo" id="txtGrupo" style="width: 100%">
                            <?php if ($F2 == "0") { ?>
                                <option value="33" <?php echo ($grupo == "33" ? "SELECTED" : ""); ?>>PRODUTOS</option>
                            <?php } if ($F2 == "1") { ?>                    
                                <option value="34" <?php echo ($grupo == "34" ? "SELECTED" : ""); ?>>SERVIÇOS</option>
                            <?php } if ($F2 == "2") { ?>                    
                                <option value="35" <?php echo ($grupo == "35" ? "SELECTED" : ""); ?>>ACESSÓRIOS</option>
                            <?php } ?>                    
                        </select>
                    </div>
                    <div style="width: 21%;float:left; margin-left: 1%;">
                        <span>Direção do texto:</span>
                        <select class="input-medium" <?php echo $disabled; ?> name="txtDirecaoTexto" id="txtDirecaoTexto" style="width: 100%">
                            <option value="0" <?php echo ($direcaoTexto == "0" ? "SELECTED" : ""); ?>>DIREITA</option>
                            <option value="1" <?php echo ($direcaoTexto == "1" ? "SELECTED" : ""); ?>>ESQUERDA</option>
                        </select>
                    </div>
                    <?php if ($mdl_seg_ > 0) { ?>
                        <div style="width: 22%;float:left; margin-left: 1%;">
                            <span>Tipo:</span>
                            <select class="input-medium" <?php echo $disabled; ?> name="txtTipo" id="txtTipo" style="width: 100%">
                                <option value="null">Selecione</option>
                                <?php
                                $sql = "select id, descricao from sf_veiculo_tipos where v_inativo = 0 ORDER BY id";
                                $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) {
                                    ?>
                                    <option value="<?php echo $RFP['id'] ?>"<?php
                                    if ($RFP["id"] == $tipoVeiculo) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                        <?php } ?>                      
                            </select>
                        </div>
                        <div style="width: 14%;float:left; margin-left: 1%;">
                            <span>Nível:</span>
                            <input name="txtNivel" id="txtNivel" <?php echo $disabled; ?> maxlength="2" type="text" class="input-medium" value="<?php echo $nivel; ?>"/>
                        </div>          
                    <?php } ?>
                    <div style="clear:both;height: 5px;"></div>  
                </div>        
                <div class="tab-pane" id="tab2" style="overflow:hidden; height:360px">
                    <div style="width:100%; float: left;">
                        <span>Grupo:</span>
                        <select id="txtFilial" name="txtFilial[]" multiple="multiple" class="select" style="width:100%;" class="input-medium" <?php echo $disabled; ?>>
                            <?php
                            $cur = odbc_exec($con, "select t.id_contas_movimento,t.descricao, ti.id_conta_ref from sf_contas_movimento t 
                            left join sf_contas_movimento_contas ti on t.id_contas_movimento = ti.id_conta_ref and id_conta = " . valoresSelect2($id) . "
                            where tipo = 'L' and inativa = 0 order by t.descricao") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) {
                                ?>
                                <option value="<?php echo $RFP['id_contas_movimento'] ?>"<?php
                                if ($RFP["id_conta_ref"] != null) {
                                    echo "SELECTED";
                                }
                                ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                    <?php } ?>
                        </select>
                    </div>
                </div>        
            </div>        
        </div>        
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" name="bntSave" id="bntSave" ><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox(1)" name="bntCancelar" id="bntCancelar"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntAlterar" ><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else {
                    ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntNew" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntEdit" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
    <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
    <script type="text/javascript" src="../../js/plugins/other/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/epiechart/jquery.easy-pie-chart.js"></script>
    <script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/plugins.js"></script>
    <script type="text/javascript" src="../../js/charts.js"></script>
    <script type="text/javascript" src="../../js/actions.js"></script>
    <script type="text/javascript" src="../../js/app.js"></script>
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>    
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>     
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type="text/javascript">
        $("#frmEnviaDados").validate({
            errorPlacement: function (error, element) {
                return true;
            },
            rules: {
                txtDescricao: {required: true}
            }
        });
    </script>
    <?php odbc_close($con); ?>
</body>