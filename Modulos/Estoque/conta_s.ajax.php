<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include "../../Connections/configini.php";

$id_area = $_REQUEST['txtGrupo'];
$local = array();
if (is_numeric($id_area)) {
    $sql = "select conta_produto,descricao from sf_produtos where inativa = 0 and conta_produto > 0 AND conta_movimento = " . $id_area . " ORDER BY descricao";
} else {
    $sql = "select conta_produto,descricao from sf_produtos where inativa = 0 and conta_produto > 0 AND tipo = 'S' ORDER BY descricao";
}
$cur = odbc_exec($con, $sql) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $local[] = array('id_contas_movimento' => $RFP['conta_produto'], 'descricao' => utf8_encode($RFP['descricao']));
}
echo( json_encode($local) );
odbc_close($con);
