<?php
include "../../Connections/configini.php";
$disabled = "disabled";
$active1 = "active";

if (isset($_POST["bntSave"])) {
    $sinal = (valoresCheck("txtNegativo") == 1 ? "-" : "");
    if ($_POST["txtId"] != "") {
        $query = "set dateformat dmy;UPDATE sf_produtos SET " .
                "descricao = " . valoresTexto("txtDescricao") .
                ",tipo = 'A',inativa = " . valoresCheck("txtInativa") .
                ",conta_movimento = " . utf8_decode($_POST["txtGrupo"]) .
                ",preco_venda = " . $sinal . valoresNumericos("txtPreco_venda") .
                ",codigo_interno = " . valoresTexto("txtCodigo_interno") .
                ",codigo_barra = " . valoresTexto("txtCodigo_barra") .
                ",desc_prod = ltrim(" . valoresTexto("ckeditor") . ")" .
                ",veiculo_tipo = " . valoresSelect("txtTipo") .
                ",tempo_producao = " . valoresNumericos("txtTempoProducao") .
                ",tp_preco = " . valoresSelect("txtTpPreco") .
                ",negativo = " . valoresCheck("txtNegativo") .
                " WHERE conta_produto = " . $_POST["txtId"];
        odbc_exec($con, $query) or die(odbc_errormsg());        
    } else {
        $query = "set dateformat dmy;INSERT INTO sf_produtos(descricao,tipo,quantidade_comercial,inativa,alterar_valor,conta_movimento,
        favorito,preco_venda,codigo_interno,codigo_barra,desc_prod,unidade_comercial,veiculo_tipo,tempo_producao,
        tp_preco, valor_comissao_venda, ativar_convite, editavel,negativo) VALUES(" .
                valoresTexto("txtDescricao") . ",'A',1.00," . valoresCheck("txtInativa") . ",0," .           
                utf8_decode($_POST["txtGrupo"]) . ",0," . $sinal . valoresNumericos("txtPreco_venda") . "," .
                valoresTexto("txtCodigo_interno") . "," . valoresTexto("txtCodigo_barra") . "," . "LTRIM(" . valoresTexto("ckeditor") . "),'UN'," .
                valoresSelect("txtTipo") . "," . valoresNumericos("txtTempoProducao") . "," .
                valoresSelect("txtTpPreco") . ",0.00,0,0," . valoresCheck("txtNegativo") . ")";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 conta_produto from sf_produtos where conta_produto not in (466) order by conta_produto desc") or die(odbc_errormsg());
        $id = odbc_result($res, 1);
        $_POST["txtId"] = $id;        
    }
    if (isset($_POST['txtContatosTotal'])) {
        $notIn = '0';        
        $max = $_POST['txtContatosTotal'];
        for ($i = 0; $i <= $max; $i++) {
            if (isset($_POST['txtIdItem_' . $i]) && is_numeric($_POST['txtIdItem_' . $i])) {
                $notIn .= "," . $_POST['txtIdItem_' . $i];
            }
        }
        odbc_exec($con, "delete from sf_produtos_grupos where id_produto = " . $_POST['txtId'] . " and id_grupo not in (" . $notIn . ")");
        for ($i = 0; $i <= $max; $i++) {            
            if (isset($_POST['txtIdItem_' . $i]) && is_numeric($_POST['txtIdItem_' . $i])) {
                odbc_exec($con, "IF NOT EXISTS (select * from sf_produtos_grupos where id_produto = " . $_POST['txtId'] . " and id_grupo = " . $_POST['txtIdItem_' . $i] . ") 
                BEGIN insert into sf_produtos_grupos(id_produto, id_grupo) values (" . $_POST['txtId'] . "," . $_POST['txtIdItem_' . $i] . "); END") or die(odbc_errormsg());
            }
        }
    }    
}
if (isset($_POST["bntDelete"])) {
    if ($_POST["txtId"] != "") {
        odbc_exec($con, "DELETE FROM sf_produtos WHERE conta_produto = " . $_POST["txtId"]);
        echo "<script>alert('Registro excluido com sucesso');window.top.location.href = 'Acessorios.php';</script>";
    }
}
if (isset($_POST["bntNew"])) {
    $_GET["id"] = "";
    $_POST["txtId"] = "";
}
if ($_GET["id"] != "" || $_POST["txtId"] != "") {
    $PegaURL = $_GET["id"];
    if ($_POST["txtId"] != "") {
        $disabled = "disabled";
        $PegaURL = $_POST["txtId"];
    }
    $cur = odbc_exec($con, "select * from sf_produtos where conta_produto =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP["conta_produto"];
        $descricao = utf8_encode($RFP["descricao"]);
        $grupo = $RFP["conta_movimento"];
        $preco_venda = escreverNumero((($RFP["negativo"] == 1 ? -1 : 1) * $RFP["preco_venda"]), 0, 4);
        $codigo_interno = utf8_encode($RFP["codigo_interno"]);
        $codigo_barra = utf8_encode($RFP["codigo_barra"]);
        $texto_desc = utf8_encode($RFP["desc_prod"]);      
        $tipoVeiculo = utf8_encode($RFP["veiculo_tipo"]);        
        $tempo_producao = utf8_encode($RFP["tempo_producao"]);        
        $prod_inativo = $RFP["inativa"];        
        $tp_preco = $RFP["tp_preco"];
        $negativo = $RFP["negativo"];        
    }
} else {    
    $disabled = "";
    $id = "";
    $descricao = "";
    $grupo = "";
    $preco_venda = escreverNumero(0, 0, 4);
    $codigo_interno = "";
    $codigo_barra = "";
    $texto_desc = "";
    $tipoVeiculo = "";
    $tempo_producao = "";
    $prod_inativo = ""; 
    $tp_preco = "0";
    $negativo = "0";
}
if (isset($_POST["bntEdit"])) {
    if ($_POST["txtId"] != "") {
        $disabled = "";
    }
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
<link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<link href="../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<link href="../../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../js/jcrop/jquery.Jcrop.min.css" type="text/css"/>
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
<body>
    <div class="row-fluid">
        <form action="FormAcessorios.php" method="POST" name="frmEnviaDados" id="frmEnviaDados" enctype="multipart/form-data">
            <div class="frmhead">
                <div class="frmtext">Acessórios</div>
                <div class="frmicon" onClick="parent.FecharBox(1)">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="tabbable frmtabs">
                <ul class="nav nav-tabs" style="margin:0">
                    <li class="active"><a href="#tab1" data-toggle="tab">Geral</a></li>
                    <li><a href="#tab2" data-toggle="tab">Descrição</a></li>
                    <li><a href="#tab3" data-toggle="tab">Dependência</a></li>
                </ul>
                <div class="tab-content frmcont" style="height:220px;">
                    <div class="tab-pane active" id="tab1">
                        <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>                                                                        
                        <div style="width: 100%;float: left">
                            <span>Grupo:</span>
                            <select name="txtGrupo" id="txtGrupo" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                                <option value="null" >Selecione o grupo:</option>
                                <?php
                                $sql = "select id_contas_movimento, cm.descricao, vt.descricao tipo 
                                from sf_contas_movimento cm 
                                left join sf_veiculo_tipos vt on cm.veiculo_tipo = vt.id and v_inativo = 0
                                where tipo = 'A' and inativa = 0 ORDER BY cm.descricao";
                                $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP["id_contas_movimento"] ?>"<?php
                                    if (!(strcmp($RFP["id_contas_movimento"], $grupo))) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP["descricao"]) ?><?php echo (strlen($RFP["tipo"]) > 0 ? " - [" . utf8_encode($RFP["tipo"]) . "]" : ""); ?></option>
                                <?php } ?>
                            </select>                            
                        </div>
                        <div style="width: 100%;float: left">
                            <span>Nome do Acessórios:</span>
                            <input name="txtDescricao" id="txtDescricao" <?php echo $disabled; ?> type="text" class="input-medium" value="<?php echo $descricao; ?>"/>
                        </div>  
                        <div style="width: 13%;float: left;">
                            <span>Nível:</span>
                            <input name="txtTempoProducao" id="txtTempoProducao" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $tempo_producao; ?>"/>
                        </div>                        
                        <div style="width: 30%;float: left; margin-left: 1%;">
                            <span>Tipo Veículo:</span> 
                            <select class="input-medium" <?php echo $disabled; ?> name="txtTipo" id="txtTipo" style="width: 100%">
                                <option value="null">Selecione</option>
                                <?php $sql = "select id, descricao from sf_veiculo_tipos where v_inativo = 0 ORDER BY id";
                                $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                    <option value="<?php echo $RFP['id'] ?>"<?php
                                    if ($RFP["id"] == $tipoVeiculo) {
                                        echo "SELECTED";
                                    }
                                    ?>><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                <?php } ?>                      
                            </select>                            
                        </div>                        
                        <div style="width: 30%;float: left; margin-left: 1%;">
                            <span>Tipo Preço:</span> 
                            <select class="input-medium" <?php echo $disabled; ?> name="txtTpPreco" id="txtTpPreco" style="width: 100%">
                                <option value="0" <?php echo ($tp_preco == "0" ? "SELECTED" : ""); ?>>Valor Fixo (R$)</option>
                                <option value="1" <?php echo ($tp_preco == "1" ? "SELECTED" : ""); ?>>Valor Máx. Veículo (%)</option>                                
                                <option value="2" <?php echo ($tp_preco == "2" ? "SELECTED" : ""); ?>>Valor FIPE (%)</option>                                
                            </select>                            
                        </div>                        
                        <div style="width: 24%;float: left; margin-left: 1%;">
                            <span>Valor de Venda:</span>
                            <input style="text-align:right" name="txtPreco_venda" id="txtPreco_venda" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $preco_venda; ?>"/>
                        </div>
                        <div style="width: 44%;float: left;">
                            <span>Código:</span>
                            <input name="txtCodigo_interno" id="txtCodigo_interno" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $codigo_interno; ?>"/>
                        </div>
                        <div style="width: 55%;float: left; margin-left: 1%;">
                            <span>Código Descrição:</span>
                            <input name="txtCodigo_barra" id="txtCodigo_barra" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="100" value="<?php echo $codigo_barra; ?>"/>
                        </div>                        
                        <div style="width: 100%;float: left; margin-top: 1%;"></div>                                                          
                        <div style="width: 25%;float: left">
                            <input name="txtInativa" id="txtInativa" <?php echo $disabled; ?> type="checkbox" <?php
                                if ($prod_inativo == "1") {
                                    echo "CHECKED";
                                }
                                ?> value="1" class="input-medium"/>
                            <span>Inativo:</span>                            
                        </div>                                                                                  
                        <div style="width: 25%;float: left">
                            <input name="txtNegativo" id="txtNegativo" <?php echo $disabled; ?> type="checkbox" <?php
                                if ($negativo == "1") {
                                    echo "CHECKED";
                                }
                                ?> value="1" class="input-medium"/>
                            <span>Valor Negativo:</span>                            
                        </div>                                                                                  
                    </div>
                    <div class="tab-pane" id="tab2">
                        <label for="ckeditor"></label>
                        <textarea id="ckeditor" name="ckeditor" <?php echo $disabled; ?>>
                            <?php echo $texto_desc; ?>
                        </textarea>
                    </div>
                    <div class="tab-pane" id="tab3">
                        <div style="background:#F1F1F1; border:1px solid #DDD; border-bottom:0; padding:10px 10px 0">
                            <div style="float:left; width:89%;">
                                <select id="txtGrupoSel" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                                    <option value="null" >Selecione o grupo:</option>
                                    <?php $sql = "select id_contas_movimento,descricao from sf_contas_movimento where tipo = 'A' and inativa = 0 ORDER BY descricao";
                                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                        <option value="<?php echo $RFP["id_contas_movimento"] ?>"><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div style="float:left; width:10%; margin-left:1%;">
                                <button type="button" onclick="addCampos();" class="btn dblue" <?php echo $disabled; ?> style="height: 26px;background-color: #308698 !important;">
                                    <span style="margin-top: 4px;" class="ico-plus icon-white"></span>
                                </button>
                            </div>
                            <div style="clear:both"></div>
                        </div>
                        <div style="background:#F1F1F1; border:1px solid #DDD; border-top:0; padding:5px 10px 10px">
                            <div id="divContatos" style="height:160px; background:#FFF; border:1px solid #DDD; overflow-y:scroll;">
                            <?php
                                $i = 0;
                                if (is_numeric($id)) {                                    
                                    $cur = odbc_exec($con, "select id_contas_movimento, descricao from sf_produtos_grupos
                                    inner join sf_contas_movimento on id_grupo = id_contas_movimento
                                    where id_produto = " . $id . " order by descricao") or die(odbc_errormsg());
                                    while ($RFP = odbc_fetch_array($cur)) { $i++; ?>                            
                                    <div id="tabela_linha_<?php echo $i; ?>" style="padding:2px; border-bottom:1px solid #DDD; overflow: hidden;">
                                        <input id="txtIdItem_<?php echo $i; ?>" name="txtIdItem_<?php echo $i; ?>" value="<?php echo utf8_encode($RFP["id_contas_movimento"]); ?>" type="hidden">
                                        <div id="tabela_desc_<?php echo $i; ?>" style="line-height:27px; float:left"><?php echo utf8_encode($RFP["descricao"]); ?></div>
                                        <div style="width: 5%; padding: 4px 0px 3px; float: right; cursor: pointer;" onclick="return removeLinha('<?php echo $i; ?>');">
                                            <span class="ico-remove" style="font-size:20px"></span>
                                        </div>
                                    </div>
                                <?php }} ?>                            
                                <input id="txtContatosTotal" name="txtContatosTotal" type="hidden" value="<?php echo $i; ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == "") { ?>
                        <button class="btn green" type="submit" name="bntSave"><span class="ico-checkmark"></span> Gravar</button>
                        <?php if ($_POST["txtId"] == "") { ?>
                            <button class="btn yellow" onClick="history.go(window.top.location.href = 'Localizacoes.php')"><span class="ico-reply"></span> Cancelar</button>
                        <?php } else { ?>
                            <button class="btn yellow" type="submit" name="bntAlterar"><span class="ico-reply"></span> Cancelar</button>
                        <?php
                        }
                    } else { ?>
                        <button class="btn green" type="submit" name="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" type="submit" name="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                        <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')"><span class="ico-remove"></span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
    <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
    <script type="text/javascript" src="../../js/plugins/other/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/animatedprogressbar/animated_progressbar.js"></script>
    <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="../../js/plugins.js"></script>
    <script type="text/javascript" src="../../js/charts.js"></script>
    <script type="text/javascript" src="../../js/actions.js"></script>
    <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../js/jquery.priceformat.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type="text/javascript">
        
        function addCampos() {
            if (validar()) {
                let id = parseInt($("#txtContatosTotal").val()) + 1;
                let linha = "<div id=\"tabela_linha_" + id + "\" style=\"padding:2px; border-bottom:1px solid #DDD; overflow: hidden;\">" +                         
                "<input id=\"txtIdItem_" + id + "\" name=\"txtIdItem_" + id + "\" value=\"" + $("#txtGrupoSel").val() + "\" type=\"hidden\">" +                
                "<div id=\"tabela_desc_" + id + "\" style=\"line-height:27px; float:left\">" + $("#txtGrupoSel option:selected").text() + "</div>" +
                "<div style=\"width: 5%; padding: 4px 0px 3px; float: right; cursor: pointer;\" onclick=\"return removeLinha('" + id + "');\"><span class=\"ico-remove\" style=\"font-size:20px\"></span></div></div>";
                $("#divContatos").append(linha);  
                $("#txtContatosTotal").val(id);
                $("#txtGrupoSel").val("null");
            }
        }
        
        function validar() {
            if ($('#txtGrupoSel').val() === "null") {
                bootbox.alert("Selecione um grupo!");
                return false;
            }
            return true;
        }

        function removeLinha(id) {
            $("#tabela_linha_" + id).remove();
        }
        
        $(document).ready(function () {
            CKEDITOR.replace("ckeditor", {removePlugins: "elementspath", height: "128px"});
            $("#txtPreco_venda").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"], centsLimit: 4});
        });
        
        jQuery.validator.addMethod("notEqual", function (value, element, param) {
            return this.optional(element) || value !== param;
        }, "Please specify a different (non-default) value");
        $("#frmEnviaDados").validate({
            errorPlacement: function (error, element) {
                return true;
            },
            rules: {
                txtDescricao: {required: true}
            }
        });                                    
    </script>    
    <?php odbc_close($con); ?>
</body>