<?php
include "../../Connections/configini.php";
$PegaURL = "";
$CheckOrcamentoImagem = '0';
$adm_associado = "Cliente";

if ($_GET["crt"] != "") {
    if (is_numeric($_GET["crt"])) {
        $contrato2 = $_GET["crt"];
    }
}

$cur = odbc_exec($con, "select * from dbo.sf_configuracao");
while ($RFP = odbc_fetch_array($cur)) {
    $CheckOrcamentoImagem = $RFP['FIN_ORC_FOTO'];
    $adm_associado = utf8_encode($RFP['adm_associado']);
}

$cur = odbc_exec($con, "select A.assinatura from sf_usuarios A where A.login_user = '" . $_SESSION["login_usuario"] . "'");
while ($RFP = odbc_fetch_array($cur)) {
    $Assinatura = $RFP['assinatura'];
}

if ($_GET["id"] != "") {
    $PegaURL = $_GET["id"];
    $cur = odbc_exec($con, "select id_requisicao, empresa,
    (select max(nome) from sf_usuarios where login_user = responsavel) responsavel, 
    (select max(nome_departamento) from sf_departamentos d where d.id_departamento = r.id_departamento) departamento, 
    data_venda, comentarios_venda, destinatario, historico_venda, status, msg_reprov
    from sf_requisicao r where id_requisicao = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_requisicao'];
        $empresa = utf8_encode($RFP['empresa']);
        $vendedor = utf8_encode($RFP['responsavel']);
        $departamento = utf8_encode($RFP['departamento']);
        $historico_venda = utf8_encode($RFP['historico_venda']);
        $data_venda = escreverData($RFP['data_venda']);
        $comentarios_venda = utf8_encode($RFP['comentarios_venda']);
        $destinatario = utf8_encode($RFP['destinatario']);
        $status = utf8_encode($RFP['status']);
        $msg_reprov = utf8_encode($RFP['msg_reprov']);
    }
} else {
    $id = '';
    $empresa = $_SESSION["filial"];
    $vendedor = $_SESSION["login_usuario"];    
    $departamento = '';
    $historico_venda = '';
    $data_venda = getData("T");
    $comentarios_venda = '';
    $destinatario = $_SESSION["login_usuario"];
    $status = 'Aguarda';
    $msg_reprov = '';
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />        
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <style type="text/css">
            body {
                margin:0px;
                padding:0px;
            }
            table {
                font: 12px Arial;
                margin-bottom: 10px;
                border: 1px solid #999;
                border-collapse: collapse;
            }
            .modelo {
                height: 50px;
                font: 14px Arial;
                font-weight: Bold;
            }
            .titulo {
                font: 14px Arial;
                font-weight: Bold;
                padding: 2px;
                padding-left: 5px;
            }
            .tabela {
                height: 18px;
                padding-left: 5px;
                padding-right: 5px;
            }
            .assinatura {
                font: 12px Arial;
                position:absolute;
                bottom:0;
                width:100%;
            }
            .assinatura p{
                line-height: 1px;
            }
        </style>
    </head>
    <body>
        <div id="main" style="height:880px; overflow:hidden; margin-bottom:100px">
            <div id="cont">
                <table width="100%" border="1">
                    <tr>
                        <td width="540px" rowspan="3" style="padding-right:5px;background:url('./../../Pessoas/<?php echo $_SESSION["contrato"]; ?>/Empresa/logo_.jpg') no-repeat;background-position:20px 15px ;background-size: 150px 50px;"><div style="float:right; text-align:right;font-size: 9px;"><?php echo $_SESSION["cabecalho"]; ?></div></td>
                        <td width="200px" class="modelo" colspan="2" align="center">Requisição</td>
                    </tr>
                    <tr>
                        <td width="100px" style="padding-left:5px">Número:</td>
                        <td width="100px" align="center"><?php echo $id . "/" . substr($data_venda, -4); ?></td>
                    </tr>
                    <tr>
                        <td style="padding-left:5px">Data:</td>
                        <td align="center"><?php echo $data_venda; ?></td>
                    </tr>
                </table>
                <table width="100%" border="1">
                    <tr>
                        <td width="50%" style="padding-left:5px"><b>Responsável pela Requisição</b> <?php echo $vendedor; ?></td>
                        <td width="50%" style="padding-left:5px"><b>Departamento:</b> <?php echo $departamento; ?></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left:5px"><b>Histórico:</b> <?php echo $historico_venda; ?></td>
                    </tr>
                </table>
                <table width="100%" border="1" style="margin-bottom: 0;">
                    <tr style="background-color: lightgrey">
                        <td class="titulo" colspan="6">Produtos</td>
                    </tr>
                    <tr>
                        <td width="20%" align="center">Grupos de Produtos</td>
                        <td width="60%" align="center">Descrição do Produto</td>
                        <td width="10%" align="center">Und.</td>
                        <td width="10%" align="center">Qtd</td>
                    </tr>
                    <?php
                    if ($PegaURL != "") {
                        $i = 0;
                        $cur = odbc_exec($con, "select id_item, produto, quantidade, p.descricao, c.descricao grupo, unidade_comercial 
                                                from sf_requisicao_itens i inner join sf_produtos p on i.produto = p.conta_produto
                                                left join sf_contas_movimento c on c.id_contas_movimento = p.conta_movimento
                                                where i.id_requisicao = " . $PegaURL . " and p.tipo = 'P' order by id_item");
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <tr>
                                <td class="tabela"><?php echo utf8_encode($RFP["grupo"]); ?></td>
                                <td class="tabela"><?php echo utf8_encode($RFP["descricao"]); ?></td>
                                <td class="tabela" align="center"><?php echo $RFP["unidade_comercial"]; ?></td>                                                                                                
                                <td class="tabela" align="center"><?php echo escreverNumero($RFP['quantidade']); ?></td>                                
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                    <tr>
                        <td class="tabela" colspan="3">Total</td>
                        <td class="tabela"><div style="float:right"><?php echo $i; ?></div></td>
                    </tr>                    
                </table>
                <br>
                <table width="100%" border="1" style="margin-bottom: 0;">
                    <tr style="background-color: lightgrey">
                        <td class="titulo" colspan="6">Serviços</td>
                    </tr>
                    <tr>
                        <td width="20%" align="center">Grupos de Serviços</td>
                        <td width="70%" align="center">Descrição do Serviço</td>
                        <td width="10%" align="center">Qtd</td>
                    </tr>
                    <?php
                    if ($PegaURL != "") {
                        $i = 0;
                        $cur = odbc_exec($con, "select id_item, produto, quantidade, p.descricao, c.descricao grupo, unidade_comercial 
                                                from sf_requisicao_itens i inner join sf_produtos p on i.produto = p.conta_produto
                                                left join sf_contas_movimento c on c.id_contas_movimento = p.conta_movimento
                                                where i.id_requisicao = " . $PegaURL . " and p.tipo = 'S' order by id_item");
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <tr>
                                <td class="tabela"><?php echo utf8_encode($RFP["grupo"]); ?></td>
                                <td class="tabela"><?php echo utf8_encode($RFP["descricao"]); ?></td>                                                                                                
                                <td class="tabela" align="center"><?php echo escreverNumero($RFP['quantidade']); ?></td>                                
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                    <tr>
                        <td class="tabela" colspan="2">Total</td>
                        <td class="tabela"><div style="float:right"><?php echo $i; ?></div></td>
                    </tr>                    
                </table>
                <br>
                <table width="100%" border="1">
                    <tr style="background-color: lightgrey">
                        <td width="100%" align="center"><b>Comentários</b></td>
                    </tr>
                    <tr>
                        <td class="tabela"><?php echo nl2br($comentarios_venda); ?></td>
                    </tr>
                </table>                
            </div>
        </div>
        <div id="foot_0" style="width:100%; position:absolute; top:950px;">
            <table width="100%" border="1">
                <tr>
                    <td width="50%" style="height:50px"></td>
                    <td width="50%"></td>
                </tr>
                <tr>
                    <td align="center">Visto Responsável</td>
                    <td align="center">Visto Autorizador</td>
                </tr>
                <tr>
                    <td colspan="2" style="font-size:8px"><div style="width:10%; float:left">&nbsp;</div><div style="width:80%; float:left; text-align:center">Gerado pelo sistema SIVIS Finance - www.sivis.com.br</div><div id="pag" style="width:10%; float:left; text-align:right">Página 1/1</div></td>
                </tr>
            </table>
        </div>
    </body>
    <script type="text/javascript" src="../../js/util.js"></script>      
    <script type="text/javascript">
        $(document).ready(function () {
            if ($("#main").height() < $("#cont").height()) {
                var maxloop = Math.ceil($("#cont").height() / $("#main").height());
                for (i = 1; i < maxloop; i++) {
                    var copy = $("#cont").clone().attr("id", "copy").css({"margin-top": "-" + ($("#main").height() * i) + "px", "height": $("#main").height() * (i + 1) + "px"});
                    $("body").append("<div id=\"copy_" + i + "\" style=\"height:895px; overflow:hidden; page-break-before:always\"></div>");
                    $("#copy_" + i).append(copy);
                    var foot = $("#foot_0").clone().attr("id", "foot_" + i).css({"top": 888 + (980 * i) + "px"});
                    $("body").append(foot);
                    $("#foot_0 #pag").html("Página 1/" + maxloop);
                    $("#foot_" + i + " #pag").html("Página " + (i + 1) + "/" + maxloop);
                }
            }
        });        
        
        $(window).load(function () {
            window.print();
            setTimeout(function () {
                window.close();
            }, 500);
        });
    </script>
    <?php odbc_close($con); ?>
</html>