<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include ('../../Connections/configini.php');
$mdl_aca_ = returnPart($_SESSION["modulos"], 6);
$mdl_seg_ = returnPart($_SESSION["modulos"], 14);
$local = array();

if ($_REQUEST['txtTipo'] == 1) {
    if ($mdl_aca_ == 1 && $mdl_seg_ == 0) {
        $query = "select id_fornecedores_despesas login_user,razao_social nome from sf_fornecedores_despesas f
        where f.inativo = 0 and dt_demissao is null and f.tipo = 'E' and (tbcomiss_prod <> 0 or tbcomiss_serv <> 0 or tbcomiss_plano <> 0) and grupo_pessoa = 16 order by razao_social";
    } else if ($mdl_seg_ == 1) {
        $query = "select login_user,nome from sf_usuarios u 
        inner join sf_fornecedores_despesas f on u.funcionario = f.id_fornecedores_despesas  
        where f.inativo = 0 and dt_demissao is null and f.tipo = 'I' and (tbcomiss_prod <> 0 or tbcomiss_serv <> 0)";        
    } else {
        $query = "select id_fornecedores_despesas login_user,razao_social nome from sf_fornecedores_despesas f
        where f.inativo = 0 and dt_demissao is null and f.tipo = 'E' and (tbcomiss_prod <> 0 or tbcomiss_serv <> 0 ) order by razao_social";
    }
} elseif ($_REQUEST['txtTipo'] == 2) {
    if ($mdl_aca_ == 1) {
        $query = "SELECT id_usuario login_user, nome FROM sf_usuarios u 
        INNER JOIN sf_fornecedores_despesas f ON u.funcionario = f.id_fornecedores_despesas  
        WHERE f.inativo = 0 AND dt_demissao IS NULL AND f.tipo = 'E' AND (tbcomiss_gere <> 0 or tbcomiss_serv <> 0 or tbcomiss_plano <> 0)";
    } else {
        $query = "select login_user,nome from sf_usuarios u 
        inner join sf_fornecedores_despesas f on u.funcionario = f.id_fornecedores_despesas  
        where f.inativo = 0 and dt_demissao is null and f.tipo = 'I' and (tbcomiss_prod <> 0 or tbcomiss_serv <> 0)";
    }
} elseif ($_REQUEST['txtTipo'] == 3) {
    $query = "select login_user,nome from sf_usuarios u 
    inner join sf_fornecedores_despesas f on u.funcionario = f.id_fornecedores_despesas  
    where f.inativo = 0 and dt_demissao is null and f.tipo = 'E' and (tbcomiss_gere <> 0)";
} else {
    $query = "select login_user,nome from sf_usuarios u 
    inner join sf_fornecedores_despesas f on u.funcionario = f.id_fornecedores_despesas  
    where f.inativo = 0 and dt_demissao is null and f.tipo = 'E' and (tbcomiss_prod <> 0 or tbcomiss_serv <> 0)";
}
$cur = odbc_exec($con, $query) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $local[] = array('login_user' => $RFP['login_user'], 'nome' => utf8_encode($RFP['nome']));
}
echo(json_encode($local));
odbc_close($con);
