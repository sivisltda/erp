<?php

include "../../Connections/configini.php";
$imagePath = "./../../Produtos/" . $contrato . "/";
$allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
$temp = explode(".", $_FILES["img"]["name"]);
$extension = end($temp);

if (in_array($extension, $allowedExts)) {
    if ($_FILES["img"]["error"] > 0) {
        $response = array(
            "status" => 'error',
            "message" => 'ERROR Return Code: ' . $_FILES["img"]["error"],
        );
        echo "Return Code: " . $_FILES["img"]["error"] . "<br>";
    } else {
        $filename = $_FILES["img"]["tmp_name"];
        list($width, $height) = getimagesize($filename);
        $novaFoto = "0_t.png";
        if (is_numeric($_GET['id'])) {
            $novaFoto = $_GET['id'] . "_t.png";
        }
        move_uploaded_file($filename, $imagePath . $novaFoto);
        if ($width > 600 || $height > 600) {
            if ($width > $height) {
                $w = 600;
                $h = ($w * $height) / $width;
            } else {
                $h = 600;
                $w = ($h * $width) / $height;
            }
            $image_p = imagecreatetruecolor($w, $h);
            $whiteBackground = imagecolorallocate($image_p, 255, 255, 255);
            imagefill($image_p, 0, 0, $whiteBackground);
            $what = getimagesize($imagePath . $novaFoto);
            switch (strtolower($what['mime'])) {
                case 'image/png':
                    $image = imagecreatefrompng($imagePath . $novaFoto);
                    break;
                case 'image/jpeg':
                    $image = imagecreatefromjpeg($imagePath . $novaFoto);
                    break;
                case 'image/gif':
                    $image = imagecreatefromgif($imagePath . $novaFoto);
                    break;
                default: die('image type not supported');
            }
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $w, $h, $width, $height);
            imagejpeg($image_p, $imagePath . $novaFoto, 100);
            imagedestroy($image_p);
            $width = $w;
            $height = $h;
        }
        $response = array(
            "status" => 'success',
            "url" => $imagePath . $novaFoto,
            "width" => $width,
            "height" => $height
        );
    }
} else {
    $response = array(
        "status" => 'error',
        "message" => 'Formato nao suportado.',
    );
}
print json_encode($response);
odbc_close($con);
