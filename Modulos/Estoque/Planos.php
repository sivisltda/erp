<?php
include '../../Connections/configini.php';
if (is_numeric($_GET["Del"])) {
    odbc_exec($con, "
        DELETE FROM sf_produtos_acrescimo WHERE id_produto = " . $_GET["Del"] . ";
        DELETE FROM sf_produtos_parcelas where id_produto = " . $_GET["Del"] . "; 
        DELETE FROM sf_produtos WHERE conta_produto = " . $_GET["Del"]);
    $cur = odbc_exec($con, "select conta_produto from sf_produtos where conta_produto =" . $_GET["Del"]);
    $idProd = 0;
    while ($RFP = odbc_fetch_array($cur)) {
        $idProd = $RFP['conta_produto'];
    }
    if ($idProd != 0) {
        echo "<script>alert('Impossivel excluir, Registro esta sendo usado!');</script>";
    } else {
        echo "<script>alert('Registro excluido com sucesso!');window.top.location.href = 'Planos.php';</script>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script> 
        <style>            
            #example td {
                padding: 5px;
                line-height: 16px;
            }
        </style>     
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">      
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1> <?php if ( $mdl_seg_ == 1) { echo 'Proteção'; } else { echo $mdl_clb_ == 1 ? "Configuração" : "Academia"; }?> <small>Planos</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="margin-top: 15px;float:left;">
                                    <div style="float:left">
                                        <form method="POST">
                                            <button  class="button button-green btn-primary" type="button" onClick="AbrirBox(0, 0)"><span class="ico-file-4 icon-white"></span></button>
                                            <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir('I')" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                            <button id="btnExcel" class="button button-blue btn-primary" type="button" onclick="imprimir('E')" title="Exportar Excel"><span class="ico-download-3"></span></button>                                            
                                            <button  class="button button-blue btn-primary" type="button" onClick="AbrirBoxX(0)"><span class="icon-plus icon-white"></span></button>
                                        </form>
                                    </div>
                                </div>
                                <div style="float:right;width: 40%;display: flex;align-items: center;justify-content: flex-end;">
                                    <div style="float:left;margin-left: 1%;">                                    
                                        <div width="100%">Estado:</div>
                                        <select id="txtStatus" class="select">
                                            <option value="0" selected>Ativos</option>                                        
                                            <option value="1">Inativos</option>                                                                            
                                        </select>                                           
                                    </div>
                                    <div style="float:left;margin-left: 1%;">                                    
                                        <div width="100%">Grupo:</div>
                                        <select name="txtGrupo" id="txtGrupo" class="select input-medium" style="width:270px; height:31px">
                                            <option value="null">Selecione</option>
                                            <?php $sql = "select id_contas_movimento,descricao from sf_contas_movimento where tipo = 'L' and inativa = 0 ORDER BY descricao";
                                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id_contas_movimento'] ?>"><?php echo $RFP['id_contas_movimento'] . " - " . utf8_encode($RFP["descricao"]) ?></option>
                                            <?php } ?>
                                        </select>                                           
                                    </div>                                    
                                    <div style="float:left;margin-left: 1%;width: 56%;">
                                        <div width="100%">Busca:</div>
                                        <input id="txtBusca" maxlength="100" type="text" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title=""/>
                                    </div>
                                    <div style="float:left;margin-left: 1%;width: 7%;">
                                        <div width="100%" style="opacity:0;">Busca:</div>
                                        <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Planos</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="example">
                            <thead>
                                <tr>
                                    <th width="20%">Descrição do Grupo</th>
                                    <th width="<?php echo ($mdl_seg_ == 1 ? "35" : "65"); ?>%">Descrição do Plano</th>
                                    <?php if ( $mdl_seg_ == 1) { ?>
                                    <th width="15%">Valor Minimo Carro</th>
                                    <th width="15%">Valor Máximo Carro</th>
                                    <?php } ?>
                                    <th width="10%">Preço</th>
                                    <th width="5%"><center>Ação</center></th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>

                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script> 
        <script type="text/javascript" src="../../js/util.js"></script>       
        <script type="text/javascript">
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }

            function finalFind(imp) {
                var retPrint = (imp === 1 ? '&imp=1' : '?imp=0');                
                if ($("#txtStatus").val() !== "null") {
                    retPrint = retPrint + '&at=' + $("#txtStatus").val();
                }
                if ($("#txtBusca").val() !== "") {
                    retPrint = retPrint + '&txtBusca=' + $("#txtBusca").val();
                }
                if ($("#txtGrupo").val() !== "null") {
                    retPrint = retPrint + '&gp=' + $("#txtGrupo").val();
                }                
                return retPrint.replace(/\//g, "_");
            }
            
            function AbrirBoxX(tp) {
                abrirTelaBox("FormMensalidades.php", 500, 500);
            }

            function AbrirBox(id, tp) {
                abrirTelaBox("FormPlanos.php" + (id > 0 ? "?id=" + id : ""), 508, 712);
            }
            
            function FecharBox() {
                var oTable = $("#example").dataTable();
                oTable.fnDraw(false);
                $("#newbox").remove();
            }

            function imprimir(imp) {
                var pRel = "&NomeArq=Planos&lbl=Descrição do Grupo|Descrição do Plano|Valor Minimo Carro|Valor Máximo Carro|Preço" +
                (imp === "E" ? "|Valores|Acessórios|Acréscimos|Serviços de Adesão|Vender Plano no site|Favorito|Plano Ativo em Oficina|Parcelar de acordo com número de meses (Cartão)|Convênios de Adesão|Validade Convênios (Meses)|Grupo de Cidades|Grupo de Fabricantes|Tipo Veículo|Limite anos|(Mensalidade + Taxa de Adesão)|COTA" : "") +
                "&siz=140|200|120|120|120" + (imp === "E" ? "|150|250|50|150|70|70|70|70|70|70|70|70|70|70|70|70" : "") +
                "&pdf=100" + // Colunas do server processing que não irão aparecer no pdf
                "&filter=" + //Label que irá aparecer os parametros de filtro
                "&PathArqInclude=" + "../Modulos/Estoque/Planos_server_processing.php" + finalFind(1).replace("?", "&"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + imp + "&PathArq=GenericModelPDF.php", '_blank');
            }

            $(document).ready(function () {
                listaTable();
                $("#btnfind").click(function () {
                    $("#example").dataTable().fnDestroy();
                    listaTable();
                });

                function listaTable() {
                    $("#example").dataTable({
                        "iDisplayLength": 20,
                        "aLengthMenu": [20, 30, 40, 50, 100],
                        "bProcessing": true,
                        "bServerSide": true,
                        "bFilter": false,
                        "bSort": false,
                        "sAjaxSource": "Planos_server_processing.php" + finalFind(0),
                        "oLanguage": {
                            "oPaginate": {
                                "sFirst": "Primeiro",
                                "sLast": "Último",
                                "sNext": "Próximo",
                                "sPrevious": "Anterior"
                            }, "sEmptyTable": "Não foi encontrado nenhum resultado",
                            "sInfo": "Visualização do registro _START_ ao _END_ de _TOTAL_",
                            "sInfoEmpty": "Visualização do registro 0 ao 0 de 0",
                            "sInfoFiltered": "(filtrado de _MAX_ registros totais)",
                            "sLengthMenu": "Visualização de _MENU_ registros",
                            "sLoadingRecords": "Carregando...",
                            "sProcessing": "Processando...",
                            "sSearch": "Pesquisar:",
                            "sZeroRecords": "Não foi encontrado nenhum resultado"},
                        "sPaginationType": "full_numbers"
                    });
                }
            });
        </script>
        <style type="text/css">
            .fancybox-custom .fancybox-skin {
                box-shadow: 0 0 50px #069;
            }
        </style>        
        <?php odbc_close($con); ?>
    </body>
</html>
