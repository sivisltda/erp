<?php

require_once(__DIR__ . '/../../Connections/configini.php');
$aColumns = array('id_contas_movimento', 'tipo', 'sf_contas_movimento.descricao', 'sf_contas_movimento.descricao', 'terminal', 'inativa');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY nivel asc, id_contas_movimento asc ";
$sLimit = 20;
$imprimir = 0;

$tipo = "'S','P','A'";
if (isset($_GET['tp'])) {
    if ($_GET['tp'] == '0') {
        $tipo = "'P'";
        $finalTp = "&tp=0";
    } else if ($_GET['tp'] == '1') {
        $tipo = "'S'";
        $finalTp = "&tp=1";
    } else if ($_GET['tp'] == '2') {
        $tipo = "'A'";
        $finalTp = "&tp=2";
    }
}

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";

    if ($_GET['iSortCol_0'] == 2) {
        $sOrder = " order by sf_contas_movimento.descricao " . $_GET['sSortDir_0'] . ", ";
    }
    if ($_GET['iSortCol_0'] == 3) {
        $sOrder = " order by terminal " . $_GET['sSortDir_0'] . ", ";
    }

    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY nivel asc, id_contas_movimento asc";
    }
}

if ($_GET['txtBusca'] != "") {
    $sWhereX .= " and (sf_contas_movimento.descricao like '%" . $_GET['txtBusca'] . "%')";
}

if (is_numeric($_GET['txtTipo'])) {
    $sWhereX .= " and sf_veiculo_tipos.id = " . $_GET['txtTipo'];
}

if (is_numeric($_GET['txtInativo'])) {
    $sWhereX .= " and sf_contas_movimento.inativa = " . $_GET['txtInativo'];
}

$sQuery = "SELECT COUNT(*) total FROM sf_contas_movimento 
inner join sf_grupo_contas on grupo_conta = id_grupo_contas
left join sf_veiculo_tipos on veiculo_tipo = id    
where deb_cre in (" . $tipo . ") $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total FROM sf_contas_movimento 
inner join sf_grupo_contas on grupo_conta = id_grupo_contas 
left join sf_veiculo_tipos on veiculo_tipo = id    
where deb_cre in (" . $tipo . ") $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,sf_contas_movimento.*, sf_grupo_contas.descricao as descr,deb_cre,sf_veiculo_tipos.descricao veiculo
FROM sf_contas_movimento inner join sf_grupo_contas on grupo_conta = id_grupo_contas 
left join sf_veiculo_tipos on veiculo_tipo = id
where deb_cre in (" . $tipo . ") " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1=" . $imprimir . " ";
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    if ($_GET['tp'] == '2') {
        $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow["nivel"]) . "'>" . utf8_encode($aRow["nivel"]) . "</div></center>";
    } else {
        $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow["tipo"]) . "'>" . utf8_encode($aRow["tipo"]) . "</div></center>";
    }
    $row[] = "<div id='formPQ' title='" . utf8_encode($aRow["descr"]) . "'>" . utf8_encode($aRow["descr"]) . "</div>";
    if ($aRow['editavel'] == 0) {
        $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(2," . utf8_encode($aRow[$aColumns[0]]) . ")'><div id='formPQ' title='" . utf8_encode($aRow["descricao"]) . "'>" . utf8_encode($aRow["descricao"]) . "</div></a>";
    } else {
        $row[] = "<strong><div id='formPQ' title='" . utf8_encode($aRow["descricao"]) . "'>" . utf8_encode($aRow["descricao"]) . "</div></strong>";
    }
    if ($_GET['tp'] == '2') {
        $row[] = utf8_encode($aRow["veiculo"]);        
    }
    $row[] = "<center>" . ($aRow[$aColumns[4]] == 1 ? "Sim" : "Não") . "</center>";
    $row[] = "<center>" . ($aRow[$aColumns[5]] == 1 ? "Sim" : "Não") . "</center>";
    if ($imprimir == 0) {
        if ($aRow['editavel'] == 0) {
            $row[] = "<center><a title='Excluir' href='Grupo-de-estoque.php?Del=" . $aRow[$aColumns[0]] . $toReturnAnd . $finalTp . "'><input name='' type='image' onClick=\"return confirm('Deseja deletar esse registro?')\" src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
        } else {
            $row[] = "";
        }
    }
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
