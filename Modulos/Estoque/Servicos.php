<?php
include '../../Connections/configini.php';

if (is_numeric($_GET["Del"])) {
    odbc_exec($con, "DELETE FROM sf_produtos WHERE conta_produto = " . $_GET["Del"]);
    if (odbc_error()) {
        echo "<script>alert('Não é possível excluir um serviço que esteja atrelado à uma venda.');</script>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />        
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>                     
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"><span class="ico-arrow-right"></span></div>
                        <h1>Serviços<small>Serviços</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="margin-top: 15px;float:left;">
                                    <div style="float:left">
                                        <form method="POST">
                                            <button  class="button button-green btn-primary" type="button" onClick="AbrirBox(0)"><span class="ico-file-4 icon-white"></span></button>
                                            <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir('I')" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                            <button id="btnExcel" class="button button-blue btn-primary" type="button" onclick="imprimir('E')" title="Exportar Excel"><span class="ico-download-3"></span></button>                                                                                        
                                        </form>
                                    </div>
                                </div>
                                <div style="float: right;width: 50%; display: flex;align-items: center;justify-content: flex-end;">
                                    <div style="float:left;margin-left: 1%;">                                    
                                        <div width="100%">Tipo:</div>
                                        <select name="txtAtivo" id="txtAtivo" class="select input-medium" style="width:150px; height:31px">
                                            <option value="0">Ativos</option>
                                            <option value="1">Inativos</option>
                                        </select>                                           
                                    </div>                                    
                                    <div style="float:left;margin-left: 1%;">                                    
                                        <div width="100%">Grupo:</div>
                                        <select name="txtGrupo" id="txtGrupo" class="select input-medium" style="width:250px; height:31px">
                                            <option value="null">Selecione</option>
                                            <?php $sql = "select id_contas_movimento,descricao from sf_contas_movimento where tipo = 'S' ORDER BY descricao";
                                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id_contas_movimento'] ?>"><?php echo utf8_encode($RFP["descricao"]) ?></option>
                                            <?php } ?>
                                        </select>                                           
                                    </div>                                    
                                    <div style="float:left;margin-left: 1%;">
                                        <div width="100%">Busca:</div>
                                        <input name="txtBusca"  id="txtBusca" maxlength="100" type="text" style="width:175px;" onkeypress="TeclaKey(event)" value=""/>
                                    </div>
                                    <div style="margin-top: 15px;float:left;margin-left:0.3%;">
                                        <div style="float:left">
                                            <button class="button button-turquoise btn-primary" type="button" id="btnfind" title="Buscar" onclick="refresh()"><span class="ico-search icon-white"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Serviços</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="example">
                            <thead>
                                <tr>
                                    <th width="8%">Código</th>                                    
                                    <th width="20%">Descrição do Grupo</th>
                                    <th width="35%">Descrição do Serviço</th>
                                    <th width="8%">Ativar Convite</th>
                                    <th width="8%">Alterar Valor</th>                                    
                                    <th width="8%">Custo</th>
                                    <th width="8%">Valor Venda</th>
                                    <th width="5%"><center>Ação:</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                    <div class="widgets"></div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/GoBody/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>        
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>       
        <script type="text/javascript">
            
            $('#example').dataTable({
                "iDisplayLength": 20,
                "aLengthMenu": [20, 30, 40, 50, 100],
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": finalFind(0),
                "bFilter": false,
                "ordering": false,
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"},
                "sPaginationType": "full_numbers"
            });
            
            function finalFind(imp) {
                var retPrint = "";
                retPrint = '?imp=0';
                if (imp === 1) {
                    retPrint = '?imp=1';
                }
                if ($("#txtBusca").val() !== "") {
                    retPrint = retPrint + '&sSearch=' + $("#txtBusca").val();
                }
                if ($("#txtAtivo").val() !== "null") {
                    retPrint = retPrint + '&at=' + $("#txtAtivo").val();
                }                
                if ($("#txtGrupo").val() !== "null") {
                    retPrint = retPrint + '&gp=' + $("#txtGrupo").val();
                }                
                return "Servicos_server_processing.php" + retPrint.replace(/\//g, "_");
            }            
            
            function refresh() {
                var oTable = $("#example").dataTable();
                oTable.fnReloadAjax(finalFind(0));
            }            

            function imprimir(tp) {
                var pRel = "&NomeArq=" + "Serviços" +
                "&lbl=Código|Descrição do Grupo|Descrição do Serviço|Ativar Convite|Alterar Valor|Custo|Valor Venda" +
                (tp === "E" ? "|Comissão Venda|Unidade|Inativo|Favorito|Descrição" : "") +
                "&siz=60|100|490|90|90|90|90" + (tp === "E" ? "|90|90|90|90|90" : "") +
                "&pdf=12" + // Colunas do server processing que não irão aparecer no pdf
                "&filter=" + //Label que irá aparecer os parametros de filtro
                "&PathArqInclude=" + "../Modulos/Estoque/" + finalFind(1).replace("?", "&"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tp + "&pOri=L&PathArq=GenericModelPDF.php", '_blank');
            }
                        
            function AbrirBox(id) {
                abrirTelaBox("FormServicos.php" + (id > 0 ? "?id=" + id : ""), 390, 700);
            } 
            
            function FecharBox() {
                refresh();
                $("#newbox").remove();
            }                        
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    refresh();
                }
            }
                        
        </script>             
        <?php odbc_close($con); ?>
    </body>
</html>
