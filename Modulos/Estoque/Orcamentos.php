<?php
include "../../Connections/configini.php";

$imprimir = 0;
$F1 = '';
$F2 = '';
$F3 = '';
$DateBegin = '';
$DateEnd = '';
$sendURL = "";

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = $_GET['imp'];
}

if (is_numeric($_GET['id'])) {
    $F1 = $_GET['id'];
} else {
    $F1 = "4";
}

if (is_numeric($_GET['tp'])) {
    $F2 = $_GET['tp'];
} else {
    $F2 = "null";
}

if ($_GET['td'] != '') {
    $F3 = $_GET['td'];
} else {
    $F3 = $_SESSION["login_usuario"];
}

if ($_GET['dti'] != '') {
    $DateBegin = str_replace("_", "/", $_GET['dti']);
}

if ($_GET['dtf'] != '') {
    $DateEnd = str_replace("_", "/", $_GET['dtf']);
}

if (isset($_POST['btnAprov'])) {
    $items = $_POST['items'];
    for ($i = 0; $i < sizeof($items); $i++) {
        if (is_numeric($items[$i])) {
            odbc_exec($con, "update sf_vendas set status = 'Aprovado', data_aprov = getdate() where id_venda = '" . $items[$i] . "'") or die(odbc_errormsg());
        }
    }
}

if (isset($_POST['btnCance'])) {
    $items = $_POST['items'];
    for ($i = 0; $i < sizeof($items); $i++) {
        if (is_numeric($items[$i])) {
            odbc_exec($con, "update sf_vendas set status = 'Reprovado', data_aprov = getdate() where id_venda = '" . $items[$i] . "'") or die(odbc_errormsg());
        }
    }
}

$sendURL = "?imp=" . $imprimir . "&id=" . $F1 . "&tp=" . $F2 . "&td=" . $F3 . "&dti=" . str_replace("/", "_", $DateBegin) . "&dtf=" . str_replace("/", "_", $DateEnd);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/bootbox.css"/>        
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>        
    </head>
    <body>
        <?php if ($imprimir == 0) { ?>
            <div id="loader"><img src="../../img/loader.gif"/></div><?php } ?>
        <div class="wrapper">
            <?php
            if ($imprimir == 0) {
                include('../../menuLateral.php');
            }
            ?>
            <div <?php if ($imprimir == 0) { ?>class="body"<?php } ?>>
                <?php
                if ($imprimir == 0) {
                    include("../../top.php");
                }
                ?>
                <div class="content">
                    <form method="POST" action="Orcamentos.php<?php echo $sendURL; ?>">
                        <div class="page-header">
                            <?php if ($imprimir == 0) { ?>
                                <div class="icon"> <span class="ico-arrow-right"></span> </div>
                                <h1>Financeiro<small>Orçamentos</small></h1>
                            <?php } ?>
                        </div>
                        <?php if ($imprimir == 0) { ?>                            
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="block">
                                        <div id="formulario">       
                                            <span style="padding-left:0px">
                                                <button id="btnNovo" class="button button-green btn-primary" type="button" onClick="javascript:location.href = 'FormOrcamentos.php'"><span class="ico-file-4 icon-white"></span></button>
                                                <button id="btnPrint" class="button button-blue btn-primary" type="button" onClick="imprimir()"><span class="ico-print icon-white"></span></button>
                                                <?php if ($F1 == "1" && $imprimir == 0) { ?>
                                                    <button id="btnAprov" name="btnAprov" onClick="return confirm('Deseja efetuar a aprovação das contas selecionadas?')" class="button button-green btn-primary" type="submit" disabled="true" title="Aprovar"><span class="ico-ok"></span></button>
                                                    <button id="btnCance" name="btnCance" onClick="return confirm('Deseja efetuar a reprovação das contas selecionadas?')" class="button button-red btn-primary" type="submit" disabled="true" title="Cancelar"><span class="ico-cancel"></span></button>
                                                <?php } ?>                                            
                                                <select class="select" style="width:128px" name="txtEstado" id="txtEstado" >
                                                    <option value="null" >Selecione</option>
                                                    <option value="1" <?php
                                                    if ($F1 == "1") {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Aguardando</option>                                                
                                                    <option value="2" <?php
                                                    if ($F1 == "2") {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Aprovadas</option>                                                
                                                    <option value="3" <?php
                                                    if ($F1 == "3") {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Reprovadas</option>                                                
                                                    <option value="5" <?php
                                                    if ($F1 == "5") {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Ativas</option>
                                                    <option value="6" <?php
                                                    if ($F1 == "6") {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Favoritos</option>
                                                    <option value="4" <?php
                                                    if ($F1 == "4") {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Todas</option>                                                
                                                </select>
                                                <select class="select" style="width:128px" name="txtTipoBusca" id="txtTipoBusca" >
                                                    <option value="null" <?php
                                                    if ($F2 == "null") {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>--Selecione--</option>
                                                    <option value="0" <?php
                                                    if ($F2 == "0") {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Responsável</option>
                                                    <option value="1" <?php
                                                    if ($F2 == "1") {
                                                        echo "SELECTED";
                                                    }
                                                    ?>>Vendedor</option>                                                
                                                </select>                                             
                                                <select class="select" style="width:170px" name="txtUsuario" id="txtUsuario" >
                                                    <option value="null" >--Selecione--</option>
                                                    <?php
                                                    $cur = odbc_exec($con, "select login_user id_usuario,nome from sf_usuarios where inativo = 0 order by nome") or die(odbc_errormsg());
                                                    while ($RFP = odbc_fetch_array($cur)) {
                                                        ?>
                                                        <option value="<?php echo $RFP['id_usuario']; ?>"<?php
                                                        if (!(strcmp($RFP['id_usuario'], $F3))) {
                                                            echo "SELECTED";
                                                        }
                                                        ?>><?php echo utf8_encode($RFP['nome']); ?></option>
                                                            <?php } ?>                                                    
                                                </select> 
                                                De <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial"> 
                                                até <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final">
                                            </span>
                                            <span style="float:right">
                                                <input name="txtBuscar" id="txtBuscar" type="text" value="" style="width:130px; height:31px"/>
                                                <button class="button button-turquoise btn-primary" type="button" onclick="refresh()" id="btnPesquisar"><span class="ico-search icon-white"></span></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="block">
                                        <?php if ($imprimir == 1) {
                                            $titulo_pagina = "RELATÓRIO DE CONSULTA<br/>ORÇAMENTOS";
                                            include "../Financeiro/Cabecalho-Impressao.php";
                                        } else { ?>
                                            <div class="boxhead">
                                                <div class="boxtext">Orçamentos</div>
                                            </div>
                                        <?php } ?>
                                        <div class="boxtable">
                                            <table <?php
                                            if ($imprimir == 0) {
                                                echo "class=\"table\"";
                                            } else {
                                                echo "border=\"1\"";
                                            }
                                            ?> cellpadding="0" cellspacing="0" width="100%" id="tblOrcamento">
                                                <thead>
                                                    <tr>      
                                                        <th width="4%" style="top: 0"><input id="checkAll" type="checkbox" class="checkall" onclick="contaCheckbox()"/></th>
                                                        <th width="9%"><center>Data</center></th>
                                                        <th width="4%"><center>Loja</center></th>
                                                        <th width="4%"><center>Cod.</center></th>
                                                        <th width="4%"><center>Prev.Fech.</center></th>
                                                        <th width="15%">Cliente</th>
                                                        <th width="7%">Cidade</th>
                                                        <th width="2%">UF</th>
                                                        <th width="7%"><center>Vendedor</center></th>                                                    
                                                        <th width="9%" style="text-align:right">Tot.Prod.</th>
                                                        <th width="9%" style="text-align:right">Tot.Serv.</th>
                                                        <th width="10%" style="text-align:right">Tot.Geral</th>
                                                        <th width="6%"><center>Status:</center></th>
                                                    </tr>
                                                </thead>
                                                <tbody>  
                                                    <tr>
                                                        <td colspan="5" class="dataTables_empty">Carregando dados do Cliente</td>
                                                    </tr>   
                                                </tbody>
                                            </table>
                                            <table id="lblTotParc" name="lblTotParc" class="table" cellpadding="0" cellspacing="0" width="100%">                                            
                                                <tr>
                                                    <td style="text-align: right"><div id="lblTotParc2" name="lblTotParc2">
                                                            Número de Orçamentos :<strong>   0</strong>
                                                        </div></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right"><div id="lblTotParc3" name="lblTotParc3">
                                                            Total em Orçamentos :<strong><?php echo escreverNumero(0,1); ?></strong></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right"><div id="lblTotParc4" name="lblTotParc4">
                                                            Ticket Médio :<strong><?php echo escreverNumero(0,1); ?></strong></div>            
                                                    </td>
                                                </tr>
                                            </table>                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="span3"></div>
                                <div class="span4"></div>
                            </div>
                        </div>                                                            
                    </form>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />                        
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>    
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>  
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>  
        <script type="text/javascript">
            
            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
            
            $(document).ready(function () {
                var columns = [{"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": true},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": true},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": false}
                ];
                $('#tblOrcamento').dataTable({
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": finalFind(0),
                    "bFilter": false,
                    "aoColumns": columns,
                    'oLanguage': {
                        'oPaginate': {
                            'sFirst': "Primeiro",
                            'sLast': "Último",
                            'sNext': "Próximo",
                            'sPrevious': "Anterior"
                        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                        'sLengthMenu': "Visualização de _MENU_ registros",
                        'sLoadingRecords': "Carregando...",
                        'sProcessing': "Processando...",
                        'sSearch': "Pesquisar:",
                        'sZeroRecords': "Não foi encontrado nenhum resultado"},
                    "sPaginationType": "full_numbers"
                });
            });

            function AbrirBox(id) {
                abrirTelaBox("FormOrcamentos.php" + (id > 0 ? "?id=" + id : ""), 525, 677);
            }

            function refresh() {
                var oTable = $('#tblOrcamento').dataTable();
                oTable.fnReloadAjax(finalFind(0));
            }

            function finalFind(imp) {
                var retorno = "?filial=" + $("#txtLojaSel").val() + "&imp=" + imp;
                if ($('#txtBuscar').val() !== "") {
                    retorno = retorno + "&Search=" + $("#txtBuscar").val();
                }
                if ($('select[id=txtEstado]').val() !== "4") {
                    retorno = retorno + "&id=" + $('select[id=txtEstado]').val();
                }
                if ($('select[id=txtTipoBusca]').val() !== "null") {
                    retorno = retorno + "&tp=" + $('select[id=txtTipoBusca]').val();
                }
                if ($('select[id=txtUsuario]').val() !== "null") {
                    retorno = retorno + "&td=" + $('select[id=txtUsuario]').val();
                }
                if ($("#txt_dt_begin").val() !== "") {
                    retorno = retorno + "&dti=" + $("#txt_dt_begin").val();
                }
                if ($("#txt_dt_end").val() !== "") {
                    retorno = retorno + "&dtf=" + $("#txt_dt_end").val();
                }
                return "./../Estoque/Orcamentos_server_processing.php" + retorno;
            }

            function FecharBox() {
                $("#newbox").remove();
                refresh();
            }

            function imprimir() {
                var pRel = "&NomeArq=" + "Orçamentos" +
                        "&lbl=" + "Data|Loja|Cod.|Prev.Fech.| Cliente|Cidade|UF|Vendedor|Tot.Prod|Tot.Geral|Status" +
                        "&siz=" + "65|30|30|65|170|65|30|65|60|60|60" +
                        "&pdf=" + "0|11" + // Colunas do server processing que não irão aparecer no pdf 
                        "&filter=" + "Orçamentos " + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + finalFind(1).replace("?", "&").replace("./../", "../Modulos/"); // server processing            
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }
        </script>
        <?php odbc_close($con);?>
    </body>
</html>