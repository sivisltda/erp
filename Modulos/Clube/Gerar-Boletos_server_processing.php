<?php

require_once(__DIR__ . '/../../Connections/configini.php');

$mdl_seg_ = returnPart($_SESSION["modulos"],14);

$aColumns = array('id_fornecedores_despesas', 'razao_social', 'dbo.VALOR_REAL_MENSALIDADE(id_mens) valor_mens', 'valor_total', 'bol_data_parcela',
    'bol_nosso_numero', 'cnpj', 'endereco', 'numero', 'bairro', 'cidade_nome', 'estado_sigla', 'cep', 'id_plano', 'id_boleto', 'descricao_grupo',
    "'a' + cast(id_mens as varchar) id_mens", 'dt_inicio_mens', 'exp_remessa', 'dt_pagamento_mens', 'datediff(year,data_nascimento, getdate()) idade');
$PageName = "Gerar-Boletos";
$iTotal = 0;
$iFilteredTotal = 0;
$aGroupBegin = "";
$aGroupEnd = "";
$sWhere = "";
$table2 = "";
$sWhereX = " and id_fornecedores_despesas > 0 ";
$sOrder = " ORDER BY dt_inicio_mens asc ";
$sOrder2 = " ORDER BY dt_inicio_mens asc ";
$sLimit = 20;
$imprimir = 0;
$sWhereDate = "";
$dependenteBoleto = "0";
$planosBoleto = "0";

$cur = odbc_exec($con, "select FIN_GRUPO_BOLETO,FIN_GRUPO_BOLETO_MOD from sf_configuracao") or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $dependenteBoleto = $RFP['FIN_GRUPO_BOLETO'];
}

if (isset($_GET['gp']) && $_GET['gp'] == "1") {
    $planosBoleto = "1";
}

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) < count($aColumns) - 1) {
                $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 0) . " " . $_GET['sSortDir_' . $i] . ", ";
                $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 1) . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY dt_inicio_mens asc";
        $sOrder2 = " ORDER BY dt_inicio_mens asc";
    }
}

if (isset($_GET['lt']) && $_GET['lt'] > 0 && $_GET['tp'] == "0") {
    $sWhereX = " and grupo_pessoa = " . valoresNumericos2($_GET['lt']);
}

if (isset($_GET['al']) && $_GET['al'] > 0 && ($_GET['tp'] == "1" || $_GET['tp'] == "2")) {
    $sWhereX = " and id_fornecedores_despesas = " . valoresNumericos2($_GET['al']);
}

if (isset($_GET['pl']) && $_GET['pl'] != '' && $_GET['pl'] != 'null') {
    $sWhereX .= ($mdl_seg_ == 0 ? " and id_prod_plano = " . valoresNumericos2($_GET['pl']) : " and vp.id_plano in (" . $_GET['pl'] . ")");
}

if (isset($_GET['bc']) && $_GET['bc'] != '') {
    $sWhere .= " and bol_id_banco = " . valoresNumericos2($_GET['bc']);
}

if (isset($_GET['cr']) && $_GET['cr'] != '') {
    $sWhere .= " and carteira_id = " . valoresNumericos2($_GET['cr']);
}

if (isset($_GET['tb']) && $_GET['tb'] != '0') {
    if ($_GET['tb'] == "1") {
        $sWhereX .= " and id_boleto is not null";
    } else if ($_GET['tb'] == "2") {
        $sWhereX .= " and id_boleto is null";
    } else if ($_GET['tb'] == "3") {
        $sWhereX .= " and exp_remessa = 1";
    } else if ($_GET['tb'] == "4") {
        $sWhereX .= " and exp_remessa = 0";
    } else if ($_GET['tb'] == "5") {
        $sWhereX .= " and exp_email = 1";
    } else if ($_GET['tb'] == "6") {
        $sWhereX .= " and exp_email = 0";
    } else if ($_GET['tb'] == "7") {
        $sWhereX .= " and exp_email = 2";
    } else if ($_GET['tb'] == "8") {
        $sWhereX .= " and id_boleto is not null and dt_pagamento_mens is not null";
    } else if ($_GET['tb'] == "9") {
        $sWhereX .= " and id_boleto is not null and dt_pagamento_mens is null";
    }
}
if (isset($_GET['dti']) && $_GET['dti'] != '' && isset($_GET['dtf']) && $_GET['dtf'] != '' && isset($_GET['td']) && $_GET['td'] == "0") {
    $sWhereDate = " and dt_inicio_mens between " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']);
} else if (isset($_GET['dti']) && $_GET['dti'] != '' && isset($_GET['dtf']) && $_GET['dtf'] != '' && isset($_GET['td']) && $_GET['td'] == "1") {
    $sWhereX .= " and bol_data_criacao between " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']);
}
//---------------------------------------------------------------------------------------------------------------------------
if ($planosBoleto > 0) { //agrupar todos os boletos do titular
    $aGroupBegin = "x.id_fornecedores_despesas,x.razao_social,
    sum(x.valor_mens) valor_mens,sum(x.valor_total) valor_total, max(bol_data_parcela) bol_data_parcela, max(bol_nosso_numero) bol_nosso_numero,
    x.cnpj,x.endereco,x.numero,x.bairro,x.cidade_nome,x.estado_sigla,x.cep, max(id_plano) id_plano, max(id_boleto) id_boleto, x.descricao_grupo,
    min(id_mens) id_mens, min(dt_inicio_mens) dt_inicio_mens, max(exp_remessa) exp_remessa, max(dt_pagamento_mens) dt_pagamento_mens,x.idade from ( select ";
    $aGroupEnd = ") as x group by x.id_fornecedores_despesas,x.razao_social,x.cnpj,x.endereco,x.numero,x.bairro,x.cidade_nome,x.estado_sigla,x.cep,x.descricao_grupo, month(dt_inicio_mens), year(dt_inicio_mens),x.idade";
}
$table1 = " sf_fornecedores_despesas a
left join sf_grupo_cliente on sf_grupo_cliente.id_grupo = a.grupo_pessoa 
left join tb_cidades on tb_cidades.cidade_codigo = a.cidade 
left join tb_estados on tb_estados.estado_codigo = a.estado
inner join sf_vendas_planos vp on vp.favorecido = a.id_fornecedores_despesas 
inner join sf_produtos p on vp.id_prod_plano = p.conta_produto
left join sf_vendas_planos_mensalidade m on vp.id_plano = m.id_plano_mens " . $sWhereDate . "
left join sf_boleto on id_referencia = m.id_mens and sf_boleto.inativo = 0 and tp_referencia = 'M' " . $sWhere . "
left join sf_vendas_itens vi on vi.id_item_venda = m.id_item_venda_mens
where a.tipo = 'C' and a.trancar = 0 and a.inativo = 0 " . $sWhereX . "
and p.inativa = 0 " . ($_GET['tp'] == 0 ? " and p.tp_valid_acesso in ('L','E') " : "") . " and vp.dt_cancelamento is null
and m.id_mens not in (select id_mens from sf_boleto_online_itens where id_mens is not null)
and isnull((select top 1 vpp.parcela from sf_vendas_planos_mensalidade vpm inner join sf_produtos_parcelas vpp on vpm.id_parc_prod_mens = vpp.id_parcela where vpm.id_plano_mens = vp.id_plano order by vpm.id_mens desc),0) > 0
and ((DATEADD(day,(select ACA_PLN_DIAS_RENOVACAO from sf_configuracao),vp.dt_fim) > GETDATE() and p.parcelar = 0) or (vp.dt_fim >= GETDATE() and p.parcelar = 1) or ((select COUNT(*) from sf_vendas_planos_mensalidade C where C.id_plano_mens = vp.id_plano and dt_pagamento_mens is null) > 0))";
if (($dependenteBoleto > 0 && $_GET['tp'] != "2")) {
    $table2 = " sf_fornecedores_despesas_dependentes fd
    inner join sf_fornecedores_despesas a on a.id_fornecedores_despesas = fd.id_titular
    left join sf_grupo_cliente on sf_grupo_cliente.id_grupo = a.grupo_pessoa 
    left join tb_cidades on tb_cidades.cidade_codigo = a.cidade 
    left join tb_estados on tb_estados.estado_codigo = a.estado
    inner join sf_vendas_planos vp on vp.favorecido = fd.id_dependente
    inner join sf_produtos p on vp.id_prod_plano = p.conta_produto
    left join sf_vendas_planos_mensalidade m on vp.id_plano = m.id_plano_mens " . $sWhereDate . "
    left join sf_boleto on id_referencia = m.id_mens and sf_boleto.inativo = 0 and tp_referencia = 'M' " . $sWhere . "
    left join sf_vendas_itens vi on vi.id_item_venda = m.id_item_venda_mens
    where a.tipo = 'C' and a.trancar = 0 and a.inativo = 0 " . $sWhereX . "
    and p.inativa = 0 " . ($_GET['tp'] == 0 ? " and p.tp_valid_acesso in ('L','E') " : "") . " and vp.dt_cancelamento is null
    and m.id_mens not in (select id_mens from sf_boleto_online_itens where id_mens is not null)        
    and isnull((select top 1 vpp.parcela from sf_vendas_planos_mensalidade vpm inner join sf_produtos_parcelas vpp on vpm.id_parc_prod_mens = vpp.id_parcela where vpm.id_plano_mens = vp.id_plano order by vpm.id_mens desc),0) > 0
    and ((DATEADD(day,(select ACA_PLN_DIAS_RENOVACAO from sf_configuracao),vp.dt_fim) > GETDATE() and p.parcelar = 0) or (vp.dt_fim >= GETDATE() and p.parcelar = 1) or ((select COUNT(*) from sf_vendas_planos_mensalidade C where C.id_plano_mens = vp.id_plano and dt_pagamento_mens is null) > 0))";
}
//---------------------------------------------------------------------------------------------------------------------------
for ($i = 0; $i < count($aColumns); $i++) {
    if ($i == 0) {
        $colunas = $aColumns[$i];
    } else {
        $colunas = $colunas . "," . $aColumns[$i];
    }
}
if (isset($_GET['bc'])) {
    $sQuery = "set dateformat dmy;" .
            "SELECT count(*) total FROM(" .
            "SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row, * from (" .
            "select " . $aGroupBegin . $colunas . " from " . $table1 .
            (($dependenteBoleto > 0 && $_GET['tp'] != "2") ? " and a.id_fornecedores_despesas not in (select id_dependente from sf_fornecedores_despesas_dependentes)" : "") .
            (($dependenteBoleto > 0 && $_GET['tp'] != "2") ? " union all select " . str_replace("'a'", "'b'", $colunas) . " from " . $table2 : "") . $aGroupEnd . ") as x) as a";
    //echo $sQuery; exit;
    $cur = odbc_exec($con, $sQuery);
    while ($aRow = odbc_fetch_array($cur)) {
        $iTotal = $aRow['total'];
        $iFilteredTotal = $aRow['total'];
    }
}
$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);
if (!isset($_GET['bc'])) {
    echo json_encode($output);
    exit;
}
//---------------------------------------------------------------------------------------------------------------------------
$sQuery1 = "set dateformat dmy; SELECT * FROM(" .
        "SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row, * from (" .
        "select " . $aGroupBegin . $colunas . " from " . $table1 .
        (($dependenteBoleto > 0 && $_GET['tp'] != "2") ? " and a.id_fornecedores_despesas not in (select id_dependente from sf_fornecedores_despesas_dependentes)" : "") .
        (($dependenteBoleto > 0 && $_GET['tp'] != "2") ? " union all select " . str_replace("'a'", "'b'", $colunas) . " from " . $table2 : "") . $aGroupEnd .
        ") as x) as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder2;
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $aling = "";
    if ($aRow["idade"] < 18) {
        $aling = "style='color:goldenrod;'";
    } else if (strlen($aRow[$aColumns[6]]) == 0 || strlen($aRow[$aColumns[7]]) == 0 || strlen($aRow[$aColumns[8]]) == 0 || strlen($aRow[$aColumns[9]]) == 0 || strlen($aRow[$aColumns[10]]) == 0 || strlen($aRow[$aColumns[11]]) == 0 || strlen($aRow[$aColumns[12]]) == 0) {
        $aling = "style='color:red;'";
    }
    if ($imprimir == 0 || (isset($_GET['ipx']) && $_GET['ipx'] == "1")) {
        $row = array();
        $btnEst = "";
        $mensId = str_replace(array("a", "b"), "", $aRow["id_mens"]);
        $btnEst .= "<a href=\"javascript:;\" data-id=\"" . $aRow[$aColumns[0]] . "\" id=\"btnAlt\"><span class=\"ico-edit\" title=\"Alterar\" style=\"font-size:18px;\"> </span></a>";
        if (is_numeric($aRow[$aColumns[14]])) {
            $btnEst .= "<a href=\"./../../Boletos/Boleto.php?id=M-" . $mensId . "&amp;crt=" . $contrato . "\" target=\"_blank\"><img src=\"../../img/boleto.png\" style=\"margin-top: -10px;\" value=\"Pago\"></a>";
        }
        $row[] = "<center><input type=\"checkbox\" " . (($aling != "" && $aRow["idade"] >= 18) || $aRow[$aColumns[3]] > 0 || strlen($aRow[$aColumns[19]]) > 0 ? "disabled" : "") . " class=\"caixa\" name=\"items[]\" value=\"" . (is_numeric($mensId) ? "M|" . $mensId : "P|" . $aRow[$aColumns[13]]) . "\"/></center>";
        $row[] = "<div " . $aling . "><center>" . utf8_encode($aRow[$aColumns[0]]) . "</center></div>";
        $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[$aColumns[0]]) . ")'><div id='formPQ' " . $aling . " title='" . utf8_encode($aRow[$aColumns[1]]) . "'>" . utf8_encode($aRow[$aColumns[1]]) . "</div></a>";
        $row[] = "<div " . $aling . ">" . utf8_encode($aRow[$aColumns[15]]) . "</div>";
        $row[] = "<div " . $aling . ">" . escreverNumero($aRow["valor_mens"]) . "</div>";
        $row[] = "<div " . $aling . ">" . ($aRow[$aColumns[3]] > 0 || strlen($aRow[$aColumns[19]]) > 0 ? escreverNumero($aRow[$aColumns[3]]) : "") . "</div>";
        $row[] = "<div " . $aling . ">" . escreverData($aRow[$aColumns[4]] == "" ? $aRow[$aColumns[17]] : $aRow[$aColumns[4]]) . "</div>";
        $row[] = "<a href='javascript:void(0)' onClick='AbrirBoxBoleto(" . utf8_encode($mensId) . ")'><div " . $aling . ">" . utf8_encode($aRow[$aColumns[5]]) . ($aRow[$aColumns[18]] == "1" ? "<img src=\"../../img/1368741588_check.png\" title=\"Arquivo Exportado\" style=\"height:16px;float: right;margin-right: 5px;\" alt=\"\">" : "") . "</div></a>";
        $row[] = "<center>" . $btnEst . "</center>";
        $output['aaData'][] = $row;
    } elseif ($imprimir == 1 && $aling != "") {
        $row = array();
        $row[] = "<div " . $aling . "><center>" . utf8_encode($aRow[$aColumns[0]]) . "</center></div>";
        $row[] = "<div " . $aling . ">" . utf8_encode($aRow[$aColumns[1]]) . "</div>";
        $row[] = "<div " . $aling . ">" . utf8_encode($aRow[$aColumns[6]]) . "</div>";
        $row[] = "<div " . $aling . ">" . utf8_encode($aRow[$aColumns[7]]) . (strlen($aRow[$aColumns[8]]) > 0 ? ", n° " . utf8_encode($aRow[$aColumns[8]]) : "") . "</div>";
        $row[] = "<div " . $aling . ">" . utf8_encode($aRow[$aColumns[9]]) . "</div>";
        $row[] = "<div " . $aling . ">" . utf8_encode($aRow[$aColumns[10]]) . "</div>";
        $row[] = "<div " . $aling . ">" . utf8_encode($aRow[$aColumns[11]]) . "</div>";
        $row[] = "<div " . $aling . ">" . utf8_encode($aRow[$aColumns[12]]) . "</div>";
        $output['aaData'][] = $row;
    }
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
} else {
    $output['aaTotal'][] = array(3, 3, "", "", 0, 0, 0);
    $output['aaTotal'][] = array(4, 4, "", "", 0, 0, 0);  
}
odbc_close($con);
