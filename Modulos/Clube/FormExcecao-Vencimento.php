<?php
include '../../Connections/configini.php';

$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "set dateformat dmy;" . 
        "update sf_excecao_vencimento set " .
        "data = " . valoresData("txtData") . "," . 
        "descricao = " . valoresTexto("txtDescricao") . "," . 
        "tipo = " . valoresTexto("txtTipo") .
        " where id = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "set dateformat dmy;" . 
        "insert into sf_excecao_vencimento(data,descricao,tipo)values(" .
        valoresData("txtData") . "," . 
        valoresTexto("txtDescricao") . "," . 
        valoresTexto("txtTipo") . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id from sf_excecao_vencimento order by id desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_excecao_vencimento WHERE id = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_excecao_vencimento where id =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id'];
        $data = escreverData($RFP['data']);
        $descricao = utf8_encode($RFP['descricao']);
        $tipo = utf8_encode($RFP['tipo']);      
    }
} else {
    $disabled = '';
    $id = '';
    $data = getData("T");
    $descricao = '';
    $tipo = $_GET['tp'];
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script> 
<script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<body>
    <form action="FormExcecao-Vencimento.php" name="frmExcecao-Vencimento" method="POST">
        <div class="frmhead">
            <div class="frmtext">Exceção de Vencimento</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont">
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <input name="txtTipo" id="txtTipo" value="<?php echo $tipo; ?>" type="hidden"/>
            <div style="width: 100%;float: left">
                <label>Data:</label>
                <input id="txtData" name="txtData" <?php echo $disabled; ?> type="<?php echo ($tipo == 1 ? "hidden" : "text"); ?>" class="datepicker" value="<?php echo $data; ?>">                
                <?php if ($tipo == 1) { ?>
                    <select id="txtDataDia" style="width: 20%; float: left" <?php echo $disabled; ?>>
                        <?php for($i = 1; $i <= 31; $i++) { ?>
                        <option value="<?php echo str_pad($i, 2, "0", STR_PAD_LEFT); ?>" <?php echo (substr($data, 0, 2) == str_pad($i, 2, "0", STR_PAD_LEFT) ? "selected" : "") ?>><?php echo str_pad($i, 2, "0", STR_PAD_LEFT); ?></option>
                        <?php } ?>
                    </select>
                    <select id="txtDataMes" style="width: 40%; float: left; margin-left: 1%;" <?php echo $disabled; ?>>
                        <?php $aPt = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
                        for($i = 1; $i <= count($aPt); $i++) { ?>
                        <option value="<?php echo str_pad($i, 2, "0", STR_PAD_LEFT); ?>" <?php echo (substr($data, 3, 2) == str_pad($i, 2, "0", STR_PAD_LEFT) ? "selected" : "") ?>><?php echo $aPt[$i - 1]; ?></option>
                        <?php } ?>
                    </select>
                <?php } ?>                
            </div>
            <div style="width: 100%;float: left">
                <label>Descrição:</label>
                <input id="txtDescricao" name="txtDescricao" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="256" value="<?php echo $descricao; ?>"/>                
            </div>
            <div style="clear:both;height: 27px;"></div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" onclick="return verificaData();" name="bntSave" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK" ><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else { ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../../js/plugins.js'></script>
    <script type="text/javascript" src="../../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type='text/javascript' src='../../../js/plugins/datatables/fnReloadAjax.js'></script>
    <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>  
    <script type="text/javascript">        
        $(document).ready(function () {
            $("#txtData").mask(lang["dateMask"]);    
        });    
        
        function verificaData() {
            if ($("#txtTipo").val() === "1") {
                $("#txtData").val($("#txtDataDia").val() + "/" + $("#txtDataMes").val() + "/2000");
            }         
            if(moment($("#txtData").val(), lang["dmy"], true).isValid()) {
                return true;
            } else {
                bootbox.alert("Data inválida!");
                return false;                
            }
        }
    </script>             
    <?php odbc_close($con); ?>
</body>