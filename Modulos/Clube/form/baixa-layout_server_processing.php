<?php

set_time_limit(1800);
require_once(__DIR__ . '/../../../Connections/configini.php');
require_once(__DIR__ . '/../../../Excel/excel_reader.php');

$pasta = "./../../../Pessoas/" . $contrato . "/Retorno";
$nome = "";
$idLayout = 0;
$page = 0;
$totalPage = 200;
$dependente = 0;
$data = [];
$layout = [];
$layoutItem = [];
$array_status = ["Desligado", "DesligadoEmAberto", "Bloqueado", "Excluido"];

function formatField($layout, $item, $line) {
    $lineClear = str_replace(" ", "", str_replace($item["separador"], "", $line));
    if ($layout["extensao"] == "xls" && $item["campo"] == "matricula" && $item["tamanho"] > 0) {
        return str_pad($line, $item["tamanho"], "0", STR_PAD_LEFT);
    } else if ($item["campo"] == "cpf" && strlen($line) == 11) {
        return substr($line, 0, 3) . "." . substr($line, 3, 3) . "." . substr($line, 6, 3) . "-" . substr($line, 9, 2);
    } else if ($item["campo"] == "cpf" && strlen($line) == 10) {
        return "0" . substr($line, 0, 2) . "." . substr($line, 2, 3) . "." . substr($line, 5, 3) . "-" . substr($line, 8, 2);
    } else if ($item["campo"] == "cpf" && strlen($line) == 9) {
        return "00" . substr($line, 0, 1) . "." . substr($line, 1, 3) . "." . substr($line, 4, 3) . "-" . substr($line, 7, 2);
    } else if ($item["campo"] == "cpf" && strlen($line) == 8) {
        return "000." . substr($line, 0, 3) . "." . substr($line, 3, 3) . "-" . substr($line, 6, 2);
    } else if ($item["campo"] == "dt_inicio_mens" && $item["data_formato"] == "ddmmaaaa" && strlen($lineClear) == 8) {
        return substr($lineClear, 0, 2) . "/" . substr($lineClear, 2, 2) . "/" . substr($lineClear, 4, 4);
    } else if ($item["campo"] == "dt_inicio_mens" && $item["data_formato"] == "aaaammdd" && strlen($lineClear) == 8) {
        return substr($lineClear, 6, 2) . "/" . substr($lineClear, 4, 2) . "/" . substr($lineClear, 0, 4);
    } else if ($item["campo"] == "dt_inicio_mens" && $item["data_formato"] == "ddmmaa" && strlen($lineClear) == 6) {
        return substr($lineClear, 0, 2) . "/" . substr($lineClear, 2, 2) . "/20" . substr($lineClear, 4, 2);
    } else if ($item["campo"] == "dt_inicio_mens" && $item["data_formato"] == "aammdd" && strlen($lineClear) == 6) {
        return substr($lineClear, 4, 2) . "/" . substr($lineClear, 2, 2) . "/20" . substr($lineClear, 0, 2);
    } else if ($item["campo"] == "dt_inicio_mens" && $item["data_formato"] == "mmaaaa" && strlen($lineClear) == 6) {
        return "01/" . substr($lineClear, 0, 2) . "/" . substr($lineClear, 2, 4);
    } else if ($item["campo"] == "dt_inicio_mens" && $item["data_formato"] == "aaaamm" && strlen($lineClear) == 6) {
        return "01/" . substr($lineClear, 4, 2) . "/" . substr($lineClear, 0, 4);
    } else if ($item["campo"] == "dt_inicio_mens" && $item["data_formato"] == "mmaa" && strlen($lineClear) == 4) {
        return "01/" . substr($lineClear, 0, 2) . "/20" . substr($lineClear, 2, 2);
    } else if ($item["campo"] == "dt_inicio_mens" && $item["data_formato"] == "aamm" && strlen($lineClear) == 4) {
        return "01/" . substr($lineClear, 2, 2) . "/20" . substr($lineClear, 0, 2);
    } else if (($item["campo"] == "valor_mens" || $item["campo"] == "valor_custo") && $item["separador"] == "") {
        return substr($line, 0, strlen($line) - 2) . "." . substr($line, -2);
    } else if (($item["campo"] == "valor_mens" || $item["campo"] == "valor_custo") && $item["separador"] == ",") {
        return str_replace(",", ".", $line);
    } else if (($item["campo"] == "valor_mens" || $item["campo"] == "valor_custo") && $item["separador"] == ".") {
        return $line;
    } else {
        return $line;
    }
}

if (isset($_POST['txtPage'])) {
    $page = $_POST['txtPage'];
}

if (isset($_POST['txtNome'])) {
    $nome = $_POST['txtNome'];
}

if (isset($_POST['txtLayout']) && is_numeric($_POST['txtLayout'])) {
    $cur = odbc_exec($con, "select * from sf_layout where origem = 'B' and inativo = 0 and id = " . $_POST['txtLayout']);
    while ($RFP = odbc_fetch_array($cur)) {
        $idLayout = $RFP["id"];
        $layout = array("id" => $RFP["id"], "descricao" => utf8_encode($RFP["descricao"]), "grupo" => $RFP["grupo"],
            "tipo" => $RFP["tipo"], "extensao" => $RFP["extensao"], "tipo_documento" => $RFP["tipo_documento"],
            "id_produto" => $RFP["id_produto"], "id_campo_livre" => $RFP["id_campo_livre"]);
    }
    $cur = odbc_exec($con, "select * from sf_layout_item where id_layout = " . $idLayout . " order by ordinal");
    while ($RFP = odbc_fetch_array($cur)) {
        $layoutItem[] = array(
            "id" => $RFP["id"],
            "descricao" => utf8_encode($RFP["descricao"]),
            "campo" => $RFP["campo"],
            "tamanho" => $RFP["tamanho"],
            "completar" => utf8_encode($RFP["completar"]),
            "separador" => $RFP["separador"],
            "data_formato" => $RFP["data_formato"],
        );
    }
}
//adm_baixa_layout
if ($nome != "") {
    $cur = odbc_exec($con, "select FIN_GRUPO_BOLETO, adm_baixa_layout from sf_configuracao where id = 1");
    while ($RFP = odbc_fetch_array($cur)) {
        $dependente = $RFP['FIN_GRUPO_BOLETO'];
        if ($RFP['adm_baixa_layout'] > 0) {
            $array_status = ["Bloqueado", "Excluido"];
        }
    }    
    if (isset($layout["extensao"]) && $layout["extensao"] == "xls") {
        $i = 2;
        $excel = new PhpExcelReader;
        $excel->read($pasta . "/" . $nome);
        $nr_sheets = count($excel->sheets);
        while ($i <= $excel->sheets[0]['numRows']) {
            $row = array();
            foreach ($layoutItem as $item) {
                $row[$item["campo"]] = formatField($layout, $item, $excel->sheets[0]['cells'][$i][$item["completar"]]);
            }
            $data[]["arquivo"] = $row;
            $i++;
        }
    } else if (isset($layout["extensao"]) && $layout["extensao"] == "csv") {
        $contents = file_get_contents($pasta . "/" . $nome);
        $datas = array_map("str_getcsv", preg_split('/\\r*\\n+|\\r+/', $contents));
        if (isset($datas[0])) {
            foreach ($datas as $l) {
                $dt = explode(";", $l[0]);
                if (count($dt) > 1) {
                    $row = array();
                    foreach ($layoutItem as $item) {
                        $row[$item["campo"]] = formatField($layout, $item, $dt[$item["completar"] - 1]);
                    }
                    $data[]["arquivo"] = $row;
                }
            }
        } else {
            $response["message"] = "No datas found";
        }
    } else if (isset($layout["extensao"]) && $layout["extensao"] == "txt") {
        $i = 0;
        $lendo = @fopen($pasta . "/" . $nome, "r");
        if (!$lendo) {
            echo "Erro ao abrir a URL.";
            exit;
        }
        while (!feof($lendo)) {
            $i++;
            $row = array();
            $linha = fgets($lendo, 9999);
            if (strlen(str_replace(array("\n", "\r"), "", $linha)) > 0) {
                foreach ($layoutItem as $item) {
                    $row[$item["campo"]] = formatField($layout, $item, substr($linha, (is_numeric($item["completar"]) ? ($item["completar"] - 1) : 0), (is_numeric($item["tamanho"]) ? $item["tamanho"] : 0)));
                }
                $data[]["arquivo"] = $row;
            }
        }
        fclose($lendo);
    }    
    $i = 0;    
    foreach ($data as $item) {
        if ($i >= ($page * $totalPage) && $i < (($page + 1) * $totalPage)) {
            $matricula = (isset($item["arquivo"]["cpf"]) ? $item["arquivo"]["cpf"] : (isset($item["arquivo"]["id_fornecedores_despesas"]) ? $item["arquivo"]["id_fornecedores_despesas"] : $item["arquivo"]["matricula"]));
            $dataref = (strlen($item["arquivo"]["dt_inicio_mens"]) > 0 ? $item["arquivo"]["dt_inicio_mens"] : $_POST["txt_dt_baixa"]);
            $valorref = $item["arquivo"]["valor_mens"];
            $codigoPessoa = "";
            $nomePessoa = "";
            $statusPessoa = "";
            $idMens = "";
            $dtVencimento = "";
            $dtPagamento = "";
            $valMens = 0;
            $valPago = 0;
            if (strlen($matricula) > 0) {
                $query = "select id_fornecedores_despesas,razao_social, fornecedores_status from sf_fornecedores_despesas
                where tipo = 'C' and inativo = 0" . (strlen($layout["grupo"]) > 0 ? " and grupo_pessoa = " . valoresNumericos2($layout["grupo"]) : "") .
                        (isset($item["arquivo"]["cpf"]) ? " and cnpj = " . valoresTexto2($item["arquivo"]["cpf"]) : " and id_fornecedores_despesas in (" . (isset($item["arquivo"]["id_fornecedores_despesas"]) ? $matricula :
                        "select fornecedores_campos from sf_fornecedores_despesas_campos_livres where campos_campos = " . valoresNumericos2($layout["id_campo_livre"]) . " and conteudo_campo = " . valoresTexto2($matricula)) . ")");
                //echo $query; exit;
                $cur = odbc_exec($con, $query);
                while ($RFP = odbc_fetch_array($cur)) {
                    $codigoPessoa = $RFP["id_fornecedores_despesas"];
                    $nomePessoa = utf8_encode($RFP["razao_social"]);
                    $statusPessoa = utf8_encode($RFP["fornecedores_status"]);
                }
                if ($layout["tipo"] == "M" && strlen($dataref) == 10 && strlen($codigoPessoa) > 0) {
                    if (is_numeric($layout["id_produto"])) {
                        $whereX = " and id_prod_plano = " . $layout["id_produto"];
                    } else if (isset($_POST['itemsPlano'])) {
                        $whereX = " and id_prod_plano in (" . $_POST['itemsPlano'] . ")";                        
                    }
                    $where = " and month(vpm.dt_inicio_mens) = month(" . valoresData2($dataref) . ") and year(vpm.dt_inicio_mens) = year(" . valoresData2($dataref) . ") ";
                    $query = "set dateformat dmy;select id_mens,dt_inicio_mens, dt_pagamento_mens,dbo.VALOR_REAL_MENSALIDADE(vpm.id_mens) valor_mens, vi.valor_total, 1 titular
                    from sf_vendas_planos vp inner join sf_vendas_planos_mensalidade vpm on vp.id_plano = vpm.id_plano_mens
                    left join sf_vendas_itens vi on vi.id_item_venda = vpm.id_item_venda_mens
                    where favorecido = " . $codigoPessoa . $where . $whereX . 
                            
                    ($dependente > 0 ? " union all select id_mens,dt_inicio_mens, dt_pagamento_mens,dbo.VALOR_REAL_MENSALIDADE(vpm.id_mens) valor_mens, vi.valor_total, 0 titular 
                    from sf_fornecedores_despesas_dependentes fd inner join sf_vendas_planos vp on fd.id_dependente = vp.favorecido
                    inner join sf_vendas_planos_mensalidade vpm on vp.id_plano = vpm.id_plano_mens
                    left join sf_vendas_itens vi on vi.id_item_venda = vpm.id_item_venda_mens
                    where fd.id_titular = " . $codigoPessoa . $where . $whereX : "") . 
                            
                    ($dependente > 0 ? " union all select id_mens,dt_inicio_mens, dt_pagamento_mens,dbo.VALOR_REAL_MENSALIDADE(vpm.id_mens) valor_mens, vi.valor_total, 0 titular 
                    from sf_fornecedores_despesas_responsavel fd inner join sf_vendas_planos vp on fd.id_dependente = vp.favorecido
                    inner join sf_vendas_planos_mensalidade vpm on vp.id_plano = vpm.id_plano_mens
                    left join sf_vendas_itens vi on vi.id_item_venda = vpm.id_item_venda_mens
                    where fd.id_responsavel = " . $codigoPessoa . $where . $whereX : "") . 
                            
                    ($dependente > 0 ? " union all select id_mens,dt_inicio_mens, dt_pagamento_mens,dbo.VALOR_REAL_MENSALIDADE(vpm.id_mens) valor_mens, vi.valor_total, 0 titular 
                    from sf_fornecedores_despesas_dependentes fd inner join sf_vendas_planos vp on fd.id_dependente = vp.favorecido
                    inner join sf_vendas_planos_mensalidade vpm on vp.id_plano = vpm.id_plano_mens
                    left join sf_vendas_itens vi on vi.id_item_venda = vpm.id_item_venda_mens
                    where fd.id_titular in (select id_dependente from sf_fornecedores_despesas_responsavel 
                    where id_responsavel = " . $codigoPessoa . ") " . $where . $whereX : "") .                             
                            
                    " order by titular ";
                    //echo $query; exit;
                    $cur = odbc_exec($con, $query);
                    while ($RFP = odbc_fetch_array($cur)) {
                        $idMens .= $RFP["id_mens"] . ",";
                        $dtVencimento = $RFP["dt_inicio_mens"];
                        $dtPagamento = $RFP["dt_pagamento_mens"];
                        $valMens = $valMens + $RFP["valor_mens"];
                        $valPago = $valPago + $RFP["valor_total"];
                    }
                }                                
            } else if ($layout["tipo"] == "B" && isset($item["arquivo"]["nosso_numero"]) && is_numeric($item["arquivo"]["nosso_numero"]) && $item["arquivo"]["nosso_numero"] > 0) {
                if (is_numeric($layout["id_produto"])) {
                    $whereX = " and id_prod_plano = " . $layout["id_produto"];
                } else if (isset($_POST['itemsPlano'])) {
                    $whereX = " and id_prod_plano in (" . $_POST['itemsPlano'] . ")";
                }
                $query = "set dateformat dmy;select id_mens,id_fornecedores_despesas,razao_social, fornecedores_status, dt_inicio_mens,dt_pagamento_mens,valor_mens,valor_total 
                from sf_boleto inner join sf_vendas_planos_mensalidade on id_referencia = id_mens and tp_referencia = 'M'
                inner join sf_vendas_planos on id_plano_mens = id_plano
                inner join sf_fornecedores_despesas on id_fornecedores_despesas = favorecido
                left join sf_vendas_itens on sf_vendas_itens.id_item_venda = sf_vendas_planos_mensalidade.id_item_venda_mens
                where sf_boleto.inativo = 0 and bol_nosso_numero = " . valoresTexto2($item["arquivo"]["nosso_numero"]) . $whereX;
                //echo $query; exit;
                $cur = odbc_exec($con, $query);
                while ($RFP = odbc_fetch_array($cur)) {
                    $idMens .= $RFP["id_mens"] . ",";
                    $codigoPessoa = $RFP["id_fornecedores_despesas"];
                    $nomePessoa = utf8_encode($RFP["razao_social"]);
                    $statusPessoa = utf8_encode($RFP["fornecedores_status"]);
                    $dtVencimento = $RFP["dt_inicio_mens"];
                    $dtPagamento = $RFP["dt_pagamento_mens"];
                    $valMens = $valMens + $RFP["valor_mens"];
                    $valPago = $valPago + $RFP["valor_total"];
                }
            }
            $data[$i]["dados"]["idMens"] = rtrim($idMens, ',');
            $data[$i]["dados"]["codigoPessoa"] = $codigoPessoa;
            $data[$i]["dados"]["nomePessoa"] = $nomePessoa;
            $data[$i]["dados"]["statusPessoa"] = $statusPessoa;
            $data[$i]["dados"]["dtVencimento"] = escreverData($dtVencimento);
            $data[$i]["dados"]["dtPagamento"] = escreverData($dtPagamento);
            $data[$i]["dados"]["valMens"] = $valMens;
            $data[$i]["dados"]["valPago"] = $valPago;
        }
        $i++;
    }
}

$output = array(
    "iTotalRecords" => count($data),
    "iTotalDisplayRecords" => $totalPage,
    "aaData" => array()
);

$i = 0;
foreach ($data as $item) {
    if ($i >= ($page * $totalPage) && $i < (($page + 1) * $totalPage)) {
        $valMatr = (isset($item["arquivo"]["id_fornecedores_despesas"]) ? $item["arquivo"]["id_fornecedores_despesas"] : (isset($item["arquivo"]["cpf"]) ? $item["arquivo"]["cpf"] : (isset($item["arquivo"]["nosso_numero"]) && is_numeric($item["arquivo"]["nosso_numero"]) && $item["arquivo"]["nosso_numero"] > 0 ? $item["arquivo"]["nosso_numero"] : $item["arquivo"]["matricula"])));
        $valData = (strlen($item["arquivo"]["dt_inicio_mens"]) > 0 ? (valoresData2($item["arquivo"]["dt_inicio_mens"]) != "null" ? $item["arquivo"]["dt_inicio_mens"] : "") : $_POST["txt_dt_pag"]);
        $valDoc = ($item["arquivo"]["valor_mens"] + ($item["arquivo"]["valor_mens"] > 0 ? $item["arquivo"]["valor_custo"] : 0));
        $estado = "";
        if ($item["dados"]["idMens"] != "" && $item["dados"]["dtPagamento"] == "" && $item["dados"]["valPago"] == "" && $item["dados"]["valMens"] != $valDoc && strlen($valData) > 0 && strlen($valDoc) > 0) {
            $estado = "apagarx";
        } else if ($item["dados"]["idMens"] != "" && $item["dados"]["dtPagamento"] == "" && $item["dados"]["valPago"] == "" && $item["dados"]["valMens"] == $valDoc && strlen($valData) > 0 && strlen($valDoc) > 0) {
            $estado = "apagar";
        } else if ($item["dados"]["idMens"] != "" && strlen($item["dados"]["dtPagamento"]) > 0 && $item["dados"]["valMens"] != $item["dados"]["valPago"]) {
            $estado = "pagox";
        } else if ($item["dados"]["idMens"] != "" && strlen($item["dados"]["dtPagamento"]) > 0) {
            $estado = "pago";
        } else if ($item["dados"]["idMens"] != "" && strlen($valDoc) > 0) {
            $estado = "baixado";
        } else if ($item["dados"]["idMens"] == "" && $item["dados"]["codigoPessoa"] > 0) {
            $estado = "inativox";
        } else {
            $estado = "inativo";
        }
        if (strlen($valMatr) > 0) {
            $row = array();
            $output['aaData'][] = "<tr class=\"" . $estado . "\">" .
            "<td><center><input type=\"checkbox\" name=\"itemsCheck[]\" value=\"" . $item["dados"]["idMens"] . "\"" .
            ($estado == "apagar" && !in_array($item["dados"]["statusPessoa"], $array_status) ? "checked" : "") . " " . ((($estado == "apagar" || $estado == "apagarx") && (!in_array($item["dados"]["statusPessoa"], $array_status))) ? "" : "disabled") . "/></center></td>" .
            "<td><b>" . $valMatr . "</b></td>" .
            "<td>" . $item["dados"]["codigoPessoa"] . "</td>" .
            "<td><a href='javascript:void(0)' onClick='AbrirCli(" . $item["dados"]["codigoPessoa"] . ")'><div id='formPQ' title='" . $item["dados"]["nomePessoa"] . "'>" . $item["dados"]["nomePessoa"] . (in_array($item["dados"]["statusPessoa"], $array_status) ? " (Desligado)" : "") . "</div></a></td>" .
            "<td><b>" . $valData . "</b></td>" .
            "<td>" . $item["dados"]["dtVencimento"] . "</td>" .
            "<td>" . $item["dados"]["dtPagamento"] . "</td>" .
            "<td><b>" . (strlen($valDoc) > 0 ? escreverNumero($valDoc) : "") . "</b></td>" .
            "<td>" . ($item["dados"]["valMens"] > 0 ? escreverNumero($item["dados"]["valMens"]) : "") . "</td>" .
            "<td>" . (strlen($item["dados"]["dtPagamento"]) > 0 ? escreverNumero($item["dados"]["valPago"]) : "") . "</td>" .
            "<td><center><img style=\"height: 16px;\" src=\"../../img/" . str_replace("x", "", $estado) . ".png\"></center></td>" .
            "</tr>";
        }
    }
    $i++;
}

echo json_encode($output);
odbc_close($con);
