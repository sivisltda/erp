<?php

require_once(__DIR__ . '/../../../Connections/configini.php');

$mdl_seg_ = returnPart($_SESSION["modulos"],14);

if (isset($_POST['bntGrupo'])) {
    if (is_numeric($_POST['txtId'])) {
        $query = "update sf_fornecedores_despesas set grupo_pessoa = " . valoresSelect("txtGrupo") . " where id_fornecedores_despesas = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
        echo "YES";
    }
}

if (isset($_POST['bntRefresh'])) {
    if (is_numeric($_POST['txtId'])) {
        $exp_remessa = 0;
        $cur = odbc_exec($con, "select exp_remessa,bol_id_banco,carteira_id,tp_referencia,id_referencia,dt_inicio_mens
        from sf_boleto inner join sf_vendas_planos_mensalidade on id_mens = id_referencia and tp_referencia = 'M'
        where inativo = 0 and id_referencia = " . $_POST['txtId'] . ";") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $exp_remessa = $RFP['exp_remessa'];
            $_POST['txtBanco'] = $RFP['bol_id_banco'];
            $_POST['txtCarteira'] = $RFP['carteira_id'];
            $_POST['txtDtBegin'] = escreverData($RFP['dt_inicio_mens']);
            $_POST['txtMens'] = "P|0," . $RFP['tp_referencia'] . "|" . $RFP['id_referencia'];
        }
        $query = "set dateformat dmy;update sf_vendas_planos_mensalidade set " .
        "valor_mens = " . (valoresNumericos("txtValor") - valoresNumericos("txtValorAdd") + valoresNumericos("txtValorDes")) . " where id_mens = " . $_POST['txtId'] . ";";
        if ($exp_remessa == "1") {
            $query .= "update sf_boleto set inativo = 1 where tp_referencia = 'M' and id_referencia = " . $_POST['txtId'] . ";";
            $query .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) 
            values ('sf_boleto', " . $_POST['txtId'] . ", '" . $_SESSION["login_usuario"] . "', 'C', 'CANCELAMENTO BOLETO: ' + 
            (select top 1 cast(bol_nosso_numero as varchar) from sf_boleto where id_referencia = " . $_POST['txtId'] . "), GETDATE(), 
            (select isnull((select favorecido from sf_vendas_planos_mensalidade inner join sf_vendas_planos on id_plano_mens = id_plano where id_mens = " . $_POST['txtId'] . "),0)));";            
            odbc_exec($con, $query) or die(odbc_errormsg());
            $_POST['bntSave'] = "S";
        } else {
            $query .= "set dateformat dmy;update sf_boleto set bol_data_parcela = " . valoresData("txtVencimento") . ",
            bol_valor = " . valoresNumericos("txtValor") . " where tp_referencia = 'M' and id_referencia = " . $_POST['txtId'] . ";";
            odbc_exec($con, $query) or die(odbc_errormsg());
            echo "YES";
            exit;
        }
    }
}

if (isset($_POST['bntSave'])) {
    $query = "set dateformat dmy;";
    $nossoNumero = "1";
    $desconto = "0.00";
    $Acrescimo = "0.00";
    $tipoDocumento = "10";
    $DateBegin = valoresData('txtDtBegin');
    $banco = valoresSelect("txtBanco");
    $carteira = valoresSelect("txtCarteira");
    $tipo = valoresNumericos('txtTipo');
    $mensArray = "0";
    $dependenteBoleto = "0";
    $planosBoleto = "0";        
    $sWherePlano = ($mdl_seg_ == 0 ? " and vp1.id_prod_plano = " . valoresNumericos2($_POST['txtPlanos']) : " and vp1.id_plano in (" . implode(",", $_POST['txtPlanos']) . ")");
    
    if (is_numeric($banco)) {        
        $cur = odbc_exec($con, "select FIN_GRUPO_BOLETO,FIN_GRUPO_BOLETO_MOD from sf_configuracao") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $dependenteBoleto = $RFP['FIN_GRUPO_BOLETO'];
            $planosBoleto = $RFP['FIN_GRUPO_BOLETO_MOD'];
        }
        $cur = odbc_exec($con, "select multa,juros,acrescimo from sf_bancos where id_bancos = '" . $banco . "'") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $MultaIndice = $RFP['multa'];
            $JurosIndice = $RFP['juros'];
            $Acrescimo = $RFP['acrescimo'];
        }        
        $cur = odbc_exec($con, "select isnull(MAX(x.bol_nosso_numero),0) vm, isnull(nosso_numero,0) nn from (
        select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_boleto p on p.bol_id_banco = b.id_bancos
        where id_bancos = '" . $banco . "' union all
        select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_solicitacao_autorizacao_parcelas p on p.bol_id_banco = b.id_bancos
        where id_bancos = '" . $banco . "' union all
        select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_venda_parcelas p on p.bol_id_banco = b.id_bancos
        where id_bancos = '" . $banco . "' union all
        select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_lancamento_movimento_parcelas l on l.bol_id_banco = b.id_bancos
        where id_bancos = '" . $banco . "') as x group by x.nosso_numero") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            if ($RFP['nn'] > $RFP['vm']) {
                $nossoNumero = $RFP['nn'];
            } else {
                $nossoNumero = bcadd($RFP['vm'], 1);
            }
        }
        //------------------------------------------------------------------------------------------------------------------------------------        
        $mensalidades = explode(",", $_POST['txtMens']);
        foreach ($mensalidades as $mens) {
            $m = explode("|", $mens);
            if ($m[0] == "P" && $m[1] > 0) {
                $result = odbc_exec($con, "set dateformat dmy;insert into sf_vendas_planos_mensalidade(id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens)
                select v.id_plano, " . $DateBegin . ", dateadd(day, -1, dateadd(month,1," . $DateBegin . ")), PP.id_parcela, GETDATE(), valor_unico from sf_vendas_planos V
                inner join sf_produtos P on P.conta_produto = V.id_prod_plano inner join sf_produtos_parcelas PP on PP.id_produto = P.conta_produto
                where parcela = 1 and id_plano in (" . $m[1] . ");SELECT SCOPE_IDENTITY() ID;") or die(odbc_errormsg());
                odbc_next_result($result);
                $mensArray .= "," . odbc_result($result, 1);
            } elseif ($m[0] == "M") {
                $mensArray .= "," . $m[1];
            }          
        }
        if ($planosBoleto > 0) { //criando as mensalidades dos outros planos do titular se não tiver
            $result = odbc_exec($con, "set dateformat dmy;insert into sf_vendas_planos_mensalidade(id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens)                
            select vpp.id_plano," . $DateBegin . ", dateadd(day, -1, dateadd(month,1," . $DateBegin . ")), PP.id_parcela, GETDATE(), valor_unico 
            from sf_vendas_planos vpp inner join sf_produtos P on P.conta_produto = vpp.id_prod_plano 
            inner join sf_produtos_parcelas PP on PP.id_produto = P.conta_produto where favorecido in (select favorecido from sf_vendas_planos_mensalidade vm
            inner join sf_vendas_planos vp on vm.id_plano_mens = vp.id_plano where id_mens in (" . $mensArray . ")) and parcela = 1 and dt_cancelamento is null
            and id_plano not in (select id_plano_mens from sf_vendas_planos_mensalidade vmm where vmm.id_plano_mens = vpp.id_plano and month(dt_inicio_mens) = month(" . $DateBegin . ")
            and year(dt_inicio_mens) = year(" . $DateBegin . "));") or die(odbc_errormsg());                
        }
        if ($dependenteBoleto > 0) {
            //criando as mensalidades dos dependentes se não tiver
        }
        //------------------------------------------------------------------------------------------------------------------------------------
        if (isset($_POST['txtServicos']) && strlen($_POST['txtServicos']) > 0) {
            $cur = odbc_exec($con, "select * from sf_produtos where conta_produto in (" . $_POST['txtServicos'] . ")") or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                $Acrescimo += $RFP['preco_venda'];    
            }            
        }
        //------------------------------------------------------------------------------------------------------------------------------------
        $table = "select id_mens, dt_inicio_mens, dbo.VALOR_REAL_MENSALIDADE(id_mens) valor_mens, id_boleto,
        isnull((select max(id_titular) from sf_fornecedores_despesas_dependentes where id_dependente = favorecido),0) titular
        from sf_vendas_planos_mensalidade inner join sf_vendas_planos on id_plano_mens = id_plano
        left join sf_boleto on sf_boleto.id_referencia = sf_vendas_planos_mensalidade.id_mens and tp_referencia = 'M' and inativo = 0            
        where id_mens in (" . $mensArray . ")";
        $cur = odbc_exec($con, $table) or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            if (!is_numeric($RFP['id_boleto'])) {                
                $valorMulta = 0;
                $valorJuros = 0;
                $dataVenc = valoresData2(getData("T"));
                $titular = $RFP['titular'];
                if ($RFP['dt_pagamento_mens'] == "") {
                    $dias = (int) floor((geraTimestamp($dataVenc) - geraTimestamp(escreverData($RFP['dt_inicio_mens']))) / (60 * 60 * 24));
                    if ($dias > 0) {
                        $valorMulta = ($RFP['valor_mens'] * $MultaIndice / 100);
                        $valorJuros = (($RFP['valor_mens'] * $JurosIndice / 100) * $dias);
                    }
                }                
                $query .= "INSERT INTO sf_boleto (id_referencia,tp_referencia,bol_id_banco,carteira_id,bol_data_criacao,bol_data_parcela,bol_valor,bol_juros,
                bol_multa,bol_nosso_numero,bol_descricao,bol_desconto,tipo_documento,sa_descricao,exp_remessa,inativo,syslogin)
                VALUES (" . $RFP['id_mens'] . ",'M', " . $banco . ", " . $carteira . "," . valoresData2(getData("T")) . "," . valoresData("txtVencimento") . ", " .
                ($RFP['valor_mens'] + $Acrescimo) . ", " . $valorMulta . ", " . $valorJuros . ", " . valoresTexto2($nossoNumero) . ",''," .
                $desconto . "," . $tipoDocumento . ",'',0,0," . valoresTexto2($_SESSION["login_usuario"]) . ");";
                if ($planosBoleto > 0 && $titular == 0) {               
                    $query .= "INSERT INTO sf_boleto (id_referencia,tp_referencia,bol_id_banco,carteira_id,bol_data_criacao,bol_data_parcela,bol_valor,bol_juros,
                    bol_multa,bol_descricao,bol_nosso_numero,bol_desconto,tipo_documento,sa_descricao,exp_remessa,syslogin,inativo)                    
                    select m1.id_mens id_referencia,tp_referencia,bol_id_banco,carteira_id,bol_data_criacao,bol_data_parcela,dbo.VALOR_REAL_MENSALIDADE(m1.id_mens) bol_valor,
                    bol_juros,bol_multa,bol_descricao,bol_nosso_numero,bol_desconto,tipo_documento,sa_descricao,exp_remessa,syslogin,inativo from sf_boleto 
                    inner join sf_vendas_planos_mensalidade m on id_mens = id_referencia and tp_referencia = 'M'
                    inner join sf_vendas_planos on id_plano = id_plano_mens 
                    inner join sf_vendas_planos vp1 on vp1.favorecido = sf_vendas_planos.favorecido and vp1.id_plano <> sf_vendas_planos.id_plano
                    inner join sf_vendas_planos_mensalidade m1 on m1.id_plano_mens = vp1.id_plano and m1.dt_inicio_mens between m.dt_inicio_mens and m.dt_fim_mens
                    where inativo = 0 and vp1.dt_cancelamento is null and m1.dt_pagamento_mens is null 
                    and m.dt_pagamento_mens is null and m.id_mens = " . $RFP['id_mens'] . 
                    $sWherePlano . " and m1.id_mens not in (select id_referencia from sf_boleto where tp_referencia = 'M' and inativo = 0);";
                }
                if($dependenteBoleto > 0 && ($tipo == 0 || $tipo == 1)) {
                    $query .= "INSERT INTO sf_boleto (id_referencia,tp_referencia,bol_id_banco,carteira_id,bol_data_criacao,bol_data_parcela,bol_valor,bol_juros,
                    bol_multa,bol_descricao,bol_nosso_numero,bol_desconto,tipo_documento,sa_descricao,exp_remessa,syslogin,inativo)
                    select m1.id_mens id_referencia,tp_referencia,bol_id_banco,carteira_id,bol_data_criacao,bol_data_parcela,dbo.VALOR_REAL_MENSALIDADE(m1.id_mens) bol_valor,
                    bol_juros,bol_multa,bol_descricao,bol_nosso_numero,bol_desconto,tipo_documento,sa_descricao,exp_remessa,syslogin,inativo from sf_boleto 
                    inner join sf_vendas_planos_mensalidade m on id_mens = id_referencia and tp_referencia = 'M'
                    inner join sf_vendas_planos on id_plano = id_plano_mens inner join sf_fornecedores_despesas_dependentes on id_titular = " . ($titular > 0 ? $titular : "favorecido") . "
                    inner join sf_vendas_planos vp1 on vp1.favorecido = id_dependente
                    inner join sf_vendas_planos_mensalidade m1 on m1.id_plano_mens = vp1.id_plano and m1.dt_inicio_mens between m.dt_inicio_mens and m.dt_fim_mens
                    where inativo = 0 and m1.dt_pagamento_mens is null and m.dt_pagamento_mens is null and m.id_mens = " . $RFP['id_mens'] . 
                    $sWherePlano . " and m1.id_mens not in (select id_referencia from sf_boleto where tp_referencia = 'M' and inativo = 0);"; 
                }
                $query .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) 
                select 'sf_boleto', id_mens, " . valoresTexto2($_SESSION["login_usuario"]) . ", 'I', 'CADASTRO BOLETO: ' + cast(bol_nosso_numero as varchar(50)), GETDATE(), favorecido 
                from sf_boleto inner join sf_vendas_planos_mensalidade on id_referencia = id_mens
                inner join sf_vendas_planos on id_plano_mens = id_plano where sf_boleto.inativo = 0 and id_mens = " . $RFP['id_mens']; 
                $nossoNumero = bcadd($nossoNumero, 1);
            }
        }
        //echo $query; exit;
        odbc_exec($con, $query) or die(odbc_errormsg());
        //------------------------------------------------------------------------------------------------------------------------------------        
        if (isset($_POST['txtServicos']) && strlen($_POST['txtServicos']) > 0) {
            $query = "";
            $cur = odbc_exec($con, "select conta_produto, (select min(id_boleto) from sf_boleto where tp_referencia = 'M' and inativo = 0 and id_referencia in (" . $mensArray . ")) id_boleto from sf_produtos where conta_produto in (" . $_POST['txtServicos'] . ")") or die(odbc_errormsg());
            while ($RFP = odbc_fetch_array($cur)) {
                $query .= "insert into sf_boleto_adicional(id_produto, id_boleto) values (" . $RFP['conta_produto'] . "," . $RFP['id_boleto'] . ");";
            }
            odbc_exec($con, $query) or die(odbc_errormsg());            
        }
        echo "YES";
    }
}

if (isset($_POST['bntSaveFile'])) {
    $id_banco = valoresSelect("txtBanco");
    if (is_numeric($id_banco)) {
        $mensArray = "0";     
        $dependenteBoleto = "0";
        $planosBoleto = "0";            
        $mensalidades = explode(",", $_POST['txtMens']);
        $cur = odbc_exec($con, "select FIN_GRUPO_BOLETO,FIN_GRUPO_BOLETO_MOD from sf_configuracao") or die(odbc_errormsg());
        while ($RFP = odbc_fetch_array($cur)) {
            $dependenteBoleto = $RFP['FIN_GRUPO_BOLETO'];
            $planosBoleto = $RFP['FIN_GRUPO_BOLETO_MOD'];
        }        
        $cur = odbc_exec($con, "SELECT * FROM sf_bancos WHERE id_bancos = " . $id_banco);
        while ($RFP = odbc_fetch_array($cur)) {
            $tipo_bol = $RFP['tipo_bol'];
        }                
        $_POST['items'] = array();
        foreach ($mensalidades as $mens) {
            $m = explode("|", $mens);
            if ($m[0] == "M") {
                $mensArray .= "," . $m[1];
            }
        }
        $cancel = "0";
        $query = "select 'M-' +cast(id_mens as VARCHAR) as pk ,isnull(bol_data_parcela,dt_inicio_mens) 'Vecto','01/01' pa ,1 empresa,razao_social,
        (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = id_fornecedores_despesas) email, 'PAG MENSALIDADE' historico_baixa,'M' Origem,'MENSALIDADE' cm, 
        (select sum(bol_valor) from sf_boleto b where b.bol_id_banco = sf_boleto.bol_id_banco and b.carteira_id = sf_boleto.carteira_id and b.bol_nosso_numero = sf_boleto.bol_nosso_numero) valor_parcela,
        isnull(valor_total,0) valor_pago,'MENSALIDADE' gc,sf_boleto.inativo,id_mens documento, null obs_baixa,bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,
        bol_nosso_numero,dt_pagamento_mens,bol_data_criacao, bol_juros,cnpj, sf_carteira, endereco, numero, bairro, cidade_nome cidade, estado_sigla estado,cep,bol_desconto,exp_remessa,sf_variacao        
        from sf_boleto inner join sf_vendas_planos_mensalidade on sf_boleto.id_referencia = sf_vendas_planos_mensalidade.id_mens and tp_referencia = 'M' inner join sf_vendas_planos on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens left join sf_vendas_itens on sf_vendas_itens.id_item_venda = sf_vendas_planos_mensalidade.id_item_venda_mens left join sf_tipo_documento on sf_boleto.tipo_documento = sf_tipo_documento.id_tipo_documento
        left join sf_bancos_carteiras on sf_bancos_carteiras.sf_carteiras_id = sf_boleto.carteira_id
        inner join sf_fornecedores_despesas on sf_fornecedores_despesas.id_fornecedores_despesas " . ($dependenteBoleto > 0 ? " in (select isnull((select max(id_titular) from sf_fornecedores_despesas_dependentes where id_dependente = sf_vendas_planos.favorecido), sf_vendas_planos.favorecido))" : " = sf_vendas_planos.favorecido ") . "
        left join tb_estados on estado = estado_codigo
        left join tb_cidades on cidade = cidade_codigo 
        where sf_boleto.inativo = 0 and id_mens in (" . $mensArray . ") order by 1";
        //echo $query; exit;
        $cur = odbc_exec($con, $query);
        while ($RFP = odbc_fetch_array($cur)) {
            $_POST['items'][] = $RFP['pk'] . "|"
                    . utf8_encode($RFP['bol_nosso_numero']) . "|"
                    . utf8_encode($RFP['sf_carteira']) . "|"
                    . utf8_encode($RFP['documento']) . "|"
                    . escreverData($RFP['Vecto'], str_replace("/", "", strtolower($lang['dmy']))) . "|"
                    . escreverNumero($RFP['valor_parcela']) . "|"
                    . escreverNumero($RFP['bol_juros']) . "|"
                    . utf8_encode($RFP['cnpj']) . "|"
                    . utf8_encode($RFP['razao_social']) . "|"
                    . utf8_encode(str_replace(array("ª","º"), "", $RFP['endereco']) . " n " . $RFP['numero']) . "|"
                    . utf8_encode($RFP['bairro']) . "|"
                    . utf8_encode($RFP['cep']) . "|"
                    . utf8_encode($RFP['cidade']) . "|"
                    . utf8_encode($RFP['estado']) . "|"
                    . utf8_encode($RFP['bol_desconto']) . "|"
                    . utf8_encode($RFP['sf_variacao']) . "|";
            $cancel .= "," . $RFP['documento'];            
        }
        if (isset($_POST['txtCancel'])) {
            $sqlInativaBoleto = "update sf_boleto set inativo = 1 where bol_nosso_numero in (select bol_nosso_numero from sf_boleto where id_referencia in (" . $cancel . ") and tp_referencia = 'M');";
            $sqlInativaBoleto .= "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas) 
            select 'sf_boleto', id_mens, '" . $_SESSION["login_usuario"] . "', 'C', 'CANCELAMENTO BOLETO: ' + cast(bol_nosso_numero as varchar), GETDATE(), favorecido 
            from sf_boleto inner join sf_vendas_planos_mensalidade on id_referencia = id_mens
            inner join sf_vendas_planos on id_plano_mens = id_plano where id_referencia in (" . $cancel . ");";
            odbc_exec($con, $sqlInativaBoleto);            
        }
        if ($tipo_bol == 12 || $tipo_bol == 13) {
            require_once(__DIR__ . '/../../Financeiro/form/FormBoletosRegistradosSicoob.php');
        } else if ($tipo_bol == 14 || $tipo_bol == 15) {
            require_once(__DIR__ . '/../../Financeiro/form/FormBoletosRegistradosSicredi.php');
        } else if ($tipo_bol == 4 || $tipo_bol == 9) {
            require_once(__DIR__ . '/../../Financeiro/form/FormBoletosRegistradosBradesco.php');
        } else {
            require_once(__DIR__ . '/../../Financeiro/form/FormBoletosRegistrados.php');    
        }
    }
}

if (isset($_POST['bntSendEmail'])) {
    $qtd_sucesso = 0;
    $invalidos = 0;
    $mensArray = "0";
    $items = array();
    $msg_banco = "";
    $mensalidades = explode("|", $_POST['txtId']);
    foreach ($mensalidades as $mens) {
        $m = explode("-", $mens);
        if ($m[0] == "M") {
            $mensArray .= "," . $m[1];
        }
    }   
    $query = "select 'M-' +cast(id_mens as VARCHAR) as pk ,isnull(bol_data_parcela,dt_inicio_mens) 'Vecto','01/01' pa ,1 empresa,sf_fornecedores_despesas.razao_social,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = id_fornecedores_despesas) email, 'PAG MENSALIDADE' historico_baixa,'M' Origem,'MENSALIDADE' cm, 
    (select sum(bol_valor) from sf_boleto b where b.bol_id_banco = sf_boleto.bol_id_banco and b.carteira_id = sf_boleto.carteira_id and b.bol_nosso_numero = sf_boleto.bol_nosso_numero) valor_parcela,
    isnull(valor_total,0) valor_pago,'MENSALIDADE' gc,sf_boleto.inativo,id_mens documento, null obs_baixa,bol_id_banco,sf_tipo_documento.abreviacao,sf_tipo_documento.descricao tdn,id_fornecedores_despesas,
    bol_nosso_numero,dt_pagamento_mens,bol_data_criacao, bol_juros,cnpj, sf_carteira, endereco, numero, bairro, cidade_nome cidade, estado_sigla estado,cep,bol_desconto,exp_remessa,msg_email
    from sf_boleto inner join sf_vendas_planos_mensalidade on sf_boleto.id_referencia = sf_vendas_planos_mensalidade.id_mens and tp_referencia = 'M' 
    inner join sf_vendas_planos on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens 
    left join sf_vendas_itens on sf_vendas_itens.id_item_venda = sf_vendas_planos_mensalidade.id_item_venda_mens 
    left join sf_tipo_documento on sf_boleto.tipo_documento = sf_tipo_documento.id_tipo_documento
    left join sf_bancos_carteiras on sf_bancos_carteiras.sf_carteiras_id = sf_boleto.carteira_id
    left join sf_bancos on sf_carteiras_bancos = id_bancos     
    inner join sf_fornecedores_despesas on sf_vendas_planos.favorecido = sf_fornecedores_despesas.id_fornecedores_despesas
    left join tb_estados on estado = estado_codigo
    left join tb_cidades on cidade = cidade_codigo 
    where sf_boleto.inativo = 0 and id_mens in (" . $mensArray . ") order by 1";    
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $msg_banco = (strlen($RFP['msg_email']) > 0 ? " <br>" . $RFP['msg_email'] : "");
        $boletos .= $RFP['pk'] . "|";
        $emails .= $RFP['email'] . "|" . $RFP['id_fornecedores_despesas'] . "|" . $RFP['razao_social'] . "|" . escreverData($RFP['Vecto']) . "|" . $RFP['valor_parcela'] . "|" . $RFP['bol_nosso_numero'] . ",";
    }
    $boletos = substr_replace($boletos, "", -1);
    $emails = substr_replace($emails, "", -1);
    if ($emails != "") {
        ob_start();
        require_once(__DIR__ . "/../../Contas-a-pagar/phpmailer/class.phpmailer.php");
        $mail = new PHPMailer(false);
        $mail->Charset = 'utf-8';
        $cur = odbc_exec($con, "select isnull(email_host,'') email_host,isnull(email_ckb,'') email_ckb, isnull(email_ssl,0) email_ssl,
        isnull(email_login,'') email_login,isnull(email_senha,'') email_senha,isnull(email_smtp,0) email_smtp,
        isnull(email_porta,'') email_porta,isnull(email_login, '')email_login,
        isnull(email_desc,'') email_desc,isnull(copyMe,'') copyMe,nome, isnull(email,'') email from sf_usuarios 
        where id_usuario = " . $_SESSION["id_usuario"] . " and email_login <> '' and email_porta <> '' and email_host <> '' and email_senha <> ''");
        if (odbc_num_rows($cur) > 0) {
            while ($RFP = odbc_fetch_array($cur)) {
                if ($RFP['email_smtp'] == 0) {
                    $mail->IsSMTP();
                }
                $mail->Host = utf8_encode($RFP['email_host']);
                $mail->SMTPAuth = utf8_encode($RFP['email_ckb']);
                $mail->Username = utf8_encode($RFP['email_login']); // Seu e-mail de saida
                $mail->Password = utf8_encode($RFP['email_senha']);
                $mail->Port = utf8_encode($RFP['email_porta']);
                $mail->From = utf8_encode($RFP['email']);       // Seu e-mail
                $mail->FromName = utf8_decode(utf8_encode($RFP['email_desc'])); //nome do remetente
                $mail->SMTPDebug = 1;
                if ($RFP['email_ssl'] == 1) {
                    $mail->SMTPSecure = 'ssl';
                } elseif ($RFP['email_ssl'] == 2) {
                    $mail->SMTPSecure = 'tls';
                }
                if ($RFP['copyMe'] == 1) {
                    $mail->AddBCC(utf8_encode($RFP['email_login']), utf8_encode($RFP['nome']));
                }
            }
        } else {
            echo "Verifique as configurações de E-mail!"; exit;
        }
        $assunto = "Boleto";
        $url = "https://sivisweb.com.br";
        $mail->IsHTML(true);
        $items = explode(",", $emails);
        for ($i = 0; $i < sizeof($items); $i++) {
            $email = explode("|", $items[$i]);
            $mensagem = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"> <head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> </head> <body bgcolor=\"#E1E1E1\" leftmargin=\"0\" marginwidth=\"0\" topmargin=\"0\" marginheight=\"0\" offset=\"0\"> <center style=\"background-color:#E1E1E1;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" width=\"100%\" id=\"bodyTable\" style=\"table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;\"> <tr> <br><td align=\"center\" valign=\"top\" id=\"bodyCell\"> <table bgcolor=\"#FFFFFF\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" id=\"emailBody\"> <tr> <td align=\"center\" valign=\"top\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"color:#FFFFFF;\" bgcolor=\"#3498db\"> <tr> <td align=\"center\" valign=\"top\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" class=\"flexibleContainer\"> <tr> <td align=\"center\" valign=\"top\" width=\"600\" class=\"flexibleContainerCell\"> <table border=\"0\" cellpadding=\"20\" cellspacing=\"0\" width=\"100%\"> <tr> <td> <img " .
            "src=\"" . $url . "/Pessoas/" . $contrato . "/Empresa/logo.png\" height=\"90\"/> </td><td valign=\"top\" class=\"textContent\"> <h1 style=\"color:#FFFFFF;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-bottom:5px;\">" .
            $_SESSION["razao_social_contrato"] . "</h1> <h2 style=\"color:#FFFFFF;font-weight:normal;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:10px;line-height:135%;\">" .
            "CNPJ/CPF: " . $_SESSION["cpf_cnpj"] . "</h2> <div style=\"font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#FFFFFF;line-height:135%;\">" .
            "E-mail: " . $mail->From . "</div></td></tr></table> </td></tr></table> </td></tr></table> </td></tr><tr> <td align=\"center\" valign=\"top\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"> <tr> <td align=\"center\" valign=\"top\"> <table border=\"0\" cellpadding=\"30\" cellspacing=\"0\" width=\"600\" class=\"flexibleContainer\"> <tr> <td style=\"padding-top:0;\" align=\"center\" valign=\"top\" width=\"600\" class=\"flexibleContainerCell\"> <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"flexibleContainer\"> <tr> <td align=\"left\" valign=\"top\" class=\"textContent\"> <div style=\"text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;margin-top:30px;color:#5F5F5F;line-height:135%;\"> Olá " .
            "<b>" . $email[2] . "</b>. <br><br>Segue o boleto de nosso número " .
            "<b>" . $email[5] . "</b>, <br>com vencimento para dia " .
            "<b>" . $email[3] . "</b>. <br>no valor de " .
            "<b>" . escreverNumero($email[4], 1) . "</b>." . $msg_banco . " </div></td></tr></table> </td></tr></table> </td></tr></table> </td></tr><tr> <td align=\"center\" valign=\"top\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"> <tr style=\"padding-top:0;\"> <td align=\"center\" valign=\"top\"> <table border=\"0\" cellpadding=\"30\" cellspacing=\"0\" width=\"600\" class=\"flexibleContainer\"> <tr> <td style=\"padding-top:0;\" align=\"center\" valign=\"top\" width=\"600\" class=\"flexibleContainerCell\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"45%\" class=\"emailButton\" style=\"background-color: #3498DB;\"> <tr> <td align=\"center\" valign=\"middle\" class=\"buttonContent\" style=\"padding-top:15px;padding-bottom:15px;padding-right:15px;padding-left:15px;\"> <a style=\"color:#FFFFFF;text-decoration:none;font-family:Helvetica,Arial,sans-serif;font-size:20px;line-height:135%;\" " .
            "href=\"" . $url . "/Boletos/Boleto.php?id=" . $boletos . "&crt=" . $contrato . "\" target=\"_blank\">Clique para Imprimir seu Boleto</a> </td></tr></table> </td></tr></table> </td></tr></table> </td></tr>Você pode efetuar o pagamento em uma agência bancária ou pela Internet.<tr> <td align=\"center\" valign=\"top\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"> <tr> <td align=\"center\" valign=\"top\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" class=\"flexibleContainer\"> <tr> <td valign=\"top\" width=\"600\" class=\"flexibleContainerCell\"> <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"> <tr> <td align=\"center\" class=\"textContent\"> </td></tr></table> </td></tr></table> </td></tr></table> </td></tr><tr> <td align=\"center\" valign=\"top\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"> <tr> <td align=\"center\" valign=\"top\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" class=\"flexibleContainer\"> <tr> <td align=\"center\" valign=\"top\" width=\"600\" class=\"flexibleContainerCell\"> <table border=\"0\" cellpadding=\"30\" cellspacing=\"0\" width=\"100%\"> <tr> <td align=\"right\" valign=\"top\"> <img " .
            "src=\"" . $url . "/Pessoas/" . $contrato . "/Empresa/logo2.png\" height=\"30\"/> </td></tr></table> </td></tr></table> </td></tr></table> </td></tr></table> <table bgcolor=\"#E1E1E1\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" id=\"emailFooter\"> <tr> <td align=\"center\" valign=\"top\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"> <tr> <td align=\"center\" valign=\"top\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" class=\"flexibleContainer\"> <tr> <td align=\"center\" valign=\"top\" width=\"600\" class=\"flexibleContainerCell\"> <table border=\"0\" cellpadding=\"30\" cellspacing=\"0\" width=\"100%\"> <tr> <td valign=\"top\" bgcolor=\"#E1E1E1\"> <div style=\"font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;\"> <div>Todos os direitos reservados &#169; " . date("Y") . " </div></div></td></tr></table> </td></tr></table> </td></tr></table> </td></tr></table> </td></tr></table> </center> </body></html>";
            $mail->AddAddress($email[0], $email[2]);
            $mail->Subject = utf8_decode($assunto);
            $mail->Body = utf8_decode($mensagem);
            $enviado = $mail->Send();
            $mail->ClearAllRecipients();
            $mail->ClearAttachments();
            if ($enviado) {
                $qtd_sucesso = $qtd_sucesso + 1;
            } else {
                $invalidos = $invalidos . utf8_decode($email[0]) . ",";
            }
            if ($email[1] > 0) {
                $query = "insert into sf_emails_hist (id_fornecedores_despesas, descricao, data_envio, id_user_resp, retorno) values
                (" . $email[1] . ",'" . utf8_decode($anexo) . "',getdate()," . $_SESSION["id_usuario"] . "," . (($enviado) ? "0" : "1") . ");
                insert into sf_telemarketing(de_pessoa,para_pessoa,data_tele,pendente,proximo_contato,obs,pessoa,data_tele_fechamento, obs_fechamento, de_pessoa_fechamento,especificacao,atendimento_servico,garantia,relato,origem,acompanhar,dt_inclusao)
                values (" . $_SESSION["id_usuario"] . "," . $_SESSION["id_usuario"] . ",getdate(),0,getdate(),'" . (($enviado) ? "" : "ERRO ") . "EMAIL MARKETING'," . $email[1] . ",getdate(),'" . (($enviado) ? "" : "ERRO ") . "EMAIL MARKETING', " . $_SESSION["id_usuario"] . ",'E', 0,0,'" . utf8_decode($assunto) . "',0,0,getdate());";
                odbc_exec($con, $query) or die(odbc_errormsg());                    
            }   
            if (is_numeric($email[5])) {
                odbc_exec($con, "update sf_boleto set exp_email = " . ($enviado ? "1" : "2") . " where bol_nosso_numero = " . valoresTexto2($email[5])) or die(odbc_errormsg());
            }
        }
        ob_end_clean();
    }
    $local[] = array('qtd_total' => sizeof($items), 'qtd_sucesso' => $qtd_sucesso, 'invalidos' => ($invalidos == "" ? 0 : substr($invalidos, 0, -1)));
    echo(json_encode($local));
}

odbc_close($con);
