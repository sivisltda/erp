<?php

require_once(__DIR__ . '/../../../Connections/configini.php');

$cpf_cnpj = "";
$cedente = "";
$nome_fantasia = "";
$inscricao = "";
$endereco = "";
$cidade_uf = "";
$cep = "";
$telefone = "";
$site = "";
$codigo = "";
$titular = "";
$convite = "";
$convidado = "";
$servico = "";
$valorServico = "";
$emissao = "";
$validade = "";
$operador = "";

if (isset($_POST['bntSave'])) {
    if (valoresSelect("txtPagamento") == "null") {
        $data = substr($_POST['txtDataInicio'], 3);
        $query = "set dateformat dmy; select count(id_convite) convites, 
        (select max(clb_limite_convite) from sf_configuracao) limite
        from sf_convites where id_fornecedor = " . valoresNumericos("txtAluno") . " and inativo = 0 and item_venda is null and id_agendamento is null
        and dt_emissao between dateadd(day, -1 * (select max(clb_dias_convite) from sf_configuracao), getdate()) and getdate()";
        $cur = odbc_exec($con, $query);
        while ($RFP = odbc_fetch_array($cur)) {
            if($RFP['convites'] >= $RFP['limite']) {
                echo "Limite máximo de convites gratuitos mensais!"; exit;
            }
        }
    }
    $query = "set dateformat dmy;";
    if ($_POST['txtId'] != '') {
        $query .= "update sf_convites set " .
                "id_fornecedor = " . valoresNumericos("txtAluno") . "," .
                "item_venda = " . valoresSelect("txtPagamento") . "," .
                "dt_ini = " . valoresData("txtDataInicio") . "," .
                "dt_fim = " . valoresData("txtDataFim") . "," .
                "documento = " . valoresTexto("txtDocumento") . "," .
                "id_agendamento = " . valoresSelect("txtAgendamento") . "," .
                "nome_convidado = " . valoresTexto("txtNome") . " " .
                " where id_convite = " . $_POST['txtId'];
        odbc_exec($con, $query) or die(odbc_errormsg());
    } else {
        $query .= "insert into sf_convites(id_fornecedor,item_venda,dt_ini,dt_fim,
        dt_emissao,operador,documento,id_agendamento,nome_convidado,inativo) values (" .
                valoresNumericos("txtAluno") . "," .
                valoresSelect("txtPagamento") . "," .
                valoresData("txtDataInicio") . "," .
                valoresData("txtDataFim") . ", getdate()," .
                valoresTexto2($_SESSION["login_usuario"]) . "," .
                valoresTexto("txtDocumento") . "," .
                valoresSelect("txtAgendamento") . "," .
                valoresTexto("txtNome") . ",0)";
        odbc_exec($con, $query) or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_convite from sf_convites order by id_convite desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
    echo $_POST['txtId'];
}

if (!is_numeric($_GET["emp"]) && is_numeric($_GET["id"])) {
    $local = array();
    $cur = odbc_exec($con, "select * from sf_convites where id_convite =" . $_GET["id"]);
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('id' => utf8_encode($RFP['id_convite']), 'item_venda' => utf8_encode($RFP['item_venda']),
            'dt_ini' => escreverData($RFP['dt_ini']), 'dt_fim' => escreverData($RFP['dt_fim']), 'id_fornecedor' => utf8_encode($RFP['id_fornecedor']),
            'documento' => utf8_encode($RFP['documento']), 'nome_convidado' => utf8_encode($RFP['nome_convidado']));
    }
    echo(json_encode($local));
}

if (is_numeric($_GET["emp"])) {
    $convites = "0";    
    $convitesArray = array();    
    $cur = odbc_exec($con, "select * from sf_filiais where numero_filial = " . $_GET["emp"]);
    while ($RFP = odbc_fetch_array($cur)) {
        $cpf_cnpj = utf8_encode($RFP["cnpj"]);
        $cedente = utf8_encode($RFP["razao_social_contrato"]);
        $nome_fantasia = utf8_encode($RFP["nome_fantasia_contrato"]);
        $inscricao = utf8_encode($RFP["inscricao_estadual"]);
        $endereco = utf8_encode($RFP['endereco']) . " n°" . utf8_encode($RFP['numero']) . "," . utf8_encode($RFP['bairro']);
        $cidade_uf = utf8_encode($RFP['cidade_nome']) . " " . utf8_encode($RFP['estado']);
        $cep = utf8_encode($RFP['cep']);
        $telefone = utf8_encode($RFP['telefone']);
        $site = utf8_encode($RFP['site']);
    }
    $ids = explode("|", $_GET["id"]);
    for ($i = 0; $i < count($ids); $i++) {
        if (is_numeric($ids[$i])) {
            $convites .= "," . $ids[$i];
        }
    }    
    $query = "select f.id_fornecedores_despesas, f.razao_social,
    c.id_convite,c.dt_ini, c.dt_fim, c.dt_emissao,c.operador,c.documento,c.nome_convidado,
    vi.valor_total,vi.quantidade,p.descricao,descricao_agenda 
    from sf_convites c inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = c.id_fornecedor
    left join sf_vendas_itens vi on vi.id_item_venda = c.item_venda
    left join sf_produtos p on p.conta_produto = vi.produto
    left join sf_agendamento on c.id_agendamento = sf_agendamento.id_agendamento
    left join sf_agenda on sf_agenda.id_agenda = sf_agendamento.id_agenda    
    where c.inativo = 0 and c.id_convite in (" . $convites . ")";
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["codigo"] = utf8_encode($RFP["id_convite"]);
        $row["titular"] = str_pad($RFP["id_fornecedores_despesas"], 6, "0", STR_PAD_LEFT) . " - " . utf8_encode($RFP["razao_social"]);
        $row["convite"] = utf8_encode($RFP["documento"]);
        $row["convidado"] = utf8_encode($RFP["nome_convidado"]);
        $row["servico"] = utf8_encode($RFP["descricao"]);
        $row["valorServico"] = escreverNumero(($RFP["valor_total"] / $RFP["quantidade"]), 1);
        $row["emissao"] = escreverData($RFP["dt_emissao"]);
        $row["validade"] = ($RFP["dt_ini"] == $RFP["dt_fim"] ? escreverData($RFP["dt_ini"]) : escreverData($RFP["dt_ini"]) . " até " . escreverData($RFP["dt_fim"]));
        $row["operador"] = utf8_encode($RFP["operador"]);
        $row["descricao_agenda"] = utf8_encode($RFP["descricao_agenda"]);
        $convitesArray[] = $row;
    }
}

if (isset($_POST['del'])) {  
    $where = str_replace("|", ",", $_POST['del']);
    $query = "UPDATE sf_convites SET operador_cancel = " . valoresTexto2($_SESSION["login_usuario"]) . ",dt_cancel = getdate(), inativo = 1 
    WHERE id_convite in (" . (substr($where, -1) == "," ? substr($where, 0, -1) : $where) . ")";
    odbc_exec($con, $query) or die(odbc_errormsg());
    echo "YES";
}

odbc_close($con);
