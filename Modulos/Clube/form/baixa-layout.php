<?php

require_once(__DIR__ . '/../../../Connections/configini.php');

if (isset($_POST["btnSave"])) {
    $id_all_mens = $_POST["txtId"];
    $mensalidades = explode(",", $id_all_mens);
    $pessoa = valoresNumericos("txtPessoa");
    $DatasPg = valoresData("txtDataPg");
    $ValorPg = valoresNumericos("txtValorPg");
    $MensPg = valoresNumericos("txtMensalidade");
    $tipo_documento = "10";
    $carteira = 0;    
    $gerarAuto = 0;
    $gerarAutoTotal = 0;
    $cur = odbc_exec($con, "select tipo_documento from sf_layout where origem = 'B' AND inativo = 0 AND id = " . valoresNumericos("txtLayout")) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $tipo_documento = (is_numeric($RFP['tipo_documento']) ? $RFP['tipo_documento'] : $tipo_documento);
    }    
    $cur = odbc_exec($con, "select * from sf_configuracao where id = 1");
    while ($RFP = odbc_fetch_array($cur)) {
        $carteira = $RFP['fin_baixa_menor_carteira'];        
        $gerarAuto = $RFP['ACA_RENOVACAO_AUTO'];
        $gerarAutoTotal = $RFP['clb_mensalidades_aberto'];
    }
    odbc_autocommit($con, false);
    $query = "set dateformat dmy;
    insert into sf_vendas(vendedor, cliente_venda, historico_venda, data_venda, sys_login, empresa, destinatario, status, tipo_documento, cov,
    descontop, descontos, descontotp, descontots, n_servicos, data_aprov, grupo_conta, conta_movimento, favorito, reservar_estoque, n_produtos)
    values ('" . $_SESSION["login_usuario"] . "'," . $pessoa . "," . valoresTexto2('PAG PLANO / SERVIÇO') . "," . $DatasPg . ",'" .
    $_SESSION["login_usuario"] . "',1,'" . $_SESSION["login_usuario"] . "','Aprovado',10,'V',0.00,0.00,0,0," . ($ValorPg > 0 ? "0" : "1") . "," . 
    $DatasPg . ",14,1,0,0," . ($ValorPg > 0 ? "0" : "1") . ");        
    DECLARE @idVenda BIGINT;SELECT @idVenda = SCOPE_IDENTITY();";
    for ($i = 0; $i < count($mensalidades); $i++) {
        $query .= "insert into sf_vendas_itens(id_venda, grupo, produto, quantidade, valor_total, valor_bruto, valor_multa)               
        select @idVenda,conta_movimento,conta_produto,1,dbo.CALC_PROPORCAO(dbo.VALOR_REAL_MENSALIDADE(id_mens)," . $ValorPg . "," . $MensPg . "),
        dbo.CALC_PROPORCAO(dbo.VALOR_REAL_MENSALIDADE(id_mens)," . $ValorPg . "," . $MensPg . "),0.00 from sf_vendas_planos_mensalidade 
        inner join sf_produtos_parcelas on id_parcela = id_parc_prod_mens left join sf_produtos on sf_produtos.conta_produto = id_produto
        where id_mens = " . $mensalidades[$i] . "; DECLARE @idItemVenda" . $i . " BIGINT;SELECT @idItemVenda" . $i . " = SCOPE_IDENTITY();"; 
    }
    for ($i = 0; $i < count($mensalidades); $i++) {
        $query .= "update sf_vendas_planos_mensalidade set id_item_venda_mens = @idItemVenda" . $i . ", dt_pagamento_mens = " . $DatasPg . " where id_mens = " . $mensalidades[$i] . ";";
    }         
    if ($ValorPg > 0) {
        $query .= "insert into sf_venda_parcelas(venda, numero_parcela, data_parcela, valor_parcela, valor_pago, data_pagamento, id_banco,pa,
        inativo, historico_baixa, valor_multa, valor_juros, syslogin, tipo_documento, valor_parcela_liquido, data_cadastro, exp_remessa, somar_rel, obs_baixa)
        values(@idVenda, '1'," . $DatasPg . ", " . $ValorPg . "," . $ValorPg . "," . $DatasPg . ",1,'01/" . (($carteira > 0 && $MensPg > $ValorPg) ? "02" : "01") . "',0," . 
        valoresTexto2('PAG PLANO / SERVIÇO') . ",0.00,0.00,'" . $_SESSION["login_usuario"] . "'," . $tipo_documento . ",0.00, GETDATE(),0,1," . valoresTexto2($nome) . ");";  
        $query .= "update sf_venda_parcelas set valor_parcela_liquido = [dbo].[FU_TAXA_FORMA_PAGAMENTO](sf_venda_parcelas.tipo_documento,sf_venda_parcelas.venda, 0, valor_parcela),
        id_banco = isnull((select max(id_banco_baixa) banco from sf_tipo_documento where sf_tipo_documento.id_tipo_documento = sf_venda_parcelas.tipo_documento),1)        
        where venda = @idVenda;";  
    }    
    $query .= "update sf_vendas set conta_movimento = [dbo].[FU_SUB_GRUPO_VENDA](id_venda) where cov = 'V' and id_venda = @idVenda;";    
    if ($carteira > 0 && $MensPg > $ValorPg) {
        $query .= "insert into sf_venda_parcelas(venda, numero_parcela, data_parcela, valor_parcela, valor_pago, data_pagamento, id_banco,pa, inativo, 
        historico_baixa, valor_multa, valor_juros, syslogin, tipo_documento, valor_parcela_liquido, data_cadastro, exp_remessa, somar_rel, obs_baixa)
        values(@idVenda, '2'," . $DatasPg . ", " . ($MensPg - $ValorPg) . ", 0.00, null, 1, '02/02',0," . 
        valoresTexto2('PAG PLANO / SERVIÇO') . ",0.00,0.00,'" . $_SESSION["login_usuario"] . "',12," . ($MensPg - $ValorPg) . ", GETDATE(),0,0," . valoresTexto2($nome) . ");";        
        $query .= "insert into sf_creditos (id_fornecedor_despesa, valor, data, tipo, id_venda, historico, dt_vencimento, usuario_resp, inativo)
        values(" . $pessoa . "," . ($MensPg - $ValorPg) . "," . $DatasPg . ",'D',@idVenda," . valoresTexto2('PAG PLANO / SERVIÇO') . "," . $DatasPg . "," . $_SESSION["id_usuario"] . ",0);";
    }        
    for ($i = 0; $i < count($mensalidades); $i++) {
        $query .= "update sf_vendas_planos set dt_fim = dbo.FU_PLANO_FIM(id_plano) where id_plano in(
        select id_plano_mens from sf_vendas_planos_mensalidade where id_mens in (" . $mensalidades[$i] . "));";
    }
    if($gerarAuto == 1) {
        for ($i = 0; $i < $gerarAutoTotal; $i++) {            
            $query .= " insert into sf_vendas_planos_mensalidade (id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens)
            select id_plano_mens, dateadd(month," . $i . ",dateadd(day,1,dt_fim_mens)),
            case when tp_valid_acesso = 'C' then dateadd(day, qtd_dias_acesso,dt_fim_mens) else 
                dateadd(day,-1, dateadd(month,1,dateadd(month," . $i . ",dateadd(day,1,dt_fim_mens))))
            end,
            id_parc_prod_mens, GETDATE(),valor_unico valor_mens
            from sf_vendas_planos_mensalidade m1 inner join sf_produtos_parcelas pp on m1.id_parc_prod_mens = pp.id_parcela
            inner join sf_produtos p on p.conta_produto = pp.id_produto
            inner join sf_vendas_planos on id_plano = id_plano_mens            
            where p.inativar_renovacao = 0 and id_mens in (" . $id_all_mens . ") and dt_cancelamento is null and parcela in (-1,0,1) and (select COUNT(*) from sf_vendas_planos_mensalidade m2
            where m2.id_plano_mens = m1.id_plano_mens and m2.id_parc_prod_mens = m1.id_parc_prod_mens
            and ((tp_valid_acesso <> 'C' and month(m2.dt_inicio_mens) = month(dateadd(month," . ($i + 1) . ",m1.dt_inicio_mens)) and year(m2.dt_inicio_mens) = year(dateadd(month," . ($i + 1) . ",m1.dt_inicio_mens))
            ) or (tp_valid_acesso = 'C' and dateadd(day," . ($i + 1) . ",m1.dt_fim_mens) = dt_inicio_mens))) = 0;";            
        }
    }
    //echo $query; exit;    
    $objExec = odbc_exec($con, $query);
    if (!$objExec) {
        odbc_rollback($con);
        echo "ERROR"; exit();
    } else if ($objExec) {
        odbc_commit($con);
        echo "YES";
    }    
}
odbc_close($con);