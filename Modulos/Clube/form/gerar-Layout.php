<?php

if (!isset($_GET['pdf'])) {
    require_once(__DIR__ . '/../../../Connections/configini.php');
    require_once(__DIR__ . '/../../../util/util.php');
}

$aIn = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$aPt = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
$data = array();
$layout = array();
$layoutItem = array();
$layoutCabecalho = array();
$layoutRodape = array();
$dependenteBoleto = 0;
$planosBoleto = 0;
$aGroupBegin = "";
$table1 = "";
$table2 = "";
$aGroupEnd = "";
$where = "";
$whereX = "";

if ($_GET['dti'] != '' && $_GET['dtf'] != '' && $_GET['dtp'] != '') {
    if ($_GET['dtp'] == "0") {
        $where .= " and sf_fornecedores_despesas.dt_cadastro between " . valoresDataHoraUnico2($_GET['dti'] . " 00:00:00") . " and " . valoresDataHoraUnico2($_GET['dtf'] . " 23:59:59") . "";
    } elseif ($_GET['dtp'] == "1") {
        $where .= " and vm.dt_inicio_mens between " . valoresDataHoraUnico2($_GET['dti'] . " 00:00:00") . " and " . valoresDataHoraUnico2($_GET['dtf'] . " 23:59:59") . " and vm.dt_pagamento_mens is null ";
    } elseif ($_GET['dtp'] == "2") {
        $where .= " and vm.dt_pagamento_mens between " . valoresDataHoraUnico2($_GET['dti'] . " 00:00:00") . " and " . valoresDataHoraUnico2($_GET['dtf'] . " 23:59:59") . "";
    }
}

if (isset($_GET['ti']) && $_GET['ti'] != 'null') {
    $where .= " and titularidade in (" . $_GET['ti'] . ")";
}

if (isset($_GET['st']) && $_GET['st'] != 'null') {
    $where .= " and (";
    $dataAux = explode(",", $_GET['st']);
    for ($i = 0; $i < count($dataAux); $i++) {
        $where .= ($i > 0 ? " or " : "") . " fornecedores_status = '" . getStatus($dataAux[$i]) . "'";
    }
    $where .= ")";
}

if (isset($_GET['cr']) && $_GET['cr'] != 'null') {
    $where .= " and status_cracha in (" . $_GET['cr'] . ")";
}

if (isset($_GET['tp']) && $_GET['tp'] != 'null') {
    $where .= " and tipo_socio in (" . $_GET['tp'] . ")";
}

if (isset($_GET['gr']) && $_GET['gr'] != 'null') {
    $where .= " and grupo_pessoa in (" . $_GET['gr'] . ")";
}

if (isset($_GET['tr']) && is_numeric($_GET['tr'])) {
    $where .= " and trancar = " . $_GET['tr'];
}

if (isset($_GET['tt']) && is_numeric($_GET['tt'])) {
    $where .= " and titular = " . $_GET['tt'];
}

$cur = odbc_exec($con, "select FIN_GRUPO_BOLETO,FIN_GRUPO_BOLETO_MOD from sf_configuracao") or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $dependenteBoleto = $RFP['FIN_GRUPO_BOLETO'];
    $planosBoleto = $RFP['FIN_GRUPO_BOLETO_MOD'];
}

if (isset($_GET['gr']) && is_numeric($_GET['gr'])) {
    $idLayout = 0;
    $cur = odbc_exec($con, "select * from sf_layout where origem = 'R' and inativo = 0 and grupo = " . $_GET['gr']);
    while ($RFP = odbc_fetch_array($cur)) {
        $idLayout = $RFP["id"];
        $layout = array("id" => $RFP["id"], "descricao" => utf8_encode($RFP["descricao"]), "tipo" => $RFP["tipo"],
            "extensao" => $RFP["extensao"], "id_produto" => $RFP["id_produto"], "id_campo_livre" => $RFP["id_campo_livre"]);
    }
    $cur = odbc_exec($con, "select * from sf_layout_item where id_layout = " . $idLayout . " order by ordinal");
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array(
            "id" => $RFP["id"],
            "descricao" => utf8_encode($RFP["descricao"]),
            "campo" => $RFP["campo"],
            "tamanho" => $RFP["tamanho"],
            "completar" => utf8_encode($RFP["completar"]),
            "completar_direcao" => $RFP["completar_direcao"],
            "separador" => $RFP["separador"],
        );
        if ($RFP["tipo_linha"] == 0) {
            $layoutItem[] = $row;
        } else if ($RFP["tipo_linha"] == 1) {
            $layoutCabecalho[] = $row;
        } else if ($RFP["tipo_linha"] == 2) {
            $layoutRodape[] = $row;
        }
    }
    if (is_numeric($layout["id_produto"])) {
        $whereX = " and id_prod_plano = " . $layout["id_produto"];
    }
    //-----------------------------------------------------------------------------------------------------------------------    

    if ($planosBoleto > 0) {
        $aGroupBegin = "select x.id_fornecedores_despesas, x.matricula, x.razao_social, x.cnpj, x.inscricao_estadual, x.funcao_contato,
        min(x.dt_inicio_mens) dt_inicio_mens, sum(x.valor_mens) valor_mens from (";
        $aGroupEnd = ") as x group by x.id_fornecedores_despesas,x.matricula, x.razao_social, x.cnpj, x.inscricao_estadual, x.funcao_contato";
    }
    $table1 = "SELECT id_fornecedores_despesas, (select conteudo_campo from sf_fornecedores_despesas_campos_livres 
    where fornecedores_campos = id_fornecedores_despesas and campos_campos = " . valoresNumericos2($layout["id_campo_livre"]) . ") matricula,
    razao_social, cnpj, inscricao_estadual,dt_inicio_mens, dbo.VALOR_REAL_MENSALIDADE(vm.id_mens) valor_mens, funcao_contato        
    from sf_fornecedores_despesas 
    left join sf_categoria_socio on sf_categoria_socio.id_cat = sf_fornecedores_despesas.titularidade 
    inner join sf_vendas_planos vp on vp.favorecido = id_fornecedores_despesas and dt_cancelamento is null 
    inner join sf_vendas_planos_mensalidade vm on vp.id_plano = vm.id_plano_mens
    where socio = 1 and tipo = 'C' and inativo = 0 " . $whereX . $where;
    if ($dependenteBoleto > 0) {
        $table2 = "union all SELECT id_fornecedores_despesas, (select conteudo_campo from sf_fornecedores_despesas_campos_livres 
        where fornecedores_campos = id_fornecedores_despesas and campos_campos = " . valoresNumericos2($layout["id_campo_livre"]) . ") matricula,
        razao_social, cnpj, inscricao_estadual,dt_inicio_mens, dbo.VALOR_REAL_MENSALIDADE(vm.id_mens) valor_mens, funcao_contato
        from sf_fornecedores_despesas_dependentes 
        inner join sf_fornecedores_despesas on id_fornecedores_despesas = id_titular
        left join sf_categoria_socio on sf_categoria_socio.id_cat = sf_fornecedores_despesas.titularidade 
        inner join sf_vendas_planos vp on vp.favorecido = id_dependente and dt_cancelamento is null 
        inner join sf_vendas_planos_mensalidade vm on vp.id_plano = vm.id_plano_mens
        where socio = 1 and tipo = 'C' and inativo = 0 " . $whereX . $where;
    }
    //echo $aGroupBegin . $table1 . $table2 . $aGroupEnd; exit;
    $cur = odbc_exec($con, "set dateformat dmy;" . $aGroupBegin . $table1 . $table2 . $aGroupEnd);
    while ($RFP = odbc_fetch_array($cur)) {
        $data[] = array(
            "id_fornecedores_despesas" => $RFP["id_fornecedores_despesas"],
            "matricula" => $RFP["matricula"],
            "razao_social" => utf8_encode($RFP["razao_social"]),
            "cnpj" => $RFP["cnpj"],
            "inscricao_estadual" => $RFP["inscricao_estadual"],
            "funcao_contato" => $RFP["funcao_contato"],
            "dt_inicio_mens" => $RFP["dt_inicio_mens"],
            "valor_mens" => $RFP["valor_mens"]);
    }
    $i = 0;
    $total = 0;
    $filename = $layout["descricao"] . " - " . str_replace($aIn, $aPt, date("F Y")) . "." . $layout["extensao"];
    if ($layout["extensao"] == "txt") {
        $filename = __DIR__ . "./../../../Pessoas/" . $_SESSION["contrato"] . "/LoteBoletos/" . $filename;
        if (!$handle = fopen($filename, 'w+')) {
            erro("Não foi possível abrir o arquivo ($filename)");
        }
        if (count($layoutCabecalho) > 0) {
            $i++;
            $linex = array("data_atual" => date("Ymd"), "numero_linha" => $i, "valor_total" => $total);
            foreach ($layoutCabecalho as $item) {
                if (fwrite($handle, formatField($layout, $item, $linex)) === FALSE) {
                    echo "Não foi possível escrever no arquivo ($filename)";
                }
            }
            if (fwrite($handle, "\n") === FALSE) {
                echo "Não foi possível escrever no arquivo ($filename)";
            }
        }
        foreach ($data as $line) {
            $i++;
            $total += ($line["valor_mens"] > 0 ? $line["valor_mens"] : 0);
            $linex = array("data_atual" => date("Ymd"), "numero_linha" => $i, "valor_total" => $total);            
            foreach ($layoutItem as $item) {
                if (fwrite($handle, formatField($layout, $item, $line)) === FALSE) {
                    echo "Não foi possível escrever no arquivo ($filename)";
                }
            }
            if (fwrite($handle, "\n") === FALSE) {
                echo "Não foi possível escrever no arquivo ($filename)";
            }
        }
        if (count($layoutRodape) > 0) {
            $i++;
            $linex = array("data_atual" => date("Ymd"), "numero_linha" => $i, "valor_total" => $total);
            foreach ($layoutRodape as $item) {
                if (fwrite($handle, formatField($layout, $item, $linex)) === FALSE) {
                    echo "Não foi possível escrever no arquivo ($filename)";
                }
            }
            if (fwrite($handle, "\n") === FALSE) {
                echo "Não foi possível escrever no arquivo ($filename)";
            }
        }
        fclose($handle);
        header("Content-Type: " . "application/txt");
        header("Content-Length: " . filesize($filename));
        header("Content-Disposition: attachment; filename=" . basename($filename));
        readfile($filename);
    } else if ($layout["extensao"] == "xls") {
        $html = '';
        $html .= '<table>';
        $html .= '<tr>';
        foreach ($layoutItem as $item) {
            $html .= '<td>' . $item["descricao"] . '</td>';
        }
        $html .= '</tr>';
        foreach ($data as $line) {
            $html .= '<tr>';
            foreach ($layoutItem as $item) {
                $html .= '<td>' . formatField($layout, $item, $line) . '</td>';
            }
            $html .= '</tr>';
        }
        $html .= $conteudoCSN;
        $html .= '</table>';
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: application/x-msexcel");
        header("Content-Disposition: attachment; filename=\"{$filename}\"");
        header("Content-Description: PHP Generated Data");
        echo "\xEF\xBB\xBF";
        echo $html;
    } else if ($layout["extensao"] == "csv") {
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Pragma: no-cache');
        header('Expires: 0');
        $file = fopen('php://output', 'w');
        $cabecalho = array();
        foreach ($layoutItem as $item) {
            $cabecalho[] = $item["descricao"];
        }
        fputcsv($file, $cabecalho, ';');
        foreach ($data as $line) {
            $row = array();
            foreach ($layoutItem as $item) {
                $row[] = formatField($layout, $item, $line);
            }
            fputcsv($file, $row, ';');
        }
    }
} else {
    echo "Favor informar 1 grupo!";
}

function formatField($layout, $item, $line) {
    $value = (strlen($item["campo"]) > 0 ? $line[$item["campo"]] : ($layout["extensao"] == "txt" ? $item["descricao"] : $item["completar"]));
    if ($item["campo"] == "dt_inicio_mens" && ($layout["extensao"] == "csv" || $layout["extensao"] == "xls")) {
        $value = escreverData($value, "d" . $item["separador"] . "m" . $item["separador"] . "Y");
    } else if ($item["campo"] == "dt_inicio_mens" && $layout["extensao"] == "txt") {
        $value = escreverData($value, "Y" . $item["separador"] . "m" . $item["separador"] . "d");
    } else if ($item["campo"] == "valor_mens") {
        $value = escreverNumero($value, 0, 2, $item["separador"], "");        
    } else if ($item["campo"] == "valor_total") {
        $value = escreverNumero($value, 0, 2, $item["separador"], "");
    }
    if ($layout["extensao"] == "txt") {
        $value = substr(str_pad($value, $item["tamanho"], ($item["completar"] == "" ? " " : $item["completar"]), ($item["completar_direcao"] == "E" ? STR_PAD_LEFT : STR_PAD_RIGHT)), 0, $item["tamanho"]);
    }
    return $value;
}

odbc_close($con);
