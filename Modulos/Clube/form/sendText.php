<?php

include "./../../../Connections/configini.php";

$pasta = "./../../../Pessoas/" . $contrato . "/Retorno/";

$nome = $_FILES['arquivo']['name'];
$type = $_FILES['arquivo']['type'];
$size = $_FILES['arquivo']['size'];
$tmp = $_FILES['arquivo']['tmp_name'];
if ($tmp) {
    move_uploaded_file($tmp, $pasta . "/" . $nome);
}

$lendo = @fopen($pasta . "/" . $nome, "r");
if (!$lendo) {
    echo "Erro ao abrir a URL.";
    exit;
}

$output = array();
while (!feof($lendo)) {
    $type = fgets($lendo, 9999);
    if ($type != false) {
        $output[] = $type;
    }
}
echo json_encode($output);
