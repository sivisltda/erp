<?php

require_once(__DIR__ . '/../../Connections/configini.php');
$aColumns = array('id', 'data', 'descricao', 'tipo');
$table = "sf_excecao_vencimento";
$PageName = "Excecao-Vencimento";
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY " . $aColumns[1] . " asc ";
$sOrder2 = " ORDER BY " . $aColumns[1] . " asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) < count($aColumns) - 1) {
                $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 0) . " " . $_GET['sSortDir_' . $i] . ", ";
                $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 1) . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY " . $aColumns[1] . " asc";
        $sOrder2 = " ORDER BY " . $aColumns[1] . " asc";
    }
}

if ($_GET['txtBusca'] != "") {
    $sWhereX .= " and (descricao like '%" . $_GET['txtBusca'] . "%')";
}

if ($_GET['tip'] != "") {
    $sWhereX .= " and tipo = " . $_GET['tip'];
}

if (isset($_GET['dti']) && isset($_GET['dtf']) && $_GET['dti'] != '' && $_GET['dtf'] != '') {
    $sWhereX .= " and data between " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']);
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($i == 0) {
        $colunas = $aColumns[$i];
    } else {
        $colunas = $colunas . "," . $aColumns[$i];
    }
}

$sQuery = "set dateformat dmy; SELECT COUNT(*) total from " . $table . " where " . $aColumns[0] . " > 0 " . $sWhereX . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iTotal,
    "aaData" => array()
);

$sQuery1 = "set dateformat dmy; SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row," . $colunas . "
from " . $table . " where " . $aColumns[0] . " > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder2;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $aPt = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
    $d = escreverData($aRow[$aColumns[1]]);
    $data = ($aRow[$aColumns[3]] == 1 ? substr($d, 0, 2) . " de " . $aPt[substr($d, 3, 2) - 1] : $d);
    $row = array();
    $row[] = utf8_encode($aRow[$aColumns[0]]);
    $row[] = "<a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[$aColumns[0]]) . "," . utf8_encode($aRow[$aColumns[3]]) . ")'><div id='formPQ'>" . $data . "</div></a>";
    $row[] = "<div id='formPQ'>" . utf8_encode($aRow[$aColumns[2]]) . "</div>";
    if ($imprimir == 0) {
        $row[] = "<center><a title='Excluir' onClick=\"return confirm('Deseja excluir esse registro?');\" href='" . $PageName . ".php?Del=" . $aRow[$aColumns[0]] . $toReturnAnd . "'><input name='' type='image' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
    }
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);