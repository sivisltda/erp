<?php
include "../../Connections/configini.php";

$pasta = "../../Pessoas/" . $contrato . "/Retorno";
$nome = "";
$idLayout = 0;
$DescPagina = "Gerar Boletos";
$PageName = "Gerar-Boletos";
$Module = "Clube";
$itensPlano = [];

if (isset($_POST['txtNome'])) {
    $nome = $_POST['txtNome'];
}

if (isset($_POST['txtLayout']) && is_numeric($_POST['txtLayout'])) {
    $idLayout = $_POST['txtLayout'];    
}

if (isset($_POST['itemsPlano'])) {
    for ($i = 0; $i < count($_POST['itemsPlano']); $i++) {
        $itensPlano[] = $_POST['itemsPlano'][$i];
    }
}

if (isset($_POST['bntLer'])) {
    $nome = $_FILES['arquivo']['name'];
    $type = $_FILES['arquivo']['type'];
    $size = $_FILES['arquivo']['size'];
    $tmp = $_FILES['arquivo']['tmp_name'];
    if ($tmp) {
        move_uploaded_file($tmp, $pasta . "/" . $nome);            
    }        
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../SpryAssets/SpryValidationTextField.css"/>
        <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>        
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1><?php echo $Module; ?><small><?php echo $DescPagina; ?></small></h1>
                    </div>
                    <form action="Baixar-Layout.php" method="post" enctype="multipart/form-data" name="enviar" id="enviar">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="boxfilter block">
                                    <div style="float:left; width: 25%;">
                                        <div width="100%">Arquivo Retorno:</div>
                                        <input class="btn  btn-primary" type="file" name="arquivo" id="arquivo" style="height:20px; line-height:21px; width:100%"/>
                                    </div>
                                    <div style="float:left;margin-top: 15px;width: 17%;margin-left: 2%;">
                                        <select id="txtLayout" name="txtLayout" class="select" style="width:100%">
                                            <option value="null" datas="1">--Selecione--</option>
                                            <?php $cur = odbc_exec($con, "select *,(select count(*) total from sf_layout_item where campo = 'dt_inicio_mens' and tipo_linha = 0 and id_layout = sf_layout.id) total_data from sf_layout where origem = 'B' AND inativo = 0") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id'] ?>" datas="<?php echo $RFP['total_data'] ?>" taxa="<?php echo escreverNumero($RFP['taxa']); ?>" <?php
                                                if (!(strcmp($RFP['id'], $idLayout))) {
                                                    echo "SELECTED";
                                                }
                                                ?>><?php echo utf8_encode($RFP['descricao']) . " (." . $RFP['extensao'] . ")"; ?></option>
                                            <?php } ?>
                                        </select>                                            
                                    </div>                                        
                                    <div style="float:left; margin-left:0.5%; display: none;">
                                        <span style="display:block;">Data Ref.</span>
                                        <input id="txt_dt_baixa" name="txt_dt_baixa" type="text" class="datepicker data-mask" style="width:79px; margin-bottom:2px" value="<?php echo (isset($_POST["txt_dt_baixa"]) ? $_POST["txt_dt_baixa"] : getData("T")); ?>">
                                    </div>  
                                    <div style="float:left; margin-left:0.5%; display: none;">
                                        <span style="display:block;">Data Pag.</span>
                                        <input id="txt_dt_pag" name="txt_dt_pag" type="text" class="datepicker data-mask" style="width:79px; margin-bottom:2px" value="<?php echo (isset($_POST["txt_dt_pag"]) ? $_POST["txt_dt_pag"] : getData("T")); ?>">
                                    </div>
                                    <div style="float:left;margin-top: 15px;width: 17%;margin-left: 0.5%;">
                                        <select name="itemsPlano[]" id="mscPlano" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                            <?php $cur = odbc_exec($con, "SELECT p.conta_produto,p.descricao from sf_produtos p left join sf_contas_movimento on p.conta_movimento = sf_contas_movimento.id_contas_movimento where p.tipo = 'C' and p.conta_produto > 0") or die(odbc_errormsg());
                                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['conta_produto'] ?>" <?php echo (in_array($RFP['conta_produto'], $itensPlano) ? "selected" : ""); ?>><?php echo utf8_encode($RFP['descricao']) ?></option>
                                            <?php } ?>
                                        </select>                                            
                                    </div>                                    
                                    <div style="float:left;margin-top: 15px; margin-left:0.3%;">                                        
                                        <button id="bntLer" name="bntLer" type="submit" value="Ler" onclick="$('#loader').show();" class="button button-turquoise btn-primary"><span>Ler</span></button>
                                    </div>
                                    <div style="float:right;margin-top: 15px;">                                        
                                        <button id="bntSave" name="bntSave" type="button" value="Salvar" onclick="baixar();" class="button button-turquoise btn-primary" <?php echo ($nome == "" || $idLayout == 0 ? "disabled" : ""); ?>>Baixar</button> 
                                    </div>                                    
                                    <div style="float:right;margin-top: 15px;margin-left: 2%;">
                                        <select id="txtLayoutStatus" name="txtLayoutStatus" class="select" style="width:100%">
                                            <option value="null">Todos</option>
                                            <option value="inativo">Não identificados</option>
                                            <option value="inativox">Sem Mensalidades</option>
                                            <option value="apagar">A pagar</option>                                            
                                            <option value="apagarx">A pagar Divergentes</option>
                                            <option value="pago">Pagos</option>
                                            <option value="pagox">Pagos Divergentes</option>
                                            <option value="baixado">Baixado</option>                                            
                                        </select>
                                    </div>                                        
                                </div>
                            </div>
                            <div style="clear:both"></div>
                            <div class="boxhead">
                                <div class="boxtext">Retorno de pagamentos - Todos</div>
                            </div>
                            <div class="boxtable">
                                <table id="tbRetorno" class="table" cellpadding="0" cellspacing="0" width="100%">                                    
                                    <thead>
                                        <tr>
                                            <th width="3%" style="text-align: center;"><input id="checkAll" type="checkbox" class="checkall"/></th>
                                            <th width="9%" style="text-align: center;">Referência</th>
                                            <th width="9%" style="text-align: center;">Código</th>
                                            <th width="22%" style="text-align: left;">Nome do Cliente</th>
                                            <th width="9%" style="text-align: center;">Data Ref.</th>                                            
                                            <th width="9%" style="text-align: center;">Dt. Venc.</th>                                                        
                                            <th width="9%" style="text-align: center;">Dt. Pagto</th>
                                            <th width="9%" style="text-align: center;">Valor Ref.</th>                                                                                        
                                            <th width="9%" style="text-align: center;">Valor Mens.</th>
                                            <th width="9%" style="text-align: center;">Valor Pag.</th>
                                            <th width="3%" style="text-align: center;"></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Total:</th>
                                            <th>0</th>
                                            <th colspan="2"><?php echo $nome; ?></th>                                            
                                            <th>Total Filtro:</th>
                                            <th>0</th>                                                                                        
                                            <th>Valor Filtro:</th>
                                            <th>0,00</th>
                                            <th>0,00</th>
                                            <th>0,00</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                                <div style="clear:both"></div>
                                <input name="txtNome" id="txtNome" value="<?php echo $nome; ?>" type="hidden"/>
                                <div class="widgets"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" style="display: none;" title="Processando">
            <div id="progressbar"></div>
            <div style="display: flex;align-items: center;justify-content: center;margin-top: 20px;">
                <div id="pgIni">0</div><div style="padding: 5px;">até</div><div id="pgFim">0</div>            
            </div>            
        </div>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />        
        <script type="text/javascript" src="../../fancyapps/source/jquery.fancybox.js?v=2.1.4"></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/maskedinput/jquery.maskedinput-1.3.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='../../js/moment.min.js'></script>        
        <script type='text/javascript' src='../../js/util.js'></script>        
        <script type="text/javascript">

            $('.data-mask').mask('99/99/9999');
            
            function ler() {
                $("#source").dialog({modal: true});
                $("#progressbar").progressbar({value: 0});
                processarLer(0);
            }
            
            function processarLer(pagina) {
                let retPrint = "";
                if ($("#mscPlano").val() !== null) {
                    retPrint = retPrint + "&itemsPlano=" + $("#mscPlano").val();
                }                
                $.post("../Clube/form/baixa-layout_server_processing.php", 
                "txtNome=" + $("#txtNome").val() + "&txtPage=" + pagina + "&txtLayout=" + $("#txtLayout").val() + 
                "&txt_dt_baixa=" + $("#txt_dt_baixa").val() + "&txt_dt_pag=" + $("#txt_dt_pag").val() + retPrint).done(function (json) {
                    let data = $.parseJSON(json);
                    let PgFim = Math.ceil(data.iTotalRecords / data.iTotalDisplayRecords);
                    $("#progressbar").progressbar({value: ((pagina * 100) / PgFim)});
                    if ((pagina < PgFim) && PgFim > 0 && $('#source').dialog('isOpen')) {
                        $("#pgIni").html((pagina + 1));
                        $("#pgFim").html(PgFim);
                        $.each(data.aaData, function(index, value) {
                            $("#tbRetorno tbody").append(value);
                        });
                        refreshTotal();
                        processarLer((pagina + 1));
                    } else {
                        $("#tbRetorno tfoot th:eq(1)").text($("#tbRetorno tfoot th:eq(4)").text());
                        $("#source").dialog('close');
                    }
                });
            }

            function baixar() {
                if ($("#txtLayout").val() === "") {
                    bootbox.alert("Selecione um Layout para baixa!");
                } else {
                    bootbox.confirm('Confirma o baixa das mensalidades selecionadas?', function (result) {
                        if (result === true) {
                            $("#source").dialog({modal: true});
                            $("#progressbar").progressbar({value: 0});
                            $("#pgIni").html("0");
                            $("#pgFim").html($('input:checkbox:checked:not(".checkall"):visible').length);
                            processar();
                        }
                    });
                }
            }            

            function processar() {
                var PgIni = parseInt($("#pgIni").html());
                var PgFim = parseInt($("#pgFim").html());
                if ((PgIni < PgFim) && PgFim > 0 && $('#source').dialog('isOpen')) {
                    let selectedRow = $('input:checkbox:checked:not(".checkall"):visible')[PgIni];
                    let IdChk = selectedRow.value;
                    let pessoa = $(selectedRow).closest("tr").find("td:eq(2)").text();
                    let data_pagamento = $(selectedRow).closest("tr").find("td:eq(4)").text();
                    let valor_pagamento = $(selectedRow).closest("tr").find("td:eq(7)").text();
                    let valor_mensalidade = $(selectedRow).closest("tr").find("td:eq(8)").text();
                    $("#progressbar").progressbar({value: ((PgIni * 100) / PgFim)});
                    $("#pgIni").html(PgIni + 1);
                    $.post("../Clube/form/baixa-layout.php", "btnSave=S&txtId=" + IdChk +
                            "&txtPessoa=" + pessoa + "&txtDataPg=" + data_pagamento + "&txtValorPg=" + valor_pagamento +
                            "&txtMensalidade=" + valor_mensalidade + "&txtLayout=" + $("#txtLayout").val()).done(function (data) {
                        if (data.trim() === "YES") {
                            $(selectedRow).closest("tr").removeClass($(selectedRow).closest("tr").attr("class"));
                            $(selectedRow).closest("tr").addClass("pago");
                            $(selectedRow).closest("tr").find("td:eq(6)").text(moment().format('DD/MM/YYYY'));
                            $(selectedRow).closest("tr").find("td:eq(9)").text(valor_pagamento);
                            $(selectedRow).closest("tr").find("td:eq(10)").html("<center><img style=\"height: 16px;\" src=\"../../img/pago.png\"></center>");
                        }
                        processar();
                    });
                } else {
                    $("#tbRetorno tbody tr.pago input[type='checkbox']").prop('checked', false).parent('span').removeClass('checked');
                    $("#tbRetorno tbody tr.pago input[type='checkbox']").attr("disabled", true);
                    $("#source").dialog('close');
                    loadContasVariaveis(parseInt(PgFim));
                }
            }

            $('#txtLayout').change(function () {
                showData();
            });

            function showData() {
                if ($("#txtLayout option:selected").attr('datas') === "0") {
                    $("#txt_dt_baixa, #txt_dt_pag").parent().show();
                } else {
                    $("#txt_dt_baixa, #txt_dt_pag").parent().hide();
                }
            }

            $('#txtLayoutStatus').change(function () {
                $(".boxtext").html("Retorno de pagamentos - " + $("#txtLayoutStatus option:selected").text());
                $("#loader").show();
                setTimeout(function(){
                    refreshTotal();
                }, 500);                     
            });
            
            function refreshTotal() {
                let totRef = 0;
                let totMen = 0;
                let totPag = 0;
                if ($('#txtLayoutStatus').val() === "null") {
                    $("#tbRetorno tbody tr").show();
                } else {
                    $("#tbRetorno tbody tr").hide();
                    $("#tbRetorno tbody tr." + $('#txtLayoutStatus').val()).show();
                }
                $("#tbRetorno tbody tr:visible").each(function (PgIni) {
                    let selectedRow = $('#tbRetorno tbody tr:visible')[PgIni];
                    totRef = totRef + textToNumber($(selectedRow).closest("tr").find("td:eq(7)").text());
                    totMen = totMen + textToNumber($(selectedRow).closest("tr").find("td:eq(8)").text());
                    totPag = totPag + textToNumber($(selectedRow).closest("tr").find("td:eq(9)").text());
                });
                $("#tbRetorno tfoot th:eq(4)").text($("#tbRetorno tbody tr:visible").length);
                $("#tbRetorno tfoot th:eq(6)").text(numberFormat(totRef));
                $("#tbRetorno tfoot th:eq(7)").text(numberFormat(totMen));
                $("#tbRetorno tfoot th:eq(8)").text(numberFormat(totPag));
                $("#loader").hide();
            }
            
            function loadContasVariaveis(total) {         
                let valor = textToNumber($("#txtLayout option:selected").attr('taxa'));       
                if (valor > 0 && total > 0) {
                    abrirTelaBox("FormSolicitacao-de-Autorizacao.php?total=" + (total * valor), 320, 400);
                }
                console.log(total);       
                console.log(valor);                
            }
            
            function FecharBox() {
                $("#newbox").remove();
            }
            
            function AbrirCli(id) {
                window.open("./../Academia/ClientesForm.php?id=" + id, '_blank');         
            }
            
            showData();
            
            if ($("#txtLayout").val() !== "null") {
                $("#tbRetorno tbody").html("");
                ler();
            }

        </script>        
    </body>
<?php odbc_close($con); ?>
</html>