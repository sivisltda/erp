<?php include "form/cupomConviteForm.php"; ?>      
<html> 
    <head>
        <title>SIVIS - Cupom Fiscal</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script> 
        <style type="text/css">
            body {
                font-size: 10px;
                font-family: Tahoma;
            }
            table {
                width: 100%;
                max-width: 302px;
                font-size: 10px;
                font-family: Tahoma;
            }
            hr {
                width: 100%;
                max-width: 302px;
                border-top: dotted 1px;
                float: left;
            }
            .emp {
                font-size: 10px;
                font-weight: bold;
                text-align: center;
                padding: 5px 0px 5px 0px;
            }
            .end {
                text-align: center;
                padding: 5px 0px 5px 0px;
            }
            .doc {
                padding-top: 5px;
            }
            .lin {
                border-bottom: 1px solid #000;
            }
            .spc {
                padding-bottom: 5px;
            }
            .grupo {
                padding: 3px 0px 3px 0px;
                background-color: #DDD;
                font-weight: bold;
            }
            .tit {
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                padding: 5px 0px 10px 0px;
            }
            .num {
                font-size: 20px;
                font-weight: bold;
                height: 27px;
            }
            .esp {
                background-color: gray;
                color: white;                
            }
        </style>
    </head>
    <body>
        <input id="txtMnContrato" type="hidden" value="<?php echo $contrato; ?>" />        
        <input id="txtMnFilial" type="hidden" value="<?php echo $filial; ?>" />
        <input id="txtMnLanguage" type="hidden" value="<?php echo $languages; ?>" />  
        <?php foreach ($convitesArray as $row) { 
        if ($convite_tipo_ == 1) { ?>
        <div style="margin-top: 10px; page-break-before: always;"> 
        <?php } ?>
        <table cellspacing="0" cellpadding="0">
        <?php if ($convite_tipo_ == 0 ) { ?>
            <tr>
                <td colspan="3" class="tit"><?php echo $nome_fantasia; ?></td>
            </tr>       
            <tr>
                <td colspan="3" class="end">
                    <?php echo (strlen($cedente) > 0 ? $cedente."<br>" : "");?>
                    <?php echo (strlen($endereco) > 0 ? $endereco."<br>" : "");?>
                    <?php echo (strlen($cidade_uf) > 0 ? $cidade_uf : "");?>
                    <?php echo (strlen($cep) > 0 ? " - CEP: " . $cep : "");?>
                    <br>
                    <?php echo (strlen($telefone) > 0 ? $telefone."<br>" : "");?>
                    <?php echo (strlen($site) > 0 ? $site."<br>" : "");?>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="doc"><b>CNPJ: <?php echo $cpf_cnpj; ?></b><br/>I.E.: <?php echo $inscricao; ?></td>
            </tr>
            <tr>
                <td colspan="3" class="lin"></td>
            </tr>
            <tr>
                <td colspan="3" class="num" style="font-size: 14px; padding: 5px;">CONVITE</td>
            </tr>
        <?php } ?>
            <tr class="esp">
                <td colspan="3" style="padding: 5px; text-align: center;" class="num"><?php echo (strlen($row["descricao_agenda"]) > 0 ? $row["descricao_agenda"] : "CONVITE AVULSO"); ?></td>
            </tr>
            <tr>
                <td colspan="3" class="lin"></td>
            </tr>               
            <tr>
                <td style="width: 20%;" class="doc">Titular:</td>
                <td colspan="2"><?php echo $row["titular"]; ?></td>
            </tr>            
            <tr>
                <td class="doc">Convidado:</td>
                <td colspan="2" class="doc"><?php echo $row["convidado"]; ?></td>
            </tr>                        
            <tr>
                <td class="doc">Doc:</td>
                <td><?php echo $row["convite"]; ?></td>
                <td><?php echo $row["valorServico"]; ?></td>
            </tr>            
            <tr>
                <td colspan="3" class="lin"></td>
            </tr>                        
            <tr class="esp">
                <td class="doc"><b>Validade:</b></td>
                <td colspan="2" class="num"><?php echo $row["validade"]; ?></td>
            </tr>
            <tr>
                <td colspan="3" class="lin"></td>
            </tr>                        
            <tr>            
                <td></td>
                <td colspan="2" class="num"><b><?php echo str_pad($row["codigo"], 6, "0", STR_PAD_LEFT); ?></b></td>
            </tr>
            <tr>
                <td colspan="3" class="lin"></td>
            </tr>            
            <tr>
                <td colspan="3" class="doc">Usuário: <?php echo $row["operador"]; ?></td>
            </tr>            
        </table>
        <?php if ($convite_tipo_ == 0) { ?>
            <hr><br><br><br>
        <?php } else { ?>
            </div>
        <?php }} ?>
    </body>
    <script type="text/javascript" src="../../js/util.js"></script>     
    <?php if(!isset($_GET["PathArq"])) { ?>
        <script type="text/javascript">
            $(window).load(function () {
                window.print();
                setTimeout(function () {
                    window.close();
                }, 500);
            });
        </script>    
    <?php } ?>
</html>