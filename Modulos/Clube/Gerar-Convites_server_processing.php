<?php

require_once(__DIR__ . '/../../Connections/configini.php');
$aColumns = array('id_convite', 'dt_ini', 'dt_fim', 'documento', 'nome_convidado', 'operador', 'item_venda');
$table = "sf_convites";
$PageName = "Gerar-Convites";
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY " . $aColumns[0] . " asc ";
$sOrder2 = " ORDER BY " . $aColumns[0] . " asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) < count($aColumns) - 1) {
                $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 0) . " " . $_GET['sSortDir_' . $i] . ", ";
                $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 1) . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY " . $aColumns[0] . " asc";
        $sOrder2 = " ORDER BY " . $aColumns[0] . " asc";
    }
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $sWhereX .= " and dt_ini between " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']) . "";
}

if (is_numeric($_GET['ps'])) {
    $sWhereX .= " and id_fornecedor = " . valoresNumericos2($_GET['ps']);
}

if (is_numeric($_GET['st'])) {
    $sWhereX .= " and inativo = " . $_GET['st'];
}

if (is_numeric($_GET['ag'])) {
    $sWhereX .= " and id_agendamento = " . valoresNumericos2($_GET['ag']);
}

if ($_GET['txtBusca'] != "") {
    $sWhere .= " and (documento like '%" . $_GET['txtBusca'] . "%' or nome_convidado like '%" . $_GET['txtBusca'] . "%')";
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($i == 0) {
        $colunas = $aColumns[$i];
    } else {
        $colunas = $colunas . "," . $aColumns[$i];
    }
}

$sQuery = "set dateformat dmy; SELECT COUNT(*) total from " . $table . " where " . $aColumns[0] . " > 0 " . $sWhereX . $sWhere;
$cur = odbc_exec($con, $sQuery);
while ($aRow = odbc_fetch_array($cur)) {
    $iFilteredTotal = $aRow['total'];
}

$sQuery = "set dateformat dmy; SELECT COUNT(*) total from " . $table . " where " . $aColumns[0] . " > 0 " . $sWhereX;
$cur = odbc_exec($con, $sQuery);
while ($aRow = odbc_fetch_array($cur)) {
    $iTotal = $aRow['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "set dateformat dmy; SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row," . $colunas . "
from " . $table . " where " . $aColumns[0] . " > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder2;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<center><input type=\"checkbox\" class=\"caixa\" name=\"items[]\" value=\"" . $aRow[$aColumns[0]] . "\"/></center>";    
    $row[] = "<center><a href='javascript:void(0)' onClick='AbrirBox(" . utf8_encode($aRow[$aColumns[0]]) . ")'><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[0]]) . "'>" . utf8_encode($aRow[$aColumns[0]]) . "</div></a></center>";
    $row[] = "<center>" . escreverData($aRow[$aColumns[1]]) . "</center>";
    $row[] = "<center>" . escreverData($aRow[$aColumns[2]]) . "</center>";
    $row[] = "<center>" . utf8_encode($aRow[$aColumns[3]]) . "</center>";
    $row[] = utf8_encode($aRow[$aColumns[4]]);
    $row[] = "<center>" . utf8_encode($aRow[$aColumns[5]]) . "</center>";
    $row[] = "<center>" . (is_numeric($aRow[$aColumns[6]]) ? "SIM" : "NÃO") . "</center>";
    if ($imprimir == 0) {        
        $detalhe = ($ckb_clb_excluir_convites_ > 0 ? "<li><a onclick=\"excluirConvite(" . $aRow[$aColumns[0]] . ")\"><input type='image' src=\"../../img/1365123843_onebit_33 copy.PNG\" width='16' height='16'> Cancelar Convite</a></li>" : "") .
        "<li><a onclick=\"imprimirConvite(" . $aRow[$aColumns[0]] . ")\"><input type='image' src=\"../../img/print.png\" width='16' height='16'> Imprimir Convite</a></li>";        
        $row[] = "<center><div style=\"text-align:left;\" class=\"btn-group\">
        <button style=\"height: 13px;background-color: gray;\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\"><span style=\"margin-top: 1px;\" class=\"caret\"></span></button>
        <ul style=\"left:-130px;\" class=\"dropdown-menu\">" . $detalhe . "</ul>
        </div></center>";        
    }
    $output['aaData'][] = $row;
}
if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
