<?php
include '../../Connections/configini.php';

$DescPagina = "Gestão de Documentos";
$PageName = "Clientes-Documentos";
$Module = "Clube";
$aColumnsX = array('', 'Matricula', 'CPF/CNPJ', 'Nome', 'Plano', 'Nascimento', 'Documento', 'Inicio', 'Validade');
$aColumnsP = array('2', '5', '9', '23', '22', '8', '12', '7', '7');

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/bootbox.css"/>        
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>                       
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1><?php echo $Module; ?><small><?php echo $DescPagina; ?></small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">  
                                <div style="margin-top: 15px;float:left;">
                                    <div style="float:left">
                                        <form method="POST">
                                            <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir()" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                        </form>
                                    </div>
                                </div>                                
                                <div style="float: right;width: 620px;"> 
                                    <div style="float:left;margin-left: 0.5%;max-width: 230px;min-width: 230px;">
                                        <span style="display:block;">Documento:</span>
                                        <select id="txtDocumento" name="txtDocumento">
                                            <option value="null">Selecione</option>
                                            <?php $cur = odbc_exec($con, "select id_doc, descricao_doc from sf_documentos") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id_doc'] ?>"><?php echo utf8_encode($RFP['descricao_doc']); ?></option>
                                            <?php } ?>
                                        </select>                                            
                                    </div>                              
                                    <div style="float:left; margin-left:0.5%;">
                                        <span style="display:block;">Validade De:</span>
                                        <input id="txt_dt_begin" type="text" class="datepicker" style="width:79px; margin-bottom:2px" value="">
                                    </div>            
                                    <div style="float:left; margin-left:0.5%;">
                                        <span style="display:block;">até:</span>
                                        <input id="txt_dt_end" type="text" class="datepicker" style="width:79px; margin-bottom:2px" value="" >
                                    </div>                                    
                                    <div style="float:left;margin-left: 0.5%;">
                                        <div width="100%">Busca:</div>
                                        <input name="txtBusca"  id="txtBusca" maxlength="100" type="text" style="width:175px;" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>"/>
                                    </div>
                                    <div style="margin-top: 15px;float:left;margin-left:0.3%;">
                                        <div style="float:left">
                                            <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext"><?php echo $DescPagina; ?></div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tbCliente">
                            <thead>
                                <tr>
                                    <?php for ($i = 0; $i < count($aColumnsX); $i++) { ?>
                                        <th width="<?php echo $aColumnsP[$i]; ?>%"><?php echo $aColumnsX[$i]; ?></th>
                                    <?php } ?>
                                    <th width="5%"><center>Ação:</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>        
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/moment.min.js"></script>        
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type="text/javascript" src="../../js/util.js"></script>           
        <script type="text/javascript">

            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
            let arrayOptions = [{t: 'Selecione', v: 'null'}];
            $.get("./../Academia/ajax/Clientes_docs_server_processing.php?op=rs&id_forn=0&servico_doc=S").done(function (data) {
                var json = JSON.parse(data);
                if (json !== null) {
                    for (var x = 0; x < json.length; x++) {
                        arrayOptions.push({t: json[x].desc, v: json[x].id});
                    }
                }
            });
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }

            function finalFind(imp) {
                var retPrint = "";
                retPrint = '?imp=0';
                if (imp === 1) {
                    retPrint = '&imp=1';
                }
                if ($("#txt_dt_begin").val() !== "") {
                    retPrint = retPrint + "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "_");
                }
                if ($("#txt_dt_end").val() !== "") {
                    retPrint = retPrint + "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "_");
                }                
                if ($("#txtDocumento").val() !== "null") {
                    retPrint = retPrint + "&doc=" + $("#txtDocumento").val();
                }                
                if ($("#txtBusca").val() !== "") {
                    retPrint = retPrint + '&txtBusca=' + $("#txtBusca").val();
                }
                return "Clientes-Documentos_server_processing.php" + retPrint.replace(/\//g, "_");
            }
            
            function imprimir() {
                if ($("#txtDocumento").val() !== "null") {
                    let label = ($("#txtDocumento").val() !== "null" ? "Documento: " + $("#txtDocumento option:selected").text() : "") +
                    ($("#txt_dt_begin").val() !== "" && $("#txt_dt_end").val() !== "" ? ", validade de " + $("#txt_dt_begin").val() + " até " + $("#txt_dt_end").val() : "");
                    var pRel = "&NomeArq=" + "Gestão de Documentos" +
                            "&lbl=Mat.|CPF|Nome|Plano|Nasc.|Documento|Inicio|Validade" +
                            "&siz=32|70|180|175|51|90|51|51" +
                            "&pdf=0,8" + // Colunas do server processing que não irão aparecer no pdf
                            "&filter=" + label + //Label que irá aparecer os parametros de filtro
                            "&PathArqInclude=" + "../Modulos/Clube/" + finalFind(1); // server processing
                    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
                } else {
                    bootbox.alert("Selecione um Documento!");
                }
            }
            
            $("#btnfind").click(function () {
                $("#tbCliente").dataTable().fnDestroy();
                listaTable();
            });

            function listaTable() {
                myTable = $('#tbCliente').dataTable({
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "aoColumns": [
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false}],
                    "sAjaxSource": finalFind(0),
                    'oLanguage': {
                        'oPaginate': {
                            'sFirst': "Primeiro",
                            'sLast': "Último",
                            'sNext': "Próximo",
                            'sPrevious': "Anterior"
                        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                        'sLengthMenu': "Visualização de _MENU_ registros",
                        'sLoadingRecords': "Carregando...",
                        'sProcessing': "Processando...",
                        'sSearch': "Pesquisar:",
                        'sZeroRecords': "Não foi encontrado nenhum resultado"},
                    "sPaginationType": "full_numbers"
                });
            }
            listaTable();
            
            var nEditing = null;
            var idEditing = "";
            var ixEditing = "";
            var exEditing = "";
            var dtEditing = "";
            
            $("body").on("click", "#tbCliente #btnAlt", function () {
                var nRow = $(this).parents('tr')[0];
                if (nEditing !== null) {
                    restoreRow(nEditing);
                } else {
                    nEditing = nRow;
                    idEditing = $(this).attr("data-id");
                    ixEditing = $(this).attr("data-ix");
                    exEditing = $(this).attr("data-ex");
                    dtEditing = $(this).attr("data-dt");
                    let jqTds = $('>td', nRow);
                    jqTds[6].innerHTML = '<select id="txtEditExame_' + idEditing + '">' + arrayOptions.map(function(i) {return '<option value="' + i.v + '">' + i.t + '</option>'; }).join("") + '</select>';
                    jqTds[7].innerHTML = '<input id="txtEditData_' + idEditing + '" type="text" value="' + (dtEditing !== "" ? dtEditing : moment().format(lang["dmy"])) + '" class="datepicker inputCenter">';
                    jqTds[9].innerHTML = `<center><a href="javascript:;" data-id="` + idEditing + `" data-ix="` + ixEditing + `" class="btn green-haze save" title="Salvar" style=\"padding: 0px 4px;background:#5bb75b;\"><span class="ico-checkmark"></span></a>
                    <a href="javascript:;" data-id="` + idEditing + `" data-ix="` + ixEditing + `" class="btn red-haze cancel" title="Cancelar" style=\"padding: 0px 4px;background:red;\"><span class="ico-reply"></span></a><center>`;
                    $('#txtEditExame_' + idEditing).val(ixEditing);
                    $(".datepicker").datepicker({dateFormat: lang["dPickerFormat"]});
                    $(".datepicker").mask(lang["dateMask"]);                    
                }
            });

            myTable.on('click', '.cancel', function (e) {
                let nRow = $(this).parents('tr')[0];
                restoreRow(nRow);
            });

            function restoreRow(nRow) {
                let jqTds = $('>td', nRow);
                jqTds[6].innerHTML = exEditing;
                jqTds[7].innerHTML = dtEditing;
                jqTds[9].innerHTML = '<center><a href="javascript:;" data-id="' + idEditing + '" data-ix="' + ixEditing + '" data-ex="' + exEditing + '" data-dt="' + dtEditing + '" id="btnAlt"><span class="ico-edit" title="Alterar" style="font-size:18px;"></span></a><center>';
                nEditing = null;
                idEditing = "";
                ixEditing = "";
                exEditing = "";
                dtEditing = "";                
            }

            myTable.on('click', '.save', function (e) {
                let id = $(this).attr("data-id");  
                let ix = $(this).attr("data-ix");              
                bootbox.confirm("Confirma o alteração do registro?", function (result) {
                    if (result === true) {
                        if ($.isNumeric(ix) && $.isNumeric($("#txtEditExame_" + id).val()) && moment($("#txtEditData_" + id).val(), lang["dmy"]).isValid()) {
                            $.get("../Academia/ajax/Clientes_docs_server_processing.php?op=u&id_forn=" + id + "&id_doc=" + ix + "&id_docx=" + $("#txtEditExame_" + id).val() + "&dt_docx=" + $("#txtEditData_" + id).val()).done(function (data) {
                                var json = JSON.parse(data);
                                if (json.result === "YES") {
                                    $("#tbCliente").dataTable().fnDestroy();
                                    listaTable();
                                } else {
                                    bootbox.alert(json.result);
                                }
                            });
                        } else if (!$.isNumeric(ix) && $.isNumeric($("#txtEditExame_" + id).val()) && moment($("#txtEditData_" + id).val(), lang["dmy"]).isValid()){
                            $.get("../Academia/ajax/Clientes_docs_server_processing.php?op=c&id_forn=" + id + "&id_doc=" + $("#txtEditExame_" + id).val() + "&dt_doc=" + $("#txtEditData_" + id).val()).done(function (data) {
                                var json = JSON.parse(data);
                                if (json.result === "YES") {
                                    $("#tbCliente").dataTable().fnDestroy();
                                    listaTable();
                                } else {
                                    bootbox.alert(json.result);                                    
                                }
                            });
                        } else if ($.isNumeric(ix) && !$.isNumeric($("#txtEditExame_" + id).val())) {
                            $.get("../Academia/ajax/Clientes_docs_server_processing.php?op=d&id_forn=" + id + "&id_doc=" + ix).done(function (data) {
                                var json = JSON.parse(data);
                                if (json.result === "YES") {
                                    $("#tbCliente").dataTable().fnDestroy();
                                    listaTable();
                                } else {
                                    bootbox.alert(json.result);                                    
                                }
                            });   
                        }
                    }
                });
            });            

        </script>         
        <?php odbc_close($con); ?>
    </body>
</html>
