<?php
include '../../Connections/configini.php';
$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_eventos set " .
        "pessoa_evento = " . valoresSelect("txtAluno") . "," .
        "agendamento_evento = " . valoresSelect("txtAgendamento") . "," .                
        "local_evento = " . valoresSelect("txtLocal") . "," .
        "observacao_evento = " . valoresTexto("txtDescricao") . 
        " where id_evento = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_eventos (pessoa_evento, agendamento_evento, local_evento, observacao_evento) values (" .
        valoresSelect("txtAluno") . "," .
        valoresSelect("txtAgendamento") . "," .
        valoresSelect("txtLocal") . "," .
        valoresTexto("txtDescricao") . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_evento from sf_eventos order by id_evento desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
    if (isset($_POST['txtContatosTotal'])) {
        $max = $_POST['txtContatosTotal'];
        $notIn = '0';
        for ($i = 0; $i <= $max; $i++) {
            if (isset($_POST['txtIdContato_' . $i]) && is_numeric($_POST['txtIdContato_' . $i])) {
                if ($_POST['txtIdContato_' . $i] != 0) {
                    $notIn = $notIn . "," . $_POST['txtIdContato_' . $i];
                }
            }
        }
        odbc_exec($con, "delete from sf_eventos_convidado where evento_convidado = " . $_POST['txtId'] . " and id_evento_convidado not in (" . $notIn . ")");
        for ($i = 0; $i <= $max; $i++) {
            if (isset($_POST['txtIdContato_' . $i])) {
                if (!is_numeric($_POST['txtIdContato_' . $i])) {
                    odbc_exec($con, "insert into sf_eventos_convidado(evento_convidado, pessoa_convidado, id_convidado) values(" .
                    $_POST['txtId'] . "," . valoresTexto('txtTxContato_' . $i) . "," . valoresSelect('txtIdPessoa_' . $i) . ")") or die(odbc_errormsg());
                }
            }
        }
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_eventos WHERE id_evento = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_eventos 
    inner join sf_fornecedores_despesas on sf_eventos.pessoa_evento = sf_fornecedores_despesas.id_fornecedores_despesas        
    where id_evento = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id =  utf8_encode($RFP['id_evento']);
        $pessoa =  utf8_encode($RFP['pessoa_evento']);
        $pessoaNome =  utf8_encode($RFP['razao_social']);
        $agendamento =  utf8_encode($RFP['agendamento_evento']);
        $local =  utf8_encode($RFP['local_evento']);
        $descricao =  utf8_encode($RFP['observacao_evento']);
    }
} else {
    $disabled = '';
    $id = '';
    $pessoa = '';
    $pessoaNome = '';
    $agendamento = '';
    $local = '';
    $descricao = '';
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script> 
<script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<body>
    <form action="FormEventos.php" name="frmEventos" method="POST">
        <div class="frmhead">
            <div class="frmtext">Eventos</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont">
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <div style="width: 100%; float:left;">
                <span>Cliente / Prospect:</span>
                <input type="hidden" id="txtAluno" name="txtAluno" value="<?php echo $pessoa; ?>">
                <input type="text" id="txtAlunoNome" style="width:100%" value="<?php echo $pessoaNome; ?>" <?php echo $disabled; ?>/>
            </div>      
            <div style="width: 50%;float: left;">
                <span>Agendamento:</span>
                <select name="txtAgendamento" id="txtAgendamento" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                    <option value="null">Selecione</option>
                    <?php 
                    if (is_numeric($pessoa)) {
                        $cur = odbc_exec($con, "select id_agendamento,assunto from sf_agendamento
                        where inativo = 0 and id_fornecedores = " . $pessoa . " order by id_agendamento") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option value="<?php echo $RFP['id_agendamento'] ?>"<?php
                            if (!(strcmp($RFP['id_agendamento'], $agendamento))) {
                                echo "SELECTED";
                            }
                            ?>><?php echo $RFP['id_agendamento'] . " - " . utf8_encode($RFP['assunto']); ?></option>
                    <?php }} ?>
                </select>                
            </div>                
            <div style="width: 49%;float: left; margin-left: 1%">
                <span>Locações:</span>
                <select name="txtLocal" id="txtLocal" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                    <option value="null">Selecione</option>
                    <?php $cur = odbc_exec($con, "select id_loc, nome_loc from sf_locacoes order by id_loc") or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) { ?>
                        <option value="<?php echo $RFP['id_loc'] ?>"<?php
                        if (!(strcmp($RFP['id_loc'], $local))) {
                            echo "SELECTED";
                        }
                        ?>><?php echo utf8_encode($RFP['nome_loc']); ?></option>
                    <?php } ?>
                </select>                
            </div>                    
            <div style="width: 100%; float: left;">
                <span>Observações:</span>
                <input name="txtDescricao" id="txtDescricao" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="512" value="<?php echo $descricao; ?>"/>
            </div>
            <div style="width: 100%; float: left;">
                <span>Convidado:</span>
                <div style="width:100%; float:left">
                    <div style="background:#F1F1F1; border:1px solid #DDD; border-bottom:0; padding:10px 10px 0">
                        <div style="float:left; width:calc(98% - 70px);">
                            <input id="txtCodConvidado" type="hidden" value=""/>                            
                            <input id="txtNomeConvidado" type="text" class="input-medium" maxlength="512" autocomplete="off">
                        </div>
                        <div style="float:left; margin-left:1%;">
                            <button type="button" onclick="addConvidado();" <?php echo $disabled; ?> class="btn dblue" style="height: 26px;background-color: #308698 !important;">
                                <span style="margin-top: 4px;" class="ico-plus icon-white"></span>
                            </button>
                            <div style="text-align:left;" class="btn-group">
                                <button style="height: 26px;background-color: gray;" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                    <span style="margin-top: 1px;" class="caret"></span>
                                </button>
                                <ul style="left:-130px;" class="dropdown-menu">
                                    <li style="">
                                        <a data-id="274" style="<?php echo ($disabled == "" ? "" : "color: lightgray;"); ?>" onclick="<?php echo ($disabled == "" ? "importFile()" : ""); ?>">Importar Lista</a>
                                    </li>
                                    <li style="">
                                        <a data-id="274" onclick="printLista('E')">Exportar Lista</a>
                                    </li>
                                    <li style="">
                                        <a data-id="274" onclick="printLista('I')">Imprimir Lista</a>
                                    </li>                                
                                </ul>
                            </div>                            
                        </div>
                        <input id="arquivo" name="arquivo" type="file" style="display: none;" value=""/>                        
                        <div style="clear:both"></div>
                    </div>
                    <div style="background:#F1F1F1; border:1px solid #DDD; border-top:0; padding:5px 10px 10px">
                        <div id="divContatos" style="height:250px; background:#FFF; border:1px solid #DDD; overflow-y:scroll;">
                        <?php $i = 0;
                        if (is_numeric($id)) {
                            $cur = odbc_exec($con, "select * from sf_eventos_convidado where evento_convidado = " . $id) or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { $i++; ?>
                            <div id="tabela_linha_<?php echo $i; ?>" style="padding:2px; border-bottom:1px solid #DDD; overflow: hidden;">
                                <input id="txtIdContato_<?php echo $i; ?>" name="txtIdContato_<?php echo $i; ?>" value="<?php echo $RFP["id_evento_convidado"];?>" type="hidden">
                                <input id="txtIdPessoa_<?php echo $i; ?>" name="txtIdPessoa_<?php echo $i; ?>" value="<?php echo $RFP["id_convidado"];?>" type="hidden">                                
                                <div style="line-height:27px; float:left"><?php echo utf8_encode($RFP["pessoa_convidado"]) . (strlen($RFP["id_convidado"]) > 0 ? " (" . $RFP["id_convidado"] . ")" : "");?></div> 
                                <div style="width: 5%; padding: 4px 0px 3px; float: right; cursor: pointer;" onclick="return removeLinha('<?php echo $i; ?>');">
                                    <span class="ico-remove" style="font-size:20px"></span>
                                </div>                                
                            </div> 
                        <?php }} ?>
                            <input id="txtContatosTotal" name="txtContatosTotal" type="hidden" value="<?php echo $i; ?>"/>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" onclick="return validar();" type="submit" name="bntSave" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK" ><span class="ico-reply"></span> Cancelar</button>
                    <?php
                    }
                } else { ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../../js/plugins.js'></script>
    <script type="text/javascript" src="../../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type='text/javascript' src='../../../js/plugins/datatables/fnReloadAjax.js'></script>
    <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>
    <script type="text/javascript">
        
        function importFile() {
            $("#arquivo").click();
        }
                        
        $("#arquivo").on('change', function() {              
            var form_data = new FormData();           
            form_data.append('arquivo', $('#arquivo').prop('files')[0]);           
            $.ajax({
                url: 'form/sendText.php',
                dataType: 'text', 
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,  
                type: 'post',
                success: function(data){
                    var itens = JSON.parse(data);
                    for(i = 0; i < itens.length; i++) {
                        if(itens[i].length > 0) {
                            $("#txtNomeConvidado").val(itens[i]);
                            addConvidado();
                        }
                    }
                }
            });                                          
        });

        function printLista(tp) {
            var pRel = "&NomeArq=" + "Lista de Convidados" +
                    "&lbl=" + "Nome|Documento CPF|Documento RG|Nascimento|Sangue" +
                    "&siz=" + "300|100|100|100|100" +
                    "&pdf=" + "7" +
                    "&filter=" + "Obs: " + $("#txtDescricao").val() + //Label que irá aparecer os parametros de filtro
                    "&PathArqInclude=" + final_url_Lista().replace("?", "&"); // server processing
            window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tp + "&PathArq=GenericModelPDF.php", '_blank');
        }               
        
        function final_url_Lista() {
            return "../Modulos/Clube/ajax/lista_convidados_server_processing.php?id=" + $("#txtId").val();
        }                

        $("#txtAlunoNome").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "../../Modulos/CRM/ajax.php",
                    dataType: "json",
                    data: {q: request.term, t: "C','P"},
                    success: function (data) {
                        response(data);
                    }
                });
            }, minLength: 3,
            select: function (event, ui) {
                $("#txtAluno").val(ui.item.id);
                $("#txtAlunoNome").val(ui.item.value);
                getAgendamento(ui.item.id);
            }
        });
        
        $("#txtNomeConvidado").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "../../Modulos/CRM/ajax.php",
                    dataType: "json",
                    data: {q: request.term, t: "C','P"},
                    success: function (data) {
                        response(data);
                    }
                });
            }, minLength: 3,
            select: function (event, ui) {
                $("#txtCodConvidado").val(ui.item.id);
                $("#txtNomeConvidado").val(ui.item.value);                
            }
        });                        
        
        function addConvidado() {  
            if($("#txtNomeConvidado").val() !== "") {
                var id = parseInt($("#txtContatosTotal").val()) + 1;
                var linha = "<div id=\"tabela_linha_" + id + "\" style=\"padding:2px; border-bottom:1px solid #DDD; overflow: hidden;\">" +
                "<input id=\"txtIdContato_" + id + "\" name=\"txtIdContato_" + id + "\" value=\"\" type=\"hidden\">" +
                "<input id=\"txtIdPessoa_" + id + "\" name=\"txtIdPessoa_" + id + "\" value=\"" + $("#txtCodConvidado").val() + "\" type=\"hidden\">" +
                "<input id=\"txtTxContato_" + id + "\" name=\"txtTxContato_" + id + "\" value=\"" + $("#txtNomeConvidado").val() + "\" type=\"hidden\">" +
                "<div style=\"line-height:27px; float:left\">" + $("#txtNomeConvidado").val() + ($("#txtCodConvidado").val().length > 0 ? " (" + $("#txtCodConvidado").val() + ")" : "") + "</div>" +
                "<div style=\"width: 5%; padding: 4px 0px 3px; float: right; cursor: pointer;\" onclick=\"return removeLinha('" + id + "');\">" +
                "<span class=\"ico-remove\" style=\"font-size:20px\"></span></div></div>";
                $("#divContatos").append(linha);
                $("#txtContatosTotal").val(id);
                $("#txtCodConvidado").val("");                
                $("#txtNomeConvidado").val("");
            }
        }
        
        function validar() {
            if ($('#txtAluno').val() === "") {
                bootbox.alert("Preencha o campo Cliente/Prospect!");
                return false;
            } else if ($('#txtAgendamento').val() === "null") {
                bootbox.alert("Preencha o campo Agendamento!");
                return false;
            } else if ($('#txtLocal').val() === "null") {
                bootbox.alert("Preencha o campo Locações!");
                return false;
            } else if ($('#txtDescricao').val() === "") {
                bootbox.alert("Preencha o campo Observação!");
                return false;
            }
            return true;
        }
        
        function removeLinha(id) {
            $("#tabela_linha_" + id).remove();
        }
        
        function getAgendamento(pessoa) {
            $.getJSON('../Clube/agendamento.ajax.php', {txtPessoa: pessoa, ajax: 'true'}, function (j) {
                var conteudo = "<option value=\"null\">Selecione</option>";
                for (var i = 0; i < j.length; i++) {
                    conteudo += "<option value=\"" + j[i].id_agendamento + "\">" + j[i].assunto + "</option>";                                                                                
                }
                $("#txtAgendamento").html(conteudo);
            });
        }

    </script>
    <?php odbc_close($con); ?>
</body>
