<?php include '../../Connections/configini.php'; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Clube<small>Gestão de Agenda</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="float:left;width: 10%;margin-top: 15px;">
                                    <button class="button button-green btn-primary" type="button" onClick="AbrirBoxAgenda(0)"><span class="ico-file-4 icon-white"></span></button>
                                    <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimirAgenda()" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                </div>
                                <div style="float:right;width: 80%;">
                                    <input name="txtStatus" id="txtStatus" value="0" type="hidden"/>
                                    <input name="txtAgendamento" id="txtAgendamento" value="" type="hidden"/>
                                    <div style="float:left;width: 10%;">
                                        <div width="100%">De:</div>
                                        <input type="text" style="width: 79px;vertical-align: top;" id="txt_dt_begin" class="datepicker" value="<?php echo getData("T"); ?>" placeholder="Data inicial">
                                    </div>
                                    <div style="float:left;width: 10%;">
                                        <div width="100%">Até:</div>
                                        <input type="text" style="width: 79px;vertical-align: top;" id="txt_dt_end" class="datepicker" value="<?php echo getData("T"); ?>" placeholder="Data Final">
                                    </div>                                     
                                    <div style="float:left;width: 16%;">                                    
                                        <div width="100%">Agenda:</div>
                                        <select id="txtAgenda" style="width: 100%;" class="select" multiple>
                                            <?php $cur = odbc_exec($con, "select id_agenda,descricao_agenda from sf_agenda where inativo = 0
                                                and ((user_resp is null or user_resp = " . $_SESSION["id_usuario"] . ") or (user_permissao is null or user_permissao in (select isnull(master, 0) from sf_usuarios where id_usuario = " . $_SESSION["id_usuario"] . ")))
                                                order by descricao_agenda") or die(odbc_errormsg());
                                                while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id_agenda'] ?>"><?php echo utf8_encode($RFP['descricao_agenda']);?></option> 
                                            <?php } ?>
                                        </select>                                           
                                    </div> 
                                    <div style="float:left; width:25%; margin-left:1%;">
                                        <span>Nome:</span>
                                        <div style="clear:both"></div>
                                        <span id="sprytextfield3">
                                            <input name="txtAluno" id="txtAluno" value="0" type="hidden"/>
                                            <input name="txtAlunoNome" id="txtAlunoNome" value="" type="text" class="inputbox" size="10" />
                                        </span>
                                    </div>                                      
                                    <div style="float:left;margin-left: 0.5%;width: 11%;">                                    
                                        <div width="100%">Concluído:</div>
                                        <select id="txtConcluido">
                                            <option value="null">Selecione</option>                               
                                            <option value="0">NÃO</option>                                                                                    
                                            <option value="1">SIM</option>                                        
                                        </select>                                           
                                    </div>                                                                                                                                                                                     
                                    <div style="float:left;margin-left: 0.5%;width: 20%;">
                                        <div width="100%">Busca:</div>
                                        <input id="txtBusca" maxlength="100" type="text" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>" title=""/>
                                    </div>
                                    <div style="float:left;margin-left: 0.5%;width: 5%;margin-top: 15px;">                                        
                                        <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Agendamentos</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblLista">
                            <thead>
                                <tr>
                                    <th width="10%">Data Início:</th>
                                    <th width="10%">Data Fim:</th>
                                    <th width="10%">Agenda:</th>                                                                                                                                    
                                    <th width="25%">Cliente:</th>                                               
                                    <th width="10%">Assunto:</th>                                                                                                
                                    <th width="14%">Observação:</th>                                                                                                                                            
                                    <th width="8%"><center>Concluído:</center></th> 
                                    <th width="8%"><center>Operador:</center></th> 
                                    <th width="5%"><center>Ação:</center></th> 
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>      
                    <?php include "../Clube/include/FormGerar-Convites.php"; ?>
                </div>
            </div>
        </div>       
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>   
        <script type='text/javascript' src='../../js/plugins.js'></script>        
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type='text/javascript' src='../../js/plugins/highlight/jquery.highlight-4.js'></script>        
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>            
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script>  
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>
        <script type='text/javascript' src='../../Modulos/Clube/js/FormGerar-Convites.js'></script>        
        <script type="text/javascript">
            
            $("#btnfind").click(function () {
                tbLista.fnReloadAjax(finalFindAgenda(0));
                $("#example").dataTable().fnDestroy();
                listaTable();                
            });

            function finalFindAgenda(imp) {
                var retPrint = (imp === 1 ? '&imp=1' : '?imp=0');
                if ($("#txtAgenda").val() !== "null") {
                    retPrint += '&fq=' + $("#txtAgenda").val();
                }
                if ($("#txtConcluido").val() !== "null") {
                    retPrint += '&cl=' + $("#txtConcluido").val();
                }
                if ($("#txtAluno").val() !== "" && $("#txtAlunoNome").val() !== "") {
                    retPrint += "&ps=" + $("#txtAluno").val();
                }                
                if ($('#txt_dt_begin').val() !== "") {
                    retPrint += "&dti=" + $('#txt_dt_begin').val().replace(/\//g, "_");
                }
                if ($('#txt_dt_end').val() !== "") {
                    retPrint += "&dtf=" + $('#txt_dt_end').val().replace(/\//g, "_");
                }
                if ($("#txtBusca").val() !== "") {
                    retPrint += '&Search=' + $("#txtBusca").val();
                }                
                return "../Comercial/Gerenciador-Agenda-Compromissos_server_processing.php" + retPrint.replace(/\//g, "_");
            }

            function AbrirBoxAgenda(id) {
                abrirTelaBox("../Comercial/FormAgenda.php" + (id > 0 ? "?id=" + id : ""), 465, 500);
            }

            function FecharBoxF() {
                $("#newbox").remove();
                tbLista.fnReloadAjax(finalFindAgenda(0));
            }

            function imprimirAgenda() {
                let filtros = '';
                if($("#txtAgenda").val() !== "null"){
                    filtros += "Agenda: " + $("#txtAgenda option:selected").text();
                }                
                var pRel = "&NomeArq=" + "Agendamentos" + "&pOri=L" +
                        "&lbl=Início|Fim|Agenda|Cliente|Assunto|Observação|Concluído|Operador" +
                        "&siz=80|80|150|200|100|250|60|90" +
                        "&pdf=9" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + filtros + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/Comercial/" + finalFindAgenda(1); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }                        

            var tbLista = $('#tblLista').dataTable({
                "iDisplayLength": 20,
                "aLengthMenu": [20, 30, 40, 50, 100],
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "aaSorting": [[0, 'desc']],
                "aoColumns": [
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true}],
                "sAjaxSource": finalFindAgenda(0),
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"},
                "sPaginationType": "full_numbers"
            });
            
            function selectConvite(id, texto, dt_ini, dt_fim, id_pessoa, nome_pessoa) { 
                $("#txtDataInicio").val(dt_ini);
                $("#txtDataFim").val(dt_fim);                
                $("#txtDataInicio, #txtDataFim").attr("readonly", (id > 0 ? true : false));
                $("#txtAgendamento").val((id > 0 ? id : ""));
                $("#lblGerarConvites").html("Convites" + (id > 0 ? " - " + texto : ""));
                if (id > 0) {
                    $("#txtAluno").val(id_pessoa);
                    $("#txtAlunoNome").val(nome_pessoa);                
                }
                $("#btnfind").click();
                $("#block").show();
                limpar();               
            }
            
        </script>        
        <?php odbc_close($con); ?>
    </body>
</html>